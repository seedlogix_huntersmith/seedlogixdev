<?php
session_start();
$site = strtolower($_SERVER['SERVER_NAME']);
$a = explode('.', $site);
if(count($a)>3 && $a[0]!='www')
	$domain = $a[count($a)-3].'.'.$a[count($a)-2].'.'.$a[count($a)-1];
else
	$domain = $a[count($a)-2].'.'.$a[count($a)-1];

if($domain!='6qube.com'){
	require_once('/home/sapphire/public_html/6qube.com/admin/inc/db_connector.php');
	$connector = new DbConnector();
	
	//if(!$_SESSION['siteInfo']){ //use session vars so db isn't called on every page load
		$query = "SELECT 
					* 
				FROM 
					resellers_network 
				WHERE 
					domain = 'http://".$site."'";
		if($_SESSION['siteInfo'] = $connector->queryFetch($query)){		
			$resellerID = $_SESSION['siteInfo']['admin_user'];
			
			$query = "SELECT path FROM themes_network WHERE id = ".$_SESSION['siteInfo']['theme'];
			$_SESSION['themeInfo'] = $connector->queryFetch($query);
			$_SESSION['themeInfo']['themeUrl'] = 'http://'.$site.'/network-themes/'.$_SESSION['themeInfo']['path'].'/';
			
			$query = "SELECT main_site FROM resellers WHERE admin_user = ".$resellerID;
			$a = $connector->queryFetch($query);
			$_SESSION['siteInfo']['reseller_main_site'] = $a['main_site'];
			
			include('/home/sapphire/public_html/6qube.com/browse/network-themes/'.$_SESSION['themeInfo']['path'].'/index.php');
		}
	//}
	//else include('/home/sapphire/public_html/6qube.com/browse/network-themes/'.$_SESSION[$site.'_themeInfo']['path'].'/index.php');
}

else {
	require_once('/home/sapphire/public_html/6qube.com/admin/inc/local.class.php');
	$local = new Local();
	
	isset($_GET['section']) ? $section = $_GET['section'] : $section = "";
	isset($_GET['category']) ? $category = $_GET['category'] : $category = "all";
	isset($_GET['state']) ? $state = $_GET['state'] : $state = "all";
	isset($_GET['city']) ? $city = $_GET['city'] : $city = "all";
	
	$select_state = NULL;
	$select_city = NULL;
	$select_section = $local->browseGetSections($section);
	$select_category = $local->browseGetCategories($section, $category);	
	if($section == 'directory' || $section == 'hub'){
		$select_state = $local->browseGetStates($section, $category, $state);
		$select_city = $local->browseGetCities($section, $category, $state, $city);
	}
	//How many results do you want per heading?
	$limit = 20;
	//do not touch
	isset($_GET['offset']) ? $offset = $_GET['offset'] : $offset = 0;
	//$offset = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/search_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/search_favicon.png" />
<!--jquery library -->
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.hint.js"></script>
<script language="javascript" type="text/javascript" src="http://6qube.com/js/ajax_form.js"></script>
<script language="javascript" type="text/javascript" src="http://6qube.com/js/jquery.uniform.min.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="css/ddlevelsmenu-base.css" />
<link rel="stylesheet" type="text/css" href="http://6qube.com/css/uniform.default.css" />
<script type="text/javascript" src="js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="js/scroll.js"></script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="css/ie6.css" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript">
$(function(){ 
	$('input[title!=""]').hint();
	$("select").uniform();
});
</script> 
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
					<a href="/" title="Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube" ><img src="images/browse-logo.png" alt="Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube"  /></a></div>
			  </div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="http://local.6qube.com/" title="Local Yellow Pages Directory by 6Qube Local"><span>Local</span></a></li>
										<li><a href="http://browse.6qube.com/" title="Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube" class="activelink"><span>Browse</span></a></li>
										<li><a href="http://hubs.6qube.com/" title="Local Website Directory by 6Qube Local | Local Websites"><span>HUBs</span></a></li>
										<li><a href="http://press.6qube.com/" title="Local Press Directory by 6Qube Local | Local Press Releases"><span>Press</span></a></li>												
										<li><a href="http://articles.6qube.com/" title="Local Articles Directory by 6Qube Local | Local Articles"><span>Articles</span></a></li>
										<li><a href="http://blogs.6qube.com/" title="Local Blog Directory by 6Qube Local | Local Blogs"><span>Blogs</span></a></li>
										<li><a href="http://elements.6qube.com/" title="Free Business Listing | Internet Marketing Software"><span>Free Business Listing</span></a></li>
									</ul>
									
 
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					<br clear="all" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<div id="search">
			<?	
		// ** Search ** //
		require_once('/home/sapphire/public_html/6qube.com/includes/searchForm.inc.php');
?>
		</div><!-- /search -->  
					<br clear="all" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container 5-->
			<div id="container_5">
				<!--Start Blogs Container-->
				<div id="blogs_container">
					<!--Start Blogs Container Blogs Container Top-->
					<div id="blogs_container_top">
						<!--Start Blogs Container Blogs Container Bottom-->
						<div id="blogs_container_bottom">
						  <!--Start Breadcrumb-->
						  <div id="breadcrumb">
							<h1>Browse Local. Find Local.</h1>
						  </div>
						  <!--End Breadcrumb-->
						  <!--Start Blog Content-->
						  <div id="blog_content">
                            <div id="browse_search">
                            <form action="index.php" method="get" id="browse">
                                <label>You can narrow by <strong>Section</strong>:</label>
                                <?=$select_section?>
                                <br />
                                <label>And <strong>Category</strong>:</label>
                                <?=$select_category?>
                                <?=$select_state?>
                                <?=$select_city?>
                            </form>
                            </div>
                        <br /><br />
							<div id="browse_display">
							<?              
							$local->displayBrowse($section, $category, $state, $city, $offset, $limit);
							?>
							</div>
                          
						  </div>
						  	<!--End Blog Content-->
							<!--Start Blog Right Panel-->
						  <!--End Blog Right Panel-->
						  <br clear="all" />
						</div>
						<!--End Blogs Container Bottom-->
					</div>
					<!--End Blogs Container Top-->
				</div>
				<!--End Blogs Container-->
			</div>
			<!--End Container 5-->
		</div>
		<!--End Page Container-->
		<?	
		// ** Footer ** //
		require_once('/home/sapphire/public_html/6qube.com/includes/v2footer.inc.php');
?>
</body>
</html>
<? } ?>