<?php
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$dispHubs = true;
	$continue = true;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	}
	
	//title and meta description -- gets changed if viewing city or category
	if($siteInfo['seo_title']){
		$title = $siteInfo['seo_title'];
	}
	else $title = "".$resellerCompany." Business Website Directory | ".$resellerCompany." HUBs";
	if($siteInfo['seo_desc']){
		$meta_desc = $siteInfo['seo_desc'];
	}
	else $meta_desc = $resellerCompany." Local Business Websites. ".$resellerCompany." HUBs provides a direct connect to local business websites.";
	
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$hub = new Hub();
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);
		
		if($args_array[0]=="cities"){
			//List of cities
			//echo "cities";
			$dispHubs = false;
			$dispCities = true;
			$title = "Browse Local Business Websites by City | Local Websites";
			$meta_desc = "Find Local Business Websites by browsing ".$resellerCompany." Local City Display of local websites.";
		}
		else if(is_numeric($args_array[0])){
			//Paginated all listings
			//echo "all listings - page ".$args_array[0];
			$currPage = $args_array[0];
			$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			$title = "".$resellerCompany." Business Website Directory | Page ".$currPage;
			$meta_desc .= " Page ".$currPage." of results.";
			$dispCities = true;
		}
		else if($city = $hub->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $hub->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $hub->majorCity($city);
				$majorCity = true;
			}
			$cityLink = $hub->getCityLink($city, $state, $site, NULL, true);
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//Paginated listings for a city
					//echo "$city." listings, page #".$args_array[1];
					$dispCats = true;
					$currPage = $args_array[1];
					$mainOffset = ($mainLimit*$currPage)-$mainLimit;
					$title = $resellerCompany.' Website Directory for '.$city.', '.$state.' | Page '.$currPage;
					$meta_desc = 'Find '.$city.', '.$state.' local business Websites in '.$city.' '.$hub->getStateFromAbv($state).'. '.$resellerCompany.' HUBs '.$city.' '.$state.' local website directory. Page '.$currPage.' of results.';
				}
				else if($args_array[1]=="categories"){
					//List of categories for a city
					//echo $city." Categories";
					$dispHubs = false;
					$dispCities = false;
					$dispCats = true;
					$title = $city.', '.$state.' Local Business Website Categories | All Business Website Categories';
					$meta_desc = 'Find Local Business Websites in '.$city.', '.$state.' by browsing '.$resellerCompany.' HUBs All Categories Display of '.$city.' '.$hub->getStateFromAbv($state).' Local Websites.';
				}
				else if($category = $hub->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							//Paginated listings for a certain category in a city
							//echo $city." ".$category."s, page ".$args_array[2];
							$currPage = $args_array[2];
							$mainOffset = ($mainLimit*$currPage)-$mainLimit;
							$title = $city.' '.$state.' '.$category.' Websites | '.$category.
									' Websites in '.$city.' | Page '.$currPage;
							$meta_desc = $city.' '.$state.' Website Directory of '.$category.' by '.$resellerCompany.' HUBs.  Discover local '.$category.' websites in '.$city.' '.$state.'. Page '.$currPage.' of results.';
						}
					}
					else {
						//All listings for a certain category in a city
						//echo $city." ".$category."s";\
						if($siteInfo['citycat_seo_title']){
						$title = $siteInfo['citycat_seo_title'];
						}	
						else $title = $city.' '.$state.' '.$category.' Websites | '.$category.
								' Websites in '.$city;
						if($siteInfo['citycat_seo_desc']){
						$meta_desc = $siteInfo['citycat_seo_desc'];
						}	
						else $meta_desc = $city.' '.$state.' Website Directory of '.$category.' by '.$resellerCompany.' HUBs.  Discover local '.$category.' websites in '.$city.' '.$state.'.';
						
					}
				}
			} //end if($args_array[1])
			else {
				//All listings for a city
				//echo "All ".$city." listings";
				$dispCities = false;
				$dispCats = true;
				
				if($siteInfo['city_seo_title']){
				$title = $siteInfo['city_seo_title'];
				}	
				else $title = $resellerCompany.' Website Directory for '.$city.', '.$state.' | '.$resellerCompany.' HUBs Directory';
				
				if($siteInfo['city_seo_desc']){
				$meta_desc = $siteInfo['city_seo_desc'];
				}	
				else $meta_desc = 'Find '.$city.' '.$state.' local business Websites in '.$city.' '.$hub->getStateFromAbv($state).'. '.$resellerCompany.' HUBs '.$city.' '.$state.' local website directory.';
			}
		}
	} //end if($args = $_GET['args'])
	else {
		//Root site
		$dispCities = true;
	}
	
if($continue){
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
					 <? if($siteInfo['hubs_network_name']){ ?>
					<h1><?=$siteInfo['hubs_network_name']?></h1>
                <? } else { ?>
                	<h1>HUBs Directory</h1>
                <?php } ?>
					<!-- Search-->
					<? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
				</div>
				<br class="clear" />
                <br class="clear" />
				<!-- Start Two Third -->
				<div class="two-third">	
					<?=$siteInfo['edit_region_1']?>
					<!-- Header Title -->
					<?php
					if($dispHubs){
						if($category) 
							echo '<h1>Local '.$category.' websites for '.$cityLink.'</h1>';
						else if($city) 
							echo '<h1><a href="http://'.$site.'">Local websites</a> for '.ucwords($city).', '.$state.'</h1>';
					}
					?>
						
					<!-- Breadcrumb -->
					<div id="breadcrumb">
					<?
					if($dispHubs){
						if($dispCities) $hub->displayCitiesHubs(6, $site, $resellerID); 
						else if($dispCats) $hub->displayCategoriesHubs($city, $state, 4, $site, $resellerID);
					}
					?>
					</div>
					
					<!-- Start Display of Listings -->
			<?php
			if($dispHubs){
				if($results = $hub->getHubsForNetwork($mainLimit, $mainOffset, $city, $state, $category, $resellerID)){
					$numListings = $results['numResults'];
					$listings = $results['results'];
					while($row = $hub->fetchArray($listings)){
						//replace tags if the hub has any
						if($row['has_tags']){
							$searchFields = array('company_name', 'description', 'keyword1', 'keyword2', 'keyword3');
							$replaceValues = array('city'=>$row['city'], 'state'=>$row['state'], 'company'=>$row['company_name'], 
							 					  'address'=>$row['street'], 'phone'=>$row['phone'], 'zip'=>$row['zip'], 
												  'slogan'=>$row['slogan'], 'keyword1'=>$row['keyword1'], 'keyword2'=>$row['keyword2'],
												  'keyword3'=>$row['keyword3'], 'keyword4'=>$row['keyword4'], 
												  'keyword5'=>$row['keyword5'], 'keyword6'=>$row['keyword6']);
							$row = $hub->replaceTags($row, $searchFields, $replaceValues);
						}
						
						if(!$row['logo']) 
							$icon = $dflt_noImage;
						else 
							$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/hub/'.$row['logo'];
						
						$created = date("F jS, Y", strtotime($row['created']));
						$createdMo = date("M", strtotime($row['created']));
						$createdDay = date("j", strtotime($row['created']));
						
						//Some hubs don't have description field filled.  use meta_description otherwise
						$description = $row['description'] ? $row['description'] : $row['meta_description'];
				?>
					<!-- Start Listing -->
					<div class="blog-post">
						<!-- Date -->
						<div class="post-date">
							<span class="post-month"><?=$createdMo?></span><br />
							<span class="post-day"><?=$createdDay?></span>
						</div>
						
						<!-- Title -->
						<div class="post-title">
							<h3><a href="<?=$row['domain']?>"><?=$row['company_name']?></a></h3>
							<span class="clear">
								Tags <a href="<?=$row['domain']?>" title="<?=$row['keyword1']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword1']?></a>
							<? if($row['keyword2']){ ?>
								, <a href="<?=$row['domain']?>" title="<?=$row['keyword2']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword2']?></a>
							<? } ?>
							<? if($row['keyword3']){ ?>
								, <a href="<?=$row['domain']?>" title="<?=$row['keyword3']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword3']?></a>
							<? } ?>
							</span>
						</div>		
						<br style="clear:both;" />
						
						<!-- Image -->
						<div class="blog2-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['company_name']?>"><img src="<?=$icon?>"  width="175px" alt="" /></a>
							</div>
						</div>
						
						<!-- Details -->
						<?=$hub->shortenSummary($description, 250)?><br />
						<a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?> | <?=$row['keyword2']?> | <?=$row['keyword3']?>">Learn More</a></p>
						<br class="clear" />
						
						<div class="add-container">
							<p class="address">Created by <a href="<?=$row['domain']?>" title="<?=$row['company_name']?>"><?=$row['company_name']?></a> on <?=$created?></p>
						</div>
						
						<div class="right">
							<a href="<?=$row['domain']?>" class="button_size4"><span>View Hub</span></a>
						</div>			
					</div> 
					<!-- End Listing -->
				<? 
					} //end while($row = $hub->fetchArray($listings))
				?>
					<!-- End Display of Listings -->
					<br style="clear:both;" />
					
					<!-- Pagination -->
					<? include('inc/pagination.php'); ?>
				<?
				} //end if($listings = $hub->getHubsForNetwork()
				else echo 'No HUBs found.';
			} //end if($dispHubs)
			else {
				if($dispCities){
					echo '<h2>All cities with <a href="http://'.$site.'">local websites</a></h2>';
					$hub->displayCitiesHubs(NULL, $site, $resellerID);
				}
				else if($dispCats){
					echo '<h2>All <a href="http://'.$site.'">local websites</a> categories for '.$cityLink.'</h2>';
					$hub->displayCategoriesHubs($city, $state, NULL, $site, $resellerID);
				}
			}
			?>
					<p><?=$siteInfo['edit_region_4']?></p>
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_2']?>
							</div>
								<!-- Local -->
								 <? if($siteInfo['network_name']){ ?>
								<h2><?=$siteInfo['network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Business Listings</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $hub->getListingsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$listings = $results['results'];
									while($row = $hub->fetchArray($listings)){
										$a = explode("->", $row['category']);
										$listingCategory = $a[1];
										$listingUrl = 'http://local.'.$domain.'/'.
													$hub->convertKeywordUrl($row['keyword_one']).
													$hub->convertKeywordUrl($row['company_name']).$row['id'].'/';
								?>
									<li><a href="<?=$listingUrl?>" title=""><?=$row['company_name']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No directory listings found.';
								?>
								</ul>
								<!-- End Local -->
								
								<!-- Press -->
								 <? if($siteInfo['press_network_name']){ ?>
								<h2><?=$siteInfo['press_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Press Releases</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $hub->getPressForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$press = $results['results'];
									while($row = $hub->fetchArray($press)){
										$a = explode("->", $row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
													$hub->convertKeywordUrl($pressCategory).
													$hub->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No press releases found.';
								?>
								</ul>
								<!-- End Press -->
								
								<!-- Blog Posts -->
								 <? if($siteInfo['blogs_network_name']){ ?>
								<h2><?=$siteInfo['blogs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Blog Posts</h2>
                                 <?php } ?>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $hub->getPostsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$posts = $results['results'];
									while($row = $hub->fetchArray($posts)){
										$postUrl = $row['domain']."/".$hub->convertKeywordUrl($row['category']).
					 								$hub->convertPressUrl($row['post_title'], $row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo 'No blog posts found.';
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->							
								
								<!-- Articles -->
								 <? if($siteInfo['articles_network_name']){ ?>
								<h2><?=$siteInfo['articles_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Articles</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $hub->getArticlesForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$articles = $results['results'];
									while($row = $hub->fetchArray($articles)){
										$a = explode("->", $row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$hub->convertKeywordUrl($articleCategory).
													$hub->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No articles found.';
								?>
								</ul>
								<!-- End Articles -->
								<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_3']?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div>
				<!-- End Last Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<? } //end if($continue) ?>