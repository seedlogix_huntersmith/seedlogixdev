<?php
require_once dirname(__FILE__) . '/../../../admin/6qube/core.php';

$thisPage = "signup";
require_once(QUBEADMIN . 'inc/reseller.class.php');

$reseller = new Reseller();
$ip = isset($_GET['ip']) ? $_GET['ip'] : $_SERVER['REMOTE_ADDR'];
//get product info
if($row['user_parent_id']==0) $product = $reseller->getResellerProducts($row['user_id'], $productID);
else $product = $reseller->getResellerProducts($row['user_parent_id'], $productID);
if($product['maps_to']==27) $freeAccount = true;
if(!$product['montly_price']==0 && !$product['setup_price']==0) $paidNetworkUser = true;
//get reseller info
if($row['user_parent_id']==0) $query = "SELECT company, payments, payments_acct FROM resellers WHERE admin_user = ".$row['user_id'];
else $query = "SELECT company, payments, payments_acct FROM resellers WHERE admin_user = '".$row['user_parent_id']."'";
$resellerInfo = $reseller->queryFetch($query);
	
if($_POST){ //if user has clicked Submit on the form
	//validate all the form fields
	//user/account info
	$user_id = $reseller->validateUserId($_POST['email'], 'Email Address');
	$password = $reseller->validatePassword($_POST['password1'], 'Password', 3, 12);
	$password2 = $_POST['password2'];
	if($product['fname_field']) $firstName = $reseller->validateText($_POST['first_name'], 'First Name');
	if($product['lname_field']) $lastName = $reseller->validateText($_POST['last_name'], 'Last Name');
	if($product['phone_field']) $phone = $reseller->validateNumber($_POST['phone'], 'Phone');
	if($product['company_field']) $company = $reseller->validateText($_POST['company'], 'Company Name');
	if($product['address_field']) $address = $reseller->validateText($_POST['address'], 'Address');
	if($product['city_field']) $city = $reseller->validateText($_POST['city'], 'City');
	if($product['state_field']) $state = $reseller->validateText($_POST['state'], 'State');
	if($product['zip_field']) $zip = $reseller->validateNumber($_POST['zip'], 'Zip Code');
	
	if($row['user_parent_id']==13963 && $product['id']==825){
	$userUrlName = $reseller->validateText($_POST['userdomain'], 'User Url Name');
	$userdomain = $reseller->validateSubDomainUserEntry($userUrlName.'.vceo.biz');
	$displayname = $reseller->validateText($_POST['displayname'], 'Display Name');
	}
	
	if($product['biz_contact_name']) $bizContactname = $reseller->validateText($_POST['biz_contact_name'], 'Contact Name');
	if($product['biz_company_name']) $bizCompanyname = $reseller->validateText($_POST['biz_company_name'], 'Business Company Name');
	if($product['biz_phone_field']) $bizPhone = $reseller->validateNumber($_POST['biz_phone_field'], 'Business Phone');
	if($product['website_url']) $bizUrl = $reseller->validateText($_POST['website_url'], 'Website URL');
	if($product['biz_address_field']) $bizAddress = $reseller->validateText($_POST['biz_address'], 'Business Address');
	if($product['biz_city_field']) $bizCity = $reseller->validateText($_POST['biz_city'], 'Business City');
	if($product['biz_state_field']) $bizState = $reseller->validateText($_POST['biz_state'], 'Business State');
	if($product['biz_zip_field']) $bizZip = $reseller->validateNumber($_POST['biz_zip'], 'Business Zip Code');
	
	$addSite = ($_POST['add_site']==1) ? 1 : 0;
	$product_id = $reseller->validateNumber($_POST['product_id'], 'Product ID');
	if($row['user_parent_id']==0) $parent_id = $reseller->validateNumber($_POST['reseller_id'], 'Reseller ID');
	else $parent_id = $reseller->validateNumber($row['user_parent_id'], 'Reseller ID');
	if($product['date_field']) $date = $reseller->validateText($_POST['date_field'], 'Date Field');
	$nopayment = $product['monthly_price']==0 && $product['setup_price']==0;
	
	if($nopayment) $access = '1';
	else $access = '0';
	
	if($freeAccount){
		//listing info
		$category = $reseller->validateCategory($_POST['industry'], 'Business Category');
		if($product['category']) $keyword_one = $city . " " . $state . " " . $product['cat_key'];
		else $keyword_one = $reseller->validateText($_POST['keyword_one'], 'Listing keyword');
		$listing_desc = $reseller->validateText($_POST['listing_desc'], 'Listing description');
		$website = $reseller->validateUrl($_POST['website_url']);
		
		require_once(QUBEADMIN . 'inc/directory.class.php');
		$dir = new Dir();
	}
	//calculate price info
	$setupCost = $product['setup_price'];
	if($addSite && $product['add_site']>0) $setupCost += $product['add_site'];
	$dueNow = $setupCost + $product['monthly_price'];
	
	if($reseller->foundErrors() || $password!=$password2){ //check to see if there were any errors
		 $errors = $reseller->listErrors('<br />'); //if so then store the errors
		 if($password!=$password2) $errors .= 'Passwords did not match.<br />';
	}
	else{
		//create row in users table
		$password = $reseller->encryptPassword($password);
		$query = "INSERT INTO users (parent_id, username, password, access, class, reseller_client, created) 
				VALUES (".$parent_id.", '".$user_id."', '".$password."', '".$access."', ".$product_id.", 1, now())";
				
		if($result = $reseller->query($query)){
			//if successfully created new user add info to user_info table
			$newUserID = $reseller->getLastId();
			$query = "INSERT INTO user_info 
					(user_id, firstname, lastname, phone, company, address, city, state, zip, referral_id, date_field, biz_contact_name, biz_company_name, 
					biz_phone_field, website_url, biz_address, biz_city, biz_state, biz_zip) 
					VALUES (".$newUserID.", '".$firstName."', '".$lastName."', '".$phone."', '".$company."', 
							'".$address."', '".$city."', '".$state."', '".$zip."', '".$row['user_id']."', '".$row['date_field']."', '".$bizContactname."', '".$bizCompanyname."',
							'".$bizPhone."', '".$bizUrl."', '".$bizAddress."', '".$bizCity."', '".$bizState."', '".$bizZip."')";
					
			if($result = $reseller->query($query)){ //if the query was successful (New user added)	
				if($product['maps_to']==463){ //if member product dont create user folders
					 
				}
				else{
				//Create folders
				if($_SERVER['QUBECONFIG']	==	'router-6qube2')
					file_get_contents('http://6qube.com/createuserdirs.php?user=' . $newUserID);
				else
					$reseller->createUserFolders($newUserID);			

				//Create campaign
				$cid = $reseller->addNewCampaign($newUserID, $company, NULL, true);
				}
				
				if($row['user_parent_id']==13963 && $product['id']==825){
					if($userdomain) {
						
						require_once(QUBEADMIN . 'inc/hub.class.php');
						$hub = new Hub();
						
						$hid = $hub->addNewHub($newUserID, $displayname, $cid, NULL, NULL, NULL, $row['user_parent_id']);	
						
						$query = "SELECT id FROM hub WHERE user_id = '".$newUserID."'";
						$newHubID = $hub->queryFetch($query);
						
						if($newHubID){		
# amado fix
	// remote call
	if($_SERVER['QUBECONFIG']	==	'router-6qube2'){
		$func	=	'setupMultiUserHubV2';
		$args	=	array($newHubID['id'], '17796', $newUserID, $row['user_parent_id']);
		$params	=	array('amado' => md5('amado'), 'func' => $func, 'args' => $args);
		$url	=	'http://6qube.com/callfunc.php?' . http_build_query($params);
		$pushinfo	=	unserialize(file_get_contents($url));	// cross fingers
	}else{
			$pushinfo	=	$hub->setupMultiUserHubV2($newHubID['id'], '17796', $newUserID, $row['user_parent_id']);
	}
# end				
							 if($pushinfo['error']	==	0){
							 
								$urlupdate = "UPDATE hub
										SET 
										httphostkey = '".$userdomain."', 
										domain = 'http://".$userdomain."',
										company_name = '".$displayname."',
										street = '".$address."',
										city = '".$city."',
										state = '".$city."',
										zip = '".$zip."',
										phone = '".$phone."'
										WHERE id = ".$newHubID['id'];
								$hub->query($urlupdate);
							}
						}
					
					}
				}
				
				if($freeAccount || $nopayment){
					if($freeAccount){
						//if user is a free listing signup, create their listing and send conf email
						if($dir_id = $dir->addNewDirectory($newUserID, $company, $cid, NULL, true, NULL, true)){
							$update = "UPDATE directory 
									SET 
									user_parent_id = ".$parent_id.", 
									company_name = '".addslashes($company)."', 
									phone = '".$phone."', 
									street = '".addslashes($address)."', 
									city = '".addslashes($city)."', 
									state = '".$state."', 
									zip = '".$zip."', 
									category = '".$category."', 
									website_url = '".$website."', 
									display_info = '".addslashes($listing_desc)."', 
									keyword_one = '".addslashes($keyword_one)."' 
									WHERE id = ".$dir_id;
							$dir->query($update);
							
							if($parent_id==2996){
								
								require_once(QUBEADMIN . 'fbauto-post.php');
								
							}
						}
					
						//Send the user a confirmation email
						if($reseller->sendRegisterMail($newUserID, true, $dir_id, $parent_id)){
							if($paidNetworkUser){
										//add info to signups table (this info is pulled on the process page)
										$query = "INSERT INTO signups 
												(user_id, user_parent_id, hub_id, product_id, setup_cost, monthly_cost, add_site, signup_ip)
												VALUES (".$newUserID.", ".$parent_id.", ".$row['id'].", ".$product_id.", '".$setupCost."', 
														'".$product['monthly_price']."', '".$addSite."', '".$ip."')";
										$reseller->query($query);
									
										//Continue to payments if not free listing
										if($resellerInfo['payments']=='paypal'){
											//paypal
											$displayForm = false;
											//temporary until we integrate our own payment form:
											$displayPaypalButton = true;
										}
										else if($resellerInfo['payments']=='other'){
											//'other' payment processor
											if($product['pay_link']){
												//replace placeholders with values
												$payLink = str_replace('#dueNow', $dueNow, $product['pay_link']);
												$payLink = str_replace('#monthly', $product['monthly_price'], $payLink);
												$payLink = str_replace('#firstname', $firstName, $payLink);
												$payLink = str_replace('#lastname', $lastName, $payLink);
												$payLink = str_replace('#email', $user_id, $payLink);
												$payLink = str_replace('#phone', $phone, $payLink);
												$payLink = str_replace('#address', $address, $payLink);
												$payLink = str_replace('#city', $city, $payLink);
												$payLink = str_replace('#state', $state, $payLink);
												$payLink = str_replace('#zip', $zip, $payLink);
												$payLink = str_replace('#company', $company, $payLink);
												header("Location: ".$payLink);exit;
											}
											else {
												$displayMessage .= 
												"<h1>Thanks for signing up!</h1>
												We're currently performing maintenance on our automatic payment system.  You will be contacted shortly to complete the signup process and activate your account.";
											}
										}
										else {

											//authorize.net - reseller's or 6qube's
											$payLink = $reseller->convertKeywordUrl($resellerInfo['company']).
													$reseller->convertKeywordUrl($product['name']).$productID.'/'.$newUserID.'/';
											if($row['lincoln_domain']){
												echo
					'<script type="text/javascript">window.location.href = "https://thepartnerportal.com/'.$payLink.'";</script>';
											} else {
												echo '<script type="text/javascript">window.location.href = "https://thepartnerportal.com/'.$payLink.'";</script>';
											}
										}
							} else {
							$displayMessage .= 
							"<br /><h1>Thanks for signing up!</h1>
							<p>A confirmation email was sent to your email address.<br />You must click the link in the email within 24 hours to activate your account otherwise it will be deleted.</p><br /><br /><br /><br /><br /><br /><br /><br />";
							}
						} else {
							$errors .= "Sorry, an error occured while sending your confirmation email.<br />
									  Please contact us for help activating your account.<br />";
						}
					}
					if($nopayment){
						$displayMessage .= $product['edit_region_4'];
					}
				} //end if($freeAccount)
				else {
					//add info to signups table (this info is pulled on the process page)
					$query = "INSERT INTO signups 
							(user_id, user_parent_id, hub_id, product_id, setup_cost, monthly_cost, add_site, signup_ip)
							VALUES (".$newUserID.", ".$parent_id.", ".$row['id'].", ".$product_id.", '".$setupCost."', 
									'".$product['monthly_price']."', '".$addSite."', '".$ip."')";
					$reseller->query($query);
				
					//Continue to payments if not free listing
					if($resellerInfo['payments']=='paypal'){
						//paypal
						$displayForm = false;
						//temporary until we integrate our own payment form:
						$displayPaypalButton = true;
					}
					else if($resellerInfo['payments']=='other'){
						//'other' payment processor
						if($product['pay_link']){
							//replace placeholders with values
							$payLink = str_replace('#dueNow', $dueNow, $product['pay_link']);
							$payLink = str_replace('#monthly', $product['monthly_price'], $payLink);
							$payLink = str_replace('#firstname', $firstName, $payLink);
							$payLink = str_replace('#lastname', $lastName, $payLink);
							$payLink = str_replace('#email', $user_id, $payLink);
							$payLink = str_replace('#phone', $phone, $payLink);
							$payLink = str_replace('#address', $address, $payLink);
							$payLink = str_replace('#city', $city, $payLink);
							$payLink = str_replace('#state', $state, $payLink);
							$payLink = str_replace('#zip', $zip, $payLink);
							$payLink = str_replace('#company', $company, $payLink);
							header("Location: ".$payLink);
exit;
						}
						else {
							$displayMessage .= 
							"<h1>Thanks for signing up!</h1>
							We're currently performing maintenance on our automatic payment system.  You will be contacted shortly to complete the signup process and activate your account.";
						}
					}
					else if($resellerInfo['payments']=='6qubebrand'){
						//6qube's authorize.net and branded 6Qube
						$salt = 'SFGDSFG*Jsem fxdfxd,x,Z >< >< jxdnf2';
						$pepper = 's$TS$EGU$NGdg,cg cxcxXMMMM ;;;{} (.)(.) ( Y )';
						$secHash = md5($salt.$newUserID.$pepper);
						$secHash = md5($pepper.$secHash);
						$redir = 'https://6qube.com/process/'.$newUserID.'/'.$secHash.'/';
						if($_POST['addSite']) $redir .= '1/';
						if($row['lincoln_domain']){
							echo
'<script type="text/javascript">window.location.href = "'.$redir.'";</script>';
						} else {
							header('Location: '.$redir); exit;
						}
					}
					else {
						//authorize.net - reseller's or 6qube's
						$payLink = $reseller->convertKeywordUrl($resellerInfo['company']).
								$reseller->convertKeywordUrl($product['name']).$productID.'/'.$newUserID.'/';
						if($row['lincoln_domain']){
							echo
'<script type="text/javascript">window.location.href = "https://thepartnerportal.com/'.$payLink.'";</script>';
						} else {
		echo							'<script type="text/javascript">window.location.href = "https://thepartnerportal.com/'.$payLink.'";</script>';
						}
					}
				}
				
			}//end if($result = $reseller->query($query))
		}//end if($result = $reseller->query($query))
		else{
			$errors .= 'Something weird happened.  Please try again?';
		}
	}// end first else
} //end if($_POST)
else{ //if form has not been posted show form
	$displayForm = true;
}
//If errors - show them
if($errors){
	$displayForm = true;
}
if($product['id']==49){
	$displayForm = false;
	$displayPaypalButton = true;
	$addSite = true;
}
$title = $product['name'];
?>
