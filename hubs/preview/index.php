<?php
require_once(dirname(__FILE__) . '/../../admin/6qube/core.php');
session_start();
//Hub preview V2
//8/24/2011
//User is directed to hubpreview.reseller.com/[uid]/[hub id]/
//variables are captured as session vars and the user is redirected to 
//just hubpreview.reseller.com for a live preview of their site

if($_GET['page'] && (substr($_GET['page'], 0, 2)=='i/')){
	//capture vars if it's the first time
	$args = explode('/', $_GET['page']);
	$uid = is_numeric($args[1]) ? $args[1] : '';
	$hid = is_numeric($args[2]) ? $args[2] : '';
	
	if($uid && $hid){
		$_SESSION['preview_uid'] = $uid;
		$_SESSION['preview_hub'] = $hid;
		//redirect
		$domain = $_SERVER['HTTP_HOST'];
		$d = explode('.', $domain);
		if($d[0]!='hubpreview'){
			//if user is trying to access this from domain.com/hubs/preview/123/456/ for some reason
			$domain = 'hubpreview.'.$domain.'/'.$uid.'/'.$hid;
		}
		$domain = 'http://'.$domain;
		header('Location: '.$domain);
	}
	else echo 'Invalid request.  Please try again (1).';
}
else if($_SESSION['preview_uid'] && $_SESSION['preview_hub']){
	//if user has already passed the preview vars
	$hub_id = $_SESSION['preview_hub'];
	include(QUBEROOT . 'hubs/domains/'.$_SESSION['preview_uid'].'/'.$_SESSION['preview_hub'].'/index.php');
}
else {
	//no vars passed and no session stored
	echo 'Invalid request.  Please try again (2).';
}
?>