<?php
if($row['ssl_on']==1){
	if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on")
	{
		$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . $redirect);
		exit();
	}
	header('Strict-Transport-Security: max-age=63072000; includeSubDomains; preload');
}

if($this->vars['request']=='sitemap'){
	
	include_once('sitemap.php');
		
} else {
	
    if($page = $_GET['page']){
		//include global script to get/process custom page
		include_once(QUBEROOT . 'hubs/global/custom_page.php');
	}
	

	if($row['edit_region_6']){
		//$str = "old/url,/new-url/|old/url,/new-url/";
		$str = $row['edit_region_6'];
	
		$redirects = array_map(
			function ($substr) {
				return explode(',', $substr);
			}, 
			explode('|', $str)
		);
	
		foreach ($redirects as $redirect){
			
			if($this->vars['request']==$redirect[0]) $this->redirect(''.$redirect[1].'');
	
		}
	}
	

	
	
	if($page_row['page_title']=="Page not found!") header('HTTP/1.0 404 Not Found', true, 404);
	
	if($page_row['page_seo_title']){
		$title = $page_row['page_seo_title'];
	}
	else $title = $page_row['page_title'];
	$meta_desc = $page_row['page_seo_desc'];
	$meta_keywords = $page_row['page_keywords'];

//ENGINE HEADER
include('inc/header.php');
include_once(QUBEROOT . 'hubs/global/global_addon.php');
	//COMMON HEADER
include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/header.php');

	$edit3 = rtrim( $page_row['page_edit_3'] );
	
	if(!empty( $edit3 )) echo $page_row['page_edit_3'];
	else echo $row['photos'];
	echo $page_row['page_region'];
	
	if($page_row['page_title']=="Page not found!") include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/404.php');

//COMMON FOOTER
include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/footer.php');
	
}
?>
