<?php
session_start();
require_once(QUBEROOT . 'admin/inc/directory.class.php');
$local = new Dir();

//make sure this script was pulled as an include and from the same person

	$email = $local->validateUserId($_POST['email'], 'Email Address');
	$password = $local->validatePassword($_POST['password'], 'Password', 3, 12);
	$password2 = $_POST['password2'];
	$firstName = $local->validateText($_POST['first_name'], 'First Name');
	$lastName = $local->validateText($_POST['last_name'], 'Last Name');
	$phone = $local->validateText($_POST['phone'], 'Phone');
	if($product['company_field']) $company = $local->validateText($_POST['company'], 'Company Name');
	$address = $local->validateText($_POST['address'], 'Address');
	$address2 = $local->sanitizeInput($_POST['address2']);
	$city = $local->validateText($_POST['city'], 'City');
	$state = $local->validateText($_POST['state'], 'State');
	$zip = $local->validateText($_POST['zip'], 'Zip Code');
	$referral_id = $_POST['referral_id'];
	$website = $_POST['website_url'];

	
	//make sure IP isn't blacklisted
	$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
	if($ip && $local->checkBlacklist($ip)) $blacklisted = true;
	
	//check for errors
	if($local->foundErrors() || $blacklisted || ($password!=$password2)){
		$errors = '';
		if($local->foundErrors()) $errors .= $local->listErrors('<br />').'<br />'; //if so then store the errors
		if($password!=$password2)
			$errors .= 'Passwords do not match<br />';
			
		$displayForm = 'createUser';
		
	}
	else {
		//form validated, continue
		$password = $local->encryptPassword($password);
		$query = "INSERT INTO users (parent_id, username, password, access, reseller_client, signup_ip, created) 
				VALUES ('".$row['user_parent_id']."', '".$email."', '".$password."', 1, 1, '".$_SERVER['REMOTE_ADDR']."', now())";
		if($result = $local->query($query)){
			$newUserID = $local->getLastId();
			$query = "INSERT INTO user_info (user_id, firstname, lastname, phone, company, address, address2, city, state, zip, tcity, referral_id, website_url, role_id)
					VALUES ('".$newUserID."', '".$firstName."', '".$lastName."', '".$phone."', '".$company."', '".$address."', 
						   '".$address2."', '".$city."', '".$state."', '".$zip."', '".$urlinfo['city']."', '".$referral_id."', '".$website."', '".$productID."')";
			if($result = $local->query($query)){			
					

					$uid = 	$newUserID;
					include('user-notifications.php');

					if($productID){
						$uda = new UserDataAccess();
						$uda->SaveRole($newUserID, $productID);
					}

			} //end if($result = $local->query($query)) [create user_info row)
			else {
				$errors .= 'Sorry, an error occured while attempting to create your account (2). Please try again.<br />';
				//if users table row succeeded but user_info row didn't, delete user row
				$query = "DELETE FROM users WHERE id = '".$newUserID."' LIMIT 1";
				$local->query($query);
				session_destroy();
			}
		} //end if($result = $local->query($query)) [create user row)
		else {
			$errors .= 'Sorry, an error occured while attempting to create your account (1). Please try again.<br />';
			session_destroy();
		}
		
		
		if($errors) $displayForm = 'createUser';
		else {
				

						//$displayMessage .= $product['edit_region_4'];
						$redir = 'http://'.$row['httphostkey'].'/useractivation/?displayMessage=success&pid='.$productID.'';
						header('Location: '.$redir);

						
				

		}
	}


?>
