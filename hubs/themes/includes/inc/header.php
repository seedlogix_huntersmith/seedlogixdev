<?php

	///slides info
	$modeling = WebsiteModel::Fetch('id = %d', $row['id']);
	$slides =   $modeling->getSlidePhotos();
	
///edit regions first
	$searchFields = array('overview', 'offerings', 'photos', 'edit_region_9', 'add_autoresponder', 'videos');
	$replaceValues = array('navtag'=>'', 'slider'=>'', 'logo'=>'', 'navtag1'=>'', 'custom1'=>'', 'custom2'=>'', 'custom3'=>'', 'custom4'=>'', 'custom5'=>'', 'custom6'=>'', 'custom7'=>'', 'custom8'=>'', 'custom9'=>'', 'custom10'=>'', 'custom11'=>'', 'custom12'=>'', 'custom13'=>'', 'custom14'=>'', 'custom15'=>'', 'custom16'=>'', 'custom17'=>'', 'custom18'=>'', 'custom19'=>'', 'custom20'=>'');
	// DO IT:
	foreach($searchFields as $searchFieldKey=>$searchField){
		if(preg_match_all('/\[\[(.*)\]\]/i', $row[$searchField], $matches)){ //;regex to check if this field has tag(s)
			foreach($matches[1] as $replaceKeyword){
				$replaceValue = $replaceValues[$replaceKeyword];				



				if($replaceKeyword=='navtag' || $replaceKeyword=='navtag1' || $replaceKeyword=='slider' || $replaceKeyword=='logo' || $replaceKeyword=='custom1' || $replaceKeyword=='custom2' || $replaceKeyword=='custom3' || $replaceKeyword=='custom4' || $replaceKeyword=='custom5' || $replaceKeyword=='custom6' || $replaceKeyword=='custom7' || $replaceKeyword=='custom8' || $replaceKeyword=='custom9' || $replaceKeyword=='custom10' || $replaceKeyword=='custom11' || $replaceKeyword=='custom12' || $replaceKeyword=='custom13' || $replaceKeyword=='custom14' || $replaceKeyword=='custom15' || $replaceKeyword=='custom16' || $replaceKeyword=='custom17' || $replaceKeyword=='custom18' || $replaceKeyword=='custom19' || $replaceKeyword=='custom20'){
					if($replaceKeyword=='navtag'){
						if($hasPages){
								
								include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/navtag.php');
								
								
						 }
					}
					if($replaceKeyword=='navtag1'){
						if($hub_pages){
							
								include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/navtag1.php');
							
						 }
					}
					if($replaceKeyword=='slider'){
						if($slides){
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/slider.php'); 
					  }
				   	}
					if($replaceKeyword=='logo'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/logo.php');
					
					}
					if($replaceKeyword=='custom1'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom1.php');
					
					}
					if($replaceKeyword=='custom2'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom2.php');
					
					}
					if($replaceKeyword=='custom3'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom3.php');
					
					}
					if($replaceKeyword=='custom4'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom4.php');
				
					}
					if($replaceKeyword=='custom5'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom5.php');
				
					}
					if($replaceKeyword=='custom6'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom6.php');
				
					}
					if($replaceKeyword=='custom7'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom7.php');
				
					}
					if($replaceKeyword=='custom8'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom8.php');
				
					}
					if($replaceKeyword=='custom9'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom9.php');
				
					}
					if($replaceKeyword=='custom10'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom10.php');
				
					}
					if($replaceKeyword=='custom11'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom11.php');
				
					}
					if($replaceKeyword=='custom12'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom12.php');
				
					}
					if($replaceKeyword=='custom13'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom13.php');
				
					}
					if($replaceKeyword=='custom14'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom14.php');
				
					}
					if($replaceKeyword=='custom15'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom15.php');
				
					}
					if($replaceKeyword=='custom16'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom16.php');
				
					}
					if($replaceKeyword=='custom17'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom17.php');
				
					}
					if($replaceKeyword=='custom18'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom18.php');
				
					}
					if($replaceKeyword=='custom19'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom19.php');
				
					}
					if($replaceKeyword=='custom20'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom20.php');
				
					}
					$row[$searchField] = str_replace('[['.$replaceKeyword.']]', $replaceValue, $row[$searchField]);
					
				} 

			}
			
		} //end if(preg_match('/\(\((.*)\)\)/i', $row['overview']))
	}
///edit regions first
	$searchFields = array('page_region', 'page_edit_2', 'page_edit_3');
	$replaceValues = array('navtag'=>'', 'slider'=>'', 'logo'=>'', 'navtag1'=>'', 'custom1'=>'', 'custom2'=>'', 'custom3'=>'', 'custom4'=>'', 'custom5'=>'', 'custom6'=>'', 'custom7'=>'', 'custom8'=>'', 'custom9'=>'', 'custom10'=>'', 'custom11'=>'', 'custom12'=>'', 'custom13'=>'', 'custom14'=>'', 'custom15'=>'','custom16'=>'', 'custom17'=>'', 'custom18'=>'', 'custom19'=>'', 'custom20'=>'');
	// DO IT:
	foreach($searchFields as $searchFieldKey=>$searchField){
		if(preg_match_all('/\[\[(.*)\]\]/i', $page_row[$searchField], $matches)){ //;regex to check if this field has tag(s)
			foreach($matches[1] as $replaceKeyword){
				$replaceValue = $replaceValues[$replaceKeyword];	

				if($replaceKeyword=='navtag' || $replaceKeyword=='navtag1' || $replaceKeyword=='slider' || $replaceKeyword=='logo' || $replaceKeyword=='custom1' || $replaceKeyword=='custom2' || $replaceKeyword=='custom3' || $replaceKeyword=='custom4' || $replaceKeyword=='custom5' || $replaceKeyword=='custom6' || $replaceKeyword=='custom7' || $replaceKeyword=='custom8' || $replaceKeyword=='custom9' || $replaceKeyword=='custom10' || $replaceKeyword=='custom11' || $replaceKeyword=='custom12' || $replaceKeyword=='custom13' || $replaceKeyword=='custom14' || $replaceKeyword=='custom15' || $replaceKeyword=='custom16' || $replaceKeyword=='custom17' || $replaceKeyword=='custom18' || $replaceKeyword=='custom19' || $replaceKeyword=='custom20'){
					if($replaceKeyword=='navtag'){
						if($hasPages){
								include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/navtag.php');
						 }
					}
					if($replaceKeyword=='navtag1'){
						if($hub_pages){
							
								include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/navtag1.php');
							
						 }
					}
					if($replaceKeyword=='slider'){
						if($slides){
								 include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/slider.php'); 
					  }
				   	}
					if($replaceKeyword=='logo'){
								
								include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/logo.php');
					
					}
					if($replaceKeyword=='custom1'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom1.php');
					
					}
					if($replaceKeyword=='custom2'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom2.php');
					
					}
					if($replaceKeyword=='custom3'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom3.php');
					
					}
					if($replaceKeyword=='custom4'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom4.php');
				
					}
					if($replaceKeyword=='custom5'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom5.php');
				
					}
					if($replaceKeyword=='custom6'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom6.php');
				
					}
					if($replaceKeyword=='custom7'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom7.php');
				
					}
					if($replaceKeyword=='custom8'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom8.php');
				
					}
					if($replaceKeyword=='custom9'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom9.php');
				
					}
					if($replaceKeyword=='custom10'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom10.php');
				
					}
					if($replaceKeyword=='custom11'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom11.php');
				
					}
					if($replaceKeyword=='custom12'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom12.php');
				
					}
					if($replaceKeyword=='custom13'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom13.php');
				
					}
					if($replaceKeyword=='custom14'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom14.php');
				
					}
					if($replaceKeyword=='custom15'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom15.php');
				
					}
					if($replaceKeyword=='custom16'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom16.php');
				
					}
					if($replaceKeyword=='custom17'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom17.php');
				
					}
					if($replaceKeyword=='custom18'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom18.php');
				
					}
					if($replaceKeyword=='custom19'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom19.php');
				
					}
					if($replaceKeyword=='custom20'){
								
								  include(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/tags/custom20.php');
				
					}
					$page_row[$searchField] = str_replace('[['.$replaceKeyword.']]', $replaceValue, $page_row[$searchField]);
					
				} 

			}
			
		} //end if(preg_match('/\(\((.*)\)\)/i', $row['overview']))
	}
	
    //Get Theme Color
    $color = $row['links_color'];
	$color = str_replace('#', '', $color);
	
	$altColor = $row['bg_color'];
	$altColor = str_replace('#', '', $altColor);
	

?>
