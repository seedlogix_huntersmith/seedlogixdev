<?php
session_start();


//This script is used by 6qube.com/process.php to create a new user's CIM profile and payment profile, 
//then charge them and begin recurring billing

//make sure this script is pulled as an include and by the same person
if($sessID = session_id()){
	require_once(QUBEADMIN . 'inc/Authnet.class.php');
	require_once QUBEADMIN . 'inc/reseller.class.php';
	$reseller = new Reseller();
	$anet = new Authnet();
	

	
	//first attempt to set up CIM profile

	$query = "SELECT support_email, payments, payments_acct, payments_acct2, company FROM resellers WHERE admin_user = '".$row['user_parent_id']."'";
	$resellerInfo = $reseller->queryFetch($query);
	//set api keys
	if($resellerInfo['payments']=='authorize'){
		$apiLogin = $resellerInfo['payments_acct'];
		$apiAuth = $resellerInfo['payments_acct2'];
	}
	$newProfile = $anet->setupCIMProfile($uid, NULL, $apiLogin, $apiAuth, 1);

	
	if($newProfile['success']){
		$profile_id = $newProfile['profile_id'];
		//attempt payment profile setup
		$params = array('firstname' => $firstName,
					 'lastname' => $lastName,
					 'company' => $company,
					 'address' => $address,
					 'city' => $city,
					 'state' => $state,
					 'zip' => $zip,
					 'phone' => $phone,
					 'cardnumber' => $cardNumber,
					 'expmonth' => $expMonth,
					 'expyear' => $expYear);
		
		$newPaymentProfile = $anet->setupPaymentProfile($uid, $profile_id, $params, $apiLogin, $apiAuth, 1);	
		
		
		if($newPaymentProfile['success']){
			$continue = 0;
			if($setupCharge && $setupCharge > 0){
				//if setup charges, attempt now
				$payment1 = $anet->chargeCIM($uid, $row['user_parent_id'], $productID, $setupCharge, 'initialSetupFees', 0, 'Initial Setup for '.$productName.' - One Time Only', $apiLogin, $apiAuth);

				
				if($payment1['success']){
					//setup charges success
					$continue = $setupFeesSuccess = 1;
					$transaction_id = $payment1['message'];
					//log transaction
					$note = "Initial setup fee";
					
					$query = "INSERT INTO resellers_billing 
							(user_id, user_parent_id, product_id, transaction_type, transaction_id, amount, 
							 method_identifier, success, note, timestamp)
							VALUES
							('".$uid."', '".$row['user_parent_id']."', '".$productID."', 'initialSetupFees','".$transaction_id."', '".$setupCharge."', 
							'".$payment1['profile_id']."', 1, '".$note."', now())";
					$reseller->query($query);	

					
				}
				else {
					//setup charges failure
					$continue = 0;
					$errors = 
					"Sorry, but there was an error processing the setup fees with the card provided:<br />".
					$payment1['message'];
				}
			} //end if($setupCharge && $setupCharge > 0)
			else $continue = 1;
			
			if($continue){
				//attempt to charge for first month
				$logNote = 'Monthly billing for '.$productName.' - first month';
				

				$payment2 = $anet->chargeCIM($uid, $row['user_parent_id'], $productID, $monthlyCost, 'initialSubscription', 0, 'Monthly billing for '.$productName.' - First Month', $apiLogin, $apiAuth);

				
				if($payment2['success']){
					//first month success, log transaction
					
						
					$query = "INSERT INTO resellers_billing 
							(user_id, user_parent_id, product_id, transaction_type, transaction_id, amount, success, note)
							VALUES
							('".$uid."', '".$row['user_parent_id']."', '".$productID."', 'initialSubscription', '".$payment2['message']."', 
							 '".$monthlyCost."', 1, 'Initial monthly subscription')";
					$reseller->query($query);
					//set up recurring
					$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
					$query = "INSERT INTO resellers_billing 
							(user_id, user_parent_id, product_id, transaction_type, amount, note, sched, sched_date, sched_reattempt)
							VALUES
							('".$uid."', '".$row['user_parent_id']."', '".$productID."', 'monthlySubscription', '".$monthlyCost."', 
							'Scheduled monthly billing for ".date('F', strtotime($nextBillDate))."', 
							1, '".$nextBillDate."', 10)";
					if(!$reseller->query($query)) $schedErr = true;	
					

					
					//update user stuff
					//create folders
					//$reseller->createUserFolders($uid);
					//update user row
					$query = "UPDATE users SET access = 1 WHERE id = '".$uid."'";
					$reseller->query($query);
		
					
					//create first campaign
					//$reseller->addNewCampaign($uid, $company, NULL, true);
					//display confirmation
					$paymentSuccess = 1;
					
			
					
				


				} //end if($payment2['success'])
				else {
					//first month failure, notify user
					$errors = "Sorry, an error occured while attempting to bill you for the first month (9994):<br />";
					$errors .= $payment2['message'];
				}
			} //end if($continue)
		} //end if($newPaymentProfile['success'])
		else {
			//payment profile setup failure
			$errors = "Sorry, an error occured (9993):<br />";
			$errors .= $newPaymentProfile['message'];
		}
	} //end if($newProfile['success'])
	else {
		//CIM profile failure
		$errors = "Sorry, an error occured (9993):<br />";
		$errors .= $newProfile['message'];
	}
}
?>