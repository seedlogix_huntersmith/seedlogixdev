<title><?=$title?></title>
<meta content="<?=$meta_keywords?>" name="keywords" />
<meta content="<?=$meta_desc?>" name="description" />
<meta content="<?=$row['company_name']?>" name="author" />
<?php if($row['display_pricing']==1){ ?>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<?php } else { ?>
<meta content="INDEX,FOLLOW" name="robots" />
<meta content="never" name="expires" />
<?php } ?>

<?php 
	if($row['ssl_on']==1){
		if($row['www_options']=='www_only') $bdomain = 'https://www.'.$row['httphostkey'].'';
		else $bdomain = 'https://'.$row['httphostkey'].'';
	} else {
		if($row['www_options']=='www_only') $bdomain = 'http://www.'.$row['httphostkey'].'';
		else $bdomain = 'http://www.'.$row['httphostkey'].'';
	}
?>

<meta property="og:description" content="<?=$meta_desc?>">
<meta property="og:title" content="<?=$title?>">
<meta property="og:image" content="<?=$bdomain?>/users<?=$row['logo']?>">
<meta name="twitter:description" content="<?=$meta_desc?>">
<meta name="twitter:title" content="<?=$title?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:image" content="<?=$bdomain?>/users<?=$row['logo']?>">


<?php if($thisPage == 'index') {?> 
<link rel='canonical' href='<?=$bdomain?>' />
<meta property="og:url" content="<?=$bdomain?>">
<? } else if($page_row) {?>
<link rel='canonical' href='<?=$pageURL?>' />
<meta property="og:url" content="<?=$pageURL?>">
<? } else { ?>
<? } ?>
