<?php
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
$pages = $hub->getHubPage($row['id'], NULL, NULL, 1, 1, NULL, NULL, 1);

header('Content-type: application/xml');


if($row['ssl_on']==1){
	if($row['www_options']=='www_only') $domain = 'https://www.'.$row['httphostkey'];
	else $domain = 'https://'.$row['httphostkey'];
} else {
	$domain = $row['domain'];
}

if($row['lincoln_domain']==1){
	
	$pdo	=	Qube::Start()->GetDB('master');
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.hub');
	$select->Fields('httphostkey, domain');
	$select->Where('hub_parent_id = %d', $row['hub_parent_id']);
	$select->orderBy('created asc');
	$stmt	=	$select->Exec();
	$rows	=	array();
	while($sub	=	$stmt->fetch()){
		$rows[$sub['httphostkey']]	=	$sub;
	}
	
}


$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

$sitemap .= '<url>
  <loc>'.$domain.'</loc>
  <lastmod>'.date('c', time()).'</lastmod>
  <priority>1.00</priority>
</url>';

if($row['lincoln_domain']==1){
		foreach(array_slice($rows, 0, 3800) as $sub){
				$domain = $sub['domain'];
				foreach ($pages as $page) {

					if($page['type']=='page'){
							if($page['outbound_url']) $pageurl = $page['outbound_url'];
							else $pageurl = ''.$domain.'/'.$page['page_title_url'].'/';
							$sitemap .= '<url>
						  <loc>'.$pageurl.'</loc>
						  <lastmod>'.date('c', time()).'</lastmod>
						  <priority>0.80</priority>
						</url>';

					} else if($page['type']=='nav'){

						$childp = $hub->getHubPage($row['id'], NULL, NULL, 1, 1, NULL, $page['id'], 1);
						foreach($childp as $child){

							if($child['outbound_url']) $pageurl = $child['outbound_url'];
							else $pageurl = ''.$domain.'/'.$page['page_title_url'].'/'.$child['page_title_url'].'/';

							$sitemap .= '<url>
							  <loc>'.$pageurl.'</loc>
							  <lastmod>'.date('c', time()).'</lastmod>
							  <priority>0.80</priority>
							</url>';

						}

					}

				}

		}
	
} else {
				foreach ($pages as $page) {

					if($page['type']=='page'){
							if($page['outbound_url']) $pageurl = $page['outbound_url'];
							else $pageurl = ''.$domain.'/'.$page['page_title_url'].'/';
							$sitemap .= '<url>
						  <loc>'.$pageurl.'</loc>
						  <lastmod>'.date('c', time()).'</lastmod>
						  <priority>0.80</priority>
						</url>';

					} else if($page['type']=='nav'){

						$childp = $hub->getHubPage($row['id'], NULL, NULL, 1, 1, NULL, $page['id'], 1);
						foreach($childp as $child){

							if($child['outbound_url']) $pageurl = $child['outbound_url'];
							else $pageurl = ''.$domain.'/'.$page['page_title_url'].'/'.$child['page_title_url'].'/';

							$sitemap .= '<url>
							  <loc>'.$pageurl.'</loc>
							  <lastmod>'.date('c', time()).'</lastmod>
							  <priority>0.80</priority>
							</url>';

						}

					}

				}

}
  
  $sitemap .= '</urlset>';
	
  echo $sitemap;

?>