<?php
    //error_reporting(E_ALL);
    //error_reporting(E_ALL & ~E_NOTICE | E_STRICT); // Warns on good coding standards
    // ini_set("display_errors", "1");
    require_once(QUBEROOT . '/admin/6qube/core.php');
    session_start();
    $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
    $continue = false;
	$processingpage = true;
	$continue = true;
$package = $_GET['package'];
$uid = is_numeric($_GET['uid']) ? $_GET['uid'] : '';

require_once(QUBEADMIN . 'inc/reseller.class.php');
        $reseller = new Reseller();


    //if passed security validations
    if ($continue)
    {



        if ($_POST)
        {
            //if user has submitted the form, include the processing script
            $firstName = $_POST['first_name'];
            $lastName = $_POST['last_name'];
            $phone = $_POST['phone'];
            $company = $_POST['company'];
            $address = $_POST['address'];
            $address2 = $_POST['address2'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $zip = $_POST['zip'];
            $email = $userInfo['username'];

            //card info
            $cardNumber = $reseller->stripNonAlphaNum($_POST['cardNumber']);
            $expMonth = $reseller->stripNonAlphaNum($_POST['expMonth']);
            $expYear = $reseller->stripNonAlphaNum($_POST['expYear']);
            $cvv = $reseller->stripNonAlphaNum($_POST['cvv']);
            //payment stuff
            //$uid, $addSite and $totalDue set above
            $productID = $pid;
            $monthlyCost = $productInfo['monthly_price'];
            if ($totalDue > $monthlyCost) {
                $setupCharge = ($totalDue - $monthlyCost);
            }

            $a = explode('.', $productInfo['type']);
            $refID = substr($a[1], 0, 14) . '_' . $uid;
            $productName = $productInfo['name'];


            $sessID = session_id();
            //include('includes/process-payment.php');
            //include(QUBEROOT . 'hubs/forms/signup/process-payment.php');
			include('inc/process-payment.php');
        }
        else
        {

            $firstName = $userInfo2['firstname'];
            $lastName = $userInfo2['lastname'];
            $phone = $userInfo2['phone'];
            $company = $userInfo2['company'];
            $address = $userInfo2['address'];
            $address2 = $userInfo2['address2'];
            $city = $userInfo2['city'];
            $state = $userInfo2['state'];
            $zip = $userInfo2['zip'];
        }

		
		$submitURL = 'http://www.alibipartners.com/processor/?uid='.$uid.'';

		include('inc/header.php');
        include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/header.php');
		echo $row['photos'];
?>
    <section id="page-title">
        <div class="container">
            <div class="row">
                <div class="span6">
                    <h1 class='page-title'>Secure Billing Setup</h1>
                </div>
                
            </div>
        </div>
    </section>
    <section id="loading"></section>
    <section id="main">
        <div id="page">
            <div class="row-container light "
                 style="background-attachment:scroll;background-repeat:repeat;background-position:left top;">
                <div class="container">
                    <div class="row">

                       

                        <div class="span12 ">
                            <div class="row-fluid">
                                <? if ( !$userInfo['billing_id2'] && !$paymentSuccess) { ?>
              
              <h2>You're almost done <?= $firstName ?>!</h2>
      
              <p>Just verify your billing information below and provide a payment method to set up a secure billing profile.<br />
                <br />
              <h3>
                <?= $package ?>
              </h3>
              Based on your account type choice you will be billed the following:<br />
              <strong>Monthly Price:</strong> $
              <?= sprintf('%01.2f', $productInfo['monthly_price']) ?>
              <br />
              <? if ($productInfo['setup_price'] != 0): ?>
                                    <strong>One-Time Setup Fee:</strong> $
                                    <?= sprintf('%01.2f', $productInfo['setup_price']) ?>
                                    <br/>
                                <? endif; ?>
                                    <? if ($productInfo['add_site'] > 0 && $addSite): ?>
                                        <strong>Custom Website:</strong> $
                                        <?= sprintf('%01.2f', $productInfo['add_site']) ?>
                                        <br/>
                                    <? endif; ?>
              <br />
              The total due right now is: <strong>$
              <?= sprintf('%01.2f', $totalDue); ?>
              </strong>
              </p>
              <p>Verify and complete the form below to set up your billing profile and activate your account.</p>
              <?
                                    if ($errors)
                                        echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">�</button><p>' . $errors . '</p></div>';
                                    ?>
              <div class="woocommerce">
                <form action="<?= $submitURL ?>" class="jquery_form" method="post" id="sign-up">
                  <div class="col2-set" id="customer_details">
                    <div class="woocommerce-billing-fields">
                   
                      <p class="form-row form-row-first">
                        <label>First Name</label>
                        <input type="text" name="first_name" value="<?= $firstName ?>" class="input-text" />
                      </p>
                      <p class="form-row form-row-last">
                        <label>Last Name</label>
                        <input type="text" name="last_name" value="<?= $lastName ?>" class="input-text" />
                      </p>
                      <div class="clear"></div>
                      <p class="form-row">
                        <label>Phone Number</label>
                        <input type="text" name="phone" value="<?= $phone ?>" class="input-text" />
                      </p>
                     
                      <p class="form-row">
                        <label>Company Name</label>
                        <input type="text" name="company" value="<?= $company ?>" class="input-text" />
                      </p>
             
                      <p class="form-row">
                        <label>Address</label>
                        <input type="text" name="address" value="<?= $address ?>" class="input-text" />
                      </p>
                      <p class="form-row">
                        <label>City</label>
                        <input type="text" name="city" value="<?= $city ?>" class="input-text" />
                      </p>
                      <p class="form-row">
                        <label>State/Province</label>
                        <?= $reseller->stateSelect('state', $state) ?>
                      </p>
                      <p class="form-row">
                        <label>Zip/Postal Code</label>
                        <input type="text" name="zip" size="10" maxlength="10" value="<?= $zip ?>" style="width:80px;" class="input-text" />
                      </p>
                      <p class="form-row">
                        <label style="width:300px;" >Credit Card Number <small>(numbers only)</small></label>
                        <input type="text" name="cardNumber" value="" maxlength="16" autocomplete="off" class="input-text" />
                      </p>
                      <p class="form-row">
                          <label>Card Expiration Date</label>
                          <div style="display:inline-block;margin-top:-5px;width:130px;">
                            <select name="expMonth" class="uniformselect" style="width:130px;">
                              <option value="01">01</option>
                              <option value="02">02</option>
                              <option value="03">03</option>
                              <option value="04">04</option>
                              <option value="05">05</option>
                              <option value="06">06</option>
                              <option value="07">07</option>
                              <option value="08">08</option>
                              <option value="09">09</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                            </select>
                          </div>
                          &nbsp;
                          <div style="display:inline-block;width:150px;">
                            <select name="expYear" class="uniformselect" style="width:150px;">
                           
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                              <option value="2020">2020</option>
                              <option value="2021">2021</option>
                              <option value="2022">2022</option>
                              <option value="2023">2023</option>
                              <option value="2024">2024</option>
                              <option value="2025">2025</option>
                              <option value="2026">2026</option>
                            </select>
                          </div>
                      </p>
                      <p class="form-row">
                        <label style="margin-top:-10px;">CVV Code <small>(3-digit code on back)</small></label>
                        <input type="text" name="cvv" value="" size="4" maxlength="4" autocomplete="off" style="width:80px;" />
                      </p>
                      <input type="submit" value="Process Now" name="submit" class="mybtn" id="submit"  />
                    </div>
                  </div>
                </form>
              </div>
              <? } else if (!$paymentSuccess) { ?>
                                    <p>It appears this account already has a billing profile set up. If this is in error
                                        please contact our support team</a>.</p>
                                <? } else { ?>
                                    <?= $productInfo['edit_region_4'] ?>

                                    <!-- insert build site here -->
                                    <?

                                    #### NEW CODE
                        //if ($userInfo['class'] == 1218 || $userInfo['class'] == 1219)
                        //{ 
                            //Create campaign
                           // /$cid = $reseller->addNewCampaign($uid, 'ignitedLOCAL', NULL, true);
                            //var_dump($cid);
                            //or die('stopped at $cid');
							//require_once(QUBEADMIN . 'inc/hub.class.php');
							///$hub = new Hub();
                            //$hid = $hub->addNewHub($uid, 'ignitedLOCAL', $cid, NULL, TRUE, NULL, $row['user_parent_id']);
                            //var_dump($hid);
							//var_dump($row['user_parent_id']);
                            //or die('stopped at $hid');

                            //$query = "SELECT id FROM hub WHERE id = '" . $hid . "'";
                            //var_dump($query);
                            //$newHubID = $hub->queryFetch($query);
                            //var_dump($newHubID);
                           // or die('stopped at $newHubID');

                            //if ($newHubID)
                           // {
                               // if ($pushinfo = $hub->setupMultiUserHubV2($newHubID['id'], '104764', $uid, $row['user_parent_id']))
                                //{
                                   // $urlupdate = "
                                              //  UPDATE
                                                   // hub
                                               // SET
                                                 // httphostkey = '" . $userdomain . "', 
												 // domain = 'http://" . $userdomain . "', 
												 // city = '" . $city . "', 
												// state = '" . $city . "', 
												 // zip = '" . $zip . "', 
												 // phone = '" . $phone . "'
                                               // WHERE
                                                   // id = '" . $newHubID['id'] . "'
                                               // LIMIT 1";

                                   // $hub->query($urlupdate);
                               /// }
                               // else { exit('pushinfo returned false'); } #remove ELSE
                          //  }
                           // else { exit('NewhubID returned false'); } #remove ELSE

                       // }
                      //  else { exit('USERCLASS returned false'); } #remove ELSE
                        #### /END NEW CODE


                                    ?>


                                <? } ?>
                                <br style="clear:both;"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
        include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/footer.php');
	}
?>