<?php

	$thisPage = "activate";
	$urlid = $_GET["urlid"];
	$productID = $_GET["pid"];
	$displayMessage = $_GET["displayMessage"];
	$successMessage = $displayMessage=='success';
	
	if($productID){
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	$query = "SELECT ID FROM 6q_roles WHERE reseller_user = '".$row['user_parent_id']."' AND ID = '".$productID."'";
	$validRole = $reseller ->queryFetch($query, NULL, 1);
	if($validRole) $validRole = true;
	}

if($_POST){
	//if user has posted form, include process script
	$sessID = session_id();
	
	if($_POST['form']=='createUser')
		include('inc/create-user-script.php');
}

include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/header.php');
echo $row['photos'];
?>

<section class="parallax-section54 neg-margin" style="background-image: url('/users/u93/93408/banner2.jpg');">
  <div class="section-overlay bg-opacity-4">
    <div class="container sec-tpadding-3 sec-bpadding-3">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 pos-margin">
          <div class="feature-box71"><h4 class="uppercase text-white">User Account Creation</h4>
          <div class="title-line-4 white less-margin"></div>
          <div class="clearfix"></div>
        <p>Please fill out the form below to access your account login.</p>
        <form class="form-vertical" method="post" action="/useractivation/?pid=<?=$productID?>">
          <?php if(!$productID){ ?>
          <div id="error-container" class="alert alert-danger"><span id="errormessage"> DENIED! Missing product ID. </span></div>
          <? } ?>
          <?php if(!$validRole){ ?>
          <div id="error-container" class="alert alert-danger"><span id="errormessage"> DENIED! Not a valid product ID. </span></div>
          <? } ?>
          <?php if($errors){ ?>
          <div id="error-container" class="alert alert-danger"><span id="errormessage">
            <?=$errors?>
            </span></div>
          <? } ?>
          <?php if($successMessage){ ?>
          <div id="error-container" class="alert alert-success"><span id="errormessage"> Account created successfully. </span></div>
          <? } ?>
          <?php if($productID && !$successMessage && $validRole){ ?>
          <div class="row info-container">
            <div class="col-sm-12 form-group">
              <div class="form-group account-info-container">
                <label>Login Information</label>
                <div class="row form-group">
                  <div class="col-sm-12 control-group">
                    <input type="text" name="email" id="email" value="<?=$_POST['email']?>" class="form-control" placeholder="Email&#x20;*"  />
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-6 control-group">
                    <input type="password" name="password" id="password" class="form-control" value="" placeholder="Password"  />
                  </div>
                  <div class="col-sm-6 control-group">
                    <input type="password" name="password2" id="password2" class="form-control" value="" placeholder="Confirm&#x20;Passowrd" />
                    <span class="help-block hide" id="last_name_DIV"></span></div>
                </div>
              </div>
              <div class="form-group account-info-container">
                <label>Account Information</label>
                <div class="row form-group">
                  <div class="col-sm-6 control-group">
                    <input type="text" name="first_name" id="first_name" class="form-control" value="<?=$_POST['first_name']?>" placeholder="First&#x20;Name&#x20;*"  />
                  </div>
                  <div class="col-sm-6 control-group">
                    <input type="text" name="last_name" id="last_name" class="form-control" value="<?=$_POST['last_name']?>" placeholder="Last&#x20;Name&#x20;*" />
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-6 control-group">
                    <input type="text" name="phone" id="phone" class="form-control" value="<?=$_POST['phone']?>" placeholder="Phone&#x20;*"  />
                  </div>
                  <div class="col-sm-6 control-group">
                    <input type="text" name="company" id="company" class="form-control" value="<?=$_POST['company']?>" placeholder="Company" />
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-12 control-group">
                    <input type="text" name="address" id="address" value="<?=$_POST['address']?>" class="form-control" placeholder="Street&#x20;*"  />
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-6 control-group">
                    <input type="text" name="city" id="city" value="<?=$_POST['city']?>" class="form-control" placeholder="City&#x20;*" />
                  </div>
                  <div class="col-sm-6 control-group">
                    <?=$reseller->stateSelect('state', $_POST['state'])?>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-6 control-group">
                    <input type="text" name="zip" id="zip" value="<?=$_POST['zip']?>" class="form-control" placeholder="Zip/Postal&#x20;Code&#x20;*" />
                  </div>
                  <div class="col-sm-6 control-group">
                    <input type="text" name="website_url" id="website_url" value="<?=$_POST['zip']?>" class="form-control" placeholder="Website&#x20;URL" />
                  </div>
                </div>
              </div>
              <div class="row ">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="billing-info-container col-sm-12">
                      <label>Sales Rep #ID</label>
                      <div class="row form-group">
                        <div class="col-sm-12 control-group">
                          <input type="text" name="referral_id" id="referral_id" value="<?=$_POST['referral_id']?>" class="form-control" placeholder="Sales&#x20;Rep&#x20;#ID"  />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <input name="form" value="createUser" type="hidden" />
              <input name="reseller_id" value="<?=$row['user_parent_id']?>" type="hidden" />
              <input name="product_id" value="<?=$productID?>" type="hidden" />
              <button type="submit" id="submit-button" class="submit-button"> <i id="spin-image"></i><span id="submit-text">Create Account</span> </button>
            </div>
          </div>
          <? } ?>
        </form></div>
        </div>
      </div>
    </div>
  </div>
</section>

<div style="clear:both;"></div>
<?php
include_once(QUBEROOT . 'hubs/themes/'.$this->hub->theme_path.'/inc/footer.php');
?>
