<?php
/**
 * Created by PhpStorm.
 * User: Jon
 * Date: 11/4/2015
 * Time: 3:30 PM
 */

$replaceValues = array('navtag'=>'', 'navtag1'=>'', 'slider'=>'', 'logo'=>'', 'posts1'=>'', 'post1'=>'', 'category1'=>'', 'pposts1'=>'', 'btitle1'=>'', 'ptitle1'=>'');
// DO IT:

if(preg_match('/\[\[(.*)\]\]/i', $content)){ //regex to check if this field has tag(s)
    foreach($replaceValues as $replaceKeyword=>$replaceValue){

        if($replaceKeyword=='navtag' || $replaceKeyword=='navtag1' || $replaceKeyword=='slider' || $replaceKeyword=='logo' || $replaceKeyword=='posts1' || $replaceKeyword=='post1' || $replaceKeyword=='category1' || $replaceKeyword=='pposts1' || $replaceKeyword=='btitle1' || $replaceKeyword=='ptitle1'){

            if($replaceKeyword=='navtag'){
                if($hub_pages){

                    $replaceValue .= '<nav id="primary-menu" class="nav_replace dark">
                                                            <ul><li class="current"><a href="/">Home</a></li>';
                    //for instructions on customizing elements (ul, li, a) see http://6qube.com/admin/pagesv2-customwraps.htm
                    $customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
                        'inner1'=>'<li>', 'inner2'=>'</li>',
                        'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
                        'aClassActive'=>'currentPage',
                        'liClassActive'=>'selected');

                    foreach($hub_pages as $key=>$value)
                    {
                        ob_start();
                        $replaceValue .= $hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
                        $replaceValue .= ob_get_clean();
                    }
                    $replaceValue .= '</ul>
                                                        </nav>';

                }
            }
            if($replaceKeyword=='navtag1'){
                if($hub_pages) {

                    $replaceValue .= '<nav id="primary-menu" class="1nav_replace style-2 center">
                                                            <div class="container clearfix">
                                                                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                                                                <ul>
                                                                    <li class="current"><a href="/">Home</a></li>';
                    //for instructions on customizing elements (ul, li, a) see http://6qube.com/admin/pagesv2-customwraps.htm
                    $customWraps = array('outer1' => '<ul>', 'outer2' => '</ul>',
                        'inner1' => '<li>', 'inner2' => '</li>',
                        'inner1Sub' => '<li>', 'inner2Sub' => '</li>',
                        'aClassActive' => 'currentPage',
                        'liClassActive' => 'selected');

                    foreach ($hub_pages as $key => $value) {
                        ob_start();
                        $replaceValue .= $hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
                        $replaceValue .= ob_get_clean();
                    }
                    $replaceValue .= '</ul>
                                                            </div>
                                                        </nav>';

                } else {

                    $replaceValue .= '<nav id="primary-menu" class="1nav_replace style-2 center">
                                                            <div class="container clearfix">
                                                                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                                                                <ul>
                                                                    <li class="current"><a href="/">Home</a></li>
                                                            </ul>
                                                            </div>
                                                        </nav>';
                }
            }
            if($replaceKeyword=='slider'){
                if($slides){
                    $replaceValue .= '<div class="slider_replace swiper-container swiper-parent">
                                                            <div class="swiper-wrapper">';
                    foreach($slides as $photo) {
                        $counter = ($c+0);
                        $replaceValue .= '<div id="slide'.$counter.'" class="swiper-slide dark" style="background-image: url(\'/users/'.$photo->background.'\')">
                                                            <div class="container clearfix">
                                                             <div class="slider-caption slider-caption-center">
                                                                <h2 id="slideTitle'.$counter.'" data-caption-animate="fadeInUp">
                                                                  '.$photo->title.'
                                                                </h2>
                                                                <p id="slideDesc'.$counter.'" data-caption-animate="fadeInUp" data-caption-delay="200">
                                                                 '.$photo->description.'
                                                                </p>
                                                             </div>
                                                            </div>
                                                          </div>';
                        $c++;
                    }
                    $replaceValue .= '</div>
                                                            <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                                                            <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                                                          </div>';
                } else {

                    $replaceValue .= '<div class="slider_replace swiper-container swiper-parent">
                                                            <div class="swiper-wrapper">
                                                      </div>
                                                            <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                                                            <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                                                          </div>';
                }
            }
            if($replaceKeyword=='posts1'){

                $replaceValue = '<div id="posts" class="posts1 post-timeline clearfix">

                                    <div class="timeline-border"></div>

                                    <div class="entry clearfix">
                                        <div class="entry-timeline">
                                            10<span>June</span>
                                            <div class="timeline-divider"></div>
                                        </div>
                                        <div class="entry-image">
                                            <a href="/hubs/themes/universal/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/hubs/themes/universal/images/blog/standard/17.jpg" alt="Standard Post with Image"></a>
                                        </div>
                                        <div class="entry-title">
                                            <h2><a href="#">This is a Standard post with a Preview Image</a></h2>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li><a href="#"><i class="icon-user"></i> admin</a></li>
                                            <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                                            <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                        </ul>
                                        <div class="entry-content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                                            <a href="#"class="more-link">Read More</a>
                                        </div>
                                    </div>


                                </div>';

            }
            if($replaceKeyword=='post1'){
                $replaceValue = '<div id="post1" class="single-post nobottommargin">

                                    <!-- Single Post
                                    ============================================= -->
                                    <div class="entry clearfix">

                                        <!-- Entry Title
                                        ============================================= -->
                                        <div class="entry-title">
                                            <h2>This is a Standard Post Example</h2>
                                        </div><!-- .entry-title end -->

                                        <!-- Entry Meta
                                        ============================================= -->
                                        <ul class="entry-meta clearfix">
                                            <li><i class="icon-calendar3"></i> 10th July 2015</li>
                                            <li><a href="#"><i class="icon-user"></i> admin</a></li>
                                            <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                                            <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                        </ul><!-- .entry-meta end -->

                                        <!-- Entry Image
                                        ============================================= -->
                                        <div class="entry-image">
                                            <a href="#"><img src="/hubs/themes/universal/images/blog/full/1.jpg" alt="Blog Single"></a>
                                        </div><!-- .entry-image end -->

                                        <!-- Entry Content
                                        ============================================= -->
                                        <div class="entry-content notopmargin">

                                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. <a href="#">Curabitur blandit tempus porttitor</a>. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>

                                            <blockquote><p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper.</p></blockquote>


                                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>
                                            <!-- Post Single - Content End -->

                                            <!-- Tag Cloud
                                            ============================================= -->
                                            <div class="tagcloud clearfix bottommargin">
                                                <a href="#">general</a>
                                                <a href="#">information</a>
                                                <a href="#">media</a>
                                                <a href="#">press</a>
                                                <a href="#">gallery</a>
                                                <a href="#">illustration</a>
                                            </div><!-- .tagcloud end -->

                                            <div class="clear"></div>

                                            <!-- Post Single - Share
                                            ============================================= -->
                                            <div class="si-share noborder clearfix">
                                                <span>Share this Post:</span>
                                                <div>
                                                    <a href="#" class="social-icon si-borderless si-facebook">
                                                        <i class="icon-facebook"></i>
                                                        <i class="icon-facebook"></i>
                                                    </a>
                                                    <a href="#" class="social-icon si-borderless si-twitter">
                                                        <i class="icon-twitter"></i>
                                                        <i class="icon-twitter"></i>
                                                    </a>
                                                    <a href="#" class="social-icon si-borderless si-pinterest">
                                                        <i class="icon-pinterest"></i>
                                                        <i class="icon-pinterest"></i>
                                                    </a>
                                                    <a href="#" class="social-icon si-borderless si-gplus">
                                                        <i class="icon-gplus"></i>
                                                        <i class="icon-gplus"></i>
                                                    </a>
                                                    <a href="#" class="social-icon si-borderless si-rss">
                                                        <i class="icon-rss"></i>
                                                        <i class="icon-rss"></i>
                                                    </a>
                                                    <a href="#" class="social-icon si-borderless si-email3">
                                                        <i class="icon-email3"></i>
                                                        <i class="icon-email3"></i>
                                                    </a>
                                                </div>
                                            </div><!--- Post Single - Share End -->

                                        </div>
                                    </div><!-- .entry end -->


                                </div>';
            }
            if($replaceKeyword=='logo'){

                $replaceValue = '<div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/users'.$logo.'"> <img src="/users'.$logo.'" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/users'.$logo.'"> <img src="/users'.$logo.'" alt=""></a> </div>';

            }
            if($replaceKeyword=='category1'){

                $replaceValue = '<ul id="pcategories">
                                            <li><a href="#"><div>Category 1</div></a></li>
                                            <li><a href="#"><div>Category 2</div></a></li>
                                            <li><a href="#"><div>Category 3</div></a></li>
                                        </ul>';

            }
            if($replaceKeyword=='pposts1'){

                $replaceValue = '<ul id="pposts">
                                  <h5>Post Category</h5>
                                  <li><a href="#"><div>Popular Post Title 1</div></a></li>
                                  <li><a href="#"><div>Popular Post Title 1</div></a></li>
                                  <li><a href="#"><div>Popular Post Title 1</div></a></li>
                                </ul>';

            }
            if($replaceKeyword=='btitle1'){

                $replaceValue = '<div id="btitle1" class="container clearfix">
                                  <h1>Dynamic Blog Title & Category Title</h1>
                                  <ol class="breadcrumb editContent">
                                    <li><a href="/">Home</a></li>
                                    <li class="active">Dynamic Category</li>
                                  </ol>
                                </div>';

            }
            if($replaceKeyword=='ptitle1'){

                $replaceValue = '<div id="ptitle1" class="container clearfix">
                                  <h1>This is a Standard Post Example</h1>
                                  <span>Posted on November 6th, 2015</span>
                                  <ol class="breadcrumb editContent">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="#">Category</a></li>
                                    <li class="active">This is a Standard Post Example</li>
                                  </ol>
                                </div>';

            }
            $content = str_replace('[['.$replaceKeyword.']]', $replaceValue, $content);

        } else {
            $content = str_replace('[['.$replaceKeyword.']]', $replaceValue, $content);
            unset($replaceValue);
        }
    }

} //end if(preg_match('/\(\((.*)\)\)/i', $row[$searchField]))

$html = str_get_html($content);