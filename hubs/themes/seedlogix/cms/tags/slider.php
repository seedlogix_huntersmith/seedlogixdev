<?php
if($slides){
$replaceValue .= '<div class="slider_replace swiper-container swiper-parent">
										<div class="swiper-wrapper">';
foreach($slides as $photo) {
	$counter = ($c+0);
	$replaceValue .= '<div id="slide'.$counter.'" class="swiper-slide dark" style="background-image: url(\'/users/'.$photo->background.'\')">
										<div class="container clearfix">
										 <div class="slider-caption slider-caption-center">
											<h2 id="slideTitle'.$counter.'" data-caption-animate="fadeInUp">
											  '.$photo->title.'
											</h2>
											<p id="slideDesc'.$counter.'" data-caption-animate="fadeInUp" data-caption-delay="200">
											 '.$photo->description.'
											</p>
										 </div>
										</div>
									  </div>';
	$c++;
}
$replaceValue .= '</div>
										<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
										<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
									  </div>';
} else {

$replaceValue .= '<div class="slider_replace swiper-container swiper-parent">
										<div class="swiper-wrapper">
								  </div>
										<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
										<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
									  </div>';
}
?>