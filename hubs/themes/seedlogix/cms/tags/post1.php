<?php
$replaceValue = '<div id="post1" class="single-post nobottommargin">

<!-- Single Post
============================================= -->
<div class="entry clearfix">

	<!-- Entry Title
	============================================= -->
	<div class="entry-title">
		<h2>This is a Standard Post Example</h2>
	</div><!-- .entry-title end -->

	<!-- Entry Meta
	============================================= -->
	<ul class="entry-meta clearfix">
		<li><i class="icon-calendar3"></i> 10th July 2015</li>
		<li><a href="#"><i class="icon-user"></i> admin</a></li>
		<li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
		<li><a href="#"><i class="icon-camera-retro"></i></a></li>
	</ul><!-- .entry-meta end -->

	<!-- Entry Image
	============================================= -->
	<div class="entry-image">
		<a href="#"><img src="/hubs/themes/universal/images/blog/full/1.jpg" alt="Blog Single"></a>
	</div><!-- .entry-image end -->

	<!-- Entry Content
	============================================= -->
	<div class="entry-content notopmargin">

		<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

		<p>Nullam id dolor id nibh ultricies vehicula ut id elit. <a href="#">Curabitur blandit tempus porttitor</a>. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>

		<blockquote><p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper.</p></blockquote>


		<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

		<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>
		<!-- Post Single - Content End -->

		<!-- Tag Cloud
		============================================= -->
		<div class="tagcloud clearfix bottommargin">
			<a href="#">general</a>
			<a href="#">information</a>
			<a href="#">media</a>
			<a href="#">press</a>
			<a href="#">gallery</a>
			<a href="#">illustration</a>
		</div><!-- .tagcloud end -->

		<div class="clear"></div>

		<!-- Post Single - Share
		============================================= -->
		<div class="si-share noborder clearfix">
			<span>Share this Post:</span>
			<div>
				<a href="#" class="social-icon si-borderless si-facebook">
					<i class="icon-facebook"></i>
					<i class="icon-facebook"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-twitter">
					<i class="icon-twitter"></i>
					<i class="icon-twitter"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-pinterest">
					<i class="icon-pinterest"></i>
					<i class="icon-pinterest"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-gplus">
					<i class="icon-gplus"></i>
					<i class="icon-gplus"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-rss">
					<i class="icon-rss"></i>
					<i class="icon-rss"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-email3">
					<i class="icon-email3"></i>
					<i class="icon-email3"></i>
				</a>
			</div>
		</div><!--- Post Single - Share End -->

	</div>
</div><!-- .entry end -->


</div>';
?>