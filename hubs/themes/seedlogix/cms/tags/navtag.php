<?php
if($hub_pages){

$replaceValue .= '<nav id="primary-menu" class="nav_replace dark">
										<ul><li class="current"><a href="/">Home</a></li>';
//for instructions on customizing elements (ul, li, a) see http://6qube.com/admin/pagesv2-customwraps.htm
$customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
	'inner1'=>'<li>', 'inner2'=>'</li>',
	'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
	'aClassActive'=>'currentPage',
	'liClassActive'=>'selected');

foreach($hub_pages as $key=>$value)
{
	ob_start();
	$replaceValue .= $hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
	$replaceValue .= ob_get_clean();
}
$replaceValue .= '</ul>
									</nav>';

} else {
	
$replaceValue .= '<nav id="primary-menu" class="nav_replace dark">
										<ul><li class="current"><a href="/">Home</a></li></ul>
									</nav>';	
	
}

?>