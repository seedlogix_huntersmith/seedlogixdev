<!-- Hero -->
	<div id="hero" class="hero-main">
		<div class="hero-title">
			<div class="wrapper">
				<div class="col col-8">
					<h1 class="type--animate" data-period="5000" data-type='[ "All-in-One Marketing Software", "Production Tools", "Marketing Automation", "Lead Management", "ROI Tracking" ]'>
						<span class="wrap"></span>
					</h1>
				</div>
			</div>
		</div>
		<div class="hero-main-image">
			<img src="/hubs/themes/seedlogix/images/software-hero.png">
		</div>
		<div class="hero-shadow"></div>
	</div>
	<!-- Production Tools -->
	<div id="productiontools" class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Production Tools</span> <br> Efficient production tools makes inbound marketing affordable.</h1>
					<p class="mt--50 text--center">SeedLogix's Production Tools enable marketers of all shapes and sizes to deploy a dominate internet foundation. Anyone can leverage our responsive content management system, content blogging platform, powerful landing pages, and easy-to-use custom lead generation forms.</p>
				</div>
			</div>
			<div class="row2-center mt--50">
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/responsive.svg"></div>CMS (Site/Sites Management)</h2>
						<p class="color--lightgrey">Our responsive content management system is optimized no matter what browser or device your prospects find you on. This provides extremely fast load times and higher conversions. Easily take control of your site management without IT.</p>
						<a class="mt--20 link--mono" href="/products/responsive-sites/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/content.svg"></div>Content Blogging</h2>
						<p class="color--lightgrey">Turn your online footprints into lead magnets with our blogging software. Marketers can fuel their inbound marketing efforts with quality content publishing that is fully integrated with social media, blogging, SEO, SEM, and all our inbound marketing tools.</p>
						<a class="mt--20 link--mono" href="/products/content-blogging/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/landing.svg"></div>Landing Pages</h2>
						<p class="color--lightgrey">All our landing pages are powered by our Multi Site CMS. This allows marketers to easily build and deploy responsive landing pages designed with proven layouts to generate more leads. Easily create and attach custom forms to any landing page to capture your leads.</p>
						<a class="mt--20 link--mono" href="/products/landing-pages/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/lead-generation.svg"></div>Lead Generation Forms</h2>
						<p class="color--lightgrey">Create lead generation forms for each landing page, blog, or website with our easy-to-use custom form builder. Our form builder offers 12 field types, multiple site usage, simple management, and much more. Online lead capture simplified.</p>
						<a class="mt--20 link--mono" href="/products/lead-generation-forms/">> View Feature</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Marketing Automation -->
	<div id="marketingautomation" class="boxed ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Marketing Automation</span> goes way beyond simply sending email.</h1>
					<p class="mt--50 text--center">SeedLogix's Marketing Automation suite helps marketers create a unique and personalized customer experience through email, website, landing pages, blogs, and social media profiles. Our simple to use system allows users to create a marketing workflow in minutes and cost effectively.</p>
				</div>
			</div>
			<div class="row2-center mt--50">
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/email-marketing.svg"></div>Email Marketing</h2>
						<p class="color--lightgrey">Email Marketing Software should be designed so you can quickly and efficiently launch conditioning email messages to prospects - quickly turning them into hot, actionable leads for your sales team.</p>
						<a class="mt--20 link--mono" href="/products/email-marketing/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/social-scheduling.svg"></div>Social Media Scheduling</h2>
						<p class="color--lightgrey">SeedLogix's seamless integration with Facebook, Twitter, and LinkedIn allows users to easily create a schedule to post content automatically across your social networks - adding dynamic automation to all your profiles.</p>
						<a class="mt--20 link--mono" href="/products/social-media-scheduling/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/lead-nurturing.svg"></div>Lead Nurturing</h2>
						<p class="color--lightgrey">Constant, personalized drip marketing is the key to converting more leads automatically. Within our system, you can set-up unlimited amounts of auto-responder emails to go out to each of your prospects.</p>
						<a class="mt--20 link--mono" href="/products/lead-nurturing/">> View Feature</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Lead Management -->
	<div id="leadmanagement" class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Lead Management</span> <br>Manage leads from all your touchpoints online efficiently.</h1>
					<p class="mt--50 text--center">SeedLogix's Lead Management suite provides a 360 degree view of all your leads generated online through your website, landing pages, microsites, blogs and social media accounts. Users can manage when, where and how leads are finding their business and quickly respond to those leads directly from our lead timeline views.</p>
				</div>
			</div>
			<div class="row2-center mt--50">
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/lead-management.svg"></div>Lead Manager</h2>
						<p class="color--lightgrey">Lead Management has never been easier with lead view timelines. With SeedLogix lead dashboard, users can see everything they need to maximize conversions - history, activity and triggers; all in one simple view.</p>
						<a class="mt--20 link--mono" href="/products/lead-manager/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/contact-management.svg"></div>Contact Manager</h2>
						<p class="color--lightgrey">Our contact Manager is designed to be a light weight lead qualification system. You can tasks, call notes, and activity history until your leads are actionable opportunities and ready for your CRM.</p>
						<a class="mt--20 link--mono" href="/products/contact-manager/">> View Feature</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ROI Tracking -->
	<div id="roitracking" class="boxed ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center">Simplified aggregated <span>ROI reporting</span> for all your domains &amp; touchpoints.</h1>
					<p class="mt--50 text--center">SeedLogix's ROI Reporting suite displays a complete aggregated dashboard of all your touchpoints, providing the big picture all marketers are looking for. Our layered dashboards display visits, keyword rankings, keyword traffic, social media adoption, conversion rates and more.</p>
				</div>
			</div>
			<div class="row2-center mt--50">
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/reporting.svg"></div>Aggregated Web Reporting</h2>
						<p class="color--lightgrey">Our aggregated dashboards provide end-to-end analytics ensuring your marketing efforts are receiving maximum conversions through all your online touchpoints.</p>
						<a class="mt--20 link--mono" href="/products/aggregated-web-reporting/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/rankings.svg"></div>SERP Rankings</h2>
						<p class="color--lightgrey">Our SERP reporting provides an in-depth look into where all your online touchpoints are ranking organically on Google, Yahoo &amp; Bing for target keywords, every 24 hours.</p>
						<a class="mt--20 link--mono" href="/products/serp-rankings/">> View Feature</a>
					</div>
				</div>
				<div class="col col-4 mb--80 animate">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/software-img.jpg"></div>
					<div>
						<h2 class="feat"><div class="software-icon"><img src="/hubs/themes/seedlogix/images/call.svg"></div>Call Tracking &amp; Recording</h2>
						<p class="color--lightgrey">Users have the ability to quickly assign phone tracking numbers. This enables phone call recording on all incoming calls, and allows you to listen to the quality of the calls through an easy-to-use interface.</p>
						<a class="mt--20 link--mono" href="/products/call-tracking-and-recording/">> View Feature</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Extras -->
	<div class="scroll--down"></div>
	<div class="bg--light cards">
		<div class="wrapper">
			<div class="row-center">
				<div class="col col-2 pb--20 animate">
					<a href="solutions.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/solution-professional.png">
						</div>
						<h2>Professional Services</h2>
						<p>Our team is here to ensure your inbound marketing strategy is designed and executed efficiently and cost effectively.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="solutions.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/solution-support.png">
						</div>
						<h2>Training &amp; Support</h2>
						<p>We stride to make our software as simple as possible, however our technical support team is here to make it even easier.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="solutions.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/solution-labs.png">
						</div>
						<h2>SeedLogix® Labs</h2>
						<p>The majority of our features are driven by our customers and our development team is hard at work delivering what you want.</p>
					</div>
					</a>
				</div>
			</div>
		</div>
	</div>