<!-- Hero -->
	<div id="hero" class="hero">
		<div class="bg--hero">
			<img src="/hubs/themes/seedlogix/images/hero.png">
		</div>
		<div class="hero-title">
			<div class="wrapper">
				<div class="col col-8">
					<h1 class="type--animate" data-period="5000" data-type='[ "All-in-One Marketing Software", "Production Tools", "Marketing Automation", "Lead Management", "ROI Tracking" ]'>
						<span class="wrap"></span>
					</h1>
				</div>
			</div>
		</div>
		<div class="hero-footer animate">
			<div class="wrapper">
				<div class="col col-6">
					<h2>Simplifying CMS, Marketing Automation Tools and Reporting</h2>
					<p>for Multi-Location Businesses, Franchises &amp; Marketing Agencies.</p>
				</div>
				<div class="col col-1">
					<div class="btn full">
						Free Trial
					</div>
				</div>
				<div class="col col-1">
					<div class="btn white">
						Take Tour
					</div>
				</div>
			</div>
		</div>
		<div class="hero-shadow"></div>
	</div>
	<!-- Software -->
	<div class="ptb--100 circles">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center">Deploy &amp; Market Your Locations or Clients in a <span>Single Console.</span></h1>
				</div>
			</div>
			<div class="row mt--50 animate">
				<div class="col col-2">
					<div class="feat-wrapper">
						<div class="feat-img">
							<img src="/hubs/themes/seedlogix/images/index-productiontools.png">
						</div>
					</div>
					<h2 class="feat">Production Tools</h2>
					<p class="color--grey">Creating a successful online foundation is more than a single website and single blog.</p>
					<a class="mt--20 link--mono" href="/software#productiontools">> View Features</a>
				</div>
				<div class="col col-2">
					<div class="feat-wrapper">
						<div class="feat-img">
							<img src="/hubs/themes/seedlogix/images/index-marketing.png">
						</div>
					</div>
					<h2 class="feat">Marketing Automation</h2>
					<p class="color--grey">We have simplified marketing automation allowing any user type to convert more prospects online.</p>
					<a class="mt--20 link--mono" href="/software#marketingautomation">> View Features</a>
				</div>
				<div class="col col-2">
					<div class="feat-wrapper">
						<div class="feat-img">
							<img src="/hubs/themes/seedlogix/images/index-leadmanagement.png">
						</div>
					</div>
					<h2 class="feat">Lead Management</h2>
					<p class="color--grey">With complete lead profiles users are able to convert more leads efficiently.</p>
					<a class="mt--20 link--mono" href="/software#leadmanagement">> View Features</a>
				</div>
				<div class="col col-2">
					<div class="feat-wrapper">
						<div class="feat-img">
							<img src="/hubs/themes/seedlogix/images/index-roi.png">
						</div>
					</div>
					<h2 class="feat">ROI Tracking</h2>
					<p class="color--grey">Get a full 360° view of your inbound marketing efforts to define a true return on investment.</p>
					<a class="mt--20 link--mono" href="/software#roitracking">> View Features</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Features -->
	<div class="bg--light cards">
		<div class="wrapper">
			<div class="row">
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/responsive.svg">
						</div>
						<h2>Responsive CMS</h2>
						<p>SeedLogix is designed to support 1 or 1000s of responsive websites from a single user.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/content.svg">
						</div>
						<h2>Content Blogging</h2>
						<p>Publish high quality content to your blog and social accounts easily with our blogging software.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/landing.svg">
						</div>
						<h2>Landing Pages</h2>
						<p>Deploy multiple landing pages to target specific products, services, or aspects of your business.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/lead-generation.svg">
						</div>
						<h2>Lead Generation Forms</h2>
						<p>Easily build lead generation forms that collect the right customer data to increase conversions.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/email-marketing.svg">
						</div>
						<h2>Email Marketing</h2>
						<p>With SeedLogix's marketing automation stack, you can constantly be in front of your leads.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/social-scheduling.svg">
						</div>
						<h2>Social Media Scheduling</h2>
						<p>Schedule and automate your social media activity to Facebook, Twitter &amp; LinkedIn.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/lead-management.svg">
						</div>
						<h2>Lead Management</h2>
						<p>SeedLogix's lead timelines help users understand more about their leads before they reach out.</p>
					</div>
					</a>
				</div>
				<div class="col col-2 pb--20 animate">
					<a href="feature.html">
					<div class="card shadow">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/reporting.svg">
						</div>
						<h2>Aggregated Web Reporting</h2>
						<p>Understand how an entire online ecosystem is performing in a single view.</p>
					</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="scroll--down"></div>
	<!-- Feature Large -->
	<div class="boxed">
		<div class="wrapper">
			<div class="col col-4 animate">
				<div class="feat-lg-text">
					<h1>Truly Complete Inbound <span>Marketing Platform.</span></h1>
					<p>Inbound marketing software is essential for getting found by more customers online. Our internet marketing software is a single platform with multiple tools. These tools allow your brand, or your client's brand, to be more visible and convert more prospects into actual customers online.</p>
					<div class="col-2 mt--50">
						<div class="btn">
							Learn More
						</div>
					</div>
				</div>
			</div>
			<div class="col col-4 line--0 animate">
				<img width="980" height="auto" src="/hubs/themes/seedlogix/images/feat-lg.png">
			</div>
		</div>
	</div>
	<!-- Stats -->
	<div class="ptb--100 stats">
		<div class="wrapper">
			<div class="row animate">
				<div class="col col-2">
					<div class="stat">
						<h1>42K<span>+</span></h1>
						<h2>Client Users</h2>
					</div>
				</div>
				<div class="col col-2">
					<div class="stat">
						<h1>225K<span>+</span></h1>
						<h2>Websites Live Today</h2>
					</div>
				</div>
				<div class="col col-2">
					<div class="stat">
						<h1>165K<span>+</span></h1>
						<h2>Blogs Launched</h2>
					</div>
				</div>
				<div class="col col-2">
					<div class="stat">
						<h1>350K<span>+</span></h1>
						<h2>Landing Pages</h2>
					</div>
				</div>
			</div>
		</div>
	</div>