<!-- header -->
	<div id="header" class="header-wrapper">
		<div class="wrapper">
			<div class="col col-3">
				<div class="row">
					<a href="/"><div class="logo">SeedLogix</div></a>
					<h4 class="tag"></h4>
					<div class="nav-active">
						<div class="hamburger hamburger--spring">
							<div class="hamburger-box">
								<div class="hamburger-inner"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col col-5">
				<ul class="nav">
					<li class="dropdown">
						<div class="main-nav">
							<a href="/software/"><p>Software</p></a>
							<div class="arrow"></div>
						</div>
						<ul>
							<li><a href="/software#productiontools" class="scroll">Production Tools</a></li>
							<li><a href="/software#marketingautomation" class="scroll">Marketing Automation</a></li>
							<li><a href="/software#leadmanagement" class="scroll">Lead Management</a></li>
							<li><a href="/software#roitracking" class="scroll">ROI Tracking</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<div class="main-nav">
							<a href="/solutions/"><p>Solutions</p></a>
							<div class="arrow"></div>
						</div>
						<ul>
							<li><a href="/solutions/franchise/">Franchise</a></li>
							<li><a href="/solutions/manufacturers/">Manufacturers</a></li>
							<li><a href="/solutions/agent-organizations/">Agent Organizations</a></li>
							<li><a href="/solutions/lead-generation/">Lead Generation</a></li>
						</ul>
					</li>
					<li>
						<div class="main-nav">
							<p><a href="/pricing/">Pricing</a></p>
							<div class="arrow"></div>
						</div>
						<ul></ul>
					</li>
					<li class="dropdown">
						<div class="main-nav">
							<p>Company</p>
							<div class="arrow"></div>
						</div>
						<ul>
							<li><a href="/contact/">Contact</a></li>
							<li><a href="http://blog.seedlogix.com/">Blog</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<div class="main-nav user">
							<div class="user-icon"></div>
							<div class="arrow"></div>
						</div>
						<ul>
							<li><a href="http://login.seedlogix.com/"><b>Login</b></a></li>
							<li><a href="/sign-up/">Sign Up</a></li>
							<li><a href="/request-demo/">Request a Demo</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>