	<!-- Hero -->
	<div id="hero" class="hero-main">
		<div class="hero-title">
			<div class="wrapper">
				<div class="col col-8">
					<h1 class="type--animate" data-period="5000" data-type="[ &quot;Pricing Plans&quot; ]">
						<span class="wrap"></span>
					</h1>
				</div>
			</div>
		</div>
		<div class="hero-main-image">
			<img src="/hubs/themes/seedlogix/images/pricing-hero.png">
		</div>
		<div class="hero-shadow"></div>
	</div>
	<!-- Pricing description -->
	<div class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-5 center">
					<h1 class="text--center"><span>Service-based Solutions</span> <br> for every kind of Company.</h1>
					<p class="mt--50 text--center">We provide clients with the highest quality service, software, and reliability. Our team is dedicated to making sure your needs are understood, and our performance surpasses expectations. With our solution plans you need simply share your goals and timeframe, and we'll take care of your digital marketing campaigns. It's our belief that a business owner's role is to focus on its growth, so let us aid that effort by providing you with a professional team ready to deliver successful marketing across all your channels!</p>
					<h1 class="mt--100 text--center">Explore the advantages of our four elite<br> <span>Marketing plans and Set-Up assistance.</span></h1>
				</div>
			</div>
		</div>
	</div>
	<!-- Marketing pricing -->
	<div class="scroll--down"></div>
	<div class="bg--light">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Marketing</span> Plans</h1>
					<p class="mt--50 text--center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>
			</div>
			<div class="row white-wrap animate">

				<div class="col col-2 border-right p1 open-options">
					<div class="price-title">
						<h2>Professional</h2>
						<h1 class="price"><span>$</span>3.5K</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Dedicated Inbound Marketing Team</p>
						</div>
						<div class="price-option">
							<p>Corporate Inbound Strategy and Planning</p>
						</div>
						<div class="price-option">
							<p>Buyer Persona Research and Development</p>
						</div>
						<div class="price-option">
							<p>Corporate Content Marketing <br><span class="color--lightgrey">(Weekly)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(Weekly on 3 major social platforms)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Layered Search Engine Optimization</p>
						</div>
						<div class="price-option">
							<p>Keyword Audit and On-going Optimization</p>
						</div>
						<div class="price-option">
							<p>Multi Locational Blogging <br> <span class="color--lightgrey">(1 article per week)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Locational Social Posting <br><span class="color--lightgrey">(Weekly posts)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(1 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Content Offer <br><span class="color--lightgrey">(1 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Monthly Analytics Review, Strategy and Goal Setting Meeting</p>
						</div>
						<div class="price-option">
							<p>Add-on Product Focused Marketing Campaign +$400/m</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p2 open-options">
					<div class="price-title">
						<h2>Corporate</h2>
						<h1 class="price"><span>$</span>6K</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Dedicated Inbound Marketing Team</p>
						</div>
						<div class="price-option">
							<p>Corporate Inbound Strategy and Planning</p>
						</div>
						<div class="price-option">
							<p>Buyer Persona Research and Development</p>
						</div>
						<div class="price-option">
							<p>Corporate Content Marketing <br><span class="color--lightgrey">(2-3 per week)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(2-3/week on 3 major social platforms)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Layered Search Engine Optimization</p>
						</div>
						<div class="price-option">
							<p>Keyword Audit and On-going Optimization</p>
						</div>
						<div class="price-option">
							<p>Multi Locational Blogging <br> <span class="color--lightgrey">(2-3 articles per week)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Locational Social Posting <br><span class="color--lightgrey">(2-3 posts/week)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Content Offer <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Monthly Analytics Review, Strategy and Goal Setting Meeting</p>
						</div>
						<div class="price-option">
							<p>Add-on Product Focused Marketing Campaign +$600/m</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p3 open-options">
					<div class="price-title">
						<h2>Enterprise</h2>
						<h1 class="price"><span>$</span>7.5K</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Dedicated Inbound Marketing Team</p>
						</div>
						<div class="price-option">
							<p>Corporate Inbound Strategy and Planning</p>
						</div>
						<div class="price-option">
							<p>Buyer Persona Research and Development</p>
						</div>
						<div class="price-option">
							<p>Corporate Content Marketing <br><span class="color--lightgrey">(4-5 per week)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(3-5/week on 3 major social platforms)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Layered Search Engine Optimization</p>
						</div>
						<div class="price-option">
							<p>Keyword Audit and On-going Optimization</p>
						</div>
						<div class="price-option">
							<p>Multi Locational Blogging <br> <span class="color--lightgrey">(3-5 articles per week)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Locational Social Posting <br><span class="color--lightgrey">(3-5 posts/week)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Content Offer <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Monthly Analytics Review, Strategy and Goal Setting Meeting</p>
						</div>
						<div class="price-option">
							<p>Add-on Product Focused Marketing Campaign +$800/m</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 p4 open-options">
					<div class="price-title">
						<h2>Enterprise Plus</h2>
						<h1 class="price"><span>$</span>9K</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Dedicated Inbound Marketing Team</p>
						</div>
						<div class="price-option">
							<p>Corporate Inbound Strategy and Planning</p>
						</div>
						<div class="price-option">
							<p>Buyer Persona Research and Development</p>
						</div>
						<div class="price-option">
							<p>Corporate Content Marketing <br><span class="color--lightgrey">(Daily)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(Daily on 3 major social platforms)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Layered Search Engine Optimization</p>
						</div>
						<div class="price-option">
							<p>Keyword Audit and On-going Optimization</p>
						</div>
						<div class="price-option">
							<p>Multi Locational Blogging <br> <span class="color--lightgrey">(Daily)</span></p>
						</div>
						<div class="price-option">
							<p>Multi Locational Social Posting <br><span class="color--lightgrey">(Daily)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Corporate Content Offer <br><span class="color--lightgrey">(2 per quarter)</span></p>
						</div>
						<div class="price-option">
							<p>Monthly Analytics Review, Strategy and Goal Setting Meeting</p>
						</div>
						<div class="price-option">
							<p>Add-on Product Focused Marketing Campaign +$1,000/m</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>

				<div class="price-full">
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Dedicated Inbound Marketing Team</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Corporate Inbound Strategy and Planning</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Buyer Persona Research and Development</p>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Corporate Content Marketing <br><span class="color--lightgrey">(Daily)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2">
							<div class="price-option">
								<p>Corporate Content Marketing <br><span class="color--lightgrey">(2-3 per week)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3">
							<div class="price-option">
								<p>Corporate Content Marketing <br><span class="color--lightgrey">(4-5 per week)</span></p>
							</div>
						</div>
						<div class="col col-2 p4">
							<div class="price-option">
								<p>Corporate Content Marketing <br><span class="color--lightgrey">(Daily)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(Weekly on 3 major social platforms)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2">
							<div class="price-option">
								<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(2-3/week on 3 major social platforms)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3">
							<div class="price-option">
								<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(3-5/week on 3 major social platforms)</span></p>
							</div>
						</div>
						<div class="col col-2 p4">
							<div class="price-option">
								<p>Corporate Social Media Publishing and Promotion <br><span class="color--lightgrey">(Daily on 3 major social platforms)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Multi Layered Search Engine Optimization</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Keyword Audit and On-going Optimization</p>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Multi Locational Blogging <br><span class="color--lightgrey">(1 article per week)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2">
							<div class="price-option">
								<p>Multi Locational Blogging <br><span class="color--lightgrey">(2-3 articles per week)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3">
							<div class="price-option">
								<p>Multi Locational Blogging <br><span class="color--lightgrey">(3-5 articles per week)</span></p>
							</div>
						</div>
						<div class="col col-2 p4">
							<div class="price-option">
								<p>Multi Locational Blogging <br><span class="color--lightgrey">(Daily)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Multi Locational Social Posting <br><span class="color--lightgrey">(Weekly posts)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2">
							<div class="price-option">
								<p>Multi Locational Social Posting <br><span class="color--lightgrey">(2-3 posts/week)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3">
							<div class="price-option">
								<p>Multi Locational Social Posting <br><span class="color--lightgrey">(3-5 posts/week)</span></p>
							</div>
						</div>
						<div class="col col-2 p4">
							<div class="price-option">
								<p>Multi Locational Social Posting <br><span class="color--lightgrey">(Daily)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(1 per quarter)</span></p>
							</div>
						</div>
						<div class="col col-6 bg--lightblue">
							<div class="price-option">
								<p>Corporate CTA and Landing Page <br><span class="color--lightgrey">(2 per quarter)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Corporate Content Offer <br><span class="color--lightgrey">(1 per quarter)</span></p>
							</div>
						</div>
						<div class="col col-6 bg--lightblue">
							<div class="price-option">
								<p>Corporate Content Offer <br><span class="color--lightgrey">(2 per quarter)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Monthly Analytics Review, Strategy and Goal Setting Meeting</p>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<div class="price-option">
								<p>Add-on Product Focused Marketing Campaign +$400/m</p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2">
							<div class="price-option">
								<p>Add-on Product Focused Marketing Campaign +$600/m</p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3">
							<div class="price-option">
								<p>Add-on Product Focused Marketing Campaign +$800/m</p>
							</div>
						</div>
						<div class="col col-2 p4">
							<div class="price-option">
								<p>Add-on Product Focused Marketing Campaign +$1,000/m</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p2">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p3">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 p4">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
					</div>
				</div>

			</div>
			<!-- Hyperlocal Pricing-->
			<div class="row mt--100">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Hyperlocal</span> Add-ons <br>for Your Marketing Plans</h1>
					<p class="mt--50 text--center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>
			</div>
			<div class="row no-wrap animate">
				<div class="col col-2 border-right p1b open-options">
					<div class="price-title">
						<h2>FastTrack</h2>
						<h1 class="price"><span>$</span>200</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Designed for Smaller City <br> <span class="color--lightgrey">(&lt; 250k in Population)</span></p>
						</div>
						<div class="price-option">
							<p>+ $500 Setup</p>
						</div>
						<div class="price-option">
							<p>Foundation Setup</p>
						</div>
						<div class="price-option">
							<p>Local SEO <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>GEO Target Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>Local Social Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>ROI Tracking</p>
						</div>
						<div class="price-option">
							<p>Inbound Marketing Software</p>
						</div>
						<div class="price-option">
							<p>1 Target City</p>
						</div>
						<div class="price-option">
							<p>0 Surrounding Cities</p>
						</div>
						<div class="price-option">
							<p>Dedicated Account Manager</p>
						</div>
						<div class="price-option">
							<p>Month-to-Month, No Contracts</p>
						</div>
						<div class="price-option">
							<p>1st Page Guarantee</p>
						</div>
						<div class="price-option">
							<p>ROI Guarantee</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p2b open-options">
					<div class="price-title">
						<h2>AdvTrack</h2>
						<h1 class="price"><span>$</span>300</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Designed for Medium City <br> <span class="color--lightgrey">(250k - 750k in Population)</span></p>
						</div>
						<div class="price-option">
							<p>+ $500 Setup</p>
						</div>
						<div class="price-option">
							<p>Foundation Setup</p>
						</div>
						<div class="price-option">
							<p>Local SEO <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>GEO Target Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>Local Social Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>ROI Tracking</p>
						</div>
						<div class="price-option">
							<p>Inbound Marketing Software</p>
						</div>
						<div class="price-option">
							<p>1 Target City</p>
						</div>
						<div class="price-option">
							<p>0 Surrounding Cities</p>
						</div>
						<div class="price-option">
							<p>Dedicated Account Manager</p>
						</div>
						<div class="price-option">
							<p>Month-to-Month, No Contracts</p>
						</div>
						<div class="price-option">
							<p>1st Page Guarantee</p>
						</div>
						<div class="price-option">
							<p>ROI Guarantee</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p3b open-options">
					<div class="price-title">
						<h2>ProTrack</h2>
						<h1 class="price"><span>$</span>400</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Designed for Larger City <br> <span class="color--lightgrey">(750k+ in Population)</span></p>
						</div>
						<div class="price-option">
							<p>+ $500 Setup</p>
						</div>
						<div class="price-option">
							<p>Foundation Setup</p>
						</div>
						<div class="price-option">
							<p>Local SEO <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>GEO Target Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>Local Social Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>ROI Tracking</p>
						</div>
						<div class="price-option">
							<p>Inbound Marketing Software</p>
						</div>
						<div class="price-option">
							<p>1 Target City</p>
						</div>
						<div class="price-option">
							<p>0 Surrounding Cities</p>
						</div>
						<div class="price-option">
							<p>Dedicated Account Manager</p>
						</div>
						<div class="price-option">
							<p>Month-to-Month, No Contracts</p>
						</div>
						<div class="price-option">
							<p>1st Page Guarantee</p>
						</div>
						<div class="price-option">
							<p>ROI Guarantee</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>

				<div class="col col-2 p4b open-options">
					<div class="price-title">
						<h2>ProTrack Plus</h2>
						<h1 class="price"><span>$</span>500</h1>
						<p class="color--lightgrey">per month</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Designed for Larger City <br> <span class="color--lightgrey">(1m+ in Population)</span></p>
						</div>
						<div class="price-option">
							<p>+ $500 Setup</p>
						</div>
						<div class="price-option">
							<p>Foundation Setup</p>
						</div>
						<div class="price-option">
							<p>Local SEO <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>GEO Target Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>Local Social Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
						<div class="price-option">
							<p>ROI Tracking</p>
						</div>
						<div class="price-option">
							<p>Inbound Marketing Software</p>
						</div>
						<div class="price-option">
							<p>1 Target City</p>
						</div>
						<div class="price-option">
							<p>0 Surrounding Cities</p>
						</div>
						<div class="price-option">
							<p>Dedicated Account Manager</p>
						</div>
						<div class="price-option">
							<p>Month-to-Month, No Contracts</p>
						</div>
						<div class="price-option">
							<p>1st Page Guarantee</p>
						</div>
						<div class="price-option">
							<p>ROI Guarantee</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>

				<div class="price-full">
					<div class="col-8">
						<div class="col col-2 border-right-option p1b">
							<div class="price-option">
								<p>Designed for Smaller City <br><span class="color--lightgrey">(&lt; 250k in Population)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2b">
							<div class="price-option">
								<p>Designed for Medium City <br><span class="color--lightgrey">(250k - 750k in Population)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3b">
							<div class="price-option">
								<p>Designed for Larger City <br><span class="color--lightgrey">(750k+ in Population)</span></p>
							</div>
						</div>
						<div class="col col-2 p4b">
							<div class="price-option">
								<p>Designed for Larger City <br><span class="color--lightgrey">(1m+ in Population)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>+ $500 Setup</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Foundation Setup</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Local SEO <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>GEO Target Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Local Social Marketing <br><span class="color--lightgrey">(Monthly)</span></p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>ROI Tracking</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Inbound Marketing Software</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>1 Target City</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>0 Surrounding Cities</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Dedicated Account Manager</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Month-to-Month, No Contracts</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>1st Page Guarantee</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>ROI Guarantee</p>
						</div>
					</div>	
					<div class="col-8">
						<div class="col col-2 border-right-option p1b">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p2b">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p3b">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 p4b">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Onboarding pricing -->
	<div class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Onboarding</span> Plans</h1>
					<p class="mt--50 text--center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>
			</div>
			<div class="row border-wrap animate">
				<div class="col col-2 border-right p1c open-options">
					<div class="price-title">
						<h2>Professional</h2>
						<h1 class="price"><span>$</span>2.5K</h1>
						<p class="color--lightgrey">One-time payment</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Corporate Brand Development</p>
						</div>
						<div class="price-option">
							<p>Agent Website Creation</p>
						</div>
						<div class="price-option">
							<p>Product-Focused Responsive Website</p>
						</div>
						<div class="price-option">
							<p>Optimize existing Content for SEO</p>
						</div>
						<div class="price-option">
							<p>Multi-Layered On-Page Optimization</p>
						</div>
						<div class="price-option">
							<p>Blog Design and Development</p>
						</div>
						<div class="price-option">
							<p>Multi-Location Blogging Module Setup</p>
						</div>
						<div class="price-option">
							<p>SeedLogix Platform Training <br> <span class="color--lightgrey">(2 Hours)</span></p>
						</div>
						<div class="price-option">
							<p>User Experience Design and Development</p>
						</div>
						<div class="price-option">
							<p>Internal Theme or Existing Client Website Import</p>
						</div>
						<div class="price-option">
							<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>CRM Integration <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>Ag Reporting Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Sales Funnel Design / Development</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Dynamic Location Details Publishing</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Multi-Location Marketing Automation Design and Setup</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p2c open-options">
					<div class="price-title">
						<h2>Corporate</h2>
						<h1 class="price"><span>$</span>5K</h1>
						<p class="color--lightgrey">One-time payment</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Corporate Brand Development</p>
						</div>
						<div class="price-option">
							<p>Agent Website Creation</p>
						</div>
						<div class="price-option">
							<p>Product-Focused Responsive Website</p>
						</div>
						<div class="price-option">
							<p>Optimize existing Content for SEO</p>
						</div>
						<div class="price-option">
							<p>Multi-Layered On-Page Optimization</p>
						</div>
						<div class="price-option">
							<p>Blog Design and Development</p>
						</div>
						<div class="price-option">
							<p>Multi-Location Blogging Module Setup</p>
						</div>
						<div class="price-option">
							<p>SeedLogix Platform Training <br> <span class="color--lightgrey">(2 Hours)</span></p>
						</div>
						<div class="price-option">
							<p>User Experience Design and Development</p>
						</div>
						<div class="price-option">
							<p>Internal Theme or Existing Client Website Import</p>
						</div>
						<div class="price-option">
							<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>CRM Integration <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>Up to 5 Pages of Content Creation and Optimization</p>
						</div>
						<div class="price-option">
							<p>One Sales Funnel Design / Development</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Dynamic Location Details Publishing</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Multi-Location Marketing Automation Design and Setup</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 border-right p3c open-options">
					<div class="price-title">
						<h2>Enterprise</h2>
						<h1 class="price"><span>$</span>7.5K</h1>
						<p class="color--lightgrey">One-time payment</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Corporate Brand Development</p>
						</div>
						<div class="price-option">
							<p>Agent Website Creation</p>
						</div>
						<div class="price-option">
							<p>Product-Focused Responsive Website</p>
						</div>
						<div class="price-option">
							<p>Optimize existing Content for SEO</p>
						</div>
						<div class="price-option">
							<p>Multi-Layered On-Page Optimization</p>
						</div>
						<div class="price-option">
							<p>Blog Design and Development</p>
						</div>
						<div class="price-option">
							<p>Multi-Location Blogging Module Setup</p>
						</div>
						<div class="price-option">
							<p>SeedLogix Platform Training <br> <span class="color--lightgrey">(2 Hours)</span></p>
						</div>
						<div class="price-option">
							<p>User Experience Design and Development</p>
						</div>
						<div class="price-option">
							<p>Fully Customized Website, Design and Development</p>
						</div>
						<div class="price-option">
							<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>CRM Integration <br><span class="color--lightgrey">(Available as add-on)</span></p>
						</div>
						<div class="price-option">
							<p>Up to 7 Pages of Content Creation and Optimization</p>
						</div>
						<div class="price-option">
							<p>Two Sales Funnel Design / Development</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Dynamic Location Details Publishing</p>
						</div>
						<div class="price-option">
							<p class="text--crossed color--lightgrey">Multi-Location Marketing Automation Design and Setup</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>
				<div class="col col-2 p4c open-options">
					<div class="price-title">
						<h2>Enterprise Plus</h2>
						<h1 class="price"><span>$</span>10K</h1>
						<p class="color--lightgrey">One-time payment</p>
						<p class="view--detail"><a class="mt--20 link--mono" href="javascript:void(0)">&gt; View Details</a></p>
					</div>
					<div class="options-display">
						<div class="price-option">
							<p>Corporate Brand Development</p>
						</div>
						<div class="price-option">
							<p>Agent Website Creation</p>
						</div>
						<div class="price-option">
							<p>Product-Focused Responsive Website</p>
						</div>
						<div class="price-option">
							<p>Optimize existing Content for SEO</p>
						</div>
						<div class="price-option">
							<p>Multi-Layered On-Page Optimization</p>
						</div>
						<div class="price-option">
							<p>Blog Design and Development</p>
						</div>
						<div class="price-option">
							<p>Multi-Location Blogging Module Setup</p>
						</div>
						<div class="price-option">
							<p>SeedLogix Platform Training <br> <span class="color--lightgrey">(2 Hours)</span></p>
						</div>
						<div class="price-option">
							<p>User Experience Design and Development</p>
						</div>
						<div class="price-option">
							<p>Fully Customized Website, Design and Development</p>
						</div>
						<div class="price-option">
							<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Included)</span></p>
						</div>
						<div class="price-option">
							<p>CRM Integration <br><span class="color--lightgrey">(Included)</span></p>
						</div>
						<div class="price-option">
							<p>Up to 10 Pages of Content Creation and Optimization</p>
						</div>
						<div class="price-option">
							<p>Two Sales Funnel Design / Development</p>
						</div>
						<div class="price-option">
							<p>Dynamic Location Details Publishing</p>
						</div>
						<div class="price-option">
							<p>Multi-Location Marketing Automation Design and Setup</p>
						</div>
						<a href="/free-trial/"><div class="btn full">
							Start Free Trial
						</div></a>
					</div>
				</div>

				<div class="price-full">
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Corporate Brand Development</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Agent Website Creation</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Product-Focused Responsive Website</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Optimize existing Content for SEO</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Multi-Layered On-Page Optimization</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Blog Design and Development</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>Multi-Location Blogging Module Setup</p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>SeedLogix Platform Training <br><span class="color--lightgrey">(2 Hours)</span></p>
						</div>
					</div>
					<div class="col-8 bg--lightblue">
						<div class="price-option">
							<p>User Experience Design and Development</p>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-4 border-right-option p1c p2c">
							<div class="price-option">
								<p>Internal Theme or Existing Client Website Import</p>
							</div>
						</div>
						<div class="col col-4 p3c p4c">
							<div class="price-option">
								<p>Fully Customized Website, Design and Development</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-6 border-right-option bg--lightblue">
							<div class="price-option">
								<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
							</div>
						</div>
						<div class="col col-2 p4c">
							<div class="price-option">
								<p>Automatic Onboarding Setup <br><span class="color--lightgrey">(Included)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-6 border-right-option bg--lightblue">
							<div class="price-option">
								<p>CRM Integration <br><span class="color--lightgrey">(Available as add-on)</span></p>
							</div>
						</div>
						<div class="col col-2 p4c">
							<div class="price-option">
								<p>CRM Integration <br><span class="color--lightgrey">(Included)</span></p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1c">
							<div class="price-option">
								<p>Ag Reporting Setup <br><span class="color--lightgrey">(Available as add-on)</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2c">
							<div class="price-option">
								<p>Up to 5 Pages of Content Creation and Optimization</p>
							</div>
						</div>
						<div class="col col-2 border-right-option p3c">
							<div class="price-option">
								<p>Up to 7 Pages of Content Creation and Optimization</p>
							</div>
						</div>
						<div class="col col-2 p4c">
							<div class="price-option">
								<p>Up to 10 Pages of Content Creation and Optimization</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1c">
							<div class="price-option">
								<p><span class="color--lightgrey">×</span></p>
							</div>
						</div>
						<div class="col col-2 border-right-option p2c">
							<div class="price-option">
								<p>One Sales Funnel Design / Development</p>
							</div>
						</div>
						<div class="col col-4 p3c p4c">
							<div class="price-option">
								<p>Two Sales Funnel Design / Development</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-6 border-right-option bg--lightblue">
							<div class="price-option">
								<p><span class="color--lightgrey">×</span></p>
							</div>
						</div>
						<div class="col col-2 p4c">
							<div class="price-option">
								<p>Dynamic Location Details Publishing</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-6 border-right-option bg--lightblue">
							<div class="price-option">
								<p><span class="color--lightgrey">×</span></p>
							</div>
						</div>
						<div class="col col-2 p4c">
							<div class="price-option">
								<p>Multi-Location Marketing Automation Design and Setup</p>
							</div>
						</div>
					</div>
					<div class="col-8">
						<div class="col col-2 border-right-option p1c">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p2c">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 border-right-option p3c">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
						<div class="col col-2 p4c">
							<a href="/free-trial/"><div class="btn full">
							Start Free Trial
							</div></a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- DIY -->
	<div class="bg--dark button-center" style="background: url('/hubs/themes/seedlogix//hubs/themes/seedlogix/images/solutions-delivery-dark.jpg') center center/1440px auto no-repeat;">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Already have a Marketing Team?</span></h1>
					<p class="mt--50 text--center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="col-3 animate">
						<a href="/pricing-software/"><div class="btn">
							Sign Up Software Only
						</div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Testimonials & clients-->
	<div class="scroll--down"></div>
	<div class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center">What Do <span>Our Clients</span> Say?</h1>
					<p class="mt--50 text--center">We are always looking to optimize our customers' experience with our platform and service solutions. With that in mind, we listen to each one of our partners.</p>
				</div>
			</div>
			<div class="testimonial boxed row mt--80 ptb--100 animate">
				<div class="col col-4">
					<h2>"I searched all over and looked at many different company websites to decide which one to use and SeedLogix was by far the best. The designs are amazing."</h2>
					<p class="mt--20 color--primary"><b>George Farley</b></p>
					<p class="color--lightgrey">CMO – Observint Technologies</p>
				</div>
				<div class="col col-4">
					<h2>"Having worked with Reva for a number of years now. They are constantly moving with the times and taking us with them, You can only admire that."</h2>
					<p class="mt--20 color--primary"><b>Robert Moore</b></p>
					<p class="color--lightgrey">Senior VP IT – WVMB</p>
				</div>
			</div>
			<div class="row mt--80 animate">
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo1.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo2.gif">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo3.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo4.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo5.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo6.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo7.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo8.jpg">
				</div>
			</div>
		</div>
	</div>