
<!-- CTA -->
	<div class="cta">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center animate">Bring the power of <b>SeedLogix</b> to your company or agency.</h1>
					<div class="col-3 animate">
						<div class="btn full">
							Start 14 Day Free Trial
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Footer -->
	<div class="footer">
		<div class="wrapper">
			<div class="col col-6 center">
				<div class="footer-dir">
					<div class="col col-7">
						<div class="logo-x"></div>
						<div class="f-arrow"></div>
						<p>Welcome</p>
					</div>
					<div class="col col-1">
						<div class="go-top">
							<a href="javascript:void(0)" id="back-top"><div class="arrow-up"></div></a>
						</div>
					</div>
				</div>
				<div class="footer-subnav">
					<div class="col col-2">
						<p class="f-title">Get Connected</p>
						<div class="row">
							<a href="http://facebook.com/seedlogix" target="_blank"><div class="f-icon fb"></div></a>
							<a href="http://twitter.com/seedlogix" target="_blank"><div class="f-icon tw"></div></a>
							<a href="https://www.linkedin.com/company/seedlogix" target="_blank"><div class="f-icon lk"></div></a>
							<a href="https://www.instagram.com/seedlogix/" target="_blank"><div class="f-icon in"></div></a>
							<a href="https://plus.google.com/+SeedlogixIMS" target="_blank"><div class="f-icon gp"></div></a>
						</div>
						<p class="f-mono"><a href="javascript:void(0)">> Privacy Policy</a></p>
						<p class="f-mono"><a href="javascript:void(0)">> Terms of Service</a></p>
					</div>
					<div class="col col-2">
						<p class="f-title">Address</p>
						<p>12007 Research Boulevard Ste 201<br>Austin, TX 78759</p>
						<p class="f-title">Phone</p>
						<p>+1 877 467-7091</p>
					</div>
					<div class="col col-2">
						<p class="f-title">Software</p>
						<p class="f-mono"><a href="/products/responsive-sites">> Responsive Sites</a></p>
						<p class="f-mono"><a href="/products/content-blogging">> Content Blogging</a></p>
						<p class="f-mono"><a href="/products/landing-pages">> Landing Pages</a></p>
						<p class="f-mono"><a href="/products/lead-generation-forms">> Lead Generation Forms</a></p>
						<p class="f-mono"><a href="/products/email-marketing">> Email Marketing</a></p>
						<p class="f-mono"><a href="/products/social-media-scheduling">> Social Media Scheduling</a></p>
					</div>
					<div class="col col-2">
						<p class="f-title">&nbsp;</p>
						<p class="f-mono"><a href="/products/lead-nurturing">> Lead Nurturing</a></p>
						<p class="f-mono"><a href="/products/lead-manager">> Lead Manager</a></p>
						<p class="f-mono"><a href="/products/contact-manager">> Contact Manager</a></p>
						<p class="f-mono"><a href="/products/aggregated-web-reporting">> Aggregated Web Reporting</a></p>
						<p class="f-mono"><a href="/products/serp-rankings">> SERP Rankings</a></p>
						<p class="f-mono"><a href="/products/call-tracking-and-recording">> Call Tracking &amp; Recording</a></p>
					</div>
				</div>
				<div class="footer-copy">
					<p>2018 &copy; SeedLogix. All rights reserved.</p>
				</div>
			</div>
		</div>
	</div>