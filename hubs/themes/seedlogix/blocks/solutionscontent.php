<!-- Hero -->
	<div id="hero" class="hero-title-solutions">
		<div class="hero-title">
			<div class="wrapper">
				<div class="col col-8">
					<h1 class="type--animate" data-period="5000" data-type='[ "For Multi-Location Businesses" ]'>
						<span class="wrap"></span>
					</h1>
				</div>
			</div>
		</div>
		<div class="hero-footer-solutions animate">
			<div class="wrapper">
				<div class="row-center">
					<div class="col col-4 text--center">
						<p class="color--white">With almost a decade of experience in multi-locational marketing and technology, we are the only choice for your franchise, manufacturer, or agent based business</p>
					</div>
				</div>
				<div class="row-center mt--50">
					<div class="col col-2">
						<div class="btn full">
							Free Consultation
						</div>
					</div>
					<div class="col col-2">
						<div class="btn white">
							Take Tour
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="hero-shadow"></div>
	</div>
<!-- Solutions -->
	<div class="pt--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Full-Service Agency</span> <br> Expert Help from Design To Execution</h1>
					<p class="mt--50 text--center"></p>
				</div>
			</div>
			<div class="row2-center animate">
				<div class="col col-2">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/solution-designdev.png"></div>
					<div>
						<h2>Design &amp; <br> Development</h2>
						<p class="color--lightgrey mt--20">Our team understands the power in 1-to-1 personal connections and it starts with the design.</p>
					</div>
				</div>
				<div class="col col-2">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/solution-deployment.png"></div>
					<div>
						<h2>Footprint <br> Deployment</h2>
						<p class="color--lightgrey mt--20">We have the technology &amp; experience for a successful multi locational deployment online.</p>
					</div>
				</div>
				<div class="col col-2">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/solution-inbound.png"></div>
					<div>
						<h2>Inbound Marketing Execution</h2>
						<p class="color--lightgrey mt--20">Executing an inbound marketing strategy for 1000s of location needs a different kind of Agency.</p>
					</div>
				</div>
			</div>
			<div class="row-center mt--100">
				<div class="col col-6 img-bottom">
					<img src="/hubs/themes/seedlogix/images/solutions-full.png">
				</div>
			</div>
		</div>
	</div>
	<div class="scroll--down"></div>
	<!-- Services -->
	<div class="bg--light cards">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center">Platform Designed for <span>Multi-Location</span></h1>
					<p class="mt--50 text--center">Our proprietary technology provides highly scalable and efficient functionality developed for inbound marketing strategies across multiple channels.</p>
				</div>
			</div>
			<div class="row mt--80">
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/responsive.svg">
						</div>
						<h2>Multi-Site CMS</h2>
						<p>Control 1000s of hyper-localized or personal websites from a single base system.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/content.svg">
						</div>
						<h2>Multi-Feed Blogging</h2>
						<p>Syndicate high quality content across locations, branches and agent blogs.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/landing.svg">
						</div>
						<h2>Multi-Site Lead Forms</h2>
						<p>Powerful and smart forms to manage leads throughout all local channels.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/lead-generation.svg">
						</div>
						<h2>Dynamic Tagging</h2>
						<p>Innovative tag system to help personalize and create uniqueness across your network.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/email-marketing.svg">
						</div>
						<h2>Collateral Branding</h2>
						<p>Instantly brand marketing collateral with personalized contact info and content.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/social-scheduling.svg">
						</div>
						<h2>Marketing Automation</h2>
						<p>Automate follow-up and communication with prospects on a location-by-location basis.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/lead-management.svg">
						</div>
						<h2>Social Scheduling</h2>
						<p>Push social media content out to unlimited amount of profiles instantly.</p>
					</div>
				</div>
				<div class="col col-2 pb--20 animate">
					<div class="card">
						<div class="card-icon">
							<img src="/hubs/themes/seedlogix/images/reporting.svg">
						</div>
						<h2>Aggregated Web Analytics</h2>
						<p>Understand how, when and what your customers are visiting through your entire network.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Delivery -->
	<div class="bg--dark title-center" style="background: url('/hubs/themes/seedlogix/images/solutions-delivery-dark.jpg') center center/1440px auto no-repeat;">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>SeedLogix</span> Delivery Platform</h1>
					<p class="mt--50 text--center">Our delivery platform is an end-to-end, closed-loop solution designed specifically for multi-location businesses. We integrate design, technology, management and inbound marketing to deliver premium results for your business.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scroll--down"></div>
	<!-- Platform -->
	<div class="platform ptb--100">
		<div class="wrapper">
			<div class="row animate">
				<div class="col col-2">
					<div class="mt--50">
						<h2 class="feat">User Experience Design/Development</h2>
						<p class="color--lightgrey">Award winning design team to develop the perfect local buyer's journey.</p>
					</div>
					<div class="mt--80">
						<h2 class="feat">Content Strategy &amp; Copywriting Services</h2>
						<p class="color--lightgrey">We create compelling content that will convert your visitors to local customers.</p>
					</div>
					<div class="mt--80">
						<h2 class="feat">Multi-Location Focused SEO</h2>
						<p class="color--lightgrey">Execute strategic SEO campaigns for corporate, location, branch and agent footprints.</p>
					</div>
				</div>
				<div class="col col-4">
					<div class="software-img"><img src="/hubs/themes/seedlogix/images/delivery-program.png"></div>
				</div>
				<div class="col col-2">
					<div class="mt--50">
						<h2 class="feat">Multi-Locational Inbound Marketing</h2>
						<p class="color--lightgrey">Create and implement inbound marketing strategies for your entire network individually.</p>
					</div>
					<div class="mt--80">
						<h2 class="feat">GEO-Based Social Media Marketing</h2>
						<p class="color--lightgrey">Grow and build authority across all your local channels throughout social media.</p>
					</div>
					<div class="mt--80">
						<h2 class="feat">In-Depth Aggregated Analytics</h2>
						<p class="color--lightgrey">Provide aggregated organic, paid, and social analytics with powerful local filters.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Solutions Categories -->
	<div class="sol-cat bg--light">
		<div class="wrapper">
			<div class="row mb--80 animate">
				<div class="col col-4 center">
					<h1 class="text--center">We Love <span>Multi-Location</span> Businesses!</h1>
					<p class="mt--50 text--center">For the past decade our team has focused on delivering unique technology
and aggressive inbound marketing to ensure successful growth across all channels.</p>
				</div>
			</div>
			<div class="row mb--80 animate">
				<div class="col col-2">
					<div class="solutions-img"><img src="/hubs/themes/seedlogix/images/franchise-solutions.png"></div>
					<div>
						<h2 class="feat text--center">Franchise <br> Solutions</h2>
						<p class="color--lightgrey">We have a laser focused approach to inbound marketing campaigns for franchises. Check out a typical campaign when Franchise organizations hire us.</p>
						<a class="mt--20 link--mono" href="/solutions/franchise/">> Learn more</a>
					</div>
				</div>
				<div class="col col-2">
					<div class="solutions-img"><img src="/hubs/themes/seedlogix/images/manufacture-solutions.png"></div>
					<div>
						<h2 class="feat text--center">Manufacturers <br> Solutions</h2>
						<p class="color--lightgrey">How do you gain loyalty, protect your brand and drive incremental revenue from your dealers? We have a proven model and solution that does all three.</p>
						<a class="mt--20 link--mono" href="/solutions/manufacturers/">> Learn more</a>
					</div>
				</div>
				<div class="col col-2">
					<div class="solutions-img"><img src="/hubs/themes/seedlogix/images/agent-solutions.png"></div>
					<div>
						<h2 class="feat text--center">Solutions for Agent <br> Organizations</h2>
						<p class="color--lightgrey">Learn how we setup a success marketing blueprint that includes inbound marketing for corporate, branch and agent online footprints.</p>
						<a class="mt--20 link--mono" href="/solutions/agent-organizations/">> Learn more</a>
					</div>
				</div>
				<div class="col col-2">
					<div class="solutions-img"><img src="/hubs/themes/seedlogix/images/lead-solutions.png"></div>
					<div>
						<h2 class="feat text--center">Lead Generation <br> Solutions</h2>
						<p class="color--lightgrey">We are experts in SeedLogix's multi-location technology. Our team can set up a unique platform for your lead generation business.</p>
						<a class="mt--20 link--mono" href="/solutions/lead-generation/">> Learn more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Testimonials & clients-->
	<div class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center">What Do <span>Our Clients</span> Say?</h1>
					<p class="mt--50 text--center">We are always looking to optimize our customers' experience with our platform and service solutions. With that in mind, we listen to each one of our partners.</p>
				</div>
			</div>
			<div class="testimonial boxed row mt--80 ptb--100 animate">
				<div class="col col-4">
					<h2>"I searched all over and looked at many different company websites to decide which one to use and SeedLogix was by far the best. The designs are amazing."</h2>
					<p class="mt--20 color--primary"><b>George Farley</b></p>
					<p class="color--lightgrey">CMO – Observint Technologies</p>
				</div>
				<div class="col col-4">
					<h2>"Having worked with Reva for a number of years now. They are constantly moving with the times and taking us with them, You can only admire that."</h2>
					<p class="mt--20 color--primary"><b>Robert Moore</b></p>
					<p class="color--lightgrey">Senior VP IT – WVMB</p>
				</div>
			</div>
			<div class="row mt--80 animate">
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo1.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo2.gif">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo3.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo4.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo5.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo6.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo7.jpg">
				</div>
				<div class="col col-2 logos">
					<img src="http://www.seedlogix.com/users/u59/59772/logo8.jpg">
				</div>
			</div>
		</div>
	</div>