<!-- Hero -->
	<div id="hero" class="hero-title-only">
		<div class="hero-title">
			<div class="wrapper">
				<div class="col col-8">
					<h1 class="type--animate" data-period="5000" data-type='[ "Responsive Sites" ]'>
						<span class="wrap"></span>
					</h1>
				</div>
			</div>
		</div>
		<div class="hero-main-icon">
			<img src="/hubs/themes/seedlogix/images/responsive.svg">
		</div>
		<div class="hero-shadow"></div>
	</div>
	<!-- Feature content -->
	<div class="ptb--100">
		<div class="wrapper">
			<div class="row">
				<div class="col col-4 center">
					<h1 class="text--center"><span>Multi Site CMS</span> <br> Powerful. Simple. Responsive.</h1>
					<p class="mt--50 text--center">SeedLogix makes content management simple and easy whether you have 1 website or 1000 websites. Publish changes and content across all your touchpoints with ease all while automatically adjusting for mobile and tablet users through our responsive framework.</p>
				</div>
			</div>
			<div class="boxed row2 mt--80 pb--60">
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Drag &amp; Drop Responsive Content Management System</h2>
						<p>Our responsive content management system is optimized no matter what browser or device your prospects find you on, providing extremely fast load times and higher conversions. Whether you choose from one of our base themes or decide to have your website imported into our system, we ensure your site looks fantastic from any device. <br>
						&nbsp; <br>
    					• Simply drag &amp; drop from many responsive content blocks to build high quality websites in minutes or import your own. <br>
    					• Create pages with ease optimized not only for SEO, but for all browsers and devices. <br>
    					• Create multiple versions of your website design with the click of a button. <br>
    					• Our Responsive themes ensure fast load times, rankings and better content placement. <br>
    					• Advanced CSS and code editing allowing designers and developers to create virtually any looking design.</p>
					</div>
				</div>
				<div class="col col-4 img-full animate">
					<img src="/hubs/themes/seedlogix/images/software-img.jpg">
				</div>
			</div>
			<div class="row2 mt--80">
				<div class="col col-4 img-full animate">
					<img src="/hubs/themes/seedlogix/images/software-img.jpg">
				</div>
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Multi Site Management</h2>
						<p>Our powerful Multi Site CMS allows users to deploy and manage thousands of websites. These websites could be for any business or agency that has a need to manage multiple sites' lead generation from a single platform. <br>
						&nbsp;<br>
						Our platform has the ability to instantly add pages, change content, update optimization, or conduct virtually any change to your master website. Our multi site CMS will instantly update content across all of your websites. Each website is supplied with its own URL of choice so you can ensure each website has the maximum potential for search engine optimization. <br>
						&nbsp;<br>
    					• Create unlimited multi site based website systems. <br>
    					• Launch independent and multiple versions of your base website. <br>
    					• Lock and unlock content for users with our innovative easy editor. <br>
    					• Create custom optimization tags for unique content across all your websites. <br>
    					• Update all child websites with the click of a button.</p>
					</div>
				</div>
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Multi Website Updating</h2>
						<p>Imagine having 500 locational websites all running on their own unique URL and you decide to add a product or service to all of them. Traditionally you must go into each website and update each version. Our multi site cms allows users to make changes or additions that update instantly across all domains allowing users to make global changes instantly.</p>
					</div>
				</div>
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Multi-Site Form Builder</h2>
						<p>Our multi-site form builder was designed specifically for split testing of multiple landing pages and microsites. Users can publish the same form on multiple websites and update anything on the form, which then updates instantly across all websites. This allows users to quickly make changes based on visitors response rates.</p>
					</div>
				</div>
			</div>
			<div class="boxed row2 mt--80 pb--60">
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Content Delivery Network</h2>
						<p>SeedLogix is built on the most reliable and scalable enterprise cloud environment available. We take load times, uptime, and security very serious. We have put in place the most advanced layers to ensure our users never have to worry about downtime, security threats, software upgrades, plugin patches, and everything in between. <br>
						&nbsp; <br>
						We have extended the very same environment our application sits on to every touchpoint built by our customers. Every website is hosted on our content delivery network and is distributed to the closest geographical node to your business. This shortens the distance of travel to your prospects. We also treat each website as its own individual site in our system, making them very fast, regardless of how many you have. <br>
						&nbsp; <br>
    					• Content is served from 24 geographically diverse nodes to shorten time of delivery. <br>
    					• Enterprise level cloud hosting infrastructure. <br>
    					• Each website is treated as a single entity, allowing for the faster load times.</p>
					</div>
				</div>
				<div class="col col-4 img-full animate">
					<img src="/hubs/themes/seedlogix/images/software-img.jpg">
				</div>
			</div>
			<div class="row2 mt--80">
				<div class="col col-4 img-full animate">
					<img src="/hubs/themes/seedlogix/images/software-img.jpg">
				</div>
				<div class="col col-4 animate">
					<div class="feature-text">
						<h2>Personalized Content Publishing</h2>
						<p>Our Personalized Content Publishing system allows users to personalize their website content based on geographic location, industry, prospect interest, or other custom elements. Users can change the content of their website for prospects who are already leads, personalizing the content with the data we already know about them or changing the layout completely for those leads. Users can also change the content of their website to be optimized for a specific geographic location with our easy-to-use content tagging system. <br>
						&nbsp; <br>
						By providing personalized content throughout your website to match the level of interest of the visitor increases your chances of converting new customers or growing existing customers for new or upsell products. We have simplified the process with global tags that can be used on any touchpoint created in our system, including our multi site platform.</p>
					</div>
				</div>
			</div>
		</div>
	</div>