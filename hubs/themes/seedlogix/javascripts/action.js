    
    jQuery(function() { jQuery( "#header" ).parent().css( "position", "static" ); });

    jQuery('img, video').bind('contextmenu', function(e) { return false; });

    jQuery('img, video').on('dragstart', function(event) { event.preventDefault(); });

    window.onload = function() {
        var TxtType = function(el, toRotate, period, view) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 10000;
            this.view = view;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
        };

        TxtType.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];

            if (this.isDeleting) {
                this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
                this.txt = fullTxt.substring(0, this.txt.length + 1);
            }

            this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

            var that = this;
            var delta = 200 - Math.random() * 100;

            if (this.isDeleting) { delta /= 2; }

            if (!this.isDeleting && this.txt === fullTxt) {
                delta = this.period;
                if (this.view != 'blog') {
                    this.isDeleting = true;
                }
            } else if (this.isDeleting && this.txt === '') {
                this.isDeleting = false;
                this.loopNum++;
                delta = 500;
            }

            setTimeout(function() {
                that.tick();
            }, delta);
        };

        var elements = document.getElementsByClassName('type--animate');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            var view = elements[i].getAttribute('data-view');
            if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period, view);
            }
        }

        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".type--animate > .wrap { border-right: 1px solid #a7b6bf; animation: caret 1s steps(1) infinite;}";
        document.body.appendChild(css);

        // to top right away
        if ( window.location.hash ) scroll(0,0);
        // void some browsers issue
        setTimeout( function() { scroll(0,0); }, 1);

        // any position
        jQuery(function() {
            // your current click function
            jQuery('.scroll').on('click', function(e) {
                //e.preventDefault();
                var idhash = jQuery(this).attr('href').split('#').pop();
                jQuery('html, body').animate({
                    scrollTop: jQuery('#' + idhash).offset().top + 'px'
                }, 1000, 'swing');
            });
            // *only* if we have anchor on the url
            if(window.location.hash) {
                // smooth scroll to the anchor id
                jQuery('html, body').animate({
                    scrollTop: jQuery(window.location.hash).offset().top + 'px'
                }, 1000, 'swing');
            }
        });

        jQuery(function() {
           var c = .95 * jQuery(window).height();
           jQuery(".animate").waypoint(function() {
             jQuery(this).addClass("is-animated")
         }, {
             offset: c
         });
       });

        var card = $('.card');
        var cardwidth = card.outerWidth();
        card.css('min-height', cardwidth*1.1);
        var circle = $('.feat-img');
        var circlewidth = circle.outerWidth();
        circle.css('height', circlewidth);
    };

    window.onresize = function(event) {
        var card = $('.card');
        var cardwidth = card.outerWidth();
        card.css('min-height', cardwidth*1.1);
        var circle = $('.feat-img');
        var circlewidth = circle.outerWidth();
        circle.css('height', circlewidth);

        if (jQuery(window).width() > 768) {
            jQuery('.options-display, .view--detail').hide();
            jQuery('.open-options').removeClass('bg--lightblue');
        }
    };

    // scroll body to 0px on click
    jQuery('a#back-top').click(function () {
        jQuery('body,html').animate({
            scrollTop: 0
        }, 800);
    });

    jQuery('a#back-top').click(function () {
        jQuery('body,html').animate({
            scrollTop: 0
        }, 800);
    });

    jQuery('.hamburger').click(function () {
        jQuery('.hamburger').toggleClass( "is-active" );
        jQuery('ul.nav').toggleClass( "open" );
    });

    jQuery('.p1').hover(function(){
        jQuery('.p1').not(this).css({'background':'#eff9fe'});
        jQuery('.p1 > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p1').not(this).removeAttr('style');
        jQuery('.p1 > .btn').removeAttr('style');
    });

    jQuery('.p2').hover(function(){
        jQuery('.p2').not(this).css({'background':'#eff9fe'});
        jQuery('.p2 > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p2').not(this).removeAttr('style');
        jQuery('.p2 > .btn').removeAttr('style');
    });

    jQuery('.p3').hover(function(){
        jQuery('.p3').not(this).css({'background':'#eff9fe'});
        jQuery('.p3 > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p3').not(this).removeAttr('style');
        jQuery('.p3 > .btn').removeAttr('style');
    });

    jQuery('.p4').hover(function(){
        jQuery('.p4').not(this).css({'background':'#eff9fe'});
        jQuery('.p4 > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p4').not(this).removeAttr('style');
        jQuery('.p4 > .btn').removeAttr('style');
    });

    jQuery('.p1b').hover(function(){
        jQuery('.p1b').not(this).css({'background':'#eff9fe'});
        jQuery('.p1b > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p1b').not(this).removeAttr('style');
        jQuery('.p1b > .btn').removeAttr('style');
    });

    jQuery('.p2b').hover(function(){
        jQuery('.p2b').not(this).css({'background':'#eff9fe'});
        jQuery('.p2b > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p2b').not(this).removeAttr('style');
        jQuery('.p2b > .btn').removeAttr('style');
    });

    jQuery('.p3b').hover(function(){
        jQuery('.p3b').not(this).css({'background':'#eff9fe'});
        jQuery('.p3b > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p3b').not(this).removeAttr('style');
        jQuery('.p3b > .btn').removeAttr('style');
    });

    jQuery('.p4b').hover(function(){
        jQuery('.p4b').not(this).css({'background':'#eff9fe'});
        jQuery('.p4b > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p4b').not(this).removeAttr('style');
        jQuery('.p4b > .btn').removeAttr('style');
    });

    jQuery('.p1c').hover(function(){
        jQuery('.p1c').not(this).css({'background':'#eff9fe'});
        jQuery('.p1c > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p1c').not(this).removeAttr('style');
        jQuery('.p1c > .btn').removeAttr('style');
    });

    jQuery('.p2c').hover(function(){
        jQuery('.p2c').not(this).css({'background':'#eff9fe'});
        jQuery('.p2c > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p2c').not(this).removeAttr('style');
        jQuery('.p2c > .btn').removeAttr('style');
    });

    jQuery('.p3c').hover(function(){
        jQuery('.p3c').not(this).css({'background':'#eff9fe'});
        jQuery('.p3c > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p3c').not(this).removeAttr('style');
        jQuery('.p3c > .btn').removeAttr('style');
    });

    jQuery('.p4c').hover(function(){
        jQuery('.p4c').not(this).css({'background':'#eff9fe'});
        jQuery('.p4c > .btn').css({'opacity': 1});
    },function(){
        jQuery('.p4c').not(this).removeAttr('style');
        jQuery('.p4c > .btn').removeAttr('style');
    });

    jQuery('.open-options').click(function() {
        jQuery('.options-display').hide();
        jQuery('.options-display', this).show();
        jQuery('.view--detail').show();
        jQuery('.view--detail', this).hide();
        jQuery('.open-options').removeClass('bg--lightblue').removeClass('open');
        jQuery(this).addClass('bg--lightblue').addClass('open');
        jQuery('html, body').animate({
            scrollTop: jQuery(this).offset().top + 'px'
        }, 1000, 'swing');
    });

 
    (function(jQuery){
        stickynav_fn = function(){
            if(window.matchMedia('(min-width: 768px)').matches) {
                // Hide Header on on scroll down
                var didScroll;
                var lastScrollTop = 0;
                var delta = 5;
                var navbarHeight = jQuery('#hero').outerHeight()*.75;
                function hasScrolled() {
                    var st = jQuery(this).scrollTop();
                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                    return;
                    // If they scrolled down and are past the navbar, add class .nav-up.
                     // This is necessary so you never see what is "behind" the navbar.
                    if (st > lastScrollTop && st > navbarHeight){
                        // Scroll Down
                        jQuery('#header').removeClass('nav-down').addClass('nav-up');
                    } else {
                        // Scroll Up
                        if(st + jQuery(window).height() < jQuery(document).height()) {
                            jQuery('#header').removeClass('nav-up').addClass('nav-down');
                        }
                    }
                    lastScrollTop = st;
                }
                jQuery(window).scroll(function(){
                    if(jQuery(document).scrollTop() > navbarHeight)
                    {
                        if (jQuery(window).width() > 768) {
                            jQuery('#header').removeClass('header-wrapper').addClass('fixed-wrapper');
                        } else {
                            jQuery('#header').removeClass('fixed-wrapper').addClass('header-wrapper');
                        }
                    } else {
                        jQuery('#header').removeClass('fixed-wrapper').addClass('header-wrapper');
                    }
                    jQuery(window).scroll(function(event){
                        didScroll = true;
                    });
                    setInterval(function() {
                        if (didScroll) {
                            hasScrolled();
                            didScroll = false;
                        }
                    }, 250);
                });
            } else if(window.matchMedia('(max-width: 767px)').matches) {
                jQuery('#header').removeClass('fixed-wrapper').addClass('header-wrapper');
            }
        };
        jQuery(window).resize(stickynav_fn).resize();
    })(jQuery);
 