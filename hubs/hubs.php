<?php
	session_start();
        require_once(dirname(__FILE__) . '/../admin/6qube/core.php');
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$hub = new Hub();
	
	if(!$hub_id){
		$script_path = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
		$InnermostDir = basename(rtrim($script_path, '/'));
		$hub_id = $InnermostDir;
	}
	if(!is_numeric($hub_id)) $hub_id = 0;
	if(!$hub_id) echo '';
        
	$query = "SELECT path FROM themes WHERE id = (SELECT theme FROM hub WHERE id = '".$hub_id."') ";
	$row = $hub->queryFetch($query);
	$themeName = $row['path'];
	$themepath = QUBEROOT . 'hubs/themes/'.$themeName.'/';
	
	if(!$_SESSION['seKeyword']){
		//pulls search engine source and keyword
		require_once(QUBEADMIN . 'inc/searchkeys.class.php');
		$keys = search_keywords::getInstance();
		$keys = $keys->get_keys();
		if (count($keys))
		{
		$_SESSION['referringURL'] = $keys[0];
		$_SESSION['seKeyword'] = $keys[1];
		$_SESSION['searchEngine'] = $keys[2];
		}
	}
        
        
        // amado 05022012 @amado @see reseller_site.php
        // this code needs to be refactored.
        // Will use a switch statement and functions for now until
        // I can efficiently convert the code into OOP model.
        // 
        
        require_once QUBEPATH . 'temporary-optout-processor.php';
        // end amado 05022012
	
        #this request processor needs to be rewritten to be more dynamic
        #I'm afraid to' do it right now because it could possibly break absolutely everything
        # so I'm gonna add a quick hack for search pagination
        
        
	//If user is trying to pull up a page (page argument is set)
	if(isset($_GET['page'])){
		//new features as of 8/12/11: product sign up and navs-in-url
		$a = explode('/', $_GET['page']);
                
                $page_request   =   $_GET['page'];
                
                # convention: format = /*searchstring*/display-results/(*pagenumber*/)
                if(preg_match('#^([^/]*?)/display-results/([0-9]+)?/?$#', $page_request . '/', $page_reqdata))
                {
                    $page   =   'display-results';
                    $search_page_number =   $page_reqdata[2];
                }else
                {
		if(count($a)>1){
			for($i=0; $i<count($a); $i++){
				$subdir .= '../';
			}
			if($a[1] && !is_numeric($a[1])){
				//case: site.com/nav-section/page-title/
				$usePage = $a[1];
				//check to see if there's also a product id
				//in case this a signup page nested in a nav (site.com/nav-section/sign-up/123/
				if($a[2]){
					if(is_numeric($a[2])){
						$usePage = 'signup';
						$productID = $a[2];
					}
					else $usePage = $a[2]; //no product id, must just be a nested page
				}
			}
			else if($a[1] && is_numeric($a[1])){
				//case: site.com/sign-up/123/
				$usePage = 'signup';
				$productID = $a[1];
			}
		}
		else $subdir = '../';
		$usePage = $usePage ? $usePage : $_GET['page'];
		$getPage = str_replace("--", "~", $usePage); //workaround for having "-" in page title
		$pagearray = explode("-", $getPage);
		$pagearray = str_replace("~", "-", $pagearray); //more workaround for having "-" in page title
		//If there are 2 or more parts to the page name
		if(count($pagearray)>=2){
			$last = $pagearray[count($pagearray)-1]; //Get last part of the page name
			if(file_exists($themepath.$last.".php")){
				$page = $last; //If the last part of the page name is a file in the template's dir, load it
			}
			else if(file_exists($themepath.$usePage.".php")){
				$page = $usePage; //Or if the whole page name is the name of a file in the template's dir
			}
			else $page = 'default'; //Otherwise load the default (blank) page, which will pick up the page var itself
		}
		else {
			if(file_exists($themepath.$usePage.".php")){
				$page = $usePage;
			}
			else $page = 'default';
		}
        }   #end not display-results
	} //end if(isset($_GET['page']))
	else {
		$page = 'index';
		$subdir = './';
	}
	
	if($_GET['test']){
		echo 'hub_id: '.$hub_id.'<br />';
		echo '_get[page]: '.$_GET['page'].'<br />';
		echo 'page: '.$page.'<br />';
		echo 'themepath: '.$themepath.'<br />';
	}
	
	$template = $themepath.$page.'.php';
	$result = $hub->getHubs(NULL, NULL, $hub_id); //primes Hub results, and is needed for getHubRows()
	if($hub->getHubRows()) { //checks to see if the user has any Hubs
            
    
    
        #undefined variables
        $baseUrl    =   QUBESTORAGEURL;
        #end
        
        
		$row = $hub->fetchArray($result, 1);
		//check to see if user has set up pages for Hub
		if($row['pages_version']==2) $v2 = true;
		else $v2 = false;
		$hub_pages = $hub->getHubPage($hub_id, NULL, TRUE, TRUE, $v2);
		if($hub->getPageRows()){
			if($hub->checkPagesForForms($hub_id, $v2) && $page!='index'){
				$hasCustomForm = true;
			}
			$hasPages = true;
		}
		
		//if the user is a reseller client pull the reseller info for URLs
		if($row['user_parent_id']){
			$query = "SELECT main_site, private_domain FROM resellers WHERE admin_user = '".$row['user_parent_id']."'";
			$reseller = $hub->queryFetch($query);
			if($row['user_parent_id']==7 || $reseller['private_domain']==1) $baseUrl = 'https://6qube.com';
			else $baseUrl = 'http://'.$reseller['main_site'];
			$parentDomain = $reseller['main_site'];
		}
		else {
			$baseUrl = 'https://6qube.com';
			$parentDomain = '6qube.com';
		}
		
		if($row['has_tags']){
			////////////////////////////////////////////////////////////////////////
			// REPLACE TAGS WITH VALUES
			/////////////////////////////////////
			$row = $hub->replaceTags($row);
		}
		
		if($row['has_custom_form']){
			//////////////////////////////////////////////////////////////
			//look through certain fields for form embeds
			//
			// Add new custom form embed fields to the array below:
			$embedFields = array('overview', 'offerings', 'photos', 'videos', 'events', 'edit_region_6', 'edit_region_7', 
							 'edit_region_8', 'edit_region_9', 'edit_region_10', 'add_autoresponder');
			//////////////////////////////////////////////////////////////
			$embedForms = $hub->embedForms($embedFields, $row, $row['user_id'], $hub_id, NULL, NULL, $_SESSION['searchEngine'], $_SESSION['seKeyword']);
			$row = $embedForms['row'];
			if(!$hasCustomForm) $hasCustomForm = $embedForms['hasCustomForm'];
		}
		if($hasCustomForm){
			//if the hub or hub page has a custom for on it, tack the css and js file embeds into a field that goes in the header
			$row['adv_css'] = '
</style>
<link rel="stylesheet" href="'.$baseUrl.'/hubs/themes/'.$themeName.'/css/custom_form.css" />
<script src="'.$baseUrl.'/js/custom-form.js" type="text/javascript"></script>
<style>'.$row['adv_css'].'';
		}
		
		//check to see if user has any articles/press
		require_once( QUBEADMIN . 'inc/press.class.php');
		$press = new Press();
		if($result2 = $press->getPresss($row['user_id'], NULL, NULL, NULL, TRUE)){
			if($press->numRows($result2)) $hasPress = true;
		}
		require_once(QUBEADMIN . 'inc/articles.class.php');
		$art = new Articles();
		if($result2 = $art->getArticles($row['user_id'], NULL, NULL, NULL, TRUE)){
			if($art->numRows($result2)) $hasArticles = true;
		}
		
		//if user has a blog connected, instantiate blog class
		if($row['connect_blog_id']){
			$blog_id = $row['connect_blog_id'];
			$blog_url = $row['blog'];
			if($blog_url && substr($blog_url, -1) != "/") $blog_url .= "/";
			$hasBlog = true;
			require_once(QUBEADMIN . 'inc/blogs.class.php');
			$blog = new Blog();
		}
		
		@require_once($template);
	} 
	else { //if no Hubs
		echo 'This Hub Not Found.';
	}
?>
