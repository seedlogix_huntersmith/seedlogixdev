<?php
//This script is included on the default.php script for any theme that 
//supports custom pages
$v2 = ($row['pages_version']==2) ? 1 : 0;
$page_array = explode('/', $page);
if(count($page_array)>1){
	$subs = count($page_array);
	$page_title = $page_array[$subs-1];
	$useNav = $page_array[$subs-2];
	//if($hub_id==8493){
		$rootNav = ($subs>2) ? $page_array[0] : '';
	//}
}
else if(!$v2){
	$page_array2 = explode("-", strtolower($page));
	if(count($page_array2)>1) $page_title = trim(implode(" ", $page_array2));
	else $page_title = trim($page);
}
else $page_title = $page;
#var_dump($this);
if(!isset($this)) throw new Exception('This is not set in ' . __FILE__);
$page_row = $this->getSinglePageByTitle($page_title, $hub_id, $row, NULL, $v2, $useNav, $searchEngine, $seKeyword, $rootNav);
if(!$page_row){
	$page_row['page_title'] = "Page not found!";
#	echo '<!-- '; var_dump($this); echo ' -->';
	if(isset($this)) 
		$this->embedSiteFile('missing.php');
	else
		Qube::LogError('This is Not defined??!!');

}
else {
#	if(isset($_GET['SIMHOST'])) var_dump($page_row);
	$page_keywords = explode(",", $page_row['page_keywords']);
	foreach($page_keywords as $key=>$value){
		$value = trim($value);
	}
	if($v2){
		if(!$page_row['nav_root_id']) $thisPage = $page_row['page_title'];
		else $thisPage = $hub->getNavRootName($page_row['nav_root_id'], $hub_id);
		//include this for older themes that use page_navname
		$page_row['page_navname'] = $thisPage;
	}
	else $thisPage = $page_row['page_navname'];
}

if($page_row['nav_parent_id']){
	//get nav parents for url
	$pageNavUrl = '';
	$getParentID = $page_row['nav_parent_id'];
	$done = false;
	while(!$done){
		$query = "SELECT nav_parent_id, page_title_url FROM hub_page2 WHERE id = '".$getParentID."'";
		if($result = $hub->queryFetch($query)){
			$pageNavUrl = $result['page_title_url'].'/'.$pageNavUrl;
			if($result['nav_parent_id']) $getParentID = $result['nav_parent_id'];
			else $done = true;
		}
		else $done = true;
	}
}
//display page's URL
if(substr($row['domain'], -1) != "/") $row['domain'] .= "/"; //check/add trailing slash
$pageURL = $row['domain'].$pageNavUrl.$page_row['page_title_url'].'/';

//if($hub_id==7652) echo '<!--PAGE ID: '.$page_row['id'].' -->'; 
?>
