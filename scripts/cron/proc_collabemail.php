<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));
function get_cli_args()
{
	return array(
		'recipient=s' => 'Specify a Recipient for all responses. This will bypass the sentdate = now() query.',
		'confirm'	=> 'Confirm program execution.'
	);
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';
require HYBRID_PATH . 'classes/EmailSystem/NotificationQueueProcessor.php';

Qube::ForceMaster();    // I love this. 

$db =   Qube::GetPDO();
$db->query('SET SESSION group_concat_max_len = 1024*10;');

$c  =   new Cron(Cron::DOPRINT | Cron::DOLOG);
$Mailer	=	Qube::Start()->getMailer();
$Mailer->setLogger($c);
$VarContainer	=	new VarContainer(array('Mailer' => $Mailer), false);

$NProc	=	new NotificationQueueProcessor();
$NProc->setLogger($c);
$NProc->Execute($VarContainer);

$c->logINFO("All Done.");
