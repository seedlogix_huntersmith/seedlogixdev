<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));
function get_cli_args()
{
	return array(
		'days|d=i'	=>	'The number of days to process.',
		'perapi|a=i' => 'The number of sites to process per api request.',
		'numthreads|t=i' => 'Specify number of threads to run simulatneously.',
		'trackingids=s' => 'Specify tracking ids (optional.)',
		'mode=s'	=> 'Set the mode. If not defined, will run all reports.',
		'where=s'	=> 'Supply a condition to filter the tracking_ids from the last24hr     view.',
		'confirm'	=> 'Confirm program execution.',
		'daterange=s' => 'Specify a date range for visited sites. ex: 2015-01-01,2015-02-01 or updateallusers (with user option) (optional)',
		'user=i'	=> 'User ID (Default: all users, touchpoints visited in last 24 hours.)'
	);
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';
//require HYBRID_PATH . 'classes/reports/PiwikReportsDataAccess.php';
require HYBRID_PATH . 'classes/Cron.php';
require HYBRID_PATH . 'classes/reports/QubePiwikReportGenerator.php';

Qube::ForceMaster();		// I love this. 
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$ReportBitch	=	new QubePiwikReportGenerator(Qube::Start());
	
$archiving_options	=	array('perapi' => $Zopts->perapi,
					'interval' => $Zopts->days,	
					'period'	=>	'day',
					'numthreads' => $Zopts->numthreads,
				'mode'	=> $Zopts->mode,
				'where'	=>	$Zopts->where,
				'confirm' => $Zopts->confirm,
				'daterange' => $Zopts->daterange,
	'user' => $Zopts->getOption('user'));

$ReportBitch->setLogger($cron);

$ReportBitch->Execute(VarContainer::getContainer($archiving_options));
#$ReportBitch->generateDailyStats(new PiwikReportsDataAccess(), $archiving_options);
#$ReportBitch->saveKeywordVisits(array(), array(), 1, 'DAY', new PiwikReportsDataAccess());

//$ReportBitch->Execute(VarContainer::getContainer($archiving_options));
// connection dies after launching threads. attempt to reconnect.
Qube::DisconnectDB();
$cron->log("All Done.");
$cron->mail_if_errors(array('system_events@sofus.mx'), 'Collab Queue Cron Error.');
