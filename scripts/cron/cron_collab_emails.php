<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 3/16/15
 * Time: 1:56 PM
 */
chdir(dirname(__FILE__));
function get_cli_args()
{
    return array(
        'confirm'	=> 'Confirm program execution.',
    );
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';
require_once('Mail.php');

Qube::ForceMaster();		// I love this.
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$queueEmailer	=	new CollabQueueEmailer(Qube::Start());

#var_dump($Zopts->analyze);
#exit;

$cli_options	=	array(
    'confirm' => $Zopts->confirm
);


$queueEmailer->setLogger($cron);

$queueEmailer->Execute(VarContainer::getContainer($cli_options));