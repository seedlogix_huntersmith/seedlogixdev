<?php


chdir(dirname(__FILE__));
function get_cli_args()
{
	return array(
		'confirm'	=> 'Confirm program execution.',
	);
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';

Qube::ForceMaster();
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$senderTask = new ProspectRateGradesCron();
$senderTask->setLogger($cron);
$senderTask->run();