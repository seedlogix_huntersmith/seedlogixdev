<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


chdir(dirname(__FILE__));

define('QUBE_NOLAUNCH', 1);
require_once '../../hybrid/bootstrap.php';
require_once HYBRID_PATH . 'library/facebook-php-sdk/src/facebook.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';
    
echo 'Running..', "\n";


$Qube   =   Qube::Start();
SocialMonitor::MonitorTwitter($Qube);
SocialMonitor::MonitorFacebook($Qube);


echo 'Done..', "\n";
