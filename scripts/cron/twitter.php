<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';

Qube::ForceMaster();    // I love this. 

/**
 * 1. loop through blogs: check 6qube_history table for non-published posts
 * 2. initialize Zend_Service_Twitter for blog_post reseller
 * 3. make the post
 * 4. save history event.
 * 5. happy dance.
 * 
 */

$q  = PendingTwitterBlogPostModel::Select(Qube::Start());

$stmt   =   $q->Exec();
//var_dump($stmt->fetchAll()); exit;


$event_q    =   'INSERT INTO 6qube_history (user_ID, object, object_ID, event, data, value) VALUES (?, "TWITTER_BLOGPOST", ?, "sent", ?, ?) ';
$event_p    =   Qube::GetPDO()->prepare($event_q);

$qube   =   Qube::Start();

printf("Processing %d posts.\n", $stmt->rowCount());

while($twpost   =   $stmt->fetch())
{
    $reseller_id    =   (0 == $twpost->user_parent_id ? $twpost->user_id : $twpost->user_parent_id);
    
    // get cached appinfo for reseller
    if(!isset($twitters_map[$reseller_id]))
    {
        // load the reseller app info
       $rstmt  =   $qube->query('SELECT twapp_id, twapp_secret FROM %s WHERE admin_user = %d and twapp_id != ""', 'ResellerModel', $reseller_id);
       if(!$rstmt || $rstmt->rowCount() == 0){  // if no app info for reseller.. dont do anything
          $twitters_map[$reseller_id] =   false;
          continue;
        }else{
            $twitters_map[$reseller_id] =   $rstmt->fetch(PDO::FETCH_OBJ);
       }
        
    }
        

        list($o_token, $o_secret, $o_screenname)    =   explode("\n", $twpost->twitter_accessid);

/*            
            $tr     =   new Zend_Oauth_Token_Access;
            $tr->setToken($o_token);
            $tr->setTokenSecret($o_secret);
  */
        $tr	=	array('token'	=> $o_token,    'secret'    => $o_secret);

        $appInfo = $twitters_map[$reseller_id];

    if(!$appInfo->twapp_id) $twappid = '7n4KT8rtdiM1sxc7HPFTvGyrI';
    else $twappid = $appInfo->twapp_id;
    if(!$appInfo->twapp_secret) $twappsecret = 'DF2UQS4roV6MYcD1OXK407nG9ZAJMRFC0gnDtpuAunoi0Mfd0j';
    else $twappsecret = $appInfo->twapp_secret;
        
        $Twitter   =   new Zend_Service_Twitter(array('accessToken' => $tr, 
			'username' => $o_screenname,
			'httpClient'	=>	array('ssltransport' => 'tls',
						'adapter' => 'Zend_Http_Client_Adapter_Socket'),
				'oauthOptions' => array(
					'consumerKey'	=> '7n4KT8rtdiM1sxc7HPFTvGyrI',
					'consumerSecret' => 'DF2UQS4roV6MYcD1OXK407nG9ZAJMRFC0gnDtpuAunoi0Mfd0j'
                    )
		));
        
        $resp	=	$Twitter->account->accountverifyCredentials();

        $data   =   '';
	$statusmsg	=	"{$twpost->post_title} " . $twpost->getPostURL();
        
        try{
            $result =                   $Twitter->statusesUpdate($statusmsg);
#			$data	=	implode("\n", $result->getErrors());
		$success = !$result->isError();
            var_dump($result);
        }  catch (Exception $e){
            $result =   false;
            $data   =   $e->getMessage();
	$success	=	false();
            echo "Error: $data\n";
#	echo $data;
        }
        

        $event_p->execute(array($twpost->user_id, $twpost->id, $data, $success ? 1 : 0));
}


/** @var QubeLogPDOStatement $statment */
$statment = Qube::GetPDO()->prepare("SELECT sched.id, post.post_description, post.name, post.post_link, post.post_title, account.token FROM social_sched sched
  LEFT JOIN social_accounts account ON sched.social_id = account.id AND account.trashed = 0 AND account.token != ''
  LEFT JOIN social_posts post ON sched.post_id = post.id AND post.trashed = 0
WHERE ADDDATE(post.publish_date, INTERVAL time HOUR) < NOW() AND sched.success = 0 AND sched.trashed = 0 AND account.network = 'TWITTER'");

$statment->execute();
$count_pending_posts = $statment->rowCount();
$published_posts = 0;

while($pending_post = $statment->fetchObject()){
    list($token, $secret, $screen_name) = explode("\n", $pending_post->token);
    
    $tr = array('token' => $token, 'secret' => $secret);

    $Twitter = new Zend_Service_Twitter(array('accessToken' => $tr,
        'username' => $screen_name,
        'httpClient' => array('ssltransport' => 'tls',
            'adapter' => 'Zend_Http_Client_Adapter_Socket'),
        'oauthOptions' => array(
            'consumerKey' => '7n4KT8rtdiM1sxc7HPFTvGyrI',
            'consumerSecret' => 'DF2UQS4roV6MYcD1OXK407nG9ZAJMRFC0gnDtpuAunoi0Mfd0j'
        )
    ));


    try{
		$resp	=	$Twitter->account->accountverifyCredentials();
                //var_dump($resp);

		$data   =   '';
		$statusmsg	=	"{$pending_post->post_description}";
                //var_dump($statusmsg);
		$result =                   $Twitter->statusesUpdate($statusmsg);
		$success = !$result->isError();
		var_dump($result);

		if($success){
			Qube::GetPDO()->prepare("UPDATE social_sched SET success = 1 WHERE id = ?")->execute(array($pending_post->id));
		}
	}  catch (Exception $e){
		$result =   false;
		$data   =   $e->getMessage();
		$success	=	false;
		echo "Error: $data\n";
	}
}
