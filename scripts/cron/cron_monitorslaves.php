<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';

Qube::ForceMaster();		// I love this. 
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$runoptions	=	array();
try{
$ZOpts	=	new Zend_Console_Getopt(array(
		'c|c=i'	=>	'The number of cycles to run.',
		'secs|a=i' => 'The number of secs to wait between cycle.',
		
));
	
}catch(Zend_Console_Getopt_Exception $e){
	echo $e->getUsageMessage();
	exit;
}

for($i	=	0;	$i < $ZOpts->c;	$i++)
{
	$qstrsel	=	sprintf('select id, value FROM 6qube_history WHERE object = "SLAVE" AND object_ID = 0 AND event = "DOWN" and user_ID = 0 AND ts > DATE_SUB(now(), INTERVAL %d SECOND);', $ZOpts->secs);
	$result		=	$db->query($qstrsel);
	
	if($result->rowCount()){
		switch(TRUE){
			CASE stristr(PHP_OS, 'WIN'):
				exec('start.exe notepad.exe');
				break;
			CASE sstristr(PHP_OS, 'LINUX'):			
				exec(realpath(QUBEROOT) . '/scripts/update-sys.sh disable-slaves', $out);
				Qube::LogError('Disabling slaves');
				break;
		}
	}
	sleep($Zopts->secs);
}


$cron->log("All Done.");
$cron->mail_if_errors(array('6qube.error@sofus.mx'), 'Collab Queue Cron Error.');


