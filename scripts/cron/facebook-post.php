<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


chdir(dirname(__FILE__));

define('QUBE_NOLAUNCH', 1);
require_once '../../hybrid/bootstrap.php';
require_once HYBRID_PATH . 'library/facebook-php-sdk/src/facebook.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';
    
echo 'Facebook Online.', "\n";

define('APPID', '1604862766397665');
define('APPSECRET', '649610b9a32a251610ffd0418caeac6a');


//Function to Get Access Token
function get_app_token($appid, $appsecret)
{
$args = array(
'grant_type' => 'client_credentials',
'client_id' => $appid,
'client_secret' => $appsecret
);

$ch = curl_init();
$url = 'https://graph.facebook.com/oauth/access_token';
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
$data = curl_exec($ch);
echo "data: $data";
list(,$token) = explode('=', $data);
return $token;
return json_encode($data);
}


$FB_Objects =   array();
$default_FBAPP      =   array(
    'appId'  => APPID,
    'secret' => APPSECRET,
    'cookie' => false,
	'fileUpload' => false, // optional
	'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
    );
	
$q  = PendingFBBlogPostModel::Select(Qube::Start());

#echo $q;
$stmt   =   $q->Exec();

$event_q    =   'INSERT INTO 6qube_history (user_ID, object, object_ID, event, value, data) VALUES (?, "FB_BLOG_POST", ?, "sent", ?, ?) ';
$event_p    =   Qube::GetPDO()->prepare($event_q);

while(/** @var $fbpost PendingFBBlogPostModel */$fbpost   =   $stmt->fetch())
{
#	var_dump($fbpost->user_id, $fbpost->user_parent_id, $fbpost->post_to_fbid);
	/** @var Facebook $fbobj */
	$fbobj  =   $fbpost->loadFBObject($FB_Objects, Qube::Start(), $default_FBAPP);
	//var_dump($default_FBAPP);
   if($fbobj   === false)
   {
       // Qube::LogError('Failed to fetch fBobject: ', $fbpost);
       echo "Something Went Wrong...";
       continue;
   }
  /* 
   $config = array(
	  'appId' => '357544740941650',
	  'secret' => 'feae6ab2ab3977dcbd16ada1766951be',
	  'fileUpload' => false, // optional
	  'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
  );

	$fbobj = new Facebook($config);
*/    
    $message    =   '';
    try{
        $result =   $fbpost->PostToFacebook($fbobj);
        $success    =   ($result !== false);
    }catch(FacebookApiException $e){
	var_dump($fbpost->getFBAttachment('test'));
        $message    =   $e->getMessage();
	var_dump($message);
        $success    =   false;
    }
    
    $event_p->execute(array($fbpost->user_id, $fbpost->id, $success ? 1 : 0, $message));
}

/** @var QubeLogPDOStatement $statment */
$statment = Qube::GetPDO()->prepare("SELECT sched.id, post.post_description, post.name, post.post_link, post.post_title, account.token FROM social_sched sched
  LEFT JOIN social_accounts account ON sched.social_id = account.id AND account.trashed = 0 AND account.token != ''
  LEFT JOIN social_posts post ON sched.post_id = post.id AND post.trashed = 0
WHERE ADDDATE(post.publish_date, INTERVAL time HOUR) < NOW() AND sched.success = 0 AND sched.trashed = 0 AND account.network = 'FACEBOOK'");

$statment->execute();
$count_pending_posts = $statment->rowCount();
$published_posts = 0;

$config = array(
	'appId' => APPID,
	'secret' => APPSECRET,
	'fileUpload' => false,
	'allowSignedRequest' => false,
);

$fbobj = new Facebook($config);

while($pending_post = $statment->fetchObject()){
	list($site_id, $site_title, $token) = explode("\n", $pending_post->token);

	$data = array(
		'access_token' => $token,
		'message' => $pending_post->post_description,
		'name' => $pending_post->name,
		'link' => $pending_post->post_link,
		'description' => $pending_post->post_title,
		'actions' => array('name' => 'View Link Now', 'link' => $pending_post->post_link)
	);

	try{
		$result = $fbobj->api("/$site_id/feed/", "post", $data);

		if(!$result->error){
			Qube::GetPDO()->prepare('UPDATE social_sched SET success = 1 WHERE id = ?')->execute(array($pending_post->id));
			$published_posts++;
		}

	}catch (FacebookApiException $exception){

	}
}

echo "$published_posts of $count_pending_posts were successfully published";
