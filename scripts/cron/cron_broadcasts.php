<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/12/16
 * Time: 1:19 AM
 */
chdir(dirname(__FILE__));
function get_cli_args()
{
	return array(
		'confirm'	=> 'Confirm program execution.',
	);
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';
require_once('Mail.php');

Qube::ForceMaster();
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$senderTask = new EmailBroadcastSender();
$senderTask->setLogger($cron);
$senderTask->run();