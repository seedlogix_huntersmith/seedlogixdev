<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Change system directory.
chdir(dirname(__FILE__));

// Load SeedLogix bootstrap.
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';

// Load SeedLogix config and database.
Qube::ForceMaster();

/*define('CLIENT_ID', "86a8wviwxw9t3o"); // API KEY
define('CLIENT_SECRET', "SZEQwNoxSIDUa8fc"); // API SECRET
define('CALLBACK_URL', "https://seedlogix.com/linkedin.php");*/


$query = PendingLinkedInBlogPostModel::Select(Qube::Start());
$result = $query->Exec();
$linkedin_posts = $result->fetch();

$event_query = "INSERT INTO 6qube_history (user_ID, object, object_ID, event, data, value) VALUES (?, 'LINKEDIN_BLOGPOST', ?, 'sent', ?, ?) ";
$prepare = Qube::GetPDO()->prepare($event_query);

$qube = Qube::Start();

printf("Processing %d posts.\n", $result->rowCount());

$linkedin_map = array();
//var_dump($result->fetch());
while ($linkedin_post = $result->fetch()) {
	
	//$reseller_id = (0 == $linkedin_post->user_parent_id ? $linkedin_post->user_id : $linkedin_post->user_parent_id);
    
    /*if (!isset($linkedin_map[$reseller_id])) {
        $result = $qube->query("SELECT linkedin_app_id, linkedin_app_secret, linkedin_code FROM %s WHERE admin_user = %d and linkedin_app_id != ''", 
                'ResellerModel', $reseller_id);
        
        if (!$result || $result->rowCount() == 0) {
            $linkedin_map[$reseller_id] = false;
            continue;
            
        } else {
            $linkedin_map[$reseller_id] = $result->fetch(PDO::FETCH_OBJ);
            
        }
    }*/
    

    // Get LinkedIn app info for CURL
    list($pid, $tokenid) = explode("\n", $linkedin_post->linkedin_accessid);
	echo $pid. PHP_EOL;
    //var_dump($li_url, $li_profile_name, $li_grant_type, $li_client_id, $li_client_secret, $li_code);
    //var_dump($li_code);
	
    /*$app_info = $linkedin_map[$reseller_id];
    
    if (!$app_info->linkedin_app_id) {
        $linkedin_app_id = CLIENT_ID;
        
    } else {
        $linkedin_app_id = $app_info->linkedin_app_id;
        
    }
    
    if (!$app_info->linkedin_app_secret) {
        $linkedin_app_secret = CLIENT_SECRET;
        
    } else {
        $linkedin_app_secret = $app_info->linkedin_app_secret;
        
    }
    
    $params = array(
        'grant_type' => 'authorization_code', 
        'client_id' => CLIENT_ID, 
        'client_secret' => CLIENT_SECRET, 
        'code' => $li_code, 
        'redirect_uri' => CALLBACK_URL
    );

    //$data_string = json_encode($params);
    //var_dump($data_string);

    $url = "https://www.linkedin.com/oauth/v2/accessToken";

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
    }

    //print($response);
    $token = json_decode($response);
    var_dump($token);

    curl_close($curl); 

    // Store access token and expiration time
    //$_SESSION['access_token'] = $token->access_token;
    //$_SESSION['expires_in'] = $token->expires_in;*/
	
	///code should work
    $post_message = "{$linkedin_post->post_title} {$linkedin_post->getPostURL()}";
    
    try {
        $data_string = "{
            \"comment\": \"{$post_message}\",
            \"content\": {
              \"title\": \"{$linkedin_post->post_title}\",
              \"description\": \"\",
              \"submitted-url\": \"{$linkedin_post->getPostURL()}\",  
              \"submitted-image-url\": \"\"
            },
            \"visibility\": {
              \"code\": \"anyone\"
            }  
          }";

        //$url = "https://api.linkedin.com/v1/people/~/shares?format=json";
		$url = 'https://api.linkedin.com/v1/companies/'.$pid.'/shares?format=json';
		
		//echo $li_page_id;


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "authorization: Bearer {$tokenid}", 
            "Content-type: application/json", 
            "Content-length: " . strlen($data_string)
        ));

        $response = curl_exec($curl);
        $data = '';
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Error in curl request.
        if (!empty(curl_error($curl))) {
            print("Curl error: " . curl_error($curl));
            exit;
        }

        $share_response = json_decode($response);
        var_dump($share_response);
        
        if (empty($share_response->errorCode)) {
            $success = true;
        }
        
    } catch (Exception $ex) {
        $share_response = false;
        $data = $ex->getMessage();
        $success = false;
        
        echo "Error:  {$data}\n";
        
    }
    
    $savehistory = $prepare->execute(array($linkedin_post->user_id, $linkedin_post->id, $data, $success ? 1 : 0));
}


$query = "SELECT sched.id, post.post_description, post.name, post.post_link, post.post_title, account.token "
        . "FROM social_sched sched "
        . "LEFT JOIN social_accounts account "
        . "ON sched.social_id = account.id AND account.trashed = 0 AND account.token != '' "
        . "LEFT JOIN social_posts post "
        . "ON sched.post_id = post.id AND post.trashed = 0 "
        . "WHERE ADDDATE(post.publish_date, INTERVAL time HOUR) < NOW() "
        . "AND sched.success = 0 "
        . "AND sched.trashed = 0 "
        . "AND account.network = 'LINKEDIN'";

$result = Qube::GetPDO()->prepare($query);
$result->execute();
$count_pending_posts = $result->rowCount();
//var_dump($count_pending_posts);
$published_posts = 0;
//exit;
while (@$pending_post = $result->fetchObject()) {
    //var_dump($pending_post);
    $exploded_token = explode(PHP_EOL, $pending_post->token);
    //var_dump($exploded_token);
    $li_client_id = $exploded_token[3];
    $li_client_secret = $exploded_token[4];
    $li_access_token = $exploded_token[5];
    
    $post_message = "{$pending_post->post_title} {$pending_post->post_link}";
    
    try {
        $data_string = "{
            \"comment\": \"{$post_message}\",
            \"content\": {
              \"title\": \"{$pending_post->post_title}\",
              \"description\": \"{$pending_post->post_description}\",
              \"submitted-url\": \"{$pending_post->post_link}\",  
              \"submitted-image-url\": \"\"
            },
            \"visibility\": {
              \"code\": \"anyone\"
            }  
          }";
        //var_dump($data_string, $li_access_token);
        
        $url = "https://api.linkedin.com/v1/people/~/shares?format=json";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "authorization: Bearer {$li_access_token}", 
            "Content-type: application/json", 
            "Content-length: " . strlen($data_string)
        ));

        $response = curl_exec($curl);
        $data = '';
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Error in curl request.
        if (!empty(curl_error($curl))) {
            print("Curl error: " . curl_error($curl));
            exit;
        }

        $share_response = json_decode($response);
        //var_dump($share_response);
        
        //var_dump($share_response->errorCode === 0 || empty($share_response->errorCode));
        if (isset($share_response->errorCode)) {
            $success = false;
            print "Error: {$share_response->message}\n";
            
        } else {
            $success = true;
            
        }
        
        //var_dump($success);
        if ($success) {
            $query = "UPDATE social_sched "
                    . "SET success = 1 "
                    . "WHERE id = ?";
            
            $prepare = Qube::GetPDO()->prepare($query);
            $result = $prepare->execute(array($pending_post->id));
        }
        
    } catch (Exception $ex) {
        $share_response = false;
        $data = $ex->getMessage();
        $success = false;
        
        print "Error:  {$data}\n";
        //exit;
        
    }
    
}

print "Finished LinkedIn job...";