<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../../hybrid/bootstrap.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';

Qube::ForceMaster();    // I love this. 

/**
 * 1. loop through blogs: check 6qube_history table for non-published posts
 * 2. initialize Zend_Service_Twitter for blog_post reseller
 * 3. make the post
 * 4. save history event.
 * 5. happy dance.
 * 
 */

$q  = PendingTwitterBlogPostModel::Select(Qube::Start());

$stmt   =   $q->Exec();


$event_q    =   'INSERT INTO 6qube_history (user_ID, object, object_ID, event, data, value) VALUES (?, "TWITTER_BLOGPOST", ?, "sent", ?, ?) ';
$event_p    =   Qube::GetPDO()->prepare($event_q);

$qube   =   Qube::Start();

printf("Processing %d posts.\n", $stmt->rowCount());

while($twpost   =   $stmt->fetch())
{
    $reseller_id    =   $twpost->user_parent_id;
    if(!isset($twitters_map[$reseller_id]))
    {
        // load the reseller app info
#        $rstmt  =   $qube->query('SELECT twapp_id, twapp_secret FROM %s WHERE admin_user = %d and twapp_id != ""', 'ResellerModel', $reseller_id);
 #       if(!$rstmt || $rstmt->rowCount() == 0){
  #          $twitters_map[$reseller_id] =   false;
#        }else{
#            $postobj    =   $rstmt->fetch(PDO::FETCH_OBJ);

            list($o_token, $o_secret, $o_screenname)    =   explode("\n", $twpost->twitter_accessid);
            
            $tr     =   new Zend_Oauth_Token_Access;
            $tr->setToken($o_token);
            $tr->setTokenSecret($o_secret);
            
            $twitters_map[$reseller_id] =   new Zend_Service_Twitter(array('accessToken' => $tr, 'username' => $o_screenname));
 #       }
        
	}
        
        $Twitter = $twitters_map[$reseller_id];

#echo "Twitter: $Twitter";        
        if(!$Twitter) continue;
        $data   =   '';
	$statusmsg	=	"{$twpost->post_title} " . $twpost->getPostURL();

        try{
            $result =                   $Twitter->statusUpdate($statusmsg);

        }  catch (Exception $e){
            $result =   false;
            $data   =   $e->getMessage();

		echo "Error: $data\n";
#	echo $data;
        }
        
	
        
            $success    =   ($result !== false);

            $event_p->execute(array($twpost->user_id, $twpost->id, $data, $success ? 1 : 0));
}

