<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));
function get_cli_args()
{
	return array(
			'cachedir=s' => 'Directory to store html search results.',
			'analyze' => 'Run in Analyze mode.',
			'crawl' => 'Run in crawl mode.',
			'parse' => 'Run in parse mode.',
			'proxyprepare' => 'Run in proxyprepare mode.',
			'no-google' => 'No Google',
			'no-bing' => 'No Bing',
			'date=s' => 'Date to parse files',
		'targetkeyword=s' => 'Parse single keyword.',
		'numthreads|t=i' => 'Specify number of threads to run simulatneously.',
		'wait|w=i'	=> 'Seconds to wait after each page request. default is 0.',
		'verbose' => 'Enable curl verbose mode.',
		'confirm'	=> 'Confirm program execution.',
	);
}
define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require_once QUBEADMIN . '/models/BlogPostModel.php';
require HYBRID_PATH . 'classes/Cron.php';
require HYBRID_PATH . 'classes/Rankings/KeywordRanker.php';
require HYBRID_PATH . 'classes/Rankings/RankingsGenerator.php';

Qube::ForceMaster();		// I love this. 
$db = Qube::GetPDO();

$cron = new Cron(Cron::DOPRINT);

$ReportBitch	=	new RankingsGenerator(Qube::Start());

#var_dump($Zopts->analyze);
#exit;

$cli_options	=	array(
			'cachedir' => $Zopts->cachedir,
					'analyze' => $Zopts->analyze,
				'crawl' => $Zopts->crawl,
				'parse' => $Zopts->parse,
				'proxyprepare' => $Zopts->proxyprepare,
					'numthreads' => $Zopts->numthreads,
				'confirm' => $Zopts->confirm,
		'verbose' => $Zopts->verbose,
		'no-google' => $Zopts->getOption('no-google'),
		'no-bing' => $Zopts->getOption('no-bing'),
			'date' => $Zopts->date,
	'targetkeyword' => $Zopts->targetkeyword,
			'pagewait' => $Zopts->wait);


$ReportBitch->setLogger($cron);

$ReportBitch->Execute(VarContainer::getContainer($cli_options));


if($ReportBitch->getThreadID() == 0)
{
	// connection dies after launching threads. attempt to reconnect.
	Qube::DisconnectDB();
	$cron->log("All Done.");
	$cron->mail_if_errors(array('info@sofus.mx'), 'Collab Queue Cron Error.');
}