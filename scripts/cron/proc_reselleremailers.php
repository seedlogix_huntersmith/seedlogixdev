<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

define('QUBE_NOLAUNCH', 1);
require '../../hybrid/bootstrap.php';
require HYBRID_PATH . 'classes/Cron.php';

$c  =   new Cron(Cron::DOPRINT);

Qube::ForceMaster();    // I love this. 

$db =   Qube::GetPDO();

$query  =   'SELECT  e.*, GROUP_CONCAT(c.id) as reseller_free_classids  FROM resellers_emailers e
                LEFT JOIN user_class c 
                    ON (c.maps_to = 27 AND c.admin_user = e.admin_user) 
                LEFT JOIN 6qube_history h 
                    ON (h.object = "reseller_email" AND h.event = "processed" AND h.object_ID = e.id AND (e.sched_mode2 = "now" OR e.retro = 1))
                WHERE (sched_mode2 = "signup" OR (sched > 0 AND 
                (CASE sched_mode WHEN "hours" THEN e.created + INTERVAL sched HOUR
                    WHEN "days" THEN e.created + INTERVAL sched DAY
                    WHEN "weeks" THEN e.created + INTERVAL sched WEEK
                    WHEN "months" THEN e.created + INTERVAL sched MONTH
                    END) < NOW()
                ) ) AND active = 1
                AND from_field != "" AND subject != "" AND body != "" AND e.trashed = 0 AND ISNULL(h.id)
             GROUP BY e.id';
$query	=	'SELECT * FROM 6qv_reselleremailers_pending';
$stmt   =   $db->query($query);

$c->log('Processing %d Reseller Emailers', $stmt->rowCount());

class ResellerEmailer {
    const FREE_USER_NEVERLOGGEDIN   =   -3;
    const NONFREE_USER              =   -2;
    protected $emailer_type;
    protected $sendto_type;
    protected $sendto_class;
    protected $reseller_free_classids;
    
    function getPendingUserRecipients(){
        $q  =   $this->getUserIdQuery();
        
        $q->leftJoin(array('6qube_history', 'h'),
            'h.object_ID = %d AND h.user_ID = u.id AND h.object = "reseller_email" AND h.event = "sent" AND h.value = 1', $this->id);
        
        $q->Where('ISNULL(h.id)');
        
        return $q;
    }
    
	function do_convert_sentto(){
		if($this->users_sent == '') return false;
		$userids	=	explode('::', $this->users_sent);
		
		Qube::GetPDO()->beginTransaction();
	foreach($userids as $userid){
			$userid	=	trim($userid);
			$this->SaveHistory($userid);	// set sent status to 1 for the particular user.
		}
		Qube::GetPDO()->query('UPDATE resellers_emailers SET users_sent = "" WHERE id = ' . $this->id);
		// reset users_sent value to '' so that we do not re-convert the sent mode.
		Qube::GetPDO()->commit();
		return true;
	}

	function UpdateDateSent(){
		Qube::GetPDO()->query('UPDATE resellers_emailers SET date_sent = NOW() WHERE id = ' . $this->id);
	}

    function getUserIdQuery(){
        $q  =   new DBSelectQuery(Qube::GetPDO());
        $q->From('users u');
        
        if($this->emailer_type == 'marketing')
            $q->Where('marketing_optout != 1');
        
        switch($this->sendto_type){
            case 'active':
                $q->Where('access = 1 AND canceled = 0');
                break;
            case 'suspended':
                $q->Where('access = 0 AND canceled = 0');
                break;
            case 'active_suspended':
                $q->Where('canceled = 0');
                break;
            case 'canceled':
                $q->Where('canceled = 1');
                break;
        }
        
        switch($this->sendto_class){
            case -1:    break;  // email all users
            case -2:    case -3:    case -4:
                if($this->admin_user    ==  7   || $this->admin_user    ==  40){    // 6qube-only admin rule
                    if($this->sendto_class  == ResellerEmailer::NONFREE_USER){
                        // all non free??
                        $q->Where('class != 1 AND class != 178');   // .. nmeed to make sense of these numbers
                    }else{  // -3 or -4
                        // all free users
                        $q->Where('(class = 1   OR  class   =   178)');
                        
                        if($this->sendto_class  == ResellerEmailer::FREE_USER_NEVERLOGGEDIN){
                            // free users never logged in
                            $q->Where('last_login = 0');
                        }
                    }
                }else{
                    // use resellers free class ids
                    if($this->sendto_class  == ResellerEmailer::NONFREE_USER){
                        $q->Where('class NOT IN (%s)', $this->reseller_free_classids);
                    }else{
                        // -3 or -4, free users ignoring logged in date
                        // the original script does not check for free_user-neverloggedin class
                        // it strictly hard codes the last_login = 0  along with the class = (free)
                        // which renders the -4 class useless (free user classes)
                        // giving -4 and -3 states the same behavior
                        // which is to only email users that have never logged in
                        // compared to the above code:
                        // -4: all free users
                        // -3: all free users never logged in.
                        // however, the database shows no values of -4. 
                        // giving the code and db info, I have to assume
                        // that emailing all non-free users that have previously loged in is disabled for 
                        // non-admin resellers.
                        // instead, Im going to assume all that buullshit is coding error
                        // and enable the -4 class (email all free users)
                        
                        // @TODO TEST THIS.
                        $q->Where('class IN (%s)', $this->reseller_free_classids);
                        if($this->sendto_class  == ResellerEmailer::FREE_USER_NEVERLOGGEDIN){
                            // free users never logged in
                            $q->Where('last_login = 0');                            
                        }
                    }
                }
                break;
            default:
                $q->Where('class = %d', $this->sendto_class);
        }
        
        if($this->admin_user    !=  7){
            $q->Where('parent_id = %d', $this->admin_user);            
        }else{
            $q->Where('(parent_id = 0 OR parent_id = 7)');
        }

	if($this->sched_mode2 == 'signup' && $this->date_fetch != ''){		
		$q->Where('created > "%s"', $this->date_fetch);
	}        
        return $q;
    }

	// the 'logging' structure is as follows:
	// the "object" value is always "reseller_email"
	// the "event" can be "sent" or  "processed"
	// for "sent" event, the user_ID corresponds to the recipient's user ID.
	// for "processed" event, the user_ID corresponds to the reseller's user ID.
    
    function SaveHistory($user_id, $value = 1, $event = "sent", $data = ''){
        static $insert    =   false;
        if(!$insert)
            $insert =   Qube::GetPDO ()->prepare('INSERT INTO 6qube_history (user_ID, object, object_ID, event, data, value) VALUES (?, "reseller_email", ?, ?, ?, ?)');
        
        return $insert->execute(array($user_id, $this->id, $event, $data, $value));
    }
    
}

$continue   =   true;
while($emailer  =   $stmt->fetchObject('ResellerEmailer')){

// convert the emailer users_sent column into 6qube_history events.
	$emailer->do_convert_sentto();

    $userQuery  =   $emailer->getPendingUserRecipients();
    $per_page   =   100;    // process 100 users at a time
    
    $userQuery->Limit($per_page);
    
#    echo $userQuery, "\n";
	
	
    
	$sent_count	=	0;
	$failed_count	=	0;
	$sent_log	=	array();
    while(true){
        // loop until there are no more pages to process.
        $recipientsStmt   =   $userQuery->Exec();
        if($recipientsStmt->rowCount() == 0) break;
        
        while($recipient    =   $recipientsStmt->fetchObject('UserModel')){

		$log	=	sprintf('Sending to (%d : %s)', $recipient->id, $recipient->username);
		$sent_log[]	=	$log;

		$result = 1;

		// to do send email and get result
	
		// end. 

		
		if($result) $sent_count++;
			else	$failed_count++;

            $emailer->SaveHistory($recipient->getID());
        }
    }
    $c->log('id: %d, Sent to %d Users. %d Errors.', $emailer->id, $sent_count, $failed_count);
    $emailer->SaveHistory($emailer->admin_user, $sent_count, "processed", implode("\n", $sent_log));
	$emailer->UpdateDateSent();
#    break;
    
}

$c->mail_if_errors(array('cron.error@sofus.mx', '6qube.error@sofus.mx'), 'Collab Queue Cron Error.');
