<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2/9/15
 * Time: 2:56 PM
 */
define('QUBE_NOLAUNCH', 1);
require_once '../hybrid/bootstrap.php';
                // ------    VARIABLE DEFINITIONS    ------- //

                $api_key = '80883169c51a9795';
                $error_limit = 20;
                // - Avoid doing API calls and just update prospect images using already collected data stored.
                $skip_data_collection = true;

                // ------ RETRIEVE USER LIST SECTION ------- //
// - Start Process
        echo "Starting...\n";
// - Amount of Users to update Query Preparation
        $count_query = "SELECT count(*) FROM prospects WHERE photo = '' AND email != '' ";
// - Initialize query to get user list limit 5000
        $query = "SELECT DISTINCT email FROM prospects WHERE email NOT IN(SELECT email FROM full_contact_data) AND prospects.photo = '' AND prospects.email != '' LIMIT 5000;";
// - Initialize DataAccess
        echo "Getting List..\nAmount of Users to Update: ";
        $Data_Access = new ProspectDataAccess();
// - Echo Amount of Users
        $user_amount = $Data_Access->query($count_query)->fetchColumn(0);
        echo $user_amount;
        $Statement = $Data_Access->query($query);
        echo "\nStandBy..\n";

// - LOOP while we keep getting results
        $processed_users = 0;
// - Initialize Wait TimeOut (X_Rate_Limit_Limit / 60)
        $time_out = 0;
// - Initialize Error Count Variable.\
        $error_count = 0;
// - Make sure no past processed prospect where left with PROCESSING value
        $restore_query = "UPDATE prospects SET photo = '' WHERE photo = 'PROCESSING'";
        if(!$Data_Access->query($restore_query)){echo "Failed restoring empty values to prospects!.\n"; $error_count++;}
// - Data Collection.
        if(!$skip_data_collection) {
                echo "Filling Contact Info...\n";

                while ($prospect_list = $Statement->FetchAll()) {

                        // - We iterate through the list
                        foreach ($prospect_list as $prospect) {
                                // Query to mark processed prospects.
                                $mark_process_query = "UPDATE prospects SET photo = 'PROCESSING' WHERE photo = '' and email = '{$prospect['email']}'";
                                $Data_Access->query($mark_process_query);
                                // - Get User Email.
                                $user_email = $prospect['email'];
                                // - Step - Request USER information through API.
                                sleep($time_out);
                                $api = new Services_FullContact_Person($api_key);
                                $api_response = $api->lookupByEmail($user_email);
                                //-- Check response
                                if ($api_response->status = 200) {
                                        echo ".";
                                        // -- Response OK , Next Step - Save Data in full_contact_data
                                        // -- Prepare Response for JSON encode
                                        $response_json_string = json_encode($api_response);
                                        // -- Prepare Sql query to save response to database
                                        $parsed_response = str_replace("'","",$response_json_string);
                                        $parsed_response = str_replace("\n","",$response_json_string);
                                        $parsed_response = str_replace("\r\n","",$response_json_string);
                                        $parsed_response = str_replace("\r","",$response_json_string);
                                        $save_response_query = "INSERT INTO full_contact_data VALUES ('$user_email','$parsed_response')";
                                        if (!$Data_Access->query($save_response_query)) {
                                                echo "Failed saving into database E+";
                                                $error_count++;
                                        }
                                        // set sleep marker
                                        $time_out = $api_response->X_Rate_Limit_Limit / 60;

                                } else {
                                        switch ($api_response->status) {
                                                case 400:
                                                        echo "Error 400, Bad Request, \n";
                                                        break;
                                                case 403:
                                                        echo "Error 403, Wrong API Key or API Calls reached their limit. Aborting.\n";
                                                        echo "Bye.";
                                                        exit();
                                                        break;
                                                case 405:
                                                        echo "Method not allowed.\n";
                                                        break;
                                                case 422:
                                                        echo "Invalid API Call\n";
                                                        break;
                                                case 500:
                                                        echo "Server Error\n";
                                                        break;
                                                default:
                                                        echo "Unknown Error Code:" . $api_response->status . "\n";
                                        }
                                        echo "x";
                                        $error_count++;

                                }

                                //-- Check Error Count
                                if ($error_count > $error_limit) {
                                        echo "Too many Errors, Aborting.\n";
                                        echo "Bye.";
                                        exit();
                                }
                                //-- Continue Working .
                                $processed_users++;

                        }
                        echo $processed_users . ' of ' . $user_amount . "\n";
                        $Statement = $Data_Access->query($query);
                }

        }

                // - Phase 2 - Image Saving.
        echo "Restoring Values...\n";
        if(!$Data_Access->query($restore_query)){echo "Failed restoring empty values to prospects!.\n"; $error_count++;}
        echo "Scanning for Images...\n";
        $query = "SELECT user_id, A.email, data from full_contact_data as A INNER JOIN (SELECT DISTINCT user_id, prospects.email FROM prospects WHERE email != '' AND photo = '') P ON P.email = A.email";
        $Statement = $Data_Access->query($query);
        $prospect_list = $Statement->fetchAll();
        echo "Found " . count($prospect_list) . " Prospects \nLooking for images...";
        $processed_users = 0;
        $users_with_images = 0;
        foreach($prospect_list as $prospect){
                if($error_count > $error_limit){
                        echo "Too many errors!\n";
                        break;
                }
                $prospect_data = json_decode($prospect['data']);
                switch(json_last_error()) {
                        case JSON_ERROR_DEPTH:
                                echo "\nJSON- Maximum stack depth exceeded.\n";
                                $error_count++;
                                break;
                        case JSON_ERROR_CTRL_CHAR:
                                echo "\nJSON- Unexpected control character found.\n";
                                $error_count++;
                                break;
                        case JSON_ERROR_SYNTAX:
                                echo "\nJSON- Syntax error, malformed JSON\n";
                                $problematic_emails[] = $prospect['email'];
                                $error_count++;
                                break;
                        case JSON_ERROR_NONE:
                                break;
                }
                if (!isset($prospect_data->photos)){
                        // - No photos
                        $image_query = "UPDATE prospects SET photo = '' WHERE user_id = '{$prospect['user_id']}' and email = '{$prospect['email']}'";
                        $Data_Access->query($image_query);

                }else{
                        // - Yes photos
                        $prospect_photo = $prospect_data->photos[0]->url;
                        // - Save into Database
                        $photo_dir_location = UserModel::getStorage($prospect['user_id'],true,'prospects') . "/" . $prospect['email'];
                        $photo_dir_url = UserModel::getStorageURL_ID($prospect['user_id']) . '/prospects' . "/" . $prospect['email'];
                        $image_query = "UPDATE prospects SET photo = '$photo_dir_url' WHERE user_id = {$prospect['user_id']} and email = '{$prospect['email']}'";
                        if(!$Data_Access->query($image_query)){
                                echo "Failed Saving image to Database E+\n";
                                $error_count++;
                        }
                        // - Save into Drive
                        if(!copy($prospect_photo,$photo_dir_location)){
                                echo "Failed Retrieving image! E+";
                                $error_count++;
                        }
                        $users_with_images++;

                }
                $processed_users++;
                echo ".";
                if(($processed_users % 100) == 0){
                        echo $processed_users ." processed of " . count($prospect_list) . "\n";

                }
        }

        // Restore PROCESSING value
        if(!$Data_Access->query($restore_query)){echo "Failed restoring empty values to some prospects!.\n";}
        echo "Finished excecution, error count = " . $error_count . "\n Found " . $users_with_images . " prospects with available images, of " . count($prospect_list) . ".\n";
        if($problematic_emails) {
                echo "Problematic Emails: \n";
                foreach ($problematic_emails as $email) {
                        echo $email . "\n";
                }
        }

