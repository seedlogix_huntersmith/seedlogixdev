<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2/16/15
 * Time: 3:42 PM
 */
define('QUBE_NOLAUNCH', 1);
require_once dirname(__FILE__) . '/../hybrid/bootstrap.php';
const LAST_MINUTES = '300';

    // -- Create Data Access
    $Data_Access = new UserDataAccess();
    $query_users_tbl = "SELECT id FROM users WHERE last_upload > date_sub(now(), interval " . LAST_MINUTES . ' MINUTE )';
    $query_hub_tbl = "SELECT user_id from hub WHERE last_updated > date_sub(now() , INTERVAL " . LAST_MINUTES . " MINUTE )";


    $query = "($query_users_tbl) UNION DISTINCT ($query_hub_tbl)";
    $Statement = $Data_Access->prepare($query);
    $Statement->execute();

    while($user_id = $Statement->fetchColumn())
    {
        echo UserModel::GetUserSubDirectory($user_id), " ";
    }
