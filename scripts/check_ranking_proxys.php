<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/27/15
 * Time: 2:10 PM
 */
chdir(dirname(__FILE__));
define('QUBE_NOLAUNCH', 1);
require '../hybrid/bootstrap.php';

$ranking = new CheckRankingProxys();
$ranking->setLogger(new Cron(Cron::DOPRINT));
$ranking->Execute();