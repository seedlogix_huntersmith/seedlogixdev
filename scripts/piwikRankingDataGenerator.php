<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 11/4/14
 * Time: 11:18 AM
 */

$campaignIDs = array(
	'39595' => array(
		'siteID' => 19,
		'keywordsIDs' => array(
			'115',
			'116',
			'117',
			'118',
			'119',
			'120',
			'121',
			'122',
			'123',
			'124',
			'125',
			'126',
			'127',
			'128',
			'129',
			'130',
			'131',
			'132',
			'133',
			'134',
			'135',
			'136',
			'137',
			'138',
			'139',
			'140',
			'141',
			'142',
			'143',
			'144',
			'145',
			'146',
			'147',
			'148',
			'149',
			'150',
			'151',
			'152',
			'153',
			'154',
			'155',
			'156',
			'157',
			'158',
			'159',
			'160',
			'161',
			'162',
			'163',
			'164',
			'165',
			'166',
			'167',
			'168',
			'169',
			'170',
			'171',
			'172',
			'173',
			'174',
			'175',
			'176',
			'177',
			'178',
			'179',
			'180',
			'181',
			'182',
			'183',
			'184',
			'185',
			'186',
			'187',
			'188',
			'189',
			'190',
			'191',
			'192',
			'193',
			'194',
			'195',
			'196',
			'197',
			'198',
			'199',
			'200',
			'201',
			'202',
			'203',
			'204',
			'205',
			'206',
			'207',
			'208',
			'209',
			'210',
			'211',
			'212',
			'213'
		)
	)
);

$searchEngines = array(
	'Google'
);

$interval = 'P1D';
$dayLimit = new DateTime();
$dayLimit->sub(new DateInterval($interval));
foreach($tblIDs as $id => $type){
	$date = new DateTime();

	$nSearchEngines = rand(2, 10);
	$searchEnginesIndexes = array();
	for($i = 0; $i <$nSearchEngines; $i++){
		do{
			$index = rand(0, count($searchengines) - 1);
		}while(in_array($index, $searchEnginesIndexes));
		$searchEnginesIndexes[] = $index;
	}

	for($d = $date; $d >= $dayLimit; $date->sub(new DateInterval('P1D'))){
		foreach($searchEnginesIndexes as $index){
			$visits = rand(0, 100);
			$leads = rand(0, $visits);
			$searchEngine = $searchengines[$index];
			echo "INSERT INTO ranking_dailystats VALUES('{$date->format('Y-m-d H:i:s')}', 6610, 39595, '$type', $id, 'DAY', $searchEngine ,$visits, $leads, null);\n";
		}
	}
}

