<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


chdir(dirname(__FILE__));
require_once '../hybrid/bootstrap.php';
    
Qube::ForceMaster();

echo 'L:aunching ..', "\n";

$q  =   Qube::Start();
$pdo    =   Qube::GetPDO();


if(in_array('forms', $argv)){
$q  =   'SELECT f.* FROM contact_form f WHERE NOT EXISTS ( select 1 FROM 6q_meta WHERE `table` = "contact_form" AND PK = f.id)';
$r  =   $pdo->query($q);

$cols   =   array('name', 'email', 'phone', 'comments');
$grades =   array('D', 'C', 'B', 'A');

if($r->rowCount()){

$pdo->beginTransaction();

while($contact = $r->fetchObject()){
    // validate name, email, phone, message
    $grade  =   0;
    foreach($cols as $colname){
        if(strlen($contact->$colname) > 3) $grade++;
    }
    
    $grade  =   5-(max($grade, 1));   // 
    $pdo->query('REPLACE INTO 6q_meta (`table`, `PK`, `col`, `valint`) VALUES ( "contact_form", ' . $contact->id . ', "grade", ' . $grade . ') ');
}
$pdo->commit();
	echo 'Done';
}else{
	echo 'No forms count. (all have been graded.)';
}
}

if(in_array('leads', $argv)){
function parse_shitty_data($data){
    $fieldarray =   array();
    $fields =   explode('[==]', $data);
    foreach($fields as $shitty_string){
        $field_data =   explode('|-|', $shitty_string);
        $fieldarray[$field_data[1]] =   $field_data[2];
    }
    
    return $fieldarray;
}


$q  =   'SELECT f.* FROM leads f WHERE NOT EXISTS (select 1 FROM 6q_meta WHERE `table` = "leads" AND PK = f.id)';
$r  =   $pdo->query($q);


if(!$r->rowCount()){
	echo 'No leads found. (all ahve been graded.)';
	exit;	
}

$pdo->beginTransaction();
while($contact = $r->fetchObject()){
    // validate name, email, phone, message
$cols   =   array('lead_name' => $contact->lead_name, 'lead_email' => $contact->lead_email);
    $grade  =   0;
    $fields = parse_shitty_data($contact->data) + $cols;
    
    $numfields  =   count($fields);
    $emptyfields    =   0;
    
    foreach($fields as $name => $value){
        if(empty($value)) $emptyfields++;
    }
    
    if($numfields < 10){
        // if less than 10 fields. 
        $grade  =   min(4, $emptyfields+1);
    }else{
        $tenpoint  = floor((($numfields-$emptyfields)/$numfields)*10);
        $grade     =   11-max($tenpoint, 7);
    }
    
//    echo "GRADE: $grade. Empty: $emptyfields/$numfields\n";
    $pdo->query('REPLACE INTO 6q_meta (`table`, `PK`, `col`, `valint`) VALUES ( "leads", ' . $contact->id . ', "grade", ' . $grade . ') ');
}

$pdo->commit();

echo 'Done. ';
}
