#!/bin/bash

if [ -z $2 ]
then
	echo "Usage: $0 [SECONDS] [LIMIT] [ITERATIONS]"
exit
fi

iterations=0
if [ ! -z "$3" ] 
then
iterations=$3
fi

limit=$2
counter=0

while [ -z "$3" ] || [ $counter -lt $iterations ]
do
	((counter++))
    mysql -D sapphire_6qube -e "SELECT label, nb_visits, user_ID, point_date FROM piwikreports_keywords ORDER BY point_date DESC LIMIT $limit"
    sleep $1
done
