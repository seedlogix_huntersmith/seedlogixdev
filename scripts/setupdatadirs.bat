#!/bin/bash

if [ "$1" == "" ]; then
	echo "Must pass the data directory as an argument.";
	exit;
fi

cdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $cdir

echo Current Directory is $cdir

cd ..

pwd

function makelink {
# 1 - linkname, 2 target

if [ -L $1 ] ; then
#	echo "$2 exists: "
	ls -lah $1
else
	echo "Creating $1 directory: "
	ln -s $2 ./$1
fi

}

for strdirname in reseller sitedata 
do
	makelink $strdirname $1/$strdirname
done


makelink users $1/6qube-users


echo "Setting up Hybrid links"

cd hybrid

makelink blogs ../blogs
makelink branding ../admin/themes
makelink hubs ../hubs
makelink js ../js
makelink models ../admin/models
makelink reseller ../reseller
makelink users ../users
makelink data $1/sitedata

