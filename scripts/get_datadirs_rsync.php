<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 1/13/15
 * Time: 11:04 AM
 */

chdir(dirname(__FILE__));

function get_cli_args()
{
    return array(
        'interval=s'	=> 'Get data where last upload is within the interval',
    );
}


define('QUBE_NOLAUNCH', 1);
require '../hybrid/bootstrap.php';


$str = $Zopts->interval ? $Zopts->interval : " INTERVAL 5 HOUR";

echo "Str: $str\n";


$pdo = Qube::GetPDO();
$q = sprintf('SELECT ID FROM users WHERE last_upload > date_sub(CURRENT_DATE, %s)', $str);

echo $q;
$ids  = $pdo->query($q)->fetchAll();

var_dump($ids);