#!/usr/bin/env bash

limit=$2
if [ -z $2 ]
then
    limit=10
fi

while [ : ]
do
    mysql -e "SELECT * FROM 6q_socialstats_tbl ORDER BY datetime LIMIT $limit"
    sleep $1
done
