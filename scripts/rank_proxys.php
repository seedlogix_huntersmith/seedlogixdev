<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/14/15
 * Time: 12:53 PM
 */

$proxys = file('cron/proxy.php');
$i = 1;
foreach($proxys as $proxy){
	$proxy = trim($proxy);
	list($ip, $port, $username, $pass) = explode(":", $proxy);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19');
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, 'http://www.bing.com/search?q=fire+alarm+installation+Cedar+Bluf%2c+AL&count=50&first=51&FORM=PORE');
//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiejar);
//curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiejar);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_STDERR, fopen('php://stdout', 'w'));

// proxy stuff
	curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
	curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_PROXY, $ip);
	curl_setopt($ch, CURLOPT_PROXYPORT, $port);
	curl_setopt($ch, CURLOPT_PROXYUSERPWD, "$username:$pass");

	$data = curl_exec($ch);
	$return_info = curl_getinfo($ch);
	$error_info = curl_error($ch);
	echo "$i: $proxy\n";
	echo "$i: {$return_info['http_code']}\n";
	curl_close($ch);
	$i++;
}


