<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 5/28/15
 * Time: 2:56 PM
 */
chdir(dirname(__FILE__));
define('QUBE_NOLAUNCH', 1);
require '../hybrid/bootstrap.php';
function info($message){
	echo "$message\n";
}
$pdo = Qube::GetPDO();
$date = new DateTime('2015-05-09');
$stmt = $pdo->prepare("SELECT MAX(ID) FROM piwik_apidata WHERE datestr = :date GROUP BY apimethod, datestr, tracking_ID");
$stmt->bindParam('date', $dateStr);

info("Script execution started...");
for($date = new DateTime('2015-05-09'), $today = new DateTime('today'); $date <= $today; $date->add(new DateInterval('P1D'))){
	$dateStr = $date->format('Y-m-d');
	info("Retreving ids for $dateStr");
	$stmt->execute();
	$ids = $stmt->fetchAll(PDO::FETCH_COLUMN);
	if(empty($ids)){
		info("Ids not founded for $dateStr Skipping...");
		continue;
	}
	$ids = join(',', $ids);
	info("Deleting ids for $dateStr");
	$deleteStmt = $pdo->prepare("DELETE FROM piwik_apidata WHERE ID NOT IN($ids) AND datestr = :date");
	$deleteStmt->bindParam('date', $dateStr);
	$deleteStmt->execute();
}

info("Script execution ended");