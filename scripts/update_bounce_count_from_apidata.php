<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 5/22/15
 * Time: 10:14 AM
 */

chdir(dirname(__FILE__));
define('QUBE_NOLAUNCH', 1);
require '../hybrid/bootstrap.php';
function info($message){
	echo "$message\n";
}
//The number of rows fetched per query
$limit = 1000;
$pdo = Qube::GetPDO();
$api_methods = array(
	'VisitsSummary.get' => 'piwikreports',
	'Referers.getSearchEngines' => 'piwikreports_se',
	'Referers.getWebsites' => 'piwikreports_ref',
	'Referers.getSocials' => 'piwikreports_social'
);

info("Script execution started...");
foreach($api_methods as $api_method => $piwikreport_table) {

	/** @var QubeLogPDOStatement $ids_stmt */
	$ids_stmt = $pdo->prepare("SELECT max(ID) FROM piwik_apidata WHERE apimethod = '$api_method' GROUP BY apimethod, tracking_ID, datestr LIMIT :index, $limit");
	$index = 0;
	$ids_stmt->bindParam('index', $index, PDO::PARAM_INT);

	info("Obtaining IDs $index - " . ($index + $limit) ." for $api_method");
	$ids_stmt->execute();
	while ($results = $ids_stmt->fetchAll(PDO::FETCH_COLUMN)) {
		$ids = join(',', $results);
		/** @var QubeLogPDOStatement $jsonsStmt */
		$jsonsStmt = $pdo->prepare("SELECT * FROM piwik_apidata INNER JOIN (SELECT DISTINCT hostname, tracking_id, blog_ID, hub_ID, touchpoint_type FROM touchpoints_info) j ON j.tracking_id = piwik_apidata.tracking_ID WHERE piwik_apidata.ID IN ($ids)");
		info("Getting apidata for $index - " . ($limit + $index));
		$jsonsStmt->execute();
		$data = $jsonsStmt->fetchAll(PDO::FETCH_OBJ);
		/** @var QubeLogPDOStatement $updateStmt */
		$updateStmt = $pdo->prepare("UPDATE $piwikreport_table SET bounce_count = :bounce_count WHERE tbl = :tbl AND tbl_ID = :tbl_id AND point_date = :point_date");
		$updateStmt->bindParam('bounce_count', $bounce_count);
		$updateStmt->bindParam('tbl', $tbl);
		$updateStmt->bindParam('tbl_id', $tbl_id);
		$updateStmt->bindParam('point_date', $point_date);
		foreach ($data as $d) {
			$json = json_decode($d->json);
			if (is_array($json) && empty($json)) $json = (object)array('bounce_count' => 0);
			$bounce_count = $json->bounce_count;
			$tbl = $d->hub_ID ? 'hub' : 'blog';
			$tbl_id = $tbl == "hub" ? $d->hub_ID : $d->blog_ID;
			$point_date = $d->datestr;
			if($tbl_id == 0){
				info("tbl_id with value 0 detected...");
				print_r($d);
			}else{
				info("Update $piwikreport_table $tbl $tbl_id");
				$updateStmt->execute();
			}
		}
		$index += $limit;
		$ids_stmt->execute();
	}

}

info("Script execution finished");