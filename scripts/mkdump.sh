#!/bin/bash

datestr=`date +'%m.%d-%Y'`
echo Dumping sapphire_6qube To: $datestr

mysqldump --routines --no-create-info --no-data --no-create-db --skip-opt sapphire_6qube > $datestr.routines.sql
mysqldump --no-data --skip-opt --create-options sapphire_6qube > $datestr.create_info.sql

#mysqldump --no-create-info --skip-opt --quick --max_allowed_packet=50m --net_buffer_length=16m sapphire_6qube > data.sql

# add locks (accelerates dump inserts)
# lock-tables (for consistent state)
# master-data (locks all databases, sorry)
# extended-insert (multiple rows per insert cmd)
# disable-keys (speed up inserts)

mysqldump --no-create-db --no-create-info \
	--extended-insert --add-locks \
	--quick --disable-keys --lock-tables \
	--master-data \
	sapphire_6qube > $datestr.data.sql

