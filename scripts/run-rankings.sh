#!/bin/bash

cd "$(dirname "$0")"

datestr=`date +%m%d`
todaystr=`date +%Y-%m-%d`
logf=log/ranking-$datestr
machine=6qube1
sendtomail=system_events@sofus.mx

# crawl search engines
./rankings/crawl $todaystr 6qube1 ./cache_crawler >$logf.crawl.log

# parse search results
./rankings/parse $machine $todaystr ./cache_crawler >>$logf.parse.log

# analyze results
./rankings/analyze $todaystr $machine >>$logf.analyze.log

