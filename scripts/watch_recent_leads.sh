#!/usr/bin/env bash


limit=$2
if [ -z $2 ]
then
    limit=10
fi

while [ : ]
do
    mysql -D sapphire_6qube -e "SELECT id, user_id, lead_form_id, hub_id, lead_name, lead_email, created FROM leads ORDER BY created DESC LIMIT $limit"
    sleep $1
done
