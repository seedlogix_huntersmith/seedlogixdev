<?php
$userspath	=	QUBEROOT . 'users';

chdir(realpath($userspath));

$dirh	=	opendir('.');

	$count	=	1000;
	$s	=	0;
	$ids	=	array();
    /* This is the correct way to loop over the directory. */
    while (false !== ($userid = readdir($dirh))) {
	$olddir	=	$userspath . '/' . $userid;

	if(!preg_match('/^[0-9]+$/', $userid) || is_link($olddir)) continue;
	$ids[]	=	(int)$userid;
	}
closedir($dirh);

asort($ids);

echo 'total ids = ', count($ids), "\n";
sleep(10);

foreach($ids as $userid){
	$olddir	=	$userspath . '/' . $userid;
	echo "$userid ..\n";
	
	$container	=	'u' . floor($userid/1000);
	$containerpath	=	$userspath . '/' . $container;
	$newdir		=	$containerpath . '/' . $userid;

	if(!is_dir($containerpath)) mkdir($containerpath, 0777) || die('Failed to make container: ' . $containerpath);
	
	if(is_dir($newdir)){
		echo '>> ' . $userid . ' / ', $newdir . ' already exists!', "\n";
		continue;
	}
	rename($olddir, $newdir) || die('Failed to move ' . $userid);
	symlink($container . '/' . $userid, $olddir) || die('failed to create symlink for ' . $userid);

	file_put_contents($newdir . '/transitioncheck.txt', $userid);
	usleep(5*1000);	// one ms
	$checkurl	=	'http://pastephp.com/users/' . $userid . '/transitioncheck.txt';
	if(file_get_contents($checkurl) != $userid){
		echo 'Failed to check userid: ', $userid, "\n$checkurl\n";
		exit;
	}
	unlink($newdir . '/transitioncheck.txt');

	echo 'Check passed..', "\n";
	if($s++%$count == 0){
		echo 'sleeping 5.. ';
		sleep(5);
	}
    }


