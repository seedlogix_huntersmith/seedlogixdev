<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 11/3/14
 * Time: 1:55 PM
 */




$table = 'piwikreports_se';
//$IDs = array(
//	'50520' => 'hub',
//	'50518' => 'hub',
//	'50517' => 'hub',
//	'50516' => 'hub',
//	'1147' => 'blog'
//);
$searchengines = array(
	"'Google'",
	"'Yahoo!'",
	"'DuckDuckGo'",
	"'Google Images'",
	"'AOL'",
	"'Bing'",
	"'Comcast'",
	"'Ask'",
	"'Baidu'",
	"'InfoSpace'",
	"'Sogou'",
	"'Rakuten'",
	"'Zapmeta'",
	"'Yandex'",
	"'Earthlink'",
	"'Google Custom Search'",
	"'MyWebSearch'",
	"'blekko'",
	"'Mailru'",
	"'Google SSL'",
	"'Bing Images'",
	"'Charter'",
	"'360search'",
	"'Google Maps'",
	"'Inbox'",
	"'Daum'",
	"'Yahoo! Images'",
	"'Yahoo! Japan'",
	"'K9 Safe Search'",
	"'Babylon'",
	"'Seznam'",
	"'Surf Canyon'"
);
//$interval = 'P1D';
$dayLimit = clone $start;
$dayLimit->sub(new DateInterval($interval));
foreach($IDs as $id => $type){
	$date = new DateTime();

	$nSearchEngines = rand(2, 10);
	$searchEnginesIndexes = array();
	for($i = 0; $i <$nSearchEngines; $i++){
		do{
			$index = rand(0, count($searchengines) - 1);
		}while(in_array($index, $searchEnginesIndexes));
		$searchEnginesIndexes[] = $index;
	}

	for($d = $date; $d >= $dayLimit; $date->sub(new DateInterval('P1D'))){
		foreach($searchEnginesIndexes as $index){
			$visits = rand(0, 100);
			$leads = rand(0, $visits);
			$searchEngine = $searchengines[$index];
			echo "INSERT INTO $table VALUES('$type', $id, $userID, $cid, 'DAY', '{$date->format('Y-m-d H:i:s')}', $searchEngine ,$visits, $leads, null);\n";
		}
	}
}
