#!/bin/bash

scriptsDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd /home/sapphire/data/

if [ $# -eq 0 ]; then
	echo "Provide server hostname as argument: $0 6qube[1,2,3]"
	exit
fi

server=$1
userids=`php -f "$scriptsDIR/last_users_that_uploaded.php" -- --nobanner --hostname 6qube1`

#echo $userids; exit;
mysync()
{
	server=$1
	localp=$2

	rsync -e ssh -avz --no-o --no-g "./$localp/" "sapphire@$server:data/$localp/"
}

if [ "$userids" != "" ]; then
	echo "Found IDs: $userids"
for userpath in $userids
do
	mysync "$server" "6qube-users/$userpath"
	retcode=$?
	if [ $retcode > 0 ]; then
		mainuserdir=`echo "$userpath" | sed -ne 's|\(u[0-9]\+\).*|\1|p'`
		mysync "$server" "6qube-users/$mainuserdir"
	fi
done else
	echo "No recent uploads."
fi

echo "Syncing other dires to: $server"

for dirname in `cat ./.rsync-dirs`
do
	echo Syncing $dirname
	mysync "$server" "$dirname"
done

rsync -e ssh -avz ./.rsync-dirs "sapphire@$server:data/"

touch $0
