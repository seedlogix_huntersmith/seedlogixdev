<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/23/15
 * Time: 10:38 AM
 */

if($argc <= 1){
	echo "Syntax:\n\t[User ID] [Campaign ID] [-h hub1 hub2 ... hubn] [-b blog1 blog2 ... blogn] [-i period]\n";
	echo "\t-h List of hub IDs\n";
	echo "\t-b List of blog IDs\n";
	echo "\t-i A period with the following format [number][period type] example 6M for six months\n";
	echo "\t[period type]\t D = day M = month Y = year\n";
	exit;
}

function contains($string, $search){
	return strpos($string, $search) !== false;
}

if(is_numeric($argv[1]))
	$userID = $argv[1];
else{
	echo "User ID required\n";
	exit;
}

if(is_numeric($argv[2]))
	$cid = $argv[2];
else{
	echo "Campaign ID required\n";
	exit;
}
$indexHubs = array_search('-h', $argv);
$IDs = array();
if($indexHubs){
	while(++$indexHubs < count($argv) && !contains($argv[$indexHubs], '-'))
		$IDs[$argv[$indexHubs]] = 'hub';
}
$indexBlogs = array_search('-b', $argv);
if($indexBlogs){
	while(++$indexBlogs < count($argv) && !contains($argv[$indexBlogs], '-'))
		$IDs[$argv[$indexBlogs]] = 'blog';

}

$indexInterval = array_search('-i', $argv);

if($indexInterval){
	$interval = "P{$argv[$indexInterval + 1]}";
}else{
	echo "You must specify an interval with -i\n";
	exit;
}

$indexStart = array_search('-s', $argv);
if($indexStart){
	$start = new DateTime($argv[$indexStart] + 1);
}else{
	$start = new DateTime();
}

require 'piwikreports_refGenerator.php';
require 'piwikreports_socialGenerator.php';
require 'piwikreportsGenerator_se.php';

foreach($IDs as $tbl_id => $tbl){
	echo "INSERT INTO piwikreports SELECT tbl, tbl_id, user_ID, cid, point_type, point_date, FLOOR(RAND() * ((sum(nb_visits) + 50) - (sum(nb_visits) + 20)+ 1)) + (sum(nb_visits) + 20),
FLOOR(RAND() * ((sum(nb_leads) + 20) - (sum(nb_leads))+ 1)) + (sum(nb_leads)), null FROM
(
SELECT tbl, tbl_id, user_ID, cid, point_type, point_date, sum(nb_visits) nb_visits, sum(nb_leads) nb_leads FROM piwikreports_se WHERE tbl_id = $tbl_id AND tbl = '$tbl' GROUP BY tbl, tbl_id, user_ID, cid, point_type, point_date
	UNION all
SELECT tbl, tbl_id, user_ID, cid, point_type, point_date, sum(nb_visits) nb_visits, sum(nb_leads) nb_leads FROM piwikreports_ref WHERE tbl_id = $tbl_id AND tbl = '$tbl' GROUP BY tbl, tbl_id, user_ID, cid, point_type, point_date
	UNION all
SELECT tbl, tbl_id, user_ID, cid, point_type, point_date, sum(nb_visits) nb_visits, sum(nb_leads) nb_leads FROM piwikreports_social WHERE tbl_id = $tbl_id AND tbl = '$tbl' GROUP BY tbl, tbl_id, user_ID, cid, point_type, point_date) pi
GROUP BY tbl, tbl_id, user_ID, cid, point_type, point_date;\n";
}
