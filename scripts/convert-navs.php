<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


chdir(dirname(__FILE__));
require_once '../hybrid/bootstrap.php';
    
Qube::ForceMaster();

echo 'Fetching Navs..', "\n";

$pdo    =   Qube::GetPDO();
$q  = new DBSelectQuery($pdo);
$q->From('hub_page2 p')->Where('type = "nav" AND nav_parent_id = ?');
$q->addFields('exists (select id FROM hub_page2 WHERE type="nav" AND nav_parent_id = p.id) as has_children');

$pStmt   =   $q->Prepare();

function process_nav($nav_node, $level, $parent_ID){
    global $pStmt, $pdo;
    echo str_repeat("-", $level) ."$nav_node->id, (parent=$nav_node->nav_parent_id) $nav_node->page_title has_children = $nav_node->has_children \n";
    // create node
    $node_ID    =   0;
    if($level > 0){
        $NavNode    =   new NavModel;
//        $NavNode->loadArray();
        $page_ID    =   0;
        if($nav_node->default_page){
            $pdo->query('INSERT IGNORE INTO 6q_hubpages_tbl
SELECT NULL, id, user_id, hub_id, nav_parent_id, nav_order*10,
	page_parent_id, feedobject_id, 
	page_title, page_title_url, page_region, page_keywords, 
	page_seo_title, page_seo_desc, page_photo, page_photo_desc, 
	page_edit_2, page_edit_3, inc_contact, inc_nav, page_full_width, 
	page_adv_css, allow_delete, has_custom_form, has_tags, multi_user, 
	multi_user_fields, default_page, outbound_url, outbound_target, trashed, 
	last_edit, created
FROM hub_page2 WHERE id = ' . $nav_node->default_page);
            $page_ID    =   $pdo->lastInsertId();
        }
        Qube::Start()->Save($NavNode, 
                array('name' => $nav_node->page_title, 
		'name_url' => $nav_node->page_title_url, 
                'user_ID' => $nav_node->user_id, 
		'hub_ID' => $nav_node->hub_id, 'parent_ID' => $parent_ID,
                    'page_ID'   =>  $page_ID, 'sortk' => $nav_node->nav_order*10
                ));
        $node_ID    =   $NavNode->ID;
    }
    if(!$nav_node->has_children) return;
    
    $pStmt->execute(array($nav_node->id));
#    $cStmt  =   clone($pStmt);
    $allnodes   =   $pStmt->fetchAll(PDO::FETCH_OBJ);
    foreach($allnodes as $node)
#    while($node = $pStmt->fetchObject())
        process_nav($node, $level+1, $node_ID);
    
//    sleep(2);
}

$origin =   (object)array('page_title' => '__ORIGIN__', 'has_children' => 1, 'id' => 0,
            'nav_parent_id' => 0);

//$pdo->query('CREATE TEMPORARY TABLE newpages_oldpages ( hb2id int(11) NOT NULL, hp_tblid int(11) NOT NULL, primary key (hb2id) ) ');

// import nav structures
process_nav($origin, 0, 0);

echo "Creating Temporary Table temppages.\n";
// import pages that are not a default nav-page (leafs)
$pdo->query('insert ignore into 6q_hubpages_tbl
SELECT NULL, id, user_id, hub_id, page_parent_id, feedobject_id, page_title, page_title_url, 
    page_region, page_keywords, page_seo_title, page_seo_desc, page_photo, page_photo_desc, 
    page_edit_2, page_edit_3, inc_contact, inc_nav, page_full_width, page_adv_css, allow_delete, 
    has_custom_form, has_tags, multi_user, multi_user_fields, default_page, outbound_url, 
    outbound_target, trashed, last_edit, created
    FROM hub_page2 t WHERE t.type = "page" AND NOT EXISTs (select id from 6q_hubpages_tbl where oldtbl_ID = t.id)
    ');


echo "done importing all types of shit!.";
