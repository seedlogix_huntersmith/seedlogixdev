<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 11/4/14
 * Time: 1:59 PM
 */


$table = 'piwikreports_social';
//$IDs = array(
//	'50520' => 'hub',
//	'50518' => 'hub',
//	'50517' => 'hub',
//	'50516' => 'hub',
//	'1147' => 'blog'
//);
$networks = array(
	"'Facebook'",
	"'Pinterest'",
	"'Twitter'",
	"'LinkedIn'",
	"'Google+'",
	"'YouTube'",
	"'reddit'",
);

//$interval = 'P10Y';
$dayLimit = clone $start;
$dayLimit->sub(new DateInterval($interval));
foreach($IDs as $id => $type){
	$date = new DateTime();

	$nNetworks = rand(2, 7);
	$networksIndexes = array();
	for($i = 0; $i <$nNetworks; $i++){
		do{
			$index = rand(0, count($networks) - 1);
		}while(in_array($index, $networksIndexes));
		$networksIndexes[] = $index;
	}

	for($d = $date; $d >= $dayLimit; $date->sub(new DateInterval('P1D'))){
		foreach($networksIndexes as $index){
			$visits = rand(0, 100);
			$leads = rand(0, $visits);
			$network = $networks[$index];
			echo "INSERT INTO $table VALUES('$type', $id, $userID, $cid, 'DAY', '{$date->format('Y-m-d H:i:s')}', $network ,$visits, $leads, null);\n";
		}
	}
}
