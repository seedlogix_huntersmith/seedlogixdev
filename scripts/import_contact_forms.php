<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2/9/15
 * Time: 2:56 PM
 */
define('QUBE_NOLAUNCH', 1);
require_once '../hybrid/bootstrap.php';
                // ------    VARIABLE DEFINITIONS    ------- //

                $STRESS_SAFE = false;


//insert into orders (product_id, qty)
//select 2, 20
////where (SELECT qty_on_hand FROM products WHERE id = 2) > 20;


                // ------ FIRST PHASE ------- //

// - Initialize.
        INITIALIZE:
        echo "Initializing...\n";
// - Start DataAccess
        $DataAccess = new ProspectDataAccess();
// - Count Contacts to be imported
        echo "Searching Table..\nAmount of Contacts to Pull: ";
        if(!$contacts_amount = $DataAccess->query("SELECT count(*) FROM contact_form WHERE type_id IN (SELECT hub_id from 6q_touchpoints) and id NOT IN (SELECT contactform_ID FROM prospects) AND type = 'hub' AND type_ID !=0")->fetchColumn(0)){
                exit('Opps... something went wrong. (Or maybe there are no contacts to import n.n)');
        };
        echo $contacts_amount . "\n";
// - Start Pulling contacts
        echo "Starting Migration, MODE:" . ($STRESS_SAFE ? "SAFE" : "FAST");
        // Define Wait Time (If STRESS SAFE is true)
        if($STRESS_SAFE) {$wait_time = 1;}else{$wait_time = 0;}
        $processed_contacts = 0;
        echo "\nProgress:";
        while($processed_contacts < $contacts_amount){
                $contact_list = $DataAccess->query("SELECT * FROM contact_form WHERE type_id IN (SELECT hub_id from 6q_touchpoints) AND id NOT IN (SELECT contactform_ID FROM prospects) AND type = 'hub' AND type_ID !=0 LIMIT $processed_contacts,500")->fetchAll();

                $looped = false;
                foreach($contact_list as $contact){
                       // -- Insert Into Prospects
                        $looped = true;
                        if(!$DataAccess->query("insert into prospects (touchpoint_ID, ip, user_ID, user_parent_ID, cid,
    page_ID, form_ID, searchengine, keywords,
    visitor_ID, lead_ID, contactform_ID, name, email, phone, created, spampoints)
  select COALESCE(tp.id, 0), c.ip, c.user_id, c.parent_id, c.cid,
    0, 0, c.search_engine, c.keyword,
    '', 0, c.id, c.name, c.email, c.phone, c.created, c.spamstatus*100
    from contact_form c left join 6q_touchpoints tp on (c.type_id = tp.hub_id)
    where c.type = 'hub' AND c.ID = " . (int)$contact['id'])){
                            $error_count++;
                            echo "x";
                        }else{
                                $processed_contacts++;
                                echo ".";
                        }
                    sleep($wait_time);
                }
            if(!$looped){
                echo "\n-CheckPoint-\n";
                goto INITIALIZE;
            }
                echo "\n" . $processed_contacts . " of " . $contacts_amount . " Contacts processed.";
        }
        echo "\nFinished Script.";

