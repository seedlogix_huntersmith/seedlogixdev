<?php
	// 	 ## LEGEND ######################
	//   mi_id = user id
	//	 mi_397 = business name
	//	 mi_401 = business category
	//	 mi_398 = pointer to logo file
	//	 mi_399 = business description
	
	include './config/text_functions.php';
	include './config/browse_functions.php';
	include './config/open_db.php';
	include './config/sessions.php';
	
	$descLimit = 160; //set the letter limit for the description
	$newAdditionsLimit = 5; //set the number of new additions to display
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$cityState = displayIP2City($ip);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>About 6Qube | Free Business Listing | Free Business Website | Free Business Tools </title>
<meta content="6Qube is the culmination of Six Powerful Tools that benefit every business. By Claiming your FREE My6QubeSpot Business Listing, your business will receive a much greater online presence resulting in the ability to attract more customers." name="description" />
<meta content="Sign up for 6Qube Directory, Free Business Directory, Free Business Listing, Free Business Advertising, Free Website, Free Lead Capture Page, Free Autorepsonder, Business Listings for Free" name="keywords" />
<meta content="6Qube" name="author" />
<meta content="admin@6qube.com" name="email" />
<meta content="INDEX,FOLLOW" name="Robots" />
<meta content="7 Days" name="Revisit-after" />
<meta content="never" name="expires" />
<style type="text/css" media="screen">
<!--
 @import url("css/style.css");
.style24 {	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 16px;
	color: #333333;
}
#elevator-content-text-area {margin-left:100px;
width:600px;  
}
.style25 {
	color: #0099FF;
	font-size: 22px;
}
-->
</style>
</head>
<body>
<div id="wrapper">
  <div id="header">
    <div id="header_form"></div>
    <div class="call-back">
      <div class="style4"><span class="style5">Start Your </span><br />
        <span class="style7">Business Search Now... </span></div>
    </div>
    <div id="nav-menu">
     <?php
	  	include './includes/nav.inc.php';
	  ?>
      <div id="search-text"> <span class="style3">Search for a business below or use <a href="browse.php?id=<?=$_SESSION['plex_id']?>">browse category </a></span></div>
    </div>
  </div>
  <div id="main-content">
    <div id="left_col">
      <div class="style24" id="elevator-content-text-area">
        <p class="style25">About 6Qube Directory</p>
        <p>&nbsp;</p>
        <p>6Qube Directory was created specifically to show you all  sides of the businesses you want.&nbsp;  Business have the ability to update their information instantly on our platform  which allows you to have access to the most current information about that  business.&nbsp; All of our businesses have  come to 6Qube to create their spot and none of our data has been imported to  have the appearance of a full directory.&nbsp;  That makes our directory the most up to date and accurate data about  these businesses. </p>
        <p><br />
        Please check back daily as many businesses in your local  community are creating their spots every day.</p>
        <br />
      </div>
    </div>
    <div id="right_col">
      <div id="search-contact-bussiness">
        <div id="search-form" >
          <form method="get" action="index.php">
            <label for="searchPhrase"><span class="style3">Business or Category</span></label>
            <input type="text" name="business" value="<?php echo $_REQUEST['business'] ?>" size="28" />
            <label for="location"><br />
            <br />
            <span class="style3">Location</span><br />
            </label>
            <input type="text" name="location" value="<?php if($_REQUEST['location']) echo $_REQUEST['location']; else echo $cityState['city'] .', '. $cityState['region']; ?>" size="28" />
            <br />
            <br />
			<input name="id" type="hidden" value="<?=$_SESSION['plex_id']?>" />
            <input type="image" name="submit" src="images/search_button.jpg" alt="Submit">
          </form>
          <div align="left" style="padding-left:10px; " ><span class="style11"><a href="advanced_search.php?id=<?=$_SESSION['plex_id']?>">Advanced Search</a> | <a href="browse.php?id=<?=$_SESSION['plex_id']?>">Browse Now</a></span> </div>
        </div>
      </div>
    </div>
    <br style="clear:both;" />
    <br />
  </div>
  <div id="footer"> </div>
</div>
</body>
</html>