<?
	require_once('admin/inc/local.class.php');
	$local = new Local();
	
	// ** Header ** //
	require_once('includes/header.inc.php');
?>
    <h1>Sign Up</h1>
    <p>All fields are required to register a new user</p>
<?
	
	if($_POST){ //if the form is submitted
		//validate all the form fields
		$user_id = $local->validateUserId($_POST['email'], 'User ID Email');
		$password = $local->validatePassword($_POST['password'], 'Password', 3, 12);
		$firstName = $local->validateText($_POST['first_name'], 'First Name');
		$lastName = $local->validateText($_POST['last_name'], 'Last Name');
		$phone = $local->validateNumber($_POST['phone'], 'Phone');
		$company = $local->validateText($_POST['company'], 'Company Name');
		$address = $local->validateText($_POST['address'], 'Address');
		$city = $local->validateText($_POST['city'], 'City');
		$state = $local->validateText($_POST['state'], 'State');
		$zip = $local->validateNumber($_POST['zip'], 'Zip Code');
		
		if($local->foundErrors()){ //check to see if there were any errors
			 $errors = $local->listErrors('<br />'); //if so then store the errors
		}
		else{
			$query = "INSERT INTO users (username, password, access) VALUES ('$user_id', '$password', '0')";
			$result = $local->query($query);
			if($insert_id = mysqli_insert_id()){
				$query = "INSERT INTO user_info (user_id, firstname, lastname, phone, company, address, city, state, zip) VALUES ('$insert_id', '$firstName', '$lastName', '$phone', '$company', '$address', '$city', '$state', '$zip')";
				if($result = $local->query($query)){ //if the query was successful (New user added)
					$local->sendRegisterMail($insert_id); //Send the user a registration email
				}
			}
			else{
				echo 'something weird happened please try again?';
			}
		}
	}
	else{ //if form has not been posted show form
		include('admin/inc/forms/signUp.php');
	}
	
	//If errors - show them
	if($errors){
		echo '<div id="error">'.$errors.'</div>';
		include('admin/inc/forms/signUp.php');
	}
?>			
	<br /><br />
<?	
	// ** Footer ** //
	require_once('includes/footer.inc.php');
?>