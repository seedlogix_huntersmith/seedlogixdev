<?php
if($_GET['id']){
	require_once(QUBEADMIN . 'inc/press.class.php');
	$press = new Press();
	$press_id = $_GET['id'];
}

$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
$resellerCompany = $siteInfo['company'];

$result = $press->getPresss(NULL, NULL, $press_id); //primes press release results, and is needed for getPressRows()
if($press->getPressRows()) { //checks to see if press release exists
	$row = $press->fetchArray($result);
	//Number of items displayed on right bar
	$rightLimit = 4;
	//URL to user's directory images
	$userImgUrl = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/press_release/';
	//Default link
	$dfltLink = $row['website'] ? $row['website'] : '#';
	//Page title
	$title = $row['headline'].' | '.$resellerCompany.' Local Press Release';
	//Meta description
	$meta_desc = $row['summary'];
		
	if($row['thumb_id'] == "") {
		$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	} else {
		$icon = $userImgUrl.$row['thumb_id'];
	}
	
	$keywords = explode(',', $row['keywords']);
	$created = date("F jS, Y", strtotime($row['created']));
	$createdMo = date("M", strtotime($row['created']));
	$createdDay = date("j", strtotime($row['created']));
	$a = explode('->', $row['category']);
	$category = $a[1];
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>

			<!-- Start Inner Page Container -->
			<div class="inner-container">
				<div id="innerpage-title">
					 <? if($siteInfo['remove_search']){ ?>
                <h1><?=$created?></h1>
				<? } else { ?>
					<h1><?=$created?></h1>
					 <? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
                <?php } ?>
				</div>
                
                
                <br class="clear" />
                <br class="clear" />
				</div>
				
				<div class="two-third">
				<?=$siteInfo['edit_region_1']?>
					<!-- Breadcrumb-->
					<div id="breadcrumb">
						You Are Here: <a title="Local Press Release Directory by <?=$resellerCompany?>" href="/">Press</a> | <?=$press->getCityLink($row['city'], $row['state'], $site, NULL, true, $row['city'].' press releases | '.$resellerCompany.' Local Press Release Directory')?> | <?=$press->getCatLink($row['city'], $row['state'], $category, $site, $row['author'].' | '.$keywords[0].' | '.$keywords[1])?> | <?=$row['headline']?>
					</div>
							
					<!-- Start Listing Content -->
					<div class="blog-post">
						<div class="post-date">
							<span class="post-month"><?=$createdMo?></span><br />
							<span class="post-day"><?=$createdDay?></span>
						</div>
						<div class="post-title">
							<h3><a href="#"><?=$row['headline']?></a></h3>
						</div>
						<br style="clear:both;" />
					</div>
					
					<div class="testimonials2">
						<p><?=$row['summary']?></p>
						<p style="text-align:right;">- <?=$row['author']?>, <?=$created?></p>
					</div>
					
					<div class="blog-post">
						<h3>For Immediate Release...<?=$created?></h3>
						<!-- Image -->
						<div class="blog3-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['author']?>"><img src="<?=$icon?>" width="165px" alt="" /></a>
							</div>
						</div>
						
						<?=$row['body']?>
						
						<p>
							<strong>Tags:</strong> 
							<a title="<?=$keywords[0]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[0]?></a> | 
						<? if($keywords[1]){ ?>
							<a title="<?=$keywords[1]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[1]?></a> | 
						<? } ?>
						<? if($keywords[2]){ ?>
							<a title="<?=$keywords[2]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[2]?></a> | 
						<? } ?>
						<? if($keywords[3]){ ?>
							<a title="<?=$keywords[3]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[3]?></a> | 
						<? } ?>
						<? if($keywords[4]){ ?>
							<a title="<?=$keywords[4]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[4]?></a> | 
						<? } ?>
						<? if($keywords[5]){ ?>
							<a title="<?=$keywords[5]?>" href="<?=$dfltLink?>" class="tags_link"><?=$keywords[5]?></a>
						<? } ?>
						</p>
						<br class="clear" />
					</div>
					
					<div class="blog-post">
						<div class="dirIcon">
							<img src="<?=$themeInfo['themeUrl2']?>images/backgrounds/home.png" alt="" />
						</div>
						<div class="post-title">
							<h3>About The Author</h3>
							<br class="clear" />
						</div>
						<br />
						<p><?=$row['contact']?></p>
						
						<h3>Contact <?=$row['author']?></h3>
						<!-- Contact Form-->
						<div id="contact-form">
							<div id="contactdiv" style="clear:both;">
							<div id="msg"></div>
							<form method="post" action="http://<?=$siteInfo['reseller_main_site']?>/process-form.php">
								<label>Name:*</label>
								<input name="name" value="" type="text" class="input1 rounded3" />
								<label>Email:*</label>
								<input name="email" value="" type="text" class="input1 rounded3" />
								<label>Phone:*</label>
								<input name="phone" value="" type="text" class="input1 rounded3" />
								<label>Message:</label>
								<textarea name="comments" class="input2 rounded3"></textarea>
								<br style="clear:both;" />
								<!-- HIDDEN VARIABLES -->
								<input name="user_id" value="<?=$row['user_id']?>" type="hidden" />
								<input name="domain" value="<?='http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" type="hidden" />
								<input name="type" value="press" type="hidden" />
								<input name="type_id" value="<?=$row['id']?>" type="hidden" />
								<input name="lead_source_id" value="<?=$row['author']?>" type="hidden" />
								<p class="left">
									<button name="submit" type="submit" class="blackButton3"><span>SUBMIT</span></button>
								</p>
							</form>
							</div>
						</div>	
						<!-- End Contact Form -->
						<br class="clear" />
					</div>
					<p style="clear:left;"><?=$siteInfo['edit_region_4']?></p>
				</div><!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_2']?>
							</div>
							<? if($siteInfo['parent'] == "0"){ ?>
							<!-- Press -->
								<h2>Latest <?=$press->getCatLink($row['city'], $row['state'], $category, $site, $row['author'].' | '.$keywords[0].' | '.$keywords[1])?> Press Releases in <?=$press->getCityLink($row['city'], $row['state'], $site, NULL, true, $row['city'].' press releases | '.$resellerCompany.' Local Press Release Directory')?></h2>
								<ul>
								<?php
								if($results = $press->getPressForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																	$row['user_id'], $row['cid'])){
									$presslist = $results['results'];
									while($press_row = $press->fetchArray($presslist)){
										$a = explode("->", $press_row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
												$press->convertKeywordUrl($pressCategory).
												$press->convertKeywordUrl($press_row['headline']).$press_row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title="<?=$press_row['headline']?>"><?=$press_row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't published any press releases.";
								?>
								</ul>
								<!-- End Articles -->
                            <? } else { ?>
								<!-- Hubs -->
								<h2>Our HUBs</h2>
								<div class="sidebar-advertise">
								<?php
								if($results = $press->getHubsForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($hub_row = $press->fetchArray($hubs)){
										if(!$hub_row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$hub_row['user_id'].'/hub/'.$hub_row['logo'];
								?>
										<li><a href="<?=$hub_row['domain']?>" title="<?=$hub_row['company_name']?> | <?=$hub_row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo "This user hasn't created any hubs.";
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Local -->
								<h2>Our Local Listings</h2>
								<ul>
								<?php
								if($results = $press->getListingsForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																	$row['user_id'], $row['cid'])){
									$listings = $results['results'];
									while($dir_row = $press->fetchArray($listings)){
										$a = explode("->", $dir_row['category']);
										$listingCategory = $a[1];
										$listingUrl = 'http://local.'.$domain.'/'.
												$press->convertKeywordUrl($dir_row['keyword_one']).
												$press->convertKeywordUrl($dir_row['company_name']).$dir_row['id'].'/';
								?>
									<li><a href="<?=$listingUrl?>" title=""><?=$dir_row['company_name']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't created any local business listings.";
								?>
								</ul>
								<!-- End Press -->
								
								<!-- Blog Posts -->
								<h2>Our Blog Posts</h2>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $press->getPostsForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$posts = $results['results'];
									while($post_row = $press->fetchArray($posts)){
										$postUrl = $row['domain']."/".$press->convertKeywordUrl($post_row['category']).
					 							$press->convertPressUrl($post_row['post_title'], $post_row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$post_row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't written any blog posts.";
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->							
								
								<!-- Articles -->
								<h2>Our Articles</h2>
								<ul>
								<?php
								if($results = $press->getArticlesForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																	$row['user_id'], $row['cid'])){
									$articles = $results['results'];
									while($art_row = $press->fetchArray($articles)){
										$a = explode("->", $art_row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$press->convertKeywordUrl($articleCategory).
													$press->convertKeywordUrl($art_row['headline']).$art_row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title="<?=$art_row['headline']?>"><?=$art_row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't written any articles.";
								?>
								</ul>
								<!-- End Articles -->
								<? } ?>
								<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_3']?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div> <!-- End One Third -->
				<div class="clear"></div>
			</div><!--End Inner Page Container-->
		</div><!-- End Main Container -->
	<?
		//COMMON FOOTER
		$customTracking = $row['tracking_id'];
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<?
} else { //if not found
	echo 'This Press Release Not Found.';
}
?>