<?php
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$dispPress = true;
	$continue = true;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	}
	
	//title and meta description -- gets changed if viewing city or category
	if($siteInfo['seo_title']){
		$title = $siteInfo['seo_title'];
	}
	else $title = "".$resellerCompany." Press Release Dirrectory | ".$resellerCompany." Press Releases";
	if($siteInfo['seo_desc']){
		$meta_desc = $siteInfo['seo_desc'];
	}
	else $meta_desc = $resellerCompany." Press Release Directory of Local Press Releases. View current press releases from local businesses.";
	
	require_once(QUBEADMIN . 'inc/press.class.php');
	$press = new Press();

	if($siteInfo['city_focus']) $city = $siteInfo['city_focus'];
	if($siteInfo['state_focus']) $state = $siteInfo['state_focus'];
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);
		
		if($args_array[0]=="cities"){
			//List of cities
			//echo "cities";
			$dispPress = false;
			$dispCities = true;
			$title = "Browse Local Local Press Releases by City | Local Articles";
			$meta_desc = "Find Local Business Press Releases by browsing ".$resellerCompany." Local Press Display of local press releases.";
		}
		else if(is_numeric($args_array[0])){
			//Paginated all listings
			//echo "all listings - page ".$args_array[0];
			$dispCities = true;
			$currPage = $args_array[0];
			$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			$title = "Local Press Release Directory by ".$resellerCompany." Press | Page ".$currPage;
			$meta_desc .= " Page ".$currPage." of results.";
		}
		else if($city = $press->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $press->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $press->majorCity($city);
				$majorCity = true;
			}
			$cityLink = $press->getCityLink($city, $state, $site, NULL, true);			
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//Paginated listings for a city
					//echo "$city." listings, page #".$args_array[1];
					$dispCats = true;
					$currPage = $args_array[1];
					$mainOffset = ($mainLimit*$currPage)-$mainLimit;
					$title = $city.', '.$state.' Local Press Release Directory by '.$resellerCompany.' Press | Page '.$currPage;
					$meta_desc = 'Find '.$city.', '.$state.' local business Press Releases in '.$city.' '.$press->getStateFromAbv($state).'. '.$resellerCompany.' Press '.$city.' '.$state.' local press directory. Page '.$currPage.' of results.';
				}
				else if($args_array[1]=="categories"){
					//List of categories for a city
					//echo $city." Categories";
					$dispPress = false;
					$dispCities = false;
					$dispCats = true;
					$title = $city.', '.$state.' Local Business Press Release Categories | All Business Press Categories';
					$meta_desc = 'Find Local Business Press Releases in '.$city.', '.$state.' by browsing '.$resellerCompany.' Press All Categories Display of '.$city.' '.$press->getStateFromAbv($state).' Local Press Releases.';
				}
				else if($category = $press->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							if($args_array[2]<10){
								//Paginated listings for a certain category in a city
								//echo $city." ".$category."s, page ".$args_array[2];
								$currPage = $args_array[2];
								$mainOffset = ($mainLimit*$currPage)-$mainLimit;
								$title = $city.' '.$state.' '.$category.' Press Releases | '.$category.' Press in '.$city.' | Page '.$currPage;
								$meta_desc = $city.' '.$state.' Press Release Directory of '.$category.' by '.$resellerCompany.' Press.  Discover local '.$category.' Press Releases in '.$city.' '.$state.'. Page '.$currPage.' of results.';
							}
							else {
								$press_id = $args_array[2];
								$continue = 0;
								include('press.php');
							}
						}
					}
					else {
						//All listings for a certain category in a city
						//echo $city." ".$category."s";\
						if($siteInfo['citycat_seo_title']){
						$title = $siteInfo['citycat_seo_title'];
						}	
						else $title = $city.' '.$state.' '.$category.' Press Releases | '.$category.' Press in '.$city;
						
						if($siteInfo['citycat_seo_desc']){
						$meta_desc = $siteInfo['citycat_seo_desc'];
						}	
						else $meta_desc = $city.' '.$state.' Press Release Directory of '.$category.' by '.$resellerCompany.' Press.  Discover local '.$category.' Press Releases in '.$city.' '.$state.'.';

					}
				}
			} //end if($args_array[1])
			else {
				//All listings for a city
				//echo "All ".$city." listings";
				$dispCities = false;
				$dispCats = true;
				
				if($siteInfo['city_seo_title']){
				$title = $siteInfo['city_seo_title'];
				}	
				else $title = ''.$resellerCompany.' '.$city.', '.$state.' Press Releases Directory';
				
				if($siteInfo['city_seo_desc']){
				$meta_desc = $siteInfo['city_seo_desc'];
				}	
				else $meta_desc = 'Find '.$city.' '.$state.' Local Press Releases and Local Businesses in '.$city.' '.$press->getStateFromAbv($state).'. '.$city.', '.$state.' Press Release Directory by '.$resellerCompany.' Press. ';
			
			}
		}
	} //end if($args = $_GET['args'])
	else {
		//Root site
		$dispCities = true;
	}
	
if($continue){
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
					<? if($siteInfo['remove_search']){ ?>
                <?=$siteInfo['edit_region_7']?>
				<? } else { ?>
                 	<? if($siteInfo['network_name']){ ?>
                        <h1><?=$siteInfo['network_name']?></h1>
                    <? } else { ?>
                        <h1>Press Release Directory</h1>
                    <?php } ?>
                        <!-- Search-->
                        <? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
                <?php } ?>

				</div>
                
                <br class="clear" />
                <br class="clear" />
				
				<!-- Start Two Third -->
				<div class="two-third">
				<?=$siteInfo['edit_region_1']?>	
					<!-- Header Title -->
					<?php
					if($dispPress){
						if($category) 
							echo '<h1>Local '.$category.' press releases for '.$cityLink.'</h1>';
						else if($city) 
							if($siteInfo['city_focus']){

							}
							else echo '<h1><a href="http://'.$site.'">Local press releases</a> for '.ucwords($city).', '.$state.'</h1>';
					}
					?>
						
					<!-- Breadcrumb -->
					<div id="breadcrumb">
					<?
					if($dispPress){
						if($dispCities) $press->displayCitiesPress(6, $site, $resellerID, $siteInfo['category'], $state); 
						else if($dispCats) $press->displayCategoriesPress($city, $state, 4, $site, $resellerID, $siteInfo['category']);
					}
					?>
					</div>
					
					<!-- Start Display of Listings -->
			<?php
			if($dispPress){
				if($results = $press->getPressForNetwork($mainLimit, $mainOffset, $city, $state, $category, $resellerID)){
					$numListings = $results['numResults'];
					$listings = $results['results'];
					while($row = $press->fetchArray($listings)){					
						if(!$row['thumb_id']) 
						$icon = $dflt_noImage;
						else 
						$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].
								'/press_release/'.$row['thumb_id'];
						
						$created = date("F jS, Y", strtotime($row['created']));
						$createdMo = date("M", strtotime($row['created']));
						$createdDay = date("j", strtotime($row['created']));
						
						$keywords = explode(",", $row['keywords']);
						foreach($keywords as $key=>$value){
							$keywords[$key] = trim($value);
						}
						
						$a = explode('->', $row['category']);
						$pressCategory = $a[1];
						
						$pressUrl = 'http://'.$site.'/'.$press->convertKeywordUrl($pressCategory).
						$press->convertKeywordUrl($row['headline']).$row['id'].'/';
				?>
					<!-- Start Listing -->
					<div class="blog-post">
						<!-- Date -->
						<div class="post-date">
							<span class="post-month"><?=$createdMo?></span><br />
							<span class="post-day"><?=$createdDay?></span>
						</div>
						
						<!-- Title -->
						<div class="post-title">
							<h3><a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>"><?=$row['headline']?></a></h3>
							<span class="clear">
								Tags <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?>" class="category_name"><?=$keywords[0]?></a>
							<? if($keywords[1]){ ?>
								, <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[1]?>" class="category_name"><?=$keywords[1]?></a>
							<? } ?>
							<? if($keywords[2]){ ?>
								, <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[2]?>" class="category_name"><?=$keywords[2]?></a>
							<? } ?>
							</span>
						</div>		
						<br style="clear:both;" />
						
						<!-- Image -->
						<div class="blog2-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['author']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
							</div>
						</div>
						
						<!-- Details -->
						<?=$press->shortenSummary($row['summary'], 250)?><br />
						<a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>">Read More</a></p>
						<br class="clear" />
						
						<div class="add-container">
							<p class="address">Published by <a href="<?=$pressUrl?>" title="<?=$row['author']?>"><?=$row['author']?></a> on <?=$created?></p>
						</div>
						
						<div class="right">
							<a href="<?=$pressUrl?>" class="button_size4"><span>View Press Release</span></a>
						</div>			
					</div> 
					<!-- End Listing -->
				<? 
					} //end while($row = $article->fetchArray($listings))
				?>
					<!-- End Display of Listings -->
					<br clear="all" />
					
					<!-- Pagination -->
					<? include('inc/pagination.php'); ?>
				<?
				} //end if($listings = $article->getArticlesForNetwork()
				else echo 'No press releases found.';
			} //end if($dispHubs)
			else {
				if($dispCities){
					echo '<h2>All cities with <a href="http://'.$site.'">local press releases</a></h2>';
					$press->displayCitiesPress(NULL, $site, $resellerID);
				}
				else if($dispCats){
					echo '<h2>All <a href="http://'.$site.'">local press release</a> categories for '.$cityLink.'</h2>';
					$press->displayCategoriesPress($city, $state, NULL, $site, $resellerID);
				}
			}
			?>
				<p><?=$siteInfo['edit_region_4']?></p>
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_2']?>
							</div>
							<? if($siteInfo['parent'] == "0"){ ?>
                            <? } else { ?>
							<!-- Hubs -->
								 <? if($siteInfo['hubs_network_name']){ ?>
								<h2><?=$siteInfo['hubs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local HUBs</h2>
                                 <?php } ?>
								<div class="sidebar-advertise">
								<?php
								if($results = $press->getHubsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($row = $press->fetchArray($hubs)){
										if(!$row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$row['user_id'].'/hub/'.$row['logo'];
								?>
										<li><a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo 'No HUBs found.';
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Local -->
								 <? if($siteInfo['network_name']){ ?>
								<h2><?=$siteInfo['network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Business Listings</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $press->getListingsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$listings = $results['results'];
									while($row = $press->fetchArray($listings)){
										$a = explode("->", $row['category']);
										$listingCategory = $a[1];
										$listingUrl = 'http://local.'.$domain.'/'.
													$press->convertKeywordUrl($row['keyword_one']).
													$press->convertKeywordUrl($row['company_name']).$row['id'].'/';
								?>
									<li><a href="<?=$listingUrl?>" title=""><?=$row['company_name']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No directory listings found.';
								?>
								</ul>
								<!-- End Local -->
								
								<!-- Blog Posts -->
								 <? if($siteInfo['blogs_network_name']){ ?>
								<h2><?=$siteInfo['blogs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Blog Posts</h2>
                                 <?php } ?>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $press->getPostsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$posts = $results['results'];
									while($row = $press->fetchArray($posts)){
										$postUrl = $row['domain']."/".$press->convertKeywordUrl($row['category']).
					 								$press->convertPressUrl($row['post_title'], $row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo 'No blog posts found.';
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->
								
								<!-- Articles -->
								 <? if($siteInfo['articles_network_name']){ ?>
								<h2><?=$siteInfo['articles_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Articles</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $press->getArticlesForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$articles = $results['results'];
									while($row = $press->fetchArray($articles)){
										$a = explode("->", $row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$press->convertKeywordUrl($articleCategory).
													$press->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No articles found.';
								?>
								</ul>
								<!-- End Articles -->
								<?php } ?>
								<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_3']?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div>
				<!-- End Last Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<? } //end if($continue) ?>