<?php
	$domain = strtolower($_SERVER['SERVER_NAME']);
	$a = explode('.', $domain);
	$site = $a[count($a)-2].'.'.$a[count($a)-1];
	if(count($a)>2 && $a[0]!='www')
		$site = $a[count($a)-3].'.'.$a[count($a)-2].'.'.$a[count($a)-1];
	else
		$site = $a[count($a)-2].'.'.$a[count($a)-1];
	
	if($site!='6qube.com'){
		require_once('/home/sapphire/public_html/6qube.com/admin/inc/db_connector.php');
		$connector = new DbConnector();
		
		$query = "SELECT admin_user, main_site_id FROM resellers WHERE main_site = '".$site."'";
		$resellerSite = $connector->queryFetch($query);
		
		include('/home/sapphire/public_html/6qube.com/reseller/reseller_site.php');
	}
	
	else {
	
	$thisPage = 'index';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Local Internet Marketing by 6Qube | Small Business Local Advertising</title>
<meta name="google-site-verification" content="Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y" />
<meta name="description" content="6Qube Local Internet Marketing Packages are designed for any small business looking for affordable local internet advertising services." /> 
<meta name="keywords" content="local internet marketing,local internet advertising,6qube" /> 
<meta content="INDEX,FOLLOW" name="Robots" />
<meta content="7 Days" name="Revisit-after" />
<meta content="never" name="Expires" />
<link rel="stylesheet" href="http://6qube.com/versions/v2/css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<!--jquery library -->
<script type="text/javascript" src="http://6qube.com/versions/v2/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="http://6qube.com/versions/v2/js/jquery.min.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="http://6qube.com/versions/v2/css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="http://6qube.com/versions/v2/js/ddlevelsmenu.js"></script>
<!-- Fancy Box -->
<link rel="stylesheet" type="text/css" href="http://www.6qube.com/admin/js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript" src="http://www.6qube.com/admin/js/fancybox/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("a#viewhub").fancybox({
		'titleShow'		: false
	});
	$(".demo").fancybox({width:660, height:450,
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
});
</script>
<!--recent projects slider-->
<script type="text/javascript" src="http://6qube.com/versions/v2/js/jquery.jcarousel.pack.js"></script>
<link rel="stylesheet" type="text/css" href="http://6qube.com/versions/v2/css/recent-projects.css" />
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#mycarousel').jcarousel();
});
</script>
<!--header slider-->
<link href="http://6qube.com/versions/v2/css/easyslider.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="http://6qube.com/versions/v2/js/easySlider1.7.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$("#slider").easySlider({
		auto: true, 
		continuous: true
	});
});	
</script>
<script type="text/javascript" src="http://6qube.com/versions/v2/js/scroll.js"></script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="http://6qube.com/versions/v2/css/ie6.css" type="text/css" />
<![endif]-->
<style type="text/css">
#top_menu {margin-top:-20px;}
</style>
</head>
<body>
<!--Start Home Main Div-->
<div id="home_main_div" >
	<!--Start Main Div 1-->
	<div id="main_div_1">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_1-->
			<div id="header_1">
		
				<div id="header_left_div">
					<div class="logo">
						<a title="Local Internet Marketing by 6Qube | Small Business Local Advertising" href="/" ><img src="http://6qube.com/versions/v2/images/logo.png" /></a>
					</div>
					<h1>Local Internet Marketing Leader<br />for Small Business Owners</h1>
					<p>…innovation starts with actual innovation.  6Qube puts technology before sales to produce results quicker and more cost effective for our small business customers.</p>
					<a href="/local-internet-advertising-testimonials/" title="Local Internet Advertising Testimonials | 6Qube Local Internet Marketing" class="view_button">View Success</a>
					<a href="/local-internet-marketing-packages/" title="Local Internet Marketing Packages by 6Qube" class="view_button">View Packages</a>
				</div><!--End Header Left Div-->
				<div id="header_right_div">
			<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="/" <? if($thisPage=="index") echo $curr; ?> title="Local Internet Marketing by 6Qube | Small Business Local Advertising"><span>Home</span></a></li>
										<li><a href="/about-6qube-internet-marketing/" <? if($thisPage=="about") echo $curr; ?> title="About Local Internet Marketing by 6Qube"><span>About Us</span></a></li>
										<li><a href="/local-internet-marketing-packages/" <? if($thisPage=="packages") echo $curr; ?> title="Local Internet Marketing Packages by 6Qube"><span>Solutions</span></a></li>
										<li><a href="/local-internet-advertising-testimonials/" <? if($thisPage=="testimonials") echo $curr; ?> title="Local Internet Advertising Testimonials | 6Qube Local Internet Marketing" ><span>Packages</span></a></li>												
										<li><a href="http://blog.6qube.com" title="Local Internet Marketing Blog"><span>Blog</span></a></li>
										<li><a href="/contact-6qube-local-internet-advertising-company/" <? if($thisPage=="contact") echo $curr; ?> title="Contact 6Qube for Local Internet Marketing"><span>Contact Us</span></a></li>
										<li><a href="http://login.6qube.com/" title="Login to Elements | Internet Marketing Software"><span>Login</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					
					<br clear="all" />
					<!--Start Header Slider-->
					<div id="header_slider">
					<div style="margin-left:15px; padding-top:20px;"><iframe src="http://blip.tv/play/hodXgsfqbwA.html" width="450" height="285" frameborder="0" allowfullscreen></iframe><embed type="application/x-shockwave-flash" src="http://a.blip.tv/api.swf#hodXgsfqbwA" wmode="transparent" style="display:none"></embed></div>
			
					</div>
					<!--End Header Slider-->
				</div><!--End Header Right Div-->
				<br style="clear:both;" />
			</div>
			<!--End Header_1-->
			<!--Start Container-->
			<div id="container">
				<!--Start White Box-->
				<div class="white_box float_left">
					<div class="white_box_top">
						<div class="white_box_bottom">
							<h2>Organic Success Stories</h2>
							<div class="padding">
								<p class="para_1"><strong>Local Internet Marketing</strong> success starts with an organic foundation you can grow from. We will build your local internet marketing foundation for your business while driving <strong>Local Targeted Traffic</strong> that will get your phone ringing and your business moving.<br/><br/>
Our Organic Success Stories section contains featured businesses with <strong>1st Page Placement</strong> from our solution.</p>
								<a href="/local-internet-advertising-testimonials/" title="Local Internet Advertising Testimonials | 6Qube Local Internet Marketing" class="float_right"><img src="http://6qube.com/versions/v2/images/read-more-button.jpg" alt="About Internet Marketing Software Elements by 6Qube" /></a>
								<br clear="all" />
							</div>
						</div>
					</div>
				</div>
				<!--End White Box-->
				<!--Start White Box-->
				<div class="white_box" style="float:left; margin-left:20px;">
					<div class="white_box_top">
						<div class="white_box_bottom">
							<h2>How Do We Help?</h2>
							<ul>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="web_des">Become Visible Online</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="web_dev">Establish Local Footprint</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="cms">Obtain Results Cost Effectively</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="ecomm">Professional Representation</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="seo">Develop Local Online Strategy</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="read_more">Read More...</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!--End White Box-->
				<!--Start White Box-->
				<div class="white_box float_right">
					<div class="white_box_top">
						<div class="white_box_bottom">
							<h2>Featured Packages</h2>
							<ul>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="checks">Up to 15 Top Searched Keywords</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="checks">1st Page of Google in 60 Days</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="checks">Zero Pay-Per-Click Charges</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="checks">No Contracts, Month to Month</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Advertising | 6Qube" id="checks">Local Phone Tracking</a></li>
								<li><a href="/local-internet-marketing-packages/" title="Local Internet Marketing | 6Qube" id="read_more">View Packages...</a></li>
							</ul>
					 	 </div>
				 	 </div>
				 </div>
				<!--End White Box-->
				<br style="clear:both;" />
				<!--Start Recent Projects-->
				<div id="recent_projects">
					<h2>Local Internet Marketing Showcase</h2>	
					<div>								
						<ul id="mycarousel" class="jcarousel-skin-tango">
							<li><a href="http://regenesisihc.com" target="blank" title="Regenesis IHC - Austin Colon Hydrotherapy - www.RegenesisIHC.com"><img src="http://6qube.com/versions/v2/images/regenesis-ihc-thumb.jpg" alt="Regenesis IHC - Colon Hydrotherapy Austin - www.RegenesisIHC.com" /></a></li>
							<li><a href="http://austintxlandscaping.net/"  target="blank" title="Austin Landscaping | Austin Landscaper | Austin TX Landscaping"><img src="http://6qube.com/versions/v2/images/vista-view-landscaping-thumb.jpg" alt="Austin Landscaping | Austin Landscaper | Austin TX Landscaping" /></a></li>
							<li><a href="http://roundrockrealestateagent.net/" target="blank" title="Round Rock Real Estate Agent | Round Rock Real Estate"><img src="http://6qube.com/versions/v2/images/no-limit-thumb.jpg" alt="Round Rock Real Estate Agent | Round Rock Real Estate" /></a></li>
							<li><a href="http://remodelaustin.net" target="blank" title="Austin Remodeling | Austin Remodel"><img src="http://6qube.com/versions/v2/images/vista-view-remodeling-thumb.jpg" alt="Austin Remodeling | Austin Remodel" /></a></li>
							<li><a href="http://electrician-boston.net/" target="blank" title="Boston Electrician | Electrician in Boston"><img src="http://6qube.com/versions/v2/images/ja-electric-thumb.jpg" alt="Boston Electrician | Electrician in Boston" /></a></li>	
							<li><a href="http://austinlandscapedesigners.net/" target="blank" title="Austin Landscape Designers | Austin Landscaping Contractor"><img src="http://6qube.com/versions/v2/images/anderson-landscapes-thumb.jpg" alt="Austin Landscape Designers | Austin Landscaping Contractor" /></a></li>
							<li><a href="http://austintexasroofing.net/" target="blank" title="Austin Roofing | Austin Roofing Contractor"><img src="http://6qube.com/versions/v2/images/vista-view-roofing-thumb.jpg" alt="Austin Roofing | Austin Roofing Contractor" /></a></li>	
							<li><a href="http://sjservicesinc.com/" target="blank" title="Houston Painter | Houston Painting Contractor"><img src="http://6qube.com/versions/v2/images/sj-services-thumb.jpg" alt="Houston Painter | Houston Painting Contractor" /></a></li>
						</ul><!--recent projects ul--> 			
					</div>	
				<!--<a href="#" class="view_projects"><img src="http://6qube.com/versions/v2/images/view-all-projects.jpg" alt="" /></a>--> 
				</div>
				<!--End Recent Projects-->
				<!--Start Testimonials-->
				<div id="testimonials">
					<h2>Testimonials</h2>
					<div class="testimonials_box">
						<div class="testimonials_top">
							<div class="testimonials_bottom">
								<p>We have used many different types of local internet marketing programs and it is amazing to finally have a product that works. Last month was our biggest yet!</p>
							</div>
						</div>
					</div><!--End Testimonials Box-->
					<p class="client_name"><span>Precision Carpet Care</span><br /> - Austin, TX</p>
				</div>
				<!--End Testimonials-->
				<br style="clear:both;" />
			</div>
			<!--End Container-->
		</div>
		<!--End Page Container-->
<?
		// ** Footer ** //
		require_once('/home/sapphire/public_html/6qube.com/includes/v2Efooter.inc.php');
?>
<!-- Analytics -->
<script type="text/javascript"> 
var pkBaseURL = (("https:" == document.location.protocol) ? "https://6qube.com/analytics/" : "http://6qube.com/analytics/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript"> 
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 636);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://6qube.com/analytics/piwik.php?idsite=636" style="border:0" alt=""/></p></noscript>
<!-- end -->	
</body>
</html>
<? } ?>