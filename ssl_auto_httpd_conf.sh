#!/bin/bash


###########################################################
## Step 5. Add new url and ip to Apache httpd.conf file. ##
###########################################################
printf "Adding URL and IP address to httpd.conf file...\r\n"

#Prepare new virtual host XML
virtual_host="<VirtualHost ${website_ip_addr}:443> \r"

virtual_host+="\tServerName ${website_url} \r"
virtual_host+="\tServerAlias www.${website_url} ${website_url} *.${website_url} \r"
virtual_host+="\tDocumentRoot \/usr\/local\/apache\/dev-trunk\/hybrid \r\r"

virtual_host+="\tSSLCertificateFile \/etc\/letsencrypt\/live\/${website_url}\/cert.pem \r"
virtual_host+="\tSSLCertificateKeyFile \/etc\/letsencrypt\/live\/${website_url}\/privkey.pem \r"
virtual_host+="\tInclude \/etc\/letsencrypt\/options-ssl-apache.conf \r"
virtual_host+="\tSSLCertificateChainFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \r"

virtual_host+="\tSSLCACertificatePath \/etc\/letsencrypt\/live\/${website_url}\/ \r"
virtual_host+="\tSSLCACertificateFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \r"

virtual_host+="<\/VirtualHost>\r\n"

# -F = fixed string (make list of strings), -q quiet (do not write anything to standard output)
if grep -Fq "${website_ip_addr}" ${httpd_conf_file}
then
    printf "Found matching ip address: \"${website_ip_addr}\"\r\n"

    # Write to file in area as same ip.
    sed -i "0,/<VirtualHost ${website_ip_addr}>/s//${virtual_host}\n<VirtualHost ${website_ip_addr}>/" ${httpd_conf_file}
else
    printf "No matching ip address found... appending new virtual host to httpd.conf\r\n."

    # Append to file with new ip.
    printf "\r${virtual_host}" >> ${httpd_conf_file}
fi

#############################
## Step 4: Restart Apache. ##
#############################
sudo apachectl -k restart

# End program.
exit 0
