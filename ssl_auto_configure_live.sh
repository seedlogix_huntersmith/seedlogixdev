#!/bin/bash

# Run from root user folder.

### Turn on debug mode ###
#set -x

### Script Arguments ###
#$1 = URL of the website.
#$2 = IP address of website (pointer)
website_url=$1
#website_ip_addr=$2

### Global Vars ###
now=$(date)
logfile=/tmp/ssl_auto_configure.log
acme_dir=/${USER}/.acme.sh/
httpd_conf_file=/usr/local/apache/conf/httpd.conf

### Global Functions ###
log_error_time() {
    printf "[${now}] " >>  ${logfile}
}

display_and_log_error() {
    err_msg=$1

    log_error_time
    printf "${err_msg}" >> ${logfile}
    printf "${err_msg}"
}

# SSL Automation Setup Script using ACME shell.
printf "Start SSL Auto Configure...\r\n"
printf "Date: ${now} @ $(hostname)\r\n"

printf "\r\n"
printf "Configuring SSL for ${website_url}...\r\n"
printf "\r\n"

#############################################
## Step 1: Switch to acme.sh source folder ##
#############################################
#printf "Moving to 'acme.sh' directory...\r\n"
if [[ -d "${acme_dir}" ]]
then
    printf "Switching to ACME directory...\r\n"
    cd "${acme_dir}"
else
    display_and_log_error "Could not find the directory: '${acme_dir}'\r\n"
    exit
fi



################################
## Step 2: Run acme commands. ##
################################
printf "\r\n"
printf "Running acme.sh script for new domain...\r\n"
./acme.sh --issue -d ${website_url} -d *.${website_url} --challenge-alias blue-jag.com --dns dns_gd



########################################################
## Step 3: Create folder for new domain if successful ##
########################################################
printf "\r\n"
printf "Creating new folder for domain: ${website_url}...\r\n"

mkdir -p /etc/letsencrypt/live/${website_url}

domain_dir=/etc/letsencrypt/live/${website_url}
if [[ -d "${domain_dir}" ]]
then
    printf "The directory '${domain_dir}' was created successfully!\r\n"
else
    display_and_log_error "Could not create the directory: '${domain_dir}'\r\n"
    exit
fi


#################################
## Step 4: Run install script. ##
#################################
./acme.sh --install-cert -d ${website_url} -d *.${website_url} \
--cert-file /etc/letsencrypt/live/${website_url}/cert.pem \
--key-file /etc/letsencrypt/live/${website_url}/privkey.pem \
--fullchain-file /etc/letsencrypt/live/${website_url}/fullchain.pem