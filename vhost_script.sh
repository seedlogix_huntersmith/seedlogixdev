#!/bin/bash
printf "Hello $USER \r\n"
printf "Insert text in vhost file...\r\n"

HTTPD_CONF_FILE=./httpd.conf

website_url="huntersmith.com"
website_ip_addr="50.97.78.212:443"


# Prep new virtual host XML
virtual_host="<VirtualHost ${website_ip_addr}> \r"

virtual_host+="\tServerName ${website_url} \r"
virtual_host+="\tServerAlias www.${website_url} ${website_url} *.${website_url} \r"
virtual_host+="\tDocumentRoot \/usr\/local\/apache\/dev-trunk\/hybrid \r\r"

virtual_host+="\tSSLCertificateFile \/etc\/letsencrypt\/live\/${website_url}\/cert.pem \r"
virtual_host+="\tSSLCertificateKeyFile \/etc\/letsencrypt\/live\/${website_url}\/privkey.pem \r"
virtual_host+="\tInclude \/etc\/letsencrypt\/options-ssl-apache.conf \r"
virtual_host+="\tSSLCertificateChainFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \r"

virtual_host+="\tSSLCACertificatePath \/etc\/letsencrypt\/live\/${website_url}\/ \r"
virtual_host+="\tSSLCACertificateFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \r"

virtual_host+="<\/VirtualHost>\r\n"

#sed -i "0,/<VirtualHost ${website_ip_addr}>/s//${virtual_host}\n<VirtualHost ${website_ip_addr}>/" ./httpd.conf
#printf "\r\r${virtual_host}" >> ${HTTPD_CONF_FILE}
#exit 0

# -F = fixed string (make list of strings), -q quiet (do not write anything to standard output)
if grep -Fq "${website_ip_addr}" ${HTTPD_CONF_FILE}
then
    printf "Found matching ip address: \"${website_ip_addr}\"\r\n"

    # Write to file in area as same ip.
    sed -i "0,/<VirtualHost ${website_ip_addr}>/s//${virtual_host}\n<VirtualHost ${website_ip_addr}>/" ${HTTPD_CONF_FILE}
else
    printf "No matching ip address found... appending new virtual host to httpd.conf\r\n."

    # Append to file with new ip.
    printf "\r${virtual_host}" >> ${HTTPD_CONF_FILE}
fi

# End program.
exit 0