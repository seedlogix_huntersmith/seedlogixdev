<?php

defined('QUBEROOT') || die('Access Denied');

session_start();

$site = $host;
$a = explode('.', $site);

if(count($a)>3 && $a[0]!='www')
	$domain = $a[count($a)-3].'.'.$a[count($a)-2].'.'.$a[count($a)-1];
else
	$domain = $a[count($a)-2].'.'.$a[count($a)-1];

if($domain!='6qube.com'){
	require_once( QUBEADMIN . 'inc/db_connector.php');
    
	$connector = new DbConnector();
	
	//if(!$_SESSION['siteInfo']){ //use session vars so db isn't called on every page load
		if(TRUE){		
			$resellerID = $siteInfo['admin_user'];
			
			//get reseller's main site info
#			$query = "SELECT main_site, main_site_id FROM resellers WHERE admin_user = ".$resellerID;
#			$a = $connector->queryFetch($query);
#			$siteInfo['reseller_main_site'] = $a['main_site'];
#			$siteInfo['reseller_main_site_id'] = $a['main_site_id'];
			
			//get select data from reseller's main site
#			$query = "SELECT domain, company_name, site_title, keyword1, keyword2, blog, connect_blog_id  
					#FROM hub 
					#WHERE id = ".$siteInfo['reseller_main_site_id'];
                        
#			$mainSite = $connector->queryFetch($query);
                        $mainHub    =   $info->getResellerHub();
                        $mainSite   =   (array)$mainHub;
                        
			if($mainSite['blog']) $hasBlog = true;
			
			//check if reseller's main site has any custom pages
			require_once(QUBEADMIN . 'inc/hub.class.php');
			$hub = new Hub();
			$hub_pages = $hub->getHubPage($siteInfo['reseller_main_site_id'], NULL, NULL, NULL, TRUE, TRUE);
			if($hub->getPageRows()) $hasPages = true;
			
#			$query = "SELECT path FROM themes_network WHERE id = ".$siteInfo['theme'];
#			$themeInfo = $connector->queryFetch($query);
                        $themeInfo  =   array('path'    => $siteInfo['network_theme_path']);
			$themeInfo['themeUrl'] = 'http://'.$site.'/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themeUrl2'] = 'http://'.$siteInfo['reseller_main_site'].'/reseller/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themePath'] = QUBEROOT . 'blogs/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themePath2'] = QUBEROOT . 'reseller/network-themes/'.$themeInfo['path'].'/';
			
			include(QUBEROOT . 'blogs/network-themes/'.$themeInfo['path'].'/index.php');
		}
	//}
	//else include('/home/sapphire/public_html/6qube.com/blogs/network-themes/'.$_SESSION['themeInfo']['path'].'/index.php');
}
else {
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$boxes = true;
	$continue = true;
	//title and meta description -- gets changed if viewing city or category
	$title = "Local Blog Directory by 6Qube Blogs | Local Blog Posts";
	$meta_desc = "6Qube Blogs offers search for Local Blog Posts in your local city. Simply search our Local Blog Directory for local blog posts.";
	
	require_once( QUBEADMIN . 'inc/blogs.class.php');
	$blog = new Blog();
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);
		
		if($args_array[0]=="cities"){
			//List of cities
			//echo "cities";
			$boxes = false;
			$disp_cities = true;
			$title = "Browse Local Business Blogs by City | Local Blog Posts";
			$meta_desc = "Find Local Business Blogs by browsing 6Qube Blogs City Display of local blog posts.";
		}
		else if(is_numeric($args_array[0])){
			//Paginated all listings
			//echo "all listings - page ".$args_array[0];
			$page = $args_array[0];
			$mainOffset = ($mainLimit*$page)-$mainLimit;
			$title = "Local Blog Posts Directory by 6Qube Blogs | Page ".$page;
			$meta_desc .= " Page ".$page." of results.";
			$disp_cities = true;
		}
		else if($city = $blog->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $blog->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $blog->majorCity($city);
				$majorCity = true;
			}
			
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//Paginated listings for a city
					//echo "$city." listings, page #".$args_array[1];
					$disp_cats = true;
					$page = $args_array[1];
					$mainOffset = ($mainLimit*$page)-$mainLimit;
					$title = $city.', '.$state.' Local Blog Post Directory by 6Qube Blogs | Page '.$page;
					$meta_desc = 'Find '.$city.', '.$state.' local business Blog Posts in '.$city.' '.$blog->getStateFromAbv($state).'. 6Qube Blogs '.$city.' '.$state.' local blog post directory. Page '.$page.' of results.';
				}
				else if($args_array[1]=="categories"){
					//List of categories for a city
					//echo $city." Categories";
					$boxes = false;
					$disp_cities = false;
					$disp_cats = true;
					$title = $city.', '.$state.' Local Business Blog Categories | All Business Blog Posts Categories';
					$meta_desc = 'Find Local Business Blog Posts in '.$city.', '.$state.' by browsing 6Qube Blogs All Categories Display of '.$city.' '.$blog->getStateFromAbv($state).' Local Blogs.';
				}
				else if($category = $blog->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							//Paginated listings for a certain category in a city
							//echo $city." ".$category."s, page ".$args_array[2];
							$page = $args_array[2];
							$mainOffset = ($mainLimit*$page)-$mainLimit;
							$title = $city.' '.$state.' '.$category.' Blog Posts | '.$category.
									' Blogs in '.$city.' | Page '.$page;
							$meta_desc = $city.' '.$state.' Blog Post Directory of '.$category.' by 6Qube Blogs.  Discover local '.$category.' Blog Posts in '.$city.' '.$state.'. Page '.$page.' of results.';
						}
					}
					else {
						//All listings for a certain category in a city
						//echo $city." ".$category."s";\
						$title = $city.' '.$state.' '.$category.' Blog Posts | '.$category.
								' Blogs in '.$city;
						$meta_desc = $city.' '.$state.' Blog Post Directory of '.$category.' by 6Qube Blogs.  Discover local '.$category.' Blogs in '.$city.' '.$state.'.';
					}
				}
			} //end if($args_array[1])
			else {
				//All listings for a city
				//echo "All ".$city." listings";
				$disp_cities = false;
				$disp_cats = true;
				
				$title = $city.', '.$state.' Local Business Blog Posts Directory | Directory of '.$city.' Blogs';
				$meta_desc = 'Find '.$city.' '.$state.' local business Blog Posts in '.$city.' '.$blog->getStateFromAbv($state).'. 6Qube Blogs '.$city.' '.$state.' local blog directory.';
			}
		}
	} //end if($args = $_GET['args'])
	else {
		//Root site
		$disp_cities = true;
	}
	if($continue){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="google-site-verification" content="Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y" />
<title><?=$title?></title>
<meta name="description" content="<?=$meta_desc?>" />
<meta content="INDEX,FOLLOW" name="Robots" />
<meta content="2 Days" name="Revisit-after" />
<link rel="sitemap" href="http://blogs.6qube.com/sitemap.xml" type="application/xml" />
<link rel="stylesheet" href="http://blogs.6qube.com/css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/blogs_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/blogs_favicon.png" />
<!--jquery library -->
<script type="text/javascript" language="javascript" src="http://blogs.6qube.com/js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="http://blogs.6qube.com/js/jquery.hint.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="http://blogs.6qube.com/css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="http://blogs.6qube.com/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="http://blogs.6qube.com/js/scroll.js"></script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="http://blogs.6qube.com/css/ie6.css" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript">
$(function(){ 
	$('input[title!=""]').hint();
});
</script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34745846-1']);
  _gaq.push(['_setDomainName', '6qube.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
						<a title="Local Blog Directory by 6Qube Blogs | Local Blogs" href="/" ><img src="http://blogs.6qube.com/images/blogs-logo.png" alt="Local Blog Directory by 6Qube Local | Local Blogs" /></a>
					</div>
				</div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="http://local.6qube.com/" title="Local Yellow Pages Directory by 6Qube Local"><span>Local</span></a></li>
										
										<li><a href="http://hubs.6qube.com/" title="Local Website Directory by 6Qube Local | Local Websites"><span>Websites</span></a></li>
										<li><a href="http://press.6qube.com/" title="Local Press Directory by 6Qube Local | Local Press Releases"><span>Press</span></a></li>												
										<li><a href="http://articles.6qube.com/" title="Local Articles Directory by 6Qube Local | Local Articles"><span>Articles</span></a></li>
										<li><a href="http://blogs.6qube.com/" class="activelink" title="Local Blog Directory by 6Qube Local | Local Blogs"><span>Blogs</span></a></li>
										<li><a href="http://6qubedirectory.com" title="6Qube Directory | Free Business Listing | Free Business Directory | Free Directory Listing"><span>Free Business Listing</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					<br clear="all" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<div id="search">
						<?	
						// ** Search ** //
						require_once('/home/sapphire/public_html/6qube.com/includes/searchForm.inc.php');
						?>
					</div><!-- /search -->  
					<br clear="all" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container 5-->
			<div id="container_5">
				<!--Start Blogs Container-->
				<div id="blogs_container">
					<!--Start Blogs Container Blogs Container Top-->
					<div id="blogs_container_top">
						<!--Start Blogs Container Blogs Container Bottom-->
						<div id="blogs_container_bottom">
							<? 
							if($boxes){
								echo '<div style="margin-left:40px;">';
								if($disp_cities) $blog->displayCitiesBlogs(10); 
								else if($disp_cats) $blog->displayCategoriesBlogs($city, $state, 7);
								echo '</div>';
							}
							?>
							<!--Start Breadcrumb-->
							<div id="breadcrumb">
								<h1 style="padding-left:0;">Featured Blog Posts. New Additions</h1>
								<?
								if($city){
									echo '<a href="http://blogs.6qube.com">Blog Post Directory</a> ';
									if($category || $args_array[1]=="categories"){
										echo $blog->getCityLink($city, $state, 'blogs', NULL, true);
										
										if(!$boxes) echo ' <b>All Categories</b>';
										else echo '<b>'.ucwords($category).'</b>';
									}
									else{
										echo '<b>'.$city.', '.$state.'</b>';
									}
								}
								else if(!$boxes){
									echo '<a href="http://blogs.6qube.com">Blog Post Directory</a> ';
									echo ' <b>All Cities</b>';
								}
								?>
							</div>
							<!--End Breadcrumb-->
							<!--Start Blog Content-->
							<div id="blog_content">
								<div id="blogs_display">
									<?php
									if($boxes){
									$blog->displayPost($mainLimit, $mainOffset, 1, NULL, NULL, $city, $state, $category);
									}
									?>
								</div>
							</div>
							<!--End Blog Content-->
							<?php 
							if(!$boxes){
								echo '<div class="list-local" style="margin-left:40px;">';
								if($disp_cities) $blog->displayCitiesBlogs();
								else if($disp_cats) $blog->displayCategoriesBlogs($city, $state);
								echo '</div>';
							}
							?>
							<? if($boxes){ ?>
							<!--Start Blog Right Panel-->
							<div id="blog_right_panel">
								<h2>New from HUBs</h2>
								<div id="hub_display">
								<?php
								// display the 4 newest HUBs
								require_once('/home/sapphire/public_html/6qube.com/admin/inc/hub.class.php');
								$hub = new Hub();
								$hub->displayHubs($rightLimit, $offset, NULL, NULL, $city, $state);
								?>
								</div>
								<br class="clear" /><br />
								
								<h2>New from Articles</h2>
								<div id="articles_display">
								<?php
								// display the 4 newest Article titles (shortened)
								require_once('/home/sapphire/public_html/6qube.com/admin/inc/articles.class.php');
								$articles = new Articles();
								$articles->displayArticles($rightLimit, $offset, NULL, NULL, $city, $state);
								?>
								</div>
								<br class="clear" /><br />
								
								<h2>New from Press</h2>
								<div id="press_display">
								<?php
								// display the 4 newest Blog post titles (shortened)
								require_once('/home/sapphire/public_html/6qube.com/admin/inc/press.class.php');
								$press = new Press();
								$press->displayPress($rightLimit, $offset, NULL, NULL, $city, $state);
								?>
								</div>
								<br class="clear" /><br />
								
								<h2>New from Local</h2>
								<div id="local_display">
								<?php
								// display the 4 newest Directory listings (shortened)
								require_once('/home/sapphire/public_html/6qube.com/admin/inc/directory.class.php');
								$dir = new Dir();
								$dir->displayDirectories($rightLimit, $offset, NULL, NULL, $city, $state);
								?>
								</div>
								<br class="clear" /><br />
							</div>
							<!--End Blog Right Panel-->
							<br clear="all" />
							<? } ?>
						</div>
						<!--End Blogs Container Bottom-->
					</div>
					<!--End Blogs Container Top-->
				</div>
				<!--End Blogs Container-->
			</div>
			<!--End Container 5-->
		</div>
		<!--End Page Container-->
	<?	
	// ** Footer ** //
	require_once('/home/sapphire/public_html/6qube.com/includes/v2footer.inc.php');
	?>
</body>
</html>
<?
	}
}
?>
