<?php
$maxPage = ceil($numListings/$mainLimit);
//if($mainOffset > 0) { $currPage = ($mainOffset/$mainLimit)+1; } else { $currPage = 1; }
//$currPage var is set at the top of parent index.php file
if(!$currPage) $currPage = 1;
$baseLink = "http://".$site."/";
if($city){
	if($major = $blog->majorCity($city)){
		if($major == $state) $baseLink .= $blog->convertKeywordUrl($city, true)."/";
		else $baseLink .= $blog->convertKeywordUrl($city, true)."-".strtolower($state)."/";
	}
	else $baseLink .= $blog->convertKeywordUrl($city, true)."-".strtolower($state)."/";
}
if($category) $baseLink .= $blog->convertKeywordUrl($category)."/";
if(($currPage < 3) || ($maxPage <= 5)){ // if there are less than 5 pages, or the current page # is less than 3
	if($maxPage<=5){ $total = $maxPage; } else { $total = 5; } // calculate how many pages to display
	for($page = 1; $page <= $total; $page++){
	 	if($page == $currPage){ //current page
			$nav .= '<li class="page-active">'.$page.'</li>';
		}
		else {
			$nav .= '<li><a href="'.$baseLink.$page.'/">'.$page.'</a></li>';
		}
	}
}
		
else if(($currPage>=3) && ($currPage<=($maxPage-3))){ // example display: 1 2 [3 4 [5] 6 7] 8
	$page_offset = ($currPage*$mainLimit)-$mainLimit;
	$nav =  '<li><a href="'.$baseLink.($currPage-2).'/">'.($currPage-2).'</a></li>';
	$nav .= '<li><a href="'.$baseLink.($currPage-1).'/">'.($currPage-1).'</a></li>';
	$nav .= '<li class="page-active">'.$currPage.'</li>';
	$nav .= '<li><a href="'.$baseLink.($currPage+1).'/">'.($currPage+1).'</a></li>';
	$nav .= '<li><a href="'.$baseLink.($currPage+2).'/">'.($currPage+2).'</a></li>';
}
		
else { // example display: 1 2 3 [4 5 6 [7] 8]
	for($page = ($maxPage-4); $page <= $maxPage; $page++){
		if($page == $currPage){ //current page
			$nav .= '<li class="page-active">'.$page.'</li>';
		}
		else {
			$nav .= '<li><a href="'.$baseLink.$page.'/">'.$page.'</a></li>';
		} 
	}
}
		
// set prev/next button links
$prevLink = '<a href="'.$baseLink.($currPage-1).'/">';
$nextLink = '<a href="'.$baseLink.($currPage+1).'/">';
if($currPage==1){
	$prevLink = '<a href="#">';
}
if($currPage==$maxPage){
	$nextLink = '<a href="#">';
}
if($maxPage!=1){ // don't display nav if there's only one page
	$html =  '<div id="pagination"><ul>
			<li class="page">Page '.$currPage.' of '.$maxPage.'</li>
			<li class="page-arrow">'.$prevLink.'&#171;</a></li>
			'.$nav.'
			<li>'.$nextLink.'&#187;</a></li>
			</ul></div>
			<br />';
}				
echo $html;
?>