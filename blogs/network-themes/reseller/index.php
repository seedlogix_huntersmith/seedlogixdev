<?php
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$dispPosts = true;
	$continue = true;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	}
	
	//title and meta description -- gets changed if viewing city or category
	if($siteInfo['seo_title']){
		$title = $siteInfo['seo_title'];
	}
	else $title = "".$resellerCompany." Blog Post Directory | Local Blog Listings";
	if($siteInfo['seo_desc']){
		$meta_desc = $siteInfo['seo_desc'];
	}
	else $meta_desc = $resellerCompany." Blogs is a directory of Local Blog Posts in your local city. View local posts from local blogs on ".$resellerCompany."";
	
	
	
	require_once(QUBEADMIN . 'inc/blogs.class.php');
	$blog = new Blog();
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);
		
		if($args_array[0]=="cities"){
			//List of cities
			//echo "cities";
			$dispPosts = false;
			$dispCities = true;
			$title = "Browse Local Business Blogs by City | Local Blog Posts";
			$meta_desc = "Find Local Business Blogs by browsing ".$resellerCompany." Blogs City Display of local blog posts.";
		}
		else if(is_numeric($args_array[0])){
			//Paginated all listings
			//echo "all listings - page ".$args_array[0];
			$dispCities = true;
			$currPage = $args_array[0];
			$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			$title = "Local Blog Posts Directory by ".$resellerCompany." Blogs | Page ".$page;
			$meta_desc .= " Page ".$page." of results.";
		}
		else if($city = $blog->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $blog->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $blog->majorCity($city);
				$majorCity = true;
			}
			$cityLink = $blog->getCityLink($city, $state, $site, NULL, true);			
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//Paginated listings for a city
					//echo "$city." listings, page #".$args_array[1];
					$dispCats = true;
					$currPage = $args_array[1];
					$mainOffset = ($mainLimit*$currPage)-$mainLimit;
					$title = $city.', '.$state.' Local Blog Post Directory by '.$resellerCompany.' Blogs | Page '.$page;
					$meta_desc = 'Find '.$city.', '.$state.' local business Blog Posts in '.$city.' '.$blog->getStateFromAbv($state).'. '.$resellerCompany.' Blogs '.$city.' '.$state.' local blog post directory. Page '.$page.' of results.';
				}
				else if($args_array[1]=="categories"){
					//List of categories for a city
					//echo $city." Categories";
					$dispPosts = false;
					$dispCities = false;
					$dispCats = true;
					$title = $city.', '.$state.' Local Business Blog Categories | All Business Blog Posts Categories';
					$meta_desc = 'Find Local Business Blog Posts in '.$city.', '.$state.' by browsing '.$resellerCompany.' Blogs All Categories Display of '.$city.' '.$blog->getStateFromAbv($state).' Local Blogs.';
				}
				else if($category = $blog->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							//Paginated listings for a certain category in a city
							//echo $city." ".$category."s, page ".$args_array[2];
							$page = $args_array[2];
							$mainOffset = ($mainLimit*$page)-$mainLimit;
							$title = $city.' '.$state.' '.$category.' Blog Posts | '.$category.' Blogs in '.$city.' | Page '.$page;
							$meta_desc = $city.' '.$state.' Blog Post Directory of '.$category.' by '.$resellerCompany.' Blogs.  Discover local '.$category.' Blog Posts in '.$city.' '.$state.'. Page '.$page.' of results.';
						}
					}
					else {
						//All listings for a certain category in a city
						//echo $city." ".$category."s";\
						if($siteInfo['citycat_seo_title']){
						$title = $siteInfo['citycat_seo_title'];
						}	
						else $title = $city.' '.$state.' '.$category.' Blog Posts | '.$category.' Blogs in '.$city;
						
						if($siteInfo['citycat_seo_desc']){
						$meta_desc = $siteInfo['citycat_seo_desc'];
						}	
						else $meta_desc = $city.' '.$state.' Blog Post Directory of '.$category.' by '.$resellerCompany.' Blogs.  Discover local '.$category.' Blogs in '.$city.' '.$state.'.';

					}
				}
			} //end if($args_array[1])
			else {
				//All listings for a city
				//echo "All ".$city." listings";
				$dispCities = false;
				$dispCats = true;
				
				if($siteInfo['city_seo_title']){
				$title = $siteInfo['city_seo_title'];
				}	
				else $title = $city.', '.$state.' Local Business Blog Posts Directory | Directory of '.$city.' Blogs';
				
				if($siteInfo['city_seo_desc']){
				$meta_desc = $siteInfo['city_seo_desc'];
				}	
				else $meta_desc = 'Find '.$city.' '.$state.' local business Blog Posts in '.$city.' '.$blog->getStateFromAbv($state).'. '.$resellerCompany.' Blogs '.$city.' '.$state.' local blog directory.';
			}
		}
	} //end if($args = $_GET['args'])
	else {
		//Root site
		$dispCities = true;
	}
	
if($continue){
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
					<? if($siteInfo['blogs_network_name']){ ?>
					<h1><?=$siteInfo['blogs_network_name']?></h1>
                <? } else { ?>
                	<h1>Blog Posts Directory</h1>
                <?php } ?>
					<!-- Search-->
					<? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
				</div>
                
                <br class="clear" />
                <br class="clear" />
				
				<!-- Start Two Third -->
				<div class="two-third">	
				<?=$siteInfo['edit_region_1']?>
					<!-- Header Title -->
					<?php
					if($dispPosts){
						if($category) 
							echo '<h1>Local '.$category.' blog posts for '.$cityLink.'</h1>';
						else if($city) 
							echo '<h1><a href="http://'.$site.'">Local blog posts</a> for '.ucwords($city).', '.$state.'</h1>';
					}
					?>
						
					<!-- Breadcrumb -->
					<div id="breadcrumb">
					<?
					if($dispPosts){
						if($dispCities) $blog->displayCitiesBlogs(6, $site, $resellerID); 
						else if($dispCats) $blog->displayCategoriesBlogs($city, $state, 4, $site, $resellerID);
					}
					?>
					</div>
					
					<!-- Start Display of Listings -->
			<?php
			if($dispPosts){
				if($results = $blog->getPostsForNetwork($mainLimit, $mainOffset, $city, $state, $category, $resellerID)){
					$numListings = $results['numResults'];
					$listings = $results['results'];
					while($row = $blog->fetchArray($listings)){	
						//the getPostsForNetwork() function in local.class.php combines the required fields
						//from the blog_post and blogs tables.				
						if(!$row['photo']) 
						$icon = $dflt_noImage;
						else 
						$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/blog_post/'.$row['photo'];
						
						$created = date("F jS, Y", strtotime($row['created']));
						$createdMo = date("M", strtotime($row['created']));
						$createdDay = date("j", strtotime($row['created']));
						
						$keywords = explode(",", $row['tags']);
						foreach($keywords as $key=>$value){
							$keywords[$key] = trim($value);
						}
						
						$postUrl = $row['domain']."/".$blog->convertCategory($row['category']).
								 			$blog->convertPressUrl($row['post_title'], $row['post_id']);
				?>
					<!-- Start Listing -->
					<div class="blog-post">
						<!-- Date -->
						<div class="post-date">
							<span class="post-month"><?=$createdMo?></span><br />
							<span class="post-day"><?=$createdDay?></span>
						</div>
						
						<!-- Title -->
						<div class="post-title">
							<h3><a href="<?=$postUrl?>" title="Read blog post <?=$row['post_title']?> about <?=$keywords[0]?> at <?=$row['blog_title']?>"><?=$row['post_title']?></a></h3>
							<span class="clear">
								Tags <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[0]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[0]?></a>
							<? if($keywords[1]){ ?>
								, <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[1]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[1]?></a>
							<? } ?>
							<? if($keywords[2]){ ?>
								, <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[2]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[2]?></a>
							<? } ?>
							</span>
						</div>		
						<br style="clear:both;" />
						
						<!-- Image -->
						<div class="blog2-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['blog_title']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
							</div>
						</div>
						
						<!-- Details -->
						<?=$blog->shortenSummary($row['post_content'], 250)?><br />
						<a href="<?=$postUrl?>" title="Read blog post about <?=$row['post_title']?> on <?=$row['blog_title']?>">Read More</a></p>
						<br class="clear" />
						
						<div class="add-container">
							<p class="address">Posted on <a href="<?=$row['domain']?>" title="<?=$row['blog_title']?>"><?=$row['blog_title']?></a> on <?=$created?></p>
						</div>
						
						<div class="right">
							<a href="<?=$postUrl?>" class="button_size4"><span>Read Blog Post</span></a>
						</div>			
					</div> 
					<!-- End Listing -->
				<? 
					} //end while($row = $article->fetchArray($listings))
				?>
					<!-- End Display of Listings -->
					<br clear="all" />
					
					<!-- Pagination -->
					<? include('inc/pagination.php'); ?>
				<?
				} //end if($listings = $article->getArticlesForNetwork()
				else echo 'No blog posts found.';
			} //end if($dispHubs)
			else {
				if($dispCities){
					echo '<h2>All cities with <a href="http://'.$site.'">local blog posts</a></h2>';
					$blog->displayCitiesBlogs(NULL, $site, $resellerID);
				}
				else if($dispCats){
					echo '<h2>All <a href="http://'.$site.'">local blog post</a> categories for '.$cityLink.'</h2>';
					$blog->displayCategoriesBlogs($city, $state, NULL, $site, $resellerID);
				}
			}
			?>
			<p><?=$siteInfo['edit_region_4']?></p>
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_2']?>
							</div>
							<!-- Hubs -->
								 <? if($siteInfo['hubs_network_name']){ ?>
								<h2><?=$siteInfo['hubs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local HUBs</h2>
                                 <?php } ?>
								<div class="sidebar-advertise">
								<?php
								if($results = $blog->getHubsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($row = $blog->fetchArray($hubs)){
										if(!$row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$row['user_id'].'/hub/'.$row['logo'];
								?>
										<li><a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo 'No HUBs found.';
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Local -->
								<? if($siteInfo['network_name']){ ?>
								<h2><?=$siteInfo['network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Business Listings</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $blog->getListingsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$listings = $results['results'];
									while($row = $blog->fetchArray($listings)){
										$a = explode("->", $row['category']);
										$listingCategory = $a[1];
										$listingUrl = 'http://local.'.$domain.'/'.
													$blog->convertKeywordUrl($row['keyword_one']).
													$blog->convertKeywordUrl($row['company_name']).$row['id'].'/';
								?>
									<li><a href="<?=$listingUrl?>" title=""><?=$row['company_name']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No directory listings found.';
								?>
								</ul>
								<!-- End Local -->
								
								<!-- Press -->
								 <? if($siteInfo['press_network_name']){ ?>
								<h2><?=$siteInfo['press_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Press Releases</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $blog->getPressForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$press = $results['results'];
									while($row = $blog->fetchArray($press)){
										$a = explode("->", $row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
													$blog->convertKeywordUrl($pressCategory).
													$blog->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No press releases found.';
								?>
								</ul>
								<!-- End Press -->
								
								<!-- Articles -->
								 <? if($siteInfo['articles_network_name']){ ?>
								<h2><?=$siteInfo['articles_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Articles</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $blog->getArticlesForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$articles = $results['results'];
									while($row = $blog->fetchArray($articles)){
										$a = explode("->", $row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$blog->convertKeywordUrl($articleCategory).
													$blog->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No articles found.';
								?>
								</ul>
								<!-- End Articles -->
								<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_3']?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div>
				<!-- End Last Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<? } //end if($continue) ?>
