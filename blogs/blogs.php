<?php
require_once dirname(__FILE__) . '/../admin/6qube/core.php';
	require_once HYBRID_PATH . 'classes/BlogLegacyWrapper.php';
	require_once(QUBEADMIN . 'inc/hub.class.php');
	if(!($blog instanceof BlogModel))
		throw new QubeException('Not as Blog Model defined.');
	
	$blog = new BlogLegacyWrapper($blog);
	$hub = new Hub();
	$category = 0;
	
	if(isset($_GET['blog']) && is_numeric($_GET['blog'])) $blogID = $_GET['blog'];
	else if(isset($_GET['blogID']) && is_numeric($_GET['blogID'])) $blogID = $_GET['blogID'];
	if(!$blogID){
		$script_path = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
		$InnermostDir = basename(rtrim($script_path, '/'));
		$blogID = is_numeric($InnermostDir) ? $InnermostDir : 0;
	}
	
	if($_GET['post'] || $_GET['postID'] || $_GET['postTitle']){
		if(isset($_GET['postID']) && is_numeric($_GET['postID'])) $postID = $_GET['postID']; //pull up a single post - updated htaccess
		else if(isset($_GET['post']) && is_numeric($_GET['post'])) $postID = $_GET['post']; //pull up a single post
		else {
			if($postTitle = $blog->sanitizeInput($_GET['postTitle'])){
				$query = "SELECT id FROM blog_post WHERE post_title_url = '".$postTitle."' AND blog_id = '".$blogID."' AND trashed = 0";
				if($postInfo = $blog->queryFetch($query, NULL, 1)){
					$postID = $postInfo['id'];
				}
				else $postID = 0;
			}
			else $postID = 0;
		}
	}
	else if($_GET['category'] || $_GET['catTitle']){
		if(isset($_GET['catTitle'])){
			$query = "SELECT category FROM blog_post WHERE category_url = '".$blog->sanitizeInput($_GET['catTitle'])."' AND blog_id = '".$blogID."'";
			$catInfo = $blog->queryFetch($query, NULL, 1);
			$catConverted = $catInfo['category'];
		}
		else if(isset($_GET['category'])){ //if user is trying to pull up a category
			$category = $_GET['category'];
			$categoryArray = explode("-", $category);
			for($i=1; $i<count($categoryArray); $i++){
				$catConverted .= $categoryArray[$i]." ";
			}
			$catConverted = rtrim($catConverted);
		}
	}
	
	if($blogID==524){
	//	echo 'blog '.$blogID.'<br />';
	//	echo 'cat = '.$catConverted.'<br />';
	//	echo 'postID = '.$postID.'<br />';
	//	var_dump($_GET);
	}
	
	//If user is trying to pull up a post, get post data (and blog id from that post)
	if($postID){
		$post_data = $blog->getSinglePost($postID);
		
		if($post_data){
			$tracking = $post_data['tracking_id'];
			if(!$blogID) $blogID = $post_data['blog_id'];
		}
		else echo "This post not found";
	}
	
	if(!$blogID){
            die('Blog not found');
        }

        if($blog_data = $blog->getBlogs(NULL, 1, $blogID)) $blog_data = $blog->fetchArray($blog_data);
        if(!$blog_data) die('Blog not found');

        $tracking = $blog_data['tracking_id'];

        //if the blog's user is a reseller client
        if($blog_data['user_parent_id']){
                //get reseller's domain for links/images
                $query = "SELECT main_site, private_domain FROM resellers WHERE admin_user = ".$blog_data['user_parent_id'];
                $a = $blog->queryFetch($query);
                if($blog_data['user_parent_id']==7 || $a['private_domain']==1) $parentSite = '6qube.com';
                else $parentSite = $a['main_site'];
        }
        else $parentSite = '6qube.com';

        //get theme info
        $query = "SELECT path, pages FROM themes WHERE id = ".$blog_data['theme'];
        $theme_data = $blog->queryFetch($query);
        $themePath = rtrim($theme_data['path'], '/');
        if(!$themePath) $themePath = 'trans';
        $template = QUBEROOT . 'blogs/themes/'.$themePath.'/index.php';

        //If blog is connected to a hub
        if($blog_data['connect_hub_id']){
                //Get hub info
                $query = "SELECT 
                                * 
                                FROM hub WHERE id = '".$blog_data['connect_hub_id']."'";
                if($hub_row = $hub->queryFetch($query)){
                        if($hub_row['has_tags']){
                        ////////////////////////////////////////////////////////////////////////
                        // REPLACE TAGS WITH VALUES
                        /////////////////////////////////////
                        $hub_row = $hub->replaceTags($hub_row);
                        }
                        if($hub_row['has_custom_form']){
                                //////////////////////////////////////////////////////////////
                                //look through certain fields for form embeds
                                //
                                // Add new custom form embed fields to the array below:
                                $embedFields = array('overview', 'offerings', 'photos', 'videos', 'events', 'edit_region_6', 'edit_region_7', 
                                                                 'edit_region_8', 'edit_region_9', 'edit_region_10', 'edit_region_10', 'add_autoresponder');
                                //////////////////////////////////////////////////////////////
                                $embedForms = $hub->embedForms($embedFields, $hub_row, $hub_row['user_id'], $hub_row['id'], NULL, NULL, $_SESSION['searchEngine'], $_SESSION['seKeyword']);
                                $hub_row = $embedForms['row'];
                                if(!$hasCustomForm) $hasCustomForm = $embedForms['hasCustomForm'];
                        }
                        if($hasCustomForm){
                                //if the hub or hub page has a custom for on it, tack the css and js file embeds into a field that goes in the header
                                $blog_data['adv_css'] = '
                                </style>
                                <link rel="stylesheet" href="/hubs/themes/'.$themePath.'/css/custom_form.css" />
                                <script src="/js/custom-form.js" type="text/javascript"></script>
                                <style>'.$blog_data['adv_css'].'';
                        }
                        //Check to make sure the blog/hub have the same theme
                        if(($blog_data['theme'] == $hub_row['theme']) || ($hub_row['theme'] == '40')){
                                //If theme supports pages, get them
                                if($theme_data['pages'] ){
                                        if($hub_row['pages_version']==2) $v2 = true;
                                        else $v2 = false;
                                        if($hub_pages = $hub->getHubPage($hub_row['id'], NULL, TRUE, TRUE, $v2)){
                                                $hasPages = true;
                                        }
                                }
                                //check to see if user has any articles/press
                                require_once(QUBEADMIN . 'inc/press.class.php');
                                $press = new Press();
                                if($result2 = $press->getPresss($blog_data['user_id'], NULL, NULL, NULL, TRUE)){
                                        if($press->numRows($result2)) $hasPress = true;
                                }
                                require_once(QUBEADMIN . 'inc/articles.class.php');
                                $art = new Articles();
                                if($result2 = $art->getArticles($blog_data['user_id'], NULL, NULL, NULL, TRUE)){
                                        if($art->numRows($result2)) $hasArticles = true;
                                }
                                //add trailing slash to URL if it's not there
                                if(substr($blog_data['website_url'], -1) != "/") $blog_data['website_url'] .= "/";
                                if(substr($hub_row['domain'], -1) != "/") $hub_row['domain'] .= "/";

                                $hubLinked = true;
                        } //end if($blog_data['theme']...
                        else {
                                $hubLinked = false;
                        }
                } //end if($hub_row = $hub->queryFetch($query))
        } //end if($blog_data['connect_hub_id']

// patch for blogs themes that use $row instead of $hub_row;
		$row	=	$hub_row;
// end hack
        require_once($template);
?>
