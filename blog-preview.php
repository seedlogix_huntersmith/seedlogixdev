<?php
require_once(dirname(__FILE__) . '/admin/6qube/core.php');
	$blog_id = $_GET['bid'];
	$post_id = $_GET['pid'];
	$user_id = $_GET['uid'];
	if($_GET['rid'] && is_numeric($_GET['rid'])){
		require_once(QUBEADMIN . 'inc/db_connector.php');
		$db = new DbConnector();
		
		$query = "SELECT main_site FROM resellers WHERE admin_user = '".$_GET['rid']."'";
		$result = $db->queryFetch($query);
		$site = $result['main_site'];
	}
	else $site = '6qube.com';
	
	$src = "http://".$site."/blogs/domains/".$user_id."/".$blog_id."/";
	if($post_id)
		$src .= "title-".$post_id.".htm";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Blog Preview</title>
<link rel="stylesheet" type="text/css" href="http://<?=$site?>/css/frame-window.css"/>
<script type="text/javascript" src="http://<?=$site?>/js/jquery-1.3.2.js"></script>
<script type="text/javascript">
//function to fix height of iframe
var calcHeight = function(){
	var headerDimensions = $('#header-bar').height();
	$('#preview-frame').height($(window).height() - headerDimensions);
}
      
$(document).ready(function(){
	calcHeight();
	
	$('#header-bar a.close').mouseover(function(){
		$('#header-bar a.close').addClass('activated');
	}).mouseout(function(){
		$('#header-bar a.close').removeClass('activated');
	});
	
	$('.hideBar').click(function(){
		$('#header-bar').slideUp('fast', function(){
			$('#preview-frame').height($(window).height());
		});
		return false;
	});
});

$(window).resize(function(){
	calcHeight();
}).load(function() {
	calcHeight();
});
</script>
    
<!--[if IE 6]>
<script type="text/javascript">
$(document).ready(function(){
	$('#close-button').remove();
});
</script>
<![endif]-->
<body>
	<div id="header-bar">
		<p class="meta-data">
			This is a preview of what your blog looks like.<br /><a href="#" class="hideBar">Click here to hide this bar</a>
		</p>
	</div>
	<iframe id="preview-frame" src="<?=$src?>" name="preview-frame" frameborder="0" height="100%"></iframe>
</body>
</html>