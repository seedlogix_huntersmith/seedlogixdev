<?php
/**
 * 4-27-2012
 * implemented queued responses
 * 
 */

die('test');

if($_POST){
    
    
	//include classes
	require_once(QUBEADMIN . 'inc/hub.class.php');
    
        // new
        require_once 'admin/6qube/core.php';
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';
    require_once QUBEPATH . '../models/ContactData.php';
    require_once QUBEPATH . '../models/Response.php';
    require_once QUBEPATH . '../classes/Notification.php';
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/NotificationMailer.php';
    require_once QUBEPATH . '../models/Responder.php';
    require_once QUBEPATH . '../models/CampaignCollab.php';
        // end
    
	$hub = new Hub();
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
        
	$smtp = Mail::factory('smtp', array ('host' => $host));
	
	$formID = is_numeric($_POST['formID']) ? $_POST['formID'] : '';
	$userID = is_numeric($_POST['userID']) ? $_POST['userID'] : '';
	$hubID = is_numeric($_POST['hubID']) ? $_POST['hubID'] : '';
	$hubPageID = is_numeric($_POST['hubPageID']) ? $_POST['hubPageID'] : '';
	$keyword = $hub->validateInject($_POST['keyword']);
	$search_engine = $hub->validateInject($_POST['search_engine']);
	$probablySpam = $_POST['hpot'] ? true : false;
	$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
	if($ip && $hub->checkBlacklist($ip)) $blacklisted = true;
	
	if($formID && $userID && !$blacklisted && !$probablySpam){
		//$query = "SELECT data, options FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
		$form = $hub->getCustomForms($userID, NULL, NULL, $formID, NULL, 1);//;$hub->queryFetch($query);
		if($form['data']){
			$fields = $hub->parseCustomFormFields($form['data']);
			$fieldData = array();
			$i = 0;
			$continue = true;
			//go through each form field and attach the user's data
			foreach($fields as $key=>$values){
				$fieldInputName = $hub->fieldNameFromLabel($values['label']);
				if($values['type']=='checkbox'){
					$fieldValue = $_POST[$fieldInputName] ? 'Yes' : 'No';
					$fieldData[$i] = array();
					$fieldData[$i]['type'] = 'checkbox';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $fieldValue;
				}
				else if($values['type']=='date'){
					$fieldValue = $_POST[$fieldInputName.'_M'].'/';
					$fieldValue .= $_POST[$fieldInputName.'_D'].'/';
					$fieldValue .= $_POST[$fieldInputName.'_Y'];
					$fieldData[$i]['type'] = 'date';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $fieldValue;
				}
				else if($values['type']=='name'){
					$fieldInputName1 = $hub->fieldNameFromLabel($values['label1']);
					$fieldInputName2 = $hub->fieldNameFromLabel($values['label2']);
					$fieldValue1 = $_POST[$fieldInputName1];
					$fieldValue2 = $_POST[$fieldInputName2];
					
					$fieldData[$i]['type'] = 'name_1';
					$fieldData[$i]['label'] = $values['label1'];
					$fieldData[$i]['value'] = $fieldValue1;
					$firstName = $fieldValue1;
					$i++;
					$fieldData[$i]['type'] = 'name_2';
					$fieldData[$i]['label'] = $values['label2'];
					$fieldData[$i]['value'] = $fieldValue2;
					$lastName = $fieldValue2;
				}
				else if($values['type']=='email'){
					$fieldData[$i]['type'] = 'email';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $_POST[$fieldInputName];
					$email = $_POST[$fieldInputName];
					//anti-spam measure -- lots of .'s in email address
					if(substr_count($email, '.')>=6) $continue = false;
				}
				else if($values['type']!='section'){
					$fieldData[$i]['type'] = $values['type'];
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $_POST[$fieldInputName];
					if(stripos($values['label'], 'phone') !== FALSE)
					  $phone = $_POST[$fieldInputName];
					  
					if($values['type']=='text' && $fieldInputName=='name'){
						$name = $_POST[$fieldInputName];
					}
				}
				$fieldInputName = $fieldInputName1 = $fieldInputName2 = $fieldValue = $fieldValue1 = $fieldValue2 = NULL;
				$i++;
			}
			if(!$name) $name = $firstName.' '.$lastName;
			//now go through again to build array string for leads table
			if($fieldData && $continue){
				$dataString = '';
				foreach($fieldData as $field=>$data){
					$dataString .= $data['type'].'|-|'.$data['label'].'|-|'.$data['value'].'[==]';
				}
				//trim trailing [==]
				if(strlen($dataString)>=4) $dataString = substr($dataString, 0, -4);
				//sanitize data string
				$dataString = $hub->sanitizeInput($dataString);
				//gather/sanitize other stuff
				$page = $hub->sanitizeInput($_POST['page']);
				
#amado 2012-05-24 check the lead doesnt already exist!
				$was_sent = FALSE;
				
				      $is_test = $email == 'test@test.com'; // @used below
				      $is_spam = $phone == '123456';
				    if(TRUE)	// 123456 will be considered spam
				    {
				      $stmt = Qube::GetPDO()->prepare('SELECT count(*) FROM leads WHERE lead_email = ? AND data = ? AND lead_form_id = ? AND created + INTERVAL 10 SECOND > NOW()');
				      $stmt->execute(array($email, $dataString, $formID));
				      
				      $was_sent = ($stmt->fetchColumn() > 0);
				      if($was_sent && !$is_spam) $success = TRUE;
				    }
#end
                                // 
				//build string to insert into leads table
				$query = "INSERT INTO leads 
						(user_id, lead_form_id, lead_email, lead_name, hub_id, hub_page_id, data, search_engine, keyword, ip, created) 
						VALUES 
						('".$userID."', '".$formID."', '".$email."', '" . $name . "', '" .$hubID."', '".$hubPageID."',
						 '".$dataString."', '".$search_engine."', '".$keyword."', '".$_SERVER['REMOTE_ADDR']."', NOW())";
				if(!$is_spam && !$was_sent && $hub->query($query)){
                                    
                                    // new
                                        $contactdata_id = $hub->getLastId();
                                        
                                    if(TRUE)
                                    {
                                        try{
                                        $save_data_stmt = Qube::GetPDO()->prepare('INSERT INTO leads_data (lead_id, label, value) VALUES (?, ?, ?)');
                                        Qube::GetPDO()->beginTransaction();
                                        
                                        foreach($fieldData as $index => $data)
                                        {
                                                // @note stripslashes as necessary (enabled by php gpc magic quotes)
                                            $vardata = new VarContainer($data);
                                            $save_data_stmt->execute(array($contactdata_id, $vardata->label, $vardata->value));
                                        }
                                        Qube::GetPDO()->commit();
                                        }Catch(PDOException $e)
                                        {
                                            Qube::Fatal($e);
                                        }
                                    }
                                    
                                        // @todo streamline this process so we query all instant and delayed autoresponders
                                        // for now, just schedule delayed responders
                                        // 
                                        
                                        $D = Qube::GetDriver();
                                        $user = $D->getUserWhere('id = %d', $userID);
                                        $user_has_email_limit = $user->hasEmailLimit();
                                        
                                        // @todo consider evaluating the limit in the cron msg dispatcher script?
                                        if($user_has_email_limit && $user->email_limit <= $user->emails_sent)
                                        {
                                            // limit has been reached
                                            // echo 'limit: ' , $user->email_limit, ' sent: ', $user->emails_sent;
                                        }else{
                                            $Query = $D->queryActiveRespondersWhere('T.table_name = "lead_forms" AND T.table_id = :form_id AND sched>0 AND T.trashed = "0000-00-00"');
#                                                    ->leftJoin(array('trash', 'tr'), 'tr.table_name = "auto_responders" AND tr.table_id = T.id');

                                            if($user_has_email_limit)
                                                $Query->Limit($user->email_limit-$user->emails_sent);
                                            
                                            $statement = $Query->Prepare();
                                            
                                            if($statement->execute(array(':form_id' => $formID)))
                                            {
                                                $responders = $statement->fetchAll();
//                                                deb($responders);
                                                foreach($responders as $r)
                                                {
                                                    // new
                                                    $scheduled_response = new ScheduledResponse();
                                                    $scheduled_response->_set('contactdata_table', 'leads');
                                                    // end
                                                    $scheduled_response->_set('responder_id', $r->id);
                                                    $scheduled_response->_set('contactdata_id', $contactdata_id);
                                                    $scheduled_response->setScheduledTime($r->sched, $r->sched_mode);
                                                    $scheduled_response->Save();
                                                }
                                            }
                                        }
                                        
                                        // end
                                        
					$success = true;
					$key = $value = NULL; //reset these vars for use below
					//get user's info
                                        /*
					$query = "SELECT username, parent_id, super_user FROM users WHERE id = '".$userID."'";
					$userInfo = $hub->queryFetch($query, NULL, 1);
					if($userInfo['super_user']==1) $resellerID = $userID;
					else if($userInfo['parent_id'] && $userInfo['parent_id']!=7) $resellerID = $userInfo['parent_id'];
					if($resellerID){
						//get reseller's main site domain
						$query = "SELECT main_site FROM resellers WHERE admin_user = '".$resellerID."'";
						$resellerInfo = $hub->queryFetch($query, NULL, 1);
						$fromDomain = $resellerInfo['main_site'];
					}
                                         * 
                                         * 
                                         * 
                                         * 
                                         */
                                        
                                        /*
                                         * Outer Joins to the Rescue!
                                         */
                                        $usrq = Qube::GetPDO()->query('SELECT u.username, u.parent_id, u.super_user,
                                                IFNULL(r.main_site, "6qube.com") as domain,
                                                IFNULL(r.company, "6Qube") as company
                                                FROM `users` u LEFT JOIN resellers r ON ((u.parent_id = 0 AND r.admin_user = u.id) or 
                                                    r.admin_user = u.parent_id) WHERE u.id = ' . (int)$userID);
                                        
                                        $userInfo = $usrq->fetch(PDO::FETCH_ASSOC);
                                        $fromDomain = $userInfo['domain'];
					if(!$fromDomain) $fromDomain = '6qube.com';
                                        // end
                                        
					$query = "SELECT name FROM lead_forms WHERE id = '".$formID."'";
					$formInfo = $hub->queryFetch($query, NULL, 1);
					//get hub info
					$query = "SELECT name, pages_version FROM hub WHERE id = '".$hubID."'";
					$hubInfo = $hub->queryFetch($query);
					if($hubPageID){
						$pTable = 'hub_page';
						if($hubInfo['pages_version']==2) $pTable .= '2';
						$query = "SELECT page_title FROM `".$pTable."` WHERE id = '".$hubPageID."' LIMIT 1";
						if($pageInfo = $hub->queryFetch($query)) $pageTitle = $pageInfo['page_title'];
					}
					//email user with form response
					$to = $is_test ? 'test@sofus.mx' : $userInfo['username'];
					$from = new SendTo($userInfo['company'], 'no-reply@' . $fromDomain);    //'no-reply@'.$fromDomain;
					$subject = 'NEW LEAD from '.$formInfo['name'];
					$message = 
					'<html><body>
					<h1>Information Captured</h1>';
					foreach($fieldData as $key=>$value){
						$message .= '<p><strong>'.$value['label'].':</strong><br />'.$value['value'].'</p>';
					}
					$message .= 
					'<br />
					<p><small><strong>HUB Name</strong>: '.$hubInfo['name'].'</small><br />';
					if($pageTitle){
					$message .=  
					'<small><strong>Page Submitted</strong>: '.$pageTitle.'</small><br />';
					}
					if($search_engine){
					$message .=  
					'<small><strong>Search Engine</strong>: '.$search_engine.'</small><br />';
					}
					if($keyword){
					$message .=  
					'<small><strong>Keyword Searched</strong>: '.$keyword.'</small><br />';
					}
					$message .= 
					'<small><strong>IP Address</strong>: '.$_SERVER['REMOTE_ADDR'].'</small></p>
					</body></html>';
					$headers = array('From' => $from,
								  'Reply-To' => $from,
								  'To' => $to,
								  'Subject' => $subject,
								  'Content-Type' => 'text/html; charset=ISO-8859-1',
								  'MIME-Version' => '1.0');
					$mail = $smtp->send($to, $headers, $message);
            
                                        // MODIFIED TO WORK WITH MU FORMS (where form campaign id and collab campaign id are different) 
#            $collaborators  = $D->queryCampaignCollabWhere('(T.scope = "campaign" OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND
#            r.site_id = %d )) AND T.campaign_id = (SELECT cid FROM hub WHERE id = %d) AND T.active = 1 AND T.trashed = "0000-00-00"', $hubID, $hubID)->Exec()->fetchAll();                                        
             $collaborators  = $D->queryCampaignCollabWhere('(
                 T.scope = "campaign"and T.campaign_id = (SELECT cid FROM hub WHERE id = %d)
                 ) OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND r.site_id = %d )', $hubID, $hubID)->Exec()->fetchAll();                       
           
            $collab_msg_qstmt = Qube::GetPDO()->prepare('INSERT INTO collab_email_queue (data_id, data_type, collab_id)
                    VALUES (?, ?, ?)');
            
            if(!$is_test){
                Qube::GetPDO()->beginTransaction();
            foreach($collaborators as $collab)
            {
//                var_dump($collab);
                // enqueue these messages
                    $collab_msg_qstmt->execute(array($contactdata_id, 'lead', $collab->id));
                // end
                $sendto = new SendTo($collab->name, $collab->email);
                $headers = array('From' => $from,
                                          'Reply-To' => $from,
                                          'To' => $is_test ? 'test@sofus.mx' : $sendto,
                                          'Subject' => $subject,
                                          'Content-Type' => 'text/html; charset=ISO-8859-1',
                                          'MIME-Version' => '1.0');
                $mail = $smtp->send($sendto, $headers, $message);
                if(Pear::isError($mail))
                {
                    Qube::Fatal('Failed 6Qube Notification', $mail);
                }
            }
                Qube::GetPDO()->commit();
            }
                                        // @amado loop through collaborators
					$to = $from = $subject = $message = $headers = NULL; //reset vars for email below
					//check for autoresponders
					$query = "SELECT * FROM auto_responders WHERE table_name = 'lead_forms' AND table_id = '".$formID."'
							AND sched = 0 AND active = 1 
							AND auto_responders.trashed = '0000-00-00'";
					$responders = $hub->query($query);
					if($responders && mysqli_num_rows($responders)){
						while($responder = $hub->fetchArray($responders)){
							//if instant, send now
							//before sending check user's remaining email sends
							if($hub->emailsRemaining($responder['user_id'])>0){
								if($name) $to = '"'.$name.'" <'.$email.'>';
								else $to = $email;
								$from = $responder['from_field'] ? $responder['from_field'] : $userInfo['username'];
								$subject = str_replace("#name", $name, $responder['subject']);
								$body = stripslashes($responder['body']);
								$body = str_replace("#name", $name, $body);
								//replace embed tags with values
								foreach($fieldData as $key=>$value){
									$searchTag = $hub->convertKeywordUrl($value['label'], 1, '_');
									$body = str_replace('[['.$searchTag.']]', $value['value'], $body);
									$subject = str_replace('[['.$searchTag.']]', $value['value'], $subject);
									unset($searchTag);
								}
			//					$headers = "From: ".$row2['from_field']."\r\n";
			//					$headers .= "Reply-To: ".$row2['from_field']."\r\n";
			//					$headers .= "MIME-Version: 1.0\r\n";
			//					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
								$headers = array ('From' => $from,
											   'Reply-To' => $from,
											   'To' => $to,
											   'Subject' => $subject,
											   'Content-Type' => 'text/html; charset=ISO-8859-1',
											   'MIME-Version' => '1.0');
								$mail2 = $smtp->send($to, $headers, $body);
								//if mail was sent update user's emails_sent
								if(!PEAR::isError($mail2)){
									$query = "UPDATE users SET emails_sent = emails_sent+1 
											WHERE id = '".$responder['user_id']."'";
									$hub->query($query);
								}
			//					mail($to, $subject, $body, $headers);
								//check/set return URL
								if($responder['return_url']) $redirUrl = $responder['return_url'];
							} //end if($hub->emailsRemaining($responder['user_id'])>0)
						} //end while($responder = $hub->fetchArray($responders))
					} //end if($responders && mysqli_num_rows($responders))
				} //end if($hub->query($query)) [insert into lead_forms table]
			} //end if($fieldData)
		} //end if($form['data'])
		if($form['options'] && !$redirUrl){
			$options = explode('||', $form['options']);
			if($options[2]) $redirUrl = $options[2];
		}
	} //end if($formID && $userID && !$blacklisted)
	else {
		//validation error or IP blacklisted
	}
	
	if($_POST['ajaxSubmit']){
		$return = array();
		$return['success'] = $success ? 1 : 0;
		$return['redir'] = $redirUrl ? $redirUrl : '';
		echo json_encode($return);
	}
	else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
	<? if($success && $redirUrl){ ?>
	window.parent.location = '<?=$redirUrl?>';
	<? } else if($success && !$redirUrl){ ?>
	alert('Your response was successfully sent!');
	<? } else if(!$success){ ?>
	alert('Sorry, there was an error submitting your form.  Please try again.');
	<? } ?>
</script>
</head>
<body></body>
</html>
<?
	}
}
?>