<?
	require_once('admin/inc/local.class.php');
	$local = new Local();
	
	// ** Header ** //
	//require_once('includes/header.inc.php');
	
	if($_POST){ //if the form is submitted
		//validate all the form fields
		$user_id = $local->validateUserId($_POST['email'], 'User ID Email');
		$password = $local->validatePassword($_POST['password'], 'Password', 3, 12);
		$firstName = $local->validateText($_POST['first_name'], 'First Name');
		$lastName = $local->validateText($_POST['last_name'], 'Last Name');
		$phone = $local->validateNumber($_POST['phone'], 'Phone');
		$company = $local->validateText($_POST['company'], 'Company Name');
		$address = $local->validateText($_POST['address'], 'Address');
		$address2 = $_POST['address2'];
		$city = $local->validateText($_POST['city'], 'City');
		$state = $local->validateText($_POST['state'], 'State');
		$zip = $local->validateNumber($_POST['zip'], 'Zip Code');
		isset($_POST['userType']) ? $userType = $_POST['userType'] : $userType = 1;
		
		if($local->foundErrors()){ //check to see if there were any errors
			 $errors = $local->listErrors('<br />'); //if so then store the errors
		}
		else{
			$query = "INSERT INTO users (username, password, access, class) VALUES ('{$user_id}', '{$password}', 0, {$userType})";
			$result = $local->query($query);
			if($insert_id = mysqli_insert_id()){
				$query = "INSERT INTO user_info (user_id, firstname, lastname, phone, company, address, address2, city, state, zip) VALUES ('{$insert_id}', '{$firstName}', '{$lastName}', '{$phone}', '{$company}', '{$address}', '{$address2}', '{$city}', '{$state}', '{$zip}')";
				if($result = $local->query($query)){ //if the query was successful (New user added)
				
				//echo "New user added.<br />";
				
					//if user chose a free account
					if($userType==1){
						$local->sendRegisterMail($insert_id); //Send the user a registration email
					}
					//Create billing account
					$postfields["action"] = "addclient"; 
					$postfields["firstname"] = $firstName;
					$postfields["lastname"] = $lastName;
					$postfields["companyname"] = $company;
					$postfields["email"] = $user_id;
					$postfields["address1"] = $address;
					$postfields["address2"] = $address2;
					$postfields["city"] = $city;
					$postfields["state"] = $state;
					$postfields["postcode"] = $zip;
					$postfields["country"] = "US";
					$postfields["phonenumber"] = $phone;
					$postfields["password2"] = $password;
					$postfields["currency"] = "1";
					$postfields["groupid"] = $userType;
					$postfields["noemail"] = true;
					
					$results = $local->billAPI($postfields);
					
					if ($results["result"]=="success") {
						//successfully created billing account.  update users table with billing accound id
						$query = "UPDATE users SET billing_id = {$results['clientid']} WHERE username = '{$user_id}'";
						$result = $local->query($query);
						
						//echo "Billing account created.<br />";
						//echo "Client ID: {$results['clientid']}.<br />";
						//echo "Username: {$user_id}.<br />";
						
						//create invoice in WHMCS for account type ordered
						//$postfields["action"] = "addorder";
						//$postfields["clientid"] = $results['clientid'];
						//$postfields["pid"] = "3";
						//$postfields["billingcycle"] = "onetime";
						//$postfields["paymentmethod"] = "authorize";
						//$postfields["noemail"] = true;
						
						//$results2 = $local->billAPI($postfields);
						
						//if($results2['result']=='success'){
							header("Location: https://6qube.com/billing/cart.php?a=add&pid=3");
						//}
						//else {
							//echo "Sorry, something went wrong.";	
						//}
						
						//echo $results['clientid']."<br />";
						//display payment page
						//include('admin/inc/forms/signUp-payment.php');
					} else {
						# An error occured
						echo "The following error occured while creating billing account: ".$results["message"];
					}
					
				}//end if($result = $local->query($query))
			}//end if($insert_id = mysqli_insert_id())
			else{
				echo 'Something weird happened.  Please try again?';
			}
		}// end first else
	}//end if($_POST)
	
	else{ //if form has not been posted show form
		include('admin/inc/forms/signUp-billing.php');
	}
	
	//If errors - show them
	if($errors){
		echo '<div id="error">'.$errors.'</div>';
		include('admin/inc/forms/signUp-billing.php');
	}
?>			
	<br /><br />
<?	
	// ** Footer ** //
	//require_once('includes/footer.inc.php');
?>