<?php
/*
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
/**/


    require_once 'admin/6qube/core.php';
    require_once(QUBEADMIN . 'inc/db_connector.php');
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';
    require_once QUBEPATH . '../models/ContactData.php';
    require_once QUBEPATH . '../models/CampaignCollab.php';

    /**
     * 1. Load site configuration.
     * 2. Create a new 'Lead' Object
     * 3. Find matching AutoResponders
     * 4. Generate QueuedResponse Object using AutoResponder Object.
     * 5. Save QueuedResponse
     * 6. Happy Dance
     * 
     */

    require_once(QUBEADMIN . 'inc/validator.php');

    // @todo refactor Validator. It looks like a great class, but should not be subclass of DbConnector
    // it should be a stand-alone class.
    // @todo more user friendly validation!

    $validator = new Validator();

    $name = $validator->validateText($_POST['name'], 'name');
    $email = $validator->validateEmail($_POST['email'], 'email');
    $phone = $validator->validateText($_POST['phone'], 'phone');
    $website = $validator->validateInject($_POST['website']);
    $comments = $validator->validateInject($_POST['comments']);
    $keyword = $validator->validateInject($_POST['keyword']);
    $search_engine = $validator->validateInject($_POST['search_engine']);
    $page = $validator->validateInject($_POST['lead_source_id']);


    $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';	
    $domain = $validator->sanitizeInput($_POST['domain']);
    if(substr($domain, -1)!="/") $domain .= "/";

    $user_id = is_numeric($_POST['user_id']) ? $_POST['user_id'] : '';        
    $type_id = is_numeric($_POST['type_id']) ? $_POST['type_id'] : '';
    $type = $validator->sanitizeInput($_POST['type']);

    //Tiny anti-spam measures
    if(strpos($comments, "<a href=")!==false) $spam = true;
    if($phone=='1010101010' || $phone=='0120120120' || $phone === false) $spam = true;
    if($ip){
            if($validator->checkBlacklist($ip)) $spam = true;
            //$query = "SELECT count(id) FROM blacklist WHERE ip = '".$ip."'";
            //$a = $connector->queryFetch($query);
            //if($a['count(id)']) $spam = true;
    }
    if((!$user_id && !$type && !$type_id) || (!$type && !$type_id)) $spam = true;

    if($validator->foundErrors()){
            $errors = $validator->listErrors('<br />');
            header("Location: ".$_POST['domain']."?error=$errors");
            exit;
            //echo $domain;
    }
    else if($spam){
            //header("Location: http://www.spam.com");
            header("Location: ".$_POST['domain']."?error=Suspected%20spam%20attempt.");
            exit;
    }

    // no spam and no errors.. continue

    // sort out some discrepancies.. 
    $_POST['website']   = $_POST['domain'];   
    $_POST['page']      = $_POST['lead_source_id'];
    $_POST['keyword']   = isset($_POST['keyword']) ? $_POST['keyword'] : '';
    $_POST['ip']        = $ip;
    $_POST['search_engine'] = $search_engine;        

//    echo 'ok';exit;
    $FormProcessor = $qube->getContactFormProcessor();
    $FormProcessor->Execute(new VarContainer($_POST));  // @todo use sanitized variables??

    $redirect = $FormProcessor->getReturnURL();

    if(!$redirect)
    {
        Header('Location: /');  // redirect to main site ($_POST['domain'] is empty on pastephp.com idk, why it may be isolated issue to the forever theme. )
        exit;
    }
    ////////////////////////////////////////////////////
    // REDIRECT TO SPECIFIED URL OR BACK TO FORM
    ////////////////////////////////////////////////////
    if($redirect && (substr($redirect, 0, 7)!='http://') && (substr($redirect, 0, 8)!='https://')) $redirect = 'http://'.$redirect;
    header('Location: '. $redirect);

    // removed a whole bunch of disabled code that was lingering in process-form.php
