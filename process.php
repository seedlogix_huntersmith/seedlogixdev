<?php 
require_once(dirname(__FILE__) . '/admin/6qube/core.php');
session_start();
if($args = trim($_GET['args'], '/')) $args_array = explode('/', $args);
$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
$continue = false;
//////////////////////////////////////////////////////////////////////////////////////////////////////
// SECURITY CHECKS / BLACKLISTING														//
if((count($args_array)==2 || count($args_array)==3) && $ip && ($_SERVER['HTTP_HOST']=='6qube.com')){
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	//first check IP blacklist
	if(!$reseller->checkBlacklist($ip)){
		//then check attempts
		if(!$_SESSION['secHash_attempts'] || ($_SESSION['secHash_attempts'] <= 3)){
			$uid = is_numeric($args_array[0]) ? $args_array[0] : '';
			$secHash = $args_array[1];
			if($args_array[2]==1) $addSite = true;
			$salt = 'SFGDSFG*Jsem fxdfxd,x,Z >< >< jxdnf2';
			$pepper = 's$TS$EGU$NGdg,cg cxcxXMMMM ;;;{} (.)(.) ( Y )';
			$correctSecHash = md5($salt.$uid.$pepper);
			$correctSecHash = md5($pepper.$correctSecHash);
			if($secHash == $correctSecHash){
				//lastly make sure it's a real user
				$query = "SELECT billing_id, billing_id2, username, class FROM users WHERE id = '".$uid."'";
				if($userInfo = $reseller->queryFetch($query)){
					$_SESSION['secHash_attempts'] = 0;
					$continue = true;
				}
				else {
					$_SESSION['secHash_attempts'] = isset($_SESSION['secHash_attempts']) ? $_SESSION['secHash_attempts']+1 : 1;
					$quit = 5;
				}
			}
			else {
				$_SESSION['secHash_attempts'] = isset($_SESSION['secHash_attempts']) ? $_SESSION['secHash_attempts']+1 : 1;
				$quit = 4;
			}
		}
		else {
			//add to blacklist if not already
			$reseller->blacklistIP($ip, NULL, 'sechash_fail');
			$quit = 3;
			session_destroy();
		}
	} //end if($ip && !$reseller->checkBlacklist($ip))
	else $quit = 2;
}
else $quit = 1;
// END SECURITY CHECKS																//
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//if passed security validations
if($continue){
	//get more user info
	$query = "SELECT firstname, lastname, email, phone, company, address, address2, city, state, zip 
			FROM user_info WHERE user_id = '".$uid."'";
	$userInfo2 = $reseller->queryFetch($query);
	//get product info
	$query = "SELECT type, name, description, setup_price, monthly_price, add_site 
			FROM user_class WHERE id = '".$userInfo['class']."'";
	$productInfo = $reseller->queryFetch($query);
	//calculate total due
	$totalDue = $productInfo['monthly_price'];
	if($productInfo['setup_price']>0) $totalDue += $productInfo['setup_price'];
	if($productInfo['add_site']>0 && $addSite) $totalDue += $productInfo['add_site'];
	
	if($_POST){
		//if user has submitted the form, include the processing script
		$firstName = $_POST['first_name'];
		$lastName = $_POST['first_name'];
		$phone = $_POST['phone'];
		$company = $_POST['company'];
		$address = $_POST['address'];
		$address2 = $_POST['address2'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip = $_POST['zip'];
		$email = $userInfo['username'];
		//card info
		$cardNumber = $reseller->stripNonAlphaNum($_POST['cardNumber']);
		$expMonth = $reseller->stripNonAlphaNum($_POST['expMonth']);
		$expYear = $reseller->stripNonAlphaNum($_POST['expYear']);
		$cvv = $reseller->stripNonAlphaNum($_POST['cvv']);
		//payment stuff
		//$uid, $addSite and $totalDue set above
		$productID = $userInfo['class'];
		$monthlyCost = $productInfo['monthly_price'];
		if($totalDue > $monthlyCost) $setupCharge = ($totalDue - $monthlyCost);
		if($addSite) $addSiteCost = $productInfo['add_site'];
		$a = explode('.', $productInfo['type']);
		$refID = substr($a[1], 0, 14).'_'.$uid;
		$productName = $productInfo['name'];
		
		$sessID = session_id();
		include('includes/process-payment.php');
	}
	else {
		$firstName = $userInfo2['firstname'];
		$lastName = $userInfo2['lastname'];
		$phone = $userInfo2['phone'];
		$company = $userInfo2['company'];
		$address = $userInfo2['address'];
		$address2 = $userInfo2['address2'];
		$city = $userInfo2['city'];
		$state = $userInfo2['state'];
		$zip = $userInfo2['zip'];
	}
	$submitURL = 'https://6qube.com/process/'.$uid.'/'.$secHash.'/';
	if($addSite) $submitURL .= '1/';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elements Internet Marketing Software Create Account: Billing</title>
<link rel="stylesheet" href="https://6qube.com/hubs/themes/elements/css/style_secure.css" />
<link rel="stylesheet" href="https://6qube.com/hubs/themes/elements/css/uniform.aristo.css" />
<link rel="shortcut icon" href="https://6qube.com/img/6qube_favicon.png" />
<link rel="apple-touch-icon" href="https://6qube.com/img/6qube_favicon.png" />
<!--jquery library -->
<script type="text/javascript" src="https://6qube.com/hubs/themes/elements/js/jquery-1.6.1.min.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="https://6qube.com/hubs/themes/elements/css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="https://6qube.com/hubs/themes/elements/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="https://6qube.com/hubs/themes/elements/js/scroll.js"></script>
<script type="text/javascript" src="https://6qube.com/hubs/themes/elements/js/jquery.uniform.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".uniformselect").uniform();
});
</script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="https://6qube.com/hubs/themes/elements/css/ie6.css" type="text/css" />
<![endif]-->
<style type="text/css">
label { font-size: 16px; }
#errors {
	padding: 10px;
	border: 2px dashed #C33;
	background-color: #FFEDED;
	font-size: 16px;
	font-weight: bold;
	color: #000;
	line-height: 22px;
}
#warnings {
	padding: 10px;
	border: 2px dashed #AAA;
	background-color: #EEE;
	font-size: 16px;
	font-weight: bold;
	color: #000;
	line-height: 22px;
}
.uniformselect {
	height: 32px;
}
div.selector span { width: 100px; overflow: hidden; }
</style>
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
						<a title="Internet Marketing Software | Elements by 6Qube" href="/">
						<img src="https://6qube.com/hubs/themes/elements/images/logo.png" />
						</a>
					</div>
				</div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu"><div id="top_menu_right"><div id="top_menu_bg"><div id="ddtopmenubar">
						<ul>
							<li><a href="http://elements.6qube.com" title="Internet Marketing Software | Elements by 6Qube"><span>Home</span></a></li>
							<li><a href="http://elements.6qube.com/internet-marketing-software-about/" title="About Internet Marketing Software Elements by 6Qube"><span>About Us</span></a></li>
							<li><a href="http://elements.6qube.com/internet-marketing-software-features/" rel="ddsubmenu1" title="Internet Marketing Software Features | Elements Internet Marketing Tools"><span>App Features</span></a></li>
							<li><a href="http://elements.6qube.com/internet-marketing-tools-signup/" class="activelink" title="Sign-up for Internet Marketing Software Elements | Professional | Agency"><span>Sign Up</span></a></li>												
							<li><a href="http://elementsblog.6qube.com/" title="Internet Marketing Software Blog | Internet Marketing Tools"><span>Blog</span></a></li>
							<li><a href="http://elements.6qube.com/internet-marketing-software-contact/" title="Contact Internet Marketing Software Elements by 6Qube"><span>Contact</span></a></li>
							<li><a href="http://login.6qube.com/" title="Login to Elements | Internet Marketing Software"><span>Login</span></a></li>
						</ul>
						<script type="text/javascript">
							ddlevelsmenu.setup("ddtopmenubar", "topbar");
						</script>	
						<ul id="ddsubmenu1" class="ddsubmenustyle">
							<li><a href="http://elements.6qube.com/seo-website-builder/">SEO Website Builder</a></li>
							<li><a href="http://elements.6qube.com/seo-blogging-platform/">SEO Blogging Platform</a></li>
							<li><a href="http://elements.6qube.com/seo-landing-page-builder/">SEO Landing Pages</a></li>
							<li><a href="http://elements.6qube.com/advanced-seo-platform/">Advanced SEO Platform</a></li>
							<li><a href="http://elements.6qube.com/email-marketing-system/">Email Marketing System</a></li>
							<li><a href="http://elements.6qube.com/prospect-management-system/">Prospect Management</a></li>
							<li><a href="http://elements.6qube.com/website-replication-manager/">Replication Manager</a></li>
							<li><a href="http://elements.6qube.com/domain-management-system/">Domain Management</a></li>
							<li><a href="http://elements.6qube.com/social-media-marketing-systems/">Social Media Marketing</a></li>
							<li><a href="http://elements.6qube.com/advanced-analytics/">Advanced Analytics</a></li>
						</ul><!--top menu ul--> 
					</div></div></div></div>
					<!--End Top Menu-->
					<br style="clear:both;" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<h1>Create Elements Internet Marketing Software Account</h1>
					<p></p>
					<br style="clear:both;" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container_3-->
			<div id="container_3">
				<!--Start Container_3 Top-->
				<div id="container_3_top">
					<!--Start Container_3 Bottom-->
					<div id="container_3_bottom">
						<!--Start Breadcrumb-->
						<div id="breadcrumb">
							<ul>
								<li>You Are Here:</li>
								<li><a title="Internet Marketing Software | Elements by 6Qube" href="http://elements.6qube.com">Home</a></li>
								<li><a title="Internet Marketing Software Signup" href="http://elements.6qube.com/internet-marketing-tools-signup/">Sign Up</a></li>
								<li class="boldbreadcrumb">Create Billing Profile</li>						 
							</ul>
						</div>
						<!--End Breadcrumb-->
						<br style="clear:both;" />

<!--Start Content-->
<div id="content" style="width:100%;">
	<h1 style="color:#000;"">Secure Billing Setup</h1><br />
<? if(!$userInfo['billing_id2'] && !$paymentSuccess){ ?>
	<h2><?=$productInfo['name']?></h2>
	<p>
		You're almost done!  Just verify your billing information below and provide a payment method to set up a secure billing profile.<br /><br />
		Based on your account type choice you will be billed the following:<br />
		<strong>Monthly Price:</strong> $<?=sprintf('%01.2f', $productInfo['monthly_price'])?><br />
		<? if($productInfo['setup_price']!=0){ ?>
		<strong>One-Time Setup Fee:</strong> $<?=sprintf('%01.2f', $productInfo['setup_price'])?><br />
		<? } ?>
		<? if($productInfo['add_site']>0 && $addSite){ ?>
		<strong>Custom Website:</strong> $<?=sprintf('%01.2f', $productInfo['add_site'])?><br />
		<? } ?>
		<br />
		The total due right now is: $<strong><?=sprintf('%01.2f', $totalDue);?></strong>
	</p>
	<p>Verify and complete the form below to set up your billing profile and activate your account.</p>
		<? if($errors){ echo '<div id="errors">'.$errors.'</div>'; } ?>
		<form action="<?=$submitURL?>" class="jquery_form" method="post" id="sign-up">
			<!--Start Top Shadow-->
			<div class="top_shadow">
				<label>First Name</label>
				<input type="text" name="first_name" value="<?=$firstName?>" /><br />
				<label>Last Name</label>
				<input type="text" name="last_name" value="<?=$lastName?>" /><br />
				<label>Phone Number</label>
				<input type="text" name="phone" value="<?=$phone?>" /><br />
				<label>Company Name</label>
				<input type="text" name="company" value="<?=$company?>" /><br />
				<label>Address</label>
				<input type="text" name="address" value="<?=$address?>" /><br />
				<label>Address 2 <small>(optional)</small></label>
				<input type="text" name="address2" value="<?=$address2?>" /><br />
				<label>City</label>
				<input type="text" name="city" value="<?=$city?>" /><br />
				<label>State</label>
				<?=$reseller->stateSelect('state', $state)?>
				<label>Zip Code</label>
				<input type="text" name="zip" size="5" maxlength="5" value="<?=$zip?>" style="width:80px;" /><br /><br />
				<br /><br />
			</div>
			<!--End Top Shadow-->
			<!--Start Top Shadow-->
			<div class="top_shadow">
				<label>Credit Card Number <small>(numbers only)</small></label>
				<input type="text" name="cardNumber" value="" maxlength="16" autocomplete="off" /><br />
				<label>Card Expiration Date</label>
				<div style="display:inline-block;margin-top:-5px;width:130px;">
				<select name="expMonth" class="uniformselect">
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
				</div>&nbsp;
				<div style="display:inline-block;width:150px;">
				<select name="expYear" class="uniformselect">
					<option value="2011">2011</option>
					<option value="2012">2012</option>
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
				</select>
				</div>
				<label style="margin-top:-10px;">CVV Code <small>(3-digit code on back)</small></label>
				<input type="text" name="cvv" value="" size="4" maxlength="4" autocomplete="off" style="width:80px;" />			
			</div>
			<!--End Top Shadow-->
			<br /><br /><br />
			<input type="image" src="https://6qube.com/hubs/themes/elements/images/start-button.png" style="width:108px;height:33px;padding:0;background:none;border:none;" />
		</form>
	<br style="clear:both;" />
<? } else if(!$paymentSuccess){ ?>
	<p>It appears this account already has a billing profile set up.  If this is in error please <a href="http://elements.6qube.com/internet-marketing-software-contact/">contact our support team</a>.</p>
<? } else {?>
	<h2>Success!</h2>
	<p>Your new account has been successfully paid for and activated.  You should receive a confirmation email soon.</p>
	<br />
	<p>If you're ready to start using Elements, <a href="http://6qube.com/admin">click here to log in now</a>.</p>
<? } ?>
</div>
<!--End Content-->
						<!--Start Right Panel-->
						<div id="right_panel">
						<? //require_once(QUBEROOT . 'includes/rightPanelE.inc.php'); ?>
						&nbsp;
						</div>
						<!--End Right Panel-->
						<br style="clear:both;" />
					</div>
					<!--End Container_3 Bottom-->
				</div>
				<!--End Container_3 Top-->
			</div>
			<!--End Container_3-->
		</div>
		<!--End Page Container-->
		<?
		// ** Footer ** //
		require_once(QUBEADMIN . '/includes/v2Efooter_secure.inc.php');
		?>
<!-- end -->
</body>
</html>
<? } else echo 'Access denied. '.rand().$quit; ?>