<?php

/* 
 * Author:     Jon Aguilar
 * License:    
 */

if( 0 ){
	ErrorPages::ServiceUnavailable();
	exit;
}

defined('HYBRID_PATH') || die('Access Denied.');

$RouterRequest	=	RouterRequest::loadWebRequest();
$Router = new HybridRouter();
$Router->setRouterRequest($RouterRequest);

if(TRUE === $Router->Dispatch($RouterRequest))
{
    // all done. // all done
    return;
}

$host	=	$RouterRequest->getHost();
$host_full   =   $RouterRequest->getHostString();
$is_www_request =   preg_match('/^www\./i', $host_full);
$host       =   $is_www_request ? preg_replace('/^www\./i', '', $host_full) : $host_full;
$is_preview =   !$is_www_request && preg_match('/^hubpreview./i', $host_full);
$request    =   @reset(explode('?', $_SERVER['REQUEST_URI']));    // GET THE raw REQUEST STRING WITHOUT THE GET VARS

if(preg_match('/([^.]+).6qube.com$/', $host, $mx) || ($resellerNetworkStmt    =   HybridRouter::findResellerNetworkService($host)))   // find entry in resellers_network
{
    // uses $row['domain'] for theme url
    // to replace $row['domain'] with $baseUrl I used
    /*
     *  find -type f -name '*.php' -exec sed -i 's/<?=\$row\['"'"'domain'"'"']?>\/reseller/<?=\$baseUrl?>\/reseller/g' {} \;
     */
	if($resellerNetworkStmt){
		$info   =   $resellerNetworkStmt->fetch();
	}else{
	    deb($mx);
		$info = (object)array('type' => $mx[1]);
	}
    /* @var $info ResellerNetworkModel */
    
#    var_dump($info);
    
    $siteInfo   =   (array)$info;
    $_GET['args']   =   ltrim(rtrim($request, '/'), '/');    
    
    $_SERVER['SERVER_NAME'] =   $host;
	if($host=='local.6qube.com'){
		require_once QUBEROOT . $info->type . '/v2.php';
	} else {
		switch($info->type)
			{
				case 'search':
					unset($_GET['args']);   // unset $_get['args'] for search. cuz its wack            
				case 'articles':
				case 'blogs':               // blogs network
				case 'press':
				case 'browse':
				case 'hubs':
				case 'local':
					require_once QUBEROOT . $info->type . '/index.hybrid.php';      // example: $root/(blogs|press|search|..)/index.hybrid.php
					break;

				default:
				die(ErrorPages::PageNotFound("Network not found."));
		#            throw new HubNotFoundException('The ResellerNetwork has invalid type');
			}
		
	}
    
    
}elseif(HybridRouter::isResellerAdmin($host)){
    $VCG	=	VarContainer::getContainer($_GET);
    $VCP	=	VarContainer::getContainer($_POST);
	if($VCG->checkValue('page', $page) && $page === 'client-optout' && $VCG->checkValue('email', $email) && $VCG->checkValue('pid', $pid))
	{
		$optout_result	=	NULL;
		if($VCP->exists('submit') && $VCP->checkValue('email', $email) && $VCP->checkValue('pid', $pid))
		{
			$optout_result	=	new ValidationStack();
			$SDA	=	new SystemDataAccess();
			$count	=	$SDA->doOptOut($pid, $email);
			if($count < 1)
			{
				$optout_result->addError('OptOut', 'Could not confirm optout result.');
			}
		}
	     require_once HYBRID_PATH . '/error/optout.php';
	}else if($RouterRequest->getRequestPath() == '/'){
				
			Header('Location: /admin.php');

	}else{
//		Header('Location: /admin.php');
		header('HTTP/1.0 404 Not Found');

		ErrorPages::PageNotFound('Error. page not found');
	}
}else{
    
    // @todo RARE CASE ecxception here is that hub does not exist, but reseller DOES EXIST. so we must fix the db
    // so the hub httphostkey column exists for the $host value (check reseller main_site_id to find the hub to fix)

#	Header('HTTP/1.0 503 Service Unavailable.');

#	echo 'Temporarily Down for maintenance. The service will be available in the next 5 minutes. Thank you for your patience.';	
#exit;
	header('HTTP/1.0 404 Not Found');    

	ErrorPages::PageNotFound('Error. domain not found: ' . $host);

//	echo 'This Page is unavailable.<br />';
//	echo '404 Error. domain not found: ' . $host;
#    throw new HubNotFoundException('Hub Not Found');
}
#var_dump($GLOBALS);
