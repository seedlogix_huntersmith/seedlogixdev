<?php
require_once '../../../hybrid/bootstrap.php';

@session_start();

$siteid = $_GET['siteid'];

if($siteid){

$hub = WebsiteModel::Fetch('id = %d', $siteid);


$preview_url = sprintf('https://hubpreview.6qube.com/i/%d/%d', $hub->user_id, $siteid);

?>

<!DOCTYPE html>
<html lang="en">

<head>
	
	<!-- Meta -->
	<meta charset="utf-8">
	<title><?=$hub->name?> Preview</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	
	<!-- Css -->

	<!--
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/template.css" rel="stylesheet" media="screen">
	-->

	<script>if ( top !== self ) top.location.replace( self.location.href );// Hey, don't iframe my iframe!</script>

	<link href="css/template.min.css" rel="stylesheet" media="screen">

	<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,300' rel='stylesheet' type='text/css'>
	
	<!--[if lt IE 9]>
		<script>/*@cc_on'abbr article aside audio canvas details figcaption figure footer header hgroup mark meter nav output progress section summary subline time video'.replace(/\w+/g,function(n){document.createElement(n)})@*/</script>
	<![endif]-->

    
</head>
	
<body>

<!-- Header -->
<header class="switcher-bar clearfix">

	<!-- Logo
	<div class="logo textual pull-left">
		<a href="#" title="Switcher">

		</a>
	</div>-->

	<!-- Product Switcher -->
	<div class="product-switcher pull-left">
		<a href="#" title="Select a Product">
			<span>+</span>
		</a>
	</div>



	<!-- Mobile Button -->
	<div class="mobile-btn header-btn pull-right hidden-xs">
		<a href="#" title="Smartphone View" class="icon-mobile-phone"></a>
	</div>

	<!-- Tablet Button -->
	<div class="tablet-btn header-btn pull-right hidden-xs">
		<a href="#" title="Tablet View" class="icon-tablet"></a>
	</div>

	<!-- Desktop Button -->
	<div class="desktop-btn header-btn pull-right hidden-xs">
		<a href="#" title="Desktop View" class="icon-desktop"></a>
	</div>

</header>

<!-- Products List -->
<section class="switcher-body">

	<a href="#" title="Prev" class="icon-chevron-left products-prev"></a>

	<div class="products-wrapper">
		<div class="products-list clearfix">

		</div>
	</div>

	<a href="#" title="Next" class="icon-chevron-right products-next"></a>

</section>


<!-- Product iframe -->
<iframe class="product-iframe" frameborder="0" border="0"></iframe>

<!-- Preloader -->
<div class="preloader"></div>




<!-- Javascript -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
    var $products,
        $current_product = 'theme';

    // List all the products here
    $products = {

        theme : {
            name     : '<?=$hub->name?>',
            tag      : '',
            img      : '',
            url      : '<?=$preview_url?>',
            purchase : '',
            tooltip  : '<?=$hub->name?>'
        }

    };

</script>

<!--
<script src="js/libs/detectmobilebrowser.js"></script>
<script src="js/libs/jquery.mousewheel.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/application.js"></script>
-->

<script src="js/application.min.js"></script>



	
</body>

</html>
<?php } else echo 'Not Authorized'; ?>