<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:43 AM

 * @var LeadFormFieldModel $f
 */
    require 'label.php';
?>
<?=$themeWrapper->inputPrefix?>
    <input type="text"
           name="<?=$f->getInputName()?>"
           class="<?=$f->isRequired() ? 'required' : ''?> <?=$f->type?> <?=$f->size?> <?=$f->numeric ? 'numsOnly' : ''?>"
           placeholder="<?= $f->getDefaultValue(); ?>"
           value = "" />
    <?php if( ($f->hasLabelNote()) && ($themeWrapper->notePos == 'input') ):?>
        <?=$themeWrapper->notePrefix?><?=$f->getLabelNote()?><?=$themeWrapper->noteSuffix?>
    <?php endif; ?>
<?=$themeWrapper->suffix?>