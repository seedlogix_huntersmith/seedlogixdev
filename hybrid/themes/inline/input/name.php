<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:31 AM
 */
/** @var LeadFormFieldModel $f */

    if($themeWrapper->defaultWrap == 'custom'):
?>
        <?=$themeWrapper->containerPrefix?>
            <?=$themeWrapper->labelPrefix?><label><?= $f->getLabel(0); ?></label><?=$themeWrapper->suffix?>
            <?=$themeWrapper->inputPrefix?><input type="text" name="<?= $f->getInputName(0); ?>[0]" placeholder="<?=$field['default1']?>" value="" <? if($field['required1']){ ?>class="required"<? } ?> /><?=$themeWrapper->suffix?>
        <?=$themeWrapper->suffix?>
        <?=$themeWrapper->containerPrefix?>
            <?=$themeWrapper->labelPrefix?><label><?= $f->getLabel(1); ?></label><?=$themeWrapper->suffix?>
            <?=$themeWrapper->inputPrefix?><input type="text" name="<?= $f->getInputName(1); ?>[1]" placeholder="<?=$field['default2']?>" value="" <? if($field['required2']){ ?>class="required"<? } ?> /><?=$themeWrapper->suffix?>
        <?=$themeWrapper->suffix?>
<?php
    elseif($themeWrapper->defaultWrap == 'legacy'):
        echo $themeWrapper->containerPrefix;
?>
        <div style="display:inline-block;" class="name-field">
            <label><?= $f->getLabel(0); ?></label>
            <input type="text" name="<?= $f->getInputName(0); ?>[0]" placeholder="<?=$field['default1']?>" value="" <? if($field['required1']){ ?>class="required"<? } ?> />
        </div>
        <div style="display:inline-block;" class="name-field">
            <label><?= $f->getLabel(1); ?></label>
            <input type="text" name="<?= $f->getInputName(1); ?>[1]" placeholder="<?=$field['default2']?>" value="" <? if($field['required2']){ ?>class="required"<? } ?> />
        </div>
<?php
        echo $themeWrapper->suffix;
    else:
?>
        <div style="display:inline-block;" class="name-field">
            <label><?= $f->getLabel(0); ?></label>
            <input type="text" name="<?= $f->getInputName(0); ?>" placeholder="<?=$field['default1']?>" value="" <? if($field['required1']){ ?>class="required"<? } ?> />
        </div>
        <div style="display:inline-block;" class="name-field">
            <label><?= $f->getLabel(1); ?></label>
            <input type="text" name="<?= $f->getInputName(1); ?>" placeholder="<?=$field['default2']?>" value="" <? if($field['required2']){ ?>class="required"<? } ?> />
        </div>
<?php
    endif;
?>

