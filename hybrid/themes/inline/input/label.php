<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:42 AM
 * @var LeadFormFieldModel $f
 */
?>
<?=$themeWrapper->labelPrefix?>
    <label>
        <?=$f->getLabel()?>
        <?php if( ($f->hasLabelNote()) && ($themeWrapper->notePos == 'label') ):?>
        <?=$themeWrapper->notePrefix?><?=$f->getLabelNote()?><?=$themeWrapper->noteSuffix?>
        <?php endif; ?>
    </label>
<? if($themeWrapper->labelPrefix != ''){echo $themeWrapper->suffix;}?>