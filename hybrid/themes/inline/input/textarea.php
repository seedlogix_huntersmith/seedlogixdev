<?php
    /** @var LeadFormFieldModel $f */
require 'label.php';
?>
<?=$themeWrapper->inputPrefix?>
   <textarea name="<?= $f->getInputName(); ?>" class="<?php if($f->isRequired()) echo 'required'; ?> <?=$f->size?>" placeholder="<?= $f->getDefaultValue(); ?>"></textarea>
<?=$themeWrapper->suffix?>