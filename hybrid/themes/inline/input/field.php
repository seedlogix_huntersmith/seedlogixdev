<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:31 AM
 */
    if( ($control == 'name') || ($control == 'hidden') || ($control == 'section') ):
        require "$control.php";
    else:
        echo $themeWrapper->containerPrefix;
        require "$control.php";
        echo $themeWrapper->suffix;
    endif;
?>
