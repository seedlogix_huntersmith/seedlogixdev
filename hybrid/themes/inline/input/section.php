<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:51 AM
 */
/** @var LeadFormFieldModel $f */
	if( $themeWrapper->defaultWrap == 'custom' ): ?>
		<div class="whead">
			<h6 class="cfLabel"><?=$f->getLabel()?></h6>
		</div>
<?php elseif($themeWrapper->defaultWrap == 'legacy'): ?>
		<h2 class="cfLabel"><?=$f->getLabel()?></h2>
<?php endif; ?>