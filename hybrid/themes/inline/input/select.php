<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:46 AM
 */
/** @var LeadFormFieldModel $f */
require 'label.php';
?>
<?=$themeWrapper->inputPrefix?>
    <select name="<?= $f->getInputName(); ?>" class="<?=$f->isRequired() ? 'required' : ''?> <?=$f->size?>">
        <?php foreach($f->getOptions() as $option):?>
            <option value="<?=$option?>"><?=$option?></option>
        <?php endforeach;?>
    </select>
    <?php if( ($f->hasLabelNote()) && ($themeWrapper->notePos == 'input') ):?>
        <?=$themeWrapper->notePrefix?><?=$f->getLabelNote()?><?=$themeWrapper->noteSuffix?>
    <?php endif; ?>
<?=$themeWrapper->suffix?>