<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/27/2014
 * Time: 2:54 AM
 */
/** @var LeadFormFieldModel $f */
?>
<?=$themeWrapper->inputPrefix?>
<div class="checkboxDiv">
	<input type="checkbox" name="<?=$f->getInputName()?>" value="1" class="<?=$f->isRequired() ? 'required' : ''?> <?= $f->size; ?>"/>
	<?php require 'label.php'; ?>
</div>
<?php
echo $themeWrapper->suffix;
?>

