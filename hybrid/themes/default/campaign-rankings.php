<?php
?>

<div class="fluid">
    <div class="widget">
        <div class="whead">
            <h6>Rankings</h6>
            <input name="exportURL" type="hidden" class="action_url" value="<?= $this->action_url('piwikAnalytics_getCSVData?chart=RankingSites&PERIOD=ALL&INTERVAL=7&SORTCOL=0&SORTDIR=ASC&cid='.$Campaign->getID()); ?>">
            <a id="exportCampaign" title="Export the Results" href="<?= $this->action_url('piwikAnalytics_getCSVData?chart=RankingSites&PERIOD=ALL&INTERVAL=7&SORTCOL=0&SORTDIR=ASC&cid='.$campaign_id); ?>" class="bDefault f_rt"><span class="export"></span></a>
            <div class="buttonS f_rt jopendialog jopen-createkeywordcampaign"><span>Create a New Keyword Campaign</span></div>
        </div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('RankingSites'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'ranking-sites',
	'data'				=> $rankingSites,
	'rowkey'			=> 'site',
	'rowtemplate'		=> 'campaign-rankings.php',
	'total'				=> $countRankingSites,
	'sourceurl'			=> $this->action_url('Campaign_ajaxRankingSites',array('ID' => $campaign_id,'user_id' => $user_id)),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div>
</div>

<!-- Create Form POPUP -->
<?php $this->getDialog('create-keywordCampign.php'); ?>


<script type="text/javascript">
<?php $this->start_script(); ?>
	$(function (){
		// Confirm delete already being handled with jQuery Dialog
		/*
		var dataTable = $('.dataTable');
		var handler = dataTable.data('events').click[0].handler;
		dataTable.unbind('click');
		dataTable.on('click', '.jajax-action', function(e){
			if(confirm('Are you sure?'))
				return handler(e);
			else
				return false;
		});
		*/

        //Select Box in dialog
        $('#selectRankingTp').on('change',function(){
            var customSelect = $('#keyword_campaign_url');
            if($(this).val() === null){
                customSelect.removeClass('disabled').removeAttr('disabled');
            } else{
                customSelect.addClass('disabled').attr('disabled','disabled');
            }
        });
        
		//===== Create New Form + PopUp =====//
		$('#jdialog-createkeywordcampaign').dialog(
		{
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
                $('[name="ranking_site[ID]"]', this).val('0');
                $('#selectRankingTp').val(null).trigger("change");
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
                    if(!ajax.error) {
                        $('.jnoforms').hide();
                        var dt =$('#ranking-sites');
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.ID);
						dt.data('newRowsIds', newRowsIds);
                        dt.dataTable().fnDraw();
                        notie.alert({text:ajax.message});
                        this.close();
                    }
                    else if(ajax.error == 'validation') {
                        alert(getValidationMessages(ajax.validation).join('\n'));
                    }
                    else
                        alert(ajax.message);

				}, '#jdialog-createkeywordcampaign')).buttons
		});

        $('#ranking-sites').on('click', '.edit-dialog', function(e){
            var jdialog = $('#jdialog-createkeywordcampaign');
            var row = $(this).closest('tr');
            var hostnameLink = row.find('span.hostname a');
            var siteID = hostnameLink.attr('href').match(/site_ID=([0-9]+)/)[1];
            var hostname = hostnameLink.text();
            var touchpoint_ID = row.find('[name="touchpoint_ID"]').val();
            jdialog.find('[name="ranking_site[ID]"]').val(siteID);
            if(touchpoint_ID != '0'){
                jdialog.find('#selectRankingTp').val(touchpoint_ID);
            }else{
                jdialog.find('#selectRankingTp').val(null);
                jdialog.find('[name="ranking_site[hostname]"]').val(hostname);
            }
            jdialog.find('#selectRankingTp').trigger("change");
            jdialog.dialog('option', 'title', 'Edit Keyword Campaign');
            jdialog.dialog('open');
        })
	});

<?php $this->end_script(); ?>
</script>