<div class="fluid">
    <div class="widget">
        <div class="whead">
            <h6>Notifications</h6>
            <div class="buttonS f_rt jopendialog jopen-createresponse"><span>Create a New Response</span></div>
        </div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('CampaignCollabs'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'campaignCollabs',
	'data'				=> $collabs,
	'rowkey'			=> 'collab',
	'rowtemplate'		=> 'campaign-collab.php',
	'total'				=> $totalCollabs,
	'sourceurl'			=> $this->action_url('Campaign_ajaxCollabs',array('ID' => $Campaign->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<?php $this->getDialog('create-new-response.php'); ?>


<script type="text/javascript">
<?php $this->start_script(); ?>

    $(function (){

        //===== Create New Form + PopUp =====//
        $('#jdialog-createresponse').dialog(
        {
            autoOpen: false,
            height: 400,
            width: 400,
            modal: true,
            close: function()
            {
                $('form', this)[0].reset();
                $('#websites').trigger("change");
                $('#websites_search').hide();
                $.uniform.update();
            },
            buttons: (new CreateDialog('Create',
                function (ajax, status) {

                    if(!ajax.error) {

                        $('.dTable').dataTable().fnDraw();
                        notie.alert({text:ajax.message});
                        this.close();
                        this.form[0].reset();
                    }
                    else if(ajax.error == 'validation') {
                        alert(getValidationMessages(ajax.validation).join('\n'));
                    } else
                        alert(ajax.message);

                }, '#jdialog-createresponse')).buttons
        });

    });

<?php $this->end_script(); ?>
</script>