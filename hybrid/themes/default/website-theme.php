<?php
/**
 * @var $this Template
 */
	$gridLength = 'grid9';

//    $disable = $User->can('edit_' . $site->getTouchpointType(), $site->getID()) ? "" : "disabled";
    $disable = 'disabled';

	if($multi_user)
		$gridLength = 'grid8';
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

<?php if($site->touchpoint_type != 'PROFILE'): ?>

    <form name="themeForm" action="<?php echo $this->action_url($site->touchpoint_type . '_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">
        <div class="grid12 m_tp">
            <div class="widget">

<?php $this->getWidget('admin-lock.php',array('fieldname' => 'theme')); ?>

                <ul class="titleToolbar">
                    <li class="activeTab"><a class="themesFilter" href="#regular-themes" id="">Base Themes</a></li>

<?php if(!$multi_user): ?>

                    <li><a class="themesFilter" href="#mu-themes" id="">MU Themes</a></li>

<?php endif; ?>

                </ul>
                <div class="formRow">
                    <div class="grid2">
                        <div class="center" style="margin-bottom: 10px;"><strong>Active Theme:</strong> <?= $sitetheme->name ? $sitetheme->name : 'None'; ?></div>
                        <div class="currentTheme"><img src="<?= $sitetheme->getThumbnailURL(); ?>" alt="" width="115"></div>
                    </div>
                    <div class="grid10">
                        <div class="center" style="margin-bottom: 10px;"><strong>All Themes:</strong></div>
                        <ul class="bxSlider">

<?php
foreach($hubthemes as $theme_i){
    $this->getRow('site-theme.php',array('theme_i' => $theme_i));
}
foreach($musites as $theme_i){
    $this->getRow('site-theme.php',array('theme_i' => $theme_i));
}
?>

                        </ul>
                    </div>
                    <input type="hidden" class="ajax" name="website[theme]" value="<?= $sitetheme->id; ?>">
                    <input type="hidden" class="ajax" name="website[hub_parent_id]" value="<?= $sitetheme->hub_parent_id; ?>">
                    <input type="hidden" class="ajax" name="website_theme_change[changed]" value="false">
                    <input type="hidden" class="ajax" name="website_theme_change[owner]" value="">
                </div>
            </div>
        </div>
    </form>

<?php endif; ?>

    <form name="themeForm" action="<?php echo $this->action_url($site->touchpoint_type . '_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">
        <div class="grid12<?= $site->touchpoint_type == 'PROFILE' ? ' m_tp' : ''; ?>">
            <div class="widget">
                <div class="whead">
                    <h6>Design Settings</h6>
                </div>
                <div class="formRow jqColorPicker">
                    <div class="grid3"><label>Links Color:</label></div>
                    <?php $this->getWidget('input-website.php', array('name' => 'links_color', 'value' => $site->links_color, 'class' => 'colorpicker')); ?>
                </div>
                <div class="formRow jqColorPicker">
                    <div class="grid3"><label>Background color:</label></div>
                    <?php $this->getWidget('input-website.php', array('name' => 'bg_color', 'value' => $site->bg_color, 'class' => 'colorpicker')); ?>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Background Image:</label></div>
                    <?php $this->getWidget('input-website.php', array(
                        'name' => 'bg_img',
                        'value' => $site->bg_img,
                        'widget' => 'moxiemanager-filepicker.php',
                        'widgetParams' => array(
                            'idWidget' => 'bg-img-widget',
                            'path' => $storagePath,
                            'noHost' => true,
                            'extensions' => 'jpg,jpeg,png,gif',
                    )))?>
                    
                </div>
		
				<div class="formRow">
						<div class="grid3"><label>Manual Theme ID Override:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'theme','value' => $site->theme)); ?>
					</div>
				
            </div>
        </div><!-- //section -->
        <!-- section -->
        <div class="widget grid12">
            <div class="whead"><h6>Advanced CSS</h6></div>
            <div class="formRow">
                <div class="grid11"><label>Advanced CSS:</label></div>
			</div>
			<div class="formRow">
                <?php $this->getWidget('input-website.php', array('maxGrid' => 12, 'widget' => 'codemirror.php', 'widgetParams' => array('id' => 'code'),'name' => 'adv_css', 'value' => $site->adv_css))?>
            </div>
        </div><!-- //section -->
        <input type="hidden" value="<?=$site->touchpoint_type?>" name="website[touchpoint_type]"/>
    </form>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>
$(document).ready(function (){
    
<?php $this->getJ('admin-lock.php', array('isAdminUser' => $isAdminUser)); ?>
/* *************************
 *   Theme Slider Code
************************* */
    var mySlider;
    var hiddenThemes;
    
    //===== bx Slider for Themes =====//
    mySlider = $('.bxSlider').bxSlider({
        slideWidth: 115,
        minSlides: 2,
        maxSlides: 10,
        slideMargin: 10
    });
    
    $('.themesFilter').click(function(){
        //grab class of clicked button
        var themeType = $(this).attr('href').replace('#', '.');
        //remove any themes not of that class and store them
        if(hiddenThemes) {
            hiddenThemes.appendTo('.bxSlider');
            hiddenThemes = false;
        }
        hiddenThemes = $('.bxSlider > li:not('+themeType+')').detach();
        //reload bx slider
        mySlider.reloadSlider();
    });
    
    //===== Click Events for themes =====//
    $('.bxSlider a.jChangeTheme').click(function(){
        var action = $(this).attr('action');
        var themeID = $(this).attr('href').replace('#','');
        var Parent = $(this).attr('Parent');
        var owner = $(this).attr('owner');

        if (action == "save") {
            var input = $('input[name="website[theme]"]');
            input.val(themeID);
            var input = $('input[name="website[hub_parent_id]"]');
            input.val(Parent);
            var input = $('input[name="website_theme_change[changed]"]');
            input.val('true');
            var input = $('input[name="website_theme_change[owner]"]');
            input.val(owner);
            input.change();
            //window.location.replace("/admin.php?ID=<?=$site->id?>&action=theme&ctrl=Website");
            //window.setTimeout(function(){window.location.replace("/admin.php?ID=<?=$site->id?>&action=theme&ctrl=Website")}, 3000);
			//location.reload(true);
            $(document).ajaxSuccess(function(){
                setTimeout(function(){
                    location.reload();
                },750);
            });
            //if(success(J)) alert('test');
        }

        console.log(action + ': theme ID '+ themeID);
        return false;
    });
    
    //===== Image gallery control buttons =====//
	$(".gallery ul li").hover(
		function() { $(this).children(".actions").show("fade", 200); },
		function() { $(this).children(".actions").hide("fade", 200); }
	);

    $(".themesFilter[href='#regular-themes']").click();

});
<?php $this->end_script();  ?>
</script>