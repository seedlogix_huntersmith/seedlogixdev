<?php
$phoneCheck = PhoneModel::Fetch('cid = "'.$account->cid.'" AND type = "voip"');
if($phoneCheck){
	if($phoneCheck->external_only==1) $showPhone = false;
	else $showPhone = true;
}

if(!$total_emails_match) $total_emails_match =0;
if(!$total_proposals_match) $total_proposals_match =0;
if(!$total_notes_match) $total_notes_match =0;
if(!$total_tasks_match) $total_tasks_match =0;
if(!$total_texts_match) $total_texts_match =0;
if(!$total_phone_match) $total_phone_match =0;
$totals = $total_emails_match+$total_proposals_match+$total_notes_match+$total_tasks_match+$total_texts_match+$total_phone_match;

if($total_emails_match>=1) $percentage += '10'; //60
if($total_proposals_match>=1) $percentage += '15'; //60
if($total_notes_match>=1) $percentage += '10'; //60
if($total_tasks_match>=1) $percentage += '10'; //60
if($total_texts_match>=1) $percentage += '10'; //60
if($total_phone_match>=1) $percentage += '10'; //60
if($total_emails_match>=2) $percentage += '3'; //72
if($total_notes_match>=2) $percentage += '3'; //72
if($total_tasks_match>=2) $percentage += '3'; //72
if($total_texts_match>=2) $percentage += '3'; //72
if($total_phone_match>=2) $percentage += '3'; //72
if($total_emails_match>=3) $percentage += '4'; //100
if($total_notes_match>=3) $percentage += '4'; //100
if($total_tasks_match>=3) $percentage += '4'; //100
if($total_texts_match>=3) $percentage += '4'; //100
if($total_phone_match>=3) $percentage += '4'; //100

foreach($Proposals as $prop){ 
			if($prop->status=='Accepted') $clientstatus = 'Client Won'; 
}
	
?>
<style>
	.select2 {min-width:200px!important;}
</style>
<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
	<input type="hidden" value="<?= $account->getID(); ?>" name="accounts[id]">


	<div class="fluid">

		<!-- section -->
		<div class="widget grid6">
			<div class="whead">
				<h6 class="capitalize"><span><?= $account->html('name'); ?></span> (<?=$percentage?>%) <?=$clientstatus?></h6>
				<a class="buttonS f_rt editFields"><span>Edit the Account</span></a>
			</div>
			
			<div class="grid12">
				 <div class="body">
				
                        <ul class="wInvoice" style="text-align:center;">
                            <li><h4 style="color:#C57FE5"><?=$totals?></h4><span>touches</span></li>              
							<li><h4 style="color:#27bdbe"><?=$total_phone_match?></h4><span>calls</span></li>
							<li><h4 style="color:#00aeef"><?=$total_emails_match?></h4><span>emails</span></li>
							<li><h4 style="color:#1e4382"><?=$total_texts_match?></h4><span>texts</span></li>
                            <li><h4 style="color:#612c5c"><?=$total_notes_match?></h4><span>notes</span></li>
							<li><h4 style="color:#ee6f23"><?=$total_tasks_match?></h4><span>tasks</span></li>
							<li><h4 style="color:#e02a74"><?=$total_proposals_match?></h4><span>proposals</span></li>
                        </ul>
			
                        <div class="contentProgress"><div class="barG tipN" title="<?=$percentage?>%" id="bar7"></div></div>
                        <ul class="ruler">
                            <li>0</li>
                            <li class="textC">50%</li>
                            <li class="textR">100%</li>
                        </ul>
                      
                    </div>
				</div>
			
			<div class="grid6">
			<!-- meta config tab -->
			<div class="formRow hide">
							<div class="grid3"><label>Account Name:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $account->html('name'); ?></span>
								<input type="text" class="edit" name="accounts[name]" value="<?= $account->html('name'); ?>">
							</div>
						</div>
			<div class="formRow">
				<div class="grid2"><label><i class="icon-home-3"></i></label></div>
				<div class="grid10 editSwitcher">
					<a target="_blank" class="editValue" href="https://www.google.com/search?q=<?=$account->address?> <?=$account->city?> <?=$account->state?> <?=$account->zip?>">
						<span class="editAddress"><?= $account->html('address'); ?></span><br>
						<?php if($account->city && $account->state){?>
						<span class="editCity"><?=$account->html('city')?></span>, <span class="editState"><?=$account->state?> <span class="editZip"><?= $account->html('zip'); ?></span>
						<?php } else {?>
						<span class="editCity"><?=$account->html('city'); ?></span> <span class="editZip"><?= $account->html('zip'); ?></span>
						<?php } ?>
					</a>
					<input type="text" class="edit" name="accounts[address]" value="<?= $account->html('address'); ?>">
				</div>
			</div>
			
			<div class="formRow hide">
				<div class="grid2"><label>City:</label></div>
				<div class="grid10 editSwitcher">
					<span class="editValue"><?= $account->html('city'); ?></span>
					<input type="text" class="edit" name="accounts[city]" value="<?= $account->html('city'); ?>">
				</div>
			</div>
				
			</div>
			<div class="grid6">
					
			<div class="formRow hide">
				<div class="grid3"><label>State:</label></div>
				<div class="grid9 editSwitcher">
	

<?php
$this->getWidget('states-dropdown.php',array(
	'name'			=> 'accounts[state]',
	'value'			=> $account->state,
	'disable'		=> $disable,
	'hidden'		=> true
));
?>

					
				</div>
			</div>
			<div class="formRow hide">
				<div class="grid3"><label>Zip Code:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $account->html('zip'); ?></span>
					<input type="text" class="edit" name="accounts[zip]" value="<?= $account->html('zip'); ?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid2"><label><i class="icon-globe"></i></label></div>
				<div class="grid10 editSwitcher">
					<span class="editValue"><a href="<?= $account->html('url'); ?>" target="_blank"><?= $account->html('url'); ?></a></span>
					<input type="text" class="edit" name="accounts[url]" value="<?= $account->html('url'); ?>">
				</div>
			</div>
				
				</div>
				
				<div style="margin-top:20px;" class="gmap" data-address="<?=$account->address?>, <?=$account->city?>, <?=$account->state?> <?=$account->zip?>" data-height="250px" data-width="100%"></div> 
				
				
			<!-- //end meta config tab -->
		</div><!-- //section -->
</form>


		<!-- section -->
		<div class="widget grid6">

			<div class="whead">
				<h6>Proposals</h6>
				<div class="buttonS f_rt jopendialog jopen-createproposal"><span>Create Proposal</span></div>
			</div>


				<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Proposals'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jProposals',
	'data'				=> $Proposals,
	'rowkey'			=> 'Proposal',
	'rowtemplate'		=> 'proposals.php',
	'total'				=> $total_proposals_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxproposals',array('ID' => $_GET['ID'], 'aid' => $account->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

			
			<div class="whead" style="margin-top:40px;border-top: solid 1px #C8CDD0;">
				<h6>Tasks</h6>
				<div class="buttonS f_rt jopendialog jopen-createtask"><span>Create Tasks</span></div>
			</div>
			
			<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Tasks'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jTasks',
	'data'				=> $Tasks,
	'rowkey'			=> 'Task',
	'rowtemplate'		=> 'tasks.php',
	'total'				=> $total_tasks_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxtasks',array('ID' => $_GET['ID'], 'aid' => $account->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>



		</div><!-- //section -->
		


	</div><!-- //end fluid -->
	
	<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Contacts</h6>
			<div class="buttonS f_rt jopendialog jopen-createcontact"><span>Create Contact</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Contacts'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jContacts',
	'data'				=> $Contacts,
	'rowkey'			=> 'Contact',
	'rowtemplate'		=> 'campaign-contact.php',
	'total'				=> $total_contacts_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxaccountcontacts',array('ID' => $_GET['ID'], 'aid' => $account->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->
	
	
	<div class="fluid">

		<!-- section -->
		<div class="widget grid12">
			<div class="whead editSwitcher">
				<h6 class="timelineName">Account Timeline</h6>
			</div>
			<div class="body"><div class="grid12"><div id="timeline"></div></div></div>
		</div><!-- //section -->

	</div><!-- //end fluid -->


<?php $this->getDialog('send-email.php'); ?>
<?php $this->getDialog('create-task.php'); ?>
<?php $this->getDialog('create-proposal.php'); ?>
<?php $this->getDialog('create-contact.php'); ?>
	<?php if($showPhone){ ?>
<?php $this->getDialog('make-call.php'); ?>
	<?php } ?>
<?php $this->getDialog('update-task.php'); ?>
<script src="https://local.6qube.com/v2/js/google-maps.js"></script>
<script src="https://local.6qube.com/v2/js/google-maps-addon.js"></script>
<script type="text/javascript">
<?php $this->start_script(); ?>
	
			function tinyMCE() {
				tinymce.init({

							selector: 'textarea',
							setup: function (editor) {
								editor.on('change', function () {
									editor.save();
								});
							},
							height: 500,
							force_br_newlines: false,
							force_p_newlines: false,
							menubar:false,
							forced_root_block: '',
							fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
							valid_elements: '*[*]',
							plugins: [
								"advlist autolink lists link image charmap print preview anchor textcolor",
								"searchreplace visualblocks code",
								"insertdatetime media table contextmenu paste autoresize"
							],
							external_plugins: {
								"moxiemanager": "/themes/default/js/plugins/moxiemanager/plugin.min.js"
							},
							relative_urls: false,
							remove_script_host: false,
							moxiemanager_title: 'File Manager',
							toolbar: "insertfile undo redo | forecolor backcolor | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright | alignjustify | bullist numlist outdent indent | link image code"
						});
			}
	
			/*function switchTemplate(temp) {
				$.ajax({ // create an AJAX call...
					data: $(temp).serialize(), // get the form data
					type: $(temp).attr('post'), // GET or POST
					url: $(temp).attr(''), // the file to call
					success: function (ajax, status) { // on success..
						
						var json = JSON.parse(ajax);
						tinymce.remove();
						$('.j-cleditor').val(json.template);
						$('#subject').val(json.subject);
						tinyMCE();
						
						//alert(json.results);
					}
				});
			}*/
	
			function switchTemplate() {
				
				var contactid = $("#contactid").val();
				var email_template = $("#email_templates").val();
				var emailTemplate = $("#emailTemplate").val();
				
				$.ajax({ // create an AJAX call...
					data: "contactid=" + contactid + "&email_template=" + email_template + "&emailTemplate=" + emailTemplate, // get the form data
					type: 'post', // GET or POST
					url: '', // the file to call
					success: function (ajax, status) { // on success..
						
						var json = JSON.parse(ajax);
						tinymce.remove();
						$('.j-cleditor').val(json.template);
						$('#subject').val(json.subject);
						tinyMCE();
						
						//alert(json.results);
					}
				});
			}

	$(document).ready(function() {
	    //Use JSON data to Fill out the timeline
	    var timeline_data = [
			<?=$calltimeline?>
			<?php foreach($Proposals as $proposal): ?>
				 <?= json_encode($proposal->getTimeLineProposal()).','; ?>
			<?php endforeach;?>
			<?php foreach($Contacts as $contact): ?>
				 <?= json_encode($contact->getTimeLineContact()).','; ?>
			<?php endforeach;?>
			<?php foreach($Notes as $note):?>
			<?= json_encode($note->getTimeLineNote($this->action_url('campaign_delete',array('note_ID' => $note->id)))).','; ?>
			<?php endforeach;?>
			<?php foreach($Texts as $text): ?>
			<?= json_encode($text->getTimeLineText($this->action_url('campaign_delete',array('Text_ID' => $text->id)), $text->contact_id)).','; ?>
			<?php endforeach;?>
			<?php foreach($TimleineTasks as $task):?>
	        	<?= json_encode($task->getTimeLineTask()).','; ?>
			<?php endforeach;?>
			<?php foreach($Emails as $email):?>
				<?= json_encode($email->getTimeLineEmail($this->action_url('campaign_delete',array('Email_ID' => $email->id)))).','; ?>
			<?php endforeach;?>

	    ];

	    var timeline = new Timeline($('#timeline'), timeline_data);
		window.TimeLineOptions = {
			animation: false,
			separator: 'month_year'
		};
		timeline.setOptions(TimeLineOptions);
		timeline.display();

		window.timeline = timeline;
		
		<?php foreach($Emails as $email):?>
		$("#expand-<?=$email->id?>").on("click",function(){
			$(".eb-<?=$email->id?>").show();
			$(".es-<?=$email->id?>").hide();
		});
		$("#collapse-<?=$email->id?>").on("click",function(){
			$(".es-<?=$email->id?>").show();
			$(".eb-<?=$email->id?>").hide();
		});
		<?php endforeach;?>
		

		$('.editSwitcher > input').change(function(e){
			var input = $(this);
			var iname = input.attr('name');
			//var divSwitcher = input.closest('.editSwitcher');
			//var span = divSwitcher.find('span').text(input.val());
			if(iname == 'accounts[name]'){
				$('h6.capitalize > span:first-child').text(input.val());
			}
			if(iname == 'accounts[address]'){
				$('.editAddress').text(input.val());
			}
			if(iname == 'accounts[city]'){
				$('.editCity').text(input.val());
			}
			if(iname == 'accounts[zip]'){
				$('.editZip').text(input.val());
			}
		});

		/*$('.editSwitcher select').change(function(e){
			var input = $(this);
			var iname = input.attr('name');
			var divSwitcher = input.closest('.editSwitcher');
			var value = divSwitcher.find('a span').text();
			var span = divSwitcher.find('span').text(value);
			if(iname == 'accounts[state]'){
				$('.state').text(input.val());
			}
		});*/
		// Edit View
		$('a.editFields').click(function(){
			var _edit		= '<span>Edit the Account</span>';
			var _update		= '<span>Update the Account</span>';
			var _label		= $(this).html() == _edit ? _update : _edit;
			$(this).html(_label);
			$('.formRow.hide').toggle();
			$('.editSwitcher > .editValue').toggle();
			$('.editSwitcher > input.edit,.editSwitcher > div.edit').toggle();
			return false;
		});
		
		//===== Create New Form + PopUp =====//
		$('#jdialog-createcontact').dialog(
		{	
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jContacts');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
				
				var data = $.merge(ajax.contacts, timeline_data);
				var newTimeline = $('#timeline').empty();
				window.timeline = new Timeline(newTimeline, data);
				window.timeline.setOptions(TimeLineOptions);
				window.timeline.display();
				
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createcontact')).buttons
		});
		
		//===== Create New Form + PopUp =====//
		$('#jdialog-createtask').dialog(
		{	
			autoOpen: false,
			height: 265,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jTasks');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createtask')).buttons
		});
		
		$('#jdialog-savetask').dialog(
		{	
			autoOpen: false,
			height: 225,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Update Task',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jTasks');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
				
				var data = $.merge(ajax.tasks, timeline_data);
				var newTimeline = $('#timeline').empty();
				window.timeline = new Timeline(newTimeline, data);
				window.timeline.setOptions(TimeLineOptions);
				window.timeline.display();
				
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-savetask')).buttons
		});
		
		//===== Create New Form + PopUp =====//
		$('#jdialog-createproposal').dialog(
		{
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jProposals');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createproposal')).buttons
		});
		
		
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.95;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.95;
		
		$('#jdialog-sendemail').dialog(
		{
			open: function( event, ui ) {
				tinyMCE();
				
			},
			autoOpen: false,
			height: dHeight,
			width: dWidth,
			modal: true,
			close: function ()
			{	
				$('form', this)[0].reset();
				tinymce.remove();
				$('.j-cleditor').val('');
				//tinyMCE();
				
			},
			buttons: (new CreateDialog('Send',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-sendemail')).buttons
		});
		

		
		///dont save modals
		
		 $( ".expiredatepicker" ).datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
		 $( ".duedatepicker" ).datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
		 $( ".duedatepicker2" ).datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
		
		$(document).on("click", ".jopen-savetask", function () {
			 var task_id = $(this).data('id');
			 var t_title = $(this).data('title');
			 var t_due = $(this).data('due');
			 var t_time = $(this).data('time');
			 $("#task_id").val( task_id );
			$('input[type=text]#task_title').val(t_title);
			$('input[type=text]#due_date2').val(t_due);
			$('select#time').val(t_time);
		});
		

		$(document).on("click", ".jopen-sendemail", function () {
			 var contact_id = $(this).data('contactid');
			 var contact_email = $(this).data('email');
			 var account_id = $(this).data('aid');
			 $("#emailto").val(contact_email);
			 $('#contactid').val(contact_id);
			$('#accountid').val(account_id);
		});
		
		$('#timeline').on('click','a.jajax-action',function(e){
			e.preventDefault();
			var t = $(e.target).parent();
			sendAjax(t.attr('href'),function(ajax){
				if(!ajax.error){
					timeline._deleteElement(t.closest('.timeline_element'));
					notie.alert({text:ajax.message});
				}else{
					notie.alert({text:'An error occurred.'});
				}
			});
		});
		
		$('#jdialog-makecall').dialog(
		{
			autoOpen: false,
			height: 315,
			width: 400,
			modal: true
		});
		
		$(document).on("click", ".jopen-makecall", function () {
			
			
			
		
			 var c_phone = $(this).data('phone')

			 $('input[type=text]#toNumber').val(c_phone);
			 
			 // As pointed out in comments, 
			 // it is superfluous to have to manually call the modal.
			 // $('#addBookDialog').modal('show');
		});
		
	  


	});
<?php $this->end_script(); ?>
</script>