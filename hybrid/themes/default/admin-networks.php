<div class="fluid">
	<div class="widget tabsAndTables">
	<div class="tabs">
		<div class="whead">
			<ul>
				<li><a href="#tabs-1">All Networks</a></li>
				<li><a href="#tabs-2">Local</a></li>
				<li><a href="#tabs-3">Websites</a></li>
				<li><a href="#tabs-4">Blogs</a></li>
				<li><a href="#tabs-5">Press</a></li>
				<li><a href="#tabs-6">Articles</a></li>
			</ul>
			<div class="BntWithTabs">
				<div class="buttonS bGreen right dynRight jopendialog jopen-createnetwork">Create Network</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="dyn">
			<div id="tabs-1">
<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks1',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => $iTotalNetworks,
					'sourceurl' => $this->action_url('Admin_ajaxnetworks'),
                    'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
?>
			</div>
			<div id="tabs-2">
<?php       // @amado @todo Once the 'Total' and 'SourceUrl' are completed the Tabs functionality will work.
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks2',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => 0,
					'sourceurl' => '?'
				)
			);
?>
			</div>
			<div id="tabs-3">
<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks3',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => 0,
					'sourceurl' => '?'
				)
			);
?>
			</div>
			<div id="tabs-4">
<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks4',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => 0,
					'sourceurl' => '?'
				)
			);
?>
			</div>
			<div id="tabs-5">
<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks5',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => 0,
					'sourceurl' => '?'
				)
			);
?>
			</div>
			<div id="tabs-6">
<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('Networks'),
					'class' => '',
					'id' => 'jNetworks6',
					'data' => $networks,
					'rowkey' => 'network',
					'rowtemplate' => 'network.php',
					'total' => 0,
					'sourceurl' => '?'
				)
			);
?>
			</div>			
		</div>
	</div><!-- /end tabs -->
	</div><!-- /end widget -->
</div>
<!-- /end fluid --> 


<!-- Create Network POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create New Network" id="jdialog-createnetwork" style="display: none;">
	<form method="post" action="<? echo $this->action_url('Admin_savenetwork'); ?>" class="jnetwork-create">
		<fieldset class="step" id="w1first">
			<div class="fluid">
				<div class="grid3" style="position: relative; top: 11px;">
					<label>Network Name:</label>
				</div>
				<div class="grid9">
					<input type="text" name="network[name]" placeholder=""/>
				</div>
				<div class="clear"></div>
			</div>
			<div class="fluid">
				<div class="grid3" style="position: relative; top: 11px;">
					<label>Network Hostname:</label>
				</div>
				<div class="grid6">
					<input type="text" name="network[httphostkey]" placeholder="Enter a hostname (mynetwork.mydomain.com)"/>
				</div>
			</div>
			<div class="fluid">
				<div class="grid3" style="position: relative; top: 11px;">
					<label>Network Type:</label>
				</div>
				<div class="grid6" style="position: relative; top: 8px;">
					<select name="network[type]">
						<option value="local" selected="">Local</option>
						<option value="hubs">Websites</option>
						<option value="blogs">Blogs</option>
						<option value="press">Press</option>
						<option value="articles">Articles</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
<? $this->start_script(); ?>

	$(function (){

		//===== Tabs =====//
		$( ".tabs" ).tabs();

		//===== Create New Form + PopUp =====//
		$( '#jdialog-createnetwork' ).dialog(
		{
			autoOpen: false,
			height: 275,
			width: 550,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create Network',
				function (ajax, status)
				{
	      	if(!ajax.error)
	      	{
	          //$('.jnoforms').hide();
	          $('.dataTableBody').prepend(ajax.row);
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createnetwork')).buttons
		});
	
	});

<? $this->end_script(); ?>

</script>