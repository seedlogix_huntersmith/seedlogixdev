<?php
//var_dump($parent_products);
?>
<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
	<input type="hidden" value="<?= $proposal->getID(); ?>" name="proposal[id]">

	<div class="fluid pdf">
		<!-- section -->
		<div class="widget grid6">
			<div class="whead">
				<h6>Proposal Settings</h6>
			</div>
			<!-- meta config tab -->
			<div class="formRow">
							<div class="grid3"><label>Name:</label></div>
							<div class="grid9">
								<input type="text" name="proposal[name]" value="<?= $proposal->html('name'); ?>">
							</div>
						</div>
			<div class="formRow">
                                <div class="grid3"><label>Date:</label></div>
                                <div class="grid4"><input type="text" name="proposal[expire_date]" class="proposaldatepicker" value="<?= $proposal->expire_date; ?>"></div>
             </div>
			
			<div class="formRow">
                                <div class="grid3"><label>Contact:</label></div>
                                <div class="grid4"><select data-placeholder="Choose a Contact" class="select2" name="proposal[contact_id]" id="contact_id">
						<option></option>
<?php

foreach($Contacts as $Contact){?>
	<option value="<?php echo $Contact->id; ?>" <?php $this->selected($Contact->id, $proposal->contact_id); ?>><?=$Contact->first_name?> <?=$Contact->last_name?></option>
<?php } ?>
					</select></div>
             </div>
			<!-- //end meta config tab -->

			

		</div><!-- //section -->
</form>		
		<!-- section -->
		<div class="widget grid6">
			<div class="whead">
				<h6>Products</h6>
				<div class="buttonS f_rt jopendialog jopen-createproduct"><span>Add Product</span></div>
				<div class="buttonS f_rt jopendialog jopen-sendProposal"><span>Send Proposal</span></div>
			</div>


				<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Products'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jProducts',
	'data'				=> $Products,
	'rowkey'			=> 'Product',
	'rowtemplate'		=> 'products.php',
	'total'				=> $total_products_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxproducts',array('ID' => $_GET['ID'], 'proid' => $proposal->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

		</div><!-- //section -->
		
		
		<div class="widget grid12">
				<div class="whead">
					<h6 class="capitalize"><span>Preview Proposal</span></h6>
				</div>	


				<object width="100%" height="1500" class="proposal-pdf" type="application/pdf" data="api/proposal-pdf.php?aid=<?=$proposal->html('account_id'); ?>&conid=<?=$proposal->html('contact_id'); ?>&proid=<?=$proposal->getID(); ?>">
				</object>
		</div>
	</div><!-- //end fluid -->

<?php $this->getDialog('send-proposal.php'); ?>
<?php $this->getDialog('create-product.php'); ?>
<?php $this->getDialog('save-product.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(document).ready(function() {
	    //Use JSON data to Fill out the timeline

		 $( ".proposaldatepicker" ).datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
		//===== Create New Form + PopUp =====//
		$('#jdialog-createproduct').dialog(
		{
			autoOpen: false,
			height: 350,
			width: 450,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jProducts');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
				
				

				//$('object').attr('data', $('object').attr('data'));
				location.reload();

					
				}, '#jdialog-createproduct')).buttons
		});
		
		$(document).on("click", ".jopen-saveproduct", function () {
			
			
			
			 var product_id = $(this).data('id');
			 var product_name = $(this).data('name')
			 var product_qty = $(this).data('qty');
			 var product_price = $(this).data('price')
			 var product_type = $(this).data('type');
			 var product_discount = $(this).data('discount');
			var product_s_desc = $(this).data('sdesc');
			 $("#product_id").val( product_id );
			 $('input[type=text]#product_name').val(product_name);
			$('input[type=text]#product_price').val(product_price);
			$('input[type=text]#product_qty').val(product_qty);
			$('input[type=text]#product_discount').val(product_discount);
			$('select#product_type').val(product_type);
			$('#product_s_desc').val(product_s_desc);
			 
			 // As pointed out in comments, 
			 // it is superfluous to have to manually call the modal.
			 // $('#addBookDialog').modal('show');
		});
		
		$('#jdialog-saveproduct').dialog(
		{
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Save',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
				
				
				location.reload();


					
				}, '#jdialog-saveproduct')).buttons
		});
		
		        //===== Send Proposal Email =====//
		$( '#jdialog-sendProposal' ).dialog({
			autoOpen: false,
			height: 200,
			width: 300,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Send Proposal',
				function (ajax, status)
				{
                    if(!ajax.error)
                    {
                      notie.alert({text:ajax.message});
                      this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	      alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	                   alert(ajax.message);
					
				}, '#jdialog-sendProposal')).buttons
		});



	});

<?php $this->end_script(); ?>
</script>
	

	
