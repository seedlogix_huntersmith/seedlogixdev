<div>
    Below are the domains you have successfully tied to our server.
    Removing a domain does not delete the whole hub or blog from your dashboard -- It only the deletes "domain" field and stops the domain from forwarding to that site.
    Note: When you remove a domain it removes all of the subdomains attached to it as well. However, you can remove a subdomain without affecting the master domain and its other subdomains. After removing a domain or subdomain it can easily be re-added.
</div>

    <br style="clear:both">
<div id="domains">
    <?php foreach($hubs as $h): ?>
    <div>
        <strong>
        <?php $this->p($h->name); ?>
        </strong> Host: 
        <?php  echo (preg_replace('@^([^\.]+)\.?@', '<u>\1</u>.', $h->httphostkey)); ?>
        <div>
            <a href="#">Remove</a>  |  
            <a href="#">Dashboard</a>
        </div>
    </div>
    <br style="clear:both">
    <?php endforeach; ?>
</div>