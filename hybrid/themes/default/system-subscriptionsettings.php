<style>
input {
    border: 1px solid #ccc;
    color: #fb8404;
    font-weight: 700;
    padding: 5px;
    width: 60px;
}
</style>

<div class="fluid" id="pricingPlans"> 
		
		<!--table one starts-->
		<div class="grid4">
			<form action="<?php
				echo $this->action_url('System_save?ID=1');
				?>" method="post" class="ajax-save">
            <div class="pricing">
				<table>
					<thead>
						<tr>
							<th> PROFESSIONAL
								<div class="label-wrapper">
									<div class="price-label"><input type="text" name="sub[price]" value="<?php $SubscriptionPlans[1]->html('price'); ?>" /><span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">PROFESSIONAL Plan</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Marketers <br>
									and Business Owners.</p>
								<ul>
									<li><strong>Up to <input type="text" name="sub[limit_TOUCHPOINTS]" value="<?php $SubscriptionPlans[1]->html('limit_TOUCHPOINTS'); ?>" /> Touchpoints</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_KEYWORDS]" value="<?php $SubscriptionPlans[1]->html('limit_KEYWORDS'); ?>" /> Keywords for SERP Rankings</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_EMAILS]" value="<?php $SubscriptionPlans[1]->html('limit_EMAILS'); ?>" /> Emails</strong></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
            </form>
		</div>
		<!--table one ends--> 
		
		<!--table two starts-->
		<div class="grid4">
            <form action="<?php
				echo $this->action_url('System_save?ID=2');
				?>" method="post" class="ajax-save">
			<div class="pricing">
				<table>
					<thead>
						<tr>
							<th> CORPORATE
								<div class="label-wrapper">
									<div class="price-label"><input type="text" name="sub[price]" value="<?php $SubscriptionPlans[2]->html('price'); ?>" /><span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">CORPORATE Plan</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Marketing Teams, SEOs <br>
									and Web Designers</p>
								<ul>
									<li><strong>Up to <input type="text" name="sub[limit_TOUCHPOINTS]" value="<?php $SubscriptionPlans[2]->html('limit_TOUCHPOINTS'); ?>" /> Touchpoints</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_KEYWORDS]" value="<?php $SubscriptionPlans[2]->html('limit_KEYWORDS'); ?>" /> Keywords for SERP Rankings</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_EMAILS]" value="<?php $SubscriptionPlans[2]->html('limit_EMAILS'); ?>" /> Emails</strong></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
            </form>
		</div>
		<!--table two ends--> 
		
		<!--table three starts-->
		<div class="grid4">
            <form action="<?php
				echo $this->action_url('System_save?ID=3');
				?>" method="post" class="ajax-save">
			<div class="pricing">
				<table>
					<thead>
						<tr>
							<th> ENTERPRISE
								<div class="label-wrapper">
									<div class="price-label"><input type="text" name="sub[price]" value="<?php $SubscriptionPlans[3]->html('price'); ?>" /><span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">ENTERPRISE Plan</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Franchises, Agencies <br>
									and Marketing Groups</p>
								<ul>
									<li><strong>Up to <input type="text" name="sub[limit_TOUCHPOINTS]" value="<?php $SubscriptionPlans[3]->html('limit_TOUCHPOINTS'); ?>" /> Touchpoints</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_KEYWORDS]" value="<?php $SubscriptionPlans[3]->html('limit_KEYWORDS'); ?>" /> Keywords for SERP Rankings</strong></li>
									<li><strong>Up to <input type="text" name="sub[limit_EMAILS]" value="<?php $SubscriptionPlans[3]->html('limit_EMAILS'); ?>" /> Emails</strong></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
            </form>
		</div>
		<!--table three ends--> 
		
</div>
