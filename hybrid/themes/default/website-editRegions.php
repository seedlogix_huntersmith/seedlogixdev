<?php

	$isAdminUser = $this->isMU;
	$gridLength = '';
	
	if($isAdminUser){
		$gridLength = 'adminUser';
	}

	$disable = $User->can('edit_' . $site->getTouchpointType(), $site->getID()) ? "" : "disabled";
	if($multi_user) $musite = '&musite=1';
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

    <?php   if($site->touchpoint_type!='PROFILE' && $hubTheme->hybrid==1) { ?>
<div class="grid12 m_tp">
    <div class="widget">
        <div id="activateT" class="whead"><h6><?= $site->touchpoint_type == 'LANDING' ? 'Landing Page' : 'Home Page'; ?></h6></div>
        <iframe src="/themes/preview-editor/v1/index.php?siteid=<?=$site->id?>&themeid=<?=$site->theme?>&column=overview&touchpoint=<?=$site->touchpoint_type?><?=$musite?>" width="100%" id="iframe1" marginheight="0" frameborder="0" scrolling="no"></iframe>
    </div>

    <?php   if($site->touchpoint_type == 'WEBSITE') { ?>
    <div class="widget m_tp">
        <div id="overviewT2" class="whead"><h6>Global Inside Header</h6></div>
        <iframe src="/themes/preview-editor/v1/index.php?siteid=<?=$site->id?>&themeid=<?=$site->theme?>&column=photos&touchpoint=<?=$site->touchpoint_type?><?=$musite?>" width="100%" id="iframe2" marginheight="0" frameborder="0" scrolling="no"></iframe>
    </div>
    <? } ?>
    <div class="widget m_tp">
        <div class="whead"><h6>Global Footer</h6></div>
        <iframe src="/themes/preview-editor/v1/index.php?siteid=<?=$site->id?>&themeid=<?=$site->theme?>&column=offerings&touchpoint=<?=$site->touchpoint_type?><?=$musite?>" width="100%" id="iframe3" marginheight="0" frameborder="0" scrolling="no"></iframe>
    </div>
</div><!-- grid12.m_tp -->
</div><!-- fluid -->

    <script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
    <script type="text/javascript">

        iFrameResize({
            log                     : true,                  // Enable console logging
            enablePublicMethods     : true,                  // Enable methods within iframe hosted page
            resizedCallback         : function(messageData){ // Callback fn when resize is received
                $('p#callback').html(
                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                    ' <b>Height:</b> '     + messageData.height +
                    ' <b>Width:</b> '      + messageData.width +
                    ' <b>Event type:</b> ' + messageData.type
                );
            },
            messageCallback         : function(messageData){ // Callback fn when message is received
                $('p#callback').html(
                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                    ' <b>Message:</b> '    + messageData.message
                );
                alert(messageData.message);
            },
            closedCallback         : function(id){ // Callback fn when iFrame is closed
                $('p#callback').html(
                    '<b>IFrame (</b>'    + id +
                    '<b>) removed from page.</b>'
                );
            }
        });


    </script>
    <? } else { ?>

<?php $this->getWidget('admin-lock-message.php',array('isAdminUser' => $isAdminUser)); ?>

<div class="grid12 m_tp">
    <form action="<?= $this->action_url("{$site->touchpoint_type}_save?ID=".$site->id); ?>" method="POST" class="ajax-save" id="WebsiteRegions">

<?php
$counter = 0;
foreach($fields as $key => $field):
++$counter;
?>

		<div class="widget<?= $counter == 2 ? ' m_tp' : ''; ?>">
            <div class="whead">
				<h6><?= $field['title']; ?></h6>

<?php $this->getWidget('admin-lock.php',array('fieldname' => $key,'isAdminUser' => $isAdminUser)); ?>

			</div>
			<div class="formRow">
				<?php $this->getWidget('input-website.php', array(
					'name' => $key,
					'maxGrid' => 12,
					'noLock' => true,
					'widget' => 'wysiwyg-ide.php',
					'widgetParams' => array(
						'fieldname' => "website[$key]",
						'fieldvalue' => $field['value']
				)))?>
			</div><!-- end Body -->
		</div>

<?php endforeach; ?>

		<input type="hidden" value="<?=$site->touchpoint_type?>" name="website[touchpoint_type]">
    </form>
</div><!-- grid12.m_tp -->
</div><!-- fluid -->
    <? } ?>

<script type="text/javascript">

<?php $this->start_script(); ?>



    $(document).ready(function (){
        <?php $this->getJ('admin-lock.php', array('isAdminUser' => $isAdminUser)); ?>
		<?php $this->getJ('wysiwyg-ide.php', array('disable' => !empty($disable))); ?>
		var tabHeads = $('#WebsiteRegions').find('.widget');
		tabHeads.click(function(){
			var codeMirrorObj = $(this).find('.j-dual-ide').data('codeMirror');
			codeMirrorObj.refresh();
		});

    });

<?php $this->end_script(); ?>

</script>