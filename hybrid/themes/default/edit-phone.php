<?php
/**
 * @var $this Template
 * @var $Campaign CampaignModel
 * @var $collab CampaignCollab
 * @var $websites stdClass[]
 */
?>

<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
	<input type="hidden" name="phoneline[id]" value="<?= $phone->getID(); ?>">
	<input type="hidden" name="phoneline[cid]" value="<?= $phone->cid; ?>">
	<div class="fluid">
		<div class="widget grid6">
			<div class="whead"><h6>{<?= $phone->phone_num; ?>} Settings</h6></div>
			
			<?php if(!$phone->type=='voip'){?>
			<div class="formRow">
				<div class="grid3"><label>Forwarding Number:</label></div>
				<div class="grid9"><input type="text" name="phoneline[cell]" value="<?= $phone->cell; ?>"></div>
			</div>
			<?php } ?>
			<?php if($phone->type=='voip'){?>
			<div class="formRow">
				<div class="grid3"><label>SIP:</label></div>
				<div class="grid9"><?= $phone->sip; ?></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Username:</label></div>
				<div class="grid9"><?= $phone->username; ?></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Password:</label></div>
				<div class="grid9"><?= $phone->password; ?></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Voicemail:</label></div>
				<div class="grid9">
					<?php
					$this->getWidget('moxiemanager-filepicker.php', array(
						'name' => 'phoneline[greeting]',
						'value' => $phone->greeting,
						'idWidget' => 'greeting',
						'path' => $storagePath,
						'noHost' => true,
						'extensions' => 'mp3,wav',
						'type' => 'audio'
					));
					?>
					<span class="note">.mp3 and .wav files only.</span>
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Forward <br>Unanswered Calls:</label></div>
				<div class="grid9">
					<input type="text" name="phoneline[cell]" value="<?= $phone->cell; ?>">
					<span class="note">This will disable voicemail and forward calls unaswered on your VOIP line.</span>
				</div>
				
			</div>
			<div class="formRow">
				<div class="grid3"><label>SMS VM Notification:</label></div>
				<div class="grid9">
					<input type="text" name="phoneline[vm_sms]" value="<?= $phone->vm_sms; ?>">
					<span class="note">Notifications when there is a voicemail will be sent to this number.</span>
				</div>
				
			</div>
			<?php } ?>
			<div class="formRow">
                    <div class="grid3"><label>Disable Call Recording:</label></div>
                    <div class="grid9">
						<input type="checkbox" name="phoneline[recording_off]" class="i_button" value="1" <?php $this->checked($phone->recording_off, 1)?> <?php $this->checked(($phone->recording_off == 1) ? 1 : 0, 1); ?>>
					</div>
           </div>
				
			<div class="formRow">
                    <div class="grid3"><label>External Device Only:</label></div>
                    <div class="grid9"><input type="checkbox" name="phoneline[external_only]" class="i_button" value="1" <?php $this->checked($phone->external_only, 1)?> <?php $this->checked(($phone->external_only == 1) ? 1 : 0,
                                        1); ?>>
					</div>
           </div>
			
		</div>
	</div>
</form>

<script type="text/javascript">
<?php $this->start_script(); ?>

<?php $this->end_script(); ?>
</script>