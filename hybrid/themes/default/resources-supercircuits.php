<div class="fluid">
  <div class="widget grid6">

<?php /*

    <div class="whead"><h6>Resources</h6></div>

*/ ?>

    <div class="formRow" style="margin: 0; padding: 0; border: 0 none;"><div class="grid12" style="margin: 0; padding: 0; border: 0 none;"><img src="/hubs/themes/clients/powerleads/powerleads-real-results.jpg" alt="" style="width: 100%; border-radius: 0;"></div></div>

<?php /*

    <div class="formRow"><div class="grid12 center"><h2 style="color: #435464; font-size: 30px; line-height: 30px;">GET MORE FROM POWERLEADS!</h2></div></div>

    <div class="formRow" style="background: #00a8cf;"><div class="grid12 center"><h3 style="color: #fff; font-size: 22px;">Custom Marketing Services to Elevate Your Business</h3></div></div>

    <div class="formRow" style="background: #0071b5;"><div class="grid12 center"><h4 style="color: #fff;">Our Custom Packages take the worry out of Marketing</h4></div></div>

    <div class="formRow" style="background: #002840;">
      <div class="grid4">
        <p style="padding-top: 0; color: #ef7623; border-bottom: 1px solid #00a8cf;"><strong>Build YOUR Brand</strong></p>
        <p style="color: #fff;">We will design a custom website that is optimized and responsive &mdash; and set you up with your own blog, social media profiles, and local directory listings.</p>
      </div>
      <div class="grid4">
        <p style="padding-top: 0; color: #ef7623; border-bottom: 1px solid #00a8cf;"><strong>Promote Reviews</strong></p>
        <p style="color: #fff;">We generate customer ratings &amp; reviews &mdash; to turn your positive WORD OF MOUTH feedback into a digital lead-generation engine.</p>
      </div>
      <div class="grid4">
        <p style="padding-top: 0; color: #ef7623; border-bottom: 1px solid #00a8cf;"><strong>Generate Leads</strong></p>
        <p style="color: #fff;">We manage your local SEO, create high-quality content, and run content &amp; social media Marketing campaigns &mdash; all optimized to compete for local demand.</p>
      </div>
    </div>

    <div class="formRow" style="background: #ef7623;">
      <div class="grid12 center">
        <h4 style="color: #fff;">Marketing takes Time, Money &amp; Expertise</h4>
        <h5 style="color: #fff;">Our all-inclusive solutions are designed to be Turnkey, Effective &amp; VERY Affordable.</h5>
      </div>
    </div>

    <div class="formRow">
      <div class="grid6" style="margin: 0 auto; float: none;">
        <iframe style="width: 100%;" src="https://www.youtube.com/embed/N9pFFbe4Bpw?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <?php //<img src="themes/default/images/powerleads/powerleads-real-results.png" alt="PowerLeads: Real Results" style="width: 100%; border-radius: 0;"> ?>
      </div>
    </div>

    <div class="formRow">
      <div class="grid12 center">
        <p style="padding-top: 0; color: #435464;">Take your Marketing to the next level and get more leads!</p>
        <h5 style="color: #435464;">Sign up now at <a href="http://www.alibipartners.com/" class="_blank">www.alibipartners.com</a><br>or call today at <span style="color: #f00;">888-858-1440</span></h5>
      </div>
    </div>

*/ ?>

  </div>
  <div class="widget grid6">

    <div class="formRow" style="margin: 0; border: 0 none;"><div class="grid6" style="margin: 0 auto; padding: 0; border: 0 none; float: none;">
        <iframe style="width: 100%;" src="https://www.youtube.com/embed/N9pFFbe4Bpw?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div></div>

  </div>
</div>