<!-- Alternative buttons -->
<?php $this->getMenu('responder-icons.php'); ?>

<div class="fluid">
    <div class="widget">
        <div class="whead">
            <h6>Responder Email</h6>
        </div>
        <div class="formRow">
            <div class="grid3"><label>From:</label></div>
            <div class="grid9"><input type="text" name="regular"></div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Subject:</label></div>
            <div class="grid9"><input type="text" name="regular"></div>
            <div class="clear"></div>
        </div>
        <div class="whead">
            <h6>Main Section</h6>
        </div>
        <div class="formRow">
            <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
        </div>
        <div class="whead"><h6>Business Name / Address</h6><div class="clear"></div></div>
        <div class="formRow">
            <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
        </div>
        <div class="whead"><h6>AntiSpam Message</h6><div class="clear"></div></div>
        <div class="formRow">
            <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
        </div>
        <div class="whead"><h6>Opt-out Message</h6><div class="clear"></div></div>
        <div class="formRow">
            <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
        </div>
    </div>     
</div>

<!-- //end content container -->
<script type="text/javascript">
    	
	//===== Code Mirror / Syntax highlight =====//
	var editor = CodeMirror.fromTextArea(document.getElementById("adv_css"), {
        lineNumbers: true
      });
    
    </script>