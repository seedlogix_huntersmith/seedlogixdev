<?php

function phone_display($num){
    $num = preg_replace('/[^0-9]/', '', $num);

    $len = strlen($num);
    if ($len==11 && substr($num,0,1)=='1'){
        return substr($num,1,10);
    }
    return $num;
}
function ConvertSectoAmount($n) { 
		//$n = 10000;
		$hour = $n / 3600; 

		$n %= 3600; 
		$minutes = $n / 60 ; 

		$n %= 60; 
		$seconds = $n; 

		//echo ("$day days $hour hours $minutes minutes $seconds seconds"); 
		return array(
			'hours'=> $hour, # Submit the result of the record to this URL
			'minutes' => $minutes, # HTTP method to submit the action URL
			'seconds'=> $seconds
		);

	} 

?>
<style>
	.feed-bubble { padding:20px; box-shadow: 0 0.46875rem 2.1875rem rgba(4, 9, 20, 0.03), 0 0.9375rem 1.40625rem rgba(4, 9, 20, 0.03), 0 0.25rem 0.53125rem rgba(4, 9, 20, 0.05), 0 0.125rem 0.1875rem rgba(4, 9, 20, 0.03);margin:10px 0 10px 0; }
	.feed-bubble h4 {font-size:16px;margin-bottom:10px;float:left;}
	.feed-bubble h6 {font-size:12px;float:right}
	.feed-bubble p {clear:both;}
	.datestamp {color: #27bdbe;}
	.product-card-title {float:left;width:75%}
	.product-card-price {float:left;width:25%; text-align:right;}
	.product-card {padding:10px; box-shadow: 0 0.46875rem 2.1875rem rgba(4, 9, 20, 0.03), 0 0.9375rem 1.40625rem rgba(4, 9, 20, 0.03), 0 0.25rem 0.53125rem rgba(4, 9, 20, 0.05), 0 0.125rem 0.1875rem rgba(4, 9, 20, 0.03); }
	.product-card-total {text-align:right;margin-top:5px;padding-top:5px;border-top:1px solid #EEE;color:#41CE1E;font-size:16px;}
	.lead-activity-card {margin-bottom:10px; }
	.top-start-card-single .widget {
		 margin-top:10px!important;
	}
	.top-start-card-single2 .widget {
		 margin-top:10px!important;
	}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
<!-- Timetable Font -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />

<!-- Timetable CSS Files -->
<link rel="stylesheet" href="/library/calendar/assets/css/magnific-popup.css">
<link rel="stylesheet" href="/library/calendar/assets/css/timetable.css">


<script src="/library/calendar/assets/js/jquery.magnific-popup.js"></script>
<script src="/library/calendar/assets/js/timetable.js"></script>

<div class="widget grid12">
			<div class="whead">
				<h6 class="timelineName">Tasks At Glance</h6>
			</div>
			<div class="body">
				<div class="grid12">
					<div class="tiva-timetable" data-source="php" data-view="month" data-userid="<?=$User->id?>"></div> 
				</div>
				
			</div>
		</div><!-- //section -->
<div class="fluid">
	
<div class="grid3">
<div class="widget">
            <div class="whead">
                <h6>Inbound Text Feed</h6>
                  
            </div>
            <div class="body">
				<div class="grid12">
              <h6 style="margin-bottom:25px;font-size:14px;">Last 3 days</h6>
				  <?php 
						foreach($Texts as $text){
							
						$Prospect = ProspectModel::Fetch('phone = "'.phone_display($text->from_text).'" AND user_id = "'.$User->id.'" AND phone != ""');	
							
						if($Prospect){
							$name = $Prospect->name;
							$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect->cid.'&pid='.$Prospect->id);
						} else $name = $text->from_text;
		
						$date = DateTime::createFromFormat('Y-m-d H:i:s', $text->created);
						$callstamp = $date->format('g:i A');
						$datestamp = $date->format('D j M');
					
					?>
					<div class="datestamp"><?=$datestamp?></div>
					<div class="feed-bubble">
						<h4><span style="color:#00aeef;"><i class="icon-comment_stroke"></i></span> SMS from <?=$name?></h4>
						<h6><?=$callstamp?></h6>
						<p><?=$text->message?></p>
						<?php if($Prospect) { ?><a title="View Message" class="bDefault f_rt" href="<?=$edit_action?>"><span class="edit"></span></a><?php } ?>
						<div style="clear:both;"></div>
						</div>
                
				  <?php } ?>
            
         <h6 style="margin-top:25px;"><a href="/admin.php?action=inboxtexts&ctrl=Campaign">View All Texts</a></h6>
					</div>
				
			</div>
        </div>
</div>
	

<div class="grid3">
<div class="widget">
            <div class="whead">
                <h6>Inbound Call Feed</h6>
                  
            </div>
            <div class="body">
				<div class="grid12">
              <h6 style="margin-bottom:25px;font-size:14px;">Last 3 days</h6>
				  <?php 
						foreach($Calls as $call){
							
						$Prospect = ProspectModel::Fetch('phone = "'.phone_display($call->caller_id).'" AND user_id = "'.$User->id.'"');	
							
						if(phone_display($call->caller_id)==$Prospect->phone){
							$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect->cid.'&pid='.$Prospect->id);
							$name = '<a href="'.$edit_action.'">'.$Prospect->name.'</a>';
						} else $name = $call->caller_id;
		
						$date = DateTime::createFromFormat('Y-m-d H:i:s', $call->date);
						$callstamp = $date->format('g:i A');
						$datestamp = $date->format('D j M');
							
						if($call->recording) $download = '<a title="Download Recording" class="bDefault f_rt phoneDownload" href="'.$call->recording.'"><span class="export"></span></a>';
							
						$ctime = ConvertSectoAmount($call->length);
						if($ctime['hours']>=1) $calltime .= round($ctime['hours']).' hrs ';
						if($ctime['minutes']>=1) $calltime .= round($ctime['minutes']).' mins ';
						if($ctime['seconds']) $calltime .= round($ctime['seconds']).' secs ';
					
					?>
					<div class="datestamp"><?=$datestamp?></div>
					<div class="feed-bubble">
						 <h4 ><span style="color:#5E57FF;"><i class="icon-phone"></i></span> Call from <?=$name?> </h4>
						<h6><?=$callstamp?></h6>
							<p><?=$calltime?> <?php if($Prospect) { ?><a title="View Message" class="bDefault f_rt" href="<?=$edit_action?>"><span class="edit"></span></a><?php } ?> <?=$download?> </p>
						
						</div>
                
				  <?php $calltime = ''; } ?>
            
         				<h6 style="margin-top:25px;"><a href="/admin.php?action=inboxcalls&ctrl=Campaign">View All Calls</a></h6>
					</div>
				
			</div>
        </div>
</div>
	
<div class="grid3">
<div class="widget">
            <div class="whead">
                <h6>Voicemail Feed</h6>
                  
            </div>
            <div class="body">
				<div class="grid12">
              	<h6 style="margin-bottom:25px;font-size:14px;">Last 3 days</h6>
				  <?php 
						foreach($Voicemails as $vm){
						
						if($vm->call_uuid){
							
						$callid = PhoneRecordsModel::Fetch('call_uuid = "'.$vm->call_uuid .'" AND user_id = "'.$User->id.'"');
						$Prospect = ProspectModel::Fetch('phone = "'.phone_display($callid->caller_id).'" AND user_id = "'.$User->id.'"');
						$trans = TranscriptionsModel::Fetch('call_uuid = "'.$vm->call_uuid.'" AND call_uuid != ""');
							
						$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect->cid.'&pid='.$Prospect->id);
						
						if($trans) $tmessage = $trans->transcription;
						else $tmessage = 'Transcription unavailable.';
						$vmname = 'from <a href="'.$edit_action.'">'.$Prospect->name.'</a>';
							
						} else {
							$vmname = '';
							$tmessage = 'Transcription unavailable.';
						}

						$date = DateTime::createFromFormat('Y-m-d H:i:s', $vm->created);
						$callstamp = $date->format('g:i A');
						$datestamp = $date->format('D j M');
							
						if($vm->url) $download = '<a title="Download Recording" class="bDefault f_rt phoneDownload" href="'.$vm->url.'"><span class="export"></span></a>';
							
						$ctime = ConvertSectoAmount($vm->length);
						if($ctime['hours']>=1) $calltime .= round($ctime['hours']).' hrs ';
						if($ctime['minutes']>=1) $calltime .= round($ctime['minutes']).' mins ';
						if($ctime['seconds']) $calltime .= round($ctime['seconds']).' secs ';
					
					?>
					<div class="datestamp"><?=$datestamp?></div>
					<div class="feed-bubble">
						 <h4 ><span style="color:#e02a74;"><i class="icon-headphones"></i></span> Voicemail</h4>
						
						<h6><?=$callstamp?></h6>
							<p><?=$tmessage?> </p>
							<p>
							<?=$vmname?><br>
								Length: <?=$calltime?>
								<?php if($Prospect) { ?><a title="View Message" class="bDefault f_rt" href="<?=$edit_action?>"><span class="edit"></span></a><?php } ?> <?=$download?>
							</p>
						</div>

				  <?php $calltime = ''; } ?>
            
        				<h6 style="margin-top:25px;"><a href="/admin.php?action=inboxvoicemails&ctrl=Campaign">View All Voicemails</a></h6>
					</div>
				
			</div>
        </div>
</div>

	
</div>


<?php if($Proposals){ ?>	
<div class="grid12">
  <div class="widget">
    <div class="whead">
      <h6>Proposals Feed</h6>
    </div>
    <div class="fluid">
      <div class="top-start-cards2" style="padding:20px;">
        
			  
			  <?php foreach($Proposals as $Pro){ 
				$proposal = ProposalModel::Fetch('id = %d', $Pro->id);
				$pa = new ProposalDataAccess();
			    $products = $pa->getProductsbyProposal($proposal, array('proid' => $Pro->id));
				$Account = AccountsModel::Fetch('id = %d', $Pro->account_id);
				$Contact = ContactModel::Fetch('id = %d', $Pro->contact_id);
	?>

		<div class="grid3 top-start-card-single2">
          <div class="widget"> 
			  
			<div class="whead">
                <h6><a href="admin.php?pid=<?=$Contact->id?>&ID=<?=$Contact->cid?>&action=contact&ctrl=Campaign"><?=$Contact->first_name?> <?=$Contact->last_name?></a></h6>       
            </div>
			<div class="body">
				<div class="grid12">
					<div class="pro-title" style="margin-bottom:20px;">
					<h6 style="font-size:12px;">Proposal: <a href="admin.php?proid=<?=$Pro->id?>&ID=<?=$Pro->cid?>&action=proposal&ctrl=Campaign"><?=$Pro->name?></a></h6>
					<span>Expiration: <?=$Pro->expire_date?></span>
					</div>
					<div class="product-card">
								<?php foreach($products as $product){ 
									//var_dump($product->price);
									if($product->discount) {
										if(is_numeric($product->discount)) {
											$product->price	= $product->price - $product->discount;
											$product->price = number_format($product->price, 2);
											//var_dump($product->price);
										}else if (strpos($product->discount, '%') !== false) {
											$discount			= (int) str_replace("%","",$product->discount);
											$discount_amount	= ($discount / 100);
											$discount_amount	= $product->price * $discount_amount;
											$product->price		= $product->price - $discount_amount;
										}
									}
	
									if($product->qty > 1){
										$product->price = $product->price*$product->qty;
										$product->price = number_format($product->price, 2);
									}
									//$product->price = number_format($product->price, 2, '.', ',');
									
	
									$total += $product->price;
	
	

								?>
					
					<div class="product-card-title"><?=$product->name?></div>
					<div class="product-card-price">$<?=$product->price?></div>
					
								<?php } ?>
			
						
						<div style="clear:both;"></div>
						<div class="product-card-total"><a href="admin.php?proid=<?=$Pro->id?>&ID=<?=$Pro->cid?>&action=proposal&ctrl=Campaign"><i class="icon-cog"></i></a> <?php $total = number_format($total, 2, '.', ','); echo '$'.$total?></div>
					</div>
					<h6 style="font-size:12px;margin-top:15px;">Account: <a href="admin.php?aid=<?=$Account->id?>&ID=<?=$Account->cid?>&action=account&ctrl=Campaign"><?=$Account->name?></a></h6>
				</div>
			</div>
		
  </div>
        </div>
	<?php } ?>
			  
			
		
        <div style="clear:both;"></div>
      </div>
    </div>
  </div>
</div>
<?php } ?>


	<div class="grid12">
<div class="widget">	
	<div class="whead">
                <h6>Leads Feed</h6>  
			
            </div>
<div class="fluid">
<div class="top-start-cards" style="padding:20px;">
<?php foreach($Leads as $Lead){ 	

//recent emails
$EQ     =   Qube::Start()->queryObjects('EmailsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 1)->calc_found_rows();
$EQ->Where('T.lead_id = "'.$Lead->id.'" AND T.trashed = 0 ORDER BY T.created DESC');
$REmails  =   $EQ->Exec()->fetchAll();
	
//recent outbound texts
$TOQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 1)->calc_found_rows();
$TOQ->Where('T.to_sent = "1'.$Lead->phone.'" AND T.trashed = 0 ORDER BY T.created DESC');
$ROTexts  =   $TOQ->Exec()->fetchAll();
	
//recent inbound texts
$TIQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 1)->calc_found_rows();
$TIQ->Where('T.from_text = "1'.$Lead->phone.'" AND T.trashed = 0 ORDER BY T.created DESC');
$RITexts  =   $TIQ->Exec()->fetchAll();
	
//recent voicemails
$RVoicmails    =   Qube::Start()->
                    query('SELECT v.url, v.created FROM voicemails v, phone_records p WHERE p.user_id = "'.$this->User->getID().'" AND p.caller_id = "1'.$Lead->phone.'" AND p.user_id = v.user_id AND p.call_uuid = v.call_uuid ORDER BY v.created DESC LIMIT 1')
                ->fetchAll();
//recent inbound
$IQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 1)->calc_found_rows();
$IQ->Where('T.direction = "inbound" AND T.caller_id = "1'.$Lead->phone.'" ORDER BY T.date DESC');
$RInboundCalls =   $IQ->Exec()->fetchAll();
	
//recent outbound
$OQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 1)->calc_found_rows();
$OQ->Where('T.direction = "outbound" AND T.destination = "1'.$Lead->phone.'" ORDER BY T.date DESC');
$ROutboundCalls =   $OQ->Exec()->fetchAll();

//recent visits
$pda = new ProspectDataAccess();
$Prospect   =   $pda->getFullProspectInfoExternal($Lead->id, $this->User->getID());
$all_activity = $pda->getLastActivityProspect($Prospect, TRUE);
	
//lead date stamps	
$leaddate = DateTime::createFromFormat('Y-m-d H:i:s', $Lead->created);
$leadtimestamp = $leaddate->format('g:i A');
$leaddatestamp = $leaddate->format('D j M');
	
//check campaign form name
	if($Lead->touchpoint_ID!=0){
		$cf_name = Qube::Start()->
                    query('SELECT name FROM lead_forms WHERE id = "'.$Lead->form_ID.'"')
                ->fetchColumn();

	}
	
?>
<div class="grid3 top-start-card-single">
<div class="widget">
	<div class="whead">
                <h6><a href="<?=$this->action_url('Campaign_prospect?ID='.$Lead->cid.'&pid='.$Lead->id)?>"><?=$Lead->name?></a></h6>  
				<span style="float: right;font-size:16px;"><a href="<?=$this->action_url('Campaign_prospect?ID='.$Lead->cid.'&pid='.$Lead->id)?>"><i class="icon-cog"></i></a></span>
            </div>
	 <div class="body">
				<div class="grid12">
              	<h6 style="margin-bottom:10px;font-size:14px;">Recent Touches</h6>
				<div id="cards-contain-<?=$Lead->id?>" class="top-level-lead-card">
							<?php foreach($RInboundCalls as $RICall){
									$icsortdate = date('Y-m-d', $RICall->date);
									$icdate = DateTime::createFromFormat('Y-m-d H:i:s', $RICall->date);
									$ictimestamp = $icdate->format('g:i A');
									$icdatestamp = $icdate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$icsortdate?></div>
								<span style="color: #27bdbe;"><?=$icdatestamp?> at <?=$ictimestamp?></span>
								<div style="clear:both;">
								<span style="color:#5E57FF;"><i class="icon-phone"></i> inbound call</span> <?php if($RICall->recording){?><a title="Download Recording" class="bDefault f_rt phoneDownload" href="<?=$RICall->recording?>"><span class="export"></span></a><? } ?>
									</div>
							</div>
							<? } ?>
							<?php foreach($ROutboundCalls as $ROCall){
									$ocsortdate = date('Y-m-d', $ROCall->date);
									$ocdate = DateTime::createFromFormat('Y-m-d H:i:s', $ROCall->date);
									$octimestamp = $ocdate->format('g:i A');
									$ocdatestamp = $ocdate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$ocsortdate?></div>
								<span style="color: #27bdbe;"><?=$ocdatestamp?> at <?=$octimestamp?></span>
								<div style="clear:both;">
								<span style="color:#5E57FF;"><i class="icon-phone"></i> outbound call</span> <?php if($ROCall->recording){?><a title="Download Recording" class="bDefault f_rt phoneDownload" href="<?=$ROCall->recording?>"><span class="export"></span></a><? } ?>
									</div>
							</div>
							<? } ?>
							<?php foreach($ROTexts as $ROText){
									$txosortdate = date('Y-m-d', $ROText->created);
									$txodate = DateTime::createFromFormat('Y-m-d H:i:s', $ROText->created);
									$txotimestamp = $txodate->format('g:i A');
									$txodatestamp = $txodate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$txosortdate?></div>
								<span style="color: #27bdbe;"><?=$txodatestamp?> at <?=$txotimestamp?></span>
								<div style="clear:both;">
								<span style="color:#1e4382;"><i class="icon-comment_stroke"></i> Outbound: <?=$ROText->message?></span> 
									</div>
							</div>
							<? } ?>
							<?php foreach($RITexts as $RIText){
									$txisortdate = date('Y-m-d', $RIText->created);
									$txidate = DateTime::createFromFormat('Y-m-d H:i:s', $RIText->created);
									$txitimestamp = $txidate->format('g:i A');
									$txidatestamp = $txidate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$txisortdate?></div>
								<span style="color: #27bdbe;"><?=$txidatestamp?> at <?=$txitimestamp?></span>
								<div style="clear:both;">
								<span style="color:#1e4382;"><i class="icon-comment_stroke"></i> Inbound: <?=$RIText->message?></span> 
									</div>
							</div>
							<? } ?>
							<?php foreach($REmails as $REmail){
									$sortdate = date('Y-m-d', $REmail->created);
									$edate = DateTime::createFromFormat('Y-m-d H:i:s', $REmail->created);
									$etimestamp = $edate->format('g:i A');
									$edatestamp = $edate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$sortdate?></div>
								<span style="color: #27bdbe;"><?=$edatestamp?> at <?=$etimestamp?></span>
								<div style="clear:both;">
								<span style="color:#C57FE5;"><i class="icon-mail-3"></i>  <?=$REmail->subject?></span> 
									</div>
							</div>
							<? } ?>
							<?php foreach($RVoicmails as $RVoicemail){
									$vsortdate = date('Y-m-d', $RVoicemail['created']);
									$vdate = DateTime::createFromFormat('Y-m-d H:i:s', $RVoicemail['created']);
									$vtimestamp = $vdate->format('g:i A');
									$vdatestamp = $vdate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$vsortdate?></div>
								<span style="color: #27bdbe;"><?=$vdatestamp?> at <?=$vtimestamp?></span>
								<div style="clear:both;">
								<span style="color:#e02a74;"><i class="icon-headphones"></i> voicemail</span> <a title="Download Recording" class="bDefault f_rt phoneDownload" href="<?=$RVoicemail['url']?>"><span class="export"></span></a>
									</div>
							</div>
							<? } ?>
							<?php
							if($all_activity->lastVisits){
							
							foreach($all_activity->lastVisits as $visit):
								$aa_reverse = array_reverse($visit->actionDetails);
							foreach($aa_reverse as $action):
								
									$tsortdate = date('Y-m-d', $action->timestamp);
									$conversiontime = date('Y-m-d H:i:s', $action->timestamp);
									$tdate = DateTime::createFromFormat('Y-m-d H:i:s', $conversiontime);
									$ttimestamp = $tdate->format('g:i A');
									$tdatestamp = $tdate->format('D j M');
							?>
							<div class="lead-activity-card">
								<div class="dateorder" style="display:none;"><?=$vtsortdate?></div>
								<span style="color: #27bdbe;"><?=$tdatestamp?> at <?=$ttimestamp?></span>
								<div style="clear:both;">
								<span style="color:#ee6f23;"><i class="icon-globe"></i> Visited <?=$action->url?></span>
									</div>
							</div>
							<?php break; endforeach;?>
							<?php break; endforeach;
							}
							?>
					</div>
					<?php if($Prospect->touchpoint_ID!=0){ ?>
					<h6 style="font-size:12px;margin-top:15px;">First Touch: <span style="color: #27bdbe;"><?=$cf_name?></span></h6>
					<?php } else { ?>
					<h6 style="font-size:12px;margin-top:15px;">First Touch: <span style="color: #27bdbe;">Manually Added</span></h6>
					<? } ?>
					<h6 style="font-size:12px;">Date: <span style="color: #27bdbe;"><?=$leaddatestamp?> at <?=$leadtimestamp?></span></h6>
		 </div>
	</div>
	
	
</div>
</div>
<?php } ?>
	<div style="clear:both;"></div>
	</div>	
		</div>
	</div>
</div>
					<script>
   function sortDescending(a, b) {
     var date1  = $(a).find(".dateorder").text();
       date1 = date1.split('-');
     date1 = new Date(date1[2], date1[1] -1, date1[0]);
     var date2  = $(b).find(".dateorder").text();
       date2= date2.split('-');
     date2= new Date(date2[2], date2[1] -1, date2[0]);

     return date1 < date2 ? 1 : -1;
    };
    $(document).ready(function() {
		<?php foreach($Leads as $Lead){ 	?>
        $('#cards-contain-<?=$Lead->id?> .lead-activity-card').sort(sortDescending).appendTo('#cards-contain-<?=$Lead->id?>');
		<?php } ?>
		
		var divs = $(".top-start-cards > .top-start-card-single");
		for(var i = 0; i < divs.length; i+=4) {
		  divs.slice(i, i+4).wrapAll("<div style=\"clear:both;\"></div>");
		}

    }); 

</script>
<script type="text/javascript">
<?php $this->start_script(); ?>    
    $(function() {


	});
<?php $this->end_script(); ?>
</script>
