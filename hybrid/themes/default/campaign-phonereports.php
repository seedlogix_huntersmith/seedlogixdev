<?php

$lbase  =   $this->action_url('Campaign_phone?ID=' . $Campaign->getID(), array('do' => 'reports',
            'mid' => $mid));
$bold   =   ''; // ?
$limit  =   false;

/* 
 * 
 * $bold    =   ''
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

?>
<h1>Call Reports</h1><br />

<a href="<?= $this->action_url('Campaign_phone?ID=' . $Campaign->getID(),
            array('do' => 'rec',
                    'mid' => $mid)); ?>">Recordings</a>
<a href="<?= $this->action_url('Campaign_phone?ID=' . $Campaign->getID(),
            array('mid' => $mid)); ?>">Services</a>
<p style="font-size:12px;">View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>&rqtype=Today" class="dflt-link" <? if($rqtype=='Today') echo $bold; ?>>Today</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>&rqtype=PreviousDay" class="dflt-link" <? if($rqtype=='PreviousDay') echo $bold; ?>>Previous Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>&rqtype=PreviousWeek" class="dflt-link" <? if($rqtype=='PreviousWeek') echo $bold; ?>>Previous Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>&rqtype=Previous7Days" class="dflt-link" <? if($rqtype=='Previous7Days') echo $bold; ?>>Previous 7 Days</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>&rqtype=PreviousMonth" class="dflt-link" <? if($rqtype=='PreviousMonth') echo $bold; ?>>Previous Month</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>&rqtype=PreviousQuarter" class="dflt-link" <? if($rqtype=='PreviousQuarter') echo $bold; ?>>Previous Quarter</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>&rqtype=MonthToDate" class="dflt-link" <? if($rqtype=='MonthToDate') echo $bold; ?>>Month to Date</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>&rqtype=YearToDate" class="dflt-link" <? if($rqtype=='YearToDate') echo $bold; ?>>Year to Date</a>
                </p>
<? if($totalResults){ ?><br /><h2><?=$totalResults?> Total Calls</h2><? } ?>
<br style="clear:both; margin-bottom:20px;" />


<div id="message_cnt" class="hide"></div>
							               
<? if($callReports && is_array($callReports)){
	echo '<div class="mailboxContain">';
	echo '<ul class="prospects">
	<li style="width:10%;"><strong>Date</strong></li>
	<li style="width:15%;"><strong>Caller ID</strong></li>
	<li style="width:15%;"><strong>City</strong></li>	
	<li style="width:5%;text-align:center;"><strong>State</strong></li>	
	<li style="width:15%;text-align:center;"><strong>Destination</strong></li>
	<li style="width:5%;text-align:center;"><strong>Length</strong></li>	
</ul>';
	foreach($callReports as $json=>$reports){	
		$date = date('m/j/Y', preg_replace('/[^\d]/','', $reports['DateTime'])/1000);   
		echo '
		<ul class="prospects">
	<li style="width:10%;" >'.$date.'</li>
	<li style="width:15%;">'.$reports['CallerId'].'</li>
	<li style="width:15%;">'.$reports['City'].'</li>	
	<li style="width:5%;text-align:center;">'.$reports['State'].'</li>	
	<li style="width:15%;text-align:center;">'.$reports['PhoneNumber'].'</li>
	<li style="width:5%;text-align:center;">'.$reports['CallDuration'].'</li>
		</ul>
		';
	}
	echo '</div><br />';
	if($totalResults){
		if($limit && ($totalResults > $limit)){
			//pagination
			$numPages = ceil($numProspects/$limit);
			if($offset>0) $currPage = ($offset/$limit)+1;
			else $currPage = 1;
			echo '<p style="font-weight:bold;">';
			if($currPage > 1){
				echo '<a title="Start of Prospects" href="'.$pagibase.'?offset=0';
				echo '"><img src="img/v3/nav-start-icon.jpg" border="0" /></a>&nbsp;';
				echo '<a title="Previous Set of Prospects" href="'.$pagibase.'?offset='.($offset-$limit);
				echo '"><img src="img/v3/nav-previous-icon.jpg" border="0" /></a> ';
			}
			if($currPage>1 && $currPage<$numPages){
				echo '';
			}
			if($currPage < $numPages){
				echo ' <a title="Next Set of Prospects" href="'.$pagibase.'?offset='.($offset + $limit);
				echo '"><img src="img/v3/nav-next-icon.jpg" border="0" /></a>';
				echo ' <a title="End of Prospects" href="'.$pagibase.'?offset='.(($numPages*$limit)-$limit);
				echo '"><img src="img/v3/nav-end-icon.jpg" border="0" /></a>';
			}
			echo '</p><br />';
		}
	}
	
} else { ?>

<? if($totalResults=='0') { ?>
<p>No Results</p>
<? } else { ?>
<p>Service line connection lost.  Please try again in 10 seconds. <a href="phone_view_call_details.php?mid=<?=$mid?>&rqtype=<?=$rqtype?>">Click here to refresh</a>.</p>
<? } ?>

<? } ?>

