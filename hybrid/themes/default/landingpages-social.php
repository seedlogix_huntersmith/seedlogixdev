<!-- LEFT SIDE NAV -->

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

			<div class="fluid">
			 <!-- section -->     
                         <form action="<?php echo $this->action_url('Website_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">
                    <div class="widget grid6">
                        <div class="whead"><h6>Social Networking Connections</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Your Blog URL:</label></div>
                        	<div class="grid9"><input type="text" value="<?php  ?>" name="website[blog]" placeholder="http://www.myblog.com"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>twitter username:</label></div>
                        	<div class="grid9"><input type="text" value="<?php  ?>" name="website[twitter]" placeholder="@6qube"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Facebook:</label></div>
                        	<div class="grid9"><input type="text" value="<?php  ?>" name="website[facebook]" placeholder="http://www.facebook.com/6qube"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>linkedIn:</label></div>
                        	<div class="grid9"><input type="text" value="<?php  ?>" name="website[linkedin]" placeholder="www.linkedin.com/in/jonathanaguilar"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                         </form>
                    
			</div> <!-- //end fluid -->

<!-- //end content container -->
</div>
<!-- //end content container -->