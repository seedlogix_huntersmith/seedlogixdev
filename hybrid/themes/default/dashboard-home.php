<!--
    JumpTrack : 1167
    FastTrack : 1168
    AdvTrack : 1169
    ProTrack : 1170
    ProTrack + : 1171
-->
<div class="fluid">
  <div class="widget">
    <div class="whead">
        <h6>Total Sales</h6>
        <div style="margin-right:45px;" class="right">
            
        </div>
        <div class="clear"></div>
    </div>
    <div id="dyn">
      <table id="jTotalSales" class="tDefault dTable">
        <thead>
          <tr>
            <td>Sales Professional</td>
            <td>Company</td>
            <td>Product</td>
            <td>Sale Total</td>
            <td>Commission %</td>
            <td>Commission Total</td>
            <td>Date</td>
          </tr>
        </thead>
        <tbody class="dataTableBody">
        <?php
            $this->getRow('dashboard-totalSales.php');
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /end fluid -->

<!--
    JumpTrack : 1167
    FastTrack : 1168
    AdvTrack : 1169
    ProTrack : 1170
    ProTrack + : 1171
-->
<div class="fluid">
  <div class="widget">
    <div class="whead">
        <h6>Clients Sold</h6>
        <div style="margin-right:45px;" class="right">
            
        </div>
        <div class="clear"></div>
    </div>
    <div id="dyn2">
      <table id="jClientsSold" class="tDefault dTable">
        <thead>
          <tr>
            <td>Sales Professional</td>
            <td>Company</td>
            <td>Product</td>
            <td>Date</td>
            <td>Actions</td>
          </tr>
        </thead>
        <tbody class="dataTableBody">
        <?php
            $this->getRow('dashboard-clientsSold.php');
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /end fluid -->

<!--
    Sales Professional : 1217
    Sales Professional + : 1218
    Sales Manager : 1219
-->
<div class="fluid">
  <div class="widget">
    <div class="whead">
        <h6>Sales Team (Managers only)</h6>
        <div style="margin-right:45px;" class="right">
            
        </div>
        <div class="clear"></div>
    </div>
    <div id="dyn">
      <table id="jSalesTeam" class="tDefault dTable">
        <thead>
          <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Phone</td>
            <td>Team Role</td>
            <td>Join Date</td>
          </tr>
        </thead>
        <tbody class="dataTableBody">
        <?php
            $this->getRow('dashboard-salesTeam.php');
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /end fluid -->

<script type="text/javascript">
<?php $this->start_script(); ?>    
    $(function (){ //ON PAGE LOAD
        
        //Call Reports dTable
        $('#jTotalSales').dataTable({
            "aoColumns": [
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"}
            ],
            "aaSorting": [[6,'desc']],
            "sDom" : '<"H"lf>rt<"F"ip>'
        });
        
        //Clients Sold dTable
        $('#jClientsSold').dataTable({
            "aoColumns": [
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center", "bSortable": false}
            ],
            "aaSorting": [[3,'desc']],
            "sDom" : '<"H"lf>rt<"F"ip>'
        });
        
        //Sales Team dTable
        $('#jSalesTeam').dataTable({
            "aoColumns": [
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"}
            ],
            "aaSorting": [[4,'desc']],
            "sDom" : '<"H"lf>rt<"F"ip>'
        });

    });
<?php $this->end_script(); ?>
</script>