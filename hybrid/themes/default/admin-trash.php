<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

?>

    <form action="<?= $this->action_url('Admin_restore'); ?>" method="POST">
<div class="widget">
    <div class="whead"><span class="titleIcon check"><input type="checkbox" id="titleCheck" name="titleCheck" /></span>
        <h6>System Trash</h6><div class="clear"></div></div>
    <table cellpadding="0" cellspacing="0" width="100%" class="tDefault checkAll check" id="checkAll">
        <thead>
            <tr>
                <td><img src="<?= $_path; ?>images/elements/other/tableArrows.png" alt="" /></td>
                <td>Object</td>
                <td>Name</td>
                <td>Deleted</td>
            </tr>
        </thead>
        <tbody><?php if($pdoresult->rowCount() == 0): ?>
            <tr><td colspan="4">The Trash bin is empty.</td></tr>
            
            
            <?php else: while($row = $pdoresult->fetch(PDO::FETCH_OBJ)): ?>
            <tr>
                <td><input type="checkbox" name="trash[]" value="<?= $row->table . '-' . $row->id; ?>" /></td>
                <td><?= $row->table; ?></td>
                <td><?= $row->name; ?></td>
                <td><?= $row->trashed; ?></td>
            </tr><?php endwhile; endif; ?>
        </tbody>
    </table>
</div><div class="formRow">
                    <input type="submit" class="buttonS bLightBlue" value="Restore Objects" />
    </div>
    </form>