<!-- LEFT SIDE NAV -->

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

<br class="clear" /><br />

                        <form action="<?php echo $this->action_url('Website_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">
			<div class="fluid">
                    <!-- section -->
                    <div class="widget grid6">
                        <div class="whead"><h6>Main Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Company Name:</label></div>
                        	<div class="grid9"><input type="text" value="<?php  ?>" name="website[company_name]"   /></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Phone Number:</label></div>
                        	<div class="grid9"><input type="text" name="website[phone]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Address:</label></div>
                        	<div class="grid9"><input type="text" name="website[street]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>City:</label></div>
                        	<div class="grid9"><input type="text" name="website[city]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>State:</label></div>
                        	<div class="grid9"><input type="text" name="website[state]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Zip Code:</label></div>
                        	<div class="grid9"><input type="text" name="website[zip]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                                           
                    <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Brand Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Company Logo:</label></div>
                        	<div class="grid9"><input type="text" name="website[logo]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Slogan/Tagline:</label></div>
                        	<div class="grid9"><input type="text" name="website[slogan]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Website Description:</label></div>
                        	<div class="grid9"><input type="text" name="website[description]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
                    <!-- section -->
                    <div class="widget grid6 widgetpadding">
                        <div class="whead"><h6>Domain Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Domain:</label></div>
                        	<div class="grid9"><input type="text" name="website[domain]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
			</div> <!-- //end fluid -->
                        </form>
	
<!-- //end content container -->
</div>
<!-- //end content container -->