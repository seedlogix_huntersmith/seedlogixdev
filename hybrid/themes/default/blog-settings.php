<?php $canEdit = !$User->can('edit_BLOG',$blog->getID()) ? ' disabled' : ''; ?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <form action="<?= $this->action_url('Blog_save?ID='.$blog->id); ?>" method="POST" class="ajax-save">
            <div class="widget grid6">
                <div class="whead"><h6>Blog Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Blog Title:</label></div>
                    <div class="grid9"><input type="text" value="<?php $this->p($blog->blog_title); ?>" name="blog[blog_title]"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Website URL:</label></div>
                    <div class="grid9"><input type="text" name="blog[website_url]" value="<?php $this->p($blog->website_url); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Company Name:</label></div>
                    <div class="grid9"><input type="text" name="blog[company_name]" value="<?php $this->p($blog->company_name); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Phone Number:</label></div>
                    <div class="grid9"><input type="text" name="blog[phone]" value="<?php $this->p($blog->phone); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Address:</label></div>
                    <div class="grid9"><input type="text" name="blog[address]" value="<?php $this->p($blog->address); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>City:</label></div>
                    <div class="grid9"><input type="text" name="blog[city]" value="<?php $this->p($blog->city); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>State/Province:</label></div>
                    <div class="grid9"><div class="<?= $gridLength; ?> noSearch"><?php $this->getWidget('states-dropdown.php',array('name' => 'blog[state]','value' => $site->state,'disable' => $canEdit)); ?></div></div>
                </div>
            </div>
            <div class="widget grid6">
                <div class="whead"><h6>Brand Settings</h6></div>
                <div class="formRow">
					<div class="grid3"><label>Blog Logo:</label></div>
					<div class="grid9">

<?php
$this->getWidget('moxiemanager-filepicker.php',array(
    'name'          => 'blog[logo]',
    'value'         => $blog->logo,// @todo fix get(logo) to include path
    'idWidget'      => 'blog-logo-widget',
    'path'          => $storagePath,
    'noHost'        => true,
    'extensions'    => 'jpg,jpeg,png,gif',
    'disable'       => !empty($canEdit)
));
?>

                    </div>
                </div>
            </div>
            <div class="widget grid6 m_lt">
                <div class="whead"><h6>Domain Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Domain:</label></div>
                    <div class="grid9"><input type="text" name="blog[domain]" value="<?php $this->p($blog->domain); ?>"<?= $canEdit; ?>></div>
                </div>
            </div>
            <div class="widget grid6">
                <div class="whead"><h6>Connect To Website</h6></div>
                <div class="formRow">
                    <div class="grid12">
                        <p>Select one of your Websites to synchronize this blog with. This will change this blog's theme and settings to match that Website.</p>
                        <p>If you make changes to your website's settings you will have to reconnect it.</p>
                        <p>A blog can only be connected to one website at a time.</p>
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Blog:</label></div>
                    <div class="grid9">

<?php
$this->getWidget('select-ajax.php',array(
    'id'            => 'connectWebsite',
    'name'          => 'blog[connect_hub_id]',
    'ajaxUrl'       => $this->action_url('Blog_ajaxSites',array('ID' => $blog->getID())),
    'field_value'   => 'id',
    'text'          => ':name:',
    'value'         => $sites
));
?>

                    </div>

<?php $this->getWidget('admin-lock.php',array('fieldname' => 'connect_hub_id')); ?>

                </div>
            </div>
    </form>
</div>

<script>

<?php $this->start_script(); ?>

$(document).ready(function(){

<?php $this->getJ('wysiwyg-ide.php',array('disable' => !empty($canEdit))); ?>

});

function logoinsert(args){
    var form        = $('form');
    var input       = $('input[name="blog[logo]"]',form);
    var preview     = $('#blog-logo-preview');
    var newValue    = args.files[0].url;
    input.val(args.files[0].url);
    new AjaxSaveDataRequest(input.get(0),newValue);
    var image       = preview.find('img');
    if(image.length == 0){
        image       = $('<img class="preview" src=""/>');
        preview.append(image);
    }
    image.attr('src','users' + newValue);
}

<?php $this->end_script(); ?>

</script>