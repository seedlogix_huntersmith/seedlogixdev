<?php
/**
 * @var $this Template
 */
?>
<div class="fluid chosen_dropfix">

    <form action="<?php //echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save">
        <div class="widget grid12">
            <div class="whead">
				<h6>User _#34994_ Billing Info</h6>
                <div class="buttonS bGreen right dynRight jopendialog jopen-createReport">Open Modal</div>
                <a class="buttonS bBlue right editFields" href="#">
                    <!-- Pencil Icon -->
                    <span class="iconb" data-icon="&#xe1db;"></span><span>Edit Profile</span>
                </a>
			</div>
            <div class="formRow">
                <div class="grid3"><label>First Name:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">FirstName</span>
				    <input type="text" class="edit" name="billinginfo[firstname]" value="" />
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Last Name:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">LastName</span>
                    <input type="text" class="edit" name="billinginfo[lastname]" value="">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Phone:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Phone </span>
                    <input type="text" class="edit" name="billinginfo[phone]" value="">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Company Name:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Company</span>
                    <input type="text" class="edit" name="billinginfo[company]" value="">
                </div>
            </div>
            <div class="formRow noBorderB">
                <div class="grid3"><label>Address:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Address</span>
                    <input type="text" class="edit" name="billinginfo[address]" value="">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Address 2:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Address</span>
                    <input type="text" class="edit" name="billinginfo[address2]" value="">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>City:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">City</span>
                    <input type="text" class="edit" name="billinginfo[city]" value="">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>State:</label></div>
                <div class="grid9">
                    <?php $this->getWidget('states-dropdown.php', array('value' => 'TX', 'name' => 'billinginfo[state]'));?>
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Zip:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Zip</span>
                    <input type="text" class="edit" name="billinginfo[zip]" value="">
                </div>
            </div>
			<div class="formRow">          
				<div class="grid3">Card Number: <span class="note">last 4</span></div>
				<div class="grid9 editSwitcher">
                    <span class="editValue">Card Number</span>
                    <input type="text" class="edit" name="billinginfo[cc]" value="">
                    <span class="note">numbers only</span>
                </div>
			</div>
            <div class="formRow">
                <div class="grid3"><label>Expiration Date:</label></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">Expiration Date</span>
                    <input type="text" class="edit" name="billinginfo[expDate]" value="" id="expDate">
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>CCV Code:</label><span class="note">3 - 4 digit code on back</span></div>
                <div class="grid9 editSwitcher">
                    <span class="editValue">CCV Code</span>
                    <input type="text" class="edit" name="billinginfo[CCV]" maxlength="4" value="">
                </div>
            </div>
        </div>
    </form>

</div>

<!-- Create Auto-Responder POPUP -->
<?php $this->getDialog('billing-profile.php'); ?>

<script type="text/javascript">
<?php $this->start_script()?>
    $(document).ready(function() {
       
        //Edit View
		$('a.editFields').click(function(){
			
			$('.editSwitcher > .editValue').toggle();
			$('.editSwitcher > input.edit').toggle();
			return false;
		});
        
    });
<?php $this->end_script()?>
</script>