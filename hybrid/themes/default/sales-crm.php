<?php

if($_GET['d']){
	$date = DateTime::createFromFormat("Y-m-d", $_GET['d']);
	$reportDate = $date->format("F Y");
} else $reportDate = date('F Y');

//var_dump($ActiveProposals);

//$ctime = PhoneRecordsModel::ConvertSectoAmount($iTotalIBTalkTime);
$phoneRecords = new PhoneRecordsModel();
$ctime = $phoneRecords->ConvertSectoAmount($iTotalIBTalkTime);

if($ctime['hours']>=1) $calltime .= round($ctime['hours']).' hrs ';
if($ctime['minutes']>=1) $calltime .= round($ctime['minutes']).' mins ';
if($ctime['seconds']) $calltime .= round($ctime['seconds']).' secs ';

$totalActive = 0;
$totalExpired = 0;
$totalWon = 0;
$totalPropActive = 0;
$totalPropExpired = 0;
$totalPropWon = 0;

foreach($ActiveProposals as $ActiveProposal){

	if($ActiveProposal->status=='Active' && $ActiveProposal->expire_date > date("Y-m-d")){
		
		$totalPropActive += 1;
		$totalActive  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ActiveProposal->id.'" AND trashed = 0')
                ->fetchColumn();
	}
	if($ActiveProposal->expire_date < date("Y-m-d")){
		
		$totalPropExpired += 1;
		$totalExpired  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ActiveProposal->id.'" AND trashed = 0')
                ->fetchColumn();
	}
}
foreach($ClosedProposals as $ClosedProposal){
	
	$totalPropWon += 1;
	$totalWon  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ClosedProposal->id.'" AND trashed = 0')
                ->fetchColumn();
}

if(!$iTotalProposals) $iTotalProposals =0;
if(!$iTotalNotes) $iTotalNotes =0;
if(!$iTotalTexts) $iTotalTexts =0;
if(!$iTotalEmails) $iTotalEmails =0;
if(!$iTotalOBCalls) $iTotalOBCalls =0;
if(!$iTotalIBCalls) $iTotalIBCalls =0;
if(!$iTotalTasks) $iTotalTasks =0;
if(!$percentagesales) $percentagesales =0;
$totals = $iTotalProposals+$iTotalNotes+$iTotalTexts+$iTotalEmails+$iTotalOBCalls+$iTotalIBCalls+$iTotalTasks;

if($totalWon>=400) $percentagesales += '10'; 
if($totalWon>=800) $percentagesales += '10';
if($totalWon>=1200) $percentagesales += '10';
if($totalWon>=1600) $percentagesales += '10';
if($totalWon>=2000) $percentagesales += '10';
if($totalWon>=2400) $percentagesales += '10';
if($totalWon>=2800) $percentagesales += '10';
if($totalWon>=3200) $percentagesales += '10';
if($totalWon>=3600) $percentagesales += '10';
if($totalWon>=4000) $percentagesales += '10';

if($iTotalIBTalkTime>=18000) $percentage += '5'; //20
if($iTotalIBTalkTime>=36000) $percentage += '5'; //20
if($iTotalIBTalkTime>=54000) $percentage += '5'; //20
if($iTotalIBTalkTime>=72000) $percentage += '5'; //20

if($iTotalOBCalls>=150) $percentage += '5'; //20
if($iTotalOBCalls>=300) $percentage += '5'; //20
if($iTotalOBCalls>=450) $percentage += '5'; //20
if($iTotalOBCalls>=600) $percentage += '5'; //20

if($iTotalProposals>=3) $percentage += '5'; //20
if($iTotalProposals>=6) $percentage += '5'; //20
if($iTotalProposals>=9) $percentage += '5'; //20
if($iTotalProposals>=12) $percentage += '5'; //20

if($iTotalLeads>=10) $percentage += '5'; //20
if($iTotalLeads>=20) $percentage += '5'; //20
if($iTotalLeads>=30) $percentage += '5'; //20
if($iTotalLeads>=40) $percentage += '5'; //20

if($iTotalTexts>=25) $percentage += '2.5'; //10
if($iTotalTexts>=50) $percentage += '2.5'; //10
if($iTotalTexts>=75) $percentage += '2.5'; //10
if($iTotalTexts>=100) $percentage += '2.5'; //10

if($iTotalEmails>=75) $percentage += '2.5'; //10
if($iTotalEmails>=150) $percentage += '2.5'; //10
if($iTotalEmails>=225) $percentage += '2.5'; //10
if($iTotalEmails>=300) $percentage += '2.5'; //10

foreach($Leads as $Lead){
	if($Lead->source=='Organic Search') $organic += 1;
	else if($Lead->source=='Business Card') $bcard += 1;
	else if($Lead->source=='Trade Show') $tradeshow += 1;
	else if($Lead->source=='Cold Call') $coldcall += 1;
	else if($Lead->source=='Inbound Call') $inboundcall += 1;
	else if($Lead->source=='Facebook Ad - Lead Form') $fblead += 1;
	else if($Lead->source=='Facebook Ad - Like Lead') $fblike += 1;
	else if($Lead->source=='Facebook Ad - Comment Lead') $fbcomment += 1;
	else if($Lead->source=='Google Ad') $gad += 1;
	else $nosource += 1;
}

if($_GET['d']){
$date = DateTime::createFromFormat("Y-m-d", $_GET['d']);
$year = $date->format("Y");
$month = $date->format("m");
$d = cal_days_in_month(CAL_GREGORIAN,$month,$year);	
	// for each day in the month
for($i = 1; $i <=  $d; $i++)
{
   // add the date to the dates array
   $dates[] = $year . "-" . $month . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
}
} else {
// for each day in the month
for($i = 1; $i <=  date('d'); $i++)
{
   // add the date to the dates array
   $dates[] = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
}	
	
}

?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>

<!-- START CSS for datepicker show only month and year START-->
<style type="text/css">
	.ui-datepicker-calendar {
		display: none;
	}
	.floatDatePicker {
		float: right;
		width: 200px;
		margin-right: 10px;
	}
	.ui-datepicker .ui-datepicker-title select{
		font-size: 16px;
		border: 0px;
	}
	.ui-datepicker-inline{
		margin-top: 0px;
	}
	#radioLabel {
		margin-right: 15px;
		line-height: 32px;
	}
	.jFilterDateCRM{
		height: 38px;
		line-height: 38px;
		box-shadow: 0 0.46875rem 2.1875rem 
			rgba(4, 9, 20, 0.03), 0 0.9375rem 1.40625rem 
			rgba(4, 9, 20, 0.03), 0 0.25rem 0.53125rem 
			rgba(4, 9, 20, 0.05), 0 0.125rem 0.1875rem 
			rgba(4, 9, 20, 0.03);
	}
	.ui-datepicker {
		border-bottom: 0px;
		position: relative;
		box-shadow: 0 0.46875rem 2.1875rem 
			rgba(4, 9, 20, 0.03), 0 0.9375rem 1.40625rem 
			rgba(4, 9, 20, 0.03), 0 0.25rem 0.53125rem 
			rgba(4, 9, 20, 0.05), 0 0.125rem 0.1875rem 
			rgba(4, 9, 20, 0.03);
		border-width: 0;
		transition: all .2s;
		overflow: visible;
	}

	.ui-datepicker .ui-datepicker-header {
		background: #fff;
		border-bottom: 0px;
	}

	.ui-datepicker .ui-datepicker-title select{
		color: #27bdbe;
	}
</style>
<!-- END CSS for datepicker show only month and year END-->
	
<div class="fluid">
	

	<!-- section -->
		<!-- ================= START Datepicker for filter the data by date  =================  -->
		<div style="top: 40px; margin: 0px;" >
			<ul role="tablist">
				<a class="buttonS f_rt jFilterDateCRM" tabindex="0" id="jFilterDateCRM" onclick="refreshDateRange();">Refresh </a> 
				<div style="float: right;">
					<input id="addRangeDate" type="checkbox" name="hasDateRange" value="range" style="margin-top: 14px;">
					<label id="radioLabel" for="addRangeDate" style="font-size: 20px;">Range</label>
				</div>
				<div id="datepickerTo" class="floatDatePicker" style=""></div>
				<div id="datepickerFrom" class="floatDatePicker" style=""></div>
			</ul>
		</div>

		<script type="text/javascript">

			$(document).ready(function () {

				//console.log('dateFrom &d: <?=$reportDate?>');
				var getUrlParameter = function getUrlParameter(sParam) {
					var sPageURL = window.location.search.substring(1),
						sURLVariables = sPageURL.split('&'),
						sParameterName,
						i;

					for (i = 0; i < sURLVariables.length; i++) {
						sParameterName = sURLVariables[i].split('=');
						if (sParameterName[0] === sParam) {
							return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
						}
					}
				};

				$(function() {
					$('#datepickerFrom').datepicker({
						changeMonth: true,
						changeYear: true,
						showButtonPanel: false,
						dateFormat: 'yy-mm'
					});
				});

				$(function() {
					$('#datepickerTo').datepicker( {
						changeMonth: true,
						changeYear: true,
						showButtonPanel: false,
						dateFormat: 'yy-mm'
					});
				});

				var //form = $('#myForm'),
					checkbox = $('#addRangeDate'),
					toggleRangeBlock = $('#datepickerTo');

				toggleRangeBlock.hide();

				checkbox.on('click', function() {
					if($(this).is(':checked')) {
						toggleRangeBlock.show("fast", function showNext() {
							$( this ).next( "div" ).show( "fast", showNext );
						});
						toggleRangeBlock.find('input').attr('required', true);
					} else {
						toggleRangeBlock.hide( 500 );
						// toggleRangeBlock.find('input').attr('required', false);
					}
				})

				var jsDateFrom = $('#datepickerFrom').datepicker('getDate');

				function monthAdd(date, month) {
					var tempDate = date;
					tempDate = new Date(date.getFullYear(), date.getMonth(), 1);
					tempDate.setMonth(tempDate.getMonth() + (month + 1));
					tempDate.setDate(tempDate.getDate() - 1); 

					if (date.getDate() < tempDate.getDate()) {
						tempDate.setDate(date.getDate()); 
					}

					return tempDate;    
				}
				//console.log(monthAdd(new Date('2019-04-01'),0));
				
				
				if (getUrlParameter('d')!= undefined) { // If the page is loading a DATE result then show the dates in the datepickerFrom
					var dateGot = monthAdd(new Date(getUrlParameter('d')),1);

					$("#datepickerFrom, selector").datepicker({
						changeMonth: true,
						changeYear: true,
						showButtonPanel: false,
						dateFormat: 'yy-mm'

					}).datepicker('setDate', dateGot);
				}
				if (getUrlParameter('r')!= undefined) { // If the page is loading a RANGE DATE result then show the dates in the datepickerTo
					dateGot = monthAdd(new Date(getUrlParameter('r')),1);

					$("#datepickerTo").datepicker({
						changeMonth: true,
						changeYear: true,
						showButtonPanel: false,
						dateFormat: 'yy-mm'
					}).datepicker('setDate', dateGot);
					toggleRangeBlock.show("fast", function showNext() {
						$( this ).next( "div" ).show( "fast", showNext );
					});
					$('#addRangeDate').prop('checked', true);
					//toggleRangeBlock.find('input').attr('required', true);
				}	

			});

			// Refresh button, refresh the page and get the new data with the date(s) selected.
			function refreshDateRange() {
				var newUrl = location.protocol + '//' + location.host + '/admin.php?action=salescrm&ctrl=Campaign';
				//console.log(newUrl);
				var month = $("#datepickerFrom .ui-datepicker-month :selected").val();
				var year = $("#datepickerFrom .ui-datepicker-year :selected").val();
				$('#datepickerFrom').datepicker('setDate', (new Date(year, month, 1)));
				
				newUrl += '&d='+year+'-'+("0" + (parseInt(month) + parseInt(1))).slice(-2)+'-01';

				if ($('#addRangeDate').is(':checked')) {
					var month = $("#datepickerTo .ui-datepicker-month :selected").val();
					var year = $("#datepickerTo .ui-datepicker-year :selected").val();
					$('#datepickerTo').datepicker('setDate', (new Date(year, month, 1)));
					newUrl += '&r='+year+'-'+("0" + (parseInt(month) + parseInt(1))).slice(-2)+'-01';
				}
				javascript:window.location.href = newUrl; 
			}

		</script>
		<!-- ================= END Datepicker for filter the data by date  =================  -->
	
		<div class="widget grid12" style="margin-top: 10px;">
			<div class="whead">
				<h6 class="timelineName"><!-- <?=$reportDate?> --> Activity</h6>
			</div>
			<div class="body">
				<div class="grid12">
					<ul class="wInvoice" style="text-align:center;">
						<li><h4 style="color:#41CE1E;font-size:55px;">$<?=number_format($totalWon)?></h4><span>won</span></li> 
						<li><h4 style="color:#00aeef;font-size:55px;">$<?=number_format($totalActive)?></h4><span>active</span></li> 
						<li><h4 style="color:#FF2626;font-size:55px;">$<?=number_format($totalExpired)?></h4><span>expired</span></li> 
						<li><h4 style="color:#27bdbe;font-size:55px;"><?=$percentagesales?>%</h4><span>quota</span></li>
						<li><h4 style="color:#C57FE5;font-size:55px;"><?=$totalPropWon?></h4><span>won</span></li> 
						<li><h4 style="color:#5E57FF;font-size:55px;"><?=$totalPropActive?></h4><span>active</span></li>
						<li><h4 style="color:#1e4382;font-size:55px;"><?=$totalPropExpired?></h4><span>expired</span></li>
					</ul>
			
					<div class="contentProgress"><div class="barG tipN" title="<?=$percentagesales?>%" id="bar7"></div></div>
					<ul class="ruler">
						<li>0</li>
					    <li class="textC">50%</li>
					    <li class="textR">100%</li>
					</ul>
				</div>
				<div class="grid12" style="margin-top:25px;">
					<ul class="wInvoice" style="text-align:center;">
							<li><h4 style="color:#41CE1E"><?=$iTotalLeads?></h4><span>leads</span></li>  
							<li><h4 style="color:#FF6C10"><?=$iTotalAccounts?></h4><span>accounts</span></li>  
							<li><h4 style="color:#61E4FF"><?=$iTotalContacts?></h4><span>contacts</span></li> 
						<li><h4 style="color:#e02a74"><?=$iTotalProposals?></h4><span>proposals</span></li>
							<li><h4 style="color:#C57FE5"><?=$totals?></h4><span>touches</span></li>              
							<li><h4 style="color:#FF0DFF"><?=$iTotalOBCalls?></h4><span>outbound calls</span></li>
							<li><h4 style="color:#27bdbe"><?=$iTotalIBCalls?></h4><span>inbound calls</span></li>
						    <li><h4 style="color:#5E57FF"><?=$calltime?></h4><span>total talk time</span></li>
							<li><h4 style="color:#00aeef"><?=$iTotalEmails?></h4><span>emails</span></li>
							<li><h4 style="color:#1e4382"><?=$iTotalTexts?></h4><span>texts</span></li>
							<li><h4 style="color:#612c5c"><?=$iTotalNotes?></h4><span>notes</span></li>
							<li><h4 style="color:#ee6f23"><?=$iTotalTasks?></h4><span>tasks</span></li>
							
                        </ul>
			
                        <div class="contentProgress"><div class="barG tipN" title="<?=$percentage?>%" id="bar8"></div></div>
                        <ul class="ruler">
                            <li>0</li>
                            <li class="textC">50%</li>
                            <li class="textR">100%</li>
                        </ul>
				</div>
			</div>
		</div><!-- //section -->
	
</div>
<div class="fluid">
		<div class="widget grid6">
			<div class="whead">
				<h6 class="timelineName">Lead Sources</h6>
			</div>
			<div class="body">
				<div class="grid12">
					<canvas id="chart-area2"></canvas>
				</div>
			</div>
		</div><!-- //section -->

	<div class="widget grid6">
			<div class="whead">
				<h6 class="timelineName">Leads vs Wins</h6>
			</div>
			<div class="body">
				<div class="grid12">
					<canvas id="chart-area3"></canvas>
				</div>
			</div>
		</div><!-- //section -->
	

	</div><!-- //end fluid -->

<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Active Campaigns</h6>
			<div class="buttonS f_rt jopendialog jopen-addCampaign"><span>Create a New Campaign</span></div>
		</div>
		
<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('CRMCampaigns'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jCampaigns',
	'data'				=> $Campaigns,
	'rowkey'			=> 'Campaign',
	'rowtemplate'		=> 'campaigns.php',
	'total'				=> $iTotalCampaigns,
	'sourceurl'			=> $this->action_url('campaign_ajaxcampaigns'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<?php $this->getDialog('create-campaign-crm.php'); ?>

<script>
//piechart	
var ctx3 = document.getElementById("chart-area2");	
var chart3 = new Chart(ctx3, {
type: 'doughnut',
data: {
				datasets: [{
					data: [<?=$nosource?>, <?=$organic?>, <?=$bcard?>, <?=$tradeshow?>, <?=$coldcall?>, <?=$inboundcall?>, <?=$fblead?>, <?=$fblike?>, <?=$fbcomment?>, <?=$gad?>],
					backgroundColor: [
						'rgba(255, 99, 132)',
						'rgba(54, 162, 235)',
						'rgba(255, 206, 86)',
						'rgba(75, 192, 192)',
						'rgba(153, 102, 255)',
						'rgba(170, 170, 170)',
						'rgba(30, 67, 130)',
						'rgba(113, 236, 255)',
						'rgba(107, 255, 156)',
						'rgba(255, 159, 64)'
					],
					label: 'My dataset' // for legend
				}],
				labels: [
					'No Source',
					'Organic Search',
					'Business Card',
					'Trade Show',
					'Cold Call',
					'Inbound Call',
					'Facebook Ad - Lead Form',
					'Facebook Ad - Like Lead',
					'Facebook Ad - Comment Lead',
					'Google Ad'
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
});
	
var ctx4 = document.getElementById("chart-area3");
ctx4.height = 361;
var timeFormat = 'DD-MM-YYYY';
var chart4 = new Chart(ctx4, {
  type: 'line',
  data:    {
            datasets: [
                {
                    label: "Leads",
                    data: [
						
						<?php foreach($dates as $date){
	
								$leadcount  =   Qube::Start()->
												query('SELECT count(*) FROM prospects WHERE user_id = "'.$User->id.'" AND created LIKE "%'.$date.'%"')
											->fetchColumn(); ?>
						
						{x: "<?=$date?>", y: <?=$leadcount?>}, 
						<?php } ?>
						
					],
                    fill: true,
					backgroundColor: 'rgba(0, 174, 239, 0.2)',
                    borderColor: 'rgba(0, 174, 239, 0.8)'
                },
                {
                    label: "Wins",
                    data:  [
						<?php foreach($dates as $date){
	
								$propcount  =   Qube::Start()->
												query('SELECT count(*) FROM proposals WHERE user_id = "'.$User->id.'" AND accepted_date LIKE "%'.$date.'%"')
											->fetchColumn(); ?>
						
						{x: "<?=$date?>", y: <?=$propcount?>}, 
						<?php } ?>
						 ],
					borderColor: 'rgba(65, 206, 30, 0.8)',
					backgroundColor: 'rgba(65, 206, 30, 0.2)'
                }
            ]
        },
        options: {
            responsive: true,
			maintainAspectRatio: false,
            scales:     {
                xAxes: [{
                    type:       "time",
                    time: {
		                    unit: 'day'
		                },
                    scaleLabel: {
                        display:     true,
                        labelString: 'Date'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display:     true,
                        labelString: 'value'
                    },
					ticks: {
						beginAtZero: true
					}
                }]
            }
        }
});

</script>


<script type="text/javascript">
<?php $this->start_script(); ?>    
    $(function() {

    	$('#sq-listingType').change(function(){
            if( $(this).val() == 'CUSTOM' )
                $('#sq-listingTypeRange').show();
            else
                $('#sq-listingTypeRange').hide();
        });

		//===== Create New Form + PopUp =====//
		$('#jdialog-addCampaign').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Add',
				function(ajax, status)
				{
					if (!ajax.error)
					{
						$('.jnoforms').hide();
						$('.jforms').prepend(ajax.row);
						$('#jCampaigns').dataTable().fnDraw();
						notie.alert({text:ajax.message});
						this.close();
						this.form[0].reset();
					}
					else if (ajax.error == 'validation')
					{
						alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
						alert(ajax.message);
	
				}, '#jdialog-addCampaign')).buttons
		});

	});
<?php $this->end_script(); ?>
</script>