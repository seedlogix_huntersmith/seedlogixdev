<?php
/**
 * @var Template $this
 * @var CampaignModel $Campaign
 */
?>
<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Leads</h6>
			<div class="buttonS f_rt jopen-addLead jopendialog"><span>Add Lead</span></div>
			<div class="buttonS f_rt jopen-importleads jopendialog"><span>Import the Leads</span></div>
			<div class="buttonS f_rt jopen-exportleads jopendialog"><span>Export the Leads</span></div>
			

<?php
$this->getWidget('select-ajax.php',array(
	'id'				=> 'forms-filter',
	'name'				=> 'formID',
	'ajaxUrl'			=> $this->action_url('Campaign_ajaxForms',array('ID' => $Campaign->getID())),
	'field_value'		=> 'id',
	'text'				=> ':name:',
	'placeholder'		=> 'Filter by Campaign&hellip;',
	'style'				=> array('width' => '250px;')
));
?>

		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('ProspectsCampaign'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'jProspects',
	'data'				=> $AllProspects,
	'rowkey'			=> 'Prospect',
	'rowtemplate'		=> 'campaign-prospect.php',
	'total'				=> $total_prospects_match,
	'sourceurl'			=> $this->action_url('Campaign_ajaxCampaignProspects',array('ID' => $Campaign->getID(),'alltime' => '1')),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'filterForms'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Campaign POPUP -->
<div class="SQmodalWindowContainer dialog" title="Import the Leads" id="jdialog-importleads" style="display: none;">
	<form id="importLeads" action="<?= $this->action_url("campaign_importLeads",array('ID' => $Campaign->getID())); ?>" enctype="multipart/form-data" data-include-files="true">
		<fieldset class="step">
			<div class="fluid spacing">
                <div class="grid4"><label>Select a Form:</label></div>
                <div class="grid8">

<?php
$this->getWidget('select-ajax.php',array(
	'id'			=> 'forms',
	'name'			=> 'formID',
	'ajaxUrl'		=> $this->action_url('Campaign_ajaxForms',array('ID' => $Campaign->getID())),
	'field_value'	=> 'id',
	'text'			=> ':name:'
));
?>

                </div>
			</div>
			<div class="fluid"><a id="downloadCVSFormExample" href="<?= $this->action_url('Campaign_DownloadCVSFormExample'); ?>" style="display: none;">Download a CSV Example</a></div>
			<div class="fluid spacing">
                <div class="grid4"><label>File Upload:</label></div>
                <div class="grid8"><input type="file" name="csvLeads"></div>
			</div>
		</fieldset>
	</form>
</div>

<div class="SQmodalWindowContainer dialog" title="Export the Leads" id="jdialog-exportleads" style="display: none;">
	<input type="hidden" id="exportLeadsLinkTemplate" value="<?= $this->action_url('campaign_exportLeads',array('formID' => 'FORMID')); ?>">
	<form id="exportleads" action="" method="post" class="jform-create">
		<fieldset class="step">
			<div class="fluid spacing">
                <div class="grid4"><label>Select a Form:</label></div>
                <div class="grid8">

<?php
$this->getWidget('select-ajax.php',array(
	'id'			=> 'forms-export',
	'name'			=> 'formID',
	'ajaxUrl'		=> $this->action_url('Campaign_ajaxForms',array('ID' => $Campaign->getID())),
	'field_value'	=> 'id',
	'text'			=> ':name:'
));
?>

                </div>
			</div>
		</fieldset>
	</form>
</div>
<div id="loading" class="SQmodalWindowContainer dialog" title="Importing Prospects" style="display: none;"><p>Please wait&hellip;</p></div>
<?php $this->getDialog('add-lead.php'); ?>
<script>
	<?php $this->start_script(); ?>
	$(document).ready(function(){
		Dashboard.CVSTemplateLink  = '<?=$this->action_url('Campaign_downloadCVSFormExample', array('formID' => 'FORMID'))?>';
		$('#jdialog-importleads').dialog({
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function(){
				$('form', this)[0].reset();
				var forms = $('#forms');
				forms.val(null);
				forms.change();
				$.uniform.update();

			},
			buttons: (new CreateDialog("Import", function(ajax, status){
				if(ajax.error){
					if(ajax.error == "validation")
						alert(getValidationMessages(ajax.validation)).join("\n");
					else
						alert(ajax.message);
				}else{
					$('#jProspects').dataTable().fnDraw();
					notie.alert({text:ajax.message});
					this.close();

				}
			}, '#jdialog-importleads')).buttons



		});

		$("#loading").dialog({
			autoOpen: false,
			modal: true
		});




		$('#jdialog-exportleads').dialog({
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function(){
				$('form', this)[0].reset();
				var forms = $('#exportleads');
				forms.val(null);
				forms.change();
				$.uniform.update();

			},
			buttons: {
				Cancel : function(){
					$(this).dialog('close');
				},
				"Export" : function(){
					$('#exportleads').submit();
					$(this).dialog('close');
				}
			}
		});

		$('#forms').change(function(e){
			var formID = $(this).val();
			var downloadLink = $('#downloadCVSFormExample');
			if(formID){
				var href = Dashboard.CVSTemplateLink.replace(/FORMID/, formID);
				downloadLink.attr('href', href);
				downloadLink.fadeIn();
			}else{
				downloadLink.fadeOut();
			}
		});

		$('#forms-filter').on('change', function () {
			$('#jProspects').dataTable().fnDraw();
		});

		$('#jdialog-addLead').dialog(
		{
			autoOpen: false,
			height: 300,
			width: 500,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jProspects');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-addLead')).buttons
		});
		$('#forms-export').on('change', function (e) {
			var form = $('#exportleads');
			var linkTemplate = $('#exportLeadsLinkTemplate').val();
			var formID = $(this).val();
			var link = '';
			if(formID){
				link = linkTemplate.replace('FORMID', formID);
			}

			form.attr('action', link);
		})
	});

	function filterForms(aoData){
		var formId = $('#forms-filter').val();
		if(formId){
			aoData.push({name: 'form_id', value: formId})
		}
	}
	<?php $this->end_script(); ?>
</script>