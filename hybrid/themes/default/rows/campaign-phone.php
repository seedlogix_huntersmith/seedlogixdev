<div class="fluid">
  <div class="widget">
    <div class="whead">
      <h6>Call Reports </h6>
      <?php
            $options = array('Day' => 'Today',
                            'PDay' => 'Previous Day',
                            'PWeek' => 'Previous Week',
                            'P7Days' => 'Previous 7 Days',
                            'PMonth' => 'Previous Month',
                            'PQuarter' => 'Previous Quarter',
                            'MtD' => 'Month to Date',
                            'YtD' => 'Year to Date');
?>

        <select name="" id="recordConfig" class="graph_rangectrl">
          <?php
                foreach($options as $key =>  $text):
?>
          <option value="<?= $key; ?>"<?php $this->selected($key, $value); ?>>
          <?= $text; ?>
          </option>
          <?php
                endforeach;                    
?>
        </select>

      <div class="buttonS bBlack right dynRight jopendialog jopen-mailboxOptions">Mailbox Options</div>
    </div>
    <div class="shownpars">
      <table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jCallReports">
        <thead>
          <tr>
            <td>Date</td>
            <td>Caller ID</td>
            <td>City</td>
            <td>State</td>
            <td>Destination</td>
            <td>Length</td>
            <td>Play</td>
          </tr>
        </thead>
        <tbody class="dataTableBody">
          <?php
                $this->getRow('campaign-callReports.php');
?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /end fluid --> 

<!-- Mailbox Options  -->
<?php $this->getDialog('mailbox-settings.php'); ?>
