<?php
$this_is_a          = 'Call';
$download_action    = $reports['call_uuid'];

foreach(@$callReports as $reports):
    $number         = str_replace('(','',$reports['CallerId']);
    $number         = str_replace(')','',$number);
    $number         = str_replace('-','',$number);
    $number         = str_replace(' ','',$number);

    $date           = date('Y-m-d H:i:s',strtotime($reports['end_time']));
    $caller         = '('.substr($reports['from_number'],1,3).') '.substr($reports['from_number'],4,3).'-'.substr($reports['from_number'],7);
    $calltime       = gmdate('H:i:s',$reports['call_duration']);

    if(in_array($reports['from_number'],$blacklist)):
        $a++;
    else:
?>

    <tr>
        <td><?= $date; ?></td>
        <td><?= $caller; ?></td>
        <td><?= $reports['to_number']; ?></td>
        <td><?= $calltime; ?></td>
        <td>
            <div class="p_container _1a">
                <a title="Download the <?= $this_is_a; ?>" class="bDefault f_rt phoneDownload" href="<?= $download_action; ?>"><span class="export"></span></a>
            </div>
        </td>
    </tr>

<?php endif; endforeach; ?>