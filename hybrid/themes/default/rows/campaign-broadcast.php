<?php
/**
 *
 */
/** @var LeadFormEmailerModel $broadcaster */

$broadcaster_bool   = $broadcaster->active ? '0' : '1';
$broadcaster_label  = $broadcaster->active ? 'Yes' : 'No';
$this_is_a		    = 'Broadcast';
$edit_action        = $this->action_url('Campaign_emailer-settings',array('ID' => $broadcaster->campaign_id,'b' => $broadcaster->getID()));
$delete_action      = $this->action_url('Campaign_delete',array('broadcast_ID' => $broadcaster->getID()));
?>

<tr>
    <td>
        <input type="hidden" value="<?= $broadcaster->getID(); ?>" name="broadcast[id]">
        <input type="hidden" value="<?= $broadcaster->name; ?>" name="broadcast[name]">
        <a href="<?= $edit_action; ?>"><?php $broadcaster->html('name'); ?></a>
    </td>
    <td><?php $broadcaster->html('form_name'); ?></td>
    <td><a title="Click to Toggle" href="<?= $this->action_url('Campaign_save'); ?>"><?= $broadcaster_label; ?></a></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-del" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>