<?php
/**
 * Created by PhpStorm.
 * User: Eric
 * Date: 8/21/2014
 * Time: 10:33 AM
 */

$this_is_a		= 'Blog Post';
$edit_action    = $this->action_url('Blog_post',array('ID' => $blog->getID(),'post' => $post->getID()));
$delete_action  = $this->action_url('Blog_trashpost',array('ID' => $blog->getID(),'post_ID' => $post->getID()));
?>

<tr>
	<td><a href="<?= $edit_action; ?>"><?php $this->p($post->post_title); ?></a></td>
	<td><?php $this->p($post->category); ?></td>
	<td><?php $this->p($post->feed); ?></td>
	<td>
        <div class="p_container _2a">
			<a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
			<a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
	</td>
</tr>