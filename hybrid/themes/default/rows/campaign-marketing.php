<?php
/*
 * Author:     cory frosch <cory.frosch@gmail.com>
 * License:    
 */
/** @var $Responder ResponderModel
 *	@var $this Template
 */

$source_action = '#';
switch($Responder->table_name){
    case 'blogs':
        $source_action  = 'Blog_dashboard?ID='.$Responder->table_id;
        break;
    case 'lead_forms':
        $source_action  = 'Campaign_form?form='.$Responder->table_id.'&ID='.$Responder->cid;
        break;
    case 'hub':
        $touchpoint_arr = CampaignController::getTouchPointTypes($Responder->touchpoint_type);
        $source_action  = sprintf($touchpoint_arr[0],$Responder->table_id);
        break;
}

$responder_bool     = $Responder->active ? '0' : '1';
$responder_label    = $Responder->active ? 'Yes' : 'No';
$this_is_a		    = 'Auto-Responder';
$edit_action        = $this->action_url('Campaign_responder-settings',array('ID' => $Responder->cid,'R' => $Responder->getID()));
$delete_action      = $this->action_url('Campaign_delete',array('autoresponder_ID' => $Responder->getID()));
?>

<tr>
    <td><a href="<?= $edit_action; ?>"><?= $Responder->html('name'); ?></a></td>
    <td><?= $Responder->getResponderType(); ?></td>
    <td><a href="<?= $this->action_url($source_action); ?>"><?= $Responder->responder_context; ?></a></td>
    <td><?= $Responder->sched; ?></td>
    <td><a title="Click to Toggle" href="<?= $this->action_url('Campaign_responderstatus',array('autoresponder_ID' => $Responder->getID(),'active' => $responder_bool,'name' => $Responder->name)); ?>" class="jajax-action"><?= $responder_label; ?></a></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt jeditbtn" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>