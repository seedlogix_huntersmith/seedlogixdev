<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/24/14
 * Time: 11:20 AM
 */
if($limit->type == "ADDON"){
	$edit_link = $this->action_url('DashboardBase_CancelAddon', array('w' => 'Subscriptions',  'subscription_id' => $limit->id, 'parent_id' => $limit->user_ID));
}else{
	$edit_link = $this->action_url('DashboardBase_CancelSubscription', array('w' => 'Subscriptions',  'subscription_id' => $limit->id, 'parent_id' => $limit->user_ID));
}

$this_is_a		= 'Subscription';
$delete_action	= $this->action_url('DashboardBase_CancelSubscription',array('w' => 'Subscriptions','subscription_id' => $limit->id,'parent_id' => $limit->user_ID));
?>

<tr>
	<td><?=$limit->id?></td>
	<td><?= $limit->sub_ID ? ($limit->sub_ID  . ' - ' . $limit->name) : ''?></td>
	<td><?=$limit->type?></td>
	<td><a href="<?= $this->action_url('UserManager_profile?ID=' . $limit->user_ID); ?>"><?= $limit->username ?></a></td>
	<td><?=$limit->user_ID?></td>
	<td><?php
		if(1 == $limit->is_expirable)
		{
			echo 'Persistent. 	';
		}else
		switch(TRUE)
		{
			case  $limit->days_to_expiration == 0:
				echo 'Today.';
				break;
			case  $limit->days_to_expiration > 1;
				printf("%s ( %d days remaining )", $limit->expiration_date, $limit->days_to_expiration);
				break;
			case $limit->days_to_expiration == 1;
				echo 'Tomorrow';
				break;
			default:
				echo $limit->expiration_date;
				break;
		}
		if($limit->is_canceled)
		{
			echo '<br />Canceled: ', $limit->when_canceled, '<br />';
		}
//		$limit->expiration_date . sprintf($limit->days_to_expiration < 1 ? ' (expired %d days ago)' : ' expires in %d days', $limit->days_to_expiration);
		?></td>
	<td><?= $limit->ts; ?></td>
	<td>

<?php if($limit->type != 'TOTAL' && $limit->is_cancelable): ?>

        <div class="p_container _1a">
			<a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
		</div>

<?php endif; ?>

	</td>
</tr>