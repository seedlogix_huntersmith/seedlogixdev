<?php
$t_ctrl         = ucfirst(mb_strtolower($site->touchpoint_type));
$this_is_a      = $t_ctrl == 'Landing' ? 'Landing Page' : $t_ctrl;
$edit_action    = $this->action_url($t_ctrl.'_settings?ID='.$site->id);
$delete_action  = '#';
?>

<tr>
	<td><?php $this->p($site->touchpoint_type); ?></td>
	<td><a href="<?= $edit_action; ?>"><?php $this->p($site->name); ?></a></td>
	<td><?php $this->p($site->created); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>