<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Account AccountsModel */

$account_url	= $this->action_url('Campaign_product?ID='.$Product->cid,array('prodid' => $Product->getID()));

$this_is_a			= 'Product';
$edit_action    	= $account_url;
$delete_action  	= $this->action_url('Campaign_delete',array('Product_ID' => $Product->getID()));
?>

<tr>
	<td><a href="#" class="jopendialog jopen-saveproduct" data-id="<?=$Product->getID()?>" data-name="<?php $Product->html('name'); ?>" data-qty="<?php $Product->html('qty'); ?>" data-type="<?php $Product->html('type'); ?>" data-price="<?php $Product->html('price'); ?>" data-discount="<?php $Product->html('discount'); ?>" data-sdesc="<?php $Product->html('s_desc'); ?>"><?php $Product->html('name'); ?></a></td>
	<td><?php $Product->html('qty'); ?></td>
	<td><?php $Product->html('type'); ?></td>
	<td>$<?php $Product->html('price'); ?></td>
	<td><?php $Product->html('discount'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a href="#" title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt jopendialog jopen-saveproduct"  data-id="<?=$Product->getID()?>" data-name="<?php $Product->html('name'); ?>" data-qty="<?php $Product->html('qty'); ?>" data-type="<?php $Product->html('type'); ?>" data-price="<?php $Product->html('price'); ?>" data-discount="<?php $Product->html('discount'); ?>" data-sdesc="<?php $Product->html('s_desc'); ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>