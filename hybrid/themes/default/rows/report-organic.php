<?php
/**
 * @var $stats stdClass
 */
?>

<tr class="j-<?= $stats->searchengine; ?>">
    <td class="j-searchengine"><?= $stats->searchengine; ?></td>
    <td class="j-visits"><?= $stats->visits; ?></td>
    <td class="j-leads"><?= $stats->leads; ?></td>
    <td class="j-rate"><?= $stats->rate; ?>%</td>
    <td class="j-rate"><?= $stats->bounce_rate; ?>%</td>
</tr>