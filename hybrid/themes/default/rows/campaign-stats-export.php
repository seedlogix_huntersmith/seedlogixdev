<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

	if(!isset($handle))	throw new QubeException('Provide a valid handle.');
	$data	=	array($camp->name,
					round($camp->visitschangepct, 2) . '%',
				$camp->visitsnow*1,
				$camp->num_leads*1,
				round($camp->conversionspct, 2) . '%',
				$camp->num_touchpoints*1,
				$camp->created_str);
	
		fputcsv($handle, $data);