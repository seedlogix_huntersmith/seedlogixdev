<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

if(!function_exists('itemCounter')){
    function itemCounter(){
        static $i = 0;
        return ++$i;
    }
}

$disableActions = $parent_hub_lock && $parent_hub_lock->isLocked('allow_pages') || !$can_edit_site ? ' disabled' : '';
$subnodes       = $Node->getChildren();

$this_is_a      = ucfirst($Node->type);
$down_action    = $this->action_url("{$controller}_moveup?node=$NextNode->type&nid=$NextNode->id");
$up_action      = $this->action_url("{$controller}_moveup?node=$Node->type&nid=$Node->id");
$edit_action    = $this->action_url("{$controller}_pagedetails",$Node->type == 'page' ? array('PID' => $Node->id) : array('NID' => $Node->id));
$delete_action  = $this->action_url("{$controller}_trashpage",$Node->type == 'page' ? array('PID' => $Node->id) : array('NID' => $Node->id));
?>

    <tr class="gradeX" id="<?= $Node->id; ?>">
        <td><?= str_repeat('&nbsp;&nbsp;',$level + ($level % 3)).($level > 0 ? '<span>&rdca;</span> ' : ''); ?><a href="<?= $edit_action; ?>"><?php $this->p($Node->page_title); ?></a></td>
        <td><?= $Node->type; ?></td>
        <td><?= $Node->nav_parent_id == 0 ? 'Root' : $ParentName; ?></td>
        <td><?= $Node->created; ?></td>

<?php if(empty($disable)): ?>

        <td>
            <div class="p_container _4a">
                <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt<?= $disableActions; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
                <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>

<?php if($siblings > 0): ?>

<?php if($k > 0): ?>
                <a title="Move the <?= $this_is_a; ?> Up" class="bDefault f_rt<?= $disableActions; ?>" href="<?= $up_action; ?>"><span class="up"></span></a>
<?php endif; ?>

<?php if($k < $siblings && $NextNode): ?>
                <a title="Move the <?= $this_is_a; ?> Down" class="bDefault f_rt<?= $disableActions; ?>" href="<?= $down_action; ?>"><span class="down"></span></a>
<?php endif; ?>

<?php endif; ?>

            </div>
        </td>

<?php endif; ?>

    </tr>
<?php
foreach($subnodes as $k => $NewNode):
    $this->getRow('nav-row.php',array(
        'Node'          => $NewNode,
        'NextNode'      => isset($subnodes[$k + 1]) ? $subnodes[$k + 1] : NULL,
        'controller'    => $controller,
        'level'         => $level + 1,
        'k'             => $k,
        'siblings'      => count($subnodes) - 1,
        'ParentID'      => $Node->id,
        'ParentName'    => $Node->page_title,
        'disable'       => $disable
    ));
endforeach;
?>