<li class="<?= $theme_i->type; ?>-themes">
    <a href="#<?= $theme_i->id; ?>" class="jChangeTheme <?= $theme_i->type; ?>" title="Use Theme" action="save" themeID="<?= $theme_i->id; ?>" Parent="<?= $theme_i->hub_parent_id; ?>" owner="<?= $theme_i->owner; ?>">
        <img src="<?= $theme_i->getThumbnailURL(); ?>" alt="" width="115">
        <span class="themeTitle<?= $sitetheme->name == $theme_i->name ? ' actv' : ''; ?>"> <?= $theme_i->name; ?> </span>
    </a>
</li>