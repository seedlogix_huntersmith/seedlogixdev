<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */
//total leads
$iTotalLeads   =   AccountsDataAccess::getTotalLeadsByCID($Campaign->id);
$iTotalAccounts   =   AccountsDataAccess::getTotalAccountsByCID($Campaign->id);
$iTotalContacts   =   AccountsDataAccess::getTotalContactsByCID($Campaign->id);


$CPRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.cid = %d', $Campaign->id)->calc_found_rows();
$CPRQ->Where('T.accepted_date LIKE "%'.date('Y-m').'%" AND T.status = "Accepted" AND T.trashed = 0');
$ClosedProposals  =   $CPRQ->Exec()->fetchAll();

$APRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.cid = %d', $Campaign->id)->calc_found_rows();
$APRQ->Where('T.expire_date LIKE "%'.date('Y-m').'%" AND T.status = "Active" AND T.trashed = 0');
$ActiveProposals  =   $APRQ->Exec()->fetchAll();

$totalActive = 0;
$totalExpired = 0;
$totalWon = 0;
$totalPropActive = 0;
$totalPropExpired = 0;
$totalPropWon = 0;
foreach($ClosedProposals as $ClosedProposal){
	
	$totalPropWon += 1;
	$totalWon  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ClosedProposal->id.'"')
                ->fetchColumn();
}
foreach($ActiveProposals as $ActiveProposal){

	if($ActiveProposal->expire_date > date("Y-m-d")){
		
		$totalPropActive += 1;
		$totalActive  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ActiveProposal->id.'"')
                ->fetchColumn();
	}
	if($ActiveProposal->expire_date < date("Y-m-d")){
		
		$totalPropExpired += 1;
		$totalExpired  +=   Qube::Start()->
                    query('SELECT SUM(price) FROM products WHERE proposal_id = "'.$ActiveProposal->id.'"')
                ->fetchColumn();
	}
}

$this_is_a		= 'Campaign';
$edit_action    = $this->action_url('Campaign_prospects',array('ID' => $Campaign->id));
$delete_action  = $this->action_url('Campaign_delete?campaign_ID='.$Campaign->id);
?>

<tr>
	<td><a href="<?=$edit_action?>"><?php $Campaign->html('name'); ?></a></td>
	<td><?=$iTotalLeads?></td>
	<td><?=$iTotalAccounts?></td>
	<td><?=$iTotalContacts?></td>
	<td style="color:#41CE1E;">$<?=$totalWon?></td>
	<td style="color:#00aeef;">$<?=$totalActive?></td>
	<td style="color:#FF2626;">$<?=$totalExpired?></td>
	<td><?php $Campaign->html('date_created'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>