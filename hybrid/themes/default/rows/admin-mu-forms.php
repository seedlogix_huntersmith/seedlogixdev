<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 12/30/14
 * Time: 10:19 AM
 */

$this_is_a		= 'Form';
$edit_action    = $this->action_url('Admin_form?form='.$leadform->id);
$delete_action  = $this->action_url('Campaign_delete?leadform_ID='.$leadform->getID());
?>
<tr class="gradeA jLeadformrow" id="jleadform_<?= $leadform->id; ?>">
	<td class="name">
		<a href="<?= $edit_action; ?>"><?php $this->p($leadform->name); ?></a>
	</td>
	<td>
		<a href="<?= $this->action_url('Campaign_prospects?ID=' . $leadform->cid . '&form=' . $leadform->id); ?>"><?= $this->p($leadform->prospects_count); ?></a>
	</td>
	<td>
		<a href="<?= $this->action_url('Campaign_emailmarketing?ID=' . $leadform->cid . '&form=' . $leadform->id); ?>"><?= $this->p($leadform->responders_count); ?></a>
	</td>
	<td>
		<?php echo $leadform->created; ?>
	</td>
	<td>
        <div class="p_container _2a">
			<a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow jdel-jleadform_<?= $leadform->id; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
			<a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
	</td>
</tr>
