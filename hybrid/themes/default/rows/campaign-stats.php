<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$this_is_a		= 'Campaign';
$edit_action    = $this->action_url('Campaign_dashboard',array('ID' => $camp->id));
$delete_action  = $this->action_url('Campaign_delete?campaign_ID='.$camp->id);
?>

<tr class="gradeA jCampaignrow" id="jcampaign_<?= $camp->id; ?>">
    <td class="name">
        <a href="<?= $edit_action; ?>" class="campaignName"><?= $camp->name; ?></a>
        <input type="text" name="campaign[name]" class="hide">
    </td>
    <td class="num_touchpoints"><?= $camp->num_touchpoints*1; ?></td>
    <td class="visitschangepct"><span class="<?= $camp->visitschangepct >= 100 || $camp->visitschangepct == 0 ? 'statsPlus' : 'statsMinus'; ?>"> <?= round($camp->visitschangepct,2).'%'; ?></span></td>
    <td class="visitsnow"><?= $camp->visitsnow*1; ?></td>
    <td class="num_leads"><?= $camp->num_leads*1; ?></td>
    <td class="conversionspct"><?= round($camp->conversionspct,2).'%'; ?></td>
    <td><?= $camp->created_str; ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow jdel-jcampaign_<?= $camp->id; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt<?php /* jeditbtn */ ?>" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>