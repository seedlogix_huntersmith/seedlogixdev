<tr>
    <td><?php $event->html('ts') ?></td>
    <td><?php echo $event->getID() ?></td>
    <td><?php echo $event->username ? $event->html('username') : "No User"; ?></td>
    <td><?php echo $event->html('object'); ?></td>
    <td><?php echo $event->html('event'); ?></td>
    <td><?php echo $event->html('value'); ?></td>
    <td><?php echo $event->data ? $event->html('data') : "No Data"; ?></td>
    <td><?php echo $event->context ? $event->html('context') : "Not Available"; ?></td>
</tr>