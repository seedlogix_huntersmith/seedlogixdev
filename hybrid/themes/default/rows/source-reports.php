<?php
$tableStats     = $piwikStats->tableStats;
$action_params  = array('report_ID' => $T_Report->ID);
?>

<tr>
    <td><a href="<?= $this->action_url('Campaign_report-organic',$action_params); ?>">Organic</a></td>
    <td class="j-SearchVisits"><?= $tableStats->SearchVisits; ?></td>
    <td class="j-SearchLeads"><?= $tableStats->SearchLeads; ?></td>
    <td class="j-searchRate"><?= $tableStats->searchRate; ?></td>
    <td class="j-searchBounceRate"><?= $tableStats->searchBounceRate; ?></td>
</tr>
<tr>
    <td><a href="<?= $this->action_url('Campaign_report-referrals',$action_params); ?>">Referrals</a></td>
    <td class="j-ReferralVisits"><?= $tableStats->ReferralVisits; ?></td>
    <td class="j-ReferralLeads"><?= $tableStats->ReferralLeads; ?></td>
    <td class="j-referralRate"><?= $tableStats->referralRate; ?></td>
    <td class="j-referralBounceRate"><?= $tableStats->referralBounceRate; ?></td>
</tr>
<tr>
    <td><a href="<?= $this->action_url('Campaign_report-social',$action_params); ?>">Social</a></td>
    <td class="j-SocialVisits"><?= $tableStats->SocialVisits; ?></td>
    <td class="j-SocialLeads"><?= $tableStats->SocialLeads; ?></td>
    <td class="j-socialRate"><?= $tableStats->socialRate; ?></td>
    <td class="j-socialBounceRate"><?= $tableStats->socialBounceRate; ?></td>
</tr>
<tr>
    <td>Direct</td>
    <td class="j-directVisits"><?= $tableStats->directVisits; ?></td>
    <td class="j-directLeads"><?= $tableStats->directLeads; ?></td>
    <td class="j-directRate"><?= $tableStats->directRate; ?></td>
    <td class="j-directBounceRate"><?= $tableStats->directBounceRate; ?></td>
</tr>