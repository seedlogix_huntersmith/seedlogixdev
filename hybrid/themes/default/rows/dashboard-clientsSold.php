<!-- 
    Sales Manager
-->
<tr>
    <td class="center">Thomas Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional</td>
    <td class="center">3/5/2015</td>
    <td class="center">
        <a href="#3/5/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arthur Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional +</td>
    <td class="center">3/4/2015</td>
    <td class="center">
        <a href="#3/4/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Aunt Polly</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Manager</td>
    <td class="center">3/3/2015</td>
    <td class="center">
        <a href="#3/3/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<!-- 
    Sales professionals &
    Sales professionals + 
-->
<tr>
    <td class="center">John Snow</td>
    <td class="center">Acme Inc.</td>
    <td class="center">JumpTrack</td>
    <td class="center">3/5/2015</td>
    <td class="center">
        <a href="#3/5/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Tyrion Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">FastTrack</td>
    <td class="center">3/4/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Cersei Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">AdvTrack</td>
    <td class="center">3/3/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Daenerys Targaryen</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack</td>
    <td class="center">3/2/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arya Stark</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack +</td>
    <td class="center">3/1/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>

<!-- 
    Sales Manager
-->
<tr>
    <td class="center">Thomas Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional</td>
    <td class="center">2/5/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arthur Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional +</td>
    <td class="center">2/4/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Aunt Polly</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Manager</td>
    <td class="center">2/3/2015</td>
    <td class="center">
        <a href="#>2/3/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<!-- 
    Sales professionals &
    Sales professionals + 
-->
<tr>
    <td class="center">John Snow</td>
    <td class="center">Acme Inc.</td>
    <td class="center">JumpTrack</td>
    <td class="center">2/5/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Tyrion Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">FastTrack</td>
    <td class="center">2/4/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Cersei Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">AdvTrack</td>
    <td class="center">2/3/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Daenerys Targaryen</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack</td>
    <td class="center">2/2/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arya Stark</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack +</td>
    <td class="center">2/1/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>

<!-- 
    Sales Manager
-->
<tr>
    <td class="center">Thomas Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional</td>
    <td class="center">1/5/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arthur Shelby</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Professional +</td>
    <td class="center">1/4/2015</td>
    <td class="center">
        <a href="#1/4/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Aunt Polly</td>
    <td class="center">Shelby limited</td>
    <td class="center">Sales Manager</td>
    <td class="center">1/3/2015</td>
    <td class="center">
        <a href="#1/3/2015" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<!-- 
    Sales professionals &
    Sales professionals + 
-->
<tr>
    <td class="center">John Snow</td>
    <td class="center">Acme Inc.</td>
    <td class="center">JumpTrack</td>
    <td class="center">1/5/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Tyrion Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">FastTrack</td>
    <td class="center">1/4/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Cersei Lannister</td>
    <td class="center">Acme Inc.</td>
    <td class="center">AdvTrack</td>
    <td class="center">1/3/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Daenerys Targaryen</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack</td>
    <td class="center">1/2/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>
<tr>
    <td class="center">Arya Stark</td>
    <td class="center">Acme Inc.</td>
    <td class="center">ProTrack +</td>
    <td class="center">1/1/2015</td>
    <td class="center">
        <a href="#" class="tablectrl_small bDefault tipS" title="Edit">
            <span class="iconb" data-icon="&#xe1f7;"></span>
        </a>
    </td>
</tr>