<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$this_is_a      = substr($selected_pointtype_label,0,-1);
$edit_action    = $this->action_url(sprintf($touchpointtypes[$site->touchpoint_type][0],$site->id));
$delete_action  = $this->action_url('Campaign_delete?'.$site->touchpoint_type.'_ID='.$site->id.'&callback=j_ben');
?>

<tr class="gradeA jSiteRow" id="jsite_<?= $site->id; ?>">
    <td><a href="<?= $edit_action; ?>"><?php $this->p($site->name); ?></a></td>
    <td><span class="<?= $site->visits_changes >= 100 || $site->visits_changes == 0 ? 'statsPlus' : 'statsMinus'; ?>"> <?= $site->visits_changes.'%'; ?></span></td>
    <td><?= $site->total_visits; ?></td>
    <td><?= $site->total_leads; ?></td>
    <td><?= $site->rate; ?></td>
    <td><?= $site->touchpoint_type; ?></td>
    <td><?= date('M d, Y',strtotime($site->created)); ?></td>
    <td>
      <div class="p_container _2a">
        <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow jdel-jsite_<?= $site->id; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
        <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt jeditbtn" href="<?= $edit_action; ?>"><span class="edit"></span></a>
      </div>
    </td>
</tr>