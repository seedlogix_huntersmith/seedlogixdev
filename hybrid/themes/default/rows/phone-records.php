<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $PhoneRecord PhoneRecordsModel */

//$date = date('Y-m-d H:i:s', strtotime($PhoneRecord->html('date')));
//$caller = '('.substr($PhoneRecord->html('caller_id'), 1, 3).') '.substr($PhoneRecord->html('caller_id'), 4, 3).'-'.substr($PhoneRecord->html('caller_id'),7);
//$calltime = gmdate("H:i:s", $PhoneRecord->html('length'));
$num = preg_replace('/[^0-9]/', '', $PhoneRecord->caller_id);

$len = strlen($num);
if ($len==11 && substr($num,0,1)=='1'){
	$PhoneRecord->caller_id = substr($num,1,10);
}
else $PhoneRecord->caller_id = $num;


$Prospect = Qube::Start()->
                    query('SELECT id, cid, name FROM prospects WHERE phone = "'.$PhoneRecord->caller_id.'" AND user_id = "'.$PhoneRecord->user_id.'" AND phone != "" AND trashed = 0')
                ->fetch();
							
if($Prospect){
	$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect['cid'].'&pid='.$Prospect['id']);
	$name = '<a href="'.$edit_action.'">'.$Prospect['name'].'</a>';
} else $name = $InboxCall->caller_id;

?>
<tr>
	
	<td class="center"><?=$PhoneRecord->html('date')?></td>
        <td class="center"><?=$PhoneRecord->html('caller_id')?></td>
        <td class="center"><?=$PhoneRecord->html('destination')?></td>
        <td class="center"><?=$PhoneRecord->html('length')?></td>
		<td>
			<?php if($PhoneRecord->recording){?>
            <div class="p_container _1a">
                <a title="Download Recording" class="bDefault f_rt phoneDownload" href="<?=$PhoneRecord->html('recording')?>"><span class="export"></span></a>
            </div>
			<?php } ?>
        </td>
	
	
</tr>

