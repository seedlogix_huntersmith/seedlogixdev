<?php
/**
 * @var $this Template
 * @var $collab CampaignCollab
 * @var $Campaign CampaignModel
 */

$this_is_a		= 'Response';
$edit_action    = $this->action_url('Campaign_edit-notification',array('ID' => $collab->campaign_id,'Collab' => $collab->getID()));
$delete_action  = $this->action_url('Campaign_delete',array('collab_ID' => $collab->getID()));
?>

<tr>
	<td><?= $collab->name; ?></td>
	<td><?= $collab->email; ?></td>
	<td><?= ucfirst($collab->scope); ?></td>
	<td><?= $collab->active ? 'Yes' : 'No'; ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt jeditbtn" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>