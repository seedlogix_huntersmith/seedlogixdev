<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 12/29/14
 * Time: 4:09 PM
 */

$this_is_a		= 'Role';
$delete_action  = $Role->role_users == 0 ? ' href="'.$this->action_url('Admin_ajaxDeleteRole',array('ID' => $Role->ID)).'"' : '';

$action_title   = $Role->role_users == 0 ? 'Delete' : 'Update';
$action_icon    = $Role->role_users == 0 ? '<span class="delete"></span>' : '<span class="update"></span>';
?>

<tr class="gradeX" id="role_<?= $Role->ID; ?>">
    <td><span class="role_ID"><?php $this->p($Role->ID); ?></span></td>
    <td><span class="role_role"><?php $this->p($Role->role); ?></span></td>
    <td><span class="users"><?php $this->p($Role->role_users); ?></span></td>
    <td><?php $this->p($Role->company); ?></td>
    <td class="role_permissions_str"><?php $this->getWidget('role-permissions.php',array('Role' => $Role)); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="<?= $action_title; ?> the <?= $this_is_a; ?>" class="bDefault f_rt <?= $Role->role_users > 0 ? 'delete-dialog' : 'jajax-action'; ?>"<?= $delete_action; ?>><?= $action_icon; ?></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt edit-role" id="roledit_<?= $Role->ID; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>