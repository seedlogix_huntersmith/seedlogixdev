<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$item_ID  = $item->tbl_ID;
$edit_url	=	$this->action_url('Campaign_network-listingInfo?ID=' . $item->cid, array('article' => $item_ID));
?>

<tr class="gradeA" id="jdirarticle_<?= $item_ID; ?>">
		<td><a href="<?php echo $edit_url;	?>"><?php $this->p($item->name); ?></a></td>
		<td>Directory</td>
		<td><?php $this->date($item->created); ?></td>
		<td class="tableActs">
				<a href="<?php echo $edit_url; ?>" class="tablectrl_small bDefault tipS" title="Edit">
						<span class="iconb" data-icon="&#xe1db;"></span>
				</a>
				<a href="<?php
          echo $this->action_url('Campaign_delete?dirarticle_ID=' . $item_ID);

  ?>" class="tablectrl_small bDefault tipS jaction-delrow jajax-action jdel-jdirarticle_<?= $item_ID; ?>" title="Remove">
						<span class="iconb" data-icon="&#xe136;"></span>
				</a>
		</td>
</tr>
