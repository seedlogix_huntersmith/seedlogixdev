<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Account AccountsModel */

$phone_url	= $this->action_url('Campaign_phoneline?ID='.$Phone->cid,array('lid' => $Phone->getID()));

$this_is_a			= 'Phone Line';
$edit_action    	= $phone_url;
//$delete_action  	= $this->action_url('Campaign_delete',array('Account_ID' => $Account->getID()));
?>

<tr>
	<td><a href="<?= $phone_url; ?>"><?php $Phone->html('phone_num'); ?></a></td>
	<td><?php $Phone->html('type'); ?></td>
	<td><?php $Phone->html('cell'); ?></td>
	<td><?php $Phone->html('date_created'); ?></td>
	<td>
        <div class="p_container _2a">
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>