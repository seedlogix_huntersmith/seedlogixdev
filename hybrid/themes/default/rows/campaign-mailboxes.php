
<?php

require_once(QUBEADMIN . 'inc/phone.class.php');
$phone = new Phone();

//get customerID
$customer_id = $phone->getCustomerID($User->getID(), $Campaign->getID());
		
$custID = $customer_id['customer_id'];
$defaultRequest = 'Previous7Days';
		
//get mailboxes
$mailboxView = $custID ? $phone->getMailboxes($User->getID(), $custID) : array();

$mailboxNumbers = $phone->getMBXNumbers($User->getID());//$mailboxID);

?>


<? if($mailboxView && is_array($mailboxView)){
	foreach($mailboxView as $mailbox){
		echo '
		<tr>
    <td><a href="/admin.php?ID='.$Campaign->getID().'&action=phone-reports&ctrl=Campaign&mid='.$mailbox['MailboxId'].'&rqtype='.$defaultRequest.'">Main Mailbox Number: '.$mailbox['PhoneNumberList'][0]['Description'].'</a></td>
    <td class="tableActs">
        <a href="/admin.php?ID='.$Campaign->getID().'&action=phone-reports&ctrl=Campaign&mid='.$mailbox['MailboxId'].'&rqtype='.$defaultRequest.'" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>';
	}
} else { ?>

	<? if(!$customer_id){ ?>
    <? } else { ?>
    <p>Service line connection lost.  Please try again in 10 seconds. <a href="phone_temp.php">Click here to refresh</a>.</p>
    <? } ?>

<? } ?>

