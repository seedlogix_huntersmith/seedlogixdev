<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$form_url           = $this->action_url('Campaign_form?ID='.$Prospect->cid,array('form' => $Prospect->form_id));

$this_is_a		    = 'Prospect';
$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect->cid.'&pid='.$Prospect->getID());
$delete_action      = $this->action_url('Campaign_delete',array('prospect_ID' => $Prospect->getID()));
?>

<tr>
    <td><span class="<?= $Prospect->getColor(); ?>"><?= $Prospect->getGrade(); ?></span></td>
    <td><a href="<?= $edit_action; ?>" title=""><?php $Prospect->html('name'); ?></a></td>
    <td><?= mb_strimwidth($Prospect->email,0,57,'&hellip;'); ?></td>
    <td><?php $Prospect->html('phone'); ?></td>
	<td><?php $Prospect->html('source'); ?></td>
    <td><a href="<?= $form_url; ?>" title=""><?php $Prospect->html('form_name'); ?></a></td>
    <td><?php $Prospect->html('created'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>