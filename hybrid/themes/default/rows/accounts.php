<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Account AccountsModel */

$account_url	= $this->action_url('Campaign_account?ID='.$Account->cid,array('aid' => $Account->getID()));

$this_is_a			= 'Account';
$edit_action    	= $account_url;
$delete_action  	= $this->action_url('Campaign_delete',array('Account_ID' => $Account->getID()));

$iTotalProposals = ProposalsDataAccess::getTotalProposal($Account->getID());
$iTotalContacts = AccountsDataAccess::getTotalContacts($Account->getID());
$totalValue = ProposalsDataAccess::getProposalsValue($Account->getID());
?>

<tr>
	<td><a href="<?= $account_url; ?>"><?php $Account->html('name'); ?></a></td>
	<td><?=$iTotalProposals?></td>
	<td><?=$iTotalContacts?></td>
	<td>$<?=$totalValue?></td>
	<td><?php $Account->html('last_updated'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>