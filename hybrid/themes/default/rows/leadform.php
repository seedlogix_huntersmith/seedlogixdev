<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$this_is_a	= 'Form';
$edit_action    = $this->action_url('Campaign_form?ID='.$leadform->cid.'&form='.$leadform->id);
$delete_action  = $this->action_url('Campaign_delete?leadform_ID='.$leadform->getID());
?>

<tr class="gradeA jLeadformrow" id="jleadform_<?= $leadform->id; ?>">
    <td class="name"><a href="<?= $this->action_url('Campaign_form?ID='.$leadform->cid.'&form='.$leadform->id); ?>"><?= $this->p($leadform->name); ?></a></td>
    <td><a href="<?= $this->action_url('Campaign_prospects?ID='.$leadform->cid.'&form='.$leadform->id); ?>"><?= $this->p($leadform->prospects_count); ?></a></td>
    <td><a href="<?= $this->action_url('Campaign_emailmarketing?ID='.$leadform->cid.'&form='.$leadform->id); ?>"><?= $this->p($leadform->responders_count); ?></a></td>
    <td><?= $leadform->created; ?></td>
    <td>
        <div class="p_container _2a">
                <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow jdel-jleadform_<?= $leadform->id; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
                <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>