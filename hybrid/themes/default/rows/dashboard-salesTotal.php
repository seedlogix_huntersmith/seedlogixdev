<?php
$percentage     = intval($rowData['commission']);
$commission     = intval($rowData['amount']) * ($percentage / 100);
$companyName    = $rowData['company'] ? $rowData['company'] : $rowData['company_fallback'];

if($rowData['timestamp'] != ''):
?>

<tr>
    <td><?= $rowData['sales_professional']; ?></td>
    <td><?= $companyName; ?></td>
    <td><?= $rowData['name']; ?></td>
    <td><?= $rowData['amount']; ?></td>
    <td><?= $percentage; ?>%</td>
    <td>$<?= $commission; ?></td>
    <td><?= $rowData['timestamp']; ?></td>
</tr>

<?php endif; ?>