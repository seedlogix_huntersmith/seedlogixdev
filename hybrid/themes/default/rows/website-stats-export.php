<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
	if(!isset($handle))	throw new QubeException('Provide a valid handle.');
	$data	=	array($site->name,
					$site->visits_changes . '%',
					$site->total_visits,
					$site->total_leads,
					$site->rate,
					$site->touchpoint_type,
					date("F jS, Y", strtotime($site->created))
				);
		fputcsv($handle, $data);