<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Proposal ProposalModel */

$proposal_url	= $this->action_url('Campaign_proposal?ID='.$Proposal->cid,array('proid' => $Proposal->getID()));

$this_is_a			= 'Proposal';
$edit_action    	= $proposal_url;
$delete_action  	= $this->action_url('Campaign_delete',array('Proposal_ID' => $Proposal->getID()));
$contact_prop = ContactModel::Fetch('id = %d', $Proposal->contact_id);

$proid =$Proposal->getID();
$prop = ProposalModel::Fetch('id = %d', $proid);

$pa = new ProposalDataAccess();
$plists = $pa->getProductsbyProposal($prop, array('proid' => $Proposal->getID()));

$priceonetime = array();
$pricemonthly = array();
$pricebiannual = array();
$priceannual = array();
foreach($plists as $plist){
if($plist->type == 'One-Time') $priceonetime[] = $plist->price;
if($plist->type == 'Monthly') $pricemonthly[] = $plist->price;
if($plist->type == 'Bi-Annual') $pricebiannual[] = $plist->price;
if($plist->type == 'Annual') $priceannual[] = $plist->price;
}
$ponetime = array_sum($priceonetime);
$pmonthly = 12*array_sum($pricemonthly);
$pricebiannual = 2*array_sum($pricebiannual);
$pannual = array_sum($priceannual);


$totaldollar = number_format($ponetime+$pmonthly+$pricebiannual+$pannual,2);

$today = date('Y-m-d');
$datetime1 = new DateTime($today);
$datetime2 = new DateTime($Proposal->expire_date);
$interval = $datetime1->diff($datetime2);
$diff = $interval->format('%a');
if($diff == '0' || $datetime2 < $datetime1){
	$class = 'expired';
} 
else if($diff == '1' || $diff <= '3'){
	$class = 'expiring';
} else $class = 'normal';
?>

<tr>
	<td><?php if($Proposal->status == 'Accepted'){?><span class="due-date won">WON</span><?php } else { ?><span class="due-date <?=$class?>"><?php $Proposal->html('expire_date'); ?></span><?php } ?></td>
	<td><?php $Proposal->html('name'); ?></td>
	<td><?=$contact_prop->first_name?> <?=$contact_prop->last_name?></td>
	<td>$<?=$totaldollar?></td>
	<td><a href="<?= $proposal_url; ?>"><?php $Proposal->html('status'); ?></a></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>