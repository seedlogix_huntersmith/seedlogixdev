<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$this_is_a		    = 'Prospect';
$edit_action        = $this->action_url('Campaign_prospect?ID='.$prospect->cid.'&pid='.$prospect->getID());
$delete_action      = $this->action_url('Campaign_delete',array('prospect_ID' => $prospect->getID()));
?>

<tr>
    <td><span class="<?= $prospect->getColor(); ?>"><?= $prospect->getGrade(); ?></span></td>
    <td><a href="<?= $edit_action; ?>" title=""><?php trim($prospect->name) != '' ? $prospect->html('name') : print 'Unknown'; ?></a></td>
    <td><?php $prospect->html('email'); ?></td>
    <td><?php $prospect->html('phone'); ?></td>
    <td><?php $prospect->html('TP_TYPE'); ?></td>
    <td><?php $prospect->html('TP_NAME'); ?></td>
    <td><?php $prospect->html('TP_PAGE'); ?></td>
    <td><?php $prospect->html('created'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>