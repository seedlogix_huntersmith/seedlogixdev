<?php
$this_is_a		= 'Client';
$manage_action  = $this->action_url('Admin_doImpersonate?ID='.$rowData['user_id']);
?>

<tr>
    <td><a href="<?= $manage_action; ?>"><?= $rowData['company']; ?></a></td>
    <td><?= $rowData['name']; ?></td>
    <td><?= $rowData['sales_professional']; ?></td>
    <td><?= $rowData['created']; ?></td>
    <td>
        <div class="p_container _1a">
            <a title="Manage the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $manage_action; ?>"><span class="manage"></span></a>
        </div>
    </td>
</tr>