<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Account AccountsModel */

//$account_url	= $this->action_url('Campaign_product?ID='.$Product->cid,array('prodid' => $Product->getID()));

$this_is_a			= 'Task';
//$edit_action    	= $account_url;
$delete_action  	= $this->action_url('Campaign_delete',array('Task_ID' => $Task->getID()));
$completed_action  	= $this->action_url('Campaign_complete-task',array('Task_ID' => $Task->getID()));
$today = date('Y-m-d');
$datetime1 = new DateTime($today);
$datetime2 = new DateTime($Task->due_date);
$interval = $datetime1->diff($datetime2);
$diff = $interval->format('%a');
//var_dump($diff);
if($diff == '0' || $datetime2 < $datetime1){
	$class = 'expired';
} 
else if($diff == '1' || $diff <= '3'){
	$class = 'expiring';
} else $class = 'normal';

if($Task->contact_id){
	$Contact = ContactModel::Fetch('id = %d', $Task->contact_id);
	$assigned = $Contact->first_name.' '.$Contact->last_name;
} else {
	$Account = AccountsModel::Fetch('id = %d', $Task->account_id);
	$assigned = $Account->name;
}
$Account = AccountsModel::Fetch('id = %d', $Task->account_id);
$Contact = ContactModel::Fetch('id = %d', $Task->contact_id);

?>

<tr>
	<td><a href="#" class="jopendialog jopen-savetask" data-id="<?=$Task->getID()?>" data-time="<?=$Task->time?>" data-due="<?=$Task->due_date?>" data-title="<?=$Task->title?>"><span class="due-date <?=$class?>"><i class="icon-calendar-2"></i><?php $Task->html('due_date'); ?></span></a></td>
	<td><?=$assigned?></td>
	<td><?php $Task->html('title'); ?></td>
    <td>
        <div class="p_container _2a">
			
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
			 <a title="Mark <?= $this_is_a; ?> Completed" class="bDefault f_rt jajax-action" href="<?= $completed_action; ?>"><span class="approve"></span></a>
            
		</div>
    </td>
</tr>