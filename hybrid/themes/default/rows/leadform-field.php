<?php
/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 * @var $this Template
 */
if($editable){
	if($form->cid == 0){
		$actionUrl = $this->action_url('Admin_form',array('form' => $field->form_ID,'field' => $field->getID()));
	} else{
		$actionUrl = $this->action_url('Campaign_form?ID='.$form->cid.'&form='.$field->form_ID.'&field='.$field->getID());
	}
	$disabled = '';
} else{
	$actionUrl = '#';
	$disabled = ' disabled';
}

$this_is_a			= 'Form Field';
$edit_action    = $disabled ? '#' : $this->action_url('Campaign_form?ID='.$form->cid.'&form='.$form->id.'&field='.$field->getID());
$delete_action  = $disabled ? '#' : $this->action_url('Campaign_delete?field_ID='.$field->getID());
?>

<tr class="gradeA jLeadfieldrow" id="jleadfield_<?= $field->getID(); ?>">
	<td class="name"><a href="<?= $actionUrl; ?>"<?= $disabled ? ' class="'.$disabled.'"' : ''; ?>><?= $field->label; ?></a></td>
	<td><span style="text-transform: capitalize;"><?= $field->type; ?></span></td>
	<td><?= $field->required ? 'Required' : ''; ?></td>
	<td class="hide"><?= $field->sort; ?></td>
	<td>
        <div class="p_container _2a">
			<a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt ajax-action jaction-delrow-leadfield jdel-jleadfield_<?= $field->getID(); ?><?= $disabled; ?><?= $this->isParam('field',$field->getID()) ? ' jajax-redirect' : ''; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
			<a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt<?= $disabled; ?>" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
	</td>
</tr>