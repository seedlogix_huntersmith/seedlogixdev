<?php
$this_is_a		= 'User';
$edit_action    = $this->action_url('UserManager_profile?ID='.$nuser->getID());
$delete_action  = $this->action_url("UserManager_removeuser?ID={$nuser->getID()}");
$manage_action  = $this->action_url('Admin_doImpersonate?ID='.$nuser->id);
?>

<tr class="gradeX">
    <td><?= $nuser->getID(); ?></td>
    <td><?php $this->p($nuser->fullname); ?></td>
    <td><a href="<?= $edit_action; ?>"><?php $this->p($nuser->username); ?></a></td>
    <td><?= $nuser->role; ?></td>
    <td><?= $nuser->created; ?></td>
    <td><?= $nuser->last_login; ?></td>
    <td>
        <div class="p_container _2a">
            <?php /* <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $delete_action; ?>"><span class="delete"></span></a> */ ?>
            <a title="Manage the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $manage_action; ?>"><span class="manage"></span></a>
			<?php if($User->id == $User->parent_id){ ?>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
			<?php } ?>
        </div>
    </td>
</tr>