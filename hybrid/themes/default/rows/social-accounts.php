<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $SocialAccount SocialAccountsModel */

$socialaccount_url	= $this->action_url('Campaign_social-account?ID='.$SocialAccount->cid,array('aid' => $SocialAccount->getID()));
$authenticated		= $SocialAccount->token ? 'Yes' : 'No';

$this_is_a			= 'Social Account';
$edit_action    	= $socialaccount_url;
$delete_action  	= $this->action_url('Campaign_delete',array('SocialAccount_ID' => $SocialAccount->getID()));
?>

<tr>
	<td><a href="<?= $socialaccount_url; ?>"><?php $SocialAccount->html('name'); ?></a></td>
	<td><?php $SocialAccount->html('network'); ?></td>
	<td><?= $authenticated; ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>