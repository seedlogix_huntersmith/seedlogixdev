<?php
    /* @var $Reseller ResellerModel */
?>
<tr class="gradeX" id="role_<?php echo $Reseller->id; ?>">
    <td><?= $Reseller->getID(); ?></td>
    <td><a href="<?= $this->action_url('System_reseller?ID=' . $Reseller->id); ?>"><?php $this->p($Reseller->company); ?></a></td>
    <td><?php $this->p($Reseller->full_name); ?></td>
    <td><?php $this->p($Reseller->support_email); ?></td>
    <td><?php $this->p($Reseller->created); ?></td>
    <td><?php $this->p($Reseller->last_login); ?></td>
</tr>
