<?php
/**
 * Author:     cory frosch <cory.frosch@gmail.com>
 * License:
 * @var $report ReportModel
 */

$arr            = array('report_ID' => $report->getID(),'ID' => $report->campaign_ID);
if($arr['ID'] == 0){
    unset($arr['ID']);// unset campaign id in action url
}

$this_is_a		= 'Report';
$export_action  = '#';
$view_action    = $this->action_url('Campaign_report',$arr);
$delete_action  = $this->action_url('Campaign_delete',array('report_ID' => $report->getID()));
?>

<!--/admin.php?ID=22380&action=report&ctrl=Campaign-->
<tr class="gradeA jReportrow odd" id="jreport_<?= $report->ID; ?>">
    <td><span class="id"><?= $report->ID; ?></span></td>
    <td><a href="<?= $view_action; ?>"><span class="name"><?= $report->name; ?></span></a></td>
    <td><span class="type"><?= $report->type; ?></span></td>
    <td><span class="touchpoint_type"><?= $report->touchpoint_type; ?></span></td>
    <td><span class="created"><?= $report->created; ?></span></td>
    <td>
        <div class="p_container _2a">

<?php // Export action missing, needs to be coded. ?>
<?php /* <a title="Export the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $export_action; ?>"><span class="export"></span></a> */ ?>

            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow jdel-jreport_<?= $report->ID; ?>" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt editReport"><span class="edit"></span></a>
        </div>
    </td>
</tr>