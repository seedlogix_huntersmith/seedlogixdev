<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2/25/15
 * Time: 2:43 PM
 */

$this_is_a		= 'Feed';
$edit_action	= $this->action_url('Admin_mu-blog-posts',array('feed_ID' => $feed->id));
?>

<tr>
	<td><a href="<?= $edit_action; ?>"><?= $feed->name; ?></a></td>
	<td><?= $feed->created; ?></td>
	<td>
        <div class="p_container _1a">
			<a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
	</td>
</tr>