<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $SocialPost SocialPostsModel */

$socialpost_url	= $this->action_url('Campaign_social-post-details?ID='.$SocialPost->cid,array('pid' => $SocialPost->getID()));  

$this_is_a		= 'Social Post';
$edit_action	= $socialpost_url;
$delete_action	= $this->action_url('Campaign_delete',array('SocialPost_ID' => $SocialPost->getID()));
?>

<tr>
	<td><a href="<?= $socialpost_url; ?>"><?php $SocialPost->html('name'); ?></a></td>
	<td><?php $SocialPost->html('accounts'); ?></td>
	<td><?php $SocialPost->html('publish_date'); ?></td>
	<td>
        <div class="p_container _2a">
			<a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
			<a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
	</td>
</tr>