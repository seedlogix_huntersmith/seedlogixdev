<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$campaign_url       = $this->action_url('Campaign_dashboard?ID='.$Prospect->cid);

$this_is_a		    = 'Prospect';
$edit_action        = $this->action_url('Campaign_prospect?ID='.$Prospect->cid.'&pid='.$Prospect->getID());
$delete_action      = $this->action_url('Campaign_delete',array('prospect_ID' => $Prospect->getID()));
?>

<tr>
    <td><span class="<?= $Prospect->getColor(); ?>"><?= $Prospect->getGrade(); ?></span></td>
    <td><a href="<?= $edit_action; ?>" title=""><?php trim($Prospect->name) != '' ? $Prospect->html('name') : print 'Unknown'; ?></a></td>
    <td><?php trim($Prospect->email) != '' ? $Prospect->html('email') : print 'Unknown'; ?></td>
    <td><?php trim($Prospect->phone) != '' ? $Prospect->html('phone') : print 'Unknown'; ?></td>

    <td><?php $Prospect->html('campaign_name'); ?></td>

    <td><?php $Prospect->html('created'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>