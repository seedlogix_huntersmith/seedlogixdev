<?php
/*
 * Author:     cory frosch <cory.frosch@gmail.com>
 * License:    
 	@amado this is the rows for the REPORTS_campaign not CAMPAIGN_reports the campaign names should be linked
 */
?>
<tr>
    <td><a href="/admin.php?ID=22380&action=report&ctrl=Campaign">Report 1</a></td>
    <td align="center">Daily</td>
    <td align="center"><a href="" class="campaignName">Campaign Name</a></td>
    <td align="center">4/16/2014</td>
    <td class="tableActs">
        <a href="/admin.php?ID=22380&action=report&ctrl=Campaign" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Export"><span class="iconb" data-icon=""></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>

<!-- Extra rows for static Data -->
<tr>
    <td><a href="/admin.php?ID=22380&action=report&ctrl=Campaign">Report 2</a></td>
    <td align="center">Weekly</td>
    <td align="center"><a href="" class="campaignName">PastePHP</a></td>
    <td align="center">4/14/2014 - 4/20/2014</td>
    <td class="tableActs">
        <a href="/admin.php?ID=22380&action=report&ctrl=Campaign" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Export"><span class="iconb" data-icon=""></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>
<tr>
    <td><a href="/admin.php?ID=22380&action=report&ctrl=Campaign">Report 3</a></td>
    <td align="center">Monthly</td>
    <td align="center"><a href="" class="campaignName">New Campaign</a></td>
    <td align="center">4/1/2014 - 4/30/2014</td>
    <td class="tableActs">
        <a href="/admin.php?ID=22380&action=report&ctrl=Campaign" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Export"><span class="iconb" data-icon=""></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>
<tr>
    <td><a href="/admin.php?ID=22380&action=report&ctrl=Campaign">Report 4</a></td>
    <td align="center">Yearly</td>
    <td align="center"><a href="" class="campaignName">new campaign!</a></td>
    <td align="center">2014 - current</td>
    <td class="tableActs">
        <a href="/admin.php?ID=22380&action=report&ctrl=Campaign" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Export"><span class="iconb" data-icon=""></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>
<tr>
    <td><a href="/admin.php?ID=22380&action=report&ctrl=Campaign">Report 5</a></td>
    <td align="center">Custom Range</td>
    <td align="center"><a href="" class="campaignName">tests-feeds</a></td>
    <td align="center">4/05/2014 - 4/25/2014</td>
    <td class="tableActs">
        <a href="/admin.php?ID=22380&action=report&ctrl=Campaign" class="tablectrl_small bDefault tipS" title="Edit"><span class="iconb" data-icon="&#xe1db;"></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Export"><span class="iconb" data-icon=""></span></a>
        <a href="#" class="tablectrl_small bDefault tipS" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
    </td>
</tr>