<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?><tr>
    <td><?php preg_match('#/Date\(([0-9]+)\)#', $Calls->DateCreated, $m); echo date('Y-m-d H:m:s', $m[1]/1000); ?></td>
    <td><?= $Calls->Status; ?></td>
    <td><?= $Calls->From; ?></td>
    <td><?= $Calls->Size; ?></td>
    <td><?= $Calls->MessageURL; ?></td>
</tr>
