<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/21/14
 * Time: 11:53 AM
 * @var $this Template
 * @var $site RankingSiteModel
 */

$this_is_a      = 'Keyword Campaign';
$edit_action	= $this->action_url('Campaign_ranking',array('ID' => $Campaign->getID(),'site_ID' => $site->getID()));
$delete_action	= $this->action_url('Campaign_delete',array('rankingSite_ID' => $site->getID()));
?>

<tr>
    <td>
        <a href="<?= $edit_action; ?>"><?php $this->p($site->hostname); ?></a>
        <input type="hidden" value="<?= $site->touchpoint_ID; ?>" name="touchpoint_ID">
    </td>
    <td><span class="statsPlus"> 0%</span></td>
    <td><?= $site->keywords; ?></td>
    <td><?= $site->google_first; ?></td>
    <td><?= $site->bing_first; ?></td>
    <td><?= $site->created; ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action jaction-delrow" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt edit-dialog" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>