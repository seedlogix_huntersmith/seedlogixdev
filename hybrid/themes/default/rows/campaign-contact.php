<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Contact ContactDataModel */

// This is not being used.
// $form_url       = $this->action_url('Campaign_form?ID=',array('form' => $Contact->getID()));

$this_is_a      = 'Contact';
$edit_action	= $this->action_url('Campaign_contact?ID='.$Contact->cid,array('pid' => $Contact->getID()));
$delete_action	= $this->action_url('Campaign_delete',array('contact_ID' => $Contact->getID()));
	
$str = trim( $Contact->phone );
$str = preg_replace( '/\s+(#|x|ext(ension)?)\.?:?\s*(\d+)/', ' ext \3', $str );

$us_number = preg_match( '/^(\+\s*)?((0{0,2}1{1,3}[^\d]+)?\(?\s*([2-9][0-9]{2})\s*[^\d]?\s*([2-9][0-9]{2})\s*[^\d]?\s*([\d]{4})){1}(\s*([[:alpha:]#][^\d]*\d.*))?$/', $str, $matches );

if ( $us_number ) {
	$displayPhone = $matches[4] . '-' . $matches[5] . '-' . $matches[6] . ( !empty( $matches[8] ) ? ' ' . $matches[8] : '' );
} else $displayPhone = $str;

$phoneCheck = PhoneModel::Fetch('cid = "'.$Contact->cid.'" AND type = "voip"');
if($phoneCheck){
$phone = '1'.preg_replace('[\D]', '', $displayPhone);
$phonea = '<a class="jopendialog jopen-makecall" data-phone="'.$phone.'">'.$displayPhone.'</a>';
} else $phonea = $displayPhone;
?>

<tr>
    <td><a href="<?= $edit_action; ?>" title=""><?php $Contact->html('first_name'); ?></a></td>
    <td><a href="<?= $edit_action; ?>" title=""><?php $Contact->html('last_name'); ?></a></td>
    <td><a class="jopendialog jopen-sendemail" data-email="<?= mb_strimwidth($Contact->email,0,57,'&hellip;'); ?>" data-contactid="<?=$Contact->getID()?>" data-aid="<?=$Contact->account_id?>"><?= mb_strimwidth($Contact->email,0,57,'&hellip;'); ?></a></td>
	<td><?=$phonea?></td>
	<td><?php $Contact->html('last_updated'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
        </div>
    </td>
</tr>