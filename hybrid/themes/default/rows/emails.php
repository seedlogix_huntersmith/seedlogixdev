<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

/* @var $Account AccountsModel */

$account_url	= $this->action_url('Campaign_email?ID='.$Email->cid,array('eid' => $Email->getID()));

$this_is_a			= 'Email';
$edit_action    	= $account_url;
$delete_action  	= $this->action_url('Campaign_delete',array('Account_ID' => $Email->getID()));
?>

<tr>
	<td><a href="<?= $account_url; ?>"><?php $Email->html('name'); ?></a></td>
	<td><?php $Email->html('subject'); ?></td>
	<td><?php $Email->html('created'); ?></td>
    <td>
        <div class="p_container _2a">
            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
            <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a>
		</div>
    </td>
</tr>