<?php
/**
 * Author: Jon Aguilar
 * @var $keyword RankingKeywordModel
 */

   /* if($keyword->rank_google == "101")
        $google_rank_sign = "+";
    if($keyword->rank_bing == "101")
        $bing_rank_sign = "+";

    if($keyword->google_change != NULL && $keyword->rank_google == "101") {
        if($keyword->google_change != "-1")
            $keyword->google_change = (string)((int)$keyword->google_change + 1);
        $google_change_sign = "+";
        $keyword->rank_google = (string)((int)$keyword->rank_google - 1) ;

    }
    if ($keyword->google_change == NULL)
    {
        if ($keyword->rank_google == "101") {
            $keyword->google_change = "0";
            $keyword->rank_google = (int)$keyword->rank_google -1;
        }
	    elseif ($keyword->rank_google <= "100") {
            if($keyword->rank_google == "100")
                $keyword->google_change = "1";
            else
            $keyword->google_change = (string)(100 - (int)$keyword->rank_google);
            $google_change_sign = "+";
        }
    }

    if($keyword->bing_change != NULL && $keyword->rank_bing == "101") {
        if($keyword->bing_change != "-1")
            $keyword->bing_change = (string)((int)$keyword->bing_change + 1);
        $bing_change_sign = "+";
        $keyword->rank_bing = (string)((int)$keyword->rank_bing - 1);
    }
    if ($keyword->bing_change == NULL)
    {
        if ($keyword->rank_bing == "101") {
            $keyword->bing_change = "0";
            $keyword->rank_bing = (int)$keyword->rank_bing - 1;
        }
	    elseif ($keyword->rank_bing <= "100") {
            if($keyword->rank_bing == "100")
                $keyword->bing_change = "1";
            else
            $keyword->bing_change = (string)(100 - (int)$keyword->rank_bing);
            $bing_change_sign = "+";
        }
    }*/

        $classGoogle = $keyword->google_change >= 0 ? "statsPlus" : "statsMinus";
        $classBing = $keyword->bing_change >= 0 ? "statsPlus" : "statsMinus";

/*if ($keyword->google_change < "0" && $keyword->rank_google != "0")
    $keyword->google_change = preg_replace("/[-]/", "", $keyword->google_change);

if ($keyword->bing_change < "0" && $keyword->rank_bing != "0")
    $keyword->bing_change = preg_replace("/[-]/", "", $keyword->bing_change);*/

if($keyword->rank_google==100) $keyword->rank_google = '+100';
if($keyword->rank_bing==100) $keyword->rank_bing = '+100';
if($keyword->google_change == NULL) $keyword->google_change = 0;
if($keyword->bing_change == NULL) $keyword->bing_change = 0;

$this_is_a      = 'Keyword';
$edit_action	= '#';
$delete_action	= $this->action_url('Campaign_delete',array('keyword_ID' => $keyword->getID(),'rankingSite_id' => $keyword->site_id));
?>

<tr>
    <td><?= $keyword->keyword; ?></td>
    <td><?= (isset($google_rank_sign) ? $google_rank_sign : '').$keyword->rank_google; ?></td>
    <td><span class="<?= $classGoogle; ?>"> <?= (isset($google_change_sign) ? $google_change_sign : '').$keyword->google_change; ?></span></td>
    <td><?= (isset($bing_rank_sign) ? $bing_rank_sign : '').$keyword->rank_bing; ?></td>
    <td><span class="<?= $classBing; ?>"> <?= (isset($bing_change_sign) ? $bing_change_sign : '').$keyword->bing_change; ?></span></td>
    <td>
        <div class="p_container _1a">

<?php /* <a title="Edit the <?= $this_is_a; ?>" class="bDefault f_rt" href="<?= $edit_action; ?>"><span class="edit"></span></a> */ ?>

            <a title="Delete the <?= $this_is_a; ?>" class="bDefault f_rt jajax-action" href="<?= $delete_action; ?>"><span class="delete"></span></a>
        </div>
    </td>
</tr>