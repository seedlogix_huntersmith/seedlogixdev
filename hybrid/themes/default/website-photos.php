<?php $disable = $User->can('create_slide',$site->id) ? '' : ' disabled'; ?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php');?>

    <div class="grid12 m_tp"><?php $this->action_input('jnewslide','Website_newslide'); ?><a href="<?= $this->action_url('Website_createslide?ID='.$site->id); ?>" id="jnewslidebtn" class="buttonS f_lt">Add a New Slide</a></div>
    <div class="grid12">
        <ul id="jslideslist">
<?php
$counter = 0;
foreach($slides as $photo):
	if($photo->hasBackground()):
?>
			<li class="slide" id="slidethumb_<?= $photo->ID; ?>">
				<a href="#slideanchor_<?= $photo->ID; ?>">
					<img src="<?= UserModel::USER_DATA_DIR.$photo->background; ?>" class="thumbsmall">
<?php else: ?>
			<li class="slide jnothumb" id="slidethumb_<?= $photo->ID; ?>">
				<a href="#slideanchor_<?= $photo->ID; ?>">
<?php endif; ?>
					<div class="thumbsmalldesc">Slide<br><?= ++$counter; ?></div>
				</a>
			</li>
<?php endforeach; ?>
		</ul>
    </div>
    <?php //var_dump($site->getSlidePhotos()); ?>

    <?php // $this->getTable('mediatbl'); ?>
</div>
<div class="fluid">
	<input type="hidden" id="photo-media-action" value="<?php echo $this->action_url('media_search'); ?>">

<?php
foreach($slides as $slide){
	$this->getWidget('slideform.php',array('slide' => $slide,'disable' => $disable));
}
?>

</div>
<!-- //end content container -->


<script type="text/javascript">

<?php $this->start_script(); ?>

//===== iButtons =====//
$('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
    labelOn     : '',
    labelOff    : '',
    change      : function($input){
				var value = $input.attr('checked') ? 1 : 0;
				new AjaxSaveDataRequest($input.get(0),value);
    }
});

$(document).ready(function(){

<?php $this->getJ('admin-lock.php',array('isAdminUser' => $User->isAdmin())); ?>

<?php $this->getJ('wysiwyg-ide.php',array('disable' => !empty($disable))); ?>

	$('.search-media').each(function(){
		new SearchMediaProvider($,this);
	});
	$('.slide_title').change(function(){
		var slideid			= this.id.split(/_/).pop();
		$('#slidethumb_' + slideid + ' div.thumbsmalldesc').text(this.value);
	});
	if(new RegExp('[\?&]maxPages([^&#]*)').exec(window.location.href)){
		notie.alert({text:'More than 6 slides is not allowed.'});
	}
	$('body').on('click','a.buttonS.disabled,.buttonS[disabled]',function(e){
		e.preventDefault();
	});
	// Fix for Firefox not always knowing where to scroll
	var anchor				= new RegExp('[\?#]slideanchor_([0-9]*)').exec(window.location.href);
	if(!anchor === undefined){
		$(document).scrollTop($('#slideanchor_' + anchor[1]).offset().top);
	}

});

<?php $this->end_script(); ?>

</script>