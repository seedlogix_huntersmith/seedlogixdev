<div class="fluid">
    <form action="<?php echo $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
    <div class="grid9"><!-- Left Column -->
			<input type="hidden" name="listing[type]" value="<?php echo $article->getNetworkListingType(); ?>" />
			<input type="hidden" name="listing[id]" value="<?php echo $article->getID(); ?>" />
        <div class="fluid">
            <div class="widget grid12">
              <div class="whead"><h6><span class="jHtml_name"><?php $article->html('name'); ?></span> Information</h6><div class="clear"></div></div>
                <div class="formRow">
                    <div class="grid3"><label>Title:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('name'); ?>" name="listing[name]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Summary:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('summary'); ?>" name="listing[summary]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>News Body:</label></div>
                    <div class="grid9"><textarea rows="8" cols="" name="listing[body]"><?php $article->html('body'); ?></textarea></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Keywords:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('keywords'); ?>" name="listing[keywords]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Website URL:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('website'); ?>" name="listing[website]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Company Name / Author:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('author'); ?>" name="listing[author]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Email:</label></div>
                    <div class="grid9"><input type="text" value="<?php $article->html('email'); ?>" name="listing[email]" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Additional Information</label></div>
                    <div class="grid9"><textarea rows="8" cols="" name="listing[summary]"><?php $article->html('summary'); ?></textarea></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="grid3"><!-- Right Column -->
      <?php /**
       * @todo VERSION 1.1
       * ?>
        <div class="fluid">
            <div class="widget grid12">
                <div class="whead"><h6>Publish</h6><div class="clear"></div></div>
                <div class="formRow">
                    <div class="grid12 yes_no">
                        <input type="checkbox" id="check1" name="chbox1" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div> <?php 
        **/ ?>
        <div class="fluid">
            <div class="widget grid12">
                <div class="whead"><h6>Industry</h6><div class="clear"></div></div>
                <div class="formRow">
                    <div class="grid12">
                      <?php $this->getControl('industry-select.php', array('selectname' => 'listing[category]',
                          'value' => $article->category)); ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="fluid">
            <div class="widget grid12">
                <div class="whead"><h6>Business Logo</h6><div class="clear"></div></div>
                <div class="formRow">
                    <?php
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'listing[thumb_id]',
                                    'value' => $article->thumb_id)); ?>
                </div>
            </div>
        </div>
		<div class="fluid chosen_dropfix">
            <div class="widget grid12">
                <div class="whead"><h6>City and State aren't shown on your press release but will help us categorize it. </h6><div class="clear"></div></div>
                <div class="formRow">
                    <div class="grid3"><label>City:</label></div>
                    <div class="grid9"><input type="text" name="listing[city]" value="<?php $article->html('city'); ?>" /></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>State:</label></div>
                    <div class="grid9">
							<?php $this->getWidget('states-dropdown.php', array('name' => 'listing[state]', 'value' => $article->state)); ?>                      
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div></form>
</div> <!-- //end fluid -->