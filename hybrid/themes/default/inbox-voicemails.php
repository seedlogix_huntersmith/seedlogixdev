<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Voicemails Inbox</h6></div>

		<?php 
		    $this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('InboxVoicemails'),
					'class' => 'col_1st,col_2nd,col_end',
					'id' => 'jInboxVoicemails',
					'data' => $InboxVoicemails,
					'rowkey' => 'InboxVoicemail',
					'rowtemplate' => 'inbox-voicemails.php',
					'total' => $totalRecords,
					'sourceurl' => $this->action_url('campaign_ajaxinboxvoicemails'),
					'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
		?>

    </div>
</div>

<script type="text/javascript">
	<?php $this->start_script(); ?>
		

	<?php $this->end_script(); ?>
</script>