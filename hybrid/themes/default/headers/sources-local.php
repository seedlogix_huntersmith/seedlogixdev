<!-- NEW JQUERY VERSION -->
<script src="<?php echo $_path; ?>js/framework/jquery-1.11.2.min.js"></script>
<script src="<?php echo $_path; ?>js/framework/jquery-migrate-1.2.1.min.js"></script>

<!-- NEW JQUERY UI VERSION -->
<script type="text/javascript" src="<?php echo $_path; ?>js/framework/jquery-ui.min.js"></script>

<?php //if($jsFileSet != 'dashboard'): ?>
<!-- TinyMCE CDN hosted by Cachefly -->
<script src="<?php echo $_path; ?>js/plugins/tinymce/tinymce.min.js"></script>
<?php //endif; >