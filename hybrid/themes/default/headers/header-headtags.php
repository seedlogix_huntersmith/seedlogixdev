<?php
$brandingLogo   = $Branding->getLogoUrl();
$allowed_hosts  = array('login.seedlogix.com');
$boBrand = ResellerModel::Fetch('admin_user = %d', $User->parent_id);
//var_dump($boBrand);
?>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>

<?php
/*if(isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$allowed_hosts)){
    $this->p($_title.' / SeedLogix');
} else{
    $this->p($_title.' / '.$Branding->company);
}*/
if($boBrand){
    $this->p($_title.' / '.$boBrand->company);
} else{
    $this->p($_title.' / SeedLogix');
}
?>

</title>
<!-- Google Fonts Roboto -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">

<link href="<?= $_path; ?>css/styles.css" rel="stylesheet" type="text/css">

<?php /* needs updating

<link href="<?= $_path; ?>css/responsive.css" rel="stylesheet" type="text/css">

needs updating */ ?>

<link href="<?= $_path; ?>css/hybrid.css" rel="stylesheet" type="text/css">
<link href="<?= $_path; ?>css/select2.min.css" rel="stylesheet" type="text/css">
<link href="<?= $_path; ?>js/plugins/codemirror/lib/codemirror.css" rel="stylesheet" type="text/css">
<link href="<?= $_path; ?>css/timeline.css" rel="stylesheet" type="text/css">
<link href="<?= $_path; ?>css/timeline_light.css" rel="stylesheet" type="text/css">
<link href="<?= $_path; ?>js/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css">

<!-- Notie JS -->
<link href="<?= $_path; ?>css/notie.min.css" rel="stylesheet" type="text/css">

<?php $this->getHeader('soucres-cdn.php'); ?>

<!-- SeedLogix Override CSS -->
<link href="<?= $_path; ?>css/override.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/excanvas.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.group.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.simplestack.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.axislabels.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.categories.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.flot.time.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/JUMFlot.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/charts/jquery.sparkline.min.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/tables/jquery.sortable.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/tables/jquery.resizable.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/autogrowtextarea.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.uniform.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.inputlimiter.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.autotab.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.chosen.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.ajax-chosen.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.dualListBox.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.cleditor.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.ibutton.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.select2.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/forms/jquery.validationEngine.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/uploader/plupload.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/uploader/plupload.html4.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/uploader/plupload.html5.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/uploader/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/wizards/jquery.form.wizard.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/wizards/jquery.validate.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/wizards/jquery.form.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.collapsible.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.progress.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.timeentry.min.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.colorpicker.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.fileTree.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/ui/jquery.sourcerer.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/plugins/others/jquery.fullcalendar.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/plugins/others/jquery.elfinder.js"></script>

<!-- CODE MIRROR -->
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/lib/codemirror.js"></script>
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/mode/css/css.js"></script>
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/mode/xml/xml.js"></script>
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/mode/javascript/javascript.js"></script>
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script language="javascript" src="<?= $_path; ?>js/plugins/codemirror/mode/htmlembedded/htmlembedded.js"></script>

<!-- Timeline -->
<script type="text/javascript" src="<?= $_path; ?>js/timeline.min.js"></script>

<!-- bxSlider Javascript file -->
<script src="<?= $_path; ?>js/plugins/bxslider/jquery.bxslider.js"></script>

<!-- Moxiemanager -->
<script src="<?= $_path; ?>js/plugins/moxiemanager/js/moxman.loader.min.js"></script>

<script type="text/javascript" src="<?= $_path; ?>js/files/bootstrap.js"></script>
<script type="text/javascript" src="<?= $_path; ?>js/files/functions.js"></script>



<script type="text/javascript" src="<?= $_path; ?>js/charts/chart.js"></script>

<link href="<?= $_path; ?>css/responsive.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
/* $this->start_script(); */

<?php
if(isset($this->scripts)){
    echo implode("\n",$this->scripts);
}
?>

/* $this->end_script(); */

$(function(){
    $('select,.check,.check :checkbox,input:radio,input:file').not('.select,.select2').uniform();
});
</script>
<?php 
if($boBrand->bg_clr){
$color = $boBrand->bg_clr;	
include('colors.php');
}
?>
<?php if($boBrand->css_link){?>
<style>
<?=$boBrand->css_link?>
</style>		
<?php } ?>