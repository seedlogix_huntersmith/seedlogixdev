<style>
/* ----------------------------------------------------------------
    Colors

    Replace the HEX Code with your Desired Color HEX
-----------------------------------------------------------------*/
.subNav li a,
.subNav li a:hover,
.subNav li.activeli > a:hover,
.widget > [class*="whead"].exp3:hover > span.chevron,
.buttonS:hover,
body.gtw .grid12 [class*="grid"]:hover > div.gtw_link > a.buttonS,
.nInformation:hover,
.nav li a.active,
#top span.hamburger,
.buttonS,
.dataTables_paginate .paginate_enabled_previous, 
.dataTables_paginate .paginate_enabled_next,
.dataTables_paginate .paginate_disabled_previous, 
.dataTables_paginate .paginate_disabled_next, 
.dataTables_paginate .paginate_button.previous, 
.dataTables_paginate .paginate_button.next,
ul.userNav li a:hover,
.ui-dialog,
.ui-dialog .ui-dialog-titlebar-close:hover,
.ui-dialog .ui-dialog-titlebar-close:focus,
.ui-dialog .ui-dialog-buttonpane button
{ background: <?php echo $color; ?>;}
.subNav li ul li.activeli > a span.titlename,
.tDefault tbody tr td > a:hover,
span.pageTitle,
.subNav li ul li a:hover span.titlename,
.subNav li ul li.activeli > a:hover span.titlename,
ul.breadcrumbs li a:hover
{ color: <?php echo $color; ?>;}
body.gtw .grid12 [class*="grid"]:hover > div.gtw_link
{ border-top-color: <?php echo $color; ?>;}
.subNav li a:hover span.chevron, 
.subNav li.activeli a span.chevron, 
.subNav li ul li a:hover span.chevron, 
.subNav li ul li.activeli a span.chevron, 
.subNav li a span.chevron,
#top span.hamburger:hover,
.ui-dialog .ui-dialog-titlebar-close span
{background: #fff;}
</style>