<?php

/*
 * Author:     Jon Aguilar
 * License:
 */
/*$brandingLogo = $Branding->getLogoUrl();

$server = $_SERVER['HTTP_HOST'];
//$allowed_hosts = array('beta.powerleads.biz', 'admin.powerleads.biz', 'www.powerleads.biz', 'powerleads.biz');
$allowed_hosts = array('login.seedlogix.com');

if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'], $allowed_hosts)) {
    //$brandingLogo = 'themes/default/images/powerleads/powerLeadsLogo7.png';
    $brandingLogo = 'themes/default/images/seed-logix_alpha.png';
}*/
$boBrand = ResellerModel::Fetch('admin_user = %d', $User->parent_id);
//var_dump($User);
if($boBrand->logo){
$brandingLogo = 'users'. $boBrand->logo;
} else $brandingLogo = 'themes/default/images/seed-logix_alpha.png';
$detect = new MobileDetect();
?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php $this->getHeader('header-headtags.php'); ?>

</head>
<body>
<!-- bodywrap start -->
<div id="bodywrap">

<?php /*
<!-- The Responsive Menu start -->
<ul id="rmenu">
  <li class=""><a href="" title=""><i class="vcentered" data-feather="home"></i><span class="vcentered">Home</span></a></li>
  <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="activity"></i><span class="vcentered">Campaigns</span></a>
    <ul style="display:none;">
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Campaigns Dashboard</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Campaigns Reports</span></a></li>
    </ul>
  </li>
  <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="sliders"></i><span class="vcentered">Admin</span></a>
    <ul style="display: none;">
      <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Manage</span></a>
        <ul style="display: none;">
          <li class=""><a href="" title=""><span class="vcentered">User Profile</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">System Info</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">Settings</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">Branding</span></a></li>
        </ul>
      </li>
      <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Users</span></a>
        <ul style="display:none;">
          <li class=""><a href="" title=""><span class="vcentered">Active Users</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">Inactive Users</span></a></li>
        </ul>
      </li>
      <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">MultiSite CMS</span></a>
        <ul style="display:none;">
          <li class=""><a href="" title=""><span class="vcentered">MU Touchpoints</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">MU Forms</span></a></li>
          <li class=""><a href="" title=""><span class="vcentered">MU Blogs</span></a></li>
        </ul>
      </li>
      <li class=""><a href="" title="" ><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Roles</span></a></li>
      <li class=""><a href="" title="" ><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Spam</span></a></li>
    </ul>
  </li>
  <li class=""><a href="" title="" class="exp"><i class="vcentered" data-feather="settings"></i><span class="vcentered">System</span></a>
    <ul style="display:none;">
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Events</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Cron</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">User Roles</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Debug</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">System Configuration</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Account Limits</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">System Subscriptions</span></a></li>
      <li class=""><a href="" title=""><i class="vcentered" data-feather="chevron-right"></i><span class="vcentered">Resellers</span></a></li>
    </ul>
  </li>
</ul>
<!-- The Responsive Menu end -->
*/ ?>

<div id="top">
    <div class="wrapper">
		<span class="hamburger"></span>
        <a href="<?= $this->action_url('Campaign_home'); ?>" title="" class="logo"><img src="<?= $brandingLogo; ?>" alt=""></a>

<?php $this->getNavigation('top-nav.php'); ?>

    </div>
</div>

<!-- Sidebar -->
<div id="sidebar">

<?php $this->getNavigation('left-icons.php'); ?>

<?php $this->getPart('secNav2.php'); ?>

</div>
<!-- /Sidebar -->

<!-- Content begins -->
<div id="content">
    <div class="contentTop">
        <span class="pageTitle"><?= $this->p($_title); ?></span>
		<?php if ( $detect->isMobile() == false || $detect->isTablet() == false){ ?>
        <ul class="quickStats2 ov_boxes">
            <?php
            if(isset($quickstats)): 
                foreach($quickstats as $qs): 
                    $qs->printli($this);
                endforeach;
            endif;
            ?>
        </ul>
		<? } ?>
    </div>
    
    <!-- Breadcrumbs bar -->
    <div class="breadLine<?=($this->showDashboardRange ? ' withDateRange"':'"')?>>
        <ul id="breadcrumbs" class="breadcrumbs">

<?php if(isset($breadcrumbs)){$breadcrumbs->printli($this);} ?>

        </ul>
        <?php /*
        <!--
            =================================
            Dashboard Date Range Buttons
            
            CSS Default Class: .bDefault
            CSS Active Class: .bBlue AND .current
            *these need to be replaced, as to make a different one active
            remove .bDefault and add .bBlue AND .current for new active and revers for old active

            CSS Class: .disabled - adding disabled class will show the button as disabled and will not fire a dashboard refresh onClick
            CSS Class: .hide - hides (display:none) the button from the set
        -->
        */ ?>
        <?php if($this->showDashboardRange): ?>
        <ul class="btn-group toolbar" style="float: right;" id="DateRangeSelectors">

<?php /*
			<li><button data-range="0-ALL" class="buttonS">All Time</button></li>
			<?php if($User->getCreatedDate()->diff(new DateTime())->m > 6): ?>
				<li><button data-range="12-MONTH" class="buttonS">Last 12 Months</button></li>
			<?php endif; if($User->getCreatedDate()->diff(new DateTime())->days > 30): ?>
				<li><button data-range="6-MONTH" class="buttonS">Last 6 Months</button></li>
			<?php endif; if($User->getCreatedDate()->diff(new DateTime())->days > 7): ?>
				<li><button data-range="30-DAY" class="buttonS">Last 30 Days</button></li>
			<?php endif; ?>
			<li><button data-range="7-DAY" class="buttonS">Last 7 Days</button></li>
*/ ?>

		</ul>
		<?php endif; ?>
		<!-- Breadlinks -->
        <div class="breadLinks">
            <?php $this->getList('OrdersTasks.php'); ?>
        </div>
    </div>
    <!-- /Breadcrumbs bar -->

    <!-- Main content -->

<?php
/* MU Hack */
$id_attr        = basename($_include_file,'.php');
$mublogpost     = $this->isAction('mu-blog-post');
$the_id         = '';

if($id_attr == 'website-prospect'){
    $the_id     = 'campaign-prospect';
} elseif($mublogpost){
    $the_id     = 'admin-mu-blogPost';
} else{
    $the_id     = $id_attr;
}
?>

    <div id="<?= $the_id; ?>" class="wrapper <?= $this->getaction(); ?>">