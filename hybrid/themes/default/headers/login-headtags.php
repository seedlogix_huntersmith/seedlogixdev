<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>

<?php
if(isset($server) && in_array($server,$allowed_hosts)){
    $this->p($_title.' / SeedLogix');
} else{
    $this->p($_title.' / '.$Branding->company);
}
?>

</title>
<?php /* Google Fonts Roboto */ ?>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
<?php /* Login Stylesheet */ ?>
<link href="<?= $_path; ?>css/login.css" rel="stylesheet" type="text/css">
<?php /* jQuery */ ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>
<?php /* JS Cookie */ ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>