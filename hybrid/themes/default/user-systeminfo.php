<h1>Super Admin - Change Subscription Plans</h1>
<div class="fluid">
    <div class="widget">
            <div class="whead">
                <h6>Subscription Plans</h6>
                <a class="buttonS f_rt editFields" href="#"><span>Update the Plans</span></a>
            </div>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tLight">
                <thead>
                    <tr>
                        <td width="40%">Plan</td>
                        <td>Base Price</td>
                        <td>TouchPoints</td>
                        <td>Keywords</td>
                        <td>Emails</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">
                            <strong>Professional</strong>
                        </td>
                        <td>
                            <div class="formRow">
                                <div class="grid1">$</div>
                                <div class="grid11"><input type="number" name="professional-base-price" value="50"></div>
                            </div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="professional-touchpoints" value="10"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="professional-keywords" value="100"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="professional-emails" value="1200"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <strong>Corporate</strong>
                        </td>
                        <td>
                            <div class="formRow">
                                <div class="grid1">$</div>
                                <div class="grid11"><input type="number" name="corporate-base-price" value="200"></div>
                            </div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="corporate-touchpoints" value="50"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="corporate-keywords" value="500"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="corporate-emails" value="60000"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <strong>Enterprise</strong>
                        </td>
                        <td>
                            <div class="formRow">
                                <div class="grid1">$</div>
                                <div class="grid11"><input type="number" name="enterprise-base-price" value="800"></div>
                            </div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="enterprise-touchpoints" value="250"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="enterprise-keywords" value="2500"></div>
                        </td>
                        <td>
                            <div class="formRow"><input type="text" name="enterprise-emails" value="300000"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
</div>