<!-- Alternative buttons -->
<?php $this->getMenu('responder-icons.php'); ?>

<div class="fluid">
    <div class="widget">
        <div class="whead"><h6>Responder Configuration</h6><div class="clear"></div></div>
        <div class="formRow">
            <div class="grid3"><label>Name:</label></div>
            <div class="grid9"><input type="text" name="regular"></div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Time Delay:</label></div>
            <div class="grid9" id="spinnerbox"><input type="text" size="2" style="width:20px;"class="spinner" value="10" />
                <select name="responder[time]">
                    <option value="hours" selected="yes">Hours</option>
                    <option value="days">Days</option>
                    <option value="weeks">Weeks</option>
                    <option value="months">Months</option>
                </select>
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Active:</label></div>
            <div class="grid9 on_off">
                <div class="floatL mr10"><input type="checkbox"    checked="checked" name="chbox" /></div></div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<style type="text/css">
    #spinnerbox .ui-spinner {  float: left; }
</style>