<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Mailboxes</h6>
            <div class="clear"></div>
		</div>
		<div id="dyn">
			<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jmailers">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td width="100">Actions</td>
                    </tr>
                </thead>
                <tbody class="jmailers">
<?php
                $this->getRow('campaign-mailboxes.php');
?>
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /end fluid -->


<script type="text/javascript">
<?php $this->start_script(); ?>


<?php $this->end_script(); ?>
</script>