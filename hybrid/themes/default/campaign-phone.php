<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Phone Calls Report</h6></div>

<?php 
                        $this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('PhoneRecords'),
					'class' => '',
					'id' => 'jPhoneRecords',
					'data' => $PhoneRecords,
					'rowkey' => 'PhoneRecord',
					'rowtemplate' => 'phone-records.php',
					'total' => $totalRecords,
					'sourceurl' => $this->action_url('campaign_ajaxPhone', array('ID' => $Campaign->getID())),
                    'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
                    ?>

    </div>
</div>


<script type="text/javascript">
<?php $this->start_script(); ?>


<?php $this->end_script(); ?>
</script>