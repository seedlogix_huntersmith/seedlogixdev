<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
<div class="grid4"> 
  <!-- Website statistics widget -->
  <div class="widget">
    <div class="whead">
      <h6>Social Activity</h6>
      <div class="clear"></div>
    </div>
    <div class="body">
        <form class="jsocialchart" action="<?php echo $this->action_url('piwikAnalytics_ajaxGetChartData'); ?>">
            <input type="hidden" name="chart" value="SocialStatsChartData" />
            <?php if(isset($Campaign)): ?>
            <input type="hidden" name="CID" value="<?php echo $Campaign->id; ?>" />
            <?php endif; ?>
      <div class="chart1 jchart1" id="chart1"></div>
        </form>
    </div>
  </div>
</div>