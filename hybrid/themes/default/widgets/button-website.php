<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 5/4/15
 * Time: 11:12 AM
 * @var $parent_hub_lock HubFieldLocks
 * @var $fieldname string
 * @var $can_edit_site boolean
 * @var $id int
 * @var $label string
 */

$disable = $parent_hub_lock && $parent_hub_lock->isLocked($fieldname) || !$can_edit_site ? ' disabled' : '';
?>

        <div class="buttonS f_rt<?= $class; ?><?= $disable; ?>" id="<?= $id; ?>"><span><?= $label; ?></span></div>