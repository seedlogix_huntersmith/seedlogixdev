<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

<!-- Top Ranking Keywords widget -->
<div class="widget grid4 rightTabs" id="jrankings">
  <div class="whead">
    <h6>Top 5 Ranking Keywords</h6>
  </div>
  <div class="tabs">
    <ul class="tabs">
      <li><a href="#tabs-8">Bing</a></li>
      <li class="activeTab"><a href="#tabs-7">Google</a></li>
    </ul>
    <div class="tab_container" style="margin: 0; padding: 0;">
      <div id="tabs-7" class="tab_content" style="margin: 0; padding: 0;">
        <table cellpadding="0" cellspacing="0" width="100%" class="tAlt">
          <thead>
            <tr>
              <td width="60">Ranking</td>
              <td>Keyword</td>
            </tr>
          </thead>
          <tbody class="google">

<?php if($kwranks['google']): foreach($kwranks['google'] as $keywordobj): ?>

            <tr>
              <td><span class="webStatsLink"><?= $keywordobj->ranking; ?></span></td>
              <td><?= $keywordobj->keyword; ?></td>
            </tr>

<?php endforeach; else: ?>

            <tr class="jstatus"><td colspan="2">No Stats Available.</td></tr>

<?php endif; ?>

          </tbody>
        </table>
      </div>
      <div id="tabs-8" class="tab_content hide" style="margin: 0; padding: 0;">
        <table cellpadding="0" cellspacing="0" width="100%" class="tAlt">
          <thead>
            <tr>
              <td width="60">Ranking</td>
              <td>Keyword</td>
            </tr>
          </thead>
          <tbody class="bing">

<?php if($kwranks['bing']): foreach($kwranks['bing'] as $keywordobj): ?>

            <tr>
              <td><span class="webStatsLink"><?= $keywordobj->ranking; ?></span></td>
              <td><?= $keywordobj->keyword; ?></td>
            </tr>

<?php endforeach; else: ?>

            <tr class="jstatus"><td colspan="2">No Stats Available.</td></tr>

<?php endif; ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>