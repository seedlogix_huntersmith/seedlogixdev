<?php

$industries_list = Defaults::Industries();

?>

<select name="<?php echo $name; ?>" class="select2">
        <option value="">Choose Category...</option>
 <?php foreach($industries_list as $code => $category): ?>
        <option value="<?php echo $code; ?>"<?php $this->selected($code, $value); ?>><?php echo $category; ?></option>
 <?php endforeach; ?>
</select>