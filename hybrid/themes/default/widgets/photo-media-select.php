<?php

/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 * @var $this Template
 */
?>
        <div class="<?php echo $grid; ?> search-media">
                <div class="input ">
					<?php $this->getWidget('moxiemanager-filepicker.php', array(
						'name' => $name,
						'value' => $value,
						'idWidget' => $idWidget,
						'background' => $background,
						'path' => $storagePath,
						'noHost' => true,
						'extensions' => 'jpg,jpeg,png,gif',
						'display' => $display,
						'oninsert' => 'selectMedia_' . $idWidget,
						'disable' => $disable
					));?>
					<label class="note">Search your media library or upload new files.</label>
				</div>
            <div class="widget previews">
                <!-- <span>Upload: </span>   -->
                <ul class="results">
                        <li class="jhasuploader"><!-- <img src="<?php echo $_path; ?>/images/icons/middlenav/create.png"/> -->
<!--                            <input type="file" class="fileInput jnoIcon" id="media-photo" name="--><?php //echo $name; ?><!--" /></li>-->
                    <?php foreach($thumbs as $img): ?>
                        <li><img src="<?php echo MediaController::thumbnail_path . $img; ?>" alt="<?php $this->p($img); ?>" class="jthumb thumbsmall"></li>
                    <?php endforeach; ?>
                </ul>
                <div class="pagination">Go to 1 | 2 | 3 | 4 .. </div>
            </div>
        </div>

<script>
<?php $this->start_script();?>
function selectMedia_<?php echo $idWidget;?>(args){
	var form = $('#slideform_<?php echo $idWidget; ?>');
	var input = $('input[name="<?php echo $name?>"]', form);
	var newValue = args.files[0].url;
	input.val(args.files[0].url);
	new AjaxSaveDataRequest(input.get(0), newValue);

	var preview = $('.preview', form);
	if(preview.length == 0){
		preview = $('<img class="preview">');
		preview.appendTo($('.preview-container', form));
	}

	preview.attr('src', newValue);
	if($('#slidethumb_<?php echo $idWidget;?> img').length == 0){
		$('#slidethumb_<?php echo $idWidget;?>').removeClass('jnothumb');
		$('#slidethumb_<?php echo $idWidget;?> a').prepend('<img src="' + newValue + '" class="thumbsmall"/>');
	}
	$('#slidethumb_<?php echo $idWidget;?> img').attr('src', newValue);

	if($('#<?php echo $idWidget;?>-cnt img').length == 0){
		$('#<?php echo $idWidget;?>-cnt').prepend('<img src="' + newValue + '" class="preview"/>');
		$('#<?php echo $idWidget;?>-cnt').parent().append('<a href="#" id="<?php echo $idWidget;?>-clear">Clear</a>');
	}
}
<?php $this->end_script();?>
</script>