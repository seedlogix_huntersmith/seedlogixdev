<?php if($User->isSystem()): ?>

<div class="fluid">
<form action="<?= $this->action_url('DashboardBase_ModifyTOTAL?w=Subscriptions'); ?>" method="POST" class="ajax-save" onkeypress="return event.keyCode != 13;">
    <div class="widget grid6">
        <div class="whead"><h6>User Subscription Information</h6></div>
        <div class="formRow">
            <div class="grid12">
                <p>An Admin User can have 1 or more add-ons and subscriptions activated. These are all combined to produce a TOTAL.</p>
                <p>The TOTAL can be updated below, but it will be overwritten if the User modifies his add-ons/subscriptions.</p>
                <p>Set the expiration date to <strong class="red">00/00/0000</strong> to make the TOTAL permanent and prevent User changes.</p>
            </div>
        </div>

<?php if($userobj->isAdmin()): if(NULL == $user_limit_row): ?>

        <div class="formRow"><div class="grid12">First create a subscription for this User to create a TOTAL.</div></div>

<?php else: ?>

        <input type="hidden" name="subscription[ID]" value="<?= $user_limit_row->ID; ?>">

<?php foreach($user_limit_row as $key => $val): ?>

        <div class="formRow">
            <div class="grid3"><label><?= $key; ?>:</label></div>
            <div class="grid6">

<?php
switch($key):
    case 'limits_json':
        $json = str_replace(array(':',',','{','}'),array(': ',",\n\t","{\n\t","\n}"),$val);
?>

                <textarea rows="<?= substr_count($json,"\n") + 2; ?>" name="subscription[limits_json]"><?= $json; ?></textarea>
            </div>
            <div class="grid3">-1 = UNLIMITED</div>

<?php
        break;
    case 'expiration_date':
?>

                <input type="text" class="inlinedate" value="<?= $val != '0000-00-00' ? date('m/d/Y',strtotime($val)) : $val; ?>" size="10" name="subscription[expiration_date]" id="sq_subExpDate">
            </div>
            <div class="grid3"><button class="buttonS f_lt setDate" value="00/00/000" type="button">Never Expires</button></div>

<?php
        break;
    default:
?>

                <?= $val; ?>
            </div>

<?php endswitch; ?>

        </div>

<?php endforeach; endif; else: ?>

        <div class="formRow"><div class="grid12">This user is not an admin account. Please find this user's parent account to modify its subscription limits.</div></div>

<?php endif; ?>

    </div>
</form>
</div>

<?php endif; ?>