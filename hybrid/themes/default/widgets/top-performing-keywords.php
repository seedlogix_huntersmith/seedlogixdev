<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$has_keywords	=	count($TopPerformingKeywords);
?>

<div class="grid4"> 
  <div class="widget">
    <div class="whead"><h6>Top 5 Performing Keywords</h6></div>
    <table cellpadding="0" cellspacing="0" width="100%" class="tAlt">
      <thead>
        <tr>
          <td width="60">Visits</td>
          <td>Keyword</td>
        </tr>
      </thead>
      <tbody id="jkeywords">

<?php
if($has_keywords):
  foreach($TopPerformingKeywords as $keyword => $count){
    $this->getRow('keyword.php',array('keyword' => $keyword,'count' => $count));
  }
else:
?>

        <tr class="jstatus"><td colspan="2">No Stats Available.</td></tr>

<?php endif; ?>

      </tbody>
    </table>
  </div>
</div>