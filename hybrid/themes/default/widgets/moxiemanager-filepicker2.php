<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/21/15
 * Time: 2:57 PM
 * @var array $options
 * @var string $name
 * @var string $id
 * @var bool $disabled
 * @var string $value
 * @var bool $showPreview
 * @var bool $showClearButton
 * @var Template $this
 */
//Default options
$moxoptions = array();
$moxoptions['oninsert'] = '';
$moxoptions['title'] = 'File Manager';
$showPreview = true;
$showClearButton = true;
array_merge($moxoptions, $options);
?>


<div class="moxieWidget" id="<?="$id"?>">
	<input type="hidden" name="<?=$name?>"/>
	<button class="sideB bGreyish <?= $disabled ? "disabled" : ""?>" id="<?="$id-button"?>" <?=$disabled ? "disabled" : ""?>>Search file</button>
	<?php if($showPreview):?>
		<div class="preview">
			<?= !empty($value) ? "<img src='/" . UserModel::USER_DATA_DIR . $value . "' />" : "";?>
		</div>
	<?php endif;?>
	<?php if($showClearButton):?>
		<a href="#" id="clearImage">Clear</a>
	<?php endif;?>
</div>

<script>
	<?php $this->start_script()?>
	$(document).ready(function()){
		var
	}
	<?php $this->end_script()?>
</script>

	<script>
		<?php $this->start_script()?>
		$(document).ready(function(){

			var p = $("#<?= $idWidget; ?>").parent();

			$('#clear-image', p).click(function(e){
				var input = $('input[name="<?php echo $name; ?>"');
				e.preventDefault();
				$('.preview img', p).remove();
				input.val("");
				new AjaxSaveDataRequest(input.get(0), "");
			});

			$("#<?php echo $idWidget;?>").click(function(event){
				event.preventDefault();
				moxman.browse({
					title : 'File Manager', fu: 'bar',
					<?php echo isset($noHost) ? 'no_host :' . ($noHost ? 'true' : 'false') . ',' : '';?>
					<?php echo isset($path) ?  'path: "/' . $path . '",' : '' ?>
					<?php echo isset($extensions) ?  'extensions: "' . $extensions . '",' : '' ?>
					<?php if(isset($oninsert)):
						   echo 'oninsert: ' . $oninsert . ',';
						 else:
						?>
					oninsert: function (args){
						var path = args.files[0].url;
						var url = "/<?php echo UserModel::USER_DATA_DIR; ?>" + path;
						var input = $('input[name="<?php echo $name; ?>"]');
						input.val(path);
						new AjaxSaveDataRequest(input.get(0), path);
						var preview = $('.preview img', p);
						if(preview.length == 0){
							p.append('<img class="preview" src="' + url + '">');
						}else{
							preview.attr('src', url);
						}
					} <?php

			     endif;
             ?>
					<?php echo isset($inputname) ?  'oninsert: updateImageFunction(' . implode(',', $inputname) . '),' : '' ?>
					<?php echo isset($onclose) ?  'onclose: ' . $onclose. ',' : '' ?>
					<?php echo isset($onopen) ?  'onclose: ' . $onopen. ',' : '' ?>
				});
			});

		});
		<?php $this->end_script()?>
	</script>
<?php else:?>
	<span>No data</span>
<?php endif;?>