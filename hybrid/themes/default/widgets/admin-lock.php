<?php
if(!$multi_user) return;

$dataActionUrl	= isset($ajax_url) ? ' data-action-url='.$ajax_url : '';
$model			= isset($model) ? $model : 'website';

if($locks->isLocked($fieldname)):
?>

			<div class="bDefault<?= $this->isAction('theme') && $fieldname == 'theme' ? ' no_w_head' : ''; ?> f_rt adminLock" title="Unlock">
				<span class="lock"></span>
				<input type="checkbox" value="0" name="<?= "{$model}[lock_$fieldname]"; ?>" checked<?= $dataActionUrl; ?> class="hide">
			</div>
<?php else: ?>
			<div class="bDefault<?= $this->isAction('theme') && $fieldname == 'theme' ? ' no_w_head' : ''; ?> f_rt adminLock" title="Lock">
				<span class="unlock"></span>
				<input type="checkbox" value="1" name="<?= "{$model}[lock_$fieldname]"; ?>" checked<?= $dataActionUrl; ?> class="hide">
			</div>
<?php endif; ?>