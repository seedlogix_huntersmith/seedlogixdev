<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 1/9/2015
 * Time: 12:04 AM
 */
if(!$SelectedRoles){
    $SelectedRoles = array('0');
}

if($User->isAdmin()): ?>
    <div class="widget grid6">
        <div class="whead"><h6>Website Roles</h6></div>
            <form action="<?php echo $this->action_url('website_save?ID=' . $site->getID()); ?>" method="POST" class="ajax-save">
                <?php foreach ($Roles as $QubeRole): ?>
                    <div class="formRow">

                        <div class="grid3"><label><?php if($User->can('is_system')):
                                    $this->p ($QubeRole->company . ' / ');
                                endif;

                                $this->p($QubeRole->role); ?>:</label></div>
                        <div class="grid9 on_off">
                            <div class="floatL mr10"><input type="checkbox" name="role_website[role][<?= $QubeRole->ID; ?>]" value="1" <?php $this->checked((in_array($QubeRole->ID,$SelectedRoles)) ? 1 : 0, 1); ?>/></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </form>
    </div>
<?php endif; ?>