<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/* @var $Reseller ResellerModel */
?>

<?php if(!$Reseller->hasFacebookApp()): ?>
    <a href="#"> Click Here to Configure your Facebook App</a>
<?php else: 
        // load authorizations
    ?>
    
    <iframe src="about:blank" id="jFB_IFRAME" class="hide"></iframe>
    <a href="<?php echo $Reseller->getFacebookAuthorizerForURL($action_url); ?>" id="jFB_SELECT">Select Facebook Page</a>    

<?php endif; ?>

<script><?php

    $this->start_script();
?>
    $(function (){
        $('#jFB_SELECTx').click(function (){
            $('#jFB_IFRAME').show().attr('src', "");
        });
    });


<?php $this->end_script(); ?></script>