<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/24/14
 * Time: 9:13 AM
 * @var $this Template
 * @var $subscriptions SubscriptionModel[]
 */
?>

<div class="widget grid12">
    <div class="whead">
        <h6>Subscriptions</h6>

<?php if($w->createSubscriptionsButton): ?>

        <div class="buttonS f_rt jopendialog jopen-createsubscription change"><span>Change a Subscription</span></div>
        <div class="buttonS f_rt jopendialog jopen-createsubscription create"><span>Add a Subscription</span></div>

<?php endif; ?>

    </div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('AccountLimits'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'accounts-datatable',
	'data'				=> $w->getFirstPageAccountLimits(),
	'rowkey'			=> 'limit',
	'rowtemplate'		=> 'system-account-limits.php',
	'total'				=> $w->getTotalAccountLimits(),
	'sourceurl'			=> $this->action_url('DashboardBase_Subscriptions',array('w' => 'Subscriptions')),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'params'
));
?>

</div>

<?php if($w->createSubscriptionsButton):?>
    <div class="SQmodalWindowContainer dialog" title="Manage a Subscription" id="jdialog-createsubscription" style="display: none;">
        <form id="createCampForm" action="<?= $this->action_url('DashboardBase_Create',array('w' => 'Subscriptions')); ?>">
            <input type="hidden" name="subscription[parent_id]" id="parent_id" value="<?= $userobj->getParentID(); ?>">
            <fieldset>
                <div class="fluid spacing">
                    <div class="grid4"><label>Subscription:</label></div>
                    <div class="grid8">
                        <select data-placeholder="Choose a Subscription&hellip;" class="select2" name="subscription[subscription_id]">
                            <option></option>
<?php foreach($subscriptions as $subscription): ?>
                            <option value="<?= $subscription->getID()?>"><?=$subscription->name?></option>
<?php endforeach; ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="fluid spacing">
                    <div class="grid4"><label>Billing:</label></div>
                    <div class="grid8"><input type="text" name="subscription[billing_id]" id="billing_id" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
                    <div class="clear"></div>
                </div>
                <div class="fluid spacing">
                    <div class="grid4"><label>Expiration Date:</label></div>
                    <div class="grid8"><input type="text" name="subscription[expiration_date]" id="expiration_date" class="text ui-widget-content ui-corner-all inlinedate" style="margin: 0;"></div>
                    <div class="clear"></div>
                </div>
            </fieldset>
        </form>
    </div>
<?php endif; ?>
<?php if(isset($w->serverParams)): ?>
<script>
    <?php $this->start_script(); ?>
        function params(aoData){
            <?php foreach($w->serverParams as $name => $value): ?>
            aoData.push({name: "<?= $name; ?>",value: <?= $parent_id; ?>});
            <?php endforeach; ?>
        }


    <?php $this->end_script(); ?>
    <?php if($w->createSubscriptionsButton): ?>
    $('.jopendialog.jopen-createsubscription.change').click(function(){
        var dialog = $('#jdialog-createsubscription form');
        dialog.attr("action","<?= $this->action_url('DashboardBase_Change',array('w' => 'Subscriptions')); ?>");
        $('.ui-dialog-title').text('Change a Subscription');
        $('.ui-dialog-buttonpane button').eq(1).text('Change');
    });

    $('.jopendialog.jopen-createsubscription.create').click(function(){
        var dialog = $('#jdialog-createsubscription form');
        dialog.attr("action","<?= $this->action_url('DashboardBase_Create',array('w' => 'Subscriptions')); ?>");
        $('.ui-dialog-title').text('Add a Subscription');
        $('.ui-dialog-buttonpane button').eq(1).text('Add');
    });

    //===== Create Form PopUp =====//
    $('#jdialog-createsubscription').dialog(
        {
            autoOpen: false,
            height: 250,
            width: 500,
            modal: true,
            open: function(){
                var stringDate = "<?= PointsSetBuilder::firstDayNextMonth(new DateTime())->format(PointsSetBuilder::MYSQLFORMAT); ?>";
                $('#expiration_date').val(stringDate);
            },
            close: function ()
            {
                $('#campaign_id',this).val('');
                $('form',this)[0].reset();
            },
            buttons: (new CreateDialog('Manage',
                function (ajax,status)
                {

                    if(!ajax.error)
                    {
                        notie.alert({text:ajax.message});
                        this.close();
                        this.form[0].reset();
                    }
                    else if(ajax.error == 'validation')
                    {
                        alert(getValidationMessages(ajax.validation).join('\n'));
                    }
                    else
                        alert(ajax.message);

					var dt = $('#accounts-datatable');
					var newRowsIds = dt.data('newRowsIds');
					newRowsIds.unshift(ajax.ID);
					dt.data('newRowsIds', newRowsIds);
					dt.dataTable().fnDraw();

                },'#jdialog-createsubscription')).buttons
        });
    <?php endif; ?>

</script>
<?php endif; ?>