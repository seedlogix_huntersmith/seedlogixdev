<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 4/25/15
 * Time: 11:05 AM
 * @var $this Template
 * @var $parent_hub_lock HubFieldLocks
 * @var $can_edit_site boolean
 * @var $name string
 * @var $id string
 * @var $class string
 * @var $type string
 * @var $value string
 * @var $widget string
 * @var $widgetParams array
 * @var $maxGrid int
 * @var $placeholder string
 * @var $noLock boolean
 * @var $ajaxUrl
 * @var $model
 */

if($model == 'slide'){
	$fieldname			= "pic_{$slide->internalname}_$name";
} else{
	$fieldname			= $name;
}

$model					= $model ? $model : 'website';
$grid					= $maxGrid ? $maxGrid : 9;
$gridLength				= $multi_user && !$noLock ? 'grid'.($grid - 1) : 'grid'.$grid;
$disable				= $parent_hub_lock && $parent_hub_lock->isLocked($fieldname) || !$can_edit_site ? ' disabled' : '';

if(!isset($widgetParams)){
	$widgetParams		= array();
}
?>

<?php if(!$whead): ?>

<div class="<?= $gridLength; ?>">

<?php endif; ?>

<?php
if($widget):
	$this->getWidget($widget,array_merge(array(
		'name'			=> "{$model}[$name]",
		'class'			=> $class,
		'value'			=> $value,
		'disable'		=> $disable
	),$widgetParams));
elseif($control):
	$this->getControl($control,array_merge(array(
		'name'			=> "{$model}[$name]",
		'class'			=> $class,
		'value'			=> $value,
		'disable'		=> $disable
	),$widgetParams));
elseif($type == 'checkbox'):// CHECKBOX
?>

<?php if($whead): ?>

<div class="f_rt">

<?php endif; ?>

	<input type="checkbox" name="<?= "{$model}[$name]"; ?>" class="i_button<?= $class ? ' '.$class : ''; ?>" value="1"<?php $this->checked($value,1); ?><?= $disable; ?>>

<?php if($whead): ?>

</div>

<?php endif; ?>

<?php elseif($type == 'select'):// SELECT ?>

	<select name="<?= "{$model}[$name]"; ?>" id="<?= $id; ?>" class="select2" data-minimum-results-for-search="Infinity"<?= $disable; ?>>

<?php foreach($options as $option => $optionValue): ?>

		<option value="<?= $optionValue; ?>"<?php $this->selected($optionValue,$value); ?>><?= $option; ?></option>

<?php endforeach; ?>

	</select>

<?php else:// INPUT ?>

	<input<?= $id ? ' id="'.$id.'"' : ''; ?> type="<?= $type ? $type : 'text'; ?>" class="<?= $class; ?>" name="<?= "{$model}[$name]"; ?>"<?= $disable; ?> value="<?php $this->p($value); ?>" placeholder="<?= $placeholder; ?>">

<?php endif; ?>

<?php if(!$whead): ?>

</div>

<?php endif; ?>

<?php
if(!$noLock){
	$this->getWidget('admin-lock.php',array(
		'fieldname'		=> $fieldname,
		'ajax_url'		=> $ajaxUrl,
		'model'			=> $lockModel
	));
}
?>