<?php
/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 *
 *@var $this Template
 *@var $slide SlideModel
 */
if(!isset($_SESSION['RF']))
{
//	$_SESSION['RF']['HYBRIDHOST']	=	'hybrid';
}

if(!function_exists('slideCounter')){
    function slideCounter(){
		static $i = 0;
		return ++$i;
	}
}
?>
<form id="slideform_<?php echo $slide->ID; ?>" action="<?php echo $this->action_url('Website_saveslide?ID=' . $site->id . '&slide=' . $slide->ID); ?>" class="ajax-save" enctype="multipart/form-data" method="POST">
<a id="slideanchor_<?php echo $slide->ID; ?>"></a>
					<!-- section -->
	<div class="fluid">
		<div class="widget grid12">

			<div class="whead">
				<h6>Slide <?= slideCounter(); ?></h6>

<?php
$this->getWidget('admin-lock.php',array(
	'fieldname'		=> "pic_{$slide->internalname}_urlenabled",
	'ajax_url'		=> $this->action_url('Campaign_save')
));
?>

<?php
$this->getWidget('input-website.php',array(
	'model'			=> 'slide',
	'name'			=> 'urlenabled',
	'type'			=> 'checkbox',
	'value'			=> $slide->urlenabled,
	'class'			=> '',
	'noLock'		=> true,
	'slide'			=> $slide,
	'whead'			=> true
));
?>

			</div>
			<?php if($site->touchpoint_type!='PROFILE'){ ?>
			<div class="formRow">
				<div class="grid2"><label>Slide Title:</label></div>
				<?php $this->getWidget('input-website.php', array('model' => 'slide', 'name' => 'title', 'value' => $slide->title, 'slide' => $slide, 'ajaxUrl' => $this->action_url('Campaign_save'))); ?>
			</div>
			<?php } ?>
			<div class="formRow">
				<div class="grid2"><label>Slide Background:</label></div>
				<?php $this->getWidget('input-website.php', array(
					'slide' => $slide,
					'model' => 'slide',
					'name' => 'background',
					'value' => $slide->background,
					'widget' => 'photo-media-select.php',
					'ajaxUrl' => $this->action_url('Campaign_save'),
					'widgetParams' => array(
						'idWidget' => $slide->ID,
						'background' => $slide->background,
						'path' => $storagePath
					)));?>
			</div>
			<?php if($site->touchpoint_type!='PROFILE'){ ?>
			<div class="formRow">
				<div class="grid2"><label>Link:</label></div>
				<?php $this->getWidget('input-website.php', array('model' => 'slide', 'name' => 'url', 'value' => $slide->url, 'slide' => $slide, 'ajaxUrl' => $this->action_url('Campaign_save'))); ?>
			</div>

			<div class="formRow">
				<div class="grid2"><label>Slide Description:</label></div>
					<div class="j-dual-ide" style="position: relative;">
						<?php $this->getWidget('input-website.php', array(
							'slide' => $slide,
							'model' => 'slide',
							'name' => 'description',
							'ajaxUrl' => $this->action_url('Campaign_save'),
							'widget' => 'wysiwyg-ide.php',
							'maxGrid' => 10,
							'widgetParams' => array(
								'fieldname' => 'slide[description]',
								'fieldvalue' => $slide->description
							))); ?>
					</div>
			</div>
			<?php } ?>
		    <div class="formRow"><a title="Delete the Slide" id="jnewslidebtn" class="bDefault f_rt<?= $disable; ?>" href="<?= $this->action_url("{$site->touchpoint_type}_deleteslide?ID=".$site->id.'&slide='.$slide->ID); ?>"><span class="delete"></span></a></div>
		</div><!-- //section -->
	</div><!-- //section -->
	<input type="hidden" value="<?=$site->touchpoint_type?>" name="website[touchpoint_type]">
	<input type="hidden" value="<?=$site->getID()?>" name="website[id]">
</form>