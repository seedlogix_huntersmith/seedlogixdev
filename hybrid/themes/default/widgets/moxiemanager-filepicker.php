<?php
/**
 * User: Eric
 * Date: 8/11/2014
 * Time: 10:46 AM
 * @var $this Template

    Button      = <?=$idWidget?>
    Input       = <?=$idWidget?>-input
    pContainer  = <?=$idWidget?>-cnt
    pImage      = <?=$idWidget?>-img
    clear       = <?=$idWidget?>-clear
 */
?>

<?php # @todo do we really want this to say no data if this is false?
if(!isset($display) || $display): ?>
    <div class="input">
        <button class="buttonS<?= $disable ? ' disabled' : ''; ?>" id="<?= $idWidget; ?>"<?= $disable ? ' disabled' : ''; ?>>Search File</button>
        <input type="hidden" name="<?= $name; ?>" value="<?php $this->p($value); ?>" id="<?= $idWidget; ?>-input" class="not-global">
        <?php if($type=='audio'){
		$player = '<br><div class="audio-con"><audio
        controls
        src="https://'.$_SERVER['SERVER_NAME'].'/users'.$value.'">
            Your browser does not support the
            <code>audio</code> element. <a href="https://'.$_SERVER['SERVER_NAME'].'/users'.$value.'">Listen Now</a>
    </audio></div>';
		?>
		<div class="preview-cnt" id="<?= $idWidget; ?>-cnt"><?= !empty($value) ? $player : ''; ?></div>
		<?php } else { ?>
		<div class="preview-cnt" id="<?= $idWidget; ?>-cnt"><?= !empty($value) ? '<img id="'.$idWidget.'-img" class="preview" src="'.UserModel::USER_DATA_DIR.$value.'">' : ''; ?></div>
		<?php } ?>

        <?php
            if(empty($disable)):
                if(empty($value))
                    $displayClear = ' style="display: none;"'; ?>
        <a href="#" id="<?= $idWidget; ?>-clear"<?= $displayClear; ?>>Clear</a>
        <?php endif; ?>
    </div>

<script type="text/javascript">
<?php $this->start_script()?>
$(document).ready(function(){

    
    $('#<?=$idWidget?>-clear').click(function(event){
        $('#<?=$idWidget?>-img').remove();
        $('#<?=$idWidget?>-input').val('');
        new AjaxSaveDataRequest($('#<?=$idWidget?>-input')[0], '');
		$('.audio-con').remove();
        $('.audio-con').val('');
		$('.audio-con').hide('');
        $(this).fadeOut('fast');
        event.preventDefault();
        return false;
    });

    $('#<?=$idWidget?>').click(function(event){
		 
		
		moxman.browse({
            title : 'File Manager', fu: 'bar',
			<?php echo isset($noHost) ? 'no_host :' . ($noHost ? 'true' : 'false') . ',' : '';?>
			<?php echo isset($path) ?  'path: "/' . $path . '",' : '' ?>
			<?php echo isset($extensions) ?  'extensions: "' . $extensions . '",' : '' ?>
<?php
        if(isset($oninsert)):
			 echo 'oninsert: ' . $oninsert . ',';
        else:
?>
            oninsert: function (args)
            {
                var path = args.files[0].url.replace(/^\/<?= addslashes(UserModel::USER_DATA_DIR); ?>/, '');
                var url = "/<?=UserModel::USER_DATA_DIR?>" + path;
                var imgurl = '<?php echo $this->action_url('UserManager_updateupload?ID=' . $User->getID()); ?>&inputname=<?= $name; ?>&rurl=' + path;
                var type = '<?=$type?>';
				$('#<?=$idWidget?>-input').val(path);
					new AjaxSaveDataRequest($('#<?=$idWidget?>-input')[0], path);
				
				if(type == 'audio'){
					$('.audio-con').remove();
					$('.audio-con').val('');
					$('.audio-con').hide('');
					$('#<?=$idWidget?>-cnt').append('<br><div class="audio-con"><audio controls src="https://<?=$_SERVER['SERVER_NAME']?>/users'+ path +'">Your browser does not support the <code>audio</code> element. <a href="https://<?=$_SERVER['SERVER_NAME']?>/users'+ path +'">Listen Now</a></audio></div>');
					
				} else {
					
					if($('#<?=$idWidget?>-img').length == 0) {
						$('#<?=$idWidget?>-cnt').append('<img class="preview" id="<?=$idWidget?>-img" src="' + imgurl + '">');
					}else {
						$('#<?=$idWidget?>-img').attr('src', imgurl);
					}
					
				}
                $('#<?=$idWidget?>-clear').show();
            } 
<?php   endif; ?>
			<?php echo isset($inputname) ?  'oninsert: updateImageFunction(' . implode(',', $inputname) . '),' : '' ?>
			<?php echo isset($onclose) ?  'onclose: ' . $onclose. ',' : '' ?>
			<?php echo isset($onopen) ?  'onclose: ' . $onopen. ',' : '' ?>
		});
        event.preventDefault();
	});
});
<?php $this->end_script()?>
</script>
<?php else:?>
	<span>No data</span>
<?php endif;?>