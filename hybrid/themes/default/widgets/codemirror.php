<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 4/28/15
 * Time: 1:59 PM
 *
 * @var $this Template
 * @var $id string
 */
?>

<textarea id="<?=$id?>" class="codemirror" name="<?=$name?>" <?=$disable?>><?php $this->p($value); ?></textarea>

<?php $this->start_script();?>
$(document).ready(function(){
var <?=$id?> = document.getElementById("<?=$id?>");
CodeMirror.fromTextArea(<?=$id?>, {
	lineNumbers: true,
	lineWrapping: true,
	mode: "css",
	styleActiveLine: true,
	viewportMargin: Infinity,
	readOnly: $(<?=$id?>).attr('disabled') === "disabled"
})
<?php if(!$disable): ?>
	.on('blur', function(cm, ev){
	var ta    =   cm.getTextArea();
	var srccode   =    cm.getValue();

	new AjaxSaveDataRequest(ta, srccode);
});
<?php endif;?>
});
<?php $this->end_script();?>