<?php
/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 *
 * @var $serverData string The javascript function's name that act as a callback when DataTable refresh
 */

/* requires:
 *
 *  $columns
 *  $class
 *  $data
 *  $rowtemplate => row.php
 *  $rowkey => 'user'
 *  $rowvar => array()
 *  $total
 *  $sourceurl
 *  $DOMTable
 */

$website = new WebsiteModel();

$website->atributo;

if(!isset($rowvars)){
    $rowvars = array();
}
?>

<table id="<?= $id; ?>" class="tDefault dTable<?= $class ? ' '.trim(preg_replace('/\s+/',' ',str_replace(',',' ',$class))) : ''; ?>" style="width: 100%;">
    <thead class="dataTableHead">
        <tr>

<?php foreach($columns as $col => $info): ?>

            <th><?= $col; ?></th>

<?php endforeach; ?>

        </tr>
    </thead>

<?php if(isset($footer)) $this->getRow($footer['template'],array($footer['rowKey'] => $footer['data'])); ?>

    <tbody class="dataTableBody">

<?php
foreach($data as $i => $row){
    $this->getRow($rowtemplate,array($rowkey => $row) + $rowvars);
}
?>

    </tbody>
</table> 

<script>
<?php $this->start_script(); ?>
$(function(){
    var columns     = [];
    var aaSorting   = [];

<?php foreach($columns as $name => $col): ?>

    columns.push({
        "sTitle"        : "<?= isset($col['Title']) ? $col['Title'] : $name; ?>",
        "sName"         : "<?= $name; ?>",
        "bSortable"     : <?= $col['Sortable'] == true ? 'true' : 'false'; ?>,
        "orderSequence" : ["desc","asc"]
    });

<?php if(isset($col['Sort'])): ?>

    aaSorting.push([<?= array_search($name,array_keys($columns)); ?>,'<?= $col['Sort']; ?>']);

<?php endif; endforeach; ?>

    if(aaSorting.length == 0){
        aaSorting.push([0,'desc']);
    }
    $('#<?= $id; ?>').data('newRowsIds',[]);
    $('#<?= $id; ?>').dataTable({
        "bDeferRender"      : true,
        "aLengthMenu"       : [5,10,25,50,75,100],
        "language"          : {
                            "search"            : "_INPUT_",
                            "searchPlaceholder" : "Search…"
                            },
        /*"autoWidth"         : false,*/
        "pagingType"        : "simple",
        "bProcessing"       : true,
        "bServerSide"       : true,
        "sServerMethod"     : "POST",
        "iDeferLoading"     : <?= $total ? $total : (empty($data) ? 0 : max(count($data),10)); ?>,
        "iDisplayLength"    : 10,
        "bDestroy"          : true,
        "aoColumns"         : columns,
        "aaSorting"         : aaSorting,
        "ajax"              : {
                            "url"               : "<?= $sourceurl; ?>",
                            "type"              : "POST",
                            "data"              : function(d){
                                                d.newRowsIds = $('#<?= $id; ?>').data('newRowsIds');

<?php if($serverParams): ?>

                                                var sentParams = [];
                                                <?= "$serverParams(sentParams);" ?>
                                                $.each(sentParams,function(i,param){
                                                    d[param.name] = param.value;
                                                });

<?php endif; ?>

                                                }
                            },

<?php if(isset($serverData)): ?>

        "fnServerData"      : function(sSource,aoData,fnCallback,oSettings){
                            oSettings.jqXHR = $.ajax({
                                "dataType"      : "json",
                                "type"          : "POST",
                                "url"           : this.DataTable().ajax.url(),
                                "data"          : aoData,
                                "success"       : <?= $serverData; ?>(fnCallback)
                            });
        },

<?php endif; ?>

        "sDom"              : '<?= isset($DOMTable) ? '<"tablePars"lf>rt<"fg-toolbar tableFooter"ip>' : "lfrtip"; ?>'

<?php if(isset($params)): ?>

        ,"fnServerParams"   : function(aoData){
                            var sortInfo = this.dataTable().fnSettings().aaSorting[0];
                            var obj = Dashboard.setIntPer({
                                chart           : "TableCompareChartsData",
                                sortCol         : sortInfo[0],
                                sortDir         : sortInfo[1]
                            },$('.graph_rangectrl'));
                            Dashboard.context.sortDir =	obj.sortDir;
                            Dashboard.context.sortCol =	obj.sortCol;
                            Dashboard.updateExportURL(Dashboard.context);

<?php foreach($params as $name => $param): ?>

                            aoData.push({'name':'<?= $name; ?>','value':'<?= $param; ?>'});

<?php endforeach; ?>

                            for(var name in obj){
                                aoData.push({'name':name,'value':obj[name]});
                            }
        }

<?php endif; if(isset($serverParams)): ?>

        ,"fnServerParams"   : <?= $serverParams; ?>

<?php endif; ?>

    });
});
<?php $this->end_script(); ?>
</script>