<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
<div class="widget tabsAndTables">
	<div class="whead tabs">
		<ul>
			<li><a href="#tabs-1">All Listings</a></li>
			<li><a href="#tabs-2">Directories</a></li>
			<li><a href="#tabs-3">Press Releases</a></li>
			<li><a href="#tabs-4">Articles</a></li>
		</ul>
		<div class="BntWithTabs">
			<div class="buttonS bGreen right dynRight jopendialog jopen-createlisting">Create Network</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="dyn">
<?php
            $this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('NetworksCampaign'),
					'class' => '',
					'id' => 'jnetw-articles',
					'data' => $w->articles,
					'rowkey' => 'item',
					'rowtemplate' => 'network-any.php',
					'total' => $w->total_users,
					'sourceurl' => $this->action_url('campaign_ajaxNetworks?ID=' . $w->campaignID),
                                        'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);?>
<!--		<table class="tDefault dTable" id="jnetw-articles">
			<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Date</th>
					<th width="100">Actions</th>
				</tr>
			</thead>
			<tbody class="dataTableBody _CLASS_"><?php foreach ($w->articles as $networkitem): ?>
					<?php $this->getRow('network-any.php', array('item' => $networkitem)); ?>
				<?php endforeach; ?>
			</tbody>
		</table>-->
	</div>
</div><!-- /end widget -->

<script>
	
    $(function (){
//        $('#jnetw-articles').dataTable(
//     {
//        "bProcessing": true,
//        "bServerSide": true,
//        "sServerMethod": "POST",
//        "iDeferLoading": <?php echo $total_users; ?>,
//        "sAjaxSource": "<?php echo $this->action_url('UserManager_ajaxList?status=' . htmlentities(@$user_status, ENT_QUOTES)); ?>"   //scripts/server_processing.php"
//    });
    $('.juser-create').submit(function (){
        Dashboard.ajaxSubmitForm($(this), function (ajax){
            if(!ajax.error){
                $('.juser-tbl').prepend(ajax.row);
				notie.alert({text:'A New User has been created.'});
                return;
            }
            if(ajax.error   ==  'validation'){
                alert(getValidationMessages(ajax.validation).join("\n"));
            }else
                alert(ajax.message);
        });
        return false;
    }); });
    
		</script>