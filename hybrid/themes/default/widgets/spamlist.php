<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

                <div class="widget">
                    <div class="whead"><h6>Spam Messages</h6><div class="clear"></div></div>
                    <ul class="updates"><?php foreach ($messages as $spam): ?>
                        <li class="jmsg jmsgid-<?php echo $spam->id; ?>">
                    <a href="javascript:void(approveMessage(<?php echo $spam->id; ?>));" class="tablectrl_small bGreen tipS" title="Approve" style="margin-left: 10px;">
                            <span class="iconb" data-icon="&#xe134;"></span>
                    </a>
                    <a href="javascript:void(deleteMessage(<?php echo $spam->id; ?>));" class="tablectrl_small bRed tipS" title="Delete">
                            <span class="iconb" data-icon="&#xe136;"></span>
                    </a>
                    
                    <span style="float:left; width: 50%">From: <a href="javascript:void(loadMessage(<?php echo $spam->id; ?>));" title=""><?php echo $spam->name; ?></a>
                    &lt;<?php echo $spam->email; ?>&gt; <br />
                    <?php
                    if($spam->user_fullname && $User->can('is_support')):
                ?>To: <a href="#"><?php echo $spam->user_fullname; ?></a> - 
                <?php
                    endif; ?></span>
                            <span class="uDate"><span><?php echo $spam->day; ?></span><?php echo $spam->monthname; ?></span>
                            
        <div class="messagesOne">
            <div class="by_user" style="padding:6px;">
                    <!--<div class="messageArea">-->
                        <span class="aro"></span>
                        <div class="jmsg-comments"><?php $this->p($spam->comments); ?> 
                                    <?php if($spam->is_summary): ?>
                                    <a href="javascript:void(loadMessage(<?php echo $spam->id; ?>));">... </a>
                                    <?php endif; ?></div>
                    <!--</div>-->
        </div>
        </div>
                            
                            <span class="clear"></span>
                        </li><?php endforeach; ?>
                        <li class="jno-messages center<?= $has_messages ? ' hide' : ''; ?>">No Spam Messages :)</li>
                    </ul>
                </div>