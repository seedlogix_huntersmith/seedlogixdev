<?php if($User->isAdmin()): ?>

    <div class="widget grid6">
        <div class="whead"><h6>User Roles</h6></div>

<?php if($userobj->isAdmin()): ?>

        <div class="formRow"><div class="grid12"><p>This User is an Account Administrator and cannot be assigned any Roles.</p></div></div>

<?php else: ?>

        <form action="<?= $this->action_url('UserManager_saverole?ID='.$userobj->getID()); ?>" method="POST" class="ajax-save">

<?php foreach($Roles as $QubeRole): ?>

            <div class="formRow">
                <div class="grid6"><label><?= ($User->can('is_system') ? $this->p($QubeRole->company.' / ') : '').$this->p($QubeRole->role); ?>:</label></div>
                <div class="grid6"><input type="radio" name="user[role]" class="i_button" value="<?= $QubeRole->ID; ?>"<?php $this->checked($QubeRole->ID == $UserRole,1); ?>></div>
            </div>

<?php endforeach; ?>

        </form>

<?php endif; ?>

    </div>

<?php endif; ?>