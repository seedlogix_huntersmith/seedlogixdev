<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

	<div class="widget check">
		<div class="whead">
			<h6><?= $Branding->company; ?> Users</h6>
			<div class="buttonS f_rt jopendialog jopen-createUser"><span>Create a New User</span></div>
            <select data-placeholder="Filter By Role&hellip;" class="select2" name="filterByRole" id="newrole">
                <option></option>

<?php foreach($role_list as $role_id=>$role): ?>

                <option value="<?= $role_id; ?>"><?= $role; ?></option>

<? endforeach; ?>

            </select>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Userlist'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'jusers',
	'data'				=> $w->userlist,
	'rowkey'			=> 'nuser',
	'rowtemplate'		=> 'user.php',
	'total'				=> $w->total_users,
	'sourceurl'			=> $this->action_url('DashboardBase_UserList',array('w' => 'UserList')),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'setParams'
));
?>

	</div>

<!-- Dynamic Data Modal -->
<?php $this->getDialog('user-details.php'); ?>

<!-- Create Form POPUP -->
<?php $this->getDialog('create-user.php'); ?>

<script>
<?php $this->start_script(); ?>

$(function (){

		//populating and displaying details view modal
		var detailCompany 	= $('#forCompany');
		var detailPhone 	= $('#forPhone');
		var detailAddress 	= $('#forAddress');
		var detailProduct 	= $('#forProduct');

		//===== Create New Form + PopUp =====//
		$( '#viewDetails' ).dialog(
		{
			autoOpen: false,
			height: 300,
			width: 550,
			modal: true,
			close: function( event, ui ) {
				//clear data on close
				detailCompany.html('');
				detailPhone.html('');
				detailAddress.html('');
				detailProduct.html('');
			}
		});

		//populate modal data then show modal
		$('.viewDetails').click(function(){

			var company = $(this).find('.forCompany').html();
			var phone 	= $(this).find('.forPhone').html();
			var address = $(this).find('.forAddress').html();
			var product = $(this).find('.forProduct').html();

			detailCompany.html(company);
			detailPhone.html(phone);
			detailAddress.html(address);
			detailProduct.html(product);

			$( '#viewDetails' ).dialog( "open" );

		});


		//create user form
		$('.juser-create').submit(function (){
			Dashboard.ajaxSubmitForm($(this), function (ajax){
				if(!ajax.error){
					$('.juser-tbl').prepend(ajax.row);
					notie.alert({text:'A New User has been created.'});
					return;
				}
				if(ajax.error   ==  'validation'){
					alert(getValidationMessages(ajax.validation).join("\n"));
				}else
					alert(ajax.message);
			});
			return false;
		});


		//===== Create New Form + PopUp =====//
		$('#jdialog-createUser').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function ()
			{
				$('#campaign_id',this).val('');
				$('form',this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					if(!ajax.error)
					{
					  	$('.jnoforms').hide();
					  	$('.juser-tbl').prepend(ajax.row);
						var dt = $('#jusers');
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.id);
						dt.data('newRowsIds', newRowsIds);
						dt.dataTable().fnSort( [ [3, 'desc'] ] );
						notie.alert({text:ajax.message});
					  	this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
						alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
					  alert(ajax.message);

				}, '#jdialog-createUser')).buttons
		});

    $('#newrole').change(function(){
        $('.dTable').dataTable().fnDraw();
    });

	});
function setParams(aoData){
	<?php if(isset($w->serverParams)):?>
	<?php foreach($w->serverParams as $name => $value): ?>
	aoData.push({name: "<?=$name?>", value: "<?=$value?>"});
	<?php endforeach;?>
	<?php endif; ?>
    var role = $('select[name="filterByRole"]').val();
    aoData.push({name : "role", value : role});
}
<?php $this->end_script(); ?>
</script>