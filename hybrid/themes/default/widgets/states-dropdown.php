<?php

$state_list = Defaults::getStates();

?>

<select name="<?php echo $name; ?>" class="select2">
        <option value="">Choose State...</option>
 <?php foreach($state_list as $code => $statename): ?>
        <option value="<?php echo $code; ?>"<?php $this->selected($code, $value); ?>><?php echo $statename; ?></option>
 <?php endforeach; ?>
</select>