<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$options        = array(
                //'7-DAY'     => 'Last 7 Days',
                '30-DAY'    => 'Last 30 Days',
                //'6-MONTH'   => 'Last 6 Months',
                //'12-MONTH'  => 'Last 12 Months',
                //'0-ALL'     => 'All Time'
);
?>

<div class="graph_range_ctrl" style="display:none;">
    <select id="<?= $id; ?>" class="graph_rangectrl">

<?php foreach($options as $key => $text): ?>

        <option value="<?= $key; ?>"<?php $this->selected($key,$value); ?>><?= $text; ?></option>

<?php endforeach; ?>

    </select>
</div>