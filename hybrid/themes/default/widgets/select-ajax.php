<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 4/6/15
 * Time: 10:51 AM
 * @var $this Template
 *
 * Widget's options
 * @var $id int 			The id of the select
 * @var $name string 		Input's name
 * @var $ajaxUrl string 	The URL where the data is requested
 * @var $field_value string The object field's name that will be used as value on options tag
 * @var $text string 		The row's template that will display in dropdown using this format :field: Example: ":name: (:email:)"
 * @var $multiple bool 		Set to true if is going to use multiple values
 * @var $placeholder string The string to be used as placeholder
 * @var $value array|object	The default value preselected, can be an object or an array of objects if multiple option is set to true
 * @var $data array			Data preloaded in select
 * @var $params mixed		Extra params sent on ajax request, can be a value, array or object
 * @var $style array		Optional style css array
 */

//$style = array_to_css_style($style,array('width' => '100%'));

if(is_array($value)){
//	$value = (object)$value;
	$comparison = function ($val1, $val2) use ($field_value) {
		return $val1->$field_value != $val2->$field_value;
	};
}

if(isset($value) || isset($data)){
	$text_formatter = function($matches) use(&$subject){return $subject->$matches[1];};
}

if($value){
	$subject = $value;
	$text_value = preg_replace_callback('(:([A-z0-9]+):)', $text_formatter, $text);
}
$text_option = preg_replace('(:([A-z0-9]+):)', "'+val.$1+'", $text);
?>

<select data-placeholder="<?= $placeholder ? $placeholder : "Search&hellip;"; ?>" class="select2" name="<?= $name; ?>" id="<?= $id; ?>"<?= $multiple ? ' multiple="multiple"' : ''; ?>>
	<option></option>
	<?php if($data):
		if(is_array($value)):
			foreach(array_uintersect($data, $value, $comparison) as $row): $subject = $row?>
				<option value="<?= $row->$field_value ?>" selected="selected"><?=preg_replace_callback('(:([A-z0-9]+):)', $text_formatter, $text)?></option>
			<?php endforeach;
			foreach(array_udiff($data, $value, $comparison) as $row):?>
				<option value="<?= $row->$field_value ?>" ><?=preg_replace_callback('(:([A-z0-9]+):)', $text_formatter, $text)?></option>
			<?php endforeach;
		else:
		foreach($data as $row): $subject = $row?>
		<option value="<?=$row->$field_value?>" <?=$row->$field_value == $value->$field_value? 'selected="selected"' : ''?>><?=preg_replace_callback('(:([A-z0-9]+):)', $text_formatter, $text)?></option>
	<?php endforeach;
		endif;
	elseif($value):;?>
		<option value="<?=$value->$field_value?>" selected="selected"><?=$text_value?></option>
	<?php endif;?>
</select>



<?php $this->start_script()?>
$(document).ready(function(){
	$('<?="#$id"?>').select2({
		ajax:{
			url: '<?=$ajaxUrl?>',
			method: 'POST',
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {search: params.term<?=$params ? ', params: ' . json_encode($params) : ''?>};
			},
			processResults: function(data){
				var results = [];
				$.each(data, function(i, val){
					results.push({id: val.<?=$field_value?>, text: '<?=$text_option?>'});
				});
				return {results: results};
			},
			cache: true
		},
		minimumInputLength: 1,
		allowClear: true
	});
});
<?php $this->end_script()?>
