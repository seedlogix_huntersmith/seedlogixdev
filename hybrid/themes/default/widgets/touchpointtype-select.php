<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 1/19/15
 * Time: 12:38 PM
 */

if($touchpoint_type): ?>
    <input type="hidden" name="website[touchpoint_type]" value="<?= $touchpoint_type; ?>">
<?php else: ?>
    <div class="fluid spacing">
        <div class="grid4"><label for="website[touchpoint_type]">Touchpoint Type:</label></div>
        <div class="grid8">
            <select name="website[touchpoint_type]" id="touchpointType">
<?php foreach($touchPointTypes as $touchPointType => $value): ?>
                <option value="<?= $touchPointType; ?>"><?= $touchPointType; ?></option>
<?php endforeach; ?>
            </select>
        </div>
    </div>
<?php endif; ?>