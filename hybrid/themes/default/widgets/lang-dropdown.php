<?php

$lang_list = Defaults::getHubLangs();


?>
<div class="chosenSelect">
<select name="<?php echo $name; ?>" class="select" disabled>
 <option value="">Choose Language</option>
 <?php foreach($lang_list as $code => $langStr): ?>
        <option value="<?php echo $code; ?>"<?php $this->selected($code, $value); ?>><?php echo $langStr; ?></option>
 <?php endforeach; ?>
</select>
</div>