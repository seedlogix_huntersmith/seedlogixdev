<?php
if(!function_exists('wysiwygidectr')){
    function wysiwygidectr(){
        static $ctr = 0;
        return ++$ctr;
    }
}

$instancectr    = wysiwygidectr();
$tab1           = 'tabA'.$instancectr;
$tab2           = 'tabB'.$instancectr;
?>

<div class="j-dual-ide" style="position: relative;"<?= $disable ? ' data-disable="true"' : ''; ?>>
    <ul class="tabs hand">
        <li class="j-codemirror activeTab"><a href="#<?= $tab1; ?>">Source Code</a></li>
        <li class="j-cleditor"><a href="#<?= $tab2; ?>">Easy Editor</a></li>
    </ul>
    <div class="tab_container">
        <div class="tab_content" id="<?= $tab1; ?>"><textarea class="j-codemirror <?= $class; ?>" name="<?= $fieldname; ?>"><?php $this->p($fieldvalue); ?></textarea></div>
        <div class="tab_content hide" id="<?= $tab2; ?>"><textarea class="j-cleditor"><?php $this->p($fieldvalue); ?></textarea></div>
    </div>
</div>