<?php
//$Controller     = !$User->can('view_admin') ? 'UserManager' : 'Admin';
$Controller     = $User->id == $User->parent_id ? 'Admin' : 'UserManager';
$show_manager   = !$User->can('view_users') && !$User->can('reports_only');
if($_GET['ID']){
	$phone = PhoneModel::Fetch('cid = "'.$_GET['ID'].'" AND type = "voip"');
	if($phone){
		$voipusername = $phone->username;
		$voippassword = $phone->password;
		if($phone->external_only==1) $showPhone = false;
		else $showPhone = true;
	}
}
?>

    <div class="topNav">
        <ul class="userNav">
			<?php if($showPhone): ?><li><a class="jopendialog jopen-makecall phone-system"></a></li><?php endif; ?>
			
            <li><a href="<?= $this->action_url($Controller.'_profile?ID='.$User->getID()); ?>" class="profile"></a></li>

<?php if($show_manager): ?>

            <li>Managing <strong class="yellow"><?= $User->username; ?></strong></li>

<?php endif; ?>

            <li><a href="<?= $this->action_url('Auth_logout'); ?>" class="logout"></a></li>
        </ul>
    </div>
<?php if($showPhone && !$this->isAction('account', 'contact')): ?>
<?php $this->getDialog('make-call.php'); ?>
<script type="text/javascript">
$(document).ready(function() {
	    
		
		$('#jdialog-makecall').dialog(
		{
			autoOpen: false,
			height: 315,
			width: 400,
			modal: true
		});
		


	});	
</script>
<?php endif; ?>