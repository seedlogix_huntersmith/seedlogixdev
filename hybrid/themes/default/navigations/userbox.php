<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/* @var $User UserWithRolesModel */
/* $User->getProfileImageUrl(); */
$user_image = $User->getProfileImageUrl() ? $User->getProfileImageUrl() : $_path . 'images/userLogin2.png';
?>

<div class="user">
    <a title="" class="leftUserDrop">
        <img src="<?php echo $user_image; ?>" width="90" height="120" alt="" />
        <!-- Not going in version 1.0 release
        <span><strong><?php echo $User->unread_msgs; ?></strong></span> -->
    </a>
    <span><?php echo $User->firstname; ?></span>
    <ul class="leftUser">
        <li><a href="<?php echo $this->action_url('UserManager_profile?ID=' . $User->getID()); ?>" title="" class="sProfile">My profile</a></li>
        <!-- Not going in version 1.0 release
        <li><a href="#" title="" class="sMessages">Messages</a></li>
        -->
        <?php if($User->can('view_admin')): ?>
        <!--<li><a href="<?php echo $this->action_url('Admin_billing?ID=' . $User->getID()); ?>" title=""
                class="sSettings">Billing</a></li>-->
        <?php endif; ?>
        <li><a href="<?php echo $this->action_url('Auth_logout'); ?>" title="" class="sLogout">Logout</a></li>
    </ul>
</div>