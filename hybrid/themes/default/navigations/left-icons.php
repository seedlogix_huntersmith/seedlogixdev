<?php
$home_action        = $User->can('view_HOME') ? 'Campaign_landing' : 'Campaign_home';
$show_usersearch    = $User->can('view_users') || $User->has_referred_users == 1;
$show_manager       = !$User->can('view_users');
?>

<div class="mainNav">
    <ul class="nav">

<?php if($User->can('view_HOME')): ?>

        <li><a href="<?= $this->action_url($home_action); ?>" title="" class="<?= @$classes['home-nav']; ?>"><span class="home"></span><span>Home</span></a></li>

<?php endif; ?>

<?php if($User->hasModuleAccess('Campaigns') && !$User->can('crm_only')): ?>

        <li><a href="<?= $this->action_url('Campaign_home'); ?>" title="" class="<?= $site->cid != 99999 ? @$classes['campaign-nav'] : '';/* MU Hack */ ?>"><span class="campaigns"></span><span>Campaigns</span></a></li>

<?php endif; ?>
		
		<li><a href="<?= $this->action_url('Campaign_salescrm'); ?>" title="" class="<?= $site->cid != 99999 ? @$classes['crm-nav'] : '';/* MU Hack */ ?>"><span class="crm"></span><span>Sales CRM</span></a></li>

<?php if($User->can('view_admin')  || $User->can('view_users')): ?>

        <li><a href="<?= $this->action_url('Admin_dashboard'); ?>" title="" class="<?= $site->cid != 99999 ? @$classes['admin-nav'] : 'active';/* MU Hack */ ?>"><span class="admin"></span><span>Admin</span></a></li>

<?php endif; ?>

<?php if($User->can('is_system')): ?>

        <li><a href="<?= $this->action_url('System_events'); ?>" title="" class="<?= @$classes['system-nav-icon']; ?>"><span class="system"></span><span>System</span></a></li>

<?php endif; ?>

    </ul>
</div>