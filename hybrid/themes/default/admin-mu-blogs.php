<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>MU Blog Feeds</h6>
			<div class="buttonS f_rt jopendialog jopen-createPost"><span>Create a New Blog Feed</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('BlogFeeds'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'leadforms-tbl',
	'data'				=> $feeds,
	'rowkey'			=> 'feed',
	'rowtemplate'		=> 'feed-row.php',
	'total'				=> $totalFeeds,
	'sourceurl'			=> $this->action_url('admin_ajaxMuFeeds'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<?php
?>
<div class="SQmodalWindowContainer dialog" title="Create a New Blog Feed" id="jdialog-createPost" style="display: none;">
	<form method="post" action="<?= $this->action_url('Admin_ajaxSaveFeed'); ?>" class="jform-create">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="feed[name]" id="post_feed" style="margin: 0;"></div>
			</div>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {
        
		//===== Create New Form + PopUp =====//
		$('#jdialog-createPost').dialog(
		{
			autoOpen: false,
			height: 150,
			width: 300,
			modal: true,
			close: function()
			{
				$('#campaign_id',this).val('');
				$('form',this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
                function(ajax, status)
                {
                    if (!ajax.error)
                    {
                        var dt = $('#leadforms-tbl').dataTable();
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.ID);
						dt.data('newRowsIds', newRowsIds);
						dt.fnDraw();
						notie.alert({text:ajax.message});
                        this.close();
                        this.form[0].reset();
                    }
                    else if (ajax.error == 'validation')
                    {
                        alert(getValidationMessages(ajax.validation).join('\n'));
                    }
                    else
                        alert(ajax.message);

                }, '#jdialog-createPost')).buttons
		});

	});

<?php $this->end_script(); ?>
</script>