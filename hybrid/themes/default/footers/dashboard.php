<?php

/*
 * Author:     Jon Aguilar
 * License:
 */
$detect = new MobileDetect();
//var_dump($detect->isMobile());
?>
</div>
</div>
<!-- Content ends -->
<a id="scrollTop"><span class="up"></span></a>
<!-- bodywrap end -->
</div>

<!-- Notie JS -->
<script type="text/javascript" src="<?= $_path; ?>js/plugins/notie/notie.min.js"></script>

<script type="text/javascript">

	(function($){
			<?php if ( $detect->isMobile() == true || $detect->isTablet() == true){ ?>
			var _wload = $('#sidebar').width() == 300 ? '0px' : '300px';
			$('#sidebar').toggle();
			$('#sidebar').css({'width':_wload});
		    $('#content').css({'margin-left':_wload});
		
			$('span.hamburger').off('click').on('click',function(){
				var _w = $('#sidebar').width() == 300 ? '0px' : '300px';
				$('#sidebar').toggle();
				$('#sidebar').css({'width':_w});
				$('#content').css({'margin-left':_w});
			});
			<? } else { ?>
			$('span.hamburger').off('click').on('click',function(){
				var _w = $('#sidebar').width() == 300 ? '100px' : '300px';
				$('.secNav').toggle();
				$('#sidebar').css({'width':_w});
				$('#content').css({'margin-left':_w});
			});
			<? } ?>

		function _resizer(){
/*
			var _mn = window.matchMedia('(min-width:992px)');
			var _mx = window.matchMedia('(max-width:991px)');
			if(_mn.matches){
				$('span.hamburger').off('click').on('click',function(){
					var _w = $('#sidebar').width() == 300 ? '100px' : '300px';
					$('.secNav').toggle();
					$('#sidebar').css({'width':_w});
					$('#content').css({'margin-left':_w});
				});
				$('ul#rmenu').removeAttr('style');
				$('ul#rmenu .exp').collapsible('close');
			} else if(_mx.matches){
				$('span.hamburger').off('click').on('click',function(){
					$('ul#rmenu').slideToggle(300);
				});
				$('.secNav,#sidebar,#content').removeAttr('style');
				if($('.subNav li.activeli > .exp').collapsible('collapsed')){
					$('.subNav li.activeli > .exp,.subNav li.activeli > .exp2').collapsible('open');
					$('.subNav .exp,.subNav .exp2').not('.subNav li.activeli > .exp,.subNav li.activeli > .exp2').collapsible('close');
				} else{
					$('.subNav .exp,.subNav .exp2').not('.subNav li.activeli > .exp,.subNav li.activeli > .exp2').collapsible('close');
				}
			}
*/
			$('.dialog').dialog('option','position','center');
		}

		$(window).resize(_resizer).resize();

	})(jQuery);

</script>
</body>
</html>