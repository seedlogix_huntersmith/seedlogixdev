<?php
// @amado pull values from DB
$planBasePrice      = 50.00;
$CurrentPrice       = 50.00;

$prof_tps           = 10;
$prof_keywords      = 100;
$prof_emails        = 1200;

$corp_tps           = 50;
$corp_keywords      = 500;
$corp_emails        = 60000;

$ent_tps            = 250;
$ent_keywords       = 2500;
$ent_emails         = 300000;
?>

<div class="fluid">
    <div class="nNote nInformation"><span>You have <strong>25 days</strong> left of your free trial.</span><span class="close"></span></div>
    <div class="nNote nWarning"><span>You're Free Trial is up. Please select a plan below to keep account active.</span><span class="close"></span></div>
</div>

<div class="fluid hide" id="pricingPlans"> 
		
		<!--table one starts-->
		<div class="grid4">
			<div class="pricing">
				<table>
					<thead>
						<tr>
							<th> PROFESSIONAL
								<div class="label-wrapper">
									<div class="price-label"> $50<span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">Current Plan</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Marketers <br>
									and Business Owners.</p>
								<ul>
									<li><strong>Up to <?=$prof_tps?> Touchpoints</strong> <br>
										<span class="text-small"><?=$prof_tps?> Sites / <?=$prof_tps?> Blogs / <?=$prof_tps?> Landing Pages</span></li>
									<li><strong>Up to <?=$prof_keywords?> Keywords for SERP Rankings</strong> <br>
										<span class="text-small">Know where you are ranking on Google &amp; Bing</span></li>
									<li><strong>Up to <?=number_format($prof_emails)?> Emails</strong> <br>
										<span class="text-small">Emails calculated on per month</span></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!--table one ends--> 
		
		<!--table two starts-->
		<div class="grid4">
            <!-- @amado add class "upgrade" for plans the user can chose -->
			<div class="pricing upgrade">
				<table>
					<thead>
						<tr>
							<th> CORPORATE
								<div class="label-wrapper">
									<div class="price-label"> $200<span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">Upgrade to CORPORATE</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Marketing Teams, SEOs <br>
									and Web Designers</p>
								<ul>
									<li><strong>Up to <?=$corp_tps?> Touchpoints</strong> <br>
										<span class="text-small"><?=$corp_tps?> Sites / <?=$corp_tps?> Blogs / <?=$corp_tps?> Landing Pages</span></li>
									<li><strong>Up to <?=$corp_keywords?> Keywords for SERP Rankings</strong> <br>
										<span class="text-small">Know where you are ranking on Google &amp; Bing</span></li>
									<li><strong>Up to <?=number_format($corp_emails)?> Emails</strong> <br>
										<span class="text-small">Emails calculated on per month</span></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!--table two ends--> 
		
		<!--table three starts-->
		<div class="grid4">
            <!-- @amado add class "upgrade" for plans the user can chose -->
			<div class="pricing upgrade">
				<table>
					<thead>
						<tr>
							<th> ENTERPRISE
								<div class="label-wrapper">
									<div class="price-label"> $800<span>/month</span> </div>
								</div>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td><!-- the current plan will say "Current Plan" others will say UPGRADE -->
                                <a class="mybtn-grey mediumbtn" href="#">Upgrade to ENTERPRISE</a>
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td><p>Best for Franchises, Agencies <br>
									and Marketing Groups</p>
								<ul>
									<li><strong>Up to <?=$ent_tps?> Touchpoints</strong> <br>
										<span class="text-small"><?=$ent_tps?> Sites / <?=$ent_tps?> Blogs / <?=$ent_tps?> Landing Pages</span></li>
									<li><strong>Up to <?=$ent_keywords?> Keywords for SERP Rankings</strong> <br>
										<span class="text-small">Know where you are ranking on Google &amp; Bing</span></li>
									<li><strong>Up to <?=number_format($ent_emails)?> Emails</strong> <br>
										<span class="text-small">Emails calculated on per month</span></li>
									<li><strong>Unlimited Users</strong> <br>
										<span class="text-small">Added users share volume amounts.</span></li>
									<li><strong>All <a href="/software/production-tools/">Production Tools</a></strong> </li>
									<li><strong>All <a href="/software/marketing-automation/">Marketing Automation Tools</a></strong> </li>
									<li><strong>All <a href="/software/lead-management/">Lead Management Tools</a></strong> </li>
									<li><strong>All <a href="/software/roi-tracking/">ROI Tracking Tools</a></strong> </li>
									<li><strong>Private Label Options</strong> </li>
									<li><strong>No Contracts, Month-to-Month</strong> </li>
									<li><strong>Training &amp; Support</strong> </li>
								</ul></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!--table three ends--> 
		
</div>

<div class="fluid">
 
        <!-- Invoice template -->
        <div class="widget">

            <div class="whead">
                <h6>Itemized Billing</h6>
                <div class="buttonS f_rt" id="pricingPlans-toggle"><span>Toggle the Pricing Plans</span></div>
                <div class="buttonS f_rt editFields"><span>Update the Subscription</span></div>
            </div>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tLight">
                    <thead>
                        <tr>
                            <td width="30%">Product</td>
                            <td width="50%">Description</td>
                            <td witdth="10%">Quantity</td>
                            <td width="5%">Total</td>
                            <td width="5%">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>Professional Plan</strong></td>
                            <td>Base plan - Best for Marketers and Business Owners.</td>
                            <td>1</td>
                            <td><strong>$50</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><?=$prof_tps?> Touchpoints</td>
                            <td><?=$prof_tps?> websites, blogs, landing pages. <strong>(Included in plan)</strong></td>
                            <td><?=$prof_tps?></td>
                            <td>$0</td>
                            <td>
                                <a href="#addTouchpoints" class="tablectrl_small bGreen tipS toggleAddRow" title="Add Touchpoints"><span class="fs1 iconb" data-icon=""></span></a>
                            </td>
                        </tr>
                        <!-- ADDON TOUCHPOINTS -->
                        <tr class="hide" id="addTouchpoints">
                            <td><strong><em>Additional Touchpoints</em></strong></td>
                            <td>Additional websites, blogs, landing pages.</td>
                            <td>
                                <div class="formRow">
                                    <input type="text" name="touchpoints" class="addon-quantity touchpoints" />
                                </div>
                            </td>
                            <td><strong id="touchpoints-price">$0</strong></td>
                            <td>
                                <a href="#addTouchpoints" class="tablectrl_small bRed tipS toggleAddRow" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            </td>
                        </tr>
                        <tr>
                            <td><?=$prof_keywords?> Keywords</td>
                            <td><?=$prof_keywords?> keywords <strong>(Included in plan)</strong></td>
                            <td><?=$prof_keywords?></td>
                            <td>$0</td>
                            <td>
                                <a href="#addKeywords" class="tablectrl_small bGreen tipS toggleAddRow" title="Add Keywords"><span class="fs1 iconb" data-icon=""></span></a>
                            </td>
                        </tr>
                        <!-- ADDON KEYWORDS -->
                        <tr class="hide" id="addKeywords">
                            <td><strong><em>Additional Keywords</em></strong></td>
                            <td>Additional Keywords</td>
                            <td>
                                <div class="formRow">
                                    <input type="text" name="keywords" class="addon-quantity keywords" />
                                </div>
                            </td>
                            <td><strong id="keywords-price">$0</strong></td>
                            <td>
                                <a href="#addKeywords" class="tablectrl_small bRed tipS toggleAddRow" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            </td>
                        </tr>
                        <tr>
                            <td><?=$prof_emails?> Emails</td>
                            <td><?=$prof-emails?> Emails for autoresponsders and broadcast <strong>(Included in plan)</strong></td>
                            <td><?=$prof_emails?></td>
                            <td>$0</td>
                            <td>
                                <a href="#addEmails" class="tablectrl_small bGreen tipS toggleAddRow" title="Add Emails"><span class="fs1 iconb" data-icon=""></span></a>
                            </td>
                        </tr>
                        <!-- ADDON EMAILS -->
                        <tr class="hide" id="addEmails">
                            <td><strong><em>Additional Emails</em></strong></td>
                            <td>Additional Emails</td>
                            <td>
                                <div class="formRow">
                                    <input type="text" name="emails" class="addon-quantity emails" />
                                </div>
                            </td>
                            <td><strong id="emails-price">$0</strong></td>
                            <td>
                                <a href="#addEmails" class="tablectrl_small bRed tipS toggleAddRow" title="Remove"><span class="iconb" data-icon="&#xe136;"></span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <div class="total">
                        <span>Monthly Rate</span>
                        <strong class="red" id="addon-total">$<?=$planBasePrice?></strong>
                    </div>
                </div>
        </div>
</div>


<div class="fluid hide" id="addon_cnt">
        <div class="widget">
            <div class="whead">
                <h6>Addon Items</h6>
                <a class="buttonS bGreen right editFields" href="#"><span class="icol-add"></span><span>Purchase</span></a>
            </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tLight">
                    <thead>
                        <tr>
                            <td width="30%">Product</td>
                            <td width="60%">Descrition</td>
                            <td witdth="9%">Quantity</td>
                            <td width="9%">Total</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <select id="addon-product">
                                    <option value="">Select Product</option>
                                    <option value="touchpoints">Touchpoints</option>
                                    <option value="keywords">Keywords</option>
                                    <option value="emails">Emails</option>
                                </select>
                            </td>
                            <td><span id="addon-product-desc"></span></td>
                            <td>
                                <div class="formRow">
                                <input type="text" name="quantity" id="addon-quantity" />
                                </div>
                            </td>
                            <td align"center"><strong id="addon-price">$0</strong></td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <div class="total">
                        <span>Adjusted Monthly Rate</span>
                        <strong class="red">$<?=$CurrentPrice?></strong>
                    </div>
                </div>
        </div>
</div>

<a name="addonWidget">&nbsp;</a>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
        
        var activeClass = 'bGreen';
        
        var planProfessional = 50; //set value with PHP variable
        var planCorporate = 200; //set value with PHP variable
        var planEnterprise = 800; //set value with PHP variable
        
		//Addon calculation
		var basePrice = <?=$planBasePrice?>; //set value with PHP variable
        var currentPrice = <?=$CurrentPrice?>; //set value with PHP variable
        var currentAddon = '';
        
        var touchpointPrice = 0;
        var keywordPrice = 0;
        var emailPrice = 0;
        var desc_touchpoints = 'Additional websites, blogs, landing pages.';
        var desc_keywords = 'Additional Keywords to be tracked and viewed in your rankings reports';
        var desc_emails = 'Additional Emails for Autoresponsders and Email Broadcast';
        
        switch(basePrice)
        {
            case planProfessional:
                touchpoints = basePrice/<?=$prof_tps?>;
                keywords = basePrice/<?=$prof_keywords?>;
                emails = basePrice/<?=$prof_emails?>;
                break;
            case planCorporate:
                touchpoints = basePrice/<?=$corp_tps?>;
                keywords = basePrice/<?=$corp_keywords?>;
                emails = basePrice/<?=$corp_emails?>;
                break;
            case planEnterprise:
                touchpoints = basePrice/<?=$ent_tps?>;
                keywords = basePrice/<?=$ent_keywords?>;
                emails = basePrice/<?=$ent_emails?>;
                break;
            default:
                alert('plan must be selected');
        }
        
        //check for quantity input
        $('.addon-quantity').bind("keyup change", function(e) {
            calculatePrices( $(this) );
        });
        
        //calculate monthly and update UI pricing
        function calculatePrices(element){
         
            var value = element.val();
            var calculatedPrice;
            var totalPrice;
            var currentAddon = element.attr('name');
            
            if(value == 0)
            {
                $('#'+currentAddon+'-price').html('$'+0);
            }
            
            switch(currentAddon)
            {
                case 'touchpoints':
                    calculatedPrice = (touchpoints*parseInt(value));
                    break;
                case 'keywords':
                    calculatedPrice = (keywords*parseInt(value));
                    break;
                case 'emails':
                    calculatedPrice = (emails*parseInt(value));
                    break; 
                default:
                    return;
            }
            
            var result = doMath(calculatedPrice);
            
            $('#'+currentAddon+'-price').html('$'+result);
            
            totalPrice = addAllAddons();
            $('#addon-total').html('$'+(totalPrice+currentPrice));
            
        }
        
        //add up all the addon prices
        function addAllAddons(){
            
            var touchpointsTotal = $('#touchpoints-price').html();
            var keywordsTotal = $('#keywords-price').html();
            var emailsTotal = $('#emails-price').html();
            
            touchpointsTotal = touchpointsTotal.replace('$','');
            keywordsTotal = keywordsTotal.replace('$','');
            emailsTotal = emailsTotal.replace('$','');
            
            return doMath(parseFloat(touchpointsTotal)) + doMath(parseFloat(keywordsTotal)) + doMath(parseFloat(emailsTotal));
        }
        
        //math stuff
        function doMath(price){
            var number = price;
            number = number * 100;
            var result = Math.ceil(number);
            return result / 100;   
        }
        
        //toggle addon row
        $('.toggleAddRow').click(function(){
            var target = $(this).attr('href');
            $(target).slideToggle(400);
            
            if( $(this).attr('original-title') == 'Remove' )
            {
                target = target.replace('#add','').toLowerCase();
                $('input.'+target).val('0');
                calculatePrices( $('input.'+target) );
            }
            
            return false;
        });
        
        //show/hide pricing tables
        $('#pricingPlans-toggle').click(function(){
            $('#pricingPlans').slideToggle(400);
            return false;
        });
	
	});
    
<?php $this->end_script(); ?>
</script>