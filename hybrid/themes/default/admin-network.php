<?php
$local  =   new Local;

	$reseller = new Reseller();
        $NetworkReseller    =   $network->getReseller();
?><div class="fluid">

    <form action="<?php echo $this->action_url('Admin_savenetwork?ID=' . $network->getID()); ?>" method="POST" class="ajax-save" enctype="multipart/form-data">
        <div class="widget grid6">
            <div class="whead"><h6>Network Configuration</h6><div class="clear"></div></div>
            <div class="formRow">
                <div class="grid3"><label>Company Name</label></div>
                <div class="grid9"><input type="text" name="network[company]" value="<?php $this->p($network->company); ?>"></div>
                <div class="clear"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Network Name</label></div>
                <div class="grid9"><input type="text" name="network[network_name]" value="<?php $this->p($network->network_name); ?>"></div>
                <div class="clear"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Hostname</label></div>
                <div class="grid9"><input type="text" name="network[httphostkey]" value="<?php $this->p($network->httphostkey); ?>"></div>
                <div class="clear"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Theme</label></div>
                <div class="grid9">
                    <? 
                    // @todo rewrite
                     $local->displayThemes(NULL, $network->theme, 'network_site', $User->class); ?>
                    </div>
                <div class="clear"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Category Only Network</label></div>
                <div class="grid9">
                    
                <select name="network[industry]" title="select">
					<option value="0">Please Choose An Industry</option>
                    <option value="Automotive" <? if($network->industry == "Automotive") echo 'selected="yes"'; ?> >Automotive</option>
					<option value="Business Services" <? if($network->industry == "Business Services") echo 'selected="yes"'; ?> >Business Services</option>
                    <option value="Community and Education" <? if($network->industry == "Community and Education") echo 'selected="yes"'; ?> >Community and Education</option>
                    <option value="Computers and Electronics" <? if($network->industry == "Computers and Electronics") echo 'selected="yes"'; ?> >Computers and Electronics</option>
                    <option value="Entertainment" <? if($network->industry == "Entertainment") echo 'selected="yes"'; ?> >Entertainment</option>
					<option value="Finance" <? if($network->industry == "Finance") echo 'selected="yes"'; ?> >Finance</option>
                    <option value="Food and Dining" <? if($network->industry == "Food and Dining") echo 'selected="yes"'; ?> >Food and Dining</option>
                    <option value="Health and Personal Care" <? if($network->industry == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Healthcare" <? if($network->industry == "Healthcare") echo 'selected="yes"'; ?> >Healthcare</option>
                    <option value="Home Based Business" <? if($network->industry == "Home Based Business") echo 'selected="yes"'; ?> >Home Based Business</option>
                    <option value="Home Improvement" <? if($network->industry == "Home Improvement") echo 'selected="yes"'; ?> >Home Improvement</option>
                    <option value="Health and Personal Care" <? if($network->industry == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Legal" <? if($network->industry == "Legal") echo 'selected="yes"'; ?> >Legal</option>
                    <option value="Photography" <? if($network->industry == "Photography") echo 'selected="yes"'; ?> >Photography</option>
                    <option value="Real Estate" <? if($network->industry == "Real Estate") echo 'selected="yes"'; ?> >Real Estate</option>
                    <option value="Shopping" <? if($network->industry == "Shopping") echo 'selected="yes"'; ?> >Shopping</option>
                    <option value="Travel and Recreation" <? if($network->industry == "Travel and Recreation") echo 'selected="yes"'; ?> >Travel and Recreation</option>
                    <option value="Web Services" <? if($network->industry == "Web Services") echo 'selected="yes"'; ?> >Web Services</option>
				</select></div>
                <div class="clear"></div>
            </div>
            <div class="formRow noBorderB">
                <div class="grid3"><label>Single Sub-Category Only Network</label></div>
                <div class="grid9"><select name="network[category]">
				<?php $industries   =   $local->returnCategories('');
//                                $local->displayCategories($network->category);
                                foreach($industries as $lwrk => $industry): ?>
                        <option value="<?= $this->p($lwrk); ?>"<?php
                            $this->selected($industry, $network->industry); ?>><?php
                            echo $industry; ?></option><?php
                        endforeach;
                                ?></select></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Featured Class</label></div>
                <div class="grid9"><select name="network[featured_class]">
                <?php
                    $productsr   =   $reseller->getResellerProducts($network->admin_user);
                    
//                echo $reseller->displayResellerProducts(                                       $network->admin_user, 'dropdown',     $network->featured_class, NULL, NULL, 'featured_class'); 
                    
                    // weak sauce
                    if($products === false){ 
                            $products   =   array(); ?>
                        <option value="">No Products available</option><?php
                    }else
                    while($row= $reseller->fetchArray($productsr))
                        $products[$row['id']]   =   $row;
                    
                    foreach($products as $row):
                ?><option value="<?= $row['id']; ?>"<?php 
                    $this->selected($row['id'], $network->featured_class); ?>><?php
                        $this->p($row['name']); ?></option>
                    <?php endforeach; ?>
                    </select></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>User Class Only</label></div>
                <div class="grid9"><select name="network[class_only]">
				<?php 
                                if(!$products)
                                    echo '<option>No products available.';
                                else
                                foreach($products as $row): ?>
                        <option value="<?= $row['id']; ?>"<?php
                            $this->selected($row['id'], $network->class_only); ?>><?= $this->p($row['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>City Focus</label></div>
                <div class="grid9"><input type="text" name="network[city_focus]" value="<?php $this->p($network->city_focus); ?>"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>State Focus</label></div>
                <div class="grid9"><?php 
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        
                        
//                    $this->getWidget('states-dropdown.php',
//                                array('name' => 'network[state_focus]',
//                                        'value' => $network->state_focus));
                    ?></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Remove Search Option</label></div>
                <div class="grid9">
				<select name="network[remove_search]" title="select">
					<option value="0">No</option>
					<option value="1" <? if($network->remove_search == "1") echo 'selected="yes"'; ?> >Yes</option>
				</select></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Remove Contact Form on Listing</label></div>
                <div class="grid9">
				<select name="network[remove_contact]" title="select">
					<option value="0">No</option>
					<option value="1" <? if($network->remove_contact == "1") echo 'selected="yes"'; ?> >Yes</option>
				</select></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Background Color</label></div>
                <div class="grid9"><input type="text" name="network[bg_color]" value="<?php $this->p($network->bg_color); ?>"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Nav Link Hover Color</label></div>
                <div class="grid9"><input type="text" name="network[nav-link-hover-clr]" value="<?php $this->p($network->get('nav-link-hover-clr')); ?>"></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="widget grid6">
            <div class="whead"><h6>Automation Settings</h6><div class="clear"></div></div>
            <div class="formRow">
                    <div class="grid3"><label>Facebook Page:</label></div>
                    <div class="grid9"><?php
                    
                    if(!$NetworkReseller):
                        ?> No Reseller configured for this Network. Please associate a reseller first.
                        <?php
                        else:
                            if($network->fb_post_authid): ?>Page: <?php echo
                                $network->fbauth_name;
                                endif; 

                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        
//                                $this->getWidget('facebook-authorization.php', array('id' => $network->fb_post_authid,
//                                        'Reseller' => $network->getReseller(),
//                                        'action_url'  => 'http://' . $network->httphostkey . '/' . $this->action_url('Admin_network?ID=' . $network->getID())));
                                
                    endif;
                        ?>

                        <input type="hidden" value="<?php $this->p($network->fb_post_authid); ?>" name="network[fb_post_authid]"   /></div>
                    <div class="clear"></div>
            </div>
        </div>
        
    <div class="widget grid6">
        <div class="whead"><h6>Images</h6><div class="clear"></div></div>
        
        <div class="formRow">
            <div class="grid3"><label>Site  Logo:</label> </div>        <!-- 
            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                        <?php 
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        ?>
        </div>
        
        <div class="formRow">
            <div class="grid3"><label>Favicon :</label> </div>        <!-- 
            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                        <?php 
                        
                        
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        ?>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Header BG :</label> </div>        <!-- 
            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                        <?php 
                        
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        
                        ?>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Footer BG :</label> </div>        <!-- 
            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                        <?php 
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                                                
                        ?>
        </div>
        <div class="formRow">
            <div class="grid3"><label>No Image :</label> </div>        <!-- 
            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                        <?php 
                        
                        
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'network[logo]',
                                    'value' => $network->get('logo')));
                        
                        ?>
        </div>
    </div>
        
    </form>
</div>
