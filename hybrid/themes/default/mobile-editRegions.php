<!-- LEFT SIDE NAV -->

<!-- //start content container -->
<div id="twoColContentArea" class="editRegions">
<!-- //start content container -->

<br class="clear" /><br />

				<!-- Left Home Page Content --> 
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb1">Left Home Page Content <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb2">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb1" style="display: block;">
                        	<form><textarea id="code1" name="code">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb2" style="display: none;">
                        	<textarea id="editor1" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Below Home Page Form -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb3">Below Home Page Form <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb4">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb3" style="display: block;">
                        	<form><textarea id="code2" name="code2">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb4" style="display: none;">
                        	<textarea id="editor2" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
                
				<!-- Mid Section Home Page -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb5">Mid Section Home Page <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb6">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb5" style="display: block;">
                        	<form><textarea id="code3" name="code3">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb6" style="display: none;">
                        	<textarea id="editor3" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Inside Pages Left Column -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb7">Inside Pages Left Column <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb8">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb7" style="display: block;">
                        	<form><textarea id="code4" name="code4">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb8" style="display: none;">
                        	<textarea id="editor4" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Contact Page Left Content -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb9">Contact Page Left Content <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb10">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb9" style="display: block;">
                        	<form><textarea id="code5" name="code5">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb10" style="display: none;">
                        	<textarea id="editor5" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Contact Page Right Content -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb11">Contact Page Right Content <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb12">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb11" style="display: block;">
                        	<form><textarea id="code6" name="code6">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb12" style="display: none;">
                        	<textarea id="editor6" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Header Edit Region -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb13">Header Edit Region <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb14">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb13" style="display: block;">
                        	<form><textarea id="code7" name="code7">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb14" style="display: none;">
                        	<textarea id="editor7" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
				<!-- Replace Default Footer -->
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb15">Replace Default Footer <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb16">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb15" style="display: block;">
                        	<form><textarea id="code8" name="code8">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb16" style="display: none;">
                        	<textarea id="editor8" name="editor" rows="" cols="16">Some cool stuff here</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
		
        
<!-- //end content container -->
</div>
<!-- //end content container -->

<script type="text/javascript">
	
	//===== Tabs =====//
	$.fn.contentTabs = function(){ 
	
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};
	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	//===== Collapsible elements management =====//
	

	$('.opened').collapsible({
		defaultOpen: 'opened,toggleOpened',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	$('.closed').collapsible({
		defaultOpen: '',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	//===== WYSIWYG editor =====//
	$("#editor1").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor2").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor3").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor4").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor5").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor6").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor7").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor8").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	//===== Code Mirror / Syntax highlight =====//
	var editor = CodeMirror.fromTextArea(document.getElementById("code1"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code2"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code3"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code4"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code5"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code6"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code7"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code8"), {
        lineNumbers: true
      });                       				
</script>


