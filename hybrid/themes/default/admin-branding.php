<?php
/**
 * @var $this Template
 */
?>
<form action="<?php echo $this->action_url('Admin_saveBranding', array('ID' => $Brand->getID())) ?>" class="ajax-save" enctype="multipart/form-data" method="POST">
<div class="fluid">
	<!-- right column -->
	<div class="grid6">
		<div class="widget">
			<div class="whead">
				<h6>Your Brand Settings</h6>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Company Name:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[company]" value="<?php $this->p($Brand->get('company'));?>">
					<span class="note">Global settings for proposals, invoices, and emails (can override at campaign level).</span>
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Address:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[address]" value="<?php $this->p($Brand->get('address'));?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>City:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[city]" value="<?php $this->p($Brand->get('city'));?>">
				</div>
			</div>
			<div class="formRow">
							<div class="grid3"><label>State/Province:</label></div>
	<?php $this->getWidget('states-dropdown.php', array('value' => $Brand->state, 'name' => 'branding[state]'));?>
						</div>
			<div class="formRow">
				<div class="grid3"><label>Zip/Postal Code:</label></div>
				<div class="grid3">
					<input type="text" placeholder="" name="branding[zip]" value="<?php $this->p($Brand->get('zip'));?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Phone:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[phone]" value="<?php $this->p($Brand->get('phone'));?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Email Address:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[support_email]" value="<?php $this->p($Brand->get('support_email'));?>">
					<span class="note">Global system wide email for notifications, reports, and alerts.</span>
				</div>
			</div>
			<!--<div class="formRow">
				<div class="grid3"><label>Login URL:</label></div>
				<div class="grid9">
					<input type="text" placeholder="" name="branding[main_site]" value="<?php $this->p($Brand->get('main_site'));?>">
					<span class="note">Login URL is the domain you can set for logging in. Subdomains are allowed.</span>
				</div>
			</div>-->
		</div>
		<div class="widget">
			<div class="whead">
				<h6>Design Settings</h6>
			</div>
			 <div class="formRow jqColorPicker">
						<div class="grid3"><label>Portal Color:</label></div>                  
						<div class="grid9">
							<input type="text" class="colorpicker" name="branding[bg_clr]" value="<?php $this->p($Brand->get('bg_clr'));?>" placeholder="">
						</div>
			  </div>
				<div class="formRow">
					<div class="grid3"><label>Advanced CSS:</label></div>
					<div class="grid9">
					<textarea id="code" class="codemirror" name="branding[css_link]" ><?php $this->p($Brand->get('css_link'));?></textarea>
					</div>
				</div>
		</div>
	</div>

    <div class="widget grid6">
		<div class="whead">
			<h6>Your Brand Logos</h6>
		</div>

			<div class="formRow">
				<div class="grid3"><label>Backoffice Logo:</label></div>
				<div class="grid9">
					<?php
					$this->getWidget('moxiemanager-filepicker.php', array(
						'name' => 'branding[logo]',
						'value' => $Brand->get('logo'),
						'idWidget' => 'logo',
						'path' => $storagePath,
						'noHost' => true,
						'extensions' => 'jpg,jpeg,png,gif',
						'preview' => 'branding-preview'
					));
					?>
				</div>
			</div>
            <div class="formRow">
				<div class="grid3"><label>Login Logo:</label></div>
				<div class="grid9">
					<?php
					$this->getWidget('moxiemanager-filepicker.php', array(
						'name' => 'branding[login-logo]',
						'value' => $Brand->get('login-logo'),
						'idWidget' => 'login-logo',
						'path' => $storagePath,
						'noHost' => true,
						'extensions' => 'jpg,jpeg,png,gif',
						'preview' => 'branding-login-preview'
					));
					?>
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>CRM Logo:</label></div>
				<div class="grid9">
					<?php
					$this->getWidget('moxiemanager-filepicker.php', array(
						'name' => 'branding[prop_logo]',
						'value' => $Brand->get('prop_logo'),
						'idWidget' => 'prop_logo',
						'path' => $storagePath,
						'noHost' => true,
						'extensions' => 'jpg,jpeg,png,gif',
						'preview' => 'branding-proposal-preview'
					));
					?>
				</div>
			</div>
	</div>
    
</div>
</form>
<script type="text/javascript">
<?php $this->start_script();?>
$(document).ready(function(){
var code = document.getElementById("code");
CodeMirror.fromTextArea(code, {
	lineNumbers: true,
	lineWrapping: true,
	mode: "css",
	styleActiveLine: true,
	viewportMargin: Infinity,
	readOnly: $(code).attr('disabled') === "disabled"
})
	.on('blur', function(cm, ev){
	var ta    =   cm.getTextArea();
	var srccode   =    cm.getValue();

	new AjaxSaveDataRequest(ta, srccode);
});
});
<?php $this->end_script();?>
</script>