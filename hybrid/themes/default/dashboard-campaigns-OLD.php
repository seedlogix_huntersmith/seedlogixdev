<?php if($User->id != 6610 && $User->can('reports_only')): ?>

<div class="fluid">
    <div class="widget tstats center grid4">
        <div class="whead"><h6>Total Visits (Last 30 Days)</h6></div>
        <div class="body"><span id="vstats"><!-- 999,999,999 --></span></div>
    </div>
    <div class="widget tstats center grid4">
        <div class="whead"><h6>Total Leads (Last 30 Days)</h6></div>
        <div class="body"><span id="lstats"><!-- 999,999,999 --></span></div>
    </div>
    <div class="widget tstats center grid4">
        <div class="whead"><h6>Total Calls (Last 30 Days)</h6></div>
        <div class="body"><span id="pstats"><!-- 999,999,999 --></span></div>
    </div>
</div>

<?php endif; ?>


<form action="<?= $this->action_url('piwikAnalytics_ajaxGetChartData'); ?>">

<?php if($User->id == 6610 || !$User->can('reports_only')): ?>

    <input type="hidden" name="interval" value="7">
    <input type="hidden" name="period" value="DAY">

<?php elseif($User->id != 6610 && $User->can('reports_only')): ?>

    <input type="hidden" name="interval" value="30">
    <input type="hidden" name="period" value="DAY">

<?php endif; ?>

    <input type="hidden" name="chart" value="LeadsVisitsChartData">
    <div class="widget chartWrapper">
        <div class="whead">

<?php if($User->id == 6610 || !$User->can('reports_only')): ?>

            <h6>Total Visits &amp; Leads for All Campaigns</h6>

<?php $this->getWidget('graph-range-select.php',array('id' => 'chartconfig','value' => Cookie::getVal('chartconfig','7-DAY'))); ?>

<?php elseif($User->id != 6610 && $User->can('reports_only')): ?>

            <h6>Total Visits, Leads &amp; Calls for All Campaigns  (Last 30 Days)</h6>

<?php $this->getWidget('graph-range-select.php',array('id' => 'chartconfig','value' => Cookie::getVal('chartconfig','30-DAY'))); ?>

<?php endif; ?>

        </div>
        <div class="body"><div class="chart"></div></div>
    </div>
</form>


<?php if($User->id == 6610 || !$User->can('reports_only')): ?>

<!-- ##### CAMPAIGNS TABLE ##### -->
<?php $this->getTable('dashboard-campaigns-stats.php'); ?>


<div class="fluid">


<!-- Top Performing Keywords widget -->
<?php $this->getWidget('top-performing-keywords.php'); ?>
<!-- Top Performing Keywords END widget -->



<!-- Top Ranking Keywords widget -->
<?php $this->getWidget('top-ranking-keywords.php'); ?>
<!-- Top Ranking Keywords END widget -->


<!-- Social Activity widget -->
<?php $this->getWidget('social-activity.php'); ?>
<!-- Social Activity END widget -->


</div>

<?php endif; ?>


<!-- Campaigns Prospect table sample -->
<?php $this->getTable('all-campaign-prospects.php'); ?>
<!-- Campaigns Prospect table END sample -->


<?php if($User->id != 6610 && $User->can('reports_only')): ?>

<!-- Campaigns Phone table sample -->
<?php //var_dump($role); //$this->getTable('all-campaign-phonerecords.php'); ?>

<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Phone Calls for All Campaigns  (Last 30 Days)</h6></div>

<?php
$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('PhoneRecords'),
					'class' => '',
					'id' => 'jPhoneRecords',
					'data' => $PhoneRecords,
					'rowkey' => 'PhoneRecord',
					'rowtemplate' => 'phone-records.php',
					'total' => $totalRecords,
					'sourceurl' => $this->action_url('campaign_ajaxPhoneMainDashboard'),
                    'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
?>

    </div>
</div>



<?php endif; ?>


<br class="clear">

<?php if($role[0]['ID'] == 722): ?>
<?php $this->getTable('dashboard-campaigns-stats.php'); ?>
<?php $this->getDialog('create-campaign.php'); ?>
<?php endif; ?>


<?php if($User->id == 6610 || !$User->can('reports_only')): ?>

<?php $this->getDialog('create-campaign.php'); ?>

<?php endif; ?>


<script type="text/javascript">

<?php $this->getJ('dashboard-campaign-js.php'); ?>

</script>