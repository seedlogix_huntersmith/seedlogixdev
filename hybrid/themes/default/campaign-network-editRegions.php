<!-- ######################## -->
<!-- ##### Edit Regions ##### -->
<!-- ######################## -->
<form action="<?php echo $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">

  <input type="hidden" name="listing[type]" value="<?php echo $dirarticle->getNetworkListingType(); ?>" />
  <input type="hidden" name="listing[id]" value="<?php echo $dirarticle->getID(); ?>" />
  <div class="fluid">
    <div class="widget grid12">
      <div class="whead">
        <h6>Your Company Spot</h6>
        <div class="clear"></div>
      </div>
      <div class="body j-dual-ide">
        <?php $this->getWidget('wysiwyg-ide.php', array('fieldname' => 'listing[company_spot]', 'fieldvalue' => $dirarticle->company_spot)); ?>
      </div>
    </div>    
  </div>

  <div class="fluid">
    <div class="widget grid12">
      <div class="whead">
        <h6>Video Spot</h6>
        <div class="clear"></div>
      </div>
      <div class="body j-dual-ide">
        <?php $this->getWidget('wysiwyg-ide.php', array('fieldname' => 'listing[video_spot]', 'fieldvalue' => $dirarticle->video_spot)); ?>
      </div>
    </div>    
  </div>
</form>

<script type="text/javascript">
<? $this->start_script(); ?>

  $(function() {

    //Listing Info
<?php $this->getJ('wysiwyg-ide.php'); ?>

  });

<? $this->end_script(); ?>
</script>