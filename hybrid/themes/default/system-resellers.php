<?php
/**
 * @var Template $this
 */
//    var_dump($events);
?>

<div class="fluid">
    <div class="widget">
        <div class="whead">
            <h6>Resellers</h6>
            <div class="buttonS f_rt jopendialog jopen-createresellers"><span>Create a New Reseller</span></div>
        </div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Resellers'),
	'class'				=> 'col_2nd,col_3rd',
	'id'				=> 'system-roles-tbl',
	'data'				=> $Resellers,
	'rowkey'			=> 'Reseller',
	'rowtemplate'		=> 'system-reseller.php',
	'total'				=> $totalResellers,
	'sourceurl'			=> $this->action_url('System_ajaxResellers'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div> 
</div>

<div class="SQmodalWindowContainer dialog" title="Create a New Reseller" id="jdialog-createresellers" style="display: none;">
    <form id="createresellersForm" action="<?= $this->action_url("system_save"); ?>">
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>HTTP Host:</label></div>
                <div class="grid8"><input type="text" name="reseller[main_site]" id="main_site" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
            </div>
            <div class="fluid spacing">
                <div class="grid4"><label>Company:</label></div>
                <div class="grid8"><input type="text" name="reseller[company]" id="main_site" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
            </div>
        </fieldset>
    </form>
</div>

<script>
<?php $this->start_script();?>
    $(function (){
        $('#jdialog-createresellers').dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true,
            close: function (){
                $('#main_site',this).val('');
                $('form',this)[0].reset();
            },
            buttons: (new CreateDialog('Create', function (ajax, status){
                // standards here will be 'error' var, possible values are : 'validation', 'other'
                //  if error = 'validation', check 'validation' object,
                // if error = 'other' then print 'message''
                // if no error or error = false, check result object. (could be a partiallyW
                if(ajax.error)
                {
                    if(ajax.error   ==  'validation')
                        alert(getValidationMessages(ajax.validation).join("\n"));
                    else
                        alert(ajax.message);
                }
                else
                {
                    $('#jnocampaigns-msg').hide()
                    $('#system-roles-tbl').dataTable().fnDraw();
                    this.close();
                    this.form[0].reset();
                    notie.alert({text:ajax.message});
                }

            },'#jdialog-createresellers')).buttons
        });
    })
<?php $this->end_script();?>
</script>