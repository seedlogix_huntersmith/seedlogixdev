<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 			  
 			  
 * License:    
 */

?>
<div id="twoColContentArea">

<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads for Blogs</h6>
                <?php $this->getPart('graph-range-select.php',
                            $ChartWidget->getSettings()); ?>
		<div class="clear"></div>
	</div>
        <input type="hidden" id="graph_action" value="<?php echo $this->action_url('campaign_graphdata?CID=' . $site->cid); ?>"/>
	<div class="body"><div class="chart"></div></div>
</div>

<!-- ##### BLOGS TABLE ##### -->           
<?php $this->getTable('dashboard-blogs-stats.php'); ?>
<br />


<!-- ##### Main Dashboard Chart ##### -->
<!--
<div class="widget chartWrapper">

            <div class="whead">
            	<h6>Example View</h6>
            	<div class="clear"></div>
            </div>
    
            
    <?php //echo 'ID='; $this->p($ID); ?>
            <div class="clear"></div> 
</div>
-->
<!--
    <div class="widget">
            <div class="whead">
            	<h6>More Examples</h6>
            	<div class="clear"></div>
            </div>
            
            <?php
                    // Example, global variables
                    
            ?>
        <pre><?php //var_dump($User, $Branding, $_path, $menu, $this->getaction(), $this->isAction('home') ? ' the action is home' : ' the action is not home'); ?></pre>
            
            <div class="clear"></div> 
        </div>

-->

<!-- //end content container -->
</div>


<script type="text/javascript">
	
	//===== Create Dashboard Graph =====//
    Dashboard.cookienames   =   <?php echo json_encode($ChartWidget->getSettings()); ?>;
        
	Dashboard.plotData(<?php
            $params = array();
            foreach($ChartWidget->getSets() as $data)
                    $params[]   =   json_encode($data);
            echo join(',', $params);
        ?> );

</script>