<form action="<?= $this->action_url('Campaign_save') ?>" class="ajax-save">
	<input type="hidden" name="social_posts[id]" value="<?=$socialpost->getID()?>"/>
	<input type="hidden" name="social_posts[cid]" value="<?=$socialpost->cid?>"/>

<div class="fluid">
	<!-- section -->
	<div class="widget grid8">
		<div class="whead">
			<h6><?=$socialpost->html('name')?></h6>
		</div> 
        <div class="formRow">
				<div class="grid3">
					<label>Name</label>
				</div>
				<div class="grid9"><input type="text" name="social_posts[name]" value="<?=$socialpost->html('name')?>"/></div>
			</div>
           <div class="formRow">
				<div class="grid3">
					<label>Post Title</label>
				</div>
				<div class="grid9"><input type="text" name="social_posts[post_title]" value="<?=$socialpost->html('post_title')?>"/></div>
			</div>
		<div class="formRow">
			<div class="grid3">
				<label>Post Link</label>
			</div>
			<div class="grid9"><input type="text" name="social_posts[post_link]" value="<?=$socialpost->html('post_link')?>"/></div>
		</div>
		<div class="formRow">
                        <div class="grid3"><label>Message/Status:</label></div>
                        <div class="grid9"><textarea rows="8" cols="" name="social_posts[post_description]" class="auto" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 160px;"><?=$socialpost->html('post_description')?></textarea></div>
                    </div>
	</div><!-- //section -->
	<!-- section -->
	<div class="widget grid4">
		<div class="whead">
			<h6>Post Scheduling</h6>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Date:</label></div>
			<div class="grid9"><input type="text" value="<?=$socialpost->publish_date?>" name="social_posts[publish_date]" class="emailerdatepicker" /></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Time:</label></div>
			<div class="grid9">
				<select title="select" value="<?=$socialpost->time?>" name="social_posts[time]"><?php
                                        for ($i = 0; $i < 24; $i++):
                                            ?>
                                            <option
                                            value="<?= $i; ?>" <?php $this->selected($socialpost->time, $i); ?>><?php
                                            if ($i == 0)
                                                echo '12 am';
                                            elseif ($i < 12)
                                                echo "$i am";
                                            elseif ($i == 12)
                                                echo '12 pm';
                                            else
                                                echo ($i - 12) . ' pm';
                                            ?></option><?php
                                        endfor;
                                        ?>
				 </select>
				<!-- @amado this does not seem to be outputting the correct time for central time zone -->
				<span class="note">Current server time: <?php echo date('h:i:s a', time());  ?></span>
			</div>
		</div>
</form>
<form action="<?= $this->action_url('Campaign_save') ?>" class="ajax-save">
    <input type="hidden" name="social_sched[post_id]" value="<?=$socialpost->getID()?>"/>
    <input type="hidden" name="social_sched[post_date]" value="<?=$socialpost->publish_date?>"/>
		<div class="formRow">
			<div class="grid3"><label>Accounts:</label></div>
			<div class="grid9">
            <?php if (!$SAccounts): ?>
            <?php else : ?>
                <?php //var_dump($SAccounts) ?>
                <?php foreach ($SAccounts as $SAccount) : ?>
                    <?php if ($SAccount->token) : ?>
                        <input type="checkbox" 
                               class="check social-account" 
                               <?php echo ($SAccount->active ? 'checked ' : '') ?> 
                               value="1" 
                               name="social_sched[social_id][<?php echo $SAccount->id ?>]" /><?php echo $SAccount->name ?><br />
                    <?php endif ?>
                <?php endforeach ?>
            <?php endif ?>
				<!--<input type="checkbox" class="check" name="" /> Account Name 1<br />
				<input type="checkbox" class="check" name="" /> Account Name 2<br />
				<input type="checkbox" class="check" name="" /> Account Name 3<br />
				<input type="checkbox" class="check" name="" /> Account Name 4<br />
				<input type="checkbox" class="check" name="" /> Account Name 5<br />-->
			</div>
			
		</div>
	</div><!-- //section -->
</div> <!-- //end fluid -->
</form>
<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		
		//===== Date Picker (Only future dates) =====//
		var datepicker = $( ".emailerdatepicker");
		datepicker.change(function(e){
			$('[name="social_sched[post_date]"]').val($(this).val());
		});
		datepicker.datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
	
	});
    
<?php $this->end_script(); ?>
</script>