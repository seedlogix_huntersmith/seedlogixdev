<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Auto Responders</h6>
			<div class="buttonS f_rt jopendialog jopen-createResponder"><span>Create a New Auto Responder</span></div>
            <select data-placeholder="Filter By&hellip;" class="select2" name="filterByContext" id="newContext">
                <option></option>

<?php foreach($Responders as $responder): if(!in_array($responder->responder_context,$responder_array)): ?>

                <option value="<?= $responder->responder_context; ?>"><?= $responder->responder_context; ?></option>

<?php $responder_array[] = $responder->responder_context; endif; endforeach; ?>

            </select>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('EmailMarketing'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'jresponders',
	'data'				=> $Responders,
	'rowkey'			=> 'Responder',
	'rowtemplate'		=> 'campaign-marketing.php',
	'total'				=> $totalEmails,
	'sourceurl'			=> $this->action_url('Campaign_ajaxEmailmarketing?ID='.$Campaign->getID()),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'categoryDataTable'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Auto-Responder POPUP -->
<?php $this->getDialog('create-responder.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){

        var beenopen	    = false;
		var dynSelContainer = $('#dynamicDataRow-emailResponder');
        var dynSelInput     = $('#responder_table_id');

        //checks the dynamic data select (Responder Context) lenght.
        //hides or shows based on empty or has values | Hidden by default
        function checkSelectLength() {
            if( $('#responder_table_id > option').length != 0 ) {
                dynSelContainer.fadeIn('fast');
                $('.ui-dialog-buttonset button:last-child').prop("disabled", false).removeClass("ui-state-disabled");
            }
            else {
                dynSelContainer.hide();
                $('.ui-dialog-buttonset button:last-child').prop("disabled", true).addClass("ui-state-disabled");
            }
        }

        //RESET dialog FORM
        function resetForm() {
            //$('form.jform-create')[0].reset();
            //$.uniform.update(); //update all uniform fields
            //dynSelContainer.show(); //reset to default display
        }

        //===== Create New Form + PopUp =====//
		$( '#jdialog-createResponder' ).dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			open: function (){

                if(beenopen)
                    return;
                beenopen = true;

                checkSelectLength();

                //(Responder Type) select change
                $('#responder_table_name').change(function (){

                    var curType	=	$(this).val();
					var url	= "<?= $this->action_url('Campaign_ajaxGetSites?ID=' . $Campaign->getID()); ?>" + "&pointtype=" + curType;

                    //Send Off input for server response
                    sendAjax(url, function (resp)
                        {
                            if(resp.iTotalRecords == 0)
                            {
                                notie.alert({text:'Please create a Touchpoint of this Category before you create a Responder. Or select a different Touchpoint Type from the list.'});
                                //remove DATA from select
                                $('#responder_table_id > option').each(function() { $(this).remove(); });
                                //this function will find no data - hides row and disables button
                                checkSelectLength();
                                return;
                            }

                            //load DATA into SELECT
                            $("#responder_table_id").html(resp.aaData.join("\n"));

                            //UPDATE UNIFORM SELECT WITH NEW DATA
                            $.uniform.update("#responder_table_id");

                            //Check IF SELECT HAS OPTIONS TO CHOSE
                            checkSelectLength();

                            /*
                             * UNIFORM TIPS / DOCS

                            If you need to change values on the form dynamically
                            you must tell Uniform to update that element’s style.

                            TIP: If you don't mind updating all Uniformed elements
                            or just don’t specifically know which element to update use $.uniform.update();  */

                        },
                        {
                            'forResponder': true,
                            'iDisplayStart': 0,
                            'iDisplayLength': 500
                        },
                        'POST');
				});
			},
			close: function (event, ui)
			{
                // @todo remove if not using
                //$('#campaign_id', this).val('');

                //RESET FORM
                resetForm();

                //REDRAW TABLE
				$('.dTable').dataTable().fnDraw();
			},
			buttons: (
                new CreateDialog('Create',
                function (ajax, status)
                {

                    if(!ajax.error)
                    {
                        //$('.jnoforms').hide();
                        //$('.jresponders').prepend(ajax.row);
						var dt = $('#jresponders');
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.id);
						dt.data('newRowsIds', newRowsIds);
						dt.dataTable().fnDraw();
                        notie.alert({text:ajax.message});
                        this.close();
                    }
                    else if(ajax.error == 'validation')
                        alert(getValidationMessages(ajax.validation).join('\n'));
                    else
                        alert(ajax.message);

				},
                '#jdialog-createResponder')).buttons
		});

        //===== Drop Down Menu Options =====//
//        $('.navi .dropdown-menu a').click(function(e)
//        {
////            var ele = $(this);
////            var clicked = ele.attr('class');
//
////            if( ele.attr('href') == '#' )
////            {
//                e.preventDefault();
////
////                if(clicked == 'Enabled'){
////                    //replace this with save function
////                    console.log('enabled clicked');
////                }
////                else if( clicked == 'Disabled'){
////                    //replace this with save function
////                    console.log('disabled clicked');
////                }
//
//                //set the value to what was clicked
////                ele.parent().parent().prev().html(clicked+'<b class="caret"></b>');
//                //close drop down
//
//			$('li.dropdown.open').parents('.dTable').dataTable().fnDraw();
//			$('li.dropdown.open').removeClass('open');
//
//                return false;
////            }
//        });

        $('#newContext').change(function(){
            $('.dTable').dataTable().fnDraw();
        });

	});


function categoryDataTable(aoData){
    var context = $('select[name="filterByContext"]').val();
    aoData.push({name : "context", value : context});
}


<?php $this->end_script(); ?>
</script>