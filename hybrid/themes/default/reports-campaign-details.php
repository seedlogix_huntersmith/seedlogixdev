<div class="fluid">
    <!-- Bars chart -->
    <div class="widget grid12 chartWrapper">
        <div class="whead">
            <h6>Visits and Leads</h6>
			<div class="buttonS bRed right dynRight jopendialog jopen-createReport">Run & Export</div>
        </div>
        <div class="body"><div class="bars" id="placeholder1"></div></div>
    </div>
</div>

<div class="fluid"> 
	<div class="widget">
		<div class="whead">
			<h6>Source Report</h6>
		</div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tDefault" id="jreports">
            <thead>
                <tr>
                    <td>Source</td>
                    <td>Visits</td>
                    <td>Leads</td>
                    <td>Conversion Rate</td>
                </tr>
            </thead>
            <tbody class="jresponders">
<?php
            $this->getRow('source-reports.php');
?>
            </tbody>
            <tfoot class="fancyFooter">
                <tr>
                    <td>Totals</td>
                    <td><strong class="buttonL bDefault">1258</strong></td>
                    <td><strong class="buttonL bDefault">568</strong></td>
                    <td><strong class="buttonL bDefault">24%</strong></td>
                </tr>
            </tfoot>
        </table>
	</div>
</div><!-- /end fluid -->

<script type="text/javascript">
<?php $this->start_script(); ?>

   
    
$(function () {
    var previousPoint;
 
    var d1 = [];
    for (var i = 0; i <= 10; i += 1)
        d1.push([i, parseInt(Math.random() * 30)]);
 
    var d2 = [];
    for (var i = 0; i <= 10; i += 1)
        d2.push([i, parseInt(Math.random() * 30)]);
 
    var ds = new Array();
 
     ds.push({
        data:d1,
        bars: {
            show: true, 
            barWidth: 0.3, 
            order: 1,
        },
        label: "Visitors"
    });
    ds.push({
        data:d2,
        bars: {
            show: true, 
            barWidth: 0.3, 
            order: 2
        },
        label: "Leads"
    });
                
    //tooltip function
    function showTooltip(x, y, contents, areAbsoluteXY) {
        var rootElt = 'body';
	
        $('<div id="tooltip2" class="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y - 35,
            left: x - 5,
			'z-index': '9999',
			'color': '#fff',
			'font-size': '11px',
            opacity: 0.8
        }).prependTo(rootElt).show();
    }
                
    //Display graph
    $.plot($("#placeholder1"), ds, {
        grid:{
            hoverable:true
        },
        legend: {
            show: true
        }
    });

 
    //add tooltip event
    $("#placeholder1").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
     
                //delete de prГ©cГ©dente tooltip
                $('.tooltip').remove();
     
                var x = item.datapoint[0];
     
                //All the bars concerning a same x value must display a tooltip with this value and not the shifted value
                if(item.series.bars.order){
                    for(var i=0; i < item.series.data.length; i++){
                        if(item.series.data[i][3] == item.datapoint[0])
                            x = item.series.data[i][0];
                    }
                }
     
                var y = item.datapoint[1];
     
                showTooltip(item.pageX+5, item.pageY+5, x + " = " + y);
     
            }
        }
        else {
            $('.tooltip').remove();
            previousPoint = null;
        }
     
    });
 
    
});
    
<?php $this->end_script(); ?>
</script>