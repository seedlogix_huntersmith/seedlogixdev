	<div class="fluid">
		<div class="widget grid12">
			<div class="whead"><h6>Customer Ratings and Reviews</h6></div>
			<div class="formRow"><div class="grid12">
				
				<h5>Turn your word of mouth into a digital lead-generation machine</h5>
<p>We have partnered with the industry leader in customer reviews to oﬀer ALL Alibi Partners a turnkey service that generates ratings and reviews from YOUR customers – and displays on YOUR PowerLeads websites
				</p><br>
				
				<h6>Not subscribed and want to learn more? Go to <a href="http://www.alibipartners.com/marketing/ratings-and-reviews/" class="_blank">http://www.alibipartners.com/marketing/ratings-and-reviews/</a></h6><br><br>
				
				<h6>Already Subscribed? To initiate your customer reviews, follow the 5 simple steps below to upload your happy customers:</h6>
<p>1.	<a href="/csv_uploads/template.csv" class="_blank">Download the .CSV template</a><br>
2.	Add your customer service date(s), customer name(s), email address(es) – in the respective columns<br>
3.	Save the .CSV file to your desktop (or a designated folder on your computer)<br>
4.	Upload that .CSV file by clicking the “+” below, locating the file on your computer, clicking “Open”<br>
5.	Click “UPLOAD” to transmit the file to our partner agency<br>
				</p><br>
<h6>NOTES:</h6>
				<ul>
<li>This portal is 100% secure – and only accessible by you, and our partner agencies</li>
<li>The customer information provided will NEVER be shared with others or used for any purpose other than generating ratings & reviews for your company.</li>
<li>You may upload an unlimited number of customers, at any time & frequency you would like.</li>
					</ul><br><br>
<h6>Have questions or concerns? Send an email to "<a href="mailto:powerleads@alibisecurity.com" class="_blank">powerleads@alibisecurity.com</a>" or call 888.858.1439</h6><br><br>
					

				
				
			<div class="grid8">	
			<!--<form id="SL_FileUploadContainer" action="/upload/csv-upload.php" method="POST" enctype="multipart/form-data">-->
			<form id="SL_FileUploadContainer" enctype="multipart/form-data">
				<input type="hidden" name="user_id" value="<?= $User->id; ?>">
				<input type="hidden" name="csv_upload" value="Upload">
				<div class="formRow noBorderB">
					<div class="grid2"><label>CSV File:</label></div>
					<div class="grid6"><input type="file" id="fileToUpload" name="fileToUpload"></div>
					<!--<div class="grid4"><input id="slFileUploadBtn" type="submit" name="csv_upload" class="sideB bCrimson" value="Upload"></div>-->
					
					<div class="grid2"><input type="button" id="slFileUploadBtn2" class="buttonS f_rt" value="Upload"></div>
				</div>
				<div id="slRespMsgContainer" class="formRow noBorderB" style="display: none; padding-top: 0px; padding-bottom: 0px;">
					<div class="grid2">&nbsp;</div>
					<div class="grid10"><span id="slRespMsg" style="font-size: smaller;"></span></div>
				</div>
				<div class="formRow noBorderB" style="padding-top: 0px; padding-bottom: 5px;">
					<div class="grid2">&nbsp;</div>
					<div class="grid10"><progress style="display: none; width: 322px;"></progress></div>
				</div>
			</form>
				</div>
				
				
				</div></div>
			
		</div>
	</div>


<script type="text/javascript">

<?php $this->start_script(); ?>

$(function(evt){
	$("#slFileUploadBtn2").click(function(evt) {
		
		$.ajax({
			url: '/upload/csv-upload.php', 
			type: 'POST', 
			data: new FormData($('form')[0]), 
			cache: false, 
			contentType: false, 
			processData: false,
			
			xhr: function() {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					// Show progress bar
					$('progress').fadeIn('slow');
					
					// For handling the progress of the upload
					myXhr.upload.addEventListener('progress', function(e) {
						if (e.lengthComputable) {
							$('progress').attr({
								value: e.loaded,
								max: e.total,
							});
						}
					} , false);
				}
				return myXhr;
			}, 
			
			success: function(responseData) {
				console.log(responseData);
				$("#slRespMsg").html(responseData);
				$("#slRespMsgContainer").fadeIn('slow', function() {
					$('progress').fadeOut('slow');
				});
				
			}
		});
	});
	
	/*$('form#SL_FileUploadContainer').off('submit').on('submit',function(e){
		e.preventDefault();
		$.ajax({
			url				: '/upload/csv-upload.php',
			type			: 'POST',
			data			: new FormData($(this)[0]),
			dataType		: 'html',
			cache			: false,
			contentType		: false,
			processData		: false,
			beforeSend		: function(){
							$('<div class="formRow noBorderB"><div class="grid12">Uploading&hellip;</div></div>').appendTo('div#SL_FileUploadContainer').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
								$(this).remove();
							});
			},
			success			: function(data){
							$('<div class="formRow noBorderB"><div class="grid12">' + data + '</div></div>').appendTo('div#SL_FileUploadContainer').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
								$(this).remove();
							});
			},
			error			: function(data){
							$('<div class="formRow noBorderB"><div class="grid12">' + data.responseText + '</div></div>').appendTo('div#SL_FileUploadContainer').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
								$(this).remove();
							});
			}
		});
	});*/
});

<?php $this->end_script(); ?>

</script>