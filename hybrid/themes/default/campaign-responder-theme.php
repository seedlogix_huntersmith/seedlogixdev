<!-- Alternative buttons -->
<div class="fluid">
    <div class="grid6">
        <div class="widget">            
            <div class="whead"><h6>Theme Configuration</h6><div class="clear"></div></div>
            <div class="formRow">
                <div class="grid3"><label>Logo:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Blog Stream:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Background Color:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Links Color:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
                <div class="clear"></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Accent Color:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
                <div class="clear"></div>
            </div>

            <div class="whead"><h6>Call to Action</h6><div class="clear"></div></div>


            <div class="formRow">
                <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
            </div>
            <div class="whead"><h6>Text A</h6><div class="clear"></div></div>

            <div class="formRow">
                <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
            </div>

            <div class="whead"><h6>Text B</h6><div class="clear"></div></div>

            <div class="formRow">
                <textarea name="blog[adv_css]" id="adv_css"><?php $this->p($blog->adv_css) ?></textarea>
            </div>

        </div>
    </div>
    <div class="grid6">


        <?php $this->getMenu('responder-icons.php'); ?>

        <div class="widget">
            <div class="whead"><h6>Themes</h6><div class="clear"></div></div>
            <div class="formRow">
                <ul>
                    <li><a href="#" original-title="Add an article"><img src="<?= $_path; ?>images/icons/middlenav/create.png" alt=""></a></li>
                    <li><a href="#" original-title="Upload files"><img src="<?= $_path; ?>images/icons/middlenav/upload.png" alt=""></a></li>
                    <li><a href="#" original-title="Add something"><img src="<?= $_path; ?>images/icons/middlenav/add.png" alt=""></a></li>
                    <li><a href="#" original-title="Messages"><img src="<?= $_path; ?>images/icons/middlenav/dialogs.png" alt=""></a><strong>8</strong></li>
                    <li><a href="#" original-title="Check statistics"><img src="<?= $_path; ?>images/icons/middlenav/stats.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>

</div>

<!-- //end content container -->
<script type="text/javascript">

    //===== Code Mirror / Syntax highlight =====//
    var editor = CodeMirror.fromTextArea(document.getElementById("adv_css"), {
        lineNumbers: true
    });

</script>