<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Texts Inbox</h6></div>
        <style type="text/css">
        	.tDefault.col_end tr th:last-child, .tDefault.col_end tr td:last-child{
        		width: 50% !important;
        		text-align: left;
        	}
        </style>

		<?php 
		    $this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('InboxTexts'),
					'class' => 'col_1st,col_2nd,col_end',
					'id' => 'jInboxTexts',
					'data' => $InboxTexts,
					'rowkey' => 'InboxText',
					'rowtemplate' => 'inbox-texts.php',
					'total' => $totalRecords,
					'sourceurl' => $this->action_url('campaign_ajaxinboxtexts'),
					'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
		?>

    </div>
</div>


<script type="text/javascript">
	<?php $this->start_script(); ?>
		

	<?php $this->end_script(); ?>
</script>