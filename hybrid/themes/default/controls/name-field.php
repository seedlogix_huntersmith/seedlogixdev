<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
<form action="<?php echo $this->action_url('Campaign_savefield?FID='. $field->getID()); ?>" method="POST" class="ajax-save">
    

    <div class="formRow">
        <div class="grid3"><label>Label 1:</label></div>
        <div class="grid9"><input type="text" name="field[label]" value="<?= $field->label ?>" /></div>
        <div class="clear"></div></div>
    <div class="formRow">
        <div class="grid3"><label>Label 2:</label></div>
        <div class="grid9"><input type="text" name="field[label_note]" value="<?= $field->label_note ?>" /></div>
        <div class="clear"></div>
    </div>

    <div class="formRow">
        <div class="grid3"><label>Default <span class="jHtml_label">First Name</span>:</label></div>
        <div class="grid9"><input type="text" name="field[default]" value="<?= $field->default ?>" /></div>
        <div class="clear"></div></div>
        
    <div class="formRow">
        <div class="grid3"><label>Default <span class="jHtml_label_note">Last Name</span>:</label></div>
        <div class="grid9"><input type="text" name="field[options]" value="<?= $field->options ?>" /></div>
        <div class="clear"></div></div>
        
    <div class="formRow">
        <div class="grid3"><label>Required:</label></div>
        <div class="grid9">
				<select name="field[required]" class="uniformselect no-upload">
					<option value="0" <? if(!$field->required) echo 'selected'; ?>>Neither</option>
					<option value="1" <? if($field->required == 1) echo 'selected'; ?>>Field 1</option>
					<option value="2"<? if($field->required == 2) echo 'selected'; ?>>Field 2</option>
					<option value="3"<? if($field->required == 3) echo 'selected'; ?>>Both</option>
				</select>
        </div><div class="clear"></div>
    </div>

</form>