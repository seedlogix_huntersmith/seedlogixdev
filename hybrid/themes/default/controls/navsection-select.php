<?php
/**
 * this view requires:
 * $selected_nav_id
 *
 */
/**
 * @var $site WebsiteModel
 */
    if(!function_exists('printNavOption')){
        function printNavOption(Template $view, NavModel $nav, $level   =   0, $selected = 0, $currentID = 0){
            if($currentID == $nav->id) return;
?>
            <option <?php $view->selected($selected, $nav->id); ?> value="<?= $nav->id; ?>">
                <?php echo str_repeat("&nbsp;", $level-1) . '&lfloor;'; $view->p($nav->page_title); ?>
            </option>
<?php
            foreach($nav->getNavs() as $subnav)
                if($subnav->getID() != $currentID)
                    printNavOption ($view, $subnav, $level+1, $selected, $currentID);
        }
    }
?>
                <select name="<?php echo $fieldname; ?>" class="" <?=$disable?>>
                    <option value="0">Root</option>
<?php
                    foreach($site->getRootNavs() as $nav) {
//                        if(isset($currentID) && $nav->getID() != $currentID)
                            printNavOption ($this, $nav, 1, isset($selected_nav_id) ? $selected_nav_id : 0, isset($currentID) ? $currentID : 0);
                    }
?>
                </select>