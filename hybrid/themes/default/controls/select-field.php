<form action="<?= $this->action_url('Campaign_savefield?FID='.$field->getID()); ?>" method="POST" class="ajax-save">
	<div class="formRow">
		<div class="grid3"><label>Label:</label></div>
		<div class="grid9"><input type="text" name="field[label]" value="<?= $field->label; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Label Footnote:</label></div>
		<div class="grid9"><input type="text" name="field[label_note]" value="<?= $field->label_note; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Required:</label></div>
		<div class="grid9"><input type="checkbox" name="field[required]" class="i_button" value="1"<?php $this->checked($field->required,1); ?>></div>
	</div>
    <div class="formRow">
        <div class="grid3"><label>Options:</label></div>
        <div class="grid9">
            <div id="create_more" class="buttonS f_rt"><span>Create Another Option Field</span></div>
            <div id="all_options" class="grid12">

<?php
$i = 0;
if($field->getOptions()){
    foreach($field->getOptions() as $key => $value){
        if(empty($value)){
            continue;
        }
        $this->getControl('option.php',array('key' => $key,'value' => $value,'i' => $i));
        $i++;
    }
}
for($i2 = $i; $i2 < $i + 5; $i2++){
    $this->getControl('option.php',array('key' => '','value' => '','i' => $i2));
}
?>

            </div>
        </div>
    </div>
</form>

<script>

<?php $this->start_script(); ?>

$(function(){
    $('#create_more').click(function(event){
        event.preventDefault();
        var _parent     = $('#all_options');
        var _options    = _parent.children().length;
        for(i = _options; i < _options + 1; i++){
            var _new    = $('<div class="formRow"></div>');
            $('<div class="grid2"><label>Option ' + (i + 1) + ':</label></div>').appendTo(_new);
            $('<input type="text" name="field[options][' + i + ']" class="jajax-required">').change(AjaxSaveDataRequest.evt).appendTo(_new).wrap('<div class="grid10"></div>');
            _new.appendTo(_parent).hide().fadeIn(300);
        }
    });
});

<?php $this->end_script(); ?>

</script>