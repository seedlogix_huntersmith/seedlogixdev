<form action="<?= $this->action_url('Campaign_savefield?FID='.$field->getID()); ?>" method="POST" class="ajax-save">
	<div class="formRow">
		<div class="grid3"><label>Label:</label></div>
		<div class="grid9"><input type="text" name="field[label]" value="<?= $field->label; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Label Footnote:</label></div>
		<div class="grid9"><input type="text" name="field[label_note]" value="<?= $field->label_note; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Default Text:</label></div>
		<div class="grid9"><input type="text" name="field[default]" value="<?= $field->default; ?>"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Required:</label></div>
		<div class="grid9"><input type="checkbox" name="field[required]" class="i_button" value="1"<?php $this->checked($field->required,1); ?>></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Size:</label></div>
		<div class="grid2">
			<select name="field[size]" class="select2 no-upload">
				<option value=""<?php $this->selected($field->size,''); ?>>Select Size&hellip;</option>
				<option value="large"<?php $this->selected($field->size,'large'); ?>>Large</option>
				<option value="medium"<?php $this->selected($field->size,'medium'); ?>>Medium</option>
				<option value="small"<?php $this->selected($field->size,'small'); ?>>Small</option>
			</select>
		</div>
	</div>
</form>