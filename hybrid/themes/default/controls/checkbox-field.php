<form action="<?= $this->action_url('Campaign_savefield?FID='.$field->getID()); ?>" method="POST" class="ajax-save">
	<div class="formRow">
		<div class="grid3"><label>Label:</label></div>
		<div class="grid9"><input type="text" name="field[label]" value="<?= $field->label; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Label Footnote:</label></div>
		<div class="grid9"><input type="text" name="field[label_note]" value="<?= $field->label_note; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Required:</label></div>
		<div class="grid9"><input type="checkbox" name="field[required]" class="i_button" value="1"<?php $this->checked($field->required,1); ?>></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Checked by Default:</label></div>
		<div class="grid9"><input type="checkbox" name="field[default]" class="i_button" value="1"<?php $this->checked($field->default,1); ?>></div>
	</div>
</form>