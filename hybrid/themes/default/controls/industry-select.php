<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<select name="<?= $selectname; ?>">
  <option value="0">Please choose an option</option>
  <?php foreach(Defaults::Industries() as $svalue => $category): ?>
  <option value="<?php $this->p($svalue); ?>"<?php $this->selected($svalue, $value); ?>><?php $this->p($category); ?></option>
  <?php endforeach; ?>
</select>
