<form action="<?= $this->action_url('Campaign_savefield?FID='.$field->getID()); ?>" method="POST" class="ajax-save">
	<div class="formRow">
		<div class="grid3"><label>Label:</label></div>
		<div class="grid9"><input type="text" name="field[label]" value="<?= $field->label; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Label Footnote:</label></div>
		<div class="grid9"><input type="text" name="field[label_note]" value="<?= $field->label_note; ?>" class="no-upload"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Default Date:</label></div>
		<div class="grid2">
			<select name="field[size]" class="select2 jOptions no-upload">
				<option value=""<?php $this->selected($field->size,''); ?>>Select Date&hellip;</option>
				<option value="today"<?php $this->selected($field->size,'today'); ?>>Current Date</option>
				<option value="custom"<?php $this->selected($field->size,'custom'); ?>>Custom</option>
			</select>
		</div>
	</div>
	<div class="formRow jDefaultDate<?= $field->size == 'custom' ? '' : ' hide'; ?>">
		<div class="grid3"><label>Date Value:</label></div>
		<div class="grid9"><input type="text" name="field[default]" class="inlinedate" value="<?= $field->default; ?>"></div>
	</div>
	<div class="formRow">
		<div class="grid3"><label>Required:</label></div>
		<div class="grid9"><input type="checkbox" name="field[required]" class="i_button" value="1"<?php $this->checked($field->required,1); ?>></div>
	</div>
</form>

<script>

<?php $this->start_script(); ?>

$(function(){
    $('.jOptions').change(function(){
        var _parent     = $('.jDefaultDate');
        var _date       = $('input[name="field[default]"]');
        var _today      = _date.val('<?= date('d/m/Y'); ?>').change();
        if($(this).val() == 'custom'){
            _parent.fadeIn(300);
            if(!_date.val()){
                _today;
            }
        } else{
            _parent.fadeOut(300);
            if($(this).val() == 'today'){
                _today;
            } else{
                _date.val(null).change();
            }
        }
    });
});

<?php $this->end_script(); ?>

</script>