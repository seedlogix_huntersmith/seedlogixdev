<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

<div class="item2" id="jfield-<?= $field->getID(); ?>">
    <div class="icons">
        <a href="#" class="moveField" title="Up" rel="0" style="margin:0 2px;padding:0px;display:inline;float:left;"><span class="icos-arrowup"></span></a>
        <a href="#" class="moveField" title="Down" rel="0" style="margin:0 2px;padding:0px;display:inline;float:left;"><span class="icos-arrowdown"></span></a>
        <a href="<?= $this->action_url('Campaign_form?ID=' . $campaign_id . '&form=' . $form->id . '&field=' . $field); ?>" class="edit" title="Edit"><span>Edit</span></a>
        <a href="<?= $this->action_url('Campaign_form?ID=' . $campaign_id . '&form=' . $form->id . '&delfield=' . $field); ?>" class="delete deleteField field" rel="0" title="Delete"><span>Delete</span></a>
    </div>
    <span class="icos-photos"></span>
    <?php
    
    $this->p($params['type'] . ' / ');
    $this->p($params['label']);
    
    ?></a>

</div>