<!-- LEFT SIDE NAV -->

<!-- //start content container -->
<div id="twoColContentArea">
                        <form action="<?php echo $this->action_url('Website_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">

		<div class="fluid">
				
				<div class="widget grid6">       
                    <ul class="tabs">
                        <li class="activeTab"><a href="#tabb3">Website Themes</a></li>
                        <li class=""><a href="#tabb4">Base Themes</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content" id="tabb3" style="display: none;">
                         <!-- Website Themes -->            
                        	<div class="gallery">
               <ul><?php foreach($available_themes as $theme_i): ?>
                    <li><a class="lightbox" title="" href="images/big.png">
                                <img alt="" width="115" src="<?= $Branding->getResellerUrl('hubs/themes/thumbnails/', $theme_i->thumbnail); ?>"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="<?php echo $_path; ?>images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="<?php echo $_path; ?>images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li><?php endforeach; ?>
               </ul> 
           </div>

                        </div>
                        <div class="tab_content" id="tabb4" style="display: block;">
                        <!-- Base Themes -->
                        <div class="gallery">
               <ul>
                    
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
               </ul> 
           </div>

                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
			
                                                   
                    <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Design Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Links Color:</label></div>
                        	<div class="grid9"><input type="text" name="website[links_color]" class="colorpicker" value="<?php   ?>"></div>
                                <div class="clear"></div>
                        </div>
                        <div class="formRow">
                        	<div class="grid3"><label>Background color:</label></div>
                        	<div class="grid9"><input type="text" name="website[bg_color]" class="colorpicker" value="<?php   ?>"></div>
                                <div class="clear"></div>
                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Background Image:</label> </div>        <!-- 
                            <div class="grid3 enabled_disabled" style="padding-top:2px;"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div> -->
                            <div class="grid6">
                                <input type="file" class="fileInput" id="fileInput" name="website[logo]" />
                            </div>
                            <div class="clear"></div>
                            <?php if(0 /*$site->logo*/): ?>
                            <img class="preview" src="<?//= $Branding->getUrl('users', $User->id, 'hub', $site->logo); ?>" />
                            <?php endif; ?>
                            <!--
                            <div class="grid5">
                                <div class="uploader" id="uniform-fileInput">
                                	<input type="file" id="fileInput" class="fileInput" size="24" style="opacity: 0;">
                                	<span class="filename" style="-moz-user-select: none;">No file selected</span>
                                	<span class="action" style="-moz-user-select: none;">Choose File</span>
                                </div>
                            </div>  -->
                        </div>
                   		<div class="formRow">
                        	<div class="grid3"><label>(pending feature) Background Image Repeat:</label></div>
                        	<div class="grid9 rpt_toggle">
                                <div class="floatL mr10"><input type="checkbox" id="check6" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                   		</div>
                    </div><!-- //section -->
         </div> <!-- //end fluid -->
         <div class="fluid">           
                    <!-- section -->
                    <div class="widget grid6 ">
                        <div class="whead"><h6>Advanced CSS</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid12"><label>Advanced CSS:</label></div>
                        	<div class="grid9">
                        		<textarea class="codemirror" name="website[adv_css]"><?php  ?></textarea>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
                    <!-- section -->
                    <div class="widget grid6 ">
                        <div class="whead"><h6>Additional Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid12"><label>Use Custom Form or Your Own Autoresponder:</label></div>
                        	<div class="grid9">
                        		<textarea class="codemirror" name="website[add_autoresponder]"><?php  ?></textarea>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
			</div> <!-- //end fluid -->

                </form>
<!-- //end content container -->
</div>
<!-- //end content container -->


<script type="text/javascript">
	
	//===== Image gallery control buttons =====//
	$(".gallery ul li").hover(
		function() { $(this).children(".actions").show("fade", 200); },
		function() { $(this).children(".actions").hide("fade", 200); }
	);	

	//===== Tabs =====//
	$.fn.contentTabs = function(){ 
	
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};
	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	//===== iButtons =====//
	$('.rpt_toggle :checkbox, .rpt_toggle :radio').iButton({
		labelOn: "Repeat",
		labelOff: "No Repeat",
		enableDrag: false
	});
	
	//===== Chosen plugin =====//
	$(".select").chosen();
			
</script>
	