<?php
/**
 * @var Template $this
 * @var LeadFormEmailerModel $Broadcaster
 * @var CampaignModel $Campaign
 */
?>

        <div class="fluid">
            <div class="grid9"><!-- Left Column -->
				<form action="<?= $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
				<input type="hidden" name="broadcast[id]" value="<?= $Broadcaster->getID(); ?>">
                <div class="fluid">
                    <div class="widget grid12">
                        <div class="buttonS no_w_head f_rt jopendialog jopen-previewEmail" id="email-preview"><span>Preview the Email</span></div>
                        <div class="buttonS no_w_head f_rt jopendialog jopen-sendTestEmail"><span>Send a Test Email</span></div>
                        <ul class="tabs">
                            <li class="activeTab"><a href="#tabs-settings">Email Settings</a></li>
                            <li><a href="#tabs-theme" class="themechange">Theme Settings</a></li>
                        </ul>
                        <div class="tab_container">
                            <div class="tab_content" id="tabs-settings">
                                <div class="formRow">
                                    <div class="grid3"><label>From:</label></div>
                                    <div class="grid9">
                                        <input type="text" value="<?php $Broadcaster->html('from_field'); ?>" name="broadcast[from_field]">
                                        <span class="note">Must be in this format: Company Name &lt;email@company.com&gt;</span>
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="grid3"><label>Subject:</label></div>
                                    <div class="grid9"><input type="text" value="<?php $Broadcaster->html('subject'); ?>" name="broadcast[subject]"></div>
                                </div>
                            </div>
                            <div class="tab_content hide" id="tabs-theme">
                                <div class="formRow">
                                    <div class="grid12">
                                        <ul class="bxSlider">
                                            <li class="autoresponder-themes">
                                                <a href="#0" class="jChangeTheme" title="Use Theme" action="save" themeID="0">
                                                    <img src="/emails/images/no-theme.jpg" alt="" width="115" height="407">
                                                </a>
                                                <span class="themeTitle<?= $Broadcaster->theme_id == 0 ? ' actv' : ''; ?>"> Custom </span>
                                            </li>
                                            <li class="autoresponder-themes">
                                                <a href="#1" class="jChangeTheme" title="Use Theme" action="save" themeID="1">
                                                    <img src="/emails/themes/corporate2/images/corporate.jpg" alt="" width="115" height="407">
                                                </a>
                                                <span class="themeTitle<?= $Broadcaster->theme_id == 1 ? ' actv' : ''; ?>"> Corporate </span>
                                            </li>

<?php if($User->parent_id == 6312 || $User->id == 6312): ?>

                                            <li class="autoresponder-themes">
                                                <a href="#3" class="jChangeTheme" title="Use Theme" action="save" themeID="3">
                                                    <img src="/emails/themes/sc/det/images/det-templates.jpg" alt="" width="115" height="407">
                                                </a>
                                                <span class="themeTitle<?= $Broadcaster->theme_id == 3 ? ' actv' : ''; ?>"> Dealer Templates </span>
                                            </li>

<?php endif; ?>

                                        </ul>
                                        <input type="hidden" class="ajax" name="broadcast[theme_id]" value="<?= $Broadcaster->theme_id; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fluid">
                        <div class="widget grid12">
                            <div class="whead"><h6>Message</h6></div>
                            <div class="<?= !$Broadcaster->theme_id == '0' ? '' : 'formRow '; ?>j-dual-ide">

<?php if(!$Broadcaster->theme_id == '0'): ?>

                                    <iframe src="/themes/preview-editor/v1/index.php?bemailid=<?= $Broadcaster->getID(); ?>&themeid=<?= $Broadcaster->theme_id; ?>&column=body&touchpoint=EBROADCAST" width="100%" id="iframe3" marginheight="0" frameborder="0" scrolling="no"></iframe>
                                    <script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
                                    <script type="text/javascript">

                                        iFrameResize({
                                            log                     : true,                  // Enable console logging
                                            enablePublicMethods     : true,                  // Enable methods within iframe hosted page
                                            resizedCallback         : function(messageData){ // Callback fn when resize is received
                                                $('p#callback').html(
                                                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                                                    ' <b>Height:</b> '     + messageData.height +
                                                    ' <b>Width:</b> '      + messageData.width +
                                                    ' <b>Event type:</b> ' + messageData.type
                                                );
                                            },
                                            messageCallback         : function(messageData){ // Callback fn when message is received
                                                $('p#callback').html(
                                                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                                                    ' <b>Message:</b> '    + messageData.message
                                                );
                                                alert(messageData.message);
                                            },
                                            closedCallback         : function(id){ // Callback fn when iFrame is closed
                                                $('p#callback').html(
                                                    '<b>IFrame (</b>'    + id +
                                                    '<b>) removed from page.</b>'
                                                );
                                            }
                                        });


                                    </script>

<?php else: ?>

<?php $this->getWidget('wysiwyg-ide.php',array('fieldname' => 'broadcast[body]','fieldvalue' => $Broadcaster->body)); ?>

<?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
				</form>
            </div>
            <div class="grid3"><!-- Right Column -->
				<form action="<?= $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
				<input type="hidden" name="broadcast[id]" value="<?= $Broadcaster->getID(); ?>">
                <div class="fluid">
                    <div class="widget grid12">
                        <div class="whead"><h6>Enabled?</h6></div>
                        <div class="formRow">
                            <div class="grid12"><input type="checkbox" id="activeCheck" name="broadcast[active]" class="i_button" value="1"<?= $Broadcaster->active ? ' checked' : ''; ?>></div>
                        </div>
                    </div>
                </div>
                <div class="fluid">
                    <div class="widget grid12">
                        <div class="whead"><h6>Settings</h6></div>
                        <div class="formRow">
                            <div class="grid3"><label>Name:</label></div>
                            <div class="grid9"><input type="text" value="<?php $Broadcaster->html('name'); ?>" name="broadcast[name]"></div>
                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Schedule:</label></div>
                            <div class="grid9">
                                <select id="schedMode" class="select2" name="broadcast[sched_mode]">
                                    <option value="date"<?= $Broadcaster->sched_mode == 'date' ? ' selected' : ''; ?>>Specify Date &amp; Time</option>
                                    <option value="now"<?= $Broadcaster->sched_mode == 'now' ? ' selected' : ''; ?>>Send Now</option>
                                </select>
                            </div>
                        </div>
                        <div class="dateTimeRows<?= $Broadcaster->sched_mode == 'date' ? '' : ' hide'; ?>">
                            <div class="formRow">
                                <div class="grid3"><label>Date:</label></div>
                                <div class="grid4"><input type="text" name="broadcast[sched_date]" class="emailerdatepicker" value="<?= $Broadcaster->sched_date; ?>"></div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Time:</label></div>
                                <div class="grid9">
                                    <select class="select2" title="select" name="broadcast[sched_hour]">

<?php for($i = 0; $i < 24; $i++): ?>

                                        <option value="<?= $i; ?>"<?php $this->selected($Broadcaster->sched_hour,$i); ?>>

<?php
if($i == 0){
    echo '12 am';
} elseif($i < 12){
    echo "$i am";
} elseif($i == 12){
    echo '12 pm';
} else{
    echo ($i - 12).' pm';
}
?>
                                        </option>

<?php endfor; ?>

                                    </select>
                                    <span class="note">Current server time: <?= date('h:i:s a',time()); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				</form>
                <div class="fluid">
                    <div class="widget grid12">
                        <div class="whead"><h6>Send Email Now:</h6></div>
                        <div class="formRow"><div class="grid12 center"><p>We strongly recommend sending yourself a test copy before broadcasting this message out to your leads.</p></div></div>
                        <div class="formRow">
                            <div class="grid12">
                                <form method="post" action="<?= $this->action_url('Campaign_sendBroadcast',array('ID' => $Campaign->getID())); ?>" class="ajax-save">
                                    <input type="hidden" name="b" value="<?= $Broadcaster->getID(); ?>">
                                    <input type="hidden" name="sendNow" value="1">
                                    <input class="buttonS f_cr <?= $Broadcaster->active && $Broadcaster->sched_mode == "now" ? "" : "disabled"?>" id="broadcast" type="submit" value="Launch Broadcast" name="broadcast[sendNow]">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Send test mail pop-up -->
<?php $this->getDialog('send-test-email.php'); ?>

<!-- ####
    this will technically be a dialogue, but wil need to pull content from DB so not sure how you want to do this modal
#### -->
<div class="SQmodalWindowContainer dialog" title="List of Leads" id="jdialog-listOfLeads" style="display: none;">
    <?php $this->getList('broadcaster-prospects.php', array('action_url' => $this->action_url('Campaign_emailer-settings?ID='.$Campaign->getID() . '&b=' . $Broadcaster->getID()))); ?>
</div>

<div class="SQmodalWindowContainer dialog" title="Email Preview" id="jdialog-previewEmail" style="display: none;">
    <iframe src="<?php echo $this->action_url('Campaign_emailer-preview?ID='.$Campaign->getID() . '&R=' . $Broadcaster->getID()); ?>" name="previewframe" width="100%" height="600"></iframe>
    <!--	<div style="width:600px; height:400px; background-color:red; color: #fff; padding:25px;">  -->
    <!--        <h1>Email Preview</h1>                                                                 -->
    <!--        <p></p>                                                                                -->
    <!--    </div>                                                                                     -->
</div>
	
<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){



        var broadcastMsg = 'To launch broadcast now set "Schedule" to "Send Now" and "Enable" your broadcast.';
        
        //===== TinyMCE =====//
        <?php $this->getJ('wysiwyg-ide.php'); ?>

        //===== Date Picker (Only future dates) =====//
        $( ".emailerdatepicker" ).datepicker({ dateFormat: "mm/dd/yy", minDate: 0 });
        
         //===== Preview Email Modal =====//
		$( '#jdialog-listOfLeads' ).dialog({
			autoOpen: false,
            modal: false,
            width: 675,
            open: function (){
                sendAjax('<?php echo $this->action_url("Campaign_emailer-settings?ID=$Broadcaster->cid&b=" . $Broadcaster->getID()); ?>',
                    function (response){
                        $('#jdialog-listOfLeads').html(response.prospects);
                        return;
                        for(var n in response.prospects)
                        {
                            var p = response.prospects[n];

                        }
                }, {'viewleads': 1}, 'POST');
            }
		});
        
        //===== Send Test Email =====//
		$( '#jdialog-sendTestEmail' ).dialog({
			autoOpen: false,
			height: 200,
			width: 550,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Send Test Email',
				function (ajax, status)
				{
                    if(!ajax.error)
                    {
                      notie.alert({text:ajax.message});
                      this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	      alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	                   alert(ajax.message);
					
				}, '#jdialog-sendTestEmail')).buttons
		});

        //===== Preview Email Modal =====//
        $("#jdialog-previewEmail").dialog({
            autoOpen: false,
            modal: true,
            width: '90%',
            height: 600,
            open: function () {
                window.previewframe.location.reload();
            }
        });

        $("#email-preview").click(function () {
            $('#jdialog-previewEmail').dialog("open");
        });


        //===== Schedule Enable/Disable Checkbox  =====//
        $('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
            labelOn     : '',
            labelOff    : ''
        });

        //==== Themes Selector Options ====//
        $('#schedMode').change(function(){
            if($(this).val() == 'now')
                $('.dateTimeRows').hide();
            else
                $('.dateTimeRows').show();
        });
        
        //==== Enable/Disable Launch Btn ====//
        $('#activeCheck, #schedMode').change(function(){
            var activeCheck = $('#activeCheck').attr('checked');
            var schedMode = $('#schedMode').val();
            
            if(activeCheck == 'checked' && schedMode == 'now')
                $('#broadcast').removeClass('disabled');
            else
                $('#broadcast').addClass('disabled');
            
        });
        
        //==== Launch Broadcast Btn ====//
        $('#broadcast').click(function(){
            if($(this).hasClass('disabled'))
            {
                notie.alert({text:broadcastMsg,stay:true});
            }else{
                $('#broadcast').addClass('disabled');
				Dashboard.ajaxSubmitForm($(this.form), function(ajax){
					if(!ajax.error){
                        notie.alert({text:ajax.message});
					}
				}, null, $(this));
			}

			return false;
		});

        //===== Tabs =====//
        //$(".tabs").tabs({ active: 1 });

        //===== bx Slider for Themes =====//
        var mySlider;
        mySlider            = $('.bxSlider').bxSlider({
                            slideMargin     : 5,
                            infiniteLoop    : false,
                            pager           : false,
                            minSlides       : 2,
                            maxSlides       : 10,
                            slideWidth      : 115
                            });
        $('.themechange').click(function(){
            // reload bx slider
            mySlider.redrawSlider();
            mySlider.reloadSlider();
        });

        //===== Click Events for themes =====//
        $('.bxSlider a.jChangeTheme').click(function(){
            var action      = $(this).attr('action');
            var themeID     = $(this).attr('href').replace('#','');
            if(action == 'save'){
                var input   = $('input[name="broadcast[theme_id]"]');
                input.val(themeID);
                input.change();
                $(document).ajaxSuccess(function(){
                    location.reload();
                });
                return false;
            } else{
                return false;
            }
        });


	});
    
<?php $this->end_script(); ?>
</script>