<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <form action="<?= $this->action_url('Blog_save?ID='.$blog->id); ?>" method="POST" class="ajax-save">
        <div class="grid12 m_tp">
            <div class="widget">
                <ul class="titleToolbar"><li class="activeTab"><a class="themesFilter" href="#regular-themes">Base Themes</a></li></ul>

<?php $this->getWidget('admin-lock.php',array('fieldname' => 'theme')); ?>

                <div class="formRow">
                    <div class="grid2">
                        <div class="center" style="margin-bottom: 10px;"><strong>Active Theme:</strong> <?= $sitetheme->name ? $sitetheme->name : 'None'; ?></div>
                        <div class="currentTheme"><img src="<?= $sitetheme->getThumbnailURL(); ?>" alt="" width="115"></div>
                    </div>
                    <div class="grid10">
                        <div class="center" style="margin-bottom: 10px;"><strong>All Themes:</strong></div>
                        <ul class="bxSlider">

<?php
foreach($hubthemes as $theme_i){
    $this->getRow('site-theme.php',array('theme_i' => $theme_i));
}
?>

                        </ul>
                    </div>
                    <input type="hidden" class="ajax" name="blog[theme]" value="<?= $sitetheme->id; ?>">
                </div>
            </div>
        </div>
        <div class="grid12">
            <div class="widget grid6">
                <div class="whead"><h6>Design Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Background Color:</label></div>
                    <div class="grid9"><input type="text" value="<?php $this->p($blog->bg_color); ?>" name="blog[bg_color]" class="colorpicker"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Links Color:</label></div>
                    <div class="grid9"><input type="text" name="blog[links_color]" value="<?php $this->p($blog->links_color); ?>" class="colorpicker"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Background Image:</label></div>
                    <div class="grid9">

<?php
$this->getWidget('moxiemanager-filepicker.php',array(
    'name'          => 'blog[bg_img]',
    'value'         => $blog->bg_img,
    'idWidget'      => 'blog-bg-widget',
    'noHost'        => true,
    'path'          => $storagePath,
    'extensions'    => 'jpg,jpeg,png,gif'
));
?>

                    </div>
                </div>
            </div>
            <div class="widget grid6">
                <div class="whead"><h6>Advanced CSS</h6></div>
                <div class="formRow"><textarea name="blog[adv_css]" id="adv_css" class="codemirror"><?php $this->p($blog->adv_css); ?></textarea></div>
            </div>
        </div>
    </form>
</div>

<?php
    if(empty($disable)):
?>
<script type="text/javascript">
<?php $this->start_script(); ?>
$(document).ready(function (){
/* *************************
 *   Theme Slider Code
************************* */
    var mySlider;
    var hiddenThemes;
    
    //===== bx Slider for Themes =====//
    mySlider = $('.bxSlider').bxSlider({
        slideWidth: 115,
        minSlides: 2,
        maxSlides: 10,
        slideMargin: 10
    });
    
//    $('.themesFilter').click(function(){
//        //grab class of clicked button
//        var themeType = $(this).attr('href').replace('#', '.');
//        //remove any themes not of that class and store them
//        if(hiddenThemes) {
//            hiddenThemes.appendTo('.bxSlider');
//            hiddenThemes = false;
//        }
//        hiddenThemes = $('.bxSlider > li:not('+themeType+')').detach();
//        //reload bx slider
//        mySlider.reloadSlider();
//    });
    
    //===== Click Events for themes =====//
    $('.bxSlider a.jChangeTheme').click(function(){
        var action = $(this).attr('action');
        var themeID = $(this).attr('href').replace('#','');

        if (action == "save") {
            var input = $('input[name="blog[theme]"]');
            input.val(themeID);
            input.change();
            //new AjaxSaveDataRequest(input.get(0), themeID);
            $(document).ajaxSuccess(function(){
                setTimeout(function(){
                    location.reload();
                },750);
            });
        }
        console.log(action + ': theme ID '+ themeID);
        return false;
    });

    //===== Codemirror =====//
    var cm1 = CodeMirror.fromTextArea(document.getElementById("adv_css"), {
        lineNumbers: true,
        lineWrapping: true,
        mode: "css",
        styleActiveLine: true,
        viewportMargin: Infinity
        <?= $disable ? ',readOnly: true' : ''?>
    });
    <?php if(!$disable): ?>
    function saveCode(cm, ev){
        var ta    =   cm.getTextArea();
        var srccode   =    cm.getValue();

        new AjaxSaveDataRequest(ta, srccode);
    }

    cm1.on('blur', saveCode);

    <?endif;?>
});
<?php $this->end_script(); ?>
</script>
<?php
    endif;
?>