<?php $canEdit = !$User->can('edit_BLOG',$blog->getID()) ? ' disabled' : ''; ?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <div class="grid12 m_tp">

<?php if($blogTheme->hybrid == 1): ?>

        <div class="widget">
            <div class="whead"><h6>Blog Home Page</h6></div>
            <iframe src="/themes/preview-editor/v1/index.php?blogid=<?= $blog->id; ?>&themeid=<?= $blog->theme; ?>&column=blog_edit_1&touchpoint=BLOG" width="100%" id="iframe1" marginheight="0" frameborder="0" scrolling="no"></iframe>
        </div>
        <div class="widget m_tp">
            <div class="whead"><h6>Global Post Page</h6></div>
            <iframe src="/themes/preview-editor/v1/index.php?blogid=<?= $blog->id; ?>&themeid=<?= $blog->theme; ?>&column=blog_edit_2&touchpoint=BLOG" width="100%" id="iframe2" marginheight="0" frameborder="0" scrolling="no"></iframe>
        </div>
        <div class="widget">
            <div class="whead"><h6>Global Footer</h6></div>
            <iframe src="/themes/preview-editor/v1/index.php?blogid=<?= $blog->id; ?>&themeid=<?= $blog->theme; ?>&column=blog_edit_3&touchpoint=BLOG" width="100%" id="iframe3" marginheight="0" frameborder="0" scrolling="no"></iframe>
        </div>

    </div>
</div>

<script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
<script type="text/javascript">
iFrameResize({
    log                     : true,
    enablePublicMethods     : true,
    resizedCallback         : function(messageData){
                            $('p#callback').html('<b>Frame ID:</b> ' + messageData.iframe.id + ' <b>Height:</b> ' + messageData.height + ' <b>Width:</b> ' + messageData.width + ' <b>Event Type:</b> ' + messageData.type);
                            },
    messageCallback         : function(messageData){
                            $('p#callback').html('<b>Frame ID:</b> ' + messageData.iframe.id + ' <b>Message:</b> ' + messageData.message);
                            alert(messageData.message);
                            },
    closedCallback          : function(id){
                            $('p#callback').html('<b>IFrame (</b>' + id + '<b>) removed from page.</b>');
                            }
});
</script>

<? else: ?>

        <form action="<?= $this->action_url('Blog_save?ID='.$blog->id); ?>" method="POST" class="ajax-save" id="blogRegions">

<?php foreach($fields as $key => $field): ?>

            <div class="widget j-dual-ide">
                <div class="whead"><h6><?= $field['title']; ?></h6></div>
                <div class="formRow">
                    <div class="grid12">
                        <div class="body j-dual-ide">

<?php $this->getWidget('wysiwyg-ide.php',array('fieldname' => "blog[$key]",'fieldvalue' => $field['value'])); ?>

                        </div>
                    </div>
                </div>
            </div>

<?php endforeach; ?>

        </form>

    </div>
</div>

<? endif; ?>

<script type="text/javascript">

<?php $this->start_script(); ?>

$(document).ready(function(){

<?php $this->getJ('wysiwyg-ide.php',array('disable' => !empty($canEdit))); ?>

    var tabHeads            = $('#blogRegions').find('.widget');
    tabHeads.click(function(){
        var codeMirrorObj   = $(this).find('.j-dual-ide').data('codeMirror');
        codeMirrorObj.refresh();
    });
});

<?php $this->end_script(); ?>

</script>