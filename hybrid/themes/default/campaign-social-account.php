<?php
/**
 * @var $this Template
 */
$baseAuthURL = 'http://login.seedlogix.com/api/';
//$baseAuthURL = 'http://seedlogix.dev/api/';
//var_dump($blog);
?>

<form action="<?= $this->action_url('Campaign_save') ?>" class="ajax-save">
    <input type="hidden" name="social_accounts[id]" value="<?= $socialaccount->getID() ?>"/>
    <input type="hidden" name="social_accounts[cid]" value="<?= $socialaccount->cid ?>"/>
    <div class="fluid">
        <div class="widget grid12">
            <div class="whead">
                <h6>Social Account Settings</h6>
            </div>
            <div class="formRow">
                <div class="grid3">
                    <label>Name:</label>
                </div>
                <div class="grid9"><input type="text" name="social_accounts[name]" value="<?= $socialaccount->name ?>"/></div>
            </div>
            
            <!-- FACEBOOK -->
            <?php if ($socialaccount->network == 'FACEBOOK'): ?>
                <div class="formRow">
                    <div class="grid3">
                        <label>Facebook Publishing: <?php if (!$socialaccount->token): ?> <?php else: $fbpost_info = explode("\n", $socialaccount->token); ?> <a class="_blank" href="https://facebook.com/<?php echo $fbpost_info[0]; ?>"><?php echo htmlentities($fbpost_info[1]); ?></a><?php endif; ?></label>
                    </div>
                    <?php if ($socialaccount->token): ?>
                        <div class="grid8">
                            <input type="text" value="<?= $socialaccount->html('token') ?>" name="social_accounts[token]" disabled="disabled" id="facebook">
                        </div>
                        <div class="grid1">
                            <a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="facebook"><span class="delete"></span></a>
                        </div>
                    <?php else: ?>
                        <div class="grid9"><a href="<?= $baseAuthURL ?>facebook-socialaccounts-app.php?aid=<?= $socialaccount->getID() ?>" class="buttonS f_lt _blank"><span>Authorize Facebook</span></a></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            
            <!-- TWITTER -->
            <?php if ($socialaccount->network == 'TWITTER'): ?>           
                <div class="formRow">
                    <div class="grid3">
                        <label>Twitter Publishing: <?php if (!$socialaccount->token): ?> <?php else: list(,, $tw_username) = explode("\n", $socialaccount->token); ?> <a class="_blank" href="https://twitter.com/<?php echo $tw_username; ?>"><?php echo htmlentities($tw_username); ?></a><?php endif; ?></label>
                    </div>
                                                                                                                                                                              <?php if ($socialaccount->token): ?>
                        <div class="grid8">
                            <input type="text" value="<?= $socialaccount->html('token') ?>" name="social_accounts[token]" disabled="disabled" id="twitter">
                        </div>
                        <div class="grid1">
                            <a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="twitter"><span class="delete"></span></a>
                        </div>
                    <?php else: ?>
                        <div class="grid9"><a href="<?= $baseAuthURL ?>twitter-socialaccount-app.php?aid=<?= $socialaccount->getID() ?>" class="buttonS f_lt _blank"><span>Authorize Twitter</span></a></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            
            <!-- LINKEDIN -->
            <?php if ($socialaccount->network == 'LINKEDIN'): ?>           
                <div class="formRow">
                    <div class="grid3">
                        <label>LinkedIn Publishing: 
                            <?php if (!$socialaccount->token): ?>
                                <!-- NO TOKEN -->
                            <?php else: ?>
                                <?php $lipost_info = explode("\n", $socialaccount->token); ?> 
                                <a class="_blank" href="<?php echo urldecode($lipost_info[0]); ?>">
                                    <?php echo htmlentities($lipost_info[1]); ?></a>
                            <?php endif; ?>
                        </label>
                    </div>
                    
                    <?php if ($socialaccount->token): ?>
                        <div class="grid8">
                            <input type="text" value="<?= $socialaccount->html('token') ?>" name="social_accounts[token]" disabled="disabled" id="linkedin">
                        </div>
                        <div class="grid1">
                            <a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="linkedin"><span class="delete"></span></a>
                        </div>
                    <?php else: ?>
                        <div class="grid9"><a href="<?= $baseAuthURL ?>linkedin-socialaccount-app.php?aid=<?= $socialaccount->getID() ?>" class="buttonS f_lt _blank"><span>Authorize LinkedIn</span></a></div>
                    <?php endif; ?>
                    
                </div>
            <?php endif; ?>
            
        </div>
    </div>
</form>

<script type="text/javascript">
<?php $this->start_script(); ?>

    $(function () {

        //allow the user to remove their connection?
        var allowDelete = true;

        $('a.sixQube_remove').click(function (event) {

            var targetName = $(this).attr('role');
            
            if(allowDelete){
                $('#'+targetName).val('').trigger('change');
            }
            else
                notie.alert({text:'Contact your system administrator to unauthorize ' + targetName + '.'});
            event.preventDefault();
            return false;
        });

    });

<?php $this->end_script(); ?>
</script>