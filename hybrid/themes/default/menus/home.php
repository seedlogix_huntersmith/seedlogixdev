<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

    $campaignReport = false;
    
    if( $this->isAction('report') || $this->isAction('reports') || $this->isAction('report-organic') || $this->isAction('report-referrals') || $this->isAction('report-social') )
        $campaignReport = true;
    
    $navmenu = array();
    	$navmenu[] = array($this->isAction('home'), 'icol-chart8', 'Main Dashboard', $this->action_url('Campaign_home'));
    //if($User->can('view_reports'))    
        $navmenu[] = array($this->isAction('reports'), 'icol-excel', 'Reports', $this->action_url('Campaign_reports'));
?>

    <!-- Tabs container -->
    <div id="tab-container" class="tab-container">
        <div id="general">
            <ul class="subNav">
<?php
            foreach($navmenu as $item):
?>
                <li<?= $item[0] ? ' class="activeli"' : ''; ?>><a href="<?= $item[3]; ?>" title=""><span class="titlename"><?= $item[2]; ?></span><span class="chevron"></span></a></li>
<?php
            endforeach; 
?> 
            </ul>
        </div>
    </div>

<?php
/*
if($User->id == 6610 || !$User->can('reports_only')){
    if(isset($viewChart)){
        $this->getWidget('touchpoints.php');
    }
}
*/
?>