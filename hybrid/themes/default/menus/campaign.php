<?php

/**
 * Author:			Amado Martinez, amado@projectivemotion.com
 * Last Updated:	2017-10-25 / Fernando Cardenas, fer.cardenas@seedlogix.com
 * License:
 *
 * @var $this Template
 * @var $User UserWithRolesModel
 */

if( /*$User->can('view_admin') || */($User->can('view_HOME') && ($this->isAction('home') || $this->isAction('landing') || $this->isAction('resources'))) ){
    if($classes['superCircuits-nav']){
        $this->getMenu('home-supercircuits.php');
    }
    if($classes['home-nav']){
        $this->getMenu('home-sales.php');
    }
    return;
}

if($site->id || $blog->id){
    $campaign_id        = $site->id ? $site->cid : $blog->cid;
    $touchpointtypes    = CampaignController::getTouchPointTypes();
}

$m      = date('m');
$y      = date('Y');
$d      = date('t');

$items  = array();

if(!$User->can('crm_only')){
	
	if($User->can('view_emailmarketing') || $User->can('view_customforms'))
    $items[] = array(
        $this->isAction('forms', 'form', 'emailmarketing', 'responder-settings', 'emailbroadcasts', 'emailer-settings', 'social-accounts', 'social-account', 'social-posts', 'social-post-details'),'icol-email', 'Marketing Automation', $this->action_url('Campaign_forms?ID=' . $campaign_id),
        array(
            array($this->isAction('forms', 'form'), 'icos-switcher', 'Campaign Forms', $this->action_url('Campaign_forms?ID=' . $campaign_id)),
            array($this->isAction('emailmarketing', 'responder-settings'), 'icos-create', 'Auto Responders', $this->action_url('Campaign_emailmarketing?ID=' . $campaign_id)),
            array($this->isAction('emailbroadcasts', 'emailer-settings'), 'icos-outbox', 'Email Broadcasts', $this->action_url('Campaign_emailbroadcasts?ID=' . $campaign_id)),
            array($this->isAction('social-accounts', 'social-account', 'social-posts', 'social-post-details'), 'icos-speech3', 'Social Scheduler', $this->action_url('Campaign_social-accounts?ID=' . $campaign_id),
                array(
                    array($this->isAction('social-accounts', 'social-account'), 'icol-blocks', 'Social Accounts', $this->action_url('Campaign_social-accounts?ID=' . $campaign_id)),
                    array($this->isAction('social-posts', 'social-post-details'), 'icol-speech3', 'Scheduled Posts', $this->action_url('Campaign_social-posts?ID=' . $campaign_id))
                )
            )
        )
    );
	
} else {
	
	if($User->can('view_emailmarketing') || $User->can('view_customforms'))
    $items[] = array(
        $this->isAction('forms', 'form', 'emailmarketing', 'responder-settings', 'emailbroadcasts', 'emailer-settings', 'social-accounts', 'social-account', 'social-posts', 'social-post-details'),'icol-email', 'Marketing Automation', $this->action_url('Campaign_forms?ID=' . $campaign_id),
        array(
            array($this->isAction('forms', 'form'), 'icos-switcher', 'Campaign Forms', $this->action_url('Campaign_forms?ID=' . $campaign_id)),
            array($this->isAction('emailmarketing', 'responder-settings'), 'icos-create', 'Auto Responders', $this->action_url('Campaign_emailmarketing?ID=' . $campaign_id)),
            array($this->isAction('emailbroadcasts', 'emailer-settings'), 'icos-outbox', 'Email Broadcasts', $this->action_url('Campaign_emailbroadcasts?ID=' . $campaign_id)),
        )
    );
	
}

if($User->can('view_prospects') || $User->can('view_contacts'))
    $items[] = array($this->isAction('prospects', 'prospect', 'contacts', 'contact', 'accounts', 'account', 'proposals', 'proposal'),
        'icol-users', 'Sales CRM', $this->action_url('Campaign_prospects?ID=' . $campaign_id),
        array(
            array($this->isAction('prospects', 'prospect'), 'icos-user2', 'Leads', $this->action_url('Campaign_prospects?ID=' . $campaign_id)),
            array($this->isAction('accounts', 'account', 'contacts', 'contact', 'proposals', 'proposal'), 'icos-admin2', 'Accounts', $this->action_url('Campaign_accounts?ID=' . $campaign_id)),
        )
    );


    if($User->can('view_reports') && !$User->can('crm_only')){
        $items[] = array($this->isAction('reports', 'report', 'report-organic', 'report-referrals', 'report-social', 'rankings', 'ranking', 'phone', 'phone-reports'),
            'icol-chart8', 'ROI Tracking', $this->action_url('Campaign_reports?ID=' . $campaign_id),
            array(
                array($this->isAction('reports', 'report', 'report-organic', 'report-referrals', 'report-social'), 'icos-excel', 'Campaign Analytics', $this->action_url('Campaign_reports?ID=' . $campaign_id)),
                // SERP Rankings (turned off)
                array($this->isAction('rankings', 'ranking'), 'icos-graph', 'SERP Rankings', $this->action_url('Campaign_rankings?ID=' . $campaign_id)),
                array($this->isAction('phone', 'phone-reports'), 'icos-phonehook', 'Phone', $this->action_url('Campaign_phone-reports?ID=' . $campaign_id.'&rqtype='.$y.'-'.$m.'-01,'.$y.'-'.$m.'-'.$d)),
            )
        );
    }

?>

    <div id="tab-container" class="tab-container">
        <div id="general">
            <ul class="subNav">

<?php if($User->id == 6610 || !$User->can('reports_only') && !$User->can('crm_only')): ?>

                <li<?= $this->isParam('pointtype','') && $this->isAction('dashboard') && !$site->id && !$blog->id ? ' class="activeli"' : ''; ?>>
                    <a href="<?= $this->action_url("Campaign_dashboard?ID=$campaign_id"); ?>" title=""><span class="titlename">Campaign Dashboard</span><span class="chevron"></span></a></li>

<?php endif; ?>

    <!-- Campaign Settings start -->
    <li<?= $this->isAction('settings', 'edit-notification', 'spam', 'phones', 'phoneline', 'emailsettings') && strpos($_SERVER['REQUEST_URI'],'Campaign') !== false ? ' class="activeli"' : ''; ?>>
		<a href="<?= $this->action_url("Campaign_settings?ID=$campaign_id"); ?>" title="" class="exp"><span class="titlename">Settings</span><span class="chevron"></span></a>
        <ul class="hide">
			
			<?php if(!$User->can('crm_only')): ?>
			<li<?= $this->isAction('settings', 'edit-notification') && strpos($_SERVER['REQUEST_URI'],'Campaign') !== false ? ' class="activeli"' : ''; ?>><a href="<?= $this->action_url("Campaign_settings?ID=$campaign_id"); ?>" title=""><span class="titlename">Lead Notifications</span></a></li>
			<?php endif; ?>
			
			<li<?= $this->isAction('emailsettings') && strpos($_SERVER['REQUEST_URI'],'Campaign') !== false ? ' class="activeli"' : ''; ?>><a href="<?= $this->action_url("Campaign_emailsettings?ID=$campaign_id"); ?>" title=""><span class="titlename">Email Settings</span></a></li>
			<li<?= $this->isAction('phones', 'phoneline') && strpos($_SERVER['REQUEST_URI'],'Campaign') !== false ? ' class="activeli"' : ''; ?>><a href="<?= $this->action_url("Campaign_phones?ID=$campaign_id"); ?>" title=""><span class="titlename">Phone Settings</span></a></li>
			
			<!--<li<?= $this->isAction('spam') && strpos($_SERVER['REQUEST_URI'],'Campaign') !== false ? ' class="activeli"' : ''; ?>><a href="<?= $this->action_url("Campaign_spam?ID=$campaign_id"); ?>" title=""><span class="titlename">Spam</span></a></li>-->
        </ul>
    </li>
    <!-- Campaign Settings end -->

<?php if($User->id == 6610 || !$User->can('reports_only') && !$User->can('crm_only')): ?>

				<li<?= $touchpoint_type ? ' class="activeli"' : ''; ?>>
                    <a href="<?= $this->action_url('Campaign_dashboard',array('ID' => $campaign_id,'pointtype' => 'WEBSITE')); ?>" title="" class="exp"><span class="titlename">Production Tools</span><span class="chevron"></span></a>
                    <ul class="hide">

<?php foreach($touchpointtypes as $pointtype => $info): ?>

                            <li<?= $this->isParam('pointtype',$pointtype) || ($this->isParam('pointtype','') && strtoupper($touchpoint_type) == $pointtype) ? ' class="activeli"' : ''; ?>>
                                <a href="<?= $this->action_url('Campaign_dashboard',array('ID' => $campaign_id,'pointtype' => $pointtype)); ?>" title=""><span class="titlename"><?= $info[1]; ?></span></a>
                            </li>

<?php endforeach; ?>

                    </ul>
                </li>

<?php endif; ?>

<?php if($items): foreach($items as $item): ?>

                <li<?= $item[0] ? ' class="activeli"' : ''; ?>>
					<a href="<?= $item[3]; ?>" title="" class="exp"><span class="titlename"><?= $item[2]; ?></span><span class="chevron"></span></a>

<?php if(@$item[4]): ?>

                    <ul class="hide">

<?php foreach($item[4] as $subitem): ?>

                        <li<?= $subitem[0] ? ' class="activeli"' : ''; ?>>
                            <a href="<?= $subitem[3]; ?>" title=""<?= @$subitem[4] ? ' class="exp2"' : ''; ?>><span class="titlename"><?= $subitem[2]; ?></span><?= @$subitem[4] ? '<span class="chevron"></span>' : ''; ?></a>

<?php if(@$subitem[4]): ?>

                            <ul class="hide">

<?php foreach($subitem[4] as $sub2item): ?>

                                <li<?= $sub2item[0] ? ' class="activeli"' : ''; ?>>
                                    <a href="<?= $sub2item[3]; ?>" title=""><span class="titlename"><?= $sub2item[2]; ?></span></a>
                                </li>

<?php endforeach; ?>

                            </ul>

<?php endif; ?>

                        </li>

<?php endforeach; ?>

                    </ul>

<?php endif; ?>

                </li>

<?php endforeach; endif; ?>

            </ul>
        </div>
    </div>