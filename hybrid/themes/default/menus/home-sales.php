<?php
$selected       = ' class="activeli"';

$selects        = array(
                $this->isAction('home','landing'),
                $this->isAction('resources'),
                $this->isAction('crm')
);

$option1        = '/admin.php?action=landing&ctrl=Campaign';
$option2        = '/admin.php?action=resources&ctrl=Campaign';
$option3        = '/admin.php?action=crm&ctrl=Campaign';
?>

<div class="salesBal">
    <div class="balInfo"><span class="bDate"><span><?= date('d'); ?></span><?= date('M Y'); ?></span></div>
    <div class="balAmount">Commissions<span>$<?= number_format($salesTotal,2); ?></span></div>
</div>
<ul class="subNav">
    <li<?= $selects[0] ? $selected : ''; ?>><a href="<?= $option1; ?>" title=""><span class="titlename">Commissions Dashboard</span><span class="chevron"></span></a></li>
    <!--<li<?= $selects[1] ? $selected : ''; ?>><a href="<?= $option2; ?>" title=""><span class="titlename">Resources</span><span class="chevron"></span></a></li>
    <?php if(!$User->can('crm_only')){?><li<?= $selects[2] ? $selected : ''; ?>><a href="<?= $option3; ?>" title=""><span class="titlename">CRM</span><span class="chevron"></span></a></li><?php } ?>-->
</ul>