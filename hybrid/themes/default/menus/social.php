<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


$items  =   array(
        array($this->isAction('forms'),'icol-arrowleft', 'Marketing Automation' , $this->action_url('Campaign_forms?ID=' . $campaign_id)),
        array($this->isAction('social-accounts', 'social-account'), 'icol-blocks', 'Social Accounts', $this->action_url('Campaign_social-accounts?ID=' . $campaign_id)),
        array($this->isAction('social-posts', 'social-post-details'), 'icol-speech3', 'Scheduled Posts', $this->action_url('Campaign_social-posts?ID=' . $campaign_id)),
);

?>

            <!-- Tabs container -->
            <div id="tab-container" class="tab-container">
                <div id="general">
                    <ul class="subNav">
                        <?php    foreach($items as $item): ?>
						<li<?php if ($item[0]) echo ' class="activeli"'; ?>><a href="<?php echo $item[3]; ?>" title=""><span class="chevron"></span><span class="vcentered"><?php echo $item[2]; ?></span></a></li>
						<?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <!-- Sidebar form -->