<?php
$selected       = ' class="activeli"';

$selects        = array(
                $this->isAction('salescrm'),
                $this->isAction('salesfeed'),
                $this->isAction('inboxcalls'),
                $this->isAction('inboxtexts'),
                $this->isAction('inboxvoicemails')
);

$option1        = '/admin.php?action=salescrm&ctrl=Campaign';
$option2        = '/admin.php?action=salesfeed&ctrl=Campaign';
$option3        = '/admin.php?action=inboxcalls&ctrl=Campaign';
$option4        = '/admin.php?action=inboxtexts&ctrl=Campaign';
$option5        = '/admin.php?action=inboxvoicemails&ctrl=Campaign';
?>
<ul class="subNav">
    <li<?= $selects[0] ? $selected : ''; ?>><a href="<?= $option1; ?>" title=""><span class="titlename">Dashboard</span><span class="chevron"></span></a></li>
    <li<?= $selects[1] ? $selected : ''; ?>><a href="<?= $option2; ?>" title=""><span class="titlename">Sales Feed</span><span class="chevron"></span></a></li>
    <li<?= ($selects[2]||$selects[3]||$selects[4]) ? $selected : ''; ?>><a href="<?= $option3; ?>" title="" class="exp"><span class="titlename">Inbox</span><span class="chevron"></span></a>
		<ul class="hide">
		<li <?= $selects[2] ? $selected : '' ?>><a href="<?= $option3; ?>" title=""><span class="titlename">Calls</span></a></li>
		<li <?= $selects[3] ? $selected : '' ?>><a href="<?= $option4; ?>" title=""><span class="titlename">Texts</span></a></li>
		<li <?= $selects[4] ? $selected : '' ?>><a href="<?= $option5; ?>" title=""><span class="titlename">Voicemails</span></a></li>
		</ul>	
	</li>
</ul>