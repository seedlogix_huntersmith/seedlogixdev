<?php
/* 
 * Author:     cory frosch <cory.frosch@gmail.com>
 * License:    
 */
$action_param = array('article' => $dirarticle->tbl_ID);
?>

<ul class="fulldd">
    <li class="has">
        <a title="">
            <span class="icos-view"></span>
            Campaign Settings
            <span><img src="<?php echo $_path; ?>images/elements/control/hasddArrow.png" alt=""></span>
        </a>
        <ul style="display: none;">
            <li><a href="<?= $this->action_url("Campaign_transfer?ID=$campaign_id"); ?>" title=""><span class="icos-shuffle"></span>Transfer Items</a></li>
            <li><a href="<?= $this->action_url("Campaign_domains?ID=$campaign_id"); ?>" title=""><span class="icos-chrome"></span>Domains</a></li>
            <li><a href="<?= $this->action_url("Campaign_crm?ID=$campaign_id"); ?>" title=""><span class="icos-refresh2"></span>CRM Integration</a></li>
            <li class="noBorderB"><a href="<?= $this->action_url("Campaign_settings?ID=$campaign_id"); ?>" title=""><span class="icos-cog4"></span>Settings</a></li>
        </ul>
    </li>
</ul>

<!-- Tabs container -->
<div id="tab-container" class="tab-container">  
    <div class="divider"><span></span></div>
    <div id="general">
        <ul class="subNav">
            <li<?php if($this->isAction('network-listingInfo')) echo ' class="activeli"'; ?>>
                <a href="<?php echo $this->action_url('Campaign_network-listingInfo?ID=' . $campaign_id, $action_param); ?>" title="">
                <span class="icos-list"></span>
                Listing Information</a>
            </li>
            <li<?php if($this->isAction('network-seo')) echo ' class="activeli"'; ?>>
                <a href="<?php echo $this->action_url('Campaign_network-listingInfo?view=seo&ID=' . $campaign_id, $action_param); ?>" title="">
                <span class="icos-search"></span>
                SEO</a>
            </li>
            <li<?php if($this->isAction('network-editRegions')) echo ' class="activeli"'; ?>>
                <a href="<?php echo $this->action_url('Campaign_network-listingInfo?view=er&ID=' . $campaign_id, $action_param); ?>" title="">
                <span class="icos-pencil"></span>
                Edit Regions</a>
            </li>
            <li<?php if($this->isAction('network-photoGallery')) echo ' class="activeli"'; ?>>
                <a title="" href="<?php echo $this->action_url('Campaign_network-listingInfo?view=ph&ID=' . $campaign_id, $action_param); ?>">
                <span class="icos-photos"></span>
                Photo Gallery</a>
            </li>
            <li<?php if($this->isAction('network-socialSpots')) echo ' class="activeli"'; ?>>
                <a title="" href="<?php echo $this->action_url('Campaign_network-listingInfo?view=ss&ID=' . $campaign_id, $action_param); ?>">
                <span class="icos-speech3"></span>
                Social Spots</a>
            </li> 
        </ul>
    </div>
</div>
<!-- Sidebar form -->
<div class="divider"><span></span></div>