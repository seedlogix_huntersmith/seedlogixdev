<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

            <!-- Tabs container -->
            <div id="tab-container" class="tab-container">

                <!--
                <div class="divider"><span></span></div>
                
                <div class="sidePad">
                        <a href="#" title="" class="sideB bLightBlue">Create new Campaign</a>
                    </div>
                -->    
                <div class="divider"><span></span></div>
                
                <div id="general">
                    
                    <ul class="subNav">
                        
                        <li<?php if($this->isAction('dashboard')) echo ' class="activeli"'; ?>>
                        	<a href="<?php echo $this->action_url('Blogs_dashboard', array('ID' => '00000')); ?>" title="">
                        	<span class="icos-list"></span>
                        	Blog Dashboard</a>
                        </li>
                        
                        <li<?php if($this->isAction('settings')) echo ' class="activeli"'; ?>>
                        	<a href="<?php echo $this->action_url('Blogs_settings', array('ID' => '00000')); ?>" title="">
                        	<span class="icos-pencil"></span>
                        	Settings</a>
                        </li>
                        
                        <li<?php if($this->isAction('seo')) echo ' class="activeli"'; ?>>
                        	<a href="<?php echo $this->action_url('Blogs_seo', array('ID' => '00000')); ?>" title="">
                        	<span class="icos-signpost"></span>
                        	SEO</a>
                        </li>

                        <li<?php if($this->isAction('theme')) echo ' class="activeli"'; ?>>
                        	<a title="" href="<?php echo $this->action_url('Blogs_theme', array('ID' => '00000')); ?>">
                            <span class="icos-pencil"></span>
                            Theme</a>
                        </li>
                        
                        <li<?php if($this->isAction('editRegions')) echo ' class="activeli"'; ?>>
                        	<a title="" href="<?php echo $this->action_url('Blogs_editRegions', array('ID' => '00000')); ?>">
                            <span class="icos-pencil"></span>
                            Edit Regions</a>
                        </li>
                        
                        
                        <li<?php if($this->isAction('social')) echo ' class="activeli"'; ?>>
                        	<a title="" href="<?php echo $this->action_url('Blogs_social', array('ID' => '00000')); ?>">
                           <span class="icos-pencil"></span>
                           Social</a>
                        </li>
                        
                        <li<?php if($this->isAction('pages')) echo ' class="activeli"'; ?>>
                            <a title="" href="<?php echo $this->action_url('Blogs_post', array('ID' => '00000')); ?>">
                            <span class="icos-pencil"></span>
                            Post</a>
                        </li>
                          
                    </ul>
                </div>
                
            </div>
            
            <!-- Sidebar form -->
        
            <div class="divider"><span></span></div>
