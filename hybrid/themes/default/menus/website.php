<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


$controller = ucfirst(strtolower($site->touchpoint_type));

switch($touchpoint_type){
    case 'social':
        $site_label = 'Social Site';
        break;
    case 'mobile':
        $site_label = 'Mobile Site';
        break;
    case 'landing':
        $site_label = 'Landing Page';
        break;
    case 'profile':
        $site_label = 'Profile';
        break;
    default:
        $site_label = 'Website';

}


$items = array();
$items[] = array($this->isAction('dashboard'), 'icol-frames', $site_label . ' Dashboard', $this->action_url($controller . '_dashboard?ID=' . $site->id));
$items[] = array($this->isAction('mu-websites'), 'icol-arrowleft', 'All Multi-User Sites', $this->action_url('Admin_mu-websites'));
$items[] = array($this->isAction('settings'), 'icol-settings2', 'Settings', $this->action_url($controller . '_settings?ID=' . $site->id));
$items[] = array($this->isAction('theme'), 'icol-images2', 'Theme', $this->action_url($controller . '_theme?ID=' . $site->id));
$items[] = array($this->isAction('seo'), 'icol-search', 'SEO', $this->action_url($controller . '_seo?ID=' . $site->id));
$items[] = array($this->isAction('editRegions'), 'icol-create2', 'Edit Regions', $this->action_url($controller . '_editRegions?ID=' . $site->id));
$items[] = array($this->isAction('photos'), 'icol-copypaste', 'Slider', $this->action_url($controller . '_photos?ID=' . $site->id));
$items[] = array($this->isAction('social'), 'icol-speech', 'Social', $this->action_url($controller . '_social?ID=' . $site->id));
if($site->touchpoint_type == 'WEBSITE')
$items[] = array($this->isAction('pages') || $this->isAction('page'), 'icol-documents', 'Pages', $this->action_url($controller . '_pages?ID=' . $site->id));




?>

    <!-- Tabs container -->
    <div id="tab-container" class="tab-container">
        <div id="general">    
            <ul class="subNav">
<?php       foreach($items as $item):
                if( (!$multi_user) && ($item[2] == 'All Multi-User Sites') ): //if NOT A multi-user, don't display MU link
                    continue; //skip this menu Item
                elseif ( ($multi_user) && ($item[2] == 'Website Dashboard') ): //if multi-user, don't display dashboard link
                    continue; //skip this menu Item
                else: ?>
                <li<?php if ($item[0]) echo ' class="activeli"'; ?>><a href="<?= $item[3]; ?>" title=""><span class="chevron"></span><span class="vcentered"><?= $item[2]; ?></span></a></li>
<?php           endif;
            endforeach; ?>                          
            </ul>
        </div>
    </div>
<?php
if($User->can('view_admin')):
    $this->getDialog('video.php');
endif;
?>