<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

            <!-- Tabs container -->
            <div id="tab-container" class="tab-container">

                
                <div class="divider"><span></span></div>
                
                <div class="sidePad">
                 <ul class="middleFree">
            <li><a href="#" title="Messages" class="bLightBlue"><span class="iconb" data-icon="&#xe079;"></span><span>Upload Media</span></a></li>
        </ul>
                        
                    </div>
                    
                <div class="divider"><span></span></div>
                
                <div id="general">
                    
                   <ul class="subNav">
                        <li<?php if( $this->isAction('home') ) echo ' class="activeli"'; ?>><a href="<?php echo $this->action_url('Campaign_home'); ?>" title=""><span class="icos-chart8"></span>
                                Campaigns Dashboard</a></li>

                        <li<?php if( $this->isAction('report') ) echo ' class="activeli"'; ?>><a href="<?php echo $this->action_url('Reports_campaigns'); ?>" title=""><span class="icos-excel"></span>
                                Campaigns Reports</a></li>
                       
                    </ul>
                </div>
                
            </div>
            
            <!-- Sidebar form -->
        
            <div class="divider"><span></span></div>