<?php
/*
 * Author:			Amado Martinez, amado@projectivemotion.com
 * Last Updated:	2017-10-25 / Fernando Cardenas, fer.cardenas@seedlogix.com
 * License:
 */

$items          = array(
                array($this->isAction('events'),'','Events',$this->action_url('System_events')),
                array($this->isAction('cron'),'','Cron',$this->action_url('System_cron')),
                array($this->isAction('roles'),'','User Roles',$this->action_url('System_roles')),
                array($this->isAction('debug'),'','Debug',$this->action_url('System_debug')),
                array($this->isAction('systemconfig'),'','System Configuration',$this->action_url('System_systemconfig')),
                array($this->isAction('account-limits'),'','Account Limits',$this->action_url('System_account-limits')),
                array($this->isAction('subscriptions'),'','System Subscriptions',$this->action_url('System_subscriptions'))
                );

if($User->can('view_resellers')){
    $items[]    = array($this->isAction('resellers','reseller'),'','Resellers',$this->action_url('System_resellers'));
}
?>

<div id="tab-container" class="tab-container">
    <div id="general">
        <ul class="subNav">

<?php foreach($items as $item): ?>

            <li<?= $item[0] ? ' class="activeli"' : ''; ?>><a href="<?= $item[3]; ?>" title=""><span class="titlename"><?= $item[2]; ?></span><span class="chevron"></span></a></li>

<?php endforeach; ?>

        </ul>
    </div>
</div>