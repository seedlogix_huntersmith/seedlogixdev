<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

<ul class="middleNavA">
    <li><a href="<?= $this->action_url('Campaign_responder-settings?ID=' . $campaign_id . '&responder=' . $responder->id . '&form=email'); ?>" title="Add an article"><img src="<?= $_path; ?>images/icons/color/pencil.png" alt="" /><span>Email</span></a></li>
    <li><a href="<?= $this->action_url('Campaign_responder-settings?ID=' . $campaign_id . '&responder=' . $responder->id . '&form=theme'); ?>" title="Upload files"><img src="<?= $_path; ?>images/icons/color/drawings.png" alt="" /><span>Email Theme</span></a></li>
    <li><a href="<?= $this->action_url('Campaign_responder-settings?ID=' . $campaign_id . '&responder=' . $responder->id . '&form='); ?>" title="Add something"><img src="<?= $_path; ?>images/icons/color/settings.png" alt="" /><span>Settings</span></a>
    </li>
</ul>