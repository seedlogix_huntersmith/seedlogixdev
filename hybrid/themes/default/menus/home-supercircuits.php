<?php
/**
 * Created by PhpStorm.
 * User: coryf_000
 * Date: 3/30/2015
 * Time: 7:20 PM
 */

$selected       = ' class="activeli"';

$selects        = array(
                $this->isAction('home') || $this->isAction('landing'),
                $this->isAction('resources')
);

$option1        = '/admin.php?action=landing&amp;ctrl=Campaign';
$option2        = '/admin.php?action=resources&amp;ctrl=Campaign';
?>

<div class="secTop salesBal"></div>
<ul class="subNav">
    <li<?= $selects[0] ? $selected : ''; ?>><a href="<?= $option1; ?>" title=""><span class="titlename">Customer Reviews</span><span class="chevron"></span></a></li>
    <li<?= $selects[1] ? $selected : ''; ?>><a href="<?= $option2; ?>" title=""><span class="titlename">Resources</span><span class="chevron"></span></a></li>
</ul>