<?php
/* CSS Classes */
$activeClassLI = ' class="activeli"';
$hasChildMenu = ' class="exp"';
$hideChildNav = ' class="hide"';
$listTag = $anchorTag = '';
//var_dump($User->can('view_branding'));

/* SubNav Menu Booleans */
$isManageAction = $isUserAction = $isMuAction = $isMuWebsite = $isMuForm = $isMuBlogPage = $isRolesAction = $isSpamAction = false;

/* Manage */
if($this->isAction('dashboard','profile','systeminfo','branding') || ($this->isAction('settings') && $site->cid != 99999)){
    $isManageAction = true;
}
$Controller = 'Admin';
$userobj = $User;
if($User->can('view_admin')){
    $items[] = array($this->isAction('dashboard'),'','Dashboard',$this->action_url('Admin_dashboard'));
    //$items[] = array($this->isAction('profile'),'','Admin Settings',$this->action_url($Controller.'_profile?ID='.$userobj->id));
    if($User->can('is_system')){
        $items[] = array($this->isAction('systeminfo'),'','System Info',$this->action_url($Controller.'_systeminfo?ID='.$userobj->id));
    }
    //$items[] = array($this->isAction('settings') && $site->cid != 99999,'','Settings',$this->action_url($Controller.'_settings?ID='.$userobj->id));
    if($User->can('view_branding')) $items[] = array($this->isAction('branding'),'','Branding',$this->action_url($Controller.'_branding?ID='.$userobj->id));
}

/* Users */
if($this->isAction('users','users-suspended','users-canceled','email','profile')){
    $isUserAction = true;
}

/* MultiSite CMS */
if($this->isAction('mu-websites','theme','seo','editRegions','photos','social','pages','page','pagedetails','mu-forms','form','mu-blogs','mu-blog-posts','mu-blog-post') || ($this->isAction('settings') && $site->cid == 99999)){
    $isMuAction = true;
}

/* MU Websites */
if($this->isAction('mu-websites','theme','seo','editRegions','photos','social','pages','page','pagedetails') || ($this->isAction('settings') && $site->cid == 99999)){
    $isMuWebsite = true;
}

/* MU Forms */
if($this->isAction('mu-forms','form')){
    $isMuForm = true;
}

/* MU Blogs */
if($this->isAction('mu-blogs','mu-blog-posts','mu-blog-post')){
    $isMuBlogPage = true;
}

/* Roles */
if($this->isAction('roles')){
    $isRolesAction = true;
}

/* Spam */
if($this->isAction('spam-dashboard')){
    $isSpamAction = true;
}

/* SubNav Menu */
$navmenu = array();
$navmenu[] = array($isUserAction,'','Users',$this->action_url('Admin_users'),array(
    array($this->isAction('users'),'','Active Users',$this->action_url('Admin_users')),
    array($this->isAction('users-suspended'),'','Inactive Users',$this->action_url('Admin_users-suspended'))
));
if($User->id == $User->parent_id){
	$navmenu[] = array($isMuAction,'','MultiSite CMS',$this->action_url('Admin_mu-websites'),array(
		array($isMuWebsite,'','MU Touchpoints',$this->action_url('Admin_mu-websites')),
		array($isMuForm,'','MU Forms',$this->action_url('Admin_mu-forms')),
		array($isMuBlogPage,'','MU Blogs',$this->action_url('Admin_mu-blogs'))
	));
	$navmenu[] = array($isRolesAction,'','Roles',$this->action_url('Admin_roles'),false);	
}
//$navmenu[] = array($isSpamAction,'','Spam',$this->action_url('Admin_spam-dashboard'),false);
?>

<div id="tab-container" class="tab-container">
    <div id="general">
		<?php 
		///disabled 12/19/2018 shows revenue for admins
		/*if($comRevTotal){ 
		<div class="salesBal" style="background: #00aeef!important;box-shadow:">
			<div class="balInfo"><span class="bDate"><span><?= date('d'); ?></span><?= date('M Y'); ?></span></div>
			<div class="balAmount"><? echo date('F'); ?> Revenue<span>$<?= number_format($comRevTotal,0); ?></span></div>
		</div>
		 }*/ ?>
        <ul class="subNav">
            <li<?= $isManageAction ? $activeClassLI : ''; ?>>
                <a href="<?= $this->action_url('Admin_dashboard'); ?>" title=""<?= $User->can('view_admin') ? ' class="exp"' : ''; ?>><span class="titlename">Manage</span><span class="chevron"></span></a>

<?php if($User->can('view_admin')): if($items): ?>

                <ul<?= $hideChildNav; ?>>

<?php foreach($items as $item): ?>

                    <li<?= $item[0] ? $activeClassLI : ''; ?>><a href="<?= $item[3]; ?>" title=""><span class="titlename"><?= $item[2]; ?></span></a></li>

<?php endforeach; ?>

                </ul>

<?php endif; endif; ?>

            </li>

<?php
foreach($navmenu as $item):
    if($item[0] != false){
        $listTag    = $activeClassLI;
    }
    if($item[4] != false){
        $anchorTag  = $hasChildMenu;
    }
?>

            <li<?= $listTag; ?>>
                <a href="<?= $item[3]; ?>" title=""<?= $anchorTag; ?>><span class="titlename"><?= $item[2]; ?></span><span class="chevron"></span></a>

<?php if($item[4] != false): ?>

                <ul<?= $hideChildNav; ?>>

<?php foreach($item[4] as $subitem): ?>

                    <li<?= $subitem[0] ? $activeClassLI : ''; ?>><a href="<?= $subitem[3]; ?>" title=""><span class="titlename"><?= $subitem[2]; ?></span></a></li>

<?php endforeach; ?>

                </ul>

<?php endif; ?>

            </li>

<?php $listTag = $anchorTag = ''; endforeach; ?>

        </ul>
    </div>
</div>