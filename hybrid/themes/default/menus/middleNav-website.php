<?php
/* MU Hack */
if($this->isAction('mu-blog-post')){
                return;
}

$selected       = ' class="slctd-opt"';

$selects        = array(
                $this->isAction('dashboard'),
                $this->isAction('settings'),
                $this->isAction('theme'),
                $this->isAction('seo'),
                $this->isAction('editRegions','editregions'),
                $this->isAction('photos'),
                $this->isAction('social'),
                $this->isAction('pages','page','pagedetails','posts','post')
);

$controller     = $site->touchpoint_type ? ucfirst(mb_strtolower($site->touchpoint_type)) : 'Blog';

$option1        = '/themes/preview/index.php?siteid=';
$option2        = '_dashboard?ID=';
$option3        = '_settings?ID=';
$option4        = '_theme?ID=';
$option5        = '_seo?ID=';
$option6        = $controller != 'Blog' ? '_editRegions?ID=' : '_editregions?ID=';
$option7        = '_photos?ID=';
$option8        = '_social?ID=';
$option9        = $controller != 'Blog' ? '_pages?ID=' : '_posts?ID=';

$tpointid       = $controller != 'Blog' ? $site->id : $blog->id;

$label1         = 'Preview';
$label2         = 'Dashboard';
$label3         = 'Settings';
$label4         = 'Theme';
$label5         = 'SEO';
$label6         = 'Regions';
$label7         = 'Slider';
$label8         = 'Social';
$label9         = $controller != 'Blog' ? 'Pages' : 'Posts';
?>

    <ul class="slogix-tpoint-nav">
        <li><h6><?php /*($multi_user ? 'MU ' : '').($controller == 'Landing' ? 'Landing Page' : $controller);*/ ?>Touchpoint Options</h6></li>

<?php if($controller != 'Blog'): ?>

        <li><a href="<?= $option1.$tpointid; ?>" title="<?= $label1; ?>" class="_blank"><span class="<?= mb_strtolower($label1); ?>"></span><span><?= $label1; ?></span></a></li>

<?php endif; ?>

<?php if($site->cid != 99999): ?>

        <li<?= $selects[0] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option2.$tpointid); ?>" title="<?= $label2; ?>"><span class="<?= mb_strtolower($label2); ?>"></span><span><?= $label2; ?></span></a></li>

<?php endif; ?>

        <li<?= $selects[1] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option3.$tpointid); ?>" title="<?= $label3; ?>"><span class="<?= mb_strtolower($label3); ?>"></span><span><?= $label3; ?></span></a></li>
        <li<?= $selects[2] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option4.$tpointid); ?>" title="<?= $label4; ?>"><span class="<?= mb_strtolower($label4); ?>"></span><span><?= $label4; ?></span></a></li>
        <li<?= $selects[3] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option5.$tpointid); ?>" title="<?= $label5; ?>"><span class="<?= mb_strtolower($label5); ?>"></span><span><?= $label5; ?></span></a></li>
        <li<?= $selects[4] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option6.$tpointid); ?>" title="<?= $label6; ?>"><span class="<?= mb_strtolower($label6); ?>"></span><span><?= $label6; ?></span></a></li>

<?php if($controller != 'Blog'): ?>

        <li<?= $selects[5] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option7.$tpointid); ?>" title="<?= $label7; ?>"><span class="<?= mb_strtolower($label7); ?>"></span><span><?= $label7; ?></span></a></li>

<?php endif; ?>

        <li<?= $selects[6] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option8.$tpointid); ?>" title="<?= $label8; ?>"><span class="<?= mb_strtolower($label8); ?>"></span><span><?= $label8; ?></span></a></li>

<?php if($controller == 'Website' || $controller == 'Blog'): ?>

        <li<?= $selects[7] ? $selected : ''; ?>><a href="<?= $this->action_url($controller.$option9.$tpointid); ?>" title="<?= $label9; ?>"><span class="pages-posts"></span><span><?= $label9; ?></span></a></li>

<?php endif; ?>

    </ul>