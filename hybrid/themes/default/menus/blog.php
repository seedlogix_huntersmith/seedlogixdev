<?php
/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


$items  =   array(
        array($this->isAction('dashboard'), 'icol-frames', 'Blog Dashboard', $this->action_url('Blog_dashboard?ID=' . $blog->id)),
        array($this->isAction('settings'), 'icol-settings2', 'Settings', $this->action_url('Blog_settings?ID=' . $blog->id)),
        array($this->isAction('theme'), 'icol-images2', 'Theme', $this->action_url('Blog_theme?ID=' . $blog->id)),
        array($this->isAction('seo'), 'icol-search', 'SEO', $this->action_url('Blog_seo?ID=' . $blog->id)),
        array($this->isAction('editregions'), 'icol-create2', 'Edit Regions', $this->action_url('Blog_editregions?ID=' . $blog->id)),
        array($this->isAction('social'), 'icol-speech', 'Social', $this->action_url('Blog_social?ID=' . $blog->id)),
        array($this->isAction('posts') || $this->isAction('post'), 'icol-documents', 'Posts', $this->action_url('Blog_posts?ID=' . $blog->id)),
);

?>

            <!-- Tabs container -->
            <div id="tab-container" class="tab-container">
                <div id="general">
                    <ul class="subNav">
<?php foreach($items as $item): ?>
                        <li<?php if ($item[0]) echo ' class="activeli"'; ?>><a href="<?= $item[3]; ?>" title=""><span class="chevron"></span><span class="vcentered"><?= $item[2]; ?></span></a></li>
<?php endforeach; ?>
                    </ul>
                </div>
            </div>