<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

$items  =   array(
        array($this->isParam('status', ''), 'icos-list', 'All Users', $this->action_url('UserManager_list?status=')),
        array($this->isParam('status', 'active'), 'icos-list', 'Active Users', $this->action_url('UserManager_list?status=active')),
        array($this->isParam('status', 'suspended'), 'icos-list', 'Suspended Users', $this->action_url('UserManager_list?status=suspended')),
        array($this->isParam('status', 'canceled'), 'icos-list', 'Canceled Users', $this->action_url('UserManager_list?status=canceled')),
);

?>

<!-- Tabs container -->
<div id="tab-container" class="tab-container">

    <!--
    <div class="divider"><span></span></div>

    <div class="sidePad">
        <a href="#" title="" class="sideB bLightBlue">Create new Campaign</a>
    </div>
    -->
    <div class="divider"><span></span></div>

    <div id="general">

        <ul class="subNav"><?php    foreach($items as $item): ?>
            <li<?php if ($item[0]) echo ' class="activeli"'; ?>>
                <a href="<?php echo $item[3]; ?>" title=""><span class="<?php echo $item[1]; ?>"></span><?php echo $item[2]; ?></a></li><?php
                    endforeach; ?>            
        </ul>
    </div>

    <div class="divider"><span></span></div>
    
    <ul><?php foreach($UserRoles as $Role): ?>
        <li><a href="<?= $this->action_url('UserManager_list?role=' . $Role->ID); ?>">
            <?= $Role->role; ?></a></li><?php
        endforeach; ?>
    </ul>
    
</div>

<!-- Sidebar form -->

<div class="divider"><span></span></div>


