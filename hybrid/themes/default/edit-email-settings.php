<?php

?>
<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save" autocomplete="off">
	<input type="hidden" name="emailsettings[id]" value="<?= $eset->getID(); ?>">
	<input type="hidden" name="emailsettings[cid]" value="<?= $eset->cid; ?>">
	<div class="fluid">
		<div class="widget grid6">
			<div class="whead"><h6>Email Settings Settings</h6></div>
			
		
			<div class="formRow">
				<div class="grid3"><label>SMTP Host:</label></div>
				<div class="grid9"><input type="text" name="emailsettings[host]" value="<?= $eset->host; ?>"></div>
			</div>
			
			<div class="formRow">
				<div class="grid3"><label>Username:</label></div>
				<div class="grid9"><input class="pass" type="text" name="emailsettings[username]" autocomplete="off" value="<?= $eset->username; ?>" readonly></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Password:</label></div>
				<div class="grid9"><input class="pass" type="password" name="emailsettings[password]" autocomplete="off" value="<?= $eset->password; ?>" readonly></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Name:</label></div>
				<div class="grid9">
					<input type="text" name="emailsettings[name]" value="<?= $eset->name; ?>">
					<span class="note">This will display on outging emails that you send.</span>
				</div>
				
			</div>
			
			<div class="formRow">
            <div class="grid3"><label>Email Signature:</label></div>
            <div class="grid9"><div class="j-dual-ide" style="position: relative;">
                    <ul class="tabs hand">
                        <li class="j-codemirror activeTab"><a href="#tabA1">Source Code</a></li>
                        <li class="j-cleditor"><a href="#tabB1">Easy Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content hide" id="tabA1">
                            <textarea class="j-codemirror " name="emailsettings[signa]"><?=$eset->signa?></textarea>
                        </div>
                        <div class="tab_content" id="tabB1">
                            <textarea class="j-cleditor"><?=$eset->signa?></textarea>
                        </div>
                    </div>
                </div>
                </div>
            </div>
			
			
		</div>
	</div>
</form>

<script type="text/javascript">
$( document ).ready(function() {
    setTimeout(function(){
        $(".pass").attr('readonly', false);
        $(".pass").focus();
    },500);
});
<?php $this->start_script(); ?>
//On Page Load
    $(document).ready(function (){

        <?php $this->getJ('wysiwyg-ide.php'); ?>
    
    });
<?php $this->end_script(); ?>
</script>