<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Custom Forms</h6>
			<div class="buttonS f_rt jopendialog jopen-createform"><span>Create a New Form</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('LeadForms'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'leadforms-tbl',
	'data'				=> $forms,
	'rowkey'			=> 'leadform',
	'rowtemplate'		=> 'leadform.php',
	'total'				=> $totalForms,
	'sourceurl'			=> $this->action_url('Campaign_ajaxDataTableForms?ID='.$Campaign->getID()),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<?php $this->getDialog('create-leadform.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {

		//===== Create New Form + PopUp =====//
		$('#jdialog-createform').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
							function(ajax, status)
							{

								if (!ajax.error)
								{
//									$('.jnoforms').hide();
//									$('.jforms').prepend(ajax.row);
									notie.alert({text:ajax.message});
									this.close();
									this.form[0].reset();
									var dt = $('#leadforms-tbl');
									var newRowsIds = dt.data('newRowsIds');
									newRowsIds.unshift(ajax.object.id);
									dt.data('newRowsIds', newRowsIds);
									dt.dataTable().fnDraw(true);
								}
								else if (ajax.error == 'validation')
								{
									alert(getValidationMessages(ajax.validation).join('\n'));
								}
								else
									alert(ajax.message);

							}, '#jdialog-createform')).buttons
		});

	});

<?php $this->end_script(); ?>
</script>