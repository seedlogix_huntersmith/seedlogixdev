<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/24/14
 * Time: 9:13 AM
 */
?>

<div class="widget">
	<div class="whead"><h6>User Subscriptions</h6></div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('AccountLimits'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'accounts-datatable',
	'data'				=> $accounts,
	'rowkey'			=> 'limit',
	'rowtemplate'		=> 'system-account-limits.php',
	'total'				=> $totalLimits,
	'sourceurl'			=> $this->action_url('System_ajaxAccount-limits'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

</div>