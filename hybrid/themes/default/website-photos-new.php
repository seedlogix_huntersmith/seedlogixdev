<!-- //start content container -->
<div id="twoColContentArea" class="editRegions">
<!-- //start content container -->
            <div class="fluid">
                <div class="grid12">
                <br />
                    <?php $this->action_input('jnewslide', 'Website_newslide'); ?>
                    <a href="<?php echo $this->action_url('Website_createslide?ID=' . $site->id); ?>" id="jnewslidebtn" class="buttonS f_lt">Add a New Slide</a>
                    <ul id="jslideslist">
                        
                    <?php foreach($slides as $photo): 
                        
                            if($photo->hasBackground()): ?>
                        <li class="slide" id="slidethumb_<?= $photo->ID; ?>"><a href="#slideanchor_<?php echo $photo->ID; ?>">
                            <img src="<?php echo MediaController::thumbnail_path . $photo->background; ?>" class="thumbsmall" />
                            <div class="thumbsmalldesc"><?php $this->p($photo->title); ?></div></a>
                        </li>
                    <?php
                                        else: ?>
                                <li class="slide jnothumb" id="slidethumb_<?= $photo->ID; ?>"><a href="#slideanchor_<?php echo $photo->ID; ?>">
                                    <div class="thumbsmalldesc"><?php $this->p($photo->title); ?></div>
                                </a></li>
                        <?php 
                                    endif; 
                        endforeach; ?></ul>
                </div>
                <?php //var_dump($site->getSlidePhotos()); ?>
                
                <?php // $this->getTable('mediatbl'); ?>
            </div>

        <div class="fluid">
            <input type="hidden" id="photo-media-action" value="<?= $this->action_url('media_search'); ?>">

<?php
foreach($slides as $slide){
    $this->getWidget('slideform.php',array('slide' => $slide));
}
?>

        </div>
<!-- //end content container -->
</div>
<!-- //end content container -->


<script type="text/javascript">
	
//	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	//===== Collapsible elements management =====//
	

	$('.opened').collapsible({
		defaultOpen: 'opened,toggleOpened',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	$('.closed').collapsible({
		defaultOpen: '',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	//===== iButtons =====//
    $('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
        labelOn     : '',
        labelOff    : '',
        change      : function($input){
                    var value = $input.attr('checked') ? 1 : 0;
                    new AjaxSaveDataRequest($input.get(0),value);
        }
    });

$(document).ready(function (){
    
        <?php $this->getJ('wysiwyg-ide.php'); ?>
    /*
        function makeCodeMirrorTab(i, el)
        {
            var tabid   =   $(this).attr('href');
            var textarea    =   $(tabid + ' textarea.codemirror');
//            textarea.val();
            console.log(textarea, tabid);
            var cMeditor  =   CodeMirror.fromTextArea(textarea.get(0), {
               lineNumbers: true
            });
            cMeditor.on('blur',
                    function (cm, ev){
                          var ta    =   cm.getTextArea();
                          var srccode   =    cm.getValue();

                          new AjaxSaveDataRequest(ta, srccode);
                    }
            );
                $(this).bind('click', function (){
                    cMeditor.setValue('#slideeditor_');
                    setTimeout(function (){cMeditor.refresh();}, 20);
                });
        }
        
        $('.codemirrortab').each(makeCodeMirrorTab);
        */
        // form.ajax-save 
        $('.search-media').each(function (){
            new SearchMediaProvider($, this);
        });
        
        $('.slide_title').change(function (){
            var slideid =   this.id.split(/_/).pop();
            $('#slidethumb_' + slideid + ' div.thumbsmalldesc').text(this.value);
        });
        
        /*
        function clEditorChange(){
            var slideid =   this.$area.attr('id').split(/_/).pop();
            var doc =   this.$frame.contents().find('body').parent().first().next();
//            var src_textarea    =   $('#slidesrc_' + slideid).val(doc.html());
//            console.log(this.$area.context.value);
        }
	$(".jwysiwyg").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true,
		controls:     // controls to add to the toolbar
                        "bold italic underline strikethrough subscript superscript | font size " +
                        "style | color highlight removeformat | bullets numbering | outdent " +
                        "indent | alignleft center alignright justify | undo redo | " +
                        "rule image link unlink | cut copy paste pastetext"
	}).each (function (){
            this.change(clEditorChange);
        });
        */
//        $('form.ajax-save .fileInput').change(InlineUploadRequest.evt);
            
})             				;

</script>

<style>
    .tab_content .CodeMirror { padding: 0px; }
    </style>