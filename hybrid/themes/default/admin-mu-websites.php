<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Multi-User Touchpoints</h6>
			<div class="buttonS f_rt jopendialog jopen-createMultiSite"><span>Create a New Multi-Site</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Multi-Sites'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'websites',
	'data'				=> $sites,
	'rowkey'			=> 'site',
	'rowtemplate'		=> 'admin-multisites.php',
	'total'				=> $total_tp_sites,
	'sourceurl'			=> $this->action_url('admin_ajaxMUWebsites',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<?php $this->getDialog('create-multisite.php'); ?>


<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		
		//===== Create New Form + PopUp =====//
		$( '#jdialog-createMultiSite' ).dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function ()
			{
				var selectType = $('#touchpointType');
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
				selectType.val(selectType.find('option:first').val());
				$('.selector span', this).text(selectType.find('option:first').val());
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
				if(!ajax.error)
				{
					$('.jnoforms').hide();
					var dt =$('#websites').dataTable();
					var newRowsIds = dt.data('newRowsIds');
					newRowsIds.unshift(ajax.object.id);
					dt.fnDraw();

					notie.alert({text:ajax.message});
					this.close();
					this.form[0].reset();
				}
				else if(ajax.error == 'validation')
				{
					alert(getValidationMessages(ajax.validation).join('\n'));
				}
				else
				  alert(ajax.message);
					
			}, '#jdialog-createMultiSite')).buttons
		});
	
	});
    
<?php $this->end_script(); ?>
</script>