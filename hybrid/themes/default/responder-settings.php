<?php $edit = $User->can('edit_responder',$Responder->getID()) ? '' : ' disabled'; ?>

<form action="<?= $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
<input type="hidden" name="autoresponder[id]" value="<?= $Responder->getID(); ?>">

<div class="fluid">
<!-- Left Column -->
    <div class="grid9">
            <div class="widget grid12">
                <div class="buttonS no_w_head f_rt jopendialog jopen-previewEmail" id="email-preview"><span>Preview the Email</span></div>
                <div class="buttonS no_w_head f_rt jopendialog jopen-sendTestEmail"><span>Send a Test Email</span></div>
                <ul class="tabs">
                    <li class="activeTab"><a href="#tabs-settings">Email Settings</a></li>
                    <li><a href="#tabs-theme" class="themechange">Theme Settings</a></li>
                </ul>
                <div class="tab_container">
                    <div class="tab_content" id="tabs-settings">
                        <div class="formRow">
                            <div class="grid3"><label>From:</label></div>
                            <div class="grid9"><input type="text" value="<?php $Responder->html('from_field'); ?>" name="autoresponder[from_field]"<?= $edit; ?>><span class="note">Must be in this format: Company Name &lt;email@company.com&gt;</span></div>
                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Subject:</label></div>
                            <div class="grid9"><input type="text" value="<?php $Responder->html('subject'); ?>" name="autoresponder[subject]"<?= $edit; ?>></div>
                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Return URL:</label></div>
                            <div class="grid9">
                                <input type="text" value="<?php $Responder->html('return_url'); ?>" name="autoresponder[return_url]"<?= $edit; ?>>
                                <span class="note">Optional: Specify the URL a prospect is taken to after submitting the contact form</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab_content hide" id="tabs-theme">
                        <div class="formRow">
                            <div class="grid12">
                                <ul class="bxSlider">
                                    <li class="autoresponder-themes">
                                        <a href="#0" class="jChangeTheme" title="Use Theme" action="save" themeID="0">
                                            <img src="/emails/images/no-theme.jpg" alt="" width="115" height="407">
                                        </a>
                                        <span class="themeTitle<?= $Responder->theme_id == 0 ? ' actv' : ''; ?>"> Custom </span>
                                    </li>
                                    <li class="autoresponder-themes">
                                        <a href="#1" class="jChangeTheme" title="Use Theme" action="save" themeID="1">
                                            <img src="/emails/themes/corporate2/images/corporate.jpg" alt="" width="115" height="407">
                                        </a>
                                        <span class="themeTitle<?= $Responder->theme_id == 1 ? ' actv' : ''; ?>"> Corporate </span>
                                    </li>
                                </ul>
                                <input type="hidden" class="ajax" name="autoresponder[theme_id]" value="<?= $Responder->theme_id; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fluid">
                <div class="widget grid12">
                    <div class="whead"><h6>Email Body</h6></div>
                    <div class="<?= $Responder->theme_id != '0' ? '' : 'formRow '; ?>j-dual-ide">

<?php if($Responder->theme_id != '0'): ?>

                        <iframe src="/themes/preview-editor/v1/index.php?responderid=<?= $Responder->getID();?>&themeid=<?= $Responder->theme_id; ?>&column=body&touchpoint=RESPONDER" width="100%" id="iframe1" marginheight="0" frameborder="0" scrolling="no"></iframe>
                        <script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
                        <script type="text/javascript">

                            iFrameResize({
                                log                     : true,                  // Enable console logging
                                enablePublicMethods     : true,                  // Enable methods within iframe hosted page
                                resizedCallback         : function(messageData){ // Callback fn when resize is received
                                    $('p#callback').html(
                                        '<b>Frame ID:</b> '    + messageData.iframe.id +
                                        ' <b>Height:</b> '     + messageData.height +
                                        ' <b>Width:</b> '      + messageData.width +
                                        ' <b>Event type:</b> ' + messageData.type
                                    );
                                },
                                messageCallback         : function(messageData){ // Callback fn when message is received
                                    $('p#callback').html(
                                        '<b>Frame ID:</b> '    + messageData.iframe.id +
                                        ' <b>Message:</b> '    + messageData.message
                                    );
                                    alert(messageData.message);
                                },
                                closedCallback         : function(id){ // Callback fn when iFrame is closed
                                    $('p#callback').html(
                                        '<b>IFrame (</b>'    + id +
                                        '<b>) removed from page.</b>'
                                    );
                                }
                            });


                        </script>

<?php else: ?>

<?php $this->getWidget('wysiwyg-ide.php',array('fieldname' => 'autoresponder[body]','fieldvalue' => $Responder->body)); ?>

                        <span class="note"><strong>Hint:</strong> You can use "#name" (without quotes) in the body or subject and it will automatically be replaced with the prospect's name when the email is sent.<?= $type == 'custom' ? ' You can also use tags to embed the submitted values for your custom form fields. Go to the custom form editor and click on a field to get its embed tag, which looks like [[form_field]].' : ''; ?></span>

<?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    <!-- RIGHT COLUMN -->
    <div class="grid3">
        <div class="fluid">
          <div class="widget grid12">
                <div class="whead"><h6>Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Name:</label></div>
                    <div class="grid9"><input type="text" value="<?php $Responder->html('name'); ?>" name="autoresponder[name]"<?= $edit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Schedule:</label></div>
                    <div class="grid2"><input type="number" max="60" id="spinner" name="autoresponder[sched]" value="<?php $Responder->html('sched'); ?>"<?= $edit; ?>></div>
                    <div class="grid7">
                        <select id="aresponder_smode" name="autoresponder[sched_mode]"<?= $edit; ?>>
                            <option value="hours"<?php $this->selected('hours',$Responder->sched_mode); ?>>Hours</option>
                            <option value="days"<?php $this->selected('days',$Responder->sched_mode); ?>>Days</option>
                            <option value="weeksts"<?php $this->selected('weeksts',$Responder->sched_mode); ?>>Weeks</option>
                            <option value="months"<?php $this->selected('months',$Responder->sched_mode); ?>>Month</option>
                        </select>
                    </div>
                </div>
            </div>  
        </div>
        <div class="fluid">
            <div class="widget grid12">
                <div class="whead"><h6>Enabled?</h6></div>
                <div class="formRow">
                    <div class="grid12"><input type="checkbox" id="check1" name="autoresponder[active]" class="i_button" value="1"<?= $Responder->active == 1 ? ' checked' : ''; ?><?= $edit; ?>></div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /fluid -->

</form>

<!-- Send test mail pop-up -->
<?php $this->getDialog('send-test-email.php',array('action_url' => $this->action_url('Campaign_send-test-responder?ID='.$Campaign->getID().'&R='.$Responder->getID()))); ?>

<!--    @todo this will technically be a dialogue, 
        but wil need to pull content from the fields on the page 
        so not sure how you want to do this modal 
-->
<div class="SQmodalWindowContainer dialog" title="Email Preview" id="jdialog-previewEmail" style="display: none;"><iframe src="<?= $this->action_url('Campaign_responder-preview?ID='.$Campaign->getID().'&R='.$Responder->getID()); ?>" name="previewframe" width="100%" height="600"></iframe></div>

<script type="text/javascript">
<?php $this->start_script(); ?>
    $(document).ready(function (){

        
        <?php $this->getJ('wysiwyg-ide.php', array('disable' => !empty($disable))); ?>
		var tabHeads = $('#WebsiteRegions').find('.widget');
		tabHeads.click(function(){
			var codeMirrorObj = $(this).find('.j-dual-ide').data('codeMirror');
			codeMirrorObj.refresh();
		});

        $('#theme_selector').val('<?= $Responder->html('theme_id'); ?>');

        //===== Send Test Email =====//
		$( '#jdialog-sendTestEmail' ).dialog({
			autoOpen: false,
			height: 200,
			width: 550,
			modal: true,
			close: function () {
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Send Test Email',
				function (ajax, status) {
                    if(!ajax.error) {
                      notie.alert({text:ajax.message});
                      this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation') {
	        	      alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	                   alert(ajax.message);
					
				}, '#jdialog-sendTestEmail')).buttons
		});

        $("#email-review").fancybox({
            ajax : {
                type : "POST",
                data : "mydata=test"
            }
        });

        //===== Preview Email Modal =====//
		$("#jdialog-previewEmail").dialog({
			autoOpen: false,
            modal: true,
            width: '90%',
            height: 600,
            open: function () {
                window.previewframe.location.reload();
            }
		});

        $("#email-preview").click(function () {
            $('#jdialog-previewEmail').dialog("open");
        });
        
		//===== Schedule Input Spinner  =====//
		$( "#spinner" ).spinner({
            min: 0,
            max: 60,
            step: 0.1,
            numberFormat: "n"
        });

		$("#spinner").on("spinchange", AjaxSaveDataRequest.evt);


        //===== Schedule Enable/Disable Checkbox  =====//
        $('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
            labelOn     : '',
            labelOff    : ''
        });

        //===== Tabs =====//
		//$(".tabs").tabs({ active: 1 });

        //===== bx Slider for Themes =====//
        var mySlider;
        mySlider            = $('.bxSlider').bxSlider({
                            slideMargin     : 5,
                            infiniteLoop    : false,
                            pager           : false,
                            minSlides       : 2,
                            maxSlides       : 10,
                            slideWidth      : 115
                            });
        $('.themechange').click(function(){
            // reload bx slider
            mySlider.redrawSlider();
            mySlider.reloadSlider();
        });

        //===== Click Events for themes =====//
        $('.bxSlider a.jChangeTheme').click(function(){
            var action      = $(this).attr('action');
            var themeID     = $(this).attr('href').replace('#','');
            if(action == 'save'){
                var input   = $('input[name="autoresponder[theme_id]"]');
                input.val(themeID);
                input.change();
                $(document).ajaxSuccess(function(){
                    location.reload();
                });
                return false;
            } else{
                return false;
            }
        });

        //==== Themes Selector Options ====//
        $('#theme_selector').change(function(){
            var selection = $(this).val();
            //if(selection == 'none') {
               //$(".corporate_options").hide();
            //}
            //else {
                //$(".corporate_options").show();
                //for (var key in themeOptions[selection]) {
                    //if (themeOptions[selection].hasOwnProperty(key)) {
                       // $("#"+key).val(themeOptions[selection][key])
                   // }
                //}
            //}
            $(document).ajaxSuccess(function(){
                if(selection) location.reload();
            });

        });
	
	});
<?php $this->end_script(); ?>
</script>