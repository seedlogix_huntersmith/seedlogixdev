<?php
//    var_dump($events);
?>

<div class="fluid">
    <div class="widget">
        <div class="whead"><h6>System Events</h6></div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Events'),
	'class'				=> '',
	'id'				=> 'dynamic',
	'data'				=> $events,
	'rowkey'			=> 'event',
	'rowtemplate'		=> 'system-events.php',
	'total'				=> $totalEvents,
	'sourceurl'			=> $this->action_url('System_ajaxSystemEvents'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div>
</div>