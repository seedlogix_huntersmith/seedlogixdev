<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
<input name="exportURL" type="hidden" class="action_url" value="<?php echo $this->action_url('piwikAnalytics_getCSVData?chart=TableCompareChartsData&period=PERIOD&interval=INTERVAL');?>" />	
	<!-- ##### Main Dashboard Chart ##### -->
<form action="<?php echo $this->action_url('piwikAnalytics_ajaxGetChartData'); ?>" method="POST">
    <input type="hidden" name="id" value="<?php echo $site->id; ?>" />
    <input type="hidden" name="interval" value=""/><input type="hidden" name="period" value=""/>
    <input type="hidden" name="chart" value="LeadsVisitsChartData"/>
    <input type="hidden" name="touchpointType" value="<?=$touchpoint_type?>"/>
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads</h6>
                <?php $this->getWidget('graph-range-select.php', array('id' => 'sitechart', 'value' => Cookie::getVal('sitechart', '7-DAY'))); ?>
		<div class="clear"></div>
	</div>
	<div class="body"><div class="chart"></div></div>
</div>
</form>
	
    <div class="grid12">

<?php $this->getTable('website-prospects.php'); ?>

    </div>
    <div class="grid12">

<?php $this->getWidget('top-performing-keywords.php'); ?>

<?php $this->getWidget('top-ranking-keywords.php'); ?>

    </div>
</div>

<script type="text/javascript">
	
 $(function (){
     
            var chartdata   =   <?php echo json_encode($chart->getDataSets()); ?>;
    if(chartdata == false)
        Dashboard.fetchGraph.call($('#chartconfig')[0]);
    else
        Dashboard.reqsettings.success({"pw": chartdata, "params": <?php echo json_encode($chart->getConfig()); ?> });
	
    
 });
	
	
 
</script>
