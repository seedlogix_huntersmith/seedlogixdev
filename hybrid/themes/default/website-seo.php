<?php

	$gridLength = 'grid9';

	$disable = $User->can('edit_' . $site->getTouchpointType(), $site->getID()) ? "" : "disabled";

	if($multi_user){
		$gridLength = 'grid8';
	}
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

<form action="<?= $this->action_url("{$site->touchpoint_type}_save?ID=".$site->id); ?>" method="POST" class="ajax-save" id="jwebsite-seo">
<div class="grid12 m_tp">
	<div class="widget grid6">       
		<ul class="tabs">
			<li class="activeTab"><a href="#tabb3">Meta Configuration</a></li>

<?php if($site->touchpoint_type != 'PROFILE'): ?>

			<li><a href="#tabb4">Tag Configuration</a></li>

<?php endif; ?>

		</ul>
		<div class="tab_container">
			<div class="tab_content" id="tabb3">
                <!-- meta config tab -->
                <div class="formRow">
                    <div class="grid3"><label>Site Title:</label></div>
					<?php $this->getWidget('input-website.php', array('name' => 'site_title', 'value' => $site->site_title))?>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Meta Keywords:</label></div>
					<?php $this->getWidget('input-website.php', array('id' => 'meta_keywords', 'name' => 'meta_keywords', 'value' => $site->meta_keywords, 'class' => 'tags'))?>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Meta Description:</label></div>
					<?php $this->getWidget('input-website.php', array('name' => 'meta_description', 'value' => $site->meta_description))?>
                </div>
                <!-- //end meta config tab -->
			</div>

<?php if($site->touchpoint_type != 'PROFILE'): ?>

			<div class="tab_content hide" id="tabb4">
				<p>If you are setting up a base site that will have custom child elements such as location data or agent data you can use the following tags to generate dynamic content for each child site.</p>
				<ul class="bullets">
					<li><strong>((city))</strong> - City</li>
					<li><strong>((state))</strong> - ST</li>
					<li><strong>((citycommastate))</strong> - City, State</li>
					<li><strong>((fullstate))</strong> - State</li>
					<li><strong>((cityorfullstate))</strong> - City or State (if no city set in settings will show full State)</li>
					<li><strong>((incitycommastate))</strong> - in City, State</li>
					<li><strong>((address))</strong> - Address</li>
					<li><strong>((zip))</strong> - Zip Code</li>
					<li><strong>((phone))</strong> - Phone</li>
					<li><strong>((domain))</strong> - Domain</li>
					<li><strong>((blog))</strong> - Blog URL</li>
					<li><strong>((blogfeed))</strong> - show posts from connected Blog</li>
					<li><strong>((facebook))</strong> - Facebook URL</li>
					<li><strong>((twitter))</strong> - Twitter URL</li>
					<li><strong>((linkedin))</strong> - LinkedIn URL</li>
					<li><strong>((google))</strong> - Google Plus URL</li>
				</ul>
			</div>

<?php endif; ?>

		</div>
	</div>

	<!-- section -->               
	<div class="widget grid6">
		<div class="whead">
			<h6>Technical Settings</h6>
		</div>

<?php if($site->touchpoint_type != 'PROFILE'): ?>

		<div class="formRow">
			<div class="grid3"><label>WWW Options:</label></div>

<?php
$this->getWidget('input-website.php',array(
	'type'			=> 'select',
	'name'			=> 'www_options',
	'value'			=> $site->www_options,
	'options'		=> array(
					'Both'			=> 'both',
					'No WWW'		=> 'no_www',
					'WWW only'		=> 'www_only'
					)
));
?>

		</div>

<?php endif; ?>

		<div class="formRow"><div class="grid11"><label>Google Website Tools:</label></div></div>
		<div class="formRow">
			<?php $this->getWidget('input-website.php', array('maxGrid' => 12, 'widget' => 'codemirror.php', 'widgetParams' => array('id' => 'code'),'name' => 'google_webmaster', 'value' => $site->google_webmaster))?>
		</div>
		
		<div class="formRow"><div class="grid11"><label>Additional Tracking Code:</label></div></div>
		<div class="formRow">
			<?php $this->getWidget('input-website.php', array('maxGrid' => 12, 'widget' => 'codemirror.php', 'widgetParams' => array('id' => 'code2'),'name' => 'google_tracking', 'value' => $site->google_tracking))?>
		</div>

<?php if($sitetheme->hybrid == 1 && $site->touchpoint_type != 'PROFILE'): ?>

		<div class="formRow"><div class="grid11"><label>301 Redirects:</label></div></div>
		<div class="formRow">
			<?php $this->getWidget('input-website.php', array('maxGrid' => 12, 'widget' => 'codemirror.php', 'widgetParams' => array('id' => 'code3'),'name' => 'edit_region_6', 'value' => $site->edit_region_6))?>
		</div>
			<div class="whead exp3">
				<h6>301 Redirect</h6>
				<span class="chevron"></span>
			</div>
			<div class="formRow hide">
				<p><strong>Format:</strong> old/url,/new-url/<strong class="red">&vert;</strong>2old/2url,/new2-url2 (separate each new redirect with a vertical bar, <strong class="red">&vert;</strong>)</p>
				<p><strong>Example:</strong></p>
				<ul class="bullets">
					<li>Old URL -----&gt; http://olddomain.com/services/</li>
					<li>New URL -----&gt; http://newdomain.com/our-services/</li>
				</ul>
				<ul class="bullets">
					<li>Old URL2 -----&gt; http://olddomain.com/services/marketing.html</li>
					<li>New URL2 -----&gt; http://newdomain.com/our-services/marketing/</li>
				</ul>
				<ul class="bullets">
					<li>Same URL -----&gt; http://samedomain.com/services/sales/</li>
					<li>Same URL -----&gt; http://samedomain.com/our-services/sales-marketing/</li>
				</ul>
				<p><strong>End Result:</strong> services,http://newdomain.com/our-services/<strong class="red">&vert;</strong>services/marketing.html,http://newdomain.com/our-services/marketing/<strong class="red">&vert;</strong>services/sales,/our-services/sales-marketing/&hellip;</p>
			</div>

<?php endif; ?>

	</div><!-- //section -->
</div>
	<input type="hidden" value="<?= $site->touchpoint_type; ?>" name="website[touchpoint_type]">
</form>
</div><!-- //end fluid -->


<script type="text/javascript">

<?php $this->start_script(); ?>

$(document).ready(function(){

<?php $this->getJ('admin-lock.php'); ?>

});

<?php $this->end_script(); ?>

</script>