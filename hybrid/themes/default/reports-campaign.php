w<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Reports</h6>
			<div class="mr45">
				<div class="buttonS bGreen right dynRight jopendialog jopen-createReport">Create Report</div>
			</div>
		</div>
		<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jreports">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Type</td>
                    <td>Campaign</td>
                    <td>Report Date</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody class="jresponders">

<?php $this->getRow('reports-campaign.php'); ?>

			</tbody>
		</table>
	</div>
</div><!-- /end fluid -->

<!-- Create Auto-Responder POPUP -->
<?php $this->getDialog('create-report.php'); // @amado pass extra var to set whether it should use campaigns or touchpoints or setup that var in Reports Controller ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){

        
        $('#sq-listingType').change(function(){
            if( $(this).val() == 'custom' )   
                $('#sq-listingTypeRange').show();
            else
                $('#sq-listingTypeRange').hide();
        });
        
		//===== Create New Form + PopUp =====//
		$( '#jdialog-createReport' ).dialog(
		{
			autoOpen: false,
			height: 450,
			width: 550,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create Report',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
	          $('.jnoforms').hide();
	          $('.jreports').prepend(ajax.row);
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createReport')).buttons
		});
	
	});
    
<?php $this->end_script(); ?>
</script>