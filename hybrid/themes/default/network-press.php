<form action="<?php echo $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
<input type="hidden" name="listing[type]" value="<?php echo $pressarticle->getNetworkListingType(); ?>" />
<input type="hidden" name="listing[id]" value="<?php echo $pressarticle->getID(); ?>" />
<div class="fluid">
    <div class="grid9"><!-- Left Column -->
      <div class="fluid">
        <div class="widget grid12">
          <div class="whead"><h6><span class="jHtml_name"><?php $pressarticle->html('name'); ?></span> Information</h6><div class="clear"></div></div>
          <div class="formRow">
            <div class="grid3"><label>Title:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('name'); ?>" name="listing[name]" /></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>Summary:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('summary'); ?>" name="listing[summary]" /></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>News Body:</label></div>
            <div class="grid9"><textarea rows="12" cols="" name="listing[body]"><?php
                $pressarticle->html('body');
                ?></textarea></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>Keywords:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('keywords'); ?>" name="listing[keywords]" /></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>Website URL:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('website'); ?>" name="listing[website]" /></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>Company Name / Author:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('author'); ?>" name="listing[author]" /></div>
            <div class="clear"></div>
          </div>
          <div class="formRow">
            <div class="grid3"><label>Email:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('email'); ?>" name="listing[email]" /></div>
            <div class="clear"></div>
          </div><?php /** @cory what is this? I cant find where this is supposed to be saved. Please skype me and explain what this is used for thanks.
                  <div class="formRow">
                  <div class="grid3"><label>Additional Information</label></div>
                  <div class="grid9"><textarea rows="8" cols="" name="textarea"></textarea></div>
                  <div class="clear"></div>
                  </div>
                 * 
                 */ ?>
        </div>
      </div>
    </div>
    <div class="grid3"><!-- Right Column -->
      <?php /**
       * @todo VERSION 1.1
       * ?>
      <div class="fluid">
        <div class="widget grid12">
          <div class="whead"><h6>Publish</h6><div class="clear"></div></div>
          <div class="formRow">
            <div class="grid12 yes_no">
              <input type="checkbox" id="check1" name="chbox1" />
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <?php **
       * 
       */ ?>
      <div class="fluid">
        <div class="widget grid12">
          <div class="whead"><h6>Industry</h6><div class="clear"></div></div>
          <div class="formRow">
            <div class="grid12">

              <?php $this->getControl('industry-select.php', array('selectname' => 'listing[category]',
                    'value' => $pressarticle->category)); ?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="fluid">
        <div class="widget grid12">
          <div class="whead"><h6>Business Logo</h6><div class="clear"></div></div>
          <div class="formRow"><?php
            $widgets['photoupload']->displayInstance('photo-upload.php', array('name' => 'listing[thumb_id]', // why is this called thumb_id ? f knows.
                'value' => $pressarticle->thumb_id));
            ?>
          </div>
        </div>
      </div>
      <div class="fluid chosen_dropfix">
        <div class="widget grid12">
          <div class="whead"><h6>City and State aren't shown on your press release but will help us categorize it. </h6><div class="clear"></div></div>
          <div class="formRow">
            <div class="grid3"><label>City:</label></div>
            <div class="grid9"><input type="text" value="<?php $pressarticle->html('city'); ?>" name="listing[city]" /></div>
            <div class="clear"></div>
          </div>

          <div class="formRow">
            <div class="grid3">
              <lable>State</lable>
            </div>
            <div class="grid9">
              <?php $this->getWidget('states-dropdown.php', array('name' => 'listing[state]', 'value' => $pressarticle->state)); ?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  
</div> <!-- //end fluid -->
</form>