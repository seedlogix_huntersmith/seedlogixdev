<div class="fluid">
    <div class="widget grid12">
        <div class="whead"><h6>Report: <?php $T_Report->html('name'); ?> (<?= ucfirst(strtolower($T_Report->type)); ?>) &mdash; Referrals</h6></div>
        <div class="body">
            <div class="formRow">
                <div class="grid1"><label>Filter Report:</label></div>
                <div class="grid4">

<?php
$this->getWidget('select-ajax.php',array(
    'id'            => 'websites',
    'name'          => 'website',
    'ajaxUrl'       => $this->action_url('Campaign_ajaxReportsWebsites',array('report_ID' => $T_Report->getID())),
    'field_value'   => "filter",
    'text'          => ":website:"
));
?>

                </div>

<?php if($T_Report->isCustom()): ?>

                <div class="grid7 m_lt">
                    <form action="<?= $this->action_url('Campaign_save'); ?>" id="dateRange">
                        <div class="grid2"><label>Custom Range:</label></div>
                        <div class="grid5"><input name="report[start_date]" type="text" id="fromDate" placeholder="min Date" value="<?= date('m/d/Y',strtotime($T_Report->start_date)); ?>"></div>
                        <div class="grid5 m_lt"><input name="report[end_date]" type="text" id="toDate" placeholder="max Date" value="<?= date('m/d/Y',strtotime($T_Report->end_date)); ?>"></div>
                        <input type="hidden" name="report[ID]" value="<?= $T_Report->getID(); ?>">
                    </form>
                </div>

<?php endif; ?>

            </div>
        </div>
    </div>
</div>

<div class="fluid">
    <!-- Bars chart -->
    <div class="widget grid12 chartWrapper">
        <div class="whead"><h6>Visits &amp; Leads</h6></div>
        <div class="body"><div class="bars" id="placeholder1"></div></div>
    </div>
</div>

<div class="fluid">
	<div class="widget check">
		<div class="whead"><h6>Stats</h6></div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('ReportReferrals'),
	'class'				=> '',
	'id'				=> 'jreports',
	'data'				=> $tableStats->ReferralSearch,
	'rowkey'			=> 'stats',
	'rowtemplate'		=> 'report-referrals.php',
	'total'				=> $tableStats->totalSearchRows,
	'sourceurl'			=> $this->action_url('Campaign_ajaxReferrals',array('R_ID' => $T_Report->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'datatableDraw',
    'footer'            => array(
                        'template'      => 'report-footer.php',
                        'data'          => $tableStats->totalReferralSearch,
                        'rowKey'        => 'totalSearch'
                        )
));
?>

	</div>
</div><!-- /end fluid -->

<script type="text/javascript">

<?php $this->start_script(); ?>

function datatableDraw(params){
    params.push({name:'filter','value':$('#websites').val()});
}

$(function(){
    var previousPoint;

    function ReloadChart(newdata){
        var ds = new Array();
        var engine_numeric_index = 0;
        if(Object.keys(newdata.chartStats).length <= 2){
            $.plot($("#placeholder1"), ds, {
                grid: {
                    tickColor: '#d4e9f4',
                    hoverable: true
                },
                legend: {
                    show: true
                },
                xaxis: {
                    mode: "time",
                    timeformat:  newdata.chartOptions.timeformat,
                    tickSize: [ newdata.chartOptions.day_size, newdata.chartOptions.tick_type],
                    autoscaleMargin: 0.0,
                    alignTicksWithAxis: 0
                }
            });
            return;
        }
        $.each(newdata.chartStats, function(engine_index, engine){
            var visits_data = [];
            var leads_data = [];
            if (engine_index == 'Bar_Colors' || engine_index == 'Chart_Size'){
                return;
            }
            $.each(newdata.chartStats[engine_index].Visits, function (index ,RowData) {
                visits_data.push([RowData.timestamp, RowData.visits]);
            });
            $.each(newdata.chartStats[engine_index].Leads, function (index, RowData) {
                leads_data.push([RowData.timestamp, RowData.visits]);
            });

            ds.push({
                data:visits_data,
                color: newdata.chartStats.Bar_Colors[engine_numeric_index*2],
                bars: {
                    show: true,
                    barWidth: newdata.chartStats.Chart_Size,
                    order: 5,
                    lineWidth: 0,
                    fillColor: {colors: [{opacity: 1.0},{opacity: 1.0}]}
                },
                label: engine_index + " Visits"
            });
            ds.push({
                data:leads_data,
                color: newdata.chartStats.Bar_Colors[((engine_numeric_index)*2)+1],
                bars: {
                    show: true,
                    barWidth: newdata.chartStats.Chart_Size,
                    order: 5,
                    lineWidth: 0,
                    fillColor: {colors: [{opacity: 1.0},{opacity: 1.0}]}
                },
                label: engine_index + " Leads"
            });
            engine_numeric_index += 1;
        });
        $.plot($("#placeholder1"), ds, {
            grid: {
                tickColor: '#d4e9f4',
                hoverable: true
            },
            legend: {
                show: true
            },
            xaxis: {
                mode: "time",
                timeformat:  <?=$chartOptions['timeformat'];?>,
                tickSize: [ <?="{$chartOptions['day_size']}, '{$chartOptions['tick_type']}'";?>],
                autoscaleMargin: 0.0,
                alignTicksWithAxis: 0
            }
        });
    }

    ReloadChart(<?php
        $newdata    =   array('chartStats' => $Stats);
        echo json_encode($newdata);
    ?>);

    function showTooltip(x,y,contents){
        var _html   = '<div id="tooltip2" class="tooltip">' + contents + '</div>';
        $(_html).appendTo('body');
        var _s      = $('div#tooltip2.tooltip');
        var _ow     = (_s.outerWidth() / 2) - 5;
        _s.css({
            top     : y - 34,
            left    : x - _ow
        }).fadeIn(300);
    }

    $('#placeholder1').bind('plothover',function(event,pos,item){
        if($('#placeholder1').length){
            if(item){
                if(previousPoint != item.datapoint){
                    previousPoint = item.datapoint;
                    $('div#tooltip2.tooltip').fadeOut(300,function(){
                        $(this).remove();
                    });
                    var D = new Date(item.series.data[item.dataIndex][0]);
                    var M = D.getUTCMonth();
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                    var x = '';
                    var y = item.datapoint[1] + ', ' + item.series.label;
                    if(item.series.xaxis.tickSize[0] != 365 && item.series.xaxis.tickSize[1] == 'day'){
                        x = months[M] + ' ' + ('0' + D.getUTCDate()).slice(-2);
                    } else{
                        x = item.series.xaxis.tickSize[0] != 365 ? months[M] + ' ' + D.getUTCFullYear() : D.getUTCFullYear() - 1;
                    }
                    showTooltip(item.pageX,item.pageY,x + ': <span>' + y + '</span>');
                }
            } else{
                $('div#tooltip2.tooltip').fadeOut(300,function(){
                    $(this).remove();
                });
                previousPoint = null;
            }
        }
    });

    function ajaxReload(){
        var pdata   = {filter:$('#websites').val()};
        var newdata = $.ajax({
                        url         : '<?= $this->action_url('Campaign_ajaxchartreportreferrals',array('report_ID' => $T_Report->ID)); ?>',
                        type        : 'POST',
                        data        : pdata,
                        dataType    : 'json'
                    }).done(ReloadChart);
        $('#jreports').dataTable().fnDraw();
    }

    $('#websites').change(ajaxReload);
    $('#dateRange').on('change','input',function(e){
        new AjaxSaveDataRequest(e.currentTarget,undefined,ajaxReload);
    });
});

<?php $this->end_script(); ?>

</script>