<div class="fluid">
	<?php		//$widgets['articles']->display('networkarticles.php');	?>
</div>
<!-- /end fluid -->

<?php //$this->getDialog('create-listing.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){


		//===== Create New Form + PopUp =====//
		$( '#jdialog-createlisting' ).dialog(
		{
			autoOpen: false,
			height: 275,
			width: 550,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create Listing',
				function (ajax, status)
				{
	      	if(!ajax.error)
	      	{
	          //$('.jnoforms').hide();
	          $('.dataTable').dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createlisting')).buttons
		});
	
		//===== Tabs =====//
		$( ".tabs" ).tabs();
	});

<?php $this->end_script(); ?>
</script>