<h1>User Roles</h1>

<!-- ##### Admin Controller ##### -->
<h3>Admin Controller</h3>
<div class="nNote <? if($User->can('view_trash')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_trash</strong></p>
</div>
<div class="nNote <? if($User->can('view_users')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_users</strong></p>
</div>
<div class="nNote <? if($User->can('view_branding')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_branding</strong></p>
</div>
<div class="nNote <? if($User->can('view_products')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_products</strong></p>
</div>
<div class="nNote <? if($User->can('view_spam')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_spam</strong></p>
</div>
<div class="nNote <? if($User->can('view_networks')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_networks</strong></p>
</div>

<!-- ##### Campaign Controller ##### -->
<h3>Campaign Controller</h3>
<div class="nNote <? if($User->can('trash_campaigns')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>trash_campaigns</strong></p>
</div>
<div class="nNote <? if($User->can('view_prospects')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_prospects</strong></p>
</div>
<div class="nNote <? if($User->can('view_customforms')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_customforms</strong></p>
</div>
<div class="nNote <? if($User->can('view_emailmarketing')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_emailmarketing</strong></p>
</div>
<div class="nNote <? if($User->can('view_contacts')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_contacts</strong></p>
</div>
<div class="nNote <? if($User->can('view_rankings')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_rankings</strong></p>
</div>
<div class="nNote <? if($User->can('view_trash')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_trash</strong></p>
</div>
<div class="nNote <? if($User->can('view_networks')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_networks</strong></p>
</div>

<!-- ##### ModelManager Controller ##### -->
<h3>ModelManager Controller</h3>
<div class="nNote <? if($User->can('create_users')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_users</strong></p>
</div>
<div class="nNote <? if($User->can('edit_users')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_users</strong></p>
</div>
<div class="nNote <? if($User->can('cancel_user')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>cancel_user</strong></p>
</div>
<div class="nNote <? if($User->can('suspend_user')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>suspend_user</strong></p>
</div>
<div class="nNote <? if($User->can('create_campaign')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_campaign</strong></p>
</div>
<div class="nNote <? if($User->can('view_campaigns')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_campaigns</strong></p>
</div>
<div class="nNote <? if($User->can('view_reports')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_reports</strong></p>
</div>
<div class="nNote <? if($User->can('create_blogfeed')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_blogfeed</strong></p>
</div>
<div class="nNote <? if($User->can('create_blogpost')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_blogpost</strong></p>
</div>
<div class="nNote <? if($User->can('create_campaign')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_campaign</strong></p>
</div>
<div class="nNote <? if($User->can('create_leadform')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_leadform</strong></p>
</div>
<div class="nNote <? if($User->can('create_responder')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_responder</strong></p>
</div>
<div class="nNote <? if($User->can('create_slide')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_responder</strong></p>
</div>
<div class="nNote <? if($User->can('create_touchpoint')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_touchpoint</strong></p>
</div>
<div class="nNote <? if($User->can('create_user')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>create_user</strong></p>
</div>
<div class="nNote <? if($User->can('edit_blogfeed')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_blogfeed</strong></p>
</div>
<div class="nNote <? if($User->can('edit_blogpost')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_blogpost</strong></p>
</div>
<div class="nNote <? if($User->can('edit_campaign')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_campaign</strong></p>
</div>
<div class="nNote <? if($User->can('edit_leadform')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_leadform</strong></p>
</div>
<div class="nNote <? if($User->can('edit_reseller')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_reseller</strong></p>
</div>
<div class="nNote <? if($User->can('edit_responder')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_responder</strong></p>
</div>
<div class="nNote <? if($User->can('edit_slide')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_slide</strong></p>
</div>
<div class="nNote <? if($User->can('edit_touchpoint')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_touchpoint</strong></p>
</div>
<div class="nNote <? if($User->can('view_touchpoint')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>view_touchpoint</strong></p>
</div>
<div class="nNote <? if($User->can('recover_blogposts')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>recover_blogposts</strong></p>
</div>
<div class="nNote <? if($User->can('edit_campaigns')) echo 'nSuccess'; else echo 'nFailure'; ?>">
    <p>User <span class="bro-verbage">Can</span> access <strong>edit_campaigns</strong></p>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		//changes verbage based on class name (untested)
		$('.nNote').each(function(element){
            if(element.hasClass('nFailure'))
              element.find('span.bro-verbage').html('Can\'t');  
        });
	
	});
    
<?php $this->end_script(); ?>
</script>