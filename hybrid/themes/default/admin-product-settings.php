<h1>Product Settings</h1>
<div class="fluid">
	<div class="widget grid12">
		<div class="whead">
			<h6>Product Info</h6>
		</div>
		<div class="formRow">
            <div class="grid3"><lable>Signup Link</lable></div>
            <div class="grid9"><strong>/free-account/334/ </strong></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Signup Link</lable></div>
            <div class="grid9"><input type="text" name="" value="" /></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Category Only Network</lable><span class="note">(Leave Blank if All Categories)</span></div>
            <div class="grid9">
			<?php $this->getControl('industry-select.php', array('selectname' => 'listing[category]',
                    'value' => $pressarticle->category)); ?>
			</div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Single Sub-Category Only Network Product</lable><span class="note">(Leave Blank if All Sub-Categories)</span></div>
            <div class="grid9">
			<?php $this->getControl('industry-select.php', array('selectname' => 'listing[category]',
                    'value' => $pressarticle->category)); ?>
			</div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Root Target Keyword</lable><span class="note">(Only if Category Selected)</span></div>
            <div class="grid9"><input type="text" name="" value="" /><span class="note">Local listings will contain city in front of this keyword. Example: Landscaper will publish Austin Landscaper to listing url. http://domainname.com/austin-landscaper/company-name/ </span></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Description</lable></div>
            <div class="grid9"><textarea rows="4"></textarea></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>One-time Setup Price</lable></div>
            <div class="grid9"><input type="text" name="" value="" /></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Monthy Price</lable></div>
            <div class="grid9"><input type="text" name="" value="" /></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Form Submit Button Link</lable><span class="note">(To 3rd Party Payment Gateway)</span></div>
            <div class="grid9"><input type="text" name="" value="" /><span class="note">URL users will be sent to make payment after creating account</span></div>
        </div>
	</div>
</div>

<h1>Design Settings</h1>
<div class="fluid">
	<div class="widget grid12">
		<div class="whead">
			<h6>Design Product</h6>
		</div>
		<div class="formRow">
            <div class="grid3"><lable>Full Width Page?</lable></div>
            <div class="grid9">
				<select class="style">
					<option value="1">Yes</option>
					<option selected="selected" value="0">No</option>
				</select>
			</div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Remove Default Price Title</lable></div>
            <div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Between Contact Info & Business Info</lable><span class="note">(must have business fields selected)</span></div>
            <div class="grid9"><textarea rows="4"></textarea></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Below Form Edit Region</lable></div>
            <div class="grid9"><textarea rows="4"></textarea></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Column Edit Region</lable></div>
            <div class="grid9"><textarea rows="4"></textarea></div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>User Success Thank You Page</lable><span class"note">(Only if no payment amount is set)</span></div>
            <div class="grid9"><textarea rows="4"></textarea></div>
        </div>
	</div>
</div>

<h1>Form Fields</h1>
<div class="fluid">
	<div class="widget grid12">
		<div class="whead">
			<h6>Sign-Up Fields</h6>
		</div>
		<div class="formRow">
            <div class="grid12"><strong>Contact Info Fields</strong></div>
        </div>
		<div class="formRow">
			<div class="grid3">First Name</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Last Name</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Phone Number</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Company Name</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Address</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Address 2</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">City</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">State</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">City</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Zip</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Date Field</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Calendar Link, Message, etc.</div>
			<div class="grid9 on_off">
				<textarea rows="4"></textarea>
				<span class="note">Please note, users can simply click on the date field and calendar pops up. You can direct them to a Google Calendar or anything by putting something in this field. </span>
			</div>
		</div>
		<div class="formRow">
			<div class="grid12"><strong>Business Info Fields</strong></div>
		</div>
		<div class="formRow">
			<div class="grid3">Contact Name</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Company Name</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Company Phone Number</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Website URL</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Business Address</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Address 2</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">City</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">State</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Zip</div>
			<div class="grid9 on_off">
				<input type="checkbox" id="" checked="checked" name="chbox" />
			</div>
		</div>
	</div>
</div>

<h1>Secure Page Design</h1>
<div class="fluid">
	<div class="widget grid12">
		<div class="whead">
			<h6>Secure Form Design</h6>
		</div>
		<div class="formRow">
            <div class="grid3"><lable>Theme</lable></div>
            <div class="grid9">
				<select class="style">
					<option selected="selected" value="1">Please choose an option</option>
					<option value="0">themes...</option>
				</select>
			</div>
        </div>
		<div class="formRow">
            <div class="grid3"><lable>Full Width Page?</lable></div>
            <div class="grid9">
				<select class="style">
					<option value="1">Yes</option>
					<option selected="selected" value="0">No</option>
				</select>
			</div>
        </div>
		<div class="formRow">
			<div class="grid3"><label>Above Processing Form</label></div>
			<div class="grid9"><textarea rows="4"></textarea></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Below Processing Form</label></div>
			<div class="grid9"><textarea rows="4"></textarea></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Left Column Edit Region</label></div>
			<div class="grid9"><textarea rows="4"></textarea></div>
		</div>
	</div>
</div>