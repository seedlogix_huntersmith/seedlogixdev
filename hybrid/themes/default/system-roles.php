<?php
/**
 * @var $this Template
 * @var $AllRoles QubeRoleModel[]
 */
?>

<div class="fluid">
    <div class="widget">
        <div class="whead">
			<h6>System Roles</h6>
			<div class="buttonS f_rt jopendialog jopen-createrole"><span>Create a New Role</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('RolesSystem'),
	'class'				=> 'col_2nd,col_4th,col_5th,col_end',
	'id'				=> 'system-roles-tbl',
	'data'				=> $Roles,
	'rowkey'			=> 'Role',
	'rowtemplate'		=> 'system-role.php',
	'total'				=> $totalRoles,
	'sourceurl'			=> $this->action_url('admin_ajaxRoles',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div> 
</div>

<?php $this->getDialog('create-role.php',array('controller' => $controller)); ?>

<div class="SQmodalWindowContainer dialog" title="Update the Role" id="jdialog-deleterole" style="display: none;">
    <form method="post" action="<?= $this->action_url('Admin_ajaxDeleteRole'); ?>" class="jlisting-create">
        <input type="hidden" name="role[ID]" value="0" id="roleID">
        <fieldset class="step" id="w1first">
            <div class="fluid spacing">
                <div class="grid4"><label>Role:</label></div>
                <div class="grid8">
                    <select name="role[newRole]" id="newRole">
<?php foreach($AllRoles as $role): ?>
                        <option value="<?= $role->getID(); ?>"><?= $role->role; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<script>
    <?php $this->start_script()?>
    $(document).ready(function(){
        $('.dTable').on('click', 'a.delete-dialog', function(e){
            e.preventDefault();
            var dialog = $('#jdialog-deleterole');
            var id = $(e.srcElement).closest('tr').find('.role_ID').text();
            dialog.dialog('open');
            dialog.find('#roleID').val(id)
        });

        //===== Create New Form + PopUp =====//
        $( '#jdialog-deleterole' ).dialog(
            {
                autoOpen: false,
                height: 150,
                width: 300,
                modal: true,
                open: function(e){
                    $.uniform.update();
                },
                close: function () {
                    $('#roleID', this).val('0');
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Update',
                    function (ajax, status) {

                        if (!ajax.error) {
                            $('.dTable').dataTable().fnDraw();
                            notie.alert({text:ajax.message});
                            this.close();
                            this.form[0].reset();
                        }
                        else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        }
                        else
                            alert(ajax.message);

                    },'#jdialog-deleterole')
                ).buttons
            }
        );
    });

    <?php $this->end_script()?>
</script>