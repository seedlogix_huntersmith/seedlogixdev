<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?> 
<br />
<!-- Login wrapper begins -->
    
<div class="wrapper" style="position:relative; top:48px;">
    <div class="grid4"><h2>
                <?php $this->p($bloginfo->blog_title); ?> / Import Blog Posts
            </h2>
                <div class="searchLine">
                    <form action="<?php echo $this->action_url('WordpressImporter_url?blog_id=' . $blog_id); ?>" method="POST">
                        <input type="text" name="url" class="ac ui-autocomplete-input" placeholder="Feed Url" 
                               autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" value="<?php
                                $this->p($url); ?>">
                       <button type="submit" name="find" value=""><span class="icos-search"></span></button>
                    </form>
                </div>
                
            
                    <?php if($stats): ?>
        <div class="widget">
            <div class="whead"><h6>                <?php $this->p($bloginfo->blog_title); ?> / Imported Posts</h6><div class="clear"></div></div>
            <div class="body">
            <ul>
                    <li> <?= $stats['posts']; ?> Posts were imported. </li>
                    <li> <?= $stats['imgcounter']; ?> Image tags were replaced. </li>
            </ul></div>
        </div>
                    <?php else: ?>
                    
        <?php if($pie): ?>
                    <form action="<?php echo $this->action_url('WordpressImporter_url?blog_id=' . $blog_id); ?>" method="POST">
                        <input type="hidden" name="url" value="<?php $this->p($url); ?>" />
                <div class="widget searchWidget">
                    <div class="whead">
                        Feed Items
                    </div>
                    <ul class="updates"><?php foreach($pie->get_items() as $item): ?>
                        <li class="formRow">
                            <span class="jfield">
                                Title: <input type="text" name="title[<?php echo $item->get_id(); ?>]" value="<?php echo $item->get_title(); ?>" />
                            </span>
                            <span class="jfield">Tags <input name="tags[<?php echo $item->get_id(); ?>]" type="text" value="" /></span>
                            <span class="jfield">Tags Url <input name="tagsurl[<?php echo $item->get_id(); ?>]" type="text" value="" /></span>

                            <span class="jdate"><input type="text" name="dates[<?php echo $item->get_id(); ?>]" value="<?php echo $item->get_gmdate('m/j/Y'); ?>" /></span>
                            <span class="category">Category: 
                                <select name="category[<?php echo $item->get_id(); ?>]"><?php foreach($item->get_categories() as $cat): ?>
                                    <option><?php echo $cat->get_label(); ?></option><?php endforeach; ?>
                                    <option value="">.. Other ..</option></select>
                            <input type="text" name="category_txt[<?php
                                echo $item->get_id(); ?>]" value="" role="textbox"/></span>
                            <span class="clear"></span>
                        </li><?php endforeach; ?> 
                    </ul>
                    
                    <div class="pagination"><input type="submit" value="Save" />
                    </div>
                    
                </div></form> <?php endif;
                
                endif;
                ?>
        
                
            </div>
</div>

<style>
    .category, .jdate {
        
        float: right; width: auto;
        margin-right: 20px; 
    }
    
    .jfield {
        float: left; padding-left: 22px; max-width: 76%; width: 
    }
    
    .formRow .jdate input { width: 80px; }
    .formRow .category input[type="text"] { display: none; }
    .formRow .jfield input { width: 300px; }
/*    .updates .formRow { border: none; }
    */
    
    </style>
    
    <script>
        $(function (){
            $( ".jdate input" ).datepicker();
           $('.category select').change(function (){
               var v    =   $(this).val();
               var sel  =   $(this).parents('.selector');
               var input    =   sel.siblings('input');
               
//               console.log(v, input);
               if(v == ''){
                    input.show();
                    sel.hide();
                    input.focus();
               }
                else{
                    input.hide();
                    sel.show();
                }
           });
           
           $('.category input').blur(function (){
               var input  =   $(this);  //.parents('.selector');
               var sel  =   input.siblings('.selector');
               
               var val  =   $.trim(this.value);
               
               if(val == '')
                   {
                       input.hide();
                       sel.show();
                       $('select option:first-child', sel).attr('selected', 'selected');
                   }else{
                       $('.category select').each(function (inx, obj){
                           var found    =   false;
                           var i;
                            for(i = 0; i < obj.options.length && !found; i++)
                                {
                                    if(obj.options[i].text ==  val){
                                        return;
                                    }
                                }
                            if(!found)
                                obj.options[i]  =   new Option(val, val);
                       });
                       sel.hide();
                       input.show();
                   }
                   $.uniform.update();
           })
        });
        </script>
        <br/><br/><br/><br/>