<!-- LEFT SIDE NAV -->
<?php   // @delete $this->getPart('website-dashboard-nav.php'); ?>

<!-- CONTENT AREA  -->
<div id="twoColContentArea">

<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads</h6>
                <?php $this->getPart('graph-range-select.php',
                            $ChartWidget->getSettings()); ?>
		<div class="clear"></div>
	</div>
        <input type="hidden" id="graph_action" value="<?php echo $this->action_url('campaign_graphdata?CID=' . $site->cid); ?>"/>
	<div class="body"><div class="chart"></div></div>
</div>


<!-- ##### CAMPAIGNS TABLE ##### -->          
<?php 
	$prospect = new LeadModel; 
	$prospect->lead_name = 'jack mosley';
	$prospect->lead_email = 'jack.mosely@gmail.com';
	$prospect->lead_phone = '521-343-56665';
	$prospect->created = 'today';
	
	$prospects = array($prospect); 
	
	$this->getTable('website-prospects.php', array('prospects' => $prospects));
?>



<!-- //end content container -->
</div>
<!-- //end content container -->

<!-- End HTML -->

<script type="text/javascript">
	
	//===== Create Dashboard Graph =====//
        Dashboard.cookienames   =   <?php echo json_encode($ChartWidget->getSettings()); ?>;
        
	Dashboard.plotData(<?php
            $params = array();
            foreach($ChartWidget->getSets() as $data)
                    $params[]   =   json_encode($data);
            echo join(',', $params);
        ?> );

</script>