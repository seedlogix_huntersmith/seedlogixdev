<?php
/**
 * @var $Prospect ProspectModel
 */
$isAddedToContacts = $Prospect->contact_ID;
if ($last_activity instanceof PageModel) {
    $edit_url_last = $this->action_url("{$last_touch->touchpoint_type}_pagedetails", array('PID' => $last_activity->id));
    $edit_last_name = $last_activity->page_title;
    $touch_created_label = 'Today';
}
if ($last_touch != null) {
    $edit_url_last = $this->action_url("{$last_touch->touchpoint_type}_dashboard", array('ID' => $last_touch->page_id));
    $edit_last_name = $last_touch->name;
    $touch_created_label = $last_touch->getTouchCreatedLabel();
} else {
    $edit_url_last = '';
    $edit_last_name = '';
    $touch_created_label = 'Unknown';
}
if ($Prospect->getGrade() == 'A') $percentage = '100%';
if ($Prospect->getGrade() == 'B') $percentage = '75%';
if ($Prospect->getGrade() == 'C') $percentage = '50%';
if ($Prospect->getGrade() == 'D') $percentage = '25%';

function valid_phone($str)
{
    $str = trim($str);
    $str = preg_replace('/\s+(#|x|ext(ension)?)\.?:?\s*(\d+)/', ' ext \3', $str);

    $us_number = preg_match('/^(\+\s*)?((0{0,2}1{1,3}[^\d]+)?\(?\s*([2-9][0-9]{2})\s*[^\d]?\s*([2-9][0-9]{2})\s*[^\d]?\s*([\d]{4})){1}(\s*([[:alpha:]#][^\d]*\d.*))?$/', $str, $matches);

    if ($us_number) {
        return $matches[4] . '-' . $matches[5] . '-' . $matches[6] . (!empty($matches[8]) ? ' ' . $matches[8] : '');
    }
    return $str;
}

$phoneCheck = PhoneModel::Fetch('cid = "' . $Prospect->cid . '" AND type = "voip"');
if ($phoneCheck) {
    if ($phoneCheck->external_only == 1) $showPhone = false;
    else $showPhone = true;
}
$phone = '1' . preg_replace('[\D]', '', valid_phone($Prospect->phone));
//var_dump($all_activity);
?>

<?php if ($phoneCheck) { ?>
    <?php if ($showPhone) { ?>
        <a class="buttonS f_rt jopendialog jopen-makecall" data-phone="<?= $phone ?>"><i class="icon-phone"
                                                                                         style="margin-top:5px;"></i></a>
    <?php } ?>
    <a class="buttonS f_rt jopendialog jopen-sendtext" data-phone="<?= $phone ?>"><i class="icon-comment_stroke"
                                                                                     style="margin-top:5px;"></i></a>
<?php } ?>
<a class="buttonS f_rt jopendialog jopen-sendemail"
   data-email="<?= mb_strimwidth($Prospect->email, 0, 57, '&hellip;'); ?>" data-leadid="<?= $Prospect->getID() ?>"><i
            class="icon-email" style="margin-top:5px;"></i></a>
<div style="clear:both;"></div>
<?php //var_dump($Prospect->contact_ID); ?>
<div class="fluid">
    <!-- section -->
    <div class="widget grid6">
        <div class="whead"><span class="<?= $Prospect->getColor(); ?>"><?= $Prospect->getGrade(); ?></span>
            <h6 class="capitalize" style="margin-left:30px;"><?php $Prospect->html('name'); ?><?= $Info['Name']; ?></h6>

            <?php if ($isAddedToContacts): ?>

                <!--<span class="buttonS f_rt disabled"><span>Added to Contacts</span></span>-->
                <a class="buttonS f_rt"
                   href="<?= $this->action_url('Campaign_account?ID=' . $Account->cid, array('aid' => $Account->id)) ?>"><span>View Account</span></a>
                <a class="buttonS f_rt"
                   href="<?= $this->action_url('Campaign_contact?ID=' . $Contact->cid, array('pid' => $Contact->id)) ?>"><span>View Contact</span></a>

            <?php else: ?>
                <div class="buttonS f_rt jopendialog jopen-addContact"><span>Convert to Account</span></div>
                <!--<a class="buttonS f_rt" href="<?= $this->action_url('Campaign_prospect-to-contact', array('ID' => $Prospect->cid, 'pid' => $Prospect->getID())); ?>"><span>Add to Contacts</span></a>-->

            <?php endif; ?>

        </div>
        <div class="body">

            <select id="transferLeadToAgentCRM" style="width:75%;"
                    data-placeholder="Transfer Lead to Sales Agent"
                    class="select2"
                    name="prospect[crm_agent]">
                <option></option>
                <?php foreach ($CRMRoleAgents as $agent) { ?>
                    <option value="<?= $agent->id; ?>" <?php //$this->selected($Form->id, $Prospect->form_ID); ?>><?= $agent->fullname ?></option>
                <?php } ?>
            </select>

            <!-- To get jopendialog to work on non-button "\_o_/" -->
            <div class="buttonS f_rt jopendialog jopen-transferLeadToAgentCRM" style="display: none;"><span>Do Something</span></div>
            <!-- End fake button -->

            <script type="text/javascript">
                $(function () {
                    $("#transferLeadToAgentCRM").on('change', function (evt) {
                        var val = $(this).val();
                        //console.log($(this).val());

                        if (val != "") {
                            $(".jopen-transferLeadToAgentCRM").trigger('click');
                        }
                    });
                });
            </script>
            <!--
			<div class="grid4">
				
				<div class="prospectPhoto">
                    <img src="<?= $Prospect->email ? $User->getStorageUrl() . "prospects/{$Prospect->email}" : 'themes/default/images/userLogin.png' ?>" alt="">
				</div>
			</div>
-->
            <div class="grid12">
                <div class="body">
                    <?php if ($all_activity) { ?>
                        <ul class="wInvoice" style="text-align:center;">
                            <li><h4 style="color:#C57FE5"><?= $all_activity->totalVisits ?> </h4><span>visits</span>
                            </li>
                            <li><h4 style="color:#27bdbe"><?= $all_activity->firstVisit->daysAgo ?> days</h4><span>first visit</span>
                            </li>
                            <li><h4 style="color:#00aeef"><?= $all_activity->lastVisit->daysAgo ?> days</h4><span>last visit</span>
                            </li>
                            <li><h4 style="color:#1e4382"><?= ceil($all_activity->totalVisitDuration / 60) ?></h4><span>time (minutes)</span>
                            </li>
                            <li><h4 style="color:#612c5c"><?= $all_activity->averagePageGenerationTime ?> </h4><span>average time</span>
                            </li>
                            <li><h4 style="color:#e02a74"><?= $all_activity->totalPageViewsWithTiming ?> </h4><span>total pages viewed</span>
                            </li>
                        </ul>
                    <?php } ?>
                    <div class="contentProgress">
                        <div class="barG tipN" title="<?= $percentage ?>" id="bar7"></div>
                    </div>
                    <ul class="ruler">
                        <li>0</li>
                        <li class="textC">50%</li>
                        <li class="textR">100%</li>
                    </ul>

                </div>

                <!--<div class="whiteSurround">
					<span class="label label-info">Last Touch</span><br><br>
					<h2><?= $touch_created_label; ?></h2>

<?php if ($User->id == 6610 || !$User->can('reports_only')): ?>

					<p>Viewed <a href="<?= $edit_url_last; ?>"><?= $edit_last_name; ?></a></p>

<?php elseif ($User->id != 6610 && $User->can('reports_only')): ?>

					<p>Viewed <strong><?= $edit_last_name; ?></strong></p>

<?php endif; ?>

				</div>-->

            </div>
            <div class="grid12">
                <?php if ($Prospect->touchpoint_ID != 0 && $first_touch) { ?>
                    <div class="whiteSurround">
                        <span class="label label-info">First Lead Submission</span><br><br>
                        <h2><?= $first_touch->getTouchCreatedLabel(); ?> ago</h2>

                        <?php if ($User->id == 6610 || !$User->can('reports_only')): ?>

                            <p>Filled out <a
                                        href="<?= $this->action_url('campaign_form', array('ID' => $first_touch->cid, 'form' => $first_touch->form_ID)); ?>"><?= $first_touch->name; ?></a>
                            </p>

                        <?php elseif ($User->id != 6610 && $User->can('reports_only')): ?>

                            <p>Filled out <strong><?= $first_touch->name; ?></strong></p>

                        <?php endif; ?>

                    </div>
                <?php } ?>

                <form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
                    <input type="hidden" name="prospect[ID]" value="<?= $Prospect->getID(); ?>">
                    <input type="hidden" name="prospect[cid]" value="<?= $Prospect->cid; ?>">

                    <div class="formRow">
                        <div class="grid3"><label>Campaign Form:</label></div>
                        <div class="grid9">
                            <select style="width:75%;" data-placeholder="Select Campaign Form" class="select2"
                                    name="prospect[form_ID]">
                                <option></option>
                                <?php foreach ($Forms as $Form) { ?>
                                    <option value="0">Remove from Drip</option>
                                    <option value="<?= $Form->id; ?>" <?php $this->selected($Form->id, $Prospect->form_ID); ?>><?= $Form->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Source:</label></div>
                        <div class="grid9">
                            <select style="width:75%;" data-placeholder="Select Source" class="select2"
                                    name="prospect[source]">
                                <option></option>

                                <option value="">No Source</option>
                                <option value="Organic Search" <?php $this->selected('Google Search', $Prospect->source); ?>>
                                    Organic Search
                                </option>
                                <option value="Business Card" <?php $this->selected('Business Call', $Prospect->source); ?>>
                                    Business Card
                                </option>
                                <option value="Trade Show" <?php $this->selected('Trade Show', $Prospect->source); ?>>
                                    Trade Show
                                </option>
                                <option value="Cold Call" <?php $this->selected('Cold Call', $Prospect->source); ?>>Cold
                                    Call
                                </option>
                                <option value="Inbound Call" <?php $this->selected('Inbound Call', $Prospect->source); ?>>
                                    Inbound Call
                                </option>
                                <option value="Facebook Ad - Lead Form" <?php $this->selected('Facebook Ad - Lead Form', $Prospect->source); ?>>
                                    Facebook Ad - Lead Form
                                </option>
                                <option value="Facebook Ad - Like Lead" <?php $this->selected('Facebook Ad - Like Lead', $Prospect->source); ?>>
                                    Facebook Ad - Like Lead
                                </option>
                                <option value="Facebook Ad - Comment Lead" <?php $this->selected('Facebook Ad - Comment Lead', $Prospect->source); ?>>
                                    Facebook Ad - Comment Lead
                                </option>
                                <option value="Google Ad" <?php $this->selected('Google Ad', $Prospect->source); ?>>
                                    Google Ad
                                </option>


                            </select>
                        </div>
                    </div>

                    <?php if ($Prospect->touchpoint_ID == 0) { ?>

                        <?php if ($Prospect->contact_ID) { ?>
                            <div class="formRow">
                                <div class="grid3"><label>Name:</label></div>
                                <div class="grid9">
                                    <span><?= $Contact->first_name ?> <?= $Contact->last_name ?></span>

                                </div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Email:</label></div>
                                <div class="grid9">
                                    <span><?= $Contact->email ?></span>

                                </div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Phone:</label></div>
                                <div class="grid9">
                                    <span><?= $Contact->phone ?></span>

                                </div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Company:</label></div>
                                <div class="grid9">
                                    <span><?= $Account->name; ?></span>

                                </div>
                            </div>

                        <?php } else { ?>
                            <div class="formRow">
                                <div class="grid3"><label>Name:</label></div>
                                <div class="grid9">
                                    <input type="text" name="prospect[name]" value="<?= $Prospect->name; ?>">
                                </div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Email:</label></div>
                                <div class="grid9"><input type="text" name="prospect[email]"
                                                          value="<?= $Prospect->email; ?>"></div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Phone:</label></div>
                                <div class="grid9"><input type="text" name="prospect[phone]"
                                                          value="<?= $Prospect->phone; ?>"></div>
                            </div>
                            <div class="formRow">
                                <div class="grid3"><label>Company:</label></div>
                                <div class="grid9"><input type="text" name="prospect[company]"
                                                          value="<?= $Prospect->company; ?>"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>


                </form>

            </div>

        </div>
        <?php if ($Prospect->touchpoint_ID != 0) { ?>
            <div class="whead"><h6>Lead Information</h6></div>
            <!-- meta config tab -->
            <?
            if ($Prospect->isContact()):
                ?>
                <div class="formRow">
                    <div class="grid6"><label>Name:</label></div>
                    <div class="grid6"><?php echo $ProspectData['Name']; ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Email:</label></div>
                    <div class="grid6"><?php echo $ProspectData['E-mail']; ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Phone:</label></div>
                    <div class="grid6"><?php echo $ProspectData['Phone']; ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Comments:</label></div>
                    <div class="grid6"><?php echo $ProspectData['Comments']; ?></div>
                </div>
            <?
            else:
                foreach ($ProspectData as $Label => $Value):
                    ?>
                    <div class="formRow">
                        <div class="grid6"><label><?= $Label; ?>:</label></div>
                        <div class="grid6"><?php $this->p($Value); ?></div>
                    </div>
                <?
                endforeach;
            endif;
            ?>
        <?php } ?>
        <!-- //end meta config tab -->
    </div><!-- //section -->
    <!-- section -->
    <div class="grid6">

        <div class="widget">
            <?php if ($Prospect->touchpoint_ID != 0) { ?>
                <div class="whead"><h6>Technical Information</h6></div>
                <div class="formRow">
                    <div class="grid6"><label>Touchpoint:</label></div>
                    <div class="grid6"><?= $Prospect->touchpoint_name ? $Prospect->html('touchpoint_name') : ''; ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Touchpoint Page:</label></div>
                    <div class="grid6"><?= $Prospect->page_title ? $Prospect->html('page_title') : ''; ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Search Engine:</label></div>
                    <div class="grid6"><?php $Prospect->html('searchengine'); ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Keywords Searched:</label></div>
                    <div class="grid6"><?php $Prospect->html('keywords'); ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Referral:</label></div>
                    <div class="grid6"><?= $all_activity->firstVisit->referrerType ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Referral Summary:</label></div>
                    <div class="grid6"><?= $all_activity->firstVisit->referralSummary ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>Visitor Settings:</label></div>
                    <div class="grid6"><?php if ($all_activity) { ?>
                            <strong>Device:</strong> <?= $all_activity->lastVisits[0]->deviceType ?>, <strong>Device
                                Brand:</strong> <?= $all_activity->lastVisits[0]->deviceBrand ?>, <strong>Device
                                Model:</strong> <?= $all_activity->lastVisits[0]->deviceModel ?>,
                            <strong>Browser:</strong> <?= $all_activity->lastVisits[0]->browser ?>, <strong>Screen
                                Size:</strong> <?= $all_activity->lastVisits[0]->resolution ?><?php } ?></div>
                </div>
                <div class="formRow">
                    <div class="grid6"><label>IP Address:</label></div>
                    <div class="grid6"><?php $Prospect->html('ip'); ?></div>
                </div>
            <?php } ?>
            <div class="whead"
                 <?php if ($User->parent_id == 160973 || $User->parent_id == 48205) { ?>style="margin-top:40px;border-top: solid 1px #C8CDD0;"<?php } ?>>
                <h6>Tasks</h6>
                <div class="buttonS f_rt jopendialog jopen-createtask"><span>Create Tasks</span></div>
            </div>

            <?php
            $this->getWidget('datatable.php', array(
                'columns' => DataTableResult::getTableColumns('Tasks'),
                'class' => 'col_1st,col_2nd,col_end',
                'id' => 'jTasks',
                'data' => $Tasks,
                'rowkey' => 'Task',
                'rowtemplate' => 'tasks.php',
                'total' => $total_tasks_match,
                'sourceurl' => $this->action_url('campaign_ajaxtasks', array('ID' => $_GET['ID'], 'leadid' => $Prospect->id)),
                'DOMTable' => '<"H"lf>rt<"F"ip>'
            ));
            ?>
        </div><!-- //section -->

    </div><!-- //section -->
</div> <!-- //end fluid -->

<div class="fluid">

    <!-- section -->
    <div class="widget grid12">
        <div class="whead editSwitcher">
            <h6 class="timelineName">Lead Timeline</h6>
            <a class="buttonS f_rt jopendialog jopen-createleadnote"><span>Create a New Note</span></a>
        </div>
        <div class="body">
            <div class="grid12">
                <div id="timeline"></div>
            </div>
        </div>
    </div><!-- //section -->

</div><!-- //end fluid -->

<?php $this->getDialog('send-email-lead.php'); ?>
<?php $this->getDialog('add-contact.php'); ?>
<?php $this->getDialog('transfer-lead-to-agent-crm.php') ?>

<?php if ($showPhone) $this->getDialog('make-call.php'); ?>

<?php $this->getDialog('send-text.php'); ?>
<?php $this->getDialog('create-leadnote.php'); ?>
<?php $this->getDialog('create-leadtask.php'); ?>
<?php $this->getDialog('update-task.php'); ?>
<script type="text/javascript">

    <?php $this->start_script(); ?>

    function tinyMCE() {
        tinymce.init({

            selector: 'textarea',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 500,
            force_br_newlines: false,
            force_p_newlines: false,
            menubar: false,
            forced_root_block: '',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            valid_elements: '*[*]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor textcolor",
                "searchreplace visualblocks code",
                "insertdatetime media table contextmenu paste autoresize"
            ],
            external_plugins: {
                "moxiemanager": "/themes/default/js/plugins/moxiemanager/plugin.min.js"
            },
            relative_urls: false,
            remove_script_host: false,
            moxiemanager_title: 'File Manager',
            toolbar: "insertfile undo redo | forecolor backcolor | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright | alignjustify | bullist numlist outdent indent | link image code | cut paste pastetext"
        });
    }

    function tinyMCEsmall() {
        tinymce.init({

            selector: 'textarea',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 500,
            force_br_newlines: false,
            force_p_newlines: false,
            menubar: false,
            forced_root_block: '',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            valid_elements: '*[*]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor textcolor",
                "searchreplace visualblocks code",
                "insertdatetime media table contextmenu paste autoresize"
            ],
            external_plugins: {
                "moxiemanager": "/themes/default/js/plugins/moxiemanager/plugin.min.js"
            },
            relative_urls: false,
            remove_script_host: true,
            moxiemanager_title: 'File Manager',
            toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright | alignjustify | bullist numlist outdent indent | link image | cut paste pastetext"
        });
    }


    function switchTemplate() {

        var leadid = $("#leadid").val();
        var email_template = $("#email_templates").val();
        var emailTemplate = $("#emailTemplate").val();

        $.ajax({ // create an AJAX call...
            data: "leadid=" + leadid + "&email_template=" + email_template + "&emailTemplate=" + emailTemplate, // get the form data
            type: 'post', // GET or POST
            url: '', // the file to call
            success: function (ajax, status) { // on success..

                var json = JSON.parse(ajax);
                tinymce.remove();
                $('.j-cleditor').val(json.template);
                $('#subject').val(json.subject);
                tinyMCE();

                //alert(json.results);
            }
        });
    }

    $(document).ready(function () {

        var timeline_data = [
            <?php
            if($all_activity->lastVisits){
            foreach($all_activity->lastVisits as $visit):
            foreach($visit->actionDetails as $action):
            $FormattedDate = date('Y-m-d', $action->timestamp);
            $timelineNote = (object)array(
                'type' => 'blog_post',
                'date' => $FormattedDate,
                'dateFormat' => 'DD MMM YYYY',
                'title' => "<span class=\"visitstimeline-title\"><i class=\"icon-globe\"></i> {$action->pageTitle}</span>",
                'width' => '92%',
                //'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
                //'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
                'content' => "<p>{$Prospect->name} visited this page <a href=\"{$action->url}\" target=\"_blank\">{$action->url}</a> for {$action->generationTime}.</p>"
            );

            ?>
            <?= json_encode($timelineNote) . ','; ?>
            <?php endforeach;?>
            <?php endforeach;
            }
            ?>

            <?php if($Prospect->touchpoint_ID != 0){ ?>
            <?php ///lead submission
            $FormattedDate = new DateTime($first_touch->created);
            $FormattedDate = $FormattedDate->format('Y-m-d');
            $timelineNote = (object)array(
                'type' => 'blog_post',
                'date' => $FormattedDate,
                'dateFormat' => 'DD MMM YYYY',
                'title' => "<span class=\"notetimeline-title\"><i class=\"icon-reply-2\"></i> {$first_touch->name}</span>",
                'width' => '92%',
                //'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
                //'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
                'content' => "<p>{$Prospect->name} submitted their information to the form {$first_touch->name} located on {$last_touch->name}.</p>"
            );
            ?>
            <?= json_encode($timelineNote) . ','; ?>
            <?php } else { ?>
            <?php ///lead submission
            $FormattedDate = new DateTime($Prospect->created);
            $FormattedDate = $FormattedDate->format('Y-m-d');
            $timelineNote = (object)array(
                'type' => 'blog_post',
                'date' => $FormattedDate,
                'dateFormat' => 'DD MMM YYYY',
                'title' => "<span class=\"notetimeline-title\"><i class=\"icon-reply-2\"></i> {$Prospect->name} Added As Lead</span>",
                'width' => '92%',
                //'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
                //'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
                'content' => "<p>{$Prospect->name} was added manually.</p>"
            );
            ?>
            <?= json_encode($timelineNote) . ','; ?>
            <?php } ?>

            <?php foreach($Texts as $text): ?>
            <?= json_encode($text->getTimeLineTextLead($this->action_url('campaign_delete', array('Text_ID' => $text->id)), $Prospect->id)) . ','; ?>
            <?php endforeach;?>
            <?php foreach($AssociatedTexts as $Atext): ?>
            <?= json_encode($Atext->getTimeLineTextLead($this->action_url('campaign_delete', array('Text_ID' => $Atext->id)), $Prospect->id)) . ','; ?>
            <?php endforeach;?>
            <?php foreach($InboundCalls as $ICalls): ?>
            <?= json_encode($ICalls->getTimeLinePhoneLead(NULL, $Prospect->id)) . ','; ?>
            <?php endforeach;?>
            <?php foreach($OutboundCalls as $OCalls): ?>
            <?= json_encode($OCalls->getTimeLinePhoneLead(NULL, $Prospect->id)) . ','; ?>
            <?php endforeach;?>
            //emails
            <?php foreach($Emails as $email): ?>
            <?= json_encode($email->getTimeLineEmailLead($this->action_url('campaign_delete', array('Email_ID' => $email->id)))) . ','; ?>
            <?php endforeach;?>
            //tasks
            <?php foreach($Tasks as $task):?>
            <?= json_encode($task->getTimeLineTask()) . ','; ?>
            <?php endforeach;?>
            <?php
            /** @var NoteModel $note */
            foreach($notes as $note):?>
            <?= json_encode($note->getTimeLineNote($this->action_url('campaign_delete', array('note_ID' => $note->id)))) . ','; ?>
            <?php endforeach;?>


        ];

        var timeline = new Timeline($('#timeline'), timeline_data);
        window.TimeLineOptions = {
            animation: false,
            separator: 'month_year'
        };
        timeline.setOptions(TimeLineOptions);
        timeline.display();

        window.timeline = timeline;

        <?php foreach($Emails as $email):?>
        $("#expand-<?=$email->id?>").on("click", function () {
            $(".eb-<?=$email->id?>").show();
            $(".es-<?=$email->id?>").hide();
        });
        $("#collapse-<?=$email->id?>").on("click", function () {
            $(".es-<?=$email->id?>").show();
            $(".eb-<?=$email->id?>").hide();
        });
        <?php endforeach;?>

        var wWidth = $(window).width();
        var dWidth = wWidth * 0.95;
        var wHeight = $(window).height();
        var dHeight = wHeight * 0.95;

        $('#jdialog-sendemail').dialog(
            {
                open: function (event, ui) {
                    tinyMCE();

                },
                autoOpen: false,
                height: dHeight,
                width: dWidth,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                    tinymce.remove();
                    $('.j-cleditor').val('');
                    //tinyMCE();

                },
                buttons: (new CreateDialog('Send',
                    function (ajax, status) {

                        if (!ajax.error) {

                            var data = $.merge(ajax.emails, timeline_data);
                            var newTimeline = $('#timeline').empty();
                            window.timeline = new Timeline(newTimeline, data);
                            window.timeline.setOptions(TimeLineOptions);
                            window.timeline.display();

                            notie.alert({text: ajax.message});
                            this.close();
                            this.form[0].reset();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-sendemail')).buttons
            });

        $(document).on("click", ".jopen-sendemail", function () {
            var lead_id = $(this).data('leadid');
            var contact_email = $(this).data('email');
            $("#emailto").val(contact_email);
            $('#leadid').val(lead_id);
        });

        //===== Create New Form + PopUp =====//
        $('#jdialog-addContact').dialog(
            {
                autoOpen: false,
                height: 275,
                width: 500,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Create Contact',
                    function (ajax, status) {

                        if (!ajax.error) {

                            notie.alert({text: ajax.message});
                            this.close();
                            this.form[0].reset();
                            location.reload();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-addContact')).buttons
            });
        //===== Create New Form + PopUp =====//
        $('#jdialog-transferLeadToAgentCRM').dialog(
            {
                autoOpen: false,
                height: 'auto',
                width: 500,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Transfer Lead',
                    function (ajax, status) {

                        if (!ajax.error) {

                            notie.alert({text: ajax.message});
                            this.close();
                            this.form[0].reset();
                            location.reload();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-transferLeadToAgentCRM')).buttons
            });
        //===== Create New Form + PopUp =====//
        $('#jdialog-createtask').dialog(
            {
                autoOpen: false,
                height: 265,
                width: 500,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Create',
                    function (ajax, status) {

                        if (!ajax.error) {
                            var dt = $('#jTasks');
                            var newRowsIds = dt.data('newRowsIds');
                            newRowsIds.unshift(ajax.object.id);
                            dt.data('newRowsIds', newRowsIds);
                            dt.dataTable().fnDraw();

                            var data = $.merge(ajax.tasks, timeline_data);
                            var newTimeline = $('#timeline').empty();
                            window.timeline = new Timeline(newTimeline, data);
                            window.timeline.setOptions(TimeLineOptions);
                            window.timeline.display();

                            this.close();
                            this.form[0].reset();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-createtask')).buttons
            });
        //===== Create New Form + PopUp =====//
        $('#jdialog-savetask').dialog(
            {
                autoOpen: false,
                height: 225,
                width: 500,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Update Task',
                    function (ajax, status) {

                        if (!ajax.error) {
                            var dt = $('#jTasks');
                            var newRowsIds = dt.data('newRowsIds');
                            newRowsIds.unshift(ajax.object.id);
                            dt.data('newRowsIds', newRowsIds);
                            dt.dataTable().fnDraw();

                            var data = $.merge(ajax.tasks, timeline_data);
                            var newTimeline = $('#timeline').empty();
                            window.timeline = new Timeline(newTimeline, data);
                            window.timeline.setOptions(TimeLineOptions);
                            window.timeline.display();

                            this.close();
                            this.form[0].reset();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-savetask')).buttons
            });
        //===== Create New Form + PopUp =====//
        $('#jdialog-createleadnote').dialog(
            {
                open: function (event, ui) {
                    tinyMCEsmall();

                },
                autoOpen: false,
                height: 450,
                width: 650,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                    tinymce.remove();
                    $('.j-cleditor').val('');
                },
                buttons: (new CreateDialog('Create',
                    function (ajax, status) {

                        if (!ajax.error) {


                            var data = $.merge(ajax.notes, timeline_data);
                            var newTimeline = $('#timeline').empty();
                            window.timeline = new Timeline(newTimeline, data);
                            window.timeline.setOptions(TimeLineOptions);
                            window.timeline.display();

                            notie.alert({text: ajax.message});
                            this.close();
                            this.form[0].reset();
                        } else if (ajax.error == 'validation') {
                            notie.alert({text: getValidationMessages(ajax.validation).join('\n')});
                        } else
                            alert(ajax.message);

                    }, '#jdialog-createleadnote')).buttons
            });
        $('#jdialog-sendtext').dialog(
            {
                autoOpen: false,
                height: 275,
                width: 500,
                modal: true,
                close: function () {
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('Send SMS',
                    function (ajax, status) {

                        if (!ajax.error) {
                            var data = $.merge(ajax.texts, timeline_data);
                            var newTimeline = $('#timeline').empty();
                            window.timeline = new Timeline(newTimeline, data);
                            window.timeline.setOptions(TimeLineOptions);
                            window.timeline.display();

                            notie.alert({text: ajax.message});
                            this.close();
                            this.form[0].reset();
                        } else if (ajax.error == 'validation') {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        } else
                            alert(ajax.message);

                    }, '#jdialog-sendtext')).buttons
            });

        $(document).on("click", ".jopen-sendtext", function () {

            var c_phone = $(this).data('phone')
            $('input[type=text]#send_text').val(c_phone);
        });

        $('#jdialog-makecall').dialog(
            {
                autoOpen: false,
                height: 315,
                width: 400,
                modal: true
            });

        $(document).on("click", ".jopen-makecall", function () {


            var c_phone = $(this).data('phone')

            $('input[type=text]#toNumber').val(c_phone);

            // As pointed out in comments,
            // it is superfluous to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });

        $(".duedatepicker").datepicker({dateFormat: "yy-mm-dd", minDate: 0});
        $(".duedatepicker2").datepicker({dateFormat: "yy-mm-dd", minDate: 0});

        $(document).on("click", ".jopen-savetask", function () {
            var task_id = $(this).data('id');
            var t_title = $(this).data('title');
            var t_due = $(this).data('due');
            var t_time = $(this).data('time');
            $("#task_id").val(task_id);
            $('input[type=text]#task_title').val(t_title);
            $('input[type=text]#due_date2').val(t_due);
            $('select#time').val(t_time);
        });

        $('#timeline').on('click', 'a.jajax-action', function (e) {
            e.preventDefault();
            var t = $(e.target).parent();
            sendAjax(t.attr('href'), function (ajax) {
                if (!ajax.error) {
                    timeline._deleteElement(t.closest('.timeline_element'));
                    notie.alert({text: ajax.message});
                } else {
                    notie.alert({text: 'An error occurred.'});
                }
            });
        })


    });

    <?php $this->end_script(); ?>

</script>
