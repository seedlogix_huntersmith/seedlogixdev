<?php if($site->id): ?>

<?php $this->getMenu('middleNav-website.php'); ?>

<?php endif; ?>

<div class="fluid">
<!-- ##### Main Dashboard Chart ##### -->
<form action="<?= $this->action_url('piwikAnalytics_ajaxGetChartData'); ?>" method="POST">
    <input type="hidden" name="interval" value="7">
    <input type="hidden" name="period" value="DAY">
    <input type="hidden" name="chart" value="LeadsVisitsChartData">
    <input type="hidden" name="cid" value="<?= $Campaign->id; ?>">
    <input type="hidden" name="touchpointType" value="<?= $touchpoint_type; ?>">
    <div class="widget chartWrapper">
        <div class="whead">
            <h6>Total Visits &amp; Leads for All <?= $selected_pointtype_label; ?></h6>

<?php $this->getWidget('graph-range-select.php',array('id' => 'chartconfig','value' => Cookie::getVal('chartconfig','30-DAY'))); ?>

        </div>
        <div class="body"><div class="chart"></div></div>
    </div>
</form>

<?php if($total_tp_sites == 0): ?>
<!-- div class="nNote nWarning jopen-createwebsite jopendialog" id="jnotps-msg">
    <p>You have not created any Touchpoints. Step 1. Create a new <?php echo $selected_pointtype_label; ?>.</p>
</div -->
<?php endif; ?>

<!-- ##### CAMPAIGNS TABLE ##### -->
<?php $this->getTable('websites-dashboard-sites.php'); ?>
        

<!-- #### Top 5 Sites Table #### -->
<?php //$this->getTable('websites-dashboard-topperforming.php'); ?>


<div class="fluid">
<!-- Top Performing Keywords widget -->
<?php $this->getWidget('top-performing-keywords.php'); ?>
<!-- Top Performing Keywords END widget -->


<!-- Top Ranking Keywords widget -->
<?php $this->getWidget('top-ranking-keywords.php'); ?>
<!-- Top Ranking Keywords END widget -->


<!-- Social Activity widget -->
<?php if(($selected_pointtype_label == 'Touchpoints')):?>
	<?php $this->getWidget('social-activity.php'); ?>
<?php else:?>

	<!--	 @cory Content for specific touchpoints -->

<?php endif;?>
<!-- Social Activity END widget -->
</div>
<!-- ##### Latest Prospects ##### -->          
<?php
if($touchpoint_type != 'BLOG'){
    $this->getTable('websites-dashboard-prospects.php');
}
?>

<!-- #### rankings data #### -->
<?php
/*
    if(false && $Campaign->getTotalTouchpoints())
        $this->getWidget('touchpoints.php');
*/
?>
</div>

<?php $this->getDialog('create-website.php'); ?>

<!-- End HTML -->

<script type="text/javascript">
	<?php $this->getJ('dashboard-website-js.php'); ?>
</script>