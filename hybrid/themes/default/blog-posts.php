<?php
/**
 * @var $feeds BlogFeedModel[]
 */
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <div class="widget grid8">
        <div class="whead">
            <h6>Blog Posts</h6>
            <div class="buttonS f_rt jopendialog jopen-createpost" id="btnCreateBlogPost"><span>Create a New Blog Post</span></div>
            <select data-placeholder="Filter By&hellip;" class="select2" name="filterByCategory" id="newCategory">
                <option></option>

<?php foreach($categories as $cat => $url_cat): ?>

                    <option value="<?php $this->p($cat); ?>"><?php $this->p($cat); ?></option>

<?php endforeach; ?>

            </select>
        </div>

<?php
$this->getWidget('datatable.php',array(
    'columns'       => DataTableResult::getTableColumns('blog-posts'),
    'class'         => 'col_1st,col_end',
    'id'            => 'jPosts',
    'data'          => $posts,
    'rowkey'        => 'post',
    'rowtemplate'   => 'blog-posts-row.php',
    'total'         => $totalPosts,
    'sourceurl'     => $this->action_url('Blog_ajaxPosts',array('ID' => $blog->id)),
    'DOMTable'      => '<"H"lf>rt<"F"ip>',
    'serverParams'  => 'categoryDataTable'
));
?>

    </div>
    <div class="widget grid4">
        <div class="whead"><h6>Blog Feeds</h6></div>
        <form action="<?= $this->action_url('Blog_ajaxSubscribeFeeds',array('ID' => $blog->getID())); ?>" id="blogfeeds-form">

<?php foreach ($feeds as $feed): ?>

            <div class="formRow">
                <div class="grid6"><label><?= $feed->get('name'); ?>:</label></div>
                <div class="grid6"><input type="checkbox" name="blogfeed[<?= $feed->getID(); ?>]" class="i_button" value="1"<?php $this->checked($feed->subscribed,1); ?>></div>
            </div>

<?php endforeach; ?>

        </form>
    </div>
</div>

<!-- Create Post POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Blog Post" id="jdialog-createpost" style="display: none;">
    <form method="post" action="<?= $this->action_url('Blog_createpost',array('ID' => $blog->id)); ?>" class="jform-create">
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>Post Title:</label></div>
                <div class="grid8"><input type="text" name="post[post_title]" id="post_title" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
            </div>
        </fieldset>
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>Category:</label></div>
                <div class="grid8 inputSwitcher">
                    <div class="inputSwitcher-select">
                        <select data-placeholder="Choose a Category&hellip;" class="select2" style="width: 100%;" name="post[category]" id="selectData">
                            <option></option>
                            <optgroup label="Existing Categories" id="newCategory2">

<?php foreach($categories as $cat => $url_cat): ?>

                                <option value="<?php $this->p($cat); ?>"><?php $this->p($cat); ?></option>

<?php endforeach; ?>

                            </optgroup>
                            <optgroup label="Create Category">
                                <option value="_new">Create a New Category</option>
                                <!-- do not change value for create new category -->
                            </optgroup>
                        </select>
                    </div>
                    <div class="inputSwitcher-input" style="display: none;">
                        <input type="text" id="inputClear" class="text ui-widget-content ui-corner-all" style="margin: 0 0 10px;">
                        <div class="buttonS f_lt" id="btnBack"><span>Back</span></div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<script type="text/javascript">

<?php $this->start_script(); ?>

    $(function(){

<?php $this->getJ('select-category-js.php'); ?>

        $('input[type="checkbox"].i_button').change(function(e){
            new AjaxSaveDataRequest(this,$(this).val(),function(ajax){
                $('#jPosts').dataTable().fnDraw();
            });
        });

        $('select[name="filterByCategory"]').change(function () {
            $('.dTable').dataTable().fnDraw();
        });

        $("#createpost").formwizard({
            formPluginEnabled: true,
            validationEnabled: false,
            focusFirstInput: false,
            disableUIStyles: true,

            formOptions: {
                success: function (data) {
                    var d = JSON.parse(data);
                    $('.dTable').dataTable().fnDraw();
                    notie.alert({text:d.message});
                },
                resetForm: true
            }
        });

        //===== Create New Form + PopUp =====//
        $('#jdialog-createpost').dialog(
                {
                    autoOpen: false,
                    height: 250,
                    width: 500,
                    modal: true,
                    close: function()
                    {
                        $('#campaign_id',this).val('');
                        $('form',this)[0].reset();
                    },
                    buttons: (new CreateDialog('Create',
                            function (ajax, status)
                            {
                                if (!ajax.error)
                                {
                                    //Refresh Select Show only post from DataTable

                                    $('#newCategory option').next().remove();

                                    for (i = 0; i < ajax.blog_categories.length; i++) {
                                        $("<option value='" + ajax.blog_categories[i] + "'>" + ajax.blog_categories[i] + "</option>").appendTo("#newCategory");
                                    }

                                    //Refresh Select Choose Category from DataTable

                                    $("#newCategory2  option").remove();

                                    for (i = 0; i < ajax.blog_categories.length; i++) {
                                        $("<option value='" + ajax.blog_categories[i] + "'> " + ajax.blog_categories[i] + "</option>").appendTo("#newCategory2");
                                    }

//                        $('.jnoforms').hide();
//                        $('.jforms').prepend(ajax.row);
                                    notie.alert({text:ajax.message});
                                    this.close();
                                    this.form[0].reset();

                                    var dt = $('#jPosts');
                                    var newRowsIds = dt.data('newRowsIds');
                                    newRowsIds.unshift(ajax.object.id);
                                    dt.data('newRowsIds', newRowsIds);
                                    dt.dataTable().fnDraw();
                                } else if (ajax.error == 'validation')
                                {
                                    alert(getValidationMessages(ajax.validation).join('\n'));
                                    $('#post_title').focus();
                                } else
                                    alert(ajax.message);

                            }, '#jdialog-createpost')).buttons
                });

    }); //close self invoking function

    function categoryDataTable(aoData){
        var category = $('select[name="filterByCategory"]').val();
        aoData.push({name: "category", value: category});
    }

<?php $this->end_script(); ?>

</script>