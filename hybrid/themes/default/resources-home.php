<?php
$salt = 'SFGDSFG*Jsem fxdfxd,x,Z >< >< jxdnf2';
$pepper = 's$TS$EGU$NGdg,cg cxcxXMMMM ;;;{} (.)(.) ( Y )';
$secHash = md5($salt.$User->id.$pepper);
$secHash = md5($pepper.$secHash);

?>
<script charset="UTF-8" src="//cdn.sendpulse.com/js/push/02d18b7de315565845c28ee444844c9d_0.js" async></script>
<link href="https://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="https://vjs.zencdn.net/4.12/video.js"></script>
<style type="text/css">
  .vjs-default-skin .vjs-control-bar { font-size: 60% }
.videocontent {width:80%:max-width:1280px;}
.video-js {padding-top: 56.25%}
.vjs-fullscreen {padding-top: 0px}
  .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
</style>

<div class="fluid">
  <div class="widget">
    <div class="whead">
      <h6>Welcome to the ignitedAGENT Program, <?php $this->p($User->firstname); ?>!</h6>

<?php
if($isSalesPro || $isSalesProPlus):
  $link_url = 'https://ignitedagent.com/processor/?uid='.$User->id.'&session='.$secHash.'&upgrade=true&productid=';
  if(!$isSalesProPlus):
?>

      <a href="<?= $link_url; ?>1218" class="buttonS f_rt _blank"><span>Upgrade to Sales Pro+</span></a>

<?php endif; ?>

      <a href="<?= $link_url; ?>1219" class="buttonS f_rt _blank"><span>Upgrade to Marketing Director</span></a>

<?php endif; ?>

    </div>
    <div class="formRow">
      <p>We are excited to be working with you. Below you will find some resources to help you get started. We make it really simple to follow our proven sales process, simply drive business owners to our <a href="http://ignitedlocal.video/" class="_blank">ignitedLOCAL presentation</a>, follow-up, and ask for their business.</p>
      <p>If you are only a Sales Professional with ignitedLOCAL, you will need to have all of your business owners sign up at <a href="http://ignitedlocal.com/" class="_blank">ignitedlocal.com</a> and send us an email at <a href="mailto:orders@ignitedlocal.com" class="_blank">orders@ignitedlocal.com</a> with the following details:</p>
      <ul class="bullets">
        <li>Company Name</li>
        <li>Point of Contact</li>
        <li>Contact Phone Number</li>
        <li>Contact Email</li>
      </ul>
      <p>This will ensure all deals by your business owners will route to your sales dashboard and you will receive commissions on that sale. If you need help, please call support at 866-423-5235 or email <a href="mailto:support@ignitedlocal.com" class="_blank">support@ignitedlocal.com</a>.</p>
    </div>
  </div>
</div>

<div class="fluid">
  <div class="widget">
    <div class="whead"><h6>ignitedLOCAL Webinars</h6></div>
    <div class="formRow"><iframe id="iframe25" src="<?= 'http'.(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 's' : '').'://'.$_SERVER['HTTP_HOST']; ?>/api/gotowebinar/webinars_v2.php" scrolling="no"></iframe></div>
  </div>
</div>

<div class="fluid">
  <div class="widget grid4">
    <div class="whead"><h6>Product Presentation</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/160906575" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: In-Depth Product Presentation</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/164658467" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: CRM &amp; Email Setup</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/168282098" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
</div>

<div class="fluid">
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: Resources Walk-Through</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/164432633" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: Questions &amp; Answers Session</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/166234072" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: Yelp Call Script Overview</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/165203047" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
</div>

<div class="fluid">
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: Social Media Setup</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/163492097" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
  <div class="widget grid4">
    <div class="whead"><h6>Sales Training: Social Media Part 2</h6></div>
    <div class="formRow"><div class="videocontent"><div class="embed-container"><iframe src="https://player.vimeo.com/video/166455743" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>
  </div>
</div>

<div class="fluid">
  <div class="widget">
    <div class="whead"><h6>CRM &amp; Email Resources</h6></div>
    <div class="formRow">
      <ul class="bullets">
        <li><a href="/admin.php?action=crm&ctrl=Campaign" class="_blank">CRM Login</a></li>
        <li><a href="http://mail.ignitedlocal.com/webmail" class="_blank">Webmail Login</a></li>
      </ul>
    </div>
    <div class="formRow"><iframe id="iframe89" src="https://cdn.secure-url.co/email/setup_email.php" scrolling="no"></iframe></div>
  </div>
</div>

<div class="fluid">
  <div class="widget">
    <div class="whead"><h6>Sales Professional Resources</h6></div>
    <div class="formRow"><p><strong class="red">All print templates are blank, ready for you to just add your personal URL, phone, etc.</strong></p></div>
    <div class="formRow">
      <p><strong class="green">Business Card (3.5 &times; 2 in)</strong></p>
      <ul class="bullets">
        <li><a href="http://files.ignitedlocal.com/bc-example.jpg" class="_blank">Example (Front)</a></li>
        <li><a href="http://files.ignitedlocal.com/bc-background.jpg" class="_blank">Template (Front)</a></li>
        <li><a href="http://files.ignitedlocal.com/bc-back.jpg" class="_blank">Template (Back)</a></li>
        <li><a href="http://qrcode.kaywa.com/" class="_blank">QR Code Generator</a><br>(After creating, download the image to provide it to the company making your business cards. <a href="https://www.vistaprint.com/" class="_blank">VistaPrint</a> allows you to upload the image and position it on your card.)</li>
      </ul>
    </div>
    <div class="formRow">
      <p><strong class="green">Flyer (8.5 &times; 11 in)</strong></p>
      <ul class="bullets">
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/flyers/generic-flyer.jpg" class="_blank">Example (Front)</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/flyers/generic-back-flyer.jpg" class="_blank">Example (Back)</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/flyers/generic-blank-flyer.jpg" class="_blank">Template (Front)</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/flyers/generic-back-blank-flyer.jpg" class="_blank">Template (Back)</a></li>
      </ul>
    </div>
    <div class="formRow">
      <p><strong class="green">Postcard (6 &times; 4 in)</strong></p>
      <ul class="bullets">
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/postcards/general-front.jpg" class="_blank">Example (Front)</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/postcards/general-back.jpg" class="_blank">Example (Back)</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/collateral/postcards/general-blank-back.jpg" class="_blank">Template (Back)</a></li>
      </ul>
    </div>
    <div class="formRow">
      <p><strong class="green">Facebook</strong></p>
      <ul class="bullets">
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/images/facebook.png" class="_blank">Cover Photo</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/images/social-logo.png" class="_blank">Profile Photo</a></li>
      </ul>
    </div>
    <div class="formRow">
      <p><strong class="green">Twitter</strong></p>
      <ul class="bullets">
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/images/1500x500.jpg" class="_blank">Header Photo</a></li>
        <li><a href="https://s3.amazonaws.com/IgnitedLOCAL/images/social-logo.png" class="_blank">Profile Photo</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="fluid">
  <div class="widget">
    <div class="whead exp3">
      <h6>Base Call Script</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p>Hello {first_name}, this is <?php $this->p($User->firstname); ?> from ignitedLOCAL. Does my name or company sound familiar?</p>
      <p><strong class="red">If prospect says NO:</strong></p>
      <p>Don't worry about it, I wasn't sure it would. Our company has been doing some marketing in your area that you may have seen. At any rate, do you have 30 seconds and I'll tell you why I am calling and you can see if it makes sense for us to talk?</p>
      <p><strong class="red">If this prospect has heard of us:</strong></p>
      <p>Great {first_name}, we have been doing some marketing in your area, so glad to hear. At any rate, do you have 30 seconds and I'll tell you why I am calling and you can see if it makes sense for us to talk?</p>
      <p><strong class="red">If this prospect has heard of you via an email:</strong></p>
      <p>Glad to hear that you received our email and checked out our website. At any rate, do you have 30 seconds and I'll tell you why I am calling and you can see if it makes sense for us to talk?</p>
      <p><strong class="red">30-Second Pitch:</strong></p>
      <p>ignitedLOCAL provides all-in-one, local internet marketing solutions for local business owners in {city}.</p>
      <ul class="bullets">
        <li>We work with companies just like yours that are looking for a solution that is less expensive, produces real measurable results, and focuses on the end-to-end spectrum of local inbound marketing.</li>
        <li>A lot of companies we work with are frustrated with results from local agencies that only provide a single layer of marketing, such as Local SEO, and frankly can't afford to leverage the combination of all the efficient methods that generate good results.</li>
        <li>If you share the same concerns, I'd like to invite you to one of our 10-minute online presentations to learn more about our solution and how we are different from the rest.</li>
      </ul>
      <p><strong class="red">If this prospect says Yes, I am interested:</strong></p>
      <p>Fantastic, all I need is your email and I can send you a link to the presentation. Do you mind if I give you a call in the next few days to see what you think and answer any questions you might have?</p>
      <p><strong class="red">If this prospect says No, I am not interested:</strong></p>
      <p>Thank them for their time and move onto the next.</p>
    </div>

    <div class="whead exp3">
      <h6>Yelp Call Script</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p>Hello, who is in charge of your Yelp campaign or profile with your company? <strong>(A, B &amp; C Paths)</strong></p>
      <p><strong class="red">(A) If they say someone ELSE:</strong></p>
      <p>Great, is he/she available?</p>
      <p><strong class="red">(A) If no they are not available:</strong></p>
      <p>No problem, do you know the best time I can call back or is there a direct line for him/her?</p>
      <p>Also, what is the best email for him/her, I have some suggestions on how to convert more customers from your Yelp profile and would love to send that along with my contact information.</p>
      <p><strong class="green">At this point you have the contact info of the person you need to speak with.</strong></p>
      <p><strong class="red">(B) If they say, what is this about?:</strong></p>
      <p>I came across your Yelp profile on my mobile phone and wasn't sure if you guys realized how many more customers you can convert from Yelp with just a few changes to your website.</p>
      <p>(Stay silent for a bit, they may just tell you who to talk to or say I am the person in charge.)</p>
      <p><strong class="red">(B) If they do not respond, simply say:</strong></p>
      <p>So if you know who handles that, I would love to spend a few minutes to talk with them about what changes could help.</p>
      <p><strong class="red">(B) If it is not the person, and they say their name:</strong></p>
      <p>Thank you for that, is he/she available right now?</p>
      <p><strong class="red">(B) If no:</strong></p>
      <p>Is there a direct line for him/her or better time to call back? Also is there an email I can send my contact information with a few of the suggestions?</p>
      <p><strong class="green">At this point you have the contact info of the person you need to speak with.</strong></p>
      <p><strong class="red">(B) If they say, I am the person responsible for our Yelp profile:</strong></p>
      <p>Awesome, well my name is <?php $this->p($User->firstname); ?>, I am with a company called ignitedLOCAL, have you heard of us?</p>
      <p><strong class="red">(B) If prospect says NO:</strong></p>
      <p>Don't worry about it, I wasn't sure you would. Our company has been doing some marketing in your area that you may have seen. At any rate, do you have 30 seconds and I'll tell you what you can do to enhance your conversions and augment what you are doing on Yelp?</p>
      <p><strong class="red">(B) If this prospect has heard of us:</strong></p>
      <p>Great {first_name}, we have been doing some marketing in your area, so glad to hear. At any rate, do you have 30 seconds and I'll tell you what you can do to enhance your conversions and augment what you are doing on Yelp?</p>
      <p><strong class="red">(B) If this prospect has heard of you via an email:</strong></p>
      <p>Glad to hear that you received our email and checked out our website. At any rate, do you have 30 seconds and I'll tell you what you can do to enhance your conversions and augment what you are doing on Yelp?</p>
      <p><strong class="red">30-Second Pitch: (A &amp; B Paths)</strong></p>
      <p>It appears you have some great reviews on Yelp, which is fantastic, are you aware of what your website looks like on mobile devices?</p>
      <p><strong class="red">(A) If yes:</strong></p>
      <p>Well the user experience is not only preventing you from ranking better on search engines, you are actually losing the amount conversions you could have with just a website that is mobile-friendly.</p>
      <p><strong class="red">(B) If no:</strong></p>
      <p>Well when a user visits your site on a mobile device or tablet, they have to zoom in and scroll right to view your website. This causes low conversion rates and with 55-65% of all your visits coming from these devices, it's starting to be a big issue with many businesses just like yourself.</p>
      <p>Are you familiar with all the changes Google and other searches have done over the last year, forcing business owners to adapt to mobile devices?</p>
      <p><strong class="red">(A) If yes:</strong></p>
      <p>Have you considered switching to a website that is also mobile-friendly?</p>
      <p><strong class="red">(B) If no:</strong></p>
      <p>It happened last year, basically Google came out and said if you want to rank on mobile devices you need to update your website. Now, search engines are including it as a ranking factor on desktops. Have you considered switching to a website that is also mobile-friendly?</p>
      <p><strong class="red">(A &amp; B Paths)</strong></p>
      <p>Our solution is centered around maximizing conversions not only on Google, but Yelp, Facebook, Twitter, virtually everywhere your business can be visible online. We offer an affordable, all-in-one approach to your brand, which of course addresses the issue of your website not being mobile-friendly.</p>
      <p>If you have some time, I'd like to send you an online presentation of our program, where you can learn how we are different and able to offer an all-in-one solution for a lot less than our competition.</p>
      <p><strong class="red">If this prospect says Yes, I am interested:</strong></p>
      <p>Fantastic, all I need is your email and I can send you a link to the presentation. You mind if I give you a call in the next few days to see what you think and answer any questions you might have?</p>
      <p><strong class="red">If this prospect says No, I am not interested:</strong></p>
      <p>Thank them for their time and move onto the next.</p>
    </div>

    <div class="whead exp3">
      <h6>Video Presentation Invite Email</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p><strong class="red">(This email is sent after you have spoken with the business owner and invited them to watch the video presentation.)</strong></p>
      <p>Hello {first_name},</p>
      <p>Thank you for your time today. Below is a quick outline of what is included in our local inbound marketing campaigns.</p>
      <p>What really sets ignitedLOCAL apart from our competition is we focus on the entire spectrum of inbound marketing cost effectively and spend a great deal of effort to deliver a foundation that is going to actually generate leads when visitors find you.</p>
      <p>All of our campaigns include the following: (you can learn more @ http://ignitedlocal.com/)</p>
      <ul class="bullets">
        <li><strong>Foundation Setup &mdash;</strong> Responsive website design, content creation, email marketing design, on-page optimization, etc.</li>
        <li><strong>Local SEO &mdash;</strong> Google Places, Yahoo Local &amp; Bing Local setup, hand-picked directory submissions, local blog posts and articles, videos, and high profile backlinks.</li>
        <li><strong>GEO Targeted Marketing &mdash;</strong> PPC without the cost per click. We drive leads to you while you wait for local SEO to work.</li>
        <li><strong>Local Social Marketing &mdash;</strong> Facebook, Twitter, Google+ marketing, posting content and retweeting content from industry experts.</li>
        <li><strong>ROI Tracking &mdash;</strong> Know everything our campaigns are producing.</li>
      </ul>
      <p>As mentioned our campaigns are guaranteed. We back each of our campaigns with the following:</p>
      <ul class="bullets">
        <li><strong>1st Page Placement Guarantee &mdash;</strong> If we fail to produce your business on the first page of Google within 6 months, we pause your billing and continue working for you on our own dime until we achieve placement.</li>
        <li><strong>ROI Guarantee &mdash;</strong> Just because you are on the 1st Page does not mean you are getting business. We back up our designs and content to convert prospects. If we fail to have our program paying for itself in 6 months, then we also stop your billing and continue working for you at no cost until we achieve this.</li>
      </ul>
      <p>I would like to invite you to view our online presentation that breaks down our product and why we are different than the rest.</p>
      <p>Visit www.ignitedlocal.video to watch now.</p>
      <p>I look forward to getting your feedback on our presentation and how we can help your business.</p>
      <p>Kind Regards,</p>
      <p><?php $this->p($User->firstname); ?> <?php $this->p($User->lastname); ?></p>
      <p>ignitedLOCAL</p>
      <p>Sales Professional</p>
      <p><?php $this->p($User->phone); ?></p>
      <p><?php $this->p($User->username); ?></p>
      <p>www.ignitedLOCAL.com</p>
    </div>

    <div class="whead exp3">
      <h6>Per My Voicemail</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p><strong class="red">(Email to send after a voicemail.)</strong></p>
      <p>Hello {first_name},</p>
      <p>Hope all is well. The reason for my call is I wanted to introduce ignitedLOCAL and find out if you are currently happy with your local SEO strategy you have in place for {business_name}? We offer a solution that is likely less expensive, provides guaranteed results, and our campaigns are focused around the entire spectrum of inbound marketing and not just local SEO.</p>
      <p>I would like to schedule some time to discuss exactly what we do for our clients and how our campaigns are different from what you are running today.</p>
      <p>All of our campaigns include the following: (you can learn more @ http://ignitedlocal.com/)</p>
      <ul class="bullets">
        <li><strong>Foundation Setup &mdash;</strong> Responsive website design, content creation, email marketing design, on-page optimization, etc.</li>
        <li><strong>Local SEO &mdash;</strong> Google Places, Yahoo Local &amp; Bing Local setup, hand-picked directory submissions, local blog posts and articles, videos, and high profile backlinks.</li>
        <li><strong>GEO Targeted Marketing &mdash;</strong> PPC without the cost per click. We drive leads to you while you wait for local SEO to work.</li>
        <li><strong>Local Social Marketing &mdash;</strong> Facebook, Twitter, Google+ marketing, posting content and retweeting content from industry experts.</li>
        <li><strong>ROI Tracking &mdash;</strong> Know everything our campaigns are producing.</li>
      </ul>
      <p>As mentioned our campaigns are guaranteed. We back each of our campaigns with the following:</p>
      <ul class="bullets">
        <li><strong>1st Page Placement Guarantee &mdash;</strong> If we fail to produce your business on the first page of Google within 6 months, we pause your billing and continue working for you on our own dime until we achieve placement.</li>
        <li><strong>ROI Guarantee &mdash;</strong> Just because you are on the 1st Page does not mean you are getting business. We back up our designs and content to convert prospects. If we fail to have our program paying for itself in 6 months, then we also stop your billing and continue working for you at no cost until we achieve this.</li>
      </ul>
      <p>I would like to invite you to view our online presentation that breaks down our product and why we are different than the rest.</p>
      <p>Visit www.ignitedlocal.video to watch now.</p>
      <p>I look forward to getting your feedback on our presentation and how we can help your business.</p>
      <p>Kind Regards,</p>
      <p><?php $this->p($User->firstname); ?> <?php $this->p($User->lastname); ?></p>
      <p>ignitedLOCAL</p>
      <p>Sales Professional</p>
      <p><?php $this->p($User->phone); ?></p>
      <p><?php $this->p($User->username); ?></p>
      <p>www.ignitedLOCAL.com</p>
    </div>

    <div class="whead exp3">
      <h6>Yelp Follow-Up Email</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p>Hello {first_name},</p>
      <p>Thank you for your time today. As discussed, by simply changing your website to be mobile-friendly you can convert more of those visitors that are finding you through Yelp, on mobile devices.</p>
      <p>I would like to schedule some time to discuss exactly what we do for our clients and how our campaigns are different from what you are running today.</p>
      <p>This is roughly 55-65% of all the people that are visiting your website today. Currently, this is what your business looks like on all devices:</p>
      <p>www.ignitedlocal.com/responsive/?url=example.com</p>
      <p>Our digital marketing campaigns not only set your brand up with a responsive (mobile-friendly) website that converts more visitors into customers, we also make your business more visible in all the other online places such as Google and social media sites.</p>
      <p>All of our campaigns include the following:</p>
      <ul class="bullets">
        <li><strong>Foundation Setup &mdash;</strong> Responsive website design, content creation, email marketing design, on-page optimization, etc.</li>
        <li><strong>Local SEO &mdash;</strong> Google Places, Yahoo Local &amp; Bing Local setup, hand-picked directory submissions, local blog posts and articles, videos, and high profile backlinks.</li>
        <li><strong>GEO Targeted Marketing &mdash;</strong> PPC without the cost per click. We drive leads to you while you wait for local SEO to work.</li>
        <li><strong>Local Social Marketing &mdash;</strong> Facebook, Twitter, Google+ marketing, posting content and retweeting content from industry experts.</li>
        <li><strong>ROI Tracking &mdash;</strong> Know everything our campaigns are producing.</li>
      </ul>
      <p>As mentioned our campaigns are guaranteed. We back each of our campaigns with the following:</p>
      <ul class="bullets">
        <li><strong>1st Page Placement Guarantee &mdash;</strong> If we fail to produce your business on the first page of Google within 6 months, we pause your billing and continue working for you on our own dime until we achieve placement.</li>
        <li><strong>ROI Guarantee &mdash;</strong> Just because you are on the 1st Page does not mean you are getting business. We back up our designs and content to convert prospects. If we fail to have our program paying for itself in 6 months, then we also stop your billing and continue working for you at no cost until we achieve this.</li>
      </ul>
      <p>I would like to invite you to view our online presentation that breaks down our product and why we are different than the rest.</p>
      <p>Visit www.ignitedlocal.video to watch now.</p>
      <p>I look forward to getting your feedback on our presentation and how we can help your business.</p>
      <p>Kind Regards,</p>
      <p><?php $this->p($User->firstname); ?> <?php $this->p($User->lastname); ?></p>
      <p>ignitedLOCAL</p>
      <p>Sales Professional</p>
      <p><?php $this->p($User->phone); ?></p>
      <p><?php $this->p($User->username); ?></p>
      <p>www.ignitedLOCAL.com</p>
    </div>
  </div>
</div>

<div class="fluid">
  <div class="widget">
    <div class="whead"><h6>Website Examples</h6></div>
    <div class="formRow">
      <p>Below are a few examples of the work we do. Please note, these are not reference accounts, we take our business owners privacy very serious, and we do not want prospects calling these businesses asking for references. If you need a reference, please reach out to support and we will get approval and setup a time that is convenient to our customer. You can however send these examples to potential customers to view the quality they should expect.</p>
      <ul class="bullets">
        <li><a href="http://austinartificialgrass.com/" class="_blank">austinartificialgrass.com</a></li>
        <li><a href="http://premierluxurytransportation.com/" class="_blank">premierluxurytransportation.com</a></li>
        <li><a href="http://www.shermansecuritycameras.com/" class="_blank">shermansecuritycameras.com</a></li>
        <li><a href="http://personal-injury-attorney.co/" class="_blank">personal-injury-attorney.co</a></li>
        <li><a href="http://lonestarpodiatry.com/" class="_blank">lonestarpodiatry.com</a></li>
        <li><a href="http://www.milwaukeepainclinic.com/" class="_blank">milwaukeepainclinic.com</a></li>
        <li><a href="http://cpafirmlosangeles.net/" class="_blank">cpafirmlosangeles.net</a></li>
        <li><a href="http://www.commerciallawtexas.com/" class="_blank">commerciallawtexas.com</a></li>
        <li><a href="http://prosurveillancegear.com/" class="_blank">prosurveillancegear.com</a></li>
        <li><a href="http://www.wvmb.com/" class="_blank">wvmb.com</a></li>
        <li><a href="http://harploanprogram.net/" class="_blank">harploanprogram.net</a></li>
        <li><a href="http://www.rjdeal.com/" class="_blank">rjdeal.com</a></li>
        <li><a href="http://www.tomburtonlaw.com/" class="_blank">tomburtonlaw.com</a></li>
        <li><a href="http://painting-services.net/" class="_blank">painting-services.net</a></li>
        <li><a href="http://qualityhvacservices.com/" class="_blank">qualityhvacservices.com</a></li>
        <li><a href="http://www.elpasosecuritycameras.com/" class="_blank">elpasosecuritycameras.com</a></li>
        <li><a href="http://woodlandsfamilylaw.com/" class="_blank">woodlandsfamilylaw.com</a></li>
        <li><a href="http://groffteam.com/" class="_blank">groffteam.com</a></li>
      </ul>
    </div>
  </div>
</div>

<?php if($isManager): ?>

<div class="fluid">
  <div class="widget">
    <div class="whead"><h6>Sales Manager Resources</h6></div>
    <div class="formRow"><p>Below you will find resources to help you launch your sales team recruiting efforts. Please note, to maximize your efforts on classified job postings, the best thing to do is invest $25 in major markets for your postings on <a href="https://www.craigslist.org/about/sites" class="_blank">Craigslist</a>.</p></div>

    <div class="whead exp3">
      <h6>Job Posting Sites</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p><strong class="green">Excellent Free Online Classifieds Websites</strong></p>
      <ul class="bullets">
        <li><a href="https://www.craigslist.org/about/sites" class="_blank">craigslist.org</a></li>
        <li><a href="https://www.indeed.com/" class="_blank">indeed.com</a></li>
        <li><a href="https://www.ziprecruiter.com/" class="_blank">ziprecruiter.com</a></li>
        <li><a href="https://www.classifiedads.com/" class="_blank">classifiedads.com</a></li>
        <li><a href="https://www.aftercollege.com/" class="_blank">aftercollege.com</a></li>
        <li><a href="https://justjobs.com/" class="_blank">justjobs.com</a></li>
        <li><a href="https://www.trovit.com/" class="_blank">trovit.com</a></li>
        <li><a href="https://www.careervitals.com/" class="_blank">careervitals.com</a></li>
        <li><a href="https://jooble.org/" class="_blank">jooble.org</a></li>
        <li><a href="https://www.ebayclassifiedsgroup.com/" class="_blank">ebayclassifiedsgroup.com</a></li>
        <li><a href="https://www.monster.com/" class="_blank">monster.com</a></li>
        <li><a href="http://www.topusajobs.com/" class="_blank">topusajobs.com</a></li>
        <li><a href="https://www.olx.com/" class="_blank">olx.com</a></li>
        <li><a href="https://www.careerjet.com/" class="_blank">careerjet.com</a></li>
        <li><a href="https://www.careerbliss.com/" class="_blank">careerbliss.com</a></li>
        <li><a href="https://www.oodle.com/" class="_blank">oodle.com</a></li>
        <li><a href="https://www.careerbuilder.com/" class="_blank">careerbuilder.com</a></li>
        <li><a href="http://www.juju.com/" class="_blank">juju.com</a></li>
      </ul>
    </div>

    <div class="whead exp3">
      <h6>Sample Job Posting</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <p><strong class="red">(Change out http://yourlink.com/ with yours and Los Angeles, CA with the city, state you are posting to.)</strong></p>
      <pre class="code">
        <div class="SRC_Wrap">Innovative local digital marketing firm exploding across the nation.
We need passionate, hardworking individuals from Los Angeles to help our expansion.
This a 4 billion dollar market that is in desperate need of our solution.

We have been delivering powerful solutions for over 7 years,
to thousands of small business owners.

We have a sales support system, which does all the heavy lifting for you.
Anyone can plug into this system without any experience in
Local Digital Marketing.

***You have not seen an opportunity like this in Los Angeles***

**Lucrative monthly residual based compensation plan.
**Be your own boss and work from home in Los Angeles or anywhere in the world.
**No Sales Quota and can offer solutions outside of Los Angeles
**Expert Sales &amp; Product Training
**Agency overrides and bonus comps.
**You can work Part-Time or Full-Time
**Technology platform to manage accounts.
**Proven sales support system.
**No experience needed, we offer proprietary training.

***THIS IS NOT AFFILIATE MARKETING, OR A PYRAMID SCHEME***

Visit &lt;a href=&quot;http://yourlink.com&quot;&gt;Overview Video - Play Now&lt;/a&gt; to schedule a conference
with our Business Development Manager that supports Los Angeles, CA</div></pre>
    </div>

    <div class="whead exp3">
      <h6>Sample Job Post Titles</h6>
      <span class="chevron"></span>
    </div>
    <div class="formRow hide">
      <pre class="code">
        <div class="SRC_Wrap">***Make $6k-$12K+/Month From A $4 Billion Dollar Market***

$60K+/YR Part-Time OR $120K+/YR Full-Time w/ Digital Marketing Leader

Revolutionary Local Digital Marketing Company Seeking Expansion Help

***Generate $5k-$12K+/Month In A $4 Billion Dollar Market***</div></pre>
    </div>
  </div>
</div>

<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.6.1/iframeResizer.min.js"></script>
<script>
iFrameResize({
  log                       : true,
  heightCalculationMethod   : 'taggedElement'
},'#iframe25');
iFrameResize({log:true},'#iframe89');
</script>