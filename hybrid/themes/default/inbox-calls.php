<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Calls Inbox</h6></div>

<?php 
                        $this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('InboxCalls'),
					'class' => 'col_1st,col_2nd,col_end',
					'id' => 'jInboxCalls',
					'data' => $InboxCalls,
					'rowkey' => 'InboxCall',
					'rowtemplate' => 'inbox-calls.php',
					'total' => $totalRecords,
					'sourceurl' => $this->action_url('campaign_ajaxinboxcalls'),
                    'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
                    ?>

    </div>
</div>



<script type="text/javascript">
<?php $this->start_script(); ?>


<?php $this->end_script(); ?>
</script>