<!-- ######################## -->
<!-- ##### Listing Info ##### -->
<!-- ######################## -->
<form action="<?php echo $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
<input type="hidden" name="listing[type]" value="<?php echo $dirarticle->getNetworkListingType(); ?>" />
<input type="hidden" name="listing[id]" value="<?php echo $dirarticle->getID(); ?>" />
<div class="fluid chosen_dropfix">
    <div class="widget grid12">
        <div class="whead">
            <h6>Listing Information</h6>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Business Name</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[name]" value="<?php $dirarticle->html('name'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Phone</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[phone]" value="<?php $dirarticle->html('phone'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Street Address</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[street]" value="<?php $dirarticle->html('street'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>City</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[city]" value="<?php $dirarticle->html('city'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>State</lable>
            </div>
            <div class="grid9">
							<?php $this->getWidget('states-dropdown.php', array('name' => 'listing[state]', 'value' => $dirarticle->state)); ?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Zip Code</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[zip]" value="<?php $dirarticle->html('zip'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Business Category</lable>
            </div>
            <div class="grid9">
                 <?php $this->getControl('industry-select.php', array('selectname' => 'listing[category]',
                     'value' => $dirarticle->category)); ?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Website URL</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[website_url]" value="<?php $dirarticle->html('website_url'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Business Listing Description</lable>
            </div>
            <div class="grid9">
                <textarea cols="" name="listing[description]" rows="8"><?php $dirarticle->html('description'); ?></textarea>
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            	<lable>Business Logo</lable>
            </div>
            <?php
                        $widgets['photoupload']->displayInstance('photo-upload.php',
                            array('name' => 'listing[photo]',
                                    'value' => $dirarticle->photo));
												?>
            
            <div class="clear"></div>
        </div>        
    </div>
</div>
</form>