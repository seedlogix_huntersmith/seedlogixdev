<?php $canEdit = !$User->can('edit_BLOG',$blog->getID()) ? ' disabled' : ''; ?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <form action="<?= $this->action_url('Blog_save?ID='.$blog->id); ?>" method="POST" class="ajax-save">
            <div class="widget grid6">
                <div class="whead"><h6>SEO Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Title:</label></div>
                    <div class="grid9"><input type="text" value="<?php $this->p($blog->seo_title); ?>" name="blog[seo_title]"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Meta Description:</label></div>
                    <div class="grid9"><input type="text" name="blog[seo_desc]" value="<?php $this->p($blog->seo_desc); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Meta Keywords:</label></div>
                    <div class="grid9"><input type="text" class="tags" id="dummy" name="blog[seo_keywords]" value="<?php $this->p($blog->seo_keywords); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Keywords:</label></div>
                    <div class="grid9"><input type="text" name="blog[keywords]" id="blog_keywords" class="tags" value="<?php $this->p($blog->keywords); ?>"<?= $canEdit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Google Webmaster Tools:</label></div>
                    <div class="grid9 j-dual-ide"><?php $this->getWidget('wysiwyg-ide.php',array('fieldname' => 'blog[google_webmaster]','fieldvalue' => $blog->google_webmaster)); ?></div>
                </div>
            </div>
    </form>
</div>


<script type="text/javascript">

<?php $this->start_script(); ?>

$(document).ready(function(){

<?php $this->getJ('wysiwyg-ide.php',array('disable' => !empty($canEdit))); ?>

});

<?php $this->end_script(); ?>

</script>