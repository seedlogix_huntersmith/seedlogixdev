<?php
/**
 * @var $this Template
 * @var $Campaign CampaignModel
 * @var $collab CampaignCollab
 * @var $websites stdClass[]
 */
?>

<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
	<input type="hidden" name="collab[id]" value="<?= $collab->getID(); ?>">
	<input type="hidden" name="collab[campaign_id]" value="<?= $collab->campaign_id; ?>">
	<div class="fluid">
		<div class="widget grid4">
			<div class="whead"><h6>Response Settings</h6></div>
			<div class="formRow">
				<div class="grid3"><label>Name:</label></div>
				<div class="grid9"><input type="text" name="collab[name]" value="<?= $collab->name; ?>"></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Email:</label></div>
				<div class="grid9"><input type="text" name="collab[email]" value="<?= $collab->email; ?>"></div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Scope:</label></div>
				<div class="grid9">
					<select id="scopeSelect" name="collab[scope]">
						<option value="campaign"<?php $this->selected($collab->scope,'campaign'); ?>>Campaign</option>
						<option value="custom"<?php $this->selected($collab->scope,'custom'); ?>>Website</option>
					</select>
				</div>
			</div>
			<div class="formRow hide" id="website-list">
				<div class="grid3"><label>Websites:</label></div>
				<div class="grid9">

<?php
$this->getWidget('select-ajax.php',array(
	'id'				=> 'websites',
	'name'				=> 'collab[websites][]',
	'ajaxUrl'			=> $this->action_url('Campaign_ajaxTouchpoints',array('ID' => $Campaign->getID())),
	'field_value'		=> 'hub_ID',
	'text'				=> ':touchpoint_name: (:hostname:)',
	'multiple'			=> true,
	'params'			=> array('type' => 'hub'),
	'data'				=> $websites,
	'value'				=> $websites
));
?>

				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Enabled?</label></div>
				<div class="grid9">
					<select name="collab[active]">
						<option value="1"<?php $this->selected($collab->active,1); ?>>Yes</option>
						<option value="0"<?php $this->selected($collab->active,0); ?>>No</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){

		checkScope();

        function checkScope(){
            var scopeVal = $('#scopeSelect option:selected').text();
            if(scopeVal == 'Campaign'){
				var websites = $('#websites');
				$('#website-list').hide();
				websites.val(null);
				websites.change();
			}
            else
                $('#website-list').show();
        }

        $('#scopeSelect').change(function(){ checkScope(); });
	
	});
    
<?php $this->end_script(); ?>
</script>