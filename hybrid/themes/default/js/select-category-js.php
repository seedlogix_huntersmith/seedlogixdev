//Button Create Blog Post

$('#btnCreateBlogPost').click(function(){
    $('#selectData').attr('name','post[category]');
    $('#inputClear').attr('name','');
    $('.inputSwitcher .inputSwitcher-select').show();
    $('.inputSwitcher .inputSwitcher-input').hide();
    return false;
});

//Select Category Create New Post


//Change select into input (for selecting category or making a new one)

$('#selectData').change(function(event){
    event.preventDefault();
    event.stopPropagation();

    $('#post_title').focus();
    var selectVal = $(this).val();

    if(selectVal == '_new')
    {
        $('#inputClear').attr('name', $(this).attr('name'));
        $(this).attr('name','');
        $('.inputSwitcher .inputSwitcher-select').hide();
        $('.inputSwitcher .inputSwitcher-input').show();
    }

    return false;
});

//Button Create New Post

$('#btnBack').click(function(event){
    event.preventDefault();
    event.stopPropagation();
    $('#selectData').attr('name', $('#inputClear').attr('name'));
    $('#inputClear').attr('name','');
    $('#post_title').focus();
    $('.inputSwitcher .inputSwitcher-select').show();
    $('.inputSwitcher .inputSwitcher-input').hide();

    return false;
});