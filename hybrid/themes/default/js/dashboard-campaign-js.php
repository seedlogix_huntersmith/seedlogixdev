<?php
//if($User->id == 6610 || !$User->can('reports_only')):
    $this->getJ('Keywords.js');
/* $this->getJ('Rankings.js'); */
?>

KwRequest.prototype.params          = {};
KwRequest.prototype.send            = function(){
                                    var _this = this;
                                    sendAjax('<?= $this->action_url('piwikAnalytics_ajaxGetKeywords'); ?>',
                                    function(ajax){_this.process(ajax);},
                                    Dashboard.setIntPer(_this.params,$('.graph_rangectrl')));
                                    }

<?php /*
Rankings.params.TOUCHPOINT_TYPE     = '<?= $touchpoint_type; ?>';
Rankings.send                       = function(){
                                    sendAjax('<?= $this->action_url('piwikAnalytics_ajaxgetKeywordRankings'); ?>',
                                    Rankings.receive,
                                    Dashboard.setIntPer(Rankings.params,$('.graph_rangectrl')));
                                    }
*/ ?>

<?php //endif; ?>

$(function(){

<?php /* if($User->id == 6610 || !$User->can('reports_only')): ?>

    var df          = [
                    ['Websites<br><strong>' + <?= (int)$touchpoints['WEBSITE']; ?> + '</strong>',<?= (int)$touchpoints['WEBSITE']; ?>],
                    ['Blogs<br><strong>' + <?= (int)$touchpoints['BLOG']; ?> + '</strong>',<?= (int)$touchpoints['BLOG']; ?>],
                    ['Landing Pages<br><strong>' + <?= (int)$touchpoints['LANDING']; ?> + '</strong>',<?= (int)$touchpoints['LANDING']; ?>]
                    ];
    var data        = [{data:df,grow:{stepMode:'linear'}}];
    var data        = [[<?= (int)$touchpoints['WEBSITE']; ?>,0],[<?= (int)$touchpoints['BLOG']; ?>,1],[<?= (int)$touchpoints['LANDING']; ?>,2]];
    var dataset     = [{label:'Total Touchpoint Volume',data:data,color:'#93c8ec'}];
    var ticks       = [[0,'<span class="icos-frames"></span>'],[1,'<span class="icos-list"></span>'],[2,'<span class="icos-documents"></span>']];
    var options     = {
                    series      : {
                                bars    : {
                                        show        : true,
                                        barWidth    : 0.5,
                                        align       : 'center',
                                        horizontal  : true
                                        }
                                },
                                yaxis   : {
                                        mode        : 'categories',
                                        ticks       : ticks,
                                        tickLength  : 0,
                                        font        : {
                                                    size                : 11,
                                                    lineHeight          : 13,
                                                    weight              : 'bold',
                                                    family              : 'sans-serif',
                                                    color               : '#545454'
                                                    }
                                        },
                                        grid        : {
                                                    hoverable           : true,
                                                    borderWidth         : 2,
                                                    backgroundColor     : {colors:['#fff','#edf5ff']}
                                                    }
                    };

    $.plot('#placeholder',dataset,options);

    var ss = null,previousLabel = null,contentLabel = null;

    $.fn.UseTooltip = function(){
        $(this).bind('plothover',function(event,pos,item){
            if(item){
                if((previousLabel != item.series.label) || (previousPoint != item.dataIndex)){
                    previousPoint   = item.dataIndex;
                    previousLabel   = item.series.label;
                    $('#tooltip').remove();
                    var x           = item.datapoint[0];
                    var y           = item.datapoint[1];
                    switch(y){
                        case 0:
                            contentLabel    = 'Websites';
                            break;
                        case 1:
                            contentLabel    = 'Blogs';
                            break;
                        case 2:
                            contentLabel    = 'Landing Pages';
                            break;
                    }
                    var color       = item.series.color;
                    showTooltip(item.pageX,item.pageY,color,'<strong>' + item.series.label + '</strong><br>' + contentLabel + ': <strong>' + x + '</strong>');
                }
            } else{
                $('#tooltip').remove();
                previousPoint       = null;
            }
        });
    };

    function showTooltip(x,y,color,contents){
        $('<div id="tooltip">' + contents + '</div>').css({
            position                : 'absolute',
            display                 : 'none',
            top                     : y - 70,
            left                    : x - 120,
            border                  : '2px solid ' + color,
            padding                 : '3px',
            'font-size'             : '9px',
            'border-radius'         : '5px',
            'background-color'      : '#fff',
            'font-family'           : 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            opacity                 : 0.9
        }).appendTo('body').fadeIn(200);
    }

    $('#placeholder').UseTooltip();

<?php endif; */ ?>

/*

<?php
$json_arr           = $chart->getDataSets();
$json_config        = $chart->getConfig()/*$User->id == 6610 || !$User->can('reports_only') ? $chart->getConfig() : array('interval' => 30,'period' => 'DAY')*/;

//echo date('Y-m-d H:i:sP',1519862400).PHP_EOL;

//if($User->id != 6610 && $User->can('reports_only')){
if($User->id != 6610){
    $calls_data                 = $json_config['interval'] == 7 ? $callReportsChart : $callReportsChart7;
    $calls_total                = $json_config['interval'] == 7 ? $totalResultsChart : $totalResultsChart7;
	$callreport_data			= $callRecordsChart;
    $range_arr                  = array();
    $dates_arr                  = array();
    $data_arr                   = array();
    foreach($json_arr as $data){
        foreach($data['data'] as $date){
            $range_arr[]        = $date['date'];
        }
    }
    $range_arr                  = array_fill_keys(array_unique($range_arr),0);
    /*foreach($calls_data as $call){
        $dates_arr[]            = substr($call['answer_time'],0,10);
    }*/
	//print_r($callreport_data);
	
	foreach($callreport_data as $call){
		echo substr($call->date,0,10).'<br>';
        $dates_arr[]            = substr($call->date,0,10);
		
    }

	//var_dump($dates_arr);
    $dates_arr                  = array_count_values($dates_arr);
    foreach($dates_arr as $k => $v){
        if(array_key_exists($k,$range_arr)){
            $range_arr[$k]      += $v;
        } else{
            $range_arr[$k]      = $v;
        }
    }

    foreach($range_arr as $k => $v){
        $data_arr[]             = array(
                                'timestamp'     => strtotime(date('Y-m-d',strtotime($k.' -1 day')).' 18:00:00-06:00'),
                                'value'         => $v,
                                'date'          => $k
                                );
    }
    $json_arr[]                 = array(
                                'label'         => '&nbsp;Calls',
                                'data'          => $data_arr,
                                'total'         => $calls_total/*array_sum($dates_arr)*/
                                );
}
?>

*/

    var chartdata = <?= json_encode($json_arr); ?>;

    if(chartdata == false){
        Dashboard.fetchGraph.call($('#chartconfig')[0]);
    } else{
        Dashboard.reqsettings.success({'pw':chartdata,'params':<?= json_encode($json_config); ?>});
    }
});

<?php //if($User->id == 6610 || !$User->can('reports_only')): ?>
function processResult(ajax){
    if(ajax.error){
        alert('error');
    }
    var d = ajax.pw;
    for(var n in d){
        for(var c in d[n]){
            $('#jcampaign_' + d[n].campaign_id + ' .' + c).html(d[n][c]);
        }
    }
}

/**
    Dashboard functions for controlling the date range buttons
    Main code is in functions.js
    can search for checkDateRangeSelect()
**/

var refreshDash = function(){
    Keywords.refresh();
    Social.refresh();

<?php /* Rankings.refresh(); */ ?>

}

/** END of dashboard date range functions **/

bindEvents.add(function(ctr){
    if($(ctr).hasClass('jneedsrefresh')){
        refreshCampaignRow($(ctr)[0]);
        return;
    }
    $('.jneedsrefresh',ctr).each(function(i,e){
        refreshCampaignRow(e);
    });
});

<?php /*
function switchTextbox(event){
    event.preventDefault();
    var row         = $(this).closest('tr');
    var label       = row.find('.campaignName');
    var textBox     = row.find('input[name="campaign[name]"]');
    var tdName      = label.closest('td');
    var id          = label.attr('href').match(/ID=([0-9]+)/)[1];
    tdName.closest('.dataTable').find('input[type="hidden"]').remove();
    tdName.append('<input type="hidden" name="campaign[id]" value="' + id + '">');
    textBox.val(label.text());
    label.hide();
    textBox.show();
    textBox.focus();
}

function blurTextbox(event){
    var textBox     = $(this);
    var label       = textBox.siblings('.campaignName');
    label.text(textBox.val());
    textBox.hide();
    label.show();
}

function textboxChange(event){
    var textBox     = $(this);alert( textBox.get(0) );
    new AjaxSaveDataRequest($.trim( textBox.get(0) ));
}

$(document).ready(function(){
    var dataTable   = $('.dataTable');
    dataTable.on('click','.jeditbtn',switchTextbox);
    dataTable.on('blur','input[type="text"]',blurTextbox);
    dataTable.on('change','input[type="text"]',textboxChange);
});
*/ ?>

<?php //endif; ?>