var HybridJs    =   function ($, defprops){
    function js(actionstr, settings){
        $.ajax(actionstr, settings);
        return this;
    };
    var o   =   $.extend(js, defprops);
    if(o.onload){
        $(document).ready(function (){
            o.onload();
        });
    }
    return o;
};

var Dashboard   = new HybridJs(jQuery,{
                actions     : {},
                context     : {},
                cache       : {},
                plotData    : function(seriesA,seriesB){
                            var data = arguments[0].data;
                            $('.quickStats').empty();
                            $('.quickStats').append(seriesA.mini);
                            $('.quickStats').append(seriesB.mini);
							//$('.quickStats').append(seriesC.mini);
                            $('.sparkline').sparkline('html',{
                                type                : 'line',
                                width               : '100%',
                                height              : '100%',
                                lineColor           : '#9E2FD5',/* #3498db */
                                fillColor           : '#C57FE5',/* #83c0e9 */
                                spotColor           : '#fff',
                                minSpotColor        : '#9E2FD5',/* #0ff */
                                maxSpotColor        : '#9E2FD5',/* #0ff */
                                highlightSpotColor  : '#C57FE5',/* #2384c5 */
                                highlightLineColor  : '#fff',
                                spotRadius          : 3
                            });
							$('.sparkline2').sparkline('html',{
                                type                : 'line',
                                width               : '100%',
                                height              : '100%',
                                lineColor           : '#27bdbe',/* #3498db */
                                fillColor           : '#71E1E1',/* #83c0e9 */
                                spotColor           : '#fff',
                                minSpotColor        : '#27bdbe',/* #0ff */
                                maxSpotColor        : '#27bdbe',/* #0ff */
                                highlightSpotColor  : '#71E1E1',/* #2384c5 */
                                highlightLineColor  : '#fff',
                                spotRadius          : 3
                            });
							$('.sparkline3').sparkline('html',{
                                type                : 'line',
                                width               : '100%',
                                height              : '100%',
                                lineColor           : '#D94600',/* #3498db */
                                fillColor           : 'rgb(238,121,81)',/* #83c0e9 */
                                spotColor           : '#fff',
                                minSpotColor        : '#D94600',/* #0ff */
                                maxSpotColor        : '#D94600',/* #0ff */
                                highlightSpotColor  : 'rgb(238,121,81)',/* #2384c5 */
                                highlightLineColor  : '#fff',
                                spotRadius          : 3
                            });
							
/*
            var date1       = new Date(data[0][0]);
            var date2       = new Date(data[data.length - 1][0]);
            var diff        = (date2.getUTCFullYear() - date1.getUTCFullYear()) * 12 + (date2.getUTCMonth() - date1.getUTCMonth());
            var period      = '';
            var timeFormat  = '%m/%d';
            if(diff >= 12){
                period      = 'MONTH';
                timeFormat  = '%m/%Y';
            } else if(diff >= 3){
                period      = 'MONTH';
                timeFormat  = '%m/%Y';
            } else{
                period      = 'DAY';
            }
*/
            $.plot($('.chart'),arguments,{
                colors  : ['#C57FE5','#27bdbe','rgb(238,121,81)','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b'],
                series  : {
                    lines   : {
                        show    : true,
                        fill    : true
                    },
                    points  : {
                        show    : true,
                        fill    : true
                    }
                },
                grid    : {
                    tickColor   : '#d4e9f4',
                    clickable   : false,
                    hoverable   : true
                },
                xaxis   : {
                    show        : true,
                    mode        : 'time',
                    tickSize    : [1,'day'],
                    timeformat  : '%m/%d'
                    //tickSize    : [1,period.toLowerCase()],
                    //timeformat  : timeFormat
                }
            });

            if(!this.cache.init){
                this.cache.init = true;

                function showTooltip(x,y,contents){
                    var _html   = '<div id="tooltip" class="tooltip">' + contents + '</div>';
                    $(_html).appendTo('body');
                    var _s      = $('div#tooltip.tooltip');
                    var _ow     = _s.outerWidth() / 2;
                    _s.css({
                        top     : y - 42,
                        left    : x - _ow
                    }).fadeIn(300);
                }

                var previousPoint = null;

                $('.chart').bind('plothover',function(event,pos,item){
                    if($('.chart').length){
                        if(item){
                            if(previousPoint != item.dataIndex){
                                previousPoint = item.dataIndex;
                                $('div#tooltip.tooltip').fadeOut(300,function(){
                                    $(this).remove();
                                });
                                var D = new Date(item.series.data[item.dataIndex][0]);
                                var M = D.getUTCMonth();
                                var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                                var x = '';
                                var y = item.datapoint[1] + ', ' + item.series.label;
                                if(item.series.xaxis.tickSize[0] != 365 && item.series.xaxis.tickSize[1] == 'day'){
                                    x = months[M] + ' ' + ('0' + D.getUTCDate()).slice(-2);
                                } else{
                                    x = item.series.xaxis.tickSize[0] != 365 ? months[M] + ' ' + D.getUTCFullYear() : D.getUTCFullYear() - 1;
                                }
                                showTooltip(item.pageX,item.pageY,x + ': <span>' + y + '</span>');
                            }
                        } else{
                            $('div#tooltip.tooltip').fadeOut(300,function(){
                                $(this).remove();
                            });
                            previousPoint = null;
                        }
                    }
                });
            }
        },
        reqsettings: {
            dataType: 'json',
            type: 'POST',
            success: function (ajax){
            if(ajax.error){
                notie.alert({text:ajax.error});
                return false;
            }
                var plotData    =   [];
                $.each(ajax.pw, function (k, dset){
                    var data    =   dset.data;
                    dset.data   =   [];
                    $.each(data, function (k2, point){
                        dset.data.push([point.timestamp*1000, point.value]);
                    });
                    plotData.push(dset);
                });

                var key = ajax.params.interval + '-' + ajax.params.period;
                Dashboard.cache[key] = plotData;
                
                Dashboard.plotData.apply(Dashboard, Dashboard.cache[key]);
            },
            error: function (){
                alert("An error ocurred..");
            }
        },
        
        setIntPer: function (obj, $sel){
            if($sel.length == 0) return;
            var parsed  =   $sel.val().split('-');
            if(!obj) return parsed;
            obj.interval    =   parsed[0];
            obj.period      =   parsed[1];
            return obj;
        },
				updateExportURL: function (context){
					var href=Dashboard.actions.exportURL;
					
					if(context.period)
						href	=	href.replace(/PERIOD/, context.period);				
				
					if(context.interval)
						href	=	href.replace(/INTERVAL/, context.interval);
					
					if(context.sortCol != undefined)
						href	=	href.replace(/SORTCOL/, context.sortCol);
					
					if(context.sortDir)
						href	=	href.replace(/SORTDIR/, context.sortDir);
					
					$("#exportCampaign").attr('href', href);
				},
				changeTimeContext: function (){
					Dashboard.setIntPer(Dashboard.context, $(this));
					Dashboard.updateExportURL(Dashboard.context);
					Dashboard.fetchGraph.apply(this);
				},

        fetchGraph: function (){
            var f   =   $(this.form);
            var cookiename  =   $(this).attr('id');
            var value   =   $(this).val();

            document.cookie =   cookiename  +   '=' +   value;

            var parsed  =   Dashboard.setIntPer(null, $(this));

            f[0].interval.value    =   parsed[0];
            f[0].period.value  =   parsed[1];

            var cachekey    =   value;
            if(Dashboard.cache[cachekey] && $('.admin_cookie[name="disable_cache"]').attr('checked') == undefined)
            {
                Dashboard.plotData.apply(Dashboard, Dashboard.cache[cachekey]);
            }else
            {
                Dashboard.ajaxSubmitForm(f, Dashboard.reqsettings.success, Dashboard.reqsettings.error);
            }

            $('.dTable').each(function (){
                    $(this).dataTable().fnDraw(false);
            });
        },
        
        user_search: function (e){
            
            if(e && e.type == 'keyup'){
                clearTimeout(Dashboard.user_to);
                Dashboard.user_to =   setTimeout(Dashboard.user_search, 400);
                return;
            }
            
            var val = $('.user-search').val();

            if(val.length < 3 && val.match(/^[0-9]+$/) == null) return false;
                      
            
            $('#topSearch .result').remove();  
            
            Dashboard($('#topSearch form').attr('action'), {
                    type: 'post', 
                    dataType: 'html',
                    data: {q: val},
                    context: $('#topSearch'),
                    success: function (f){                        
                            this.append(f);
                    }
                });
            return false;
        },
        
        onload   : function (){
					
						Dashboard.setIntPer(Dashboard.context, $('.graph_rangectrl'));
					
						$('.action_url').each(function (){
							Dashboard.actions[this.name]	=	this.value;
						});
						
            $('.graph_rangectrl').change(Dashboard.changeTimeContext);

            $('.hoverShow').click(function (){
                $('#subusers').slideToggle();
            });
            
            // any element of class jopen-dialog can have a jopen-(dialogname) class
            // the dialogname is used to find the #jdialog-(dialogname) dialog and open it.
            
            $('.jopendialog').click(function (e){
                e.preventDefault();
                var classes =   $(this).attr('class');
                var diagid  =   '#jdialog-'+ (classes.match(/jopen-([^\s]+)/).pop());
                var title   =   $(this).data('title');
                
                if(title)
                    $(diagid).dialog('option', 'title', title);
                
                $(diagid).dialog('open');
            });

            $('.user-search').keyup(Dashboard.user_search);
            $('#user-search').submit(Dashboard.user_search);

            return this;
        },
        
        ajaxSubmitForm  : function (form, successf, errorf, contxt){
			var includeFiles = form.attr('enctype') == 'multipart/form-data';
			var data = includeFiles ? new FormData(form.get(0)) : form.serialize();
            $.ajax(form.attr('action'),
                    {   
                        dataType: 'json',
                        type: 'POST',
                        beforeSend: function(){
                            //this.close();
                            $("#loading").dialog('open').html("<p>Please Wait...</p>");
                        },
                        data: data,
                        success: successf,
                        error: errorf,
                        context: contxt,
                        complete: function() {
                            $("#loading").dialog('close');
                        },
						processData: !includeFiles,
						contentType: includeFiles ? false : 'application/x-www-form-urlencoded; charset=UTF-8'
                    });
        }
    });          


var CreateDialog  =   function (create_text, fajax_success, dialogselector){
    var cc  =   this;

    this.open   =   function (){
        $(dialogselector).dialog('open');
    };
    
    this.close  =   function (){
        $(dialogselector).dialog('close');
    };
    
    this.buttons    =   {
        Cancel: function() {
                $( this ).dialog( "close" );
        }
    };

    function submitAjax() {
        Dashboard.ajaxSubmitForm(cc.form, fajax_success, null, cc);

        return false;
    };
    
    this.form   =   $(dialogselector + ' form');
    
    $(this.form).bind('submit', submitAjax);
    this.buttons[create_text]   =   submitAjax;

    return this;
};


var InsertObjectAtTable =   function (tbl, objbindings, dashboardurl){
    var T   =   $(tbl);
  $(tbl).dataTable().fnAddData(objbindings);
  
  var newrow        =   $('tbody tr:not(.gradeA)', T);
  var firstchild        =   newrow.find('td:first-child');

    newrow.find('td').addClass('center');
    firstchild.removeClass('center').wrapInner('<a href="#" />');
    
    $('a', firstchild).attr('href', dashboardurl);
    
    newrow.addClass('new-row').addClass('gradeA');

    var fbg =   newrow.css('backgroundColor');
    var green   =   '#9cffff';

    newrow.stop().css("background-color", green);
    setTimeout(function (){
        newrow.css('background-color', '');
        setTimeout(function (){
            newrow.css('background-color', green);
            setTimeout(function (){
                    newrow.css('background-color', '');
            }, 200);
        }, 200);
    }, 400);
};

function createDashBoard(){
   Dashboard.plotData.apply(Dashboard,arguments);
}

    function sendAjax(url, fsuccess, fdata, type){        
        if(!type) type = 'POST';
        $.ajax(url, {
                dataType: 'json',
                data: fdata,
                error: CheckAjaxResponse(),
                success: CheckAjaxResponse(fsuccess),
                type: type
        });
    }
    
function flashit(el, finishf){
            el.stop().fadeOut(600, finishf);
}

function bindEvents(ctr){
    return function (){
        for(var i in bindEvents.evts){
            bindEvents.evts[i].call(this, ctr);
        }
    };
}
bindEvents.evts   =   [];
bindEvents.add    =   function (f){
    this.evts.push(f);
}

$(bindEvents(document));

function getValidationMessages(validationobj){
    var m   =   [];
    for(var i in validationobj){
        for(var mi in validationobj[i]){
            m.push(validationobj[i][mi]);
        }
    }
    return m;
}

function Social(){}

Social.params   =   {chart: "SocialStatsChartData"};

Social.refresh  =   function (){
        var f   =   $('.jsocialchart');
//    console.log('sending..?', f);

    if(f.length ==  0) return;
        sendAjax(f.attr('action'), function (ajax){
            if(ajax.error){
                notie.alert({text:ajax.error});
                return false;
            }
            
            var plotData    =   [];
            $.each(ajax.pw, function (k, dset){
                var data    =   dset.data;
                dset.data   =   [];
                $.each(data, function (k2, point){
                    dset.data.push([point.timestamp*1000, point.value]);
                });
                plotData.push(dset);
            });
            
//            console.log(ajax, 'heyoo', plotData);
            Social.plot.apply(Social, plotData);
        }, Dashboard.setIntPer(Social.params, $('.graph_rangectrl')));
}

$(Social.refresh);

Social.plot     = function(){
    var $chart  = $('.jsocialchart .jchart1');

    if($chart.length == 0) return;

    $.plot($chart,arguments,{
        colors  : ['#C57FE5','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b'],
        series  : {
            lines   : {
                show    : true,
                fill    : false
            },
            points  : {
                show    : true,
                fill    : true
            }
        },
        grid    : {
            tickColor   : '#d4e9f4',
            clickable   : false,
            hoverable   : true
        },
        xaxis   : {
            show        : true,
            mode        : 'time',
            timeformat  : '%m/%d'
        }
    });

    function showTooltip(x,y,contents){
        var _html   = '<div id="tooltip" class="chart-tooltip">' + contents + '</div>';
        $(_html).appendTo('body');
        var _s      = $('div#tooltip.chart-tooltip');
        var _ow     = _s.outerWidth() / 2;
        _s.css({
            top     : y - 42,
            left    : x - _ow
        }).fadeIn(300);
    }

    var previousPoint = null;

    $chart.bind('plothover',function(event,pos,item){
        if($chart.length){
            if(item){
                if(previousPoint != item.dataIndex){
                    previousPoint = item.dataIndex;
                    $('div#tooltip.chart-tooltip').fadeOut(300,function(){
                        $(this).remove();
                    });
                    var D = new Date(item.series.data[item.dataIndex][0]);
                    var M = D.getUTCMonth();
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                    var x = '';
                    var y = item.datapoint[1] + ', ' + item.series.label;
                    if(item.series.xaxis.tickSize[0] != 365 && item.series.xaxis.tickSize[1] == 'day'){
                        x = months[M] + ' ' + ('0' + D.getUTCDate()).slice(-2);
                    } else{
                        x = item.series.xaxis.tickSize[0] != 365 ? months[M] + ' ' + D.getUTCFullYear() : D.getUTCFullYear() - 1;
                    }
                    showTooltip(item.pageX,item.pageY,x + ': <span>' + y + '</span>');
                }
            } else{
                $('div#tooltip.chart-tooltip').fadeOut(300,function(){
                    $(this).remove();
                });
                previousPoint = null;
            }
        }
    });
};


$.fn.dataTableExt.oApi.fnAddTr = function ( oSettings, nTr, bRedraw ) {
    if ( typeof bRedraw == 'undefined' )
    {
        bRedraw = true;
    }
 
    var nTds = nTr.getElementsByTagName('td');
    if ( nTds.length != oSettings.aoColumns.length )
    {
        alert( 'Warning: not adding new TR - columns and TD elements must match' );
        return;
    }
 
    var aData = [];
    var aInvisible = [];
    for ( var i=0 ; i<nTds.length ; i++ )
    {
        aData.push( nTds[i].innerHTML );
        if (!oSettings.aoColumns[i].bVisible)
        {
            aInvisible.push( i );
        }
    }
 
    /* Add the data and then replace DataTable's generated TR with ours */
    var iIndex = this.oApi._fnAddData( oSettings, aData );
    nTr._DT_RowIndex = iIndex;
    oSettings.aoData[ iIndex ].nTr = nTr;
 
    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
 
    // Hidding invisible columns
    for ( var i = (aInvisible.length - 1) ; i >= 0 ; i-- )
    {
        oSettings.aoData[iIndex]._anHidden[ i ] = nTds[aInvisible[i]];
        nTr.removeChild( nTds[aInvisible[i]] );
    }
 
    // Redraw
    if ( bRedraw )
    {
        this.oApi._fnReDraw( oSettings );
    }
};



$.fn.dataTableExt.oApi.fnGetPageOfRow = function (oSettings, iRow) {
    // get the displayLength being used
    var displayLength = oSettings._iDisplayLength;

    // get the array of nodes, sorted (default) and using current filters in place for all pages (default)
    // see http://datatables.net/docs/DataTables/1.9.beta.1/DataTable.html#%24_details for more detals
    var taskListItems = this.$("tr", { "filter": "applied" });

    // if there's more than one page continue, else do nothing
    if (taskListItems.length <= displayLength) return;

    // get the index of the row inside that sorted/filtered array
    var index = taskListItems.index(iRow);

    // get the page by removing the decimals
    var page = Math.floor(index / displayLength);

    // paginate to that page 
    this.fnPageChange(page);
};