
    <?php if(!$disable): ?>
    function saveCode(cleditor_textarea, cleditor_obj){
        return function (cm, ev){
          var ta    =   cm.getTextArea();
          var srccode   =    cm.getValue();

          new AjaxSaveDataRequest(ta, srccode);

          cleditor_textarea.setContent(srccode);
          //cleditor_obj.updateFrame();
          //cleditor_obj.refresh();
        }
    }
    <?php endif;?>

        tinymce.init({

            selector: "textarea.j-cleditor",
            autoresize_min_height : 200,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            valid_elements : '*[*]',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor textcolor",
                "searchreplace visualblocks code",
                "insertdatetime media table contextmenu paste autoresize"
            ],
            external_plugins: {
                "moxiemanager": "/themes/default/js/plugins/moxiemanager/plugin.min.js"
            },
            relative_urls: false,
            remove_script_host: true,
            moxiemanager_title: 'File Manager',
            toolbar: "insertfile undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright | alignjustify | bullist numlist outdent indent | link image | fontsizeselect",
            setup: function(editor) {
                var jDualIde = $(this.getElement()).closest('.j-dual-ide');
                editor.settings.readonly = jDualIde.data('disable');
                var CodeMirrorObj = CodeMirror.fromTextArea(jDualIde.find('textarea.j-codemirror').get(0), {
                    lineNumbers: true,
                    lineWrapping: true,
                    mode: "htmlmixed",
                    styleActiveLine: true,
                    viewportMargin: Infinity,
                    readOnly: jDualIde.data('disable')<?//= $disable ? 'true' : 'false' ?>
                });
                jDualIde.data('codeMirror', CodeMirrorObj); <?php //Now you can get the code mirror instance with $('selector to j-Dual-ide').data('codeMirror') ?>
                jDualIde.find("li.j-codemirror").click(function(){
                    CodeMirrorObj.refresh();
                });
                if(!jDualIde.data('disable')){
                    editor.on('blur', function(e) {
                        console.log('change event', e);
                        CodeMirrorObj.setValue(this.getContent());
                        CodeMirrorObj.refresh();
                        new AjaxSaveDataRequest(CodeMirrorObj.getTextArea(), this.getContent());
                    });
                    CodeMirrorObj.on('blur', saveCode(editor, null));
                    CodeMirrorObj.refresh();
                }
            }
        });