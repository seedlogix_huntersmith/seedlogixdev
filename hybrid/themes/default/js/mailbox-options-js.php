/*
    Create Modal for specific mailbox Options 
    There could be multiple mailboxes on initial page. Would need to create as many dialogues as there are mailboxes.
*/
$( '#jdialog-mailboxOptions' ).dialog({
    autoOpen: false,
    height: 200,
    width: 550,
    modal: true,
    close: function ()
    {
        $('#campaign_id', this).val('');
        $('form', this)[0].reset();
    },
    buttons: (new CreateDialog('Save Options',
        function (ajax, status)
        {
            if(!ajax.error)
            {
                notie.alert({text:ajax.message});
                this.close();
                this.form[0].reset();
            }
            else if(ajax.error == 'validation')
            {
                alert(getValidationMessages(ajax.validation).join('\n'));
            }
            else
                alert(ajax.message);
        }, '#jdialog-mailboxOptions')).buttons
});
