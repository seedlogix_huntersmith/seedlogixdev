<?php
$this->getJ('Keywords.js');
$this->getJ('Rankings.js'); 
?>

Social.params.CID = <?php echo $Campaign->id; ?>;

KwRequest.prototype.params = {ID: <?php echo $Campaign->getID(); ?>};
KwRequest.prototype.send = function(){
    var _this   =   this;
    sendAjax("<?= $this->action_url('piwikAnalytics_ajaxGetKeywords?ID=' . $Campaign->id . "&pointtype=" . $_GET['pointtype'] ); ?>",
        function (ajax){ _this.process(ajax); }, 
		Dashboard.setIntPer(_this.params, $('.graph_rangectrl')));
}


Rankings.params.campaign_ID = <?php echo $Campaign->getID(); ?>;
Rankings.params.TOUCHPOINT_TYPE = "<?php echo $touchpoint_type; ?>";
Rankings.send = function (){

    sendAjax("<?= $this->action_url('piwikAnalytics_ajaxgetKeywordRankings'); ?>",
        Rankings.receive,
        Dashboard.setIntPer(Rankings.params, $('.graph_rangectrl')));
}


$(function(){

/*

<?php
$json_arr           = $chart->getDataSets();
$json_config        = $chart->getConfig()/*$User->id == 6610 || !$User->can('reports_only') ? $chart->getConfig() : array('interval' => 30,'period' => 'DAY')*/;

//echo date('Y-m-d H:i:sP',1519862400).PHP_EOL;

//if($User->id != 6610 && $User->can('reports_only')){
if($User->id != 6610){
    $calls_data                 = $json_config['interval'] == 30 ? $callReportsChart : $callReportsChart7;
    $calls_total                = $json_config['interval'] == 30 ? $totalResultsChart : $totalResultsChart7;
	$callreport_data			= $callRecordsChart;
    $range_arr                  = array();
    $dates_arr                  = array();
    $data_arr                   = array();
    foreach($json_arr as $data){
        foreach($data['data'] as $date){
            $range_arr[]        = $date['date'];
        }
    }
    $range_arr                  = array_fill_keys(array_unique($range_arr),0);
    /*foreach($calls_data as $call){
        $dates_arr[]            = substr($call['answer_time'],0,10);
    }*/
	//print_r($callreport_data);
	
	foreach($callreport_data as $call){
		echo substr($call->date,0,10).'<br>';
        $dates_arr[]            = substr($call->date,0,10);
		
    }

	//var_dump($dates_arr);
    $dates_arr                  = array_count_values($dates_arr);
    foreach($dates_arr as $k => $v){
        if(array_key_exists($k,$range_arr)){
            $range_arr[$k]      += $v;
        } else{
            $range_arr[$k]      = $v;
        }
    }

    foreach($range_arr as $k => $v){
        $data_arr[]             = array(
                                'timestamp'     => strtotime(date('Y-m-d',strtotime($k.' -1 day')).' 18:00:00-06:00'),
                                'value'         => $v,
                                'date'          => $k
                                );
    }
    $json_arr[]                 = array(
                                'label'         => '&nbsp;Calls',
                                'data'          => $data_arr,
                                'total'         => $calls_total/*array_sum($dates_arr)*/
                                );
}
?>

*/

    var chartdata   =   <?php echo json_encode($json_arr); ?>;
    if(chartdata == false)
        Dashboard.fetchGraph.call($('#chartconfig')[0]);
    else
        Dashboard.reqsettings.success({"pw": chartdata, "params": <?php echo json_encode($json_config); ?> });

<?php /*
    $('#jsites').dataTable(
     {
        "sDom": '<"H"fl>t<"F"ip>',
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "POST",
        "iDeferLoading": <?php echo $total_tp_sites; ?>,
        "sAjaxSource": "<?php echo $this->action_url('Campaign_ajaxSites?ID=' . $campaign_id . '&pointtype=' . htmlentities($touchpoint_type, ENT_QUOTES)); ?>",
        "fnDrawCallback": bindEvents('#jsites'),
        "fnServerParams": function ( aoData ) {
            aoData.push({ "name" : "sSortDir_0", "value" : "desc" });
            aoData.push({ "name" : "iSortCol_0", "value": '6' });
        },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                            var siteid  =   nRow.id.split(/_/).pop();
                            var classnames  =   ['name', 'visitsChange', 'visits', 'numleads', 'conversionrate'];
                            var actionsCol  =   $('td:eq(7)', nRow);

                            actionsCol.html($('.jsites-actions').html().replace(/{id}/g, siteid));

                            for(var k in classnames) {
                                $('td:eq(' + k + ')', nRow).addClass(classnames[k]);
                            }

                        }
    });

	 $('#topOrder').change(function(){
		 var top = $('#top-touchpoints');
		 top.DataTable().fnSettings().aaSorting[0] = $('#topOrder').val();
		 top.dataTable().fnDraw();
	 });
*/ ?>

});

 function processResult(ajax){
	if(ajax.error)	alert("error");
	var d = ajax.pw;
	for(var n in d)  // each row
        {
            for(var c in d[n]){
                var elm =   
		$('#jsite_' + d[n].id + ' .' + c);
                elm.html(d[n][c]);
                }
        }
 }


var kwoffset	=	[];
function updateKeywords(ajax){

    var kw  =   new KeywordsResult(ajax);    
    return;
}

/** 
    Dashboard functions for Controlling the date range buttons
    Main code is in functions.js
    can search for checkDateRangeSelect()
**/
var refreshDash = function(){
    Keywords.refresh();
    Social.refresh();
	Rankings.refresh();

<?php /* Rankings.refresh(); */ ?>

}

	
	//===== Export Campaign Form =====//
        /*
	var exFrom = $( "#export_from" ),
		exTo = $( "#export_to" ),
		exMessage = $( "#export_msg" ),
		allFields = $( [] ).add( name ),
		tips = $( ".validateTips" );
	*/
       
	//===== Export Dialog =====//
	$( "#mwExport" ).dialog({
		autoOpen: false,
		height: 500,
		width: 550,
		modal: true,
		buttons: {
			"Export Campaign Data": function() {
				var bValid = true;
				allFields.removeClass( "ui-state-error" );
				
				/***********
				// VALIDATION CHECKS - NOT SURE ON THE RULES 
				
				bValid = bValid && checkLength( name, "Campaign Name", 3, 30 );
				
				bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Campaign Name may consist of a-z, 0-9, underscores, and begin with a letter." );
				***********/
					if ( bValid )
				{
					var campForm = $('#createCampForm');
					
					params = campForm.serialize();
					
					$.post(campForm.attr("action"), params, function(data)
					{
						json = $.parseJSON(data);
			
						//SUCCESS
						if(json.success)
						{
							$('#campTable').text(json.success.message);
							//$(json.success.container).find(".item:first").slideDown();
							
						}
						//ERROR
						else if(json.error.message)
						{
							if(json.error.type = 'displayMessage')
							{
								$(json.error.container).html(json.error.message);
								$(json.error.container).show();
							}	
						}
						else
						{
							alert('There was an error creating your item.\nPlease reload the page and try again.  Contact support if the problem persists.');
						}
					});
											
					$( this ).dialog( "close" );
					}
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});