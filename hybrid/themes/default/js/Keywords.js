function KwRequest(offset){
    this.params.offset  =   offset;// = {offset: offset};
}

KwRequest.prototype.process = function (ajax){
//    var ajax = {pw: {next: false, total: 102, rows: "<tr><td class=jkeyword colspan=2>row1</td></tr><tr><td class=jkeyword colspan=2>row1</td></tr><tr><td class=jkeyword colspan=2>row1</td></tr><tr><td class=jkeyword colspan=2>row1</td></tr>"}};
    // end
    
//    console.log('processing', ajax);
    this.response = ajax;
    var d = ajax.pw;
    $("#jkeywords .jkeyword").remove();
    $("#jkeywords").append(ajax.pw.rows);
    $("#jkeywords tr.jkeyword:last").hide();
//	Keywords.status("Processing " + d.next + " of " + d.total + " touchpoints..");
    if(this.next())
        this.send();
//    var Result =    new KwResult(dummy);
}

KwRequest.prototype.finish = function (){
    var msg = "No Keywords Found";
    var msgel=$('#jkeywords .jstatus');
    if(this.response.pw.count > 0){
            // hide msg
        msgel.hide();
        $("#jkeywords tr.jkeyword:last").show();
        return;
    }
//    var msg = "Processing..." + this.response.pw.length + " Websites";
	Keywords.status(msg);
}

KwRequest.prototype.next = function (){
    var d = this.response.pw;
    if(d.next === false){
        this.finish();
        return false;
    }
    
    this.params.offset = d.next;
//    this.process();
    return true;
};


function refreshKeywords(e){
	console.log('@depr use Keywords.refresh');
	Keywords.refresh();
}

function Keywords(){}

Keywords.loaded	=	function (){
	return $("#jkeywords .jkeyword").length;
}

Keywords.status	=	function (s){
	$("#jkeywords .jstatus td").html(s);
}

Keywords.refresh	=	function (){
    $('#jkeywords tr.jkeyword').remove();

        $("#jkeywords .jstatus").show();
//	Keywords.status("Processing...");
        var Request = new KwRequest(0);
            Request.send();
}

Keywords.preload	=	function (){
	if(!Keywords.loaded())
		Keywords.refresh();
	else
		$("#jkeywords .jstatus").hide();
}
