/*
 * Element must have a name attribute with a value in this format: "something[key]" to
 *  work with validation, which uses the key as a id in the ajax response
 */

var AjaxSaveDataRequest = function (element, value, callback){
    
    // Sigh!!! Yup!
    $("#SL_CategoryNameReadable").val($("#post-category").find(':selected').attr('category-name-readable'));
    
    if( element.jquery ){
        var el = element;
    }
    else {
        var el = $(element);
    }

    var t = this;
    this.el = el;

    var fieldname = element.name.match(/.*\[([^\]]+)\]/);
    this.fieldkey = fieldname.pop();

    this.form = element.form;
    this.action_url = el.data('actionUrl') !== undefined ? el.data('actionUrl') : element.form.action;

    this.getValue = function(el, val)
    {
        if (!el.is(':checkbox'))
            return typeof val != "undefined" ? val : el.val();
        else
            return el.is(':checked') ? el.val() : 0;
    };

    this.success = function (resp)
    {
        if (resp.error == 'validation')
        {
            for (var errorCode in resp.validation){
                notie.alert({text:'Error: ' + resp.validation[errorCode].join('\n')});
            }

            t.setIcon('alert');
            t.errors(resp);
        }
        else
        {

            if (resp.params){
                if (resp.params[t.fieldkey]) {
                    $('.jHtml_' + t.fieldkey).text(resp.params[t.fieldkey]);
                }
            }

            if (resp.uploads && resp.uploads.length > 0){
                t.setImage(resp);
            }

            t.setIcon('check');
            el.siblings('label').fadeOut();

            if($.isFunction(callback)){
                callback(resp);
            }
        }
    };

    this.errors = function (resp)
    {
        try {
            setLabel( resp.validation[this.fieldkey].join("<br />") );
        }
        catch(err) {
            notie.alert({text:'NOTICE! Please contact your system administrator if this problem persists.',stay:true});
            notie.alert({text:'Developer Error Code: ' + err.message,stay:true});
        }
    };

    this.error = function (resp) {
        notie.alert({text:'An error occurred.'});
    };

    if (element.name == '') {
        console.log("Element has no name", element, el.val());
//            el.blur();
        return false;
    }

    var data = {};
    data[element.name] = this.getValue(el, value);

    $('input[type=hidden]:not(.not-global), :input.jajax-required', this.form).not(el).each(function () {
        data[this.name] = t.getValue($(this));
    });

    this.setImage = function (resp) {
        var img = new Image();
        img.src = resp.uploads[this.fieldkey];
        var imgs = '<img src="' + img.src + '" class="preview" />';
        var p = el.parent().parent().parent();
        p.find('img').remove();
        p.append(imgs);
    };

    this.setIcon = function (name, src) {
        var sibling = el.hasClass('select') ? el.parent() : el;
        var img = sibling.siblings('.fieldIcon');
        var src = 'themes/default/images/' + (src ? src : 'icons/usual/icon-' + name + '.png');
        if (!img.length)
            sibling.after('<img src="' + src + '" class="fieldIcon"/>');
        else
            img.attr('src', src);
    }

    function setLabel(textarr) {
        var lbl = el.siblings('label');
        if (!lbl.length)
            el.parent().append('<label for="' + element.id + '" class="error">' + textarr + '</label>');
        else
            lbl.text(textarr).fadeIn();
    }

    function upload() {
//        console.log(element.form);
        var iframeid = 'inline_upload_' + element.id;
        var iframe = $('#' + iframeid);
        if (!iframe.length) {
            el.after('<iframe name="' + iframeid + '" id="' + iframeid + '" src="about:blank"></iframe>');
            iframe = $('#' + iframeid);
        }
        element.form.target = iframeid;
        element.form.enctype = "multipart/form-data";

        iframe.load(function () {
            var resp = $.parseJSON(iframe.contents().find('body').text());
            t.success(resp);
            iframe.remove();
            $(element).replaceWith($(element).clone(true));
        });
//        console.log(el.value);
//        $(element.form).submit(function (){        });
        element.form.submit();
    }

    if (!$(element).hasClass('jnoIcon'))
        this.setIcon(null, 'elements/loaders/10s.gif');

    if (el.hasClass('fileInput')) {
        upload();
    } else
        $.ajax(this.action_url,
            {
                dataType: 'json',
                type: "POST",
                data: data,
                context: t,
                success: t.success,
                error: t.error
            });


};

AjaxSaveDataRequest.evt =   function (e){
	e.preventDefault();
	$(this).data('ajax', new AjaxSaveDataRequest(this));
};

//Bind and event function as a first handler $(element).bindUp('type', function(){})
$.fn.bindUp = function(type, fn) {

    type = type.split(/\s+/);

    this.each(function() {
        var len = type.length;
        while( len-- ) {
            $(this).bind(type[len], fn);

            var evt = $._data(this, 'events')[type[len]];
            evt.splice(0, 0, evt.pop());
        }
    });
};

function CheckAjaxResponse(success){

    return function (J){
        if(J.error)
            {
                console.log('An error occurred: ',J);
                notie.alert({text:'An error occurred: ' + J.message});
            }else
                success(J);
    }
}

var SearchMediaProvider =   function ($, widget) {

    this.value = null;

    var search = this;
    var previews = $('.previews', widget);
    var actionurl = $('#photo-media-action').val();
    var input = $('.input .jtext', widget);
    var timeout = null;

    function loadUploadedImage(resp) {
        if (resp.error)  alert("Error ocurred.");

        var key = this.fieldkey;
        var slide = resp.slide;
        var bgurl = resp.uploads.background;
        var title = $('#slide_title_' + slide.ID).val();

//        console.log(resp, slide);
//        var filename    =   url.split(/\//).pop();
//        console.log(arguments, resp, url, filename);
        var slidethumb = $('#slidethumb_' + slide.ID + ' a');

        if (bgurl != '') {
            if (slidethumb.parent().hasClass('jnothumb')) {
//                $('.thumbsmall', slidethumb).remove();  // remove text thumb
                slidethumb.prepend('<img src="' + bgurl + '" class="thumbsmall" />').parent().removeClass('jnothumb');
            } else
                $('img', slidethumb).attr('src', bgurl);

            $('.results .jhasuploader', widget).after('<li><img src="' + bgurl + '" class="thumbsmall" /></li>');
            $('.results .li:eq(2)', widget).each(bindSelectImage);

        } else {
            $('.thumbsmall', slidethumb).remove();  // remove text thumb
            $('.thumbsmalldesc', slidethumb).text(title);
            slidethumb.parent().addClass('jnothumb');
        }
        input.val(slide.background);
    }

    function showuploadErrors(resp) {
        var key = this.fieldkey;
        var errors = resp.validation[key];
        alert(errors.join("<br>"));
    }

    $('.fileInput', previews).change(function () {
        savedatareq = $(this).data('ajax');
        savedatareq.setImage = loadUploadedImage;
        savedatareq.setIcon = function () {
        };
        savedatareq.errors = showuploadErrors;
//        console.log(savedatareq);
    });

    function inputChange() {
        if (!this.value.match(/^https?:\/\//i) && $.trim(this.value) != '') return;
        var savedreq = $(this).data('ajax');
        savedreq.setImage = loadUploadedImage;
    }

    function selectImage() {
        hideWidget();
        input.val($(this).attr('alt')).change();
        inputChange.apply(input.get(0));
        input.data('ajax').setImage = loadUploadedImage;
        var slideid = input.parents('form').attr('id').split(/_/).pop();
//        console.log(slideid);
        $('#slidethumb_' + slideid + ' img').attr('src', $(this).attr('src'));
    }

    function unselectImage() {
        $(this).remove();
        previews.hide();
        $(widget).fadeIn();
        searchWidget();
    }

    function bindSelectImage() {
        $(this).click(selectImage);
    }

    function showResults(resp) {
        $('.results li:not(.jhasuploader)', widget).remove();
        for (var i in resp.items) {
            var url = resp.thumbs.replace(/--FILE--/, resp.items[i]);
            $('.results', previews).append('<li><img class="jnew thumbsmall" alt="' + resp.items[i] + '" src="' + url + '" /></a></li>');
        }
        $('.results li:not(.jhasuploader) img.jnew', widget).each(bindSelectImage).removeClass('jnew');
    }


    function searchWidget(e) {
        var offsetctr = $('.xtab_container').offset();
        var woffset = $(widget).offset();
        var liw = 96; //$('.results li:first-child', widget).outerWidth();
        var totalw = $('.results li', widget).length * liw;
        var maxw = $('.xtab_container').width() + offsetctr.left - woffset.left;
        var adjw = Math.floor(maxw / liw) * liw;
//        if(totalw > maxw)
        $('.previews', widget).css('width', adjw + 'px');

        previews.show();
        if (e != undefined && e.type == 'focus') {
            console.log('blidnclick');
//                    previews.fadeIn('fast', function (){                    });
            return;
        }
//        previews.fadeIn('fast');


        var post = {'q': search.value};
        $.ajax(actionurl, {
            dataType: 'json',
            type: 'POST',
            'data': post,
            success: showResults
        })
    }

    function hideWidget(e) {
        if (e && $(e.target).hasClass('jtext')) return;   // dont process inputt.click evt

//        console.log(e, 'hidew');
//        $('html').unbind('click', hideWidget);
        previews.hide();//fadeOut('fast');
    }

    function ffshowResults() {
        previews.fadeIn();
//        $('.fileInput', previews).attr('name', )
    }

    function typeevt(e) {
        var val = $(this).val();
        var len = val.length;
        var is_url = val.match(/^http:\/\//);

        clearTimeout(timeout);

        search.value = val;


        if (e.type == 'focus') {
            searchWidget(e);
            return;
        }

        if (is_url)
            hideWidget();
        else
            timeout = setTimeout(searchWidget, 500);
//        console.log(is_url, len, val);
    }

    function showUpload() {
        input.hide();
        $('.input .uploader').fadeIn().click();
    }

    $('.results .jthumb', widget).each(bindSelectImage);

    input.keyup(typeevt).focus(typeevt)/*blur(hideWidget)*/.change(inputChange);//.unbind('change', AjaxSaveDataRequest.evt);
    $('.ctrls .juploadbtn', widget).click(showUpload);

    $(this).click(function (e) {
        e.stopPropagation();
    });
};

var highlightDateRangeButton = function( element ) {
    $('#DateRangeSelectors button.current').removeClass('current');
    element.addClass('current');
}

var checkDateRangeSelect = function() {
    var selected = $('.graph_rangectrl option[selected="selected"]').val();
    var element = $('#DateRangeSelectors button[data-range="'+ selected +'"]');
    highlightDateRangeButton( element );
}

$(function(){
    $('a._blank').attr('target','_blank');

	// Scroll event that toggles scroll to top button
	$(window).scroll(function(){
		if($(this).scrollTop() >= 180){
			$('#scrollTop').fadeIn(300);
		} else{
			$('#scrollTop').fadeOut(300);
		}
	});

	// Click event to scroll to top
	$('#scrollTop').click(function(){
		$('html,body').animate({scrollTop:0},300);
		return false;
	});

    $('.disabled').on('click', function(e){
        e.stopImmediatePropagation();
        return false;
    });

    if (typeof(refreshDash) != "function")
        refreshDash = function(){};

    checkDateRangeSelect();

    //old select value trigger
    $('.graph_rangectrl').change(refreshDash);

    //Date Range Buttons
    $('#DateRangeSelectors button').click(function() {
        var selectedValue = $(this).attr('data-range');
        //exit function if already selected.
        if($(this).hasClass('current,disabled'))
            return;
        //set select data and remove old selection
        $(".graph_rangectrl option").each(function() {
            if ( $(this).val() == selectedValue )
                $(this).attr('selected', true).val(selectedValue);
            else
                $(this).removeAttr('selected');
        });
        //update uniform style
        $.uniform.update('.graph_rangectrl');
        //call refresh
        $('.graph_rangectrl').trigger('change');
    });

    // website photos
    $('form.ajax-save input:password, form.ajax-save input[type=text], form.ajax-save input.fileInput, form.ajax-save select, form.ajax-save input:checkbox, form.ajax-save input.ajax').change(AjaxSaveDataRequest.evt);
    $('form.ajax-save input[type=radio]').bind('change', AjaxSaveDataRequest.evt);
    $('form.ajax-save textarea').blur(AjaxSaveDataRequest.evt);

    //===== File manager =====//

    $().ready(function () {
        var elf = $('#fileManager').elfinder({
            // lang: 'ru',             // language (OPTIONAL)
            url: 'php/connector.php'  // connector URL (REQUIRED)
        }).elfinder('instance');
    });


    //===== ShowCode plugin for <pre> tag =====//

    $('.code').sourcerer('js html css php'); // Display all languages


    //===== Left navigation styling =====//

    //$('.subNav li a.this').parent('li').addClass('activeli');


    //===== Login pic hover animation =====//
/*
    $('.loginEmailDiv').hover(
        function () {
            $('.logright').animate({right: 0, opacity: 1}, 200);
        },

        function () {
            $('.logright').animate({right: -10, opacity: 0}, 200);

        }
    );

     $(".loginPic").hover(
     function() {

     $('.logleft, .logback').animate({left:10, opacity:1},200);
     $('.logright').animate({right:10, opacity:1},200); },

     function() {
     $('.logleft, .logback').animate({left:0, opacity:0},200);
     $('.logright').animate({right:0, opacity:0},200); }
     );
*/

    //===== Image gallery control buttons =====//

    $(".gallery ul li").hover(
        function () {
            $(this).children(".actions").show("fade", 200);
        },
        function () {
            $(this).children(".actions").hide("fade", 200);
        }
    );


    //===== Autocomplete =====//

    var availableTags = ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"];
    $(".ac").autocomplete({
        source: availableTags
    });


    //===== jQuery file tree =====//

    $('.filetree').fileTree({
        root: '../images/',
        script: 'php/jqueryFileTree.php',
        expandSpeed: 200,
        collapseSpeed: 200,
        multiFolder: true
    }, function (file) {
        alert(file);
    });


    //===== Sortable columns =====//

    $("table").tablesorter();


    //===== Resizable columns =====//

    $("#resize, #resize2").colResizable({
        liveDrag: true,
        draggingClass: "dragging"
    });


    //===== Bootstrap functions =====//

    // Loading button
    $('#loading').click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {
            btn.button('reset')
        }, 3000)
    });

    // Dropdown
    $('.dropdown-toggle').dropdown();


    //===== Animated progress bars (ID dependency) =====//

    var percent = $('#bar1').attr('title');
    $('#bar1').animate({width: percent}, 1000);

    var percent = $('#bar2').attr('title');
    $('#bar2').animate({width: percent}, 1000);

    var percent = $('#bar3').attr('title');
    $('#bar3').animate({width: percent}, 1000);

    var percent = $('#bar4').attr('title');
    $('#bar4').animate({width: percent}, 1000);

    var percent = $('#bar5').attr('title');
    $('#bar5').animate({width: percent}, 1000);

    var percent = $('#bar6').attr('title');
    $('#bar6').animate({width: percent}, 1000);

    var percent = $('#bar7').attr('title');
    $('#bar7').animate({width: percent}, 1000);

    var percent = $('#bar8').attr('title');
    $('#bar8').animate({width: percent}, 1000);

    var percent = $('#bar9').attr('title');
    $('#bar9').animate({width: percent}, 1000);

    var percent = $('#bar10').attr('title');
    $('#bar10').animate({width: percent}, 1000);


    //===== Fancybox =====//

    $(".lightbox").fancybox({
        'padding': 2
    });

    $('.iframe-btn').fancybox({
        'width': 900,
        'height': 600,
        'type': 'iframe',
        'autoScale': false
    });

    //===== Color picker =====//
    // website-theme.php

    $('input.colorpicker').ColorPicker({
        onSubmit: function (hsb, hex, rgb, el) {
            $(el).val('#' + hex);

            if ($(el.form).hasClass('ajax-save'))
                new AjaxSaveDataRequest(el);

            $(el).ColorPickerHide();
        },
        onShow: function (colpkr) {
            $(colpkr).fadeIn(500);
            $(this).ColorPickerSetColor($(this).val());
            return false;
        },
        onHide: function (colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function (hsb, hex, rgb) {

            console.log(this, arguments);
        }
    }).bind('keyup', function () {
        $(this).ColorPickerSetColor(this.value);
    });


    //===== Time picker =====//

    $('.timepicker').timeEntry({
        show24Hours: true, // 24 hours format
        showSeconds: true, // Show seconds?
        spinnerImage: 'images/elements/ui/spinner.png', // Arrows image
        spinnerSize: [19, 26, 0], // Image size
        spinnerIncDecOnly: true // Only up and down arrows
    });


    //===== Usual validation engine=====//

    $("#usualValidate").validate({
        rules: {
            firstname: "required",
            minChars: {
                required: true,
                minlength: 3
            },
            maxChars: {
                required: true,
                maxlength: 6
            },
            mini: {
                required: true,
                min: 3
            },
            maxi: {
                required: true,
                max: 6
            },
            range: {
                required: true,
                range: [6, 16]
            },
            emailField: {
                required: true,
                email: true
            },
            urlField: {
                required: true,
                url: true
            },
            dateField: {
                required: true,
                date: true
            },
            digitsOnly: {
                required: true,
                digits: true
            },
            enterPass: {
                required: true,
                minlength: 5
            },
            repeatPass: {
                required: true,
                minlength: 5,
                equalTo: "#enterPass"
            },
            customMessage: "required",
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            customMessage: {
                required: "Bazinga! This message is editable",
            },
            agree: "Please accept our policy"
        }
    });


    //===== Validation engine =====//

    $("#validate").validationEngine();


    //===== iButtons =====//

    $('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
        labelOn     : '',
        labelOff    : '',
        change      : function(){
                    if($(this.form).hasClass('ajax-save')){
                        $(this).data('ajax',new AjaxSaveDataRequest(this));
                    }
        }
    });
	
	$('input[type="checkbox"].plivo').iButton({
        labelOn     : '',
        labelOff    : '',
        change      : function(){
			
			
					if($('input[type="checkbox"].plivo').is(':checked'))
						{
						  login();
						}else
						{
						 logout();
						}

			
        }
    });

    //===== Notification boxes =====//

    $(".nNote").click(function () {
        $(this).fadeTo(200, 0.00, function () { //fade
            $(this).slideUp(200, function () { //slide up
                $(this).remove(); //then remove from the DOM
            });
        });
    });


    //===== Animate current li when closing button clicked =====//

    $(".remove").click(function () {
        $(this).parent('li').fadeTo(200, 0.00, function () { //fade
            $(this).slideUp(200, function () { //slide up
                $(this).remove(); //then remove from the DOM
            });
        });
    });


    //===== Check all checbboxes =====//

    $(".titleIcon input:checkbox").click(function () {
        var checkedStatus = this.checked;
        $("#checkAll tbody tr td:first-child input:checkbox").each(function () {
            this.checked = checkedStatus;
            if (checkedStatus == this.checked) {
                $(this).closest('.checker > span').removeClass('checked');
                $(this).closest('table tbody tr').removeClass('thisRow');
            }
            if (this.checked) {
                $(this).closest('.checker > span').addClass('checked');
                $(this).closest('table tbody tr').addClass('thisRow');
            }
        });
    });

    $(function () {
        $('#checkAll tbody tr td:first-child input[type=checkbox]').change(function () {
            $(this).closest('tr').toggleClass("thisRow", this.checked);
        });
    });


    //===== File uploader =====//

    $("#uploader").pluploadQueue({
        runtimes: 'html5,html4',
        url: 'php/upload.php',
        max_file_size: '100kb',
        unique_names: true,
        filters: [
            {title: "Image files", extensions: "jpg,gif,png"}
        ]
    });


    //===== Wizards =====//

    $("#wizard1").formwizard({
        formPluginEnabled: true,
        validationEnabled: false,
        focusFirstInput: false,
        disableUIStyles: true,

        formOptions: {
            success: function (data) {
                $("#status1").fadeTo(500, 1, function () {
                    $(this).html("<span>Form was submitted!</span>").fadeTo(5000, 0);
                })
            },
            beforeSubmit: function (data) {
                $("#w1").html("<span>Form was submitted with ajax. Data sent to the server: " + $.param(data) + "</span>");
            },
            resetForm: true
        }
    });

    $("#wizard2").formwizard({
        formPluginEnabled: true,
        validationEnabled: true,
        focusFirstInput: false,
        disableUIStyles: true,

        formOptions: {
            success: function (data) {
                $("#status2").fadeTo(500, 1, function () {
                    $(this).html("<span>Form was submitted!</span>").fadeTo(5000, 0);
                })
            },
            beforeSubmit: function (data) {
                $("#w2").html("<span>Form was submitted with ajax. Data sent to the server: " + $.param(data) + "</span>");
            },
            dataType: 'json',
            resetForm: true
        },
        validationOptions: {
            rules: {
                bazinga: "required",
                email: {required: true, email: true}
            },
            messages: {
                bazinga: "Bazinga. This note is editable",
                email: {required: "Please specify your email", email: "Correct format is name@domain.com"}
            }
        }
    });

    $("#wizard3").formwizard({
        formPluginEnabled: false,
        validationEnabled: false,
        focusFirstInput: false,
        disableUIStyles: true
    });


    //===== WYSIWYG editor =====//

    $("#editor").cleditor({
        width: "100%",
        height: "250px",
        bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
        useCSS: true
    });


    //===== Dual select boxes =====//

    $.configureBoxes();


    //===== Chosen plugin =====//

    $(".select").not(".graph_rangectrl, .select2").chosen();

    //===== Select2 plugin =====//
    $('.select2').select2({
        allowClear: true
    });

    //===== Autotabs. Inline data rows =====//

    $('.onlyNums input').autotab_magic().autotab_filter('numeric');
    $('.onlyText input').autotab_magic().autotab_filter('text');
    $('.onlyAlpha input').autotab_magic().autotab_filter('alpha');
    $('.onlyRegex input').autotab_magic().autotab_filter({format: 'custom', pattern: '[^0-9\.]'});
    $('.allUpper input').autotab_magic().autotab_filter({format: 'alphanumeric', uppercase: true});


    //===== Masked input =====//

    $.mask.definitions['~'] = "[+-]";
    $(".maskDate").mask("99/99/9999", {
        completed: function () {
            alert("Callback when completed");
        }
    });
    $(".maskPhone").mask("9-999-999-9999");
    $(".maskPhoneExt").mask("(999) 999-9999? x99999");
    $(".maskIntPhone").mask("+33 999 999 999");
    $(".maskTin").mask("99-9999999");
    $(".maskSsn").mask("999-99-9999");
    $(".maskProd").mask("a*-999-a999", {placeholder: " "});
    $(".maskEye").mask("~9.99 ~9.99 999");
    $(".maskPo").mask("PO: aaa-999-***");
    $(".maskPct").mask("99%");


    //===== Tags =====//

    var tagsloaded = false;
    var keywordsWebsite = $('.tags');
    $.each(keywordsWebsite, function () {
        $(this).tagsInput({
            width: '100%',
            onChange: function (a, b, c) {
                if (!tagsloaded) return;


                console.log(this);
//                    console.log(typeof this, this.toString(), this.value, 'args', a.val(),  a, b, c);

                if ($(this.form).hasClass('ajax-save'))
                    new AjaxSaveDataRequest(this);

            }
        });
        if($(this).is(':disabled')){
            $('#' + this.id + '_addTag').hide();
            $(this).siblings().find('a').hide();
        }
    });

    // note. tagsinput requires the element to have an id attribute otherwise it crashes.

    $.each($('.keywords-website'), function(){
		var keywordsWebsite = $(this);
        keywordsWebsite.tagsInput({

            onChange: function (g, et) {
                if (!tagsloaded) return;
                //var g = $(this).val().split(/,/);
                var id = this.id;

                new AjaxSaveDataRequest(this);

                var addt = $('#' + id + '_addTag');


                if (g.length > 5) {
                    addt.hide();
//                $(this).importTags(g.join(','));
                }
                else
                    addt.show();
            }
        });

        if(keywordsWebsite.is(':disabled')){
            $('#' + this.id + '_addTag').hide();
            keywordsWebsite.siblings().find('a').hide();
        }
    });

    tagsloaded = true;
    //===== Input limiter =====//

    $('.lim').inputlimiter({
        limit: 140,
        boxId: 'limitingtext',
        boxAttach: false
    });


    //===== Placeholder =====//

    $('input[placeholder], textarea[placeholder]').placeholder();


    //===== Autogrowing textarea =====//

    $('.auto').elastic();
    $('.auto').trigger('update');


    //===== Full width sidebar dropdown =====//
	/*
    $('.fulldd li').click(function () {
        $(this).children("ul").slideToggle(150);
    });
    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("has"))
            $('.fulldd li').children("ul").slideUp(150);
    });
	*/

    //===== Top panel search field =====//
	/*
    $('.userNav a.search').toggle(function () {
        $('#topSearch').fadeIn(150);
        $('input.user-search').focus();
    }, function () {
        $('#topSearch').fadeOut();
    });
	*/


    //===== 2 responsive buttons (320px - 480px) =====//
    /*
    $('.iTop').click(function () {
        $('#sidebar').slideToggle(100);
    });

    $('.iButton').click(function () {
        $('.altMenu.buttonMenu').slideToggle(100);
    });

     $('.iCamp').click(function () {
        $('.altMenu.campMenu').slideToggle(100);
    });
    */


    //===== Animated dropdown for the right links group on breadcrumbs line =====//
    /*
    @corfro
    Not sure if using this functionality
        $('.breadLinks ul li').click(function () {
            $(this).children("ul").slideToggle(150);
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("has"))
                $('.breadLinks ul li').children("ul").slideUp(150);
        });
    */

    //===== Tabs =====//

    $.fn.contentTabs = function(){
        $('ul.tabs li',this).click(function(){
            $(this).parent().parent().find('ul.tabs li').removeClass('activeTab');
            $(this).addClass('activeTab');
            $(this).parent().parent().find('.tab_content').hide();
            var activeTab = $(this).find('a').attr('href');
            $(activeTab).show();
            if($('.themechange').length){
                $('.themechange').triggerHandler('click');
            }
            return false;
        });
        $('ul.titleToolbar li',this).click(function(){
            $(this).parent().parent().find('ul.titleToolbar li').removeClass('activeTab');
            $(this).addClass('activeTab');
            return false;
        });
    };
    $('div[class^="widget"],div[class^="widget"] .formRow').contentTabs();

    //===== Dynamic data table =====//
	/*
     oTable = $('.dTable').dataTable({
     "bJQueryUI": false,
     "bAutoWidth": false,
     "sPaginationType": "full_numbers",
     "sDom": '<"H"fl>rt<"F"ip>'
     });
	 */

    //===== Add classes for sub sidebar detection =====//

    if ($('div').hasClass('secNav')) {
        //$('#sidebar').addClass('with');
        //$('#content').addClass('withSide');
    }
    else {
        // @amado $('#sidebar').addClass('without');
        // @amado $('#content').css('margin-left','100px');//.addClass('withoutSide');
        $('#footer > .wrapper').addClass('fullOne');
    }
    ;


    //===== Collapsible elements management =====//

    $('.exp,.exp2,.exp3').collapsible({
        defaultOpen : 'actv',
        cssOpen     : 'opnd',
        cssClose    : 'clsd',
        speed       : 225
    });
	// @fernando on load open active collapsible menu if it exists
	if($('.subNav li.activeli > .exp').length){
		$('.subNav li.activeli > .exp,.subNav li.activeli > .exp2').collapsible('open');
	}
	// @fernando only one parent menu open at a time for less clutter
    $('.subNav .exp').on('click',function(){
        var _this   = $(this);
        var _desc   = _this.find('.exp2');
        var _obj    = _this.add(_desc);
        $('.subNav .exp,.subNav .exp2').not(_obj).collapsible('close');
    });

    //===== Accordion =====//

    $('div.menu_body:eq(0)').show();
    $('.acc .whead:eq(0)').show().css({color: "#2B6893"});

    $(".acc .whead").click(function () {
        $(this).css({color: "#2B6893"}).next("div.menu_body").slideToggle(200).siblings("div.menu_body").slideUp("slow");
        $(this).siblings().css({color: "#404040"});
    });

/*
    //==== Spark lines ====//
    $('.sparkline').sparkline('html', {
        type: 'line',
        width: '100',
        height: '30'
    });
*/

    //===== User nav dropdown =====//
	/*
    $('a.leftUserDrop').click(function () {
        $('.leftUser').slideToggle(200);
    });
    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("leftUserDrop"))
            $(".leftUser").slideUp(200);
    });
	*/

    //===== Calendar =====//

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        events: [
            {
                title: 'All Day Event',
                start: new Date(y, m, 1)
            },
            {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2)
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/'
            }
        ]
    });


    $('.spinner').spinner();

    //===== jQuery UI dialog =====//

    $('#dialog').dialog({
        autoOpen: false,
        width: 400,
        buttons: {
            "Gotcha": function () {
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    // Dialog Link
    $('#dialog_open').click(function () {
        $('#dialog').dialog('open');
        return false;
    });

    // Dialog
    $('#formDialog').dialog({
        autoOpen: false,
        width: 400,
        buttons: {
            "Nice stuff": function () {
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    // Dialog Link
    $('#formDialog_open').click(function () {
        $('#formDialog').dialog('open');
        return false;
    });

    // Dialog
    $('#customDialog').dialog({
        autoOpen: false,
        width: 650,
        buttons: {
            "Very cool!": function () {
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    // Dialog Link
    $('#customDialog_open').click(function () {
        $('#customDialog').dialog('open');
        return false;
    });


    //===== Vertical sliders =====//

    $("#eq > span").each(function () {
        // read initial values from markup and remove that
        var value = parseInt($(this).text(), 10);
        $(this).empty().slider({
            value: value,
            range: "min",
            animate: true,
            orientation: "vertical"
        });
    });


    //===== Modal =====//

    $('#dialog-modal').dialog({
        autoOpen: false,
        width: 400,
        modal: true,
        buttons: {
            "Yep!": function () {
                $(this).dialog("close");
            }
        }
    });

    $('#modal_open').click(function () {
        $('#dialog-modal').dialog('open');
        return false;
    });


    //===== jQuery UI stuff =====//

    // default mode
    $('#progress1').anim_progressbar();

    // from second #5 till 15
    var iNow = new Date().setTime(new Date().getTime() + 5 * 1000); // now plus 5 secs
    var iEnd = new Date().setTime(new Date().getTime() + 15 * 1000); // now plus 15 secs
    $('#progress2').anim_progressbar({start: iNow, finish: iEnd, interval: 1});

    // Progressbar
    $("#progress").progressbar({
        value: 80
    });

    // Modal Link
    $('#modal_link').click(function () {
        $('#dialog-message').dialog('open');
        return false;
    });

    // Datepicker
    $('.inlinedate').datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        onSelect: function(d,i){
            if(d !== i.lastVal){
                $(this).change();
            }
        }
    });

    $(".datepicker").datepicker({
        defaultDate: +7,
        showOtherMonths: true,
        autoSize: true,
        appendText: '(dd-mm-yyyy)',
        dateFormat: 'dd-mm-yy',
        onSelect: function(d,i){
            if(d !== i.lastVal){
                $(this).change();
            }
        }
    });

    $('#expDate').datepicker({
        showOtherMonths: true,
        autoSize: true,
        dateFormat: 'mm/y',
        onSelect: function(d,i){
            if(d !== i.lastVal){
                $(this).change();
            }
        }
    });

    $(function () {
        var dates = $("#fromDate, #toDate").datepicker({
            defaultDate: "+1w",
            autoSize: true,
            showOtherMonths: true,
            onSelect: function (selectedDate, i) {
                var option = this.id == "fromDate" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(
                        instance.settings.dateFormat ||
                        $.datepicker._defaults.dateFormat,
                        selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
                if(selectedDate !== i.lastVal){
                    $(this).change();
                }
            }
        });
    });


    $(".uSlider").slider();
    /* Usual slider */


    $(".uRange").slider({
        /* Range slider */
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#rangeAmount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#rangeAmount").val("$" + $(".uRange").slider("values", 0) + " - $" + $(".uRange").slider("values", 1));


    $(".uMin").slider({
        /* Slider with minimum */
        range: "min",
        value: 37,
        min: 1,
        max: 700,
        slide: function (event, ui) {
            $("#minRangeAmount").val("$" + ui.value);
        }
    });
    $("#minRangeAmount").val("$" + $(".uMin").slider("value"));


    $(".uMax").slider({
        /* Slider with maximum */
        range: "max",
        min: 1,
        max: 100,
        value: 20,
        slide: function (event, ui) {
            $("#maxRangeAmount").val(ui.value);
        }
    });
    $("#maxRangeAmount").val($(".uMax").slider("value"));

    //===== Button for showing up sidebar on iPad portrait mode. Appears on right top =====//
	/*
    $("ul.userNav li a.sidebar").click(function () {
        $(".secNav").toggleClass('display');
    });
	*/

    //===== Form elements styling =====//

    //$("select, .check, .check :checkbox, input:radio, input:file").uniform();

    //===== Notie JS customize defaults =====//
    notie.setOptions({
        alertTime: 4,
        positions: {
            alert: 'bottom'
        }
    });
    //==== Trigger Ajax for links on Datatables with jajax-action class ====//
    $('.dTable').on('click','.jajax-action:not(.disabled)',function(e){/* .tDefault */
        e.preventDefault();
        var _t = $(e.currentTarget);
        var _h = _t.attr('href');
		var _a1 = /action=delete/;
        var _a2 = /action=mu-blog-post-trash/;
        var _a3 = /action=ajaxDeleteRole/;
        var _a4 = /action=CancelSubscription/;
        var _a5 = /action=trashpost/;
		if(_a1.test(_h) || _a2.test(_h) || _a3.test(_h) || _a4.test(_h) || _a5.test(_h)){
			$('<div id="jdialog-deleteitem" class="SQmodalWindowContainer dialog"></div>').dialog({
                title:      'Delete Confirmation',
                autoOpen:   true,
				draggable:  false,
				modal:      true,
				resizable:  false,
				height:     150,
                width:      300,
                open: function(){
                    var _html = '<p style="text-align: center;"><span class="red">Are you sure you want to delete this?</span></p>';
                    $(this).html(_html);
                },
				close: function(event,ui){
					$(this).dialog('destroy').remove();
				},
				buttons: {
					Cancel: function(){
						$(this).dialog('close');
					},
					'Delete': function(){
						$(this).dialog('close');
						sendAjax(_h,function(J){
                            notie.alert({text:J.message});
							_t.closest('.dTable').dataTable().fnDraw(false);
							for(var touchpoint in J.touchpoints){
								if(touchpoint != '')
									$('a[href*=' + touchpoint + '] .dataNumGrey').text(J.touchpoints[touchpoint]);
							}
							if($.isFunction(J.callback)){
								eval(J.callback)(J);
							}
						});
						return true;
					}
				}
			});
		} else{
			sendAjax(_h,function(J){
                notie.alert({text:J.message});
				_t.closest('.dTable').dataTable().fnDraw(false);
				for(var touchpoint in J.touchpoints){
					if(touchpoint != '')
						$('a[href*=' + touchpoint + '] .dataNumGrey').text(J.touchpoints[touchpoint]);
				}
				if($.isFunction(J.callback)){
					eval(J.callback)(J);
				}
			});
			return true;
		}
    });

    $('a.jajax-action').not('.dTable a.jajax-action,#timeline a.jajax-action').on('click',function(e){
		e.preventDefault();
		var t = $(this);
		sendAjax(t.attr('href'),function(ajax){
			if(!ajax.error){
                notie.alert({text:ajax.message});
			}else{
                notie.alert({text:'An error occurred.'});
			}
		});
	});
	//==== Enable compatibilty with legacy server side parameters ====//
	$.fn.dataTable.ext.legacy.ajax = true;
});