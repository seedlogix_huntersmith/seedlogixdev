if($('.login p').length){
	$('.login p').delay(3000).slideToggle(300,function(){
		$(this).remove();
	});
}

(function($){

	var _c = Cookies.get('remember');
	var _r = /.+@.+\..+/i;

	if(_c){
		var _l = Cookies.get('login');
		$('input[name="login"]').val(_l);
		$('input#remember').prop('checked',true);
	}

	$('.login form#login').off('submit').on('submit',function(){
		var _e = $('input[name="login"]').val();
		var _p = $('input[name="password"]').val();
		var _m = $('input#remember').prop('checked');
		if($.trim(_e).length && _r.test(_e) && $.trim(_p).length){
			if(_m){
				Cookies.set('login',_e,{expires:14});
				Cookies.set('remember',_m,{expires:14});
			} else{
				Cookies.remove('login');
				Cookies.remove('remember');
			}
			return true;
		} else{
			$('<p>Please enter valid credentials.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
				$(this).remove();
			});
			return false;
		}
	});

	$('a#forgot').off('click').on('click',function(){
		var _e = $('input[name="login"]').val();
		var _p = $('input[name="password"]').val();
		var _f = $('input#SL_ForgotPassword');
		var _s = $('.login form#login');
		if($.trim(_p).length){
			$('input[name="password"]').val('');
		}
		if($.trim(_e).length && _r.test(_e)){
			_f.val('forgot-password');
                        
                        $.ajax({
                            method: 'POST', 
                            url: _s.attr('action'), 
                            data: _s.serialize(), 
                            dataType: 'html',
                            success: function(data) {
                                $('<p class="success">Check your inbox to reset your password.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
                                        $(this).remove();
                                        return true;
                                });
                            },
                            error: function() {
                                $('<p>Password reset submit failed.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
                                        $(this).remove();
                                        return false;
                                });
                            }
                        });
                        
			//_s.submit();
			/*$('<p class="success">Check your inbox to reset your password.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
				$(this).remove();
			});
			return true;*/
		} else{
			$('<p>Please enter a valid email.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
				$(this).remove();
			});
			return false;
		}
	});

	$('.login.reset form#login').off('submit').on('submit',function(){
		var _p = $('input[name="password"]').val();
		var _c = $('input[name="password-confirm"]').val();
		if($.trim(_p).length && $.trim(_c).length){
			return true;
		} else{
			$('<p>Please enter a valid password.</p>').appendTo('.login').hide().slideToggle(300).delay(5000).slideToggle(300,function(){
				$(this).remove();
			});
			return false;
		}
	});

})(jQuery);