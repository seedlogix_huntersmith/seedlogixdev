<?php if(!$multi_user) return; ?>
var FieldLock	=	{ LOCKED: 1, UNLOCKED: 0};
FieldLock.lock	=	function (el){
	el.val(FieldLock.LOCKED);
	new AjaxSaveDataRequest(el[0]);	
};

FieldLock.unlock	=	function (el){
	el.val(FieldLock.UNLOCKED);
	
	new AjaxSaveDataRequest(el[0]);	
}

$('.adminLock').click(function(event){

	event.stopPropagation();

	var state	= $(this).attr('title');
	var input	= $('input',this);
	var span	= $('span',this);

	span.remove();

	if(state == 'Unlock'){
		$(this).append('<span class="unlock"></span>');
		FieldLock.unlock(input);
		$(this).attr('title','Lock');
	} else if(state == 'Lock'){
		$(this).append('<span class="lock"></span>');
		FieldLock.lock(input);
		$(this).attr('title','Unlock');
	} else{
		notie.alert({text:'An error occurred, please contact your system administrator.'});
	}

	
});