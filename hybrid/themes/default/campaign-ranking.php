<?php
/**
 * @var $this Template
 * @var $Campaign CampaignModel
 * @var $site_id int
 * @var $chartDataBing string
 * @var $chartDataGoogle string
 * @var $chartOptions string
 * @var $rankingKeywords string
 * @var $countRankingKeywords string
 */
//echo 'hello';
//var_dump($countRankingKeywords);
?>
<style>
.legendColorBox:first-of-type div > div, .legendColorBox:only-of-type div > div { background: #00aeef!important;}
.legendColorBox:nth-of-type(3) div > div { background: #27bdbe!important; }
.legendColorBox:nth-of-type(5) div > div { background: #C57FE5!important; }
</style>

    <!-- Top Ranking Keywords widget -->
<form action="<?=$this->action_url('Campaign_ajaxRankingChart', array('site_ID' => $site_id))?>">
	<input type="hidden" name="interval" value=""/>
	<input type="hidden" name="period" value=""/>
	<input type="hidden" name="chart" value="LeadsVisitsChartData"/>
    <div class="widget rightTabs">
        <div class="whead">
            <h6>Keyword Rankings</h6>

<?php $this->getWidget('graph-range-select.php',array('id' => 'chartconfig','value' => Cookie::getVal('chartconfig','7-DAY'))); ?>

        </div>
        <div class="tabs">
            <ul class="tabs">
                <li><a href="#tabs-8">Bing</a></li>
                <li class="activeTab"><a href="#tabs-7">Google</a></li>
            </ul>
            <div class="tab_container" style="margin: 0; padding: 0;">
                <div id="tabs-7" class="tab_content" style="margin: 0; padding: 10px;">
                    <div class="bars" id="googleRanking"></div>
                </div>
                <div id="tabs-8" class="tab_content hide" style="margin: 0; padding: 10px;">
                    <div class="bars" id="bingRanking"></div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="widget">
    <div class="whead">
        <h6>Keywords</h6>
        <input name="exportURL" type="hidden" class="action_url" value="<?= $this->action_url('piwikAnalytics_getCSVData?chart=RankingKeywords&period=PERIOD&interval=INTERVAL&iSortCol_0=SORTCOL&sSortDir_0=SORTDIR&bSortable_0=0&site_ID='.$site_id.'&campaign_id='.$Campaign->getID()); ?>">
        <a id="exportCampaign" title="Export Results" href="<?= $exportURL; ?>" class="bDefault f_rt"><span class="export"></span></a>
        <div class="buttonS f_rt jopendialog jopen-addkeywords"><span>Add a New Keyword</span></div>
    </div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('RankingKeywords'),
	'class'				=> 'col_end',
	'id'				=> 'ranking-keywords',
	'data'				=> $rankingKeywords,
	'rowkey'			=> 'keyword',
	'rowtemplate'		=> 'campaign-keywords.php',
	'total'				=> $countRankingKeywords,
	'sourceurl'			=> $this->action_url('Campaign_ajaxRankingKeywords',array('ID' => $Campaign->getID(),'site_ID' => $site_id)),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'params'            => array()
));
?>

</div>

<!-- Create Form POPUP -->
<?php $this->getDialog('add-ranking-keywords.php'); ?>


<script type="text/javascript">
$(document).ready(function () {
	var googleRanking = $("#googleRanking");
	var bingRanking = $("#bingRanking");
	function plotCharts(data){
		$.plot(googleRanking, data.chartDataGoogle, data.chartOptions);
		$.plot(bingRanking, data.chartDataBing, data.chartOptions);
	}

    Dashboard.reqsettings.success = plotCharts;
	//***@todo override ajax chart functions ***///
	Dashboard.reqsettings.error = function(){};
    //*********************************************

	// Confirm delete already being handled with jQuery Dialog
	/*
	var dataTable = $('.dataTable');
	var handler = dataTable.data('events').click[0].handler;
	dataTable.unbind('click');
	dataTable.on('click', '.jajax-action', function(e){
		if(confirm('Are you sure?'))
			handler(e);
		else
			return false;
	});
    */

    //===== Create New Form + PopUp =====//
    $('#jdialog-addkeywords').dialog(
    {
        autoOpen: false,
        height: 250,
        width: 500,
        modal: true,
        close: function ()
        {
            $('form',this)[0].reset();
        },
        buttons: (
			new CreateDialog(
				'Add',
				function (ajax, status) {
					if (!ajax.error) {
						$('.jnoforms').hide();
						var dt = $('#ranking-keywords');
						//var newRowsIds = dt.data('newRowsIds');
						//newRowsIds = ajax.object.map(function(obj){return obj.ID}).concat(newRowsIds);
						//dt.data('newRowsIds', newRowsIds);
						//dt.dataTable().fnDraw();
						
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.ID);
						dt.data('newRowsIds', newRowsIds);
                        dt.dataTable().fnDraw();
                        notie.alert({text:ajax.message});
                        this.close();
					}
					else if (ajax.error == 'validation') {
						alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
						alert(ajax.message);

				},
				'#jdialog-addkeywords'))
			.buttons
    });


    function showTooltip(x, y, contents, z) {
        var rootElt = 'body';

        $('<div id="tooltip3" class="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y - 55,
            left: x - 55,
            'z-index': '9999',
            'color': '#fff',
            'font-size': '11px'
        }).prependTo(rootElt).show();
    }

    function tooltip (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                $("#tooltip3").remove();

                y = item.series.data[item.dataIndex][1];
                z = item.series.color;
                var mdate = new Date(item.series.data[item.dataIndex][0]);
                var day = mdate.getUTCDate();
                var month = mdate.getUTCMonth();
                var monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b><br /> " + monthArray[month] + " " + day + " = " + y + " Keywords",
                    z);
            }
        } else {
            $("#tooltip3").remove();
            previousPoint = null;
        }
    }

	plotCharts({
		chartDataBing: <?=$chartDataBing?>,
		chartDataGoogle: <?=$chartDataGoogle?>,
		chartOptions: <?=$chartOptions?>
	});
    googleRanking.bind("plothover", tooltip);
    bingRanking.bind("plothover", tooltip);

	$('.graph_rangectrl').change(function(){

	});
});
</script>