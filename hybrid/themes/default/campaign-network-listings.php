<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Network Listings</h6>
			<div class="mr45">
				<div class="buttonS bCrimson right dynRight jopendialog jopen-addAccount"><span class="iconb"
																								data-icon="&#xe099;
																								"></span> Add Listing</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="dyn">
			<?php
			$this->getWidget('datatable.php',
				array('columns' => DataTableResult::getTableColumns('NetworkListings'),
					'class' => '',
					'id' => 'jNetworkListings',
					'data' => $NetworkListings,
					'rowkey' => 'NetworkListing',
					'rowtemplate' => 'network-listings.php',
					'total' => $total_networklistings_match,
					'sourceurl' => $this->action_url('campaign_ajaxnetworklistings', array('ID' => $_GET['ID'])),
					'DOMTable' => '<"H"lf>rt<"F"ip>'
				)
			);
			?>
                    
			<!--<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="leadforms-tbl">
				<thead>
					<tr>
						<td>Name</td>
						<td>Network</td>
						<td>Authenticated</td>
						<td width="100">Actions</td>
					</tr>
				</thead>
				<tbody class="jforms">
		
				</tbody>
			</table>-->
		</div>
	</div>
</div><!-- /end fluid -->



