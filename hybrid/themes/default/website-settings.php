<?php

	$gridLength = 'grid9';

	$touchpointType = $site->getTouchpointType();
	$disable = $User->can('edit_' . $touchpointType, $site->getID()) ? "" : "disabled";

	if($multi_user){
		$gridLength = 'grid8';
	}
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

	<div class="grid12 m_tp">
		<form action="<?= $this->action_url("{$touchpointType}_save?ID=".$site->id); ?>" method="POST" class="ajax-save">
				<div class="widget grid6">
					<div class="whead">
						<h6>Main Settings</h6>
					</div>
					<div class="formRow">
						<div class="grid3"><label>TouchPoint Name:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'name','value' => $site->name)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Company Name:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'company_name','value' => $site->company_name)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Phone Number:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'phone','value' => $site->phone)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Address:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'street','value' => $site->street)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>City:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'city','value' => $site->city)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>State/Province:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'state','value' => $site->state,'widget' => 'states-dropdown.php')); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Zip/Postal Code:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'zip','value' => $site->zip)); ?>
					</div>
<?php if($touchpointType == 'PROFILE'): ?>
					<div class="formRow">
						<div class="grid3"><label>Category:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'category','value' => $site->category,'widget' => 'industries-dropdown.php')); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Website URL:</label></div>
<?php $this->getWidget('input-website.php',array('name' => 'mobile_domain','value' => $site->mobile_domain)); ?>
					</div>
					<div class="formRow">
						<div class="grid11"><label>Profile Description:</label></div>
					</div>
					<div class="formRow">
<?php $this->getWidget('input-website.php',array('maxGrid' => 12,'widget' => 'codemirror.php','widgetParams' => array('id' => 'code'),'name' => 'description','value' => $site->description)); ?>
					</div>
<?php endif; ?>
				</div>

				<div class="widget grid6">
					<div class="whead"><h6>Brand Settings</h6></div>
					<div class="formRow">
						<div class="grid3"><label>Company Logo:</label></div>
						<div class="<?= $gridLength; ?>">
<?php
$this->getWidget('input-website.php',array(
	'name' => 'logo',
	'value' => $site->logo,
	'widget' => 'moxiemanager-filepicker.php',
	'widgetParams' => array(
		'idWidget' => 'fileInput',
		'path' => $storagePath,
		'noHost' => true,
		'extensions' => 'jpg,jpeg,png,gif'
		)
	)
);
?>
						</div>
					</div>
					<?php   if($sitetheme->hybrid==1) { ?>
					<?php if($User->parent_id == 6312 || $User->id == 6312 && $sitetheme->hybrid==1) { ?>
						<div class="formRow">
							<div class="grid3"><label>Website Description:</label></div>
							<?php $this->getWidget('input-website.php', array(
								'name' => 'description',
								'value' => $site->description
							));?>
						</div>
						<div class="formRow">
							<div class="grid3"><label>Category:</label></div>
							<?php $this->getWidget('input-website.php', array('name' => 'category', 'value' => $site->category, 'widget' => 'industries-dropdown.php')); ?>
						</div>
					<? } ?>
					<? } ?>

					<?php   if(!$sitetheme->hybrid==1) { ?>
					<div class="formRow">
						<div class="grid3"><label>Coupon Offer:</label></div>
						<div class="j-dual-ide" style="position: relative;">
							<?php $this->getWidget('input-website.php', array(
								'widget' => 'wysiwyg-ide.php',
								'widgetParams' => array(
									'fieldname' => 'website[coupon_offer]',
									'fieldvalue' => $site->coupon_offer
								))); ?>
						</div>
					</div>

						<div class="formRow">
							<div class="grid3"><label>Slogan/Tagline:</label></div>
							<div class="j-dual-ide" style="position: relative;">
								<?php $this->getWidget('input-website.php', array(
									'widget' => 'wysiwyg-ide.php',
									'widgetParams' => array(
										'fieldname' => 'website[slogan]',
										'fieldvalue' => $site->slogan
									))); ?>
							</div>
						</div>

					<div class="formRow">
						<div class="grid3"><label>Website Description:</label></div>
							<?php $this->getWidget('input-website.php', array(
								'name' => 'description',
								'value' => $site->description
							));?>
					</div>
					<? } ?>


				</div>

			<?php if($User->parent_id == 6312 || $User->id == 6312) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>
					<div class="formRow">
						<div class="grid3"><label>Feed ID:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'has_feedobjects', 'value' => $site->has_feedobjects)); ?>
					</div>

				</div><!-- //section -->
			<? } ?>

			<?php if($User->parent_id == 40257 || $User->id == 40257) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>
					<div class="formRow">
						<div class="grid3"><label>Lat:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'lat', 'value' =>
							$site->lat)); ?>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Lng:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'lng', 'value' =>
							$site->lng)); ?>
					</div>

				</div><!-- //section -->
			<? } ?>


			<?php if($site->hub_parent_id == 116543 || $site->id == 116543 || $site->hub_parent_id == 105981 || $site->id == 105981) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>
					<div class="formRow">
						<div class="grid3"><label>ignitedAGENT Domain:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword1', 'value' => $site->keyword1)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>ignitedLOCAL Domain:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword2', 'value' => $site->keyword2)); ?>
					</div>


				</div><!-- //section -->
			<? } ?>

			<?php if($site->hub_parent_id == 136134 || $site->id == 136134 || $site->hub_parent_id == 119174 || $site->id == 119174) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>

					<?php if($site->hub_parent_id == 136134 || $site->id == 136134) { ?>
					<div class="formRow">
						<div class="grid3"><label>SYNLawn.com Domain:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword1', 'value' => $site->keyword1)); ?>
					</div>
					<? } ?>
					<?php if($site->hub_parent_id == 119174 || $site->id == 119174) { ?>
					<div class="formRow">
						<div class="grid3"><label>SYNLawnGolf.com Domain:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword2', 'value' => $site->keyword2)); ?>
					</div>
					<? } ?>


				</div><!-- //section -->
			<? } ?>

			<?php if($User->parent_id == 53988 || $User->id == 53988) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>
					<div class="formRow">
						<div class="grid3"><label>Custom Tag1: ((custom1))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword1', 'value' => $site->keyword1)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Custom Tag2:((custom2))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword2', 'value' => $site->keyword2)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Custom Tag3:((custom3))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword3', 'value' => $site->keyword3)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Custom Tag4:((custom4))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword4', 'value' => $site->keyword4)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Custom Tag5:((custom5))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword5', 'value' => $site->keyword5)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Custom Tag6:((custom6))</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword6', 'value' => $site->keyword6)); ?>
					</div>

				</div><!-- //section -->
			<? } ?>

			<?php if($User->parent_id == 2556 || $User->id == 2556) { ?>
				<!-- section -->
				<div class="widget grid6">
					<div class="whead"><h6>Custom Settings</h6></div>

					<div class="formRow">
						<div class="grid3"><label>Branch Name:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'full_home', 'value' => $site->full_home)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Team Name:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'remove_font', 'value' => $site->remove_font)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>LO Photo:</label></div>
						<div class="<?=$gridLength?>">
							<?php $this->getWidget('input-website.php', array(
								'name' => 'profile_photo',
								'value' => $site->profile_photo,
								'widget' => 'moxiemanager-filepicker.php',
								'widgetParams' => array(
									'idWidget' => 'profile-photo-widget',
									'path' => $storagePath,
									'noHost' => true,
									'extensions' => 'jpg,jpeg,png,gif'
								)));?>
						</div>
					</div>

					<div class="formRow">
						<div class="grid3"><label>LO Name:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword1', 'value' => $site->keyword1)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Title:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword2', 'value' => $site->keyword2)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>NMLS#:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword3', 'value' => $site->keyword3)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Additional NMLS Info:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword4', 'value' => $site->keyword4)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Email:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword5', 'value' => $site->keyword5)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Cell:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'keyword6', 'value' => $site->keyword6)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Fax:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'button_1', 'value' => $site->button_1)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Additional Text:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'button1_link', 'value' => $site->button1_link)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Schedule Meeting Link:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'button_2', 'value' => $site->button_2)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Apply Now ID:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'button2_link', 'value' => $site->button2_link)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Benefit IQ Link:</label></div>
						<div class="j-dual-ide" style="position: relative;">
							<?php $this->getWidget('input-website.php', array(
								'widget' => 'wysiwyg-ide.php',
								'widgetParams' => array(
									'fieldname' => 'website[button_3]',
									'fieldvalue' => $site->button_3
								))); ?>
						</div>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Mortgage Calculator Link:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'button3_link', 'value' => $site->button3_link)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>MortgageMapp Link:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'legacy_gallery_link', 'value' => $site->legacy_gallery_link)); ?>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Zillow Screename:</label></div>
						<?php $this->getWidget('input-website.php', array('name' => 'form_box_color', 'value' => $site->form_box_color)); ?>
					</div>


					<div class="formRow">
						<div class="grid3"><label>About LO:</label></div>
						<div class="j-dual-ide" style="position: relative;">
							<?php $this->getWidget('input-website.php', array(
								'widget' => 'wysiwyg-ide.php',
								'widgetParams' => array(
									'fieldname' => 'website[slogan]',
									'fieldvalue' => $site->slogan
								))); ?>
						</div>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Testimonials:</label></div>
						<div class="j-dual-ide" style="position: relative;">
							<?php $this->getWidget('input-website.php', array(
								'widget' => 'wysiwyg-ide.php',
								'widgetParams' => array(
									'fieldname' => 'website[edit_region_10]',
									'fieldvalue' => $site->edit_region_10
								))); ?>
						</div>
					</div>

					<div class="formRow">
						<div class="grid3"><label>Additional LO Links:</label></div>
						<div class="j-dual-ide" style="position: relative;">
							<?php $this->getWidget('input-website.php', array(
								'widget' => 'wysiwyg-ide.php',
								'widgetParams' => array(
									'fieldname' => 'website[edit_region_11]',
									'fieldvalue' => $site->edit_region_11
								))); ?>
						</div>
					</div>



				</div><!-- //section -->
			<? } ?>

            <?php
            if(!$multi_user):
                ?>
                <!-- section -->
                <div class="widget grid6 m_tp m_lt">
                    <div class="whead"><h6><?php if($touchpointType=='PROFILE'){?>Profile URL Settings<?php } else { ?>Domain Settings<?php } ?></h6></div>
                    <div class="formRow">
						<?php if(!$site->domain && $touchpointType=='PROFILE'){?>
							<div class="grid3"><label>Username (e.g. localmashup):</label></div>
							<?php $this->getWidget('input-website.php', array('name' => 'domain', 'value' => $site->domain)); ?>
						<?php } elseif($site->domain && $touchpointType=='PROFILE') { ?>
							<div class="grid3"><label><?=$site->domain?></label></div>
						<?php } else { ?>
							<div class="grid3"><label>Domain:</label></div>
							<?php $this->getWidget('input-website.php', array('name' => 'domain', 'value' => $site->domain)); ?>
						<?php } ?>
                    </div>
					<?php if($touchpointType!='PROFILE'){?>
					<div class="whead exp3">
						<h6>Domain Setup</h6>
						<span class="chevron"></span>
					</div>
                    <div class="formRow hide">
                        <p>To activate a domain you must setup an A record pointing to any of the following IP Addresses. If you are setting up multiple subdomains on the same domain, please setup an A record with the name * and point to any of these IP Addresses.</p>
						<p>If you need help, please contact support.</p>
						<ul class="bullets">
							<li>50.22.60.10</li>
							<li>50.23.225.229</li>
							<li>50.97.74.154</li>
							<li>50.97.78.212</li>
							<li>50.97.78.210</li>
							<li>50.97.78.208</li>
							<li>50.97.78.209</li>
							<li>50.97.78.211</li>
							<li>50.97.78.213</li>
							<li>50.97.78.214</li>
							<li>50.97.78.215</li>
							<li>50.97.112.106</li>
							<li>184.173.219.90</li>
							<li>184.173.219.91</li>
							<li>184.173.219.92</li>
							<li>184.173.219.93</li>
							<li>184.173.219.94</li>
							<li>184.173.219.95</li>
						</ul>
                    </div>
					<?php } ?>
                </div><!-- //section -->

            <?php
            endif;
            ?>
			<?php
			if($multi_user){
			$this->getWidget('website-roles.php');
			}
			?>



			<input type="hidden" value="<?=$site->touchpoint_type?>" name="website[touchpoint_type]"/>
		</form>
	</div>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>
	
    var FieldLock	=	{ LOCKED: 1, UNLOCKED: 0};
    FieldLock.lock	=	function (el){
        el.val(FieldLock.LOCKED);
        new AjaxSaveDataRequest(el[0]);	
    };

    FieldLock.unlock	=	function (el){
        el.val(FieldLock.UNLOCKED);
        new AjaxSaveDataRequest(el[0]);	
    };

    $(document).ready(function (){

        <?php $this->getJ('admin-lock.php'); ?>
        <?php $this->getJ('wysiwyg-ide.php', array('disable' => !empty($disable))); ?>

    });

<?php $this->end_script(); ?>
</script>