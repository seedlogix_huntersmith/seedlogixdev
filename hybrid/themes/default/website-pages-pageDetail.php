<?php
$disable        = $User->can('edit_touchpoint',$site->getID()) ? '' : 'disabled';
if($multi_user){
    $musite     = '&musite=1';
}
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

    <form action="<?= $this->action_url('Website_savepage?PID='.$page->ID); ?>" method="POST" class="ajax-save" id="jpage-form">
        <div class="grid12 m_tp">
            <div class="widget grid6">
                <div class="whead"><h6>Main Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Parent Nav Section:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'name'          => 'nav_parent_id',
    'model'         => 'page',
    'lockModel'     => 'page',
    'control'       => 'navsection-select.php',
    'widgetParams'  => array(
                    'fieldname'             => 'page[nav_parent_id]',
                    'selected'              => $page->nav_parent_id,
                    'currentID'             => 0
                    )
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Override Page Order:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'nav_order',
    'value'         => $page->nav_order
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Include In Navigation:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'inc_nav',
    'value'         => $page->inc_nav,
    'type'          => 'checkbox',
    'class'         => ''
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Include In Tag Navigation:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'inc_contact',
    'value'         => $page->inc_contact,
    'type'          => 'checkbox',
    'class'         => ''
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Outbound Link:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'outbound_url',
    'value'         => $page->outbound_url
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Outbound Link Target:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'type'          => 'select',
    'name'          => 'outbound_target',
    'value'         => $page->outbound_target,
    'options'       => array(
                    'New Window/Tab'        => '_self',
                    'No Same Window/Tab'    => '_blank'
                    )
    )
);
?>

                </div>

<?php if(!$sitetheme->hybrid == 1): ?>

                <div class="formRow">
                    <div class="grid3"><label>Full Width:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_full_width',
    'value'         => $page->page_full_width,
    'type'          => 'checkbox',
    'class'         => ''
    )
);
?>

                </div>

<? endif; ?>

            </div>
            <div class="widget grid6">
                <div class="whead"><h6>Page Level Meta</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Page Name:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_title',
    'value'         => $page->page_title
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>SEO Page Description:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_seo_desc',
    'value'         => $page->page_seo_desc
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>SEO Title (Optional):</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_seo_title',
    'value'         => $page->page_seo_title
    )
);
?>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Keywords:</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_keywords',
    'id'            => 'page_keywords',
    'value'         => $page->page_keywords,
    'class'         => 'tags'
    )
);
?>

                </div>
				 <div class="formRow">
                    <div class="grid3"><label>Canonical Page Override (relative path only):</label></div>

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_photo_desc',
    'value'         => $page->page_photo_desc
    )
);
?>

                </div>
				
            </div>
        </div>
        <div class="grid12">

<?php if($sitetheme->hybrid == 1): ?>

            <div class="widget">
                <div class="whead"><h6>Page Content</h6></div>
                <iframe src="/themes/preview-editor/v1/index.php?siteid=<?= $site->id; ?>&themeid=<?= $site->theme; ?>&pageid=<?= $page->id; ?>&column=page_region&touchpoint=<?= $site->touchpoint_type.$musite; ?>" width="100%" id="iframe1" marginheight="0" frameborder="0" scrolling="no"></iframe>
            </div>
            <div class="widget">
                <div class="whead"><h6>Override Page Header</h6></div>
                <iframe src="/themes/preview-editor/v1/index.php?siteid=<?= $site->id; ?>&themeid=<?= $site->theme; ?>&pageid=<?= $page->id; ?>&column=page_edit_3&touchpoint=<?= $site->touchpoint_type.$musite; ?>" width="100%" id="iframe2" marginheight="0" frameborder="0" scrolling="no"></iframe>
            </div>
            <div class="widget">
                <div class="whead"><h6>Override Page Footer</h6></div>
                <iframe src="/themes/preview-editor/v1/index.php?siteid=<?= $site->id; ?>&themeid=<?= $site->theme; ?>&pageid=<?= $page->id; ?>&column=page_edit_2&touchpoint=<?= $site->touchpoint_type.$musite; ?>" width="100%" id="iframe3" marginheight="0" frameborder="0" scrolling="no"></iframe>
            </div>

<script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
<script type="text/javascript">
iFrameResize({
    log                     : true,
    enablePublicMethods     : true,
    resizedCallback         : function(messageData){
                            $('p#callback').html('<b>Frame ID:</b> ' + messageData.iframe.id + ' <b>Height:</b> ' + messageData.height + ' <b>Width:</b> ' + messageData.width + ' <b>Event Type:</b> ' + messageData.type);
                            },
    messageCallback         : function(messageData){
                            $('p#callback').html('<b>Frame ID:</b> ' + messageData.iframe.id + ' <b>Message:</b> ' + messageData.message);
                            alert(messageData.message);
                            },
    closedCallback          : function(id){
                            $('p#callback').html('<b>IFrame (</b>' + id + '<b>) removed from page.</b>');
                            }
});
</script>

<?php else: ?>

            <div class="widget">
                <div class="whead"><h6>Main Page Section</h6></div>
                <div class="formRow">

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'maxGrid'       => 12,
    'widget'        => 'wysiwyg-ide.php',
    'name'          => 'page_region',
    'widgetParams'  => array(
                    'fieldname'             => 'page[page_region]',
                    'fieldvalue'            => $page->page_region
                    )
    )
);
?>

                </div>
            </div>
            <div class="widget">
                <div class="whead"><h6>Sub Section</h6></div>
                <div class="formRow">

<?php
$this->getWidget('input-website.php',array(
    'lockModel'     => 'page',
    'maxGrid'       => 12,
    'widget'        => 'wysiwyg-ide.php',
    'name'          => 'page_edit_2',
    'widgetParams'  => array(
                    'fieldname'             => 'page[page_edit_2]',
                    'fieldvalue'            => $page->page_edit_2
                    )
    )
);
?>

                </div>
            </div>

<?php endif; ?>

            <div class="widget">
                <div class="whead"><h6>Page Level Advanced CSS</h6></div>
                <div class="formRow"><label>Advanced CSS:</label></div>
                <div class="formRow">

<?php
$this->getWidget('input-website.php',array(
    'maxGrid'       => 12,
    'widget'        => 'codemirror.php',
    'lockModel'     => 'page',
    'model'         => 'page',
    'name'          => 'page_adv_css',
    'widgetParams'  => array('id' => 'code'),
    'name'          => 'page_adv_css',
    'value'         => $page->page_adv_css
    )
);
?>

                </div>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">

<?php $this->start_script(); ?>

$(document).ready(function(){

<?php $this->getJ('admin-lock.php'); ?>

    //===== iButtons =====//
    $('input[type="checkbox"].i_button,input[type="radio"].i_button').iButton({
        labelOn     : '',
        labelOff    : '',
        change      : function($input){
                    var value = $input.attr('checked') ? 1 : 0;
                    new AjaxSaveDataRequest($input.get(0),value);
        }
    });

<?php $this->getJ('wysiwyg-ide.php',array('disabled' => $disable)); ?>

});

<?php $this->end_script(); ?>

</script>