<?php
/**
 * @var $roles stdClass[]
 * @var $form LeadFormModel
 */

$isAdminUser		= $User->isAdmin();
$disabled			= $form->isLegacyForm() ? ' disabled' : '';
?>

		<div class="fluid" id="FormConfig">

<?php if(!isset($field_edit)): ?>

			<div class="widget">
				<div class="whead"><h6><?= $form->name; ?> - Options</h6></div>
				<div class="formRow">
					<div class="grid3"><label>Embed Code:</label></div>
					<div class="grid9">
						<span><?= "|@form-{$form->id}@|"; ?></span>
						<span class="note">(Paste this into any Hub or Hub Page edit region)</span>
					</div>
				</div>
				<form class="ajax-save" method="POST" action="<?= $this->action_url('Campaign_save'); ?>">
					<input type="hidden" value="<?= $form->getID(); ?>" name="form[ID]">
					<input type="hidden" value="<?= $form->cid; ?>" name="campaign[cid]">
					<div class="formRow">
						<div class="grid3"><label>Submit Button Text:</label></div>
						<div class="grid9"><input type="text" class="no-upload jajax-required" value="<?php $form->html('submit_button_text'); ?>" name="form[submit_button_text]"></div>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Redirect URL:</label></div>
						<div class="grid9"><input type="text" class="no-upload jajax-required" value="<?php $form->html('redirect_url'); ?>" name="form[redirect_url]"></div>
					</div>
					<div class="formRow">
						<div class="grid3"><label>Show Reset Button:</label></div>
						<div class="grid9"><input type="checkbox" name="form[show_reset_button]" class="i_button jajax-required" value="1"<?php $this->checked($form->show_reset_button,1); ?>></div>
					</div>
				</form>
			</div>

<?php endif; ?>

<?php if(isset($field_edit)): ?>

			<div class="widget">
				<div class="whead"><h6>Form Field Configuration</h6></div>

<?php $this->getControl($field_edit->type.'-field.php',array('fieldID' => $field_edit->getID(),'type' => $field_edit->type,'field' => $field_edit)); ?>

			</div>

<?php endif; ?>

<?php if($isAdminUser && $form->multi_user): ?>

			<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save" method="post">
				<input type="hidden" name="form[ID]" value="<?= $form->getID(); ?>">
				<div class="fluid">
					<div class="widget grid3">
						<div class="whead"><h6>MU Form Access</h6></div>

<?php foreach($roles as $role): ?>

						<div class="formRow">
							<div class="grid6"><label><?= $role->role; ?>:</label></div>
							<div class="grid6"><input type="checkbox" name="form[roles][<?= $role->ID; ?>]" class="i_button" value="<?= $role->ID; ?>"<?php $this->checked($role->assigned,1); ?>></div>
						</div>

<?php endforeach; ?>

					</div>
				</div>
			</form>

<?php endif; ?>

<?php if(!isset($field_edit)): ?>

			<div class="widget jfields-list">
				<div class="whead">
					<h6><?=$form->name?> - Fields</h6>
					<div class="buttonS f_rt jopendialog jopen-createformfield<?= $disabled; ?>"><span>Create a New Form Field</span></div>

<? if($form->isLegacyForm()): ?>

					<div class="buttonS f_rt convertform" id="convertform" hiddendata="<?= $this->action_url('Campaign_LeadConvert'."?ID=".$Campaign->id.'&LF_ID='.$form->getID()); ?>"><span>Edit the Fields</span></div>

<? endif; ?>

				</div>
				<table cellpadding="0" cellspacing="0" width="100%" class="tDefault" id="leadforms-tbl">
					<thead>
						<tr>
							<td>Name</td>
							<td>Type</td>
							<td>Required</td>
							<td class="hide">Order</td>
							<td>Actions</td>
						</tr>
					</thead>
					<tbody class="jforms"<?= !$form->isLegacyForm() ? ' id="sortable"' : ''; ?>>

<?php
if($form_fields):
	foreach($form_fields as $ke => $field){
		$this->getRow('leadform-field.php',array('field' => $field,'editable' => !$form->isLegacyForm()));
	}
else:
?>

						<tr class="jnoforms"><td colspan="4">Add fields to your form.</td></tr>

<?php endif; ?>

					</tbody>
				</table>
			</div>

<?php endif; ?>

		</div><!-- /.fluid -->

<!-- Create Form Field POPUP -->
<?php $this->getDialog('create-field.php'); ?>

<script>

<?php $this->start_script(); ?>

	$(function(){
	
			//===== Sortable Table Columns =====//
			// Return a helper with preserved width of cells
			var fixHelper = function(e, ui) {
			    ui.children().each(function() {
			        $(this).width($(this).width());
			    });
			    return ui;
			};
			<?php if(!$form->isLegacyForm()):?>
			$("#sortable").sortable({
			    helper: fixHelper,
                forceHelperSize: true,
                stop: function( event, ui ) {
                    var tr = ui.item;
                    var fID = tr.attr('id').split(/_/).pop();
                    var index = tr[0].rowIndex;
                    sendAjax("<?= $this->action_url('Campaign_save') ?>", function (){}, {'field': {'ID': fID, 'index': index, 'name': 'sorted'}}, 'POST');
                }
			}).disableSelection();
			<?php endif;?>

			//===== Create Form PopUp =====//
			$('#jdialog-createformfield').dialog(
			{
				autoOpen: false,
				height: 200,
				width: 400,
				modal: true,
				close: function ()
				{
					$('#campaign_id',this).val('');
					$('form',this)[0].reset();
				},
				buttons: (new CreateDialog('Create',
					function (ajax, status)
					{
						
		      	if(!ajax.error)
		      	{
		          $('.jnoforms').hide();
		          $('.jforms').append(ajax.row);
				  notie.alert({text:ajax.message});
		          this.close();
							this.form[0].reset();
						}
						else if(ajax.error == 'validation')
						{
		        	alert(getValidationMessages(ajax.validation).join('\n'));
						}
						else
		          alert(ajax.message);
						
					},'#jdialog-createformfield')).buttons
			});


            //========Convert Form Process==========//

        $( "#convertform" ).click(function(e) {
            var linkButton = $(e.currentTarget);
            sendAjax(linkButton.attr('hiddendata'), function (J){
				notie.alert({text:J.message});
                location.reload();
            });
        });

			
			$('#leadforms-tbl').on('click', '.ajax-action', function(e){
				e.preventDefault();
				var linkButton = $(e.currentTarget);
				sendAjax(linkButton.attr('href'), function (J){
					notie.alert({text:J.message});
					if(linkButton.hasClass('jaction-delrow-leadfield')){
						linkButton.closest('tr').remove();
					}
				});
				return true;
			});

		$('.jajax-redirect').click(function(){
			document.location = "<?=$this->action_url('campaign_form', array('ID' => $form->cid, 'form' => $form->getID()))?>" ;
		});

	});

<?php $this->end_script(); ?>

</script>