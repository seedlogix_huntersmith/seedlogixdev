<!-- ######################## -->
<!-- ##### Social Spots ##### -->
<!-- ######################## -->
<div class="fluid">
    <form action="<?php echo $this->action_url('Campaign_save'); ?>" method="POST" class="ajax-save">
      
			<input type="hidden" name="listing[type]" value="<?php echo $dirarticle->getNetworkListingType(); ?>" />
			<input type="hidden" name="listing[id]" value="<?php echo $dirarticle->getID(); ?>" />
    <div class="widget grid12">
        <div class="whead">
            <h6>Social Networking</h6>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Company Blog Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[blog]" value="<?php $dirarticle->html('blog'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Twitter Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[twitter]" value="<?php $dirarticle->html('twitter'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Facebook Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[facebook]" value="<?php $dirarticle->html('facebook'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Myspace Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[myspace]" value="<?php $dirarticle->html('myspace'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Youtube Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[youtube]" value="<?php $dirarticle->html('youtube'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Diigo Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[diigo]" value="<?php $dirarticle->html('diigo'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>Technorati Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[technorati]" value="<?php $dirarticle->html('technorati'); ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="grid3">
            <lable>LinkedIn Link</lable>
            </div>
            <div class="grid9">
                <input type="text" name="listing[linkedin]" value="<?php $dirarticle->html('linkedin'); ?>" />
            </div>
            <div class="clear"></div>
        </div>    
    </div>
    </form>
</div>