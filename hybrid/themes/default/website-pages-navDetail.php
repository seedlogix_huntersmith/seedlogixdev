<!-- LEFT SIDE NAV -->
<?php
$disable = $User->can('edit_touchpoint', $site->getID()) ? "" : "disabled";
$controller = strtolower($site->touchpoint_type);
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

<div class="grid12 m_tp">
	<form action="<?= $this->action_url('Website_savepage?PID=' . $page->ID); ?>" method="POST" class="ajax-save" id="jpage-form">
		<div class="widget">
			<div class="whead">
				<h6>Main Settings</h6>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Nav Name:</label></div>
				<?php $this->getWidget('input-website.php', array('lockModel' => 'page','model' => 'page', 'name' => 'page_title', 'value' => $page->page_title)); ?>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Override Page Order:</label></div>
				<?php $this->getWidget('input-website.php', array('lockModel' => 'page','model' => 'page', 'name' => 'nav_order', 'value' => $page->nav_order)); ?>
			</div>
			<div class="formRow">
				<div class="grid3">
					<label>Parent Nav Section:</label>
				</div>

				<?php $this->getWidget('input-website.php', array(
					'name' => 'nav_parent_id',
					'model' => 'page',
					'lockModel' => 'page',
					'maxGrid' => 9,
					'control' => 'navsection-select.php',
					'widgetParams' => array(
						'fieldname' => 'page[nav_parent_id]',
						'selected_nav_id' => $page->nav_parent_id,
						'currentID' => $page->id
					))); ?>

			</div>
		</div>
	</form>
    <div class="widget m_tp">
		<div class="whead">
			<h6><?= $this->p($page->page_title); ?></h6>

<?php $this->getWidget('admin-lock.php',array('model' => 'page','fieldname' => 'allow_pages','isAdminUser' => $isAdminUser)); ?>

		<?php $this->getWidget('button-website.php', array(
			'id' => 'createNav',
			'fieldname' => 'allow_pages',
			'label' => 'Create a New Nav Section',
			'class' => ''
		))?>
		<?php $this->getWidget('button-website.php', array(
			'id' => 'createPage',
			'fieldname' => 'allow_pages',
			'label' => 'Create a New Page',
			'class' => ''
		))?>
		</div>
<?php if(!$Nodes): ?>
		<ul class="updates center">
			<li>No Stats Available.</li>
		</ul>
<?php else: ?>
		<table class="tDefault col_1st col_end" style="width: 100%;">
			<thead>
				<tr>
					<td>Title</td>
					<td>Type</td>
					<td>Nav Root</td>
					<td>Creation Date</td>
<?php if(empty($disable)): ?>
					<td>Actions</td>
<?php endif; ?>
				</tr>
			</thead>
			<tbody>
<?php
foreach($Nodes as $k => $Node){
	$this->getRow('nav-row.php',array(
		'NextNode'		=> isset($Nodes[$k+1]) ? $Nodes[$k+1] : NULL,
		'controller'	=> $controller,
		'Node'			=> $Node,
		'level'			=> 0,
		'k'				=> $k,
		'siblings'		=> count($Nodes)-1,
		'ParentName'	=> $page->page_title,
		'disabled'		=> !empty($disable)
	));
}
?>
			</tbody>
		</table>
<?php endif; ?>
    </div>
</div>
</div>

<?php if(empty($disable)):?>
<!-- Create New Page POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Page" id="create-page" style="display: none;">
	<form id="createPageForm" action="<?= $this->action_url("{$site->touchpoint_type}_newpage?ID=$site->id"); ?>" method="post">
	<input type="hidden" name="action[name]" value="pagedetails">
	<input type="hidden" name="action[param]" value="NID">
	<input type="hidden" name="page[nav_parent_id]" value="<?= $page->ID; ?>">
	<fieldset>
		<div class="fluid spacing">
			<div class="grid4"><label>Name:</label></div>
			<div class="grid8"><input type="text" name="page[page_title]" id="campaign_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
		</div>
	</fieldset>
	</form>
</div>
<!-- Create Nav Section POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Nav Section" id="create-nav" style="display: none;">
	<form id="createNavForm" action="<?= $this->action_url("{$site->touchpoint_type}_newnav?ID=$site->id"); ?>" method="post">
	<input type="hidden" name="action[name]" value="pagedetails">
	<input type="hidden" name="action[param]" value="NID">
	<input type="hidden" name="nav[nav_parent_id]" value="<?= $page->ID; ?>">
	<fieldset>
		<div class="fluid spacing">
			<div class="grid4"><label>Name:</label></div>
			<div class="grid8"><input type="text" name="nav[page_title]" id="campaign_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
		</div>
	</fieldset>
	</form>
</div>
<?php endif;?>
<script type="text/javascript">

	//Valid CreateNav, DelNav
<?php if(isset($validNav)): ?>
		notie.alert({text:getValidationMessages(<?= json_encode($validNav); ?>)});
<?php elseif(isset($verifyNav)): ?>
		notie.alert({text:'<?= $verifyNav; ?>'});
<?php elseif(isset($delNav)): ?>
		notie.alert({text:'<?= $delNav; ?>'});
<?php endif; ?>

	//Valid CreatePage
<?php if(isset($validCP)): ?>
		notie.alert({text:getValidationMessages(<?= json_encode($validCP); ?>)});
<?php endif; ?>

	//===== Dynamic data table =====//
	
	oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"bSort": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});

	$( "#create-page" ).dialog({
		autoOpen: false,
		height: 200,
		width: 400,
		modal: true,
		buttons: {
			Cancel: function(){
				$(this).dialog('close');
			},
			"Create" : function(){
				$('#createPageForm').submit();
			}
		},
		buttonsf: (new CreateDialog("Create",
			function (ajax, status){
				// standards here will be 'error' var, possible values are : 'validation', 'other'
				//  if error = 'validation', check 'validation' object,
				// if error = 'other' then print 'message''
				// if no error or error = false, check result object. (could be a partially
				if(ajax.error)
				{

				}else{
					//var arr =   [ajax.result.name, 0, 0, "0 %", 0, new Date(), "other"];
					//new InsertObjectAtTable('#campaigns-stats-tbl', arr, ajax.result.actions.dashboard);
					$('#create-page').dialog('close');
				}

			})).buttons
    });
    
    $( "#create-nav" ).dialog({
		autoOpen: false,
		height: 200,
		width: 400,
		modal: true,
		buttons : {
			Cancel: function(){
				$(this).dialog('close');
			},
			"Create" : function(){
				$('#createNavForm').submit();
			}
		},
		buttonsf: (new CreateDialog("Create",
			function (ajax, status){
				// standards here will be 'error' var, possible values are : 'validation', 'other'
				//  if error = 'validation', check 'validation' object,
				// if error = 'other' then print 'message''
				// if no error or error = false, check result object. (could be a partially
				if(ajax.error)
				{

				}else{
					//var arr =   [ajax.result.name, 0, 0, "0 %", 0, new Date(), "other"];
					//new InsertObjectAtTable('#campaigns-stats-tbl', arr, ajax.result.actions.dashboard);
					$('#create-nav').dialog('close');
				}

			})).buttons
    });
    
    $( "#createNav:not(.disabled)" ).click(function() {
    	$( "#create-nav" ).dialog( "open" );
    });
    
    $( "#createPage:not(.disabled)" ).click(function() {
    	$( "#create-page" ).dialog( "open" );
    });
	$(document).ready(function(){
		<?php $this->getJ('admin-lock.php', array('isAdminUser' => $isAdminUser)); ?>
	})
</script>