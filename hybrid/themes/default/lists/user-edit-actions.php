<?php
/*
 * Author:		cory frosch <cory.frosch@6qube.co>
 * License:

  Admin Menu
  Admin -> Manage

  Manage Sub-Menu
  Billing History | Billing Profile | Billing Subscription | User Profile | System Info | Settings | Branding

  Manage Sub-Menu - Items Not going in v1.0
  Products | Trash
 */
/*
  @todo don't think this code is needed any longer.
  #   $Controller = 'Admin';
  #   $displayBilling = true;
  #   if($this->isAction('billing') || $this->isAction('billing-profile') || $this->isAction('billing-subscription'))
  #       $displayBilling = true;
 */
$displayManageSubNav = false;

##  check action for sub-menu views
if (
        $this->isAction('dashboard') ||
        $this->isAction('profile') ||
        $this->isAction('systeminfo') ||
        $this->isAction('settings') ||
        $this->isAction('branding') ||
        $this->isAction('products') ||
        $this->isAction('billing') ||
        $this->isAction('billing-profile') ||
        $this->isAction('billing-subscription') ||
        $this->isAction('trash') ||
        $this->isAction('product-settings')
) {
    $displayManageSubNav = true;
}

##  build sub-menu to display
if ($User->can('view_admin')) {
    //commented out billing tabs for Admin until we get that finished.
    //if ($User->can('view_billing'))
    // $items[] = array('billing', 'icos-money2', 'Billing History', $this->action_url($Controller . '_billing?ID=' . $userobj->id));
    // if ($User->can('view_billing'))
    // $items[] = array('billing-profile', 'icos-money2', 'Billing Profile', $this->action_url($Controller . '_billing-profile?ID=' . $userobj->id));
    //if ($User->can('view_billing'))
    //$items[] = array('billing-subscription', 'icos-money2', 'Billing Subscription', $this->action_url($Controller . '_billing-subscription?ID=' . $userobj->id));
#   if($user->can('view_profile'))  @todo
    $items[] = array('profile', 'icos-user2', 'User Profile', $this->action_url($Controller . '_profile?ID=' . $userobj->id));
    if ($User->can('is_system'))
        $items[] = array('systeminfo', 'icos-windows', 'System Info', $this->action_url($Controller . '_systeminfo?ID=' . $userobj->id));
#   if($User->can('view_settings')) @todo
    $items[] = array('settings', 'icos-cog4', 'Settings', $this->action_url($Controller . '_settings?ID=' . $userobj->id));
#   if($User->can('view_branding')) @todo
    $items[] = array('branding', 'icos-bigbrush', 'Branding', $this->action_url($Controller . '_branding?ID=' . $userobj->id));
#   if($User->can('view_products'))
#       $items[] = array('products', 'icos-list', 'Products', $this->action_url($Controller . '_products?ID=' . $userobj->id));
#   if($User->can('view_trash'))
#       $items[] = array('trash', 'icos-trash', 'Trash', $this->action_url($Controller . '_trash?ID=' . $userobj->id));
}
?>
<!-- Display Sub-Menu -->
<ul class="<?= !$displayManageSubNav ? 'subNav' : 'hide'; ?>">
    <?php if($items) foreach($items as $item): ?>
    <li<?=($this->isAction($item[0]) ? ' class="activeli"' : '' )?>><a href="<?php echo $item[3]; ?>" title="View <?php echo $item[2]; ?>"><span class="titlename"><?php echo $item[2]; ?></span></a></li>
    <?php endforeach; ?>
</ul>