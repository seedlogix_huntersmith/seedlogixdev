<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

//var_dump($subusers);
?>

<?php foreach($subusers as $user): ?>
    <li class="result<?php
            if(!$user->access) echo ' noaccess';
            if($user->canceled) echo ' canceled';
            if($user->isreseller) echo ' reseller';
        ?>"><a href="<?php
        echo $this->action_url('UserManager_profile?ID=' . $user->id);
    ?>" title="<?php $this->p(@$user->title); ?>"><?php echo $this->p($user->username); ?></a>
        <a href="<?php echo $this->action_url('Admin_doImpersonate?ID=' . $user->id); ?>"><div class="fs1 iconb" data-icon=""></div></a></li>
<?php endforeach; ?>