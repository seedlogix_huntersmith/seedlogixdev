<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
if(!$User->can('view_trash'))
	return;
?>

<ul>
  <!-- Not going in version 1.0 release
    <li><a href="<?php
        echo $this->action_url('Campaign_trash'); ?>" title=""><i class="icos-trash"></i><span>Trash</span></a></li>
    -->
<!--  <li class="has">
        <a title="">
            <i class="icos-money3"></i>
            <span>Invoices</span>
            <span><img src="<?= $_path; ?>images/elements/control/hasddArrow.png" alt="" /></span>
        </a>
        <ul>
            <li><a href="#" title=""><span class="icos-add"></span>New invoice</a></li>
            <li><a href="#" title=""><span class="icos-archive"></span>History</a></li>
            <li><a href="#" title=""><span class="icos-printer"></span>Print invoices</a></li>
        </ul>
    </li>-->
</ul>