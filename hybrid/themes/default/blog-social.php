<?php
$canEdit        = !$User->can('edit_BLOG',$blog->getID()) ? ' disabled' : '';
$baseAuthURL    = 'https://login.seedlogix.com/api/';
//$baseAuthURL    = 'http://seedlogix.test/api/';
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <form action="<?= $this->action_url('Blog_save?ID='.$blog->id); ?>" method="POST" class="ajax-save">
        <div class="widget grid6">
            <div class="whead"><h6>Social Networking Connections</h6></div>
            <div class="formRow">
                <div class="grid3"><label>Facebook App ID:</label></div>
                <div class="grid9"><input type="text" value="<?php $this->p($blog->facebook_app_id); ?>" name="blog[facebook_app_id]"<?= $canEdit; ?>></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Facebook Publishing:</label></div>

<?php if($blog->facebook_id): ?>

                <div class="grid8"><input type="text" value="<?php $this->p($blog->facebook_id); ?>" name="blog[facebook_id]" disabled id="facebook"></div>
                <div class="grid1"><a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="facebook"><span class="delete"></span></a></div>

<?php else: ?>

                <div class="grid9"><a href="<?= $baseAuthURL; ?>facebook-blog-app.php?bloghttphost=<?php $this->p($blog->httphostkey); ?>" class="buttonS f_lt<?= $canEdit; ?> _blank"><span>Authorize Facebook</span></a></div>

<?php endif; ?>

            </div>
            <div class="formRow">
                <div class="grid3"><label>Twitter Publishing:</label></div>

<?php if($blog->twitter_accessid): ?>

                <div class="grid8"><input type="text" value="<?php $this->p($blog->twitter_accessid); ?>" name="blog[twitter_accessid]" disabled id="twitter"></div>
                <div class="grid1"><a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="twitter"><span class="delete"></span></a></div>

<?php else: ?>

                <div class="grid9"><a href="<?= $baseAuthURL; ?>twitter-blog-app.php?bloghttphost=<?php $this->p($blog->httphostkey); ?>" class="buttonS f_lt<?= $canEdit; ?> _blank"><span>Authorize Twitter</span></a></div>

<?php endif; ?>

            </div>
            <div class="formRow">
                <div class="grid3"><label>LinkedIn Access Token:</label></div>

<?php if($blog->linkedin_accessid): ?>

                <div class="grid8"><input type="text" value="<?php $this->p($blog->linkedin_accessid); ?>" name="blog[linkedin_accessid]" disabled id="linkedin"></div>
                <div class="grid1"><a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="linkedin"><span class="delete"></span></a></div>

<?php else: ?>

                <div class="grid9"><a href="<?= $baseAuthURL; ?>linkedin-blog-app.php?bloghttphost=<?php $this->p($blog->httphostkey); ?>" class="buttonS f_lt<?= $canEdit; ?> _blank"><span>Authorize LinkedIn</span></a></div>

<?php endif; ?>

            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

<?php $this->start_script(); ?>

$(function(){
    var allowDelete     = true;
    $('a.sixQube_remove').click(function(event){
        var targetName  = $(this).attr('role');
        if(allowDelete){
            $('#' + targetName).val('').trigger('change');
        } else{
            notie.alert({text:'Contact your system administrator to unauthorize ' + targetName + '.'});
            event.preventDefault();
            return false;
        }
    });
});

<?php $this->end_script(); ?>

</script>