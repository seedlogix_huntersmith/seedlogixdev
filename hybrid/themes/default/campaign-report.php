<?php
/**
 * @var $this Template
 * @var $T_Report ReportModel
 */
?>

<div class="fluid">
    <div class="widget grid12">
        <div class="whead"><h6>Report: <?php $T_Report->html('name'); ?> (<?= ucfirst(strtolower($T_Report->type)); ?>)</h6></div>
        <div class="body">
            <div class="formRow">
                <div class="grid1"><label>Filter Report:</label></div>
                <div class="grid4">

<?php
$this->getWidget('select-ajax.php',array(
    'id'            => 'websites',
    'name'          => 'website',
    'ajaxUrl'       => $this->action_url('Campaign_ajaxReportsWebsites',array('report_ID' => $T_Report->getID())),
    'field_value'   => "filter",
    'text'          => ":website:"
));
?>

                </div>

<?php if($T_Report->isCustom()): ?>

                    <div class="grid7 m_lt">
                        <form action="<?= $this->action_url('Campaign_save'); ?>" id="dateRange">
                            <div class="grid2"><label>Custom Range:</label></div>
                            <div class="grid5"><input name="report[start_date]" type="text" id="fromDate" placeholder="min Date" value="<?= date('m/d/Y',strtotime($T_Report->start_date)); ?>"></div>
                            <div class="grid5 m_lt"><input name="report[end_date]" type="text" id="toDate" placeholder="max Date" value="<?= date('m/d/Y',strtotime($T_Report->end_date)); ?>"></div>
                            <input type="hidden" name="report[ID]" value="<?= $T_Report->getID(); ?>">
                        </form>
                    </div>

<?php endif; ?>

            </div>
        </div>
    </div>
</div>
<div class="fluid">
    <div class="widget grid12 chartWrapper">
        <div class="whead"><h6>Visits &amp; Leads</h6></div>
        <div class="body"><div class="bars" id="placeholder1"></div></div>
    </div>
</div>
<div class="fluid"> 
	<div class="widget">
		<div class="whead"><h6>Stats</h6></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tDefault" id="jreports">
            <thead>
                <tr>
                    <td>Source</td>
                    <td>Visits</td>
                    <td>Leads</td>
                    <td>Conversion</td>
                    <td>Bounce</td>
                </tr>
            </thead>
            <tbody class="jresponders">

<?php $this->getRow('source-reports.php'); ?>

            </tbody>
            <tfoot class="fancyFooter">
                <tr>
                    <td>TOTALS</td>
                    <td><strong class="bDefault f_cr j-totalVisits"><?= $piwikStats->tableStats->totalVisits; ?></strong></td>
                    <td><strong class="bDefault f_cr j-totalLeads"><?= $piwikStats->tableStats->totalLeads; ?></strong></td>
                    <td><strong class="bDefault f_cr j-totalRate"><?= $piwikStats->tableStats->totalRate; ?></strong></td>
                    <td><strong class="bDefault f_cr j-totalBounceRate"><?= $piwikStats->tableStats->totalBounceRate; ?></strong></td>
                </tr>
            </tfoot>
        </table>
	</div>
</div>

<script type="text/javascript">

<?php $this->start_script(); ?>

$(function(){
    var previousPoint;

    function ReloadChart(newdata){
        $.each(newdata.tableStats,function(selector,val){
            $('.j-' + selector).html(val);
        });

        var d1      = [];
        var d2      = [];

        $.each(newdata.chartStats.Visits,function(index,AV){
            d1.push([AV.timestamp,AV.visits]);
        });
        $.each(newdata.chartStats.Leads,function(index,AV){
            d2.push([AV.timestamp,AV.visits]);
        });

        var ds      = new Array();

        ds.push({
            data    : d1,
            label   : 'Visits'
        });
        ds.push({
            data    : d2,
            label   : 'Leads'
        });

        $.plot($('#placeholder1'),ds,{
            colors  : ['#C57FE5','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b'],
            series  : {
                lines   : {
                    show    : true,
                    fill    : true
                },
                points  : {
                    show    : true,
                    fill    : true
                }
            },
            grid    : {
                tickColor   : '#d4e9f4',
                clickable   : false,
                hoverable   : true
            },
            xaxis   : {
                show        : true,
                mode        : 'time',
                tickSize    : [newdata.chartOptions.day_size,newdata.chartOptions.tick_type],
                timeformat  : newdata.chartOptions.timeformat
            }
        });
    }

    var d1      = [<?php foreach($piwikStats->chartStats->Visits as $stat){echo "[$stat->timestamp,$stat->visits],";} ?>];
    var d2      = [<?php foreach($piwikStats->chartStats->Leads as $stat){echo "[$stat->timestamp,$stat->visits],";} ?>];
    var ds      = new Array();

    ds.push({
        data    : d1,
        label   : 'Visits'
    });
    ds.push({
        data    : d2,
        label   : 'Leads'
    });

    $.plot($('#placeholder1'),ds,{
        colors  : ['#C57FE5','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b'],
        series  : {
            lines   : {
                show    : true,
                fill    : true
            },
            points  : {
                show    : true,
                fill    : true
            }
        },
        grid    : {
            tickColor   : '#d4e9f4',
            clickable   : false,
            hoverable   : true
        },
        xaxis   : {
            show        : true,
            mode        : 'time',
            tickSize    : [<?= "{$piwikStats->chartOptions['day_size']},'{$piwikStats->chartOptions['tick_type']}'"; ?>],
            timeformat  : <?= $piwikStats->chartOptions['timeformat']; ?>
        }
    });

    function showTooltip(x,y,contents){
        var _html   = '<div id="tooltip2" class="tooltip">' + contents + '</div>';
        $(_html).appendTo('body');
        var _s      = $('div#tooltip2.tooltip');
        var _ow     = _s.outerWidth() / 2;
        _s.css({
            top     : y - 42,
            left    : x - _ow
        }).fadeIn(300);
    }

    $('#placeholder1').bind('plothover',function(event,pos,item){
        if($('#placeholder1').length){
            if(item){
                if(previousPoint != item.datapoint){
                    previousPoint = item.datapoint;
                    $('div#tooltip2.tooltip').fadeOut(300,function(){
                        $(this).remove();
                    });
                    var D = new Date(item.series.data[item.dataIndex][0]);
                    var M = D.getUTCMonth();
                    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                    var x = '';
                    var y = item.datapoint[1] + ', ' + item.series.label;
                    if(item.series.xaxis.tickSize[0] != 365 && item.series.xaxis.tickSize[1] == 'day'){
                        x = months[M] + ' ' + ('0' + D.getUTCDate()).slice(-2);
                    } else{
                        x = item.series.xaxis.tickSize[0] != 365 ? months[M] + ' ' + D.getUTCFullYear() : D.getUTCFullYear() - 1;
                    }
                    showTooltip(item.pageX,item.pageY,x + ': <span>' + y + '</span>');
                }
            } else{
                $('div#tooltip2.tooltip').fadeOut(300,function(){
                    $(this).remove();
                });
                previousPoint = null;
            }
        }
    });

    function ajaxReload(){
        var pdata   = {filter:$('#websites').val()};
        var newdata = $.ajax({
                        url         : '<?= $this->action_url('Campaign_ajaxchartreport',array('report_ID' => $T_Report->ID)); ?>',
                        type        : 'POST',
                        data        : pdata,
                        dataType    : 'json'
                    }).done(ReloadChart);
    }

    $('#websites').change(ajaxReload);
    $('#dateRange').on('change','input',function(e){
        new AjaxSaveDataRequest(e.currentTarget,undefined,ajaxReload);
    });
});

<?php $this->end_script(); ?>

</script>