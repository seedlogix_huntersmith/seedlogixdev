<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Social Accounts</h6>
			<div class="buttonS f_rt jopendialog jopen-addAccount"><span>Add a New Social Account</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('SocialAccounts'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'jSocialAccounts',
	'data'				=> $SocialAccounts,
	'rowkey'			=> 'SocialAccount',
	'rowtemplate'		=> 'social-accounts.php',
	'total'				=> $total_socialaccounts_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxsocialaccounts',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<?php $this->getDialog('add-social-account.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {

		//===== Create New Form + PopUp =====//
		$('#jdialog-addAccount').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Add',
				function(ajax, status)
				{
					if (!ajax.error)
					{
						$('.jnoforms').hide();
						$('.jforms').prepend(ajax.row);
						$('#jSocialAccounts').dataTable().fnDraw();
						notie.alert({text:ajax.message});
						this.close();
						this.form[0].reset();
					}
					else if (ajax.error == 'validation')
					{
						alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
						alert(ajax.message);
	
				}, '#jdialog-addAccount')).buttons
		});

	});

<?php $this->end_script(); ?>
</script>