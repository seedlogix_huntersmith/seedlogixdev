<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Products</h6>
			<div class="mr45">
				<div class="buttonS bGreen right dynRight jopendialog jopen-createproduct">Create Product</div>
				<div class="clear"></div>
			</div>
		</div>
		<div id="dyn">
			<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jproducts">
				<thead>
				  <tr>
					<td style="text-align:left">Name</td>
					<td width="100">Actions</td>
				  </tr>
				</thead>
				<tbody class="jproducts">
					
<?              
			$this->getRow('admin-products.php');
?>
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<?php //$this->getDialog('create-contact.php'); ?>


<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		
		//===== Create New Form + PopUp =====//
		$( '#jdialog-createproduct' ).dialog(
		{
			autoOpen: false,
			height: 200,
			width: 550,
			modal: true,
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create Product',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
	          $('.jnoforms').hide();
	          $('.jproducts').prepend(ajax.row);
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createproduct')).buttons
		});
	
	});
    
<?php $this->end_script(); ?>
</script>