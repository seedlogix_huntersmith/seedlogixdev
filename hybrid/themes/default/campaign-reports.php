<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Reports</h6>
			<div class="buttonS f_rt jopendialog jopen-createReport"><span>Create a New Report</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('CampaignReports'),
	'class'				=> 'col_2nd,col_end',
	'id'				=> 'reports-datatable',
	'data'				=> $reports,
	'rowkey'			=> 'report',
	'rowtemplate'		=> 'campaign-reports-row.php',
	'total'				=> $totalreports,
	'sourceurl'			=> $this->action_url('Campaign_ajaxReports',!empty($Campaign) ? array('ID' => $Campaign->getID()) : array()),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Reports POPUP -->
<?php $this->getDialog('create-report.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		// Confirm delete already being handled with jQuery Dialog
		/*
		var dataTable = $('.dataTable');
		var handler = dataTable.data('events').click[0].handler;
		dataTable.unbind('click');
		dataTable.on('click', '.jajax-action', function(e){
			if(confirm('Are you sure?'))
				handler(e);
			else
				return false;
		});
		*/

        $('#sq-listingType').change(function(){
            if( $(this).val() == 'CUSTOM' )
                $('#sq-listingTypeRange').show();
            else
                $('#sq-listingTypeRange').hide();
        });
        
		//===== Create New Form + PopUp =====//
		$('#jdialog-createReport').dialog(
			{
				autoOpen: false,
				height: 300,
				width: 600,
				modal: true,
				open: function(e){
					$.uniform.update();
				},
				close: function () {
					$('#campaign_id',this).val('');
					$('form',this)[0].reset();
					$('#reportID',this).val('0');
					$(this).dialog('option','title','Create a New Report');
					$('.ui-dialog-buttonpane button:contains(Edit)').text('Create');
				},
				buttons: (new CreateDialog('Create',
					function (ajax, status) {

						if (!ajax.error) {
							var dt = $('#reports-datatable');
							var newRowsIds = dt.data('newRowsIds');
							newRowsIds.unshift(ajax.object.ID);
							dt.data('newRowsIds', newRowsIds);
							dt.dataTable().fnDraw();
							notie.alert({text:ajax.message});
							this.close();
							this.form[0].reset();
						}
						else if (ajax.error == 'validation') {
							alert(getValidationMessages(ajax.validation).join('\n'));
						}
						else
							alert(ajax.message);

					}, '#jdialog-createReport')).buttons
			});

		$('.dTable').on('click','.editReport',function(e){
			e.preventDefault();
			var row = $(this).closest('tr');
			var id = row.find('.id').text();
			var jdialog = $('#jdialog-createReport');
			var input = jdialog.find('input[name="report[ID]"]');
			jdialog.find('input[name="report[name]"]').val(row.find('.name').text());
			jdialog.find('select[name="report[type]"]').val(row.find('.type').text());
			jdialog.find('select[name="report[touchpoint_type]"]').val(row.find('.touchpoint_type').text());
			input.val(id);
			jdialog.dialog('open').dialog('option','title','Edit the Report');
			$('.ui-dialog-buttonpane button:contains(Create)').text('Edit');
		});

		$( ".emailerdatepicker" ).datepicker({ dateFormat: "mm/dd/yy", minDate: 0 });

	});


    
<?php $this->end_script(); ?>
</script>