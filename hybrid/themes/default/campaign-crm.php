
<div class="widget grid6 rightTabs">       
    <ul class="tabs">
        <li><a href="#tabb3">SugarCRM</a></li>
        <li><a href="#tabb4">Salesforce.com</a></li>
    </ul>
    <div class="tab_container">
        <div id="tabb3" class="tab_content">        
            <div class="formRow">
                <div class="grid3"><label>SugarCRM Username:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>SugarCRM Password:</label></div>
                <div class="grid9"><input type="password" name="pass"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>SugarCRM Install Path:</label></div>
                <div class="grid9"><input type="text" placeholder="Bazinga" name="placeholder"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>SugarCRM Default Assigned To:</label></div>
                <div class="grid9"><input type="text" placeholder="Bazinga" name="placeholder"></div>
            </div>
            <div class="formRow noBorderB">
                <div class="grid3"><label>SugarCRM NuSOAP Files Absolute Path:</label></div>
                <div class="grid9"><input type="text" value="read only" readonly="readonly" name="readonly"></div>
            </div>
        </div>
        <div id="tabb4" class="tab_content hide">     
            <div class="formRow">
                <div class="grid3"><label>Salesforce.com Username:</label></div>
                <div class="grid9"><input type="text" name="regular"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Salesforce.com Password:</label></div>
                <div class="grid9"><input type="password" name="pass"></div>
            </div>
            <div class="formRow noBorderB">
                <div class="grid3"><label>Salesforce.com Token:</label></div>
                <div class="grid9"><input type="text" placeholder="Bazinga" name="placeholder"></div>
            </div></div>
    </div>	
</div>
