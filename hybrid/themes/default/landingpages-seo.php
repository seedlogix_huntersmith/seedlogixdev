<!-- LEFT SIDE NAV -->

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

		<div class="fluid">
		
		
                        <form action="<?php echo $this->action_url('Website_save?ID=' . $site->id); ?>" method="POST" class="ajax-save">
		<div class="widget grid6">       
                    <ul class="tabs">
                        <li class="activeTab"><a href="#tabb3">Meta Configuration</a></li>
                        <li class=""><a href="#tabb4">Tag Configuration</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb3" style="display: block;">
                        <!-- meta config tab -->
                        <?php // var_dump($site); ?>
                        <div class="formRow">
                        	<div class="grid3"><label>Site Title:</label></div>
                        	<div class="grid9"><input type="text" name="website[site_title]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Meta Keywords:</label></div>
                        	<div class="grid9"><input type="text" id="meta_keywords" name="website[meta_keywords]" class="tags" value="<?php  ?>" /></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Meta Description:</label></div>
                        	<div class="grid9"><input type="text" name="website[description]" value="<?php  ?>"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow no-border">
                        	<div class="grid3"><label>Core Keywords:</label></div>
                        	<div class="grid9"><input type="text" id="keywords" name="website[keywords]" class="keywords-website" value="<?php  ?>" /></div>
                        	<div class="clear"></div>
                    	</div>
                    	<!-- //end meta config tab -->
                        </div>
                        <div class="tab_content" id="tabb4" style="display: none;">
                        <!-- tag config tab -->
                       	<p>If you plan on replicating your efforts for SEO, simply optimize your site with these tags and it will pull data from the fields in your HUB setttings automatically.</p>

      <p><strong>((city))</strong> - City (Austin Landscaping)<br />
      <strong>((state))</strong> - State (TX Landscaping Services)<br />
      <strong>((address))</strong> - Address<br />
      <strong>((zip))</strong> - Zip Code<br />
      <strong>((phone))</strong> - Phone<br />
      <strong>((domain))</strong> - Domain<br />
      <strong>((keyword1))</strong> - Keyword 1<br />
      <strong>((keyword2))</strong> - Keyword 2<br />
      <strong>((keyword3))</strong> - Keyword 3<br />
      <strong>((keyword4))</strong> - Keyword 4<br />
      <strong>((keyword5))</strong> - Keyword 5<br />
      <strong>((keyword6))</strong> - Keyword 6</p>
                        <!-- end tag config tab -->
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
		
	
		
		
                    
                    <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Technical Settings</h6><div class="clear"></div></div>
                        
                        <div class="formRow">
                            <div class="grid3"><label>WWW Options:</label></div>
                            <div class="grid9 noSearch">
                            <select name="select2" class="select">
                                <option value="opt1">Usual select box with dropdown styling</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="formRow">
                            <div class="grid3"><label>Build Site Map:</label></div>
                            <div class="grid9 on_off noPad">
                                <div class="floatL mr10"><input type="checkbox" id="check20" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                    	<div class="formRow">
                        	<div class="grid12"><label>Google Website Tools:</label></div>
                        	<div class="grid9">
                        		<form><textarea id="code" name="code">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                        	<div class="grid12"><label>Additional Tracking Code:</label></div>
                        	<div class="grid9">
                        		<form><textarea id="code2" name="code2">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                        	<div class="grid12"><label>6Qube Backlink:</label></div>
                        	<div class="grid9">
                        		<form><textarea id="code3" name="code3">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
                        </form>
         </div> <!-- //end fluid -->


<!-- //end content container -->
</div>
<!-- //end content container -->

<script type="text/javascript">
	
	//===== Tabs =====//
	$.fn.contentTabs = function(){ 
	
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};
	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	
	//===== iButtons =====//
	$('.on_off :checkbox, .on_off :radio').iButton({
		labelOn: "",
		labelOff: "",
		enableDrag: false 
	});
		
	
	//===== Chosen plugin =====//
	$(".select").chosen();
	
	
	//===== Style Form Elements =====//
	$("select").uniform();
	
	
	//===== Code Mirror / Syntax highlight =====//
	var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true
      });
    
    var editor2 = CodeMirror.fromTextArea(document.getElementById("code2"), {
        lineNumbers: true
      });
      
    var editor3 = CodeMirror.fromTextArea(document.getElementById("code3"), {
        lineNumbers: true
      });
			
</script>
