<?php
/**
 * @var $this Template
 */

$isAdminUser	= $this->isMU;
$controller		= ucfirst(mb_strtolower($site->touchpoint_type));
$gridLength		= 'grid9';

if($isAdminUser){
	$gridLength	= 'grid8';
}

$disable		= $User->can("edit_{$site->getTouchpointType()}",$site->getID()) ? "" : "disabled";
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php',array('isAdminUser' => $isAdminUser)); ?>

<div class="grid12 m_tp">
	<div class="widget">
	<div class="whead">
		<h6>Pages</h6>
		<form action="<?= $this->action_url('Campaign_save'); ?>">
			<input type="hidden" value="<?= $site->getID(); ?>" name="website[id]">
			<input type="hidden" value="<?= $site->touchpoint_type; ?>" name="website[touchpoint_type]">
<?php
$this->getWidget('admin-lock.php',array(
	'fieldname'		=> 'allow_pages',
	'isAdminUser'	=> $isAdminUser
	));
?>
		</form>
<?php
$this->getWidget('button-website.php',array(
	'id'			=> 'createNav',
	'fieldname'		=> 'allow_pages',
	'label'			=> 'Create a New Nav Section'
));
?>
<?php
$this->getWidget('button-website.php',array(
	'id'			=> 'createPage',
	'fieldname'		=> 'allow_pages',
	'label'			=> 'Create a New Page',
	'class'			=> ' jopendialog jopen-createpage'
));
?>

	</div>
<?php if(!$Nodes): ?>
    <ul class="updates center">
		<li>No Stats Available.</li>
	</ul>
<?php else: ?>
	<form id="sortingForm" action="<?= $this->action_url('Website_sortpages?ID='.$site->id); ?>" method="POST" class="ajax-save">
		<input id="page_table_input" type="hidden" name="website[table]" value="0">
		<table class="tDefault col_1st col_end2" style="width: 100%;">
			<thead>
				<tr>
					<td>Title</td>
					<td>Type</td>
					<td>Nav Root</td>
					<td>Creation Date</td>
<?php if(empty($disable)): ?>
					<td>Actions</td>
<?php endif; ?>
				</tr>
			</thead>
			<tbody>
<?php
foreach($Nodes as $k => $Node){
	$this->getRow('nav-row.php',array(
		'NextNode'		=> isset($Nodes[$k+1]) ? $Nodes[$k+1] : NULL,
		'controller'	=> $controller,
		'Node'			=> $Node,
		'level'			=> 0,
		'k'				=> $k,
		'siblings'		=> count($Nodes)-1,
		'disabled'		=> !empty($disable)
	));
}
?>
			</tbody>
		</table>
	</form>
<?php endif; ?>
</div>
</div>
</div>
<?php if(empty($disable)): ?>
<?php $this->getDialog('create-page.php'); ?>
<?php $this->getDialog('create-navsection.php'); ?>
<?php endif; ?>

<script type="text/javascript">
<?php $this->start_script(); ?>
$(document).ready(function(){

	// Initialize Session variable of Validation Nav in Website

	// validNav, delNav
<?php if(isset($validNav)): ?>
	notie.alert({text:getValidationMessages(<?= json_encode($validNav); ?>)});
<?php elseif(isset($verifyNav)): ?>
	notie.alert({text:'<?= $verifyNav; ?>'});
<?php elseif(isset($delNav)): ?>
	notie.alert({text:'<?= $delNav; ?>'});
<?php endif; ?>

	// validCP, delPage
<?php if(isset($validCP)): ?>
	notie.alert({text:getValidationMessages(<?= json_encode($validCP); ?>)});
<?php elseif(isset($verifyCP)): ?>
	notie.alert({text:'<?= $verifyCP; ?>'});
<?php elseif(isset($delPage)): ?>
	notie.alert({text:'<?= $delPage; ?>'});
<?php endif; ?>

<?php $this->getJ('admin-lock.php',array('isAdminUser' => $isAdminUser)); ?>

	//===== Sortable Table Columns =====//
	// Return a helper with preserved width of cells
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	var saveNewOrder = function(e, ui)
    {
        var ids = $(this).sortable('toArray').toString();
        var input = document.getElementById('page_table_input');
        
        console.dir(ids);
        input.value = ids;
        
        $('#sortingForm').submit();
	}

	//===== Dynamic data table =====//
	
	oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"bSort": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});

<?php if(empty($disable)): ?>
	$('#jdialog-createpage').dialog({
		autoOpen	: false,
		height		: 200,
		width		: 400,
		modal		: true,
		buttons		:{
			Cancel: function(){
				$(this).dialog('close');
			},
			'Create': function(){
				$('#jcreatepage-form').submit();
			}
		},
		buttonsf: (new CreateDialog("Create", 
                function (ajax, status){
                    // standards here will be 'error' var, possible values are : 'validation', 'other'
                    //  if error = 'validation', check 'validation' object,
                    // if error = 'other' then print 'message''
                    // if no error or error = false, check result object. (could be a partially 
                    if(ajax.error)
                    {
			
                    }else{
                        //var arr =   [ajax.result.name, 0, 0, "0 %", 0, new Date(), "other"];
                        //new InsertObjectAtTable('#campaigns-stats-tbl', arr, ajax.result.actions.dashboard);
                        $('#jdialog-createpage').dialog('close');
                    }

                })).buttons
    });
    
    $( "#create-nav" ).dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true,
            buttons: {
				Cancel: function(){
					$(this).dialog('close');
				},
                'Create': function(){
                    $('#jcreatenav-form').submit();
                }
            },
            buttonsf: (new CreateDialog("Create",
                function (ajax, status){
                    // standards here will be 'error' var, possible values are : 'validation', 'other'
                    //  if error = 'validation', check 'validation' object,
                    // if error = 'other' then print 'message''
                    // if no error or error = false, check result object. (could be a partially

                    if(ajax.error)
                    {

                    }else{
                        //var arr =   [ajax.result.name, 0, 0, "0 %", 0, new Date(), "other"];
                        //new InsertObjectAtTable('#campaigns-stats-tbl', arr, ajax.result.actions.dashboard);
                        $('#create-nav').dialog('close');                        
                    }

                })).buttons
    });
    
    $( "#createNav" ).click(function() {
    	$( "#create-nav" ).dialog( "open" );
    });
    
    $( "#createPage" ).click(function() {
    	$( "#create-page" ).dialog( "open" );
    });
	<?php endif; ?>
});
<?php $this->end_script();  ?>
</script>