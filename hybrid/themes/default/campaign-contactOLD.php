<?php
	// test data
	$Info = array(
		"First Name" => "Test",
		"Last Name" => "Testers",
		"Email" => "test@gmail.com",
		"State" => "TX"
	);
	$states = Defaults::getStates();
// @amado input fields need the proper "name=" values
	
	$formResponse = true;
	$contactInfoGrid = 'grid6';

	if(!$formResponse)
		$contactInfoGrid = 'grid12';
?>

<form action="<?= $this->action_url('Campaign_save'); ?>" class="ajax-save">
	<input type="hidden" value="<?= $contact->getID(); ?>" name="contact[id]">

	<div class="fluid">

		<!-- section -->
		<div class="widget grid12">
			<div class="whead">
				<h6 class="capitalize"><span><?= $contact->html('first_name'); ?></span> <span><?= $contact->html('last_name'); ?></span></h6>
				<a class="buttonS f_rt editFields"><span>Edit the Contact</span></a>
			</div>
			<div class="body">
				<div class="grid2">
					<!-- Prospect Photo -->
					<div class="prospectPhoto"><img src="themes/default/images/userLogin.png" alt=""></div>
				</div>
				<div class="grid10">
					<div class="grid6">
						<div class="formRow hide">
							<div class="grid3"><label>First Name:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('first_name'); ?></span>
								<input type="text" class="edit" name="contact[first_name]" value="<?= $contact->html('first_name'); ?>">
							</div>
						</div>
						<div class="formRow">
							<div class="grid3"><label>Title:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('title'); ?></span>
								<input type="text" class="edit" name="contact[title]" value="<?= $contact->html('title'); ?>">
							</div>
						</div>
						<div class="formRow">
							<div class="grid3"><label>Company Name:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('company'); ?></span>
								<input type="text" class="edit" name="contact[company]" value="<?= $contact->html('company'); ?>">
							</div>
						</div>
					</div>
					<div class="grid6">
						<div class="formRow hide">
							<div class="grid3"><label>Last Name:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('last_name'); ?></span>
								<input type="text" class="edit" name="contact[last_name]" value="<?= $contact->html('last_name'); ?>">
							</div>
						</div>
						<div class="formRow">
							<div class="grid3"><label>Phone:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('phone'); ?></span>
								<input type="text" class="edit" name="contact[phone]" value="<?= $contact->html('phone'); ?>">
							</div>
						</div>
						<div class="formRow">
							<div class="grid3"><label>Email:</label></div>
							<div class="grid9 editSwitcher">
								<span class="editValue"><?= $contact->html('email'); ?></span>
								<input type="text" class="edit" name="contact[email]" value="<?= $contact->html('email'); ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- //section -->

	</div><!-- //end fluid -->
	<div class="fluid">

		<!-- section -->
		<div class="widget <?= $contactInfoGrid; ?>">
			<div class="whead"><h6>Lead Information</h6></div>
			<!-- meta config tab -->
			<div class="formRow">
				<div class="grid3"><label>Address:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $contact->html('address'); ?></span>
					<input type="text" class="edit" name="contact[address]" value="<?= $contact->html('address'); ?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Address 2:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $contact->html('address2'); ?></span>
					<input type="text" class="edit" name="contact[address2]" value="<?= $contact->html('address2'); ?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>City:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $contact->html('city'); ?></span>
					<input type="text" class="edit" name="contact[city]" value="<?= $contact->html('city'); ?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>State:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $states[$contact->state]; ?></span>
					<div class="edit" class="hide">

<?php
$this->getWidget('states-dropdown.php',array(
	'name'			=> 'contact[state]',
	'value'			=> $contact->state,
	'disable'		=> $disable,
	'hidden'		=> true
));
?>

					</div>
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Zip Code:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $contact->html('zip'); ?></span>
					<input type="text" class="edit" name="contact[zip]" value="<?= $contact->html('zip'); ?>">
				</div>
			</div>
			<div class="formRow">
				<div class="grid3"><label>Website:</label></div>
				<div class="grid9 editSwitcher">
					<span class="editValue"><?= $contact->html('website'); ?></span>
					<input type="text" class="edit" name="contact[website]" value="<?= $contact->html('website'); ?>">
				</div>
			</div>
			<!-- //end meta config tab -->
		</div><!-- //section -->

<?php if($contact->form_response): ?>

		<!-- section -->
		<div class="widget grid6">
			<div class="whead"><h6>Form Response</h6></div>

<?php foreach($contact->form_response as $label => $value): ?>

				<div class="formRow">
					<div class="grid6"><label><?= htmlentities($label); ?></label></div>
					<div class="grid6"><?= htmlentities($value); ?></div>
				</div>

<?php endforeach; ?>

		</div><!-- //section -->

<?php endif; ?>

	</div><!-- //end fluid -->
	<div class="fluid">

		<!-- section -->
		<div class="widget grid12">
			<div class="whead">
				<h6>Notes</h6>
				<a class="buttonS f_rt jopendialog jopen-createcontactnote"><span>Create a New Note</span></a>
			</div>
			<div class="body"><div class="grid12"><div id="timeline"></div></div></div>
		</div><!-- //section -->

	</div><!-- //end fluid -->

</form>

<!-- Create Form POPUP -->
<?php $this->getDialog('create-contactnote.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(document).ready(function() {
	    //Use JSON data to Fill out the timeline
	    var timeline_data = [
			<?php
			/** @var NoteModel $note */
			foreach($notes as $note):?>
	        	<?= json_encode($note->getTimeLineNote($this->action_url('campaign_delete',array('note_ID' => $note->id)))).','; ?>
			<?php endforeach;?>

	    ];

	    var timeline = new Timeline($('#timeline'), timeline_data);
		window.TimeLineOptions = {
			animation: false,
			separator: 'month_year'
		};
		timeline.setOptions(TimeLineOptions);
		timeline.display();

		window.timeline = timeline;

		$('.editSwitcher > input').change(function(e){
			var input = $(this);
			var iname = input.attr('name');
			var divSwitcher = input.closest('.editSwitcher');
			var span = divSwitcher.find('span').text(input.val());
			if(iname == 'contact[first_name]'){
				$('h6.capitalize > span:first-child').text(input.val());
			} else if(iname == 'contact[last_name]'){
				$('h6.capitalize > span:last-child').text(input.val());
			}
		});

		$('.editSwitcher select').change(function(e){
			var input = $(this);
			var divSwitcher = input.closest('.editSwitcher');
			var value = divSwitcher.find('a span').text();
			var span = divSwitcher.find('span').text(value);
		});
		// Edit View
		$('a.editFields').click(function(){
			var _edit		= '<span>Edit the Contact</span>';
			var _update		= '<span>Update the Contact</span>';
			var _label		= $(this).html() == _edit ? _update : _edit;
			$(this).html(_label);
			$('.formRow.hide').toggle();
			$('.editSwitcher > .editValue').toggle();
			$('.editSwitcher > input.edit,.editSwitcher > div.edit').toggle();
			return false;
		});

		//===== Create New Form + PopUp =====//
		$('#jdialog-createcontactnote').dialog(
		{
			autoOpen: false,
			height: 300,
			width: 600,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var data = ajax.notes;
				var newTimeline = $('#timeline').empty();
				window.timeline = new Timeline(newTimeline, data);
				window.timeline.setOptions(TimeLineOptions);
				window.timeline.display();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
						notie.alert({text:getValidationMessages(ajax.validation).join('\n')});
					}
					else
	          alert(ajax.message);

				}, '#jdialog-createcontactnote')).buttons
		});

		$('#timeline').on('click','a.jajax-action',function(e){
			e.preventDefault();
			var t = $(e.target).parent();
			sendAjax(t.attr('href'),function(ajax){
				if(!ajax.error){
					timeline._deleteElement(t.closest('.timeline_element'));
					notie.alert({text:ajax.message});
				}else{
					notie.alert({text:'An error occurred.'});
				}
			});
		})

	});

<?php $this->end_script(); ?>
</script>