<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Email Broadcasts</h6>
			<div class="buttonS f_rt jopendialog jopen-createmailer"><span>Create a New Email Broadcast</span></div>
		</div>

<?php $this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Broadcast'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'jmailers',
	'data'				=> $broadcasters,
	'rowkey'			=> 'broadcaster',
	'rowtemplate'		=> 'campaign-broadcast.php',
	'total'				=> $total_broadcasters,
	'sourceurl'			=> $this->action_url('Campaign_ajaxBroadcasters',array('ID' => $Campaign->getID())),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Emailer POPUP -->
<?php $this->getDialog('create-mailer.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>
function saveAjax(ajax, status) {

	if (!ajax.error) {
		notie.alert({text:ajax.message});
		var dt = $('#jmailers');
		var newRowsIds = dt.data('newRowsIds');
		newRowsIds.unshift(ajax.object.id);
		dt.data('newRowsIds', newRowsIds);
		dt.dataTable().fnDraw(false);
		this.close();
		this.form[0].reset();
	}
	else if (ajax.error == 'validation') {
		alert(getValidationMessages(ajax.validation).join('\n'));
	}
	else
		alert(ajax.message);

}
	$(function (){
		
		//===== Create New Form + PopUp =====//
		$('#jdialog-createmailer').dialog(
			{
				autoOpen: false,
				height: 200,
				width: 400,
				modal: true,
				close: function () {
					$('form', this)[0].reset();
				},
				buttons: (new CreateDialog('Create',
					saveAjax, '#jdialog-createmailer')).buttons
			});

		$('.dTable').on('click','a[title="Click to Toggle"]',function(e){
			e.preventDefault();
			var _el		= $(this);
			var _txt	= _el.text();
			var _href	= _el.attr('href');
			var _bool	= _txt == 'No' ? 1 : 0;
			var _label	= _bool == 1 ? 'Yes' : 'No';
			var _id		= $(this).closest('tr').find('input[name="broadcast[id]"]').val();
			var _name	= $(this).closest('tr').find('input[name="broadcast[name]"]').val();
			$.ajax(_href,{
				dataType		: 'json',
				type			: 'POST',
				data			: {
					broadcast	: {id:_id,active:_bool,name:_name}
				},
				success			: saveAjax
			});
			_el.text(_label);
			return false;
		});
	
	});
    
<?php $this->end_script(); ?>
</script>