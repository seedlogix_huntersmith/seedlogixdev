<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

?>


<a href="<?= $this->action_url('Campaign_phone?ID=' . $Campaign->getID(),
            array('do' => 'rec',
                    'mid' => $mid)); ?>">Recordings</a>
<a href="<?= $this->action_url('Campaign_phone?ID=' . $Campaign->getID(),
            array('mid' => $mid)); ?>">Services</a>
<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Call Recordings</h6>
			<div class="mr45">
			</div>
		</div>
		<div id="dyn">
<?php
                        $this->getWidget('datatable.php',
                            array('columns' => array('Date', 'Status', 'From', 'Length', 'Link'),
                                'class' => '',
                                'id' => 'jPhoneCalls',
                                'data' => $Calls,
                                'rowkey' => 'Call',
                                'rowtemplate' => 'phonerecording.php',
                                'total' => $info->TotalResults,
                                'sourceurl' => '?'
                                )
                        );
?>

		</div>
	</div>
</div>