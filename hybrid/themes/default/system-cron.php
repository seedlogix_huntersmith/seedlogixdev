<?php
//    var_dump($events);
?>

<div class="fluid">
    <div class="widget">
        <div class="whead"><h6>System Events</h6></div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('CronEvents'),
	'class'				=> '',
	'id'				=> 'jCron',
	'data'				=> $cron,
	'rowkey'			=> 'event',
	'rowtemplate'		=> 'system-cron-row.php',
	'total'				=> 40,
	'sourceurl'			=> '',
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

    </div>
</div>