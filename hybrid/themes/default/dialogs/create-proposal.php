<div class="SQmodalWindowContainer dialog" title="Create a New Proposal" id="jdialog-createproposal" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<?php if($is_contact){?>
		
		<input type="hidden" name="proposal[cid]" id="campaign_id" value="<?=$contact->cid?>">
		<input type="hidden" name="proposal[account_id]" id="account_id" value="<?=$contact->account_id?>">
		<input type="hidden" name="proposal[contact_id]" id="contact_id" value="<?=$contact->id?>">
		<input type="hidden" name="proposal[status]" id="active" value="Active">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="proposal[name]" id="proposal_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Expired Date:</label></div>
				<div class="grid8"><input id="expire_date" type="text" name="proposal[expire_date]" class="expiredatepicker" value=""></div>
			</div>
		
		<?php } else { ?>
		<input type="hidden" name="proposal[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<input type="hidden" name="proposal[account_id]" id="account_id" value="<?= $_GET['aid']; ?>">
		<input type="hidden" name="proposal[status]" id="active" value="Active">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="proposal[name]" id="proposal_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing" id="dynamicDataRow-emailResponder">
				<div class="grid4"><label>Contact:</label></div>
				<div class="grid8">
					<select data-placeholder="Choose a Contact" class="select2" name="proposal[contact_id]" id="contact_id">
						<option></option>
<?php
foreach($Contacts as $Contact){
	$this->getRow('account_contacts_option.php',array('context' => $Contact));
}
?>
					</select>
				</div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Expired Date:</label></div>
				<div class="grid8"><input id="expire_date" type="text" name="proposal[expire_date]" class="expiredatepicker" value=""></div>
			</div>
		<?php } ?>
		</fieldset>
	</form>
</div>