<?php
/* 
 * Author:     cory frosch <cory@6qube.com>
 * License:    
 */
?>
<!-- Add or Update Billing Profile -->
<div class="SQmodalWindowContainer dialog" title="Billing Profile" id="jdialog-createReport" style="display: none;">
	<form method="post" action="<? //echo $this->action_url('Campaign_save'); ?>" class="jlisting-create">
		<input type="hidden" name="listing[cid]" value="<?php //echo $Campaign->getID(); ?>"	/>
		<fieldset class="step" id="w1first">
			<div class="fluid chosen_dropfix">
				
                <div class="formRow">
                    <div class="grid3"><label>First Name:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[firstname]" value="" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Last Name:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[lastname]" value="">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Phone:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[phone]" value="">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Company Name:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[company]" value="">
                    </div>
                </div>
                <div class="formRow noBorderB">
                    <div class="grid3"><label>Address:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[address]" value="">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Address 2:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[address2]" value="">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>City:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[city]" value="">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>State:</label></div>
                    <div class="grid9">
                        <?php $this->getWidget('states-dropdown.php', array('value' => 'TX', 'name' => 'billinginfo[state]'));?>
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Zip:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[zip]" value="">
                    </div>
                </div>
                <div class="formRow">          
                    <div class="grid3">Card Number:</div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[cc]" value="">
                        <span class="note">numbers only</span>
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Expiration Date:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[expDate]" value="" id="expDateModal">
                    </div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>CCV Code:</label></div>
                    <div class="grid9">
                        <input type="text" class="edit" name="billinginfo[CCV]" maxlength="4" value="">
                        <span class="note">3 - 4 digit code on back</span>
                    </div>
                </div>
                
			</div>
		</fieldset>
	</form>
</div>


<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
        
        $('#expDateModal').datepicker({
            showOtherMonths:true,
            autoSize: true,
            dateFormat: 'mm/y'
        });
        
		//===== Create New Form + PopUp =====//
		$( '#jdialog-createReport' ).dialog(
		{
			autoOpen: false,
			height: 450,
			width: 550,
			modal: true,
            open: function( event, ui )
            {   
                
            },
			close: function ()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Update Billing Profile',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
	          $('.jnoforms').hide();
	          $('.jreports').prepend(ajax.row);
              notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createReport')).buttons
		});
        
        $("#ui-datepicker-div").css("z-index", "9999");
	
	});
    
<?php $this->end_script(); ?>
</script>