<div class="SQmodalWindowContainer dialog" title="Create a New Multi-Site" id="jdialog-createMultiSite" style="display: none;">
	<form method="post" action="<?= $this->action_url('admin_saveMUWebsite'); ?>" class="jform-create">
		<div class="fluid spacing">
			<div class="grid4"><label>Multi-Site Name:</label></div>
			<div class="grid8"><input type="text" name="website[name]" id="" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
		</div>
<?php $this->getWidget('touchpointtype-select.php'); ?>
	</form>
</div>