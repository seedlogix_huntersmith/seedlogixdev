<div class="SQmodalWindowContainer dialog" title="Send a Text Message" id="jdialog-sendtext" style="display: none;">
	<form method="post" action="<?php echo $action_url; ?>" class="ajax">
  	<input type="hidden" name="campaign[id]" id="campaign_id" value="<?=$campaign_id?>">
		<fieldset>
			
			<div class="fluid spacing">
				<div class="grid4"><label>To:</label></div>
				<div class="grid8"><input type="text" name="sendText" id="send_text" style="margin: 0;" value="<?=$phone?>"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Message:</label></div>
				<div class="grid8"><textarea name="TextMessage" class="text ui-widget-content ui-corner-all form-control" cols="65" style="height: 100px;"></textarea></div>
			</div>
		</fieldset>
	</form>
</div>