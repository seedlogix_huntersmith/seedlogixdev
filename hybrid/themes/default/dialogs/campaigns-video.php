<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/4.12/video.js"></script>
<style type="text/css">
    .vjs-default-skin .vjs-play-progress,
    .vjs-default-skin .vjs-volume-level { background-color: #397494 }
</style>
<!-- Create Campaign POPUP -->
<div class="SQmodalWindowContainer dialog" title="Campaigns Video" id="jdialog-campaignsvideo" style="display: none;">

    <video id="MY_VIDEO_1" class="video-js vjs-default-skin" controls
           preload="auto" width="100%" height="100%"
           data-setup="{}">
        <source src="https://s3-sa-east-1.amazonaws.com/seedlogix/training/videos/campaigns-overview-video.mp4" type='video/mp4'>
        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>

</div>

<script type="text/javascript">


    $(document).ready(function(){

        $( "#jdialog-campaignsvideo" ).dialog({
            autoOpen: false,
            height: 530,
            width: 860,
            modal: true
        });

    });

</script>