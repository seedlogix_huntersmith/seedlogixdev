<?php $selectingCampaigns = false; ?>

<div class="SQmodalWindowContainer dialog" title="Create a New Report" id="jdialog-createReport" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jlisting-create">
		<input type="hidden" name="report[ID]" value="0" id="reportID">
<?php if(isset($Campaign)): ?>
		<input type="hidden" name="report[campaign_ID]" value="<?= $Campaign->getID(); ?>">
<?php endif; ?>
		<fieldset class="step" id="w1first">
			<div class="fluid spacing">
                <div class="grid4"><label>Report Name:</label></div>
                <div class="grid8 m_lt"><input type="text" name="report[name]" placeholder="" style="margin: 0;"></div>
            </div>
            <div class="fluid spacing">
                <div class="grid4"><label>Type:</label></div>
                <div class="grid8 m_lt">
                    <select name="report[type]" id="sq-listingType">
                        <option value="DAILY">Daily</option>
                        <option value="WEEKLY">Weekly</option>
                        <option value="MONTHLY">Monthly</option>
                        <option value="YEARLY">Yearly</option>
                        <option value="CUSTOM">Custom Range</option>
                    </select>
                </div>
            </div>
            <div class="fluid spacing" id="sq-listingTypeRange" style="display: none;">
                <div class="grid4"><label>Custom Range:</label></div>
                <div class="grid4"><input type="text" name="report[start_date]" id="fromDate" placeholder="min Date" style="margin: 0;"></div>
                <div class="grid4"><input type="text" name="report[end_date]" id="toDate" placeholder="max Date" style="margin: 0;"></div>
            </div>
			<div class="fluid spacing"<?= $selectingCampaigns ? ' style="display: none;"' : ''; ?>>
                <div class="grid4"><label>Touchpoints:</label></div>
                <div class="grid8 m_lt">
                    <select name="report[touchpoint_type]">
                        <option value="WEBSITE">Websites</option>
                        <option value="BLOGS">Blogs</option>
                        <option value="LANDING">Landing Pages</option>
                        <option value="ALL">All Touchpoints</option>
                    </select>
                </div>
            </div>
			<div class="fluid spacing"<?= $selectingCampaigns ? '' : ' style="display: none;"'; ?>>
                <div class="grid4"><label>Campaign:</label></div>
                <div class="grid8 m_lt">
                    <select name="report[]">
<?php foreach($campaignList as $campaign){echo '<option value="'.$campaign->id.'">'.$campaign->name.'</option>';}?>
                    </select>
                </div>
            </div>
		</fieldset>
	</form>
</div>