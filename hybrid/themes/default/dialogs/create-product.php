<div class="SQmodalWindowContainer dialog" title="Create a New Product" id="jdialog-createproduct" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" name="product[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<input type="hidden" name="product[proposal_id]" id="proposal_id" value="<?= $_GET['proid']; ?>">
		<input type="hidden" name="product[qty]" id="proposal_id" value="1">
		<fieldset>
		<div class="fluid spacing">
				<div class="grid4"><label>Select Product:</label></div>
                <div class="grid8">
                        <select data-placeholder="Choose a Product&hellip;" class="select2" name="product[prodid]" id="selectData">
                            <option></option>

<?php foreach($parent_products as $Parentlist): ?>

                                <option value="<?=$Parentlist->id?>"><?=$Parentlist->name?></option>

<?php endforeach; ?>

                        </select>
                    
    
                </div>
			</div>
		<hr style="margin:20px 10px 20px 10px;">
			<h6>Or Create Custom Product</h6><br>
				<div class="fluid spacing">
					<div class="grid4"><label>Name:</label></div>
					<div class="grid8"><input type="text" name="product[name]" id="product_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				<div class="fluid spacing">
					<div class="grid4"><label>Price:</label></div>
					<div class="grid8"><input type="text" name="product[price]" id="product_price" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				<div class="fluid spacing">
					<div class="grid4"><label>Select Type:</label></div>
					<div class="grid8">

							<select data-placeholder="Choose Type&hellip;" class="select2" name="product[type]" id="product_type">
								<option></option>
									<option value="One-Time">One-Time</option>  
									<option value="Monthly">Monthly</option>  
								<option value="Annual">Annual</option>  
							</select>


					</div>
				</div>

			
			
		</fieldset>
	</form>
</div>