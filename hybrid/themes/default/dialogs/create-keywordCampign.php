<div class="SQmodalWindowContainer dialog" title="Create a New Keyword Campaign" id="jdialog-createkeywordcampaign" style="display: none;">
    <form id="createCampForm" action="<?= $this->action_url("Campaign_save"); ?>">
        <input type="hidden" name="ranking_site[ID]" value="0">
        <input type="hidden" name="ranking_site[campaign_ID]" id="campaign_id" value="<?= $campaign_id; ?>">
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>Select a Touchpoint:</label></div>
                <div class="grid8">

<?php
$this->getWidget('select-ajax.php',array(
    'id'            => 'selectRankingTp',
    'name'          => 'ranking_site[touchpoint_ID]',
    'ajaxUrl'       => $this->action_url('Campaign_ajaxTouchpoints',array('ID' => $Campaign->getID())),
    'field_value'   => 'touchpoint_ID',
    'text'          => ':touchpoint_name: (:hostname:)',
    'data'          => $rankingSites
));
?>

                </div>
            </div>
            <div class="fluid spacing">
                <div class="grid4"><label><strong>OR</strong> Add a Custom Domain:</label></div>
                <div class="grid8">
                    <div class="input-group input-group-sm" style="margin: 0; width: 100%;">
                        <span class="input-group-addon" style="margin: 0; width: 20%;">http://</span>
                        <input type="text" name="ranking_site[hostname]" id="keyword_campaign_url" class="text ui-widget-content ui-corner-all form-control" placeholder="example.com" style="margin: 0;">
                    </div>
                </div>
            </div>
        </fieldset>
	</form>
</div>