<div class="SQmodalWindowContainer dialog" title="Are you sure?" id="jdialog-transferLeadToAgentCRM" style="display: none;">
    <?php
        $form_url = $this->action_url('Campaign_transfer-lead-to-agent-crm',array('ID' => $Prospect->cid,'pid' => $Prospect->getID()));
    ?>
    <form method="post"
          id="prosCont"
          action="<?= $this->action_url('Campaign_transfer-lead-to-agent-crm', array('ID' => $Prospect->cid, 'pid' => $Prospect->getID())); ?>"
          class="jform-create">
        <input type="hidden" id="agentUserId" name="agent_user_id" value="" />
        <script type="text/javascript">
            $(function(){
                $("#transferLeadToAgentCRM").on('change', function() {
                    var agentUserId = $(this).val();
                    //console.log(agentUserId);
                    $("#agentUserId").val(agentUserId);
                });
            });
        </script>
        <fieldset>
            <div class="fluid spacing" id="dynamicDataRow-emailResponder">
                <div class="grid4"><label>Select Campaign:</label></div>
                <div class="grid8">
                    <select data-placeholder="Select Campaign" class="select2" name="campaign_id" id="campaign_id">
                        <option></option>
                        <?php foreach ($Campaigns as $campaign) : ?>
                            <option value="<?php echo $campaign->id; ?>"><?php $this->p($campaign->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </fieldset>
    </form>
</div>