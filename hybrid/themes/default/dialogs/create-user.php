<div class="SQmodalWindowContainer dialog" title="Create a New User" id="jdialog-createUser" style="display: none;">
	<form method="post" action="<?= $this->action_url('UserManager_save'); ?>" class="jform-create">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Username: (email address)</label></div>
				<div class="grid8"><input type="text" name="user[username]" id="user_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Role:</label></div>
				<div class="grid8">
					<select data-placeholder="Choose a Role&hellip;" class="select2" name="user[role]">
						<option></option>
<?php if($User->isSystem()): ?>
						<option value="0">No Role - New Admin Account</option>
<?php endif; ?>
<?php foreach($roles as $role): ?>
						<option value="<?= $role->ID; ?>"><?php $this->p($role->company); ?> - <?php $this->p($role->role); ?></option>
<?php endforeach; ?>
					</select>
				</div>
			</div>
		</fieldset>
	</form>
</div>