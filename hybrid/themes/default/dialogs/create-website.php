<?php
$info       = array(
            'title'         => 'Create a New '.substr($selected_pointtype_label,0,-1),
            'name_title'    => substr($selected_pointtype_label,0,-1).' Name:');
?>

<!-- Create Website Dialog -->
<div class="SQmodalWindowContainer dialog" title="Create a New <?= substr($selected_pointtype_label,0,-1); ?>" id="jdialog-createwebsite" style="display: none;">
	<form id="createCampForm" action="<?= $this->action_url("campaign_save"); ?>">
        <input type="hidden" name="website[cid]" value="<?= $Campaign->id; ?>">
        <div class="fluid spacing">
            <div class="grid4"><label for="website[name]">Name:</label></div>
            <div class="grid8"><input type="text" name="website[name]" id="website_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
<?php $this->getWidget('touchpointtype-select.php'); ?>
	</form>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>
    $(function (){
        
        $("#jdialog-createwebsite").dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true,
            buttons: (new CreateDialog("Create", 
                function (ajax, status) {
                /*
                    standards here will be 'error' var, possible values are : 'validation', 'other'
                    if error = 'validation', check 'validation' object,
                    if error = 'other' then print 'message''
                    if no error or error = false, check result object. (could be a partially 
                */
                    if (ajax.created >= ajax.limit){
                        $('.jopen-createwebsite').addClass('disabled');
                        $('.jopen-createwebsite').prop('title','Change your subscription to create more <?= $selected_pointtype_label; ?>.');
                        $('.jopen-createwebsite').removeClass('jopendialog');
                        $('.jopen-createwebsite').removeClass('jopen-createwebsite');

                    }
                    if (ajax.error) {
                        if (ajax.error   ==  'validation') {
                            notie.alert({text:getValidationMessages(ajax.validation).join('\n')});
                        }
                        else {
                            notie.alert({text:ajax.message});
                        }
                    }
                    else {
                        // @todo prompt to go to new campaign dashboard.
                        $('#jnotps-msg').hide();
                        //var arr =   [ajax.result.name, 0, 0, "0 %", new Date(), "other"];
                        //new InsertObjectAtTable('#jsites', arr, ajax.result.actions.dashboard);
                        var row = $(ajax.row)[0];
                        var dT = $('#websites');
                        //dT.fnAddTr(row);
                        //prepend(ajax.row);
                        //dT.fnGetPageOfRow(row);
                        //bindEvents.evts[0](row);
						var newRowsIds = dT.data('newRowsIds');
						newRowsIds.unshift(ajax.object.id);
						dT.data('newRowsIds', newRowsIds);
                        dT.dataTable().fnSort( [ [6,'desc'] ] );
                        dT.dataTable().fnDraw();
                        this.close();
                        this.form[0].reset();
                        notie.alert({text:ajax.message});
                        for (var touchpoint in ajax.touchpoints) {
                            if (touchpoint != '') {
                                $('a[href*=' + touchpoint + '] .dataNumGrey').text(ajax.touchpoints[touchpoint]);
                            }
                        }                        
                    }
                }, "#jdialog-createwebsite")).buttons
        });
        
    });
    function j_ben(J){
        $('#create-website').removeClass('disabled');
        $('#create-website').prop('title','');
        $('#create-website').addClass('jopendialog');
        $('#create-website').addClass('jopen-createwebsite');
    }
<?php $this->end_script(); ?>
</script>