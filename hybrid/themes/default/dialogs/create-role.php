<style type="text/css">
    #jdialog-createrole .permissions label { width: 30%; display: block; float: left; }
</style>

<div class="SQmodalWindowContainer dialog" title="Create a New Role" id="jdialog-createrole" style="display: none;">
	<form id="createCampForm" action="<?= $this->action_url($controller.'_newrole'); ?>">
        <input type="hidden" name="role[ID]" value="" class="role_id">
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>Name:</label></div>
                <div class="grid8"><input type="text" name="role[role]" id="role_name" class="text ui-widget-content ui-corner-all role_role" style="margin: 0;"></div>
            </div>
        </fieldset>
<?php if($User->can('is_system')): // allow system users to create roles for other users ?>
        <fieldset id="reselleruser_fieldset">
            <div class="fluid spacing">
                <div class="grid4"><label>Account:</label></div>
                <div class="grid8">
                    <select data-placeholder="Choose an Account&hellip;" class="role_reselleruser select2" name="role[reseller_user]">
                        <option></option>
<?php foreach($MainAccounts as $UserInfo): ?>
                        <option value="<?= $UserInfo->getID(); ?>"><?php $this->p($UserInfo->company.' #'.$UserInfo->getID()); ?></option>
<?php endforeach; ?>
                    </select>

                </div>
            </div>
        </fieldset>
<?php endif; ?>
        <label>Permissions:</label><br>
        <fieldset class="permissions">
<?php foreach($AllPermissions as $QubePermission): ?>
<?php
$replace_arr1 = array('_','socialaccounts','HOME','customforms','editcontact','emailmarketing');
$replace_arr2 = array(' ','Social Accounts','Home','Custom Forms','Edit Contact','Email Marketing');
?>
            <label><input type="checkbox" value="1" name="role[permissions][<?php $this->p($QubePermission->ID); ?>]" id="chk_permission_<?php $this->p($QubePermission->flag); ?>"><?= ucwords(str_replace($replace_arr1,$replace_arr2,$QubePermission->flag)); ?></label>
<?php endforeach; ?>
        </fieldset>
        <!--<label><a href="#" style="color:#2b6893"><strong>View Permission Descriptions</strong></a></label>-->
	</form>
</div>

<script type="text/javascript">
    
				<?php if($User->isSystem()): ?>
					function set_reselleruser_visible(visible){
						if(!visible){
							$("#reselleruser_fieldset").hide();
							$("#reselleruser_fieldset select").attr('disabled', 'disabled');
						}else{
							$("#reselleruser_fieldset").show();
							$("#reselleruser_fieldset select").attr('disabled', false);
						}
        $.uniform.update();
					}
				<?php endif; ?>
    function setup_edit_links(sethooks){
        
//        console.log(this, arguments);

        if(jQuery.type(sethooks) === "string"){
//            $(sethooks).click(setup_edit_links);
            $(sethooks).on('click', 'a.edit-role' ,setup_edit_links);
            return;
        }
        
        // handle CLICK event
        
        var id      =   this.id.split('_').pop();
//        var rolename    =   $('#role_'+id + ' .role_role').text();
		var rolename = $('.role_role', $('.dTable tr').has('#permissions_' + id)).text()
        var opts    =   $(this).closest('tr').find('span.p_checks').text().split(',');
        
//        console.log(opts);
        
        $('#jdialog-createrole input:checkbox').attr('checked', false);
        $('#jdialog-createrole .role_role').val(rolename);
        $('#jdialog-createrole .role_id').val(id);
        
//                var two = $("#check2").attr("checked", this.checked);
        for(var i in opts){
            var chk =   $('#chk_permission_' + $.trim(opts[i]));
            chk.attr('checked', true);            
        }
        
				<?php if($User->isSystem()): ?>
						set_reselleruser_visible(false);
				<?php endif; ?>

        // click event on 'a'
         $('#jdialog-createrole').dialog('open').dialog('option','title','Edit the Role');

        // @fer: dynamically switch button text
        $('.ui-dialog-buttonpane button:contains(Create)').text('Edit');

        $.uniform.update();
    }
    
    $(function (){
        setup_edit_links('#system-roles-tbl');
        
$('#jdialog-createrole').dialog({
        close: function (){
					$('#createCampForm')[0].reset();
					$('#createCampForm .permissions input:checkbox').removeAttr('checked'); 
				<?php if($User->isSystem()): ?>
						set_reselleruser_visible(true);
				<?php endif; ?>
							},
            autoOpen: false,
            height: 500,
            width: 500,
            modal: true,
            buttons: (new CreateDialog('Create',
                function (ajax, status){
                    // standards here will be 'error' var, possible values are : 'validation', 'other'
                    //  if error = 'validation', check 'validation' object,
                    // if error = 'other' then print 'message''
                    // if no error or error = false, check result object. (could be a partially 
                    if(ajax.error)
                    {
                        if(ajax.error   ==   'validation')
                        {
                            var msg =   getValidationMessages(ajax.validation); /*
                            for(var i in ajax.validation){
                                for(var mi in ajax.validation[i]){
                                    msg.push(ajax.validation[i][mi]);
                                }
                            }   */
//                                console.log(msg, ajax.validation, ajax);
                            alert("The following errors occurred: \n" + msg.join("\n"));
                        }else
                            alert(ajax.message);
                    }else{
                        if(ajax.error){
                            notie.alert({text:ajax.error});
                        }else{
							var dt = $('#system-roles-tbl');
							var newRowsIds = dt.data('newRowsIds');
							newRowsIds.unshift(ajax.object.ID);
							dt.data('newRowsIds', newRowsIds);
							dt.dataTable().fnDraw();
                            notie.alert({text:'A New Role has been created.'});
                        }
//
////                        var arr =   ['', ajax.result.role, "other"];
//
////                        var dT  =   $('#system-roles-tbl');
//                        var rowSel  =   'tr#role_' + ajax.object.ID;
//                        var rowEl   =   $(rowSel);
//
//                        if(ajax.is_update){
//                            rowEl.after(ajax.row);
//                            rowEl.remove();
//                        }else
//                            $('#system-roles-tbl tbody').append(ajax.row);
////#                            new InsertObjectAtTable('#system-roles-tbl', arr, ajax.result.actions.dashboard);
//
//                        $(rowSel + ' input:checkbox').uniform();
//                        setup_edit_links('#system-roles-tbl .role_permissions_str a');
//
////                            $('#system-roles-tbl').dataTable();
////                            $('#system-roles-tbl input:checkbox').uniform();
////                        dT.fnDraw(false);
                        this.close();
////                        this.form[0].reset();
                    }

                },'#jdialog-createrole')).buttons
    });
    });
</script>