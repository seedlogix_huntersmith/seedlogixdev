<div class="SQmodalWindowContainer dialog" title="Add a New Social Account" id="jdialog-addAccount" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
    <input type="hidden" name="social_accounts[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="social_accounts[name]" id="account_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Network:</label></div>
				<div class="grid8">
					<select name="social_accounts[network]">
						<option value="FACEBOOK">Facebook</option>
						<option value="TWITTER">Twitter</option>
						<option value="LINKEDIN">LinkedIn</option>
					</select>
				</div>
			</div>
		</fieldset>
	</form>
</div>