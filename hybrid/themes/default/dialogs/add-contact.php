<div class="SQmodalWindowContainer dialog" title="Add Contact" id="jdialog-addContact" style="display: none;">
    <?php
        $form_url = $this->action_url('Campaign_prospect-to-contact',array('ID' => $Prospect->cid,'pid' => $Prospect->getID()));
    ?>
	<form method="post" action="<?= $this->action_url('Campaign_prospect-to-contact',array('ID' => $Prospect->cid,'pid' => $Prospect->getID())); ?>" class="jform-create">
		<fieldset>
			<div class="fluid spacing" id="dynamicDataRow-emailResponder">
				<div class="grid4"><label>Select Account:</label></div>
				<div class="grid8">
					<select data-placeholder="Select An Account" class="select2" name="account_id" id="account_id">
						<option></option>
<?php
foreach($Accounts as $Account){?>
	<option value="<?php echo $Account->id; ?>" ><?php $this->p($Account->name); ?></option>
<?php }
?>
					</select>
				</div>
			</div>
			<hr style="margin:20px 10px 20px 10px;">
			<h6>Or Create New Account</h6><br>
				<div class="fluid spacing">
					<div class="grid4"><label>Name:</label></div>
					<div class="grid8"><input type="text" name="account_name" id="account_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				

			
		</fieldset>
	</form>
</div>