<div class="SQmodalWindowContainer dialog" title="Create a New Page" id="jdialog-createpage" style="display: none;">
	<form id="jcreatepage-form" action="<?= $this->action_url("Website_newpage?ID=$site->id"); ?>" method="POST">
		<input type="hidden" value="<?= $site->touchpoint_type; ?>" name="website[touchpoint_type]">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="page[page_title]" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Nav Section:</label></div>
				<div class="grid8">
<?php $this->getControl('navsection-select.php',array('fieldname' => 'page[nav_parent_id]')); ?>
				</div>
			</div>
		</fieldset>
	</form>
</div>