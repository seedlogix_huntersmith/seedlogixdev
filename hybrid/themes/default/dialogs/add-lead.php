<div class="SQmodalWindowContainer dialog" title="Add a New Lead" id="jdialog-addLead" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
    <input type="hidden" name="prospect[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="prospect[name]" id="prospect_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Email:</label></div>
				<div class="grid8"><input type="text" name="prospect[email]" id="prospect_email" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Phone:</label></div>
				<div class="grid8"><input type="text" name="prospect[phone]" id="prospect_phone" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Company:</label></div>
				<div class="grid8"><input type="text" name="prospect[company]" id="prospect_company" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
		</fieldset>
	</form>
</div>