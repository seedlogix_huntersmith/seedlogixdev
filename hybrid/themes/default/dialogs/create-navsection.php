<div class="SQmodalWindowContainer dialog" title="Create a New Nav Section" id="create-nav" style="display: none;">
	<form id="jcreatenav-form" action="<?= $this->action_url("Website_newnav?ID=$site->id"); ?>" method="POST">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="nav[page_title]" id="section_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Nav Section:</label></div>
				<div class="grid8">
<?php $this->getControl('navsection-select.php',array('fieldname' => 'nav[nav_parent_id]','selected_nav_id' => 0)); ?>
				</div>
			</div>
		</fieldset>
	</form>
</div>