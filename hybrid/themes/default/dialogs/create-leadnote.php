<div class="SQmodalWindowContainer dialog" title="Create a New Note" id="jdialog-createleadnote" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jlisting-create">
		<input type="hidden" name="note[cid]" id="campaign_id" value="<?= $Prospect->cid; ?>">
		<input type="hidden" name="note[lead_id]" id="lead_id" value="<?= $Prospect->id; ?>">
		<input type="hidden" name="note[user_id]" id="user_id" value="<?= $User->getID(); ?>">
		<fieldset class="step" id="w1first">
			<div class="fluid spacing">
				<div class="grid4"><label>Subject:</label></div>
				<div class="grid8"><input type="text" name="note[subject]" placeholder="" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Message:</label></div>
				<div class="grid8"><textarea name="note[note]" class="text ui-widget-content ui-corner-all form-control" cols="65" style="height: 100px;"></textarea></div>
			</div>
		</fieldset>
	</form>
</div>