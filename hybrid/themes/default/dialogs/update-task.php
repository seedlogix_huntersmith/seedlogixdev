<div class="SQmodalWindowContainer dialog" title="Update Task" id="jdialog-savetask" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" id="task_id" value="" name="task[id]">
		<fieldset>


			<div class="fluid spacing">
				<div class="grid4"><label>Description:</label>
				</div>
				<div class="grid8"><input type="text" name="task[title]" id="task_title" value="" class="text ui-widget-content ui-corner-all" style="margin: 0;">
				</div>
			</div>

			<div class="fluid spacing">
				<div class="grid4"><label>Due Date:</label>
				</div>
				<div class="grid8"><input id="due_date2" type="text" name="task[due_date]" class="duedatepicker2" value="">
				</div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Time:</label></div>
				<div class="grid8">
					<select data-placeholder="Choose a Time" class="select2" name="task[time]" id="time">
						<option></option>
						
<option value="5:00">5:00 AM</option>
<option value="5:15">5:15 AM</option>
<option value="5:30">5:30 AM</option>
<option value="5:45">5:45 AM</option>
 
<option value="6:00">6:00 AM</option>
<option value="6:15">6:15 AM</option>
<option value="6:30">6:30 AM</option>
<option value="6:45">6:45 AM</option>
 
<option value="7:00">7:00 AM</option>
<option value="7:15">7:15 AM</option>
<option value="7:30">7:30 AM</option>
<option value="7:45">7:45 AM</option>
 
<option value="8:00">8:00 AM</option>
<option value="8:15">8:15 AM</option>
<option value="8:30">8:30 AM</option>
<option value="8:45">8:45 AM</option>
 
<option value="9:00">9:00 AM</option>
<option value="9:15">9:15 AM</option>
<option value="9:30">9:30 AM</option>
<option value="9:45">9:45 AM</option>
 
<option value="10:00">10:00 AM</option>
<option value="10:15">10:15 AM</option>
<option value="10:30">10:30 AM</option>
<option value="10:45">10:45 AM</option>
 
<option value="11:00">11:00 AM</option>
<option value="11:15">11:15 AM</option>
<option value="11:30">11:30 AM</option>
<option value="11:45">11:45 AM</option>
 
<option value="12:00">12:00 PM</option>
<option value="12:15">12:15 PM</option>
<option value="12:30">12:30 PM</option>
<option value="12:45">12:45 PM</option>
 
<option value="13:00">1:00 PM</option>
<option value="13:15">1:15 PM</option>
<option value="13:30">1:30 PM</option>
<option value="13:45">1:45 PM</option>
 
<option value="14:00">2:00 PM</option>
<option value="14:15">2:15 PM</option>
<option value="14:30">2:30 PM</option>
<option value="14:45">2:45 PM</option>
 
<option value="15:00">3:00 PM</option>
<option value="15:15">3:15 PM</option>
<option value="15:30">3:30 PM</option>
<option value="15:45">3:45 PM</option>
 
<option value="16:00">4:00 PM</option>
<option value="16:15">4:15 PM</option>
<option value="16:30">4:30 PM</option>
<option value="16:45">4:45 PM</option>
 
<option value="17:00">5:00 PM</option>
<option value="17:15">5:15 PM</option>
<option value="17:30">5:30 PM</option>
<option value="17:45">5:45 PM</option>
 
<option value="18:00">6:00 PM</option>
<option value="18:15">6:15 PM</option>
<option value="18:30">6:30 PM</option>
<option value="18:45">6:45 PM</option>
						
<option value="19:00">7:00 PM</option>
<option value="19:15">7:15 PM</option>
<option value="19:30">7:30 PM</option>
<option value="19:45">7:45 PM</option>
 
<option value="20:00">8:00 PM</option>
<option value="20:15">8:15 PM</option>
<option value="20:30">8:30 PM</option>
<option value="20:45">8:45 PM</option>
 
<option value="21:00">9:00 PM</option>
<option value="21:15">9:15 PM</option>
<option value="21:30">9:30 PM</option>
<option value="21:45">9:45 PM</option>
 
<option value="22:00">10:00 PM</option>
<option value="22:15">10:15 PM</option>
<option value="22:30">10:30 PM</option>
<option value="22:45">10:45 PM</option>
 
<option value="23:00">11:00 PM</option>
<option value="23:15">11:15 PM</option>
<option value="23:30">11:30 PM</option>
<option value="23:45">11:45 PM</option>
						
						</select>
				</div>
			</div>
		</fieldset>
	</form>
</div>