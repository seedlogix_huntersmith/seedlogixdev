<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
$tpoint = $site->touchpoint_type;
if($campaignsVideo) {
    $video = 'http://cdn.secure-url.co/seedlogix/campaigns-overview-video.mp4';
    $titleVid = 'Campaigns Video';
}
if($campaignVideo){
    $video = 'http://cdn.secure-url.co/seedlogix/campaign-overview-video.mp4';
    $titleVid = 'Campaign Video';
}
if(($campaignVideo && $touchpoint_type == 'website') || ($campaignVideo && $touchpoint_type == 'WEBSITE')) {
    $video = 'http://cdn.secure-url.co/seedlogix/websites-overview-video.mp4';
    $titleVid = 'Websites Video';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('settings')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hubs-settings-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Settings';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('theme')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hubs-themes-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Theme';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('seo')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hub-seo-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' SEO';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('editRegions')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hubs-editregions-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Edit Regions';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('photos')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hub-slider-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Slider';
}
if($site->touchpoint_type == 'WEBSITE' && $this->isAction('social')) {
    $video = 'http://cdn.secure-url.co/seedlogix/hubs-social-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Social';
}
if($site->touchpoint_type == 'WEBSITE' && ($this->isAction('pages') || $this->isAction('pagedetails'))) {
    $video = 'http://cdn.secure-url.co/seedlogix/website-pages-video.mp4';
    $titleVid = ucfirst(strtolower($tpoint)).' Pages';
}
?>
<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/4.12/video.js"></script>
<style type="text/css">
    .vjs-default-skin .vjs-play-progress,
    .vjs-default-skin .vjs-volume-level { background-color: #397494 }
</style>
<!-- Create Campaign POPUP -->
<div class="SQmodalWindowContainer dialog" title="<?=$titleVid?>" id="jdialog-campaignsvideo" style="display: none;">

    <video id="MY_VIDEO_1" class="video-js vjs-default-skin" controls
           preload="auto" width="100%" height="100%"
           data-setup="{}">
        <source src="<?=$video?>" type='video/mp4'>
        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>

</div>

<script type="text/javascript">


    $(document).ready(function(){

        $( "#jdialog-campaignsvideo" ).dialog({
            autoOpen: false,
            height: 530,
            width: 860,
            modal: true
        });

    });

</script>