<?php

/* 
 * Author:     Jon Aguilar
 * License:    
 */
?>

<!-- Create Campaign POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Campaign" id="jdialog-addCampaign" style="display: none;">
    <form id="createCampForm" action="<?= $this->action_url("campaign_save"); ?>">
        <input type="hidden" name="campaigncrm[id]" id="campaign_id" value="">
        <div class="fluid spacing">
            <div class="grid4"><label for="campaign_name">Campaign Name:</label></div>
            <div class="grid8"><input type="text" name="campaigncrm[name]" id="campaign_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
    </form>
</div>