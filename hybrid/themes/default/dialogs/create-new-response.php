<?php
/**
 * @var $this Template
 * @var $Campaign CampaignModel
 * @var HubModel[] $websites
 */
?>

<div class="SQmodalWindowContainer dialog" title="Create a New Response" id="jdialog-createresponse" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
        <input type="hidden" name="collab[campaign_id]" id="campaign_id" value="<?= $Campaign->getID(); ?>">
        <div class="fluid spacing">
            <div class="grid4"><label for="name">Name:</label></div>
            <div class="grid8"><input type="text" name="collab[name]" id="name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
        <div class="fluid spacing">
            <div class="grid4"><label for="email">Email Address:</label></div>
            <div class="grid8"><input type="text" name="collab[email]" id="email" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
        <div class="fluid spacing">
            <div class="grid4"><label for="scope">Scope:</label></div>
            <div class="grid8">
                <select name="collab[scope]" id="scope">
                    <option value="campaign">Campaign</option>
                    <option value="custom">Website</option>
                </select>
                <span class="note" style="display: none;">Please edit the entry to specify which websites.</span>
            </div>
        </div>
        <div id="websites_search" style="display: none;">
            <div class="fluid spacing">
                <div class="grid4"><label for="website">Touchpoint Name:</label></div>
                <div class="grid8">
<?php $this->getWidget('select-ajax.php',array(
    'id' => 'websites',
    'name' => 'collab[websites][]',
    'ajaxUrl' => $this->action_url('Campaign_ajaxTouchpoints',array('ID' => $Campaign->getID())),
    'field_value' => 'hub_ID',
    'text' => ':touchpoint_name: (:hostname:)',
    'multiple' => true,
    'params' => array('type' => 'hub')
)); ?>
                </div>
            </div>
        </div>
        <div class="fluid spacing">
            <div class="grid4"><label for="active">Active:</label></div>
            <div class="grid8">
                <select name="collab[active]" id="active">
                    <option value="1">Active</option>
                    <option value="2">Inactive</option>
                </select>
            </div>
        </div>
	</form>
</div>

<script>
<?php $this->start_script();?>
    $(document).ready(function(){
        $('#scope').change(function(e){
            if($(this).val() == 'custom'){
                $('.note,#websites_search').show();
            } else{
                $('.note,#websites_search').hide();
            }
        });
    });
<?php $this->end_script();?>
</script>