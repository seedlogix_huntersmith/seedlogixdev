<?php
$eset = EmailSettingsModel::Fetch('cid = %d', $campaign_id);
?>
<div class="SQmodalWindowContainer dialog" title="Send Email" id="jdialog-sendemail" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" name="email[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
	<input type="hidden" id="contactid" value="" name="email[contact_id]">
		<input type="hidden" id="accountid" value="" name="email[account_id]">



<div class="fluid">
		<!-- section -->
		<div class="widget grid6" style="margin-top:0px!important">
			<div class="whead">
				<h6><span><?=$eset->name?> &lt;<?=$eset->username?>&gt;</span></h6>
			</div>
			
			<div class="formRow emailform">
                <div class="grid2"><label>To:</label></div>
                <div class="grid10"><input id="emailto" type="text" name="email[to_sent]" value="" placeholder="" readonly="readonly"></div>
            </div>
				
				<div class="formRow emailform">
                <div class="grid2"><label>CC:</label></div>
                <div class="grid10"><input id="emailcc" type="text" class="tags" name="email[cc_sent]" value="" placeholder=""></div>
            </div>
				
				<div class="formRow emailform">
                <div class="grid2"><label>BC:</label></div>
                <div class="grid10"><input id="emailbc" type="text" class="tags" name="email[bcc_sent]" value="" placeholder=""></div>
            </div>
				
				
	
				
				

	</div>
	
	

	
		<!-- section -->
		<div class="widget grid6" style="margin-top:0px!important">
			<div class="whead">
				<h6 class="capitalize"><span>Choose a template.</span></h6>
			</div>
			
			<div class="formRow">
		
				<div class="grid12"><!--switchTemplate(this.form)-->
		
					<select style="width:50%;" data-placeholder="Pick A Template" class="select2" name="email_template" id="email_templates" onchange="switchTemplate()" >
						<option></option>
<?php
foreach($EmailTemplates as $EmailTemplate){
	$this->getRow('email_templates_option.php',array('context' => $EmailTemplate));
}
?>
					</select>
				
				</div>
			</div>
			
			<div class="formRow">
                <div class="grid2"><label>Subject:</label></div>
                <div class="grid10"><input id="subject" type="text" name="email[subject]" value="" placeholder=""></div>
            </div>
				

	</div>
	

<div class="widget grid12">	
<textarea class="j-cleditor" name="email[body]"></textarea>
	</div>
	
</div>
	
</div>
	
	
	</form>	
	
	
	
