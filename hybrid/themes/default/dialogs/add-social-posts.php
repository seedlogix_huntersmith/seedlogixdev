<div class="SQmodalWindowContainer dialog" title="Create a New Social Post" id="jdialog-addAccount" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" name="social_posts[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Post Name:</label></div>
				<div class="grid8"><input type="text" name="social_posts[name]" id="name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
		</fieldset>
	</form>
</div>