<div class="SQmodalWindowContainer dialog" title="Create New Listing" id="jdialog-createlisting" style="display: none;">
	<form method="post" action="<? echo $this->action_url('Campaign_save'); ?>" class="jlisting-create">
		<input type="hidden" name="listing[cid]" value="<?php echo $Campaign->getID(); ?>"	/>
		<fieldset class="step" id="w1first">
			<div class="fluid">
				<div class="grid3" style="position: relative; top: 11px;">
					<label>Listing Name:</label>
				</div>
				<div class="grid9">
					<input type="text" name="listing[name]" placeholder=""/>
				</div>
				<div class="clear"></div>
			</div>
			<div class="fluid">
				<div class="grid3" style="position: relative; top: 11px;">
					<label>Listing Type:</label>
				</div>
				<div class="grid6" style="position: relative; top: 8px;">
					<select name="listing[type]">
						<option value="directory" selected="">Directory</option>
						<option value="press">Press Release</option>
						<option value="article">Article</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
		</fieldset>
	</form>
</div>