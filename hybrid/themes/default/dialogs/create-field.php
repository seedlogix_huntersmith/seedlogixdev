<div class="SQmodalWindowContainer dialog" title="Create a New Form Field" id="jdialog-createformfield" style="display: none;">
    <form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jfield-create">
        <input type="hidden" name="field[form_ID]" value="<?= $form->id; ?>">
        <fieldset class="step" id="w1first">
            <div class="fluid spacing">
                <div class="grid4"><label>Name:</label></div>
                <div class="grid8"><input type="text" name="field[label]" style="margin: 0;"></div>
            </div>
            <div class="fluid spacing">
                <div class="grid4"><label>Type:</label></div>
                <div class="grid8">
                    <select data-placeholder="Choose a Type&hellip;" class="select2" name="field[type]">
                        <option></option>
                        <optgroup label="Basic">
                            <option value="text">Text Input</option>
                            <option value="textarea">Text Area</option>
                            <option value="select">Dropdown</option>
                            <option value="checkbox">Checkbox</option>
                            <option value="hidden">Hidden</option>
                        </optgroup>
                        <optgroup label="Presets">
                            <option value="email">Email Address</option>
                            <option value="money">Money Amount</option>
                            <option value="date">Date</option>

<?php /* <option value="name">Name</option> */ ?>

                            <option value="state">US States</option>
                        </optgroup>
                        <optgroup label="Non-Input">
                            <option value="section">Section Header</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        </fieldset>
    </form>
</div>