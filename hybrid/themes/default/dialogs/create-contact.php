<div class="SQmodalWindowContainer dialog" title="Create a New Contact" id="jdialog-createcontact" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" name="contact[cid]" id="campaign_id" value="<?= $campaign_id; ?>">
		<input type="hidden" name="contact[account_id]" id="account_id" value="<?= $_GET['aid']; ?>">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>First Name:</label></div>
				<div class="grid8"><input type="text" name="contact[first_name]" id="contact_first_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Last Name:</label></div>
				<div class="grid8"><input type="text" name="contact[last_name]" id="contact_last_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
		</fieldset>
	</form>
</div>