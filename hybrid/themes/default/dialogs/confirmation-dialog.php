<!-- Create Keyword Campaign POPUP -->
<!-- @amado @todo 
        use the corect input names and form action
        values are currently: CHANGE_ME
-->

<div class="SQmodalWindowContainer dialog" title="Confirm" id="jdialog-confirm" style="display: none;">
	
        <form id="confirm" action="">
        
        <input type="hidden" name="action[name]" id="action" value="<?php echo $action_url;?>"/>
        
        <fieldset>
            <label>Confirmation</label>
        </fieldset>
        
	</form>
    
</div>

<script type="text/javascript">
    <?php $this->start_script(); ?>

    $(function (){

        $('#sq-listingType').change(function(){
            if( $(this).val() == 'custom' )
                $('#sq-listingTypeRange').show();
            else
                $('#sq-listingTypeRange').hide();
        });

        //===== Confirm Dialogz =====//
        $( '#jdialog-confirm' ).dialog(
            {
                autoOpen: false,
                height: 250,
                width: 550,
                modal: true,
                close: function ()
                {
                    $('#campaign_id', this).val('');
                    $('form', this)[0].reset();
                },
                buttons: (new CreateDialog('change',
                    function (ajax, status)
                    {

                        if(!ajax.error)
                        {
                            $('.jnoforms').hide();
                            $('.jreports').prepend(ajax.row);
                            notie.alert({text:ajax.message});
                            this.close();
                            this.form[0].reset();
                        }
                        else if(ajax.error == 'validation')
                        {
                            alert(getValidationMessages(ajax.validation).join('\n'));
                        }
                        else
                            alert(ajax.message);

                    }, '#jdialog-confirm')).buttons
            });

    });

    <?php $this->end_script(); ?>
</script>