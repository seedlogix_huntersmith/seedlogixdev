<div class="SQmodalWindowContainer dialog" id="jdialog-saveproduct" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" id="product_id" value="" name="product[id]">
		<fieldset>

	
				<div class="fluid spacing">
					<div class="grid4"><label>Name:</label></div>
					<div class="grid8"><input type="text" name="product[name]" id="product_name" value="" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				<div class="fluid spacing">
					<div class="grid4"><label>Price:</label></div>
					<div class="grid8"><input type="text" name="product[price]" id="product_price" value="" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				<div class="fluid spacing">
					<div class="grid4"><label>Select Type:</label></div>
					<div class="grid8">

							<select data-placeholder="Choose Type&hellip;" class="select2" name="product[type]" id="product_type">
								<option></option>
									<option value="One-Time">One-Time</option>  
									<option value="Monthly">Monthly</option>  
								<option value="Annual">Annual</option>  
							</select>


					</div>
				</div>
			
				<div class="fluid spacing">
					<div class="grid4"><label>Quantity:</label></div>
					<div class="grid8"><input type="text" name="product[qty]" id="product_qty" value="" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
				<div class="fluid spacing">
					<div class="grid4"><label>Discount:</label></div>
					<div class="grid8"><input type="text" name="product[discount]" id="product_discount" value="" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
				</div>
			
			<div class="fluid spacing">
					<div class="grid4"><label>Small Description:</label></div>
					<div class="grid8"><textarea id="product_s_desc" name="product[s_desc]" ></textarea></div>
				</div>
			

	
			
		</fieldset>
	</form>
</div>