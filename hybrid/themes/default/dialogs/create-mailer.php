<div class="SQmodalWindowContainer dialog" title="Create a New Email Broadcast" id="jdialog-createmailer" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" name="broadcast[campaign_id]" id="campaign_id" value="<?= $campaign_id; ?>">
		<fieldset class="step" id="w1first">
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="broadcast[name]" placeholder="" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing">
				<div class="grid4"><label>Prospect Form:</label></div>
				<div class="grid8">
					<select name="broadcast[lead_form_id]">
<?php foreach($lead_forms as $lead_form): ?>
						<option value="<?= $lead_form->id; ?>"><?= $lead_form->name; ?></option>
<?php endforeach; ?>
					</select>
				</div>
			</div>
		</fieldset>
	</form>
</div>