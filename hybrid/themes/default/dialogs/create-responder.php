<div class="SQmodalWindowContainer dialog" title="Create a New Auto Responder" id="jdialog-createResponder" style="display: none;">
		<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
        <input type="hidden" name="autoresponder[cid]" id="campaign_id" value="<?= $Campaign->getID(); ?>">
			<input type="hidden" name="autoresponder[table_name]" value="lead_forms">
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="autoresponder[name]" placeholder="" style="margin: 0;"></div>
			</div>
			<div class="fluid spacing" id="dynamicDataRow-emailResponder">
				<div class="grid4"><label>Campaign Form:</label></div>
				<div class="grid8">
					<select data-placeholder="Choose a Campaign Form&hellip;" class="select2" name="autoresponder[table_id]" id="responder_table_id">
						<option></option>
<?php
foreach($lead_forms as $form){
	$this->getRow('responder_context_option.php',array('context' => $form));
}
?>
					</select>
				</div>
			</div>
	</form>
</div>