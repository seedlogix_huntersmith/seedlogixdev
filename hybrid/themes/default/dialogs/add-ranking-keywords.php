<div class="SQmodalWindowContainer dialog" title="Add a New Keyword" id="jdialog-addkeywords" style="display: none;">
    <form id="createCampForm" action="<?= $this->action_url("Campaign_save"); ?>">
        <input type="hidden" name="keyword[campaign_id]" id="campaign_id" value="<?= $Campaign->getID(); ?>">
        <input type="hidden" name="keyword[site_id]" id="campaign_id" value="<?= $site_id; ?>">
        <fieldset>
            <div class="fluid spacing">
                <div class="grid4"><label>Keyword(s):</label></div>
                <div class="grid8">
                    <textarea id="keywords" name="keyword[keywords]" class="text ui-widget-content ui-corner-all form-control" cols="65" style="height: 100px;"></textarea>
                    <span class="note">Add each keyword in its own line.</span>
                </div>
            </div>
        </fieldset>
	</form>
</div>