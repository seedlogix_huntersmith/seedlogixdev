<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>

<!-- Create Campaign POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Campaign" id="jdialog-createcampaign" style="display: none;">
    <form id="createCampForm" action="<?= $this->action_url("campaign_save"); ?>">
        <input type="hidden" name="campaign[id]" id="campaign_id" value="">
        <div class="fluid spacing">
            <div class="grid4"><label for="campaign_name">Campaign Name:</label></div>
            <div class="grid8"><input type="text" name="campaign[name]" id="campaign_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
    </form>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>
    
    $(document).ready(function(){
		
        $( "#jdialog-createcampaign" ).dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true,
            close: function(){
                $('#campaign_id',this).val('');
                $('form',this)[0].reset();
            },
            buttons: (new CreateDialog("Create", function (ajax, status){
				/* standards here will be 'error' var, possible values are : 'validation', 'other'
				 * if error = 'validation', check 'validation' object,
				 * if error = 'other' then print 'message''
				 * if no error or error = false, check result object. (could be a partially */ 
				if(ajax.error) {
					if(ajax.error   ==  'validation')
						alert(getValidationMessages(ajax.validation).join("\n"));
					else
						alert(ajax.message);
                }
				else {
                        // @todo prompt to go to new campaign dashboard.   
                        $('#jnocampaigns-msg').hide();
					var dt = $('#campaigns-stats-tbl');
					var newRowsIds = dt.data('newRowsIds');
					newRowsIds.unshift(ajax.object.id);
					dt.data('newRowsIds', newRowsIds);
					dt.dataTable().fnDraw();
                        this.form[0].reset();
                        notie.alert({text:ajax.message});
                        this.close();
                    }
                }, '#jdialog-createcampaign')).buttons
    	});
        
    });
    
<?php $this->end_script(); ?>
</script>