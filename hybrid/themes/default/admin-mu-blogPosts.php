<?php
/**
 * @var $feeds BlogFeedModel[]
 */
?>

<div class="fluid">
    <div class="widget">
        <div class="whead">
            <h6>MU Blog Posts</h6>
            <div class="buttonS f_rt jopendialog jopen-createpost" id="btnCreateBlogPost"><span>Create a New Blog Post</span></div>
            <select data-placeholder="Filter By&hellip;" class="select2" name="filterByCategory" id="newCategory">
                <option></option>

<?php foreach($categories as $index=>$cat): ?>

                <option value="<?php $this->p($cat); ?>"><?= $index ?></option>

<?php endforeach; ?>

            </select>
        </div>

<?php
$this->getWidget('datatable.php',array(
    'columns'           => DataTableResult::getTableColumns('masterposts'),
    'class'             => 'col_1st,col_end',
    'id'                => 'jPosts',
    'data'              => $posts,
    'rowkey'            => 'post',
    'rowtemplate'       => 'blog-muposts-row.php',
    'total'             => $totalPosts,
    'sourceurl'         => $this->action_url('admin_ajaxMuBlogs',array("feed_ID" => $_GET['feed_ID'])),
    'DOMTable'          => '<"H"lf>rt<"F"ip>',
    'serverParams'      => 'categoryDataTable'
));
?>

    </div>
</div>

<!-- Create Post POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Blog Post" id="jdialog-createpost" style="display: none;">
    <form method="post" action="<?= $this->action_url('Admin_saveBlogFeeds',array('ID' => $blog->id)); ?>" class="jform-create">
        <div class="fluid spacing">
            <div class="grid4"><label>Post Title:</label></div>
            <div class="grid8"><input type="text" name="post[post_title]" id="post_title" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
        </div>
        <div class="fluid spacing">
            <div class="grid4"><label>Category:</label></div>
            <div class="grid8 inputSwitcher">
                <div class="inputSwitcher-select">
                    <select data-placeholder="Choose a Category&hellip;" class="select2" style="width: 100%;" name="post[category]" id="selectData">
                        <option></option>
                        <optgroup label="Existing Categories" id="newCategory2">
<?php foreach($categories as $index=>$cat): ?>
                            <option value="<?= $index; ?>"><?= $index; ?></option>
<?php endforeach; ?>
                        </optgroup>
                        <optgroup label="Create Category">
                            <option value="_new">Create a New Category</option>
                            <!-- do not change value for create new category -->
                        </optgroup>
                    </select>
                </div>
                <div class="inputSwitcher-input" style="display: none;">
                    <input type="text" id="inputClear" class="text ui-widget-content ui-corner-all" style="margin: 0 0 10px;">
                    <div class="buttonS f_lt" id="btnBack"><span>Back</span></div>
                </div>
                <input type="hidden" name="post[feedID]" value="<?= $feed; ?>">
            </div>
        </div>
	</form>
</div>


<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {
        
<?php
        $this->getJ('select-category-js.php');
?>
		$('#blogfeeds-form').change(function(){
            Dashboard.ajaxSubmitForm($(this), function(ajax){
                if(ajax.error)
                    alert(ajax.error);
            });
        });

        $('select[name="filterByCategory"]').change(function(){
			$('.dTable').dataTable().fnDraw();
		});

		$("#createpost").formwizard({
			formPluginEnabled: true,
			validationEnabled: false,
			focusFirstInput : false,
			disableUIStyles : true,

			formOptions :{
				success: function(data){
					var d = JSON.parse(data);
					$('.dTable').dataTable().fnDraw();
                    notie.alert({text:d.message});
				},
				resetForm: true
			}
		});

		//===== Create New Form + PopUp =====//
		$('#jdialog-createpost').dialog(
		{
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			close: function()
			{
				$('#campaign_id',this).val('');
				$('form',this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
                function(ajax, status)
                {
                    if (!ajax.error)
                    {
                        //Refresh Select Show only post from DataTable

                        $('#newCategory option').next().remove();

                        for (i=0;i<ajax.blog_categories.length;i++) {
                            $("<option value='"+ ajax.blog_categories_values[i] +"'>" + ajax.blog_categories[i] + "</option>").appendTo("#newCategory");
                        }

                        //Refresh Select Choose Category from DataTable

                        $("#newCategory2  option").remove();

                        for (i=0;i<ajax.blog_categories.length;i++) {
                            $("<option value='"+ ajax.blog_categories[i] +"'> " + ajax.blog_categories[i] + "</option>").appendTo("#newCategory2");
                        }

//                        $('.jnoforms').hide();
//                        $('.jforms').prepend(ajax.row);
                        $.uniform.update();
                        notie.alert({text:ajax.message});
                        this.close();
                        this.form[0].reset();

						var dt = $('#jPosts');
						var newRowsIds = dt.data('newRowsIds');
						newRowsIds.unshift(ajax.object.id);
						dt.data('newRowsIds', newRowsIds);
						dt.dataTable().fnDraw();
                    }
                    else if (ajax.error == 'validation')
                    {
                        alert(getValidationMessages(ajax.validation).join('\n'));
                        $('#post_title').focus();
                    }
                    else
                        alert(ajax.message);

                }, '#jdialog-createpost')).buttons
		});

	}); //close self invoking function

function categoryDataTable(aoData){
	var category = $('select[name="filterByCategory"]').val();
	aoData.push({name : "category", value : category});
}

<?php $this->end_script(); ?>

</script>