<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Custom Forms</h6>
			<div class="buttonS f_rt jopendialog jopen-createform"><span>Create a New Form</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('LeadForms'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'forms',
	'data'				=> $masterForms,
	'rowkey'			=> 'leadform',
	'rowtemplate'		=> 'admin-mu-forms.php',
	'total'				=> $totalMasterForms,
	'sourceurl'			=> $this->action_url('admin_ajaxMUForms',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<div class="SQmodalWindowContainer dialog" title="Create a New Form" id="jdialog-createform" style="display: none;">
	<form method="post" action="<?= $this->action_url('Campaign_save'); ?>" class="jform-create">
		<input type="hidden" value="1" name="form[multi_user]">
		<fieldset>
			<div class="fluid spacing">
				<div class="grid4"><label>Name:</label></div>
				<div class="grid8"><input type="text" name="form[name]" id="form_name" class="text ui-widget-content ui-corner-all" style="margin: 0;"></div>
			</div>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {

		//===== Create New Form + PopUp =====//
		$('#jdialog-createform').dialog(
		{
			autoOpen: false,
			height: 150,
			width: 300,
			modal: true,
			close: function()
			{
				$('#campaign_id',this).val('');
				$('form',this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
							function(ajax, status)
							{

								if (!ajax.error)
								{
									notie.alert({text:ajax.message});
									var dt = $('#forms');
									var newRowsIds = dt.data('newRowsIds');
									newRowsIds.unshift(ajax.object.id);
									dt.data('newRowsIds', newRowsIds);
									this.close();
									this.form[0].reset();
									dt.dataTable().fnDraw(true);
								}
								else if (ajax.error == 'validation')
								{
									alert(getValidationMessages(ajax.validation).join('\n'));
								}
								else
									alert(ajax.message);

							}, '#jdialog-createform')).buttons
		});

	});

<?php $this->end_script(); ?>
</script>