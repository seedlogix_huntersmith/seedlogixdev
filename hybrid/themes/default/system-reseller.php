<?php
/**
 *	@var $this Template
 */
$edit = $User->can('edit_reseller') ? "" : "disable";

// create upload dir if it does not exist.
UserModel::getStorage($Branding->admin_user);
?>

<div class="fluid">

    <form action="<?php echo $this->action_url('System_save?ID=' . $reseller->getID()); ?>" method="POST" class="ajax-save" enctype="multipart/form-data">
        <div class="widget grid6">
            <div class="whead"><h6>Company Information</h6></div>
            <div class="formRow">
                <div class="grid3"><label>Company Name</label></div>
                <div class="grid9"><input type="text" name="info[company]" value="<?php $this->p($reseller->company); ?>" <?=$edit?>></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Main Site</label></div>
                <div class="grid9"><input type="text" name="info[main_site]" value="<?php $this->p($reseller->main_site); ?>" <?=$edit?>>
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Support e-mail</label></div>
                <div class="grid9"><input type="text" name="reseller[support_email]" value="<?php $this->p($reseller->support_email); ?>" <?=$edit?>>
                </div>
            </div>
        </div>

                    <div class="widget grid6">
                        <div class="whead"><h6>Main Settings</h6></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Admin user:</label></div>
                        	<div class="grid9"><input type="text"
                                                          name="reseller[admin_user]"
                                                          value="<?= $reseller->admin_user; ?>" <?=$edit?>/>

                                <br /><a href="<?= $this->action_url('UserManager_view?ID=' . $reseller->admin_user); ?>" <?=$edit?>><?php
                                    echo $reseller->username; ?></a>

                                </div>
                    	</div>
                        <div class="formRow">
                        	<div class="grid3"><label>Main Site ID:</label></div>
                        	<div class="grid9"><input type="text"
                                                          name="reseller[main_site_id]"
                                                          value="<?= $reseller->main_site_id; ?>" <?=$edit?>/>
                                <br /><a href="<?= $this->action_url('Website_dashboard?ID=' . $reseller->main_site_id); ?>"><?php
                                    echo $reseller->site_name; ?></a>

                                </div>
                    	</div>
                    </div>

        <div class="widget grid6">
            <div class="whead"><h6>Files</h6></div>

            <div class="formRow">
                <div class="grid3"><label>Background Image:</label> </div>
                <div class="grid9">
                <?php
                    $this->getWidget('moxiemanager-filepicker.php', array(
                        'name' => 'reseller[bg-img]',
                        'value' => $reseller->get('bg-img'),
                        'idWidget' => 'bg-img',
                        'path' => $storagePath,
                        'noHost' => true,
                        'extensions' => 'jpg,jpeg,png,gif',
                        'display' => empty($edit)
                    ));
                ?>
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Logo Image:</label></div>
                <div class="grid9">
                <?php
                    $this->getWidget('moxiemanager-filepicker.php', array(
                        'name' => 'reseller[logo]',
                        'value' => $reseller->get('logo'),
                        'idWidget' => 'logo',
                        'path' => $storagePath,
                        'noHost' => true,
                        'extensions' => 'jpg,jpeg,png,gif',
                        'display' => empty($edit)
                    ));
                ?>
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Favicon Image:</label> </div>
                <div class="grid9">
                <?php
                    $this->getWidget('moxiemanager-filepicker.php', array(
                        'name' => 'reseller[favicon]',
                        'value' => $reseller->get('favicon'),
                        'idWidget' => 'favicon',
                        'path' => $storagePath,
                        'noHost' => true,
                        'extensions' => 'jpg,jpeg,png,gif',
                        'display' => empty($edit)
                    ));
                ?>
                </div>
            </div>

        </div>
    </form>
</div>

<script>
<?php $this->start_script(); ?>
function updateImageFunction(inputname, preview) {
	return function (args) {
		var form = $('form');
		var input = $('input[name="' + inputname + '"]', form);
		var newValue = args.files[0].url;
		input.val(args.files[0].url);
		new AjaxSaveDataRequest(input.get(0), newValue);

		var image = $('#' + preview + ' img');
		if(image.length == 0){
			$('#' + preview).append('<img class="preview" src="<?=UserModel::USER_DATA_DIR?>/"/>')
		}
		image.attr('src', 'users' + newValue);
	}
}

<?php $this->end_script(); ?>
</script>
