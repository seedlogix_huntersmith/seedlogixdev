<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>Billing info for <?=$Branding->company?></h6>
			<p style="position:relative; top:-4px;">History of items we've attempted to bill you for.</p>
		</div>
		<div id="dyn">
			<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jBilling">
				<thead>
				  <tr>
					<td>Date</td>
					<td>Type</td>
					<td>Transaction ID</td>
					<td>Amount</td>
					<td>Success</td>
					<td width="100">Actions</td>
				  </tr>
				</thead>
				<tbody class="jbilling">
<?
           		$this->getRow('admin-billing.php');
?>
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /end fluid -->

<div class="fluid"> 
	<div class="widget check">
		<div class="whead">
			<h6>User Billing info for _COMPANY_NAME_</h6>
			<p style="position:relative; top:-4px;">History of items we've charged your users for on your behalf.</p>
		</div>
		<div id="dyn">
			<table cellpadding="0" cellspacing="0" width="100%" class="tDefault dTable" id="jBilling">
				<thead>
				  <tr>
					<td>Date</td>
					<td>Type</td>
					<td>Transaction ID</td>
					<td>Amount</td>
					<td>Success</td>
					<td width="100">Actions</td>
				  </tr>
				</thead>
				<tbody class="jbilling"> 
<?
           		$this->getRow('admin-userBilling.php');
?>
				</tbody>
			</table>
		</div>
	</div>
</div><!-- /end fluid -->

<!-- Dynamic Data Modal -->
<div class="SQmodalWindowContainer dialog" title="Billing Details" id="viewDetails" style="display: none;">
	<p><strong>For product:</strong> <span id="forProduct"></span></p>
	<p><strong>For user:</strong> <span id="forUser"></span></p>
	<p><strong>Notes:</strong> <span id="notes"></span></p>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		
		//populating and displaying details view modal
		var detailProduct = $('#forProduct');
		var detailUser = $('#forUser');
		var detailNote = $('#notes');
		
		//===== Create New Form + PopUp =====//
		$( '#viewDetails' ).dialog(
		{
			autoOpen: false,
			height: 200,
			width: 550,
			modal: true,
			close: function( event, ui ) {
				detailProduct.html('');
				detailUser.html('');
				detailNote.html('');
			}
		});
		
		$('.viewDetails').click(function(){
			
			var product = $(this).find('.forProduct').html();
			var user = $(this).find('.forUser').html();
			var notes = $(this).find('.notes').html();
			
			detailProduct.html(product);
			detailUser.html(user);
			detailNote.html(notes);
			
			$( '#viewDetails' ).dialog( "open" );
			
		});
	
	});
    
<?php $this->end_script(); ?>
</script>