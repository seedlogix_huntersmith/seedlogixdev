<div class="fluid">
    <div class="widget">
        <div class="whead"><h6>Total Sales</h6></div>
        <table id="jTotalSales" class="tDefault dTable col_1st col_2nd" style="width: 100%;">
            <thead class="dataTableHead">
                <tr>
                    <td>Sales Professional</td>
                    <td>Company</td>
                    <td>Product</td>
                    <td>Sale Total</td>
                    <td>Commission %</td>
                    <td>Commission Total</td>
                    <td>Date</td>
                </tr>
            </thead>
            <tbody class="dataTableBody">

<?php
if(isset($saleData) && is_array($saleData)){
    foreach($saleData as $s_d => $row){
        $this->getRow('dashboard-salesTotal.php',array('rowData' => $row,'commission' => $commission));
    }
}
?>

            </tbody>
        </table>
    </div>
    <div class="widget">
        <div class="whead"><h6>Clients Sold</h6></div>
        <table id="jClientsSold" class="tDefault dTable col_1st col_3rd col_end" style="width: 100%;">
            <thead class="dataTableHead">
                <tr>
                    <td>Company</td>
                    <td>Product</td>
                    <td>Sold By</td>
                    <td>Date</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody class="dataTableBody">

<?php
if(isset($clientsData) && is_array($clientsData)){
    foreach($clientsData as $c_d => $row){
        $this->getRow('dashboard-salesClients.php',array('rowData' => $row));
    }
}
?>

            </tbody>
        </table>
    </div>

<?php if($isManager): ?>

    <div class="widget">
        <div class="whead"><h6>Sales Team</h6></div>
        <table id="jSalesTeam" class="tDefault dTable col_1st" style="width: 100%;">
            <thead class="dataTableHead">
                <tr>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Team Role</td>
                    <td>Join Date</td>
                </tr>
            </thead>
            <tbody class="dataTableBody">

<?php
if(isset($teamData) && is_array($teamData)){
    foreach($teamData as $t_d => $row){
        $this->getRow('dashboard-salesTeam.php',array('rowData' => $row));
    }
}
?>

            </tbody>
        </table>
    </div>

<?php endif; ?>

</div><!-- /.fluid -->

<script type="text/javascript">

<?php $this->start_script(); ?>

$(function(){

    $('#jTotalSales').dataTable({
        "aLengthMenu"       : [5,10,25,50,75,100],
        "language"          : {
                            "search"            : "_INPUT_",
                            "searchPlaceholder" : "Search…"
                            },
        "pagingType"        : "simple",
        "bProcessing"       : true,
        "iDisplayLength"    : 10,
        "bDestroy"          : true,
        "aoColumns"         : [null,null,null,null,null,null,null],
        "aaSorting"         : [[6,'desc']],
        "sDom"              : '<"tablePars"lf>rt<"fg-toolbar tableFooter"ip>'
    });

    $('#jClientsSold').dataTable({
        "aLengthMenu"       : [5,10,25,50,75,100],
        "language"          : {
                            "search"            : "_INPUT_",
                            "searchPlaceholder" : "Search…"
                            },
        "pagingType"        : "simple",
        "bProcessing"       : true,
        "iDisplayLength"    : 10,
        "bDestroy"          : true,
        "aoColumns"         : [null,null,null,null,{"bSortable":false}],
        "aaSorting"         : [[3,'desc']],
        "sDom"              : '<"tablePars"lf>rt<"fg-toolbar tableFooter"ip>'
    });

<?php if($isManager): ?>

    $('#jSalesTeam').dataTable({
        "aLengthMenu"       : [5,10,25,50,75,100],
        "language"          : {
                            "search"            : "_INPUT_",
                            "searchPlaceholder" : "Search…"
                            },
        "pagingType"        : "simple",
        "bProcessing"       : true,
        "iDisplayLength"    : 10,
        "bDestroy"          : true,
        "aoColumns"         : [null,null,null,null,null],
        "aaSorting"         : [[4,'desc']],
        "sDom"              : '<"tablePars"lf>rt<"fg-toolbar tableFooter"ip>'
    });

<?php endif; ?>

});

<?php $this->end_script(); ?>

</script>