<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

?>
<div id="twoColContentArea">

<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">

            <div class="whead">
            	<h6>Example View</h6>
            	<div class="clear"></div>
            </div>
    
            <?php
            
                    echo "USE \$this->p(\$var); to print html variables and prevent XSS.<strong>";
                    
                    $this->p($test);
                    
                    echo '</strong>';
            ?>
            <div class="clear"></div> 
</div>


    <div class="widget">
            <div class="whead">
            	<h6>More Examples</h6>
            	<div class="clear"></div>
            </div>
            
            <?php
                    // Example, global variables
                    
            ?>
        <pre><?php var_dump($User, $Branding, $_path, $menu, $this->getaction(), $this->isAction('home') ? ' the action is home' : ' the action is not home'); ?></pre>
            
            <div class="clear"></div> 
        </div>



<!-- //end content container -->
</div>
