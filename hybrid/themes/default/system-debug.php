<?php

//    var_dump($events);
?>
<div class="fluid">
    <div class="widget">
        <div class="whead"><h6>System Debug Console</h6></div>
        <pre><?php 
                $check_PERMS  = SystemController::getAllPermissions(Qube::Start())->fetchAll(PDO::FETCH_OBJ);
                
                foreach($check_PERMS as $perm){
                    foreach(array(0, 1, 3) as $id){
                        echo "$perm->flag ($id) = ", $User->can($perm->flag, $id), "\n";
                    }
                }
            
            
                var_dump($User); ?>
        </pre>
    </div> 
</div>

