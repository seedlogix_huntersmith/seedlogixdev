<?php
/*
include_once(HYBRID_PATH.'classes/colors.inc.php');
$gc		= new GetMostCommonColors();
$gc_arr	= $gc->Get_Color(ltrim($Branding->getLogoUrl(false),'/'),1,true,true,1);
*/
?>

<div class="loginWrapper">

<?php //if(isset($gc_arr) && is_array($gc_arr)): foreach($gc_arr as $hex => $count): if($count > 0): ?>

	<div class="brandlogo" style="<?php //$hex; ?>"><img src="<?= $Branding->getLogoUrl(); ?>" alt=""></div>

<?php //endif; endforeach; endif; ?>

	<h6>Welcome to</h6>
	<h2><?= $Branding->company; ?></h2>
    <form action="<?= $this->action_url('Auth_login'); ?>" id="login" method="POST">
		<span class="email"></span>
		<input type="text" name="login" placeholder="Email">
		<span class="password"></span>
		<input type="password" name="password" placeholder="Password">
		<div class="checkbox">
			<input type="checkbox" name="remember" id="remember">
			<label for="remember"><span>Remember Me</span></label>
		</div>
		<input type="hidden" id="SL_ForgotPassword" name="forgot" value="">
		<button type="submit" name="sl_submit"><span>Sign In</span></button>
	</form>
	<a id="forgot"><span>Forgot your Password?</span></a>

<?php if(isset($messages) && is_array($messages)): foreach($messages as $msg): ?>

<?= $msg->body; ?>

<?php endforeach; endif; ?>

</div>