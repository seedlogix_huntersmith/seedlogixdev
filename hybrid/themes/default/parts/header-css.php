
<style type="text/css">
body{ overflow-y: scroll; }
ul.prospects li.comment p { width:95%; }
<?php if($_SESSION['theme']){
		//nav-bg 
		$a = explode('_', $_SESSION['thm_nav-bg2']);
		$navbgcolor = $a[1];
		//app-right-link
		$a = explode('_', $_SESSION['thm_app-right-link']);
		$apprightlinkcolor = $a[1];
		//app-right-link2
		$a = explode('_', $_SESSION['thm_app-right-link2']);
		$apprightlinkcolor2 = $a[1];
	?>
	
			/* header/body */
				<?php if($_SESSION['thm_bg-img']){ ?>
				html,body { background: <?=$_SESSION['thm_bg-clr']?> url('<?php echo $_path; ?><?=$_SESSION['themedir'].$_SESSION['thm_bg-img']?>') <?=$_SESSION['thm_bg-img-rpt']?> top; }
				<?php } else { ?>
				html,body { background-color:<?=$_SESSION['thm_bg-clr']?>;}
				<?php } ?>
			#admin-panel { background-color:<?=$_SESSION['thm_adminbar-clr']?>; color:<?=$_SESSION['thm_userbar-text']?>; }
			#header p.slogan {width:350px; height:64px; background:url('<?php echo $_path; ?><?=$_SESSION['themedir'].$_SESSION['thm_logo']?>') no-repeat;  margin:15px 0 0 20px; float:left;}
			
			/* left navigation */
			#navigation ul.top li a {background:<?=$_SESSION['thm_left_nav_active']?> url('<?php echo $_path; ?>img/v3/private/non-active-bg.png') no-repeat; color:<?=$_SESSION['thm_nav-links3']?>; text-decoration:none; text-shadow:none;}
			#navigation ul.top li a.hover {color:<?=$_SESSION['thm_nav-links']?>; text-shadow:<?=$_SESSION['thm_nav-links4']?> 1px 1px;}
			#navigation ul.child li a {background-color:<?=$_SESSION['thm_left_nav_slide']?>; background-image:none; color:<?=$_SESSION['thm_nav-links2']?>; text-shadow:none;}
			#navigation ul.child li a:hover{ color:<?=$_SESSION['thm_nav-links3']?>; text-shadow:none;}
			#navigation ul.child li a .child-link p{color:<?=$_SESSION['thm_nav-links3']?>; text-shadow:none;}
			
			/* main link color */
			a.dflt-link { color:<?=$_SESSION['thm_main_link_color']?>; }
			#header #user-bar p a { color:<?=$_SESSION['thm_main_link_color']?>;}
			#header #user-bar p a:hover { color:<?=$_SESSION['thm_main_link_hover']?>;}
			.header-nav ul li a:hover {color:<?=$_SESSION['thm_main_link_color']?>;}
			.header-nav ul li a.current {color:<?=$_SESSION['thm_main_link_color']?>;}
			.header-nav ul li a.current:hover {color:<?=$_SESSION['thm_main_link_color']?>;}
			.block-right a.blog_title:hover { color:<?=$_SESSION['thm_main_link_hover']?>; }
			#dash .item a {color:<?=$_SESSION['thm_main_link_color']?>; }
			#dash .item:hover a {color:<?=$_SESSION['thm_main_link_hover']?>; }
			#dash .item2 a {color:<?=$_SESSION['thm_main_link_color']?>; }
			#dash .item2:hover a {color:<?=$_SESSION['thm_main_link_hover']?>; }
			#dash .item3 a {color:<?=$_SESSION['thm_main_link_color']?>; }
			#dash .item3:hover a {color:<?=$_SESSION['thm_main_link_hover']?>; }
			#subform-nav  li a:hover {color:<?=$_SESSION['thm_main_link_color']?>;}
			#subform-nav  li a.current {color:<?=$_SESSION['thm_main_link_color']?>; }
			#subform-nav  li a.current:hover {color:<?=$_SESSION['thm_main_link_color']?>;}
			#ajax-load .contactDetails h2 {color:<?=$_SESSION['thm_main_link_color']?>;}
			#ajax-load .noteDetails h2 {color:<?=$_SESSION['thm_main_link_color']?>; font-size:32px; }
			#ajax-load .leadDetails h2 {color:<?=$_SESSION['thm_main_link_color']?>; font-size:22px; }
			ul.prospects a { color:<?=$_SESSION['thm_main_link_color']?>; }
			.block-right a.blog_title { color:<?=$_SESSION['thm_main_link_color']?>; }
			.block-right a.blog_title:hover { color:<?=$_SESSION['thm_main_link_hover']?>; }
			
			/* custom campaigns panel */
			.custom-block {  border:1px solid <?=$_SESSION['thm_camp-pnl-brdr-clr']?>;  }
			.custom-block h3 {color:<?=$_SESSION['thm_nav-links']?>; background:<?=$_SESSION['thm_left_nav_active']?>  url('<?php echo $_path; ?>img/v3/private/title-bar.png') bottom left;}
			.block-right {  border:1px solid <?=$_SESSION['thm_camp-pnl-brdr-clr']?>;  }
			.block-right h4 {border-bottom: 1px dashed <?=$_SESSION['thm_camp-pnl-brdr-clr']?>;   }
			
			/* forms */
			label { color:<?=$_SESSION['thm_form-lbl-clr']?>; }
			.jquery_form input { <?php if($_SESSION['thm_form-input-clr']){ ?>background-image:none; background-color:<?=$_SESSION['thm_form-input-clr']?>;<?php } else { ?><?php } ?> border:1px solid <?=$_SESSION['thm_form-input-brdr-clr']?>; }
			.jquery_form textarea { border: 1px solid <?=$_SESSION['thm_form-input-brdr-clr']?>; }
			#createDir input[type="text"] { <?php if($_SESSION['thm_form-input-clr']){ ?>background-image:none; background-color:<?=$_SESSION['thm_form-input-clr']?>;<?php } else { ?><?php } ?> border:1px solid <?=$_SESSION['thm_form-input-brdr-clr']?>;}
			#camps input[type="text"] { <?php if($_SESSION['thm_form-input-clr']){ ?>background-image:none; background-color:<?=$_SESSION['thm_form-input-clr']?>;<?php } else { ?><?php } ?> border:1px solid <?=$_SESSION['thm_form-input-brdr-clr']?>;}
			.img_cnt { border:9px solid <?=$_SESSION['thm_form-img-brdr-clr']?>;  }
			
			/* others */
			#app_right_panel h2 {color:<?=$_SESSION['thm_nav-links']?>; background:<?=$_SESSION['thm_left_nav_active']?>  url('<?php echo $_path; ?>img/v3/private/title-bar.png') bottom left;}
			/* app right panel links */
			.app_right_links ul li a { color:#<?=$apprightlinkcolor?>; background:url('<?php echo $_path; ?>img/app-right-icon/<?=$_SESSION['thm_app-right-link']?>.png') no-repeat 8px 3px; }
			.app_right_links ul li a:hover { color:#<?=$apprightlinkcolor2?>; background:url('<?php echo $_path; ?>img/app-right-icon/<?=$_SESSION['thm_app-right-link2']?>.png') no-repeat 8px 3px; }
			
			
			
<?php  } //end if($_SESSION['theme'])
else { ?>
			/* html,body { background:#E1E1E1 url('img/v3/gradient-top.gif') repeat-x; } */
			#header h1, #header .slogan { cursor: pointer; }
			
<?php } ?>
<?php if($_SESSION['reseller'] || $_SESSION['admin']){ 
	if($_SESSION['theme']){
		echo '#admin-panel { background-color:#'.$_SESSION['thm_adminbar-clr'].'; color:#'.$_SESSION['thm_userbar-text'].' }';
		$bartop = '30';
	}
	
	else $bartop = '27';
?>
	html,body{ background-position:0px 25px; }
	#wrapper,#header, #user-bar { top:<?=$bartop?>px; }
<?php } //end if($_SESSION['reseller'] || $_SESSION['admin']) ?>
</style>