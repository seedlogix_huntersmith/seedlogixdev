<div id="userBar">
	<ul id="userBarNav">
	<? 
		if($_SESSION['reseller'] || $_SESSION['admin'] || $_SESSION['user_manager'])
		{ 
			$permissions = new UserPermissions();
			
			if
			( 
				$permissions->getManagedUsers($_SESSION['parent_id'], $_SESSION['login_uid']) ||
				$reseller->getResellerUsers($_SESSION['login_uid'], null, null) ||
				$_SESSION['admin']
			)
			{
				echo '<li><a href="<?php echo $_path; ?>#" id="manageUsersSelect" class="SQdropDown" ddid="SQswitchUsers"><span class="manageUsersTxt">Manage Users</span></a></li>';
			}
			
		}
	?>
		
		<!-- 
			COMMNENT OUT FOR NOW
			untill we develope user notifications
			
		<li><a href="<?php echo $_path; ?>" id="msgAlerts"><span class="msgAlertsTxt"><strong class="numberOfAlerts">43</strong> <span class="alertsTxt">Total New Leads</span></span></a></li>
		-->
		<li><a href="<?php echo $_path; ?>#" id="userSettings" class="SQdropDown" ddid="SQuserSettingOptions"><span class="userNameCnt"><?=$_SESSION['user_fullName']?></span></a></li>
	</ul>
</div>

<div id="SQuserSettingOptions" class="userMngDD SQdropDownCnt hide">
	<ul class="dropdown-menu nav user_menu pull-right">
		<li><a class="ajax" href="<?php echo $_path; ?>account.php">Account Settings</a></li>
		<li><a class="ajax" href="<?php echo $_path; ?>account.php">User Profile</a></li>
		<li><a class="ajax" href="<?php echo $_path; ?>account.php">Billing</a></li>
		<li class="divider"></li>
		<li><a href="<?php echo $_path; ?>logout.php">Logout</a></li>
	</ul>
</div>

<div id="SQswitchUsers" class="userMngDD SQdropDownCnt SQalignRight hide">
	<ul class="dropdown-menu">
	<li><a href="<?php echo $_path; ?>admin-users.php" class="ajax dropDownHighlight">Show All Users</a></li>
	<li><a href="<?php echo $_path; ?>#" rel="<?=$_SESSION['FIRST_login_uid']?>" class="manage-user"><?=$_SESSION['FIRST_user_fullName']?></a></li>
	<li class="divider"></li>
		<?
			if($_SESSION['user_manager']) //IF USER MANAGER
			{
				$permissions->outputManagedUsers($_SESSION['parent_id'], $_SESSION['login_uid'], $_SESSION['user_id'], $_SESSION['login_user'], $_SESSION['user']);
			}
			else if($_SESSION['reseller'] || $_SESSION['admin']) //IF RESELLER OR ADMIN
			{
				$reseller->displayUsersHTML($_SESSION['admin'], $_SESSION['login_uid'], $_SESSION['user_id'], $_SESSION['login_user'], $_SESSION['user'], 20);
			}
		?>
	</ul>
</div>