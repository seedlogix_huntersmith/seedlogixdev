<?php
    $cond1  =   (($_SESSION['reseller'] || $_SESSION['reseller_client']) && $_SESSION['thm_custom-backoffice']);
    
    $cond2  =   ($_SESSION['reseller_client'] || ($_SESSION['reseller'] && $_SESSION['user_id']!=$_SESSION['login_uid'])) && !$_SESSION['admin'] && $_SESSION['payments']!='other' && !$_SESSION['support_user'];
    $cond3  =   $_SESSION['reseller'] && !$_SESSION['reseller_client'];
    $cond4  =   $_SESSION['admin'] && !$_SESSION['support_user'];
    $cond5  =   $_SESSION['admin'];
    
    ?>
        <ul class="nav">
            <li><a href="index.html" title="" class="active"><img src="images/icons/mainnav/dashboard.png" alt="" /><span>Dashboard</span></a></li>
            <li><a href="ui.html" title=""><img src="images/icons/mainnav/ui.png" alt="" /><span>UI elements</span></a>
                <ul>
                    <li><a href="ui.html" title=""><span class="icol-fullscreen"></span>General elements</a></li>
                    <li><a href="ui_icons.html" title=""><span class="icol-images2"></span>Icons</a></li>
                    <li><a href="ui_buttons.html" title=""><span class="icol-coverflow"></span>Button sets</a></li>
                    <li><a href="ui_grid.html" title=""><span class="icol-view"></span>Grid</a></li>
                    <li><a href="ui_custom.html" title=""><span class="icol-cog2"></span>Custom elements</a></li>
                    <li><a href="ui_experimental.html" title=""><span class="icol-beta"></span>Experimental</a></li>
                </ul>
            </li>
            <li><a href="forms.html" title=""><img src="images/icons/mainnav/forms.png" alt="" /><span>Forms stuff</span></a>
                <ul>
                    <li><a href="forms.html" title=""><span class="icol-list"></span>Inputs &amp; elements</a></li>
                    <li><a href="form_validation.html" title=""><span class="icol-alert"></span>Validation</a></li>
                    <li><a href="form_editor.html" title=""><span class="icol-pencil"></span>File uploader &amp; WYSIWYG</a></li>
                    <li><a href="form_wizards.html" title=""><span class="icol-signpost"></span>Form wizards</a></li>
                </ul>
            </li>
            <li><a href="messages.html" title=""><img src="images/icons/mainnav/messages.png" alt="" /><span>Messages</span></a></li>
            
            <li><a href="tables.html" title=""><img src="images/icons/mainnav/tables.png" alt="" /><span>Tables</span></a>
                <ul>
                    <li><a href="tables.html" title=""><span class="icol-frames"></span>Standard tables</a></li>
                    <li><a href="tables_dynamic.html" title=""><span class="icol-refresh"></span>Dynamic table</a></li>
                    <li><a href="tables_control.html" title=""><span class="icol-bullseye"></span>Tables with control</a></li>
                    <li><a href="tables_sortable.html" title=""><span class="icol-transfer"></span>Sortable and resizable</a></li>
                </ul>
            </li>
            <li><a href="other_calendar.html" title=""><img src="images/icons/mainnav/other.png" alt="" /><span>Other pages</span></a>
                <ul>
                    <li><a href="other_calendar.html" title=""><span class="icol-dcalendar"></span>Calendar</a></li>
                    <li><a href="other_gallery.html" title=""><span class="icol-images2"></span>Images gallery</a></li>
                    <li><a href="other_file_manager.html" title=""><span class="icol-files"></span>File manager</a></li>
                    <li><a href="#" title="" class="exp"><span class="icol-alert"></span>Error pages <span class="dataNumRed">6</span></a>
                        <ul>
                            <li><a href="other_403.html" title="">403 error</a></li>
                            <li><a href="other_404.html" title="">404 error</a></li>
                            <li><a href="other_405.html" title="">405 error</a></li>
                            <li><a href="other_500.html" title="">500 error</a></li>
                            <li><a href="other_503.html" title="">503 error</a></li>
                            <li><a href="other_offline.html" title="">Website is offline error</a></li>
                        </ul>
                    </li>
                    <li><a href="other_typography.html" title=""><span class="icol-create"></span>Typography</a></li>
                    <li><a href="other_invoice.html" title=""><span class="icol-money2"></span>Invoice template</a></li>
                </ul>
            </li>
            
            <li><a href="<?php echo $_path; ?>support.php" title=""><img src="images/icons/mainnav/statistics.png" alt="" /><span>Statistics</span></a></li>
            <li class="last"><a href="" rev="support">Support</a></li>
        </ul>
<?php return; ?>
    




<?php if($cond1){ ?>
<li class="first"><a href="<?php echo $_path; ?>home.php" <? if(!$_GET['rtrn']) echo 'class="current"'; ?> rev="home">Home</a></li>
<?php if($_SESSION['login_uid']==3558 || $_SESSION['user_class']==387){ ?>
<li class="first"><a href="<?php echo $_path; ?>affiliates.php" rev="affiliates" >My Team</a></li>
<? } ?>
<?php if(!$_SESSION['backoffice_user']){ ?><li><a href="<?php echo $_path; ?>campaigns.php" rev="campaigns">Campaigns</a></li><? } ?>
<? } 

if(!$cond1){ ?>
<li class="first"><a href="<?php echo $_path; ?>campaigns.php" <? if(!$_GET['rtrn']) echo 'class="current"'; ?> rev="campaigns" id="header-campaigns">Campaigns</a></li>
<? } ?>

<?php if($cond2){ ?>
<li><a href="<?php echo $_path; ?>reseller-client-billing.php" rev="billing">Billing</a></li>
<? } else if($_SESSION['sixqube_client'] && ($_SESSION['user_class']==1 || $_SESSION['user_class']==178 || $_SESSION['user_class']==421)){ ?>
<li><a href="<?php echo $_path; ?>6qube-client-billing.php" rev="billing">Upgrade</a></li>
<? } else if($_SESSION['sixqube_client'] && ($_SESSION['user_class']>20)){ ?>
<li><a href="<?php echo $_path; ?>6qube-client-billing.php" rev="billing">Billing</a></li>
<? } ?>

<?php if($cond3){ ?>

<li><a href="<?php echo $_path; ?>reseller.php" <? if($_GET['rtrn']) echo'class="current"'; ?> rev="reseller">Admin</a></li>

<? } 
else if($cond4){ ?>
<li><a href="<?php echo $_path; ?>admin-resellers.php" rev="admin">Admin</a></li>
<? } else if($cond5) {?>
<li><a href="<?php echo $_path; ?>admin-users.php" rev="admin">Admin</a></li>
<? } ?>
