
<script type="text/javascript" language="javascript">

$(document).ready(function() {
	
	/***************
	GLOBAL VARIABLES
	***************/
	//used for custom drop down function
	ddOpen = 0;
	
	<? if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && $_SESSION['thm_custom-backoffice']){ ?>
	//nav state
	var nav_state = 'home';
	<? } else { ?>
	var nav_state = 'campaigns';
	<? } ?>
	
	
	/*
	*	::MODULE:: Custom Drop Down Box
	*/
	$(".SQdropDown").live('click', function(event){
				
		//hide any currently open drop downs
		$('.SQdropDownCnt').hide();
		
		var element = $(this);
		
		if(ddOpen != element.attr('id'))
		{
			var dropDownContainer = $('#'+element.attr("ddid"));
			var position = element.position();
			var dropDownPositionX = position.left;
			var dropDownPositionY = position.top + element.height();
			var elementPaddingY = parseInt(element.css('padding-top')) + parseInt(element.css('padding-bottom'));
			var elementPaddingX = parseInt(element.css('padding-left')) + parseInt(element.css('padding-right'));
			
			//set the width of the drop down menu
			dropDownContainer.css('min-width', elementPaddingX+element.width()+'px').css('width', 'auto');
			
			//if they want to align the drop down container to the right of the menu
			if(dropDownContainer.hasClass('SQalignRight'))
			{
				//refactor
				dropDownContainer.css('left', dropDownPositionX+'px').css('top', elementPaddingY+dropDownPositionY+'px');
			}
			else
			{
				dropDownContainer.css('left', dropDownPositionX+'px').css('top', elementPaddingY+dropDownPositionY+'px');
			}
			
			dropDownContainer.show();
			ddOpen = element.attr('id');
				
		}
		else
		{
			ddOpen = 0;
		}
		
		return false;
	});
	
	//observe onClick of body - used to close drop down containers
	$('body').click(function(){
		
		var element = $(this).hasClass('SQdropDown');
		
		if(element || ddOpen)
		{
			$('.SQdropDownCnt').hide();
			ddOpen = 0;
		}
		
	});
	
	
	/*
	*	::MODULE:: Modal Window Controller
	*/
	//SQmodalDialog
	$(".SQmodalDialog").live('click', function(event){
		
		var element = $(this);
		var modalContainer = $('#'+element.attr("mwid")); 
		
		modalContainer.dialog({
			height: 140,
			modal: true
		});
	});
	
	
	//SQmodalMessage
	$(".SQmodalMessage").live('click', function(event){
		
		var element = $(this);
		var modalContainer = $('#'+element.attr("mwid")); 
		
		modalContainer.dialog({
			height: 140,
			modal: true
		});
	});
	
	
	//SQmodalConfirmation
	$(".SQmodalConfirmation").live('click', function(event){
		
		var element = $(this);
		var modalContainer = $('#'+element.attr("mwid")); 
		
		modalContainer.dialog({
			height: 140,
			modal: true
		});
	});
	
	//SQmodalForm
	$(".SQmodalForm").live('click', function(event){
		
		var element = $(this);
		var modalContainer = $('#'+element.attr("mwid")); 
		
		modalContainer.dialog({
			height: 140,
			modal: true
		});
	});
	
	
	/*
		Updated by $Corfro june 24th, 2012
		
		Description:
		added error messaging instead of js alerts 
		(left in js alerts for legacy code the doesn't support the new error messaging)
		and cleaned up some of the formatting
		
	*/
	
	$('.chzn-single').live('click', function(e){
		e.preventDefault();
    	e.stopPropagation();
    	return false;
	});
	
	//submit form 
	$("form.ajax").live('submit', function(event){
		if(!$(this).hasClass('bypass')){
		event.stopPropagation();
			params = $(this).serialize();
			$.post($(this).attr("action"), params, function(data)
			{
				<? if($_SESSION['login_uid']==496){ ?>
				//alert(data);
				<? } ?>
				
				json = $.parseJSON(data);
				
				//SUCCESS
				if(json.success)
				{
					if(json.success.action == 'load')
					{
						$('#wait').show();
						loadPage(json.success.page);
					}
					else if(json.success.action == 'replace')
					{
						$(json.success.container).html(json.success.message);
					}
					else
					{
						$(json.success.container).prepend(json.success.message);
						$(json.success.container).find(".item:first").slideDown();
					}
				}
				//ERROR
				else if(json.error.message)
				{
					//instead of a js alert display an error message on the page
					/*
						EXAMPLE CODE:
						
						HTML:
						<div id="formErrorMessageContainer" class="hide"></div>
						
						PHP JSON:
						$response['error'] = '1';
						$response['error']['type'] = 'displayMessage';
						$response['error']['message'] = '<div id="formErrorMessageContainer"><span class="error">There was an error</span></div>';
						$response['error']['container'] '#formErrorMessageContainer';
					*/
					if(json.error.type = 'displayMessage')
					{
					
						$(json.error.container).html(json.error.message);
						$(json.error.container).show();
					}
					//leaving alert message for legacy support 
					else
					{
						alert(json.error.message);					
					}
				}
				else
				{
					alert('There was an error creating your item.\nPlease reload the page and try again.  Contact support if the problem persists.');
				}
			});
		return false;
		}
	});
	
	// DELETE ITEM
	$('.delete').live('click', function(){
		var elm = $(this);
		if(!$(this).hasClass('field') && !$(this).hasClass('deleteCamp')){
			var optional = "";
			$params = $(this).attr('rel'); //get delete parameters
			$container_div = $(this).parent().parent().parent().parent().attr('id'); //get parent div
			$test = "#"+$container_div;
			if($(this).parent().parent().attr('title') == "prospect"){
				$item = '#'+$(this).parent().parent().attr('id');
				$.get("delete_item.php?"+$params, function(data){
					if(data){
						$($item).hide('slow');
						$('#all').hide('slow');
					}
				});
			}
			<? if($_SESSION['reseller']){ ?>
			else if($(this).parent().parent().attr('title') == "network"){
				$.get("delete_item.php?"+$params, function(data){
					$("#message_cnt").html(data);
					loadPage('networks.php');
				});
			}
			<? } ?>
			else {
				$.get("delete_item.php?"+$params+"&section="+$container_div, function(data){
					$("#message_cnt").html(data);
					if($('#message_cnt').css('display')=='none') $('#message_cnt').show();
					if(elm.hasClass('rmParent')){
						elm.parent().parent().hide();
					}
					else {
						if($($test).attr('title')=="item_list") optional = "?id="+$("#dash").attr('title');
						$.get($container_div+".php"+optional, function(data){	//load new dash section
							$($test).html(data);
						});
					}
				});
			}
			return false;
		}
		else return false;
	});
	
	// UNDO DELETE
	$('.undo').live('click', function() {
  		var $params = $(this).attr('id');
		var $refresh = $(this).attr('rel');
	    if($('#formsSelect').length == 1){
	      var rememberSelection = $('#formsSelect').val();
		  $refresh = 'inc/forms/form_emailers';
		}
  		var $container_div = $(this).attr('title');
  		$.get("delete_undo.php?id="+$params, function(){
			if($refresh){
				if(rememberSelection){
					loadPage($refresh + '.php?mode=all&formID=' + rememberSelection);
				}
				else{
				  loadPage($refresh+'.php');
			   }
			}
			else {
				$.get($container_div+".php", function(data){
					$("#"+$container_div).html(data);
					$("#undo-test").fadeOut("slow");
				});
			}
		});
	});
	
	// DELETE CAMPAIGN
	$('.deleteCamp').live('click', function(){
		var cid = $(this).attr('rel');
		if(confirm('Are you SURE you want to delete this entire campaign and all associated items?  This cannot be undone.')){
			$.get('delete_item.php?table=campaigns&perm=1&id='+cid, function(data){
				data = $.parseJSON(data);
				if(data.success==1) alert('Campaign and all associated items was deleted successfully!');
				if(data.message) alert(data.message);
			});
		}
		return false;
	});
	
	// RESTORE item from trash
	$('.restore').live('click', function(){
		container = $(this).parent().parent().attr('id');
		params = {
				id: $(this).attr('id'),
				table: $(this).attr('title')
		};
		$.post("restore_item.php", params, function(data){
			if(data.error === 0){
				$("#"+container).hide('slow');
			} else {
				alert("An error occured and this item could not be restored.\nError: "+data.message);
			}
		}, 'json');
		return false;
	});
	
	// PERM DELETE item from trash
	$('.permdelete').live('click', function(){
		if(confirm("Are you sure you want to permanently delete this item?")){
			container = $(this).parent().parent().attr('id');
			$.get("delete_item.php?id="+$(this).attr('id')+"&table="+$(this).attr('title')+"&perm=true", function(data){
				if(data.error==0){
					$("#"+container).hide('slow');
				} else {
					alert("An error occured and this item could not be deleted.");
				}
			}, 'json');
		}
		return false;
	});
	
	//When Navigation is clicked
	$("#navigation > ul.top > li > a").live('click', function(event){
		$('#wait').show();
		$.get($(this).attr("href"), function(data){
			checkData(data);
			$("#ajax-load").html(data);
		});
		
		slideNav($(this));
		event.returnValue = false;
		return false;
	});
	
	//select drop down
	$("select.ajax-select").live('change', function() {
		$.get("inc/forms/"+$(this).attr("name")+".php?id="+$(this).attr("value"), function(data){
			$("#ajax-select").html(data);
		});
	});
	
	// TEXT INPUT AUTO SUBMIT
	$(".jquery_form input, .jquery_form select, .jquery_form textarea").live('blur', function() {           
		$elm = $(this);
                if($(this.form).hasClass('ctrlr_form')) return;
                
		$elm.removeClass("error");
		$elm.removeClass("approved");
		
		if($(this).attr('name')=="domain"){
			$('#domainerror').css("color", "#666");
			$('#domainerror').html("Please wait, this may take up to a minute to complete.");
			params = {
				domain: $(this).attr('value'),
				type: $(this).attr('title'),
				uid: '<?=$_SESSION['user_id']?>',
				sid: $(this).parent().parent().parent().attr('id'), 
				lincoln: $(this).attr('rel') 
			};
			$.post("check_domain.php", params, function(data){
				<? if($_SESSION['user_id']==496){ ?>alert(data);<? } ?>
				data = $.parseJSON(data);
				if(data.error==1){
					$('#domainerror').css("color", "red");
					$elm.addClass("error");
				} else {
					$('#domainerror').css("color", "green");
					saveFormData($('#domain'));
					if(data.type == "blog"){
						saveFormData($("#website_url"), data.message2);
					}
				}
				$('#domainerror').html(data.message);
			});
		}
		
		else if($(this).attr('title')=='new_blog_post'){
			//var blog = $(this).parent().parent().parent().attr('id');
			var blog = $(this).attr('rel');
			params = { title: $(this).attr('value'), blog_id: blog, user_id: <?=$_SESSION['user_id']?> };
			$.post("create-blog-post.php", params, function(data){
				if(data.error === 0) {
					$("#new-post-form").attr('id', data.post_id);
					$("#img_id").attr('value', data.post_id);
					$("#new_blog_post").attr('title', '');
					saveFormData($elm, '', data.post_id);
				} else {
					$elm.addClass("error");
				}
			}, 'json');
		}
		
		else if($(this).attr('title')=='new_hub_page'){
			//var hub = $(this).parent().parent().parent().attr('id')
			var hub = $(this).attr('rel');
			params = { title: $(this).attr('value'), hub_id: hub, user_id: <?=$_SESSION['user_id']?> };
			if($(this).hasClass('muPage')){
				params.multi_user = 'true';
				params.inputClass = $(this).attr('class');
			}
			$.post("create-hub-page.php", params, function(data){
				if(data.error === 0) {
					$("#new-page-form").attr('id', data.page_id);
					$("#new-page-form2").attr('id', data.page_id);
					$("#img_id").attr('value', data.page_id);
					$("#new_hub_page").attr('title', '');
					if(!$elm.hasClass('muPage')) saveFormData($elm, '', data.page_id);
					else {
						$elm.addClass("approved");
						$('#new-page-img-lock').attr('rel', data.page_id);
					}
				} else {
					<? if($_SESSION['login_uid']==496){ ?>
					alert(data.message);
					<? } ?>
					$elm.addClass("error");
				}
			}, 'json');
		}
		
		else if($(this).attr('title')=='select'){
			if(!$(this).hasClass('readonly') && !$(this).hasClass('imgLock')){
				if($(this).attr('name')=='theme'){
					if(($(this).attr('value')!=0) && ($(this).attr('rel')!=$(this).val())){
						var cont = true;
						if($(this).val().substring(0,3)=='hub'){
							cont = confirm("Are you sure you want to change this hub's theme?\nThis may cause you to lose all custom changes!");
						}
						if(cont) saveFormData($elm);
					}
				}
				else {
					saveFormData($elm);
				}
			}
		}
		
		else {
			saveFormData($elm);
		}
	});
	
	// JQUERY_FORM CHECKBOX AUTO SUBMIT
	$(".jquery_form input:checkbox").live('click', function(){
		if(!$(this).hasClass('no-upload')){
			var $elm = $(this);
			var value = 0;
			var theParent = $elm.parent().parent().parent().parent().parent();
			var tableID = theParent.attr('id');
			var form = theParent.attr('name');
			if($(this).attr('checked')) value = 1;
			var params = {
				'name': $elm.attr('name'),
				'value': value,
				'form': form,
				'id': tableID,
				'user_id': <?=$_SESSION['user_id']?>,
				"class": $elm.attr('class')
			};
			<? if($_SESSION['user_id']==496){ ?>alert(params.name+', '+params.value+', '+params.form+', '+params.id+', '+params.user_id+', '+params.class);<? } ?>
			saveFormData($elm, value, tableID, params);
		}
	});
	
	// FILE INPUT AUTO SUBMIT
	$('.test_file').live('change', function() {
		$elm = $(this).parent().parent().attr('id');
		$theForm = $(this).parent().parent();
		$theForm.submit();
		$('#uploadIFrame').load(function(){
			eval("data = "+$(this).contents().text());
			if(data.result == 'true'){
				$("#"+$elm+" + img")
				.attr('src', data.file)
				.addClass('pic');
			}
			else {
				alert(data.msg);	
			}
		});
	});

	<?php if($_SESSION['user_manager']){ ?>
		
	$('#usermanagerdropdown').live('change', function(){
		manageUser($(this).attr('value'));
	});
	
	$('.manage-user').live('click', function(){
		
		manageUser($(this).attr('rel'));
		
		return false;
	});

	function manageUser(new_id){
		if(new_id != <?=$_SESSION['user_id']?> && new_id != ''){
			$('#wait').show();
			params = { action: 'manageUser', 'new_id': new_id };
			$.post('user-permissions-functions.php', params, function(data){
				json = $.parseJSON(data);
				if(json.action == 'redir'){
					window.location = json.url;
				}
				else if(json.action == 'errors'){
					alert('Error changing users:\n'+json.errors);
				}
			});
			$('#wait').hide();
		}
	}

	<? } ?>

	<?php if($_SESSION['admin'] || $_SESSION['reseller']){ ?>
	// Reseller Functions
	$("form.new_account").live('submit', function(event) {
		event.stopPropagation();
		
		<? if($_SESSION['reseller'] && $_SESSION['payments']!='6qube' && !$_SESSION['thm_no_billing']){ ?>
		var rusure = confirm('You will be automatically charged our wholesale rate for this account type if successful.  Click OK to confirm.\n\nPlease be patient and only click the Sign Up button once!');
		<? } else { ?>
		var rusure = true;
		<? } ?>
		
		if(rusure===true){
			params = $(this).serialize();
			postResellerForm(params);
		}
		
		return false;
	});
	$("form.transfer_items").live('submit', function(event) {
		event.stopPropagation();
		
		var rusure = confirm('Are you sure?');
		params = $(this).serialize();
		if(rusure===true){ postResellerForm(params); }
		
		return false;
	});
	$('.continueTransferButton').live('click', function(){
		var uid = $('#uid2').attr('value');
		if(!uid) uid = $('#uid1').attr('value');
		var params = '?uid='+uid+'&action=getCampaignsForTransfer';
		$.get('reseller-functions.php'+params, function(data){
			if(data){
				if($('#uid2').attr('value')){ $('#transferUserChoiceDiv').html('<li>User #'+uid+'</li>'); }
				else { $('#transferUserChoiceDiv').html('<li>User #'+$('#uid1 option:selected').text()+'</li>'); }
				$('#continueTransferDiv').html(data);
				$('#toUid').attr('value', uid);
				$(".uniformselect").uniform();
				$('#transferSubmit').slideDown();
			}
			else alert('Sorry, something went wrong.  Please try again.');
		});
		
		return false;
	});
	$('.updateCheckbox').live('click', function(){
		var checked = $(this).attr('value');
		var id = $(this).attr('rel');
		var product = $(this).attr('name');
		
		params = { 'action': 'updateAllowedClass', 'checked': checked, 'id': id, 'product': product };
		postResellerForm(params);
	});
	$(".dupe-product").live('click', function(){
		var product = $(this).attr('rel');
		var user = $(this).attr('title');
		params = { 'action': 'dupeProduct', 'product': product, 'admin_user': user };
		
		postResellerForm(params);
		return false;
	});
	$("form.new_product").live('submit', function(event) {
		event.stopPropagation();
		
		params = $(this).serialize();
		postResellerForm(params);
		
		return false;
	});
	$(".del-product").live('click', function(){
		if(confirm("Are you sure you want to delete this product?")){
			var product = $(this).attr('rel');
			var user = $(this).attr('title');
			params = { 'action': 'delProduct', 'product': product, 'admin_user': user };
			
			postResellerForm(params);
		}
		return false;
	});
	$('.del-user').live('click', function(){
		var user = $(this).attr('rel');
		if(confirm('Are you SURE you want to permanently delete user #'+user+'?\nIf you just want to deactivate this user\'s account temporarily, click Edit and then set "Active" to "No".')){
			var del_all = confirm('Would you also like to permanently delete all of this user\'s created items?');
			params = { action: 'delUser', del_user: user, user_id: <?=$_SESSION['user_id']?>, 'del_all': del_all };
			$.post('reseller-functions.php', params, function(data){
				<? if($_SESSION['login_uid']==496){ ?>
				//alert(data);
				<? } ?>
				json = $.parseJSON(data);
				if(json.action == 'redir'){
					$.get(json.url, function(data){
						checkData(data);
						$("#ajax-load").html(data);
					});
				}
				else if(json.action == 'errors'){
					alert('Error deleting user:\n'+json.errors);
				}
			});
		}
		
		return false;
	});
	$('.upgrade-user').live('click', function(){
		<? if($_SESSION['login_uid']==496){ ?>
		//alert('uid: '+$(this).attr('rel')+'\n'+'upgradeClass: '+$('#upgradeClass').attr('value'));
		<? } ?>
		if(confirm('You\'re about to upgrade this user to a paid account and you will be charged our wholesale fee for the account type.\nClick OK to confirm.')){
			var uid = $(this).attr('rel');
			var upgradeClass = $('#upgradeClass').attr('value');
			params = { action: 'upgradeUser', user_id: uid, upgrade_class: upgradeClass };
			
			postResellerForm(params);
		}
		
		return false;
	});
	$('.user-details').live('click', function(){
		var user = $(this).attr('rel');
		var div = '#user-details-'+user
		
		if($(div).data('isDown')==1){
			$(div).slideUp();
			$(div).data('isDown', 0);
		}
		else {
			$(div).slideDown();
			$(div).data('isDown', 1);
		}
		
		return false;
	});

	$('.manage-user').live('click', function(){
		
		changeUser($(this).attr('rel'));
		
		return false;
	});

	$('#userdropdown').live('change', function(){
		changeUser($(this).attr('value'));
	});
	
	$('#chgUsrSubmit').live('click', function(){
		changeUser($('#chgUsrId').attr('value'));
		return false;
	});
	$('.edit-theme-select').live('change', function(){
		var params = { action: 'editTheme', user_id: <?=$_SESSION['login_uid']?>, property: $(this).attr('name'), 'value': $(this).attr('value') };
		editTheme(params);
	});
	$('.edit-theme-text').live('blur', function(){
		//if($(this).attr('value') != ""){
			var params = { action: 'editTheme', user_id: <?=$_SESSION['login_uid']?>, property: $(this).attr('name'), 'value': $(this).attr('value') };
			editTheme(params);
		//}
	});
	$('.edit-theme-textarea').live('blur', function(){
		var params = { action: 'editTheme', user_id: <?=$_SESSION['login_uid']?>, property: $(this).attr('name'), 'value': $(this).attr('value') };
		editTheme(params);
	});
	$('.theme_logo').live('change', function(){
		$(this).parent().parent().submit();
		previewID = '#'+$(this).parent().parent().attr('id')+'_prvw';
		$('#uploadIFrame').load(function(){
			eval("data = "+$(this).contents().text());
			var img = '<?=$_SESSION['thm_logo']?>';
			if(data.result == 'true'){
				if(img!=''){
					$(previewID).attr('src', data.file).addClass('pic');
					if(previewID=="#theme_logo_prvw"){
						$('#headerlogo').attr('src', '<?=$_SESSION['themedir']?>'+data.filename);
					}
					if(previewID=="#theme_bgImg_prvw"){
						$('html,body').css('background-image', "url('themes/<?=$_SESSION['login_uid']?>/"+data.filename+"')");
					}
				}
				else {
					window.location = './?rtrn=edit-theme';
				}
			}
			else{
				alert(data.msg);
			}
		});
	});
	$('#network-setup-sbmt').live('click', function(){
		$('#wait').show();
		var params = {
			company: $('#new-network-company').attr('value'), 
			domain: $('#new-network-domain').attr('value'), 
			networkID: $('#new-network-id').attr('value'), 
			type: 'setupWhole'
		};
		$.post("add-network.php", params, function(data){
			if(data.success==1){
				$('#wait').hide();
				$.get("inc/forms/edit_whole_network.php?id="+$('#new-network-id').attr('value'), function(data){
					data2 = data;
					checkData(data2);
					$("#ajax-load").html(data2);
				});
			} else {
				$('#wait').hide();
				$("#ajax-load").prepend(data.message);
			}
		}, 'json');
		return false;
	});
	$('#new-reseller-site-sbmt').live('click', function(){
		$('#wait').show();
		$('#domainerror').css("color", "#666");
		$('#domainerror').html("Please wait, this may take up to a minute to complete.");
		$('#new-reseller-site').removeClass("error");
		$('#new-reseller-site-sbmt').hide('slow');
		var params = {
			domain: $('#new-reseller-site').attr('value'), 
			type: 'reseller', 
			uid: '<?=$_SESSION['login_uid']?>'
		};
		$.post("check_domain.php", params, function(data){
			if(data.error==1){
				$('#domainerror').css("color", "red");
				$('#new-reseller-site').addClass("error");
			} else {
				$('#domainerror').css("color", "green");
				$('#new-reseller-site').addClass("approved");
				finishResellerSiteSetup();
			}
			$('#wait').hide();
			$('#domainerror').html(data.message);
		}, 'json');
		
		return false;
	});
	$('#sendtoEmailList').live('click', function(){
		var listState = $('#sendToEmailListDiv').attr('name');
		if(listState==0){
			var emailerID = $(this).attr('rel');
			$.get('inc/forms/reseller_emailers_email.php?id='+emailerID+'&getList=1', function(data){
				checkData(data);
				$("#sendToEmailListDiv").html(data);
				$("#sendToEmailListDiv").slideDown();
				$('#sendToEmailListDiv').attr('name', 1);
				$('#sendtoEmailList').html('[-] Click here to hide list');
			});
		}
		else if(listState==1){
			$("#sendToEmailListDiv").slideUp();
			$('#sendToEmailListDiv').attr('name', 2);
			$(this).html('[+] Click here to show a list of users who will be emailed');
		}
		else {
			$("#sendToEmailListDiv").slideDown();
			$('#sendToEmailListDiv').attr('name', 1);
			$(this).html('[-] Click here to hide list');
		}
		return false;
	});
	$('#popEmailerHistory').live('click', function(){
		var id = $(this).attr('rel');
		newwindow = window.open('http://<?=$_SESSION['main_site']?>/admin/inc/forms/reseller_emailers_history.php?popHistoryID='+id,'sentTo','height=600,width=400,menubar=0,resizable=1,status=0,scrollbars=1,location=0');
		
		return false;
	});
	$('.imgLock, .pagesLock').live('change', function(){
		//alert('test');
		var params = {
			name: $(this).attr('name'),
			value: $(this).attr('value'),
			id: $(this).attr('rel'),
			"class": $(this).attr('class')
		}
		if($(this).hasClass('pagesLock') && $(this).hasClass('imgLock'))
			params.form = 'hub_page';
		else
			params.form = 'hub';
		//alert('name: '+params.name+', value: '+params.value+', form: '+params.form+', id: '+params.id+', class: '+params.class);
		saveFormData($(this), '', '', params);
	});
	<? if($_SESSION['admin']){ ?>
	$('.delFromBlacklist').live('click', function(){
		var id = $(this).attr('rel');
		var params = { 'id': id, action: 'del' };
		
		$.post('admin-blacklist.php', params, function(data){
			if(data.success==1){
				$('#bl_'+id).slideUp();
			}
			else {
				alert('Sorry, IP couldn\'t be removed.\nTry deleting from the database manually.');
			}
		}, 'json');
	});
	<? } ?>
	function finishResellerSiteSetup(){
		var params2 = {
			action: 'resellerSiteSetup', 
			domain: $('#new-reseller-site').attr('value'), 
			uid: '<?=$_SESSION['login_uid']?>'
		};
		$.post('reseller-functions.php', params2, function(data2){
			json = $.parseJSON(data2);
			if(json.action == 'success'){
				$('#success-message').show();
			}
			else {
				alert(json.errors);
			}
		});
	}
	function postResellerForm(params){
		$.post('reseller-functions.php', params, function(data){
			<? if($_SESSION['login_uid']==496){ ?>
			//alert(data);
			<? } ?>
			json = $.parseJSON(data);
			if(json.action == 'redir' || json.action == 'popSuccessRedir'){
				if(json.action=='popSuccessRedir') alert('Success!');
				var data2 = '';
				$.get(json.url, function(data){
					data2 = data;
					checkData(data2);
					if(json.url=='inc/campaigns.php') checkNavState('campaigns');
					$("#ajax-load").html(data2);
				});
				if(data2==''){
					$("#ajax-load").html("Success.");
				}
			}
			else if(json.action == 'errors'){
				$("#ajax-load").prepend(json.errors+'<br style="clear:both" />');
			}
		});
	}
	function editTheme(params){
		$.post('reseller-functions.php', params, function(data){
			//alert(data);
			json = $.parseJSON(data);
			if(json.action == 'css'){
				$(json.div1).css(json.property1, json.value1);
				if(json.div2!="") $(json.div2).css(json.property2, json.value2);
				if(json.div3!="") $(json.div3).css(json.property3, json.value3);
				if(json.div4!="") $(json.div4).css(json.property4, json.value4);
				if(json.div5!="") $(json.div5).css(json.property5, json.value5);
				if(json.div6!="") $(json.div6).css(json.property6, json.value6);
				if(json.returnPage!=""){
					//reload page
					$.get('inc/forms/'+json.returnPage+'.php', function(data){
						checkData(data);
						$("#ajax-load").html(data);
					});
				}
			}
			else if(json.action == 'refresh'){
				window.location = './?rtrn='+json.returnPage;
			}
			else if(json.action == 'errors'){
				alert('Error changing theme:\n'+json.errors);
			}
		});
	}
	function changeUser(new_id){
		if(new_id != <?=$_SESSION['user_id']?> && new_id != ''){
			$('#wait').show();
			params = { action: 'changeUser', 'new_id': new_id };
			$.post('reseller-functions.php', params, function(data){
				json = $.parseJSON(data);
				if(json.action == 'redir'){
					window.location = json.url;
				}
				else if(json.action == 'errors'){
					alert('Error changing users:\n'+json.errors);
				}
			});
			
			$('#wait').hide();
			
			
		}
	}
	<? } ?>
	$('#showCustomPageTags').live('click', function(){
		if($('#availableTags').data('isDown')==1){
			$('#availableTags').slideUp();
			$('#availableTags').data('isDown', 0);
			return false;
		}
		else {
			$('#availableTags').slideDown();
			$('#availableTags').data('isDown', 1);
			return false;
		}
	});
	
	$('#newBillingProfile').live('click', function(){
		var uid = $(this).attr('rel');
		var action = $(this).attr('title');
		$('#dialog').remove();
		$('body').append('<div id="dialog" \/>');
		$('#dialog').dialog({	
			autoOpen: false,
			bgiframe: true,
			resizable: true,
			width: 845,
			position: ['center','center'],
			overlay: { backgroundColor: '#000', opacity: 0.5 },
			beforeclose: function(event, ui) {
				$('#wait').show();
				$.get("reseller-client-billing.php", function(data){
					checkData(data);
					$("#ajax-load").html(data);
				});
			}				
		});
		$('#dialog').dialog('option', 'title', 'New Billing Profile');
		$('#dialog').dialog('option', 'modal', true);
		$('#dialog').dialog('option', 'buttons', {
			'Close': function() {
				$(this).dialog('close');
			}
		})
		<? if($_SESSION['admin'] || $_SESSION['reseller']){ ?>
		var iframeSrc = 'https://6qube.com/admin/create-billing-profile.php?id='+uid+'&id2=<?=$_SESSION['login_uid']?>&sec=<?=md5($_SESSION['login_uid'].'4a0fBMMDDR(Cc,,c.bh07890')?>&action='+action;
		
		<? } else { ?>
		var iframeSrc = 'https://6qube.com/admin/create-billing-profile.php?id='+uid+'&sec=<?=md5($_SESSION['login_uid'].'4a0fBMMDDR(Cc,,c.bh07890')?>&action='+action;
		<? } ?>
		$('#dialog').html('<iframe width="800" height="670" frameborder="0" scrolling="no" src="'+iframeSrc+'"></iframe>');
		$('#dialog').dialog('open');
	});
	
	<? if(!$_SESSION['reseller'] && !$_SESSION['reseller_client']){ ?>
	$('#upgrade6QubeUser, #update6QubeUserProfile').live('click', function(){
		var uid = $(this).attr('rel');
		var cont = true;
		var action = 'create';
		var upgrade_type = $(this).attr('title');
		if(upgrade_type){
			var upgrade_class = $('#'+upgrade_type).attr('value');
			if(!upgrade_class){
				alert('Please choose an account type or package to upgrade to.');
				cont = false;
			}
		}
		if($(this).attr('id')=='update6QubeUserProfile') action = 'update';
		if(cont) {
			$('#dialog').remove();
			$('body').append('<div id="dialog" \/>');
			$('#dialog').dialog({
				autoOpen: false,
				bgiframe: true,
				resizable: true,
				width: 845,
				position: ['center','center'],
				overlay: { backgroundColor: '#000', opacity: 0.5 },
				beforeclose: function(event, ui) {
					$('#wait').show();
					$.get("6qube-client-billing.php", function(data){
						checkData(data);
						$("#ajax-load").html(data);
					});
				}				
			});
			$('#dialog').dialog('option', 'title', 'New Billing Profile');
			$('#dialog').dialog('option', 'modal', true);
			$('#dialog').dialog('option', 'buttons', {
				'Close': function() {
					$(this).dialog('close');
				}
			})
			<? if($_SESSION['admin']){ ?>
			var iframeSrc = 'https://6qube.com/admin/create-billing-profile.php?id='+uid+'&id2=<?=$_SESSION['login_uid']?>&sec=<?=md5($_SESSION['login_uid'].'4a0fBMMDDR(Cc,,c.bh07890')?>&action='+action+'&6qube=true&upgradeClass='+upgrade_class;
			
			<? } else { ?>
			var iframeSrc = 'https://6qube.com/admin/create-billing-profile.php?id='+uid+'&sec=<?=md5($_SESSION['login_uid'].'4a0fBMMDDR(Cc,,c.bh07890')?>&action='+action+'&6qube=true&upgradeClass='+upgrade_class;
			<? } ?>
			$('#dialog').html('<iframe width="800" height="670" frameborder="0" scrolling="no" src="'+iframeSrc+'"></iframe>');
			$('#dialog').dialog('open');
		}
		return false;
	});
	<? } ?>
	
	//Duplicate Hub
	$('.duplicateHub').live('click', function(){
		var newHubName = prompt("Duplicating hub.  This will create a new hub with the same settings and pages.\nPlease enter a name for the new hub:", "");
		if(newHubName!=null && newHubName!=""){
			params = { id: $(this).attr('rel'), name: newHubName };
			$.post("duplicate-hub.php", params, function(data){
				if(data.error === 0){
					//alert(data.message);
					$('#wait').show();
					$.get(page, function(data){
						checkData(data);
						$("#ajax-load").html(data);
					});
				} else {
					alert("There was an error duplicating this hub:\n"+data.message);
					alert(data.message2);
				}
			}, 'json');
			return false;
		}
		else return false;
	});
	
	$('.resetEmailPass').live('click', function(){
		if(confirm('Are you sure you want to reset the password for this account?')){
			var elm = $(this);
			var lincoln = 0;
			if($(this).hasClass('lincolnDomain')) lincoln = 1;
			var params = { domain: elm.attr('rel'), account: elm.attr('title'), 'lincoln': lincoln };
			elm.hide('slow');
			$.post("setup_email_reset_pass.php", params, function(data){
				if(data.success === 1){
					elm.attr('href', 'http://'+elm.attr('rel')+'/webmail/');
					elm.attr('target', '_blank');
					elm.html('Temporary password: <strong>'+data.message+'</strong>');
					elm.removeClass('resetEmailPass');
					elm.show('slow');
				} else {
					alert('There was an error resetting the password.  Please try again or contact support if the problem persists.\n\nError details:\n'+data.message);
					elm.show('slow');
				}
			}, 'json');
		}
		return false;
	});
	
	$('.installGoogleApps').live('click', function(){
		if(confirm('This will install the MX entries needed to set this domain up for Google Apps and delete any existing MX entries.\nIt may take a minute, please be patient.\n\nClick OK to continue')){
			$(this).hide('slow');
			var lincoln = 0;
			if($(this).hasClass('lincolnDomain')) lincoln = 1;
			var params = { domain: $(this).attr('rel'), 'lincoln': lincoln };
			//alert(params.domain);
			$.post("setup_google_apps.php", params, function(data){
				<? if($_SESSION['login_uid']==496){ ?>alert(data);<? } ?>
				data = $.parseJSON(data);
				if(data.success === 1){
					alert('Entries created successfully!\nPlease allow up to 24 hour for the changes to propagate throughout the internet.');
				} else {
					alert('There was an error trying to create the MX entries.\nPlease try again or contact support.\n\nError details:\n'+data.message);
					$(this).show('slow');
				}
			});
		}
		return false;
	});
	
	//pop up Jquery UI with TMCE to email prospect
	$('.email-prospect').live('click', function(){
		var email = $(this).attr('href');
		var id = $(this).attr('id');
		var name = $(this).attr('name');
		var comment = $('#'+id+'-comment').html();
		var dfltSubject = '';
		var dispComment = true;
		var html = '';
		var addHint = '';
		var postTo = 'email_prospect.php';
		var type = $(this).attr('rel');
		if(type=='prospect'){
			dfltSubject = 'Response To Your Inquiry';
			
		}
		else if(type=='user'){
			dispComment = false;
			postTo = 'email_users.php';
			addHint = '<br /><strong>#company</strong> is replaced with the user\'s company.';
		}
		
		if(id=="all"){
			html = '<p>Emailing all '+type+'s.</p><br />';
			html += 'Subject: <input id="emailsubject" type="text" size="60" value="'+dfltSubject+'" ><br /><br />';
			html += 'Your message:<br />';
			html += '<textarea id="emailbody" name="'+name+'" class="TMCE"></textarea><br />';
			html += '<span style="font-size:9.5px;">Hint: You can use "<strong>#name</strong>" (without quotes) in the body or subject and it will automatically be replaced with the '+type+'\'s name when the email is sent.</span>';
			email = "All";
		}
		else {		
			if(dispComment) html = '<p>'+name+'\'s message:<br /><small>'+comment+'</small></p><br />';
			html += 'Subject:<br /><input id="emailsubject" type="text" size="60" value="'+dfltSubject+'" ><br /><br />';
			html += 'Your message:<br />';
			html += '<textarea id="emailbody" name="'+id+'" class="TMCE"></textarea><br />';
			html += '<span style="font-size:9.5px;">Hint: You can use "<strong>#name</strong>" (without quotes) in the body or subject and it will automatically be replaced with the '+type+'\'s name when the email is sent.';
			html += addHint;
			html += '</span>';
		}
		
		$('#dialog').remove();
		$('body').append('<div id="dialog" \/>');
		$('#dialog').dialog({	
			autoOpen: false,
			bgiframe: true,
			resizable: true,
			width: "75%",
			minHeight: 500,
			position: ['center','center'],
			overlay: { backgroundColor: '#000', opacity: 0.5 },
			beforeclose: function(event, ui) {
				tinyMCE.get('emailbody').remove();
				$('#emailbody').remove();
			}				
		});
		$('#dialog').dialog('option', 'title', 'Email '+type+': '+email);
		$('#dialog').dialog('option', 'modal', true);
		$('#dialog').dialog('option', 'buttons', {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Send': function() {
				if(tinyMCE.get('emailbody').getContent()!=""){
					params = {
						'id': $('#emailbody').attr('name'),
						'name': name,
						emailsubject: $('#emailsubject').attr('value'),
						emailbody: tinyMCE.get('emailbody').getContent(),
						emailtype: type
					};
					$.post(postTo, params, function(data){
						if(data.error === 0) {
							alert(data.message);
						}
						else {
							alert("Sorry, an error occured:\n"+data.message);
						}
					}, 'json');
					$(this).dialog('close');
				}
				else {
					alert("Email body cannot be blank!");
				}
			}
		});
		$('#dialog').html(html);
		$('#dialog').dialog('open');
		
		tinymce.init({
			mode : "specific_textareas",
			editor_selector : "TMCE",
			theme : "advanced",
			skin : "cirkuit",
			plugins : "safari,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,imagemanager,template",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,insertimage,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			extended_valid_elements: "style[*]",
			width: "100%",
			height: "100%",
			convert_urls : false,
			
			setup : function(ed) {
				ed.onInit.add(function(ed) {
					tinyMCE.execCommand('mceRepaint');
				});
			}
		});
		
		return false;
	});
	
	//Rich Text Editors
	$('.tinymce').live('click', function(){
		loadEditor($(this).attr('id'));
		return false;
	});
	function loadEditor(n){
		var note = '<br /><span style="font-size:9.5px;">Hint: When pasting in content from a word processor click the Paste From Word icon, otherwise you may notice strange characters and glitches in your text.</span>';
		$('#dialog').remove();
		$('body').append('<div id="dialog" \/>');
		$('#dialog').dialog({	
			autoOpen: false,
			bgiframe: true,
			resizable: true,
			width: "75%",
			minHeight: 500,
			position: ['center','center'],
			overlay: { backgroundColor: '#000', opacity: 0.5 },
			beforeclose: function(event, ui) {
				//ALERT
				//alert(tinyMCE.get('area'+n+'-tmce').getContent());
				tinyMCE.get('area'+n+'-tmce').remove();
				$('#area'+n+'-tmce').remove();
			}				
		});
		
		$('#dialog').dialog('option', 'title', 'Edit');
		$('#dialog').dialog('option', 'modal', true);
		$('#dialog').dialog('option', 'buttons', {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'OK': function() {
				var content = tinyMCE.get('area'+n+'-tmce').getContent();
				$('#edtr'+n).val(content);
				uploadTextArea('#edtr'+n);
				$(this).dialog('close');
			}
		});
		
		$('#dialog').html('<textarea name="area" id="area'+n+'-tmce" class="TMCE"><\/textarea>'+note);
		$('#dialog').dialog('open');
		tinymce.init({
			mode : "specific_textareas",
			editor_selector : "TMCE",
			theme : "advanced",
			skin : "cirkuit",
			plugins : "safari,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,insertimage,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			extended_valid_elements: "style[*]",
			width: "100%",
			height: 500,
			convert_urls : false,
			setup : function(ed) {
				ed.onInit.add(function(ed) {
					//alert('Editor is done: ' + ed.id);
					tinyMCE.get('area'+n+'-tmce').setContent($('#edtr'+n).val());
					tinyMCE.execCommand('mceRepaint');
				});
			}
		});
		return false;	
	};
	
	//upload Text Area
	function uploadTextArea(n){
		$elm = $(n);
		
		if(!$elm.hasClass('no-upload')){
			$elm.trigger('blur');
		}
		<? if($_SESSION['reseller']){ ?>
		else if($elm.hasClass('edit-theme-textarea')){
			$('.edit-theme-textarea').trigger('blur');
		}
		<? } ?>
	}
	
	$('.prospect-details').live('click', function(){
		var prospect = $(this).attr('rel');
		var div = '#prospect-details-'+prospect
		
		if($(div).data('isDown')==1){
			$(div).slideUp();
			$(div).data('isDown', 0);
		}
		else {
			$(div).slideDown();
			$(div).data('isDown', 1);
		}
		
		return false;
	});
	
	//When a link is clicked
	$("#ajax-load a, a.ajax, .child a").live('click', function(event){
		if(!$(this).hasClass('external') && !$(this).hasClass('view') && $(this).attr('target')!='_new' && 
		$(this).attr('target')!='_blank' && !$(this).hasClass('email-prospect') && 
		$(this).attr('href')!='#' && $(this).attr('href')!='javascript:;'){
			$('#wait').show();
			$.get($(this).attr("href"), function(data){
				checkData(data);
				$("#ajax-load").html(data);
			});
			
			if($(this).attr('rev')){
				if( 
					($(this).attr('rev') == 'campaigns') || 
					($(this).attr('rev') == 'support') || 
					($(this).attr('rev') == 'training') ||
					($(this).attr('rev') == 'reseller') || 
					($(this).attr('rev') == 'billing') ||
					($(this).attr('rev') == 'affiliates') || 
					($(this).attr('rev') == 'admin') || 
					($(this).attr('rev') == 'dashboard') || 
					($(this).attr('rev') == 'home') 
				  )
				{
					checkNavState($(this).attr('rev'));
				}
				//checkNavState($(this).attr('rev'));
				slideNav('#'+$(this).attr('rev'));
			}
			event.returnValue = false;
			return false;
		}
	});
	
	//when top navigation is clicked
	$(".header-nav a").live('click', function() {
		if(!$(this).hasClass("no-ajax")){
			if(!$(this).hasClass("current")){
				$(".header-nav a").each(function(){ $(this).removeClass("current"); });
				$(this).addClass("current");
			}
			
			$('#wait').show();
		
			$.get($(this).attr("href"), function(data){
				checkData(data);
				$("#ajax-load").html(data);
			});
			
			checkNavState($(this).attr('rev'));
			
			return false;
		}
	});
	
	//check/upload form data
	function saveFormData(elm, value, id, customParams){
		var theParent = elm.parent().parent().parent().parent();
		if(!customParams){
			if((value===undefined || value=='') && value!==0){
				sendVal = elm.attr('value');
			}
			else sendVal = value;
			id===undefined ? specID = '' : specID = id;
		}
		if(elm.attr('title')=='select'){
			if(customParams){ params = customParams; }
			else {
				//the Uniform plugin wraps inputs in another div so add one parent
				params = {
					name: elm.attr('name'),
					value: sendVal,
					form: theParent.attr('name'),
					id: theParent.attr('id'),
					user_id: <?=$_SESSION['user_id']?>,
					"class": elm.attr('class')
				};
			}
			if(elm.attr('name')=="connect_blog_id"){
				var rusure = confirm("Are you sure you want to connect this hub to the selected blog?  This will replace any existing hub/blog connections to the blog.");
				if(rusure==false) params = "";
			}
		} else if(specID){
			if(customParams){ params = customParams; }
			else {
				params = {
					name: elm.attr('name'),
					value: sendVal,
					form: elm.parent().parent().parent().attr('name'),
					id: specID,
					user_id: <?=$_SESSION['user_id']?>,
					"class": elm.attr('class')
				};
			}
		} else {
			if(customParams){ params = customParams; }
			else {
				params = {
					name: elm.attr('name'),
					value: sendVal,
					form: elm.parent().parent().parent().attr('name'),
					id: elm.parent().parent().parent().attr('id'),
					user_id: <?=$_SESSION['user_id']?>,
					"class": elm.attr('class')
				};
			}
		}
		
		if(!elm.hasClass('no-upload') && !elm.attr('readonly')){
			<?php if($_SESSION['user_id']==496 || $_SESSION['user_id']==105){ ?>
			//alert('name:'+params.name+', value:'+params.value+', form:'+params.form+', id:'+params.id+', uid:'+params.user_id+', class:'+params.class);
			<? } ?>
			$.post("upload_form.php", params, function(data){
				<?php if($_SESSION['login_uid']==496 || $_SESSION['login_uid']==105){ ?>
				alert(data);
				<? } ?>
				data = $.parseJSON(data);
				if(data.error === 0) {
					elm.find(' + p').html('');
					elm.addClass("approved");
					<?php if($_SESSION['parent_id']==496 || $_SESSION['user_id']==105){ ?>
					//alert(data.message);
					<? } ?>
					if(elm.attr('title')=='select'){
						if(theParent.attr('name')=="hub" && elm.attr('name')=="theme"){
							$('#wait').show();
							$.get('hub.php?form=theme&themeID='+params.value+'&id='+theParent.attr('id'), function(data){
								checkData(data);
								$("#ajax-load").html(data);
							});
						}
						if(elm.attr('name')=="connect_blog_id"){
							alert("Blog connected successfully!");
						}
					}
				}
				else {
					<?php if($_SESSION['parent_id']==496 || $_SESSION['user_id']==105){ ?>
					alert('saveFormData error: '+data.message);
					<? } ?>
					elm.find(' + p').html(data.message);
					elm.addClass("error");
				}
			});//, 'json');	
		}
	}
	
	//check returned data
	function checkData(n){
		$('#wait').hide();
		if(n == 'login'){
			window.location="login.php";
		}
		return true;
	}
	
	function checkNavState(n){
		if(nav_state != n){
			$.get('admin/inc/nav_'+n+'.php', function(data) {
				$('#navigation').html(data);	
			});
			nav_state = n;
		}
	}
	//Slide Nav
	function slideNav(n){
		$navElm = $(n);
		if($navElm.attr("class") != "hover"){
			<? if($_SESSION['reseller'] || $_SESSION['reseller_client']){ ?>
			$("#navigation ul.top li a.hover").css('color', '#<?=$_SESSION['thm_nav-links3']?>');
			$navElm.css('color', '#<?=$_SESSION['thm_nav-links']?>');
			$("#navigation ul.top li a.hover").css('text-shadow', '#<?=$_SESSION['thm_nav-links4']?> 1px 1px');
			$navElm.css('text-shadow', 'none');
			<? } ?>
			$("#navigation ul.top li a").removeClass("hover");
			$navElm.addClass("hover");		
			$("#navigation .active").slideUp().removeClass();
			$navElm.parent().next().slideDown().addClass("active");
		}
	}
	
	function loadPage(page){
		$('#wait').show();
		$.get(page, function(data){
			checkData(data);
			$("#ajax-load").html(data);
			var pagename = page.split('.', 1);
			<? if($_SESSION['reseller']){ ?>
			if(page=='hub.php?multi_user=1' || pagename=='networks'){
				checkNavState('reseller');
			}
			else if(pagename=='hub' || pagename=='websites' || pagename=='social_media' || pagename=='mobile' || pagename=='landing_pages' || pagename=='setup_email' || pagename=='hub-forms'){
				checkNavState('dashboard');
			}
			else {
				checkNavState('campaigns');
			}
			<? } else { ?>
			if(page=='inc/campaigns.php'){
				checkNavState('campaigns');
			}
			<? if($_SESSION['admin']){ ?>
			else if(pagename=='admin-blacklist'){
				checkNavState('admin');
			}
			<? } ?>
			else {
				checkNavState('dashboard');
			}
			<? } ?>
		});
	}
	
	
	<? if(!$_SESSION['theme']){ ?>
	$('#header h1, #header .slogan').live('click', function(){
		//loadPage('inc/campaigns.php');
		$('#header-campaigns').trigger('click');
	});
	<? } ?>
	
//end $(document).ready(function()
});
</script>