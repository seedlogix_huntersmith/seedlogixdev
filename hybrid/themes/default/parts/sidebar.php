<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
<div class="mainNav">
    <?php $this->getNavigation('userbox.php'); ?>
    <!-- Responsive nav -->
    <div class="altNav">
        <div class="userSearch">
            <form action="">
                <input type="text" placeholder="search..." name="userSearch">
                <input type="submit" value="">
            </form>
        </div>
        <!-- User nav -->
        <ul class="userNav">
            <li><a href="<?=$this->action_url('UserManager_profile?ID='.$User->getID())?>" title="" class="profile"></a></li>
            <li><a href="#" title="" class="messages"></a></li>
            <li><a href="#" title="" class="settings"></a></li>
            <li><a href="#" title="" class="logout"></a></li>
        </ul>
    </div>
    <!-- Main nav -->
    <ul class="nav">
        <?php if($User->can('is_reseller')): ?>
        <li><a href="<?=$this->action_url('Campaign_home')?>" title=""><img src="<?php echo $_path; ?>images/icons/mainnav/dashboard.png" alt=""><span>Home</span></a></li>
        <?php endif; ?>
        <li><a href="<?=$this->action_url('Campaign_home')?>" title="" class="<?=@$classes['campaign-nav']?>"><img src="<?=$_path?>images/icons/mainnav/statistics.png" alt=""><span>Campaigns</span></a></li>
        <?php if($User->can('view_admin')): ?>
        <li><a href="<?=$this->action_url('Admin_dashboard')?>" title="" class="<?= @$classes['admin-nav']; ?>"><img src="<?=$_path?>images/icons/mainnav/forms.png" alt=""><span>Admin</span></a></li>            
        <?php endif; 
        if($User->can('is_system')): ?>
        <li><a href="<?=$this->action_url('System_dashboard')?>" title="" class="<?= @$classes['system-nav-icon']; ?>"><img src="<?=$_path?>images/icons/mainnav/tables.png" alt=""><span>System</span></a></li>
        <?php endif; ?>
    </ul>
</div>