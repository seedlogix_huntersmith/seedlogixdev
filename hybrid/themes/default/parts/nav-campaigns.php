<?php

	session_start();
	if($_SESSION['reseller'] || $_SESSION['reseller_client']) $isReseller = true;
        
        $is_network_user    =   $_SESSION['network_user'];
        $is_resports_user   =   $_SESSION['reports_user'];
        
        $is_rankings_user   =   $_SESSION['login_uid']==7 || $_SESSION['login_uid']==7990;
        
        $additional_style = '';
	if($isReseller){
		$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
		$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
		$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
		if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow: #'.$_SESSION['thm_nav-links4'].' 1px 1px;';
		$style3 .= '"';
                
                $additional_style = $style2;
	}
?>

        <ul class="nav">
<?php
    if(!$is_network_user){ ?>
	<!--dashboard-->
        
            <li>
                <a href="dashboardv3.php" class="hover" id="dashboard" <? if($isReseller) echo $style1; ?>>
                    <img src="<?php echo $_path; ?>img/v3/dashboard-icon.png" alt="" /><span>Dashboard</span></a>
                <ul>
    <? if(!$is_reports_user){ ?>
			<!-- <li><a href="trash-can.php" <?php echo $additional_style; ?>>Trash Can</a></li> -->
            <li><a href="transfer-items.php" class="child-link">Transfer Items</a></li>
			<li><a href="domains-manager.php" <?php echo $additional_style; ?>>Domains</a></li>
            <li><a href="crm-integration.php"  <?php echo $additional_style; ?>>CRM Integration</a></li>			
        <? } ?>    
            <li><a href="controller.php?action=settings" class="last child-link"  <?php echo $additional_style; ?>>Settings</a></li>	
        </ul></li>
        
    <? if($is_reports_user){ ?>
    <li><a href="website-analytics.php" id="website" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/website-icon.png" /><span>Analytics</span></a></li>
    <? } ?>
    <?php

        if(!$is_reports_user){ ?>
	<!-- #### websites version 2.0 #### -->
	<li><a href="controller.php?action=loadDefault&ctrl=Website" id="website2" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/website-icon.png" /><span>Websites 2</span></a>
		<ul>
            <? if($_SESSION['login_uid']==3625){ ?>
            <li><a href="http://store.<?=$_SESSION['main_site']?>/admin/" target="_blank" <?=$style2?>>eCommerce Admin</a></li>
            <? } ?>
			<li><a href="website-analytics.php" class="last child-link" <?php echo $additional_style; ?>>Analytics</a></li>
		</ul>
	</li>
	<? } ?>
        
    <? if(!$is_reports_user){ ?>
	<!--landing pages-->
	<li><a href="landing_pages.php" id="landing" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/landing-icon.png" /><span>Landing Pages</span></a>
		<ul>
			<li><a href="landing-analytics.php" class="last child-link" <?php echo $additional_style; ?>>Analytics</a></li>
		</ul>
	</li>
	<? } ?>
    <? if(!$is_reports_user){ ?>
	<!--blogs-->
	<li><a href="blog.php" id="blog" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/blogs-icon.png" /><span>Blogs</span></a>
		<ul>
			<li><a href="blog-analytics.php" class="last child-link" <?php echo $additional_style; ?>>Analytics</a></li>
		</ul>
	</li>
	<? } ?>
        

    <? if(!$is_reports_user){ ?>
	<!--social-->
	<li><a href="social_media.php" id="social" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/social-icon.png" /><span>Social Media</span></a>
		<ul>
			<li><a href="social-analytics.php" class="last child-link" <?php echo $additional_style; ?>>Analytics</a></li>
		</ul>
	</li>
    <? } ?>
    <? if(!$is_reports_user){ ?>
    <!--mobile-->
	<li><a href="mobile.php" id="social" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/mobile-icon.png" /><span>Mobile</span></a>
		<ul>
			<li><a href="mobile-analytics.php" class="last child-link" <?php echo $additional_style; ?>>Analytics</a></li>
		</ul>
	</li>
	<? } ?>        
        
	<? if($is_rankings_user){ ?>
	<!--rankings-->
	<li><a href="rankings.php" id="rankings" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/rankings-icon.png" /><span>Rankings</span></a></li>
	<? } ?>

    <? if(!$is_reports_user){ ?>
	<!--custom forms-->
	<li><a href="hub-forms.php" id="forms" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/forms-icon.png" /><span>Custom Forms</span></a></li>
  	<? } ?>

    <? if(!$is_reports_user){ ?>
    <!--email marketing-->
	<li><a href="forms-autoresponders.php" id="email" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/email-icon.png" /><span>Email Marketing</span></a>
		<ul>
			<li><a href="forms-autoresponders.php" <?php echo $additional_style; ?>>Auto-Responders</a></li>
			<li><a href="hub-form-emailers.php" class="last child-link" <?php echo $additional_style; ?>>Email Broadcast</a></li>
		</ul>
	</li>
    <? } ?>


    <!--prospects-->
	<li><a href="prospects.php" id="prospects" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/prospects-icon.png" /><span>Prospects</span></a>
		<ul>
			<li><a href="lead-form-prospects.php" <?php echo $additional_style; ?>>Lead Form Prospects</a></li>
			<li><a href="contact-form-prospects.php" <?php echo $additional_style; ?>>Contact Form Prospects</a></li>
                                <? if(!$is_reports_user){ ?>
			<li><a href="blog-prospects.php" <?php echo $additional_style; ?>>Blog Prospects</a></li>
            <li><a href="network-prospects.php" class="last child-link" <?php echo $additional_style; ?>>Network Prospects</a></li>
            <? } ?>
		</ul>
	</li>


    <li><a href="contacts.php" id="contacts" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/contact-icon.png" /><span>Contacts</span></a></li>
    
    <? if(!$is_reports_user){ ?>
    <!--network-->
	<li><a href="directory.php" id="network" <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/network-icon.png" /><span>Network</span></a>
		<ul>
			<li><a href="directory.php" <?php echo $additional_style; ?>>Directory</a></li>
			<li><a href="press.php" <?php echo $additional_style; ?>>Press Releases</a></li>
			<li><a href="articles.php" <?php echo $additional_style; ?>>Articles</a></li>
		</ul>
	</li>
	<? } ?>
    <li><a href="phone_temp.php" id="phone"  <? if($isReseller) echo $style3; ?>><img src="<?php echo $_path; ?>img/v3/phones-icon.png" /><span>Phone</span></a></li>

	       
            
            <li><a href="<?php echo $_path; ?>support.php" title=""><img src="images/icons/mainnav/statistics.png" alt="" /><span>Statistics</span></a></li>
<?php } ?>

        </ul>