<?php //$has_messages = count($messages); ?>

<?php if(isset($spamaction)): ?>
	<div class="nNote nWarning"><p><?= $spamaction; ?></p></div>
<?php endif; ?>

<div class="fluid">    
	<div class="widget grid4">
		<div class="whead"><h6>Spam Messages</h6></div>
		<ul class="updates">
<?php foreach($messages as $spam): ?>
			<li class="jmsg jmsgid-<?= $spam->id; ?>">
				<div style="width: 100%; display: block; overflow: hidden;">
					<span class="uDate"><span><?= $spam->day; ?></span><?= $spam->monthname; ?> <?= $spam->year; ?></span>
					<a href="javascript:void(deleteMessage(<?= $spam->id; ?>));" class="bDefault f_rt" title="Delete the Message"><span class="delete"></span></a>
					<a href="javascript:void(approveMessage(<?= $spam->id; ?>));" class="bDefault f_rt" title="Approve the Message"><span class="approve"></span></a>
				</div>
				<div style="width: 100%; display: block; overflow: hidden;">
					<div style="float: left; display: block;"><span>From:</span>&nbsp;</div>
					<div style="float: left; display: block;"><?= $spam->name; ?> &lt;<?= $spam->email; ?>&gt;</div>
				</div>
<?php if($spam->user_fullname && $User->can('is_support')): ?>
				<div style="width: 100%; display: block; overflow: hidden;">
					<div style="float: left; display: block;"><span>To:</span>&nbsp;</div>
					<div style="float: left; display: block;"><?= ucwords($spam->user_fullname); ?></div>
				</div>
<?php endif; ?>
				<div class="messagesOne">
					<div class="by_user"><span class="jmsg-label-<?= $spam->id; ?>">Message:</span>
						<div class="jmsg-comments"><?php $this->p($spam->comments); ?>
<?php if($spam->is_summary): ?>
							<a href="javascript:void(loadMessage(<?= $spam->id; ?>));" class="buttonS f_rt"><span>View the Message</span></a>
<?php endif; ?>
						</div>
					</div>
				</div>
			</li>
<?php endforeach; ?>
			<li class="jno-messages center<?= $has_messages ? ' hide' : ''; ?>">No Spam Messages :)</li>
		</ul>
	</div>
</div>

<?php if($has_messages && 1): ?>
	<div class="messagesOne widget hide"><div class="by_user"><div class="messageArea"><div id="jmsg-comments"><?php $this->p($top_msg->comments); ?></div></div></div></div>
<?php endif; ?>

<script type="text/javascript">
<?php $this->start_script(); ?>
    
	function populate(prefix, keys, object){
        var els =   {};
        for(var k in keys){
            var keyname =   keys[k];
            els[keyname]    =   $(prefix + keyname).text(object[keyname]);
        }
        object._text  =   function (name, value){
            els[name].text(value);
        }
        object._get = function (name){
            return els[name];
        }
    }
    
    function approveMessage(mID)
    {
        sendAjax("<?php echo $this->action_url('campaign_approvespam'); ?>&ID=<?=$Campaign->id?>&MID=" + mID, function (){
			$('.jmsgid-' + mID).fadeOut('slow', function (){
				$(this).remove();
				if($('.jmsg').length == 0)
					$('.jno-messages').removeClass('hide');
			});
		},
		{'MID': mID});
    }
    
    function deleteMessage(mID){
        sendAjax("<?php echo $this->action_url('campaign_deletespam'); ?>&ID=<?=$Campaign->id?>&MID=" + mID, function (){
			$('.jmsgid-' + mID).fadeOut('slow', function (){
				$(this).remove();
				if($('.jmsg').length == 0)
					$('.jno-messages').removeClass('hide');
			});
		},
		{'MID': mID});
    }
    
    function loadMessage(mID){
        sendAjax("<?php echo $this->action_url('campaign_loadspam-msg'); ?>&ID=<?=$Campaign->id?>",
        function (J){
			var msg =   J.message;
			populate('.jmsgid-' + msg.id + ' .jmsg-', ['comments', 'name', 'email'], msg);
			
			if(msg.comments == '')
				msg._text('comments', "[ No Message ]");   
			
			//$('.jmsgid-' + msg.id + ' .wNews').addClass('hide').in
			//$('.messagesOne').appendTo('.jmsgid-' + msg.id).removeClass('hide');
			$('span.jmsg-label-' + msg.id).show();
        },
        {'MID': mID});
    }

<?php $this->end_script(); ?>
</script>