<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Phone Lines</h6>
			<div class="buttonS f_rt jopendialog jopen-addPhone"><span>Add Phone Line</span></div>
		</div>
		
<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Phones'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jPhones',
	'data'				=> $Phones,
	'rowkey'			=> 'Phone',
	'rowtemplate'		=> 'phones.php',
	'total'				=> $total_phones_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxphones',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<?php $this->getDialog('add-phone.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {

		//===== Create New Form + PopUp =====//
		$('#jdialog-addPhone').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true
		});

	});

<?php $this->end_script(); ?>
</script>

