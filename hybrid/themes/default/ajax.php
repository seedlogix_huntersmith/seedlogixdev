<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
if($object instanceof HybridModel)
	echo json_encode($object->jsonSerialize());
else
	echo json_encode($object);