<?php

	$disable = $User->can('edit_' . $site->getTouchpointType(), $site->getID()) ? "" : "disabled";
	$isAdminUser = $this->isMU;
	$gridLength = 'grid9';
	
	$baseAuthURL = 'http://login.seedlogix.com/api/';
	
	if($multi_user){
		$gridLength = 'grid8';
	}
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">

<?php $this->getWidget('admin-lock-message.php'); ?>

<div class="grid12 m_tp">
	<form action="<?php echo $this->action_url("{$site->touchpoint_type}_save?ID=" . $site->id); ?>" method="POST" class="ajax-save">
		<div class="widget grid6">
			<div class="whead"><h6><?php if($site->touchpoint_type=='PROFILE') { ?>Social Mashup Connections<?php }
					else {?>Social Networking Connections<?php } ?></h6></div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Blog RSS Feed URL<?php }
						else {?>Your Blog URL<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'blog', 'value' => $site->blog, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Twitter Profile ID<?php }
						else {?>Twitter<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'twitter', 'value' => $site->twitter, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Facebook Page ID<?php }
						else {?>Facebook<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'facebook', 'value' => $site->facebook, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>LinkedIn Page ID<?php }
						else {?>LinkedIn<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'linkedin', 'value' => $site->linkedin, 'placeholder' => ''));?>
			</div>
            <div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Google+ Page ID<?php }
						else {?>Google+<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'google_local', 'value' => $site->google_local, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Google Places ID<?php }
						else {?>Google Places<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'google_places', 'value' => $site->google_places, 'placeholder' => ''));?>
			</div>
			<?php if($site->touchpoint_type!='PROFILE') { ?>
			<div class="formRow">
				<div class="grid3"><label>YouTube:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'youtube', 'value' => $site->youtube, 'placeholder' => ''));?>
			</div>
			<?php } ?>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Instagram Username<?php }
						else {?>Instagram<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'instagram', 'value' => $site->instagram, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Yelp Page ID<?php }
						else {?>Yelp<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'yelp', 'value' => $site->yelp, 'placeholder' => ''));?>
			</div>
			<div class="formRow">
				<div class="grid3"><label><?php if($site->touchpoint_type=='PROFILE') { ?>Pinterest Username<?php }
						else {?>Pinterest<?php } ?>:</label></div>
				<?php $this->getWidget('input-website.php', array('name' => 'pinterest', 'value' => $site->pinterest, 'placeholder' => ''));?>
			</div>
			<?php if($site->touchpoint_type=='PROFILE') { ?>
			<div class="formRow">
				<div class="grid3"><label>Social Mashup Design:</label></div>
				<div class="grid9">

					<select name="website[full_home]" class="select2" style="width:100px;">
						<option value="Timeline" <?php if($site->full_home=='Timeline') echo 'selected';?>>Timeline</option>
						<option value="Masonry" <?php if($site->full_home=='Masonry') echo 'selected';?>>Masonry</option>
					</select></div>


			</div>
				<div class="formRow">
					<div class="grid3"><label>Facebook Publishing:</label></div>
					<?php               if($site->facebook_id): ?>
						<div class="grid8"><input type="text" value="<?php $this->p($site->facebook_id); ?>" name="website[facebook_id]" disabled="disabled" id="facebook"></div>
						<div class="grid1"><a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="facebook"><span class="delete"></span></a></div>
					<?php               else: ?>
						<div class="grid9"><a href="<?= $baseAuthURL; ?>facebook-website-app.php?sitehttphost=<?php $this->p($site->httphostkey); ?>" class="buttonS f_lt <?=$canEdit?> _blank"><span>Authorize Facebook</span></a></div>
					<?php               endif; ?>
				</div>
			<?php } ?>

            <?php   if($User->parent_id == 6312 || $User->id == 6312) { ?>
             <div class="formRow">
                    <div class="grid3"><label>Facebook Publishing:</label></div>
<?php               if($site->facebook_id): ?>
                    <div class="grid8"><input type="text" value="<?php $this->p($site->facebook_id); ?>" name="website[facebook_id]" disabled="disabled" id="facebook"></div>
                    <div class="grid1"><a title="Un-Authorize" class="bDefault f_lt sixQube_remove" role="facebook"><span class="delete"></span></a></div>
<?php               else: ?>
                    <div class="grid9"><a href="<?= $baseAuthURL; ?>facebook-website-app.php?sitehttphost=<?php $this->p($site->httphostkey); ?>" class="buttonS f_lt <?=$canEdit?> _blank"><span>Authorize Facebook</span></a></div>
<?php               endif; ?>
                </div>
            <? } ?>
		</div><!-- //section -->
		<input type="hidden" value="<?= $site->touchpoint_type; ?>" name="website[touchpoint_type]">
	</form>
</div>
</div>

<script type="text/javascript">
<?php $this->start_script(); ?>
$(document).ready(function (){

	<?php $this->getJ('admin-lock.php'); ?>
	
	//allow the user to remove their connection?
        var allowDelete = false;

		$('a.sixQube_remove').click(function(event){
            
            var targetName = $(this).attr('role');
            
            if(allowDelete){
                $('#'+targetName).val('').trigger('change');
            }
            else
				notie.alert({text:'Contact your system administrator to unauthorize ' + targetName + '.'});
            event.preventDefault();
            return false;
        });

});
<?php $this->end_script();  ?>
</script>
