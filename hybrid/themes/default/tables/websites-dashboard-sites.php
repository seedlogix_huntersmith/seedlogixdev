<?php
/**
 * This table is based on a view `dashboard_sites_analytics_summary` which combines current day analytics with user sites.
 * Any modifications to that view will have direct effect on this table.
 */

if($touchpoint_type == 'PROFILE'){
    $can_create = TRUE;
} else{
    $can_create = $User->can('create_'.(isset($touchpoint_type) ? $touchpoint_type : 'touchpoint'),$campaign_id);
}

$is_limit       = $User->verifyLimit("create_$touchpoint_type",array('Validation' => new ValidationStack()));
?>

<input name="exportURL" type="hidden" class="action_url" value="<?= $this->action_url('piwikAnalytics_getCSVData?chart=CampaignTouchPoints&period=PERIOD&interval=INTERVAL&iSortCol_0=SORTCOL&sSortDir_0=SORTDIR&cid='.$campaign_id); ?>">
<div class="widget">
    <div class="whead">
        <h6><?= $selected_pointtype_label; ?> on <?= date('m/d/Y'); ?></h6>
        <a id="exportCampaign" title="Export the Results" href="<?= $this->action_url('piwikAnalytics_getCSVData?chart=CampaignTouchPoints&period=DAY&interval=7&cid='.$campaign_id); ?>" class="bDefault f_rt"><span class="export"></span></a>

<?php if($selected_pointtype_label !== 'Touchpoints'): ?>

        <div class="buttonS f_rt<?= $can_create && $is_limit ? ' jopen-createwebsite jopendialog' : ' disabled" title="Change your subscription to create more '.$selected_pointtype_label; ?>" id="create-website"><span>Create a New <?= substr($selected_pointtype_label,0,-1); ?></span></div>

<?php endif; ?>

    </div>

<?php
$params                     = array('ID' => $campaign_id);
if(isset($touchpoint_type) && $touchpoint_type != ''){
    $params['pointtype']    = $touchpoint_type;
}

$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Websites'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'websites',
	'data'				=> $sites,
	'rowkey'			=> 'site',
	'rowtemplate'		=> 'website-stats.php',
	'total'				=> $total_tp_sites,
	'sourceurl'			=> $this->action_url('campaign_ajaxGetSites',$params),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

</div>