<div class="widget">
	<div class="whead"><h6>Latest Prospects</h6></div>

<?php
$this->getWidget('datatable.php',array(
	'columns'		=> DataTableResult::getTableColumns('Prospects'),
	'class'			=> 'col_2nd,col_end',
	'id'			=> 'ProspectsCampaign',
	'data'			=> $prospects,
	'rowkey'		=> 'Prospect',
	'rowtemplate'	=> 'dashboard-prospect.php',
	'total'			=> $totalProspects,
	'sourceurl'		=> $this->action_url($touchpoint_type.'_ajaxProspects?ID='.$site->getID()),
	'DOMTable'		=> '<"H"lf>rt<"F"ip>',
	'params'		=> array('table' => 'Prospects')
));
?>

</div>