<div class="widget">
            <div class="whead"><h6>Forms</h6></div>
                <table cellpadding="0" cellspacing="0" border="0" class="dTable prospTable" id="dynamic">
                <thead>
                <tr>
                	<th>Name <span class="sorting" style="display: block;"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                	if($forms):
                		foreach($forms as $form): 
                ?>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Campaign_editform', array('id' => $form->id)); ?>"><?php 
                                    echo ucwords($form->meta('name'));
                                ?></a>
                	</td>
                </tr>
                <?php 
                		endforeach;
                	endif; 
                ?>
                </tbody>
                </table> 
        </div>