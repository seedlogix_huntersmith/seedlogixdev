<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
                <div class="widget check grid12">
            <div class="whead">
                <span class="titleIcon"><div class="checker" id="uniform-titleCheck"><span><input type="checkbox" id="titleCheck" name="titleCheck" style="opacity: 0; "></span></div></span>
                <h6>Media table</h6><div class="clear"></div>
            </div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tDefault checkAll tMedia" id="checkAll">
                <thead>
                    <tr>
                        <td><img src="images/elements/other/tableArrows.png" alt=""></td>
                        <td width="50">Image</td>
                        <td class="sortCol header"><div>Description<span></span></div></td>
                        <td width="130" class="sortCol header"><div>Date<span></span></div></td>
                        <td width="120">File info</td>
                        <td width="100">Actions</td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="itemActions">
                                <label>Apply action:</label>
                                <div class="selector" id="uniform-undefined"><span>Select action...</span><select style="opacity: 0; ">
                                    <option value="">Select action...</option>
                                    <option value="Edit">Edit</option>
                                    <option value="Delete">Delete</option>
                                    <option value="Move">Move somewhere</option>
                                </select></div>
                            </div>
                            <div class="tPages">
                                <ul class="pages">
                                    <li class="prev"><a href="#" title=""><span class="icon-arrow-14"></span></a></li>
                                    <li><a href="#" title="" class="active">1</a></li>
                                    <li><a href="#" title="">2</a></li>
                                    <li><a href="#" title="">3</a></li>
                                    <li><a href="#" title="">4</a></li>
                                    <li>...</li>
                                    <li><a href="#" title="">20</a></li>
                                    <li class="next"><a href="#" title=""><span class="icon-arrow-17"></span></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td><div class="checker" id="uniform-undefined"><span><input type="checkbox" name="checkRow" style="opacity: 0; "></span></div></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="images/live/face3.png" alt=""></a></td>
                        <td class="textL"><a href="#" title="">Image1 description</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="fileInfo"><span><strong>Size:</strong> 215 Kb</span><span><strong>Format:</strong> .jpg</span></td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Edit"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Remove"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Options"><span class="iconb" data-icon=""></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="checker" id="uniform-undefined"><span><input type="checkbox" name="checkRow" style="opacity: 0; "></span></div></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="images/live/face7.png" alt=""></a></td>
                        <td class="textL"><a href="#" title="">Image1 description</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="fileInfo"><span><strong>Size:</strong> 215 Kb</span><span><strong>Format:</strong> .jpg</span></td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Edit"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Remove"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Options"><span class="iconb" data-icon=""></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="checker" id="uniform-undefined"><span><input type="checkbox" name="checkRow" style="opacity: 0; "></span></div></td>
                        <td><a href="images/big.png" title="" class="lightbox"><img src="images/live/face6.png" alt=""></a></td>
                        <td class="textL"><a href="#" title="">Image1 description</a></td>
                        <td>Feb 12, 2012. 12:28</td>
                        <td class="fileInfo"><span><strong>Size:</strong> 215 Kb</span><span><strong>Format:</strong> .jpg</span></td>
                        <td class="tableActs">
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Edit"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Remove"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Options"><span class="iconb" data-icon=""></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="noBorderB"><div class="checker" id="uniform-undefined"><span><input type="checkbox" name="checkRow" style="opacity: 0; "></span></div></td>
                        <td class="noBorderB"><a href="images/big.png" title="" class="lightbox"><img src="images/live/face5.png" alt=""></a></td>
                        <td class="textL noBorderB"><a href="#" title="">Image1 description</a></td>
                        <td class="noBorderB">Feb 12, 2012. 12:28</td>
                        <td class="fileInfo noBorderB"><span><strong>Size:</strong> 215 Kb</span><span><strong>Format:</strong> .jpg</span></td>
                        <td class="tableActs noBorderB">
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Edit"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Remove"><span class="iconb" data-icon=""></span></a>
                            <a href="#" class="tablectrl_small bDefault tipS" original-title="Options"><span class="iconb" data-icon=""></span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
            