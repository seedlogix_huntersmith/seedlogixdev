<?php
/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
/* @var $statsTable RenderedTable */
?>

<input name="exportURL" type="hidden" class="action_url" value="<?= $this->action_url('piwikAnalytics_getCSVData?chart=TableCompareChartsData&period=PERIOD&interval=INTERVAL&iSortCol_0=SORTCOL&sSortDir_0=SORTDIR'); ?>">
<div class="fluid">
	<div class="widget">
        <div class="whead">
            <h6>Campaigns</h6>
            <a id="exportCampaign" title="Export the Results" href="<?= $this->action_url('piwikAnalytics_getCSVData?chart=TableCompareChartsData&period=DAY&interval=7'); ?>" class="bDefault f_rt"><span class="export"></span></a>
            <div class="buttonS f_rt jopendialog jopen-createcampaign"><span>Create a New Campaign</span></div>
        </div>
        <form action="<?= $this->action_url('campaign_save'); ?>">

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Campaigns'),
	'class'				=> 'col_1st,col_end',
	'id'				=> 'campaigns-stats-tbl',
	'data'				=> $campaigns,
	'rowkey'			=> 'camp',
	'rowtemplate'		=> 'campaign-stats.php',
	'total'				=> $total_camps,
	'sourceurl'			=> $this->action_url('piwikAnalytics_ajaxCampaigns'),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>',
    'params'            => array('chart' => 'TableCompareChartsData')
));
?>

        </form>
	</div>
</div>