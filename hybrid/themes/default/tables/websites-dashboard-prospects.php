<div class="widget">
    <div class="whead"><h6>Latest Prospects</h6></div>

<?php
$this->getWidget('datatable.php',array(
    'columns'       => DataTableResult::getTableColumns('ProspectsDashboardCampaign'),
    'class'         => 'col_2nd,col_end',
    'id'            => 'ProspectsCampaign',
    'data'          => $prospects,
    'rowkey'        => 'prospect',
    'rowtemplate'   => 'Prospect.php',
    'total'         => $totalProspects,
    'sourceurl'     => $this->action_url('Campaign_ajaxDashboardProspects?ID='.$campaign_id),
    'DOMTable'      => '<"H"lf>rt<"F"ip>',
    'params'        => array('TOUCHPOINT_TYPE' => $touchpoint_type)
));
?>

</div>