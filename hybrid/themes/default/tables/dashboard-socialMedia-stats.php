<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
?>
<div class="widget">
        <div class="whead">
            <h6>Social Media for {Campaign Name}</h6>
            <div class="buttonS bLightBlue right dynRight" id="exportCampaign">Export</div>
            <div class="buttonS bGreen right dynRight open-dialog" id="open-create-campaign">Create Social Media</div>
            <div class="clear"></div>
        </div>
        <div id="dyn">
            <table cellpadding="0" cellspacing="0" border="0" class="dTable" id="campaigns-stats-tbl">
            <thead>
            <tr>
                    <th>Social Media Name <span class="sorting" style="display: block;"></span></th>
                    <th>Total Visits</th>
                    <th>Total Leads</th>
                    <th>Conversion Rate</th>
                    <th>Touchpoints</th>
                    <th>Social Media Creation Date</th>
                    <th>Action</th>
            </tr>
            </thead>
            <tbody>
                
            <tr class="gradeA">
                    <td><a href="<?php echo $this->action_url('Social Medias_dashboard', array('ID' => '00000')); ?>">Social Media Name</a></td>
                    <td class="center">5</td>
                    <td class="center">3</td>
                    <td class="center">0%</td>
                    <td class="center">1</td>
                    <td class="center">December 26th, 2012</td>
                    <td class="center">
                            <a href="#" original-title="Edit Social Media">
                                    <span class="icos-create2"></span>
                            </a>
                            <a href="#" original-title="Delete Social Media">
                                    <span class="icos-cross"></span>
                            </a>
                    </td>
            </tr>
            <tr class="gradeA">
                    <td><a href="<?php echo $this->action_url('Social Medias_dashboard', array('ID' => '00000')); ?>">Social Media Name</a></td>
                    <td class="center">5</td>
                    <td class="center">3</td>
                    <td class="center">0%</td>
                    <td class="center">1</td>
                    <td class="center">December 26th, 2012</td>
                    <td class="center">
                            <a href="#" original-title="Edit Social Media">
                                    <span class="icos-create2"></span>
                            </a>
                            <a href="#" original-title="Delete Social Media">
                                    <span class="icos-cross"></span>
                            </a>
                    </td>
            </tr>
            <tr class="gradeA">
                    <td><a href="<?php echo $this->action_url('Social Medias_dashboard', array('ID' => '00000')); ?>">Social Media Name</a></td>
                    <td class="center">5</td>
                    <td class="center">3</td>
                    <td class="center">0%</td>
                    <td class="center">1</td>
                    <td class="center">December 26th, 2012</td>
                    <td class="center">
                            <a href="#" original-title="Edit Social Media">
                                    <span class="icos-create2"></span>
                            </a>
                            <a href="#" original-title="Delete Social Media">
                                    <span class="icos-cross"></span>
                            </a>
                    </td>
            </tr>
            <tr class="gradeA">
                    <td><a href="<?php echo $this->action_url('Social Medias_dashboard', array('ID' => '00000')); ?>">Social Media Name</a></td>
                    <td class="center">5</td>
                    <td class="center">3</td>
                    <td class="center">0%</td>
                    <td class="center">1</td>
                    <td class="center">December 26th, 2012</td>
                    <td class="center">
                            <a href="#" original-title="Edit Social Media">
                                    <span class="icos-create2"></span>
                            </a>
                            <a href="#" original-title="Delete Social Media">
                                    <span class="icos-cross"></span>
                            </a>
                    </td>
            </tr>
            
            
            </tbody>
            </table> 
        </div>
        <div class="clear"></div> 
</div>


<!-- USe for later -->

<!--
<?php //foreach($campaigns as $camp): ?>
            <tr class="gradeA">
                    <td><a href="<?php //echo $this->action_url('Social Medias_dashboard', array('ID' => '00000')); ?>">Social Media Name</a></td>
                    <td class="center"><?php //echo $camp->nb_visits; ?></td>
                    <td class="center"><?php //echo $camp->numleads; ?></td>
                    <td class="center"><?php //echo $camp->convert_rate; ?>%</td>
                    <td class="center"><?php //echo $camp->nb_touchpoints; ?></td>
                    <td class="center"><?php //echo $camp->created_str; ?></td>
                    <td class="center">
                            <a class="tipN" href="#" original-title="Edit Campaign">
                                    <span class="icos-create2"></span>
                            </a>
                            <a class="tipN" href="#" original-title="Delete Campaign">
                                    <span class="icos-cross"></span>
                            </a>
                    </td>
            </tr><?php //endforeach; ?>
-->
