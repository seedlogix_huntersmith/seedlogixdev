<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */

//var_dump($User->getID(), $User->can('reports_only'));
?>



<div class="fluid">
    <div class="widget check">
        <div class="whead"><h6>Leads for All Campaigns</h6></div>

<?php
$datatable_info = array(
    'columns'       => DataTableResult::getTableColumns('Prospects'),
    'class'         => 'col_2nd,col_end',
    'id'            => 'AllProspects',
    'data'          => $Prospects,
    'rowkey'        => 'Prospect',
    //'rowtemplate'   => 'dashboard-prospect.php',
    'total'         => $totalProspects,
    'sourceurl'     => $this->action_url('campaign_ajaxProspects'),
    'DOMTable'      => '<"H"lf>rt<"F"ip>'
);

if($User->id == 6610 || !$User->can('reports_only')) {
    $datatable_info['rowtemplate'] = 'dashboard-prospect.php';
    
} else if($User->id != 6610 && $User->can('reports_only')) {
    $datatable_info['rowtemplate'] = 'dashboard-prospect-no-url.php';
    
}

$this->getWidget('datatable.php', $datatable_info);
?>

    </div>
</div>