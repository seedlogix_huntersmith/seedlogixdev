
<div class="widget">
    <div class="whead">
        <h6>Top 5 Performing <?= $selected_pointtype_label; ?></h6>
        <select data-placeholder="Filter By&hellip;" class="select2" name="select2" id="topOrder">
            <option></option>
            <option value="4">Conversion Rate</option>
            <option value="2">Total Visits</option>
            <option value="3">Total Leads</option>
        </select>
    </div>
<?php
    $params = array('ID' => $campaign_id);
    if(isset($touchpoint_type) && $touchpoint_type != '')
        $params['pointtype'] = $touchpoint_type;
    $this->getWidget('datatable.php',
        array('columns' => DataTableResult::getTableColumns('TopPerforming'),
            'class' => 'col_1st,col_end',
            'id' => 'top-touchpoints',
            'data' => $topsites,
            'rowkey' => 'site',
            'rowtemplate' => 'website-stats.php',
            'total' => $total_top_sites,
            'sourceurl' => $this->action_url('campaign_ajaxTopPerformingSites', $params),
            'DOMTable' => 'rt<"F"i>'
        )
    );
?>
</div>