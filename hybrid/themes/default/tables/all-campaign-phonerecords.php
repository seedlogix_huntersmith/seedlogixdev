<?php
// Request Type Options
$options            = array(
                    'Today'             => 'Today',
                    'PreviousDay'       => 'Previous Day',
                    'PreviousWeek'      => 'Previous Week',
                    'Previous7Days'     => 'Previous 7 Days',
                    'PreviousMonth'     => 'Previous Month',
                    'PreviousQuarter'   => 'Previous Quarter',
                    'MonthToDate'       => 'Month to Date',
                    'YearToDate'        => 'Year to Date'
                    );

$start              = $month = strtotime(date('Y-m-d'));
if(!$created || $created == '0000-00-00'){
    $end            = strtotime('-1 month',$month);
} else{
    $end            = strtotime($created);
}
$a                  = 0;
?>

<div class="fluid">

<?php if(!$did): ?>

    <div class="nNote nInformation"><span>Please contact Support to setup your phone tracking.</span><span class="close"></span></div>
    <div class="grid12 m_tp">

<?php endif; ?>

    <div class="widget check">
        <div class="whead">
            <h6>Monthly Calls for all Campaigns</h6>
            <select data-placeholder="Filter By Month&hellip;" class="select2 s6_recordRange" name="rqtype" id="recordConfig">
                <option></option>

<?php
while($month > $end):
    $listdate       = date('F Y',$month);
    $monthvalue     = date('m',$month);
    $yearvalue      = date('Y',$month);
    $daysvalue      = date('t',$month);
?>

                <option value="<?= $yearvalue.'-'.$monthvalue.'-01,'.$yearvalue.'-'.$monthvalue.'-'.$daysvalue; ?>"<?= $key == $rqtype ? ' selected' : ''; ?>><?= $listdate; ?></option>

<?php
    $month          = strtotime('-1 month',$month);
endwhile;
?>

            </select>
        </div>
        <table id="jCallReports" class="tDefault dTable" style="width: 100%;">
            <thead class="dataTableHead">
                <tr>
                    <th>Date</th>
                    <th>Caller ID</th>
                    <th>Destination</th>
                    <th>Length</th>
                    <th>Download Recording</th>
                </tr>
            </thead>
            <tbody class="dataTableBody">

<?php $this->getRow('campaign-callReports.php'); ?>

            </tbody>
        </table>
    </div><!-- widget -->

<?php if(!$did): ?>

    </div><!-- /grid12.m_tp -->

<?php endif; ?>

</div><!-- fluid -->

<script type="text/javascript">
<?php $this->start_script(); ?>    
    $(function (){ // ON PAGE LOAD

        // Download Link
        $('.phoneDownload').live('click', function(event){

            var dlBtn = $(this);
            // if disabled then don't fetch again.
            if(dlBtn.hasClass('disabled'))
                return false;
            var ajaxURL = 'admin.php?action=ajaxPhoneRecording&ctrl=campaign';
            var number = $(this).attr('href');

            dlBtn.addClass('disabled');

            // ajax call
            $.ajax({
                type: "POST",
                url: ajaxURL,
                data: { number: number },
                dataType: "json",
                success: function(resultData, resultStatus){
                    var downloadURL = resultData.response.objects[0].recording_url;
                   window.location.assign(downloadURL);
                },
                complete: function(){
                    //gross
                    setTimeout(function() { dlBtn.removeClass('disabled');  }, 4000)
                }
            });

            // stop link from going anywhere
            event.stopPropagation();
            return false;
        });

        // Range selector
        $('#recordConfig').change(function(){
			$('#jCallReports').dataTable().fnDraw();
        });

        // Call Reports dTable
        $('#jCallReports').dataTable({
            "aoColumns": [
                { "sClass": "center", "sWidth": "175px"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center"},
                { "sClass": "center", "sWidth": "200px"}
            ],
            "aaSorting": [[0,'desc']],
            "bDeferRender": true,
            "aLengthMenu": [5,10,25,50,75,100],
            "language": {
                "search": "_INPUT_",
                "searchPlaceholder": "Search…"
            },
            /*"autoWidth": false,*/
            "pagingType": "simple",
            "bProcessing": true,
            "bServerSide": true,
            "sServerMethod": "POST",
            "iDeferLoading": <?=$totalResults-$a?>,
            "iDisplayLength": 10,
            "sAjaxSource": "<?=$this->action_url('campaign_ajaxPhone')?>",
            "bDestroy" : true,
            "sDom" : '<"tablePars"lf>rt<"fg-toolbar tableFooter"ip>',
			'fnServerParams': function(aoData){
				var RequestType = $('#recordConfig').val();
				aoData.push({name: 'requestType', value: RequestType});
				aoData.push({name: 'did', value: '<?=$did?>'});
                aoData.push({name: 'aaSorting', value: [[0,'desc']]});
			}
        });


        //Mailbox Dialog
        <?php $this->getJ('mailbox-options-js.php'); ?>

    });
<?php $this->end_script(); ?>
</script>