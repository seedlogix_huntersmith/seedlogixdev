<div class="fluid">
    <div class="widget grid6">
                    <div class="whead"><h6>Input fields</h6><div class="clear"></div></div>
                    <div class="formRow">
                        <div class="grid3"><label>Email:</label></div>
                        <div class="grid9"><input type="text" name="regular"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Password:</label></div>
                        <div class="grid9"><input type="password" name="pass"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Access:</label></div>
                        <div class="grid9"><input type="text" placeholder="Bazinga" name="placeholder"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Canceled:</label></div>
                        <div class="grid9"><input type="text" value="read only" readonly="readonly" name="readonly"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            <div class="widget grid6">
                <div class="whead"><h6>6Qube Admin Options Select User managers</h6><div class="clear"></div></div>
                
                    <div class="formRow">
                        <div class="grid3"><label>Client Access:</label></div>
                        <div class="grid9"><input type="text" value="disabled" disabled="disabled" name="disabled"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Class:</label></div>
                        <div class="grid9"><input type="text" name="regular"><span class="note">Some nice stuff goes here</span></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>User Type:</label></div>
                        <div class="grid9"><input type="text" value="these,are,sample,tags" class="tags" name="tags" id="tags" style="display: none;"><div class="tagsinput" id="tags_tagsinput" style="width: 100%;"><span class="tag"><span>these&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span><span class="tag"><span>are&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span><span class="tag"><span>sample&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span><span class="tag"><span>tags&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span><div id="tags_addTag"><input data-default="add a tag" value="" id="tags_tag" style="color: rgb(102, 102, 102); width: 60px;"></div><div class="tags_clear"></div></div></div>
                        <div class="clear"></div>
                    </div>
            </div>
        </div>

<div class="widget">
            <div class="invoice">
                <div class="inHead">
                    <span class="inLogo"><a title="invoice" href="index.html"><img alt="logo" src="<?= $Branding->getLogoUrl(); ?>"></a></span>
                    <div class="inInfo">
                        <span class="invoiceNum">Invoice # 1258</span>
                        <i>May 18, 2011</i>
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="inContainer">
                    
                    <div class="floatR">
                        
                <!-- Send message widget -->
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="inFooter">
                    <div class="footnote">Thank you very much for choosing us. It was pleasure to work with you.</div>
                    <ul class="cards">
                        <li class="discover"><a href="#"></a></li>
                         <li class="visa"><a href="#"></a></li>
                         <li class="mc"><a href="#"></a></li>
                         <li class="pp"><a href="#"></a></li>
                         <li class="amex"><a href="#"></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

<div class="fluid">
            <div class="widget grid6">
                <div class="whead"><h6>User Info </h6><div class="clear"></div></div>
                <div class="body">                    
                        <h5><?php echo $userobj->company; ?></h5>
                        <span><?php echo $userobj->address; ?></span>
                        <span><?php echo $userobj->address2; ?></span>
                        <span><?php echo $userobj->city; ?></span>
                        <span><?php echo $userobj->state; ?></span>
                        <span><?php echo $userobj->zip; ?></span>
                        <span class="number">Mobile Phone: <strong class="red">+<?php echo $userobj->phone; ?></strong></span>
                        <span class="black">Send To: <a href="#"><?php echo $userobj->email; ?></a></span>
                        <span>Created <strong><?php echo $userobj->created_date; ?></strong></span>
                </div>
            </div>
    
                <div class="widget grid6">
                    <div class="body">
                        <div class="messageTo">
                            <a href="#" title="" class="uName"><img src="images/live/face5.png" alt=""></a><span> Send message to <strong>Eugene</strong></span>
                            <a href="#" title="" class="uEmail">e.kopyov@gmail.com</a>
                        </div>
                        <textarea style="overflow: hidden; height: 74px;" rows="5" cols="" name="textarea" class="auto" placeholder="Write your message"></textarea>
                        <div class="mesControls">
                            <span><span class="iconb" data-icon=""></span>Some basic <a href="#" title="">HTML</a> is OK</span>
                            
                            <div class="sendBtn sendwidget">
                                <a href="#" title="" class="attachPhoto"></a>
                                <a href="#" title="" class="attachLink"></a>
                                <input name="sendMessage" class="buttonM bLightBlue" value="Send message" type="submit">
                            </div>
                            <div class="clear"></div>
                        </div>
                    <div style="position: absolute; display: none; word-wrap: break-word; white-space: pre-wrap; border-left: 1px solid rgb(215, 215, 215); border-color: rgb(215, 215, 215); border-style: solid; border-width: 1px; font-weight: 400; width: 333px; font-family: Arial,Helvetica,sans-serif; line-height: 12px; font-size: 11px; padding: 6px 7px;">&nbsp;</div></div>
                </div>   
        </div>

<?php $this->getWidget('touchpoints.php'); ?>

