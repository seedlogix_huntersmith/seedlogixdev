<?php
/*
include_once(HYBRID_PATH.'classes/colors.inc.php');
$gc		= new GetMostCommonColors();
$gc_arr	= $gc->Get_Color(ltrim($Branding->getLogoUrl(false),'/'),1,true,true,1);
*/
?>

<div class="loginWrapper reset">

<?php //if(isset($gc_arr) && is_array($gc_arr)): foreach($gc_arr as $hex => $count): if($count > 0): ?>

	<div class="brandlogo" style="<?php //$hex; ?>"><img src="<?= $Branding->getLogoUrl(); ?>" alt=""></div>

<?php //endif; endforeach; endif; ?>

	<h2>Hello.</h2>
	<h6>Please enter your new password.</h6>
    <form action="<?= $this->action_url('Auth_reset?token='.htmlentities($tokenstr,ENT_QUOTES)); ?>" id="login" method="POST">
		<span class="email"></span>
		<input type="password" name="password" placeholder="New Password">
		<span class="password"></span>
		<input type="password" name="password-confirm" placeholder="Confirm Password">
		<button type="submit" name="submit"><span>Save</span></button>
	</form>

<?php if(isset($messages) && is_array($messages)): foreach($messages as $msg): ?>

<?= $msg->body; ?>

<?php endforeach; elseif(isset($errors) && is_array($errors)): ?>

<?= $errors['password'][0]; ?>

<?php endif; ?>

</div>