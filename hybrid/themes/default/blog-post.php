<?php
if((bool)$User->override_admin_content === false){
    $edit       = $User->can('edit_blogpost',$post->id) ? '' : ' disabled';
} else{
    $edit       = null;
}
?>

<?php $this->getMenu('middleNav-website.php'); ?>

<div class="fluid">
    <form action="<?php echo $this->action_url('Blog_save?ID=' . $blog->id . '&post=' . $post->id); ?>" method="POST" class="ajax-save">
            <div class="widget grid6">
                <div class="whead"><h6>Post Content</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Post Title:</label></div>
                    <div class="grid9"><input type="text" name="post[post_title]" value="<?php $this->p($post->post_title); ?>"<?= $edit; ?>></div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Post Category:</label></div>
                    <div class="grid4 inputSwitcher">
                        <div class="inputSwitcher-select">
                            <input id="SL_CategoryNameReadable" type="hidden" name="post[category_name_readable]" value="">
                            <select id="post-category" name="post[category]"<?= $edit; ?>>
                                <optgroup label="Existing Categories">
                                    <?php foreach($categories as $category => $category_value):?>
                                        <option category-name-readable="<?= $category; ?>"<?= $category == $post->category ? ' selected' : ''; ?> value="<?php $this->p($category_value); ?>"><?= $category; ?></option>
                                    <?php endforeach;?>
                                </optgroup>
                                <optgroup label="New Category">
                                    <option value="_new">Create a New Category</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="inputSwitcher-input hide">
                        <input type="text" name="post[category]" value="<?php $this->p($post->category); ?>"<?= $edit; ?>>
                        </div>
                    </div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Include in Network:</label></div>
                    <div class="grid9"><input type="checkbox" name="post[network]" class="i_button" value="1"<?php $this->checked($post->network,1); ?><?= $edit; ?>></div>
                </div>
                <div class="formRow">Post Content
                    <div class="grid12">
                        <div class="body j-dual-ide" style="position: relative;">
                            <?php $this->getWidget('wysiwyg-ide.php', array('fieldname' => 'post[post_content]', 'fieldvalue' => $post->post_content)); ?>
                        </div>
                    </div>
 
                </div>
            </div>
            <div class="widget grid6">
                <div class="whead"><h6>Post Settings</h6></div>
                <div class="formRow">
                    <div class="grid3"><label>Schedule Date:</label></div>
                    <div class="grid4"><input type="text" class="datepicker" value="<?php $this->p($post->publish_date); ?>" name="post[publish_date]"<?= $edit; ?>></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Post Photo:</label></div>
                    <div class="grid9">
                        <?php //wasn't able to QA this @todo check Value 
                            $this->getWidget('moxiemanager-filepicker.php', array(
                            'name' => 'post[photo]',					
                            'value' => $post->photo,
                            'idWidget' => 'blog-logo-widget',
                            'noHost' => true,
							'path' => $storagePath,
							'extensions' => 'jpg,jpeg,png,gif',
                            'disable' => !empty($edit)
						));?>
                    </div>
 
                </div>



                <div class="formRow">
                    <div class="grid11"><label>Video Embed Code:</label></div>
                </div>
                <div class="formRow">
                    <textarea name="post[video_embed]" id="video_embed" class="codemirror"><?php $this->p($post->video_embed); ?></textarea>
                </div>

                <div class="formRow">
                    <div class="grid3"><label>Tags:</label></div>
                    <div class="grid9"><input type="text" id="tags" class="tags"  name="post[tags]" value="<?php $this->p($post->tags); ?>"<?= $edit; ?>></div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Tags URL:</label></div>
                    <div class="grid9"><input type="text" name="post[tags_url]" value="<?php $this->p($post->tags_url); ?>"<?= $edit; ?>></div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>SEO Title *optional:</label></div>
                    <div class="grid9"><input type="text" name="post[seo_title]" value="<?php $this->p($post->seo_title); ?>"<?= $edit; ?>></div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>SEO Description:</label></div>
                    <div class="grid9"><input type="text" name="post[post_desc]" value="<?php $this->p($post->post_desc); ?>"<?= $edit; ?>></div>
 
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Author:</label></div>
                    <div class="grid9"><input type="text" name="post[author]" value="<?php $this->p($post->author); ?>"<?= $edit; ?>></div>

                </div>
                <div class="formRow">
                    <div class="grid3"><label>Author URL:</label></div>
                    <div class="grid9"><input type="text" name="post[author_url]" value="<?php $this->p($post->author_url); ?>"<?= $edit; ?>></div>

                </div>
            </div>
    </form>
</div>

<?php if($blogTheme->hybrid==1) : ?>
    <div class="widget">
        <div class="whead"><h6>Override Global Post Page</h6></div>
        <iframe src="/themes/preview-editor/v1/index.php?blogid=<?=$blog->id?>&postid=<?=$post->id?>&themeid=<?=$blog->theme?>&column=post_edit_1&touchpoint=BLOG" width="100%" id="iframe1" marginheight="0" frameborder="0" scrolling="no"></iframe>
    </div>


    <script type="text/javascript" src="/themes/preview-editor/v1/js/iframeResizer.min.js"></script>
    <script type="text/javascript">

        iFrameResize({
            log                     : true,                  // Enable console logging
            enablePublicMethods     : true,                  // Enable methods within iframe hosted page
            resizedCallback         : function(messageData){ // Callback fn when resize is received
                $('p#callback').html(
                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                    ' <b>Height:</b> '     + messageData.height +
                    ' <b>Width:</b> '      + messageData.width +
                    ' <b>Event type:</b> ' + messageData.type
                );
            },
            messageCallback         : function(messageData){ // Callback fn when message is received
                $('p#callback').html(
                    '<b>Frame ID:</b> '    + messageData.iframe.id +
                    ' <b>Message:</b> '    + messageData.message
                );
                alert(messageData.message);
            },
            closedCallback         : function(id){ // Callback fn when iFrame is closed
                $('p#callback').html(
                    '<b>IFrame (</b>'    + id +
                    '<b>) removed from page.</b>'
                );
            }
        });


    </script>
    <?php endif ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

    $(function (){
        <?php
            $this->getJ('select-category-js.php');
            $this->getJ('wysiwyg-ide.php', array('disable' => !empty($edit)));
        ?>
    });

$(document).ready(function (){


    //===== Codemirror =====//
    var cm1 = CodeMirror.fromTextArea(document.getElementById("video_embed"), {
        lineNumbers: true,
        lineWrapping: true,
        mode: "css",
        styleActiveLine: true,
        viewportMargin: Infinity
        <?= $disable ? ',readOnly: true' : ''?>
    });
    <?php if(!$disable): ?>
    function saveCode(cm, ev){
        var ta    =   cm.getTextArea();
        var srccode   =    cm.getValue();

        new AjaxSaveDataRequest(ta, srccode);
    }

    cm1.on('blur', saveCode);

    <?php endif;?>
});

<?php $this->end_script(); ?>
</script>