<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Contacts</h6>
			<div class="buttonS f_rt jopendialog jopen-createcontact"><span>Create a New Contact</span></div>
		</div>

<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Contacts'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jContacts',
	'data'				=> $Contacts,
	'rowkey'			=> 'Contact',
	'rowtemplate'		=> 'campaign-contact.php',
	'total'				=> $total_contacts_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxcontacts',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<!-- Create Form POPUP -->
<?php $this->getDialog('create-contact.php'); ?>


<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function (){
		
		//===== Create New Form + PopUp =====//
		$('#jdialog-createcontact').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function ()
			{
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Create',
				function (ajax, status)
				{
					
	      	if(!ajax.error)
	      	{
				var dt = $('#jContacts');
				var newRowsIds = dt.data('newRowsIds');
				newRowsIds.unshift(ajax.object.id);
				dt.data('newRowsIds', newRowsIds);
				dt.dataTable().fnDraw();
			  notie.alert({text:ajax.message});
	          this.close();
						this.form[0].reset();
					}
					else if(ajax.error == 'validation')
					{
	        	alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
	          alert(ajax.message);
					
				}, '#jdialog-createcontact')).buttons
		});
	
	});
    
<?php $this->end_script(); ?>
</script>