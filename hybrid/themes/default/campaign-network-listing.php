<?php
$state_list = Defaults::getStates();
$industries_list = Defaults::Industries();
?>


<?php


?>
<div class="fluid">
    <form action="<?= $this->action_url('Campaign_save') ?>" class="ajax-save">

        <!-- section -->
        <div class="widget grid6">
            <div class="whead">
                <h6>Listing Settings</h6>
            </div>

            <div class="formRow">
                <div class="grid3">
                    <label>Business Name</label>
                </div>
                <div class="grid9"><input type="text" name="network_listings[company_name]" value="<?=$networklisting->company_name ?>"/></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Company Logo:</label></div>
                <div class="grid9">
                    <?php
                    $this->getWidget('moxiemanager-filepicker.php', array(
                        'name' => 'network_listings[photo]',
                        'value' => $networklisting->photo,
                        'idWidget' => 'photo',
                        'noHost' => true,
                        'extensions' => 'jpg,jpeg,png,gif',
                        'path' => $storagePath
                    ));
                    ?>
                </div>
            </div>

            <div class="formRow">
                <div class="grid3">
                    <label>Phone</label>
                </div>
                <div class="grid9"><input type="text" name="network_listings[phone]"
                                          value="<?=$networklisting->phone ?>"/></div>
            </div>

            <div class="formRow">
                <div class="grid3">
                    <label>Address</label>
                </div>
                <div class="grid9"><input type="text" name="network_listings[street]"
                                          value="<?=$networklisting->street ?>"/></div>
            </div>

            <div class="formRow">
                <div class="grid3">
                    <label>City</label>
                </div>
                <div class="grid9"><input type="text" name="network_listings[city]" value="<?=$networklisting->city
                    ?>"/></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>State/Province:</label></div>
                <div class="grid9">
                <select name="network_listings[state]" class="select2">
                    <option value="">Choose State...</option>
                    <?php foreach($state_list as $code => $statename): ?>
                        <option value="<?php echo $code; ?>"<?php $this->selected($code, $networklisting->state); ?>><?php echo $statename; ?></option>
                    <?php endforeach; ?>
                </select>
                </div>
            </div>

            <div class="formRow">
                <div class="grid3">
                    <label>Zip/Postal Code</label>
                </div>
                <div class="grid9"><input type="text" name="network_listings[zip]"
                                          value="<?=$networklisting->zip ?>"/></div>
            </div>

            <div class="formRow">
                <div class="grid3"><label>Business Category:</label></div>
                <div class="grid9">

                    <select name="network_listings[category]" class="select2">
                        <option value="">Choose Category...</option>
                        <?php foreach($industries_list as $code => $category): ?>
                            <option value="<?php echo $code; ?>"<?php $this->selected($code,
                                $networklisting->category); ?>><?php echo $category; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>



            <div class="formRow">
                <div class="grid3"><label>Short Description:</label></div>
                <div class="grid9">
                    <div class="formRow">
                        <?php $this->getWidget('codemirror.php', array('id' => 'code1','name' =>
                            'network_listings[display_info]',
                            'value' => $networklisting->display_info))?>
                    </div>

                </div>
            </div>




            <div class="clear"></div>
        </div><!-- //section -->




        <div class="widget grid6">
            <div class="whead">
                <h6>Personalize Listing</h6>
            </div>
            <ul class="tabs">
                <li class="activeTab"><a href="#tabb3">About</a></li>
                <li class=""><a href="#tabb4">Photos</a></li>
            </ul>
            <div class="tab_container">

                <!-- meta config tab -->

                <div class="tab_content no_padding" id="tabb3" style="display: block;">



                    <div class="formRow">
                        <div class="grid12"><div class="j-dual-ide" style="position: relative;">
                                <ul class="tabs hand">
                                    <li class="j-codemirror"><a href="#tabA1"><em>{Source Code}</em></a></li>
                                    <li class="j-cleditor"><a href="#tabB1">Easy Editor</a></li>
                                </ul>
                                <div class="tab_container">
                                    <div class="tab_content no_padding" id="tabA1" style="display: block;">
                                        <textarea class="j-codemirror " name="network_listings[company_spot]"><?=$networklisting->company_spot ?></textarea>

                                    </div>
                                    <div class="tab_content" id="tabB1">
                                        <textarea class="j-cleditor"><?=$networklisting->company_spot ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                    <!-- //end meta config tab -->

                <!-- photos config tab -->
                <div class="tab_content" id="tabb4" style="display: none;">

                    <div class="formRow">
                        <div class="grid3"><label>Photo 1:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_one]',
                                'value' => $networklisting->pic_one,
                                'idWidget' => 'pic_one',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Photo 2:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_two]',
                                'value' => $networklisting->pic_two,
                                'idWidget' => 'pic_two',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Photo 3:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_three]',
                                'value' => $networklisting->pic_three,
                                'idWidget' => 'pic_three',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Photo 4:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_four]',
                                'value' => $networklisting->pic_four,
                                'idWidget' => 'pic_four',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Photo 5:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_five]',
                                'value' => $networklisting->pic_five,
                                'idWidget' => 'pic_five',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="formRow">
                        <div class="grid3"><label>Photo 6:</label></div>
                        <div class="grid9">
                            <?php
                            $this->getWidget('moxiemanager-filepicker.php', array(
                                'name' => 'network_listings[pic_six]',
                                'value' => $networklisting->pic_six,
                                'idWidget' => 'pic_six',
                                'noHost' => true,
                                'extensions' => 'jpg,jpeg,png,gif',
                                'path' => $storagePath
                            ));
                            ?>
                        </div>
                    </div>

                </div>


            </div>
            <div class="clear"></div>
        </div>

        <div class="widget grid6" >
            <div class="whead">
                <h6>Listing Promotion</h6>
            </div>
            <ul class="tabs">
                <li class=""><a href="#tabb6">Social Mashup Connections</a></li>
                <li class=""><a href="#tabb7">SEO Settings</a></li>
            </ul>
            <div class="tab_container">



                <!-- social config tab -->
                <div class="tab_content" id="tabb6" style="display: none;">

                    <div class="formRow">
                        <div class="grid3"><label>Blog RSS Feed URL:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[blog]" value="<?=$networklisting->blog ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Twitter Profile ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[twitter]" value="<?=$networklisting->twitter ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Facebook Page ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[facebook]" value="<?=$networklisting->facebook ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>LinkedIn Page ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[linkedin]" value="<?=$networklisting->linkedin ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Google+ Page ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[google_plus]" value="<?=$networklisting->google_plus ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Google Places ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[google_places]" value="<?=$networklisting->google_places ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>YouTube:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[youtube]" value="<?=$networklisting->youtube ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Instagram Username:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[instagram]" value="<?=$networklisting->instagram ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Yelp Page ID:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[yelp]" value="<?=$networklisting->yelp ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Pinterest Username:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[pinterest]" value="<?=$networklisting->pinterest ?>"/></div>
                    </div>

                </div>

                <!-- end social config tab -->


                <!-- seo config tab -->
                <div class="tab_content" id="tabb7" style="display: none;">

                    <div class="formRow">
                        <div class="grid3"><label>Meta Title:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[title]"
                                                  value="<?=$networklisting->title ?>"/></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Meta Keywords:</label></div>
                        <div class="grid9"><input id="keywords" type="text" class="tags" name="network_listings[keywords]"  value="<?=$networklisting->keywords ?>" placeholder=""/></div>

                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Meta Description:</label></div>
                        <div class="grid9"><input type="text" name="network_listings[description]"
                                                  value="<?=$networklisting->description ?>"/></div>
                    </div>

                </div>

                <!-- end seo config tab -->



            </div>
            <div class="clear"></div>
        </div>


        <input type="hidden" name="network_listings[id]" value="<?=$networklisting->getID()?>"/>
        <input type="hidden" name="network_listings[cid]" value="<?=$networklisting->cid?>"/>
    </form>
</div> <!-- //end fluid -->


<script type="text/javascript">
    <?php $this->start_script(); ?>
    $(document).ready(function (){

        <?php $this->getJ('wysiwyg-ide.php', array('disable' => !empty($disable))); ?>

        //===== Tabs =====//
        $.fn.contentTabs = function(){

            $(this).find(".tab_content").hide(); //Hide all content
            $(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
            $(this).find("#tabA1").addClass("activeTab").show(); //Activate first tab
            $(this).find(".tab_content:first").show(); //Show first tab content

            $("ul.tabs li").click(function() {
                $(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
                $(this).addClass("activeTab"); //Add "active" class to selected tab
                $(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
                var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
                $(activeTab).show(); //Fade in the active content
                return false;
            });

        };
        $("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"






        //===== Chosen plugin =====//
//	$(".select").chosen();


        //===== Style Form Elements =====//
//	$("select").uniform();
    });
    <?php $this->end_script();  ?>
</script>

