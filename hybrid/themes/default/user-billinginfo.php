<div class="fluid">
	<!-- left column -->
	<div class="widget grid6">
		<div class="whead">
			<h6>User _#34994_ Billing Info</h6>
		</div>
		<div class="formRow">                    
			<div class="grid3">First Name:</div>
			<div class="grid9"><input type="text" name="" value="" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Last Name:</div>
			<div class="grid9"><input type="text" name="" value="" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Company:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->company; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Address:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->address; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Address 2:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->address2; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">City:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->city; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">State:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->state; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Zip:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->zip; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Phone:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->phone; ?>" /></div>
		</div>
		<div class="formRow">                    
			<div class="grid3">Created Date:</div>
			<div class="grid9"><input type="text" name="" value="<?php //echo $userobj->created_date; ?>" /></div>
		</div>
		<div class="formRow">          
			<div class="grid3">Card Number: <span class="note">last 4</span></div>
			<div class="grid9"></div>
		</div>
	</div>
	<!-- right column -->
	<div class="widget grid6">
		<div class="whead">
			<h6>Basic User Info</h6>
		</div>
		<div class="formRow">
			<div class="grid3">Email:</div>
			<div class="grid6"><input type="text" name="" value="<?php //echo $userobj->email; ?>" /></div>
		</div>
		<div class="formRow">
			<div class="grid3">Password:</div>
			<div class="grid6"><input type="password" name="" value="test123" /></div>
		</div>
		<div class="formRow">
			<div class="grid3">Access:</div>
			<div class="grid6">
				<select>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Canceled:</div>
			<div class="grid6">
				<select>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
		</div>
		<div class="formRow">
			<div class="grid3">Product:</div>
			<div class="grid6"></div>
		</div>
		<div class="formRow">
			<div class="grid3">Select User Managers:</div>
			<div class="grid6"></div>
		</div>
	</div>
</div>