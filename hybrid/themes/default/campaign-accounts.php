<div class="fluid">
	<div class="widget check">
		<div class="whead">
			<h6>Accounts</h6>
			<div class="buttonS f_rt jopendialog jopen-addAccount"><span>Create a New Account</span></div>
		</div>
		
<?php
$this->getWidget('datatable.php',array(
	'columns'			=> DataTableResult::getTableColumns('Accounts'),
	'class'				=> 'col_1st,col_2nd,col_end',
	'id'				=> 'jAccounts',
	'data'				=> $Accounts,
	'rowkey'			=> 'Account',
	'rowtemplate'		=> 'accounts.php',
	'total'				=> $total_accounts_match,
	'sourceurl'			=> $this->action_url('campaign_ajaxaccounts',array('ID' => $_GET['ID'])),
	'DOMTable'			=> '<"H"lf>rt<"F"ip>'
));
?>

	</div>
</div><!-- /end fluid -->

<?php $this->getDialog('add-account.php'); ?>

<script type="text/javascript">
<?php $this->start_script(); ?>

	$(function() {

		//===== Create New Form + PopUp =====//
		$('#jdialog-addAccount').dialog(
		{
			autoOpen: false,
			height: 200,
			width: 400,
			modal: true,
			close: function()
			{
				$('#campaign_id', this).val('');
				$('form', this)[0].reset();
			},
			buttons: (new CreateDialog('Add',
				function(ajax, status)
				{
					if (!ajax.error)
					{
						$('.jnoforms').hide();
						$('.jforms').prepend(ajax.row);
						$('#jAccounts').dataTable().fnDraw();
						notie.alert({text:ajax.message});
						this.close();
						this.form[0].reset();
					}
					else if (ajax.error == 'validation')
					{
						alert(getValidationMessages(ajax.validation).join('\n'));
					}
					else
						alert(ajax.message);
	
				}, '#jdialog-addAccount')).buttons
		});

	});

<?php $this->end_script(); ?>
</script>

