<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

interface HubPageElement
{
    function PrintElement();
}

interface hasObjects
{
    function getElements();
    function hasElement($element_name);
    function getElement($element_name);
}

class HubPageObject implements HubPageElement
{
    protected $object_value;
    protected $object_name;
    protected $id;
    
    function getID(){ return $this->id; }
    
    function getObjectName() { return $this->object_name; }
    
    function PrintElement() {
        return $this->object_value;
    }
}

class HubPage implements hasObjects
{
    function getID(){
        return 1;
    }
    
    function __construct($objects   =   array()) {
        foreach($objects as $objid => $objrow)
        {
            $this->{$objrow->getObjectName()}   =   $objrow;
        }
    }
    
    function hasElement($element_name) {
        return isset($this->$element_name);
    }
    
    /**
     * 
     * @param type $element_name
     * @return HubPageObject
     */
    function getElement($element_name) {
        return $this->$element_name;
    }
    
    function getElements() {
        return (array)$this;
    }
    
    static function LoadPage($page_id)
    {
        $db =   Qube::GetPDO();
        $stmt   =   $db->query('SELECT id, object_value, object_name FROM page_objects WHERE page_id = ' . (int)$page_id);
        $page = new HubPage($stmt->fetchAll(PDO::FETCH_CLASS, 'HubPageObject'));
        return $page;
    }
}

function smarty_block_printObject($params, $content, Smarty_Internal_Template $template, &$repeat)
{
    if(!$repeat){
        $hub    =   $template->getTemplateVars('hub');
        $name   =   $params['name'];
        
        if($hub->hasElement($name)) // todo otehr stuff like disabled/enabled/ whatever
            return $hub->getElement($name)->PrintElement();
        
        return $content;
    }
#    return $hub->hasElement($params['']);
}


function smarty_function_objectID($params, Smarty_Internal_Template $template)
{    
    $hub    =   $template->getTemplateVars('hub');
    $hubid  =   $hub->getID();
    $name   =   $params['name'];
    $objid  =   ($hub->hasElement($name)) ? $hub->getElement($name)->getID() : '?';
    
    return "$hubid-$name-$objid";
}


