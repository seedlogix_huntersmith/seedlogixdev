<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Build the right security solution to protect your students and staff!</title>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template2-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
 <table id="block-template2-<?=$height?>-<?php echo $divorder;?>" cellpadding="0" cellspacing="0" border="0"
           class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
        <tr>
            <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                    <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/DETbanner.jpg" alt="9 Tips for Protecting Your Home While On Vacation" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:20px 19px 15px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 5px 0;color: #333333;padding:0;text-align:left;">The joy of summer vacations stems from experiencing new places or visiting an old favorite. However, being away from your home for any length of time leaves your house and property vulnerable to break-ins and theft. Protect your family and loved ones in <?=$info['city']?>, <?=$info['state']?> and surrounding area with <?=$info['company']?>.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip1.jpg" alt="1. Install a Video Security System" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Install a Video Security System</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Installing a high-quality security camera system in your home provides a first-line-of-defense against break-ins and theft. Choosing a video security system with the right amount of cameras needed to cover your property, inside and out is essential. Make sure key areas such as your front and back doors, yard, rooms with valuables, and entryways inside of your home have ample surveillance coverage.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Keep Your Home and <br />Property Well-Lit</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Thieves often target homes that are not well-lit. They use the lack of light to their advantage to break into your home, while going unnoticed. Therefore, it's critical to keep your home adequately lit. You can accomplish this by installing timers to turn on indoor lights at various intervals, and at various times while you are away. You can also install motion detection flood lights that turn on whenever motion is detected in your yard.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip2.jpg" alt="2. Keep Your Home and Property Well-Lit" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip3.jpg" alt="3. Hide Your Valuables" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Hide Your Valuables</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Before you embark on your vacation, you'll want to take a look around your home and make sure your valuables are hidden from view. Remove any expensive electronics such as laptops or tablets away from a clear view from a window. Place valuables such as jewelry, credit cards, rare coins, money, and other assets in a safe, or a safety deposit box while you are away from&nbsp;home.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Lock All Doors and Windows</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Locking all doors and windows before you leave is also very important. Many thieves break into your home by entering through an unlocked door or window. Double check all windows and side, back, and front doors are locked securely before you depart for your&nbsp;vacation.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip4.jpg" alt="4. Lock All Doors and Windows" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip5.jpg" alt="5. Hold Your Mail" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Hold Your Mail</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">If you are going to be on vacation for more than a couple of days, you'll want to let the post office know to hold your mail, or have a trusted neighbor or friend pick up your mail. That way, you won't have mail piling up in your mailbox, which can tip off thieves that you are not at home. Also, if you have a daily or weekly newspaper delivery, have a friend or neighbor pick up the newspapers.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Unplug Electronics</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">In order to protect your electronics such as computers, TV's, gaming systems, and other valuable items while you are away from home, simply unplug them. Unplugging these devices, will prevent any damage occurred while you are away by a potential power surge, power outage, brownout, or rolling blackout.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip6.jpg" alt="6. Unplug Electronics" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip7.jpg" alt="7. Check Heating and Cooling Systems" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Check Heating and <br />Cooling Systems</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Before you leave on vacation, it's a good idea to check your heating and cooling systems, and hot water heaters to make sure they are functioning optimally, and are not in need of repair. The last thing you need to do is to come home to a flooded home.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Don't Advertise Your Vacation on&nbsp;Social Media</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">f you are going on vacation, don't advertise to the world on social media. Posting that you are on vacation, can alert thieves that you are away, and provide an open invitation for breaking into your home. Also, don't post vacation photos on your social media page while you are away.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip8.jpg" alt="8. Don't Advertise Your Vacation on Social Media" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/tip9.jpg" alt="9. Maintain Landscaping" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 15px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:18px;margin:15px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#005d6a;font-weight:bold;">Maintain Landscaping</p>
                                        <p style="font-size:14px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Before you leave on vacation, trim bushes, trees, shrubs, and any other type of landscaping that is near doors and windows, or near any security cameras that you may have installed at your home. This will prevent thieves being able to hide behind overgrown landscaping and enter your home undetected.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 19px 20px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/homesecurity/customize.jpg" alt="Customize Your Solution" class="image_fix step" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" align="left" />
                            <p style="font-size: 28px; line-height:1.2em; font-family: sans-serif;margin:0 0 5px 0;color: #333333;padding:0;text-align:left;font-weight:bold;">Customize Your System!</p>
                            <p style="font-size:17px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#333333;">Don't see what you're looking for? Call us to customize a turn-key system, or we can help you build your system from scratch. There's no extra charge! <span style="font-weight:bold;">Call&nbsp;us&nbsp;at <span class="mobile_link" style="font-size:17px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;color:#333333;font-weight:bold;"><?=$info['phone']?></span> to&nbsp;get&nbsp;started!</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#015a66;padding:10px 20px 10px;width:618px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                            <p style="font-size:36px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#fffefd;">Why Choose Us?</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#015a66;padding:0px 10px 30px;width:638px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Advanced <br/>Technology</strong> <br />We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Dedicated <br/>Support</strong> <br />Our trained security technicians are experts at servicing virtually any size security application.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Comprehensive <br/>Security Service</strong> <br />We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td style="padding:10px 0px 10px 20px;background-color:#ffffff;text-align:left;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><span style="font-weight:normal">Call</span> <?=$info['phone']?> <br /><span style="font-weight:normal;font-size:15px;">to discuss your security needs today!</span></p></td>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:right;" valign="top" align="right"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" align="right" /></a></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- end wrapper table -->
<? } ?>
</body>
</html>