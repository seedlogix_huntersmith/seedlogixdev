<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper">

  <?php if($divparse = $html->find('table[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>


 <!--DO NOT REMOVE THIS DIV-->
    <table id="block-<?=$divid?>-<?=$height?>-<?php echo $divorder;?>" cellpadding="0" cellspacing="0" border="0"
           class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
   <!--REPLACEABLE CONTENT BELOW-->

        <?
        //TEMPLATE INCLUDE
        include(QUBEROOT . 'emails/themes/sc/det/'.$divid.'.php');
        ?>

        <!--REPLACEABLE CONTENT ABOVE-->
        </table>
      <!--DO NOT REMOVE THIS DIV-->
<? } ?>

</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
