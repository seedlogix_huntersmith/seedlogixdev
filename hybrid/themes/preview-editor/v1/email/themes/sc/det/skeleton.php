<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Build the right security solution to protect your students and staff!</title>
  <style type="text/css">
    .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }
  </style>
</head>
<body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<table id="wrapper" cellpadding="0" cellspacing="0" border="0"
       class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
</table>
</body>
</html>

