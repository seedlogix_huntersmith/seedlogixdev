<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Build the right security solution to protect your students and staff!</title>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template1-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
<table id="block-template1-<?=$height?>-<?php echo $divorder;?>" cellpadding="0" cellspacing="0" border="0"
           class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
        <tr>
            <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                    <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/DETbanner.jpg" alt="7 Steps to Maintaining a Video Security System!" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb1.jpg" alt="15 Seconds" /></td>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 20px 10px 0px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">A burglary occurs somewhere in the United States every 15&nbsp;seconds. </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 0px 10px 20px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">Most residential burglaries occur from 9am to 3pm when people are at work or&nbsp;school.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:right;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb2.jpg" alt="9AM to 3PM" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb3.jpg" alt="$2,230 Loss" /></td>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 20px 10px 0px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">The average dollar loss per burglary offense is&nbsp;$2,230.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 0px 10px 20px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">Police clear about 13% of all reported burglaries and rarely catch the thief in&nbsp;the&nbsp;act.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:right;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb4.jpg" alt="13 Percent" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb5.jpg" alt="74.5 Percent" /></td>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 20px 10px 0px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">Burglaries of residential properties accounted for 74.5 percent of all burglary&nbsp;offenses.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 0px 10px 20px;margin:0;text-align:left;">

                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">From time of entry, burglars spend an average of less than 12 minutes in a&nbsp;home.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:right;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/thumb6.jpg" alt="12 Minutes" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:20px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:5px 0px 20px 0px;margin:0;text-align:left;border-right:15px solid #ffffff;width:50%;background-color:#E9E9E9;border-left:10px solid #ffffff;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/tools.jpg" alt="What They Use to Break In" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:17px;margin:10px 20px 0px;padding:0;text-align:left;line-height:1.2em;color:#333333;">70% of burglars use some amount of force to enter a dwelling, but they prefer to gain easy access through an open door or&nbsp;window.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:5px 0px 20px 0px;margin:0;text-align:left;width:50%;background-color:#E9E9E9;border-right:10px solid #ffffff;border-left:15px solid #ffffff;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/target.jpg" alt="What They Steal" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:17px;margin:10px 20px 0px;padding:0;text-align:left;line-height:1.2em;color:#333333;">Favorite items to steal are cash, jewelry, guns, watches, laptop computers, tablets, DVRs, DVDs and other small electronic&nbsp;devices.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="middle" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:left;background-color:#E9E9E9;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 0px 10px 20px;margin:0;text-align:left;">

                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/who.jpg" alt="Who They Are" class="image_fix step" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:17px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.2em;color:#333333;">Burglaries are committed most often by young males under 25 years of age looking for items that are small, expensive, and can easily be converted to cash for living expenses and&nbsp;drugs.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="bottom" style="font-size: 1px; line-height: 1px; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px 0px;margin:0;text-align:right;background-color:#E9E9E9;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/burglar.png" alt="Burglaries" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/burglary/bythetime.jpg" alt="By the time you finish reading this infographic, 13 burglaries will have occurred in the US." style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 19px 10px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 22px; line-height:1.4em; font-family: sans-serif;margin:0 0 5px 0;color: #333333;padding:0;text-align:left;font-weight:bold;">Protect your home with a security&nbsp;camera&nbsp;system:</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#333333;">A video surveillance system is your first-line-of defense against break-ins, theft, vandalism, and property damage. Installing a high-quality, multi-camera security camera systems at your home enables you to keep watch 24/7.</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#333333;"><?=$info['company']?> can design surveillance camera system that's right for your location, determine the best locations for security cameras, and install and configure everything for you—so your security solution is optimized and ready-to-go.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#015a66;padding:10px 20px 10px;width:618px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                            <p style="font-size:36px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#fffefd;">Why Choose Us?</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#015a66;padding:0px 10px 30px;width:638px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Advanced <br/>Technology</strong> <br />We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Dedicated <br/>Support</strong> <br />Our trained security technicians are experts at servicing virtually any size security application.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Comprehensive <br/>Security Service</strong> <br />We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td style="padding:10px 0px 10px 20px;background-color:#ffffff;text-align:left;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><span style="font-weight:normal">Call</span> <?=$info['phone']?> <br /><span style="font-weight:normal;font-size:15px;">to discuss your security needs today!</span></p></td>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:right;" valign="top" align="right"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" align="right" /></a></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <!-- end wrapper table -->
<? } ?>
</body>
</html>