<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template5-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
    <table cellpadding="0" cellspacing="0" border="0" id="block-template5-<?=$height?>-<?php echo $divorder;?>" class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
        <tr>
            <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="Company Name" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                    <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/DETbanner.jpg" alt="Top 5 Locations for Home Hidden Cameras" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:20px 19px 15px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 15px 0;color: #333333;padding:0;text-align:left;">Along with a video security system, hidden cameras provide an additional line of defense for your home. Homeowners rely on hidden cameras to complement an existing video security system and deter vandalism and property damage, theft and break-ins, monitor domestic help and service providers, and keep their families safe.</p>
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 15px 0;color: #333333;padding:0;text-align:left;">There are a number of camera locations that are advantageous for providing optimal hidden surveillance coverage for your home.</p>
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 5px 0;color: #333333;padding:0;text-align:left;">Here are the best locations for hidden security cameras for your home:</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#eeeeee;padding:15px 10px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:33%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/thumb1.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:66%;">
                                        <p style="font-size:17px;margin:0px 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><strong>The Front Door</strong></p>
                                        <p style="font-size:15px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;">The front door is the first point of entry into your home. Placing a camera in this high-traffic location will help you monitor who enters and when. A well-designed covert camera blends in well in the home environment and the camera is virtually undetectable.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:15px 10px 15px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:66%;">
                                        <p style="font-size:17px;margin:0px 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><strong>Living Rooms &amp; Family Rooms</strong></p>
                                        <p style="font-size:15px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;">The living room or family room is one of the main areas in a home where family activities happen. Placing a camera in this high-traffic location will help you keep your family safe. </p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:33%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/thumb2.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#eeeeee;padding:15px 10px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:33%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/thumb3.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:66%;">
                                        <p style="font-size:17px;margin:0px 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><strong>Home Offices</strong></p>
                                        <p style="font-size:15px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;">Keep sensitive documents and safes hiding valuable secure by installing a video camera in your home office. A hidden camera can help you keep an eye on domestic help, service providers or thieves who may access files, personal information and records that are confidential.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:15px 10px 15px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:66%;">
                                        <p style="font-size:17px;margin:0px 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><strong>Patio Doors</strong></p>
                                        <p style="font-size:15px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;">Thieves love entering through the backyard and sliding glass patio doors and French doors. Placing a hidden camera above the entry point of your patio doors outside will provide a view of your backyard and anyone who may be entering through your patio&nbsp;door.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:33%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/thumb4.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#eeeeee;padding:15px 10px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:33%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/thumb5.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 11px 0px 10px;margin:0;text-align:left;width:66%;">
                                        <p style="font-size:17px;margin:0px 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><strong>Safe Rooms</strong></p>
                                        <p style="font-size:15px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;">Keep your valuables and assets safe. Placing a hidden camera inside any room where a safe that hides your important information and valuables such as jewelry, coins, cash and other items provides another layer of security.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#343434;padding:20px 19px 10px 20px;width:620px;">
                            <p style="font-size: 28px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #ffffff;padding:0;text-align:left;font-weight:bold;">Protect Your Home with Hidden Cameras</p>
                            <p style="font-size: 22px; line-height:1.4em; font-family: sans-serif;margin:0 0 15px 0;color: #ffffff;padding:0;text-align:left;font-weight:bold;">We're Here to help!</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#ffffff;"><?=$info['company']?> will customize a surveillance camera and hidden camera system to meet your specific needs. Our FREE security needs analysis will identify the proper number and recommended location of hidden cameras - and provide you with camera options that will deliver the best results, for the best price.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#000000;padding:20px 10px;width:638px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/technology.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Advanced <br />Surveillance <br />Technology</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/service.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Outstanding <br />Customer Service </p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">Our trained security technicians are experts are servicing virtually any size security application.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/hiddencameras/support.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Comprehensive <br />Installation and <br />Support</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#252525;padding:15px 19px 15px 20px;width:620px;">
                            <p style="font-size: 18px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #898989;padding:0;text-align:center;font-weight:bold;">Call <?=$info['phone']?> for a FREE security needs analysis today!</p>
                        </td>
                    </tr>
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:center;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="text-align:center;border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- end wrapper table -->
<? } ?>
</body>
</html>