<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template4-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
    <table cellpadding="0" cellspacing="0" border="0" id="block-template4-<?=$height?>-<?php echo $divorder;?>" class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
        <tr>
            <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                    <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/DETbanner_01.jpg" alt="Retail Security Solutions" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#015a66;padding:0px 19px 15px 20px;width:620px;background-image:url(http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/DETbanner_02.jpg);background-repeat:no-repeat;color:#ffffff;">
                            <p style="font-size:17px;margin:0 0 0px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.4em;color:#ffffff;">With high-performance video surveillance technology and dedicated support team, <?=$info['company']?> has provided retail security solutions and security system installation services for hundreds of satisfied business owners n <?=$info['city']?>, <?=$info['state']?>  and surrounding&nbsp;area.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#fdf8e1;padding:15px 19px 10px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 36px; line-height:1.2em; font-family: sans-serif;margin:0 0 5px 0;color: #111111;padding:0;text-align:left;font-weight:bold;">Reduce Shrink by Monitoring 6&nbsp;Key&nbsp;Areas&nbsp;in&nbsp;Your&nbsp;Store!</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.4em;color:#111111;">Whether you are a small, local shop, national convenience store chain, or a large box retailer – your store is vulnerable to theft! Monitor these key areas where your store is most vulnerable in order to protect your profits.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#fdf8e1;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb1.jpg" alt="Store Entrances" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Store Entrances</span><br />
                                            While it may seem obvious, shoplifting and theft happens at store entrances. Thieves look for unsecured merchandise near the front of the store that they can quickly grab and walk out with. Some thieves work with a partner who will create a distraction in the store while they walk out with merchandise, or have someone waiting in a car out front to help them make a quick getaway.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb2.jpg" alt="Store Perimeters" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Store Perimeters</span><br />
                                            Monitoring and securing store entrances is absolutely essential for store owners, and having a visible video security system and signage in place near store entrances is your first line of defense to deter shoplifting and theft. Unlit areas and locations obscured by overgrown landscaping are also targeted areas for burglars to gain entry to your store and avoid being seen.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="main" style="background-color:#fdf8e1;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb3.jpg" alt="Cash Registers" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Cash Registers</span><br />
                                            The cash register and cash wrap area is a vulnerable location for both employee theft and shoplifting. Dishonest employees can steal from the register by over-ringing sales and pocketing the difference, or avoiding ringing up cash transactions, voiding transactions and gifting merchandise to friends and family. Cash wrap areas are also prime targets for thieves who grab easy-to-steal and conceal items such as gift cards, convenience items and accessories.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb4.jpg" alt="Loading Docks" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Loading Docks</span><br />
                                            Loading docks are prime targets for theft, because of the continuous flow of merchandise being delivered. Without proper surveillance and security, it’s very easy for an employee, delivery driver, or thief to steal merchandise from the loading dock during a scheduled or unscheduled delivery. Sometimes employees leave loading dock doors open to provide airflow, or simply forget to close them after a delivery, which provides thieves with easy access to enter the facility and steal goods.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="main" style="background-color:#fdf8e1;padding:0px 10px 0px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb5.jpg" alt="Warehouse and Stockrooms" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Warehouse and Stockrooms</span><br />
                                            Warehouses and stock rooms are prime targets for theft, especially employee theft. In fact, employee theft is estimated to cost employers between $20 billion and $40 billion annually. Warehouses and stockrooms are full of merchandise and are sometimes poorly lit, not heavily-monitored, or do not have a surveillance solution in place. The first line of defense for reducing employee theft in the warehouse is to install both highly-visible security cameras that offer low light surveillance capabilities along with high-resolutions, and covert cameras that are hidden inside of an enclosure that looks like a common object you would see in a warehouse.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 10px 30px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="text-align:center;padding:0;margin:0 0 0px 0;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb6.jpg" alt="Dumpsters" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></p>
                                        <p style="font-size:15px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;"><span style="font-weight:bold;color:#005d6a;font-size:17px;">Dumpsters</span><br />
                                            Why is it important to monitor dumpsters and trash receptacle areas? Dumpsters provide employees with an easy way to steal merchandise by placing items inside of a trash bag throwing the bag into the dumpster, with the intention of returning later to retrieve the stashed goods. Installing video surveillance solution, including a high-resolution IR bullet or dome camera above a door or high up on the building with a clear view of the dumpster or receptacle, will enable you to keep an eye on activity and provide an overview shot of the location.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#343434;padding:20px 19px 10px 20px;width:620px;">
                            <p style="font-size: 28px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #ffffff;padding:0;text-align:left;font-weight:bold;">Protect Your Store!</p>
                            <p style="font-size: 22px; line-height:1.4em; font-family: sans-serif;margin:0 0 15px 0;color: #ffffff;padding:0;text-align:left;font-weight:bold;">Call <span style="color:#f4b41e"><?=$info['phone']?></span> for your FREE Needs Analysis</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#ffffff;">Having the right security plan and video surveillance solutions in place at your most vulnerable store locations will enable you to reduce theft and protect your profits. Let our security experts help you! We will provide a FREE security needs analysis – and will work with you to meet your security needs, and budget.</p>
                        </td>
                    </tr>

                    <tr>
                        <td class="main" style="background-color:#343434;padding:0px 0px 30px 10px;width:650px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:33%;background-color:#ffffff;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:7px 12px 7px;margin:0;text-align:left;width:100%;background-color:#919191;border-right:10px solid #343434;">
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Prevent Shoplifting</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#ffffff;border-right:10px solid #343434;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb7.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:14px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">It's not a matter of whether you’ll be a victim of shoplifting, it's when. It is a major drain on retailer's profits.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:33%;background-color:#ffffff;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:7px 12px 7px;margin:0;text-align:left;width:100%;background-color:#919191;border-right:10px solid #343434;">
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Prevent Employee Theft</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#ffffff;border-right:10px solid #343434;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb8.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:14px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Estimates show that 75% of employees steal from their employers at least once.<br />&nbsp;<br />&nbsp;</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:33%;background-color:#ffffff;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:7px 12px 7px;margin:0;text-align:left;width:100%;background-color:#919191;border-right:10px solid #343434;">
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Stop Transaction Fraud</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#ffffff;border-right:10px solid #343434;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/thumb9.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:14px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">This involves an employee and another person working together at the cash register.<br />&nbsp;<br />&nbsp;</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="main" style="background-color:#000000;padding:20px 10px;width:638px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/technology.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Advanced <br />Surveillance <br />Technology</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/service.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Outstanding <br />Customer Service </p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">Our trained security technicians are experts are servicing virtually any size security application.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/retail/support.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Comprehensive <br />Installation and <br />Support</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#252525;padding:15px 19px 15px 20px;width:620px;">
                            <p style="font-size: 18px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #898989;padding:0;text-align:center;font-weight:bold;">Call <?=$info['phone']?> for a FREE security needs analysis today!</p>
                        </td>
                    </tr>
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:center;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="text-align:center;border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- end wrapper table -->
<? } ?>
</body>
</html>