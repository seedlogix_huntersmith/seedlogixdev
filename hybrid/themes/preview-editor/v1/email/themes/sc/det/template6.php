<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template6-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
    <table cellpadding="0" cellspacing="0" border="0" id="block-template6-<?=$height?>-<?php echo $divorder;?>" class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
        <tr>
            <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                    <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                        <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                    <tr>
                        <!-- main banner -->
                        <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/DETbanner.jpg" alt="Surveillance Solutions for Day Care" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                        <!-- end main banner -->
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#293d58;padding:10px 19px 10px 25px;width:615px;">
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#ffffff;">Safe and nurturing environments are the necessary foundation for education. <?=$info['company']?> understands that ensuring the safety and security of all the children is parent's top priority, and our security solutions to ensure that daycare facilities reflect this philosophy.</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#ffffff;">While it is important to take security measures, it is also imperative that your center conveys a nurturing child care atmosphere. Our security solutions are made in a way that reassures children and their parents and makes them feel comfortable.</p>
                            <p style="font-size:17px;margin:0 0 15px 0;padding:0;font-family: sans-serif;text-align:left;line-height:1.2em;color:#ffffff;font-weight:bold;">We'll help create the school security solution that meets the unique needs and specifications of your facility.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 19px 10px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 30px; line-height:1.2em; font-family: sans-serif;margin:0 0 10px 0;color: #333333;padding:0;text-align:left;font-weight:bold;">The Solution: Layered Security</p>
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #333333;padding:0;text-align:left;">For most daycare facilities, the most effective plan will combine multiple layers&nbsp;of&nbsp;security.</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb1.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 0px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:20px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Perimeter Security</p>
                                        <p style="font-size:15px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">One of the most effective ways to prevent an intruder is to install a security camera outside the building that alerts staff when anyone is about to enter. A safe, secure perimeter ensures that children can enjoy outdoor play&nbsp;safely.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:20px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Access Control</p>
                                        <p style="font-size:15px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Access Control ensures that only authorized staff and families may enter and pick up children. Restricted access to designated areas can ensure that intruders can never reach critical areas of the&nbsp;school.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb2.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb3.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 0px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:20px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Mobile Surveillance</p>
                                        <p style="font-size:15px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Install an extra set of eyes to monitor buses, drivers, and students. A video record or student and employee behavior can help you identify problems, establish patterns, and ensure&nbsp;safety.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 30px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <p style="font-size:20px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Training and Drills</p>
                                        <p style="font-size:15px;margin:0px 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family:sans-serif;">Students, faculty and staff are the last line of defense. Training and unannounced drills practiced at least annually build confidence and reassure students that safety is being seriously by the school.</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb4.png" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:10px;line-height:10px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:5px 19px 10px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 30px; line-height:1.2em; font-family: sans-serif;margin:0 0 10px 0;color: #333333;padding:0;text-align:left;font-weight:bold;">Keep Your Children and Facility Safer... at&nbsp;an Affordable Price</p>
                            <p style="font-size: 17px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #333333;padding:0;text-align:left;">Our security system experts will oversee your security installation services from start&nbsp;to&nbsp;finish. We offer professional and comprehensive daycare facility security&nbsp;services: </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb5.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:20px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Site Assessment</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb6.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:20px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Installation and Fine-tuning Security Equipment</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:10px 10px 30px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;color: #333333;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb7.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:20px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Documentation of Your Complete Security Systems</p>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:50%;">
                                        <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb8.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                        <p style="font-size:20px;margin:10px 0 0px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;color:#293d58;">Security System Maintenance and Support</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr><td style="border-top:1px solid #cccccc;font-size:10px;line-height:10px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:5px 19px 10px 20px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <p style="font-size: 30px; line-height:1.2em; font-family: sans-serif;margin:0 0 5px 0;color: #333333;padding:0;text-align:left;font-weight:bold;">Our Products</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:0px 0px 0px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:50%;background-color:#e3e3e3;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#e3e3e3;border-right:20px solid #ffffff;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb9.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:20px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.2em;color:#293d58;font-weight:bold;">Security Cameras</p>
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Whether you are looking for HD analog, IP, or traditional analog cameras, we have a camera with the capabilities to meet your surveillance needs.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:50%;background-color:#e3e3e3;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#e3e3e3;border-right:20px solid #ffffff;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb10.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:20px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.2em;color:#293d58;font-weight:bold;">Security DVRs</p>
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Security video recorders come with powerful features such as event notification, apps or remote monitoring, and multiple recording modes.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#ffffff;padding:20px 0px 30px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:50%;background-color:#e3e3e3;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#e3e3e3;border-right:20px solid #ffffff;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb11.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:20px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.2em;color:#293d58;font-weight:bold;">Access Control</p>
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Our IP door access control solutions scale from one door to thousands, and work with powerful, user-friendly access control management software.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:0px 0px 0px;margin:0;text-align:left;width:50%;background-color:#e3e3e3;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #000000;padding:10px 12px 10px;margin:0;text-align:left;width:100%;background-color:#e3e3e3;border-right:20px solid #ffffff;">
                                                    <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/thumb12.jpg" alt="" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                                                    <p style="font-size:20px;margin:10px 0 5px 0;padding:0;text-align:left;line-height:1.2em;color:#293d58;font-weight:bold;">Mobile Surveillance</p>
                                                    <p style="font-size:14px;margin:0px 0 0px 0;padding:0;text-align:left;line-height:1.2em;">Our vehicle cameras, shock-proof DVRs with GPS, and complete mobile surveillance systems are ideal for use on buses and other school vehicles.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#000000;padding:20px 10px;width:638px;">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/technology.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Advanced <br />Surveillance <br />Technology</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;border-right:1px solid #ffffff;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/service.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Outstanding <br />Customer Service </p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">Our trained security technicians are experts are servicing virtually any size security application.</p>

                                    </td>
                                    <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 15px 10px;margin:0;text-align:left;width:33%;">
                                        <center><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/daycare/support.png" alt="" class="image_fix step" style="margin:0 auto 10px;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></center>
                                        <p style="font-size:18px;margin:0 0 5px 0;padding:0;text-align:center;line-height:1.2em;font-weight:bold;color:#898989;">Comprehensive <br />Installation and <br />Support</p>
                                        <p style="font-size:14px;margin:0;padding:0;text-align:center;line-height:1.4em;color:#fefefe;">We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="main" style="background-color:#252525;padding:15px 19px 15px 20px;width:620px;">
                            <p style="font-size: 18px; line-height:1.4em; font-family: sans-serif;margin:0 0 0px 0;color: #898989;padding:0;text-align:center;font-weight:bold;">Call <?=$info['phone']?> for a FREE security needs analysis today!</p>
                        </td>
                    </tr>
                    <tr>
                        <td id="header" width="660" valign="top">
                            <!-- logo, links and number -->
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                <tr>
                                    <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:center;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="text-align:center;border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                </tr>
                            </table>
                            <!-- end logo, links and number -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- end wrapper table -->
<? } ?>
</body>
</html>