<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Build the right security solution to protect your students and staff!</title>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template3-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
<table id="block-template3-<?=$height?>-<?php echo $divorder;?>" cellpadding="0" cellspacing="0" border="0"
            class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
         <tr>
             <td valign="top" align="center" width="100%" style="border-collapse: collapse;">
                 <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
                     <tr>
                         <td id="header" width="660" valign="top">
                             <!-- logo, links and number -->
                             <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
                                     <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
                                         <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
                                 </tr>
                             </table>
                             <!-- end logo, links and number -->
                         </td>
                     </tr>
                     <tr>
                         <!-- main banner -->
                         <td valign="top" style="margin:0;padding:0;width:660px;"><img class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/DETbanner.jpg" alt="7 Steps to Maintaining a Video Security System!" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></td>
                         <!-- end main banner -->
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:0px 20px 10px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <p style="font-size:18px;margin:0 0 0px 0;padding:0;text-align:center;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#333333;">In order to determine that a video security system is performing optimally, it’s a good idea to schedule regular system&nbsp;maintenance.</p>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:20px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">1</span> Trim Landscaping</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Take a look at outdoor camera locations and make sure that each camera has a clear view. Keep vines, bushes, trees and other landscaping near cameras neat and trimmed, to prevent camera&nbsp;obstruction.</p>
                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 11px 0px 0px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb1.jpg" alt="3" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 11px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb2.jpg" alt="2" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">2</span> Clean Camera Lenses</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Check indoor and outdoor camera lenses to make sure that the lenses are clean. Look for dust, smudges, water spots and other undesirable elements on lenses. Clean camera lenses regularly to ensure optimal image&nbsp;quality.</p>
                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">3</span> Check Camera Housing</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Open the housing for each security camera to check for water, condensation and dirt, in order to confirm that the camera enclosures are still protecting your security cameras from the&nbsp;elements.</p>
                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 11px 0px 0px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb3.jpg" alt="3" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 11px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb4.jpg" alt="2" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">4</span> Make Sure Cabling is Securely Connected</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Check all cable connections, including those connected to the security cameras, DVR and monitor to ensure that all cables are securely connected and receiving power. Also look for cables to see if they are showing signs of wear. If there is any exposed wire, it is best to replace the&nbsp;cable.</p>
                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">5</span> Look for Corrosion</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Take a look at the cable connectors for corrosion. Be sure to replace any connectors that show signs of corrosion, because it can cause equipment to short&nbsp;out.</p>
                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 11px 0px 0px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb5.jpg" alt="3" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 11px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb6.jpg" alt="2" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">6</span> Remove Dust from Your DVR</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Over time, a DVR can develop dust if not dusted regularly – this may affect the DVR’s performance. Frequently wipe security DVRs with a microfiber cloth, and use a can of compress air to remove dust from crevices. It’s not recommended to remove DVRs from their&nbsp;enclosure.</p>
                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#FFFFFF;padding:10px 10px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                                         <p style="font-size:26px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.2em;font-weight:bold;"><span style="font-size:36px;color:#FF0000;">7</span> Check Your Power Supplies</p>
                                         <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;">Frequently check security system power supplies to ensure that the system has not lost power due to tampering, a storm, a brown out or some other unwanted event. If there is an Uninterruptible Power Supply (UPS), take a look at the unit to make sure that the battery is fully charged and does not show any warning&nbsp;lights.</p>
                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 11px 0px 0px;margin:0;text-align:left;width:33%;">
                                         <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/systemmaintenance/thumb7.jpg" alt="3" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />

                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#015a66;padding:10px 20px 10px;width:618px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                             <p style="font-size:36px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#fffefd;">Why Choose Us?</p>
                         </td>
                     </tr>
                     <tr>
                         <td class="main" style="background-color:#015a66;padding:0px 10px 30px;width:638px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
                             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Advanced <br/>Technology</strong> <br />We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>

                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Dedicated <br/>Support</strong> <br />Our trained security technicians are experts at servicing virtually any size security application.</p>

                                     </td>
                                     <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Comprehensive <br/>Security Service</strong> <br />We offer a comprehensive set of security services ranging from site survey to technical support.</p>

                                     </td>
                                 </tr>
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td id="header" width="660" valign="top">
                             <!-- logo, links and number -->
                             <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                     <td style="padding:10px 0px 10px 20px;background-color:#ffffff;text-align:left;" align="right" valign="bottom" class="rightbox">
                                         <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><span style="font-weight:normal">Call</span><?=$info['phone']?> <br /><span style="font-weight:normal;font-size:15px;">to discuss your security needs today!</span></p></td>
                                     <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:right;" valign="top" align="right"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" align="right" /></a></td>
                                 </tr>
                             </table>
                             <!-- end logo, links and number -->
                         </td>
                     </tr>
                 </table>
             </td>
         </tr>
     </table>

     <!-- end wrapper table -->
<? } ?>
</body>
</html>