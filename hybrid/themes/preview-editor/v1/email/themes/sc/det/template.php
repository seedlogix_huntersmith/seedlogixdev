<?
	//COMMON HEADER
	include('inc/header.php');
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Build the right security solution to protect your students and staff!</title>
  <style type="text/css">
      .applelinks a {color:#CC3300; text-decoration: none;}
    #outlook a {padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    table td {border-collapse: collapse;}
    @media only screen and (max-device-width: 480px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #CCCCCC;pointer-events: none;cursor: default;}
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #FFFFFF !important;pointer-events: auto;cursor: default;}
    }
    @media (max-width: 640px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:10px !important}
      table[class=section] .main {padding:15px 10px 15px !important; }
      table[class=section] .social, table[class=section] #nav, table[class=section] #fb {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:5px !important}
      table[class=section] .greenbutton {padding:11px 25px 11px 25px !important}
      table[class=section] .reshuffle {padding-left:50px !important;}
    }
    @media (max-width: 480px) {
      table[class=section] { width:100%!important;background-color:#ffffff !important}
      table[class=section] .product { width:100% !important; margin-bottom:5px !important }
      table[class=section] .main {padding:15px 5px 15px !important; }      
      table[class=section] .social, table[class=section] #nav {display:none !important; }
      table[class=section] .number {vertical-align:middle !important; font-size:16px !important; line-height:18px !important}
      table[class=section] .divider {margin:2px !important}
      table[class=section] .greenbutton {padding:11px 11px 11px 11px !important}
      table[class=section] .reshuffle {padding-left:20px !important;}
      table[class=section] .adjustinglogo {width:165px !important;height:35px !important;}
      table[class=section] #logo {padding-left:5px !important;}
      table[class=section] .rightbox {padding-top:0px !important;}
    }    
  </style>
</head>
<body id="wrapper" style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
<?php if($divparse = $html->find('table[id="block-template-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<!-- wrapper table -->
<table id="block-template-<?=$height?>-<?php echo $divorder;?>" cellpadding="0" cellspacing="0" border="0" class="section" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;margin:0; padding:0; width:100% !important; line-height: 100% !important;table-layout: fixed;" align="center">
<tr>
<td valign="top" align="center" width="100%" style="border-collapse: collapse;">
  <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;" align="center">
    <tr>
    <td id="header" width="660" valign="top">
      <!-- logo, links and number -->
      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
        <tr>
        <td id="logo" style="padding:10px 20px;background-color:#ffffff;" valign="top"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
        <td style="padding:10px 20px 10px 0;background-color:#ffffff;text-align:right;" align="right" valign="bottom" class="rightbox">
            <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><?=$info['phone']?></p></td>
        </tr>
        </table>
        <!-- end logo, links and number -->
    </td>
    </tr>
    <tr>
    <!-- main banner -->
    <td valign="top" style="margin:0;padding:0;width:660px;"><a href="<?=$info['website_url']?>" target="_blank" style="border:none;"><img
                class="image_fix" src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/detbanner.jpg" alt="Proven Security Solutions for
                Schools and Campuses" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" /></a></td>
    <!-- end main banner -->
    </tr>
    <tr>
    <td class="main reshuffle" style="padding:0px 24px 10px 25px;width:608px;background-color:#ce9c9d;background-image:url(http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/bgfade.jpg);background-repeat:repeat-x;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
        <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:normal;color:#333333;"><strong>Safe and nurturing environments are the necessary foundation for education.</strong> Unfortunately, security breaches in a small number of schools in the US have educators concentrating not on teaching, but on how to make their environment safer. We understand these unique challenges faced by schools, universities, and educational institutions.</p>
        <p style="font-size:17px;margin:0 0 5px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:normal;color:#333333;">Whether you are single-room nursery school or a multi-campus college or university in <?=$info['city']?>, <?=$info['state']?> and the surrounding area, <?=$info['company']?> offers effective, proven campus surveillance solutions that ensure the safety and security of people and property, while staying within the budgets of education&nbsp;providers.</p>
	</td>
    </tr>
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 20px 0px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> 
            		<tr><td style="border-top:1px solid #cccccc;font-size:20px;line-height:20px;mso-line-height-rule:exactly;">&nbsp;</td></tr> 
            </table>
    </td>
    </tr>
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 20px 10px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
        <p style="font-size:24px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#333333;">Nursery &amp; Daycare</p>
	</td>
    </tr>
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">             
                    <tr>
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 11px;margin:0;text-align:left;width:33%;">
                      <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/nurserydaycare.jpg" alt="Nursery &amp; Daycare" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                      
                      </td>  
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/concern.png" />&nbsp;<strong>Safety Concern:</strong> Child safety is top-priority. Childcare centers need security solutions that solve real-life challenges, including preventing unauthorized access, improving facility safety, and providing liability protection.</p>
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/solution.png" />&nbsp;<strong>Solution:</strong> Our video surveillance and access control solutions help solve these challenges by securing entry and exit points, and by providing continuous monitoring around-the-clock, along with video-based evidence should an incident occur.</p>
                      </td>                    
                    </tr> 
            </table>
	</td>
    </tr> 
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 20px 10px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
        <p style="font-size:24px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#333333;">K-12</p>
	</td>
    </tr>
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 10px 10px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">             
                    <tr>
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/concern.png" />&nbsp;<strong>Safety Concern:</strong> K-12 schools need to be able to monitor hallways, classrooms and entryways, restrict access to secure areas, prevent vandalism, and keep students and staff safe.</p>
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/solution.png" />&nbsp;<strong>Solution:</strong> Our video security solutions provide around-the-clock surveillance for school interiors and perimeters, and offer real-time video and notifications. IP-based access control solutions let schools restrict access during after hours, and our mobile surveillance solutions enable school administrators to monitor schools buses to ensure driver safety and protect&nbsp;students.</p>
                      </td> 
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 11px 0px 0px;margin:0;text-align:left;width:33%;">
                      <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/k12.jpg" alt="K-12" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                      
                      </td>                     
                    </tr> 
            </table>
	</td>
    </tr> 
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 20px 10px;width:618px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
        <p style="font-size:24px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#333333;">College &amp; University</p>
	</td>
    </tr>
    <tr>
    <td class="main" style="background-color:#FFFFFF;padding:0px 10px 20px;width:638px;border-left:1px solid #cccccc;border-right:1px solid #cccccc;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">             
                    <tr>
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 0px 0px 11px;margin:0;text-align:left;width:33%;">
                      <img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/collegeuniversity.jpg" alt="College &amp; University" class="image_fix step" style="width:100%;display:block;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" />
                      
                      </td>  
                      <td class="text shrinking" valign="top" style="font-size: 17px; line-height: 1.4em; font-family: Arial, sans-serif; color: #333333;padding:0px 10px 0px 10px;margin:0;text-align:left;width:66%;">
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/concern.png" />&nbsp;<strong>Safety Concern:</strong> Multi-building security solutions require sophisticated technology to secure college campuses and provide protection for students, staff and visitors. College and university security staff need to be able to keep a 24/7 watch over common areas, cafeterias, parking garages, and open spaces.</p>
                      <p style="font-size:17px;margin:0 0 10px 0;padding:0;text-align:left;line-height:1.5em;"><img src="http://login.powerleads.biz/themes/preview-editor/v1/email/themes/sc/det/img/solution.png" />&nbsp;<strong>Solution:</strong> Our IP-based access control solutions provide the ability to control and restrict access to key campus locations. Our integrated, remote-viewable security systems are designed for campus environments of all sizes, to improve student safety and simplify systems management for administrators.</p>
                      </td>                    
                    </tr> 
            </table>
	</td>
    </tr> 
    <tr>
    <td class="main" style="background-color:#015a66;padding:10px 20px 10px;width:618px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
        <p style="font-size:36px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.4em;font-family: sans-serif;font-weight:bold;color:#fffefd;">Why Choose Us?</p>
	</td>
    </tr>
    <tr>
    <td class="main" style="background-color:#015a66;padding:0px 10px 30px;width:638px;border-left:1px solid #015a66;border-right:1px solid #015a66;">
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin:0 0 0 0;border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">             
                    <tr>
                      <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Advanced <br/>Technology</strong> <br />We offer the latest security surveillance solutions that meet the needs of your application and your budget.</p>
                      
                      </td>  
                      <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;border-right:1px solid #80adb3;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Dedicated <br/>Support</strong> <br />Our trained security technicians are experts at servicing virtually any size security application.</p>
                      
                      </td>  
                      <td class="text shrinking" valign="top" style="font-size: 15px; line-height: 1.4em; font-family: Arial, sans-serif; color: #ffffff;padding:0px 10px 0px 11px;margin:0;text-align:left;width:33%;"><p style="font-size:15px;margin:0 0 0px 0;padding:0;text-align:left;line-height:1.5em;color: #ffffff;"><strong style="color:#ecbb41;font-size:18px;">Comprehensive <br/>Security Service</strong> <br />We offer a comprehensive set of security services ranging from site survey to technical support.</p>
                      
                      </td>  
                    </tr> 
            </table>
	</td>
    </tr>
    <tr>
    <td id="header" width="660" valign="top">
      <!-- logo, links and number -->
      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
        <tr>
        <td style="padding:10px 0px 10px 20px;background-color:#ffffff;text-align:left;" align="right" valign="bottom" class="rightbox">
            <p class="number" style="font-size: 23px; font-family: Arial,sans-serif;margin:10px 0 0;color:#333333;font-weight:bold;"><span style="font-weight:normal">Call</span> <?=$info['phone']?> <br /><span style="font-weight:normal;font-size:15px;">to discuss your security needs today!</span></p></td>
        <td id="logo" style="padding:10px 20px;background-color:#ffffff;text-align:right;" valign="top" align="right"><a target="_blank" href="<?=$info['website_url']?>"><img src="http://login.powerleads.biz/users<?=$info['profile_photo']?>" alt="<?=$info['company']?>" height="85" border="0" style="display: block; border: 0pt none;outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;" align="right" /></a></td>
        </tr>
        </table>
        <!-- end logo, links and number -->
    </td>
    </tr>
    </table>
</td>
</tr>
</table>  
<!-- end wrapper table -->
<? } ?>
</body>
</html>