<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no"/>
  <style type="text/css">

    /* Resets: see reset.css for details */
    .ReadMsgBody { width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    .ExternalClass, .ExternalClass p, .ExternalClass span,
    .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
    #outlook a{ padding:0;}
    body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0;}
    body{ -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    html{width:100%;}
    table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;}
    table td {border-collapse:collapse;}
    table p{margin:0;}
    br, strong br, b br, em br, i br { line-height:100%; }
    div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
    h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
    span a { text-decoration: none !important;}
    a{ text-decoration: none !important; }
    ul{list-style: none; margin:0; padding:0;}
    img{height: auto !important; line-height: 100%; outline: none; text-decoration: none; display:block !important;
      -ms-interpolation-mode:bicubic;}
    .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
    .yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
    /*mailChimp class*/
    .default-edit-image{
      height:20px;
    }
    .tpl-repeatblock {
      padding: 0px !important;
      border: 1px dotted rgba(0,0,0,0.2);
    }
    @media only screen and (max-width: 640px){
      /* mobile setting */
      table[class="container"]{width:100%!important; max-width:100%!important; min-width:100%!important;
        padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
      td[class="container"]{width:100%!important; padding-left:20px!important; padding-right:20px!important; clear: both;}
      table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
      table[class="full-width-center"] {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      table[class="auto-center"] {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      *[class="auto-center-all"]{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      table[class="col-3"]{width:30.35%!important; max-width:100%!important;}
      table[class="col-2"]{width:47%!important; max-width:100%!important;}
      td[class="col-top"]{display:table-header-group !important;}
      td[class="col-bottom"]{display:table-footer-group !important;}
      *[class="full-block"]{width:100% !important; display:block !important;}
      /* image */
      td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important;}
      /* helper */
      table[class="space-w-20"]{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
      table[class="space-w-20"] td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
      table[class="space-w-25"]{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
      table[class="space-w-25"] td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.4% !important;}
      *[class="h-20"]{display:block !important;  height:20px;}
      *[class="h-30"]{display:block !important; height:30px;}
      *[class="h-40"]{display:block !important;  height:40px;}
      *[class="remove-640"]{display:none !important;}
    }
    @media only screen and (max-width: 479px){
      /* mobile setting */
      table[class="container"]{width:100%!important; max-width:100%!important; min-width:124px!important;
        padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
      td[class="container"]{width:100%!important; padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
      table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
      table[class="full-width-center"] {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
      *[class="auto-center-all"]{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
      table[class="col-3"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
      table[class="col-2"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
      *[class="full-block-479"]{display:block !important; width:100% !important; text-align: center!important; clear: both;}
      /* image */
      td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
      td[class="image-min-80"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
      td[class="image-min-100"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
      /* halper */
      table[class="space-w-20"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
      table[class="space-w-20"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
      table[class="space-w-25"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
      table[class="space-w-25"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
      *[class="remove-479"]{display:none !important;}
    }
  </style>
</head>
<body style="font-size:12px; width:100%; height:100%;">
<table id="wrapper" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#f7f7f7;">
</table>
</body>
</html>

