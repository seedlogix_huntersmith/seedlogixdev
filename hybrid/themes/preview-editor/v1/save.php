<?php

require_once '/home/eric/web/6qube/hybrid/bootstrap.php';
require_once QUBEADMIN . 'library/Popcorn/BaseController.php';
require_once HYBRID_PATH . 'controllers/SecureController.php';
@session_start();

Qube::ForceMaster();

require_once QUBEADMIN . 'inc/hub.class.php';
$hub = new Hub();

$postid =   $_POST['postid'];
$blogid =   $_POST['blogid'];
$siteid =   $_POST['siteid'];
$themeid   =   $_POST['themeid'];
$pageid   =   $_POST['pageid'];
$column   =   $_POST['column'];
$touchpoint   =   $_POST['touchpoint'];
$fullscreen   =   $_POST['fullscreen'];


//$hub_pages = $hub->getHubPage($siteid, NULL, TRUE, TRUE, TRUE);


include('themes/inc/simple_html_dom.php');


foreach( $_POST['pages'] as $page=>$content ) {

    //$zip->addFromString($page.".html", $_POST['doctype']."\n".stripslashes($content));

    $html .= stripslashes($content);

}


$replaceValues = array('div[class="swiper-container swiper-parent"]'=>'[[slider]]', 'nav[class^=treplace]'=>'[[navtag]]', 'nav[class^=1treplace]'=>'[[navtag1]]', 'div[id="logo"]'=>'[[logo]]', 'div[class^=posts1]'=>'[[posts1]]', 'ul[id="pcategories"]'=>'[[category1]]', 'div[id="post1"]'=>'[[post1]]');
// DO IT:

		foreach($replaceValues as $replaceKeyword=>$replaceValue){
				
		$tagprep = str_get_html($html);
				
		$replace = $tagprep->find(''.$replaceKeyword.'', 0)->outertext;
		$html = str_replace($replace,$replaceValue,$tagprep);
		unset($replace);
				

		}
		
if($pageid){
    if($column=='page_region') {
        $q = 'UPDATE hub_page2legacy SET page_region = ? WHERE id = ? AND hub_id = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $pageid, $siteid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&pageid=' . $pageid . '&column=page_region&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
    if($column=='page_edit_2') {
        $q = 'UPDATE hub_page2legacy SET page_edit_2 = ? WHERE id = ? AND hub_id = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $pageid, $siteid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&pageid=' . $pageid . '&column=page_edit_2&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
	if($column=='page_edit_3') {
        $q = 'UPDATE hub_page2legacy SET page_edit_3 = ? WHERE id = ? AND hub_id = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $pageid, $siteid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&pageid=' . $pageid . '&column=page_edit_3&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
} else if($postid && $blogid) {
	if($column=='post_edit_1') {
        $q = 'UPDATE blog_post SET post_edit_1 = ? WHERE id = ? AND blog_id = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $postid, $blogid))) header('Location: /themes/preview-editor/v1/index.php?blogid=' . $blogid . '&postid=' . $postid . '&themeid=' . $themeid . '&column=post_edit_1&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }	
} else if($blogid && $themeid) {
	if($column=='blog_edit_1') {
        $q = 'UPDATE blogs SET blog_edit_1 = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $blogid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?blogid=' . $blogid . '&themeid=' . $themeid . '&column=blog_edit_1&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
	if($column=='blog_edit_2') {
        $q = 'UPDATE blogs SET blog_edit_2 = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $blogid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?blogid=' . $blogid . '&themeid=' . $themeid . '&column=blog_edit_2&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
	if($column=='blog_edit_3') {
        $q = 'UPDATE blogs SET blog_edit_3 = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $blogid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?blogid=' . $blogid . '&themeid=' . $themeid . '&column=blog_edit_3&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
} else if($siteid && $themeid) {
    if($column=='overview') {
        $q = 'UPDATE hub SET overview = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $siteid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&column=overview&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
    if($column=='offerings') {
        $q = 'UPDATE hub SET offerings = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $siteid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&column=offerings&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
	if($column=='photos') {
        $q = 'UPDATE hub SET photos = ? WHERE id = ? AND theme = ? LIMIT 1';
        $prepared = Qube::GetPDO()->prepare($q);
        if ($prepared->execute(array($html, $siteid, $themeid))) header('Location: /themes/preview-editor/v1/index.php?siteid=' . $siteid . '&themeid=' . $themeid . '&column=photos&touchpoint=' . $touchpoint . '&fullscreen=' . $fullscreen . '');
    }
} else return;
	
	
	//foreach( $_POST['pages'] as $page=>$content ) {
	
		//$zip->addFromString($page.".html", $_POST['doctype']."\n".stripslashes($content));
		
		//echo $content;
	
	//}
	
	//$zip->addFromString("testfilephp.txt" . time(), "#1 This is a test string added as testfilephp.txt.\n");
	//$zip->addFromString("testfilephp2.txt" . time(), "#2 This is a test string added as testfilephp2.txt.\n");
	
	//$zip->close();
	

?>