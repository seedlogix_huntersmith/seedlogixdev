<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/magnific-popup.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/universal/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="/themes/preview-editor/v1/themes/universal2/css/colors.php?links_color=<?=$color?>" type="text/css" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <?php if(!$sandbox==1){ ?>
    <script type="text/javascript" src="/hubs/themes/universal/js/jquery.js"></script>
    <script type="text/javascript" src="/hubs/themes/universal/js/plugins.js"></script>
    <? } ?>
    <style>
.videoframe {
	position: absolute;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	background: none;
}
</style>
</head>

<body class="stretched">