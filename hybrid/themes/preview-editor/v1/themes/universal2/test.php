<div class="widget clearfix" id="editContent20">

    <h4>Testimonials</h4>
    <div class="fslider testimonial noborder nopadding noshadow" data-animation="slide" data-arrows="false"  id="editContent21">
        <div class="flexslider" id="editContent22">
            <div class="slider-wrap" id="editContent23">
                <div class="slide" id="editContent24">
                    <div class="testi-image" id="editContent25">
                        <a href="#"><img src="http://canvashtml-cdn.semicolonweb.com/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content" id="editContent26">
                        <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                        <div class="testi-meta" id="editContent27">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide" id="editContent27">
                    <div class="testi-image" id="editContent28">
                        <a href="#"><img src="http://canvashtml-cdn.semicolonweb.com/images/testimonials/2.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content" id="editContent29">
                        <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
                        <div class="testi-meta" id="editContent30">
                            Collis Ta'eed
                            <span>Envato Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide" id="editContent31">
                    <div class="testi-image" id="editContent32">
                        <a href="#"><img src="http://canvashtml-cdn.semicolonweb.com/images/testimonials/1.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content" id="editContent33">
                        <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
                        <div class="testi-meta" id="editContent34">
                            John Doe
                            <span>XYZ Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>