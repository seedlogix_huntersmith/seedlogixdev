<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>

  <!-- Header
        ============================================= -->
  <div id="block-header2-<?=$height?>-<?php echo $divorder;?>">
    <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">
      <div id="header-wrap">
        <div class="container clearfix">
          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
          
          <!-- Logo
                    ============================================= -->
          <?php if($logo) { ?>
          
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> </div>
          <? } else { ?>
          <div id="logo" class="logo_replace">
              <a href="/" class="standard-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a>
              <a href="/" class="retina-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> </div>
          <? } ?>
          <!-- #logo end --> 
          
          <?php if($info['connect_hub_id']){ ?>
          <!-- Primary Navigation
                    ============================================= -->
          <nav id="primary-menu" class="nav_replace dark">
            <ul>
              <li class="current"><a href="/">Home</a></li>
              <?
				$customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
			        'inner1'=>'<li>', 'inner2'=>'</li>', 
						  'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
						  'aClassActive'=>'currentPage',
						  'liClassActive'=>'selected');
	
				foreach($hub_pages as $key=>$value)
				{ 
						$hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
				}
			?>
            </ul>
          </nav>
          <!-- #primary-menu end --> 
          <? } ?>
          
        </div>
      </div>
    </header>
    <!-- #header end -->
      <!-- Page Title
          ============================================= -->
      <section id="page-title">

          <div class="container clearfix">
              <h1 class="editContent"><?php if($info['blog_title']) echo $info['blog_title']; else echo 'Blog Title'; ?></h1>
              <span class="editContent">Dolorum elaboraret te eam</span>
              <ol class="breadcrumb editContent">
                  <li><a href="/">Home</a></li>
                  <li class="active"><?php if($info['blog_title']) echo $info['blog_title']; else echo 'Blog Title'; ?></li>
              </ol>
          </div>

      </section><!-- #page-title end -->

  </div>
  <? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
