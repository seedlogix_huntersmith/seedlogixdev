<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-contentblog-<?=$height?>-<?php echo $divorder;?>">

<!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent nobottommargin clearfix">

                        <!-- Posts
                        ============================================= -->
                        <div id="posts" class="posts1 post-timeline clearfix">

                            <div class="timeline-border"></div>

                            <div class="entry clearfix">
                                <div class="entry-timeline">
                                    10<span>June</span>
                                    <div class="timeline-divider"></div>
                                </div>
                                <div class="entry-image">
                                    <a href="/hubs/themes/universal/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/hubs/themes/universal/images/blog/standard/17.jpg" alt="Standard Post with Image"></a>
                                </div>
                                <div class="entry-title">
                                    <h2><a href="#">This is a Standard post with a Preview Image</a></h2>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><a href="#"><i class="icon-user"></i> admin</a></li>
                                    <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                                    <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                </ul>
                                <div class="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                                    <a href="#"class="more-link">Read More</a>
                                </div>
                            </div>


                        </div>
                        <!-- #posts end -->

                    </div><!-- .postcontent end -->

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">

                           	<div class="widget widget_links clearfix">
                                <h4>Categories</h4>
                                <ul id="pcategories">
                                    <li><a href="#"><div>Category 1</div></a></li>
                                    <li><a href="#"><div>Category 2</div></a></li>
                                    <li><a href="#"><div>Category 3</div></a></li>
                                </ul>
                            </div>


                        </div>

                    </div><!-- .sidebar end -->

                </div>

            </div>

        </section><!-- #content end -->

</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>