<?
	//COMMON HEADER
	include('inc/header.php');
?>

<div id="wrapper" class="clearfix"> 
  <!-- Content
        ============================================= -->

      <!-- ADD CONTENT BLOCK CODE HERE -->
      
      		<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){ 
              		echo $divparse;
  
				 } else {
	  
				?>

                   <div id="block-content10-<?=$height?>-<?php echo $divorder;?>" class="section">
                    <div class="container clearfix">

                        <div class="row topmargin-sm">

                            <div class="heading-block center">
                                <h3>Meet Our Team</h3>
                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="/hubs/themes/universal/images/team/3.jpg" alt="John Doe">
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="/hubs/themes/universal/images/team/2.jpg" alt="Josh Clark">
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="/hubs/themes/universal/images/team/8.jpg" alt="Mary Jane">
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="/hubs/themes/universal/images/team/4.jpg" alt="Nix Maxwell">
                                    </div>
                                    <div class="team-desc team-desc-bg">
                                        <div class="team-title"><h4>John Doe</h4><span>CEO</span></div>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
      <? } ?>
      
      <!-- ADD CONTENT BLOCK CODE HERE --> 

</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
