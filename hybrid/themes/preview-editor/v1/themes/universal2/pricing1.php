<?
//COMMON HEADER
include('inc/header.php');
?>
    <div id="wrapper" class="clearfix">

        <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
            echo $divparse;

        } else {

            ?>
            <div id="block-pricing1-<?=$height?>-<?php echo $divorder;?>">
    <section id="content">

    <div class="content-wrap">
        <div class="container clearfix">

            <div class="heading-block center">
                <h2>Pricing.</h2>
                <span>We believe in Flexible Costing.</span>
            </div>

            <div class="pricing pricing-5 clearfix">

                <div class="pricing-box pricing-minimal">
                    <div class="pricing-title">
                        <h3>Starter</h3>
                        <span>5GB in the Cloud</span>
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&#36;</span>0<span class="price-tenure">/mo</span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-block">Get Started</a>
                    </div>
                </div>

                <div class="pricing-box pricing-minimal best-price">
                    <div class="pricing-title">
                        <h3>Professional</h3>
                        <span>50GB in the Cloud</span>
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&#36;</span>12<span class="price-tenure">/mo</span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-block bgcolor border-color">Start Trial</a>
                    </div>
                </div>

                <div class="pricing-box pricing-minimal">
                    <div class="pricing-title">
                        <h3>Business</h3>
                        <span>100GB in the Cloud</span>
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&#36;</span>19<span class="price-tenure">/mo</span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-block">Start Trial</a>
                    </div>
                </div>

                <div class="pricing-box pricing-minimal">
                    <div class="pricing-title">
                        <h3>Enterprise</h3>
                        <span>500GB in the Cloud</span>
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&#36;</span>29<span class="price-tenure">/mo</span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-block">Start Trial</a>
                    </div>
                </div>

                <div class="pricing-box pricing-minimal">
                    <div class="pricing-title">
                        <h3>Unlimited</h3>
                        <span>&infin; GB in the Cloud</span>
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&#36;</span>39<span class="price-tenure">/mo</span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-block">Start Trial</a>
                    </div>
                </div>

            </div>

        </div>
        </div>
        </section>
            </div>
        <? } ?>
    </div>
<?
//COMMON FOOTER
include('inc/footer.php');
?>