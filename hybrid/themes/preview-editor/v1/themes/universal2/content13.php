<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-content13-<?=$height?>-<?php echo $divorder;?>">
  <!-- Content
        ============================================= -->
  <section id="content">
      <!-- ADD CONTENT BLOCK CODE HERE -->

			<div class="container clearfix">
         <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

                    <div id="section-pricing" class="heading-block title-center nobottomborder page-section">
                        <h2>Pricing Details</h2>
                        <span>Diceret moderatius no eos, in populo eleifend reformidans mel.</span>
                    </div>

                    <div class="pricing-box pricing-extended bottommargin clearfix">

                        <div class="pricing-desc">
                            <div class="pricing-title">
                                <h3>Diceret moderatius no eos?</h3>
                            </div>
                            <div class="pricing-features">
                                <ul class="iconlist-color cleafix">
                                    <li><i class="icon-desktop"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-eye-open"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-beaker"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-magic"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-font"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-stack3"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-file2"></i> Prima Choro Mumquam</li>
                                    <li><i class="icon-support"></i> Prima Choro Mumquam</li>
                                </ul>
                            </div>
                        </div>

                        <div class="pricing-action-area">
                            <div class="pricing-meta">
                                As Low as
                            </div>
                            <div class="pricing-price">
                                <span class="price-unit">$</span>499<span class="price-tenure">monthly</span>
                            </div>
                            <div class="pricing-action">
                                <a href="#" class="button button-3d button-large btn-block nomargin">Get Started</a>
                            </div>
                        </div>

                    </div>

                    <div class="clear"></div>
                    </div>
      <!-- ADD CONTENT BLOCK CODE HERE -->
  </section>
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>