<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>

  <!-- Header
        ============================================= -->
  <div id="block-header-<?=$height?>-<?php echo $divorder;?>">
    <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">
      <div id="header-wrap">
        <div class="container clearfix">
          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
          
          <!-- Logo
                    ============================================= -->
          <?php if($logo) { ?>
          
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> </div>
          <? } else { ?>
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> </div>
          <? } ?>
          <!-- #logo end --> 
          
          <!-- Primary Navigation
                    ============================================= -->
          <nav id="primary-menu" class="nav_replace dark">
            <ul>
              <li class="current"><a href="/">Home</a></li>
              <?
				$customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
			        'inner1'=>'<li>', 'inner2'=>'</li>', 
						  'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
						  'aClassActive'=>'currentPage',
						  'liClassActive'=>'selected');
	
				foreach($hub_pages as $key=>$value)
				{ 
						$hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
				}
			?>
            </ul>
          </nav>
          <!-- #primary-menu end --> 
          
        </div>
      </div>
    </header>
    <!-- #header end -->
    <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
      <div class="slider_replace swiper-container swiper-parent">
        <div class="swiper-wrapper">
          <?php if($slides){ ?>
          <?php foreach($slides as $photo) { ?>
          <div id="slide<?php echo ($c+0);?>" class="swiper-slide dark" style="background-image: url('/users<?=$photo->background?>');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle<?php echo ($c+0);?>" data-caption-animate="fadeInUp">
                  <?=$photo->title?>
                </h2>
                <p id="slideDesc<?php echo ($c+0);?>" data-caption-animate="fadeInUp" data-caption-delay="200">
                  <?=$photo->description?>
                </p>
              </div>
            </div>
          </div>
          <?php $c++;?>
          <?php } ?>
          <?php } else { ?>
          <div id="slide1" class="swiper-slide dark" style="background-image: url('/hubs/themes/universal/images/slider/full/1.jpg');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle1" data-caption-animate="fadeInUp">Welcome to Universal</h2>
                <p id="slideDesc1" data-caption-animate="fadeInUp" data-caption-delay="200">Create just what you need for your Responsive Website. Choose from a wide range of Content Block and &amp; simply put them on your own Canvas.</p>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
      </div>

    </section>
  </div>

<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
