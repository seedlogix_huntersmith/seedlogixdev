<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){ 
  
  echo $divparse;
  
  } else {
  
  ?>

  <!-- Header
        ============================================= -->
  <div id="block-footer-<?=$height?>-<?php echo $divorder;?>">
<!-- Footer
        ============================================= -->

<footer id="footer">
  <div class="container"> 
    
    <!-- Footer Widgets
                ============================================= -->
    <div class="footer-widgets-wrap clearfix">
      <div class="col_two_third">
        <div class="col_one_third">
          <div class="widget clearfix">
            <p>Tale quando conclusionemque eu vim. </p>
            <div style="background: url('/hubs/themes/universal/images/world-map.png') no-repeat center center; background-size: 100%;">
              <address>
              <strong>Headquarters:</strong><br>
              Street<br>
              City, State, Zip<br>
              </address>
              <abbr title="Phone Number"><strong>Phone:</strong></abbr> (555) 555-5555<br>
              <abbr title="Fax"><strong>Fax:</strong></abbr> (555) 555-5555<br>
              <abbr title="Email Address"><strong>Email:</strong></abbr> email@email.com</div>
          </div>
        </div>
        <div class="col_one_third">
          <div class="widget widget_links clearfix">
            <h4>Blogroll</h4>
            <ul>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
              <li><a href="#">Eum an soluta percipitur. </a></li>
            </ul>
          </div>
        </div>
        <div class="col_one_third col_last">
          <div class="widget clearfix">
            <h4>Mazim copiosae</h4>
            <div id="post-list-footer">
              <div class="spost clearfix">
                <div class="entry-c">
                  <div class="entry-title">
                    <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                  </div>
                  <ul class="entry-meta">
                    <li>10th July 2015</li>
                  </ul>
                </div>
              </div>
              <div class="spost clearfix">
                <div class="entry-c">
                  <div class="entry-title">
                    <h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
                  </div>
                  <ul class="entry-meta">
                    <li>10th July 2015</li>
                  </ul>
                </div>
              </div>
              <div class="spost clearfix">
                <div class="entry-c">
                  <div class="entry-title">
                    <h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
                  </div>
                  <ul class="entry-meta">
                    <li>10th July 2015</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col_one_third col_last">
        <div class="widget clearfix" style="margin-bottom: -20px;">
          <div class="row">
            <div class="col-md-6 clearfix bottommargin-sm"> <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;"> <i class="icon-facebook"></i> <i class="icon-facebook"></i> </a> <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>
              on Facebook</small></a> </div>
            <div class="col-md-6 clearfix"> <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;"> <i class="icon-rss"></i> <i class="icon-rss"></i> </a> <a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>
              to RSS Feeds</small></a> </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .footer-widgets-wrap end --> 
    
  </div>
  
  <!-- Copyrights
            ============================================= -->
  <div id="copyrights">
    <div class="container clearfix">
      <div class="col_half"> Copyrights &copy; 2015 All Rights Reserved<br>
        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
      </div>
      <div class="col_half col_last tright">
        <div class="fright clearfix"> 
        <a href="#" class="social-icon si-small si-borderless si-facebook"> <i class="icon-facebook"></i> <i class="icon-facebook"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-twitter"> <i class="icon-twitter"></i> <i class="icon-twitter"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-gplus"> <i class="icon-gplus"></i> <i class="icon-gplus"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-pinterest"> <i class="icon-pinterest"></i> <i class="icon-pinterest"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-vimeo"> <i class="icon-vimeo"></i> <i class="icon-vimeo"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-github"> <i class="icon-github"></i> <i class="icon-github"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-yahoo"> <i class="icon-yahoo"></i> <i class="icon-yahoo"></i> </a> 
        <a href="#" class="social-icon si-small si-borderless si-linkedin"> <i class="icon-linkedin"></i> <i class="icon-linkedin"></i> </a> 
        </div>
        <div class="clear"></div>
        <div><i class="icon-envelope2"></i> email@email.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 555-555-5555 <span class="middot">&middot;</span> <i class="icon-skype2"></i> Skype </div></div>
    </div>
  </div>
  <!-- #copyrights end --> 
  
</footer>
<!-- #footer end -->
  </div>
</div>

<? } ?>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
