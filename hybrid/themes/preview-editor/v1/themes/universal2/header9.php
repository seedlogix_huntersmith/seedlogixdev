<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>

  <!-- Header
        ============================================= -->
  <div id="block-header9-<?=$height?>-<?php echo $divorder;?>">
     <!-- Header
        ============================================= -->
        <header id="header" class="transparent-header floating-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <?php if($logo) { ?>
          
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> </div>
          <? } else { ?>
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> </div>
          <? } ?><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu" class="nav_replace dark">
            <ul>
              <li class="current"><a href="/">Home</a></li>
              <?
				$customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
			        'inner1'=>'<li>', 'inner2'=>'</li>', 
						  'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
						  'aClassActive'=>'currentPage',
						  'liClassActive'=>'selected');
	
				foreach($hub_pages as $key=>$value)
				{ 
						$hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
				}
			?>
            </ul>
          </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

       <section id="slider" style="background: url(/hubs/themes/universal/images/landing/landing2.jpg) center center / cover; overflow: visible;" data-height-lg="500" data-height-md="500" data-height-sm="600" data-height-xs="600" data-height-xxs="600">
            <div class="container clearfix">

                <div class="landing-wide-form landing-form-overlay dark nobottommargin clearfix">
                    <div class="heading-block nobottommargin nobottomborder">
                        <h2>Quam, ex, inventore!</h2>
                        <span>Lorem ipsum dolor sit</span>
                    </div>
                    <div class="line" style="margin: 10px 0 20px;"></div>
                    <div class="header1form">
                        **Put Your Form Tag Here**
                    </div>
                </div>

            </div>
        </section>

        <!-- Content
        ============================================= -->
        <section id="content" style="overflow: visible;">

            <div class="content-wrap">

                <div class="promo promo-dark promo-full landing-promo header-stick">
                    <div class="container clearfix">
                        <h3>Eam cu tale equidem molestie, vide partem lucilius an usu. <i class="icon-circle-arrow-right" style="position:relative;top:2px;"></i></h3>
                        <span>Duo eu erant offendit, id salutatus constituto dissentiunt nec, vide partem lucilius an usu.</span>
                    </div>
                </div>
            </div>
        </section>
  </div>

<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
