<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-content14-<?=$height?>-<?php echo $divorder;?>">
  <!-- Content
        ============================================= -->
 <div class="section">

                    <div class="container clearfix">

                        <div id="section-testimonials" class="heading-block title-center page-section">
                            <h2>Testimonials</h2>
                            <span>Prima choro numquam his in, cibo insolens mea no.</span>
                        </div>

                        <ul class="testimonials-grid grid-3 clearfix">
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/1.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum repellendus!</p>
                                        <div class="testi-meta">
                                            John Doe
                                            <span>XYZ Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/2.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/7.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Fugit officia dolor sed harum excepturi ex iusto magnam asperiores molestiae qui natus obcaecati facere sint amet.</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/4.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, perspiciatis illum totam dolore deleniti labore.</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/8.jpg" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Porro dolorem saepe reiciendis nihil minus neque. Ducimus rem necessitatibus repellat laborum nemo quod.</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>

                </div>
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>