<?
//COMMON HEADER
include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
    <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){

        echo $divparse;

    } else {

    ?>

    <!-- Header
          ============================================= -->
    <div id="block-headerlanding-<?=$height?>-<?php echo $divorder;?>" class="wrp">
        <!-- Header
        ============================================= -->
        <header id="header" class="transparent-header page-section dark">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <?php if($logo) { ?>
                        <div id="logo" class="logo_replace">
                            <a href="/" class="standard-logo" data-dark-logo="/users<?=$logo?>"><img src="/users<?=$logo?>" ></a>
                            <a href="/" class="retina-logo" data-dark-logo="/users<?=$logo?>"><img src="/users<?=$logo?>" ></a>
                        </div><!-- #logo end -->
                    <? } else { ?>
                        <div id="logo" class="logo_replace">
                            <a href="/" class="standard-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"><img src="/hubs/themes/universal2/images/universal-logo.png" ></a>
                            <a href="/" class="retina-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"><img src="/hubs/themes/universal2/images/universal-logo.png" ></a>
                        </div><!-- #logo end -->
                    <? } ?>

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu">
                            <li class="current"><a href="#" data-href="#header"><div>Home</div></a></li>
                            <li><a href="#" data-href="#section-features"><div>Features</div></a></li>
                            <li><a href="#" data-href="#section-pricing"><div>Pricing</div></a></li>
                            <li><a href="#" data-href="#section-testimonials"><div>Testimonials</div></a></li>
                            <li><a href="#" data-href="#section-specs"><div>Specifications</div></a></li>
                            <li><a href="#" data-href="#section-buy"><div>Buy Now</div></a></li>
                        </ul>

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header>
        <!-- #header end -->
        <section id="slider" style="background: url(/hubs/themes/universal/images/landing/landing2.jpg) center center / cover; overflow: visible;" data-height-lg="500" data-height-md="500" data-height-sm="600" data-height-xs="600" data-height-xxs="600">
            <div class="container clearfix">

                <div class="landing-wide-form landing-form-overlay dark nobottommargin clearfix">
                    <div class="heading-block nobottommargin nobottomborder">
                        <h2>Quam, ex, inventore!</h2>
                        <span>Lorem ipsum dolor sit</span>
                    </div>
                    <div class="line" style="margin: 10px 0 20px;"></div>
                    <div class="header1form">
                        **Put Your Form Tag Here**
                    </div>
                </div>

            </div>
        </section>

        <!-- Content
        ============================================= -->
        <section id="content" style="overflow: visible;">

            <div class="content-wrap">

                <div class="promo promo-dark promo-full landing-promo header-stick">
                    <div class="container clearfix">
                        <h3>Eam cu tale equidem molestie, vide partem lucilius an usu. <i class="icon-circle-arrow-right" style="position:relative;top:2px;"></i></h3>
                        <span>Duo eu erant offendit, id salutatus constituto dissentiunt nec, vide partem lucilius an usu.</span>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <? } ?>
</div>


<?
//COMMON FOOTER
include('inc/footer.php');
?>
