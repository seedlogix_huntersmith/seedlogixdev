<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-750-' . $divorder . ''.$sbx.'"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>

  <!-- Header
        ============================================= -->
  <div id="block-slider-750-<?php echo $divorder;?><?php echo $sbx;?>">
    <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
      <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
          <?php if($slides){ ?>
          <?php foreach($slides as $photo) { ?>
          <div id="slide<?php echo ($c+0);?>" class="swiper-slide dark" style="background-image: url('/users<?=$photo->background?>');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle<?php echo ($c+0);?>" data-caption-animate="fadeInUp">
                  <?=$photo->title?>
                </h2>
                <p id="slideDesc<?php echo ($c+0);?>" data-caption-animate="fadeInUp" data-caption-delay="200">
                  <?=$photo->description?>
                </p>
              </div>
            </div>
          </div>
          <?php $c++;?>
          <?php } ?>
          <?php } else { ?>
          <div id="slide1" class="swiper-slide dark" style="background-image: url('/hubs/themes/universal/images/slider/full/1.jpg');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle1" data-caption-animate="fadeInUp">Welcome to SeedLogix</h2>
                <p id="slideDesc1" data-caption-animate="fadeInUp" data-caption-delay="200">Create just what you need for your Responsive Website. Choose from a wide range of Conntent Block and &amp; simply put them on your own Canvas.</p>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
      </div>
     
    </section>
  </div>
</div>

<? } ?>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
