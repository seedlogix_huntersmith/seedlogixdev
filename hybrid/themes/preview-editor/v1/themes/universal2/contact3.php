<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-contact3-<?=$height?>-<?php echo $divorder;?>">
<!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget_links clearfix">

                               <h4>Links Somewhere</h4>
                                <ul>
                                    <li><a href="#"><div>Link 1</div></a></li>
                                    <li><a href="#"><div>Link 2</div></a></li>

                                </ul>


                            </div>


                        </div>
                    </div><!-- .sidebar end -->

                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent bothsidebar nobottommargin">

                        <!-- Google Map
                        ============================================= -->
                        <section id="google-map" class="gmap bottommargin" style="height: 300px;"></section>


                        <!-- Contact Form
                        ============================================= -->
                        <h3>Send us an Email</h3>

                       ****Add form tag here*** 

                        <div class="line"></div>


                        <!-- Contact Info
                        ============================================= -->
                        <div class="col_one_third col_last nobottommargin">
                            <address>
                                <strong>Headquarters:</strong><br>
                            123 Any Street, Suite 600<br>
                            Anytown, US 55555<br>
                            </address>
                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Fax"><strong>Fax:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> nothing@nothing.com
                        </div><!-- Contact Info End -->

                    </div><!-- .postcontent end -->

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">

                             <div class="widget widget_links clearfix">

                                <h4>Links Somewhere</h4>
                                <ul>
                                    <li><a href="#"><div>Link 1</div></a></li>
                                    <li><a href="#"><div>Link 2</div></a></li>

                                </ul>

                            </div>

                          
                        </div>
                    </div><!-- .sidebar end -->

                </div>

            </div>

        </section><!-- #content end -->
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>