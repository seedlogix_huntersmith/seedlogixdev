<?
//COMMON HEADER
include('inc/header.php');
?>
    <div id="wrapper" class="clearfix">

        <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
            echo $divparse;

        } else {

            ?>
            <div id="block-content17-<?=$height?>-<?php echo $divorder;?>">
                <!-- Content
                      ============================================= -->
                <div class="col_full bottommargin-lg common-height">

                    <div class="col-md-4 dark col-padding ohidden" style="background-color: #1abc9c;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">Why choose Us</h3>
                            <p style="line-height: 1.8;">Has errem aliquando et, scaevola erroribus disputationi te mea. Sit et aliquam salutatus torquatos, an quem dolorum pro, te sit idque prompta. Et nobis facete vulputate eum, no duo omittam qualisque, has ne invenire consetetur scripserit. </p>
                            <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Read More</span></a>
                            <i class="icon-bulb bgicon"></i>
                        </div>
                    </div>

                    <div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">Our Mission</h3>
                            <p style="line-height: 1.8;">Has errem aliquando et, scaevola erroribus disputationi te mea. Sit et aliquam salutatus torquatos, an quem dolorum pro, te sit idque prompta. Et nobis facete vulputate eum, no duo omittam qualisque, has ne invenire consetetur scripserit. </p>
                            <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                            <i class="icon-cog bgicon"></i>
                        </div>
                    </div>

                    <div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">What you get</h3>
                            <p style="line-height: 1.8;">Has errem aliquando et, scaevola erroribus disputationi te mea. Sit et aliquam salutatus torquatos, an quem dolorum pro, te sit idque prompta. Et nobis facete vulputate eum, no duo omittam qualisque, has ne invenire consetetur scripserit. </p>
                            <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                            <i class="icon-thumbs-up bgicon"></i>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>
            </div>
        <? } ?>
    </div>
<?
//COMMON FOOTER
include('inc/footer.php');
?>