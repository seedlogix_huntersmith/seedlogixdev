<?
	//COMMON HEADER
	include('inc/header.php');
?>

<div id="wrapper" class="clearfix">
    <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
        echo $divparse;

    } else {

    ?>
    <div id="block-content5-<?=$height?>-<?php echo $divorder;?>">
  <!-- Content
        ============================================= -->
  <section id="content">
    <div class="content-wrap">
      <!-- ADD CONTENT BLOCK CODE HERE -->

      <div class="container clearfix">
        <div class="col_one_third">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn">
            <div class="fbox-icon"> <a href="#"><i class="icon-line-monitor"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="200">
            <div class="fbox-icon"> <a href="#"><i class="icon-line-eye"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third col_last">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="400">
            <div class="fbox-icon"> <a href="#"><i class="icon-line-star"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="clear"></div>
        <div class="col_one_third">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="600">
            <div class="fbox-icon"> <a href="#"><i class="icon-line-play"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="800">
            <div class="fbox-icon"> <a href="#"><i class="icon-params"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third col_last">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="1000">
            <div class="fbox-icon"> <a href="#"><i class="icon-line-circle-check"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="clear"></div>
        <div class="col_one_third bottommargin-sm">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="600">
            <div class="fbox-icon"> <a href="#"><i class="icon-bulb"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third bottommargin-sm">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="800">
            <div class="fbox-icon"> <a href="#"><i class="icon-heart2"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="col_one_third bottommargin-sm col_last">
          <div class="feature-box fbox-small fbox-plain" data-animate="fadeIn" data-delay="1000">
            <div class="fbox-icon"> <a href="#"><i class="icon-note"></i></a> </div>
            <h3>Eu modo regione sea</h3>
            <p>Et wisi feugait pro, eu natum adhuc appellantur vim, ei nulla accusamus philosophia usu. </p>
          </div>
        </div>
        <div class="clear"></div>
      </div>

      
      <!-- ADD CONTENT BLOCK CODE HERE -->
  </section>
</div>
    <? } ?>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
