<?
	//COMMON HEADER
	include('inc/header.php');
?>

<div id="wrapper" class="clearfix">
<!-- Content
        ============================================= -->

  
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){ 
              		echo $divparse;
  
				 } else {
	  
				?>
<div id="block-content6-<?=$height?>-<?php echo $divorder;?>">
    <div class="section topmargin nobottommargin nobottomborder">
      <div class="container clearfix">
        <div class="heading-block center nomargin">
          <h3>Our Latest Works</h3>
        </div>
      </div>
    </div>
    <div id="portfolio" class="portfolio-nomargin portfolio-notitle portfolio-full clearfix">
      <article class="portfolio-item pf-media pf-icons">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/1.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="/hubs/themes/universal/images/portfolio/full/1.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
          </div>
      </article>
      <article class="portfolio-item pf-illustrations">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/2.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="/hubs/themes/universal/images/portfolio/full/2.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
          </div>
      </article>
      <article class="portfolio-item pf-graphics pf-uielements">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/3.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="http://vimeo.com/89546048" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a> <a href="portfolio-single-video.php" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="portfolio-single-video.php">Eu modo regione</a></h3>
          </div>
      </article>
      <article class="portfolio-item pf-icons pf-illustrations">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/4.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay" data-lightbox="gallery"> <a href="/hubs/themes/universal/images/portfolio/full/4.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a> <a href="/hubs/themes/universal/images/portfolio/full/4-1.jpg" class="hidden" data-lightbox="gallery-item"></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
        </div>
      </article>
      <article class="portfolio-item pf-uielements pf-media">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/5.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="/hubs/themes/universal/images/portfolio/full/5.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
      </div>
      </article>
      <article class="portfolio-item pf-graphics pf-illustrations">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/6.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay" data-lightbox="gallery"> <a href="/hubs/themes/universal/images/portfolio/full/6.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a> <a href="/hubs/themes/universal/images/portfolio/full/6-1.jpg" class="hidden" data-lightbox="gallery-item"></a> <a href="/hubs/themes/universal/images/portfolio/full/6-2.jpg" class="hidden" data-lightbox="gallery-item"></a> <a href="/hubs/themes/universal/images/portfolio/full/6-3.jpg" class="hidden" data-lightbox="gallery-item"></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
         </div>
      </article>
      <article class="portfolio-item pf-uielements pf-icons">
        <div class="portfolio-image"> <a href="portfolio-single-video.php"> <img src="/hubs/themes/universal/images/portfolio/4/7.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="http://www.youtube.com/watch?v=GCQlmyM7GkM" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a> <a href="portfolio-single-video.php" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="portfolio-single-video.php">Eu modo regione</a></h3>
          </div>
      </article>
      <article class="portfolio-item pf-graphics">
        <div class="portfolio-image"> <a href="#"> <img src="/hubs/themes/universal/images/portfolio/4/8.jpg" alt="Eu modo regione"> </a>
          <div class="portfolio-overlay"> <a href="/hubs/themes/universal/images/portfolio/full/8.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a> <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a> </div>
        </div>
        <div class="portfolio-desc">
          <h3><a href="#">Eu modo regione</a></h3>
        </div>
      </article>
    </div>
    <div class="clear"></div>
</div>
    <? } ?>
    
    <!-- ADD CONTENT BLOCK CODE HERE --> 

</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
