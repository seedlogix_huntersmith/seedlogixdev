<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-contact4-<?=$height?>-<?php echo $divorder;?>">
<!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Google Map
                    ============================================= -->
                    <div class="col-md-6 bottommargin">

                        <section id="google-map" class="gmap" style="height: 240px;"></section>

                    </div><!-- Google Map End -->

                    <div class="col-md-6">

                        <!-- Contact Info
                        ============================================= -->
                        <div class="col_two_fifth">

                           <address>
                                <strong>Headquarters:</strong><br>
                            123 Any Street, Suite 600<br>
                            Anytown, US 55555<br>
                            </address>
                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Fax"><strong>Fax:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> nothing@nothing.com

                        </div><!-- Contact Info End -->

                      

                        <div class="clear"></div>

                        <!-- Modal Contact Form
                        ============================================= -->
                        <a href="#" data-toggle="modal" data-target="#contactFormModal" class="button button-3d nomargin btn-block button-xlarge center">Click here to Send an Email</a>

                        <div class="modal fade" id="contactFormModal" tabindex="-1" role="dialog" aria-labelledby="contactFormModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="contactFormModalLabel">Send Us an Email</h4>
                                    </div>
                                    <div class="modal-body">
                                        ****Add form tag here*** 
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <!-- Modal Contact Form End -->

                    </div>

                </div>

            </div>

        </section><!-- #content end -->
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>