<?
	//COMMON HEADER
	include('inc/header.php');
?>

<div id="wrapper" class="clearfix">
    <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
        echo $divparse;

    } else {

    ?>
    <div id="block-content1-<?=$height?>-<?php echo $divorder;?>">
  <!-- Content
        ============================================= -->
  <section id="content">

      <!-- ADD CONTENT BLOCK CODE HERE -->
      

      <div class="section nobottommargin">
        <div class="container clear-bottommargin clearfix">
          <div class="row topmargin-sm clearfix">
            <div class="col-md-4 bottommargin"> <i class="i-plain color i-large icon-line2-screen-desktop inline-block" style="margin-bottom: 15px;"></i>
              <div class="heading-block nobottomborder" style="margin-bottom: 15px;"> <span class="before-heading">Ad vis habeo debet luptatum</span>
                <h4>Ne minimum epicurei has</h4>
              </div>
              <p>Alienum consulatu quaerendum id sit, est eu viris graecis oporteat. Diceret bonorum similique eos id, ne partem delicata his, per audiam aliquid tincidunt et. Nec ex corpora oportere. Id vide duis ipsum sit, at tale possit nam.</p>
            </div>
            <div class="col-md-4 bottommargin"> <i class="i-plain color i-large icon-line2-energy inline-block" style="margin-bottom: 15px;"></i>
              <div class="heading-block nobottomborder" style="margin-bottom: 15px;"> <span class="before-heading">Ad vis habeo debet luptatum</span>
                <h4>Ne minimum epicurei has</h4>
              </div>
              <p>Alienum consulatu quaerendum id sit, est eu viris graecis oporteat. Diceret bonorum similique eos id, ne partem delicata his, per audiam aliquid tincidunt et. Nec ex corpora oportere. Id vide duis ipsum sit, at tale possit nam.</p>
            </div>
            <div class="col-md-4 bottommargin"> <i class="i-plain color i-large icon-line2-equalizer inline-block" style="margin-bottom: 15px;"></i>
              <div class="heading-block nobottomborder" style="margin-bottom: 15px;"> <span class="before-heading">Ad vis habeo debet luptatum</span>
                <h4>Ne minimum epicurei has</h4>
              </div>
              <p>Alienum consulatu quaerendum id sit, est eu viris graecis oporteat. Diceret bonorum similique eos id, ne partem delicata his, per audiam aliquid tincidunt et. Nec ex corpora oportere. Id vide duis ipsum sit, at tale possit nam.</p>
            </div>
          </div>
        </div>
      </div>

      
      <!-- ADD CONTENT BLOCK CODE HERE --> 

  </section>
    </div>
    <? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
