<?
//COMMON HEADER
include('inc/header.php');
?>
    <div id="wrapper" class="clearfix">

        <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
            echo $divparse;

        } else {

            ?>
            <div id="block-pricing2-<?=$height?>-<?php echo $divorder;?>">
    <section id="content">

    <div class="content-wrap">
        <div class="container clearfix">

            <div class="heading-block center">
                <h2>Pricing.</h2>
                <span>We believe in Flexible Costing.</span>
            </div>

            <div class="table-responsive">

                <table class="table table-hover table-comparison nobottommargin">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Starter</th>
                        <th>Professional</th>
                        <th>Business</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Space</td>
                        <td>1 GB</td>
                        <td>5 GB</td>
                        <td>20 GB</td>
                    </tr>
                    <tr>
                        <td>Bandwidth</td>
                        <td>10 GB</td>
                        <td>100 GB</td>
                        <td>500 GB</td>
                    </tr>
                    <tr>
                        <td>Email Accounts</td>
                        <td>100</td>
                        <td>1000</td>
                        <td>5000</td>
                    </tr>
                    <tr>
                        <td>MySQL Accounts</td>
                        <td>100</td>
                        <td>1000</td>
                        <td>5000</td>
                    </tr>
                    <tr>
                        <td>SSH Access</td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-ok"></i></td>
                    </tr>
                    <tr>
                        <td>SMTP Management</td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-ok"></i></td>
                    </tr>
                    <tr>
                        <td>FTP Access</td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-ok"></i></td>
                        <td><i class="icon-ok"></i></td>
                    </tr>
                    <tr>
                        <td>Google Adwords Credit</td>
                        <td><i class="icon-remove"></i></td>
                        <td><i class="icon-ok"></i></td>
                        <td><i class="icon-ok"></i></td>
                    </tr>
                    <tr>
                        <td>Monthly Price</td>
                        <td>Free</td>
                        <td>$9.99</td>
                        <td>$19.99</td>
                    </tr>
                    <tr>
                        <td>Quaterly Price</td>
                        <td>Free</td>
                        <td>$19.99</td>
                        <td>$49.99</td>
                    </tr>
                    <tr>
                        <td>Yearly Price</td>
                        <td>Free</td>
                        <td>$69.99</td>
                        <td>$149.99</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><a href="#" class="btn btn-default">Sign Up</a></td>
                        <td><a href="#" class="btn btn-default">Sign Up</a></td>
                        <td><a href="#" class="btn btn-default">Sign Up</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>

        </div>
        </div>
        </section>
            </div>
        <? } ?>
    </div>
<?
//COMMON FOOTER
include('inc/footer.php');
?>