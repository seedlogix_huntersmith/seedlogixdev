<?
	//COMMON HEADER
	include('inc/header.php');
?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="clearfix">
  <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){
  
  echo $divparse;
  
  } else {
  
  ?>
<div id="block-header4-<?=$height?>-<?php echo $divorder;?>">
<!-- Top Bar
		============================================= -->
		<div id="top-bar">

			<div class="container clearfix">

				<div class="col_half nobottommargin clearfix">

					<!-- Top Social
					============================================= -->
					<div id="top-social">
						<ul>
							<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="#" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
							<li><a href="#" class="si-dribbble"><span class="ts-icon"><i class="icon-dribbble"></i></span><span class="ts-text">Dribbble</span></a></li>
							<li><a href="#" class="si-github"><span class="ts-icon"><i class="icon-github-circled"></i></span><span class="ts-text">Github</span></a></li>
							<li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest"></i></span><span class="ts-text">Pinterest</span></a></li>
							<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
						</ul>
					</div><!-- #top-social end -->

				</div>

				<div class="col_half fright col_last clearfix nobottommargin">

					<!-- Top Links
					============================================= -->
					<div class="top-links">
						<ul>
							<li><a href="#">Locations</a>
								<ul>
									<li><a href="#">San Francisco</a></li>
									<li><a href="#">Austin</a></li>
									<li><a href="#">New York</a></li>
								</ul>
							</li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div><!-- .top-links end -->

				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">

				<!-- Logo
				============================================= -->
                <?php if($logo) { ?>
          
          <div id="logo" class="logo_replace"> <a href="/" class="standard-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> <a href="/" class="retina-logo" data-dark-logo="/users<?=$logo?>"> <img src="/users<?=$logo?>" alt=""></a> </div>
          <? } else { ?>
          <div id="logo" class="logo_replace">
              <a href="/" class="standard-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a>
              <a href="/" class="retina-logo" data-dark-logo="/hubs/themes/universal2/images/universal-logo.png"> <img src="/hubs/themes/universal2/images/universal-logo.png" alt=""></a> </div>
          <? } ?>

				<ul class="header-extras">
					<li>
						<i class="i-plain icon-call nomargin"></i>
						<div class="he-text">
							Call Us
							<span>(555) 555-5555</span>
						</div>
					</li>
					<li>
						<i class="i-plain icon-line2-envelope nomargin"></i>
						<div class="he-text">
							Email Us
							<span>info@anything.com</span>
						</div>
					</li>
					<li>
						<i class="i-plain icon-line-clock nomargin"></i>
						<div class="he-text">
							We'are Open
							<span>Mon - Sat, 9AM to 6PM</span>
						</div>
					</li>
				</ul>

			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="1nav_replace style-2 center">
					<div class="container clearfix">
						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
						<ul>
							<li class="current"><a href="/">Home</a></li>
							  <?
                                $customWraps = array('outer1'=>'<ul>', 'outer2'=>'</ul>',
                                    'inner1'=>'<li>', 'inner2'=>'</li>', 
                                          'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
                                          'aClassActive'=>'currentPage',
                                          'liClassActive'=>'selected');
                    
                                foreach($hub_pages as $key=>$value)
                                { 
                                        $hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
                                }
                            ?>
						</ul>
					</div>
				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->

		<section id="slider" class="slider-parallax swiper_wrapper clearfix" style="height: 550px;">

			<div class="slider_replace swiper-container swiper-parent">
    <div class="swiper-wrapper">
      <?php if($slides){ ?>
          <?php foreach($slides as $photo) { ?>
          <div id="slide<?php echo ($c+0);?>" class="swiper-slide dark" style="background-image: url('/users<?=$photo->background?>');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle<?php echo ($c+0);?>" data-caption-animate="fadeInUp">
                  <?=$photo->title?>
                </h2>
                <p id="slideDesc<?php echo ($c+0);?>" data-caption-animate="fadeInUp" data-caption-delay="200">
                  <?=$photo->description?>
                </p>
              </div>
            </div>
          </div>
          <?php $c++;?>
          <?php } ?>
          <?php } else { ?>
          <div id="slide1" class="swiper-slide dark" style="background-image: url('/hubs/themes/universal/images/slider/full/1.jpg');">
            <div class="container clearfix">
              <div class="slider-caption slider-caption-center">
                <h2 id="slideTitle1" data-caption-animate="fadeInUp">Welcome to Universal</h2>
                <p id="slideDesc1" data-caption-animate="fadeInUp" data-caption-delay="200">Create just what you need for your Responsive Website. Choose from a wide range of Content Block and &amp; simply put them on your own Canvas.</p>
              </div>
            </div>
          </div>
          <?php } ?>
    </div>
    <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
    <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
    <div id="slide-number">
      <div id="slide-number-current"></div>
      <span>/</span>
      <div id="slide-number-total"></div>
    </div>
  </div>
			

		</section>
    
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>
