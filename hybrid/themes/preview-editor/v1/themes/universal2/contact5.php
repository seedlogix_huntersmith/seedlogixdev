<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-contact5-<?=$height?>-<?php echo $divorder;?>">
<!-- Contact Form & Map Overlay Section
        ============================================= -->
        <section id="map-overlay">

            <div class="container clearfix">

                <!-- Contact Form Overlay
                ============================================= -->
                <div id="contact-form-overlay" class="clearfix">

                    <div class="fancy-title title-dotted-border">
                        <h3>Send us an Email</h3>
                    </div>

                    ****Add form tag here***

                    <div class="line"></div>

                    <!-- Contact Info
                    ============================================= -->
                    <div class="col_one_third nobottommargin">

                        <address>
                                <strong>Headquarters:</strong><br>
                            123 Any Street, Suite 600<br>
                            Anytown, US 55555<br>
                            </address>
                            <abbr title="Phone Number"><strong>Phone:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Fax"><strong>Fax:</strong></abbr> (555) 555-5555<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> nothing@nothing.com

                    </div><!-- Contact Info End -->

                   

                </div><!-- Contact Form Overlay End -->

            </div>

            <!-- Google Map
            ============================================= -->
            <section id="google-map" class="gmap"></section>

            </script><!-- Google Map End -->

        </section><!-- Contact Form & Map Overlay Section End -->
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>