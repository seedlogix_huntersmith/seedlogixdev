<?
//COMMON HEADER
include('inc/header.php');
?>
    <div id="wrapper" class="clearfix">

        <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
            echo $divparse;

        } else {

            ?>
            <div id="block-content18-<?=$height?>-<?php echo $divorder;?>">
                <!-- Content
                      ============================================= -->
                <section id="content">
                    <div class="content-wrap">
                        <!-- ADD CONTENT BLOCK CODE HERE -->


                        <div class="container clearfix">
                            <div class="col_one_third nobottommargin">
                                <div class="feature-box media-box">
                                    <div class="fbox-media">
                                        <img src="/hubs/themes/universal/images/services/1.jpg" alt="Why choose Us?">
                                    </div>
                                    <div class="fbox-desc">
                                        <h3>Why choose Us.<span class="subtitle">Because we are Reliable.</span></h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi rem, facilis nobis voluptatum est voluptatem accusamus molestiae eaque perspiciatis mollitia.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col_one_third nobottommargin">
                                <div class="feature-box media-box">
                                    <div class="fbox-media">
                                        <img src="/hubs/themes/universal/images/services/2.jpg" alt="Why choose Us?">
                                    </div>
                                    <div class="fbox-desc">
                                        <h3>Our Mission.<span class="subtitle">To Redefine your Brand.</span></h3>
                                        <p>Quos, non, esse eligendi ab accusantium voluptatem. Maxime eligendi beatae, atque tempora ullam. Vitae delectus quia, consequuntur rerum molestias quo.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col_one_third nobottommargin col_last">
                                <div class="feature-box media-box">
                                    <div class="fbox-media">
                                        <img src="/hubs/themes/universal/images/services/3.jpg" alt="Why choose Us?">
                                    </div>
                                    <div class="fbox-desc">
                                        <h3>What we Do.<span class="subtitle">Make our Customers Happy.</span></h3>
                                        <p>Porro repellat vero sapiente amet vitae quibusdam necessitatibus consectetur, labore totam. Accusamus perspiciatis asperiores labore esse ab accusantium ea modi ut.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ADD CONTENT BLOCK CODE HERE -->
                    </div>
                </section>
            </div>
        <? } ?>
    </div>
<?
//COMMON FOOTER
include('inc/footer.php');
?>