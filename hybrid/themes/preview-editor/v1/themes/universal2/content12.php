<?
	//COMMON HEADER
	include('inc/header.php');
?>
<div id="wrapper" class="clearfix">

<?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
    echo $divparse;

} else {

    ?>
<div id="block-content12-<?=$height?>-<?php echo $divorder;?>">
  <!-- Content
        ============================================= -->
  <section id="content">
      <!-- ADD CONTENT BLOCK CODE HERE -->


          <div class="container clearfix">

              <div class="clear bottommargin-lg"></div>

              <div id="section-features" class="heading-block title-center page-section">
                  <h2>Sit ei tota tantas essent.</h2>
                  <span>Affert eirmod suavitate et vel, ornatus pericula eam ei</span>
              </div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/responsive.png" alt="Responsive Layout"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/retina.png" alt="Retina Graphics"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third col_last">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/performance.png" alt="Powerful Performance"></a>
                      </div>
                     <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="clear"></div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="600">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/flag.png" alt="Responsive Layout"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/tick.png" alt="Retina Graphics"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third col_last">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="1000">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/tools.png" alt="Powerful Performance"></a>
                      </div>
                     <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="clear"></div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="1200">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/map.png" alt="Responsive Layout"></a>
                      </div>
                     <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="1400">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/seo.png" alt="Retina Graphics"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>

              <div class="col_one_third col_last">
                  <div class="feature-box fbox-plain">
                      <div class="fbox-icon" data-animate="bounceIn" data-delay="1600">
                          <a href="#"><img src="/hubs/themes/universal2/images/icons/features/support.png" alt="Powerful Performance"></a>
                      </div>
                      <h3>Doctus Deterruisset</h3>
                      <p>Prima choro numquam his in, cibo insolens mea no. Eu percipit maluisset urbanitas sea, nulla noluisse mea et, no debet.</p>
                  </div>
              </div>
              </div>
              
              <div class="clear"></div>
      <!-- ADD CONTENT BLOCK CODE HERE -->
  </section>
</div>
<? } ?>
</div>
<?
			//COMMON FOOTER
			include('inc/footer.php');
		?>