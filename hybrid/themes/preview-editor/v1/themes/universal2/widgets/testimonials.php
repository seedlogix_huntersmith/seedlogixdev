<div class="widget clearfix" id="editContent20">

    <h4>Testimonials</h4>
    <div class="fslider testimonial noborder nopadding noshadow" data-animation="slide" data-arrows="false">
        <div class="flexslider">
            <div class="slider-wrap">
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/2.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/1.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/2.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="testi-image">
                        <a href="#"><img src="/hubs/themes/universal/images/testimonials/1.jpg" alt="Customer Testimonails"></a>
                    </div>
                    <div class="testi-content">
                        <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
                        <div class="testi-meta">
                            Steve Jobs
                            <span>Apple Inc.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>