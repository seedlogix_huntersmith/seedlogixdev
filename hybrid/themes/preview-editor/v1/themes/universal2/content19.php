<?
//COMMON HEADER
include('inc/header.php');
?>
    <div id="wrapper" class="clearfix">

        <?php if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)){
            echo $divparse;

        } else {

            ?>
            <div id="block-content19-<?=$height?>-<?php echo $divorder;?>">
    <section id="content">

    <div class="content-wrap">
 <div class="container clear-bottommargin clearfix">

                        <div class="heading-block center">
                            <h2>Features List.</h2>
                            <span>Eos at alii salutatus dissentias, adolescens deterruisset</span>
                        </div>

                        <div class="row">

                            <div class="col-md-4 bottommargin">
                                <img data-animate="fadeInLeftBig" src="/hubs/themes/universal/images/services/iphone7.png" alt="iPhone" class="center-block">
                            </div>

                            <div class="col-md-4 col-sm-6 topmargin bottommargin">

                                <div class="col_full">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-monitor"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                                <div class="col_full">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-eye"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                                <div class="col_full nobottommargin">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-power"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-4 col-sm-6 topmargin bottommargin">

                                <div class="col_full">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-layers"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                                <div class="col_full">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-star"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                                <div class="col_full nobottommargin">

                                    <div class="feature-box fbox-plain fbox-small fbox-dark">
                                        <div class="fbox-icon">
                                            <a href="#"><i class="icon-line-anchor"></i></a>
                                        </div>
                                        <h3>Mel no aliquip tritani</h3>
                                        <p>Odio dignissim cu mel, inermis perfecto urbanitas cu usu. Est atqui mazim platonem ut, ceteros accommodare ex est, veniam animal duo ut.</p>
                                    </div>

                                </div>

                            </div>

                        </div>

                </div>
        </div>
        </section>
            </div>
        <? } ?>
    </div>
<?
//COMMON FOOTER
include('inc/footer.php');
?>