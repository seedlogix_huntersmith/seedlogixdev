<?
    //ENGINE HEADER
    include('inc/header.php');

	//THEME HEADER
    include(QUBEROOT . 'hubs/themes/'.$info['path'].'/cms/header.php');

?>
<!-- Document Wrapper
    ============================================= -->

<div id="wrapper" class="site_wrapper">
    <?
    //THEME PARSER
    include('inc/parse.php');
    ?>
</div>

<?
//THEME FOOTER
include(QUBEROOT . 'hubs/themes/'.$info['path'].'/cms/footer.php');
?>
