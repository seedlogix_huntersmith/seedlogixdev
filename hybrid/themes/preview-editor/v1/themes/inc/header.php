<?php
/**
 * Created by PhpStorm.
 * User: Jon
 * Date: 11/4/2015
 * Time: 3:22 PM
 */

$divorder = $_GET['divorder'];
$height = $_GET['height'];
$sandbox = $_GET['sandbox'];
$sbx = $_GET['sbx'];

if($_GET['siteid'] || $_GET['blogid']) {
	//require_once '/home/sapphire/production-code/sofus-unstable/hybrid/bootstrap.php';
	require_once '../../../../bootstrap.php';

    @session_start();

    Qube::ForceMaster();

    $divid = $_GET['divid'];
    $siteid = $_GET['siteid'];
    $blogid = $_GET['blogid'];
    $postid = $_GET['postid'];
    $themeid = $_GET['themeid'];
    $pageid = $_GET['pageid'];
    $column = $_GET['column'];
    ///theme and website info
    if($pageid) {
        $stmt = Qube::GetPDO()->prepare('select b.overview, b.logo, b.links_color, b.bg_color, b.company_name, b.street, b.city, b.state, b.zip, r.path, p.page_region, p.page_edit_2, p.page_edit_3, p.page_title from hub b, themes r, hub_page2legacy p where
	b.id = "' . $siteid . '" AND r.id = "' . $themeid . '" AND p.id = "' . $pageid . '"');
        $stmt->execute();
        $info = $stmt->fetch(PDO::FETCH_ASSOC);
        $content = $info[$column];

    } else if($postid) {
        $stmt = Qube::GetPDO()->prepare('select b.logo, b.links_color, b.bg_color, b.connect_hub_id, r.path, p.post_edit_1, p.post_title from blogs b, themes r, blog_post p where
	b.id = "' . $blogid . '" AND r.id = "' . $themeid . '" AND p.id = "' . $postid . '"');
        $stmt->execute();
        $info = $stmt->fetch(PDO::FETCH_ASSOC);
        $content = $info[$column];

    } else if($blogid) {
        $stmt = Qube::GetPDO()->prepare('select b.blog_edit_1, b.logo, b.blog_edit_2, b.blog_edit_3, b.links_color, b.bg_color, b.connect_hub_id, b.blog_title, r.path from blogs b, themes r where
	b.id = "' . $blogid . '" AND r.id = "' . $themeid . '"');
        $stmt->execute();
        $info = $stmt->fetch(PDO::FETCH_ASSOC);
        $content = $info[$column];

    } else {
        $stmt = Qube::GetPDO()->prepare('select b.overview, b.offerings, b.photos, b.logo, b.links_color, b.bg_color, b.street, b.city, b.state, b.zip, r.path from hub b, themes r where
	b.id = "' . $siteid . '" AND r.id = "' . $themeid . '"');
        $stmt->execute();
        $info = $stmt->fetch(PDO::FETCH_ASSOC);
        $content = $info[$column];
    }
    //var_dump($info);

    if($siteid) {
        ///slides info
        $stmt2 = Qube::GetPDO()->prepare('select user_id, title, background, url, description from slides where
	hub_id = "' . $siteid . '" AND deleted = 0');
        $stmt2->execute();
        $slides = $stmt2->fetchAll(PDO::FETCH_OBJ);
        $c = 1;
    }



//preg_match_all('/<div id=\"[$divid]\">(.*?)<\/div>/s', $info['overview'], $code);
	include('simple_html_dom.php');
    //include('/home/sapphire/production-code/sofus-unstable/hybrid/themes/preview-editor/v1/themes/inc/simple_html_dom.php');
    //$divparse = $html->find('div[id="' . $divid . '"]', 0);
    //$div = $divparse->innertext; // Some Content

    if($blogid && $info['connect_hub_id']){
        require_once QUBEADMIN . 'inc/hub.class.php';
        $hub = new Hub();

        $hub_pages = $hub->getHubPage($info['connect_hub_id'], NULL, TRUE, TRUE, TRUE);

    } else if($siteid) {
        require_once QUBEADMIN . 'inc/hub.class.php';
        $hub = new Hub();

        $hub_pages = $hub->getHubPage($siteid, NULL, TRUE, TRUE, TRUE);
    }

    $logo = $info['logo'];
    if($sbx==1) $sbx = '-sandbox';

    if($content){

        $replaceValues = array(
            'navtag'        => '',
            'navtag1'       => '',
            'slider'        => '',
            'logo'          => '',
            'posts1'        => '',
            'post1'         => '',
            'category1'     => '',
            'pposts1'       => '',
            'btitle1'       => '',
            'btitle2'       => '',
            'ptitle1'       => ''
        );

        if(preg_match('/\[\[(.*)\]\]/i',$content)){// regex to check if this field has tag(s)
            $tags_path = QUBEROOT.'hubs/themes/'.$info['path'].'/cms/tags/';
            foreach($replaceValues as $replaceKeyword => $replaceValue){
                if($replaceKeyword == 'navtag' || $replaceKeyword == 'navtag1' || $replaceKeyword == 'slider' || $replaceKeyword == 'logo' || $replaceKeyword == 'posts1' || $replaceKeyword == 'post1' || $replaceKeyword == 'category1' || $replaceKeyword == 'pposts1' || $replaceKeyword == 'btitle1' || $replaceKeyword == 'btitle2' || $replaceKeyword == 'ptitle1'){
                    if($replaceKeyword == 'navtag' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'navtag1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'slider' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'logo' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'posts1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'post1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'category1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'pposts1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'btitle1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'btitle2' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    if($replaceKeyword == 'ptitle1' && file_exists($tags_path.$replaceKeyword.'.php')){
                        include($tags_path.$replaceKeyword.'.php');
                    }
                    $content = str_replace('[['.$replaceKeyword.']]',$replaceValue,$content);
                } else{
                    $content = str_replace('[['.$replaceKeyword.']]',$replaceValue,$content);
                    unset($replaceValue);
                }
            }
        }

        $html = str_get_html($content);

    }
    else $html = str_get_html(' ');

    //var_dump($html);

    $color = $info['links_color'];
    $color = str_replace('#', '', $color);

} else echo 'no access';
?>