<?php
/**
 * Created by PhpStorm.
 * User: Jon
 * Date: 11/4/2015
 * Time: 3:38 PM
 */

if($divparse = $html->find('div[id="block-' . $divid . '-' . $height . '-' . $divorder . '"]', 0)->outertext){

    echo $divparse;

} else {

?>

    <!--DO NOT REMOVE THIS DIV-->
    <div id="block-<?=$divid?>-<?=$height?>-<?php echo $divorder;?>">
        <!--REPLACEABLE CONTENT BELOW-->

        <?
        //TEMPLATE INCLUDE
        include(QUBEROOT . 'hubs/themes/'.$info['path'].'/blocks/'.$divid.'.php');
        ?>

        <!--REPLACEABLE CONTENT ABOVE-->
    </div>
    <!--DO NOT REMOVE THIS DIV-->


<? } ?>
