<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!--<![endif]-->
<!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Stylesheets -->
    <link rel="stylesheet" media="screen" href="/hubs/themes/flex/law/js/bootstrap/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/js/mainmenu/menu.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/css/default.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/css/layouts.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/css/shortcodes.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="/hubs/themes/flex/law/css/responsive-leyouts.css" type="text/css" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" type="text/css" href="/hubs/themes/flex/law/js/cubeportfolio/cubeportfolio.min.css">
    <link rel="stylesheet" type="text/css" href="/hubs/themes/flex/law/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
    <link rel="stylesheet" href="/hubs/themes/flex/law/css/et-line-font/et-line-font.css">
    <link href="/hubs/themes/flex/law/js/owl-carousel/owl.carousel.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/hubs/themes/flex/law/js/tabs/assets/css/responsive-tabs.css">

    <link rel="stylesheet" type="text/css" href="/hubs/themes/flex/law/js/smart-forms/smart-forms.css">
    <link rel="stylesheet" href="/themes/preview-editor/v1/themes/flex/law/css/colors.php?links_color=<?=$color?>" type="text/css" />


</head>

<body>