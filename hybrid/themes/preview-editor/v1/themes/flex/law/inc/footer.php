
<!-- #wrapper end -->
<?php if(!$sandbox==1){ ?>
<!-- ============ JS FILES ============ -->
<script type="text/javascript" src="/hubs/themes/flex/law/js/universal/jquery.js"></script>
<script src="/hubs/themes/flex/law/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/hubs/themes/flex/law/js/masterslider/jquery.easing.min.js"></script>
<script src="/hubs/themes/flex/law/js/masterslider/masterslider.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";
	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	slider.setup('masterslider' , {
		 width:1600,    // slider standard width
		 height:630,   // slider standard height
		 space:0,
		 speed:45,
		 layout:'fullwidth',
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"parallaxMask"
	});

})(jQuery);
</script>
<script src="/hubs/themes/flex/law/js/mainmenu/customeUI.js"></script>
<script src="/hubs/themes/flex/law/js/owl-carousel/owl.carousel.js"></script>
<script src="/hubs/themes/flex/law/js/owl-carousel/custom.js"></script>
<script src="/hubs/themes/flex/law/js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/tabs/smk-accordion.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/tabs/custom.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/cubeportfolio/main-masonry2.js"></script>
<script src="/hubs/themes/flex/law/js/scrolltotop/totop.js"></script>
<script src="/hubs/themes/flex/law/js/mainmenu/jquery.sticky.js"></script>

<script type="text/javascript" src="/hubs/themes/flex/law/js/smart-forms/jquery.form.min.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/smart-forms/jquery.validate.min.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/smart-forms/additional-methods.min.js"></script>
<script type="text/javascript" src="/hubs/themes/flex/law/js/smart-forms/smart-form.js"></script>
<script src="/hubs/themes/flex/law/js/scripts/functions.js" type="text/javascript"></script>
<? } ?>
</body>
</html>