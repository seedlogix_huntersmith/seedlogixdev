<?php
/**
 * Created by PhpStorm.
 * User: Jon
 * Date: 11/4/2015
 * Time: 4:09 PM
 */
$replaceValues = array('navtag'=>'', 'navtag1'=>'', 'slider'=>'', 'logo'=>'', 'posts1'=>'', 'post1'=>'', 'category1'=>'');
// DO IT:

if(preg_match('/\[\[(.*)\]\]/i', $content)){ //regex to check if this field has tag(s)
    foreach($replaceValues as $replaceKeyword=>$replaceValue){
        if($replaceKeyword=='navtag' || $replaceKeyword=='navtag1' || $replaceKeyword=='slider' || $replaceKeyword=='logo' || $replaceKeyword=='posts1' || $replaceKeyword=='post1' || $replaceKeyword=='category1'){

            if($replaceKeyword=='navtag'){
                if($hub_pages){

                    $replaceValue .= '<div id="navbar-collapse-grid" class="nav_replace navbar-collapse collapse pull-right">
													<ul class="nav stone navbar-nav">
                                                    <li><a href="/" class="dropdown-toggle">Home</a></li>';
                    //for instructions on customizing elements (ul, li, a) see http://6qube.com/admin/pagesv2-customwraps.htm
                    $customWraps = array('outer1'=>'<ul class="dropdown-menu five" role="menu">', 'outer2'=>'</ul>',
                        'inner1'=>'<li class="dropdown" >', 'inner2'=>'</li>',
                        'inner1Sub'=>'<li>', 'inner2Sub'=>'</li>',
                        'aClassActive'=>'currentPage',
                        'liClassActive'=>'selected');

                    foreach($hub_pages as $key=>$value)
                    {
                        ob_start();
                        $replaceValue .= $hub->dispV2Nav($value, NULL, $thisPage, $customWraps);
                        $replaceValue .= ob_get_clean();
                    }
                    $replaceValue .= '</ul>
          										</div>';

                } else {

                    $replaceValue .= '<div id="navbar-collapse-grid" class="nav_replace navbar-collapse collapse pull-right">
													<ul class="nav stone navbar-nav">
                                                    <li><a href="/" class="dropdown-toggle">Home</a></li>
                                                    </ul>
          										</div>';
                }
            }
            if($replaceKeyword=='navtag1'){
                if($hub_pages){



                }
            }
            if($replaceKeyword=='slider'){
                if($slides){
                    $replaceValue .= '<div class="slider_replace master-slider ms-skin-default" id="masterslider">';
                    foreach($slides as $photo) {
                        $counter = ($c+0);
                        $replaceValue .= '<div class="ms-slide slide-'.$counter.'" data-delay="9">
                                                        <div class="slide-pattern"></div>

                                                        <img src="/hubs/themes/flex/law/js/masterslider/blank.gif" data-src="/users/'.$photo->background.'" alt=""/>

                                                        <h3 class="ms-layer text35 text-center"
                                                            style="top: 230px;"
                                                            data-type="text"
                                                            data-delay="500"
                                                            data-ease="easeOutExpo"
                                                            data-duration="1230"
                                                            data-effect="top(250)"> '.$photo->title.'</h3>

                                                        <h3 class="ms-layer text36 text-center"
                                                            style="top: 300px;"
                                                            data-type="text"
                                                            data-delay="1000"
                                                            data-ease="easeOutExpo"
                                                            data-duration="1230"
                                                            data-effect="top(250)"> '.$photo->description.'</h3>';

                        if($photo->url){
                            $replaceValue .= '<a href="'.$photo->url.'" class="ms-layer sbut14"
                                                           style="left: 730px; top: 360px;"
                                                           data-type="text"
                                                           data-delay="1500"
                                                           data-ease="easeOutExpo"
                                                           data-duration="1200"
                                                           data-effect="top(250)"> Read more </a>';
                        }
                        $replaceValue .= '</div>
                                                    <!-- end slide '.$counter.' -->';
                        $c++;
                    }
                    $replaceValue .= '</div>
                                                        <!-- end of masterslider -->
                                                        <div class="clearfix"></div>';
                } else {

                    $replaceValue .= '<div class="slider_replace master-slider ms-skin-default" id="masterslider">
                                              </div>
                                                        <!-- end of masterslider -->
                                                        <div class="clearfix"></div>';
                }
            }
            if($replaceKeyword=='posts1'){

                $replaceValue = '<div id="posts" class="posts1 post-timeline clearfix">

                            <div class="timeline-border"></div>

                            <div class="entry clearfix">
                                <div class="entry-timeline">
                                    10<span>June</span>
                                    <div class="timeline-divider"></div>
                                </div>
                                <div class="entry-image">
                                    <a href="/hubs/themes/universal/images/blog/full/17.jpg" data-lightbox="image"><img class="image_fade" src="/hubs/themes/universal/images/blog/standard/17.jpg" alt="Standard Post with Image"></a>
                                </div>
                                <div class="entry-title">
                                    <h2><a href="#">This is a Standard post with a Preview Image</a></h2>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><a href="#"><i class="icon-user"></i> admin</a></li>
                                    <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                                    <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                </ul>
                                <div class="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
                                    <a href="#"class="more-link">Read More</a>
                                </div>
                            </div>


                        </div>';

            }
            if($replaceKeyword=='post1'){
                $replaceValue = '<div id="post1" class="single-post nobottommargin">

                            <!-- Single Post
                            ============================================= -->
                            <div class="entry clearfix">

                                <!-- Entry Title
                                ============================================= -->
                                <div class="entry-title">
                                    <h2>This is a Standard Post Example</h2>
                                </div><!-- .entry-title end -->

                                <!-- Entry Meta
                                ============================================= -->
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> 10th July 2015</li>
                                    <li><a href="#"><i class="icon-user"></i> admin</a></li>
                                    <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
                                    <li><a href="#"><i class="icon-camera-retro"></i></a></li>
                                </ul><!-- .entry-meta end -->

                                <!-- Entry Image
                                ============================================= -->
                                <div class="entry-image">
                                    <a href="#"><img src="/hubs/themes/universal/images/blog/full/1.jpg" alt="Blog Single"></a>
                                </div><!-- .entry-image end -->

                                <!-- Entry Content
                                ============================================= -->
                                <div class="entry-content notopmargin">

                                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. <a href="#">Curabitur blandit tempus porttitor</a>. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>

                                    <blockquote><p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper.</p></blockquote>


                                    <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>
                                    <!-- Post Single - Content End -->

                                    <!-- Tag Cloud
                                    ============================================= -->
                                    <div class="tagcloud clearfix bottommargin">
                                        <a href="#">general</a>
                                        <a href="#">information</a>
                                        <a href="#">media</a>
                                        <a href="#">press</a>
                                        <a href="#">gallery</a>
                                        <a href="#">illustration</a>
                                    </div><!-- .tagcloud end -->

                                    <div class="clear"></div>

                                    <!-- Post Single - Share
                                    ============================================= -->
                                    <div class="si-share noborder clearfix">
                                        <span>Share this Post:</span>
                                        <div>
                                            <a href="#" class="social-icon si-borderless si-facebook">
                                                <i class="icon-facebook"></i>
                                                <i class="icon-facebook"></i>
                                            </a>
                                            <a href="#" class="social-icon si-borderless si-twitter">
                                                <i class="icon-twitter"></i>
                                                <i class="icon-twitter"></i>
                                            </a>
                                            <a href="#" class="social-icon si-borderless si-pinterest">
                                                <i class="icon-pinterest"></i>
                                                <i class="icon-pinterest"></i>
                                            </a>
                                            <a href="#" class="social-icon si-borderless si-gplus">
                                                <i class="icon-gplus"></i>
                                                <i class="icon-gplus"></i>
                                            </a>
                                            <a href="#" class="social-icon si-borderless si-rss">
                                                <i class="icon-rss"></i>
                                                <i class="icon-rss"></i>
                                            </a>
                                            <a href="#" class="social-icon si-borderless si-email3">
                                                <i class="icon-email3"></i>
                                                <i class="icon-email3"></i>
                                            </a>
                                        </div>
                                    </div><!--- Post Single - Share End -->

                                </div>
                            </div><!-- .entry end -->


                        </div>';
            }
            if($replaceKeyword=='logo'){

                if($logo){
                    $replaceValue = '<div class="logo_replace"><a href="/" class="navbar-brand"><img src="/users'.$logo.'" alt=""/></a></div>';
                } else {
                    $replaceValue = '<div class="logo_replace"><a href="/" class="navbar-brand"><img src="/hubs/themes/flex/law/images/logo.png" alt=""/></a></div>';
                }

            }
            if($replaceKeyword=='category1'){

                $replaceValue = '<ul id="pcategories">
                                    <li><a href="#"><div>Category 1</div></a></li>
                                    <li><a href="#"><div>Category 2</div></a></li>
                                    <li><a href="#"><div>Category 3</div></a></li>
                                </ul>';

            }
            $content = str_replace('[['.$replaceKeyword.']]', $replaceValue, $content);

        } else {
            $content = str_replace('[['.$replaceKeyword.']]', $replaceValue, $content);
            unset($replaceValue);
        }

    }

} //end if(preg_match('/\(\((.*)\)\)/i', $row[$searchField]))

$html = str_get_html($content);