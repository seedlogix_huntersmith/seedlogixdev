<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

define('HYBRID_PATH', realpath(dirname(__FILE__) . '/../..') . '/');
require_once HYBRID_PATH . 'bootstrap.php';

require_once 'HubPage.php';


    class HubSmarty extends QubeSmarty {
        
        static function getTemplatePath()
        {
            return QUBEROOT . 'smarty/hubs';    
        }
        
        function __construct() {
            parent::__construct();

            $this->setTemplateDir(self::getTemplatePath());
        }  
    }
    
    class InlineHubPreview extends HubSmarty
    {
        static function getCDNPath()
        {
            return QUBEROOT . 'smarty/cdn/hubs';
        }
        
        function __construct($cdn_url   =   'http://cdn.hybridui'){
            parent::__construct();
            
            $this->assign('theme_cdn_url', $cdn_url . '/hubs/corporate'); // NO TRAILING SLASH!
            $this->assign('cdn_url', $cdn_url); // NO TRAILING SLASH!
            $this->assign('hub', HubPage::LoadPage(1));
        }        
    }