<?php
    Header('Content-Type: text/css');

    $topHeaderHeight = '55';
    $subHeaderHeight = '25';
    $headerHeight = $topHeaderHeight + $subHeaderHeight;
    $footerHeight = '250';
    $leftWidth = '60';
    $rightWidth = '300';
?>

* {margin:0;padding:0;}
body#hubeditor {
			overflow: hidden
		}
#hubeditor 		#header {
			position: absolute;
			top:0;
			left:0;
			right:0;
			height:<?=$topHeaderHeight?>px;
		}
#hubeditor 		#subHeader {
			position: absolute;
			top:<?=$topHeaderHeight?>px;
			left:0;
			right:0;
			height:25px;
			z-index: 205;
		}
#hubeditor 		#leftSideBar {
			position: absolute;
			top:<?=$headerHeight?>px;
			left:0;
			bottom:0;
			width:<?=$leftWidth?>px;
			z-index: 200;
		}
#hubeditor 		#rightSideBar {
			position: absolute;
			top:<?=$headerHeight?>px;
			right:0;
			bottom:0;
			width:<?=$rightWidth-15?>px;
		}
#hubeditor 		#wysiwyg {
                        width: 100%;
			position: absolute;
			height:<?=$footerHeight?>px;
			left:<?=$leftWidth?>px;
			right:0;
			bottom:0;			
		}
		
#hubeditor 		.templateArea {
			position: absolute;
			top: <?=$headerHeight?>px;
			left: <?=$leftWidth?>px;
			bottom: 0px;
			right: <?= 0 ; //$rightWidth+4?>px;
			background-color: #ccc;	
                        overflow: scroll;
		}
		
#hubeditor 		#switchThemesCnt  {
			position: absolute;
			top:<?=$topHeaderHeight?>px;
			left:0;
			right:0px;
		}