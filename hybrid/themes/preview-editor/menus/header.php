<div class="logo"><img src="images/logo.png" alt="ignite-cloud" /></div>
<h1 class="">Sample Site</h1>


<ul class="right">
	<li class="leftLI">
		<a href="#" id="showThemes" class="leftNavHyperlink">
			<img src="http://pastephp.com/aquin/images/icons/mainnav/tables.png" />
			<span class="leftSideSpan">Themes</span>
		</a>
	</li>
	<li class="leftLI">
		<a href="#" class="leftNavHyperlink" id="showMeta">
			<div class="topNavIcon iconMeta"></div>
			<span class="leftSideSpan">Page Meta</span>
		</a>
	</li>	
	<li class="leftLI">
		<a href="#" class="leftNavHyperlink" id="showScripts">
			<div class="topNavIcon iconScript"></div>
			<span class="leftSideSpan">Scripts</span>
		</a>
	</li>
	<li class="leftLI">
		<a href="#" id="showCss" class="leftNavHyperlink">
			<div class="topNavIcon iconCss"></div>
			<span class="leftSideSpan">CSS</span>
		</a>
	</li>
	<li class="leftLI">
		<a href="#" id="save" class="leftNavHyperlink">
			<div class="topNavIcon iconSave"></div>
			<span class="leftSideSpan">Save</span>
		</a>
	</li>
</ul>

<div class="mode crumb right">
	<a class="crumb-link current" href="#">Edit</a>
	<a class="crumb-link" href="#">Preview</a>
</div>

