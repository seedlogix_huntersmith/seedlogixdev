<ul class="dragNDrop">
	<li>
		<a href="#" class="dragIcon iconText">
			<span class="iconDesc">Text</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconBox">
			<span class="iconDesc">Box</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconButton">
			<span class="iconDesc">Button</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconForm">
			<span class="iconDesc">Form</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconHtml">
			<span class="iconDesc">HTML</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconImage">
			<span class="iconDesc">Image</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconPageSection">
			<span class="iconDesc">Page&nbsp;Section</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconSocial">
			<span class="iconDesc">Social</span>
		</a>
	</li>
	<li>
		<a href="#" class="dragIcon iconVideo">
			<span class="iconDesc">Video</span>
		</a>
	</li>
</ul>