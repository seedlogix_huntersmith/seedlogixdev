<ul id="templateNavigation">
	<li>
		<a href="#" class="selected">home</a>
	</li>
	<li class="hasSubMenu">
		<a href="#">About Corporate</a>
		<ul class="sub-menu">
			<li><a href="#">About the Theme</a></li>
		</ul>
	</li>
	<li class="hasSubMenu">
		<a href="#">Theme styles</a>
		<ul class="sub-menu">
			<li><a href="#">Home Page</a></li>
			<li><a href="#">Global</a></li>
			<li><a href="#">Defined</a></li>
			<li><a href="#">Pricing Table</a></li>
			<li><a href="#">Side Navigation</a></li>
			<li><a href="#">Gallery Elements</a></li>			
		</ul>
	</li>
	<li>
		<a href="#">Blog</a>
	</li>
	<li>
		<a href="#">Contact</a>
	</li>
</ul>