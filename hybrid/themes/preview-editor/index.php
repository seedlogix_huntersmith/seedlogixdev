<?php

    require_once 'inlinehubpreview.php';
    $cdnhost   =   'http://' . preg_replace('/^(www\.|qube\.)?(.*)$/', 'cdn.$2', $_SERVER['HTTP_HOST']);

    $inline =   new InlineHubPreview($cdnhost);
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Preview Website</title>
	<link rel="StyleSheet" href="css/preview-styles.css" type="text/css" media="screen" />
	<link rel="StyleSheet" href="jquery.cleditor.css" type="text/css" media="screen" />
	<link rel="StyleSheet" href="hubeditor.php" type="text/css" media="screen" /><!-- 
	<link rel="StyleSheet" href="css/codemirror.css" type="text/css" media="screen" /> -->
 
        <!-- load the jQuery and require.js libraries 
        
        -->

    <script type="text/javascript" src="<?php echo $cdnhost; ?>/js/aloha-0.23.0/lib/require.js"></script>         
    <script type="text/javascript">
        var CDN =   "<?echo $cdnhost; ?>";
        requirejs.config({
   paths: {
   	
//            'htmlmixed': CDN + '/js/mode/htmlmixed/htmlmixed',
        'jquery': CDN + '/js/aloha-0.23.0/lib/vendor/jquery-1.7.2',  //
//        	'jquery': 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min',
//        'bootstrap': '../bootstrap/js/bootstrap.min',
        'jq-cleditor': 'js/jquery.cleditor',
        'jq-colorpicker': 'js/jquery.colorpicker',
        'jq-scrollto': 'js/jquery.scrollto',
            'jquery-ui': 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min'
    }, 

    // Use shim for plugins that does not support AMD
    shim: {
        'jq-cleditor': ['jquery'],
        'jq-colorpicker': ['jquery'],
        'jq-scrollto': ['jquery'],
        'jquery-ui': ['jquery'],
//        'htmlmixed': ['aloha']
    }
});
        </script>
    <link rel="stylesheet"  href="<?php echo $cdnhost; ?>/editor.css" media="screen" type="text/css" />
</head>
<body   id="hubeditor">		
	<div id="switchThemesCnt">
            <?php include('inc/themesbox.php'); ?>
	</div>
		
	<div id="header">
	<?
		require_once('menus/header.php');
	?>
	</div>
	<div id="subHeader">
	<?
		require_once('menus/sub-header.php');
	?>
	</div>
	<div id="leftSideBar">
	<?
		require_once('menus/left-side-menu.php');
	?>
	</div>
	<?  //	<div id="rightSideBar">
    //		require_once('menus/right-side-menu.php');
            //</div>
	?>
	<div id="wysiwyg">
		<textarea id="editor" name="editor" rows="" cols="16">Some cool stuff here</textarea>
	</div>
	
	<div class="templateArea"   id="p-corporate">
<?php echo $inline->fetch('corporate-theme-editor.html'); ?>
	</div>
	
	<?	//Include Modals
		require_once('modals/showMeta.php');
	?>

	
	<!-- JAVASCRIPT -->
	<!-- JQUERY latest version doesn't work with some of the plugins 
        
            <script type="text/javascript" src="http://cdn.aloha-editor.org/latest/lib/vendor/jquery-1.7.2.js"></script>
        -->
</body>
</html>

        <script src="js/main.js"></script>
        
<!-- Configure Aloha -->
<script type="text/javascript">
    Aloha = window.Aloha || {};
    Aloha.settings = {
    	/*
    	requireConfig: {
    		paths: {
				'htmlmixed': 'mode/htmlmixed/htmlmixed'    			
    			},
/*		shim: {
			htmlmixed : './codemirror'
		}, 
			
		}, */
		
		locale: 'en',
                toolbar: {
                tabs: [
                    {
                        label: 'Format',
                        components: [
                            [ 'bold', 'italic', 'underline', '\n',
                              'subscript', 'superscript', 'strikethrough' ],
                            [ 'formatBlock' ]
                        ]
                    },
                    {
                        label: 'Insert',
                        exclusive: true,
                        components: [
                            "createTable", "characterPicker", "insertLink",'sourceview'
                        ]
                    },{
                label: 'Actions',
                showOn: {
                    scope: 'Aloha.continuoustext'
                },
                components: [['htmlsource']]
            }
                ] }
	};
        
//    Aloha.settings.jQuery = Aloha.settings.jQuery || jQuery.noConflict(true);
</script>

<!-- load the Aloha Editor core and some plugins http://cdn.aloha-editor.org/latest/lib/aloha.js -->
<script src="<?php echo $cdnhost; ?>/js/aloha-0.23.0/lib/aloha-bare.min.js"
                     data-aloha-plugins="common/ui,
                                          common/format,
                                          common/list,
                                          common/link,
                                          common/highlighteditables,
                                          common/image,3rd/htmlsource"></script>

<!-- load the Aloha Editor CSS styles -->
<link href="<?php echo $cdnhost; ?>/js/aloha-0.23.0/css/aloha.css" rel="stylesheet" type="text/css" />
<link href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="css/codemirror.css" rel="stylesheet" type="text/css" />

<!-- make all elements with class="editable" editable with Aloha Editor -->
<script type="text/javascript">
       Aloha.ready( function() {
              var $ = Aloha.jQuery;
              $('.qw-text').aloha();
       });
</script>
              
<script type="text/javascript" src="<?php echo $cdnhost; ?>/editor.js"></script>
