
		<h2>Change Theme</h2>
		<p class="desc">Just click a theme to change. You can preview different themes.</p>
		<ul>
			<li><img src="themes/template01.jpg" /></li>
			<li><img src="themes/template02.jpg" /></li>
			<li><img src="themes/template03.jpg" /></li>
			<li><img src="themes/template04.jpg" /></li>
			<li><img src="themes/template05.jpg" /></li>
			<li><img src="themes/template06.jpg" /></li>
			<li><img src="themes/template07.jpg" /></li>
			<li><img src="themes/template08.jpg" /></li>
			<li><img src="themes/template09.jpg" /></li>
			<li><img src="themes/template10.jpg" /></li>
			<li><img src="themes/template11.jpg" /></li>
			<li><img src="themes/template12.jpg" /></li>
		</ul>