<?php

    require_once 'inlinehubpreview.php';
    $cdnhost   =   'http://' . preg_replace('/^(www\.|qube\.)?(.*)$/', 'cdn.$2', $_SERVER['HTTP_HOST']);

    $vars   =   new VarContainer($_POST);
    
     $value =   $vars->content;
     $object_id =   $vars->get('object-id');
     $object_name   =   $vars->get('object-name');
     $page_id       =   $vars->get('page-id');
     
     $db=   Qube::GetDB('master');
     $prepare   =   $db->prepare('REPLACE INTO page_objects (id, object_name, object_value, page_id) VALUES (?, ?, ?, ?)');
     $prepare->execute(array($object_id, $object_name, $value, $page_id));
    
     echo 'Value has been updated.';
     