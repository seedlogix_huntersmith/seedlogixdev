//jQuery is required for this script to work
$(document).ready(function(){
	$('.custom-form').die();
	$('.custom-form').submit(function(event){
		if($(this).hasClass('ajaxSubmit')) event.stopPropagation();
		var formID = $(this).attr('id');
		$('.form'+formID+' .highlightError').each(function(){
			$(this).removeClass('highlightError');
		});
		$('.form'+formID+' .checkboxDiv').each(function(){
			if($(this).hasClass('checkboxDivHighlightError')){
				$(this).removeClass('checkboxDivHighlightError');
			}
		});
		var errors = 0;
		var numOnlyErrors = 0;
		var numOnlyErrorText = '';
		$('.form'+formID+' .required').each(function(){
			if($(this).val()==''){
				$(this).addClass('highlightError');
				errors += 1;
			}
			else if($(this).hasClass('checkbox')){
				if(!$(this).attr('checked')){
					$(this).parent().addClass('checkboxDivHighlightError');
					errors += 1;
				}
			}
		});
		$('.form'+formID+' .numsOnly').each(function(){
			var val = $(this).attr('value');
			if(val && isNaN(val)){
				var fieldName = $(this).parent().find('label').html();
				numOnlyErrors += 1;
				numOnlyErrorText = numOnlyErrorText+fieldName+' may only contain numbers\n';
			}
		});
		$('.form'+formID+' .email').each(function(){
			var val = $(this).attr('value');
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(val && !filter.test(val)) {
				var fieldName = $(this).parent().find('label').html();
				numOnlyErrors += 1;
				numOnlyErrorText = 'Please enter a valid email address\n';
			}
		});
		if(errors>0 || numOnlyErrors>0){
			if(errors>0) alert('Please complete the highlighted required fields');
			if(numOnlyErrorText) alert(numOnlyErrorText);
			return false;
		}
		else if($(this).hasClass('ajaxSubmit')){
			var submitURL = $(this).attr('action');
			var params = $(this).serialize();
			$.post(submitURL, params, function(data){
				if(data.success==1){
					if(!data.redir) alert('Your response was successfully sent');
					else window.location = data.redir;
				}
				else {
					alert('Sorry, there was an error submitting your form.  Please try again.');
				}
			}, 'json');
			return false;
		}
	});
	
	$('.custom-form #reset').live('click', function(){
		var formID = $(this).parent().parent().parent().parent().attr('id');
		if(confirm('Are you sure you want to clear the entire form?')){
			$('.form'+formID+' input').each(function(){
				if($(this).attr('id')!='submit' && $(this).attr('id')!='reset'){
					$(this).attr('value', '');
					if($(this).hasClass('checkbox')){
						$(this).attr('checked', false);
					}
				}
			});
			$('.form'+formID+' textarea').each(function(){
				$(this).val('');
			});
		}
		return false;
	});
});