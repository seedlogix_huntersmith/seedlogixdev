<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

#$_SERVER['QUBECONFIG']	=	'qube.pastephp.com';
require_once 'bootstrap.php';
require_once QUBEADMIN . 'library/Popcorn/BaseController.php';
require_once HYBRID_PATH . 'controllers/SecureController.php';

Qube::ForceMaster();

/* 
 * 1. Load the Controller.
 * 
 */
/*
if(!Qube::IS_DEV()){
    header('Location: /');
    exit;
}
*/

HybridRPC::RunServer();
