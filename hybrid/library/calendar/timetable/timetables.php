<?php
chdir(dirname(__FILE__));

require '../../../bootstrap.php';
@session_start();

Qube::ForceMaster();

$user_id = $_GET['userid'];

$TQ     =   Qube::Start()->queryObjects('TasksModel', 'T', 'T.user_id = %d', $user_id)->calc_found_rows();
$TQ->Where('T.status != "Completed" AND trashed = 0');
$alltasks  =   $TQ->Exec()->fetchAll();

// Fetch tha data from the database 
$timetables = array();
//while ($row = mysql_fetch_array($result)) 
foreach($alltasks as $task){
	$today = date('Y-m-d');
	$datetime1 = new DateTime($today);
	$datetime2 = new DateTime($task->due_date);
	$interval = $datetime1->diff($datetime2);
	$diff = $interval->format('%a');
	//var_dump($diff);
	if($diff == '0' || $datetime2 < $datetime1){
		$class = 4;
	} 
	else if($diff == '1' || $diff <= '3'){
		$class = 3;
	} else $class = 1;
	
	if($task->time){
		if($task->time >= 12) $time = $task->time.' PM';
		else $time = $task->time.' AM';
	} else $time = '08:30 AM';
	
	if($task->account_id){
		$Account = AccountsModel::Fetch('id = %d', $task->account_id);
		$alink = 'admin.php?aid='.$Account->id.'&ID='.$Account->cid.'&action=account&ctrl=Campaign';
	}
	
	if($task->contact_id){
		$Contact = ContactModel::Fetch('id = %d', $task->contact_id);
		$clink = 'admin.php?pid='.$Contact->id.'&ID='.$Contact->cid.'&action=contact&ctrl=Campaign';
	}
	
	if($task->lead_id){
		$Lead = ProspectModel::Fetch('id = %d', $task->lead_id);
		$clink = 'admin.php?pid='.$Lead->id.'&ID='.$Lead->cid.'&action=prospect&ctrl=Campaign';
	}
	
	if($task->account_id) $a_desc = '<h5 style="padding-bottom:15px;">Account: <a href="'.$alink.'">'.$Account->name.'</a></h5>';
	if($task->contact_id) $c_desc = '<h5>Contact: <a href="'.$clink.'">'.$Contact->first_name.' '.$Contact->last_name.'</a></h5>';
	else $c_desc = '<h5>Lead: <a href="'.$clink.'">'.$Lead->name.'</a></h5>';
	
	$timetable = new stdClass();
	$timetable->name = $task->title;
	//$timetable->image = $row{'image'};
	$timetable->date = date('j', strtotime($task->due_date)); // date : type DATE. For example: 2016-09-07
	$timetable->month = date('n', strtotime($task->due_date));
	$timetable->year = date('Y', strtotime($task->due_date));
	$timetable->day = date('d', strtotime($task->due_date));
	$timetable->start_time = $time; // start_time : Must be 24 hour format. For example: 18:00
	//$timetable->end_time = '17:00'; // end_time : Must be 24 hour format. For example: 20:30
	//teal = 1, navy = 2, yellow = 3, orange = 4
	$timetable->color = $class;
	if($task->contact_id) $timetable->description = utf8_encode(nl2br($a_desc.$c_desc));
	else $timetable->description = utf8_encode(nl2br($c_desc));
	
	
	array_push($timetables, $timetable);
}

echo json_encode($timetables);
?>