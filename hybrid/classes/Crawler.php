<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 1/27/15
 * Time: 11:44 AM
 */

class Crawler extends \Logger {
    protected $proxy = NULL;
    protected $referrer = '';
	private $proxyList = array();

	/** @var RankingProxiesDataAccess */
	private $rankingDataAccess;

	const COOKIE_FILENAME = 'cookie.txt';

	function __construct(Qube $qube, $MODE = Logger::DOLOG)
	{
		parent::__construct($MODE);
//		$proxyList = file(RankingsGenerator::PROXY_FILE);
//		array_walk($proxyList, function(&$value){$value = trim($value);});
//		$this->setProxyList($proxyList);
		$this->qube = $qube;
		$this->rankingDataAccess = new RankingProxiesDataAccess();
	}

	static function getCookieJarFile($proxy, $url = null)
    {
        $cookies_directory = QUBEROOT . 'scripts/cron/cookies/' . $proxy;
        if(!FileUtil::CreatePath($cookies_directory) || !is_writable($cookies_directory))
            return false;
        return $cookies_directory . '/' . static::COOKIE_FILENAME;
    }

    function load_page($url, $VERBOSE = false, $proxy = NULL)
    {
        if(!$proxy)
            $proxy  =   $this->getProxy();

        list($proxy, $proxyport, $proxy_userpass)   =   explode(':', $proxy, 3);

        $cookiejar = static::getCookieJarFile($proxy, $url);

        if(!touch($cookiejar) || !is_writable($cookiejar))
        {
            throw new \QubeException('could not create cookie . ' .  $cookiejar);
        }

        $cookiejar = realpath($cookiejar);

        $ttime = microtime(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.168 Safari/535.19');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_VERBOSE, $VERBOSE);
        if($this->referrer)
            curl_setopt($ch, CURLOPT_REFERER, $this->referrer);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiejar);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiejar);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_STDERR, fopen('php://stdout', 'w'));

		// proxy stuff
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
		curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
		if($proxy_userpass){
			curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy_userpass);
		}
        //

        $data = curl_exec($ch);
        $return_info = curl_getinfo($ch);
		$error_info = curl_error($ch);
        curl_close($ch);


//        print_r($url); print_r($cookiejar);

        $this->showProgress(__FUNCTION__, $ttime, 'Time: %s Cookie: %s, %s', "%s",
            basename($cookiejar), str_replace('%', '%%', $url));

        return $this->returnData($return_info, $data, $proxy, $error_info);
    }

	/**
	 * Returns a random proxy.
	 * @return null|string
	 */
    public function getProxy()
    {
//        if(!$this->proxy){
//			$this->proxy = $this->proxyList[rand(0, count($this->proxyList) - 1)];
//        }
		if(!$this->proxy)
			$this->proxy = $this->getRankingProxiesDataAccess()->getRandomProxy();
        return $this->proxy;
    }

	public function removeProxy($proxie){
		$index = array_search($proxie, $this->proxyList);
		unset($this->proxyList[$index]);
		$this->proxyList = array_values($this->proxyList);
	}

    function returnData($curl_info, $data, $proxy, $error_info)
    {
        $return_code = $curl_info['http_code'];
        if($return_code !== 200)
        {
            $this->logERROR("%s, Encountered HTTP Code: %d %s/ Proxy: %s", __FILE__, $return_code, $error_info, $proxy);
            return FALSE;
        }

        return $data;
    }

	/**
	 * @deprecated
	 * @param array $proxyList
	 */
	public function setProxyList($proxyList){
		$this->proxyList = $proxyList;
	}

	/**
	 * @deprecated
	 * @return bool
	 */
	public function proxyListEmpty(){
		return empty($this->proxyList);
	}

	/**
	 * @return RankingProxiesDataAccess
	 */
	public function getRankingProxiesDataAccess()
	{
		return $this->rankingDataAccess;
	}
}
