<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of InputValidator
 *
 * @author amado
 */
interface InputValidator {
    //put your code here
    
    function Validate(array &$vars, ValidationStack $vs);
}
