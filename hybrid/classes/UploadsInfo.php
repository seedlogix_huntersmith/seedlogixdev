<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of UploadsInfo
 *
 * @author amado
 */


class UploadsInfo
{
    protected $files    =   array();
    protected $destination  =   'hub';
    
    function setDestination($dest){
        $this->destination  =   $dest;
        return $this;
    }
    
    function add($key, $fileinfo)
    {
        if(is_array($fileinfo) && count($fileinfo) > 1)
                throw new Exception('Not prepared for many files.');
                
        $this->files[$key]       =   is_array($fileinfo) ? array_pop($fileinfo) : $fileinfo;
    }
    
    function getFiles($key  =   null, $value    =   null)
    {
        if($value){
            $file   =   $this->getFiles($key);
            return $file[$value];
        }
        
        return $key ? $this->files[$key]    :   $this->files;
    }
    
    function hasFiles()
    {
        return count($this->files);
    }
    
    function accept($arrkey, ValidationStack $errors, $uploadDir    =   null)
		{
        if($uploadDir == null)
            $uploadDir  =   $this->destination;
        
        $upload = new Zend_File_Transfer();
        
        /**
         * the information returned from getFileInfo consists
         * of a a assoc-array with key
         *  such that a field name "website[logo]" is converted to website_logo_ 
         */
        $files = $upload->getFileInfo();
        
        $uploadsInfo    =   $this;
        
        $result =   array();
        
        if(empty($files)) return false;
        
//            $this->findReseller();
//            ini_set('display_errors', false);
            
            // @todo change. this is dependent on some weird shit, see upload_file.php:23
            $destination    =   $uploadDir; //$this->User->getStoragePath($uploadDir);
            
#temp disabled (error on 6qube1)            $upload->addValidator('IsImage', true);
#            $upload->addValidator('NotExists', false, '.');
            $depths =   preg_split('[\\/]', $destination);
            if(!is_dir($destination)){
                $exists =   $depths[0];
                $ix=1;
                for(;is_dir($exists);$ix++){
                    $exists =   implode(DIRECTORY_SEPARATOR, array_slice($depths, 0, $ix));
                }
                $ix--;
                while($ix <= count($depths)){
                    mkdir(implode(DIRECTORY_SEPARATOR, array_slice($depths, 0, $ix)));
                    $ix++;
                }
            }
            $upload->setDestination($destination);
            
            foreach($files as $fkey => $file)
            {
                if(empty($file['tmp_name'])) continue;  // nothing uploaded..
                preg_match('/^' . $arrkey . '_(.*?)_$/', $fkey, $matches);
                $key    =  $matches[1];
                if(!$upload->isValid($fkey))
                {
//                    var_dump($matches, $fkey);
                    $errors->addErrors($key, $upload->getMessages());
                    $error_status   =   'validation';
                }else{
                // rename jpeg to jpg extensions
    //                if(preg_match('/jpeg$/i', $file['name']))                {
                    $counter    =   0;
                    $fname  =   preg_replace(array('/[^\w\.-_]+/', '/jpeg$/'), array('_', 'jpg'), $file['name']);
                    $finfo  =   pathinfo($fname);
                    while(true){
                        $newname    =   $destination . '/' . $finfo['filename'] . (++$counter == 1 ? '' : '_' . $counter) . '.' . $finfo['extension'];
                        
                        // generate new name based on counter if the file exists
                        if(file_exists($newname))
                            continue;

                        // if not exists, set the new name and break out of the loop
                        $upload->addFilter('Rename', array('target' => $newname), $fkey);
                        break;
                    }
    //                }

                    if(!$upload->receive($fkey))
                    {
                        $errors->addErrors($key, $upload->getMessages());
                    }  else {
    //                    echo 'fileinfo:'; var_dump($upload->getFileInfo($fkey), $fkey);
                        $uploadsInfo->add($key, $upload->getFileInfo($fkey));
    //                    if(!is_array($result['uploads']))   $result['uploads']  =   array();
      //                  $result['uploads'][$key]   =   '/' . $uploadDir .'/' . $object_vars[$key];  //$this->ResellerData->getUrl($destination, $object_vars[$key]);
                    }
                }
            }
            //ini_set('display_errors', true);
            
            return $uploadsInfo;
    }
}


