<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of ObCaptureView
 *
 * @author amado
 */
class ObCaptureView implements BaseView
{
    protected $content  =   '';
    function __construct() {
        ob_start();
    }
    
    function endCapture(){
        $this->content  = ob_get_clean();
        return $this;
    }
    
    function init($var, $val) {
        ;
    }
    
    function display() {
        echo $this->content;
    }

}