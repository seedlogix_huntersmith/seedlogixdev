<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of AllCampaignsStatsTable
 *
 * @author amado
 */
class AllCampaignsStatsTable {
//	
//	function __construct(CampaignController $CampContr) {
//		parent::__construct($CampContr->getCampsStats());
//	}
	
	static function getColumnInfo()
	{
		return DataTableResult::getTableColumns('Campaigns');
//            throw new Exception('GetColumnInfo depracated, now using DataTableResult::getTableColumns');
//		return array ('Campaign Name' => 'name',
//							'Visits Changes' => 'visitschangepct',
//				'Total Visits' => 'visitsnow',
//				'Total Leads' => 'num_leads',
//				'Conversion Rate' => 'conversionspct',
//				'Touchpoints' => 'num_touchpoints',
//					'Creation Date' => 'created_str'
//				);
	}		
	
	static function sortRowColumns($row)
	{
		$row2	=	array();
		foreach(self::getColumnInfo() as $colname)
		{
			$row2[]	=	$row[$colname];
		}
		
		return $row2;
	}
        
	static function getColumnTitles() {
		
		return array_keys(self::getColumnInfo());
	}
}
