<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactNotifier
 *
 * @author sofus
 */
class ContactNotifier extends NotificationProcessor
{
	
	function createNotification()
	{
		return new ContactFormNotification($this);
	}
	
	function Execute(\VarContainer $vars) {
		
		//** SENDER IS RESELLER (OR OWN USER EMAIL IF RESELLER IS NOT FOUND)
		$sender_addr   =   'no-reply@' . $this->reseller_domain;
		
		$Notification	=	$this->createNotification();
		$Sender         = new SendTo($user->reseller_company, $sender_addr);
		$Notification->setSender($Sender);
		
		$recipient = new SendTo($this->collab_name, $this->collab_email);
		
		
		return $this->DoSend($vars->Mailer, $Notification, $recipient);
	}
	
}
