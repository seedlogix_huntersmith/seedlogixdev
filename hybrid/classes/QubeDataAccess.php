<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of QubeDataAccess
 *
 * @author amado
 */
class QubeDataAccess extends QubeDependant
{
	/**
	 *
	 * @var UserWithRolesModel
	 */
	protected $_actionuser	=	NULL;
	static $preparedStatements	=	array();

	static function FromDataAccess(QubeDataAccess $dataAccess){
		$qda = new static($dataAccess->getQube());
		$qda->setActionUser($dataAccess->getActionUser());
		return $qda;
	}

	static function keyToPlaceHolder($key)
	{
		return str_replace(array('.', '-'), '_', $key);
	}

    /**
     * Decides what type of data to return given a pdostatement, and qOpts
     *
     * @param PDOStatement $stmt
     * @param array $args
     * @param array|object $qOpts
     * @param string $class
     * @return array|mixed|PDOStatement
     */
	static  function  HandleQueryResults(PDOStatement $stmt, array $args, $qOpts, $class = '??')
	{
        if(is_object($qOpts)) $qOpts = (array)$qOpts;

		self::callPreparedQuery($stmt, $args);
		if($qOpts['getStatement'])
		{
			return $stmt;
		}
		if($qOpts['returnSingleObj'])
		{
			return $stmt->fetchObject($qOpts['returnSingleObj']);
		}
		return $stmt->fetchAll(PDO::FETCH_CLASS, $class);
	}

	/**
	 * <ul>
	 * <li><b>calc_found_rows:</b> Set to true if calc_found_rows is needed in the query</li>
	 * <li><b>returnSingleObj:</b> Set to true to get a single row</li>
	 * <li><b>where:</b> An array with pairs field and value to use in WHERE clause, elements without key will be used directly in where clause</li>
	 * <li><b>orderCol:</b> Specify the column which will be used in ORDER BY clause</li>
	 * <li><b>orderDir:</b> Specify the sort direction in ORDER BY clause</li>
	 * <li><b>cols:</b> A single string with comma separated of the fields required in SELECT statement</li>
	 * <li><b>sSearch:</b> Special param for datatables, use it for a filter in WHERE cluase</li>
	 * <li><b>limit:</b> Specify the limit rows to be returned</li>
	 * <li><b>limit_start:</b> Specifiy where are going to start the results</li>
	 * </ul>
	 * @param array $args An array with data
	 * @param array $defaultWhere
	 * @return array Returns an array with the following elements: </br>
	 * <ul>
	 * <li><b>returnSingleObj:</b> Specify if will get a single row </li>
	 * <li><b>CALC_FOUND_ROWS:</b> Use calc_found_rows in the query </li>
	 * <li><b>orderDir:</b> Sort direction </li>
	 * <li><b>orderCol:</b> Sort by column </li>
	 * <li><b>getStatement:</b> Specify if a PDOStatement is required in the return </li>
	 * <li><b>cols:</b> Fields in the select statement </li>
	 * <li><b>limitstr:</b> The limit query clause string </li>
	 * <li><b>isCount:</b> Specifiy if the statement is for count purposes </li>
	 * <li><b>where:</b> A DBWhereStatement</li>
	 * </ul>
	 */
	static function getQueryOptions(array &$args, $defaultWhere = NULL)
	{
		$CALC_FOUND_ROWS	=	'';
		$orderDir	=	'';
		if(isset($args['calc_found_rows']))
		{
			$CALC_FOUND_ROWS	=	' SQL_CALC_FOUND_ROWS';
			unset($args['calc_found_rows']);
		}

		$returnSingleObj = '';
		if(isset($args['returnSingleObj']))
		{
			$returnSingleObj = $args['returnSingleObj'];
			unset($args['returnSingleObj']);
		}

		$where = NULL;
		if(isset($args['where']))
		{
			$where	=	DBWhereExpression::Create($args['where']);
			unset($args['where']);
		}elseif($defaultWhere)
        {
            $where = DBWhereExpression::Create($defaultWhere);
        }

		if(isset($args['orderCol']))
		{
			$orderCol	=	$args['orderCol'];
			unset($args['orderCol']);
		}
		
		
		if(isset($args['orderDir']))
		{
			$orderDir	=	$args['orderDir'];
			unset($args['orderDir']);
			$orderDir	= in_array(strtoupper($orderDir), array('ASC', 'DESC')) ? $orderDir : '';
		}
		$getStatement	=	isset($args['getStatement']);
		if($getStatement)
		{
			unset($args['getStatement']);
		}
		
		$cols	=	'';
		if(isset($args['cols']))
		{
			$cols	=	$args['cols'];
			unset($args['cols']);
		}

		$isCount	=	$cols == 'count(*)';
		if(isset($args['sSearch']))
		{
			if(trim($args['sSearch'])	==	'')
				unset($args['sSearch']);
			elseif(!is_numeric($args['sSearch']))
				$args['sSearch']	=	'%' . $args['sSearch'] . '%';
		}

		$limitstr	=	'';
		if(isset($args['limit']))
		{
			if(!isset($args['limit_start']))
				$args['limit_start']	=	0;
			$limitstr	=	' LIMIT :limit_start, :limit';
		}


		if(isset($args['newRowsIds'])){
			$new_rows_ids = array_map('intval', $args['newRowsIds']);
			unset($args['newRowsIds']);
		}
		return compact('returnSingleObj',
			'CALC_FOUND_ROWS', 'orderDir', 'orderCol', 'getStatement', 'cols', 'limitstr', 'isCount', 'where', 'new_rows_ids');
	}
	
	static function getPreparedStatement($key1)
	{
		$key	=	implode('-', func_get_args());
		if(isset(self::$preparedStatements[$key]))
			return self::$preparedStatements[$key];
		
		return NULL;
	}
	
	/**
	 * 
	 * @param string $sql
	 * @param string $key1
	 * @return PDOStatement
	 */
	function setPreparedQuery($sql, $key1)
	{
		$args	= func_get_args();	array_shift($args);
		$key	=	implode('-', $args);
		self::$preparedStatements[$key]	=	$this->getPDO()->prepare($sql);
		return self::getPreparedStatement($key);
	}
	
	/**
	 * checks for LIMIT placeholder and uses bindValue()
	 * 
	 * @param PDOStatement $stmt
	 * @param array $args
	 */
	static function callPreparedQuery(PDOStatement $stmt, array $args)
	{
		foreach ($args  as $key => $val)
		{
			$paramtype	=	PDO::PARAM_STR;
			if($key == 'limit' || $key == 'limit_start'){
				$paramtype	=	PDO::PARAM_INT;
				$val = (int)$val;
			}
#		deb($paramtype, $key, $val);
			$stmt->bindValue(':' . $key, $val, $paramtype);
		}
		return $stmt->execute();
	}
	
	function UserCan($permission, $id = 0)
	{
		return $this->getActionUser()->can($permission, $id);
	}
	
	function setActionUser(UserWithRolesModel $user)
	{
		$this->_actionuser	=	$user;
		return $this;
	}
	
	/**
	 * 
	 * @return UserWithRolesModel
	 */
	function getActionUser()
	{
		return $this->_actionuser;
	}

    /**
     * @return int
     */
    function getActionUserID()
    {
        return $this->getActionUser()->getID();
    }

	/**
	 * 
	 * @param HybridBaseController $C
	 * @return \static
	 */
	static function FromController(HybridBaseController $C)
	{
		$qda	=	new static($C->getQube());
		$qda->setActionUser($C->getUser());
		return $qda;
	}
	
	function refreshObject(HybridModel $model, $fields = NULL, $fetch_mode = NULL)
	{
        $q     =    $this->getQube()->queryObjects($model, 'T', $model->_getCondition('T', HybridModel::FOR_REFRESH));
        if($fields)
            $q->Fields($fields);
        
		if($fetch_mode){
		switch($fetch_mode)
		{
			case PDO::FETCH_COLUMN:
				return $q->Exec()->fetchColumn();
			default:
                return $q->Exec()->fetch($fetch_mode);
		}}
        
        return $q->Exec()->fetch();
	}
	
	/**
	 * 
	 * @param HybridModel $model
	 * @param array $columns
	 * @param string $wherestr
	 * @param array $params
	 * @return HybridModel
	 */
    function getObject($model, $columns, $wherestr = '', $params = array())
    {

//		$obj	=call_user_func_array(
//					array($this->getQube(), 'queryObjects'),					$params)
//				->Fields($columns)->Exec()->fetch();
		
        $obj =   $this->getQube()->queryObjects($model, 'T')->Where($wherestr)
            ->Fields($columns)->Prepare();
		$obj->execute($params);
        return $obj->fetchObject($model);
    }

	function FOUND_ROWS(){

		return $this->query('SELECT FOUND_ROWS()')->fetchColumn();
	}
	
	function beginTransaction()
	{
		return $this->getPDO('master')->beginTransaction();
	}
	
	function commit()
	{
		return $this->getPDO('master')->commit();
	}
	
	function rollBack()
	{
		return $this->getPDO('master')->rollBack();
	}
}
