<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LeadNotifier
 *
 * @author sofus
 */
class LeadNotifier extends NotificationProcessor {
	
	public $q_id	=	0;
	public $data_id	=	0;
	public $hub_name = '#Unknown#';
	public $page_name = '#Unknown#';
	public $data = '#Unknown#';
	public $form_name = '#Unknown#';
	public $keyword = '#Unknown#';
	public $ip = '#Unknown#';	
	public $reseller_domain	=	'unknown';
	
	public $collab_name = 'Uknown';
	public $collab_email = 'Uknown';

    function getLeadData($lead_id)
    {
        $lead = new LeadModel($this->getQube());
        $lead->setID($lead_id);

        $lead_data = $lead->getData(LeadsDAO::create($this));
        return $lead_data;
    }

	function Execute(\VarContainer $vars) {
		$recipient  =   new SendTo($this->collab_name, $this->collab_email);
		$sender	=	new  SendTo(NULL, 'no-reply@' . $this->reseller_domain);

		$lead_data = $this->getLeadData($this->data_id);

		$this->data = "";
		foreach($lead_data as $key => $value)
			$this->data .= "<p><strong>$key:</strong><br />$value</p>\n";
		
		$Notification   =   new LeadFormNotification($this);
		$Notification->setSender($sender);
		
		$this->logINFO("Notifying.. q_id %d (R: %s)", $this->q_id, $recipient);
        
		return $this->DoSend($vars->Mailer, $Notification, $recipient);
	}
}
