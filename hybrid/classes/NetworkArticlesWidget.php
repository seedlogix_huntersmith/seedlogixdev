<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NetworkArticlesWidget
 *
 * @author amado
 */
class NetworkArticlesWidget extends DataTableWidget {
	
	/**
	 *
	 * @var CampaignController
	 */
	protected $_ctrl	=	NULL;
	/**
	 *
	 * @var CampaignModel
	 */
	protected $_camp	=	NULL;
	
	static function LoadCampaign(DashboardBaseController $ctrl, $campaign)
	{
		if($campaign instanceof CampaignModel) return $campaign;
		if(!is_string($campaign)) throw new QubeException('Security Exception');
		return CampaignController::getCampaignObject($campaign, new \Qube\Hybrid\DataAccess\CampaignDataAccess());
	}
	
	function configure(DashboardBaseController $ctrl, array $config) {
		$this->_camp	=	self::LoadCampaign($ctrl, $config['campaign']);
	}
	
	function initView(\ThemeView $V) {
		$qitems	=	$this->getController()
						->queryNetworkArticles($this->_camp)
						->orderBy('name')
						->calc_found_rows(true)
						->Exec();
                $total = $this->getController()->query('SELECT FOUND_ROWS()')->fetchColumn();
		return array(
						'articles'	=> $qitems->fetchAll(),
                                                'total_users' => $total,
                                                'campaignID' => $this->_camp->getID()
						);
	}
	
}
