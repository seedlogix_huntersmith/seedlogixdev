<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of Cron
 *
 * @author amado
 */
class Cron extends Logger {
    //put your code here
    
    private $startk  =   0;
    
    public $numerrors =   0;
    
    public $log =   array();
    
    function __construct($MODE  =   Cron::DOLOG, $debuglevel	= ProcessLog::INFO) {
//			parent::__construct($debuglevel);
			parent::__construct($MODE);
			$this->init();
    }
    
    function init($mode =   Cron::DOLOG){			
        $this->startk   = time();
				$this->mtime	=	microtime(TRUE);
    }
		
		function getLog($separator	=	"\n")
		{
			$stream	= stream_get_contents($this->getMemoryStream(), -1, 0);
			return $stream;
			return implode($separator, $this->log);
		}
    
    function error(){
        $this->numerrors++;
    }
    
    function has_errors(){
        return $this->numerrors != 0;
    }
    
    function duration(){
			return microtime(true) - $this->mtime ;
    }

    function mail_if_errors($emails, $subject, $func=	'mail'){
        if(!$this->numerrors) return FALSE;
        
        $message =   $this->getLog();
        return $func(join(', ', $emails), 
                $subject . ' Date: ' . date('Y-m-d', $this->startk), 
                "The following cron job errors ocurred: $message");
                   
    }
    
    function __destruct() {
        
        $status =   $this->has_errors() ? 0 : 1;
        $cronfile   = basename($_SERVER['argv'][0]);
        
        $q  =   'insert into 6qube_history (object, event, value, data)
                VALUES(?, ?, ?, ?)';
        $prep   =   Qube::GetPDO()->prepare($q);
        
        $prep->execute(array("cron", $cronfile, $status, join("\n", $this->log)));
    }
}

