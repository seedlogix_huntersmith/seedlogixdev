<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BlogLegacyWrapper
 *
 * @author amado
 */
class BlogLegacyWrapper {


	/**
	 *
	 * @var Blog
	 */
	protected $_blog	=	NULL;
	
	/**
	 *
	 * @var BlogModel
	 */
	protected $blog_model	=	NULL;
	
	/**
	 *
	 * @var BlogDataAccess
	 */
	protected $da	=	NULL;
	
	function __construct(BlogModel $bm, $array)
	{
		require_once(QUBEADMIN . 'inc/blogs.class.php');
		$this->_blog	=	new Blog();
		$this->_blog->wrapper	=	$this;
		$this->blog_model	=	$bm;
		$this->da		=	new BlogDataAccess();

	}



	/**
	 * returns a blogpostrowmodel that parses tags.
	 * 
	 * @param type $post_id
	 * @return \BlogPostRowModel
	 */
	function getSinglePost($post_id){
		$BPM	=	$this->da->getSinglePost($post_id, $this->_blog->post_rows);
		if(!$BPM) return FALSE;
		$BPRM	=	new BlogPostRowModel($BPM->getValues(NULL, DBObject::FOR_SELECT), $this->blog_model);
		return $BPRM;
	}
	
	function __call($name, $arguments) {
			return call_user_func_array(array($this->_blog, $name), $arguments);
	}

    function __set($name, $value) {
        $this->_blog->$name  =   $value;
    }
    
    function __get($name) {
        return isset($this->_blog->$name) ? $this->_blog->$name : '';
    }
	
	function getBlogPostSummary($blogInfo, $blogPostRow, $length)
	{
		deb($blogInfo, $blogPostRow);
		$this->_blog->wrapper	=	$this;
		if($blogPostRow['blog_id'] != $this->blog_model->getID())
		{
			$this->_blog->wrapper	=	NULL;
			return $this->_blog->getBlogPostSummary($blogInfo, $blogPostRow, $length);
		}
		
		// create a blogpostrow for parsing
		$bpr	=	new BlogPostRowModel($blogPostRow, $this->blog_model);
		$parsedContent	=	$bpr['post_content'];
		
		return $this->_blog->shortenSummary($parsedContent, $length);
	}

	function fetchArray($result, $assocOnly = '', $assocOnly2 = ''){
		$blogData = $this->_blog->fetchArray($result, $assocOnly, $assocOnly2);

		$this->parseEmbbedForms($blogData);
		$this->parseTags($blogData);

		return $blogData;
	}

	function parseTags(&$values)
	{



		$blogModel = $this->blog_model;
		$bda = $this->da;

		$parsableFields = array(
			'blog_edit_1', 'blog_edit_2', 'blog_edit_3'
		);

		if($blogModel->connect_hub_id){

			$hub_row = HubModel::Fetch('id = %d', $blogModel->connect_hub_id);

			$posts	=	$bda->getLatestPosts($blogModel->id, 5);



			$parsableTags = array('phone'=>$hub_row->phone, 'address'=>$hub_row->street,
				'company'=>$hub_row->company_name, 'city'=>$hub_row->city, 'state'=>$hub_row->state,
				'citycommastate'=>$hub_row->city.', '.$hub_row->state, 'zip'=>$hub_row->zip,
				'facebook'=>$hub_row->facebook, 'linkedin'=>$hub_row->linkedin, 'twitter'=>$hub_row->twitter,
				'instagram'=>$hub_row->instagram, 'youtube'=>$hub_row->youtube, 'pinterest'=>$hub_row->pinterest,
				'blog'=>$hub_row->blog, 'custom1'=>$hub_row->zip, 'custom1'=>$hub_row->keyword1, 'custom2'=>$hub_row->keyword2, 'custom3'=>$hub_row->keyword3,
				'custom4'=>$hub_row->keyword4, 'custom5'=>$hub_row->keyword5, 'custom6'=>$hub_row->keyword6,
				'custom7'=>$hub_row->button_1, 'custom8'=>$hub_row->button1_link, 'custom9'=>$hub_row->button_2,
				'custom10'=>$hub_row->button2_link, 'custom11'=>$hub_row->button_3, 'custom12'=>$hub_row->button3_link, 'custom13'=>$hub_row->legacy_gallery_link,
				'custom14'=>$hub_row->form_box_color, 'custom15'=>$hub_row->full_home, 'custom16'=>$hub_row->remove_font, 'custom17'=>$hub_row->gallery_page,
				'custom18'=>$hub_row->slider_option, 'custom19'=>$hub_row->display_page,
				'blogfeed'=>BlogModel::getHTMLPosts($posts),
				'custom20'=>$hub_row->remove_contact, 'domain'=>$hub_row->domain);



		} else {

			$parsableTags = array('phone'=>$blogModel->phone, 'address'=>$blogModel->address,
				'company'=>$blogModel->company_name, 'citycommastate'=>$blogModel->city.', '.$blogModel->state);

		}



		foreach($parsableFields as $field){

			if(preg_match('/\(\((.*)\)\)/i', $values[$field])){
				foreach($parsableTags as $replaceKeyword=>$replaceValueRaw){

					if(is_callable($replaceValueRaw))
					{
						$replaceValue	=	call_user_func($replaceValueRaw, $field, $replaceKeyword, $values);
					}else
						$replaceValue	=	$replaceValueRaw;

					$values[$field] = str_replace('(('.$replaceKeyword.'))', $replaceValue, $values[$field]);



				}

			}

		}


	}

	function parseEmbbedForms(&$values){
		$embbedableForms = array(
			'blog_edit_1', 'blog_edit_2', 'blog_edit_3'
		);

		foreach($embbedableForms as $field){
			if(isset($values[$field])){
				$value = $values[$field];

				if(preg_match('/\|@form-(.*?)@\|/', $value, $matches)){
					$form_id = $matches[1];
					$blogModel = $this->blog_model;
					$Form   =   new FormOutput();

					$params =   array('formID' => $form_id,
						'userID'	=> $blogModel->user_id,
						'hubID' => 0,
						'hubPageID' => 0,
						'searchEngine' => '',
						'seKeyword' => '',
						'js'    => false,
						'style' => false,
						'outputWrapper' => 'router'
					);

					$values[$field] = str_replace($matches[0], $Form->GetForm($params, $blogModel), $value);
				}
			}
		}
	}
}
