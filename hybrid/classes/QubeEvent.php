<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QubeEvent
 *
 * @author Amado
 */
class QubeEvent {
    //put your code here
    
    protected $_listeners  =   array();
    protected $_event   =   '';
    
    protected $_dispatched  =   false;
    protected $_autodispatch    =   true;
    static $events  =   array();
    
    static function Trigger($EventID){

        $args   =   func_get_args();
        
//        array_unshift($args, $EventID);   // trim eventid from args
        
				if(!isset(self::$events[$EventID]))	return;	// no events
				
        call_user_func_array(array(
            self::$events[$EventID], 'Dispatch'),
        $args);
    }
    
    static function Bind($EventID, $func){
        if(!isset(self::$events[$EventID]))
            self::$events[$EventID]  =   new self($EventID);
        
        self::$events[$EventID]->AddListener($func);
    }
    
    function AddListener($func)
    {
        $this->_listeners[] =   $func;
        return $this;
    }
    
    static function UnBind($EventID, $key)
    {
        unset(self::$events[$EventID]->_listeners[$key]);
    }
    
    function Dispatch()
    {
        $args   =   func_get_args();
//        array_unshift($args, $this);
        
        foreach($this->_listeners as $event_key => $listener){  
            $args   =   func_get_args();
            array_unshift($args, $event_key);
//            $args   =   ;
                
            call_user_func_array($listener, 
                    $args);
//            $listener($this->_event);
        }
    }
    
    protected function __construct($EventID){
        $this->_event   =   $EventID;
    }
}
