<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of SpamChecker
 *
 * @author amado
 */
class SpamChecker {
	
	const ISNOT_SPAM	=	0;
	const IS_SPAM	=	1;
	
	/**
	 *
	 * @var SystemDataAccess
	 */
	protected $SDA	=	NULL;
	function __construct(SystemDataAccess $SDA	=	NULL) {
		$this->SDA	=	$SDA ? $SDA : new SystemDataAccess();
	}
	
	function checkSpammerIP($ip, Qube $qube)
	{
		$threshold_seconds	=	2;
		$q	=	'select 1 FROM leads WHERE ip = "' . $ip . '" and (spam > 19 or timestampdiff(SECOND,created, now()) < ' . $threshold_seconds . ') order by created desc limit 1';

		$is_spammer	=	$qube->query($q)->fetchColumn() == 1;
		return $is_spammer;
	}
	/**
	 * Returns Zero (0) for non-spam. Uses the spam_keywords setting.
	 * 
	 * @param type $message
	 * @param SystemDataAccess $SDA
	 * @return int
	 */
	function checkMessage($message)
	{
//		static $blacklist	=	NULL;
		$spamcount	=	0;
//		if(!$blacklist)
//		{
			$blacklist	=	explode("\n", $this->SDA->getConfig('spam_keywords', 0, 'valstr'));
//		}
		
		foreach($blacklist as $spamword)
		{
			if(stripos($message, trim($spamword)) !== FALSE)	$spamcount++;
		}
		
		return $spamcount;
	}
	
}
