<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/22/16
 * Time: 9:59 AM
 */

class ProspectRateGradesCron {
	/** @var  Logger */
	private $logger;

	/** @var  LeadsDAO */
	private $leadsDAO;

	/** @var  LeadDataAccess */
	private $leadsDataAccess;

	/**
	 * @param Logger $logger
	 */
	public function setLogger(Logger $logger)
	{
		$this->logger = $logger;
		$this->leadsDataAccess = new LeadDataAccess();
		$this->leadsDAO = new LeadsDAO();

		$this->leadsDataAccess->setActionUser($this->leadsDataAccess->createSystemUser());
		$this->leadsDAO->setActionUser($this->leadsDataAccess->createSystemUser());
	}

	public function run(){
		$this->logger->logINFO('Starting...');
		$statement = $this->leadsDataAccess->getPDO()->prepare("SELECT leads.* FROM prospects LEFT JOIN leads ON prospects.lead_ID = leads.id WHERE prospects.grade = 0 AND prospects.created > '2014-12-12 18:55:26' ORDER BY prospects.created DESC");
		$statement->setFetchMode(PDO::FETCH_CLASS, 'LeadModel');
		$statement->execute();
		/** @var LeadModel[] $leads */

		$countStatement = $this->leadsDataAccess->query("SELECT count(*) FROM prospects LEFT JOIN leads ON prospects.lead_ID = leads.id WHERE prospects.grade = 0 AND prospects.created > '2014-12-12 18:55:26' ORDER BY prospects.created DESC");
		$countStatement->execute();
		$remainging = $countStatement->fetchColumn();

		$this->logger->logINFO('Found %d leads without grade', $remainging);


		while(($lead = $statement->fetch()) != null){
			$grade = $this->getGradeRate($lead);
			$this->leadsDataAccess
				->getPDO()
				->prepare('UPDATE leads SET grade = :grade WHERE id = :lead_id')
				->execute(array('grade' => $grade, 'lead_id' => $lead->getID()));

			$this->leadsDataAccess
				->getPDO()
				->prepare('UPDATE prospects SET grade = :grade WHERE lead_ID = :lead_id')
				->execute(array('grade' => $grade, 'lead_id' => $lead->getID()));

			$remainging--;
			$this->logger->logINFO('Remaining leads %d', $remainging);
		}
		$this->logger->logINFO('Finished');
	}

	public function getGradeRate(LeadModel $lead){
		$leadForm = $this->leadsDAO->getLeadForm($lead->lead_form_id);

		if(!$leadForm) return 0;

		$fields = $leadForm->getFields();
		$fieldValues = $lead->getData($this->leadsDAO);


		$values_count	=	count(array_filter($fieldValues));
		$fields_count	=	count($fields);

		$empty_fields	=	$fields_count-$values_count;

		$value = 0;

		if($empty_fields == 0)
			$value = 100;

		else if($fields_count < 10)
			$value = 100-$empty_fields*10;

		else
			$value = ceil($values_count/$fields_count*100);

		return ceil($value / 10);
	}
}