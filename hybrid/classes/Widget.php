<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/21/15
 * Time: 4:09 PM
 */

abstract class Widget{
	/** @var  array */
	protected $WidgetOptions;

	protected static $widgetId = 0;

	protected static $groupId = 0;

	/** @var  Template */
	protected $template;

	/** Must override for children widgets */
	const WIDGET_TEMPLATE = '';

	const WIDGET_VAR = '';

	const WIDGET_NAME_GROUP = '';

	const WIDGET_NAME = '';
	public function setOptions(){

	}

	public abstract function createNew($id, $classGroup, $options = null);

	public abstract function getJS();

	public function display($options = null){
		$this->template->getWidget(static::WIDGET_TEMPLATE, array(static::WIDGET_VAR => $this->_createNew($options)));
	}

	private function _createNew($options = null){
		$id = static::WIDGET_NAME . (++static::$widgetId);
		if($options) ++static::$groupId;
		$classGroup = static::WIDGET_NAME_GROUP . static::$groupId;
		return $this->createNew($id, $classGroup, $options);
	}

}