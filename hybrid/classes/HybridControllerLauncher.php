<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HybridContextLauncher
 *
 * @author amado
 */
class HybridControllerLauncher {
    
    /**
     *
     * @var DashboardBaseController
     */
    protected $_Ctrl;
    
    function __construct($Controller) {
        $this->_Ctrl    =   self::GetController($Controller);
    }
    
    static function GetController($Controller){
        $ClrClass    =   ucfirst($Controller) . 'Controller';
        
        $Clr    =   new $ClrClass;
        return $Clr;
    }
    
    function Launch(Zend_Session_Namespace $Namespace){
        $Clr    =   $this->_Ctrl;
        
        $Clr->Initialize();
		$Clr->setSystemEventDispatch(new Qube\Hybrid\Events\SystemEventDispatch());
        $Clr->loadSession($Namespace);
        
        return $Clr;
    }
    
    function RunView(Zend_Session_Namespace $NS, $action)
    {
        try{
            $this->Run($NS, $action)->display();
        }catch(Exception $e)
        {
            Qube::Fatal($e);
        }
    }
    
    function Run(Zend_Session_Namespace $NS, $action){
        return $this->Launch($NS)->Execute($action);
    }
    
    static function getWebNamespace($namespacename){
				$httphost	=	$_SERVER['HTTP_HOST'];
				Zend_Session::setOptions(array('cookie_domain' => '.' . $httphost));
        $Session    =   new Zend_Session_Namespace($namespacename);
        $Session->HTTP_HOST =   $httphost;
        $Session->REQUEST_URI   =   $_SERVER['REQUEST_URI'];
        
        return $Session;
    }
}
