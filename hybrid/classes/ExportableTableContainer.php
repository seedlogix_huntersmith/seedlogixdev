<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportableTableContainer
 *
 * @author sofus
 */
class ExportableTableContainer implements ExportableTable {
	protected $columns	=	array();
	/** @var ExportableTable  */
	protected $exporter	=	NULL;
	protected $customArgs	=	array();

	/**
	 * @param $cols
	 * @deprecated ExportableTableContainer use GetExportableColumnTitles when Export is called
	 */
	function setColumnTitles($cols)
	{
		$this->columns	=	$cols;
	}
	
	function setExportable(ExportableTable $export) {
		$this->exporter=	$export;
	}
	
	function setArgs(array $args)
	{
		$this->customArgs	=	$args;
	}
	
	function Export($fhandle, array $args)
	{
		fputcsv($fhandle, $this->exporter->GetExportableColumnTitles());
		return $this->exporter->Export($fhandle, $args + $this->customArgs);
	}

	function GetExportableColumnTitles()
	{
		return $this->exporter->GetExportableColumnTitles();
	}
}
