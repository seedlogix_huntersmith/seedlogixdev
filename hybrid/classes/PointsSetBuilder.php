<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PointsSetBuilder
 *
 * @author amado
 */
class PointsSetBuilder
{   
	const MYSQLFORMAT	=	'Y-m-d H:i:s';
    /**
     *
     * @var string
     */
    public $colfetch;
    
    /**
     *
     * @var DateTime
     */
    public $start;
    
    /**
     *
     * @var DateTime
     */
    public $end;
    public $loop_increment;
    public $tbl_loop_limit;
    
    public $isalltime;
    /**
     *
     * @var string
     */
    protected $inception;
    /**
     *
     * @var VarContainer
     */
    public $vc;
    
    static function createFromOffset($interval, $period, $inception   =   null)
    {
        $vc =   VarContainer::getContainer(compact('interval', 'period'));
        return new PointsSetBuilder($vc, $inception);
    }
    
    function __construct(VarContainer $c = null, $inception    =   null) {
        $this->vc   =   $c;
        $this->inception    =   (is_null($inception) OR (is_string($inception) && strpos($inception, '0000-00-00') === 0)) ? piwik_inception() : $inception;
        if($c->exists('interval') && $c->exists('period'))
            $this->Init($c->interval, $c->period);
    }
    
    function getStartDate($fmt  =   'Y-m-d'){
        return $this->start->format($fmt);
    }
    
    function getEndDate($fmt  =   'Y-m-d'){
        return $this->end->format($fmt);
    }
    
    static function firstDayNextMonth(DateTime $T){
        $T->modify(date('Y-m-01', strtotime('+1 Month', $T->getTimestamp())));
		$T->setTime(0, 0, 0);
        return $T;
    }
    
    static function firstDayCurrentMonth(DateTime $T){
        $T->modify(date('Y-m-01 00:00:00', $T->getTimestamp()));
        return $T;
    }
    
    function previousSet(){
        $this->end =   clone($this->start->sub(new DateInterval('P1D')));
        $this->start->sub(DateInterval::createFromDateString($this->interval . ' '. $this->incrementstr));
        return $this;
    }
    
    function createValueArray(PDOStatement $q){
        $rows   =   $q->fetchAll(PDO::FETCH_KEY_PAIR);	//PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
        $points =   array();
        for($i = clone($this->start); $i < $this->end; ){
            $curstamp   =   $i->getTimestamp();
            $value  =   array_key_exists($curstamp, $rows) ? $rows[$curstamp] : 0;
//            $row    =   array('timestamp'   =>  $curstamp, 'value'   =>  $value, 'date' => $startdate->format('Y-m-d'));
            $points[]   =   $value;
//            $startdate->add($increment);
            $i->add($this->loop_increment);
        }
        
        return $points;
    }
    
		/**
		 * the select must return a unixtimestamp, value columns
		 * 
		 * @param PDOStatement $q
		 * @return type
		 */
    function createDataSet(PDOStatement $q, $type	=	'chart'){
			
				if($type	==	'array')
				{
					return $this->createValueArray($q);
				}
				
        $rows   =   $q->fetchAll(PDO::FETCH_KEY_PAIR);	//PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
#	var_dump($rows);	var_dump($startdate->getTimestamp());
        $points =   array();
        for($i = clone($this->start); $i < $this->end; ){
            $curstamp   =   $i->getTimestamp();
            $value  =   array_key_exists($curstamp, $rows) ? $rows[$curstamp] : 0;
            $row    =   array('timestamp'   =>  strtotime($i->format('Y-m-d') . ' UTC'), 'value'   =>  $value, 'date' => $i->format('Y-m-d'));
            $points[]   =   $row;
            $i->add($this->loop_increment);
        }
        
        return $points;
    }
		
		
    
    static function validateTimeValues(&$interval, &$period){
            $interval   =   max(0, (int)$interval);
            $period       = preg_match('/^(DAY|WEEK|MONTH|YEAR|ALL)$/i', $period) ? $period : 'DAY';
    }
    
    function Init($interval, $period){
        
        $period= strtoupper($period);
        
        // loop by month for ALL, and for MONTH periods, associate by first day of the month
        $loop_increment =   'MONTH';
        $time_key   	=	'DATE_FORMAT(date1, "%Y-%m-01")';        
        // end monthly pre-config
        
        $loop_end   =   new DateTime(); // the limit for looping through archive tables.
        $loop_end->setTime(0, 0, 0);
        self::firstDayNextMonth($loop_end);
        
        $this->isalltime    =   false;
        if($period    ==  'ALL'){
            $this->isalltime    =   true;
            $datestart  =   $this->firstDayCurrentMonth(new DateTime($this->inception));
            $dateend    =   clone($loop_end);//$this->firstDayCurrentMonth(new DateTime());
        }else{
            
            self::validateTimeValues($interval, $period);
            $interval--;
            $datestart  =   new DateTime("-$interval $period");
            $dateend    =   new DateTime(); // today
            
            if($period	==	'WEEK'){
                $loop_increment =   'WEEK';
                $datestart->modify('last monday');
                $dateend->modify('+1 Week');
                // associate by first day of the week. (monday)
                $time_key	=	'DATE_SUB(date1, INTERVAL WEEKDAY(date1) DAY)';
              }
            if($period	==	'DAY'){
                $loop_increment =   'DAY';
                // set loop_end as first day of next month so to include the current months's archive table
//                $this->firstDayNextMonth($loop_end);
                // associate by date
                $time_key   =   'date1';
            }
            if($period	==	'MONTH'){
                $this->firstDayCurrentMonth($datestart);
//                $intrv++;	// add current month.
            }
        }
        
        $datestart->setTime(0,0,0);
        $this->incrementstr =   $loop_increment;
        $this->loop_increment = DateInterval::createFromDateString('1 ' . $loop_increment);
        $this->colfetch =   $time_key;
        $this->tbl_loop_limit  =   $loop_end;
        $this->start   =   $datestart;
        $this->end     =   $dateend;
        $this->interval =   $interval;
        $this->period   =   $period;
    }
    
    function DateDiff(){
        return date_diff($this->start, $this->end)->format('%a');
    }
    
    function getTimeRangeRestriction(){        
        return ' BETWEEN "' .
                $this->start->format('Y-m-d') .
                '" AND "' .
                $this->end->format('Y-m-d') . '"';
    }

    function __clone()
    {
        $this->start = clone $this->start;
        $this->end = clone $this->end;
    }


}
