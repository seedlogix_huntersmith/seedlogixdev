<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of LegacyFuncs
 *
 * @author amado
 */
class LegacyFuncs {
	
	static function getFieldDataStringLegacy(CollectedLeadInput $inputs)
	{
				$dataString = '';
				foreach($inputs->getInputs() as $field_ID => $input){
					$dataString .= $input->field->type.'|-|'.$input->field->getLabel().'|-|'.$input->valueToText.'[==]';
				}
				//trim trailing [==]
				if(strlen($dataString)>=4) $dataString = substr($dataString, 0, -4);
				//sanitize data string
//				$dataString = $hub->sanitizeInput($dataString);
		
				return $dataString;
	}
	
	static function CreateLeadInfoEmailBody(CollectedLeadInput $inputs, $pageTitle, $search_engine, $keyword, $hubName)
	{
		$message	=	'<html><body><h1>Information Captured</h1>';
					foreach($inputs->getInputs() as $field_ID => $input){
//						$message .= '<p><strong>'.$value['label'].':</strong><br />'.$value['value'].'</p>';
						$message .= '<p><strong>'.$input->field->getLabel().':</strong><br />'.$input->valueToText.'</p>';
					}
					
					$message .= '<br />					<p><small><strong>HUB Name</strong>: '.$hubName.'</small><br />';
					if($pageTitle){
					$message .=  				'<small><strong>Page Submitted</strong>: '.$pageTitle.'</small><br />';
					}
					if($search_engine){
					$message .=  				'<small><strong>Search Engine</strong>: '.$search_engine.'</small><br />';
					}
					if($keyword){
					$message .=  					'<small><strong>Keyword Searched</strong>: '.$keyword.'</small><br />';
					}
					$message .= 					'<small><strong>IP Address</strong>: '.$_SERVER['REMOTE_ADDR'].'</small></p>					</body></html>';		
					
					return $message;
	}
	
	static function encodeLeadFormPostValues($fields, $hub, &$continue, &$firstName, &$lastName, &$phone, &$name, &$email)
	{
			$fieldData = array();
//go through each form field and attach the user's data
			foreach($fields as $key=>$values){
				$fieldInputName = $hub->fieldNameFromLabel($values['label']);
				if($values['type']=='checkbox'){
					$fieldValue = $_POST[$fieldInputName] ? 'Yes' : 'No';
					$fieldData[$i] = array();
					$fieldData[$i]['type'] = 'checkbox';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $fieldValue;
				}
				else if($values['type']=='date'){
					$fieldValue = $_POST[$fieldInputName.'_M'].'/';
					$fieldValue .= $_POST[$fieldInputName.'_D'].'/';
					$fieldValue .= $_POST[$fieldInputName.'_Y'];
					$fieldData[$i]['type'] = 'date';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $fieldValue;
				}
				else if($values['type']=='name'){
					$fieldInputName1 = $hub->fieldNameFromLabel($values['label1']);
					$fieldInputName2 = $hub->fieldNameFromLabel($values['label2']);
					$fieldValue1 = $_POST[$fieldInputName1];
					$fieldValue2 = $_POST[$fieldInputName2];
					
					$fieldData[$i]['type'] = 'name_1';
					$fieldData[$i]['label'] = $values['label1'];
					$fieldData[$i]['value'] = $fieldValue1;
					$firstName = $fieldValue1;
					$i++;
					$fieldData[$i]['type'] = 'name_2';
					$fieldData[$i]['label'] = $values['label2'];
					$fieldData[$i]['value'] = $fieldValue2;
					$lastName = $fieldValue2;
				}
				else if($values['type']=='email'){
					$fieldData[$i]['type'] = 'email';
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $_POST[$fieldInputName];
					$email = $_POST[$fieldInputName];
					//anti-spam measure -- lots of .'s in email address
                                        $continue = $hub->validateEmail($email) !== false;
					if($continue && substr_count($email, '.')>=6) $continue = false;
				}
				else if($values['type']!='section'){
					$fieldData[$i]['type'] = $values['type'];
					$fieldData[$i]['label'] = $values['label'];
					$fieldData[$i]['value'] = $_POST[$fieldInputName];
					if(stripos($values['label'], 'phone') !== FALSE)
					  $phone = $_POST[$fieldInputName];
					  
					if($values['type']=='text' && $fieldInputName=='name'){
						$name = $_POST[$fieldInputName];
					}
				}
				$fieldInputName = $fieldInputName1 = $fieldInputName2 = $fieldValue = $fieldValue1 = $fieldValue2 = NULL;
				$i++;
			}
					return $fieldData;
	}
	//put your code here
}
