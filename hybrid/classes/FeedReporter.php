<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */



class FeedReporter
{
    /** @var FeedObjects The feed objects .*/
    protected $feedobjects;
    
    function __construct(FeedObjects $list) {
        $this->init($list);
    }
    function init(FeedObjects $list){
        $this->feedobjects  =   $list;
        return $this;
    }
    
    function getFeedObjects(){
        return $this->feedobjects;
    }
    /**
     * 
     * returns object with these info: total_items, new_items_count, found_items_count, items => (array), new_items => (array)
     * 
     * @param FeedObject $parseObject
     * @return object 
     */
    function getItemsSummary(FeedObject &$parseObject, $feedrow =   NULL){
        $import =   &$this->feedobjects;    
        $itemsinfo  =   array();
        $KEYS   =   array();
        $total_feed_items   =  $import->totalItems(); 
        for($i = 0; $i < $total_feed_items; $i++)
        {
            $import->getItem($i, $parseObject);
            $itemsinfo[$parseObject->getKEY()]    =   $parseObject->getSummary();
            $itemsinfo[$parseObject->getKEY()]['object']    =   clone $parseObject;
            $itemsinfo[$parseObject->getKEY()]['exists']    =   false;
            $KEYS[] =   $parseObject->getKEY();
        }

	if(!empty($KEYS)){        
        $find_objects   =   '`KEY` IN (?' . str_repeat(', ?', count($KEYS)-1) . ')';
        
        if($feedrow)
            $find_objects .= ' AND FEEDID = ' . (int)$feedrow->ID;
        
        $pdo    =   Qube::Start()->GetDB('slave');
        $qprepare   =   'SELECT `KEY`, `CREATED`, `md5` FROM feedobjects WHERE ' . $find_objects;
        $q      =  $pdo->prepare($qprepare);
        $q->execute($KEYS);
#        echo $qprepare, "\n";
#        asciiarray($q->fetchAll(PDO::FETCH_ASSOC));
        
        $found_items    =   $q->rowCount();

	}else{
		$found_items	=	0;	
	}        
        $new_items  =   $total_feed_items - $found_items;

        $changed_items  =   array();
        
        $return =   array('total_items' =>  $total_feed_items, 'new_items_count' => $new_items,
                            'found_items_count'   => $found_items);
	if($found_items){
        while($found_obj    =   $q->fetch(PDO::FETCH_ASSOC)){
#            echo 'found: ', $found_obj['KEY'], "\n";
            
            // the following lines move all the 'found' items in the array to the end of the array
            // so that the new (not found) items remain at the top of the array and we can perform an array_slice;
            
            $curitem    =   $itemsinfo[$found_obj['KEY']];
            unset($itemsinfo[$found_obj['KEY']]);
            
            
            $curhash    =   $curitem['md5'];    // hash from summary
            $dbitemhash =   $found_obj['md5'];
            
            $curitem['CREATED'] =  $found_obj['CREATED'];
            
            if($dbitemhash !=   $curhash){
#                echo "$dbitemhash != $curhash <br>";
                $changed_items[]    =   $curitem;
//                continue;   // do not append back on item stack.
            }
            
            $itemsinfo[$found_obj['KEY']]   =   $curitem;
        }
	}
        
        $return['items']    = array_slice($itemsinfo, $new_items, $found_items);
        $return['new_items']    =   array_slice($itemsinfo, 0, $new_items);
        $return['changed_items']    =   $changed_items;
        $return['changed_items_count']  =   count($changed_items);
        
#        exit;
        return (object)$return;
    }
}

