<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/24/14
 * Time: 12:17 PM
 */

class DataTableAccountLimitsResult extends DataAccessTableResult {
	
	protected $defaultSortColumn	=	'expiration_date';
	
	/**
	 * @param UserDataAccess $da
	 * @param VarContainer $var
	 */
	public function __construct(UserDataAccess $da, VarContainer $var){
		parent::__construct($da, $var, $var->table, 'getAllAccountLimits', NULL);
	}

	function prepareArgs(&$args)
	{
		if($this->_req->checkValue('parent_id', $user_id))
		{
			$args['user_id']	=	$user_id;
		}
	}
	
	function callFetchFunction($args = array(), $limit = 10, $offset = 0, $filter = null, $order = "expiration_date", $direction = "desc")
	{
		$stmn	=	$this->getAllAccountLimits($args, $limit, $offset, $filter, $order, $direction);
		$stmn->execute(empty($args) ? NULL : $args);
		return $stmn->fetchAll(PDO::FETCH_OBJ);
	}
	
	function getTotalRecords($args = array()) {
		$this->prepareArgs($args);
		$args['COUNTONLY']	=	TRUE;
		$stmn	=	$this->getAllAccountLimits($args);
		$stmn->execute($args);
		return $stmn->fetchColumn();
	}

	function getAllAccountLimits(&$args, $limit = 10, $offset = 0, $filter = null, $order = "expiration_date", $direction = "desc")
	{
		$actionuser	=	$this->getDataAccess()->getActionUser();
		$where = array();
		$fields = 'sql_calc_found_rows l.id, username, type, expiration_date, l.ts, l.cancelation_date, subscriptions.name, l.user_ID,
				l.sub_ID, datediff(l.expiration_date, :curdate) as days_to_expiration,
				if(l.expiration_date=0,1,0) as is_expirable,
				if(l.cancelation_date!=0,1,0) as is_canceled,
					if(datediff(curdate(), l.cancelation_date) = 0, concat("Today at ", time(l.cancelation_date)),
						if(l.cancelation_date=0,"Never", date_format(l.cancelation_date, "%W %b %D"))) as when_canceled,
					if(l.cancelation_date = 0 and (l.expiration_date = 0 OR l.expiration_date > curdate()), 1, 0) as is_cancelable';
		
		$args['curdate'] = date(PointsSetBuilder::MYSQLFORMAT);		// using hardcoded date for mysql-cache purposes.
		if(isset($args['COUNTONLY']))
		{
			$fields = 'count(*)';
			$limit = NULL;
			$order = NULL;
			unset($args['COUNTONLY'], $args['curdate']);
		}
		
		$q = "SELECT $fields
				from account_limits l 
				inner join users u on u.id = l.user_ID ";

		if($actionuser->isSystem())
		{
			// can visualize any users.
		}elseif($actionuser->can('view_users'))
		{
			// visualize only child users.
			$where[] = 'u.parent_id = ' . $actionuser->getParentID();
		}else{
			// can visualize only 'own' subscriptions. which are probably none.
			$where[]	=	'u.id = ' . $actionuser->getID();
		}
		
		$q .= ' left join subscriptions on subscriptions.ID = l.sub_ID';
		
//		$args = array();
		
		if($filter){
			if(is_numeric($filter))
			{
				$where[] = ' (l.user_ID = :search or l.sub_ID = :search)';
			}else
				$where[] = " (username LIKE CONCAT(CONCAT('%', :search),'%') OR l.id = :search)";
			
			$args['search'] = $filter;
		}
		
		// do not check permissions, let higher levels handle that stuff
		if(isset($args['user_id']))
		{
			$where[]	=	'l.user_ID = :user_id';
		}

		if(!empty($where))
			$q .= ' WHERE ' . join(' AND ', $where) . ' ';
		
		if($order || $args['order']){
			$order = $order ? $order : $args['order'];
			$newRow = $args['new_rows_ids'] ? 'l.id = ' . join(' DESC, l.id = ', $args['new_rows_ids']) . ' DESC, ' : '';
			$q .= " Order By $newRow $order $direction";
			unset($args['order'], $args['new_rows_ids']);
		}

		if($limit != null){
			$q .= " LIMIT $offset, $limit";
		}
		$stmn = $this->getDataAccess()->getQube()->GetDB()->prepare($q);
		
		return $stmn;
	}

}
