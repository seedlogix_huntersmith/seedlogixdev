<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Data Access Strategy.
 *
 * @author amado
 */
class DAStrategy {
	protected $_DA;
	function setDA(QubeDataAccess $DA)
	{
		$this->_DA	=	$DA;
		return $this;
	}
	
	function getDA()
	{
		return $this->_DA;
	}
}
