<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of WebsiteStatsRow
 *
 * @author amado
 */
class WebsiteStatsRow extends RowRender {
    
    function render($arr) {
        // order of assignment is IMPORTANT!
        $this->renderVisitsChange($arr);
        $this->renderVisits($arr);
//        $this->values['numleads'] =   $arr['numleads'];
        $this->renderConversionRate($arr);
    }
    
}