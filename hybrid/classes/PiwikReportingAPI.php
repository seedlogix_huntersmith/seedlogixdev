<?php

/**
 * Created by PhpStorm.
 * User: Eric
 * Date: 19/02/2016
 * Time: 02:13 PM
 */
class PiwikReportingAPI{
    function getPiwikApiRequest($method, $params = array(), $domain = '6qube.mobi') {
        $tokenAuth = Qube::get_settings('analytics_token_auth');


        $VERBOSE = false;

        $params['token_auth'] = $tokenAuth;
        $params['format'] = "JSON";
        $urlEncodedParams = http_build_query($params);
        $url	=	"http://$domain/?module=API&method=$method&$urlEncodedParams";

        $client = new Zend\Http\Client($url, array(
            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_CONNECTTIMEOUT => 0, CURLOPT_VERBOSE => $VERBOSE, CURLOPT_ENCODING => 'gzip,deflate')));

        return $client;
    }

    public function liveGetVisitorProfile($idSite, $visitorId, $segment = null, $limitVisits = null){
        $params = array('idSite' => $idSite, 'visitorId' => bin2hex($visitorId));
        if($segment != null){
            $params['segment'] = $segment;
        }

        if($limitVisits != null){
            $params['limitVisits'] = $limitVisits;
        }

        $request = $this->getPiwikApiRequest('Live.getVisitorProfile', $params);
        $response = $request->send();

        if($response->isSuccess() === FALSE){
            return null;
        }

        $content = $response->getContent();

        if($content->result == "error"){
            return null;
        }

        return json_decode($content);
    }
}