<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubdomainXmlResults
 *
 * @author Amado
 */
class SubdomainXmlResults
{
    protected $subs =   array();
    function __construct(SimpleXMLElement $e    =   null) {
        if($e)
            $this->set($e);
    }

    function set(SimpleXMLElement $r)
    {
        foreach($r->data as $sub)
        {
            $this->subs[(string)$sub->domain]   =   (array)$sub;
        }
    }

    function isEmpty()
    {
        return count($this->subs)   ==  0;
    }
    
    function exists($completesubdomain)
    {
        return array_key_exists($completesubdomain, $this->subs);
    }
    
    function get($completesub)
    {
        return $this->subs[$completesub];
    }
    function getAll(){
        return $this->subs;
    }
}

