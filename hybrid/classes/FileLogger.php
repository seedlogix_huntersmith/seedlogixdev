<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileLogger
 *
 * @author Amado
 */
class FileLogger {
    //put your code here
    
    protected $linefunc =   NULL;
    protected $fh   =   NULL;
    function __construct(Closure $linefunc, $filename, $mode = 'w'){
        $this->linefunc=    $linefunc;
        $this->fh   =   fopen($filename, $mode);
    }
    
    function __invoke() {
        $ARGS   =   func_get_args();
        array_unshift($ARGS, $this->fh);
        $str    =   call_user_func_array($this->linefunc, $ARGS);
        if($str !== FALSE)
            fwrite($this->fh, $str);
    }
    
    function __destruct() {
        fclose($this->fh);
    }
}
