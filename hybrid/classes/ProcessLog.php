<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of ProcessLog
 *
 * @author amado
 */
abstract class ProcessLog {
	const ERR = 3;
	const INFO	=	5;
	const DEBUG	=	7;
	
	protected $nextLogger	=	NULL;
	protected $level	=	0;
	
	function __construct($level	=	self::INFO) {
		$this->level	=	$level;
	}
	
	function __invoke()
	{
		return call_user_func_array(array($this, 'writeLog'), func_get_args());
	}
	
	function writeLog($message, $level)
	{
		call_user_func_array(array($this, 'Write'), func_get_args());
		if($this->nextLogger)
			call_user_func_array(array($this->nextLogger, 'writeLog'), func_get_args());
		return 0;
	}
	
	abstract function Write($message, $level);
	
	function setNext(ProcessLog $L)
	{
		$this->nextLogger	=	$L;
	}

	function logINFO()
	{
		$args	=	func_get_args();

		return $this->writeLog(call_user_func_array('sprintf', $args), ProcessLog::INFO);
	}
	
	function logERROR()
	{
		$args	=	func_get_args();

		return $this->logger->writeLog(call_user_func_array('sprintf', $args), ProcessLog::ERR);
	}
}

