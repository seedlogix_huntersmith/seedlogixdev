<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of ChartData
 *
 * @author amado
 */
class ChartData {
    //put your code here
    
    public $config  =   array();
    public $data;
    public $cookiename;
    
    function getConfig(){
        return $this->config;
    }
    
    function loadConfig($config){
        $this->config   =   $config;
    }
    
    function loadCookieConfig($cookiename, $default){
        $this->cookiename   =   $cookiename;
        $this->loadConfig(Cookie::getVal($cookiename, $default));
    }
    
    function getDataSets(){
        return $this->data;
    }
    
    function addDataSet($datax){
        $this->data[]   =   $datax;
    }
    
    function getChartPoints(PointsSetBuilder $b, &$data, PiwikReportsDataAccess $PRDA , $type = 'chart'){
				// check a campaign
        if($b->vc->checkValue('cid', $cid))
		{
			$data	= $PRDA->getCampaignStats($b, $cid, 'nb_visits', $type, $b->vc->getValue('touchpointType', ''));
			return true;
//                $DBWhere->Where ('cid = %d', $cid);
		}
				// check a hub				
        if($b->vc->checkValue('id', $id) && $b->vc->checkValue('touchpointType', $touchpoint_type))
		{
            switch(strtolower($touchpoint_type)){
                case 'website':
                case 'landing':
                case 'mobile':
                case 'profile':
                case 'social':
                    $data	=	$PRDA->getHubStats($b, $id, 'nb_visits', $type);
                break;
                case 'blog':
                    $data	=	$PRDA->getBlogStats($b, $id, 'nb_visits', $type);
                    break;
            }
			return true;
		}
				// check all stats for current user_id
				$data		=	$PRDA->getUserGlobalStats($b, $PRDA->getActionUser()->getID(), $type);
				return true;
    }
    
//    function getLeadsPoints(VarContainer $c, &$data){
    function getLeadsPoints(PointsSetBuilder $b, &$data, PiwikReportsDataAccess $PRDA, $type = 'chart'){
        
        if($b->vc->checkValue('cid', $cid))
		{
			$data	=	$PRDA->getCampaignStats($b, $cid, 'nb_leads', $type, $b->vc->getValue('touchpointType', ''));
			return true;
		}
//                $DBWhere->Where('t.cid = %d', $cid);
        
        if($b->vc->checkValue('id', $id) && $b->vc->checkValue('touchpointType', $touchpoint_type))
		{
            switch($touchpoint_type){
                case 'website':
                case 'landing':
                case 'mobile':
                case 'profile':
                case 'social':
                    $data	=	$PRDA->getHubStats($b, $id, 'nb_leads', $type);
                    break;
                case 'blog':
                    $data	=	$PRDA->getBlogStats($b, $id, 'nb_leads', $type);
                    break;
            }
			return true;
		}
		
		$data	=	$PRDA->getCampaignStats($b, 0, 'nb_leads', $type, $b->vc->getValue('touchpointType', ''));
		return true;
    }
/*
    function getPhoneCallPoints(PointsSetBuilder $b, &$data, $type = 'chart') {
        var_dump($callReports);
    }
*/
}
