<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubURIRequest
 *
 * @author amado
 */
class HubURIRequest extends QubeDependant {

	//put your code here
	protected $_str = '';
	protected $page = 'default';
	protected $productID = 0;
	protected $_dirs	=	array();

	/**
	 * Get the string without slash at beginning or end. /(this)/
	 * 
	 * @return type
	 */
	function getStr() {
		return $this->_str;
	}
	
	function getDirs(){
		return $this->_dirs;
	}

	function setStr($str) {
		$this->_str = trim($str, '/');
		if ($this->_str == '') {
			$this->_str = false;
			return false;
		}

		$this->parsePageLocal($this->_str);
		return true;
	}

	/**
	 * Sets the page value based on predefined rules. THese rules are used to find theme php pages.
	 * 
	 * @param type $str
	 * @return type
	 */
	function parsePageLocal($str) {
		$this->page = 'default';

		$dirs = explode('/', $str);
		$this->_dirs	=	$dirs;
		$depth = count($dirs);

		// @todo display-results
//        if(preg_match('#^([^/]*?)/display-results/([0-9]+)?/?$#', $page_request_raw, $page_reqdata))
//        {
//            $page   =   'display-results';
//            $search_page_number =   $page_reqdata[2];
//            return $page;
//        }

		if ($depth >= 2) {
			if (is_numeric($dirs[1])) { // "a/789/....."
				$this->page = 'signup';
				$this->productID = (int) $dirs[1];
			} else {	 // "a/b/..."
				$this->page = $dirs[1]; // "a/custom-page"
				if ($depth > 2) { // "a/b/c/..."
					$this->page = $dirs[2]; // "a/somethjing/custom-page"
					if (is_numeric($dirs[2])) { // "a/b/789/..."
						$this->page = 'signup';
						$this->productID = (int) $dirs[2];
					}
				}
			}
		}
		else
			$this->page	=	$dirs[0];

		if ($dirs[0] == 'activation' || $dirs[0] == 'client-optout')
			$this->page = 'system';
		
	}

	function __construct(Qube $Qube, $str = '/') {
		parent::__construct($Qube);
		$this->setStr($str);
	}

	function getPageParameter() {
		return $this->page;
	}

	function getProductID() {
		return $this->productID;
	}
	
	function isFeedRequest(HubModel $hub)
	{
		$split_request = $this->getDirs();		

		$is_feed_request = $split_request[0] == $hub->root;
		return $is_feed_request;
	}

}
