<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of KeywordsWidget
 *
 * @author amado
 */
class KeywordsWidget {
    
    protected $config   =   array();
    protected $user_ID  =   null;
    protected $cid  =   null;
    
    function __construct($config, $userid) {
        $this->config   =   $config;
        $this->user_ID= $userid;
    }
    
    //put your code here
    function getKeywords(){
        $where  =   null;
        $subkey =   '';
        switch(true){
            case isset($this->config['cid']):
                $where  =   array('cid' => $this->config['cid']);
                $subkey=    'C' . $this->config['cid'];
                break;
            case isset($this->config['id']):
                $where  =   array('id'  =>  $this->config['id']);
                $subkey =   'ID' . $this->config['id'];
                break;
        }
        $Keywords   =   new Keywords($this->user_ID . $subkey);
        
        
            $Keywords->setWhere($where);
        // otherwise, 6q_trackingids already gets info for @dashboard_userid
        
        $Keywords->setInterval($this->config['interval']);
        $Keywords->setPeriod(strtolower($this->config['period']));
        $Keywords->setPerPage(50);
        
        $Keywords->loadCache();
        
        $kwlist =   $Keywords->getKeywords();
        return $kwlist;
        return array();
    }
}
