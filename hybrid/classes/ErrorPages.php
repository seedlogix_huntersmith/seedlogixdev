<?php

class ErrorPages {
	public static function PageNotFound($domain = ''){
		include HYBRID_PATH . '/error/404.php';
	}

	public static function Forbidden(){
		return include HYBRID_PATH . '/error/403.php';
	}

	public static function MethodNotAllowed(){
		include HYBRID_PATH . 'error/405.php';
	}

	public static function InternalServerError(){
		include HYBRID_PATH . 'error/500.php';
	}

	public static function ServiceUnavailable(){
		include HYBRID_PATH . 'error/503.php';
	}

	public static function PageError($errorCode = 'Error'){
		include HYBRID_PATH . 'error/error.php';
	}

	public static function PageOfflinte(){
		include HYBRID_PATH . 'error/offline.php';
	}
} 