<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 11/26/14
 * Time: 9:09 AM
 */

class HttpValidator extends \Zend\Uri\Http{

	public function isValid()
	{
		if($this->host)
			return parent::isValid();
		return false;
	}

}