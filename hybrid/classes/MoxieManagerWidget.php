<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/22/15
 * Time: 10:16 AM
 */

class MoxieManagerWidget extends Widget{
	/** @var  Template */
	protected $template;

	/** Must override for children widgets */
	const WIDGET_TEMPLATE = 'moxiemanager-filepicker2.php';

	const WIDGET_VAR = 'moxie';

	const WIDGET_NAME_GROUP = 'moxieWidget';

	const WIDGET_NAME = 'moxie';

	public function __construct($options, $template){
		$this->template = $template;
		$this->WidgetOptions = $options;
	}

	public function createNew($id, $classGroup, $options = null)
	{
		return new MoxieManagerWidget($options, $this->template);
	}

	public function getJS()
	{
		if(self::$widgetId = 1){
			$this->template->getJS(self::WIDGET_TEMPLATE);
		}

		if($this->WidgetOptions[])
	}
}