<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


/**
 * Description of LeadsVisitsChartData
 *
 * @author amado
 */
abstract class AutoLoadChartData extends ChartData
{
    /**
     *
     * @var HybridBaseController
     */
    protected $controller   =   NULL;
    
    function __construct($cookiename, $defaultconfig = NULL, $refresh	=	false, $initvars	=	array()) {
        if(!is_null($defaultconfig))
        		$this->loadCookieConfig($cookiename, $defaultconfig);
        else {
            $this->cookiename   =   $cookiename;
        }
        $this->init($initvars);
        $this->loadData($refresh);
    }
    
    function checkFnCache($fn, VarContainer $vc = null){
        $cachekey   =   $this->controller->getUser()->getID() . '_atycs_' . $fn . 
                    ($vc ? md5(join('', $vc->getData())) : '' );
        $data =   $this->controller->cacheCheck($cachekey, 10 * 60 * 60);
        return $data;
    }
    
    function checkFnCache_Table($fn, $key, &$row, VarContainer $vc = null){
        $tbl    =   $this->checkFnCache($fn, $vc);
        $row    =   ($tbl !== false && isset($tbl[$key])) ? $tbl[$key] : null;
        
        return $tbl;
    }
    
    function init(&$initvars){
        $this->controller   =   $initvars['controller'];
        unset($initvars['controller']);
    }
    
    abstract function loadData($refresh);
}

