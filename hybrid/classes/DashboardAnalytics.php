<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class QueryBatch
{
    protected $qs   =   array();
    function add($q)
    {
        $this->qs[]    =   $q;
    }
    
    function run(PDO $p)
    {
        foreach($this->qs as $q)
        {
//            echo $q, "\n";
            $p->query($q);
        }
    }
}


/**
 * Description of DashboardAnalytics
 *
 * @author amado
 */
class DashboardAnalytics {
    
    /**
     *
     * @var Qube
     */
    protected $q;
    protected $batch    =   array();
    
    protected $point_increment  =   '+1 day';
    function __construct() {
        $this->q    =   Qube::Start();
    }
    
    
    // main functions

    
//    function getLeadsGraphData($offset)
    function getLeadsGraphData(PointsSetBuilder $b, DBWhereExpression $W)
    {
		$RDA	=	new ReportDataAccess($this->q);
		return $RDA->getLeadsGraphData($b, $W);
    }
        
    function getVisitsGraphData($offset)
    {
        // @todo convert this magic to mysql procedure.
#        $offset =   "-$interval $unit";
#        $db =   $this->q->GetPDO();
        
        $t2 =   time();
        
        $this->queryAnalytics($offset, $t2, 'nb_visits');
        
        return $this->createSetPoints($this->getResultsStatement(), $offset, $t2, "Visits");
    }    
    // end main functions
    
    
    // helper / processing functions
    function prepareHubTrackingIds($hub_where_str, $hubModel   =   'WebsiteModel')
    {
        $this->trackingmodel    =   $hubModel;
        $this->trackingwhere  =   DBWhereExpression::Create($hub_where_str);
        
        return;
        return $this->q->GetPDO()->query('CREATE TEMPORARY TABLE dashboard_summary_trackingids as select tracking_id, id as hub_id from ' .
                $this->q->getTable($hubModel) . ' WHERE ' . DBWhereExpression::Create($hub_where_str));
    }
    
    /**
     * This thread will populate the dashboard_tempvisits table with date1, value  rows
     * @param type $t1
     * @param type $t2
     * @param type $name
     * @return \QueryBatch
     */
    function queryAnalytics($t1, $t2, $name =   'nb_visits', $forceinsert    =   false)
    {
        $ts1    =   self::getTs($t1, 'php');
        $ts2    =   self::getTs($t2, 'php');
        $m1 =   strtotime(date('Y-m-1', $ts1));
        $m2 =   strtotime(date('Y-m-t', $ts2));
        $xt =   $m1;
        
        $diffdays   =   ($ts2    -    $ts1)/(60*60*24);
        $mi = 0;
#        echo $diffdays, " $m1 $m2";
        $qs =   new QueryBatch;
        // ITERATE THROUGH MONTHLY ARCHIVES
        if($diffdays < 32)
        {
            $this->point_increment  =   '+1 day';
            do{
                $mi++;
                $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . date('Y_m', $xt);
                $alias  =   "a$mi";
                $create_or_insert   =   ($mi == 1 && $forceinsert == false ? ' CREATE TEMPORARY TABLE dashboard_tempvisits AS ' : ' INSERT INTO dashboard_tempvisits ');
/*
                $q   =   "$create_or_insert SELECT date1, SUM(value) as value FROM $tbl WHERE idsite IN (SELECT tracking_id FROM dashboard_summary_trackingids) AND
                        name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
                            ' AND date1 >= ' . self::getTs($t1, 'mysql') : '') . " GROUP BY date1";
 * 
 */
                
                $q   =   " SELECT date1, SUM(value) as value FROM " . $this->trackingmodel . " t inner join $tbl s ON (s.idsite = tracking_id and 
                        s.name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
                            ' AND date1 >= ' . self::getTs($t1, 'mysql') : '') . ") WHERE {$this->trackingwhere} GROUP BY date1";
                
                $this->batch[]  =   $q;
            }while(($xt = strtotime("+1 MONTH", $xt))    <  $m2);
        }else{  // query monthly increments
            $this->point_increment  =   '+1 week';
            $tbl    = Qube::PIWIKDB . '.analytics_log_visit';
            
            $q      =   'SELECT SUBTIME(visit_first_action_time, EXTRACT(MINUTE_SECOND from visit_first_action_time)) as date1, 
                            SUM(visitor_count_visits) as value /*,
                            DAYOFYEAR(visit_first_action_time)*24+hour(visit_first_action_time) as yrhr /*,
                            count(*) as numdays */ from ' . $this->trackingmodel . ' t inner join ' . $tbl . ' ON
                                (idsite = tracking_id and
                        visit_first_action_time > ' . self::getTs($t1, 'mysql') .
                        ') WHERE ' . $this->trackingwhere . ' group by 
                            DAYOFYEAR(visit_first_action_time)*24+hour(visit_first_action_time)';// ORDER BY `visit_last_action_time`  DESC';
            
            /*
            $q      =   'SELECT 
                            SUBTIME(visit_first_action_time, EXTRACT(MINUTE_SECOND from visit_first_action_time)) as date1, 
                            SUM(visitor_count_visits) as value,
                            DAYOFYEAR(visit_first_action_time)*24+hour(visit_first_action_time) as yrhr,
                            count(*) as numdays 
                            FROM ' . $tbl . '
                            where 
                        idsite IN (SELECT tracking_id from dashboard_summary_trackingids) and
                        visit_first_action_time > ' . self::getTs($t1, 'mysql') .
                        ' group by yrhr ORDER BY `visit_last_action_time`  DESC';
            */
            $alias  =   "a$mi";
            $insert   =   ' CREATE TEMPORARY TABLE dashboard_tempvisits AS ';
            
#            $q   =   "$insert $q";
            $this->batch[]  =   $q;
        }
        // append log data!                
        
        return $qs;
    }
    
    /**
     * This func is a awesome. it decides if we need a temporary table or not.
     * 
     */
    protected function getTempVisitsTbl()
    {
#        $numq   =   count($this->batch);
#        if($numq < 2)   // only 1 query
#        {
            return '(' . join(' UNION ALL ', $this->batch) . ') x';
#        }
        
        
    }
    
    function getResultsStatement()
    {
        $q  =   //$this->batch[]  =   
                'SELECT UNIX_TIMESTAMP(datev) ts, value FROM ' . $this->getTempVisitsTbl() . ' ORDER BY datev ASC';
#echo $q;
        $db =   $this->q->GetDB();
       return $db->query($q);
       
        foreach($this->batch as $q)
        {
            $last   =   $db->query($q);
        }
        
        return $last;
    }
    
    function purgeTemporaryTable()
    {
        $this->q->GetDB()->query('DROP TEMPORARY TABLE IF EXISTS dashboard_tempvisits ');
    }
    
    /**
     * Creates array for use with flot. the query columns must be in this order: timestamp, date1, value
     * @param PDOStatement $q
     * @param int $d1   timestamp of day1 . days, secs, etc are automatically removed
     * @param int $d2   timestamp of day2 . days, secs, etc are automatically removed
     */
    function getGraphPoints(PDOStatement $q, $d1, $d2, $valuesonly  =   false)
    {
        // remove hours and seconds from TimeStamps
        $d1 =   strtotime(date('Y-m-d', self::getTs($d1, 'php')));
        $d2 =   strtotime(date('Y-m-d', self::getTs($d2, 'php')));
                
        $rows   =   $q->fetchAll(PDO::FETCH_KEY_PAIR);//PDO::FETCH_ASSOC|PDO::FETCH_GROUP);

#	var_dump("rows", $rows);
        $points =   array();
        $ri =   0;
        for($i = $d1; $i < $d2; $i = strtotime($this->point_increment, $i))
        {
#		echo $i, "\n";
            $value  =   array_key_exists($i, $rows) ? $rows[$i] : 0;
            $points[]   =   $valuesonly ? $value : (object)array('timestamp' => $i, 'value' => $value, 'date' => date('Y-m-d', $i));
        }   
        return $points;        
    }
    
    static function SelectArchiveWhereIn(DateTime $loopdate, $where){
        $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . $loopdate->format('Y_m');// date('Y_m', $xt);
        $q  =   new DBSelectQuery(Qube::GetPDO());
        $q->From($tbl)->Where('tracking_id IN (%s)', $where);
        
        return $q;
    }
    
    static function JoinStatsWhere(DBSelectQuery $q, DateTime $loopdate, DBWhereExpression $where){
        $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . $loopdate->format('Y_m');// date('Y_m', $xt);
        $q->Join("$tbl s", $where);
        return $q;
    }
    
	/**
	 * Queries the piwik analytics database directly to obtain visits and other shit.
	 * 
	 * @deprecated
	 * @param PointsSetBuilder $b
	 * @param type $name
	 * @param string $returnType
	 * @return type
	 */
    function getChartData(PointsSetBuilder $b, $name, $returnType =   'DataSet'){
        $datestart  =   $b->start;
        $OneMonth   =   new DateInterval('P1M');
        $today  =   $b->tbl_loop_limit;
        
        
        $groupby    =   array('DAY' => 'date1', 'WEEK' => 'datev', 'MONTH'    => 'year(date1), month(date1)');
  
//	$timecol	=	'date1';
	
//        if(!isset($groupby[$period])) throw new Exception('period' . " failed");
        
        $groupstr   =   $groupby[$b->incrementstr];
        $loopdate   =   clone($datestart);
        $mi =   0;
        $q  =   array();
        
        do{
            $mi++;
            $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . $loopdate->format('Y_m');// date('Y_m', $xt);
            $alias  =   "a$mi";
/*
            $q   =   "$create_or_insert SELECT date1, SUM(value) as value FROM $tbl WHERE idsite IN (SELECT tracking_id FROM dashboard_summary_trackingids) AND
                    name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
                        ' AND date1 >= ' . self::getTs($t1, 'mysql') : '') . " GROUP BY date1";
* 
*/

            $this->batch[]   =   " SELECT $b->colfetch as datev, SUM(value) as value FROM " . $this->trackingmodel . " t inner join $tbl s ON (s.idsite = tracking_id and 
                    s.name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
                        ' AND date1 >= "' . $datestart->format('Y-m-d') . '"' : '') . ") " . 
			$this->trackingwhere->FullClause() . " GROUP BY $groupstr";
            
        }while($loopdate->add($OneMonth) < $today);
        

//	$datestart->setTime(0, 0, 0);
        $returnType =   'create' . $returnType;
        $data   =   $b->$returnType($this->getResultsStatement());
        return $data;
    }
    
//
        function createValueArray(PDOStatement $q, PointsSetBuilder $b){    //DateTime $startdate, $interval, $period){
//        $increment = DateInterval::createFromDateString('1 ' . $period);
        $rows   =   $q->fetchAll(PDO::FETCH_KEY_PAIR);	//PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
			
	//var_dump($rows);	//var_dump($startdate->getTimestamp()); echo $startdate->format('Y-m-d');
        $points =   array();

        for($i = clone($b->start); $i < $b->end; ){
            $curstamp   =   $i->getTimestamp().'.000000';
			//var_dump($curstamp);

            $value  =   array_key_exists($curstamp, $rows) ? $rows[$curstamp] : 0;
			//var_dump($value);
//            $row    =   array('timestamp'   =>  $curstamp, 'value'   =>  $value, 'date' => $startdate->format('Y-m-d'));
            $points[]   =   $value;
//            $startdate->add($increment);
            $i->add($b->loop_increment);
        }

        return $points;
    }
    
//    function createDataSet(PDOStatement $q, DateTime $startdate, $interval, $period){
    function createDataSet(PDOStatement $q, PointsSetBuilder $b){
        $increment = DateInterval::createFromDateString('1 ' . $period);
        $rows   =   $q->fetchAll(PDO::FETCH_KEY_PAIR);	//PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
#	var_dump($rows);	var_dump($startdate->getTimestamp());
        $points =   array();
        for($i = 0; $i < $interval; $i++){
            $curstamp   =   $startdate->getTimestamp();
            $value  =   array_key_exists($curstamp, $rows) ? $rows[$curstamp] : 0;
            $row    =   array('timestamp'   =>  $curstamp, 'value'   =>  $value, 'date' => $startdate->format('Y-m-d'));
            $points[]   =   $row;
            $startdate->add($increment);
        }
        
        return $points;
    }
        
    function getAnalyticsPoints__depr($t1, $t2, $name, $valuesonly =   false)
    {
        $this->queryAnalytics($t1, $t2, $name);//->run($this->q->GetPDO());
        return $this->getGraphPoints($this->getResultsStatement(), $t1, $t2, $valuesonly);
    }
    
    function createSetPoints(PDOStatement $q, $d1, $d2, $label)
    {
        return array('label' => $label, 'data' => $this->getGraphPoints($q, $d1, $d2));
    }
    
    static function getTs($t, $mode =   'mysql')
    {
        if($mode == 'mysql')
        {
            if(is_int($t))
                    return 'FROM_UNIXTIME(' . $t . ')';
            
            if($t == 'CURDATE')
                return 'CURDATE()';
            
            if(preg_match('/^([-+])\s*(\d+)\s+(DAY|WEEK|MONTH)$/i', $t, $m))
            {
                $func   =   $m[1]   ==  '-' ? 'DATE_SUB' : 'DATE_ADD';
                
                return "$func(CURDATE(), INTERVAL $m[2] $m[3])";
            }else
                throw new Exception('bad ts:' . $t);
        }
        // return php timestamp or mktime
        
        return is_int($t) ? $t : strtotime($t == 'CURDATE' ? 'now' : $t);
    }
}
