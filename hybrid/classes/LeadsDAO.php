<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of LeadsDAO
 *
 * @author amado
 */
class LeadsDAO extends QubeDataAccess {
	/**
	 *
	 * @param array              $leadparams
	 * @param CollectedLeadInput $inputs
	 *
	 * @return int
	 * @throws QubeException
	 * @internal param array $assocLeadData
	 */
	function SaveLead($leadparams, CollectedLeadInput $inputs) {
		$query = "INSERT INTO leads 
																		(user_id, parent_id, lead_form_id, cid, lead_email, lead_name, hub_id, hub_page_id, data, search_engine, 
																						keyword, ip, piwikVisitorId, spam, server, created, touchpoint_id, grade)
																		VALUES 
																		(:user_id, (select parent_id from users where id=:user_id), :form_id, 
																						if(:touchpoint_id = 0, if(:hub_id = 0, :cid, (select campaign_ID from 6q_touchpoints where hub_ID = :hub_id)), (select campaign_ID from 6q_touchpoints where id = :touchpoint_id)), :lead_email, :lead_name, :hub_id,
																						:hub_page_id,	:dataString, :search_engine, :keyword, :remote_addr, ifnull(unhex(:piwikVisitorId), 0), :is_spam, :server, NOW(), :touchpoint_id, :grade)";
		$form_data = array();

		$PDO = $this->getQube()->GetDB('master');
		$stmt = $PDO->prepare($query);

		if(empty($leadparams['lead_name']))
			$leadparams['lead_name']	=	$inputs->getNameValue();

		if(empty($leadparams['lead_email']))
			$leadparams['lead_email']	=	$inputs->getEmailValue();

		unset($leadparams['lead_phone']);
		$leadparams['server'] = $_SERVER['QUBECONFIG'];

		if(!($leadparams['hub_id'] xor $leadparams['cid'])){
			throw new QubeException('Wrong number of params: SaveLead only needs hub_id or cid, not both');
		}

		if($leadparams['hub_id'] || $leadparams['touchpoint_id']){
			$leadparams['cid'] = 0;
		}else if($leadparams['cid']){
			$leadparams['touchpoint_id'] = 0;
		}

		if (!$stmt->execute($leadparams))
						throw new QubeException('Failed to save lead.');

		$lead_ID = $PDO->lastInsertId();

		try {
			$save_data_stmt = $PDO->prepare('INSERT INTO leads_datax (lead_id, field_ID, valuestr, label) VALUES (?, ?, ?, ?)');
//			$save_data_stmt = $PDO->prepare('INSERT INTO leads_data (lead_id, label, value) VALUES (?, ?, ?)');
			$PDO->beginTransaction();
			foreach ($inputs->getInputs() as $field_ID => $input) {
//				$save_data_stmt->execute(array($lead_ID, $label, $value));
				$labellook = $this->query('SELECT label
						FROM `6q_formfields_tbl`
						WHERE id = ' . (int)$input->field->ID);

				$fieldLabel = $labellook->fetch(PDO::FETCH_ASSOC);
				$save_data_stmt->execute(array($lead_ID,
					(int)$input->field->ID ? (int)$input->field->ID : 0, $input->value, (int)$input->field->ID ? $fieldLabel['label'] : $input->field->label));
			}
			$PDO->commit();
		} Catch (PDOException $e) {
						Qube::Fatal($e);
		}

		return $lead_ID;
	}
        
	//put your code here
	function getHubCollaborators($hubID) {
		$D = new QubeDriver($this->getQube());
		$collaborators = $D->queryCampaignCollabWhere('(
                 T.scope = "campaign"and T.campaign_id = (SELECT cid FROM hub WHERE id = %d)
                 ) OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND r.site_id = %d )', $hubID, $hubID)->Exec()->fetchAll();

		return $collaborators;
	}

	function CollabNotificationPush($lead_ID, array $collaborators) {
		$PDO = $this->getQube()->GetDB('master');
		$collab_msg_qstmt = $PDO->prepare('INSERT INTO collab_email_queue (data_id, data_type, collab_id)
                    VALUES (?, ?, ?)');

#		deb($collab_msg_qstmt);
		$PDO->beginTransaction();

		foreach ($collaborators as $collab) {
			// enqueue these messages
			$res	=	$collab_msg_qstmt->execute(array($lead_ID, 'lead', $collab->getID()));
			deb($res);
			// end
		}
		$PDO->commit();
	}

	/**
	 * 
	 * @param type $form_ID
	 * @return LeadFormModel
	 */
	function xgetLeadForm($form_ID)
	{
		$this->getQube()->queryObjects('LeadFormModel', 'F', 'F.user_id = %d AND F.id = %d AND trashed = 0')->Exec()->fetch();
	}
	
	/**
	 * Return assoc array of [label] => $value
	 * 
	 * @param int $lead_ID
	 * @return array
	 */
	function getLeadFieldsData($lead_ID)
	{
        $qstr = "SELECT label, value FROM leads_data WHERE lead_id = $lead_ID
          UNION ALL SELECT coalesce(6q_formfields_tbl.label, leads_datax.label), valuestr FROM leads_datax LEFT JOIN 6q_formfields_tbl ON leads_datax.field_ID = 6q_formfields_tbl.ID WHERE lead_ID = $lead_ID";
		return $this->query($qstr)->fetchAll(PDO::FETCH_KEY_PAIR);
	}
	
	function getLeadUserInfo(LeadModel $lead)
	{
		$usrq = $this->query('SELECT u.username, u.parent_id, u.super_user,
						IFNULL(r.main_site, "seedlogix.com") as domain,
						IFNULL(r.company, "SeedLogix") as company
						FROM `users` u 
						LEFT JOIN resellers r
						  ON (((u.parent_id = 0 AND r.admin_user = u.id) or r.admin_user = u.parent_id))
								WHERE u.id = ' . (int) $lead->user_id);
		
		return $usrq->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $lead_form_id
	 * @param int $campaign_id
	 *
	 * @return LeadFormModel
	 */
	function getLeadForm($lead_form_id, $campaign_id = 0)
	{
		if($campaign_id){
			return LeadFormModel::Fetch('id = %d AND (cid = %d OR multi_user = 1)', $lead_form_id, $campaign_id);
		}
		return LeadFormModel::Fetch('id = %d', $lead_form_id);
	}

	static function getOrderBy(array $qOpts, $direction = 'ASC')
	{
		if($qOpts['isCount']) return '';
		$orderCol	= $qOpts['orderCol'];
		$newRow = "";
		switch($orderCol)
		{
			case 'responders_count':
			case 'prospects_count':
			case 'created';
				break;
			default:
				$orderCol	=	'name';
		}
		if($qOpts['new_rows_ids']){
			$newRow = "l.id = " . join(' DESC, l.id = ', $qOpts['new_rows_ids']) . ' DESC, ';
		}
		return " ORDER BY $newRow $orderCol $direction";
	}

	function getLeadForms(array $args)
	{
		$qOpts	=	self::getQueryOptions($args);
		$table = 'lead_forms l';
		$fields	=	'l.*';
		// cfr, fields, table, where, order, limit
		$query	=	'SELECT %s %s FROM %s %s %s %s';

		$args['user_id']	=	$this->getActionUser()->getID();
		$whereArr	=	!empty($qOpts['where']) ? array($qOpts['where']) : array();
		$whereArr[]	=	'l.user_ID = :user_id';
		if($qOpts['cols'])
		{
			$fields	=	$qOpts['cols'];
		}
		if(isset($args['count_prospects']) && !$qOpts['isCount'])
		{
			$table .= ' left join (select form_ID, count(*) as count1 from prospects
				WHERE /* trashed = "0000-00-00" AND */ user_id = :user_id GROUP BY form_ID) cp ON (cp.form_ID = l.ID)';
			$fields .= ', coalesce(cp.count1, 0) as prospects_count';
//			$fields .= ', (select count(*) FROM prospects WHERE form_ID = l.ID AND trashed = "0000-00-00") as prospects_count';
		}
		if(isset($args['count_responders']) && !$qOpts['isCount'])
		{
			$table .= ' left join (select table_id, count(*) as count1 FROM auto_responders
				WHERE table_name = "lead_forms" AND user_id = :user_id AND trashed = "0000-00-00" GROUP BY table_id) cr ON (cr.table_id = l.ID)';
			$fields .= ', coalesce(cr.count1, 0) as responders_count';
		}
		if(isset($args['sSearch']))
		{
			$whereArr[]	=	'(l.name LIKE :sSearch)';
		}
		$query	=	sprintf($query, $qOpts['CALC_FOUND_ROWS'],
						$fields, $table,
				' WHERE ' . DBWhereExpression::Create($whereArr),
			self::getOrderBy($qOpts, $qOpts['orderDir']), $qOpts['limitstr']);
		$stmt	=	$this->prepare($query);

		unset($args['where'], $args['count_prospects'], $args['count_responders']);
		self::callPreparedQuery($stmt, $args);

		if($qOpts['getStatement'])
		{
			return $stmt;
		}

		return $stmt->fetchAll(PDO::FETCH_CLASS, 'LeadFormModel');
	}

	function SortFormInputField($field_ID, $index)
	{
		$this->query('SELECT @f_ID := form_ID, @indexa := sort, @indexb := 0 FROM 6q_formfields_tbl WHERE ID = ' . (int)$field_ID);
		$q = 'UPDATE 6q_formfields_tbl SET sort = :sort*10+5*if(:sort*10 < sort, -1, 1) WHERE ID = :field_ID';
		$stmt 	=	$this->prepare($q);
		$stmt->execute(array('sort' => $index, 'field_ID' => $field_ID));
		$newField = '';
		if($index == -1)
			$newField = 'id = ' . (int) $field_ID . ', ';
		$this->query("UPDATE 6q_formfields_tbl SET sort = (@indexb := @indexb+10) WHERE form_ID = @f_ID ORDER BY $newField sort ASC");
	}



	function SaveLeadFormField(array $data)
	{
		if(isset($data['ID']))
		{

		}
		$q	=	'INSERT INTO 6q_formfields_tbl
			SET form_ID = :form_ID, user_ID = (select user_ID FROM lead_forms WHERE id = :form_ID), `type` = :type,
				`size` = :size, label = :label, label_note = :label_note, required = :required, numeric = :numeric, default = :default,
				options = :options';
		$stmt	=	$this->prepare($q);
		self::callPreparedQuery($stmt, $data);

		return $stmt->rowCount();
	}

	function getLatestLead(){
		$q = "SELECT * FROM active_leads WHERE user_id = :user_id order by created LIMIT 1";
		$stmt = $this->prepare($q);
		$stmt->execute(array('user_id' => $this->getActionUser()->getID()));
		return $stmt->fetchObject('LeadModel');
	}

	function getEmailQueuedCollaborators(){
		$q = "SELECT n.name as form_name, h.name as hub_name,
                    ifnull(p.page_title, p2.page_title) as page_name,
                    f.search_engine, f.keyword, f.ip,
                    c.name as collab_name, c.email as collab_email, ifnull(r.main_site, 'seedlogix.com') as
                    reseller_domain,
                    f.*,
                    q.q_id,
group_concat(DISTINCT concat('<p><strong>', coalesce(x.label, d.label), ':</strong><br />', coalesce(x.valuestr, d.value), '</p>') separator '\n') as data
                    FROM collab_email_queue q
                    INNER JOIN campaign_collab c ON ( c.id = q.collab_id )
                    INNER JOIN leads f ON ( f.id = q.data_id )
                    INNER JOIN lead_forms n ON ( n.id = f.lead_form_id)
                    LEFT JOIN leads_data d ON ( d.lead_id = f. id)
                    LEFT JOIN leads_datax x ON ( x.lead_id = f. id)
LEFT JOIN hub h ON (h.id = f.hub_id)
LEFT JOIN hub_page2 p2 ON (p2.id = f.hub_page_id AND h.pages_version=2)
LEFT JOIN hub_page p ON (p.id = f.hub_page_id AND h.pages_version=1)
                    LEFT JOIN users u ON ( u.id = f.user_id )
                    LEFT JOIN resellers r ON (r.admin_user = if(u.parent_id = 0, u.id, u.parent_id)) WHERE q.data_type = 'lead' and date_sent = 0
                    GROUP BY q.q_id";
		return $this->query($q)->fetchAll(PDO::FETCH_OBJ);
	}

	public function updateCollabEmailSent($collaborator)
	{
		$q = "UPDATE collab_email_queue SET date_sent = CURDATE() WHERE q_id = {$collaborator->q_id}";
		return $this->query($q)->rowCount() == 1;
	}

	/**
	 * @param $lead_form_id
	 * @param array $args
	 * @return LeadModel[]|LeadModel
	 */
	public function getLeadsByFormId($lead_form_id, $args = array()){
		$qOpts = self::getQueryOptions($args, "trashed = 0 AND user_id = :user_id");
		$q = "SELECT * FROM leads WHERE lead_form_id = :lead_form_id AND {$qOpts['where']}";
		$args['user_id'] = $this->getActionUser()->getID();
		$args['lead_form_id'] = $lead_form_id;
		$stmt = $this->prepare($q);
		$stmt->execute($args);

		if($qOpts['returnSingleObj'])
			return $stmt->fetchObject('LeadModel');

		return $stmt->fetchAll(PDO::FETCH_CLASS, 'LeadModel');
	}
	public function getLeadParentOptoutURL($parent_id){

		$q = "SELECT main_site FROM resellers WHERE admin_user = '.$parent_id.'";
		$statement = $this->prepare($q);
		$statement->execute();
		//$stmt = $this->prepare($q);
		//$stmt->execute();

		//return $stmt->fetchObject(PDO::FETCH_OBJ);
		return $statement->fetch(PDO::FETCH_ASSOC);

	}
}
