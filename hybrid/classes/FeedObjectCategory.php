<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FeedObjectCategory
 *
 * @author Amado
 */
class FeedObjectCategory extends FeedObject
{
    function getPageRow() {
        return array();
    }
    function getFeedObjectTemplateFilename() {
        return 'feed-category.php';
    }
    
    protected $result;
    protected $classname;
    protected $found_rows   =   0;
    
    function getRequiredTags() {
        return array();
    }

	function getFoundRows(){
	return $this->found_rows;
	}    
    function __construct(PDOStatement $p, $classname, $fullname, $feedid, $current_path, $current_page = 0, $perpage  =   20, $search_str	=	'') {
        
        $this->currentpage  =   $current_page;
        
	$this->search_str	=	$search_str;
        $this->found_rows   =   Qube::GetPDO()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $this->perpage  =   $perpage;
        $this->foundpages =   ceil($this->found_rows/$perpage);
        $this->currentpath  =   $current_path;
        
        if($fullname){
            $prepare    =   Qube::GetPDO()->prepare('SELECT * FROM feedobjects_categories WHERE FULL_NAME = ? AND FEEDID = ?');
            $prepare->execute(array($fullname, $feedid));

            if($prepare->rowCount())
            {
                $category   =   $prepare->fetch(PDO::FETCH_ASSOC);
                foreach($category as $key => $value)
                    $this->$key =   $value;
            }
       }
        $this->result   =   $p;
        $this->classname    =   $classname;
        
    }
    
    function pagination(){
	$cp	=	$this->currentpage;
	$tp	=	$this->foundpages;

	if($cp > 10){
		$this->printpagination_range(1, 5, 1);
		echo '<span class="dots">...</span>';

	}

	if($tp > 10){
		$sp	=	max(1, $cp-5);
		$ep	=	min($tp, $cp+5);
		$this->printpagination_range($sp, $ep, $cp);
		if($ep < $tp)
		{
			echo '<span class="dots">...</span>';
			$this->printpagination_range(max($tp-5, $ep+1), $tp, $cp);
		}
	}else
		$this->printpagination_range(1,$tp, $cp);
}

	function printpagination_range($a, $b, $c){
	$params	=	$this->search_str ? '?search=' . htmlentities(urlencode($this->search_str), ENT_QUOTES) : '';
        for($i  =   $a; $i <= $b ; $i++){
            echo '<a href="/', htmlentities($this->currentpath, ENT_QUOTES),  '/', $i, '/' . $params . '"', $i == $c ? ' class="currentpage"' : '',
                       '>',  $i, '</a>';
        }
    }
    
    function fetchAll(){
        return $this->result->fetchAll(PDO::FETCH_CLASS, $this->classname);
    }
}
