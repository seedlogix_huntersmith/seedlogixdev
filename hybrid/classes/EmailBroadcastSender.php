<?php

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/12/16
 * Time: 1:25 AM
 */
class EmailBroadcastSender{
	/** @var  LeadsDAO */
	private $leadsDataAccess;

	/** @var EmailBroadcastDataAccess */
	private $broadCastDataAccess;

	/** @var  Logger */
	private $logger;

	/** @var  LeadDataAccess */
	private $leadDataAccess;

	public function __construct(){
		$this->leadDataAccess = new LeadDataAccess();
		$this->leadsDataAccess = new LeadsDAO();
		$this->broadCastDataAccess = new EmailBroadcastDataAccess();

		$this->leadDataAccess->setActionUser($this->leadDataAccess->createSystemUser());
		$this->leadsDataAccess->setActionUser($this->leadDataAccess->createSystemUser());
		$this->broadCastDataAccess->setActionUser($this->leadDataAccess->createSystemUser());
	}

	public function run(){
		$emailers = $this->broadCastDataAccess->getPendingBroadcasts();
		$this->logger->logINFO("Found %d pending broadcasts", count($emailers));

		foreach($emailers as $emailer){
			$leads = $this->getLeadsFromEmailer($emailer);

			$leadsSent = $emailer->getLeadsSent();


			/** @var LeadModel[] $toSendLeads */
			$toSendLeads = array_filter($leads, function ($lead) use($leadsSent){
				return !in_array($lead->id, $leadsSent);
			});


			if($toSendLeads){

				$leadsSentNow = array();
				$emailsSent = 0;
				$errorNum = 0;
				$emails = array();
				$errors = array();

				foreach($toSendLeads as $toSendLead){

					$from = $this->getFrom($emailer);
					$to = $this->getTo($toSendLead);
					$body = $this->getBody($emailer, $toSendLead);
					$smtp = $this->getSmtpServer();

					$subject = $this->getSubject($emailer, $toSendLead);
					$headers = array('From' => $from,
						'To' => $to,
						'Subject' => $subject,
						'Content-Type' => 'text/html; charset=ISO-8859-1',
						'Reply-To' => $from,
						'MIME-Version' => '1.0');

					if($toSendLead->lead_email){

						if(!in_array(strtolower($toSendLead->lead_email), $emails))
							$mailed = $smtp->send($to, $headers, $body);
						else
							$mailed = true;

						if($mailed && !PEAR::isError($mailed)){
							$emailsSent++;
							$emails[$emailsSent] = strtolower($toSendLead->lead_email); //add email to array of emails

							//update string of users_sent
							$leadsSent[] = $toSendLead->getID();
							$leadsSentNow[] = $toSendLead->getID();

						} else {
							$data['error'] = 1;
							$errors[$errorNum] = "Lead ID: ".$toSendLead->getID()."\nEmail: ".$toSendLead->lead_email;
							if($mailed) $errors[$errorNum] .= "\nReason:\n".addslashes($mailed->getMessage());
							$errorNum++;
						}
					}
				}
				$query = "UPDATE lead_forms_emailers SET leads_sent = '".join("::", $leadsSent)."' WHERE id = '".$emailer->getID()."'";
				$statement = $this->leadDataAccess->query($query);
				$statement->execute();
				$this->logger->logINFO("%d Leads was sent from emailer id = %d", count($leadsSentNow), $emailer->getID());
				$this->logger->logDEBUG(join(', ',$leadsSentNow));

				if($errors){
					$this->logger->logERROR("%d Leads was not sent due to an error", $errorNum);
					foreach($errors as $error){
						$this->logger->logERROR($error);
					}
				}
			}else{
				$this->logger->logINFO("No leads found to send broadcast for Braodcast id = %d", $emailer->getID());
			}
		}

		$this->logger->logINFO("Finish broadcast");
	}

	public function getFrom(LeadFormEmailerModel $emailer){
		$query = "SELECT company FROM user_info WHERE user_info.user_id = (SELECT lead_forms_emailers.user_id FROM lead_forms_emailers WHERE lead_forms_emailers.id = '".$emailer->getID()."')";
		$query2 = "SELECT username FROM users WHERE id = (SELECT user_id FROM lead_forms_emailers WHERE lead_forms_emailers.id = '".$emailer->getID()."')";

		$statementCompany = $this->leadDataAccess->query($query);
		$statementCompany->execute();

		$statementUsername = $this->leadDataAccess->query($query2);
		$statementUsername->execute();

		$username = $statementUsername->fetchColumn();

		$company = $statementCompany->fetchColumn();


		if(!$emailer->from_field)
			return '"'.$company.'" <'.$username.'>';

		return stripslashes($emailer->from_field);
	}

	public function getTo(LeadModel $lead){
		if($lead->lead_name){
			return '"'.$lead->lead_name.'" <'.$lead->lead_email.'>';
		}

		return $lead->lead_email;
	}

	/**
	 * @return Mail_smtp
	 */
	public function getSmtpServer(){
		if(!class_exists('Mail')){
			require_once 'Mail.php';
		}

		$mail = new Mail();
		$host = "relay.jangosmtp.net";
		$port = '25';
		$smtp = $mail->factory('smtp',
			array ('host' => $host,
				'port' => $port,
				'auth' => false));

		return $smtp;
	}


	/**
	 * @param LeadFormEmailerModel $emailer
	 *
	 * @return LeadModel[]
	 */
	public function getLeadsFromEmailer(LeadFormEmailerModel $emailer){
		$query = "SELECT l.id, l.lead_name, l.lead_email FROM leads l, prospects p
			  WHERE l.user_id = '{$emailer->user_id}'
			  AND l.lead_form_id = '{$emailer->lead_form_id}'
			  AND p.lead_ID = l.id
			  AND p.user_ID = '{$emailer->user_id}'
			  AND p.trashed = 0
			  AND l.optout != 1 ";

		$statement = $this->leadsDataAccess->query($query);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_CLASS, 'LeadModel');
	}

	public function setLogger(Logger $logger){
		$this->logger = $logger;
	}

	/**
	 * @param LeadFormEmailerModel $emailer
	 * @param LeadModel            $lead
	 *
	 * @return string
	 */
	public function getSubject(LeadFormEmailerModel $emailer, LeadModel $lead)
	{
		$subject = $this->parseContent($lead, $emailer->subject);
		if($lead->lead_name)
			return stripslashes(str_replace("#name", $lead->lead_name, $subject));
		return stripslashes($subject);
	}

	public function getBody(LeadFormEmailerModel $emailer, LeadModel $lead){
		$body = $emailer->getBody();
		$body = $this->parseContent($lead, $body);

		if($lead->lead_name){
			$body = str_replace("#name", $lead->lead_name, $body);
		}

		$main_site = ResellerModel::Fetch('admin_user = %d', $lead->parent_id);

		$body .= '<br /><br />'.$emailer->contact.'<br />'.$emailer->anti_spam;
		$body .= '<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0" cellspacing="0" align="center" width="660">
						  <tbody>
							<tr>
							  <td valign="top" align="left"><p><br />
								  TO UNSUBSCRIBE: This email was sent to you because you filled out our contact form.
								   To remove yourself from our email program, please <a href="http://'.$main_site->main_site.'/?page=client-optout&email='.$lead->lead_email.'&pid='.$lead->getID().'" target="_blank" style="color:#666666; text-decoration:underline;" >click here to opt-out.</a></p></td>
							</tr>
						  </tbody>
						</table>';

		return $body;
	}

	/**
	 * @param LeadModel $lead
	 * @param           $content
	 *
	 * @return mixed
	 */
	public function parseContent(LeadModel $lead, $content)
	{
		$leadData = $lead->getData($this->leadsDataAccess);
		$content = preg_replace_callback('/\[\[([^\]]+)\]\]/', function ($matches) use ($leadData) {
			if (isset($leadData[$matches[1]]))
				return $leadData[$matches[1]];
			return $matches[0];
		}, $content);
		return $content;
	}
}