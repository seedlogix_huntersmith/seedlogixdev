<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HybridRPC
 *
 * @author amado
 */
class HybridRPC extends HybridControllerLauncher
{
    const KEY   =   'sOK59kbhnhW41vNY1qV6';
    static function RunServer(){
        $CallInfo   = VarContainer::getContainer($_GET);
        
        if(!$CallInfo->checkValue('$Ctrl', $Controller)){
            throw new Exception('Fail.');
        }
            
        if(!$CallInfo->checkValue('$action', $action)){
            throw new Exception('fail');
        }
        
        if(!$CallInfo->checkValue('$KEY', $apikey) || $apikey !== self::KEY){
            throw new Exception('Security Exception');
        }
        
        if(!$CallInfo->checkValue('$user_id', $user_id)){
            throw new Exception('Security Exception');
        }
        
//        Zend_Session::setId('amado');
        $Launcher   =   new self($Controller);
        
        Zend_Session::setSaveHandler(new DummySessionSaver());  // dont save shit
        
        $Session    =   $Launcher->getWebNamespace('RPC');
        $Session->access_user_ID   =   $user_id;
        
        $Result =   $Launcher->Run($Session, $action);
//        $ST =   new Zend_Session_Namespace('RPC');
        
        Header('Content-Type: application/json');
            
        $Result->display();
        
//        echo session_save_path();
//        var_dump($Result);
#        $ST->dabomb =   time();
#        echo $ST->dabomb;
    }


    
    static function RunClientForward($url, $controller, $action, $user_id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
#        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, true);

        $dataPOST = $_POST;
	$data	=	$_GET;

        $data['$action']    =   $action;
        $data['$Ctrl']      =    $controller;
        $data['$KEY']   =   self::KEY;
        $data['$user_id']   =   $user_id;

        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPOST);

        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }
}

