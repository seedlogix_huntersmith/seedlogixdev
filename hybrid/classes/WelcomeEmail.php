<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WelcomeEmail
 *
 * @author amado
 */
class WelcomeEmail extends EmailNotification
{
	/**
	 *
	 * @var UserModel
	 */
	protected $UserModel	=	NULL;
	
	
	function __construct(UserModel $U) {
		$this->UserModel	=	$U;
	}
	
	function getContent()
	{
		$data	=	$this->UserModel->toArray();
		$body	=	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0"/>
<meta name="format-detection" content="telephone=no"/>
<style type="text/css">

			/* Resets: see reset.css for details */
			.ReadMsgBody { width: 100%; background-color: #ffffff;}
			.ExternalClass {width: 100%; background-color: #ffffff;}
			.ExternalClass, .ExternalClass p, .ExternalClass span,
			.ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
			#outlook a{ padding:0;}
			body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0;}
			body{ -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
			html{width:100%;}
			table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;}
			table td {border-collapse:collapse;}
			table p{margin:0;}
			br, strong br, b br, em br, i br { line-height:100%; }
			div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
			h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
			span a { text-decoration: none !important;}
			a{ text-decoration: none !important; }
			ul{list-style: none; margin:0; padding:0;}
			img{height: auto !important; line-height: 100%; outline: none; text-decoration: none; display:block !important;
				-ms-interpolation-mode:bicubic;}
			.yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
			.yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
			/*mailChimp class*/
			.default-edit-image{
                    height:20px;
			}
			.tpl-repeatblock {
                    padding: 0px !important;
				border: 1px dotted rgba(0,0,0,0.2);
			}
		@media only screen and (max-width: 640px){
                    /* mobile setting */
                    table[class="container"]{width:100%!important; max-width:100%!important; min-width:100%!important;
				padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
			td[class="container"]{width:100%!important; padding-left:20px!important; padding-right:20px!important; clear: both;}
			table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
			table[class="full-width-center"] {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			table[class="auto-center"] {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			*[class="auto-center-all"]{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			*[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			table[class="col-3"]{width:30.35%!important; max-width:100%!important;}
			table[class="col-2"]{width:47%!important; max-width:100%!important;}
			td[class="col-top"]{display:table-header-group !important;}
			td[class="col-bottom"]{display:table-footer-group !important;}
			*[class="full-block"]{width:100% !important; display:block !important;}
			/* image */
			td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important;}
			/* helper */
			table[class="space-w-20"]{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
			table[class="space-w-20"] td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
			table[class="space-w-25"]{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
			table[class="space-w-25"] td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.4% !important;}
			*[class="h-20"]{display:block !important;  height:20px;}
			*[class="h-30"]{display:block !important; height:30px;}
			*[class="h-40"]{display:block !important;  height:40px;}
			*[class="remove-640"]{display:none !important;}
		}
		@media only screen and (max-width: 479px){
                    /* mobile setting */
                    table[class="container"]{width:100%!important; max-width:100%!important; min-width:124px!important;
				padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
			td[class="container"]{width:100%!important; padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
			table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
			table[class="full-width-center"] {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
			*[class="auto-center-all"]{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			*[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
			table[class="col-3"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
			table[class="col-2"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
			*[class="full-block-479"]{display:block !important; width:100% !important; text-align: center!important; clear: both;}
			/* image */
			td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
			td[class="image-min-80"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
			td[class="image-min-100"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
			/* halper */
			table[class="space-w-20"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
			table[class="space-w-20"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
			table[class="space-w-25"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
			table[class="space-w-25"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
			*[class="remove-479"]{display:none !important;}
		}
		</style>
</head>
<body  style="font-size:12px; width:100%; height:100%;">
<table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#f7f7f7;">
				<!--START LAYOUT-1 ( TOP LOGO AND MENU ) -->
				<tr>
					<td align="center" valign="top" style="background-color: rgb(255, 255, 255);">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="min-width: 600px; padding-left: 20px; padding-right: 20px; background-color: rgb(255, 255, 255);">
							<!-- start space -->
							<tr>
								<td valign="top" height="30" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<!-- start content container-->
							<tr>
								<td valign="top">
									<table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width">
										<tr>
											<td valign="middle" class="full-block">
												<!-- start logo -->
												<table align="left" border="0" cellspacing="0" cellpadding="0" class="full-width-center">
													<tr>
														<td align="center" valign="top">
															<a href="#" style="font-size: inherit; text-decoration: none;">
																<img src="https://6qube.com/hubs/themes/cloudmu/img/logo-alternative.png" width="108" style="max-width: 200px; display: block !important;" alt="logo-top" border="0" hspace="0" vspace="0">
															</a>
														</td>
													</tr>
												</table>
												<!-- end logo -->
												</td>
											<td valign="middle" class="full-block">
												<!-- height space for mobile -->
												<table width="20" border="0" cellpadding="0" cellspacing="0" align="left" class="full-width" style="height:1px; mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;">
													<tr>
														<td height="1" class="h-30" style="border-collapse: collapse; line-height: 1px; font-size: 1px; display: block;">
														</td>
													</tr>
												</table>
												<!-- height space for mobile -->
												</td>
											<td valign="middle" class="full-block">
												<!-- start menu -->
												<table align="right" border="0" cellpadding="0" cellspacing="0" class="full-width-center">
													<tr>
														<td align="center" class="block" style="font-size: 14px;
														color: rgb(51, 51, 51); font-weight: normal; text-align:
														center; font-family: Arial, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 24px;">
															<span style="text-decoration: none; color: #333333; font-size: inherit;">
																<a href="http://www.seedlogix.com/software/production-tools/" style="color: rgb(51, 51, 51); font-size: inherit; text-decoration: none;">Software &nbsp;</a> |
																<a href="http://www.seedlogix.com/pricing/" style="color: rgb(51, 51, 51); font-size: inherit; text-decoration: none;">&nbsp;Pricing</a> | <a href="http://login.seedlogix.com/" style="color: rgb(51, 51, 51); font-size: inherit; text-decoration: none;">&nbsp;Login</a>
															</span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- end content container-->
							<!-- start space -->
							<tr>
								<td valign="top" height="30" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
						</table>
						<!-- end container -->
					</td>
				</tr>

				<tr>
					<td align="center" valign="top" class="container" style="background-color: #f7f7f7;">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="min-width:600px; background-color:#f7f7f7; padding-left:20px; padding-right:20px;">
							<!-- start space -->
							<tr>
								<td valign="top" height="60" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<tr>
								<td valign="top">
									<table idth="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width">
													<tr>

														<td valign="top" class="full-block">
															<!-- start col right -->
															<table width="100%" align="right" border="0" cellpadding="0" cellspacing="0" class="full-width">
																<!-- start space -->
																<tr>
																	<td valign="top" height="20" style="line-height: 1px; font-size: 1px; display: block;">
																	</td>
																</tr>
																<!-- end space -->
																<tr>
																	<td align="left" style="font-size: 20px; color: rgb(51, 51, 51); font-weight: bold; text-align: left; font-family: Arial, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 24px;">
																		<span style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit;">
																			<a href="http://login.seedlogix.com/" style="color: rgb(51, 51, 51); font-weight: bold; font-size: inherit; text-decoration: none;">Welcome '.$data['username'].'</a>
																			<br>
																		</span>
																	</td>
																</tr>
																<!-- start space -->
																<tr>
																	<td valign="top" height="5" style="line-height: 1px; font-size: 1px; display: block;">
																	</td>
																</tr>
																<!-- end space -->
																<tr>
																	<td align="left" style="font-size: 14px; color: rgb(126, 182, 224); font-weight: normal; text-align: left; font-family: Arial, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 24px;">
																		<span style="color: rgb(126, 182, 224); font-style: italic; font-size: inherit;">Below is your password you can use to login.</span>
																	</td>
																</tr>
																<!-- start space -->
																<tr>
																	<td valign="top" height="15" style="line-height: 1px; font-size: 1px; display: block;">
																	</td>
																</tr>
																<!-- end space -->
																<tr>
																	<td align="left" style="font-size: 14px; color: rgb(153, 153, 153); font-weight: normal; text-align: left; font-family: Arial, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 24px;">
																		<span style="text-decoration: none; color: #999999; font-weight: normal; font-size: inherit;">
																			'.$data['password'].'
																		</span>
                                                                        <br /><br />You can change this password by clicking on your profile image as soon as you login.
																	</td>
																</tr>
																<!-- start space -->
																<tr>
																	<td valign="top" height="15" style="line-height: 1px; font-size: 1px; display: block;">
																	</td>
																</tr>
																<!-- end space -->

															</table>
															<!-- start col right -->
														</td>
													</tr>

												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- start space -->
							<tr>
								<td valign="top" height="60" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
						</table>
						<!-- end container -->
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" class="container" style="background-color: #ffffff;">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#ffffff; min-width:600px;  padding-left:20px; padding-right:20px;">
							<!-- start space -->
							<tr>
								<td valign="top" height="40" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<!-- start content container-->
							<tr>
								<td valign="top">
									<table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width">
										<tr>
											<td valign="top">
												<!-- start content -->
												<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
													<!-- start image -->
														<tr>
															<td align="center" valign="top">
																<a href="#" style="font-size: inherit; text-decoration: none;">
																	<img src="https://6qube.com/hubs/themes/cloudmu/img/logo-alternative.png" width="108" style="max-width: 200px; display: block !important;" alt="logo-footer">
																</a>
															</td>
														</tr>
														<!-- end image -->
												</table>
												<!-- end content -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- end content container-->
						</table>
						<!-- end container -->
					</td>
				</tr>
				<!-- END LAYOUT-22 ( LOGO FOOTER ) -->

				<!-- START LAYOUT-23 ( FOOTER SOCIAL ) -->
				<tr>
					<td valign="top" class="container" align="center" style="background-color: #ffffff;">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#ffffff; min-width:600px; ">
							<!-- start space -->
							<tr>
								<td valign="top" height="40" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<!-- start content container -->
							<tr>
								<td valign="top" align="center">
									<table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">
										<tr>
											<td valign="top" align="center">
												<table width="auto" border="0" align="center" cellpadding="0" cellspacing="0">
													<tr>
														<td height="20" align="center" valign="middle">
															<a href="https://www.facebook.com/seedlogix" style="font-size: inherit; text-decoration: none;">
																<img src="https://6qube.com/emails/themes/seedlogix/images/icon-facebook.png" width="20" alt="icon-facebook" style="max-width:20px; display:block !important;" border="0" hspace="0" vspace="0">
															</a>
														</td>
														<td style="padding-left:8px;" height="20" align="center" valign="middle">
															<a href="https://twitter.com/seedlogix" style="font-size: inherit; text-decoration: none;">
																<img src="https://6qube.com/emails/themes/seedlogix/images/icon-twitter.png" width="20" alt="icon-twitter" style="max-width:20px; display:block !important;" border="0" hspace="0" vspace="0">
															</a>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- end content container -->
							<!-- start space -->
							<tr>
								<td valign="top" height="40" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<!-- start divider  -->
							<tr>
								<td valign="top" align="center">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:0 auto; height:1px; background-color:#e9e9e9;">
										<tr>
											<td height="1" valign="top" style="line-height: 1px; font-size: 1px; display: block;">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- end divider  -->
						</table>
						<!-- end container -->
					</td>
				</tr>
				<!-- END LAYOUT-23 ( FOOTER SOCIAL ) -->

				<!--START LAYOUT-24 ( FOOTER COMPANY ) -->
				<tr>
					<td align="center" valign="top" class="container" style="background-color:#ffffff;">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="min-width:600px; background-color:#ffffff;">
							<!-- start space -->
							<tr>
								<td valign="top" height="40" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
							<tr>
								<td valign="top">
									<table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width">
										<!-- start copyright -->
											<tr>
												<td align="center" style="font-size: 14px; color: rgb(51, 51, 51); font-weight: normal; text-align: center; font-family: Arial, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 24px;">
													<span style="color: #333333; font-size: inherit;">Copyright &copy; 2015 SeedLogix. All rights reserved.</span>
												</td>
											</tr>
										<!-- end copyright -->
									</table>
								</td>
							</tr>
							<!-- start space -->
							<tr>
								<td valign="top" height="15" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
						</table>
						<!-- end container -->
					</td>
				</tr>
				<!--END LAYOUT-24 ( FOOTER COMPANY ) -->

				<!--START LAYOUT-25 ( FOOTER UNSUBSCRIBE ) -->
				<tr>
					<td align="center" valign="top" class="container" style="background-color:#ffffff;">
						<!-- start container -->
						<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="min-width:600px; background-color:#ffffff;">
							<!-- start space -->
							<tr>
								<td valign="top" height="15" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->

							<!-- start space -->
							<tr>
								<td valign="top" height="30" style="line-height: 1px; font-size: 1px; display: block;">
								</td>
							</tr>
							<!-- end space -->
						</table>
						<!-- end container -->
					</td>
				</tr>
				<!--END LAYOUT-25 ( FOOTER UNSUBSCRIBE ) -->
			</table></body>
</html>';
		
		return $body;
	}
	
	function getSubject()
	{
		$subj	=	"Welcome to SeedLogix - Your Account Details";
		
		return $subj;
	}
}
