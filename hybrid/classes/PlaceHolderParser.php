<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlaceHolderParser
 *
 * @author Amado
 */
class PlaceHolderParser {

	//put your code here

	protected $_vars = array();
//	protected $_func	=	array();
	
	protected $_eval = array();

	protected $_stack	=	array();
	
	protected $wrap1	=	'((';
	protected $wrap2	=	'))';
	protected $wrap1q	=	'';
	protected $wrap2q	=	'';
	protected $matcher = '\w+';
	
	protected $returncallback = NULL;
	
	function setReturnCALLBACK($cb)
	{
		$this->returncallback = $cb;
	}
	
	function callReturnCALLBACK($var, $value)
	{
		if($this->returncallback)
		{
			return call_user_func($this->returncallback, $var, $value, $this);
		}
		
		return $value;
	}
	
	function __construct(array $vars, $wrap1 = '((', $wrap2 = '))', $matcher = NULL)
	{
		$this->_vars = $vars;
		if($matcher)
			$this->matcher = $matcher;
		$this->wrap1	=	$wrap1;
		$this->wrap2	=	$wrap2;
		$this->wrap1q	=	preg_quote($wrap1);
		$this->wrap2q	=	preg_quote($wrap2);
	}

	function FindTags($str) {
		preg_match_all("#$this->wrap1q({$this->matcher})$this->wrap2q#", $str, $tags);
		return array_unique($tags[1]);
	}

	/**
	 * Evaluate
	 * 
	 * @param type $var
	 */
	function evSTR($str) {

		if(empty($str)) return '';
		$tags = $this->FindTags($str);
		if (count($tags) > 0) {
			foreach ($tags as $replace) {
				$str = str_replace("{$this->wrap1}$replace{$this->wrap2}", $this->evVAR($replace), $str);
			}
			return $this->evSTR($str);
		}
		return $str;
	}

	function evVAR($var) {
		if (!isset($this->_eval[$var])) {
			$varval = $this->_vars[$var];
			if(isset($this->stack[$var]))
			{
					$val	=	'(recursion error: ' . implode("/", array_keys($this->stack)) . ')';
			}else{
				$this->stack[$var]	=	1;
				// pass a function as a var: ('funcname' => array($callback, $args), ..)
				if(is_array($varval) && is_callable($varval[0]))
				{
					$val	= call_user_func_array($varval[0], $varval[1]);
				}else{
					$val	=	$this->callReturnCALLBACK($var, $this->evSTR($varval));
				}
				unset($this->stack[$var]);
			}
			$this->_eval[$var] = $val;
		}

		return $this->_eval[$var];
	}

	/**
	 * @return array
	 */
	public function getVars(){
		return $this->_vars;
	}

}
