<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\Authorizer;

/**
 * Description of UserLevelAuthorizer
 *
 * @author amado
 */
class UserLevelAuthorizer extends PermissionCheckerAuthorizer
{
    /**
     * @var \PermissionsSet
     */
    private $permissionSet;

    function init(\PermissionsSet $p, \UserWithRolesModel $user)
    {
        parent::init($p, $user);
        $this->permissionSet = $p;
    }

    function check($p, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL)
    {
			if(parent::check($p, $id, $SED)) return true;
			
        if($this->permissionSet->hasPermission($p, 0) || $this->permissionSet->hasPermission($p, $id)){
            return parent::check($p, $id, $SED);
        }
		return false;
    }

    function edit($type, $id){
			
			if(parent::edit($type, $id)) return true;
			
        switch($type){
						
            case 'campaigns':
			case 'collab':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM campaigns WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $this->permissions->getUser()->getID()));
                return $stmnt->fetchColumn();
            case 'touchpoint':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $this->permissions->getUser()->getID()));
                return $stmnt->fetchColumn();
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'SOCIAL':
            case 'MOBILE':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id AND hub.touchpoint_type =  '$type'");
                $stmnt->execute(array('id' => $id, 'user_id' => $this->permissions->getUser()->getID()));
                return $stmnt->fetchColumn();
            case 'BLOG':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM blogs WHERE id = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $this->permissions->getUser()->getID()));
                return $stmnt->fetchColumn();
            case 'slide':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM slides WHERE ID = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $this->permissions->getUser()->getID()));
                return $stmnt->fetchColumn();
            case 'blogpost':
                return $this->isObjectOwner('blogpost', $id);
            case 'leadform':
                return $this->isObjectOwner('leadform', $id);
            case 'prospect':
                return $this->isObjectOwner('prospect', $id);
            case 'responder':
                return $this->isObjectOwner('responder', $id);
            case 'broadcast':
                return $this->isObjectOwner('broadcast', $id);
			case 'report':
				return $this->isObjectOwner('report', $id);
            case 'contact':
                return $this->isObjectOwner('contact', $id);
			case 'accounts':
                return $this->isObjectOwner('accounts', $id);
			case 'products':
                return $this->isObjectOwner('products', $id);
			case 'proposals':
                return $this->isObjectOwner('proposals', $id);
			case 'socialaccounts':
                return $this->isObjectOwner('socialaccounts', $id);
			case 'socialposts':
                return $this->isObjectOwner('socialposts', $id);
            case 'note':
                return $this->isObjectOwner('prospect', $id);
            case 'ranking':
                return $this->isObjectOwner('keywordCampaign', $id);
            case 'keyword':
                return $this->isObjectOwner('keyword', $id);
            case 'formfield':
                return $this->isObjectOwner('formfield', $id);
        }
        return false;
    }


    function view($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL){
        if(parent::view($type, $id, $SED)) return true;

        $actionUser = $this->permissions->getUser();
        if($actionUser->can("edit_$type", $id)) return true;

        switch($type){

            case 'campaigns':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM campaigns WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'touchpoint':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'SOCIAL':
            case 'MOBILE':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id AND hub.touchpoint_type =  '$type'");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'BLOG':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM blogs WHERE id = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
			case 'leadform':
				$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM lead_forms LEFT JOIN lead_forms_roles ON lead_forms.id = lead_forms_roles.lead_form_id LEFT JOIN 6q_roles ON 6q_roles.ID = lead_forms_roles.role_id WHERE (lead_forms.user_id = :user_id OR (lead_forms_roles.role_id = (SELECT 6q_userroles.role_ID FROM 6q_userroles WHERE 6q_userroles.user_ID = :user_id) AND lead_forms.multi_user = 1))");
				$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
				return $stmnt->fetchColumn();
        }
        return false;
    }

    function trash($type, $id){
        if(parent::trash($type, $id)) return true;

        $actionUser = $this->permissions->getUser();

        switch($type){
            case 'campaigns':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM campaigns WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'touchpoint':
                $stmnt = \Qube::Start()->GetPDO()->prepare('SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id');
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'SOCIAL':
            case 'MOBILE':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id AND hub.touchpoint_type =  '$type'");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'BLOG':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM blogs WHERE id = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'note':
                return true;
        }

        return false;
    }

    function create($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL)
    {
        if(parent::create($type, $id, $SED)) return true;
        $actionUser = $this->permissions->getUser();
        switch($type){
            case 'slide':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM hub WHERE id = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'SOCIAL':
            case 'MOBILE':
            case 'BLOG':
                $stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM campaigns WHERE id = :id AND user_id = :user_id");
                $stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
                return $stmnt->fetchColumn();
            case 'blogpost':
                return $this->isObjectOwner('BLOG', $id);
			case 'report':
				if($id == 0) return true; // Means the report will be created without campaign and doesn't require validation
			case 'blogfeed':
			case 'leadform':
            case 'keywordCampaign':
            case 'form':
            case 'responder':
            case 'broadcast':
            case 'collab':
                return $this->isObjectOwner('campaign', $id);
            case 'keyword':
                return $this->isObjectOwner('keywordCampaign', $id);
            case 'page':
            case 'nav':
                return $this->isObjectOwner('hub', $id);
//            case 'campaigns': // disabled. requires explicit create_campaigns permission.
            case 'emailer':
            case 'networklisting':
            case 'users':
            case 'reseller':
            case 'role':
            case 'note':
                return true;
        }
        return false;
    }


}
