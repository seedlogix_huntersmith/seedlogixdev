<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

namespace Qube\Hybrid\Authorizer;
use Qube\Hybrid\DataAccess\AccountDataAccess;


/**
 * Description of ResellerLevelAuthorizer
 *
 * @author amado
 */
class AdminLevelAuthorizer extends UserLevelAuthorizer
{    
    function init(\PermissionsSet $p, \UserWithRolesModel $user)
	{
			parent::init($p, $user);
			
        // init default permissions that come with is_reseller flag
        $p->setPermission('is_admin');
	}
	
    function impersonate($user_id){
        // impersonte users where, they are sub-users of logged in user, OR, 
        // they are sibling-users (have the same parent-user)
        $id =   \Qube::Start()->query('SELECT id FROM %s WHERE id = %d AND parent_id != 0 AND parent_id = %d',
                    'UserModel', $user_id, $this->permissions->getUser()->getParentID())->fetchColumn();
        
        return $id == $user_id;
    }
    
		function view($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL){
			if(parent::view($type, $id)) return true;
			$actionUser = $this->permissions->getUser();
			$table = '';
			switch($type)
			{
                case 'billing':
				case 'settings':
				case 'branding':
				case 'trash':
				case 'rankings':
				case 'customforms':
				case 'emailmarketing':
				case 'prospects':
				case 'contacts':
				case 'accounts':
				case 'networks':
				case 'reports':
				case 'admin':	// access admin views
					return true;
				case 'campaigns':
					$table = 'campaigns';
					break;
				case 'touchpoint':
				case 'WEBSITE':
				case 'LANDING':
				case 'PROFILE':
				case 'SOCIAL':
				case 'MOBILE':
					$table = 'hub';
					break;
				case 'BLOG':
					$table = 'blogs';
					break;
				case 'users':
                    return true;
/*
					$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM users WHERE id = :id AND parent_id = :user_id");
					$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
					return $stmnt->fetchColumn();
*/
			}

			if(!empty($table)){
				$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM $table WHERE id = :id AND user_id = :user_id");
				$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
				return $stmnt->fetchColumn();
			}
        return false;
    }
	
	
	/**
	 * Count the user posts
	 */
	function verifyPostCount()
	{
		$bda	=	new BlogDataAccess();
		$bda->getResellerPostCount();
	}
	
	/**
	 * All Admins can create users.. as long as their user count remains bellow their subscription limits.
	 * 
	 * @todo move this to a specialized class?
	 * 
	 */
	function verifyCanCreateUser(UserDataAccess $uda)
	{
	    return $this->d;
	}
	
//	function create($type)
//	{
//		switch($type)
//		{
//			case 'users':
//				return $this->getLimitVerify(new \UserDataAccess())->canCreateUser();
//
//			case 'BLOG':
//				return $this->getLimitVerify(new \UserDataAccess())->canCreateBLOG();
//			case 'WEBSITE':
//				return $this->getLimitVerify(new \UserDataAccess())->canCreateWEBSITE();
//
//			case 'blogpost':
//				return $this->verifyPostCount();
//		}
//
//
//		return false;
//	}
	
	function save($type, $id)
	{
		switch($type)
		{
			// adminss are only allowed to edit their own reseller info.
			case 'reseller':
				return $this->user->getID()	==	$id;
		}
		
		return false;
	}
	
	function edit($type, $id)
	{
		if(parent::edit($type, $id)) return true;
		$actionUser = $this->permissions->getUser();
		switch($type)
		{
			case 'responder':
			case 'leadform':	// @todo implement logic
			case "blogfeed":
    				return $id == 0;
			case 'blogpost':
					return true;
			case 'users':
				if($id == 0) return true; //Is an admin creating a new user, It's ok to let them create
				$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM users WHERE id = :id AND parent_id = :user_id");
				$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
				return $stmnt->fetchColumn();
			case 'role':
				return $this->isObjectOwner('role', $id);
		}

		if(isset($this->permissionModelsMap[$type])){
			$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM {$this->permissionModelsMap[$type]['table']} WHERE id = :id AND user_id = :user_id");
			$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
			return $stmnt->fetchColumn();
		}

		return false;
	}

	function cancel_subscription($sub_limit_ID)
	{
		return true;
		$userDa	=	new \UserDataAccess();
		$userDa->setActionUser($this->user);
		return $this->user->isSystem() || $userDa->hasPersistentTOTALLimits();
	}

	function subscribe($subscription_ID)
	{
		return true;
	}
    
//    function check($p, $id){
//
//        DebugStuff::Debug("$p/$id");
//
//        if(method_exists($this, $p))
//            return $this->$p($id);
//
//        // view_this, view_that, view_myballs
//        list($func, $type)    =   explode('_', $p);
//
//        if(method_exists($this, $func))
//                return $this->$func($type, $id);
//
//        return false;
//    }

	function create($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL){
		if(parent::create($type, $id, $SED)) return true;

		//Add more cases to allow admin create certain objects
		switch($type){
			case "leadform":
			case "blogfeed":
			case "role":
				return $id == 0;
		}

		$actionUser = $this->permissions->getUser();

		if(isset($this->permissionModelsMap[$type])){
			$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM {$this->permissionModelsMap[$type]['table']} WHERE id = :id AND user_id = :user_id");
			$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
			return $stmnt->fetchColumn();
		}

		return false;

	}

	function trash($type, $id){
		if(parent::trash($type, $id)) return true;

		$actionUser = $this->permissions->getUser();

		if(isset($this->permissionModelsMap[$type])){
			$stmnt = \Qube::Start()->GetPDO()->prepare("SELECT 1 FROM {$this->permissionModelsMap[$type]['table']} WHERE id = :id AND user_id = :user_id");
			$stmnt->execute(array('id' => $id, 'user_id' => $actionUser->getID()));
			return $stmnt->fetchColumn();
		}

		return false;
	}
}

class DebugStuff
{
    protected $stuff    =   array();
    
    protected function __construct() {
        ;
    }
    
    function log($stuff){
        $this->stuff[]  =   $stuff;
    }
    
    static function Debug($stuff){
        static $obj =   null;
        if(!$obj) $obj  =   new DebugStuff ();
        $obj->log($stuff);
    }
    function __destruct() {
		if(\Qube::IS_DEV() < 2) return;
        deb($this->stuff);
    }
}
