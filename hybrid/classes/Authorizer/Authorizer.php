<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

namespace Qube\Hybrid\Authorizer;

/**
 * Description of Authorizer
 *
 * @author amado
 */
interface Authorizer
{
    function check($p, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL);
}

