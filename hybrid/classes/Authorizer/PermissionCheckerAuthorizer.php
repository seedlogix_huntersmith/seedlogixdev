<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\Authorizer;

use Qube\Hybrid\DataAccess\AccountDataAccess;
use Zend\XmlRpc\Value\DateTime;

/**
 * Description of PermissionCheckerAuthorizer
 *
 * @author amado
 */
abstract class PermissionCheckerAuthorizer implements Authorizer {

    protected $permissionModelsMap = array(
        'touchpoint' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'WEBSITE' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'SOCIAL' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'MOBILE' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'LANDING' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'PROFILE' => array('table' => 'hub', 'parentColumn' => 'user_parent_id'),
        'BLOG' => array('table' => 'blogs', 'parentColumn' => 'user_parent_id'),
        'users' => array('table' => 'users', 'parentColumn' => 'parent_id'),
    );

    /**
     *
     * @var UserWithRolesModel
     */
    protected $user = NULL;

    /**
     *
     * @var PermissionsSet
     */
    protected $permissions = NULL;

    /**
     *
     * @var \AccountLimitsVerify
     */
    protected $_limitVerify = NULL;
    protected $parent_id = NULL;
    protected $variablesPermissions = array('touchpoint');

    function __construct(\PermissionsSet $p, \UserWithRolesModel $user) {
        $this->permissions = $p;
        $this->user = $user;
        $this->parent_id = $user->getParentID();
        $this->init($p, $user);
    }

    /**
     *
     * @return \AccountLimitsVerify
     */
    function getLimitVerify(\UserDataAccess $uda) {
        if (!$this->_limitVerify) {
            // create an account data access for calculating objects from the user's (or his parent's ) account
            $ada = new \Qube\Hybrid\DataAccess\AccountDataAccess();
            $ada->setActionUser($this->user);

            // initialize accounnt limits from the reseller subscription.
            $limits = $uda->getAccountLimits($this->user->getParentID());

            // return the class responsible for comparing the number of existing objects vs limits
            $alv = new \AccountLimitsVerify($limits, $ada);
            $this->_limitVerify = $alv; //$uda->getUserLimitsVerify($ada);
        }
        return $this->_limitVerify;
    }

    function setLimitVerify() {
        
    }

    function check($p, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL) {

//        DebugStuff::Debug("$p/$id");

        if (method_exists($this, $p))
            return $this->$p($id);

        // view_this, view_that, view_myballs
        list($func, $type) = explode('_', $p);

        if (method_exists($this, $func))
            return $this->$func($type, $id, $SED);

        return false;
    }

    function isObjectOwner($type, $campaign_ID) {
        $queries = array(
            'campaign' => 'select 1 from campaigns c where c.user_ID = %d and c.id = %d',
            'BLOG' => 'SELECT 1 FROM blogs where user_ID = %d and id = %d',
            'WEBSITE' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'LANDING' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'PROFILE' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'MOBILE' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'SOCIAL' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'hub' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'touchpoint' => 'SELECT 1 FROM hub where user_ID = %d and id = %d',
            'campaigns' => 'SELECT 1 FROM campaigns where user_ID = %d and id = %d',
            'users' => 'SELECT 1 FROM users where user_ID = %d and id = %d',
            'keywordCampaign' => 'SELECT 1 FROM ranking_sites where user_ID = %d and id = %d',
            'directory' => 'SELECT 1 FROM directory WHERE user_id = %d AND id = %d',
            'press_release' => 'SELECT 1 FROM press_release WHERE user_id = %d AND id = %d',
            'blog_feed' => 'SELECT 1 FROM blog_feedsmu WHERE user_id = %d AND id = %d',
            'blogpost' => 'SELECT 1 FROM blog_post WHERE user_id = %d AND id = %d',
            'leadform' => 'SELECT 1 FROM lead_forms WHERE user_id = %d AND id = %d',
            'prospect' => 'SELECT 1 FROM prospects WHERE user_id = %d AND id = %d',
            'responder' => 'SELECT 1 FROM auto_responders where user_id = %d AND id = %d',
            'broadcast' => 'SELECT 1 FROM lead_forms_emailers where user_id = %d AND id = %d',
            'report' => 'SELECT 1 FROM campaign_reports WHERE user_ID = %d AND id = %d',
            'contact' => 'SELECT 1 FROM contacts WHERE user_ID =%d AND id =%d',
			'accounts' => 'SELECT 1 FROM accounts WHERE user_id =%d AND id =%d',
			'emails' => 'SELECT 1 FROM emails WHERE user_id =%d AND id =%d',
			'tasks' => 'SELECT 1 FROM tasks WHERE user_id =%d AND id =%d',
			'proposals' => 'SELECT 1 FROM proposals WHERE user_id =%d AND id =%d',
			'products' => 'SELECT 1 FROM products WHERE user_id =%d AND id =%d',
            'socialaccounts' => 'SELECT 1 FROM social_accounts WHERE user_ID =%d AND id =%d',
            'socialposts' => 'SELECT 1 FROM social_posts WHERE user_ID =%d AND id =%d',
            'note' => 'SELECT 1 FROM notes WHERE user_ID =%d AND id =%d',
            'keyword' => 'SELECT 1 FROM ranking_keywords LEFT JOIN ranking_sites ON ranking_keywords.site_ID = ranking_sites.id WHERE ranking_sites.user_id = %d AND ranking_keywords.id = %d',
            'formfield' => 'SELECT 1 FROM 6q_formfields_tbl WHERE user_ID = %d AND ID = %d',
            'role' => 'SELECT 1 FROM 6q_roles WHERE reseller_user = %d AND ID = %d'
        );

        if (!isset($queries[$type]))
            throw new QubeException("Please define a query to verify object ownership.");

        return \Qube::Start()->squery($queries[$type], $this->user->getID(), $campaign_ID)->fetchColumn();
    }

    function view($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL) {
        switch ($type) {
//				case 'BLOG':
//					return $this->permissions->can('create_BLOG', 0);

            case 'campaigns':
                return ($id == 0 && $this->checkAnyTOUCHPOINT('view', 0));
            case 'campaign':
                return $this->permissions->can('view_campaigns') || $this->isObjectOwner($type, $id);
        }
//			if(in_array($type, self::TouchPointTypes()))
//			{
//				
//			}
        return false;
    }

    function create($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL) {
        switch ($type) {
            case 'contact':
                return true;
//					case 'users':
//					case 'campaigns':
//						return $this->checkAnyTOUCHPOINT('create', $id);
//					case 'users':
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateUser($SED);
//					case 'BLOG':
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateBLOG($SED);
//					case 'WEBSITE':
//							return $this->user->verifyLimit('create_WEBSITE');
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateWEBSITE($SED);
//					case 'LANDING':
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateLANDING($SED);
//						
//					case 'MOBILE':
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateMOBILE($SED);
//						
//					case 'SOCIAL':
//							return $this->getLimitVerify(new \UserDataAccess())->canCreateSOCIAL($SED);
//					case 'blogpost':
//							return $this->verifyPostCount();
//					case 'responder':
//						return $this->getLimitVerify(new \UserDataAccess())->canCreateResponder($SED);
        }


        return false;
    }

    function remove($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL) {
        switch ($type) {
            case 'addon':
                $ada = new AccountDataAccess();
                $ada->setActionUser($this->user);
                $uda = new \UserDataAccess();
                $limits = (object) $uda->SumActiveLimits($this->user->getID(), new \DateTime(), $id);
                $alv = new \AccountLimitsVerify($limits, $ada);

                return $alv->canRemoveAddon($SED);
                break;
            case 'subscription':
                $ada = new AccountDataAccess();
                $ada->setActionUser($this->user);
                $uda = new \UserDataAccess();
                $limits = (object) $uda->SumActiveLimits($this->user->getID(), new \DateTime(), null, $id);
                $alv = new \AccountLimitsVerify($limits, $ada);

                return $alv->canRemoveSubscription($SED);
                break;
        }
    }

    function edit($type, $id) {
        switch ($type) {
            case 'user':
                if ($id == $this->user->getID())
                    return true;
                return false;
        }

        return false;
    }

    function checkAnyTOUCHPOINT($permission, $id) {
        foreach (self::TouchPointTypes() as $tp) {
            if ($this->permissions->can($permission . '_' . $tp, $id))
                return true;
        }
        return false;

        foreach ($permissions as $permissionstr) {
            if ($this->check($permissionstr, $id))
                return true;
        }
        return false;
    }

    static function makeTouchpointPermissions($permission) {
        $arr = array();
        foreach (self::TouchPointTypes() as $tp)
            $arr[] = $permission . '_' . $tp;

        return $arr;
    }

    function getNewLimits() {
        
    }

    static function assignSubPermissions(\PermissionsSet $p, $permission, $id = 0, $assign_parent = true) {
        $derivedPermissions = array(
            'is_admin' => array('create_campaigns', 'view_admin', 'edit_prospects', 'create_users', 'create_touchpoint'),
//					'create_campaigns' => array('trash_campaigns'),
            'edit_campaigns' => array('edit_touchpoint'),
            'create_touchpoint' => self::makeTouchpointPermissions('create'),
            'edit_touchpoint' => self::makeTouchpointPermissions('edit'),
            'view_touchpoint' => self::makeTouchpointPermissions('view'),
            'edit_WEBSITE' => array('create_slide', 'view_WEBSITE'),
            'edit_LANDING' => array('create_slide', 'view_LANDING'),
            'edit_PROFILE' => array('create_slide', 'view_PROFILE'),
            'edit_MOBILE' => array('create_slide', 'view_MOBILE'),
            'edit_SOCIAL' => array('create_slide', 'view_SOCIAL'),
        );

        if (isset($derivedPermissions[$permission])) {
            foreach ($derivedPermissions[$permission] as $derived_permission) {
                self::assignSubPermissions($p, $derived_permission);
            }
        } else
            switch ($permission) {
                case 'create_users':
                    foreach (array('view_admin') as $derived_permission) {
                        self::assignSubPermissions($p, $derived_permission);
                    }
                    break;
                default:
                    // assign edit_*TOUCHPOINTTYPE* for each create_*TOUCHPOINTTYPE*
                    // assign view_*TOUCHPOINTTYPE* for each edit_*TOUCHPOINTTYPE*
                    if (preg_match('#^(edit|view)_(.*?)$#', $permission, $m) === 1) { // implode('|', self::TouchPointTypes())
                        // removed edit_* activation by create-*. create allows user to create his own websites, edit allows users to edit all sites.
//					if($m[1] == 'create')						$p->setPermission ('edit_' . $m[2], $id);
//						self::assignSubPermissions($p, 'edit_' . $m[2]);

                        if ($m[1] == 'edit')
                            $p->setPermission('view_' . $m[2], $id);
//						self::assignSubPermissions($p, 'view_' . $m[2]);
                        // if view_*TOUCHPOINTTYPE*, enable view_campaigns
                        if ($m[1] == 'view' && in_array($m[2], self::TouchPointTypes()))
                            $p->setPermission('view_campaigns', $id);
//					self::assignSubPermissions ($p, 'view_campaigns');
                    }
            }

        // assign main permission.
        if ($assign_parent)
            $p->setPermission($permission);
    }

    static function TouchPointTypes() {
        return array('WEBSITE', 'BLOG', 'LANDING', 'PROFILE', 'MOBILE', 'SOCIAL');
    }

    function init(\PermissionsSet $p, \UserWithRolesModel $user) {
        $p->setPermission('edit_users', $user->getID(), TRUE);
    }

    function trash($type, $id) {
        return false;
    }

}
