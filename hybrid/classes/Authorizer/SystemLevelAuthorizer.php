<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

namespace Qube\Hybrid\Authorizer;

/**
 * Description of SystemLevelAuthorizer
 *
 * @author amado
 */

class SystemLevelAuthorizer extends AdminLevelAuthorizer implements Authorizer
{
    function create($type, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL)
		{
//        echo "Can: $p/$id<br />";
        return true;
    }

    function init(\PermissionsSet $p, \UserWithRolesModel $user)
    {
			return;
        parent::init($p, $user);
        $p->setPermission('view_reports');
        $p->setPermission('view_rankings');
        $p->setPermission('view_customforms');
        $p->setPermission('view_emailmarketing');
        $p->setPermission('view_prospects');
        $p->setPermission('view_contacts');
        $p->setPermission('view_networks');
        $p->setPermission('view_reports');
        $p->setPermission('view_users');
    }

    function impersonate($user_id)
    {
        return true;
    }

	// temporary
		function check($p, $id, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL){
		return true;
	}
	// end

    function trash($type, $object_id){
        return true;
    }

    function edit($type, $id){
        return true;
    }
}