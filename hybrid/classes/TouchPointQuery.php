<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TouchPointQuery
 *
 * @author amado
 */
class TouchPointQuery extends QubeDataAccess implements ExportableTable
{
	CONST MULTISITE_CAMPAIGNID = 99999;
	protected $touchpoint_type	=	NULL;
	function setTouchpointType($tp)
	{
		$this->touchpoint_type	=	$tp;
	}

	/**
	 * Produces a query returning the columns: ID, name (blog_title), touchpoint_type, created
	 * if querying all touchpoints, it integrates the where clause into the query
	 * if a specific tuochpoint type is specified, it returns only the SELECT and FROM parts of the query.
	 *
	 * @param $tp
	 * @param array $args
	 * @return string
	 */
	function getModelQuery($tp, array $args)
	{
		$where = '';
		$cols	=	'';
		if(isset($args['options']) && $tp != '')
		{
			$where = $this->getTouchpointsWhere($tp, $args);
			if($args['options']['query-mode'] == 'without-stats' && $args['options']['cols'])
			{
				$cols = $args['options']['cols'];
			}
		}
		switch($tp)
		{
			case '':
				$args2 = $args;
				unset($args2['options']['SQL_CALC_FOUND_ROWS']);
//				unset($args2['options']['limitstr']);
				return	'( ' . $this->getModelQuery('BLOG', $args2) . $this->getTouchpointsWhere('BLOG', $args)
							. ' UNION ALL ' . $this->getModelQuery('TOUCHPOINT', $args2) . $this->getTouchpointsWhere('TOUCHPOINT', $args) . ' ) ';
				
			case 'BLOG':
				$q	=	"SELECT {$args['options']['CALC_FOUND_ROWS']} " . ($cols ? $cols : 'T.`id`, T.`blog_title` as name, "BLOG" as touchpoint_type, T.`created`') . "
					FROM blogs as T $where ";
				return $q;
				
			case 'TOUCHPOINT':
			default:
				$q = "SELECT {$args['options']['CALC_FOUND_ROWS']} " . ($cols ? $cols : 'T.id, T.name, T.touchpoint_type, T.created') . "
						FROM hub as T $where";
				return $q;
		}
	}
	
	function getStatsJoin($tp, $isalltime)
	{
		$where = array('cid = :campaign_id');
		
// enable this to only show stats in the givenr ange (:d3, :d4)
//		$where[]	=	'point_date BETWEEN :d3 AND :d4';
		
		switch($tp)
		{
			case 'WEBSITE':
			case 'MULTIUSER':
			case 'MOBILE':
			case 'SOCIAL':
			case 'PROFILE':
			case 'LANDING':
				$where[]	=	'r.tbl = "hub"';
				break;
			case 'BLOG':
				$where[]	=	'r.tbl = "blog"';
				break;
			default :
				$where[]	=	'(r.tbl = "blog" or r.tbl = "hub")';
		}

		if($isalltime){
			$nb_leads = 'nb_leads';
			$nb_visits = 'nb_visits';
		}else{
			$nb_leads = 'IF(point_date > :d1, nb_leads, 0)';
			$nb_visits = 'IF(point_date > :d1,nb_visits, 0)';
		}
		return "SELECT r.tbl, tbl_ID, user_ID, cid, IFNULL(SUM($nb_leads), 0) total_leads, SUM(COALESCE($nb_visits, 0)) total_visits
						FROM piwikreports r WHERE " . join(' AND ', $where) . ' group by r.tbl, tbl_ID, user_ID, cid';
	}
	
	static function JoinVisitsChanges()
	{
		$q	=	"SELECT	z.tbl, z.tbl_ID, cid,	SUM(IF(point_date < :d1, 0, nb_visits)) / SUM(IF(point_date < :d1, nb_visits, 0)) * 100 AS visits_changes
			FROM piwikreports z
			WHERE z.point_date > :d2 AND z.user_ID = :user_id GROUP BY z.cid , z.tbl , z.tbl_ID";
		
		return $q;
	}
	
	function getTouchpointsWhere($touchpoint_type, array $args)
	{		
		$actionUser	=	$this->getActionUser();
		$where	=	array('T.trashed = 0', 'T.cid = :campaign_id');
		if(isset($args['sSearch']))
		{
			switch($touchpoint_type)
			{
				case 'BLOG':
					$where[]	=	is_numeric($args['sSearch']) ? 'T.ID = :sSearch' : 'T.blog_title LIKE :sSearch';
					break;
				case 'TOUCHPOINT':
				default:
					$where[]	=	is_numeric($args['sSearch']) ? 'T.ID = :sSearch' : 'T.name LIKE :sSearch';
					break;
			}
		}
		
		$where_security_check	=	'T.user_id = :user_id';
		if($touchpoint_type == '' || $touchpoint_type == 'TOUCHPOINT')
		{
			if(!$actionUser->can('view_touchpoint'))
			{
				$systemTPs	= array_keys(CampaignController::getTouchPointTypes());
				$touchpoint_check = array($where_security_check);
				foreach($systemTPs as $TP_TYPEX)
				{
					if($actionUser->can('view_' . $TP_TYPEX))
					{
						if($TP_TYPEX == 'BLOG'){
//							$blogWhere = "user_parent_id = {$actionUser->parent_id}";
							continue;
						}
						$touchpoint_check[] = "(T.touchpoint_type = '$TP_TYPEX' AND T.user_parent_id = {$actionUser->getParentID()})";
					}
				}

				$where_security_check	=	join(' OR ', $touchpoint_check);
			}
			// querying all touchpoints. 
		}else{
			if($actionUser->can("view_$touchpoint_type") ){
				$where_security_check .= sprintf(" OR user_parent_id = %d", $actionUser->getParentID());
			}
			if($touchpoint_type != 'BLOG'){
				$where[] = "touchpoint_type = :touchpoint_type";
			}
//			else $where[]	=	":touchpoint_type = 'BLOG'";
		}
		
		$where[]	=	'(' . $where_security_check . ')';
		
		return ' WHERE ' . join(' AND ', $where);
	}
	
	static function getOrderBy(array $opts, $direction = 'ASC')
	{
		$newRow = "";
		if(!isset($opts['orderCol'])) return '';
		$orderCol = $opts['orderCol'];
//		unset($opts['orderCol']);
		if(isset($opts['orderDir'])){
			$direction = $opts['orderDir'];
//			unset($opts['orderDir']);
		}
		switch($orderCol)
		{
			case 'conversion':
				$orderCol = 'ifnull(total_leads / total_visits * 100, 0)';
				break;
			case 'ifnull(total_leads / total_visits * 100, 0)':
			case 'name':
			case 'total_leads':
			case 'total_visits':
			case 'visits_changes':
			case 'rate':
			case 'touchpoint_type':
			case 'created':
				break;
			default:
				$orderCol	=	'name';
		}

		if($opts['new_rows_ids']){
			$newRow = "id = " . join(' DESC,  id = ', $opts['new_rows_ids']) . " DESC, ";
		}
		return " ORDER BY $newRow $orderCol $direction ";
	}

	/**
	 * Produces a stand-alone query for touchpoints with where[]
	 *
	 * @param array $args
	 * @return string
	 */
	function getTouchPointsSQL(array $args, array $options)
	{
		$args['options']	=	$options;
		$where = '';
		if(isset($options['where'])){
			$where = ' AND ' . $options['where'];
		}

		$orderBy = self::getOrderBy($args['options']);
		return	$this->getModelQuery($this->touchpoint_type, $args) . $where . $orderBy .  $options['limitstr'];
	}
	
	function getTouchPointsWithStatsSQL(array $args, array $options)
	{
		$fields	=	'x.id, x.name, x.touchpoint_type, '
						. 'x.created, x.total_leads, '
						. 'x.total_visits, ifnull(total_leads / total_visits * 100, 0) as rate, '
						. 'x.visits_changes';
		
		if($options['cols'])
		{
			$fields	=	$options['cols'];
			unset($options['cols']);
		}				
		
		$bigquery	=	'SELECT%s %s FROM (%s%s) x %s%s';
		$touchpoint_query	=	$this->getModelQuery($this->touchpoint_type, $args);
		$stats_query	=	$this->getStatsJoin($this->touchpoint_type, $args['isalltime']);
		$visitchanges_join	=	$args['isalltime'] ? ' INNER JOIN ( select 0 as visits_changes) vc ' : ' LEFT JOIN (' . self::JoinVisitsChanges() . ' ) vc ON (vc.tbl = stats.tbl AND vc.tbl_ID = stats.tbl_ID)';
		$orderBy	=	self::getOrderBy($options, $options['orderDir']);
		/**
		 * if touchpoint_type == '',
		 * touchpoint_query will contain a union all query with independent where clauses
		 */
		if($this->touchpoint_type == '')
		{
			$query	=	sprintf($bigquery, $options['CALC_FOUND_ROWS'], $fields,
						"select g.id, g.touchpoint_type, g.name, g.created, if(:isalltime=1, 0, coalesce(vc.visits_changes, 0)) as visits_changes, coalesce(stats.total_leads, 0) as total_leads,
								coalesce(stats.total_visits, 0) as total_visits"
							. " from $touchpoint_query as g LEFT JOIN ($stats_query) stats on "
						. "(stats.tbl_ID = g.id and stats.tbl = IF(g.touchpoint_type = 'BLOG', 'blog', 'hub'))",
							$visitchanges_join,
							$orderBy,
							$options['limitstr']
					);
			return $query;
		}
		/**
		 * if touchpoint_type !=
		 * get the where clause and generate the full query.
		 */
		$wherestr	=	$this->getTouchpointsWhere($this->touchpoint_type, $args);
		$touchpoint_table = $this->touchpoint_type == 'BLOG' ? 'blog' : 'hub';
		$query	=	sprintf($bigquery, $options['CALC_FOUND_ROWS'], $fields,
						"select g.id, g.touchpoint_type, g.name, g.created, if(:isalltime=1, 0, coalesce(vc.visits_changes, 0)) as visits_changes, coalesce(stats.total_leads, 0) as total_leads,
							coalesce(stats.total_visits, 0) as total_visits"
						. " FROM ( $touchpoint_query $wherestr ) g LEFT JOIN ($stats_query) stats on "
						. "(stats.tbl_ID = g.id and stats.tbl = '$touchpoint_table') ",
						$visitchanges_join,
						$orderBy,
						$options['limitstr']
				);
		
		return $query;
	}
	
	/**
	 * Prepares $args, and returns a prepared statement.
	 * 
	 * @param array $args
	 * @param array $options
	 * @return PDOStatement
	 */
	function Prepare(array &$args, $options = NULL)
	{
		$options	=	$options ? $options : self::getQueryOptions($args);
		
		if(isset($args['touchpoint_type']))
		{
			$this->setTouchpointType($args['touchpoint_type']);
//			unset($args['touchpoint_type']);
		}
		
		if(isset($args['sSearch']))
		{
			if(trim($args['sSearch']) == '')
						unset($args['sSearch']);
			else{
				$args['sSearch']	=	is_numeric($args['sSearch']) ? $args['sSearch']*1 : '%' . $args['sSearch'] . '%';
			}
		}
//		if(!isset($args['limit']))
//			$args['limit']	=	10;

//		if(!isset($args['limit_start']))
//			$args['limit_start']	=	0;
		
		
		$args['user_id']	=	$this->getActionUser()->getID();

		$query_mode	=	isset($args['query-mode']) ? $args['query-mode'] : '';
		$options['query-mode']	=	$query_mode;
		switch($query_mode) {
			case 'without-stats':
				$query	=	$this->getTouchPointsSQL($args, $options);
				break;
			case 'with-stats':
			default:

				if(isset($args['timecontext'])) {
					/* @var $psb PointsSetBuilder */
					$psb = $args['timecontext'];
					unset($args['timecontext']);
					if ($psb->isalltime) {
//			$data['d2']	=	$psb->getEndDate();
//			$data['d1']	=	$psb->getStartDate();
					} else {
						$args['d1'] = $psb->previousSet()->getEndDate();
						$args['d2'] = $psb->previousSet()->getEndDate();
					}

					$args['isalltime'] = $psb->isalltime * 1;
				}else
					$args['isalltime'] = 0;
			$query = $this->getTouchPointsWithStatsSQL($args, $options);
		}

		if($args['touchpoint_type'] == 'TOUCHPOINT'){
			unset($args['touchpoint_type']);
		}
		unset($args['query-mode']);
		return $this->getPDO()->prepare($query);
	}
	
	function ProcessQueryRequest(array $args)
	{
		$options	=	self::getQueryOptions($args);
		$Prepared	=	$this->Prepare($args, $options);

		if(isset($args['touchpoint_type']) && $args['touchpoint_type'] == 'BLOG') unset($args['touchpoint_type']);
		self::callPreparedQuery($Prepared, $args);
		
		if($options['getStatement'])
		{
			return $Prepared;
		}
		
		return $Prepared->fetchAll(PDO::FETCH_OBJ);
	}
	
	function Export($fhandle, array $args)
	{
		$rowCompiler	=	$args['rowCompiler'];
		$rowVar	=	$args['rowVar'];
		unset($args['rowVar'], $args['rowCompiler']);
		
		$rows = $this->ProcessQueryRequest($args);
		foreach($rows as $row)
		{
			$rowCompiler->init($rowVar, $row)->init('handle', $fhandle)->display();
		}
	}

	function getTouchpointsInfo(array $args)
	{
		$qOpts	=	self::getQueryOptions($args);
		$qstr	=	'SELECT %s FROM touchpoints_info WHERE user_id = :user_ID
						AND %s %s';

		$args['user_ID']	=	$this->getActionUser()->getID();
		$q =	sprintf($qstr,
				$qOpts['cols'] ? $qOpts['cols'] : '*',
				$qOpts['where'],
				$qOpts['limitstr']);

		$stmt = $this->getPDO()->prepare($q);

		self::callPreparedQuery($stmt, $args);

		if($qOpts['getStatement'])
		{
			return $stmt;
		}
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'TouchpointInfoModel');
	}

	function getTouchPointForDisplay($httphost)
	{
		$q = 'CALL getTouchPointByHost(:httphost);';
		$stmt	=	$this->getPDO()->prepare($q);
		$res = $stmt->execute(array('httphost' => $httphost));

		try {
			$rowcount = $stmt->rowCount();
			$result = $stmt->fetch(PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
		}catch(PDOException $e)
		{
			// error related to zero results or some bullshit.
			if($e->errorInfo[0] == 'HY000')
				$result =  false;
		}
		return $result;
	}

	function GetExportableColumnTitles()
	{
		return array_keys(array_filter(DataTableResult::getTableColumns('Websites'), function($column){return $column['Exportable'] !== false; }));
	}
}
