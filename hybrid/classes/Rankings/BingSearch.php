<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 1/26/15
 * Time: 9:32 AM
 */

namespace Qube\Hybrid\Rankings;


use Zend\Dom\Query;;

class BingSearch extends \SearchCrawler implements SearchEngine {

	private $baseURL = "http://www.bing.com";
	private $startURL = "/search?q=[keyword]&sc=1-27&sp=-1&sk=&cvid=4171a512dfed4b77be2970b0e6a12db2&count=50";
	private $nextURL = "/search?q=[keyword]&first=[position]&count=[size]&sc=1-27&sp=-1&sk=&cvid=4171a512dfed4b77be2970b0e6a12db2";

	protected $proxy = NULL;

	protected $cache_dir = './';
	protected $verbose = FALSE;
	protected $searchengine = 'bing';
	protected $resultsPerPage = 10;
	protected $resultsPerFetch = 50;

	function getURL($keyword, $page)
	{
		$url = str_replace("[keyword]", urlencode($keyword), $this->baseURL . $this->nextURL);
		$url = str_replace("[position]", $this->resultsPerFetch * ($page - 1) + 1, $url);
		$url = str_replace("[size]", $this->resultsPerFetch, $url);
		return $url;
	}

	public function fetchNextURL($content, $keyword)
	{
		$dom = new \Zend_Dom_Query($content);
//		if($dom->query('#sp_requery')->count() > 0){
//			$this->logERROR("Proxy: {$this->proxy}; Results not found in bing with keyword $keyword");
//			return false;
//		}
		$content = $dom->query('#b_results .b_pag .sb_pagN');
		if(count($content)){
			return $this->baseURL . $content->current()->getAttribute('href');
		}else{
			$this->logERROR("Not enough results found for $keyword with proxy: $this->proxy URL: %s", $this->nextURL);
			$this->getRankingProxiesDataAccess()->incrementProxyKeywordError($this->proxy, $this->searchengine);
			return FALSE;
		}
	}

	/**
	 * Returns false if it finds < 100 results on a page. or
	 * Returns an object with results, total_results, and nextURL
	 * @deprecated
	 * @param $pagecode
	 * @param $pageNumber
	 * @param $position
	 * @param $keyword
	 * @return bool|object
	 * @throws \QubeException
	 */
	function getPageInfo($pagecode, &$pageNumber, &$position, $keyword)
	{
		$pageInfo	=	(object)array('results' => array(), 'total_results' => 0, 'nextURL' => NULL);

		$dom = new \Zend_Dom_Query($pagecode);
		// get total search results
		$results_count = $dom->query('.sb_count')->current()->textContent;
		preg_match('/(?:[0-9\-]+ .+ )?([0-9,]+).+/', $results_count, $results_html);
		$results_count = intval(str_replace(",", "",$results_html[1]));

		if($results_count < 100 && $pageNumber == 1){
			$this->logINFO("Found $results_count results.");
			return FALSE;
		}


		//Get next page
		$content = $dom->query('#b_results .b_pag .sb_pagN');
		if(count($content)){
//			if($pageNumber > 1)
//				$this->referrer = $this->baseURL . $this->nextURL;
			$pageInfo->nextURL = $content->current()->getAttribute('href');
		}else{
			$this->logERROR("Could Not Find Next Page for $keyword/$pageNumber");
			return FALSE;
		}

		$pageInfo->total_results = $results_count;
		$results_html = $dom->query('#b_results .b_algo');
		foreach($results_html as $result){
			$currentDom = new \Zend_Dom_Query($result->ownerDocument->saveXML($result));
			$rankingResult = array();
			$rankingResult['title'] = $currentDom->query('h2 > a')->current()->textContent;
			$rankingResult['description'] = $currentDom->query('.b_caption p')->current()->textContent;
			$rankingResult['url'] = $currentDom->query('h2 > a')->current()->getAttribute('href');
			preg_match('/(?:(?:https?:\/\/)?(?:www\.)?)?([\w\.-]+)(\/*\S*)*/', $rankingResult['url'], $domain);
			$rankingResult['domain'] = $domain[1];
			$rankingResult['position'] = $position++;
			$rankingResult['keyword'] = $keyword;
			$rankingResult['searchengine'] = 'bing';
			$rankingResult['pagenumber'] = $pageNumber;
			$pageInfo->results[] = $rankingResult;
		}
		return $pageInfo;
	}

	function parsePage($content, &$results, $keyword, $pageNumber, &$position){
		$dom = new \Zend_Dom_Query($content);
		$results_html = $dom->query('#b_results .b_algo');
		foreach($results_html as $result){
			$currentDom = new \Zend_Dom_Query($result->ownerDocument->saveXML($result));
			$description = $currentDom->query('.b_caption p')->current()->textContent;

			$rankingResult = array();
			$rankingResult['title'] = $currentDom->query('h2 > a')->current()->textContent;
			$rankingResult['description'] = $description ? $description : '';
			$rankingResult['url'] = $currentDom->query('h2 > a')->current()->getAttribute('href');
			preg_match('/(?:(?:https?:\/\/)?(?:www\.)?)?([\w\.-]+)(\/*\S*)*/', $rankingResult['url'], $domain);
			$rankingResult['domain'] = $domain[1];
			$rankingResult['pagenumber'] = ceil($position / $this->resultsPerPage);
			$rankingResult['position'] = $position++;
			$rankingResult['keyword'] = $keyword;
			$rankingResult['searchengine'] = 'bing';
			$results[] = $rankingResult;
		}
		return $results;
	}

	function attemptDisableCache($disable = TRUE)
	{
		static $num_calls = 0;
		static $cur_cachedir = NULL;

		if($disable)
		{
			if( $num_calls > 0)
				return false;

			$num_calls = 1;
			$cur_cachedir = $this->cache_dir;
			return TRUE;    // error
		}

		// if previously called
		if(!$disable && $num_calls > 0)
		{
			if(++$num_calls > 2)
			{
				$num_calls = 0;
				$this->cache_dir = $cur_cachedir;
				$cur_cachedir = NULL;
			}
		}
		return TRUE;
	}

	/**
	 * Get the number of results indicated in the content page
	 * @param $content
	 * @return int
	 */
	public function getNoResultsInContent($content)
	{
		$zdom = new Query($content);
		$results = $zdom->execute('.sb_count');
		$text = $results->current()->textContent;
		preg_match('/([0-9,]+) [\s\S]+/', $text, $result);
		return intval(preg_replace('/,/', '', $result[1]));
	}
}