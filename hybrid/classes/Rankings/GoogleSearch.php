<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\Rankings;
use Zend\Dom\Exception\RuntimeException;
use Zend\Dom\Query;

/**
 * Description of GoogleSearch
 *
 * @author sofus
 */
class GoogleSearch extends \SearchCrawler implements SearchEngine {
	
	protected $lastURL	=	'';
	protected $nextURL = '';
	protected $searchengine = 'google';
	protected $resultsPerPage = 10;
	protected $resultsPerFetch = 100;

	function getURL($q, $page)
	{
		$q = urlencode($q);
		$curURL = 'https://www.google.com/search?q='.$q.'&ie=utf-8&oe=utf-8&rls=org:mozilla:us:official&client=firefox' . (
			'&start=' . (($page - 1) * $this->resultsPerPage) . "&num=$this->resultsPerFetch"
			);

//		if($this->nextURL)
//		{
//			return 'https://www.google.com' . $this->nextURL;
//		}

		return $curURL;
	}

	/**
	 * Returns an array with parsed data.
	 * Ignores image results.
	 *
	 * @param \DOMElement $e
	 * @param type $position
	 * @param $pagenumber
	 * @param type $keyword
	 * @return type
	 */
	private function parseSingleResult(\DOMElement $e, &$position, $pagenumber, $keyword)
	{
		$elID = $e->getAttribute('id');
		if($elID == 'imagebox_bigimages')
		{
			file_put_contents('html', $e->ownerDocument->saveHTML($e));
			return NULL;
		}
		if(strpos($e->getAttribute('class'), 'obcontainer') !== FALSE)
		{
			$this->logDEBUG('obcontainer found. skipping result and ignoring position. %s/%d', $keyword, $position);
			$position--;
			return NULL;
		}
		if(strpos($e->getAttribute('class'), 'card-section') !== FALSE)
		{
			$this->logDEBUG('News Item Found on %s/%d', $keyword, $position);
			return NULL;
		}
//		echo($e->textContent);
		$zdom = new Query($e->ownerDocument->saveHTML($e));
		$link	=	$zdom->execute('h3.r a')->current();
		if(!$link)	// not a normal link.. figure out what type of result it is.
		{
			$this->logDEBUG('Error With Node p: %s, kw: %s, Google.', $position, $keyword);
			return NULL;
		}
		
		$descEL = $zdom->execute('.s .st')->current();
		
		$url = $link->getAttribute('href');
		$title = $link->textContent;
		$description = $descEL ? $descEL->textContent : "";
		
		$host = parse_url($url, PHP_URL_HOST);
		$domain = str_replace('www.', '', $host);
		
		// constant stuff
		$searchengine = 'google';
		$pagenumber = ceil($position /$this->resultsPerPage);
		// end
		return compact('title', 'description', 'url', 'domain', 'position', 'keyword',
							'searchengine', 'pagenumber');
	}
	
	function removeSearchBlockStuff(\Zend\Dom\Nodelist $newsq, Query $zdom)
	{
		$count = $newsq->count();
		if(!$count) return;
	
		$this->logDEBUG("Found %d removable elements in Search.", $count);
		
		$doc = $newsq->getDocument();
		foreach($newsq as $el)
		{
			$assign_result_class = TRUE;
			while($el->firstChild)
			{
				if($el->firstChild->getAttribute('class') == 'kp-blk')
				{
					// found a definition.
					$assign_result_class = false;
				}
				$el->removeChild($el->firstChild);
			}
			if($assign_result_class)
			{
				$el->setAttribute('class', 'g');
			}
		}

		$zdom->setDocument($doc->saveHTML());
	}

	public function fetchNextURL($content, $keyword)
	{
		$zdomx = new Query($content);
		$nextPage = $zdomx->execute('#pnnext');
		if(!count($nextPage))
		{
			$this->logERROR("Proxy: {$this->proxy}; Next page not found for keyword $keyword");
			return FALSE;
		}

		return 'https://www.google.com' . $nextPage->current()->getAttribute('href');
	}

	public function parsePage($content, &$results, $keyword, $pageNumber, &$position){
		try {

			$html_results = preg_match('#<!--a-->[\s\S]*?<!--z-->#', $content, $matches);

			$page = $matches[0];
			$zdom = new Query($page);

			/* remove sidebar wiki info stuff. */
			$rhs = $zdom->execute('.rg_meta');
			if ($rhs->count()) {
				$doc = $rhs->getDocument();
				foreach ($rhs as $removeNode) {
					$removeNode->parentNode->removeChild($removeNode);
				}
				$zdom->setDocument($doc->saveHTML());
			}

			if (FALSE) {
				// remove news stuff
				$newsq = $zdom->execute('.mnr-c, .r-search-3');
				$this->removeSearchBlockStuff($newsq, $zdom);
				//
			}
			$DomResults = $zdom->execute('.g');


			// incremented for every external-linking result.

			$data = array();
			foreach ($DomResults as $result) {
				$resultinfo = $this->parseSingleResult($result, $position, $pageNumber, $keyword);
				$position++;

				if (!$resultinfo) {
					$this->logINFO('Could Not Parse Result! google:%s, p: %d, url: %s', $keyword, $position, $this->lastURL);
					continue;
				}
//				$position++;
				$results[] = $resultinfo;
			}

			$countel = count($results);


			return true;
		}catch (RuntimeException $e){
			return false;
		}
	}

	/**
	 * Get the number of results indicated in the content page
	 * @param $content
	 * @return int
	 */
	public function getNoResultsInContent($content)
	{
		$zdom = new Query($content);
		$results = $zdom->execute('#resultStats');
		$text = $results->current()->textContent;
		preg_match('/[A-Za-z0-9 ]+ ([0-9,]+) [\s\S]+/', $text, $result);
		return intval(preg_replace('/,/', '', $result[1]));

	}
}
