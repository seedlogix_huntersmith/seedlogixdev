<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\Rankings;

/**
 * Description of FakeSearchResults
 *
 * @author sofus
 */
class FakeSearchResults extends \Logger implements SearchEngine
{
	protected $searchengine = 'Fake';

	protected $fakeDomains = array(
		'rankingsite1.com',
		'rankingsite2.com',
		'rankingsite3.com',
		'softlab.org',
		'sismor.edu',
		'somesite.com',
		'visitme.com',
		'freethings.com',
		'electronics.com',
		'siss.edu',
		'opennetwork.org',
		'labslan.org',
		'Office365.com',
		'Ucoz.ru',
		'Exoclick.com',
		'Yesky.com',
		'Google.co.hu',
		'Npr.org',
		'Turboloves.net',
		'Appledaily.com.tw',
		'Sberbank.ru',
		'Ero-advertising.com',
		'Constantcontact.com',
		'Irctc.co.in',
		'Nordstrom.com',
		'Free-tv-video-online.me',
		'Zhihu.com',
		'Zippyshare.com',
		'Citibank.com',
		'Agoda.com',
		'58.com',
		'Styletv.com.cn',
		'Nbcnews.com',
		'Superiends.org',
		'Youjizz.com',
		'Clkmon.com',
		'Elmundo.es',
		'rankingtest.com'
	);
	
	function __construct($searchengine) {
		$this->searchengine	=	$searchengine;
	}
	
	function getParsedResult($keyword, $options = array())
	{
		static $i	=0;
		
		if(++$i > 100) $i = 1;
		
		// create random result


		$fakeDomain = '';
		if(rand(0, 4))
			$fakeDomain = $this->fakeDomains[rand(0, count($this->fakeDomains) - 1)];
		$data	=	array();
		$data['searchengine']	=	$this->searchengine;
		$data['position']	=	$i;
		$data['pagenumber']	=	floor($i/10);
		$data['keyword']	=	$keyword;
		$data['title']	=	"dummy title";
		$data['url']	=	"$fakeDomain/foo/bar.aspx";
		$data['domain']	=	$fakeDomain;
		$data['description']	=	"$i Result for $keyword description";
		
		return (object)$data;
	}
	
	function get100Results($keyword, array $options)
	{
		$results	=	array();
		for($i = 0; $i < 100; $i++)
		{
			$results[]	=	$this->getParsedResult($keyword, $options);
		}
			if(@$options['sleep'])
			{
				$this->logINFO("SearchEngine: %s, KW: %s, wait %d..", $this->searchengine, $keyword, $options['sleep']);
				sleep($options['sleep']);
			}
		return $results;
	}
}
