<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Qube\Hybrid\Rankings\BingSearch;
use Qube\Hybrid\Rankings\FakeSearchResults;
use \Qube\Hybrid\Rankings\GoogleSearch;
use Qube\Hybrid\Rankings\SearchEngine;

/**
 * Description of RankingsGenerator
 *
 * @author sofus
 */
class RankingsGenerator extends QubeThreadedDataProcessor {

	const ANALYZE = 1;
	const CRAWL	= 2;
	const PARSE = 3;
	const PROXYPREPARE = 4;
	protected $dataX	=	NULL;
	protected $num_threads	=	10;
	protected $script_mode = 0;
	
	protected $options = array();

	const PROXY_FILE = 'proxy.php';

	function getDataAccess()
	{
		if(!$this->dataX)
		{
			$this->dataX	=	new RankingDataAccess($this->getQube());
		}
		
		return $this->dataX;
	}

	function RunSearch($searchengine, KeywordRanker $KeywordRanker)
	{
#		var_dump($KeywordRanker, $this);

		if (FALSE === $this->options[$searchengine]) {
			return TRUE;
		}
		if($KeywordRanker->hasFreshResults($searchengine))
		{
			$this->logDEBUG("Found fresh results for $searchengine/$KeywordRanker->keyword..Skipping ");
			return TRUE;
		}

		$options	=	$this->options;

		/** @var $searcher SearchCrawler */
		switch($searchengine)
		{
			case 'google':
				$searcher	=	new GoogleSearch($this->getQube());
				break;
			case 'bing':
				$searcher	=	new BingSearch($this->getQube());
				break;
			default:
				return FALSE;
		}
		$searcher->setLogger($this->getLogger());

		$KeywordRanker->setSearchEngine($searcher);

		$result = $KeywordRanker->saveSearchResults($searchengine, $options);
		if(!$result){
			$this->logERROR("Engine:%s Did not generate any results for %s", $searchengine, $KeywordRanker->keyword);
			return FALSE;
		}
		return TRUE;
	}
	
	function RunChildThread($data)
	{
		/* @var $KeywordRanker KeywordRanker */
		$KeywordRanker	=	NULL;

		extract($data);

		$this->RunSearch('google', $KeywordRanker);
		$this->RunSearch('bing', $KeywordRanker);
	}
	
	function RunThreads() {
		switch($this->script_mode)
		{
			case self::CRAWL:
				return $this->RunCrawlerThreads();
				
			case self::ANALYZE:
				return $this->RunAnalyzerThreads();

			case self::PARSE:
				if($this->options['date'] && preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $this->options['date'])){
					if($this->options['targetkeyword']) return $this->RunSingleKeywordThread();
					else return $this->RunParseThreads();
				}else{
					throw new QubeException('Date not specified or invalid date');
				}
			case self::PROXYPREPARE:
					return $this->RunProxyPrepareThreads();
			default:
				throw new QubeException('Mode not specified.');
		}
		
		return false;
	}

	function RunAnalyzerThreads(){
		$this->logINFO("Start update to ranking_dailystats");
		$data = $this->getDataAccess()->getRankingDataDailyStats($this->options['date']);


		if($this->getDataAccess()->saveDailyStatsResultFromArray($data) !== false) {
			$this->logINFO("Update to ranking_dailystats success");
		}else{
			$this->logERROR("Fail at update ranking_dailystats");
		}
	}

	function RunCrawlerThreads()
	{
		$keyword_failures = array();
		
		$continue	=	true;
		$keywords_info	=	NULL;
		for($loop = 0; true; $loop++)
		{
			try{
				$keywords_info	=	$this->getDataAccess()->getKeywordRankers($this->num_threads*100, ($loop * $this->num_threads*100) + 1);
			} catch (PDOException $ex) {
				static $errorcount	=	0;
				if(++$errorcount == 2)
					throw $ex;
				
				$this->logERROR("MYSQL SERVER DISCONNECTED UNEXPECTEDLY. ATTEMPTING RECONNECTION.");
				// if the mysql server has gone away, disconnect db and reattempt to get teh KeywordRankers
				Qube::DisconnectDB();
				continue;
			}
			
			if(empty($keywords_info))
			{
				$this->logINFO("Found no more keywords on iteration #%d.", $loop+1);
				break;
			}
#			var_dump($keywords_info);
			$total_keywords	=	count($keywords_info);
			
			$this->logINFO("Loop %d, Found %d Keywords", $loop, $total_keywords);
			
			for($i = 0; $i < $total_keywords;)
			{

				// launch threads
				for($t = 0; $t < $this->num_threads && $i < $total_keywords; $t++)
				{
					if(!$this->canCreateChild()){
						if($this->WaitForThread() == FALSE){
							$this->logINFO("Fatal error, aborting %s", "");
							return;
						}
					}
					/* @var $KeywordRanker KeywordRanker */
					$KeywordRanker	=	$keywords_info[$i];
					
					// verify that the keyword was not previously processed..
					if(isset($keyword_failures[$KeywordRanker->keyword]) && $keyword_failures[$KeywordRanker->keyword] > 2)
					{
						$this->logERROR("Found Previously Processed Keyword: %s. Aborting.", $KeywordRanker->keyword);
						return;
					}else
						@$keyword_failures[$KeywordRanker->keyword]++;
					
					if(TRUE === $this->LaunchThread(compact('KeywordRanker')))
					{
						// child has run, exit out of thread runner function.
						return;
					}
					$i++;
//					$this->RunChildThread(compact('KeywordRanker'));
				}
			
//				if(false === $this->WaitForThreads())
//					return;
				
			}
		}
	}
	
	function setOptions(\VarContainer $vars) {
		if(!$vars->isEmpty('pagewait'))
			$this->options['sleep'] =	$vars->pagewait;
		
		if($vars->analyze)
			$this->script_mode = self::ANALYZE;
		elseif($vars->crawl)
			$this->script_mode = self::CRAWL;
		elseif($vars->parse)
			$this->script_mode = self::PARSE;
		elseif($vars->proxyprepare)
			$this->script_mode = self::PROXYPREPARE;

		if($vars->verbose)
				$this->options['verbose'] = TRUE;

		if($vars->date)
				$this->options['date'] = $vars->date;

		if($vars->targetkeyword)
			$this->options['targetkeyword'] = $vars->targetkeyword;

		if($vars->getValue('no-google', ""))
			$this->options['google'] = FALSE;

		if($vars->getValue('no-bing',""))
			$this->options['bing'] = FALSE;

		if($vars->cachedir && FileUtil::CreatePath($vars->cachedir)) {
			$this->options['cachedir'] = rtrim($vars->cachedir, '/') . '/';
		}else{
			$this->options['cachedir'] = 'ranking_cache/';
		}
		$this->logINFO("Running %d threads, sleeping %d after each pageload, cachedir is %s",
			$this->num_threads, $this->options['sleep'], $this->options['cachedir']
		);

//		if($vars->ignorecache){
			$this->options['ignorecache'] = false;
//		}
#var_dump($this->options, $vars);
	}

	protected function beforeLaunchThreads()
	{
		switch($this->script_mode){
			case self::CRAWL:
				$date = date('Y-m-d');
				FileUtil::CreatePath($this->options['cachedir']. $date);
				$this->duplicateProxies(true);
				break;
			default:
				break;
		}
	}



	function duplicateProxies($overwrite = false){
		$uid = SearchCrawler::getUIDProxy();
		$proxyFile = 'proxy.' . $uid . '.php';
		if(!file_exists($proxyFile) || $overwrite){
			copy(self::PROXY_FILE, $proxyFile);
		}
	}

	private function RunParseThreads()
	{
		/** @var SearchCrawler[] $searchengines */
		$searchengines = array();
		if(!isset($this->options['google']) || $this->options['google']){
			$searchengines['google'] = new GoogleSearch($this->getQube());
			$searchengines['google']->setLogger($this);
		}

		if(!isset($this->options['bing']) || $this->options['bing']){
			$searchengines['bing'] = new BingSearch($this->getQube());
			$searchengines['bing']->setLogger($this);
		}

		$date = $this->options['date'];
		$cache_dir = $this->options['cachedir'] . $date;
		if(is_dir($cache_dir)){
			$files = scandir($cache_dir);
			$current_keyword = '';
			$current_searchengine = '';
			$rda = new RankingDataAccess();
			$skipKeyword = false;
			foreach($files as $file){
				if (preg_match('/(.+)\.(.+)\.([0-9])/', $file, $info)) {
					list(, $keyword, $searchengine, $page) = $info;
					$keyword = urldecode($keyword);

					if($skipKeyword && $skipKeyword == $keyword){
						$this->logINFO("Skiping page $cache_dir/$file");
						continue;
					}elseif($skipKeyword && $skipKeyword != $keyword){
						$skipKeyword = false;
					}

					if(isset($searchengines[$searchengine])){
						if($current_keyword != $keyword || $current_searchengine != $searchengine){
							$position = 1;
							$current_keyword = $keyword;
							$current_searchengine = $searchengine;
						}

						$results = array();
						$content = file_get_contents("$cache_dir/$file");
						if(!$searchengines[$searchengine]->parsePage($content, $results, $keyword, $page, $position)){
							$this->logERROR("Failed parsing page $cache_dir/$file");
							$skipKeyword = $keyword;
							continue;
						}


						$this->logINFO("Found %s Results on page %s", count($results), $file);
						if($results){
							$rda->saveSearchResults2($results, $date);
						}else{
							$this->logINFO("Not results found for page %s", $file);
						}
					}
				}
			}
		}
	}

	private function RunProxyPrepareThreads()
	{
		/** @var \Zend\Log\Writer\Stream[] $writers */
		$writers = $this->getLogger()->getWriters()->toArray();
		$writers[0]->addFilter(new \Zend\Log\Filter\Priority(\Zend\Log\Logger::ERR));
		$writers[0]->addFilter(new \Zend\Log\Filter\Regex('/.+Proxy.+/'));
		$this->duplicateProxies(true);
		$KeywordRanker = new KeywordRanker(new RankingDataAccess());
		$KeywordRanker->keyword = 'sadasfaghh';
		$this->options['ignorecache'] = true;
		$this->RunSearch('google', $KeywordRanker);
		$this->RunSearch('bing', $KeywordRanker);
	}

	private function RunSingleKeywordThread()
	{
		$searchengines = array();
		if(!isset($this->options['google']) || $this->options['google']){
			$searchengines['google'] = new GoogleSearch($this->getQube());
			$searchengines['google']->setLogger($this);
		}

		if(!isset($this->options['bing']) || $this->options['bing']){
			$searchengines['bing'] = new BingSearch($this->getQube());
			$searchengines['bing']->setLogger($this);
		}

		$date = $this->options['date'];
		$cache_dir = $this->options['cachedir'] . $date;
		$targetkeyword = $this->options['targetkeyword'];
		if(is_dir($cache_dir)){
			$files = scandir($cache_dir);
			$current_keyword = '';
			$current_searchengine = '';
			$rda = new RankingDataAccess();
			$skipKeyword = false;
			foreach($files as $file){
				if (preg_match('/(.+)\.(.+)\.([0-9])/', $file, $info)) {
					list(, $keyword, $searchengine, $page) = $info;

						$keyword = urldecode($keyword);
					if($targetkeyword == $keyword) {
						if ($skipKeyword && $skipKeyword == $keyword) {
							$this->logINFO("Skiping page $cache_dir/$file");
							continue;
						} elseif ($skipKeyword && $skipKeyword != $keyword) {
							$skipKeyword = false;
						}

						if (isset($searchengines[$searchengine])) {
							if ($current_keyword != $keyword || $current_searchengine != $searchengine) {
								$position = 1;
								$current_keyword = $keyword;
								$current_searchengine = $searchengine;
							}

							$results = array();
							$content = file_get_contents("$cache_dir/$file");
							if (!$searchengines[$searchengine]->parsePage($content, $results, $keyword, $page, $position)) {
								$this->logERROR("Failed parsing page $cache_dir/$file");
								$skipKeyword = $keyword;
								continue;
							}


							$this->logINFO("Found %s Results on page %s", count($results), $file);
							if ($results) {
								$rda->saveSearchResults2($results, $date);
							} else {
								$this->logINFO("Not results found for page %s", $file);
							}
						}
					} else	{
						//$this->logINFO("Skiping page $cache_dir/$file");
						continue;
					}
				}
			}
		}
	}
}
