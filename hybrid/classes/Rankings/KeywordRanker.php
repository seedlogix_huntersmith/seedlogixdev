<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Qube\Hybrid\Rankings\SearchEngine;

/**
 * Description of KeywordRanker
 *
 * @author sofus
 */
class KeywordRanker {
	
	protected $site_ID =	NULL;
	protected $keyword_ID =	NULL;
	public $keyword =	NULL;
	public $hostname =	NULL;
	
	/**
	 *
	 * @var SearchEngine
	 */
	private $_searchengine	=	NULL;
	
	/**
	 *
	 * @var RankingDataAccess
	 */
	private $_da	=	NULL;
	
	function setSearchEngine(SearchEngine $dataprovider)
	{
		$this->_searchengine	=	$dataprovider;
	}
	
	function getSearchEngine()
	{
		if(!$this->_searchengine)	throw new QubeException('Search Engine Not Found.');
		
		return $this->_searchengine;
	}
	
	function saveSearchResults($searchengine, array $options)
	{
		$options['keyword_ranker'] = $this;
		try {
			$this->getSearchEngine()->StartCrawler($this->keyword, $options);
		}catch(QubeException $e)
		{
//			$this->getSearchEngine()->logE
			return FALSE;
		}
//		if(!$results)
//		{
//			return FALSE;
//		}
		return true;
//		return $this->_da->saveSearchResults($results, $this, $searchengine);
	}
	
	function __construct(RankingDataAccess $da) {
		$this->_da		=	$da;
	}
	
	function getKeywordID()
	{
		return $this->keyword_ID;
	}
	
	function hasFreshResults($searchengine)
	{
		return $this->{'last_' . $searchengine} == date('Y-m-d');
	}
	
}
