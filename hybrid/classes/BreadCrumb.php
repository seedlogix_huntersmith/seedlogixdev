<?php
/*
 * Author:			Amado Martinez, amado@projectivemotion.com
 * Last Updated:	2017-12-19 / Fernando Cardenas, fer.cardenas@seedlogix.com
 * License:    
 */

/**
 * Description of BreadCrumb
 *
 * @author amado
 */

 class BreadCrumb implements ListItem{
    protected $children     = array();
    protected $text         = '';
    protected $action       = '';
    protected $maxList      = 10;
    /**
     *
     * @var BreadCrumb
     */
    protected $nextcrumb    = NULL;

    function __construct($action,$text,$title = ''){
        $this->action       = $action;
        $this->text         = $text;
        $this->title        = $title;
    }

    function addCrumbTxt($action,$text,$title = ''){
        return $this->addCrumb(new BreadCrumb($action,$text,$title));
    }

    function getNextCrumb(){
        return $this->nextcrumb;
    }

    function addCrumb(BreadCrumb $c){
        if($this->nextcrumb){
            $this->nextcrumb->addCrumb($c);
        } else{
            $this->nextcrumb = $c;
        }
        return $c;
    }

    function setText($text){
        $this->text = $text;
    }

    function addChild($crumb_or_action,$text = NULL){
        $crumb              = isset($text) ? new BreadCrumb($crumb_or_action,$text) : $crumb_or_action;
        $this->children[]   = $crumb;
        return $crumb;
    }

    function printli($url_maker){
?>

                <li><span>/</span><a href="<?= $url_maker->action_url($this->action); ?>"><?= $this->text; ?></a></li>
<?php
        if($this->nextcrumb){
            $this->nextcrumb->printli($url_maker);
        }
    }
}
?>