<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DomainSetup
 *
 * @author Amado
 */
class DomainSetup {
    protected $server;
    protected $XmlApi;
    
    function __construct() {
    }
    
    function Analyze(DomainConfig $DC){
        $this->server   =   $this->getServer($DC);
        $this->XmlApi   =   $this->getXmlApi($this->server);
        return $this;
    }
    
    function SetupRouter(DomainConfig $DC, IgnitedServerApi $XmlApi   =   NULL){
//        if(!$this->XmlApi) $this->Analyze ();
        // set it up
        if(is_null($XmlApi))
            $XmlApi =   $this->Analyze ($DC)->getXmlApi ();
        
        $subdomains =   $DC->getSubdomains($XmlApi);

        if($subdomains->exists('*.' . $DC->getHostname()))    return; // do nothing
        
        $DC->setupWildcardHOST($XmlApi);
    }
    
    /**
     * 
     * @param type $servername
     * @return IgnitedServerApi
     */
    function getXmlApi($servername  =   NULL)
    {
        if(!$servername)
        {
            if($this->XmlApi) return $this->XmlApi;
            
            if($this->server)
                $servername=    $this->server;
            else{
                throw new QubeException('The Nameservers are not configured properly.');
            }
        }
        
        if($servername  ==  'Kennedy') return new KennedyXmlApi();
        if($servername  ==  'Lincoln') return new LincolnXmlApi();
        return new Ignited03XmlApi();
    }
    
    function getServer(DomainConfig $DC){
        $server =   $DC->getConfigStatus();
        
        switch($server)
        {
            case DomainConfig::NS_6Q1:
                return 'Kennedy';
            case DomainConfig::NS_6Q2:
                return 'Lincoln';
            case DomainConfig::NS_6Q3:
                return 'Ignited03';
        }
        return NULL;
        
        $ns1    =   $ns[0];
        $ns2    =   $ns[1];
        $ns3    =   $ns[2];
        $ns4    =   $ns[3];
        
        $kennedyns  =   array('NS1.6QUBE.NET', 'NS1.6QUBE.COM', 'NS1.THEPARTNERPORTAL.COM',
                            'NS2.6QUBE.NET', 'NS2.6QUBE.COM', 'NS2.THEPARTNERPORTAL.COM');
        $lincolnns  =   array('NS3.6QUBE.NET', 'NS3.6QUBE.COM', 'NS3.THEPARTNERPORTAL.COM',
                                    'NS4.6QUBE.NET', 'NS4.6QUBE.COM', 'NS4.THEPARTNERPORTAL.COM');
        $ignited03ns    =   array('NS5.6QUBE,NET', 'NS5.6QUBE.COM', 'NS5.THEPARTNERPORTAL.COM',
                                'NS6.6QUBE.NET', 'NS6.6QUBE.COM', 'NS6.THEPARTNERPORTAL.COM');

        $is_kennedy_dns =   (in_array($ns1, $kennedyns)  && in_array($ns2, $kennedyns) && $ns1 != $ns2) || 
                                (in_array($ns3, $kennedyns) && in_array($ns3, $kennedyns));

        $is_lincoln_dns =   (in_array($ns1, $lincolnns) && in_array($ns2, $lincolnns) && $ns1 != $ns2) ||
                                (in_array($ns3, $lincolnns) && in_array($ns4, $lincolnns));

        $is_3rd_dns     =   (in_array($ns1, $ignited03ns) && in_array($ns2, $ignited03ns) && $ns1 != $ns2) ||
                                (in_array($ns3, $ignited03ns) && in_array($ns4, $ignited03ns));
      
        if($is_kennedy_dns == $is_lincoln_dns && $is_kennedy_dns == $is_3rd_dns)      // if both true or both false.. something isnt right
        {
            return NULL;
            $results['failure'] =   true;
            $results["error"] = 1;
            if($ns1 == ""){

                    $results["message"] = "ns: $domain2 We could not verify the nameserver settings for your domain.  Please contact us at 877-570-5005 to have your website set up manually. Error 5";	
#                        $results["message"] = "ns: We could not verify the nameserver settings for your domain.  Please contact us at 877-570-5005 to have your website set up manually. Error 5";	
            }
            else {
                    $results["message"] = "Domain's nameservers must be set to ".$dispNS1.".".$ns_domain." and ".$dispNS2.".".$ns_domain."<br />";
                    $results["message"] .= "Note: If you recently changed them it may take up to 24 hours to take effect";
            }
        }
        
        if($is_kennedy_dns) return 'Kennedy';
        if($is_lincoln_dns) return 'Lincoln';
        return 'Ignited03';
    }
    
}
