<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 12/28/2014
 * Time: 1:20 AM
 */

class CollectedLeadInput {

    /**
     *
     * @var LeadFormFieldModel[]
     */
    protected $fields   =   array();

    protected $email_value  =   ''; //NULL;

    protected $name_value   =   NULL;

    protected $phone_value = NULL;

    protected $input    =   array();

    private $phone_trust_level = 0;

    private $name_trust_level = 0;

    /** Priority phone values, 1 means Highest priority
     *  Change priority values in order to decide which value should keep in trapPhoneValue function
     */
    /** Match the label name with phone */
    const PHONE_LABEL = 1;

    /** Get phone_value if a field has a valid phone number */
    const PHONE_VALID_NUMBER = 2;

    const NAME_TYPE = 1;

    const NAME_LABEL = 2;

    function setFormFields(array $fields)
    {
        $this->fields = $fields;
    }

    function collectData(VarContainer $data)
    {
        foreach($this->fields as $field)
        {
            $value = $data->getValue($field->ID, '');

            if(is_array($value)){
                $value = implode(' ', $value);
            }

            $this->input[$field->ID]   =
                (object)array('field' => $field,
                            'value' => $value,
                        'valueToText' => $field->getUserFriendlyValue($value));

            if($value == '') continue;
            $this->trapNameValue($field, $value);
            $this->trapEmailValue($field, $value);
            $this->trapPhoneValue($field, $value);
        }
    }

    function trapNameValue(LeadFormFieldModel $f, $value)
    {
        if((!$this->name_trust_level || $this->name_trust_level > self::NAME_TYPE ) && $f->type == 'name')
        {
            $this->name_value   =   $value;
            $this->name_trust_level = self::NAME_TYPE;
            return;
        }

        if((!$this->name_trust_level || $this->name_trust_level > self::NAME_LABEL ) && strpos(strtolower($f->label), 'name') !== false)
        {
            $this->name_value   =   $value;
            $this->name_trust_level = self::NAME_LABEL;
            return;
        }
    }

    function trapEmailValue(LeadFormFieldModel $f, $value)
    {
        if($f->type == 'email')
        {
            $this->email_value  =   $value;
        }
    }

    function trapPhoneValue(LeadFormFieldModel $f, $value)
    {
        if ((!$this->phone_trust_level || $this->phone_trust_level > self::PHONE_LABEL ) && !is_bool(strpos(strtolower($f->label),'phone')))
        {
            $this->phone_value = $value;
            $this->phone_trust_level = self::PHONE_LABEL;
            return;
        }

        if((!$this->phone_trust_level || $this->phone_trust_level > self::PHONE_VALID_NUMBER ) && preg_match('/^[-0-9\(\) \+]{6,}$/', $value)){
            $this->phone_value = $value;
            $this->phone_trust_level = self::PHONE_VALID_NUMBER;
            return;
        }
    }

    function getEmailValue()
    {
        return $this->email_value;
    }

    function getNameValue()
    {
        return $this->name_value;
    }

    function getPhoneValue(){
        return $this->phone_value;
    }

    function asPlaceHolderValues()
    {
        $assocArr   =   array();
        foreach($this->getInputs() as $input)
        {
            $assocArr[$input->field->getLabel()]    =   $input->value;
        }
        return $assocArr;
    }

    function getInputs()
    {
        return $this->input;
    }

    function getInput($field)
    {
        $field_ID = ($field instanceof LeadFormFieldModel) ? $field->ID : (int)$field;
        return $this->input[$field_ID];
    }

    function validateRequiredFields()
    {
        $VS =   new ValidationStack();
        foreach($this->fields as $field)
        {
            if(!$field->isRequired()) continue;
            $value = $this->getInput($field)->value;
            if(empty($value))
            {
                $VS->addError($field->getID(), 'This field is required.');
            }
        }
        return $VS;
    }
}
