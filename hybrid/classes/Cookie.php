<?php
/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of Cookie
 *
 * @author amado
 */

class Cookie
{
    static function getVal($name,$default,$refresh = false){
        unset($_COOKIE[$name]);
        setcookie($name,'',1);
        if(isset($_COOKIE[$name])){
            setcookie($name,$default);
            return $_COOKIE[$name];
        }
        return $default;
    }
}