<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/30/14
 * Time: 2:20 PM
 */

class ReportGenerator {
    /** @var  IReportGenerator */
    private $reportGenerator;

    public function __construct(IReportGenerator $reportGenerator){
        $this->reportGenerator = $reportGenerator;
    }

    public function getOrganicSearchStats(){
        return $this->reportGenerator->getOrganicSearchStats();
    }

    public function getReferralsStats(){
        return $this->reportGenerator->getReferralsStats();
    }

    public function getSocialMediaStats(){
        return $this->reportGenerator->getSocialMediaStats();
    }

    public function getPaidSearchStats(){
        return $this->reportGenerator->getPaidSearchStats();
    }

    public function getDirectTrafficStats(){
        return $this->reportGenerator->getOrganicSearchStats();
    }

    public function getAllStatsDaily(){
        return $this->reportGenerator->getAllStatsDaily();
    }

} 