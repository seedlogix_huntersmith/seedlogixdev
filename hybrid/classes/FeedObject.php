<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */


abstract class FeedObject 
{
    /*  FeedObject Mandatory Fields */
	public $ID;
        /* a name for the object */
	public $NAME;	
        /* a unique key for this object. can be sku, md5, whatever */
	public $KEY;
	
	public $FEEDID;
        /* DB-VALUE if exists */
        protected $CREATED =   false;
        
        /* default meta values set by FeedObjects::getList */
        public $title;
        public $description;
	
        const FOR_SUMMARY   =   1;
        CONST FOR_FULL      =   2;
        
        function __construct($feedinfo  =   null) {
            ;
        }
        
	function Statement()
	{
		static $stmt	=	null;
		if(!$stmt){
			$stmt	=	Qube::GetDB('master')->prepare('REPLACE INTO feedobjects_meta (OBJECTID, KEY, VALUE) VALUES(?, ?, ?)');
		}	
		return $stmt;
	}
        
        abstract function getRequiredTags();
        
        function loadMeta(PDO $db)
        {
            $ID =   $this->ID;
//            $this->Reset();
            $res =   $db->query('SELECT `KEY`, VALUE FROM feedobjects_meta WHERE `KEY` != "title" AND OBJECTID   =   ' . (int)$ID);
            while($row  =   $res->fetch(PDO::FETCH_ASSOC))
            {
//                if(!isset($this->{$row['KEY']}))
                    $this->set($row['KEY'], $row['VALUE']);
            }
            
            return $this;
        }

        function getOrderBy_Categories(){
            return 'order by FULL_NAME ASC';
        }
        
        abstract function getPageRow();
        
        /**
         * this returns the pagename when loading a feedobjects row joined with a feeds row
         * 
         * @return type
         */
        function getFeedObjectTemplateFilename() {
            return $this->pagename;
        }

	protected function UpdateObject(PDO $db){
		static $stmt	=	null;
                static $categories  =   array();    // cache
                
                $objcategories  =   $this->getCategories();
#                echo "Categoreies (Imported) : " . print_r($objcategories, 1) . "\n";
                
                $objcategories_ids  =   array();
                
                $summary    =   $this->getSummary();
                $md5    =   $summary['md5'];
                
                foreach($objcategories as $category_info){
                    list($category_namefull, $category_nameshort)   =   $category_info;
                    
                    if($category_namefull)
                    {
                        if(!isset($categories[$category_namefull]))
                        {
                            $q  =   $db->prepare('SELECT `ID` FROM feedobjects_categories WHERE FULL_NAME    =   ? AND FEEDID = ?');
                            if($q->execute(array($category_namefull, $this->FEEDID)) && $q->rowCount())
                                $category_id    =   $q->fetchColumn();
                            else{
                                $qinsert    =   $db->prepare('INSERT INTO feedobjects_categories (FEEDID, FULL_NAME, SHORT_NAME) VALUES (?, ?, ?)');
                                $qinsert->execute(array($this->FEEDID, $category_namefull, $category_nameshort));
                                $category_id    =   $db->lastInsertId();
                            }
                            $categories[$category_namefull] =   $category_id;
                        }else
                            $category_id    =   $categories[$category_namefull];
                    }
                    $objcategories_ids[]    =   $category_id;
                }
#                var_dump($objcategories_ids);
                
		if(!isset($this->ID))
		{
			if($this->KEY)
			{
				if(!$stmt)
					$stmt	=	$db->prepare('SELECT ID FROM feedobjects WHERE `KEY` = ? AND FEEDID = ?');
					
#					echo 'searching for key:'; var_dump($this);
/*

update hub_page2 hp SET feedobject_id = (select fo.ID from feedobjects fo WHERE 
replace(concat(fo.name,' ', fo.`KEY`), ' ', '-') = hp.page_title_url) WHERE hp.user_parent_id = 6312
*/
				$this->ID	=	$stmt->execute(array($this->KEY, $this->FEEDID)) ? $stmt->fetchColumn() : NULL;
				
#				var_dump('found ID for key', $this->KEY, $this->ID);
			}else{
				throw new Exception('failed');			
			}
		}
	
		if(!$this->ID){
			static $savestmt	=	null;
				if(!$savestmt)
					$savestmt	=	$db->prepare('INSERT INTO feedobjects (ID, FEEDID, NAME, `KEY`, `md5`, CREATED) VALUES(NULL, ?, ?, ?, ?, NOW())');
					
			if($savestmt->execute(array($this->FEEDID, $this->NAME, $this->KEY, $md5)))
				$this->ID	=	$db->lastInsertId();
		}
	
#                if($this->ID == 18) xdebug_break ();
                
		if(($this->ID))
		{
			static $updstmt	=	null;
			if(!$updstmt)
				$qupdstmt	=	$db->prepare('UPDATE feedobjects SET ts=NOW(), NAME = ?, `md5` = ? WHERE ID = ?');
			
			if($qupdstmt->execute(array($this->NAME, $md5, $this->ID)))
                        {
                            $db->query('DELETE FROM feedobjects_catindex WHERE OBJECTID = ' . (int)$this->ID);
                            
                            $catindexvalues =   array();
                            foreach($objcategories_ids as $catid){
				if(empty($catid))	continue;	// something seriously wrong here
                                $catindexvalues[]   =   "($this->ID, $catid)";
                            }
				if(!empty($catindexvalues)){
                            $q  =   'INSERT INTO feedobjects_catindex (OBJECTID, CATEGORYID) VALUES ' . join(', ', $catindexvalues);
                            
#                            echo $q, "\n"; deb($objcategories_ids);
                            $db->query($q);
	}
                        }
		}else 
			throw new Exception('fail 2');
	}
        
        protected function normalize_category($fullcategorypath)
        {
            return preg_replace('/\s+/', '-', strtolower($fullcategorypath));
        }

	function set($name, $value)
	{
            if($name    ==  'title')                // set NAME value based on `title`
                $this->set('NAME', $value);
            
		$isset	=	isset($this->$name);
		$isarray	=	$isset && is_array($this->$name);
		if(!$isarray){
                        $this->$name	=	$isset ? array($this->$name, $value) : $value;
                        return;
                }
		$array	=	&$this->$name;
		$array[]	=	$value;
	}
        
        function get($name)
        {
            return isset($this->$name) ? $this->$name : false;
        }

        /**
         * 
         * @staticvar null $stmtmeta
         * @param type $feedinfo a obj from the feeds table
         */
	function Save($feedinfo)
	{
		$this->FEEDID = $feedinfo->ID;
#		echo 'Saving ' . $this->FEEDID, "\n";
		
		$db	=	Qube::Start()->GetDB('master');
		$db->beginTransaction();
		$this->UpdateObject($db);	
	
		$db->query('DELETE FROM feedobjects_meta WHERE OBJECTID = ' . (int)$this->ID);
		static $stmtmeta	=	null;
		if(!$stmtmeta)
			$stmtmeta	=	$db->prepare('INSERT INTO feedobjects_meta (`OBJECTID`, `KEY`, `VALUE`) VALUES(?, ?, ?)');
                
                foreach($this as $name => $value)
                {
                    if(in_array($name, array('CREATED', 'ID', 'NAME', 'KEY', 'FEEDID'))) continue;
                        if(is_array($value))
                        {

//					var_dump($value);
                                foreach($value as $key => $str)
                                {
                                                $stmtmeta->execute(array($this->ID,
                                                         $name, 
                                                                $str));
                                }
                        }else 
                                                $stmtmeta->execute(array($this->ID, $name, $value));
                }
                $db->commit();
	}
        
	function Reset(){
            foreach($this as $key => $value)
                unset($this->$key);			
            $this->CREATED  =   0;
	}
        
        function getDescription($MODE   = FeedObject::FOR_FULL)
        {
            return 
            $this->description;
        }
        
        function getTitle($MODE = FeedObject::FOR_FULL)
        {
            return $this->title;
        }
        
        function getKEY()
        {
            return $this->KEY;
        }
        
        function getSummary()
        {
            $summary    =   array('title' => $this->getTitle(self::FOR_SUMMARY), 
                        'description' => $this->getDescription(self::FOR_SUMMARY),
                        'KEY' => $this->KEY) + $this->customSummary();
            
            $hash   = md5(serialize($summary));
            
#            echo $hash, "\n";
            $summary['CREATED']   = $this->CREATED;
            $summary['md5'] =   $hash;
            
            return $summary;
        }
        
        /**
         * return array of categories [ [full path, short name ] ]
         * 
         * @return array
         */
        function getCategories()
        {
            return array(NULL, NULL);
        }
        
    function JoinMetaValuesToCategoryQuery(\DBSelectQuery &$q)
    {
        $i = 0;
        foreach($this->appendMetaValuesToCategoryObjects() as $meta_key)
        {
            $tbl_alias  =   'm' . $i;
            $q->leftJoin(array('feedobjects_meta', $tbl_alias), "$tbl_alias.OBJECTID = o.ID AND $tbl_alias.`KEY` = '" . $meta_key . "'")
                        ->addFields("$tbl_alias.VALUE as `$meta_key`");
        }
    }
    
    function appendMetaValuesToCategoryObjects(){
        return array();
    }
    
        protected function customSummary()
        {
            return array();
        }
}


