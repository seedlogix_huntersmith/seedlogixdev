<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 3/17/15
 * Time: 10:26 AM
 */

class CollabQueueEmailer extends QubeThreadedDataProcessor{
	/** @var  NotificationMailer */
	private $mailer;
	/** @var  LeadsDAO */
	private $LDA;

	protected $num_threads = 1;

	function getDA(){
		if($this->LDA == null){
			$this->LDA = new LeadsDAO();
		}

		return $this->LDA;
	}

	function RunThreads()
	{
		$this->mailer = $this->getQube()->getMailer();
		$collaborators = $this->getDA()->getEmailQueuedCollaborators();
		foreach($collaborators as $collaborator){
			$this->LaunchThread(compact('collaborator'));
			if($this->WaitForThreads() === false) return;
		}
	}

	function RunChildThread($data)
	{
		/** @var $collaborator stdClass*/
		extract($data);

		$notification = new LeadFormNotification($collaborator);
		$notification->setSender(new SendTo(null, "no_reply@{$collaborator->reseller_domain}"));
		$to = new SendTo($collaborator->name, $collaborator->email);
		if(!$this->mailer->Send($notification, $to)){
			throw new QubeException('Failed send email');
		}
		$this->logINFO("Email sent: %s", $notification->getContent());
		$this->getDA()->updateCollabEmailSent($collaborator);
	}

	function setOptions(\VarContainer $vars)
	{
		// TODO: Implement setOptions() method.
	}
}