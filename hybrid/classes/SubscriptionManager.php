<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubscriptionManager
 *
 * @author amado
 */
class SubscriptionManager extends Qube\Hybrid\Events\SystemEventDispatch
{
	const N_TRIAL_DAYS = 30;
	/**
	 *
	 * @var ValidationStack
	 */
	protected $VS	=	NULL;
	
	function setValidationStack(ValidationStack $VS)
	{
		$this->VS	=	$VS;
		return $this->VS;
	}
	
	function getValidationStack()
	{
		if(NULL === $this->VS)
		{
			$this->setValidationStack(new ValidationStack());
		}
		
		return $this->VS;
	}
	
	function doTrialSubscriptionRegistration(array $data, SystemDataAccess $sda, ModelManager $MM)
	{
		$SystemUser	=	$sda->createSystemUser();
		$trial_ID	=	$sda->getTrialSubscriptionID();
		
		$uda	=	new UserDataAccess();
		$uda->setActionUser($SystemUser);

		$userinfo = isset($data['userinfo']) ? $data['userinfo'] : false;
		unset($data['userinfo']);

		$UM	=	$MM->doSaveUser($data, 0, $uda);
		
		if($UM instanceof ValidationStack)
			return $UM;
		
		/**
		 *	for Hybrid. Ignore the Reseller Table.=> do not create resellermodel
		 *
		 */
		$expiration = new DateTime();
		$expiration->add(new DateInterval('P' . SubscriptionManager::N_TRIAL_DAYS . 'D'));
		$boolResult	=	$this->subscribeUser($trial_ID, $UM->getID(), 0, $expiration, $uda);

		if($boolResult instanceof ValidationStack)
		{
			$this->getEventManager()->trigger('TrialSubscriptionFailed', $UM->getObject(), array('data' => $data));
			return $boolResult;
		}

		if($userinfo){
			$userinfo['user_id'] = $UM->getObject()->getID();
			$uda->saveUserInfo($userinfo);
		}

		return $UM;
	}
	
	function saveSubscription(array $data, $ID,	UserDataAccess $uda)
	{
		$sub	=	new SubscriptionModel();
		$result	=	new IOResultInfo();
		$VS	=	$this->getValidationStack();
		
		/**
		 * 
		 * Only System Admins allowed to create and modify subscriptions.
		 * 
		 */
		if(!$uda->getActionUser()->can('is_system'))
		{
			return $VS->addError('Access', 'Denied');
		}
		
		$update	=	false;
		if($ID)
		{
			$update	= DBWhereExpression::Create('id = %d', $ID);
			
			// required for setting limits.
			$sub->setID($ID);
			$data['ID']	=	$ID;
		}else{	// create
			
			unset($data['ID']);
		}		
		
		if($sub->SaveData($data, NULL, $VS, NULL, $update, $result))
		{
			$result['object']	=	$sub->Refresh();
			$result['message']	=	'Subscription has been saved.';
			return $result;
		}
		
		return $VS->addError('Error', 'Unexpected Error ocurred');
	}
	
	function subscribeUser($plan_ID, $user_ID, $billing_ID, $date,	UserDataAccess $uda)
	{
		$VS	=	$this->getValidationStack();
		$result	=	new IOResultInfo();
		
		if(!$uda->getActionUser()->can('subscribe', $plan_ID))
		{
			return $VS->addError('Access', 'Denied');
		}
		
		if($new_sub_ID = $uda->subscribeUser($plan_ID, $user_ID, $date, $billing_ID))
		{
			$expiration = new DateTime();
			if($uda->recalculateTOTALLimits($user_ID, $expiration)) {
				$result->message = 'New subscription created.';
				$result->ID = $new_sub_ID;
				return $result;
			}
		}
		
		return $VS->addError('Subscription', 'Error Creating Account Subscription.');
	}

	/**
	 * 
	 * @param type $user_ID
	 * @param type $billing_ID
	 * @param type $limits
	 * @param UserDataAccess $uda
	 * @return int|ValidationStack
	 */
	function subscribeAddOn($user_ID, $billing_ID, $limits, UserDataAccess $uda)
	{
		$VS	=	new ValidationStack();
		$result	=	new IOResultInfo();
		
		$result=	$uda->CreateAccountAddOn($user_ID, $billing_ID, $limits, PointsSetBuilder::firstDayNextMonth(new DateTime()));
		
		if($result)
		{
			return $result;
		}

		return $VS->addError('Subscription', 'Error.');
	}

	function removeAddon($user_ID, $ID, UserDataAccess $uda){
		
		$VS	=	$this->setValidationStack(new ValidationStack());
		$user = $uda->getActionUser();

		if($user->can('remove_addon', 0, $this)){
			return $uda->RemoveAddon($user_ID, $ID);
		}else{
			return $VS->deny('Access denied');
		}
	}

	function removeSubscription($user_ID, $ID, UserDataAccess $uda){
		$VS = $this->setValidationStack(new ValidationStack());
		$user = $uda->getActionUser();

		if($user->can('remove_subscription', $ID, $this)) {
			return $uda->RemoveSubscription($user_ID, $ID);
		}else{
			return $VS->deny('Access denied');
		}
	}

	/**
	 * Cancels all existing unexpired subscriptions.
	 * Adds a New Subscription.
	 *
	 * @param $user_ID
	 * @param $subscription_ID
	 * @param $new_plan
	 * @param $date
	 * @param UserDataAccess $uda
	 * @return IOResultInfo|ValidationStack
	 */
	function changeSubscription($user_ID, $new_plan, $date, UserDataAccess $uda){
		$VS = $this->setValidationStack(new ValidationStack());
		$new_subscription = $this->subscribeUser($new_plan, $user_ID, 0, $date, $uda);
		if($new_subscription instanceof ValidationStack)
			return $new_subscription;

		$result = $uda->CancelSubscription($user_ID, $new_subscription->ID*-1);
		//$this->cancelSubscription($user_ID, -1, $uda);

		if($result && !($result instanceof ValidationStack)){
//				$uda->RemoveSubscription($user_ID, $subscription_ID);
			return $new_subscription;
		}
//        if(FALSE === $uda->recalculateTOTALLimits($user_ID, new DateTime()))
//        {
            return $VS->addError('Database', 'Database error');
//        }
//        return TRUE;
//		$uda->RemoveSubscription($user_ID, $new_plan);

        if($result instanceof ValidationStack){
            return $result->addError("Not enough limits", "Can't remove subscription");
        }else{
		}
	}

	function cancelSubscription($user_id, $subscription_id, UserDataAccess $uda){
		$VS = $this->setValidationStack(new ValidationStack());
		$actionUser = $uda->getActionUser();
		if($actionUser->can('cancel_subscription', $subscription_id, $this)){
			if(!$uda->CancelSubscription($user_id, $subscription_id))
			{
				return $VS->deny("Failed to cancel the subscription.");
			}
		}else{
			return $VS->deny('Access Denied');
		}
		$result = new IOResultInfo();
		$result->message = 'Subscription Canceled.';
		return $result;
	}

}
