<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/2/14
 * Time: 10:33 AM
 */

class SubscriptionsWidget extends DataTableWidget{

    function initView(ThemeView $V)
    {
        return $this->_config;

    }

    function ajaxModifyTOTAL(VarContainer $V)
    {
        if(!$this->getController()->getUser()->isSystem()) return NULL; // @todo log security event

        $id = $V->ID;

        $updatefields   =   array();
        foreach($V->getData() as $k => $v)
        {
            switch($k)
            {
                case 'limits_json':
                    $json_obj = json_decode($v);
                    if(NULL == $json_obj)
                        return $this->getController()->returnValidationError('Bad Json Format.');
                    $v = json_encode($json_obj);
                    // validate
                case 'expiration_date':
                    // validate
                    $v = preg_match('#^[0-9]+/[0-9]+/[0-9]+$#', $v) && $v != '00/00/0000' ? date('Y-m-d', strtotime($v)) : $v;

                    $updatefields[$k]   =   $v;
                    $updateX[]  =   "`$k` = :$k";
            }
        }

        $qda    =   QubeDataAccess::FromController($this->getController());
        $query = sprintf('UPDATE account_limits SET %s WHERE ID = %d', join(', ', $updateX), $id);
        $stmt = $qda->prepare($query);
        $stmt->execute($updatefields);

        return $this->getController()->returnAjaxData('message', "OK");
    }

    function doAction($widget_action)
    {
        switch($widget_action){
            case 'ModifyTOTAL':
                return $this->ajaxModifyTOTAL(VarContainer::getContainer($this->_config['subscription']));

            case 'Subscriptions':
               return $this->ajaxSubscriptions(VarContainer::getContainer($this->_config));
            case 'Create':
                return $this->ajaxSuscribeUser(VarContainer::getContainer($this->_config['subscription']));
            case 'Change':
                return $this->ajaxChangeSubscription(VarContainer::getContainer($this->_config['subscription']));
            case 'CancelSubscription':
                return $this->ajaxCancelSubscription(VarContainer::getContainer(VarContainer::getContainer($_GET)));
            case 'CancelAddon':
                return $this->ajaxCancelAddon(VarContainer::getContainer(VarContainer::getContainer($_GET)));
        }
    }

		
		function getFirstPageAccountLimits()
		{
			$var	= VarContainer::getContainer(array('iDisplayStart' => 0));
			return $this->getDT($var)->getData($this->_config['serverParams']);
		}
		
		function getTotalAccountLimits()
		{
			return $this->getDT(new VarContainer())->getTotalDisplayRecords();
		}
		
		function getDT(VarContainer $var)
		{  
			$var->load(array('table' => 'AccountLimits'));
			$uda = UserDataAccess::FromController($this->getController());
			$DT =   new DataTableAccountLimitsResult($uda, $var);
			return $DT;
		}

    function ajaxSubscriptions(VarContainer $var){      
				$DT	=	$this->getDT($var);
        $DT->setRowCompiler($this->getController()->getView('rows/system-account-limits', false, false), 'limit');
		$columnsDef = DataTableResult::getTableColumns('AccountLimits');
		$columns = explode(',', $this->_config['sColumns']);
        return $this->getController()->getAjaxView($DT->getArray(array(
			'user_id' => $this->_config['user_id'],
			'order' => $columnsDef[$columns[intval($this->_config['iSortingCols'])]]['Name'],
			'new_rows_ids' => $this->_config['newRowsIds']
		)));
    }

    private function ajaxSuscribeUser(VarContainer $var)
    {
        $uda = new UserDataAccess();
        $user = $this->getController()->getuser();  //$uda->getUserWithRoles($this->getController()->getUser()->getID());
        $uda->setActionUser($user);

        $user_subscriber_id =   $user->getID();
        if($user->isSystem())
        {
            $user_subscriber_id =   $var->get('parent_id');
        }

        $sm = new SubscriptionManager();
        $result = $sm->subscribeUser($var->get('subscription_id'), $user_subscriber_id,
                $var->get('billing_id'), $var->get('expiration_date'), $uda);


        return $this->getController()->getAjaxView($result);
    }

    /**
     * Cancels the current user subscription and creates a new subscription with the specified parameters.
     *
     * @param VarContainer $var
     * @return ThemeView
     * @throws Exception
     * @throws QubeException
     */
    private function ajaxChangeSubscription(VarContainer $var)
    {
        $uda = new UserDataAccess();
        $sm = new SubscriptionManager();
        $uda->setActionUser($this->getController()->getUser());
//        $stmnt = $this->getController()->getQube()->GetDB()->prepare("SELECT id FROM account_limits WHERE user_ID = :user_id AND type = 'SUB' AND expiration_date > CURRENT_DATE");
//        $stmnt->execute(array('user_id' => $var->get('parent_id')));
//
//        $old_subscription_id = $stmnt->fetchColumn();
        $result = $sm->changeSubscription($var->get('parent_id'), $var->get('subscription_id'), $var->get('expiration_date'), $uda);

        if($result instanceof ValidationStack)
            return $this->getController()->returnValidationError($result);

        return $this->getController()->returnAjaxData('message', "OK.");

    }

    private function ajaxCancelSubscription(VarContainer $var)
    {
        $uda = new UserDataAccess();
        $sm = new SubscriptionManager();
        $user = $this->getController()->getUser();
        $uda->setActionUser($user);

        $user_subscriber_id =   $user->getID();
        if($user->isSystem())
        {
            $user_subscriber_id =   $var->get('parent_id');
        }

        $result = $sm->cancelSubscription($user_subscriber_id, $var->get('subscription_id'), $uda);
        return $this->getController()->getAjaxView($result);
    }

    private function ajaxCancelAddon(VarContainer $var)
    {
        $uda = new UserDataAccess();
        $sm = new SubscriptionManager();
        $uda->setActionUser($this->getController()->getUser());
        $result = $sm->removeAddon($var->get('parent_id'), $var->get('subscription_id'), $uda);
        return $this->getController()->getAjaxView($result);
    }


}