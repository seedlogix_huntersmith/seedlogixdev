<?php
use Qube\Hybrid\Rankings\SearchEngine;

/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 1/27/15
 * Time: 11:53 AM
 */

abstract class SearchCrawler extends \Crawler implements SearchEngine{
    public $ignoreCache;
    protected $verbose = FALSE;
    protected $cache_dir = FALSE;
	protected $searchengine = '';
	protected $cache_enabled = TRUE;
	/** @var int Max number of intents before abort with a keyword*/
	const MAX_FAILS = 20;

	/**
	 * @var int <p>Default number of results in a search engine page.</p>
	 * Must be overwriten in child class
	 */
	protected $resultsPerPage = NULL;

	/** @var int <p>Number of results retreived in a single curl call</p>
	 *  Must be overwriten in child class
	 */
	protected $resultsPerFetch = NULL;

	/** @var int <p>Number of pages required in ranking results reports</p>*/
	protected $pagesRequired = 10;

	/** @var Qube  */
	private $qube;

	public abstract function fetchNextURL($content, $keyword);
	public abstract function parsePage($content, &$results, $keyword, $pageNumber, &$position);
	public abstract function getUrl($keyword, $pagenumber);

	function getCacheKey($q, $position)
    {
        $date = date('Y-m-d');
		$success = FileUtil::CreatePath($this->cache_dir . $date);
		$subpath = $success ? $date . '/' : $date;
        $q = urlencode($q); //Prevent special chars used in file name, like '/'
        return "$date/$q.{$this->searchengine}.$position";
    }

	static function getUIDProxy(){
		$date = new DateTime();
		return $uid = $date->format('y-m-d');
	}

    public function StartCrawler($keyword, array $options){
        $this->cache_dir = $options['cachedir'];
        $pagenumber = 1;
        $fail_count = 0;
        $startAgain = false;
        $url = $this->getURL($keyword, $pagenumber);
		$this->getProxy(); //initialize $this->proxy
        $this->ignoreCache = $options['ignorecache'];
		$DA = new RankingProxiesDataAccess();
        while($pagenumber <= $this->getRequiredPages() && $fail_count <= self::MAX_FAILS){
            if($startAgain){
                $fail_count++;
                $startAgain = false;
                $pagenumber = 1;
				$DA->incrementProxyKeywordError($this->proxy, $this->searchengine);
				$this->proxy = null;
				$this->ignoreCache = true;
				$url = $this->getURL($keyword, $pagenumber);
				$this->logDEBUG("Starting again, fail_count: %d, cache disabled for %s on page %s for %s", $fail_count, $keyword, $pagenumber, $this->searchengine);
            }

            if($content = $this->fetchPage($url, $keyword, $pagenumber)) {
                if($pagenumber < $this->getRequiredPages())
                    $url = $this->fetchNextURL($content, $keyword);
                if(!$url){
                    $startAgain = true;
                    continue;
                }
            }else{
                $startAgain = true;
                continue;
            }

			$DA->incrementProxyAttempts($this->proxy, $this->searchengine);
            $pagenumber++;
        }

		if($fail_count >= self::MAX_FAILS){
            $this->logERROR("Max intents reached for $keyword; With SearchEngine: $this->searchengine");
            return false;
        }

        return true;
    }

    public function fetchPage($nextURL, $keyword, $position){
        $fileName = $this->getCacheKey($keyword, $position);;
        $path = $this->cache_dir . $fileName;
        if(file_exists($path) && !$this->ignoreCache){
            $this->logINFO('Cache hit for %s.', $fileName);
            return file_get_contents($path);
        }elseif($content = $this->savePageToCache($nextURL, $path, $keyword)){
            $this->logINFO('Page saved for %s. with URL: %s', $fileName, $nextURL);
            return $content;
        }

        return false;
    }

    public function savePageToCache($nextURL, $savePath, $keyword){
        $content = $this->load_page($nextURL, false, $this->proxy);
        if(!$content){
            $this->logERROR("Failed to retreive page: %s with Proxy: %s URL: %s", $nextURL, $this->proxy, $nextURL);
			$this->getRankingProxiesDataAccess()->GetPDO()->query("INSERT INTO rankings_log (keyword, proxy, url, date) VALUES ('$keyword', '{$this->proxy}', '$nextURL', CURDATE())");
			$this->getRankingProxiesDataAccess()->incrementProxyTimeOutError($this->proxy, $this->searchengine);
            return false;
        }

        if(!file_put_contents($savePath, $content)){
            $this->logERROR("Failed to save content page: %s", $nextURL);
            return false;
        }

        return $content;
    }

	/**
	 * <p>Get the number of pages in terms of curl calls based on how many curl calls
	 *  must execute to fulfill the pages required in ranking results reports</p>
	 * @return float
	 */
	function getRequiredPages(){
		return ($this->resultsPerPage * $this->pagesRequired) / $this->resultsPerFetch;
	}

	/**
	 * @return string
	 */
	public function getProxy(){
		if(!$this->proxy)
			$this->proxy = $this->getRankingProxiesDataAccess()->getPreferredProxy($this->searchengine);
		return $this->proxy;
	}

	/**
	 * @return Qube
	 */
	public function getQube()
	{
		return $this->qube;
	}

	/**
	 * Get the number of results indicated in the content page
	 * @param $content
	 * @return int
	 */
	public abstract function getNoResultsInContent($content);
}
