<?php
# require_once(dirname(__FILE__) . '/../6qube/core.php');
//this script is pulled as an include on theme pages and builds a custom form onto the page
//it MUST be called with the form's ID and user ID.  it can alternately be called with get-type args

use Zend\Form\Element;

class FormOutput
{
	static function getFormOutput($userID, $formID, HubModel $hub, $searchEngine, $seKeyword, $pageID, $referrerID)
	{
		//get form data
		$params =   array('formID' => $formID,
#                 'userID'    => $row['user_id'],
			'userID'	=> $userID,
			'hubID' => $hub->getID(),
			'hubPageID' => $pageID,
'searchEngine' => $searchEngine,
			'seKeyword' => $seKeyword,
			'js'    => false,
			'style' => false,
			'outputWrapper' => 'router'
		);
		require_once HYBRID_PATH . 'classes/FormOutput.php';

		$Form   =   new FormOutput();
		$formCode   =   $Form->GetForm($params, $hub);
		//form tags:
		$formData = str_replace('((referrer))', $referrerID, $formCode);
		return $formData;
	}

    function printStyle()
    {

?>
<style type="text/css">
form fieldset {
	border: 0;
}
form ul {
	list-style: none;
}
form ul li {
	padding: 5px;
}
form ul li label {
	display: block;
	font-size: 15px;
	font-weight: bold;
	color: #000;
}
form ul li label span {
	font-size: 13px;
	font-weight: normal;
	color: #666;
}
form ul li input.small {
	width: 100px;
}
form ul li input.medium {
	width: 250px;
}
form ul li input.large {
	width: 400px;
}
form ul li textarea.small {
	width: 200px;
	height: 75px;
}
form ul li textarea.medium {
	width: 350px;
	height: 120px;
}
form ul li textarea.large {
	width: 500px;
	height: 200px;
}
form ul li .checkboxDiv {
	margin:0;
	padding:0;
	display: inline-block;
}
form ul h2 {
	margin-top: 20px;
	margin-bottom: 5px;
}
.highlightError {
	border: 1px solid #ff0000;
}
.checkboxDivHighlightError {
	padding: 1px;
	background-color: #ff0000;
}
</style>
<?	        
    }
    
    function printJS($ajaxSubmit, $formID)
    {

?>
<script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.form<?= $formID ?>').unbind();
	$('.form<?= $formID ?>').bind('submit', function (<?php
                                                                if($ajaxSubmit) echo 'event'; ?> ){
		<? if($ajaxSubmit){ ?>
			event.stopPropagation();
			$(this).attr('target', '');
		<? } ?>
		$('.highlightError').each(function(){
			$(this).removeClass('highlightError');
		});
		$('.checkboxDiv').each(function(){
			if($(this).hasClass('checkboxDivHighlightError')){
				$(this).removeClass('checkboxDivHighlightError');
			}
		});
		var errors = 0;
		var numOnlyErrors = 0;
		var numOnlyErrorText = '';
		$('.required').each(function(){
			if($(this).val()==''){
				$(this).addClass('highlightError');
				errors += 1;
			}
			else if($(this).hasClass('checkbox')){
				if(!$(this).attr('checked')){
					$(this).parent().addClass('checkboxDivHighlightError');
					errors += 1;
				}
			}
		});
		$('.numsOnly').each(function(){
			var val = $(this).attr('value');
			if(val && isNaN(val)){
				var fieldName = $(this).parent().find('label').html();
				numOnlyErrors += 1;
				numOnlyErrorText = numOnlyErrorText+fieldName+' may only contain numbers\n';
			}
		});
		if(errors>0 || numOnlyErrors>0){
			if(errors>0) alert('Please complete the highlighted required fields');
			if(numOnlyErrorText) alert(numOnlyErrorText);
			return false;
		}
		<? if($ajaxSubmit){ ?>
		if(errors==0){
			var submitURL = $(this).attr('action');
			var params = $(this).serialize();
			$.post(submitURL, params, function(data){
				if(data.success==1){
					if(!data.redir) alert('Your response was successfully sent!');
					else window.location = data.redir;
				}
				else {
					alert('Sorry, there was an error submitting your form.  Please try again.');
				}
			}, 'json');
			return false;
		}
		<? } ?>
	});
	$('#reset').bind('click', function(){
		if(confirm('Are you sure you want to clear the entire form?')){
			$('.form<?= $formID ?> input').each(function(){
				if($(this).attr('id')!='submit' && $(this).attr('id')!='reset'){
					$(this).attr('value', '');
					if($(this).hasClass('checkbox')){
						$(this).attr('checked', false);
					}
				}
			});
			$('.form<?= $formID ?> textarea').each(function(){
				$(this).val('');
			});
		}
		return false;
	});
});
</script>
<?php

    }

    function GetForm($vars, TouchpointPage $touchpointPage)
    {
        if(!isset($formID)) $formID = $vars['formID'];
        if(!isset($userID)) $userID = $vars['userID'];
        if(!isset($hubID)) $hubID = $vars['hubID'];
        if(!isset($hubPageID)) $hubPageID = $vars['hubPageID'];
        if(!isset($searchEngine)) $searchEngine = $vars['searchEngine'];
        if(!isset($seKeyword)) $seKeyword = $vars['seKeyword'];
        if(!isset($vars['outputWrapper']))
			$outputWrapper	=	$vars['outputWrapper'];
		else
			$outputWrapper	=	'router';

/* @var $LeadForm LeadFormModel */
        $LeadForm   =   Qube::Start()->queryObjects('LeadFormModel')->Where('id = %d AND user_id = %d', $formID, $userID)->Exec()->fetch();
        if($LeadForm    === FALSE)
        {
            /*
            //get user info
            $query = "SELECT parent_id, class FROM users WHERE id = '".$userID."'";
            $user = $hub->queryFetch($query, NULL, 1);
            
            //if the form couldn't be found check if it's a reseller MU form
            $query = "SELECT user_parent_id, data, options FROM lead_forms 
                            WHERE user_id = '".$user['parent_id']."' AND id = '".$formID."'
                            AND no_access_classes NOT LIKE '".$user['class']."::%'
                            AND no_access_classes NOT LIKE '%::".$user['class']."::%'
                            AND no_access_classes NOT LIKE '%::".$user['class']."'";
             * 
             * 
             */
            $LeadForm = Qube::Start()->queryObjects('LeadFormModel', 't')->Join(array('UserModel', 'u'), 't.user_id = u.parent_id')
                        ->Fields('t.id, t.user_id, t.user_parent_id, t.data, t.options, t.submit_button_text, t.redirect_url, t.show_reset_button, t.ajax_submit, t.version')
                    ->Where('u.id = %d AND t.id = %d AND CONCAT("::", no_access_classes, "::") NOT LIKE CONCAT("%%s::", u.class, "::%%") ', $userID, $formID)->Exec()->fetch();
        }
        
        // @todo cache?
        $obj    =   ob_start();
        if($LeadForm    === false   or !$LeadForm->data){
#           Qube::Fatal('ivnalid form [not found: ' . $formID . ', ' . $userID);
#		echo 'Invalid Form: ', $formID, ',', $userID';
	}
        else{
            if(@$vars['style'])
                $this->printStyle();
            
            $options    =   $this->printForm($LeadForm, $touchpointPage, $seKeyword, $searchEngine, $hubPageID, $outputWrapper);
            
            if(@$vars['js'])
                $this->printJS ($options['ajaxSubmit'], $formID);
        }
        $formhtml   = ob_get_contents();
        
        ob_end_clean();
        
        return $formhtml;
    }

	/**
	 * @param array $fields
	 * @param       $themeWrapper
	 *
	 * @internal param object $theme
	 */
	function printFields(array $fields, $themeWrapper)
	{
		foreach($fields as $f)
		{
			self::printElement($f, $themeWrapper);
		}
	}

	/**
	 * @param LeadFormFieldModel $f
	 * @param object             $themeWrapper
	 */
	static function printElement(LeadFormFieldModel $f, $themeWrapper)
	{
//		require HYBRID_PATH . 'themes/inline/input/textarea.php';// . $f->type . '.php';
//		return;
//		$element = new Element();

		$control = $f->type;
		switch($f->type)
		{
			case 'checkbox':
			case 'section':
			case 'name':
			case 'hidden':
			case 'textarea':
				break;
			case 'select':
			case 'state':
				$control = 'select';
				break;
			case 'date':
			case 'money':
			case 'email':
			case 'text':
				$control = 'text';
				break;
		}
		require HYBRID_PATH . 'themes/inline/input/field.php';
	}

	/**
	 * @param $fields
	 * @deprecated
	 */
	function printFieldsLegacy($fields)
	{

		foreach($fields as $fieldNum=>$field){
			$fieldType = $field['type'];
			if($fieldType!='hidden') echo '<li>';
			if($fieldType!='name' && $fieldType!='checkbox' && $fieldType!='section' && $fieldType!='hidden'){ ?>
				<label><?=$field['label']?> <? if($field['label_note']){ ?><span><?=$field['label_note']?></span><? } ?></label>
				<?		if($fieldType=='text'){ ?>
					<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" placeholder="<?=$field['default']?>" value="" class="<? if($field['required']){ ?>required<? } ?> <? if($field['numsOnly']){ ?>numsOnly<? } ?> <?=$field['size']?>" id="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" />
				<?		}
				else if($fieldType=='textarea'){ ?>
					<textarea name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" class="<? if($field['required']){ ?>required<? } ?> <?=$field['size']?>"><?=$field['default']?></textarea>
				<?		}
				else if($fieldType=='select'){ ?>
					<select name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" <? if($field['required']){ ?>class="required"<? } ?>>
						<?
						if($field['options']){
							foreach($field['options'] as $key=>$value){ ?>
								<option value="<?=$value['key']?>"><?=$value['value']?></option>
							<?	}
						}
						?>
					</select>
				<?		}
				else if($fieldType=='email'){ ?>
					<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" placeholder="<?=$field['default']?>" value="" class="email <? if($field['required']){ ?>required<? } ?> <?=$field['size']?>" />
				<?		}
				else if($fieldType=='money'){ ?>
					<span style="float:left;" class="moneySymbol">$</span><input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" value="<?=$field['default']?>" class="numsOnly <? if($field['required']){ ?>required<? } ?>" style="width:75px;" />
				<?		}
				else if($fieldType=='date'){
					if(!$field['default']) $dateM = $dateD = $dateY = '';
					else if($field['default']=='today'){ $dateM = date('m'); $dateD = date('d'); $dateY = date('Y'); }
					else if($field['default']=='custom'){ $dateM = $field['defaultM']; $dateD = $field['defaultD']; $dateY = $field['defaultY']; }
					?>
					<div style="display:inline-block;">
						<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>_M" value="<?=$dateM?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:30px;" maxlength="2" />
					</div>
					<span class="dateSeparator">/</span>
					<div style="display:inline-block;">
						<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>_D" value="<?=$dateD?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:30px;" maxlength="2" />
					</div>
					<span class="dateSeparator">/</span>
					<div style="display:inline-block;">
						<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>_Y" value="<?=$dateY?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:45px;" maxlength="4" />
					</div>
				<?		}
			} //end if($fieldType!='name'){
			else if($fieldType=='name'){
				?>
				<div style="display:inline-block;" class="name-field">
					<label><?=$field['label1']?></label>
					<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label1'])?>" placeholder="<?=$field['default1']?>" value="" <? if($field['required1']){ ?>class="required"<? } ?> />
				</div>
				<div style="display:inline-block;" class="name-field">
					<label><?=$field['label2']?></label>
					<input type="text" name="<?=Hub::fieldNameFromLabelStatic($field['label2'])?>" placeholder="<?=$field['default2']?>" value="" <? if($field['required2']){ ?>class="required"<? } ?> />
				</div>
			<?	}
			else if($fieldType=='checkbox'){ ?>
				<div class="checkboxDiv"><input type="checkbox" name="<?=Hub::fieldNameFromLabelStatic($field['label'])?>" class="checkbox <? if($field['required']){ ?>required<? } ?>" <? if($field['checked']){ ?>checked<? } ?> /> <label style="display:inline-block;"><?=$field['label']?> <? if($field['label_note']){ ?><span><?=$field['label_note']?></span><? } ?></label></div>
			<?
			}
			else if($fieldType=='section'){ ?>
				<h2><?=$field['label']?></h2>
			<?
			}
			else if($fieldType=='hidden'){ /*
				$hiddenInputs .= '<input type="hidden" name="'.Hub::fieldNameFromLabelStatic($field['label']).'" value="'.$field['default'].'" />';
 */
			}
			if($fieldType!='hidden') echo '</li>';
			$fieldType = $dateM = $dateD = $dateY = NULL; //reset vars
		} //end foreach(fields)
	}

	/**
	 * @param string $skin
	 *
	 * @return stdClass
	 */
	function getCustomWrapper($skin = 'router')
	{
		$customWrapper = new stdClass();

		switch($skin)
		{
			case 'router':
			case 'legacy':
				$customWrapper->defaultWrap = 'legacy';
				$customWrapper->wrapperPrefix = '<fieldset><ul>';
				$customWrapper->wrapperSuffix = '</ul></fieldset>';
				$customWrapper->containerPrefix = '<li>';
				$customWrapper->labelPrefix = '';
				$customWrapper->inputPrefix = '';
				$customWrapper->notePrefix = '<span>';
				$customWrapper->noteSuffix = '</span>';
				$customWrapper->notePos = 'label';
				$customWrapper->submitWrapSuffix = '<li class="submit-li">';
				$customWrapper->submitWrapPreffix = '</li>';
				$customWrapper->suffix = '</li>';
				$customWrapper->submitClass = '';
				$customWrapper->resetClass = '';
				break;
			case 'preview':
			case 'hybrid':
				$customWrapper->defaultWrap = 'custom';
				$customWrapper->wrapperPrefix = '';//<fieldset><div class="widget fluid"><div class="whead"><h6>Generated Form Preview</h6></div>';
				$customWrapper->wrapperSuffix = '';//</div></fieldset>';
				$customWrapper->containerPrefix = '<div class="formRow">';
				$customWrapper->labelPrefix = '<div class="grid3">';
				$customWrapper->inputPrefix = '<div class="grid9">';
				$customWrapper->notePrefix = '<span class="note">';
				$customWrapper->noteSuffix = '</span>';
				$customWrapper->notePos = 'input';
				$customWrapper->submitWrapSuffix = '<div class="formRow"><div class="right">';
				$customWrapper->submitWrapPreffix = '</div></div>';
				$customWrapper->suffix = '</div>';
				$customWrapper->submitClass = 'buttonS bLightBlue';
				$customWrapper->resetClass = 'buttonS bRed';
				break;
		}

		return $customWrapper;
	}

	/**
	 * @param LeadFormModel  $form
	 * @param TouchpointPage $hub
	 * @param                $seKeyword
	 * @param                $searchEngine
	 * @param                $hubPageID
	 * @param                $theme
	 *
	 * @return array
	 */
	function printForm(LeadFormModel $form, TouchpointPage $hub, $seKeyword, $searchEngine, $hubPageID, $theme)
    {
		$fields	= $form->getFields();
		//$fields = Hub::parseCustomFormFieldsStatic($form->data);
		//$options = $this->getOptions($form);
		//$ajaxSubmit = $options['ajaxSubmit'];
		list($showButtonReset, $submitButtonText, $redirectUrl, $ajaxSubmit) = $this->getOptions($form);
        $hiddenInputs = '';
        $formID = $form->id;
        $userID = $hub->user_id;	#$form->user_id;
        
		//$domain = $hub->domain;   @todo prefix with 'http://'
		$domain = $hub->httphostkey ? $hub->httphostkey . '/' : '';
		if($hub->ssl_on==1){
			if($hub->lincoln_domain==1){
				$domain = $_SERVER['HTTP_HOST'].'/';
			}
			$submitUrl = 'https://' . $domain . 'process-custom-form.php';
		}
		else $submitUrl = 'http://' . $domain . 'process-custom-form.php';

		if($hub instanceof HubModel){
        	$pv = $hub->pages_version;
		}else{
			$pv = 1;
		}

	    $themeWrapper = self::getCustomWrapper($theme);
?>
	<form action="<?=$submitUrl?>" method="post" class="custom-form form<?= $formID ?><?=$ajaxSubmit ? ' ajaxSubmit"' : '" target="hiddenIframe"'?> id="<?= $formID ?>">

<?php
	    echo $themeWrapper->wrapperPrefix;
	    if($fields):
			self::printFields($fields, $themeWrapper);
			//self::printFieldsLegacy($fields);
?>
            <?=$themeWrapper->submitWrapSuffix?>
				<?=$hiddenInputs?>

				<?php if($hub):?>
				<input type="hidden" name="touchpointID" value="<?=$hub->touchpoint_ID?>"/>
				<?php endif;?>

				<input type="hidden" name="visitorId" value="" />
				<input type="hidden" name="formID" value="<?= $formID ?>" />
				<input type="hidden" name="userID" value="<?=$userID?>" />
				<input type="hidden" name="hubID" value="<?= $hub->id ?>" />

				<?php if($hubPageID): ?>
				<input type="hidden" name="hubPageID" value="<?=$hubPageID?>" />
				<input type="hidden" name="pagesVersion" value="<?=$pv?>" />
				<?php endif; ?>

				<input name="keyword" value="<?=$seKeyword?>" type="hidden" />
				<input name="search_engine" value="<?=$searchEngine?>" type="hidden" />
				<input type="text" name="firstmiddlelastsurname" value="" style="width:1px;display:none;" tabindex="-1" />
				<!-- RESET -->
				<?php if($showButtonReset): ?>
				&nbsp;&nbsp;&nbsp;
				<input type="reset"
						value="Reset"
						class="reset <?=$themeWrapper->resetClass?>" />
				<?php endif; ?>
				<!-- SUBMIT -->
				<input type="submit"
					value="<?=$submitButtonText?>"
					<? if($themeWrapper->defaultWrap == 'legacy'){ echo 'id="submit"'; } ?>
					class="submit <?=$themeWrapper->submitClass?>" />
				<!-- Ajax Submit -->
                <?php if($ajaxSubmit): ?>
                <input type="hidden" name="ajaxSubmit" value="1" />
                <?php endif; ?>
            <?=$themeWrapper->submitWrapPreffix?>
<?php
		endif;
		echo $themeWrapper->wrapperSuffix;
?>

	</form>
        <? if(!$ajaxSubmit){ ?>
        <iframe src="<?=$submitUrl?>" id="hiddenIframe" name="hiddenIframe" scrolling="no" hidefocus="true" width="1" height="1" frameborder="0" style="display:none;"></iframe>
        <? }
        
        return compact($showButtonReset, $submitButtonText, $redirectUrl, $ajaxSubmit);
    }

	/**
	 * @param LeadFormModel $form
	 * @return array
	 */
	public function getOptions(LeadFormModel $form)
	{
		if($form->version < 1){
			return  explode('||', $form->options);
		}
		return array($form->show_reset_button,
			$form->submit_button_text,
			$form->redirect_url,
			$form->ajax_submit
		);
	}
}

