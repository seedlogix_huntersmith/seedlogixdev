<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileUtil
 *
 * @author Amado
 */
class FileUtil {

  /**
   * 
   * rETURNS  a Basename
   * 
   * @param type $fname
   * @param type $destination
   * @return string
   * @throws QubeException
   */
  static function GetFileUniqueName($fname, $destination) {
    $counter = 0;
    
    // quick sanitation
    $fname = preg_replace(array('/[^\w\.-_]+/', '/jpeg$/'), array('_', 'jpg'), $fname);
    
    $finfo = pathinfo($fname);
    while (true) {
      $newname = $finfo['filename'] . ( ++$counter == 1 ? '' : '_' . $counter) . '.' . $finfo['extension'];

      // generate new name based on counter if the file exists
      if (file_exists($destination . DIRECTORY_SEPARATOR . $newname))
        continue;
      
      return $newname;

//      // if not exists, set the new name and break out of the loop
//      $upload->addFilter('Rename', array('target' => $newname), $fkey);
//      break;
    }
    
    throw new QubeException('Flow Error');
  }

  static function CreatePath($fullpath) {
    $destination = $fullpath;
    $depths = preg_split('[\\/]', $destination);
//        if($depths[0] == "") array_shift ($depths); // /home/path (remove empty "" string)
//         var_dump($depths);
    if (is_dir($destination)) return true;
      $exists = $depths[1];
      $ix = 2;
      for ($ix = 2; $exists = implode(DIRECTORY_SEPARATOR, array_slice($depths, 0, $ix)); $ix++) {
        if (!is_dir($exists))
          break;
      }
#            $ix--;
      while ($ix <= count($depths)) {
        $dir = implode(DIRECTORY_SEPARATOR, array_slice($depths, 0, $ix));
//                echo $ix . ' Making ' . $dir;
        if(FALSE === mkdir($dir)) return FALSE;
        $ix++;
      }

    return true;
  }

  static function CreateSymlink($target, $link)
  {
	  $br	=	@symlink($target, $link);
	  
	  return $br;
  }
  
  static function getIDPath($prefix, $id)
  {
	  $kilo	=	floor($id/1000);
	  
	  return "$prefix$kilo/$id";
  }

  static function FileExists($filename)
  {
    return file_exists($filename) !== FALSE;
  }
}

?>
