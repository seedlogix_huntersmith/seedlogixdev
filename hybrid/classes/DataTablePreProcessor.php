<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 12/23/14
 * Time: 1:29 PM
 */

class DataTablePreProcessor extends QubeDataAccess {

    /**
     * @var VarContainer
     */
    protected $actionVars  =   NULL;
    /**
     * @var HybridBaseController
     */
    protected $controller   =   NULL;

    static function RunIt(HybridBaseController $ctrl, $V = NULL)
    {
        $F  =   static::FromController($ctrl);
        $F->controller  =   $ctrl;
        $F->Execute(VarContainer::getContainer($V));
        return $F;
    }

    function Execute(VarContainer $vars)
    {
        $this->actionVars  =   $vars;
        return $this;
    }

    static function inCampaign(CampaignModel $Campaign, $action, CampaignController $controller)
    {
        $proc   =   self::FromController($controller);
        return $proc->$action($Campaign, $Campaign, $controller);
    }

    function getLeadForms(CampaignModel $Campaign, array $customargs    =   array())
    {
		/** @var LeadsDAO $DA */
        $DA =   LeadsDAO::FromController($this->controller);
        $where  =   $this->actionVars->getValue('where', isset($customargs['where']) ? $customargs['where'] : array()) + array('cid' => $Campaign->getID());
        // detect datatable ajax request
        if($this->actionVars->getValue('sEcho', false))
        {
//            $vars   =   $this->actionVars->getData();
            $args   =   $customargs + DataTableResult::getRequestArgs($this->actionVars);
            $args['where']  =   $where;
            $rowTemplate    =   $this->actionVars->getValue('rowTemplate', 'rows/leadform');
            $rowVar     =   $this->actionVars->getValue('rowVar', 'leadform');
            return DataTableResult::returnDataTableStructure(array($DA, 'getLeadForms'), $args,
                $this->controller->getView($rowTemplate, false, false), $rowVar, 'LeadFormModel');
        }

        $args   =   $customargs + $this->actionVars->getData();
        $args['where']  =   $where;

        $data   =   $DA->getLeadForms($args);

        if($this->actionVars->getValue('calc_found_rows', false))
        {
           return (object)array('objects' => $data, 'count' => $DA->FOUND_ROWS());
        }

        return $data;
    }

    function getProspects(CampaignModel $Campaign = NULL, $touchpoint_ID = NULL)
    {
        $C = $this->actionVars;
		/** @var ProspectDataAccess $PDA */
        $PDA = ProspectDataAccess::FromController($this->controller);
        $C->checkValue('table', $table);
        if ($table == 'ProspectsDashboardCampaign') {
            $rowTemplate = 'rows/Prospect';
            $rowVar = 'prospect';
        } else if ($table == 'ProspectsCampaign') {
            $rowTemplate = 'rows/campaign-prospect';
            $rowVar = 'Prospect';
        }else if ( $table == 'Prospects' )
        {
            $rowTemplate = 'rows/dashboard-prospect';
            $rowVar = 'Prospect';
        }else
            $C->table = 'Prospects';
        $args = DataTableResult::getRequestArgs($C);
        if ($Campaign) {
//            $C->load(array('table' => 'Prospects'));
            $args['cid']    =   $Campaign->getID();
        } elseif($touchpoint_ID) {
            $args['touchpoint_ID']  =   $touchpoint_ID;
        }else{
            $C->table		=		'Prospects';

            $rowTemplate = 'rows/dashboard-prospect';
            $rowVar = 'Prospect';
        }

        if($C->exists('TOUCHPOINT_TYPE'))
        {
            $args['TOUCHPOINT_TYPE']    =   $C->TOUCHPOINT_TYPE;
        }

        if($C->exists('timecontext')){
            $args['timecontext'] = $C->timecontext;
        }

        if($C->exists('form_id')){
            $args['form_id'] = $C->get('form_id', 'intval');
        }

        $dataTableData = DataTableResult::returnDataTableStructure(array($PDA, 'getAccountProspects'), $args,
            $this->controller->getView($rowTemplate, false, false), $rowVar, 'ProspectModel');
        return $dataTableData;
    }

    function getTouchPoints()
    {
        $TPDA   =   TouchPointQuery::FromController($this->controller);
        return $TPDA->getTouchpointsInfo($this->actionVars->getData());
    }

    function getSites(CampaignModel $Campaign, $tp_type)
    {
        $C =    $this->actionVars;
        $C->table = 'Websites';
        $args = DataTableResult::getRequestArgs($C);
        $args['campaign_id'] = $Campaign->getID();;

        if ($C->getValue('forResponder', false)) {
            $rowTemplate    =   'rows/responder_context_option';
            $rowVar =   'context';


            if($tp_type == 'lead_forms')
            {
                $C->sEcho = TRUE;
                $C->table = 'LeadForms';
                $C->rowTemplate =   $rowTemplate;
                $C->rowVar  =   $rowVar;
                return DataTablePreProcessor::RunIt($this->controller, $C)
                    ->getLeadForms($Campaign, array('cols' => 'l.id, l.name',
                        'where' => array('l.trashed = "0000-00-00"')));
            }

            $args['query-mode'] =   'without-stats';
        } else {
            /*$cc_user_id = $this->getUser()->getID();
            $cc_is_reports_only = $this->getUser()->can('reports_only');*/
        
            $args['timecontext'] = new PointsSetBuilder(CampaignController::getChartTimeContext(), $this->getActionUser()->created);
            $rowTemplate = 'rows/website-stats';
            $rowVar = 'site';
        }

        $args['touchpoint_type'] = $tp_type;
		/** @var TouchPointQuery $TouchPointDA */
        $TouchPointDA = TouchPointQuery::FromController($this->controller);
        return DataTableResult::returnDataTableStructure(array($TouchPointDA, 'ProcessQueryRequest'), $args,
            $this->controller->getView($rowTemplate, false, false)
                ->init('touchpointtypes', CampaignController::getTouchPointTypes()), $rowVar, 'stdClass');
    }

    function getReports(CampaignModel $C = NULL)
    {

    }
}