<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SpamWidget
 *
 * @author Amado
 */
class SpamWidget extends DataTableWidget {    
    /**
     *
     * @var UserModel
     */
    protected $User;
		
		protected $user_ID;
            
    function configure(DashboardBaseController $ctrl, array $config)
		{
        $this->User =   $ctrl->getUser();
        $this->user_ID  =   $this->User->getID();
    }
    
    //put your code here
    function initView(ThemeView $V){
        
        $q  =   $this->getQuery();
        
        $ret    =   $q->Exec();
        $contactmsgs    =   $ret->fetchAll();
        $has_messages   =   $ret->rowCount();
        
        
        $V->init('messages', $contactmsgs);
        $V->init('has_messages', $has_messages);
        $V->init('top_msg', $has_messages ? $contactmsgs[0] : null);
    }
    
    function getQuery(){
        static $q   =   NULL;
        
        if(!$q){
			if($this->config){
				extract($this->config);
			}
            $q  =   $this->getController()->getQube()->queryObjects('ContactDataModel')
                ->Where('(spamstatus != 0 AND (T.user_id = %d OR T.parent_id = %d OR %d)) AND T.trashed = 0', 
                        $this->user_ID, $this->user_ID, $this->User->can('manage_spam'))
                ->addFields(sprintf('substr(T.comments,1, %d) as comments, DAY(T.created) as day, substr(MONTHNAME(T.created), 1, 3) as monthname, YEAR(T.created) as year,
                            CHAR_LENGTH(T.comments) > %d as is_summary', $max_text_length, $max_text_length))
                ->orderBy('T.created DESC')
                ->leftJoin(array('HubModel', 'H'), 'H.id = T.type_id AND T.type = "hub"')
                ->addFields('H.name as hub_name, H.httphostkey as hub_domain')
                ->Limit(60)
                 ;
        if(isset($ID))
            $q->Where('T.id = %d', $ID);
        
        if(isset($site))
            $q->Where('T.type = "hub" AND T.type_id = %d', $site);
        
//        if($this->hasPermission('is_support'))
            $q->leftJoin(array('user_info', 'ui'), 'ui.id = T.user_id')->addFields('concat(ui.firstname, " ", ui.lastname) as user_fullname');

            if(isset($this->_config['Campaign_ID'])){
                $q->Where('T.cid = %d ', $this->_config['Campaign_ID']);
            }
        
        }
        
        return $q;
    }
}

?>
