<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require_once HYBRID_PATH . 'models/HubModel.php';
require_once HYBRID_PATH . 'models/HubThemeModel.php';

function get_userdir_rel($user_id){
    return 'users/u' . floor($user_id/1000) . '/' . $user_id;
}

/**
 * Description of HybridRouter
 *
 * @author amado
 */
class HybridRouter {

    /**
     * @var RouterRequest
     */
    protected $Request;

    function setRouterRequest(RouterRequest $R)
    {
        $this->Request  =   $R;
    }

    function getRouterRequest()
    {
        return $this->Request;
    }

    function doCustomRequestProcessing()
    {
        switch($this->Request->getHost())
        {
            /** DETECT BLANK HTTPHOST */
            case '':
                ErrorPages::Forbidden();
                return true;

            /** NOT SURE ABOUT THIS ONE */
            case 'bayville-ny.nassaucountypestcontrol.com':
                ErrorPages::ServiceUnavailable();
            return true;

            /** OR THIS ONE */
            case 'www.homeimprovementnetwork.co':
                header("HTTP/1.1 301 Moved Permanently");
                header('Location: http://homeimprovementnetwork.co' . $this->Request->getRequestURI());
            return true;
        }

        return false;
    }

    /**
     * Returns true on success. false on failure.
     *
     * @param RouterRequest $RouterRequest
     * @return bool
     * @throws Exception
     * @throws QubeException
     */
    function Dispatch(RouterRequest $RouterRequest)
    {
        if($RouterRequest->getSubdomainType()	== RouterRequest::SUBDOM_PREVIEW)
        {
            return $this->DispatchHubPreview();
        }
        if($this->doCustomRequestProcessing())
        {
            return true;
        }
        // hard
        session_start();
        QubeEvent::Trigger('Router.SessionStart', $RouterRequest);
		
		//var_dump($RouterRequest->getRequestURI());
		if($RouterRequest->getRequestURI()){
			$folderArray = explode('/', $RouterRequest->getRequestURI());
			//var_dump(count($folderArray));
			$furl	=	HybridRouter::findTouchPoint($folderArray[1], 0);
		}
        
		if($furl) $tp	=	$furl;
		else $tp	=	HybridRouter::findTouchPoint($RouterRequest->getHost(), 0);
        if(!$tp)
        {
            return false;
//            throw new QubeException('Could not find the touchpoint.');
        }
        if($tp instanceof HubModel)
        {
            if($furl) {
				if(count($folderArray)>2) return $this->DispatchHubResponse($tp, FALSE);
				else return $this->DispatchHubResponse($tp, TRUE);
			}
			else return $this->DispatchHubResponse($tp, FALSE);
        }
        if($tp instanceof BlogModel)
        {
            return $this->DispatchBlogResponse($tp);
        }
        return false;
    }

    function DispatchHubPreview()
    {
        session_set_cookie_params(60*60*24);	// 24 hours session length
        session_start();
        $host	=	$this->Request->getHost();
        $previewparams  =   NULL;
        if(preg_match('#/i/([0-9]+)/([0-9]+)/$#', $this->Request->getRequestPath(), $previewparams)){
            $userid =   (int)$previewparams[1];
            $hubID  =   (int)$previewparams[2];
	$this->Request->setRequestURI('/');
        }elseif(isset($_SESSION['preview_hub'])){
            $hubID	=	$_SESSION['preview_hub'];
        }else{
            Header('HTTP/1.1 403 Forbidden');
//		echo 'Session has expired.';
            ErrorPages::Forbidden();
            exit;
#		throw new Exception('Failed to generate preview. Session has Expired');
        }
        $hub	=	HybridRouter::findHub($host, $hubID);

        if(!$hub) throw new Exception("Failed to find the hub for preview.");

        $_SESSION['preview_hub']	=	$hubID;
        return $this->DispatchHubResponse($hub);
    }

    function findTouchPoint($http_host)
    {
        $TPQ    =   new TouchPointQuery();
        return $TPQ->getTouchPointForDisplay($http_host);
    }
    
    /**
     * returns a dynamic query object with the following placeholders in place and no WHERE condition.
     * :theme ( 0 or a specified theme id to go with the hub)
     * 
     * @return type
     */
    static function getfindHubQuery(){
        // @todo this needs to be OPTIMIZED for unique indexes using $HTTP_HOST right now its a plain index
        return Qube::Start()->queryObjects('HubModel', 'h')
                    ->Fields('ifnull(r.main_site, "6qube.com") as reseller_domain, h.*, h.theme as theme_id, t.path as theme_path,
                        @a := exists( select id  from hub_page2 p2 where p2.hub_id = h.id AND p2.trashed = 0), @ac := (@a AND exists( select id from hub_page2 p2 where p2.hub_id = h.id and p2.trashed = 0 and p2.has_custom_form = 1)), @b := ( @a OR exists( select id from hub_page p1 where p1.hub_id = h.id and p1.trashed = 0)) as haspages, (@ac OR (@b AND !@a AND exists( select id from hub_page p1 where p1.hub_id = h.id and p1.trashed = 0 and p1.has_custom_form = 1))) as hascustomformpages    
                    ')
                    ->leftJoin(array('HubThemeModel', 't'), '(:theme = 0 AND t.id    =   h.theme) OR (t.id = :theme)')
                    ->leftJoin(array('ResellerModel', 'r'), 'r.admin_user = h.user_parent_id AND r.support_email != ""')
                    ->Join(array('UserModel', 'u'), 'u.id = h.user_id AND u.canceled = 0')
                    ->leftJoin(array('feeds', 'f'), 'h.has_feedobjects != 0 AND f.ID = h.has_feedobjects')
                    ->addFields('f.root, f.classname, f.pagename, f.key_match');
    }
    
    /**
     * Finds a hub and its theme info from a given http_host . It should also find any main_site_ids 
     * 
     * @param type $HTTP_HOST
     * @return boolean|HubModel
     */
    static function findHub($HTTP_HOST, $preview    =   0, $theme = 0)
    {
        // not goin to lie, it gets messy calculating hascustomforms and haspages for the hub only because we're doing it with the least possible overhead
        // the alternative is to create hascustomform and haspages columns on the hub table and update those conditionally.
        
        // or create a view.
        // 
        // 
        // 
        $query_params   =   array();
#echo '<!-- preview ', $preview, ' -->';        
        if($preview)
        {
            $stmt  =    self::getfindHubQuery()
			->Where("(h.user_parent_id = (select user_id FROM hub g
                                                        WHERE (httphostkey = :httphost or :httphost = '6qube.com') AND trashed = 0 AND (user_parent_id = 0 OR user_id = user_parent_id) and user_id = h.user_ID LIMIT 1
                                                    ) OR
                                        exists (select private_domain FROM resellers WHERE (h.user_parent_id = 0 AND admin_user = h.user_id) OR (admin_user=h.user_parent_id)))
                                    AND h.id = :hubid")
                    ->Prepare();
            
            $query_params   =   array('hubid' => $preview, 'httphost' => $HTTP_HOST, 'theme' => $theme);
        }else{
            $stmt  =   self::getfindHubQuery()->Where('httphostkey = :httphost AND trashed = 0')->Prepare();
            $query_params   =   array('httphost' => $HTTP_HOST, 'theme' => 0);
        }
        
#        echo $stmt->queryString;
        /* @var $stmt PDOStatement */
        
        if($stmt->execute($query_params) == false)
        {
            return false;
        }
        $found  =   $stmt->rowCount();
        if($found > 1){
		throw new QubeException('Found ' . $found . ' Hubs.');
#            Qube::LogError('Found ' . $found . ' hub(s) for ' . $HTTP_HOST);
            return false;
        }
        
        return $stmt->fetch();
    }
    
    /**
     * 
     * @param type $HTTP_HOST
     * @return boolean|PDOStatement
     */
    static function findBlog($HTTP_HOST)
    {
        $stmt   =   Qube::Start()->queryObjects('BlogModel', 'b')->Where('httphostkey = ? AND trashed = 0')
                    ->Join(array('UserModel', 'u'), 'u.id = b.user_id AND u.canceled = 0')->Prepare()
                    ;
        
        if($stmt->execute(func_get_args())  == false || $stmt->rowCount() != 1)
            return false;
        
        return $stmt;
    }
    
    static function find301($ID, $table, $url){
#return false; 
        static $q   =   null;
        if(!$q)
                $q= Qube::GetPDO()->prepare('SELECT REDIRECT_URL FROM 301_redir WHERE ID = ? and `TABLE`=  ? AND REQUEST_URI=?');
#echo '<!-- url: ', $url, ', ', $ID, ', ', $table, ' -->';        
        if($q->execute(array($ID, $table, $url)) && $q->rowCount() == 1){
            return $q->fetchColumn();
        }
        
        return false;
    }

	static function isResellerAdmin($HTTP_HOST){

		$stmt	=	Qube::Start()->GetDB()->prepare('SELECT 1 FROM resellers WHERE main_site = ? LIMIT 1');
		return $stmt->execute(array($HTTP_HOST)) && $stmt->fetchColumn();

#		return Qube::Start()->query('SELECT main_site FROM resellers WHERE main_site = "' . $HTTP_HOST . '" LIMIT 1')->rowCount();
	}
    
    static function findResellerNetworkService($HTTP_HOST)
    {
        $q   =   Qube::Start()->queryObjects('ResellerNetworkModel', 'n')->Where('n.httphostkey    =   ? AND n.trashed = 0')
                    ->Fields('r.main_site as reseller_main_site, r.main_site_id as reseller_main_site_id, r.private_domain, n.*, t.path as network_theme_path ')
                    ->Join(array('ResellerModel', 'r'), 'r.admin_user = n.admin_user and support_email != ""')
                    ->Join(array('themes_network', 't'), 't.id = n.theme')
                    ->Join(array('UserModel', 'u'), 'u.id = n.admin_user AND u.canceled = 0');
#        echo $q;
        $stmt = $q
                    ->Prepare();
        if($stmt->execute(func_get_args()) == false || $stmt->rowCount()    != 1)
            return false;

        return $stmt;
    }

    /**
     * returns true on success. false on failure
     *
     * @param BlogModel $blog
     * @return bool
     */
    function DispatchBlogResponse(BlogModel $blog)
    {
        $_GET['blog']   =   $blog->getID();
        $request_URI    =   $this->Request->getRequestPath();
        $request   =   trim($request_URI, '/');

        if($redir   =   HybridRouter::find301($blog->getID(), 'blog', $request_URI))
        {
            header('Location: ' . $redir , true, 301);
            return true;
        }else{
            if(!$blog->tracking_id && $blog->httphostkey != '' && !Qube::IS_DEV()){
				$piwik = new PiwikReportsDataAccess();
				$piwik->addBlogToPiwik($blog);
			}
            if(preg_match('/^([0-9]{1,9})-(.*)$/', $request, $match))
            {
#        var_dump($match);
                $_GET['blogID'] =   $match[1];
                $_GET['catTitle'] =   $match[2];
            }elseif(preg_match('/-([0-9]{1,9})\.htm$/', $request, $match))
            {
                $_GET['postID'] =   $_GET['post']	=	$match[1];
            }else{
                $request_arr    =   explode('/', $request);
                switch(count($request_arr))
                {
                    case 1:
                        if($request) $_GET['catTitle']   =   $request;
                        break;
                    default:
                        $_GET['postTitle']  =   array_pop($request_arr);
                        break;
                }
            }

            require_once QUBEROOT . '/blogs/blogs.php';
        }
        return true;
    }
    /**
     * returns true on done. false on failure.
     *
     * @param HubModel $hub
     * @return bool
     */
    function DispatchHubResponse(HubModel $hub, $folderurl){

        if(!$hub->tracking_id && $hub->httphostkey != ''){
            $dataAccess = new PiwikReportsDataAccess();
            $dataAccess->addSite($hub);
        }
        $R = $this->getResponder($hub);

        if($R instanceof HubOutput) {
            $HubRequest	=	new HubURIRequest(Qube::Start(), $this->Request->getRequestPath());
            return $R->printResponse($HubRequest, $folderurl);
        }

        return $R;
    }

    /**
     * @param HubModel $hub
     * @return bool|HubOutput
     */
    function getResponder(HubModel $hub)
    {
        /* @var $hub HubModel */
        require_once HYBRID_PATH . 'classes/HubOutput.php';
        $hr = $this->Request;

        // Redirect based on www_options
        if(!HubOutput::acceptHost($hub, $hr))	return true;

        return	HubOutput::getResponder($hub, new FeedDataAccess());
    }
}

