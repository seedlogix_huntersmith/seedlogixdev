<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of CampaignsStatsRow
 *
 * @author amado
 */
class CampaignStatsRow extends RowRender {
    
    function loadCampaignData(Qube $qube, $campaign_ID, DBWhereExpression $w    =   null)
    {
        $query    =   $qube->queryObjects('CampaignModel')
                    ->Fields('T.id, T.name, DATE_FORMAT(date_created, "%M %D, %Y") as created_str, 
                                (select count(l.id) from active_leads l WHERE l.cid = T.id ' . ($w ? $w->FullClause('AND') : '') .
                            ') as numleads, 
                                count(tr.id) as nb_touchpoints')
                    ->Where('T.user_id = dashboard_userid() AND T.trashed = 0 AND T.id    =   %d', $campaign_ID)
                    ->leftJoin('6q_trackingids tr', 'tr.cid =   T.id')
                    ->groupBy('T.id');
        
        
        $db_data_ret    =   $query->Exec()->fetch(PDO::FETCH_ASSOC);
        if($db_data_ret)
            return $db_data_ret;
        
        return array();
    }
    
    function render($arr) {
        // order of assignment is IMPORTANT!
        $this->renderVisitsChange($arr);
        $this->renderVisits($arr);
        $this->values['numleads'] =   (int)@$arr['numleads'];
        $this->renderConversionRate($arr);
        foreach(array('nb_touchpoints') as $col)
            $this->values[$col]   =   (int)@$arr[$col];
    }
    
}
