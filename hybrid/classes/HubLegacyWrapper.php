<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubLegacyWrapper
 *
 * @author amado
 */
class HubLegacyWrapper {
    //put your code here
    
    protected $_hub =   NULL;
    
    protected $hub  =   NULL;
    function __construct(HubModel $Hub) {
        $this->_hub=    new Hub();
        $this->hub  =   $Hub;
    }
    
    function __call($name, $arguments) {
        return call_user_func_array(array($this->_hub, $name), $arguments);
    }
    
	static function GetPageURLLegacy_NavType($thisNav, $navPath	=	''){
		if($thisNav['page_path']) return $thisNav['page_path'];

		// return legacy url
		return $navPath	.	 $thisNav['page_title_url'] . '/';
	}
    
	//returns all pages for specified hub ID
	function getHubPage($hub_id='', $user_id='', $check_nav='', $limited='', $v2='', $type='', $navParent=0, $noArray=''){
		if(!$v2){ if($_GET['amado']) var_dump(func_get_args());
			$table = 'hub_page';
			$navField = 'nav';
		}
		else {
			$table = 'hub_page2';
			$navField = 'inc_nav';
		}
		//build query
		if($limited){
			if(!$v2) $query = "SELECT T.id, T.page_title, T.page_navname, T.allow_delete, T.nav";
			else $query = "SELECT T.id, T.type, T.nav_parent_id, T.nav_order, 
					ifnull(l.translation, T.page_title) as page_title, T.page_title_url, T.outbound_url,
							T.allow_delete, T.inc_nav, T.default_page, 
                                                        if(isnull(i.path), '', concat('/',i.path,'/')) as page_path, T.outbound_target,
					if(isnull(n.name_url), '', concat(n.name_url, '/')) as nav_url";
			$query .= " FROM ".$table . ' T ';
			if($v2) $query .= 'left join 6q_hubpages_tbl p on (p.oldtbl_ID = T.id) left join 6q_pathindex i on'
                                . '  (i.page_ID = p.ID AND p.hub_ID = i.hub_ID and i.is_current = 1)'
				. ' left join 6q_navs_tbl n on (n.oldtbl_ID = T.id)'
				. ' left join 6q_contenttranslations_tbl l on (l.tbl = T.type AND l.ID = T.id AND l.language = "' . $this->hub->language . '" AND l.key = "page_title")';
		}
		else $query = "SELECT * FROM ".$table . ' T';

		if($hub_id) $query .= " WHERE T.hub_id = '".$hub_id."' ";
		else if($user_id) $query .= " WHERE T.hub_id IN (SELECT id FROM hub WHERE user_id = '".$user_id."') ";
		if($check_nav && !$v2) $query .= " AND T.".$navField." = 1 ";
		if($v2 && $type) $query .= " AND T.type = '".$type."' ";
		if($v2) $query .= " AND T.nav_parent_id = '".$navParent."' ";
		$query .= " AND T.trashed = '0000-00-00' ";
		//$query .= " AND `".$table."`.hub_id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub' AND table_id = ".$table.".hub_id) ";
	if($_GET['amado'])			echo $query;
		if($v2){
			if($type) $query .= " ORDER BY nav_order ASC, id ASC";
			else $query .= " ORDER BY nav_order ASC, id DESC ";
		}
		else $query .= " ORDER BY created ASC, id ASC";
		
		if(@$_GET['qdebug'] == md5('amado'))
		{
		  echo 'Query: getHubPage ', $query, '<br />';
		}
		
		//query for results
		$result = $this->query($query);
		//set number of rows
		if($result){
			$this->page_rows += $this->numRows($result);
			//if both navs and pages are being pulled, return an array
			if($v2 && !$type){
				$return = array();
				while($row = $this->fetchArray($result, 1)){
					if($row['type']=='nav'){
						if($noArray) $return[] = $row;
						else {
							//if type is a nav, get sub-pages (this is iterative)
							$navPages = $this->getHubPage($hub_id, $user_id, $check_nav, $limited, 1, NULL, $row['id']);
							$return2 = $navPages;
							array_unshift($return2, $row);
							$return[] = $return2;
						}
					}
					else $return[] = $row;
				}
			}
			else $return = $result;
		}
		else $return = false;
		//return query results
		return $return;
	}

    //Pages V2: output the nav/pages onto the site
    function dispV2Nav5($thisNav, $parentTitle='', $active='', $customWraps='', $iterate='', $footer='', $urlBase=''){
        
    }
    
    function __set($name, $value) {
        $this->_hub->$name  =   $value;
    }
    
    function __get($name) {
        return isset($this->_hub->$name) ? $this->_hub->$name : '';
    }
}
