<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportableTable
 *
 * @author sofus
 */
interface ExportableTable {
	function GetExportableColumnTitles();
	function Export($fhandle, array $args);
}
