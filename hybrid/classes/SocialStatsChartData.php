<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of SocialStatsChartData
 *
 * @author amado
 */
class SocialStatsChartData extends AutoLoadChartData
{
    /**
     *
     * @var PointsSetBuilder
     */
    protected $dateMagic    =   NULL;
    /**
     *
     * @var VarContainer
     */
    protected $params   =   NULL;
    function init(&$newconfig){
        parent::init($newconfig);
        $this->config   =   $newconfig + $this->config;
        $this->params   = new VarContainer($this->config);
        $this->dateMagic    =   new PointsSetBuilder($this->params);
        
    }
    
    function getQuery(){
        $q  =   new DBSelectQuery(Qube::GetPDO());
        if($this->params->period == "MONTH" || $this->params->period == "ALL") {
            $monthly = ' AND (datetime = LAST_DAY(datetime) OR datetime = CURDATE())';
        }
        return $q->From('6q_socialstats_tbl T')
                ->Fields('ROUND(unix_timestamp(if(datediff(CURDATE(), :start) > 31, DATE_FORMAT(T.datetime, "%Y-%m-01"), DATE(T.datetime)))) as date1, SUM(T.value)')
                ->groupBy('IF( datediff( CURDATE(), :start )  < 32, DATE( T.datetime ) , DATE_FORMAT( T.datetime,  "%Y-%m" ) )')
                ->Where( 'datetime >= :start AND name = :name AND (T.cid = :cid OR :cid = 0) AND user_ID = :user_ID' . $monthly)
                ->Prepare();
    }
    
    function fetchDataSetLikes($argsn = array()){
        $stmt   =   $this->getQuery();

        $args   =   array('start' =>$this->dateMagic->getStartDate(), 'name' => 'likes') + $argsn;
        $stmt->execute($args);//, 'end' => $this->dateMagic->getEndDate()));
        
        return $this->dateMagic->createDataSet($stmt);
    }
    
    function fetchDataSetFollowers($argsn = array()){
        $stmt   =   $this->getQuery();
        $args   =   array('start' =>$this->dateMagic->getStartDate(), 'name' => 'followers') + $argsn;
        
        $stmt->execute($args);//, 'end' => $this->dateMagic->getEndDate()));
        
        return $this->dateMagic->createDataSet($stmt);
    }
    
    function fetchLikesPoints(array $args){
        return array('label' => 'Facebook Likes', 'data' => $this->fetchDataSetLikes($args));
    }
    
    function fetchFollowersPoints(array $args){
        return array('label' => 'Twitter Followers', 'data' => $this->fetchDataSetFollowers($args));
    }
    
    function fetchPoints($args = array()){
        return array($this->fetchLikesPoints($args), $this->fetchFollowersPoints($args));
    }
        
    function loadData($refresh) {
        $args   =   array();
        $CID = 0;
        if($this->params->checkValue('CID', $CID))
        {
//            Qube::GetPDO()->query('SET @dashboard_campaignid = ' . (int)$CID);
        }
        $args['cid']    =   (int)$CID;
        $args['user_ID']    =   QubeDataAccess::FromController($this->controller)->getActionUser()->getID();
        $this->data =   $this->fetchPoints($args);
        return;
        
        static $cachedtable =   array();
        
        $vc =   new VarContainer($this->config);
        
        $timekey    =   $vc->period . $vc->interval;
        
        if($vc->checkValue('cID', $cID)){            
            $this->data =   array('camp');
            return;
        }
                
        // return the table!
        $this->data =   $this->getSocialStats($vc);
    }
}