<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of TranslationApi
 *
 * @author amado
 */
class TranslationApi {
    //put your code here
    
    static function GetTranslation($text, $langtarget){        
    //	$langtarget	=	$this->hub->language;
        $url = 'https://www.googleapis.com/language/translate/v2';

        $values = array(
#        'key'    => 'AIzaSyAiKhig4PlkjWnBzIe05Ly3dYnezYUdais',
	'key' => 'AIzaSyAidjw09F2aQNdOWC50dC-DghVv8tf_4SU',
        'target' => $langtarget,
        'q'      => $text
        );

            $formData = http_build_query($values);

        $handle = curl_init($url);
//        curl_setopt($handle, CURLOPT_VERBOSE, 1);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $formData);
            curl_setopt($handle, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
        $response = curl_exec($handle);
        $responseDecoded = json_decode($response, true);
        curl_close($handle);

deb("Translating $language ($text)");
deb($response);

        if($responseDecoded['error']){
return '';

}
        return $responseDecoded['data']['translations'][0]['translatedText'];
    }
}
