<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NeedsController
 *
 * @author amado
 */
abstract class NeedsController {
	//put your code here
	/**
	 *
	 * @var AdminBaseController
	 */
	protected $_ctrl	=	NULL;
	protected $_config	=	array();
	
	function configure(DashboardBaseController $ctrl, array $config)	{	}
	
	function __construct(DashboardBaseController $ctrl, $config	=	array())
	{
		$this->_ctrl	=	$ctrl;
		$this->_config	=	$config;
		$this->configure($ctrl, $config);
	}
	
	
	function getController()
	{
		return $this->_ctrl;
	}
	
}
