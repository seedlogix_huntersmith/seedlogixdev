<?php
/*
 * Author:    Jon Aguilar
 * License:    
 */

/**
 * Description of TrashManager
 *
 * @author amado
 */
class TrashManager extends QubeDependant
{
    /**
     * @var SystemDataAccess
     */

    private $da;

    static function getConfig(){
        $str_before     = 'The ';
        $str_span1      = ' <span>{$obj->name}</span> ';
        $str_span2      = ' <span>{$obj->label}</span> ';
        $str_span3      = ' <span>{$obj->blog_title}</span> ';
        $str_span4      = ' <span>{$obj->hostname}</span> ';
        $str_span5      = ' <span>{$obj->keyword}</span> ';
        $str_span6      = ' <span>{$obj->post_title}</span> ';
        $str_span7      = ' <span>{$obj->page_title}</span> ';
        $str_span8      = ' <span>{$obj->subject}</span> ';
        $str_span9      = ' <span>{$obj->first_name} {$obj->last_name}</span> ';
        $str_span10     = ' <span>{$obj->title}</span> ';
        $str_after1     = 'has been deleted.';
        $str_after2     = 'was not found.';

        $config         = array(
                        'campaign_ID'       => (object)array(
                                            'message'       => $str_before.'Campaign'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'CampaignModel',
                                            'error'         => $str_before.'Campaign'.$str_span1.$str_after2
                                            ),
                        'WEBSITE_ID'        => (object)array(
                                            'message'       => $str_before.'Website'.$str_span1.$str_after1,
                                            'columns'       => 'name,id,touchpoint_type,cid',
                                            'model'         => 'WebsiteModel',
                                            'error'         => $str_before.'Website'.$str_span1.$str_after2
                                            ),
                        'MOBILE_ID'         => (object)array(
                                            'message'       => $str_before.'Mobile Page'.$str_span1.$str_after1,
                                            'columns'       => 'name,id,touchpoint_type,cid',
                                            'model'         => 'WebsiteModel',
                                            'error'         => $str_before.'Mobile Page'.$str_span1.$str_after2
                                            ),
                        'SOCIAL_ID'         => (object)array(
                                            'message'       => $str_before.'Social Page'.$str_span1.$str_after1,
                                            'columns'       => 'name,id,touchpoint_type,cid',
                                            'model'         => 'WebsiteModel',
                                            'error'         => $str_before.'Social Page'.$str_span1.$str_after2
                                            ),
                        'LANDING_ID'        => (object)array(
                                            'message'       => $str_before.'Landing Page'.$str_span1.$str_after1,
                                            'columns'       => 'name,id,touchpoint_type,cid',
                                            'model'         => 'WebsiteModel',
                                            'error'         => $str_before.'Landing Page'.$str_span1.$str_after2
                                            ),
                        'PROFILE_ID'        => (object)array(
                                            'message'       => $str_before.'Profile'.$str_span1.$str_after1,
                                            'columns'       => 'name,id,touchpoint_type,cid',
                                            'model'         => 'WebsiteModel',
                                            'error'         => $str_before.'Profile'.$str_span1.$str_after2
                                            ),
                        'leadform_ID'       => (object)array(
                                            'message'       => $str_before.'Form'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'LeadFormModel',
                                            'error'         => $str_before.'Form'.$str_span1.$str_after2
                                            ),
                        'field_ID'          => (object)array(
                                            'message'       => $str_before.'Form Field'.$str_span2.$str_after1,
                                            'columns'       => 'label,ID',
                                            'model'         => 'LeadFormFieldModel',
                                            'method'        => 'Delete',
                                            'error'         => $str_before.'Form Field'.$str_span2.$str_after2
                                            ),
                        'article_ID'        => (object)array(
                                            'message'       => $str_before.'Article'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'NetworkArticleModel',
                                            'error'         => $str_before.'Article'.$str_span1.$str_after2
                                            ),
                        'dirarticle_ID'     => (object)array(
                                            'message'       => $str_before.'Article'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'DirectoryArticleModel',
                                            'error'         => $str_before.'Article'.$str_span1.$str_after2
                                            ),
                        'pressarticle_ID'   => (object)array(
                                            'message'       => $str_before.'Article'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'PressArticleModel',
                                            'error'         => $str_before.'Article'.$str_span1.$str_after2
                                            ),
                        'autoresponder_ID'  => (object)array(
                                            'message'       => $str_before.'Auto Responder'.$str_span1.$str_after1,
                                            'columns'       => 'name,id',
                                            'model'         => 'ResponderModel',
                                            'error'         => $str_before.'Auto Responder'.$str_span1.$str_after2
                                            ),
                        'BLOG_ID'           => (object)array(
                                            'message'       => $str_before.'Blog'.$str_span3.$str_after1,
                                            'columns'       => 'blog_title,id,cid',
                                            'model'         => 'BlogModel',
                                            'error'         => $str_before.'Blog'.$str_span3.$str_after2
                                            ),
                        'rankingSite_ID'    => (object)array(
                                            'message'       => $str_before.'Keyword Campaign'.$str_span4.$str_after1,
                                            'columns'       => 'hostname,ID',
                                            'model'         => 'RankingSiteModel',
                                            'error'         => $str_before.'Keyword Campaign'.$str_span4.$str_after2,
                                            'method'        => 'Trash'
                                            ),
                        'keyword_ID'        => (object)array(
                                            'message'       => $str_before.'Keyword'.$str_span5.$str_after1,
                                            'columns'       => 'keyword,ID,site_ID',
                                            'where'         => 'id = :keyword_ID AND site_ID = :site_ID',
                                            'model'         => 'RankingKeywordModel',
                                            'error'         => $str_before.'Keyword'.$str_span5.$str_after2,
                                            'method'        => 'Delete',
                                            'whereValues'   => array(
                                                            'keyword_ID'    => 'keyword_ID',
                                                            'site_ID'       => 'rankingSite_id'
                                                            )
                                            ),
                        'report_ID'         => (object)array(
                                            'message'       => $str_before.'Report'.$str_span1.$str_after1,
                                            'columns'       => 'name,ID',
                                            'model'         => 'ReportModel',
                                            'error'         => $str_before.'Report'.$str_span1.$str_after2,
                                            'method'        => 'Delete'
                                            ),
                        'post_ID'           => (object)array(
                                            'message'       => $str_before.'Blog Post'.$str_span6.$str_after1,
                                            'columns'       => 'post_title,id,master_ID,user_id',
                                            'model'         => 'BlogPostModel',
                                            'error'         => $str_before.'Blog Post'.$str_span6.$str_after2
                                            ),
                        'NID'               => (object)array(
                                            'message'       => $str_before.'Nav'.$str_span7.$str_after1,
                                            'columns'       => 'page_title,id,hub_id,page_parent_id,nav_parent_id',
                                            'model'         => 'NavModel',
                                            'error'         => $str_before.'Nav'.$str_span7.$str_after2
                                            ),
                        'PID'               => (object)array(
                                            'message'       => $str_before.'Page'.$str_span7.$str_after1,
                                            'columns'       => 'page_title,id,hub_id,page_parent_id,nav_parent_id',
                                            'model'         => 'PageModel',
                                            'error'         => $str_before.'Page'.$str_span7.$str_after2
                                            ),
                        'prospect_ID'       => (object)array(
                                            'message'       => $str_before.'Prospect'.$str_span1.$str_after1,
                                            'columns'       => 'ID,name,user_ID,contactform_ID,lead_ID',
                                            'model'         => 'ProspectModel',
                                            'error'         => $str_before.'Prospect'.$str_span1.$str_after2
                                            ),
                        'broadcast_ID'      => (object)array(
                                            'message'       => $str_before.'Email Broadcast'.$str_span1.$str_after1,
                                            'columns'       => 'id,name,user_id',
                                            'model'         => 'LeadFormEmailerModel',
                                            'error'         => $str_before.'Email Broadcast'.$str_span1.$str_after2
                                            ),
                        'note_ID'           => (object)array(
                                            'message'       => $str_before.'Note'.$str_span8.$str_after1,
                                            'columns'       => 'id,subject,user_id',
                                            'model'         => 'NoteModel',
                                            'error'         => $str_before.'Note'.$str_span8.$str_after2
                                            ),
						'Account_ID'        => (object)array(
                                            'message'       => $str_before.'Account'.$str_span9.$str_after1,
                                            'columns'       => 'id,user_id,name',
                                            'model'         => 'AccountsModel',
                                            'error'         => $str_before.'Account'.$str_span9.$str_after2
                                            ),
						'Proposal_ID'        => (object)array(
                                            'message'       => $str_before.'Proposal'.$str_span9.$str_after1,
                                            'columns'       => 'id,user_id,name',
                                            'model'         => 'ProposalModel',
                                            'error'         => $str_before.'Proposal'.$str_span9.$str_after2
                                            ),
						'Product_ID'        => (object)array(
                                            'message'       => $str_before.'Product'.$str_span9.$str_after1,
                                            'columns'       => 'id,user_id,name',
                                            'model'         => 'ProductsModel',
                                            'error'         => $str_before.'Product'.$str_span9.$str_after2
                                            ),
						'Task_ID'        => (object)array(
                                            'message'       => $str_before.'Task'.$str_span9.$str_after1,
                                            'columns'       => 'id,user_id,title',
                                            'model'         => 'TasksModel',
                                            'error'         => $str_before.'Task'.$str_span9.$str_after2
                                            ),
                        'contact_ID'        => (object)array(
                                            'message'       => $str_before.'Contact'.$str_span9.$str_after1,
                                            'columns'       => 'id,user_id,first_name,last_name',
                                            'model'         => 'ContactModel',
                                            'error'         => $str_before.'Contact'.$str_span9.$str_after2
                                            ),
                        'SocialAccount_ID'  => (object)array(
                                            'message'       => $str_before.'Social Account'.$str_span1.$str_after1,
                                            'columns'       => 'id,user_id,name',
                                            'model'         => 'SocialAccountsModel',
                                            'error'         => $str_before.'Social Account'.$str_span1.$str_after2
                                            ),
                        'SocialPost_ID'     => (object)array(
                                            'message'       => $str_before.'Social Post'.$str_span1.$str_after1,
                                            'columns'       => 'id,user_id,name',
                                            'model'         => 'SocialPostsModel',
                                            'error'         => $str_before.'Social Post'.$str_span1.$str_after2
                                            ),
                        'role_ID'           => (object)array(
                                            'message'       => $str_before.'Role'.$str_span1.$str_after1,
                                            'columns'       => 'id,name',
                                            'model'         => 'QubeRoleModel'
                                            ),
                        'collab_ID'         => (object)array(
                                            'message'       => $str_before.'Notification'.$str_span1.$str_after1,
                                            'columns'       => 'id,name,campaign_id',
                                            'model'         => 'CampaignCollab'
                                            ),
                        'slide_ID'          => (object)array(
                                            'message'       => $str_before.'Slide'.$str_span10.$str_after1,
                                            'columns'       => 'ID,title,hub_id,internalname',
                                            'model'         => 'SlideModel'
                                            )
                        );

        return $config;
    }

    function __construct(SystemDataAccess $dataAccess, Qube $Q = NULL)
    {
        parent::__construct($Q);
        $this->da = $dataAccess;
    }


//    function getObject($objinfo, $obj_ID)
//    {
//        $obj =   $this->getQube()->queryObjects($objinfo->model, 'T', 'user_id = dashboard_userid() and id = %d', $obj_ID)
//                    ->Fields($objinfo->columns)->Exec()->fetch();
//        return $obj;
//    }
    
    function TrashByParam(VarContainer $params, &$message, $trash = true)
    {
        include_once(QUBEADMIN . '/models/CampaignCollab.php');
        $result =   false;
        $paramkeys =   static::getConfig();
        
        
        foreach($paramkeys as $keyparam => $objinfo){
            if(!$params->checkValue($keyparam,  $obj_ID)) continue;  // not set, continue
            $user = $this->da->getActionUser();

            if(!property_exists($objinfo, 'where'))
                $where = 'id = :id';
            else
                $where = $objinfo->where;

            $whereValues = array();
            if(!property_exists($objinfo, 'whereValues')){
                $whereValues['id'] = $obj_ID;
            }else{
                foreach($objinfo->whereValues as $placeholder => $value){
                    $whereValues[$placeholder] = $params->get($value);
                }
            }

            $obj    =   $this->da->getObject($objinfo->model, $objinfo->columns, $where, $whereValues);
            
            if(!$obj){
                $message    =   $objinfo->error;
                return false;
            }

            $can_trash = false;
            $model = get_class($obj);

            switch($model){
				case 'CampaignCollab':
					$can_trash = $user->can('edit_campaigns', $obj->campaign_id);
					break;
                case 'CampaignModel':
                    $can_trash = $user->can('trash_campaigns', $obj_ID);
                    break;
                case 'ContactModel':
                    $can_trash = $user->can('edit_contact', $obj_ID);
                    break;
				case 'AccountsModel':
                    //$can_trash = $user->can('edit_accounts', $obj_ID);
					$can_trash = true;
                    break;
				case 'ProductsModel':
                    //$can_trash = $user->can('edit_accounts', $obj_ID);
					$can_trash = true;
                    break;
				case 'ProposalModel':
                    //$can_trash = $user->can('edit_accounts', $obj_ID);
					$can_trash = true;
                    break;
				case 'TasksModel':
                    //$can_trash = $user->can('edit_accounts', $obj_ID);
					$can_trash = true;
                    break;
				 case 'SocialAccountsModel':
                    $can_trash = $user->can('edit_socialaccounts', $obj_ID);
                    break;
				 case 'SocialPostsModel':
                    $can_trash = $user->can('edit_socialposts', $obj_ID);
                    break;
                case 'BlogModel':
                    $can_trash = $user->can('edit_BLOG', $obj_ID);
                    break;
                case 'WebsiteModel':
                    $can_trash = $user->can('edit_' . $obj->touchpoint_type, $obj_ID);
                    break;
                case 'RankingSiteModel':
                    $can_trash = $user->can('edit_ranking', $obj_ID);
                    break;
                case 'RankingKeywordModel':
                    $can_trash = $user->can('edit_keyword', $obj_ID);
                    break;
                case 'ReportModel':
                    $can_trash = $user->can('edit_report', $obj_ID);
                    break;
                case 'BlogPostModel':
                    $can_trash = $user->can('edit_blogpost', $obj_ID);
                    break;
                case 'PageModel':
                case 'NavModel':
                    $touchpointType = $params->getValue('touchpointType');
                    $can_trash = $user->can("edit_{$touchpointType}", $obj->hub_id);
					if($can_trash){
						$WDA = new WebsiteDataAccess($this->getQube());
						$WDA->setActionUser($user);
						if($obj->page_parent_id) {   // Check if a slave page is being deleted
							if ($obj->nav_parent_id) { //Check if has a nav associated (Nav parents has it's own allow_pages)
								$can_trash = $WDA->getPageParentLocks($obj->nav_parent_id)->isLocked('allow_pages');
							} else {  // If page is on root level, check allow_pages of hub
								$can_trash = HubDataAccess::FromDataAccess($WDA)->getWebsiteLocks($obj->hub_id)->isLocked('allow_pages');
							}
						}
					}
                    break;
                case 'LeadFormModel':
                    $can_trash = $user->can("edit_leadform", $obj_ID);
                    break;
                case 'LeadFormFieldModel':
                    $can_trash  =   $user->can('edit_leadform', $obj->form_ID);
                    break;
                case 'ProspectModel':
                    //$can_trash = $user->can('edit_prospect', $obj_ID);
					$can_trash = true;
                    break;
                case 'ResponderModel':
                    $can_trash = $user->can('edit_responder', $obj_ID);
                    break;
                case 'LeadFormEmailerModel':
                    $can_trash = $user->can('edit_broadcast', $obj_ID);
                    break;
                case 'NoteModel':
                    //$can_trash = $user->can('edit_note', $obj_ID);
                    // Edit note is not being added to the permissions_cache; will have to do more in depth research to make this proper. For now $can_trash will
                    // be set to true or 1 to allow all users to delete notes they create. 
                    $can_trash = 1;
                    break;
				case 'SlideModel':
					$can_trash = $user->can('edit_slide', $obj_ID);
            }


            if($can_trash){
                if($trash){
                    $method = isset($objinfo->method) ? $objinfo->method : 'Trash';
                    $result =   $obj->$method($this->getQube()) == 1;
                }
                else
                    $result =   $obj->UnTrash($this->getQube()) == 1;
                $message    =   eval('return "' . $objinfo->message . '";');    #'Campaign: ' . $name->fetchColumn() . ' has been moved to the trash bin.';
            }else{
                $message = "Action denied";
                $result = false;
            }


        }
        return $result;
    }
}