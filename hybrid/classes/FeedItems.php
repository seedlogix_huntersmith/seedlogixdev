<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FeedItems
 *
 * @author Amado
 */

interface FeedItems
{
	function hasItem();
	function getItem($key, FeedObject &$item);
}

