<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HtmlNotification
 *
 * @author amado
 */
class SpamNotification extends EmailNotification {
    
    /**
     *
     * @var ContactDataModel
     */
    protected $form;
    
    function __construct(ContactDataModel $f) {
        $this->form =   $f;
    }
    
    function getContent() {
        
        $message =   <<<EOF
                <html><body><p>
                The following message has been reported by our anti-spam system.
                Please review the following information below. The message will be sent to the intended recipient only after you approve it.
                </p>
EOF;
        $message   .=  $this->form->Notify()->getInnerContent();
        $url_dashbrd    =   'http://hybrid.6qube.com/admin.php?action=spam&ctrl=admin';
        $url_approve    =   'http://hybrid.6qube.com/admin.php?action=spam&ctrl=admin&APPROVE=' . $this->form->id;
        $url_reject     =   'http://hybrid.6qube.com/admin.php?action=spam&ctrl=admin&DELETE=' . $this->form->id;
        
        $message .= <<<EOF
                <br /><br />
Approve This Message:<br />
<a href="$url_approve">$url_approve</a><br /><br />
                
Delete This Message:<br />
<a href="$url_reject">$url_reject</a><br /><br />
                
Spam Dashboard:<br />
<a href="$url_dashbrd">$url_dashbrd</a><br /><br />
                </body></html>
EOF;

	return $message;
        
    }
    
    function getSubject() {
        return "A New Spam Message has been reported.";
    }
}
