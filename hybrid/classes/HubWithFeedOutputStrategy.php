<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Instantiates a HubOutput and initializes a feednav
 *
 * @author amado
 */
class HubWithFeedOutputStrategy extends HubOutputStrategy
{
	function createResponder() {
		$hub	=	$this->hub;
			// load helper class
			require_once HYBRID_PATH . 'classes/FeedObject.php';
			require_once HYBRID_PATH . 'classes/custom/' . $hub->classname . '.php';
			require_once HYBRID_PATH . 'classes/FeedNav.php';
			
			$HubOut	= new HubFeedOutput(Qube::Start());
			$feednav    =   new FeedNav($hub->has_feedobjects, $hub->classname, $hub->root);
			$feednav->setDA($this->getDA());
			$HubOut->setVar('feednav', $feednav);
			return $HubOut;
	}
}
