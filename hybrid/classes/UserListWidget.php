<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserListWidget
 *
 * @author Amado
 */
class UserListWidget extends DataTableWidget {

	function doAction($widget_action) {
		switch ($widget_action) {
			case 'UserList':
				return $this->ajaxUserList(VarContainer::getContainer($this->_config), $_GET['status']);
		}
	}

	function ajaxUserList(VarContainer $C) {
		extract($this->_config);

		$columnsDB = DataTableResult::getTableColumns('Userlist');
		$columns =  explode(',', $C->get('sColumns'));
		
		$qparams = array();
		$qparamswhere = array();
//		$Q = UserListWidget::queryUsers($this->getController(), $status)->calc_found_rows()
//						->Fields('T.id,if(isnull(i.firstname), '
//						. '"Unknown", concat(i.firstname, " ", i.lastname)) as fullname'
//						. ', T.username, T.created, T.upgraded, T.last_login');

//        $iTotal =   $this->queryUsers($status)->Fields('count(*)')->Exec()->fetchColumn();


                
            $Q = self::queryUsers($this->getController(), $status, $roles, $C->role != '' ? intval($C->role) : null)
						->addFields('concat(i.firstname, " ", i.lastname) as fullname')->calc_found_rows()
						->Limit(10);

		if ($C->checkValue('sSearch', $search) && $search != '') {
			if(is_numeric($search))
			{
				$Q->Where('(T.id = :search AND T.parent_id = %d) OR (%d = 1 AND T.parent_id = :search)', $this->getController()->getLoggedInUserID(), $this->getController()->getUser()->isSystem());
			}else
			$Q->Where('(T.username LIKE CONCAT("%", :search, "%") OR (i.firstname != "" AND i.lastname != "" AND
                    (CONCAT(i.firstname, " ", i.lastname) LIKE  CONCAT("%", REPLACE(:search, " ", "%"), "%") OR i.firstname LIKE CONCAT("%", :search, "%") OR i.lastname LIKE CONCAT("%", :search, "%%"))
                    ))');
			$qparamswhere['search'] = $search;
		}

		if ($C->checkValue('iSortCol_0', $colIndex) && isset($columnsDB[$columns[$colIndex]]['Name'])) {
			$colname = $columnsDB[$columns[$colIndex]]['Name'];
			$newRow = $C->checkValue('newRowsIds', $new_rows_ids) ? 'T.id = ' . join(' DESC, T.id = ', array_map('intval', $new_rows_ids)) . ' DESC, ' : '';
			$Q->orderBy($newRow . $colname . ' ' . ($C->sSortDir_0 == 'asc' ? 'ASC' : 'DESC'));
		}


		$actionurl = $this->getController()->ActionUrl('UserManager_profile?ID=');

		/*
		 * Output
		 */
		return $this->ajaxUserDataTableSet($C, $Q, $qparams, $qparamswhere);

//        return $this->getAjaxView($output);
	}

    /**
     * @param DashboardBaseController $ctrl
     * @param string $status
     * @param null $roles
     * @param int $role_filter Role ID, passed to filter users.
     * @return mixed
     * @throws QubeException
     */
	static function queryUsers(DashboardBaseController $ctrl, $status = '', $roles = NULL, $role_filter = NULL ) {
        $q = $ctrl->getQube()->queryObjects('UserModel')
            ->addFields('(' . (int)$ctrl->getLoggedInUserID() . ' = T.id) as is_current_user')
            ->Where(
                '( %d = 1 OR (T.parent_id = %d AND %d = 1) OR %d = T.id)',
                $ctrl->getUser()->isSystem(),
                $ctrl->getUser()->getParentID(),
                $ctrl->getUser()->can('view_users'),
                $ctrl->getLoggedInUserID() // i am reseller
            )
            ->leftJoin(array('user_info', 'i'), 'i.user_id = T.id')
            ->addFields('i.*, T.parent_id, if(isnull(i.firstname), "Unknown", concat(i.firstname, " ", i.lastname)) as fullname,
						    T.id, i.profile_photo');
            //->orderBy('T.id'); // need to have T.id after i.* because if user_info.id is null then

        $checkstatus = array(
            'active' => 'access = 1 and canceled = 0',
            'suspended' => 'access = 0 and canceled = 0',
            'canceled' => 'canceled = 1'
        );

		if ($status && isset($checkstatus[$status]))
			$q->Where($checkstatus[$status]);

        if (is_int($role_filter))
            $q->Where('r.role_ID = ' . strval($role_filter));

		if ($roles) {
			$roles = is_array($roles) ? $roles : array($roles);
			$roleids = array_map('intval', $roles);
			$q->Join('6q_userroles r', 'r.user_ID = T.id AND r.role_ID IN (%s)', implode(',', $roleids));

		}
        $q->leftJoin('6q_userroles r', 'r.user_ID = T.id');
        $q->leftJoin('6q_roles r2', 'r.role_ID = r2.id');
        $q->addFields("r.role_ID as role_id");
        $q->addFields("r2.role as role");

        $q->orderBy('T.id DESC');

		return $q;
	}

	function initView(ThemeView $V) {
		extract($this->_config);

		$Q = self::queryUsers($this->getController(), $serverParams['status'], $roles)
						->addFields('concat(i.firstname, " ", i.lastname) as fullname')->calc_found_rows()
						->Limit(10);

		$Users = $Q
						->Exec();

		$total_users = $this->getController()->query('SELECT FOUND_ROWS()', FALSE)->fetchColumn();
		return array(
				'userlist' => $Users->fetchAll(),
				'total_users' => $total_users,
				'serverParams' => $serverParams
		);
//        $V->init('menu', 'users');
	}

	function ajaxUserDataTableSet(VarContainer $C, DBSelectQuery $Q, $qparams = array(), $paramswhere	=	array()){

		$totalQ =   clone $Q;

		if($C->checkValue('iDisplayStart', $start)){
			$length =   min(100, $C->iDisplayLength);
			$Q->Limit($start, $length);
		}

		$qiTotalRec      =   $totalQ->calc_found_rows(false)->
		Fields('count(*)')->Fields('count(*)')->Prepare();

		if($paramswhere)
			$qiTotalRec->execute($paramswhere);
		else
			$qiTotalRec->execute();
		$iTotalRec	=	$qiTotalRec->fetchColumn();

		$result =   $Q->Prepare();
		$result->execute($qparams+$paramswhere);

		$out    =   array(
			"sEcho" => intval($C->sEcho),
			"iTotalRecords" => $iTotalRec,
			"iTotalDisplayRecords" => $this->getController()->queryColumn('SELECT FOUND_ROWS()'),
			"aaData" => $result->fetchAll()
		);

		$controller = $this->getController();

		$rowTemplate = $controller->getView('rows/user', false, false);
		$out = $this->parser($out, $rowTemplate);


		return $this->getController()->getAjaxView($out);
	}

}
