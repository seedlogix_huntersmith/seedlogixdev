<?php

require_once QUBEADMIN . 'inc/cpanel.xmlapi.php';        

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IgnitedServerApi
 *
 * @author Amado
 */
abstract class IgnitedServerApi  extends xmlapi
{   
    protected $router_path  =   'production-code/dev-trunk/hybrid';
    
    function api2_query($user, $module, $function, $args = array()) {
        return parent::api2_query('sapphire', $module, $function, $args);
    }
    
    function getRouterPath()
    {
        return $this->router_path;
    }    
}
