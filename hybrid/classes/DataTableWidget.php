<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of DataTableWidget
 *
 * @author amado
 */
abstract class DataTableWidget extends NeedsController {
	
	protected $_view	=	NULL;
	protected $_vars	=	array();
	
	function __isset($name) {
		return isset($this->_vars[$name]);
	}
	
	function __get($var)
	{
		return $this->_vars[$var];
	}
	
	function appendToView(ThemeView $V, $widgetname)
	{
		$widgets	=	$V->getTemplate()->widgets;
		if(!$widgets)	$widgets	=	array();
		$widgets[$widgetname]	=	$this;
		
		$V->init('widgets', $widgets);
		$this->_template	=	$V->getTemplate();
		$this->_vars	=	$this->initView($V);
	}
	
	function display($widget_template)
	{
		$this->_template->getWidget($widget_template,
						array('w' => $this));
	}
	
	static function findWidget($W)
	{
		return !empty($W) && class_exists($W . 'Widget', TRUE);
	}
	
	function doAction($widget_action)	{	}
	
	abstract function initView(ThemeView $V);

	function ajaxDataTableSet(VarContainer $C, DBSelectQuery $Q, $qparams = array(), $paramswhere	=	array()){

		$totalQ =   clone $Q;

		if($C->checkValue('iDisplayStart', $start)){
			$length =   min(100, $C->iDisplayLength);
			$Q->Limit($start, $length);
		}

		$qiTotalRec      =   $totalQ->calc_found_rows(false)->
		Fields('count(*)')->Fields('count(*)')->Prepare();

		if($paramswhere)
			$qiTotalRec->execute($paramswhere);
		else
			$qiTotalRec->execute();
		$iTotalRec	=	$qiTotalRec->fetchColumn();

		$result =   $Q->Prepare();
		$result->execute($qparams+$paramswhere);

		$out    =   array(
			"sEcho" => intval($C->sEcho),
			"iTotalRecords" => $iTotalRec,
			"iTotalDisplayRecords" => $this->getController()->queryColumn('SELECT FOUND_ROWS()'),
			"aaData" => $result->fetchAll()
		);

		$controller = $this->getController();

		$rowTemplate = $controller->getView('rows/user', false, false);
		$out = $this->parser($out, $rowTemplate);


		return $this->getController()->getAjaxView($out);
	}

	/**
	 * @param $out
	 * @param $rowTemplate
	 * @return mixed
	 */
	protected function parser($out, $rowTemplate)
	{
		foreach ($out['aaData'] as &$user) {
//								$user[2] = "<a href=\"" . $actionurl . $user[0] . "\">{$user[2]}</a>";
			$html = $rowTemplate->init('nuser', $user)->toString();
			if (preg_match_all('/<td[^>]*?>([\s\S]*?)<\/td>/', $html, $matches)) {
				$user = $matches[1];
			}
		}
		return $out;
	}
}
