<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Qube\Hybrid\Events;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;

/**
 * Description of SystemEventManager
 *
 * @author amado
 */
class SystemEventDispatch extends \SystemDataAccess implements \Zend\EventManager\EventManagerAwareInterface
{
	/**
	 *
	 * @var \Zend\EventManager\EventManager()
	 */
	protected $eventManager;
	
	function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
		$this->eventManager	=	$eventManager;
		return $this;
	}
	
	/**
	 * 
	 * @return \Zend\EventManager\EventManager()
	 */
	function getEventManager() {
		if(null === $this->eventManager)
		{
			$this->setEventManager(new \Zend\EventManager\EventManager());
		}
		return $this->eventManager;
	}
	
	/**
	 * 
	 * @todo move this to a specialized class.
	 * 
	 * @param type $reseller_ID
	 */
	function setUpSystemForReseller(\UserModel $reseller, \UserDataAccess $uda)
	{
		// if the user does not have a buscription associated, create the subscription
		$default_subscription_ID	=	3;
		
		$reseller_ID	=	$reseller->getID();
		// set up the default system roles
		$uda->createDefaultSystemRoles($reseller_ID);
		
		
		// check if a subscription exists for the user.
		$account_limits	=	$uda->getAccountLimits($reseller_ID);
		if(NULL == $account_limits)
		{
			$date = new \DateTime();
			$uda->subscribeUser($default_subscription_ID, $reseller_ID, \PointsSetBuilder::firstDayNextMonth($date));
		}
		
	}
	
	function onUserLogin(\UserModel $User, \UserDataAccess $uda)
	{
		\UserModel::CreateUserDirectory(QUBEROOT . 'users', $User->getID());
		
		// first time a user is logging in
		if($User->last_login == '0000-00-00 00:00:00')
		{
			if($User->isReseller())
			{
				$this->setUpSystemForReseller($User, $uda);
				$this->getEventManager()->trigger('onResellerFirstLogin', compact('User'));
			}
			$this->getEventManager()->trigger(__FUNCTION__, compact('User'));
		}
		$this->getEventManager()->trigger(__FUNCTION__, compact('User'));
	}
	
	function onPermissionDenied(\UserModel $User, $permission)
	{
		#if()
		system('export XAUTHORITY=/home/amado/.Xauthority && export DISPLAY=:0 && sudo -u amado notify-send hello');
		$this->getEventManager()->trigger(__FUNCTION__, compact('User', 'permission'));
	}
		
	static function onEvent($SET, $evstr, $evTarget, $arguments)
	{
		if(NULL === $SET) return NULL;
		
		/* @var $SET SystemEventDispatch */
		
		return $SET->getEventManager()->trigger($evstr, $evTarget, $arguments);
//		return call_user_func_array(array($SET, $evstr), $args);
	}
}
