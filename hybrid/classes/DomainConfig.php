<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of DomainConfig
 *
 * @author amado
 */
class DomainConfig implements InputValidator {
    //put your code here
    
    const NS_6Q1    =   1;
    const NS_6Q2    =   2;
    const NS_6Q3    =   3;
    
    const NS_NOEXIST    =   -1;  // the domain is not registered
    const NS_ERROR    =   -2;  // error ocurred: no nameservers were returned, whois[status] != available, wtf? error
    const NS_NOCONFIG   =   -3;  // the nameservers are not configured
    
    /*  @const  */
//    protected static $SYSTEM_NS    =   array(0 => array('ns1.6qube.com', 'ns1.6qube.net', 'ns1.thepartnerportal.com'),
//                                             1  =>  array('ns2.6qube.com', 'ns2.6qube.net', 'ns2.thepartnerportal.com'));
    
    /**
     *
     * tHE KEYs are based on NS_6Q* KEYS
     * 
     * @var type 
     */
    protected static $SYSTEM_NS    =   array(1 => array('NS1.6QUBE.NET', 'NS1.6QUBE.COM', 'NS1.THEPARTNERPORTAL.COM',
                                                        'NS2.6QUBE.NET', 'NS2.6QUBE.COM', 'NS2.THEPARTNERPORTAL.COM'),
                                             2  =>  array('NS3.6QUBE.NET', 'NS3.6QUBE.COM', 'NS3.THEPARTNERPORTAL.COM',
                                                        'NS4.6QUBE.NET', 'NS4.6QUBE.COM', 'NS4.THEPARTNERPORTAL.COM'),
                                            3   => array('NS5.6QUBE,NET', 'NS5.6QUBE.COM', 'NS5.THEPARTNERPORTAL.COM',
                                                        'NS6.6QUBE.NET', 'NS6.6QUBE.COM', 'NS6.THEPARTNERPORTAL.COM'));
    protected $hostname   =   '';
    protected $nameservers  =   NULL;
    protected $whoisInfo    =   NULL;
    function __construct($hostname    =   NULL)
    {
        if($hostname)
            $this->setHostname($hostname);
    }
    
    /**
     * 
     * @param type $hostname
     * @return \ValidationStack|\self
     */
    static function FromHostname($hostname, $VS =   NULL)
    {        
        $V  =   $VS ? $VS : new ValidationStack;
        if(!self::ValidateHostname($hostname, $V))  return $V;
        
        // OK! Valid
        return new self($hostname);
        
        $parts  =   explode('.', $hostname);
        $domain = implode('.', array_slice($parts, -2));    // strip subdomains            
        
        return new self($domain);
    }
    
    static function ValidateHostname($hostname, ValidationStack $Validation)
    {
        $HostValidator          =   new Zend_Validate_Hostname();
        
        if($HostValidator->isValid($hostname)) return true;
        
        $Validation->addError ('hostname', 'Not a valid hostname');
        return false;
    }
    
    function ValidateDomain(ValidationStack $Validation)
    {
        return self::ValidateHostname($this->getHostname(), $Validation);
    }
    
    function setHostname($domain)
    {
        $this->hostname   =   $domain;
    }
    
    function getHostname(){
        return $this->hostname;
    }
    
    /**
     * Get the whois information as a string (LEGACY)
     * @deprecated since version number
     * @return boolean|string
     */
    function getWhoisInfo()
    {
        if(!$this->whoisInfo)
        {
            require_once QUBEADMIN . 'inc/domains.class.php';
            $result =   Domains::getWhoisStatic($this->hostname);

            $this->whoisInfo    =   (!is_array($result)) ? array() : $result;    // || $result['status']    ==  'unavailable') return false;
        }
        
        return $this->whoisInfo; // results a array of whois and status (unavailable) keys
    }
    
    function getNameServers()
    {
        if(!$this->nameservers){
            $ns = dns_get_record($this->hostname, DNS_NS);
            
            $this->nameservers  = array_map('strtoupper', extract_column($ns, 'target'));
        }
        
        return $this->nameservers;
    }
    
    /**
     * 
     * @return NS_ERROR|NS_NOEXIST|NS_NOCONFIG|NS_6Q1
     */
    function getConfigStatus()
    {
        $nameservers    =   $this->getNameServers();
        if(empty($nameservers))
        {
            $whoisInfo  =   $this->getWhoisInfo();  // implement legacy whoisinfo
            if($whoisInfo['status'] ==  'available')
                return self::NS_NOEXIST;
						
            if($whoisInfo['result'] == 'error' && strpos($whoisInfo['message'], 'Invalid IP') === 0)	// if running query from a dev server, assume no error and continue.
										return self::NS_NOEXIST;
						
            return self::NS_ERROR;
        }
        
        // nameservers are configured, thus domain exists.
        
        foreach(self::$SYSTEM_NS as $SERVERID => $SERVER_NS){
            if(in_array($nameservers[0], $SERVER_NS) && in_array($nameservers[1], $SERVER_NS))
                    return $SERVERID;
        }
        return self::NS_NOCONFIG;
    }
    
    /**
     * Creates the addon domain, and creates the *.domain.com entry consequently changing the dns row from A to CNAME
     * @param xmlapi $xmlapi
     * @return boolean
     * @throws HybridWebsiteSetupFailureException
     */
    function setupWildcardHOST(IgnitedServerApi $xmlapi, $createaddon	=	true)
    {
        
#        $xmlapi =   new xmlapi($host, $user, $pass);
        
	if($createaddon){
            $result  =   $xmlapi->api2_query(null, "AddonDomain", "addaddondomain",
                            array('dir' => $xmlapi->getRouterPath(),
                                    'newdomain' =>  $this->hostname,
    #                                'pass'  =>  $password,
                                    'subdomain' =>  $this->hostname
                                ));

            $errormsg   =   'Api Error: ' . $result->data->reason;
        }else{
            $errormsg	=	"Skipped creating addon for $this->hostname";
	}
        
        if(!$createaddon || (string)$result->data->result    ==  '1' || preg_match('/is already configured.$/', $result->data->reason))  // domain exists or has been successfully created. lets rock!
        {
            // create *.domain.com. don't worry about result. the subdomain probably already exists.
            $dynsubsresult  =   $xmlapi->api2_query(null, 'SubDomain', 'addsubdomain',
                        array('domain'  =>  '*',
                                'rootdomain'    =>  $this->hostname,
                                'dir'   =>  $xmlapi->getRouterPath()
                            ));
            
            
            // fix/reset cname dns
            
            $zoneinfo   =   $xmlapi->api2_query(null, 'ZoneEdit', 'fetchzone_records',
                        array('domain'  => $this->hostname));
            
            $dns_line   =   NULL;
            $records    =   $zoneinfo->data;
            foreach($records as $line)
                if($line->name  ==  '*.' . $this->hostname . '.')
                        $dns_line   =   $line;
                
                
                
            if($dns_line){
                if($dns_line->type == "CNAME") return TRUE;
            
                // change aname to cname
                $res3   =   $xmlapi->api2_query(null, "ZoneEdit", "edit_zone_record", 
                        array('domain'=>$this->hostname, 'Line' => (int)$dns_line->line, 'type'=>'CNAME', 'cname'=>$this->hostname)
                    );
                
#                var_dump($res3);
                if($res3->data->status == "1")
                    return TRUE;
                else {
                    $errormsg   =   $res3->data->statusmsg;
                }
            }else{
                // could not find the line
            
                $errormsg   .= "\n Error2: " . $dynsubsresult->data->reason .   "\n Error3: " . $zoneinfo;            
                
            }
            /*  replace the 
            if($dynsubsresult->data->result ==  '1')
                return TRUE;
            */
            
            
        }
        
        throw new HybridWebsiteSetupFailureException($errormsg);
    }
    
    function getSubdomains(xmlapi $xmlapi)
    {
        $result  =   $xmlapi->api2_query(null, "SubDomain", "listsubdomains",
                        array('regex' => $this->hostname));
        
        return new SubdomainXmlResults($result);
    }
    
    /**
     * Basically accepts options through $vars
     * Checks NS and validates that the name servers have been configured properly.
     * 
     * 
     * @param array $vars
     * @param \ValidationStack $vs
     * @return type
     * @throws QubeException
     */
    function Validate(array &$vars, \ValidationStack $vs) {
        if(!$this->hostname) throw new QubeException('Must assign domainname to domaincofig.');
        if(is_amadodevlocal()) return true; // bypass
        /*switch($this->getConfigStatus())
        {
            case DomainConfig::NS_NOCONFIG:
                $vs->addError('domain', 'The Nameservers Are not configured properly.');
                break;
            case DomainConfig::NS_ERROR:
                $vs->addError('domain', 'Could not detect nameserver configuration for the domain.');
                break;
            case DomainConfig::NS_NOEXIST:
                if(!isset($vars['ALLOW_UNREGISTERED']))
                    $vs->addError('domain', 'The domain name is not registered.');
                break;
        }*/
        return !$vs->hasErrors();
    }

    function getDomainName()
    {
        $hostname = $this->getHostname();
        $split = explode('.', $hostname);
        $count = count($split);

        // match: site, site.com, site.foo, site.foobar
        if($count < 3)
            return $hostname;

        $list = file_get_contents(HYBRID_LIB . 'tlds.txt');
            // cannot use a single preg_match because regex is too long and compile errors ocur, so we must loop
        $tlds = explode('|', trim(preg_replace(array("#(//[\\s\\S]*?\n)|(\r)#", "#[\n]+#"), array("", "|"), $list), '|'));

        $name = $hostname;
        // there could be possible matches: example: domain.hk.org matches both hk.org and org tlds
        $tld_depth = 0;
        foreach($tlds as $tld)
        {
            // case: subdomain.domain.hk.org
            // first match: org (tld_depth = 1) $name_str = subdomain.domain.hk, $name = hk
            // 2nd match: hk.org (tld_depth = 2, $name_str = subdomain.domain, $name = domain
            //
            if( 1 == preg_match('#^(.+)\.'.$tld.'$#', $hostname, $m))
            {
                $depth = substr_count('.', $tld)+1; // hk.org => 2, org => 1
                if($depth > $tld_depth)
                {
                    $tld_depth = $depth;
                    $name_str = $m[1];
                    $name =  preg_replace('#.*?([^\.]+)$#', '\1', $name_str) . '.' . $tld;
                }
            }
        }
        return $name;
    }
}

class HybridWebsiteSetupFailureException extends PDOException {}
