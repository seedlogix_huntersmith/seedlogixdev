<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of Keywords
 *
 * @author amado
 */

class Keywords
{
//    protected $offset   =   0;
    protected $perpage  =   0;
//    protected $cid  =   0;
    protected $keywords =   array();
    protected $offset   =   0;
    protected $cache    =   array();    
    protected $cachekey    =   array();
    /**
     *
     * @var DBWhereExpression
     */
    protected   $whereids   =   null;
    
    function setPerPage($p){
        $this->perpage= $p;
    }
    
    function setWhere($w){
        $this->whereids = DBWhereExpression::Create($w);
    }    
    
    function __construct($cacheprefix){
        $this->cachepostfix =   $cacheprefix;
    }
    
    function setOffset($offset, $perpage){
        $this->offset   =   $offset;
        $this->perpage  =   $perpage;
    }
    
    function setInterval($int){
        $this->interval =   $int;
    }

	function getOldestTouchpointCreationDate(){
                if(is_amadodevlocal()) return piwik_inception(); // @todo remove this or copy the _archive_ table to my local machine.
		return		Qube::Start()->query('SELECT DATE(MIN(created)) FROM 6q_trackingids ' . $this->whereids->FullClause())
			->fetchColumn();
	}
    
    function setPeriod($per){
	if($per	== 'all'){
		$this->period	=	'range';

		$date	=	$this->getOldestTouchpointCreationDate();
		$this->setInterval($date . ',today');
		return;
	}
        $this->period   =   $per;
    }
   
    function loadCache($lengthmins  =   30){        
        
        $zCache =   HybridBaseController::getZendCache($lengthmins*60);
        $key    =   "KEYWORDS_{$this->cachepostfix}";
        
        $cache  =   $zCache->load($key);
        if(!$cache)
            $cache  =   array();//'keywords'    => array(), 'offset' => 0);
        $key2   =   "{$this->interval}{$this->period}";
        if(!isset($cache[$key2]))
            $cache[$key2]   =   array('keywords'    => array(), 'offset' => 0);
        
        $this->cache    =   $cache;
        $cache  =   $cache[$key2];
        
        $this->keywords    =   $cache['keywords'];
        $this->offset       =   max($cache['offset'], $this->offset);
        $this->cachekey =   $key2;
        
    }
    
    protected function cacheSave(){
        $this->cache[$this->cachekey]   =   array('keywords' => $this->keywords, 'offset' => $this->offset+$this->perpage);
        HybridBaseController::getZendCache(90)->save($this->cache);
    }
    
    function loadKeywords(){
		// temporarily disabled @todo fix this shit
		return array();
		
        $ids =   $this->getTrackingIdsStr(Qube::Start(), $this->offset, $this->perpage);
        
        if(!$ids) return array();
        
        set_time_limit(0);
        $request = $this->getPiwikApiRequest('
                                method=Referers.getKeywords
                                &idSite=' . $ids . '
                                &date=last' . $this->interval . '
                                &period=' . $this->period . '
                                &format=php&serialize=0
                                ');
        $alldata = array();

        $dt =   $request->process();
if($this->period == 'range'){	// return structure is different than lastX period
	foreach($dt as $site => $siteinfo){
		foreach($siteinfo as $keywords){
		$keyword	=	$keywords['label'];
		$nbvisits	=	(int) $keywords['nb_visits'];
		if(!isset($alldata[$keyword]))
		$alldata[$keyword]	=	$nbvisits;
		else
		$alldata[$keyword]	+=	$nbvisits;
		}
	}
}else
        foreach ($dt as $site => $siteinfo) {
            if (!is_array($siteinfo)) {
                var_dump($dt, $site, $ids, $this->interval, $this->period);
                exit;
            }
            if (empty($siteinfo))
                continue;
            foreach ($siteinfo as $dates) {
                if (empty($dates))
                    continue;
                foreach ($dates as $kwinfo) {
                    $keyword = $kwinfo['label'];
                    $nbvisits = (int) $kwinfo['nb_visits'];
                    if (!isset($alldata[$keyword]))
                        $alldata[$keyword] = $nbvisits;
                    else
                        $alldata[$keyword] += $nbvisits;
                }
            }
        }
//		var_dump($alldata, $dt);
        unset($alldata['Keyword not defined']);
        return $alldata;
        arsort($alldata, SORT_NUMERIC);

//        return array_slice($alldata, 0, $length);        
    }
    
    function load(){
	if(!getflag('disable_cache'))
        $this->loadCache(); // preload info
        $newkeywords    =   $this->loadKeywords();
        foreach($newkeywords as $keyword => $count){
            if(!isset($this->keywords[$keyword]))
                $this->keywords[$keyword]   =   $count;
            else
                $this->keywords[$keyword]   +=  $count;
        }
if(!getflag('disable_cache'))
        $this->cacheSave();
    }
    
    
    
    function getKeywords($length = 5){

        arsort($this->keywords, SORT_NUMERIC);

        return array_slice($this->keywords, 0, $length);
    }
        /**
     * 
     * @return Piwik_API_Request
     */
    function getPiwikApiRequest($str) {

        $piwikpath = Qube::get_settings('PIWIK_PATH');
        define('PIWIK_INCLUDE_PATH', $piwikpath ? $piwikpath : realpath(HYBRID_PATH . '../../../piwik/public_html/'));

        define('PIWIK_USER_PATH', PIWIK_INCLUDE_PATH);
        define('PIWIK_ENABLE_DISPATCH', false);
        define('PIWIK_ENABLE_ERROR_HANDLER', false);
        define('PIWIK_ENABLE_SESSION_START', false);

        require_once PIWIK_INCLUDE_PATH . "/index.php";
        require_once PIWIK_INCLUDE_PATH . "/core/API/Request.php";
        require_ONCE PIWIK_INCLUDE_PATH . '/core/DataTable/Renderer/Php.php';
        Piwik_FrontController::getInstance()->init();


        $request = new Piwik_API_Request($str . '
                                &token_auth=b00ce9652e919198e1c4daf7bf5aed5a
        ');
        return $request;
    }
    
    /**
     * 
     * This is more complex than it looks. group_concat has a max string length, which means we have to LOOP to get all the tracking ids.
     * This is a helper function to return a set of tracking ids based on offset and perpage.
     * 
     * @param Qube $q
     * @param type $offset
     * @param type $perpage
     * @return type
     */
    function getTrackingIdsStr(Qube $q, $offset, $perpage){                
        $strids = $q->query('SELECT 
		GROUP_CONCAT(tracking_id)  FROM ( SELECT tracking_id FROM 	
		6q_trackingids ' . $this->whereids->FullClause() . ' order by id asc LIMIT ' . (int) $offset . ', ' . (int) $perpage . '
	) g', false)->fetchColumn();


        return $strids;
    }
    
    function getTotalIds(){        
        return Qube::Start()->query('SELECT count(*) FROM 6q_trackingids ' . $this->whereids->FullClause(), false)->fetchColumn();
    }
    
}

