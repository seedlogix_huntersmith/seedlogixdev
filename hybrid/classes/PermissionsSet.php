<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

use Qube\Hybrid\Authorizer\Authorizer;

/**
 * Description of PermissionsSet
 *
 * @author amado
 */

class PermissionsSet
{
    /**
     *
     * @var UserWithRolesModel
     */
    protected $User =   null;
    protected $permission_cache    =   array();
    static $notcachable	=	array(
//				'create_users' => 1,
//					    'create_BLOG' => 1,
//					    'create_WEBSITE' => 1,	// making this cacheable, because we are separating the limit verification into an idependent verification (via DataAccess::actionUser::verifyLimit)
//					    'create_LANDING' => 1,
//					    'create_SOCIAL' => 1,
//					    'create_MOBILE' => 1,
						'remove_addon' => 1,
                        'remove_subscription' => 1);

    /**
     *
     * @var Authorizer[]
     */
    protected $authorizers     =   array();
    
    function __construct(UserWithRolesModel $User) {
        $this->User =   $User;
    }
    
    function getUser(){
        return $this->User;
    }
    
    function addAuthorizer(Authorizer $auth){
        $this->authorizers[]    =   $auth;
    }
    
    protected function checkPermission($p, $id =   0, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL){        
//        if($id !=   0){
            foreach($this->authorizers as $authorizer){
                if($authorizer->check($p, $id, $SED)) return true;
            }
            return false;   //$this->callFunc($p, $id);
//        }
        
        return isset($this->permission_cache[$p . '_0']) ? $this->permission_cache[$p . '_0'] : false;
    }

    function hasPermissions(){
        return !empty($this->permission_cache) && !empty($this->authorizers);
    }
    
    function setPermission($permission, $id =   0, $value   =   true){
			\Qube\Hybrid\Authorizer\PermissionCheckerAuthorizer::assignSubPermissions($this, $permission, $id, false);
        $this->permission_cache[$permission . '_' . $id]    =   $value;
        return $this;
    }
    
    function can($permission, $id =   0, \Qube\Hybrid\Events\SystemEventDispatch $SED = NULL) {
        $key    =   $permission . '_' . $id;
        // this one is a bit tricky
        // if id != 0 AND there is no permission with that id then:
        // check the cache for the same permission with id of 0, 
        //  effectively rendeing an id of 0 as a super global.
        // end.

        if(!preg_match('/create_.+/', $permission) && isset($this->permission_cache[$permission . '_0']) && $this->permission_cache[$permission . '_0']) return true;

        if(!isset($this->permission_cache[$key]) || isset(self::$notcachable[$permission]))
            $this->permission_cache[$key]   =   $this->checkPermission($permission, $id, $SED);

//            return isset($this->permission_cache[$permission . '_0']) ? $this->permission_cache[$permission . '_0'] : $this->permission_cache[$key];
            return $this->permission_cache[$key];
    }

    function hasPermission($permission, $id = 0){
        $key = $permission . '_' . $id;
        return isset($this->permission_cache[$key]);
    }
}
