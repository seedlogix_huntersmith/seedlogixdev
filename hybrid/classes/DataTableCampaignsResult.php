<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataTableCampaignsResult
 *
 * @author Eric
 */
class DataTableCampaignsResult extends DataTableResult{
    protected $campaignsQuery;
    protected $filter;


	public function __construct(VarContainer $var)
	{

		if (!isset($var->interval)) {
			$chartConfig = Cookie::getVal('chartconfig', null);

			if ($chartConfig == '') {
				$chartConfig = '7-DAY';
			}

			list($interval, $period) = preg_split('/-/', $chartConfig);
			$var->load(compact('interval', 'period'));
		}

		$var->load(array('table' => 'Campaigns', 'isalltime' => $var->period == 'ALL'));

		parent::__construct(null, $var); //var_dump($this->_req);

//        if(!isset($var->iSortCol_0)){
//            $var->iSortCol_0 = 0;
//            $var->sSortDir_0 = 'asc';
//				}
		if (!isset($var->sEcho)) {
			$var->sEcho = 0;
			$var->iDisplayStart = 0;
			$var->iDisplayLength = 10;
			$var->bSortable_0 = 'true';
			$var->sColumns = implode(',', $this->getColumnTitles());
			if (isset($var->chart)) {
				$var->iDisplayLength = NULL;
			}
		}
		$this->campaignsQuery = $this->getQueryCampaigns();
	}

    public function getQuery($where = array()){
				$user	=	$this->_req->ActionUser;
        $where[] = ' (:cid = 0 OR c.id = :cid) and c.trashed = 0';
				
				// 
				if(/*$user->isAdmin() ||*/ $user->can('view_campaigns'))	// allow user to view campaigns from admin perspective.
				{
					$where[]	=	'( c.user_ID = ' . $user->getID() . ' OR c.user_parent_id = ' . $user->getParentID() . ' )';
				}else{
					$where[]	=	'c.user_ID = ' . $user->getID();
				}
				
				$QWhere	=	' WHERE ' . join(' AND ', $where);
				
        $order = 'order by visitsnow desc';
        $Query = "from (
		select c.id, c.name, c.date_created, ";
				
				if($this->_req->isalltime)
				{
					$Query .= "0 as visitsprev, ";
				}else{
						$Query .= "
					(
						select sum(r.nb_visits) from piwikreports r where
							r.user_id = c.user_id and r.cid = c.id and r.point_date BETWEEN :d1 AND :d2
					) as visitsprev, ";
				}
				
				$Query .= "
			(
				select sum(r2.nb_visits) from piwikreports r2 where
					r2.user_id = c.user_id and r2.cid = c.id
					and	r2.point_date BETWEEN :d3 AND :d4
			) as visitsnow, c.total_touchpoints - c.trashed_touchpoints as num_touchpoints,
			(
				select sum(r.nb_leads) from piwikreports r where
					r.user_id = c.user_id and r.cid = c.id and r.point_date BETWEEN :d3 AND :d4
			) as num_leads
		from campaigns c $QWhere $order ) as j";
        return $Query;

    }

    public function getQueryCampaigns($where = array()){
        $Q	=	'select SQL_CALC_FOUND_ROWS j.id, j.name, coalesce(j.visitsprev, 0) as visitsprev, coalesce(j.visitsnow, 0) as visitsnow, j.num_touchpoints,
				j.num_leads, if(j.visitsnow = 0, 0, j.num_leads/j.visitsnow)*100 as conversionspct,
				j.visitsnow/j.visitsprev*100 as visitschangepct,
				DATE_FORMAT(j.date_created, "%b %d, %Y") as created_str ' . $this->getQuery($where);
								
			if($this->_req->checkValue('sSearch', $search) && !empty ($search)){
					if(is_numeric($search)){
							$Q .= ' where j.id = :search';
					}else{
							$Q .= ' where j.name like :search';
					}
			}
			return $Q;
    }
    
    protected function OrderBy($column, $dir = 'asc') {
        switch ($column){
            case 'visitschangepct':
		$c	=	'j.visitsnow/j.visitsprev*100';
#                $c = 'if(j.visitsprev != 0, (j.visitsnow-j.visitsprev)/j.visitsprev, j.visitsnow)*100';
                break;
            case 'visitsnow':
                $c = 'coalesce(j.visitsnow, 0)';
                break;
            case 'conversionspct':
                $c = 'if(j.num_leads = 0, 0, j.visitsnow/j.num_leads)*100';
                break;
            case 'created_str':
                $c = 'j.date_created';
                break;
            default :
                $c = 'j.' . $column;
                break;
        }
        $this->campaignsQuery .= ' order by ' . $c . ' ' . $dir; 
    }
    
    public function getTotalRecords($args = array()) {
        $total = Qube::GetPDO()->prepare('SELECT COUNT(*) ' . $this->getQuery());
        $total->execute($args);
        
        return $total->fetchColumn();
    }
    
    public function modifyQuery() {
			if($this->_req->checkValue('iDisplayLength', $length)){
					$start =   max(0, (int)$this->_req->getValue('iDisplayStart', 0));
					$this->campaignsQuery .= ' Limit ' . $start . ', ' . min(100, $length);
			}
    }
		
		function getPlaceHolders()
		{
			$vc	=	$this->_req;
			$dateMagic = new PointsSetBuilder($vc, $this->ActionUser->created);
			$d4 =   $dateMagic->getEndDate();
			$d3 =   $dateMagic->getStartDate();
			$d2 =   $d3;
			$d1 =   $dateMagic->previousSet()->getStartDate();
			$cid = (int)$vc->getValue('cid', 0);
			$args = compact('d1', 'd2', 'd3', 'd4', 'cid');
			
			if($vc->checkValue('sSearch', $search) && !empty($search)){
					if(!is_numeric($search)){
							$args['search'] = '%' . $search . '%';
					}
					else {
							$args['search'] = $search;
					}
			}
			return $args;
		}
		
		function getData($args = NULL, $fetchMode = NULL) {
			$args	=	$args ? $args : $this->getPlaceHolders();
			return parent::getData($args, $fetchMode);
		}
    
    protected function getPreparedStatement() {
        $statement = Qube::GetPDO()->prepare($this->campaignsQuery);
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement;
    }

    function getRowsData(){
        $vars = array();
        $vars['campaigns'] = $this->getData(NULL, PDO::FETCH_OBJ);
        $vars['totalDisplayRecords'] = $this->getTotalDisplayRecords();
        return $vars;
    }

}
