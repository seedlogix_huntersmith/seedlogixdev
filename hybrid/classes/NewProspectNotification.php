<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 12/20/14
 * Time: 1:55 PM
 */

class NewProspectNotification extends EmailNotification {
    /**
     * the object
     *
     * @var ProspectModel
     */
    protected $form;

    function __construct(ProspectModel $form) {
        $this->form = $form;
    }
    // @todo use smarty to parse this kind of templates.

    function getInnerContent(){
        $form   =   $this->form;
        return
            '<h1>Information Captured</h1>
        <p><strong>Name:</strong> '.$form->name.'</p>
        <p><strong>Email:</strong> '.$form->email.'</p>
        <p><strong>Phone:</strong> '.$form->phone.'</p>
        <p><strong>Comments:</strong> '.$form->comments.'</p>
        <p><strong>Website:</strong> '.$form->website.'</p>
        <p><strong>Page:</strong> '.$form->page.'</p>';
    }

    function getContent() {

        $form = $this->form;

        //begin HTML message
        $message = '<html><body>';
        $message .= $this->getInnerContent();
        $message .= '</body></html>';

        return $message;
    }

    function getSubject()
    {
        return 'New Prospect Notification';
    }
}