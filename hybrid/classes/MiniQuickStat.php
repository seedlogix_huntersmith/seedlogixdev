<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

interface ListItem
{
    function printli($urlmaker);
}

/**
 * Description of MiniQuickStat
 *
 * @author amado
 */
class MiniQuickStat implements ListItem {
    
    protected $data =   array();
    protected $description  =   '';
    protected $unit =   '';
    function __construct($data, $unit, $desc, $class) {
        $this->setData($data);
        $this->setDescription($desc);
        $this->unit =   $unit;
		 $this->style =   $class;
    }
    
    function setData(&$data)
    {
        $this->data =    &$data;
    }
    function setDescription($description)
    {
        $this->description= $description;
    }
    function getData()
    {
        return $this->data;
    }
    function getDescription()
    {
        return $this->description;
    }
		
		function getLi()
		{
			ob_start();
			$this->printli('fff');
			$str	= ob_get_clean();
			
			return $str;
		}
    
    function printli($urlmaker)
    {
        ?>
	<li>
		<div class="p_line_up p_canvas">
                    <span class="<?=$this->style?>"><?php echo join(',', $this->getData()); ?></span>
		</div>
		<div class="ov_text">
                    <span><strong><?php echo array_sum($this->data); ?></strong> <?php echo $this->unit; ?></span>
                    <?php print $this->getDescription(); ?>
		</div>
	</li><?php
    }
}
