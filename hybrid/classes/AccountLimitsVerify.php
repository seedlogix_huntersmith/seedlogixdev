<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 use Qube\Hybrid\DataAccess;
 use Qube\Hybrid\Events;
 
/**
 * Description of AccountLimitsVerify
 *
 * @author amado
 */
class AccountLimitsVerify
{
	const COMPARE_LESSTHAN	=	1;
	const COMPARE_LESSTHANOREQUAL	=	2;
	
	protected $verify_options = array();
	
	/**
	 *
	 * @var stdObject
	 */
	protected $limits	=	NULL;
	
	/**
	 *
	 * @var DataAccess\AccountDataAccess
	 */
	protected $ada		=	NULL;
	
	protected	$compare_type	=	self::COMPARE_LESSTHAN;
	
	function __construct($limits, DataAccess\AccountDataAccess $ada){
		$this->setLimits($limits)
			->setAccountDataAccess($ada);			
	}
	
	function setOptions(array $options)
	{
		$this->verify_options= $options;
		return $this;
	}
	
	function setLimits($limits)
	{
		
		$this->limits	=	$limits ? $limits : (object)array();
		return $this;
	}
	
	function setAccountDataAccess(DataAccess\AccountDataAccess $ada)
	{
		$this->ada	=	$ada;
		return $this;
	}
	
	function getLimit($obj_type)

	{
		// NOT-ACTIVATED-LIMITS, DEFAULT = UNLIMITED
		switch($obj_type)
		{
			case 'RESPONDERS':
				return SubscriptionModel::UNLIMITED;
		}
		
		if(!isset($this->limits->$obj_type))
		{
			$this->limits->$obj_type	=	0;	// limit is zero, not allowed to create any objects of this type.
		}
		
		return (int)$this->limits->$obj_type;
	}
	
	function reportLimitReached($SED, $created, $limit, $func)
	{
		$args = $this->verify_options;
        $args['Validation']->created = $created;
        $args['Validation']->limit = $limit;
		$args['func']	=	$func;

        if($created < $limit) return;	// dont do anything if limit hasn't been reached
		Events\SystemEventDispatch::onEvent($SED, 'LimitReached', $this->ada->getActionUser(), $args);
	}

	function canCreateUser(\Zend\EventManager\EventManagerAwareInterface $e)
	{
		$user_limit	=	$this->getLimit('USERS');
		if(SubscriptionModel::UNLIMITED === $user_limit)		return true;
		
		$total_users	=	$this->ada->getTotalCreatedUsers();
		return $this->compare($total_users, $user_limit, __FUNCTION__, $e);
	}
	
	function canCreateBLOG(\Zend\EventManager\EventManagerAwareInterface $e)
	{
		$tp_limit	=	$this->getLimit('TOUCHPOINTS');
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;
		
		$total_BLOG	=	$this->ada->getTotalCreatedBlogs();
		return $this->compare($total_BLOG, $tp_limit, __FUNCTION__, $e);
	}
	
	protected function compare($created, $limit, $func, \Zend\EventManager\EventManagerAwareInterface $e	=	NULL)
	{
		$result	=	false;
		switch($this->compare_type)
		{
			case self::COMPARE_LESSTHAN:
				$under_limit	=	$created < $limit;
				$this->reportLimitReached($e, $created, $limit, $func);
				return $under_limit;
				break;
			
			case self::COMPARE_LESSTHANOREQUAL:
				return $created <= $limit;
				break;
		}
		
		throw new QubeException();
	}
	
	function canCreateWEBSITE(\Zend\EventManager\EventManagerAwareInterface $e)
	{
		$tp_limit	=	$this->getLimit('TOUCHPOINTS');
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;
		
		$total_WEBSITE	=	$this->ada->getTotalCreatedTP('WEBSITE');
		
		return $this->compare($total_WEBSITE, $tp_limit, __FUNCTION__, $e);
	}

    function canCreateLANDING(\Zend\EventManager\EventManagerAwareInterface $e)
    {
        $tp_limit	=	$this->getLimit('TOUCHPOINTS');
        if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

        $total_WEBSITE	=	$this->ada->getTotalCreatedTP('LANDING');
		
		return $this->compare($total_WEBSITE, $tp_limit, __FUNCTION__, $e);
    }

	function canCreatePROFILE(\Zend\EventManager\EventManagerAwareInterface $e)
	{
		$tp_limit	=	$this->getLimit('TOUCHPOINTS');
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

		$total_WEBSITE	=	$this->ada->getTotalCreatedTP('PROFILE');

		return $this->compare($total_WEBSITE, $tp_limit, __FUNCTION__, $e);
	}

    function canCreateMOBILE(\Zend\EventManager\EventManagerAwareInterface $e)
    {
        $tp_limit	=	$this->getLimit('TOUCHPOINTS');
        if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

        $total_WEBSITE	=	$this->ada->getTotalCreatedTP('MOBILE');
		
		return $this->compare($total_WEBSITE, $tp_limit, __FUNCTION__, $e);
    }

    function canCreateSOCIAL(\Zend\EventManager\EventManagerAwareInterface $e)
    {
        $tp_limit	=	$this->getLimit('TOUCHPOINTS');
        if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

        $total_WEBSITE	=	$this->ada->getTotalCreatedTP('SOCIAL');

        return $this->compare($total_WEBSITE, $tp_limit, __FUNCTION__, $e);
    }
	
    function canCreateResponder(\Zend\EventManager\EventManagerAwareInterface $e)
    {
        $responder_limit	=	$this->getLimit('RESPONDERS');
        if(SubscriptionModel::UNLIMITED === $responder_limit)	return true;

        $total_RESPONDER	=	$this->ada->getTotalCreatedTP('SOCIAL');
        $under_limit	=	$total_RESPONDER < $responder_limit;
		
		$this->reportLimitReached($e, $under_limit, __FUNCTION__);
		
		return $under_limit;
    }

	function canRemoveAddon(\Zend\EventManager\EventManagerAwareInterface $e){
		$this->compare_type	=	self::COMPARE_LESSTHANOREQUAL;
		foreach($this->limits as $limit => $value){
			switch($limit){
				case 'TOUCHPOINTS':
					return $this->canCreateMOBILE($e) 
							&& $this->canCreateWEBSITE($e)
							&& $this->canCreateLANDING($e)
							&& $this->canCreatePROFILE($e)
							&& $this->canCreateSOCIAL($e)
							&& $this->canCreateBLOG($e);
			}
		}
	}
	
	function can($permission, \Zend\EventManager\EventManagerAwareInterface $e)
	{
		switch($permission)
		{
			case 'create_WEBSITE':
				return $this->canCreateWEBSITE($e);
			case 'create_SOCIAL':
				return $this->canCreateSOCIAL($e);
			case 'create_LANDING':
				return $this->canCreateLANDING($e);
			case 'create_PROFILE':
				return $this->canCreatePROFILE($e);
			case 'create_MOBILE':
				return $this->canCreateMOBILE($e);
			case 'create_BLOG':
				return $this->canCreateBLOG($e);
			case 'create_users':
				return $this->canCreateUser($e);
			case 'create_keyword':
				return $this->canCreateKeyword($e);
            case 'create_contact':
			case 'create_responder':
//				return $this->canCreateResponder($e);
			case 'create_campaigns':
//				return $this->canCreateCampaign($e);
			case 'create_blogposts':
//				return $this->canCreateBlogposts($e);
			case 'create_nav':
//				return $this->canCreateNav($e);
			case 'create_blogfeed':
//				return $this->canCreateBlogfeed($e);
			case 'create_campaign':
//				return $this->canCreateCampaign($e);
			case 'create_emailer':
//				return $this->canCreateEmailer($e);
			case 'create_networklisting':
//				return $this->canCreateNetworkListing($e);
			case 'create_leadform':
//				return $this->canCreateLeadform($e);
			case 'create_reseller':
//				return $this->canCreateReseller($e);
			case 'create_slide':
//				return $this->canCreateSlide($e);
			case 'create_role':
//				return $this->canCreateRole($e);
			case 'create_keywordcampaign':
//				return $this->canCreateKeywordCampaign($e);
			case 'create_report':
//				return $this->canCreateReport($e);
			case 'create_page':
//				return $this->canCreatePage($e);
				return true;

		}
		
		return false;
	}

	public function canRemoveSubscription($e)
	{
		$this->compare_type	=	self::COMPARE_LESSTHANOREQUAL;
		foreach($this->limits as $limit => $value){
			switch($limit){
				case 'TOUCHPOINTS':
					return $this->canCreateMOBILE($e)
					&& $this->canCreateWEBSITE($e)
					&& $this->canCreateLANDING($e)
					&& $this->canCreatePROFILE($e)
					&& $this->canCreateSOCIAL($e)
					&& $this->canCreateBLOG($e);
			}
		}
	}

	public function countItems($e, $limit, $dataAccessFunction, $function, $compareType = self::COMPARE_LESSTHAN){
		$tp_limit	=	$this->getLimit($limit);
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

		$total_WEBSITE	=	$this->ada->$dataAccessFunction();

		$this->compare_type = $compareType;
		return $this->compare($total_WEBSITE, $tp_limit, $function, $e);
	}


	function canCreateCampaign($e)
	{
		$tp_limit	=	$this->getLimit('CAMPAIGNS');
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

		$totalKeywords	=	$this->ada->getTotalCreatedCampaigns();
		$under_limit	=	$totalKeywords < $tp_limit;

		$this->compare_type = self::COMPARE_LESSTHANOREQUAL;
		$this->reportLimitReached($e, $under_limit, __FUNCTION__);

		return $under_limit;
	}

	function canCreateKeyword($e)
	{
		$tp_limit	=	$this->getLimit('KEYWORDS');
		if(SubscriptionModel::UNLIMITED === $tp_limit)	return true;

		$total_WEBSITE	=	$this->ada->getTotalCreatedKeywords();

		$this->compare_type = self::COMPARE_LESSTHANOREQUAL;
		return $this->compare(intval($total_WEBSITE) + $this->verify_options['Keywords'], $tp_limit, __FUNCTION__, $e);
	}

	function canCreateBlogposts($e)
	{
		return $this->countItems($e, 'BLOGPOSTS', 'getTotalCreatedBlogposts', __FUNCTION__);
	}

	function canCreateNav($e)
	{
		return $this->countItems($e, 'NAVS', 'getTotalCreatedNavs', __FUNCTION__);
	}

	function canCreateBlogfeed($e)
	{
		return $this->countItems($e, 'BLOGFEEDS', 'getTotalCreatedBlogfeeds', __FUNCTION__);
	}

	function canCreateEmailer($e)
	{
		return $this->countItems($e, 'EMAILERS', 'getTotalCreatedEmailers', __FUNCTION__);
	}

	function canCreateNetworkListing($e)
	{
		return $this->countItems($e, 'NETWORKLISTINGS', 'getTotalCreatedNetworkListings', __FUNCTION__);
	}

	function canCreateLeadform($e)
	{
		return $this->countItems($e, 'LEADFORMS', 'getTotalCreatedLeadforms', __FUNCTION__);
	}

	function canCreateReseller($e)
	{
		return $this->countItems($e, 'RESELLERS', 'getTotalCreatedResellers', __FUNCTION__);
	}

	function canCreateSlide($e)
	{
		return $this->countItems($e, 'SLIDERS', 'getTotalCreatedSlides', __FUNCTION__);
	}

	function canCreateRole($e)
	{
		return $this->countItems($e, 'ROLES', 'getTotalCreatedRoles', __FUNCTION__);
	}

	function canCreateKeywordCampaign($e)
	{
		return $this->countItems($e, 'KEYWORDCAMPAIGNS', 'getTotalCreatedKeywordCampaigns', __FUNCTION__);
	}

	function canCreateReport($e)
	{
		return $this->countItems($e, 'REPORTS', 'getTotalCreatedReports', __FUNCTION__);
	}

	function canCreatePage($e)
	{
		return $this->countItems($e, 'PAGES', 'getTotalCreatedPages', __FUNCTION__);
	}
}
