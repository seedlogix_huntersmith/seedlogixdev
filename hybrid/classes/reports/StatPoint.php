<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of StatPoint
 *
 * @author amado
 */
class StatPoint {
#	protected $tbl	=	'hub';
#	protected $tbl_ID	=	0;
#	protected $user_ID	=	0;
#	protected $cid	=	0;
#	protected $point_type	=	'MONTH';
#	protected $point_date	=	0;
#	protected $point_value	=	0;
#	protected $ts	=	'';
	
	function getData(){
		return get_object_vars($this);
	}
	
	
	function __set($name, $value) {
		$this->$name	=	$value;
	}
}
