<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of StatReport
 *
 * @author amado
 */
abstract class StatReport {
	
	/**
	 *
	 * @var PiwikReportsDataAccess
	 */
	protected $PDA	=	NULL;
	
	function __construct(PiwikReportsDataAccess $PRDA	=	NULL) {
		if($PRDA)
			$this->setDataAccess($PRDA);
	}
	
	function setDataAccess(PiwikReportsDataAccess $PRDA)
	{
		$this->PDA	=	$PRDA;
	}
	
	function getDataAccess()
	{
		return $this->PDA;
	}
	
	
	static function getStatReport($period, PiwikReportsDataAccess $PDA)
	{
		switch($period)
		{
			case 'MONTH':
				return new MonthlyStatReport($PDA);
			case 'DAY':
				return new DailyStatReport($PDA);
		}
		
		throw new Exception('Period nt supported in ' . __FILE__);
	}
	
	//put your code here
}
