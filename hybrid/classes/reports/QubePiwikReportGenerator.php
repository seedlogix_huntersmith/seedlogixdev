<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require 'StatReport.php';
require 'StatPoint.php';
require 'DailyStatReport.php';

use Piwik\API\Request;
use Piwik\FrontController;

/**
 * Description of QubePiwikReportGenerator
 *
 * @author amado
 */
class QubePiwikReportGenerator extends QubeThreadedDataProcessor {

	protected $period	=	'day';
	protected $interval	=	7;
	protected $per_api_request		=	1;
	protected $mode	=	'';
	protected $wherestr	=	'';
	protected $user = 0;
	protected $page	=	0;
	protected $totalpages	=	0;
	protected $TotalTime	=	0;
	protected $piwik_api_domains = array(
		'analytics.6qube.com',	// 6qube2
		'analytics1.6qube.com'	// 6qube1
	);
	const API_REQUEST_FORMAT = 'json';
//	const API_REQUEST_FORMAT = 'php&serialize=1';

	/** Number of days processed per thread */
	const DAYS_PER_API = 10;

	/**
	 * @var PiwikReportsDataAccess
	 */
	protected $pda	=	NULL;
	protected $daterange;

	function showProgress($func, $timeBegin) {
		return parent::showProgress($func, $timeBegin, sprintf("%s Page: %d/%d (Elapsed: %s)\n\t\t%s",
						$func, $this->page, $this->totalpages, "%s", $this->getMem()));
	}

	function setPiwikDataAccess(PiwikReportsDataAccess $pda)
	{
		$this->pda	=	$pda;
	}

	function getPiwikDataAccess()
	{
		if(!$this->pda)
		{
			$this->setPiwikDataAccess(new PiwikReportsDataAccess($this->getQube()));
		}
		return $this->pda;
	}

	function getMem()
	{
		$s	=	sprintf("Total Memory Usage: %d M Peak: %d M", memory_get_usage()/(1024*1024),
			memory_get_peak_usage()/(1024*1024));
		return "\t\t$s\n";
	}
	/**
	 *
	 * @param PiwikReportsDataAccess $PDA
	 * @param type $options	array with options: interval, period, per_api_request
	 */
	function generateDailyStats(PiwikReportsDataAccess $PDA, $options	=	array())
	{
        $period = NULL;
        $interval = NULL;
		extract($options);
		$this->logINFO("Processing [%s] Stats for last [%d]", $period, $interval);

		$timeT	=	0;
		$leadvisitorids	=	$PDA->getLeadTrackingIdsLast24Hr();
		while($continue)
		{
			$tracking_ids	=	array();
			$trackingids_ref	=	array();
			$curbatch_lastvisited	=	array_slice($lastvisited_info, $page*$per_api_request, $per_api_request, TRUE);
			if(empty($curbatch_lastvisited))	break;//	$continue	=	false;
			foreach($curbatch_lastvisited as $visited_obj_info)
			{
				$tracking_ids[]	=	$visited_obj_info->tracking_id;
				$trackingids_ref[$visited_obj_info->tracking_id]	=	$visited_obj_info;
			}


			$timeS	=	microtime(true);
#			var_dump($curbatch_lastvisited);
            $this->logINFO("Page: %d/%d | Total Time: %01.2f | Time Per Page: %01.2f | Processing ids: %s\n\t\t%s",      $page, $totalpages, $timeT, $page ? $timeT/$page : 0, implode(',', $tracking_ids), $this->getMem());

			$this->saveSearchLeads($tracking_ids, $leadvisitorids, $trackingids_ref);

			$timeT	+=	microtime(true)-$timeS;

			$page++;
			// unset all types of shit to reduce memory usage
			unset($tracking_ids);
		}


#		$this->ArchiveSearchEngineLeads($PDA);
	}

	function saveSearchLeads($tracking_ids, $trackingids_ref, $daterange, $domain)
	{

		$segments	=	array();
		$t	=	microtime(true);
		$VisitorIds	=	$this->getPiwikDataAccess()
            ->getLeadVisitorIds($tracking_ids, $this->interval, $this->period);
        if(count($VisitorIds) == 0)
        {
            $this->logINFO("NO LEADS FOUND FOR tracking_ids: %s", implode(',', $tracking_ids));
            return;
        }

		foreach($VisitorIds as $visitorid)
		{
			$segments[]	=	'visitorId==' . $visitorid;
		}

		$dt	=	$this->getPiwikApiRequest('
											method=Referers.getSearchEngines
											&idSite=' . implode(',', $tracking_ids) . '
											&segment=' . implode(',', $segments) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
        if(count($tracking_ids) == 1)
        {
            $dt = array($tracking_ids[0] => $dt);
        }
		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());

#		$dt	=	$request->process();
		foreach($dt as $tr_id => $dates_info){
#			var_dump($dates_info);sleep(2);
			foreach($dates_info as $datestr => $date_info){
				foreach($date_info as $SE_INFO){
					$report->SaveSearchEngineLead($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);

	}

	function saveSocialVisits(array $tracking_ids, array $trackingids_ref, $daterange, $domain)
	{
		$fn	=	date('Y-m-d-').'se.log';
		$dt	=	false;
		$t	=	microtime(true);
		if(true || !file_exists($fn)){
		$dt=	$this->getPiwikApiRequest('
											method=Referers.getSocials
											&idSite=' . implode(',', $tracking_ids) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
#		file_put_contents($fn, serialize($dt));	exit;
		}
		if(count($tracking_ids) == 1)
		{
			$dt = array($tracking_ids[0] => $dt);
		}
		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());

		foreach($dt as $tr_id => $dates_info){
#			echo 'tr_id: ', $tr_id, ' ';
			foreach($dates_info as $datestr => $date_info){
#				var_dump($date_info);
				foreach($date_info as $SE_INFO){
					$report->SaveSocialVisit($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		echo "\n";
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);
	}


	function saveSocialLeads($tracking_ids, $trackingids_ref, $daterange, $domain)
	{

		$segments	=	array();
		$t	=	microtime(true);
		$VisitorIds	=	$this->getPiwikDataAccess()
            ->getLeadVisitorIds($tracking_ids, $this->interval, $this->period);
        if(count($VisitorIds) == 0)
        {
            $this->logINFO("NO LEADS FOUND FOR tracking_ids: %s", implode(',', $tracking_ids));
            return;
        }
		foreach($VisitorIds as $visitorid)
		{
			$segments[]	=	'visitorId==' . $visitorid;
		}

		$dt	=	$this->getPiwikApiRequest('
											method=Referers.getSocials
											&idSite=' . implode(',', $tracking_ids) . '
											&segment=' . implode(',', $segments) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());

        if(count($tracking_ids) == 1){
            $dt = array( $tracking_ids[0] => $dt);
#			var_dump($dt);
        }
#		$dt	=	$request->process();
		foreach($dt as $tr_id => $dates_info){
#			var_dump($dates_info);sleep(2);
			foreach($dates_info as $datestr => $date_info){
				foreach($date_info as $SE_INFO){
					$report->SaveSocialLead($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);

	}

	function saveSearchEngineVisits(array $tracking_ids, array $trackingids_ref, $daterange, $domain)
	{
		$fn	=	date('Y-m-d-').'se.log';
		$dt	=	false;
		$t	=	microtime(true);
		if(true || !file_exists($fn)){
		$dt=	$this->getPiwikApiRequest('
											method=Referers.getSearchEngines
											&idSite=' . implode(',', $tracking_ids) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
#		file_put_contents($fn, serialize($dt));	exit;
		}

		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());

		if(count($tracking_ids) == 1){
			$dt = array( $tracking_ids[0] => $dt);
#			var_dump($dt);
		}
		foreach($dt as $tr_id => $dates_info){
#			echo 'tr_id: ', $tr_id, ' ';
			foreach($dates_info as $datestr => $date_info){
#				var_dump($date_info);
				foreach($date_info as $SE_INFO){

					$report->SaveSearchEngineVisit($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		echo "\n";
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);
	}

	function saveReferralVisits(array $tracking_ids, array $trackingids_ref, $daterange, $domain)
	{
		$fn	=	date('Y-m-d-').'se.log';
		$dt	=	false;
		$t	=	microtime(true);
		if(true || !file_exists($fn)){
		$dt=	$this->getPiwikApiRequest('
											method=Referers.getWebsites
											&idSite=' . implode(',', $tracking_ids) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
#		$dt	=	$request->process();
#		file_put_contents($fn, serialize($dt));	exit;
		}

		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());
		if(count($tracking_ids) == 1)
		{
			$dt = array($tracking_ids[0] => $dt);
		}

#		var_dump($dt);exit;
		foreach($dt as $tr_id => $dates_info){
			foreach($dates_info as $datestr => $date_info){
#				var_dump($date_info);
				foreach($date_info as $SE_INFO){
					$report->SaveReferralVisits($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		unset($dt);

		$this->showProgress(__FUNCTION__, $t);
	}


	function saveReferrerKeywords($tracking_ids, $trackingids_ref, $daterange, $domain)
	{
		$t	=	microtime(true);
		$dt=	$this->getPiwikApiRequest('
											method=Referers.getKeywords
											&idSite=' . implode(',', $tracking_ids) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
		/** results example:
		 * {
  "732": {
    "2014-12-13": [],
    "2014-12-14": [],
    "2014-12-15": [
      {
        "label": "seo website",
        "nb_uniq_visitors": 1,
        "nb_visits": 1,
        "nb_actions": 9,
        "nb_users": 0,
        "max_actions": 9,
        "sum_visit_length": 91,
        "bounce_count": 0,
        "nb_visits_converted": 0,
        "idsubdatatable": 11440
      }
    ],
    "2014-12-16": [],
    "2014-12-17": [],
    "2014-12-18": []
  } }
		 */
		$pda	=	$this->getPiwikDataAccess();
//		$report	= StatReport::getStatReport(strtoupper($this->period), );

		if(count($tracking_ids) == 1)
		{
			$dt	=	array($tracking_ids[0] => $dt);
		}

		foreach($dt as $tr_id => $dates_info){
			foreach($dates_info as $datestr => $date_keywords){
				if(empty($date_keywords))	continue;	// some dates may not have keywords info
				foreach($date_keywords as $keyword_info){
					$point	=	array(
							'tbl' =>  $trackingids_ref[$tr_id]->touchpoint_type,
							'tbl_ID'	=>	 $trackingids_ref[$tr_id]->id,
							'user_ID'	=>	 $trackingids_ref[$tr_id]->user_id,
							'cid'	=>	 $trackingids_ref[$tr_id]->cid,
//							'point_type' =>  'DAY',
							'point_date'	=> $datestr,
							'label'	=> $keyword_info['label'],	// the search keyword
							'nb_visits'	=>	$keyword_info['nb_visits'],
							'nb_uniq_visitors'	=>	$keyword_info['nb_uniq_visitors']
					);
					$pda->SaveReferralKeyword($point);
				}
			}
		}

		$this->showProgress(__FUNCTION__, $t);
	}

	function saveReferralLeads($tracking_ids, $trackingids_ref, $daterange, $domain)
	{

		$segments	=	array();
		$t	=	microtime(true);
		$VisitorIds	=	$this->getPiwikDataAccess()
            ->getLeadVisitorIds($tracking_ids, $this->interval, $this->period);

        if(count($VisitorIds) == 0)
        {
            $this->logINFO("NO LEADS FOUND FOR tracking_ids: %s", implode(',', $tracking_ids));
            return;
        }
		foreach($VisitorIds as $visitorid)
		{
			$segments[]	=	'visitorId==' . $visitorid;
		}

		$dt	=	$this->getPiwikApiRequest('
											method=Referers.getWebsites
											&idSite=' . implode(',', $tracking_ids) . '
											&segment=' . implode(',', $segments) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);
		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());
		if(count($tracking_ids) == 1){
			$dt = array($tracking_ids[0] => $dt);
		}
#		$dt	=	$request->process();
		foreach($dt as $tr_id => $dates_info){
			if(!isset($trackingids_ref[$tr_id]))
			{
				$this->logERROR('tracking id not found: %s %s', $tr_id, print_r($dt, true));
				throw new QubeException('Tracking Id Not Found ' . $tr_id);
			}
#			var_dump($dates_info);sleep(2);
			foreach($dates_info as $datestr => $date_info){
				foreach($date_info as $SE_INFO){
					$report->SaveReferralLead($datestr, (object)$SE_INFO, $trackingids_ref[$tr_id]);
				}
			}
		}
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);

	}

	/**
	 * This must always be called before saveleads stats.
	 *
	 * @param array $tracking_ids
	 * @param array $trackingids_ref
	 * @param $daterange
	 * @param $domain
	 * @throws Exception
	 * @throws QubeException
	 */
	function saveVisitsStats(array $tracking_ids, array $trackingids_ref, $daterange, $domain)
	{
		$t	=	microtime(true);
		$piwikdts = $this->getPiwikApiRequest('
											method=VisitsSummary.get
											&idSite=' . implode(',', $tracking_ids) . '
											&date=' . $daterange . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT, $domain);

		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());
#		$report->SavePiwikStats($piwikdts, $trackingids_ref);

		if(isset($piwikdts['result']) && $piwikdts['result']	==	'error')
			die("Piwik Error: " . $piwikdts['message']);
#			var_dump($piwikdts);

		if(count($tracking_ids) == 1)
		{
			$piwikdts	=	array($tracking_ids[0] => $piwikdts);
		}
		foreach($piwikdts as $trackingid => $dates_info)
		{
	foreach($dates_info as $datestr => $site_data)
		{
#			$this->SaveDailyStat($dayrow->getColumns(), $obj_info);

#			foreach($date_info as $SE_INFO){
		// Empty site_data means there's no info for that date
		$visits = empty($site_data) ? (object)array('nb_visits' => 0, 'bounce_count' => 0) : (object)$site_data;
				$report->SaveDailyStat($datestr, $visits, $trackingids_ref[$trackingid]);
#			}
		}
		}
		unset($piwikdts);

		$this->showProgress(__FUNCTION__, $t);
	}


	/**
	 * This requires taht saveLeadsStats is called first,
	 * after saveleadsstats is called, this function does an update with the nb_leads values.
	 *
	 * @throws Exception
	 */
	function saveLeadsStats()//$tracking_ids, $trackingids_ref)
	{
        $date = date('Y-m-d');
		$q	=	"update piwikreports r set r.nb_leads = (select count(*) from leads l where l.hub_id = r.tbl_ID and date(l.created) = r.point_date) where r.tbl = 'hub'
			AND r.point_date > DATE_SUB('$date', INTERVAL $this->interval $this->period)";

		Qube::Start()->getPDO('master')->query($q);
		return;


		$segments	=	array();
		$t	=	microtime(true);
		$VisitorIds	=	$this->getPiwikDataAccess()
						->getLeadVisitorIds($tracking_ids, $this->interval, $this->period);
		foreach($VisitorIds as $visitorid)
		{
			$segments[]	=	'visitorId==' . $visitorid;
		}
		if(empty($segments))
		{
			// callin piwik api with empty segments var returns the total # of visits! return;
			$this->logINFO("NO LEADS FOUND FOR tracking_ids: %s", implode(',', $tracking_ids));
			return;
		}

		$dt	=	$this->getPiwikApiRequest('
											method=VisitsSummary.get
											&idSite=' . implode(',', $tracking_ids) . '
											&segment=' . implode(',', $segments) . '
											&date=last' . $this->interval . '
											&period=' . $this->period . '
											&format=' . self::API_REQUEST_FORMAT);
		$report	= StatReport::getStatReport(strtoupper($this->period), $this->getPiwikDataAccess());

#		$dt	=	$request->process();

		if(count($tracking_ids) == 1)
		{
			$dt	=	array($tracking_ids[0] => $dt);
		}
		foreach($dt as $tr_id => $dates_info){
#			if(!is_array($dates_info))
#			{
#				// possibilty zero visits.
#				continue;
#			}
			foreach($dates_info as $datestr => $site_data){
//				foreach($date_info as $SE_INFO){
				$visits = empty($site_data) ? 0 : $site_data['nb_visits'];
				$report->SaveDailyLeadsStat($datestr, $visits, $trackingids_ref[$tr_id]);
//				}
			}
		}
		unset($dt);
		$this->showProgress(__FUNCTION__, $t);

	}

	static function getReportList() {
		return array(
				array('interval' => 7, 'period' => 'DAY'),
				array('interval' => 30, 'period' => 'DAY'),
				array('interval' => 6, 'period' => 'MONTH'),
				array('interval' => 12, 'period' => 'MONTH')
		);
	}

	/**
	 *
	 * @param $str
	 * @param string $domain
	 * @return stdClass
	 * @throws QubeException
	 */
	function getPiwikApiRequest($str, $domain = 'analytics.6qube.com') {

        $VERBOSE = false;
		$str	=	preg_replace('/\s*/', '', $str)	. '&token_auth=b00ce9652e919198e1c4daf7bf5aed5a';

		$url	=	"http://$domain/?module=API&$str";

		$this->logDEBUG("URL: %s\n", $url);

        $client = new Zend\Http\Client($url, array(
            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_CONNECTTIMEOUT => 0, CURLOPT_VERBOSE => $VERBOSE, CURLOPT_ENCODING => 'gzip,deflate')));

        $response = $client->send();

        if(FALSE === $response->isSuccess())
        {
            $this->logERROR('Failed HTTP request.');
            exit(QubeThreadedDataProcessor::THREAD_FAIL_FATAL);
        }

        $contents = $response->getContent();
		parse_str($str, $params);
		$params['json'] = $contents;
		$this->getPiwikDataAccess()->saveApiRequest($params);
		$result	=	 json_decode($contents, true);

		if($result === false)
		{
			$this->logERROR('PIWIK RESULTS ARE FALSE. (Invalid Json Response) ' . $url . "\n\n" . print_r($contents, true));
            sleep(5);
            exit(QubeThreadedDataProcessor::THREAD_FAIL_FATAL);
        }
		if(isset($result['result']) && $result['result'] == 'error')
		{
			throw new QubeException($result['message']);
		}
		return $result;

#		$request = new Piwik_API_Request($str);
		$request = new Request($str);
		return $request;
	}

	function generateSiteReports(VarContainer $reportConfig)
	{
		$perPage	=	20;
		$numPage	=	0;
		$Q	=	$this->getDriver()->QueryObject('WebsiteModel')
							->Where('trashed = 0 AND tracking_id != 0 AND NOT ISNULL(s.idsite)')
							->orderBy('T.id ASC')->Fields('tracking_id')
							->leftJoin('sapphire_analytics.analytics_site s', 's.idsite = T.tracking_id');

		echo $Q;
		$continue	=	true;
		while($continue)
		{
			$QS	=	$Q->Limit(($numPage * $perPage) . ', ' . $perPage )->asSubSelect()->Fields('group_concat(tracking_id)');
//			echo $QS; exit;
			$stmt	=	$QS->Exec();

			while($trackingids	=	$stmt->fetchColumn())
			{
				echo $trackingids;exit;
				$this->generateReport($reportConfig, $trackingids);
				sleep(1);
			}
		}
	}

	function generateReport(VarContainer $v, $trackingids) {
		$interval = max(1, (int) $v->interval);
		$period = strtolower(preg_match('/^(DAY|WEEK|MONTH|YEAR)$/', $v->period) ? $v->period : 'DAY');
		$request = $this->getPiwikApiRequest('
														method=VisitsSummary.getVisits
														&idSite=' . $trackingids . '
														&date=last' . $interval . '
														&period=' . $period . '
														&format=php&serialize=0');
		//														&filter_limit=10');

		$dt	=	$request->process();

		$report	=	StatReport::getStatReport($period);
		$report->SavePiwikStats($dt);
	}

	function ImportAllUsersStats()
	{
		$PDA	=	new PiwikReportsDataAccess($this->getQube());
		$users	=	$PDA->getUsersForImportStats();
		if(count($users) == 0)
		{
			$this->logINFO("No users to update. Query returned zero users. Done.");
			if(!$this->checkMode('')){
				return;
			}
		}
#var_dump($users);exit;
		$report = new DailyStatReport($this->getPiwikDataAccess());
		foreach($users as $user_info)
		{
			$this->user = $user_info->id;
			if($user_info->range1 != "")
			{
				$this->logINFO("Importing Range 1: %s for user %d", $user_info->range1, $user_info->id);
				$this->daterange = $user_info->range1;
				$call_result = $this->RunThreads();
				if($call_result == self::THREAD_EXIT) return $call_result;				
				$limits = explode(',', $user_info->range1);
				$saved_limit = $report->SaveZeroDailyStat($limits[0], $this->user);
				if($saved_limit)
					$this->logINFO('Saved Zero stat for Lower Limit Date %s for user %d', $limits[0], $this->user);
			}
			if($user_info->range2 != "")
			{
				$this->logINFO("Importing Range 2: %s for user %d", $user_info->range2, $user_info->id);
				$this->daterange = $user_info->range2;
				$call_result = $this->RunThreads();
				if($call_result == self::THREAD_EXIT) return $call_result;
				$limits = explode(',', $user_info->range2);
				$saved_limit = $report->SaveZeroDailyStat($limits[0], $this->user);
				if($saved_limit)
					$this->logINFO('Saved Zero stat for Lower Limit Date %s for user %d', $limits[0], $this->user);
			}
			$this->logINFO("ALL DONE WITH USER %d", $this->user);
#			break;
		}
		return self::MAIN_THREAD;
	}

	function RunThreads()
	{
		if($this->daterange == 'updateallusers')
		{
			// this function will call RunThreads() recursively.
			return $this->ImportAllUsersStats();
		}

		if($this->daterange){
			$dates = explode(',', $this->daterange);
			$startDate = DateTime::createFromFormat('Y-m-d', $dates[0]);
			$endDate = DateTime::createFromFormat('Y-m-d', $dates[1]);
		}else{
			$startDate = new DateTime();
			$interval = $this->interval - 1;
			$startDate->sub(new DateInterval("P{$interval}D"));
			$endDate = new DateTime();
		}

		if($this->user)
		{
			return $this->LoopDates($startDate, $endDate, 'ProcessUserTouchpoints', array('user_ID' => $this->user));
		}

		return $this->RunThreadsLatest($startDate, $endDate);
	}

	function ProcessUserTouchpoints($args)
	{
		$daterange = NULL;
		$date1 = NULL;
		$date2 = NULL;
		$user_ID = NULL;
		extract($args);

		// loop through sites
		$last_id = 0;
		$page = 0;
		while($page > -1)
		{
			// launch N threads at a time, and wait for all of them to finish, indefinitely until no more tracking ids are found
			for ($t = 0; $t < $this->num_threads; $t++) {
					// load a batch of site infos
					$this->page = $page;
					$touchpoints_info = $this->getPiwikDataAccess()->getUserTrackingIds($user_ID, $date1, $last_id, $this->per_api_request);
					if (empty($touchpoints_info)) {
						// tell while loop to stop creating threads.
						$page = -1;

						// break to wait-for-threads call
						break;
					}

					// launch thread using combination of daterange + obtained touchpoint info

					$tracking_ids = array();
					$trackingids_ref = array();

					// create index of touchpoints_info based on tracking_id
					foreach ($touchpoints_info as $visited_obj_info) {
						$tracking_ids[] = $visited_obj_info->tracking_id;
						$trackingids_ref[$visited_obj_info->tracking_id] = $visited_obj_info;
					}
					if (TRUE === $this->LaunchThread(compact('tracking_ids', 'trackingids_ref', 'daterange'))) {
						// child has run, exit out of thread runner function.
						return self::THREAD_EXIT;
					}
					$last_id = end($touchpoints_info)->touchpoint_id;
					$page++;
			}
			// detect an error waiting for the threads
			if (FALSE === $this->WaitForThreads())
				return FALSE;
		}
		return TRUE;
	}

	/**
	 * @param DateTime $startDate
	 * @param DateTime $endDate
	 * @param callable $func
	 * @param array $func_args
	 * @return int|null
	 * @throws QubeException
	 */
	protected function LoopDates(DateTime $startDate, DateTime $endDate, $func, $func_args = array())
	{
		$this->logINFO("Start: Looping Dates %s-%s", $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
		while(!$startDate->diff($endDate)->invert) {
			$date1 = $startDate->format('Y-m-d');
			$interval = self::DAYS_PER_API - 1;

			$startDate->add(new DateInterval("P{$interval}D"))->format('Y-m-d');
			$date2 = $startDate->diff($endDate)->invert ? $endDate->format('Y-m-d') : $startDate->format('Y-m-d');

			$daterange = "$date1,$date2";
			$this->logINFO("Iteration: Dates %s", $daterange);

			$func_args['daterange']	=	$daterange;
			$func_args['date1'] = $date1;
			$func_args['date2']	=	$date2;

			$CALL_RESULT = call_user_func_array(array($this, $func), array($func_args));

			if(FALSE === $CALL_RESULT)
				throw new QubeException("Failed");
			elseif(self::THREAD_EXIT === $CALL_RESULT)
			{
				return self::THREAD_EXIT;
			}
			$startDate->add(new DateInterval('P1D'));
		}
		return NULL;
	}

	function ProcessLatestVisits($args)
	{
		$daterange = NULL;
		$lastvisited_info = NULL;
		extract($args);

		$page = 0;
		while ($page != -1) {
			for ($t = 0; $t < $this->num_threads; $t++) {
				if(!$this->canCreateChild()){
					if($this->WaitForThread() === FALSE){
						return false;
					}
				}
				$tracking_ids = array();
				$trackingids_ref = array();
				$curbatch_lastvisited = array_slice($lastvisited_info, $page * $this->per_api_request, $this->per_api_request, TRUE);

				if (empty($curbatch_lastvisited)) {
					// all  done!
					$page = -1;
					break;
				}
				foreach ($curbatch_lastvisited as $visited_obj_info) {
					$tracking_ids[] = $visited_obj_info->tracking_id;
					$trackingids_ref[$visited_obj_info->tracking_id] = $visited_obj_info;
				}
				if (TRUE === $this->LaunchThread(compact('tracking_ids', 'trackingids_ref', 'daterange'))) {
					// child has run, exit out of thread runner function.
					return self::THREAD_EXIT;
				}

				$page++;
			}

//			if (FALSE === $this->WaitForThreads())
//				return FALSE;

//			$this->logINFO("All Threads finished for %s", $daterange);
		}

		if (FALSE === $this->WaitForThreads())
			return FALSE;

		return TRUE;
	}

    /**
     * Runs stats for sites visited within a specified daterange.
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @return int|void
     * @throws QubeException
     */
	function RunThreadsLatest(DateTime $startDate, DateTime $endDate)
	{
		$lastvisited_info	=	$this->getPiwikDataAccess()
			->getVisitedHubs($this->wherestr, $this->interval . ' ' . $this->period);

		$totalhubsvisited	=	count($lastvisited_info);
		$this->totalpages	=	ceil($totalhubsvisited/$this->per_api_request);
		$this->logINFO("Found [%d] Hubs visited in the last %d %s", $totalhubsvisited, $this->interval, $this->period);

		if(count($lastvisited_info) == 0)
		{
			$this->logINFO("Nothing to do.");
				return;
		}

		$call_result = $this->LoopDates($startDate, $endDate, 'ProcessLatestVisits', compact('lastvisited_info'));

		if(self::THREAD_EXIT === $call_result)
			return self::THREAD_EXIT;

        if($this->checkMode('LeadsStats') && $this->getThreadID() == self::MAIN_THREAD)
        {
            $this->logINFO("Updating stats for existing visits.");
            $this->saveLeadsStats();
            if(!$this->checkMode('')){
                return;
            }
        }
	}

	function checkMode($runmode)
	{
		return ($this->mode == '' || $this->mode == $runmode);
	}

	/**
	 * Run Stats based on Mode.
	 *
	 * @param $trackingids_ref
	 * @param $tracking_ids
	 * @param $daterange
	 * @param $domain string
	 * @throws QubeException
	 */
    function RunStats($trackingids_ref, $tracking_ids, $daterange, $domain)
    {
        if($this->checkMode('ReferrerKeywords'))
            $this->saveReferrerKeywords($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('VisitsStats'))// || $this->checkMode('LeadsStats'))
            $this->saveVisitsStats($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('SearchEngineVisits'))
            $this->saveSearchEngineVisits($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('SearchLeads'))
            $this->saveSearchLeads($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('ReferralVisits'))
            $this->saveReferralVisits($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('ReferralLeads'))
            $this->saveReferralLeads($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('SocialVisits'))
            $this->saveSocialVisits($tracking_ids, $trackingids_ref, $daterange, $domain);

        if($this->checkMode('SocialLeads'))
            $this->saveSocialLeads($tracking_ids, $trackingids_ref, $daterange, $domain);

//        if($this->checkMode('BounceRate'));
    }

	function RunChildThread($data)
	{
        $trackingids_ref = NULL;
        $tracking_ids = NULL;
		$daterange = null;
		$domain = $this->getPiwikApiDomain();
		extract($data);

		$this->TotalTime = 0;
		$this->logINFO("Thread ". $this->getThreadID() . ": $domain");
        $this->RunStats($trackingids_ref, $tracking_ids, $daterange, $domain);
	}

	function setOptions(\VarContainer $vars) {
		if(!$vars->isEmpty('perapi')) $this->per_api_request	=	$vars->perapi;
		if(!$vars->isEmpty('interval')) 	$this->interval	=	$vars->interval;
		if(!$vars->isEmpty('numthreads')) 	$this->num_threads	=	$vars->numthreads;
		if(!$vars->isEmpty('mode')) 	$this->mode	=	$vars->mode;
		if(!$vars->isEmpty('period')) 	$this->period	=	$vars->period;
		if(!$vars->isEmpty('where'))	$this->wherestr	=	$vars->where;
		if(!$vars->isEmpty('daterange'))	$this->daterange	=	$vars->daterange;
		if(!$vars->isEmpty('user') && $this->daterange != 'updateallusers')
				$this->user = $vars->user;

		if($vars->isEmpty('confirm')){
		$this->logINFO('Available Archiving Modes are: %s',
			'ReferrerKeywords VisitsStats LeadsStats SearchEngineVisits SearchLeads ReferralVisits ' .
			' ReferralLeads SocialVisits SocialLeads');
		}
		$this->logINFO("Running %d threads with %d sites per thread for %d %s (MODE: %s)",
			$this->num_threads, $this->per_api_request, $this->interval, $this->period,
			$this->mode == '' ? 'ALL' : $this->mode
		);

		return true;
	}

	public function getPiwikApiDomain(){
		$threadID = $this->getThreadID();
		$n_domains = count($this->piwik_api_domains);
		return $this->piwik_api_domains[$threadID % $n_domains];
	}

	private function launchPiwikThread($lastvisited_info, &$page, $daterange)
	{
		$tracking_ids = array();
		$trackingids_ref = array();
		$curbatch_lastvisited = array_slice($lastvisited_info, $page * $this->per_api_request, $this->per_api_request, TRUE);

		if (empty($curbatch_lastvisited)) {
			return false;
		}

		foreach ($curbatch_lastvisited as $visited_obj_info) {
			$tracking_ids[] = $visited_obj_info->tracking_id;
			$trackingids_ref[$visited_obj_info->tracking_id] = $visited_obj_info;
		}
		if (TRUE === $this->LaunchThread(compact('tracking_ids', 'trackingids_ref', 'daterange'))) {
			// child has run, exit out of thread runner function.
			return self::THREAD_EXIT;
		}

		$page++;

		return true;
	}

}
