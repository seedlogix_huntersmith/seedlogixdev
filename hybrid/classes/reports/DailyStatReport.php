<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Not an actual report. Repressents a value for a point.
 *
 * @author amado
 */
class DailyStatReport extends StatReport
{
	/**
	 * if stats do not exist for a given date, save a zero value for that date.
	 * returns true for saved new stat. false for exists.

	 * @param $datestr
	 * @param $user_id
	 * @return bool
	 */
	function SaveZeroDailyStat($datestr, $user_id)
	{
		if($this->getDataAccess()->hasDailyStat($datestr, $user_id) == TRUE)
			return false;

		$piwik_data_info = (object) array(
			'nb_visits' => 0,
			'bounce_count' => 0
		);

		$this->SaveDailyStat($datestr, $piwik_data_info, (object)array(
			'touchpoint_type' => 'hub',
			'id' => 0,
			'user_id' => $user_id,
			'cid' => 0
		));
		return TRUE;
	}

	function SaveDailyStat($datestr, $piwik_data_info, $obj_info)
	{
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_data_info->nb_visits;
		$SP->bounce_count = $piwik_data_info->bounce_count;
		
		$this->getDataAccess()->SaveVisitsStat($SP);
	}
	
	function SaveSearchEngineVisit($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->searchengine	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveSearchEngineVisitPoint($SP);
	}
	
	function SaveDailyLeadsStat($datestr, $date_visits, $obj_info)
	{
#		echo 'saving date ', $label, ' => ', $value, "\n";
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$date_visits;	//$piwik_keyword_info->nb_visits;
		
		$this->getDataAccess()->SaveLeadsStat($SP);
	}
	
	function SaveSearchEngineLead($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->searchengine	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveSearchEngineLeadPoint($SP);
	}
	
	function SaveReferralVisits($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->referrer	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveReferralVisitPoint($SP);
	}
	
	function SaveSocialVisit($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->referrer	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveSocialVisitPoint($SP);
	}
	
	function SaveSocialLead($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->referrer	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveSocialLeadPoint($SP);
	}
	
	function SaveReferralLead($datestr, $piwik_keyword_info, $obj_info)
	{
#		var_dump($obj_info);
		$SP	= new StatPoint();
		$SP->tbl	=	$obj_info->touchpoint_type;
		$SP->tbl_ID	=	$obj_info->id;
		$SP->user_ID	=	$obj_info->user_id;
		$SP->cid	=	$obj_info->cid;
		$SP->point_type	=	'DAY';
		$SP->point_date	=	$datestr;
		$SP->point_value	=	$piwik_keyword_info->nb_visits;
		$SP->referrer	=	$piwik_keyword_info->label;
		$SP->bounce_count = $piwik_keyword_info->bounce_count;
#		var_dump($obj_info, $piwik_keyword_info);
		$this->getDataAccess()->SaveReferralLeadPoint($SP);
	}
	
	function SaveSiteDTResults($obj_info, $dt)
	{
	}
	
	
	function SaveLeadStats($datestr, $piwik_keyword_info, $obj_info)
	{
#return;
#		var_dump($dt);exit;
#		$merged	=	$dt->mergeChildren();
#		$rows	=	$merged->getRows();
#		foreach($rows as $dayrow)
	foreach($dt as $date => $numvisits)
		{
#			$this->SaveDailyStat($dayrow->getColumns(), $obj_info);

			$this->SaveDailyLeadsStat(array($date, $numvisits), $obj_info);

		}
	}
}
