<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of DataTableResult
 *
 * @author amado
 */
class DataTableResult implements ExportableTable {
    //put your code here
    
    /**
     *
     * @var DBSelectQuery
     */
    protected $_query;
    /**
     *
     * @var VarContainer
     */
    protected $_req;
    
    /**
     *
     * @var ThemeView
     */
    protected $_rowCompiler;
    
    /**
     *
     * @var QubeLogPDOStatement
     */
    protected $_result;

    /**
     * @var string
     */
    protected $_rowVar;
            
    function __construct(DBSelectQuery $q = null, VarContainer $req) {
        $this->_query = $q;
        $this->_req =   $req;
    }
    
    function setRowCompiler(ThemeView $View, $rowVar){
        $this->_rowCompiler =   $View;
        $this->_rowVar  =   $rowVar;
    }
    
		function getPlaceHolders()
		{
				return NULL;
		}

	/**
	 * @param callable $function
	 * @param $placeholders
	 * @param ThemeView $compiler
	 * @param $rowName
	 * @param null $fetch_class
	 * @return array
	 */
	static function returnDataTableStructure($function, $placeholders, ThemeView $compiler, $rowName, $fetch_class = NULL)
	{
		$dataStructure	=	array();
		// 1 - getTotalRecords (without filter)
		$placeholders1	=	self::deep_copy_array($placeholders);
		$placeholders1['getStatement'] = true;
		$placeholders1['cols']	= 'count(*)';
		unset($placeholders1['sSearch']);
		unset($placeholders1['limit_start']);
		unset($placeholders1['limit']);
//		$placeholders['getStatement']	=	true;
//		$placeholders['cols']	=	'count(*)';

		$dataStructure['iTotalRecords']	=	call_user_func($function, $placeholders1)->fetchColumn();

		$placeholders['calc_found_rows']	=	true;
		$placeholders['getStatement']	=	true;
//      $configTable = $placeholders['configTable'];
//      unset($placeholders['configTable']);
//      $placeholders = $configTable ? array_merge($configTable, $placeholders) : $placeholders;
		/** @var QubeLogPDOStatement $stmt */
		$stmt	=	call_user_func($function, $placeholders);

		$dataStructure['iTotalDisplayRecords']	=	Qube::GetPDO()->query('SELECT FOUND_ROWS()')->fetchColumn();

		$dataStructure['aaData']		= self::CompileResults($stmt, $compiler, $rowName, $fetch_class);

		return $dataStructure;
	}

	static function CompileRow(ThemeView $compiler, $row, $rowVarName = 'row')
	{
		$html   =   $compiler->init($rowVarName, $row)->toString();
		if(preg_match_all('/<td[^>]*?>([\s\S]*?)<\/td>/', $html, $matches)){
				return $matches[1];
		}
		return $html;
	}

    static function CompileResults(PDOStatement $stmt, ThemeView $compiler, $rowName = 'row', $fetch_class = 'stdClass')
    {
        $arr    =   array();
        while($rowObj	=	($fetch_class ? $stmt->fetchObject($fetch_class) : $stmt->fetch()))
        {
            $arr[]	=	self::CompileRow($compiler, $rowObj, $rowName);
        }
        return $arr;
    }
		
    function getData($args = NULL, $fetchMode = NULL){
			
        $this->Sort();
        $this->modifyQuery();
			
        $this->_result      =   $this->getPreparedStatement();
        $this->_result->execute($args);

        if(!$this->_rowCompiler || $fetchMode)
            return $this->_result->fetchAll($fetchMode ? $fetchMode : PDO::FETCH_NUM);
        
        $data   =   array();
        while($row  =   $this->_result->fetch()){
					$data[]	=	self::CompileRow($this->_rowCompiler, $row, $this->_rowVar);
//            echo $html;
        }
        
        return $data;
    }
    
    function modifyQuery(){
        $C  =   $this->_req;
        
        if($C->checkValue('iDisplayStart', $start)){
            $length =   min(100, $C->iDisplayLength);
            $this->_query->Limit($start, $length);
        }
        
    }
    
		/**
		 * Returns the total # of records without $filter
		 * 
		 * @param array $args
		 * @return int
		 */
    function getTotalRecords($args = array()){
        $totalQ =   clone $this->_query;
        $qiTotalRec      =   $totalQ->calc_found_rows(false)->Limit(null)
            ->Fields('count(*)')->Prepare();
        $qiTotalRec->execute($args);
        
        
        return $qiTotalRec->fetchColumn();
    }
		
		function getColumnTitles()
		{
			return array_keys(self::getTableColumns($this->_req->get('table')));
		}

	function Export($fhandle, array $args = NULL)
	{
		$data = $this->getData($args ? $args : $this->getPlaceHolders(), PDO::FETCH_ASSOC);
		$headers = self::getTableColumns($this->_req->get('table'));
		$headers = array_filter($headers, function($item){return $item['Exportable'] !== false;});
		foreach($data as $row){
			$row = is_object($row) ? (array) $row : $row;
			$result = array_flip(array_map(function($item){return $item['Name'];}, $headers));
			foreach($result as $key => &$value){
				$value = $row[$key];
			}
			fputcsv($fhandle, $result);
		}
	}
    
    function getArray($args =   array()){
				$args	=	$args ? $args : $this->getPlaceHolders();
        $iTotalRec  =   $this->getTotalRecords($args);
//
//        $this->Sort();
//
//        $this->modifyQuery();
        $data = $this->getData($args);
        $iTotalDisplayRecords = $this->getTotalDisplayRecords();
        $out = array(
            "sEcho" => intval($this->_req->sEcho),
            "iTotalRecords" => $iTotalRec,
            "iTotalDisplayRecords" => $iTotalDisplayRecords,
            "aaData" => $data
	    );
        return $out;
    }

    protected function Sort(){
        if($this->_req->checkValue('iSortCol_0', $index)) {
            if (!$this->_req->checkValue('sColumns', $columns)) {
                throw new QubeException("Columns not found.");
            }
//            $column = call_user_func(array($model, 'getDataTableColumn'), $index);
            $columns = explode(',', $columns);
            $columnsDB = self::getTableColumns($this->_req->get('table'));
            $columnInfo = $columnsDB[$columns[$index]];
        } else{
            $columnInfo = self::getDefaultColumn($this->_req->getValue('table'));
        }

        if($columnInfo && $columnInfo['Sortable'] === TRUE){
			$newRow = $this->_req->checkValue('newRowsIds', $new_rows_ids) ? 'id = ' . join(' DESC, j.id = ', $new_rows_ids) . ' DESC, ' : '';
            $this->OrderBy("$newRow{$columnInfo['Name']}", $this->_req->checkValue('sSortDir_0', $dir) ? $dir : $columnInfo['Sort']);
        }

    }

    protected function OrderBy($column, $dir = 'asc'){
        $this->_query->orderBy("T.$column $dir");
    }
    
    protected function getPreparedStatement(){
        return $this->_query->Prepare();
    }
		
		static function getRequestArgs(VarContainer $C)
		{
			$titles	=	self::getTableColumnTitles($C->table);
			$columndata	=	self::getTableColumns($C->table);
            $default = self::getDefaultColumn($C->table);
			$requestArgs = array(
				'limit_start' => max(0, (int)$C->getValue('iDisplayStart', 0)),
				'limit' => min(100, (int)$C->getValue('iDisplayLength', 0)),
				'sSearch' => $C->getValue('sSearch', ''),
				'orderCol' => ($C->getValue('iSortCol_0', -1) == -1) ? $default['Name'] : $columndata[$titles[(int)$C->getValue('iSortCol_0', 0)]]['Name'],
				'orderDir' => ($C->getValue('sSortDir_0', $default['Sort']))
			);
			if($C->checkValue('newRowsIds', $newRow)) $requestArgs['newRowsIds'] = $newRow;
			return $requestArgs;
		}
		
		static function getTableColumnTitles($table)
		{
			$cols	=	self::getTableColumns($table);
			return	array_keys($cols);
		}

    static function getTableColumns($table){
        switch($table){
			case 'ReportSocialMedia':
				$columns = array(
					'Platform' => array('Name' => 'network', 'Sortable' => true),
					'Visits' => array('Name' => 'visits', 'Sortable' => true),
					'Leads' => array('Name' => 'leads', 'Sortable' => true),
					'Conversion' => array('Name' => 'rate', 'Sortable' => true),
					'Bounce' => array('Name' => 'bounce_rate', 'Sortable' => true)
				);
				break;
			case 'ReportReferrals':
				$columns = array(
					'Domain' => array('Name' => 'referrer', 'Sortable' => true),
					'Visits' => array('Name' => 'visits', 'Sortable' => true),
					'Leads' => array('Name' => 'leads', 'Sortable' => true),
					'Conversion' => array('Name' => 'rate', 'Sortable' => true),
					'Bounce' => array('Name' => 'bounce_rate', 'Sortable' => true)
				);
				break;
			case 'ReportOrganic':
				$columns = array(
					'Search Engine' => array('Name' => 'searchengine', 'Sortable' => true),
					'Visits' => array('Name' => 'visits', 'Sortable' => true),
					'Leads' => array('Name' => 'leads', 'Sortable' => true),
					'Conversion' => array('Name' => 'rate', 'Sortable' => true),
					'Bounce' => array('Name' => 'bounce_rate', 'Sortable' => true)
				);
				break;
            case 'CronEvents':
                $columns = array(
                    'Timetamp' => array('Name' => '', 'Sortable' => false),
                    'Event ID' => array('Name' => '', 'Sortable' => false),
                    'User' => array('Name' => '', 'Sortable' => false),
                    'Object' => array('Name' => '' , 'Sortable' => false),
                    'Event' => array('Name' => '' , 'Sortable' => false),
                    'Value' => array('Name' => '' , 'Sortable' => false),
                    'Data' => array('Name' => '' , 'Sortable' => false),
                    'Context' => array('Name' => '' , 'Sortable' => false)

                );
                break;
            case 'Resellers':
                $columns = array(
                    'ID' => array('Name' => 'T.id', 'Sortable' => true),
                    'Company' => array('Name' => 'T.company', 'Sortable' => true),
                    'Full Name' => array('Name' => 'CONCAT(i.firstname, " ", i.lastname)', 'Sortable' => true),
                    'Support Email' => array('Name' => 'T.support_email' , 'Sortable' => true),
                    'Created' => array('Name' => 'T.created' , 'Sortable' => true, 'Sort' => 'asc'),
                    'Last Login' => array('Name' => 'u.last_login' , 'Sortable' => true)
                );
                break;
            case 'Networks':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Hostname' => array('Name' => 'httphostkey', 'Sortable' => true), 
                    'Type' => array('Name' => 'type', 'Sortable' => true),
                    'Created' => array('Name' => 'created' , 'Sortable' => true, 'Sort' => 'asc'));
                break;
			case 'blog-posts':
				$columns = array(
					'Post Title' => array('Name' => 'post_title', 'Sortable' => true, 'Sort' => 'asc'),
					'Category' => array('Name' => 'category', 'Sortable' => true),
					'Blog Feed' => array('Name' => '', 'Sortable' => false),
					'Actions' => array('Name' => '', 'Sortable' => false),
				);
				break;
            case 'masterposts':
                $columns = array(
                    'Post Title' => array('Name' => 'post_title', 'Sortable' => true, 'Sort' => 'asc'),
                    'Category' => array('Name' => 'category', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            case 'Prospects':
            case 'ProspectsCampaign':
            case 'ProspectsDashboardCampaign':
                $columns = array(
                    'Grade' => array('Name' => 'grade', 'Sortable' => true),
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Email' => array('Name' => 'email', 'Sortable' => true),
                    'Phone' => array('Name' => 'p.phone', 'Sortable' => true),
					'Source' => array('Name' => 'p.source', 'Sortable' => true),
                    'Touchpoint Type' => array('Name' => 'TOUCHPOINT_TYPE', 'Sortable' => true), //ProspectsDashboardCampaign
                    'Touchpoint Name' => array('Name' => 'tp.name', 'Sortable' => true), //ProspectsDashboardCampaign
                    'Touchpoint Page' => array('Name' => 'p2.page_title', 'Sortable' => true), //ProspectsDashboardCampaign
                    'Campaign Form' => array('Name' => 'f.name', 'Sortable' => true), //ProspectsCampaign
                    'Campaign' => array('Name' => 'c.name', 'Sortable' => true), //ProspectsDahsboardCampaign
                    'Date' => array('Name' => 'p.created', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false));
                break;
            case 'Campaigns':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Touchpoints' => array('Name' => 'num_touchpoints', 'Sortable' => true, 'Sort' => 'desc'),
                    'Visit Changes' => array('Name' => 'visitschangepct', 'Sortable' => true),
                    'Total Visits' => array('Name' => 'visitsnow', 'Sortable' => true),
                    'Total Leads' => array('Name' => 'num_leads', 'Sortable' => true),
                    'Conversion' => array('Name' => 'conversionspct', 'Sortable' => true),
                    'Creation Date' => array('Name' => 'created_str', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false, 'Exportable' => false)
                );
                break;
			case 'TopPerforming':
            case 'Websites':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Visits Changes' => array('Name' => 'visits_changes', 'Sortable' => true),
                    'Total Visits' => array('Name' => 'total_visits', 'Sortable' => true, 'Sort' => 'desc'),
                    'Total Leads' => array('Name' => 'total_leads', 'Sortable' => true),
                    'Conversion' => array('Name' => 'ifnull(total_leads / total_visits * 100, 0)', 'Sortable' => true),
                    'Touchpoint Type' => array('Name' => 'touchpoint_type', 'Sortable' => true),
                    'Website Creation Date' => array('Name' => 'created', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false, 'Exportable' => false)
                );
                break;
            case 'Contacts':
                $columns = array(
                    'First Name' => array('Name' => 'first_name', 'Sortable' => true, 'Sort' => 'asc'),
                    'Last Name' => array('Name' => 'last_name', 'Sortable' => true),
                    'Email' => array('Name' => 'email', 'Sortable' => true),
                    'Phone' => array('Name' => 'phone', 'Sortable' => true),
					'Last Touch' => array('Name' => 'last_updated', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
            case 'LeadForms':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true, 'Sort' => 'asc'),
                    'Prospects' => array('Name' => 'prospects_count', 'Sortable' => true),
                    'Auto Responders' => array('Name' => 'responders_count', 'Sortable' => true),
                    'Date' => array('Name' => 'created', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            case 'NetworksCampaign':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Type' => array('Name' => 'tbl', 'Sortable' => true),
                    'Date' => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'asc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            case 'EmailMarketing':
                $columns = array(
                    'Name' => array('Name' => 'u.name', 'Sortable' => true, 'Sort' => 'asc'),
                    'Type' => array('Name' => 'h.touchpoint_type', 'Sortable' => true),
                    'Context' => array('Name' => 'responder_context', 'Sortable' => true),
                    'Time Delay' => array('Name' => 'u.sched', 'Sortable' => true),
                    'Enabled' => array('Name' => 'u.active', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            case 'Userlist':
                $columns = array(
                    'ID' => array('Name' => 'T.id', 'Sortable' => true),
                    'Full Name' => array('Name' => 'CONCAT(i.firstname, " ", i.lastname)', 'Sortable' => true),
                    'Username' => array('Name' => 'T.`username`', 'Sortable' => true),
                    'Role' => array('Name' => 'r.role_id', 'Sortable' => true),
                    'Created Date' => array('Name' => 'T.`created`', 'Sortable' => true, 'Sort' => 'desc'),
                    'Last Login Date' => array('Name' => 'T.`last_login`', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            case 'Events':
                $columns = array(
                    'Timestamp' => array('Name' => 'T.ts', 'Sortable' => true, 'Sort' => 'asc'),
                    'ID' => array('Name' => 'T.id', 'Sortable' => true),
                    'User' => array('Name' => 'coalesce(U.username, "System")', 'Sortable' => true),
                    'Object' => array('Name' => 'T.Object', 'Sortable' => true),
                    'Event' => array('Name' => 'T.Event', 'Sortable' => true),
                    'Value' => array('Name' => 'T.Value', 'Sortable' => true),
                    'Context' => array('Name' => 'T.Context', 'Sortable' => true)
                );
                break;
            case 'BlogFeeds':
                $columns = array(
                    'Blog MU Feed Name' => array('Name' => 'name', 'Sortable' => true),
                    'Created' => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'asc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
			case 'Multi-Sites':
				$columns = array(
					'Touchpoint Type' => array('Name' => 'touchpoint_type', 'Sortable' => true),
                    'Name' => array('Name' => 'name', 'Sortable' => true),
					'Created Date' => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'asc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
				break;
            case 'RolesSystem':
                $columns = array(
                    'ID' => array('Name' => '', 'Sortable' => false),
                    'Name' => array('Name' => '', 'Sortable' => false, 'Sort' => 'asc'),
                    'Users' => array('Name' => 'role_users', 'Sortable' => false),
                    'Company' => array('Name' => '', 'Sortable' => false),
                    'Permissions' => array('Name' => 'flag', 'Sortable' => false),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
							break;
            case 'Roles':
                $columns = array(
                    'Name' => array('Name' => '', 'Sortable' => false, 'Sort' => 'asc'),
                    'Permissions' => array('Name' => 'flag', 'Sortable' => false),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
			case 'AccountLimits':
				$columns = array(
					'ID' => array('Name' => 'l.id', 'Sortable' => true),
                    'Name' => array('Name' => 'subscriptions.name', 'Sortable' => true),
					'Type' => array('Name' => 'l.type', 'Sortable' => true),
					'User' => array('Name' => 'users.username', 'Sortable' => true),
					'User ID' => array('Name' => 'l.user_ID', 'Sortable' => true),
                    'Subscription Expiration' => array('Name' => 'l.expiration_date', 'Sortable' => true, 'Sort' => 'asc'),
					'Timestamp' => array('Name' => 'l.ts', 'Sortable' => true),
					'Actions' => array('Name' => '', 'Sortable' => false)
				);
				break;
            case 'RankingSites':
                $columns = array(
                    'Website URL' => array('Name' => 'hostname', 'Sortable' => true),
                    'Rankings Change' => array('Name' => '', 'Sortable' => false, 'Exportable' => false), //@todo Calculate ranking change per site report
                    'Total Keywords' => array('Name' => 'keywords', 'Sortable' => false),
                    '1st Page Google' => array('Name' => 'google_first', 'Sortable' => false),
                    '1st Page Bing' => array('Name' => 'bing_first', 'Sortable' => false),
                    'Created' => array('Name' => 'created', 'Sortable' => false, 'Sort' => 'asc'),
                    'Actions' => array('Name' => '', 'Sortable' => false, 'Exportable' => false)
                );
                break;
            case 'CampaignReports':
                $columns = array(
                    'ID' => array('Name' => 'ID', 'Sortable' => true),
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Type' => array('Name' => 'type', 'Sortable' => true),
                    'TouchPoint' => array('Name' => 'touchpoint_type' , 'Sortable' => true),
                    'Created' => array('Name' => 'created' , 'Sortable' => true, 'Sort' => 'asc'),
                    'Actions' => array('Name' => '' , 'Sortable' => false),
                );
                break;
            case 'RankingKeywords':
                $columns = array(
                    'Keyword' => array('Name' => 'keyword', 'Sortable' => true, 'Sort' => 'asc'),
                    'Google Rank' => array('Name' => 'rank_google', 'Sortable' => true),
                    'Google Change' => array('Name' => 'google_change', 'Sortable' => true),
                    'Bing Rank' => array('Name' => 'rank_bing', 'Sortable' => true),
                    'Bing Change' => array('Name' => 'bing_change', 'Sortable' => true),
                    'Action' => array('Name' => '', 'Sortable' => false, 'Exportable' => false)
                );
                break;
            case 'Broadcast':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true, 'Sort' => 'asc'),
                    'Form Name' => array('Name' => 'f.name', 'Sortable' => true),
                    'Enabled' => array('Name' => 'active', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false),

                );
                break;
            case 'CampaignCollabs':
                $columns = array(
                    'Name' => array('Name' => 'name', 'Sortable' => true, 'Sort' => 'asc'),
                    'Email' => array('Name' => 'email', 'Sortable' => true),
                    'Scope' => array('Name' => 'scope', 'Sortable' => true),
                    'Enabled' => array('Name' => 'active', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false),
                );
                break;
			case 'Accounts':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'Proposals' => array('Name' => '', 'Sortable' => true),
                    'Contacts' => array('Name' => '', 'Sortable' => true),
					'Value' => array('Name' => '', 'Sortable' => true),
					'Last Touch' => array('Name' => 'last_updated', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'Phones':
                $columns = array(
                    'Number'  => array('Name' => 'phone_num', 'Sortable' => true),
                    'Type' => array('Name' => 'type', 'Sortable' => true),
                    'Forwarding Number' => array('Name' => 'cell', 'Sortable' => true),
					'Created Date' => array('Name' => 'date_created', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'Emails':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'Subject' => array('Name' => 'subject', 'Sortable' => true),
                    'Created' => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'CRMCampaigns':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'Leads' => array('Name' => 'leads', 'Sortable' => true),
					'Accounts' => array('Name' => 'accounts', 'Sortable' => true),
					'Contacts' => array('Name' => 'contacts', 'Sortable' => true),
					'Won' => array('Name' => 'won', 'Sortable' => true),
					'Active' => array('Name' => 'active', 'Sortable' => true),
					'Expired' => array('Name' => 'expired', 'Sortable' => true),
                    'Created' => array('Name' => 'date_created', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'Tasks':
                $columns = array(
                    'Due Date'  => array('Name' => 'due_date', 'Sortable' => true, 'Sort' => 'desc'),
					'Assigned' => array('Name' => 'assigned', 'Sortable' => true),
                    'Description' => array('Name' => 'title', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'Proposals':
                $columns = array(
                    'Expires' => array('Name' => 'expire_date', 'Sortable' => true, 'Sort' => 'desc'),
                    'Name' => array('Name' => 'name', 'Sortable' => true),
                    'Contact' => array('Name' => '', 'Sortable' => true),
					'Value (Annualized)' => array('Name' => '', 'Sortable' => true),
					'Status'  => array('Name' => 'status', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'Products':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'QTY' => array('Name' => 'qty', 'Sortable' => true),
                    'Type' => array('Name' => 'type', 'Sortable' => true),
					'Price' => array('Name' => 'price', 'Sortable' => true),
					'Discount' => array('Name' => 'discount', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'SocialAccounts':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'Network' => array('Name' => 'network', 'Sortable' => true),
                    'Authenticated' => array('Name' => '', 'Sortable' => true),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'SocialPosts':
                $columns = array(
                    'Name'  => array('Name' => 'name', 'Sortable' => true),
                    'Accounts' => array('Name' => 'accounts'),
					'Scheduled Date' => array('Name' => 'publish_date', 'Sortable' => true, 'Sort' => 'desc'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                    );
                break;
			case 'PhoneRecords':
                $columns = array(
                    'Date'  => array('Name' => 'date', 'Sortable' => true, 'Sort' => 'desc'),
                    'Caller ID' => array('Name' => 'caller_id', 'Sortable' => true),
					'Destination' => array('Name' => 'destination', 'Sortable' => false),
                    'Length' => array('Name' => 'length', 'Sortable' => true),
					'Recording' => array('Name' => 'recording', 'Sortable' => false)
                    );
                break;
			case 'InboxCalls':
                $columns = array(
                    'Date'  => array('Name' => 'date', 'Sortable' => true, 'Sort' => 'desc'),
                    'Caller ID' => array('Name' => 'display', 'Sortable' => true),
					'Destination' => array('Name' => 'destination', 'Sortable' => false),
                    'Length' => array('Name' => 'length', 'Sortable' => true),
					'Recording' => array('Name' => 'recording', 'Sortable' => false)
                    );
                break;
            case 'InboxTexts':
                $columns = array(
                    'Date'  => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'desc'),
                    'Text From' => array('Name' => 'display', 'Sortable' => true),
                    'Destination' => array('Name' => 'destination', 'Sortable' => false),
                    'Message' => array('Name' => 'message', 'Sortable' => true)
                    );
                break;
            case 'InboxVoicemails':
                $columns = array(
                    'Date'  => array('Name' => 'created', 'Sortable' => true, 'Sort' => 'desc'),
                    'Length' => array('Name' => 'length', 'Sortable' => false),
                    'Recording' => array('Name' => 'url', 'Sortable' => true)
                    );
                break;
            case 'NetworkListings':
                $columns = array(
                    'Company Name' => array('Name' => 'company_name', 'Sortable' => true),
                    'Phone' => array('Name' => 'phone'),
                    'Address' => array('Name' => 'street'),
                    'City' => array('Name' => 'city'),
                    'State' => array('Name' => 'state'),
                    'Zip' => array('Name' => 'zip'),
                    'Category' => array('Name' => 'category'),
                    'Actions' => array('Name' => '', 'Sortable' => false)
                );
                break;
            default:
                $columns = array();
                break;
        }
        
        switch($table){
            case 'Prospects':
                unset($columns['Campaign Form']);
				unset($columns['Source']);
                unset($columns['Touchpoint Type']);
                unset($columns['Touchpoint Name']);
                unset($columns['Touchpoint Page']);
                break;
            case 'ProspectsCampaign':
                unset($columns['Campaign']);
                unset($columns['Touchpoint Type']);
                unset($columns['Touchpoint Name']);
                unset($columns['Touchpoint Page']);
                break;
            case 'ProspectsDashboardCampaign':
                unset($columns['Campaign Form']);
				unset($columns['Source']);
                unset($columns['Campaign']);
                break;
			case 'TopPerforming':
				foreach($columns as &$col){ $col['Sortable'] = false;}
				break;
        }
        return $columns;
    }

    /**
     * Used to get the default column to be used for DataTable sorting.
     *
     * @param string $table ID of the table to get Default Column from.
     * @return array Default sorting table ID and DIR I.E. ('created','asc').
     */
    static function getDefaultColumn($table){

        // Get All Columns.
        $Columns = DataTableResult::getTableColumns($table);
        // Find first column that contains 'Sort' key.
        if($Columns){
            foreach($Columns as $Column){
                if(array_key_exists("Sort",$Column)){
                    return $Column;
                }
            }
        }
        return array();
    }

    /**
		 * Returns the total # of records with $filter
		 * This method is called immediately after getData()
		 * 
     * @return mixed
     */
    protected function getTotalDisplayRecords()
    {
        return Qube::GetPDO()->query('SELECT FOUND_ROWS()')->fetchColumn();
    }

    private static function deep_copy_array(array $array){
        $newArray = array();
        foreach($array as $name => $value){
            if(is_array($value))
                $newArray[$name] = self::deep_copy_array($value);
            elseif(is_object($value))
                $newArray[$name] = clone $value;
            else
                $newArray[$name] = $value;
        }
        return $newArray;
    }

	function GetExportableColumnTitles()
	{
		return array_keys(array_filter(self::getTableColumns($this->_req->get('table')), function($column){return $column['Exportable'] !== false;}));
	}


}
