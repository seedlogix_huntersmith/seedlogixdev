<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of TableCompareChartsData
 *
 * @author amado
 */
class TableCompareChartsData extends AutoLoadChartData
{
    function init(&$newconfig){
        parent::init($newconfig);
        $this->config   =   $newconfig + $this->config;
    }
    
    function queryArchives(DBSelectQuery $q, DBWhereExpression $joins, PointsSetBuilder $DateMagic, $archivet   =   'nb_visits')
    {
        $OneMonth   =   new DateInterval('P1M');
        $A  =   $DateMagic->start;
        $B  =   $DateMagic->end;
        $join  =   clone($joins);
        
        $join->Where("date1 = date2 AND s.name = '%s' AND date1 BETWEEN '%s' AND '%s'", $archivet, $A->format('Y-m-d'), $B->format('Y-m-d'));
        
        $loopdate   =   clone($A);
        $archives   =   array();
        
        do{
            $archive[]  =   $this->getMonthArchiveQuery($q, $loopdate, $join);
//            
//            $this->batch[]   =   " SELECT $b->colfetch as datev, SUM(value) as value FROM " . $this->trackingmodel . " t inner join $tbl s ON (s.idsite = tracking_id and 
//                    s.name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
//                        ' AND date1 >= "' . $datestart->format('Y-m-d') . '"' : '') . ") " . 
//			$this->trackingwhere->FullClause() . " GROUP BY $groupstr";
//            
        }while($loopdate->add($OneMonth) < $DateMagic->tbl_loop_limit);
        
        return $archive;
    }
    
    function getMonthArchiveQuery(DBSelectQuery $joinit, DateTime $loopdate, DBWhereExpression $join)
    {        
        $query  =   clone($joinit);
        return DashboardAnalytics::JoinStatsWhere($query, $loopdate, $join);
    }
    
    function compareTouchpointChanges(VarContainer $params, $tpID){
        
        // @todo possible bug here, the tpid is found in multiple tables (blogs, hubs, etc..)
        
        $dateMagic  =   new PointsSetBuilder($params);        
        $campaignRow    =   new WebsiteStatsRow();
        
        
        // get info before doing comparelast2 because comparelast2 iterates through dates and screwsx up inner dates of dateMagic
//        $campaign_info  =   $campaignRow->loadCampaignData(Qube::Start(), $campaign_ID, DBWhereExpression::Create('l.created ' . $dateMagic->getTimeRangeRestriction()));
        
        $campaign_info  =   array();
        
        $qS     =   new DBSelectQuery(Qube::GetPDO());
                    $qS->From('6q_trackingids t')->Fields('SUM(s.value) as value')->Where('t.id = %d', $tpID);

        $row_values  =   $this->compareLast2($qS, DBWhereExpression::Create('s.idsite = t.tracking_id'), $dateMagic) + $campaign_info;
        
        $campaignRow->render($row_values);
        $campaignRow->id   =   (int)$tpID;
        
        return $campaignRow->getRenderedValues();
    }
    
    function deletemecompareCampaignChanges(VarContainer $params, $campaign_ID)
    {
        $dateMagic  =   new PointsSetBuilder($params);
        $DA =   new PiwikReportsDataAccess();
        $row   =   $DA->getCampaignStatsChangeReport($campaign_ID, $dateMagic);
        return $row;
    }
    
    function compareCampaignChanges(VarContainer $params, $campaign_ID){
				$table = new DataTableCampaignsResult($params);
				
//        $table->setRowCompiler($this->getView('rows/campaign-stats', false, false), 'camp');
        $campaignsTable = $table->getRowsData();
				
				return $campaignsTable['campaigns'];
				
        $dateMagic  =   new PointsSetBuilder($params);        
        $campaignRow    =   new CampaignStatsRow();
        
        
        // get info before doing comparelast2 because comparelast2 iterates through dates and screwsx up inner dates of dateMagic
        $campaign_info  =   $campaignRow->loadCampaignData(Qube::Start(), $campaign_ID, DBWhereExpression::Create('l.created ' . $dateMagic->getTimeRangeRestriction()));
        
        $qS     =   new DBSelectQuery(Qube::GetPDO());
                    $qS->From('6q_trackingids t')->Fields('SUM(s.value) as value')->Where('cid = %d', $campaign_ID);

        $row_values  =   $this->compareLast2($qS, DBWhereExpression::Create('s.idsite = t.tracking_id'), $dateMagic) + $campaign_info;
        
        $campaignRow->render($row_values);
        $campaignRow->campaign_id   =   $campaign_ID;
        
        return $campaignRow->getRenderedValues();
    }
    
    function compareLast2(DBSelectQuery $q, DBWhereExpression $join, PointsSetBuilder $DateMagic)
    {
//        if($DateMagic->isalltime) return array('prevw' => 0, 'curw' => 0);
//        $DateMagic  =   new PointsSetBuilder($params);
        $q2   =   'SELECT SUM(x.value) as value FROM (' . join(" UNION ALL ", $this->queryArchives($q, $join, $DateMagic)) . ' ) x';
        $q1   =   'SELECT SUM(x.value) as value FROM (' . join(" UNION ALL ", $this->queryArchives($q, $join, $DateMagic->previousSet())) . ' ) x';
        
        if(!$DateMagic->isalltime){
        $f  =   Qube::GetPDO()->query("SELECT coalesce(($q2), 0) as prevw, coalesce(($q1), 0) curw")->fetch(PDO::FETCH_ASSOC);
//        $f->change    =   $f->last    ==  0   ? $f->current : (($f->current-$f->last) / $f->last) ;
        
//        return array('prevw' => 10, 'curw' => 7, 'numleads' => 7);
        // load a CampaignStats Row and initialize all values
        return $f;
        }
        
        $f  =   Qube::GetPDO()->query($q2)->fetchColumn();
        
        return array('prevw' => 0, 'curw' => $f);
    }
    
    function loadData($refresh) {
        static $cachedtable =   array();
        
        $vc =   new VarContainer($this->config);
        
        $timekey    =   $vc->period . $vc->interval;
        
        if($vc->checkValue('cID', $cID)){
            if(getflag('disable_cache') || $refresh)
            {
                    $result =   $this->compareCampaignChanges($vc, $cID);
                    if($result && $refresh) // save it
                    {
                        $cachedtable[$cID]  =   $result;
                        $this->controller->cacheSave($cachedtable);
                    }
            }  else {
                    // get cache
                    $cachedtable    =   $this->checkFnCache_Table('campsstats' . $timekey, $cID, $result, NULL);
            }
            $this->data =   $result ? array($cID => $result) : false;
            return;
        }
        
        // touchpoint ID
        if($vc->checkValue('tpID', $tpID)){
            $result =   false;            
            if(getflag('disable_cache') || $refresh){
                $result =   $this->compareTouchpointChanges($vc, $tpID);
                
                if($result && $refresh){
                    $cachedtable['tp' . $tpID]  =   $result;
                    $this->controller->cacheSave($cachedtable);
                }
            } else {
                $cachedtable    =   $this->checkFnCache_Table('tpstats' . $timekey, $tpID, $result, NULL);
            }
            $this->data =   $result?    array($tpID =>  $result)    : false;
            return;
        }
				
				// return comparison table for all cids
				
				$this->data	=   $this->compareCampaignChanges($vc, 0);
        
				return;
				
        // return the table!
        $this->data =   $this->checkFnCache('campsstats' . $timekey);
            
    }
}
