<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationProcessor
 *
 * @author sofus
 */
abstract class NotificationProcessor extends QubeDataProcessor 
{
	/**
	 *
	 * @type MailerQueueDataAccess
	 */
	protected $da	=	0;
	
	function __construct(MailerQueueDataAccess $da)
	{
		$this->da	=	$da;
		parent::__construct($da->getQube());
	}
	
	
	function DoSend(NotificationMailer $Mailer, EmailNotification $Notification, Sendto $recipient)
	{
		$stime = microtime(true);
		$status    =   $Mailer->Send($Notification, $recipient);
		if($status){
			$this->markNotified(true);
			$this->logINFO("OK: (q_id: %d, lead: %d)", $this->q_id, $this->data_id);
			return true;
		}
		
		// error ocurred..
		$this->markNotified(false);
		$this->logERROR('Processing q_id (%d) %s, Data: %s', $this->q_id, $Mailer->getError(), print_r($this, 1));

		throw new QubeException();
		return false;		
	}
	
	function markNotified($sent)
	{
		return $this->da->setSentStatus($this->q_id, $sent);
	}
}
