<?php

/**
 * Description of ChartWidget
 *
 * @author amado
 */
class ChartWidget {
    //put your code here
    
    protected $offset_interval   =   7;
    protected $offset_unit  =   'DAY';
    protected $widget_id    =   '#id';
    
    protected $sets =   array();
    
    static function isValidUnit($unit, $default)
    {
        return preg_match('/^(DAY|WEEK|MONTH)$/', $unit) ? $unit : $default;
    }
    
    /**
     * 
     * @param type $prefix
     * @param type $defint
     * @param type $defunit
     * @return \ChartWidget
     */
    static function fromCookie($prefix, $defint, $defunit)
    {
        $W  =   new ChartWidget();
        $int    =   max(1, (int)Cookie::getVal($prefix . '_chartint', $defint));
        $unit   =   self::isValidUnit(Cookie::getVal($prefix . '_chartunit', $defunit), $defunit);
        
#        echo $int, ', ', $unit;
        
        $W->widget_id   =   $prefix;
        $W->setInterval($int);
        $W->setUnit($unit);
        
        return $W;
    }
        
    function getSettings()
    {
        return array(
                        'unit_cookie'=> $this->widget_id . '_chartunit',
                        'int_cookie' => $this->widget_id . '_chartint',
                        'unit'  => $this->offset_unit,
                        'int'   => $this->offset_interval);
    }
    
    function setInterval($int)
    {
        $this->offset_interval  =   $int;
    }
    
    function setUnit($unit)
    {
        $this->offset_unit  =   $unit;
    }
    
    function addSet($axisarr)
    {
        $this->sets[]   =   $axisarr;
    }
    
    function loadData(DashboardAnalytics $a)
    {
        $timerange  =   "-{$this->offset_interval} {$this->offset_unit}";
        $this->addSet($a->getVisitsGraphData($timerange));
        $this->addSet($a->getLeadsGraphData($timerange));
        
        return $this;
    }
    
    
    function getSets()
    {
        return $this->sets;
    }
}

?>
