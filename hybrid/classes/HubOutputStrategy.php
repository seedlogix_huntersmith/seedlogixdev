<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Instatiates a normal HubOutput object
 *
 * @author amado
 */

class HubOutputStrategy extends DAStrategy
{
	/**
	 *
	 * @var HubModel
	 */
	protected $hub;
	
	function __construct(HubModel $hub) {
		$this->hub	=	$hub;
	}
	
	function createResponder()
	{
		return new HubOutput(Qube::Start());
	}	
}