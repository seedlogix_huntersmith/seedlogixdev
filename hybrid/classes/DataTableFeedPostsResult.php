<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataTableFeedPostsResult
 *
 * @author Eric
 */
class DataTableFeedPostsResult extends DataTableResult{
    private $_limits;
    private $_orderBy;
    
    public function __construct(VarContainer $var) {
        parent::__construct(null, $var);
        $this->_req = $var;
    }


    protected function getPreparedStatement() {
        $query = BlogDataAccess::getOwnMasterPosts();
        $args = array();
        if($this->_req->checkValue('sSearch', $search) && $search != ''){
            if(is_numeric($search)){
                $query .= ' AND p.id = :search';
            }else{
                $query .= ' AND (p.post_title LIKE :search OR F.name LIKE :search)';
            }
        }

        if($this->_req->checkValue('user_id', $user_id)){
            $query .= ' AND p.user_id = ' . intval($user_id);
        }
        
        if($this->_orderBy){
            $query .= $this->_orderBy;
        }
        
        if($this->_limits){
            $query .=  $this->_limits;
        }
        
        $statement = Qube::GetPDO()->prepare($query);
        $statement->setFetchMode(PDO::FETCH_CLASS, 'BlogPostModel');
        return $statement;
    }
    
    protected function OrderBy($column, $dir = 'asc') {
        $this->_orderBy =  ' ORDER BY ' . $column . ' ' . $dir;
    }
    
    public function modifyQuery() {
        $this->_limits = ' LIMIT ' . $this->_req->get('iDisplayStart') . ', ' . $this->_req->get('iDisplayLength');
    }
    
    public function getTotalRecords($args = array()) {
        Qube::GetPDO()->query(BlogDataAccess::getOwnMasterPosts() . ' Limit 1')->execute();
        return Qube::GetPDO()->query('SELECT FOUND_ROWS()')->fetchColumn();
    }
}
