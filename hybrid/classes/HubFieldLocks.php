<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubFieldLocks
 *
 * @author amado
 */
class HubFieldLocks {
	protected $_locks	=	array();
	
	function __construct($locks	=	NULL) {
		if($locks) $this->load($locks);
	}
	
	/**
	 * Append, not overwriting, any locks.
	 * 
	 * @param HubFieldLocks $L
	 */
	function appendLocks(HubFieldLocks $L)
	{
		$this->_locks	=	$this->_locks + $L->_locks;
	}
	
	function load($locks)
	{
		if(is_string($locks))
			$locks	=	json_decode($locks, true);
		
		$this->_locks	=	$locks;

		// This fix keywords not saving on child sites, all keywords use instead 'keywords' lock
		unset($this->_locks->keyword1);
		unset($this->_locks->keyword2);
		unset($this->_locks->keyword3);
		unset($this->_locks->keyword4);
		unset($this->_locks->keyword5);
		unset($this->_locks->keyword6);

		return $this;
	}
	
	function lock($field)
	{
		$this->setLock($field, 1);
	}
	
	function unlock($field)
	{
		$this->setLock($field, 0);
	}
	
	function setLock($field, $state)
	{
		$this->_locks[$field]['lock']	=	$state;
	}
	
	function isLocked($field)
	{
		return isset($this->_locks[$field]) && $this->_locks[$field]['lock'];
	}
	
	function __toString() {
		return json_encode($this->_locks);
	}
	//put your code here
}
