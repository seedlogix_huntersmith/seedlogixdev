<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of FeedDataAccess
 *
 * @author amado
 */
class FeedDataAccess extends QubeDataAccess {
	
	/**
	 *
	 * @var PDOStatement
	 */
	protected $statement;

	function createFeedNavMenu(&$stack, FeedObject $parserObj, $statement	=	NULL) {
		
		if(!$statement)
			$statement = $this->getCategoriesStatement($this->getQube()->getDB('slave'), $parserObj);
		
		$row = $statement->fetch(PDO::FETCH_OBJ);
		if (!$row)
			return '';
		$str = $row->FULL_NAME;

		$cur_path = explode('/', $str);
		$last = &$stack;
		foreach ($cur_path as $p) {
			if (!isset($last[$p])) {
				$last[$p] = (object) array('CATEGORY' => $row, 'CHILD' => '');
			}
			$last = &$last[$p]->CHILD;
		}

		$this->createFeedNavMenu($stack, $parserObj, $statement);
	}

	function getCategoriesStatement(PDO $db, FeedObject $parserObj) {
		$objcols = $parserObj->getSortColumn();
#	 echo $objcols;
		$q = new DBSelectQuery($db);
		$q->from('feedobjects_categories T');
		$q->Fields('FULL_NAME, SHORT_NAME')->addFields($objcols);
		$q->Where($this->getNavCategoriesWhere('T.FEEDID=' . (int) $parserObj->FEEDID, $parserObj));
		if ($_GET['amado'])
			echo $q;
		return $db->query($q . ' HAVING sortval != 900 ' . $parserObj->getOrderBy_Categories());

		return $db->query('SELECT FULL_NAME, SHORT_NAME, ' . $objcols . ' as sortval FROM feedobjects_categories T WHERE ' .
										$this->getNavCategoriesWhere('T.FEEDID=' . (int) $parserObj->FEEDID, $parserObj) . ' ' . $parserObj->getOrderBy_Categories());
	}

	function getNavCategoriesWhere($s, FeedObject $parserObj) {

		$where = DBWhereExpression::Create($s);

		return $parserObj->getNavCategoriesWhere($where);

		$nav_categories = $this->parserObj->getCategories_OrderedArray();
		$or_check = array();
		foreach ($nav_categories as $cat_shortname)
			$or_check[] = sprintf('SHORT_NAME = "%s"', addslashes($cat_shortname));

		$where->Where('(' . implode(' OR ', $or_check) . ')');

#	echo $where;
		return $where;
	}
}