<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IOResultInfo
 *
 * @author amado
 */
class IOResultInfo extends ArrayObject {
	
	function __construct($array = array()) {
		$array['error']	= isset($array['error']) ? $array['error'] :  false;
		parent::__construct($array, self::ARRAY_AS_PROPS);
	}
	
	function getID()
	{
		return $this['object']->getID();
	}
	
	/**
	 * 
	 * @return HybridModel
	 */
	function getObject(){
		return $this->object;
	}
	
	function toJSON()
	{
		return json_encode($this);
	}

}
