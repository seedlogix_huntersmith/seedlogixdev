<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/30/14
 * Time: 2:06 PM
 */

class StaticDataReport implements IReportGenerator{

    private $data;

    public function __construct(){
    }

    public function getOrganicSearchStats()
    {
        return array(
            'Visits' => 321, 'Leads' => 20, 'Conversion' => 20 / 321 * 100
        );
    }

    public function getReferralsStats()
    {
        return array(
            'Visits' => 321, 'Leads' => 20, 'Conversion' => 20 / 321 * 100
        );
    }

    public function getSocialMediaStats()
    {
        return array(
            'Visits' => 321, 'Leads' => 20, 'Conversion' => 20 / 321 * 100
        );
    }

    public function getPaidSearchStats()
    {
        return array(
            'Visits' => 321, 'Leads' => 20, 'Conversion' => 20 / 321 * 100
        );
    }

    public function getDirectTrafficStats()
    {
        return array(
            'Visits' => 321, 'Leads' => 20, 'Conversion' => 20 / 321 * 100
        );
    }

    public function getAllStatsDaily()
    {
        return (object)array(
            'Visits' => array(
            (object)array('day' => '1', 'type' => 'visit', 'visits' => 30),
            (object)array('day' => '2', 'type' => 'visit', 'visits' => 40),
            (object)array('day' => '3', 'type' => 'visit', 'visits' => 37),
            (object)array('day' => '4', 'type' => 'visit', 'visits' => 34),
            (object)array('day' => '5', 'type' => 'visit', 'visits' => 48),
            (object)array('day' => '6', 'type' => 'visit', 'visits' => 31),
            (object)array('day' => '7', 'type' => 'visit', 'visits' => 22),
            (object)array('day' => '8', 'type' => 'visit', 'visits' => 15),
            (object)array('day' => '9', 'type' => 'visit', 'visits' => 35),
            (object)array('day' => '10', 'type' => 'visit', 'visits' => 45),
            (object)array('day' => '11', 'type' => 'visit', 'visits' => 47),
            (object)array('day' => '12', 'type' => 'visit', 'visits' => 49),
            (object)array('day' => '13', 'type' => 'visit', 'visits' => 42),
            (object)array('day' => '14', 'type' => 'visit', 'visits' => 41),
            (object)array('day' => '15', 'type' => 'visit', 'visits' => 48),
            (object)array('day' => '16', 'type' => 'visit', 'visits' => 29),
            (object)array('day' => '17', 'type' => 'visit', 'visits' => 34),
            (object)array('day' => '18', 'type' => 'visit', 'visits' => 38),
            (object)array('day' => '19', 'type' => 'visit', 'visits' => 17),
            (object)array('day' => '20', 'type' => 'visit', 'visits' => 19),
            (object)array('day' => '21', 'type' => 'visit', 'visits' => 22),
            (object)array('day' => '22', 'type' => 'visit', 'visits' => 28),
            (object)array('day' => '23', 'type' => 'visit', 'visits' => 22),
            (object)array('day' => '24', 'type' => 'visit', 'visits' => 25),
            (object)array('day' => '25', 'type' => 'visit', 'visits' => 27),
            (object)array('day' => '26', 'type' => 'visit', 'visits' => 33),
            (object)array('day' => '27', 'type' => 'visit', 'visits' => 38),
            (object)array('day' => '28', 'type' => 'visit', 'visits' => 30),
            (object)array('day' => '29', 'type' => 'visit', 'visits' => 38),
            (object)array('day' => '30', 'type' => 'visit', 'visits' => 41),
            ),
            'Leads' => array(
            (object)array('day' => '1', 'type' => 'leads', 'visits' => 20),
            (object)array('day' => '2', 'type' => 'leads', 'visits' => 16),
            (object)array('day' => '3', 'type' => 'leads', 'visits' => 15),
            (object)array('day' => '4', 'type' => 'leads', 'visits' => 14),
            (object)array('day' => '5', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '6', 'type' => 'leads', 'visits' => 9),
            (object)array('day' => '7', 'type' => 'leads', 'visits' => 10),
            (object)array('day' => '8', 'type' => 'leads', 'visits' => 5),
            (object)array('day' => '9', 'type' => 'leads', 'visits' => 10),
            (object)array('day' => '10', 'type' => 'leads', 'visits' => 15),
            (object)array('day' => '11', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '12', 'type' => 'leads', 'visits' => 16),
            (object)array('day' => '13', 'type' => 'leads', 'visits' => 17),
            (object)array('day' => '14', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '15', 'type' => 'leads', 'visits' => 12),
            (object)array('day' => '16', 'type' => 'leads', 'visits' => 9),
            (object)array('day' => '17', 'type' => 'leads', 'visits' => 8),
            (object)array('day' => '18', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '19', 'type' => 'leads', 'visits' => 13),
            (object)array('day' => '20', 'type' => 'leads', 'visits' => 14),
            (object)array('day' => '21', 'type' => 'leads', 'visits' => 15),
            (object)array('day' => '22', 'type' => 'leads', 'visits' => 16),
            (object)array('day' => '23', 'type' => 'leads', 'visits' => 17),
            (object)array('day' => '24', 'type' => 'leads', 'visits' => 15),
            (object)array('day' => '25', 'type' => 'leads', 'visits' => 15),
            (object)array('day' => '26', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '27', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '28', 'type' => 'leads', 'visits' => 12),
            (object)array('day' => '29', 'type' => 'leads', 'visits' => 11),
            (object)array('day' => '30', 'type' => 'leads', 'visits' => 10)
            ),
        );
    }

    public function getReportData(CampaignModel $camp, ReportModel $report)
    {
        // TODO: Implement getReportData() method.
    }
}