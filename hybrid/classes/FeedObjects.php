<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */



interface FeedItems
{
	function hasItem();
	function getItem($key, FeedObject &$item);
}

class FeedObjects implements FeedItems
{
	protected $file;
	
	function __construct($url   =   ''){
                $this->pie	=	new SimplePie();
		$this->pie->set_cache_location(QUBEADMIN . '6qube/cache/');
                if($url)
                    $this->open($url);
	}

        function openFile($file){
            $this->pie->set_file(new SimplePie_File($file));
            $this->pie->init();
        }
        
	function open($url){			
            if(parse_url($url) !== FALSE)
            {
                $this->pie->set_feed_url($url);
                $this->pie->init();
                return;
            }

            $this->openFile($url);
	}
	
	function hasItem(){
		return true;
	}

// returns array of [key] = [ (data => $data), (data => $data), ..]
	function getItem($key, FeedObject &$item) {
		$item->reset();
		$feeditem	=	$this->pie->get_item($key);	
		$tags	=	$item->getRequiredTags();
		$values	=	array();
		foreach($tags as $tagname){
			
			$tagsfound	=	$feeditem->get_item_tags('http://base.google.com/ns/1.0', $tagname);
			
			if(!$tagsfound) {
                            $tagsfound  =   $feeditem->get_item_tags('', $tagname);                            
                        }
                        
                        if(!$tagsfound) continue;
                        
			foreach($tagsfound as $tag){
				$item->set($tagname, $tag['data']);			
			}		
		}
	
                if(!$item->get('title'))
                    $item->set('title', $feeditem->get_title());
                
                if(!$item->get('description'))
                    $item->set('description', $feeditem->get_description());
		return $item;
	}

	function totalItems(){
		return $this->pie->get_item_quantity();
	}
        
        /**
         * 
         * @param FeedObject $item
         * @param type $feedinfo a row from the feeds table
         */
        function SaveAll(FeedObject &$item, $feedinfo)
        {
            for($i  =   0; $i < $this->totalItems(); $i++)
                $this->getItem($i, $item)->Save($feedinfo);
        }
}
