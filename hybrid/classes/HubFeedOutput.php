<?php

/**
 * HubOutput Class
 * 
 * Converted reseller_site.php to a class
 */
class HubFeedOutput extends HubOutput {

	function initFeedResultInfo($search_str, HubURIRequest $req)
	{
//			$template	=	$this->vars['template'];
			// 1: process a feed request.
		$feed_template	=	false;
			if(($object    =   $this->findFeedObjectTemplate($search_str, $req)) && (
TRUE	//					($object instanceof FeedObjectCategory && $object->getFoundRows() > 0) || $object->ID != ''
			))
			{
					$page_row   =   $object->getPageRow();
					$feed_template   =   $this->getTemplate($this->getThemePath(), $object->getFeedObjectTemplateFilename());
					$feedobject =   &$object;
					$request    =   false;
#			}
			deb($object);
			if($feed_template)
				$this->setVar('template', $feed_template);
			
			$this->setVar('page_row', $page_row);
			$this->setVar('feedobject', $feedobject);
			$this->setVar('request', $request);
			$this->setVar('object', $object);
		}
# deb($object);
		
	}

	
	/**
	 * a feedobject request is composed of atleast 3 directories: /$roothook/$category/$pagename
	 * @return FeedObject
	 */
	function findFeedObjectTemplate($search_str, HubURIRequest $req)
	{
		$hub	=	$this->hub;
		$trimmed_request = $req->getStr();	//trim($fullpath, '/');
		$split_request = $req->getDirs();

		$root_hook = $hub->root;
		$object_hook = "/{$hub->key_match}/"; // this is loaded from the feed row

		$root = array_shift($split_request);
		$is_feed_request	=	$root == $root_hook;
		if(!$is_feed_request) return false;

		$page_name = array_pop($split_request); // the deepest directory of the path
		$current_page = 1;

		if (is_numeric($page_name) && count($split_request)) { // get the page number
			$current_page = $page_name;
			$page_name = array_pop($split_request);
		}

		$full_directory = implode('/', $split_request); //join the $categories except for the roothook and pagename and pagenumber
		$is_feedobject_request = preg_match($object_hook, $page_name, $matches);

		$db = $this->getQube()->GetDB('slave');

		if ($search_str) { // find by key
			$q = $db->prepare("SELECT c.FULL_NAME, o.KEY, o.NAME FROM `feedobjects` o JOIN feedobjects_catindex i ON (i.OBJECTID = o.ID)
	JOIN feedobjects_categories c ON (c.ID = i.CATEGORYID) WHERE o.`KEY` LIKE ? AND o.`FEEDID` = ?  LIMIT 1");
			if ($q->execute(array($search_str, $hub->has_feedobjects)) && $q->rowCount()) {
				$matches = $q->fetch(PDO::FETCH_OBJ);
				$objparser = new $hub->classname;
				$objparser->NAME = $matches->NAME;
				$objparser->KEY = $matches->KEY;
				$objparser->current_path = $root_hook . '/' . $matches->FULL_NAME;
				Header("Location: " . $objparser->getPageUrl());
				exit;
#			var_dump($matches);
			}
		}
		if ($is_feedobject_request) {
			$page_key = $matches[1];

			$object = false;

			// fetch the category
			$q = 'SELECT * FROM feedobjects_categories c, feedobjects_catindex i, feedobjects o
                        WHERE (? <> "" AND c.FULL_NAME = ? AND i.CATEGORYID  =   c.ID AND o.ID=i.OBJECTID) and 
				o.KEY= ? AND o.FEEDID= ?';

			$qp = $db->prepare($q);
			if ($qp->execute(array($full_directory, $full_directory, $page_key, $hub->has_feedobjects)) && $qp->rowCount()) {
				$object = $qp->fetchObject($hub->classname)->loadMeta($db);
				$object->set('pagename', $hub->pagename);
			}

			return $object;
		} else		// if the page_name does not match a object key string, then append the page_name and treat it as part of the category path
			$full_directory .= ($full_directory == '' ? '' : '/') . $page_name;

		$fancyquery = new DBSelectQuery($db);
		$fancyquery->From(array('feedobjects', 'o'));
		$fancyquery
						->Join(array('feedobjects_catindex', 'i'), 'o.ID = i.OBJECTID');

		if ($full_directory == 'all-' . $root) {
			$fancyquery->Where('o.FEEDID    =   ' . $hub->has_feedobjects . ' AND i2.CATEGORYID IS NULL')
							->leftJoin(array('feedobjects_catindex', 'i2'), 'i2.OBJECTID = o.ID AND i2.CATEGORYID > i.CATEGORYID')
							->Join(array('feedobjects_categories', 'c'), 'c.ID = i.CATEGORYID')
							->Fields('o.*, CONCAT(?, "/", c.FULL_NAME) as current_path');

			$query_params = array($root);

			if ($search_str) {
				$q = '%' . preg_replace('@[\s]+@', '%', $search_str) . '%';
#                echo $q;

				$fancyquery->Join(array('feedobjects_meta', 'm'), 'm.`KEY` IN ("description", "meta_title", "title") AND m.OBJECTID = o.ID');
				$fancyquery->Where('(m.`VALUE` LIKE ? OR o.`KEY` like ?)')->groupBy('o.ID');

				$query_params[] = $q;
				$query_params[]	=	"$q%";
				#               echo $fancyquery;
			}

			$full_cat_key = '';
		} else {
			// detect paths: root/category/browse-category and convert them to "root/category" full_directory value

			$full_directory = preg_replace('#([^/]+)/browse-\1$#', '$1', $full_directory);
#            echo $full_directory;
			// select based on a specific category
			$fancyquery->Fields('o.*, ? as current_path')
							->Where('i.CATEGORYID = (SELECT ID FROM feedobjects_categories WHERE FULL_NAME = ? AND FEEDID = o.FEEDID) AND o.FEEDID=%d', $hub->has_feedobjects);


			$query_params = array($root . '/' . $full_directory, $full_directory);
			$full_cat_key = $full_directory;
		}

		$classname = $hub->classname;
		$helperobj = new $classname;

		$helperobj->JoinMetaValuesToCategoryQuery($fancyquery);

		/*
		  $q  =   'SELECT o.*, concat(f.root, "/", ?, "/") as current_path  FROM feedobjects_catindex i, feedobjects o, feeds f
		  WHERE i.CATEGORYID = (SELECT ID FROM feedobjects_categories WHERE FULL_NAME = ?)
		  AND o.ID = i.OBJECTID AND f.ID = o.FEEDID';
		 * 
		 */

		$perpage = 20;
		$fancyquery->calc_found_rows();
		$fancyquery->Limit((max(1, $current_page) - 1) * $perpage, $perpage);

		$qp = $fancyquery->Prepare();

		if ($qp->execute($query_params)) {
			return new FeedObjectCategory($qp, $hub->classname, $full_cat_key, $hub->has_feedobjects, $root . '/' . $full_directory, $current_page, $perpage, $search_str);
		}

		throw new Exception('failed');
	}
	
	function doAdditionalHubProcessing(\HubURIRequest $req) {
		$this->initFeedResultInfo($_GET['search'], $req);
	}

}
