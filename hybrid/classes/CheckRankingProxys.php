<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/27/15
 * Time: 2:02 PM
 */

use Qube\Hybrid\Rankings\BingSearch;
use Qube\Hybrid\Rankings\GoogleSearch;

/**
 * Class CheckRankingProxys
 */
class CheckRankingProxys extends QubeDataProcessor{
	/**
	 * The code used with an unexpected http code returned by curl
	 */
	const UNKNOWN_CODE = -1;

	/**
	 * The code returned by curl when it couldn't connect to a proxy
	 */
	const UNAVAILABLE_CODE = 0;

	/**
	 * The code returned by curl with a successful connection to a proxy
	 */
	const SUCCESS_CODE = 200;

	/**
	 * The code returned by curl when the service is unavailable, it is assumed that a searchengine has blacklisted a proxy
	 */
	const BLACKLISTED_CODE = 503;

	/**
	 * @var  RankingProxiesDataAccess
	 */
	private $rankingProxiesDataAcces;

	/**
	 * @var array Urls used to initialize cookies for each searchengine
	 */
	public $initCookiesUrls = array(
		'google' => 'https://www.google.com/search?q=16+camera+dvrs+Billings%2C+MT&ie=utf-8&oe=utf-8&rls=org:mozilla:us:official&client=firefox&start=0&num=100',
		'bing' => 'http://www.bing.com/search?q=16+camera+dvrs+Billings%2C+MT&qs=n&form=QBLH&pq=16+camera+dvrs+billings%2C+mt&count=50'
	);
	/**
	 * The URL to test connectivity to a proxy
	 * @var string
	 */
	public $testURL = 'https://www.google.com/search?q=16+camera+dvrs+Billings%2C+MT&ie=utf-8&oe=utf-8&rls=org:mozilla:us:official&client=firefox&start=0&num=100';

	/**
	 * @param Qube|NULL $Q
	 * @param int $logmode
	 */
	function __construct(Qube $Q = NULL, $logmode = Logger::DOLOG)
	{
		parent::__construct($Q, $logmode);
		$this->rankingProxiesDataAcces = new RankingProxiesDataAccess();
	}


	/**
	 * @param VarContainer|null $vars
	 */
	function Execute(VarContainer $vars = null)
	{
		$this->logINFO('Start');
		$this->getRankingProxiesDataAccess()->initializeProxies();
		$results = $this->checkConnectivity();
		$this->setUpProxies($results);
		$this->initCookies();
		$this->checkSearchEngines(array(
			'google' => true,
			'bing' => true
		));

	}

	/**
	 * Verifies that all proxies can be connected
	 * @return array
	 */
	function checkConnectivity(){
		$proxyResults = array();

		$this->logINFO('Getting proxy list');
		$proxys = $this->getRankingProxiesDataAccess()->getAllProxies();
		foreach($proxys as $proxy){
			$results = $this->doRequest($this->testURL, array(), $proxy);
			switch($results['info']['http_code']){
				case CheckRankingProxys::UNAVAILABLE_CODE:
					$this->logINFO("Couldn't connect to proxy $proxy");
					$proxyResults[CheckRankingProxys::UNAVAILABLE_CODE][] = $proxy;
					break;

				case 200:
					$this->logINFO("proxy Success!! $proxy");
					$proxyResults[CheckRankingProxys::SUCCESS_CODE][] = $proxy;
					break;

				case 503:
					$this->logInfo("Service unavailable with proxy $proxy");
					$proxyResults[CheckRankingProxys::BLACKLISTED_CODE][] = $proxy;
					break;

				default:
					$this->logInfo("Unexpected code {$results['info']['http_code']} with proxy $proxy");
					$proxyResults[CheckRankingProxys::UNKNOWN_CODE][] = $proxy;
					break;
			}
		}

		return $proxyResults;
	}


	/**
	 * Do a request to the url using the specified proxy
	 * @param string $url
	 * @param array $options
	 * @param string $proxy
	 * @return array An array with the following elements
	 * <ul>
	 * <li>content: The content retrieved by the URL</li>
	 * <li>info: The data returned by curl_getinfo()</li>
	 * <li>error: The data returned by curl_error()</li>
	 * </ul>
	 */
	function doRequest($url, $options = array(), $proxy = null){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		foreach($options as $option => $value){
			curl_setopt($ch, $option, $value);
		}
		// proxy stuff

		if($proxy){
			list($proxy, $proxyport, $proxy_userpass)   =   explode(':', $proxy, 3);
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
			curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
			if($proxy_userpass){
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy_userpass);
				curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
			}
		}

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		$data = curl_exec($ch);
		$return_info = curl_getinfo($ch);
		$error_info = curl_error($ch);
		curl_close($ch);

		return array('content' => $data, 'info' => $return_info, 'error' => $error_info);
	}

	/**
	 * Returns the cookie file path for a the specified proxy
	 * @param $proxyIP
	 * @return bool|string
	 * @throws QubeException
	 */
	protected function getCookieFilePath($proxyIP)
	{
		$cookiejar = Crawler::getCookieJarFile($proxyIP);
		if (!touch($cookiejar) || !is_writable($cookiejar)) {
			throw new \QubeException('could not create cookie . ' . $cookiejar);
		}
		return $cookiejar;
	}

	/**
	 * Check all active proxies and setup the ranking_proxys table for getting the best proxy for each searchengine when RankingProxiesDataAccess::getPreferredProxy is called
	 * @param $searchengines
	 * @throws QubeException
	 */
	private function checkSearchEngines($searchengines)
	{
		/** @var SearchCrawler[] $engines */
		$engines = array();

		if($searchengines['google']){
			$s = new GoogleSearch($this->getQube());
			$s->setLogger($this);
			$engines['google'] = $s;
		}

		if($searchengines['bing']){
			$s = new BingSearch($this->getQube());
			$s->setLogger($this);
			$engines['bing'] = $s;
		}

		$proxies = $this->getRankingProxiesDataAccess()->getActiveProxies();
		foreach($proxies as $proxy){
			list($proxyIP) = explode(':', $proxy);
			$options = array(
				CURLOPT_STDERR => fopen('php://stdout', 'w'),
				CURLOPT_COOKIEFILE => $this->getCookieFilePath($proxyIP),
				CURLOPT_COOKIEJAR => $this->getCookieFilePath($proxyIP),
			);

			foreach($engines as $name => $searchengine){
				$content = $this->doRequest($this->initCookiesUrls[$name], $options, $proxy);
				if($content['content']){
					$results = $searchengine->getNoResultsInContent($content['content']);
					if($results < 100){
						$this->getRankingProxiesDataAccess()->setUpSearchengineProxy($proxy, $name, 100);
					}
					$this->logINFO("Searchengine: $name Found $results results with $proxy");
				}else{
					$this->logINFO("Couldn't connect to proxy $proxy");
					break;
				}
			}
		}
	}

	/**
	 * Initialize cookies for each proxy
	 * @throws QubeException
	 */
	function initCookies(){
		$this->logINFO('Initializing cookies');
		$proxys = $this->getRankingProxiesDataAccess()->getActiveProxies();
		foreach($proxys as $proxy) {
			foreach($this->initCookiesUrls as $url){
				list($proxyIP) = explode(':', $proxy);

				$cookiejar = $this->getCookieFilePath($proxyIP);

				$options = array(
					CURLOPT_COOKIEJAR => $cookiejar,
					CURLOPT_COOKIEFILE => $cookiejar,
					CURLOPT_STDERR => fopen('php://stdout', 'w')
				);

				$this->logINFO(preg_replace('/%/', '%%',"Getting cookies for $proxy with $url"));
				$this->doRequest($url, $options, $proxy);
			}
		}
	}

	/**
	 * Set active all proxies with a succes code in CheckRankingProxys::checkConnectivity and disable the proxies with unavialable code
	 * @see checkConnectivity()
	 * @param $proxies
	 */
	function setUpProxies($proxies){
		foreach($proxies[CheckRankingProxys::UNAVAILABLE_CODE] as $proxy){
			$this->getRankingProxiesDataAccess()->setActiveProxy($proxy, 0);
			$this->getRankingProxiesDataAccess()->updateLastUpdated($proxy);
		}

		foreach($proxies[CheckRankingProxys::SUCCESS_CODE] as $proxy){
			$this->getRankingProxiesDataAccess()->setActiveProxy($proxy);
			$this->getRankingProxiesDataAccess()->updateLastUpdated($proxy);
		}
	}

	/**
	 * Get the RankingProxiesDataAccess
	 * @return RankingProxiesDataAccess
	 */
	public function getRankingProxiesDataAccess(){
		return $this->rankingProxiesDataAcces;
	}
}
