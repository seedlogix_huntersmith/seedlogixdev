<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of FeedNav
 *
 * @author amado
 */
class FeedNav {

	/**
	 *
	 * @var FeedObject
	 */
	protected $parserObj;
	protected $categories;
	protected $root;
	
	/**
	 *
	 * @var FeedDataAccess
	 */
	protected $DA	=	NULL;

	//put your code here
	function __construct($feed_id, $class_or_obj, $feedroot) {

		$this->categories = array();
		$this->root = $feedroot;
		
		$this->parserObj = is_string($class_or_obj) ? new $class_or_obj : $class_or_obj;
		$this->parserObj->FEEDID = $feed_id;
	}

	/*
	 * check a dispv2nav array to see if we shoud latch on and replace the nav with our nav.
	 */

	function checkV2Nav($value) {
#return false;
		if (is_array($value) && isset($value[0]) && $value[0]['type'] == 'nav' && $value[0]['page_title_url'] == $this->root) {
#            array_shift()
#            var_dump($value);
			$this->printNav();
			return true;
		}
		return false;
	}

	function printNav() {
		$this->parserObj->printNavMenu($this->getArray());
	}
	
	function getDA()
	{
		return $this->DA;
	}
	
	function setDA(FeedDataAccess $DA)
	{
		$this->DA	=	$DA;
	}

	function getArray() {
		if(empty($this->categories))
		{
			$this->getDA()->createFeedNavMenu($this->categories, $this->parserObj);
		}
		return $this->categories;
	}

}

?>
