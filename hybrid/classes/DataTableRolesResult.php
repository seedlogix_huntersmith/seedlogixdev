<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/10/14
 * Time: 1:33 PM
 */

class DataTableRolesResult extends DataTableResult{
    /** @var  UserDataAccess */
    private $dataAccess;
    /**
     * @param UserDataAccess $da
     * @param VarContainer $var
     */
    public function __construct(UserDataAccess $da, VarContainer $var){
        parent::__construct(null, $var);
        $this->dataAccess = $da;
    }

    function getTotalRecords($args = array())
    {
        return $this->dataAccess->CountRoles();
    }

    protected function Sort()
    {

    }

    function modifyQuery()
    {

    }

    function getData()
    {
        $searchstr = '';
        if($this->_req->checkValue('sSearch', $search) && $search != ''){
            $searchstr = $search;
        }

        $roles = $this->dataAccess->getRoles(0, array('search' => $searchstr), $this->_req->get('iDisplayStart'), $this->_req->get('iDisplayLength'), array('new_rows_ids' => $this->_req->getValue('newRowsIds', array())));

        if(!$this->_rowCompiler)
            return $roles;

        $data   =   array();
        foreach($roles as $row){
            $html   =   $this->_rowCompiler->init($this->_rowVar, $row)->toString();
            if(preg_match_all('/<td[^>]*?>([\s\S]*?)<\/td>/', $html, $matches)){
                $data[] =   $matches[1];
            }
        }

        return $data;
    }

    protected function getTotalDisplayRecords()
    {
        return $this->dataAccess->getQube()->query('SELECT FOUND_ROWS()')->fetchColumn();
    }


}