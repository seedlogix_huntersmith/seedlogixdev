<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of ValidationStack
 *
 * @author amado
 */

class ValidationStack
{
    protected $errors   =  array();
    protected $limit_reached = null;

    function setlimit($limit){
        $this->limit_reached = $limit;
        return $this->limit_reached;
    }

    function getlimit(){
        return $this->limit_reached;
    }
    
    /**
     * Adds an error message to a key. Can have multiple keys.
     * 
     * @param type $key
     * @param type $message
     * @return \ValidationStack
     */
    function addError($key, $message)
    {
        if(!isset($this->errors[$key]))
            $this->errors[$key] =   array();
        $this->errors[$key][]   =   $message;
        return $this;
    }
    
    function addErrors($key, $messages)
    {
        foreach($messages as $message)
            $this->addError($key, $message);
    }
    
    function getStatus()
    {
        if($this->hasErrors()) return 'validation';
        return false;
    }
    
    function hasErrors(){
        return !empty($this->errors);
    }
    
    function getErrors($k   =   NULL)
    {
        if($k)
            return $this->errors[$k];
        
        return $this->errors;
    }

    function deny($message	=	'Permission Denied.'){
		$this->addError('permission', $message);
        return $this;
    }

    function isDenied(){
        return isset($this->errors['permission']);
    }
	
	function Validate($key, $value, \Zend\Validator\AbstractValidator $validator)
	{
		if(!$validator->isValid($value))
		{
			$this->addErrors($key, $validator->getMessages());
			return false;
		}
		
		return true;
	}
	
	function ValidateURL($key, $value){
        return $this->Validate($key, $value, new \Zend\Validator\Uri(array(
            'uriHandler' => 'HttpValidator',
            'messages' => array(
                'notUri' => 'Not a valid URL'
            )
        )));
    }

    function ValidateEmail($key, $value){
        return $this->Validate($key, $value, new \Zend\Validator\EmailAddress());
    }
    
    function Reset(){
        $this->errors   =  array();
    }
}
