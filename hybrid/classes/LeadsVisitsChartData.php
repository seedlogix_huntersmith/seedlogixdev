<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
class LeadsVisitsChartData  extends AutoLoadChartData
{
	static function getTimeContext($cookiename, $default)
	{
		list($interval, $period)    =   explode('-', Cookie::getVal($cookiename, $default));
		PointsSetBuilder::validateTimeValues($interval, $period);
		return VarContainer::getContainer(compact('interval', 'period'));
	}
	
    function init(&$newconfig){
    	if(!empty($newconfig)){
            parent::init($newconfig);
            $this->config	=	$newconfig + $this->config;
        }
        $this->saveConfig();
    }
    
    function loadConfig($configstr) {
        list($interval, $period)    =   explode('-', $configstr);
        PointsSetBuilder::validateTimeValues($interval, $period);
        $this->config   =   compact('interval', 'period');
    }

    function saveConfig(){
        //setcookie($this->cookiename,'',1);
        setcookie($this->cookiename,"{$this->config['interval']}-{$this->config['period']}");
    }

    function getLeadsVisitsPoints(VarContainer $c,&$result){

		$PRDA               = PiwikReportsDataAccess::FromController($this->controller);
        $PointCreator       = new PointsSetBuilder($c,$this->controller->getUser()->created);
        $ministats          = array();
        $result             = array();

        if(!$this->getChartPoints($PointCreator,$result[0],$PRDA,'chart')) return false;
        if(!$this->getLeadsPoints($PointCreator,$result[1],$PRDA,'chart')) return false;

        if(!$this->getChartPoints($PointCreator,$ministats[0],$PRDA,'array')) return false;
        if(!$this->getLeadsPoints($PointCreator,$ministats[1],$PRDA,'array')) return false;

        $quickstats_desc    = $c->period == 'ALL' ? 'All Time' : "$c->interval $c->period";

        $ministats_1        = new MiniQuickStat($ministats[0],'Visits',$quickstats_desc,'sparkline');
		$ministats_2        = new MiniQuickStat($ministats[1],'Leads',$quickstats_desc,'sparkline');

        $result[0]          = array('label' => '&nbsp;Visits','data' => $result[0],'mini' => $ministats_1->getLi(),'total' => array_sum($ministats[0]));
        $result[1]          = array('label' => '&nbsp;Leads','data' => $result[1],'mini' => $ministats_2->getLi(),'total' => array_sum($ministats[1]));

        return true;
    }
    
    function loadData($refresh) {
        $vc		=	new VarContainer($this->config);
        if(getflag('disable_cache') || $refresh || TRUE){
        		$result	=	$this->getLeadsVisitsPoints($vc, $this->data);
        		if($result && $refresh)
        			$this->controller->cacheSave($this->data);
        }else
        		$this->data  =   $this->checkFnCache('getLeadsVisitsPoints', $vc);
    }
}
