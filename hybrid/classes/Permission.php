<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Permission
 *
 * @author Amado
 */
class Permission {
    //put your code here
    
    protected $system_admin =   0;
    protected $support_admin    =   0;
    protected $_rendered    =   array();
    
    function __set($name, $value){
        $this->$name    =   $value;
    }
    
    function __call($name, $arguments) {
        $key    =   $name . '_' . implode('_', $arguments);
        if(!isset($this->_rendered[$key]))
        {
            array_unshift($arguments, $name);
            $this->_rendered[$key]   = call_user_func_array(array($this, '_check'), $arguments);
        }
        
        return $this->_rendered[$key];
    }
    
    function _set($prop, $val){
        $this->$prop    =   $val;
    }
    
    function _check($name, $key){
        if($this->system_admin) return true;
        if($this->support_admin){
            if(strpos($name, 'view_') === 0) return true;
            if(strpos($name, 'edit_')   ==  0)  return true;
            if(strpos($name, 'delete_') ==  0)  return false;
            
            return false;
        }
        
        return $this->$name;
    }
    
}

?>
