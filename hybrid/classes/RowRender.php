<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of RowRender
 *
 * @author amado
 */
abstract class RowRender {

    protected $values = array();

    function getRenderedValues() {
        return (object)$this->values;
    }
    
    function loadValues($values){
        $this->values   =   $values + $this->values;
    }

    function __set($name, $value) {
        $this->values[$name]    =   $value;
    }
    
    abstract function render($array);
    
    function renderVisitsChange($arr) {
        extract($arr);
        $p = $prevw ? ($prevw - $curw) / $prevw : 0;
        $css = 'statsMinus';
        if ($p > 1) {
            $css = 'statsPlus';
# 		$p	-=	1;
        }
        $str = "<span class=\"$css\">" . round($p * 100) . ' %</span>';
        $this->values['visitsChange'] = $str;
    }

    function renderVisits($arr) {
        $this->values['nb_visits'] = $arr['curw'];
    }

    function renderConversionRate($arr) {
//        reset($arr);
        extract($arr);
        $cr = $curw ? $numleads / $curw : 0;
        $this->values['convert_rate'] = round($cr*100) . '%';
    }

}

