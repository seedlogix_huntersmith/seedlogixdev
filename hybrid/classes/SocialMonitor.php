<?php

/*
 * Author:    Jon Aguilar
 * License:    
 */

/**
 * Description of SocialMonitor
 */
class SocialMonitor {
    
    function queryFacebookBlogPosts(Qube $q){
        $BlogsQuery =   $q->queryObjects('ResellerModel', 'R', 'fbapp_secret != ""')
                ->Join(array('BlogModel', 'B'), '(B.user_parent_id = R.admin_user OR 
                                    (B.user_parent_id = 0 AND B.user_id = R.admin_user)) AND B.trashed = 0 AND B.facebook_id != ""')
                ->Fields('B.user_id, B.cid, R.fbapp_secret, R.fbapp_id, B.facebook_id');
        return $BlogsQuery;
    }
    
    /**
     * 
     * @param Qube $q
     * @return DBQuerySelect
     */
    function queryTwitterBlogPosts(Qube $q){
        $BlogsQuery = $q->queryObjects('ResellerModel', 'R', 'fbapp_secret != ""')
                ->Join(array('BlogModel', 'B'), '(B.user_parent_id = R.admin_user OR 
                                    (B.user_parent_id = 0 AND B.user_id = R.admin_user)) AND B.trashed = 0 AND B.twitter_accessid != ""')
                ->Fields('B.user_id, B.cid, R.twapp_secret, R.twapp_id, B.twitter_accessid as blog_twaccessid');

        return $BlogsQuery;
    }
    
    static function getTwitterObject($useraccess_info, $app_id, $app_secret)
    {
        list($o_token, $o_secret, $o_screenname)    =   explode("\n", $useraccess_info);

        $tr	=	array('token'	=> $o_token,
                        'secret'	=> $o_secret);

        $TW =   new Zend_Service_Twitter(array('accessToken' => $tr, 
			'username' => $o_screenname,
				'oauthOptions' => array(
					'consumerKey' => '7n4KT8rtdiM1sxc7HPFTvGyrI',    //'cZnMrZtUh9VvYgoHlfGUQ',
	'consumerSecret' => 'DF2UQS4roV6MYcD1OXK407nG9ZAJMRFC0gnDtpuAunoi0Mfd0j', 
    'AccessToken' => '3241730810-8PxQoOtanQVdynQRKL4IZTY1xz4wnL1n6YLWOcf', //'VKAfHegB8NhFv2QmQfEosXzV6rvyfPtV7fHLUS4zc'
	'AccessTokenSecret' => 'wtxzUfJX3TAJlOIxhoSHxPL8RGCEOa3CpA2trWLLGCaJM', 
        )));
        
        return $TW;
    }

    static function MonitorTwitter(Qube $q) {
        $Monitor = new SocialMonitor();

        /* @var $PastePHPReseller ResellerModel */
        $BlogsQuery = $Monitor->queryTwitterBlogPosts($q);

        /* @var $stmt PDOStatement */
        $stmt = $BlogsQuery->Exec();

        /* @var $BlogInfo ResellerModel */
        while ($Reseller = $stmt->fetchObject('ResellerModel')) {
            $Twitter = $Reseller->getTwitter($Reseller->blog_twaccessid);
            $Twitter->accountVerifyCredentials();
            $UserInfo = $Twitter->usersShow($Twitter->getUsername());
		if($UserInfo->isError()) continue;	
		var_dump($UserInfo);
            self::SaveData($Reseller->user_id, "followers", $Reseller->cid, $UserInfo->followers_count);
        }
    }

    static function SaveData($user_ID, $name, $cid, $value) {
        static $stmt = NULL;
        if (!$stmt)
            $stmt = Qube::GetPDO()->prepare('REPLACE INTO 6q_socialstats (user_ID, name, datetime, cid, value) VALUES (?, ?, NOW(), ?, ?)');

        return $stmt->execute(array($user_ID, $name, $cid, $value));
    }

    static function MonitorFacebook(Qube $q) {
        $Monitor    =   new SocialMonitor();

        /* @var $PastePHPReseller ResellerModel */
        $BlogsQuery = $Monitor->queryFacebookBlogPosts($q);

        /* @var $stmt PDOStatement */
        $stmt = $BlogsQuery->Exec();

        /* @var $BlogInfo ResellerModel */
        while ($BlogInfo = $stmt->fetchObject('ResellerModel')) {
            $FB = $BlogInfo->getFacebook();
            list($page_id, $pagename, $access_token) = explode("\n", $BlogInfo->facebook_id);   // id of the BLOG FB PAGE

            $fields = 'fan_count';
            try {
                $result = $FB->api('/' . $page_id . '/?fields=' . $fields);
				var_dump($result);
                if(!$result['fan_count']){
//                    Qube::LogError('Facebook Result likes is null: ', $BlogInfo);
                    $result['fan_count']    =   0;
                }
                self::SaveData($BlogInfo->user_id, 'likes', $BlogInfo->cid, (int)$result['fan_count']);
//        $replacestmt->execute(array($BlogInfo->user_id, 'likes', $BlogInfo->cid, $result['likes']));
            } catch (Exception $e) {
		if($e->getType() == 'GraphMethodException'){
			// the page_id is likely  invalid
			// @todo notify user and disable the facebook id in the db
#			Qube::LogError('Caught Exception', $e);
		}else
	                Qube::LogError('Cron Exception: ', $e);
            }
        }
    }

}

?>
