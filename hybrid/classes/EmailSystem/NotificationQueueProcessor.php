<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationQueueProcessor
 *
 * @author sofus
 */
class NotificationQueueProcessor extends QubeDataProcessor
{
	/**
	 *
	 * @var MailerQueueDataAccess
	 */
	protected $da	=	NULL;
	
	function getDataAccess()
	{
		if(!$this->da){
			$this->setDataAccess(new MailerQueueDataAccess($this->getQube()));
		}
		
		return $this->da;
	}
	
	function setDataAccess(MailerQueueDataAccess $da)
	{
		$this->da	=	$da;
		return $this;
	}

    /**
     * @param LeadNotifier[] $Notifiers
     * @param VarContainer $vars
     * @return bool
     */
	function SendNotifications(array $Notifiers, VarContainer $vars)
	{
			if(empty($Notifiers))
			{
				$this->logINFO("No More Notifications.");
				return false;
			}

			$this->logINFO("%d found, %01.2f secs to load", count($Notifiers), microtime(true)-$vars->time);
			foreach($Notifiers as $Notifier)
			{
//				if($vars->recipient)					$Notifier->collab_email = $Zopts->recipient;

				$Notifier->setLogger($this);
				$Notifier->Execute($vars);
			}
			return true;
	}
	
	function Execute(\VarContainer $vars) {
		
		if(!$vars->Mailer instanceof NotificationMailer)
			throw new QubeException();
			
		/**
		 * Send Contact Notifications!
		 */
		while(false)
		{
			$vars->time = microtime(true);
			$LeadNotifications	=	$this->getDataAccess()->getQueuedContactNotifications();

			if(FALSE === $this->SendNotifications($LeadNotifications, $vars))
							break;
		}
		
		/**
		 * Send Lead Notifications!
		 */
		while(true)
		{
			$vars->time = microtime(true);
			/* @var $LeadNotifications LeadNotifier[] */
			$LeadNotifications	=	$this->getDataAccess()->getQueuedLeadNotifications();

			if(FALSE === $this->SendNotifications($LeadNotifications, $vars))
							break;
		}
	}
}
