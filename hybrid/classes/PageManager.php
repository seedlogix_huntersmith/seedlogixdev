<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PageManager
 *
 * @author amado
 */

class PageManager
{
    /**
     *
     * @var PageModel
     */
    protected $_Page;
    
    function __construct(PageModel $page) {
        $this->_Page    =   $page;
    }
    
    function AddToSite(WebsiteModel $site){
        if(!$site->id) $site->Save();
        $this->_Page->loadArray(array('user_ID' => $site->user_id,
                'hub_ID'    => $site->id));
        return $this->_Page;
    }
    
    function AddToNav(NavModel $nav){
        if(!$nav->id) $nav->Save();
        
        /*$this->_Page->loadArray(array('nav_ID' => $nav->id, 'hub_ID' => $nav->hub_ID,
                'user_ID' =>    $nav->user_ID));*/
        $this->_Page->loadArray(array('parent_nav_id' => $nav->id, 'hub_id' => $nav->hub_id,
                'user_id' =>    $nav->user_id));
        
        return $this->_Page;
    }
}
