<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of RouterRequest
 *
 * @author amado
 */
class RouterRequest {

	const SUBDOM_WWW = 1;
	const SUBDOM_PREVIEW = 2;
	const SUBDOM_NOWWW = 3;

	protected $_subdom_type = 0;
	protected $host_raw = '';
	protected $request_URI	=	'';
	protected $request_path	=	'/';

	protected function __construct() {
		;
	}
	
	function setHostString($host) {
		$this->host_raw = $host;

		$this->_subdom_type = self::SUBDOM_NOWWW;

		$is_www = preg_match('/^www\./i', $host);
		if ($is_www) {
			$this->_subdom_type = self::SUBDOM_WWW;
		} else {
			$is_preview = preg_match('/^hubpreview./i', $host);
			if ($is_preview) {
				$this->_subdom_type = self::SUBDOM_PREVIEW;
			}
		}
	}

	function setRequestURI($request_uri)
	{
		$this->request_URI	=	$request_uri;
		$this->request_path	=	@reset(explode('?', $request_uri));
	}

	function getRequestURI()
	{
		return $this->request_URI;
	}

	function getRequestPath()
	{
		return $this->request_path;
	}
	
	function getHostString(){
		return $this->host_raw;
	}

	function getSubdomainType() {
		return $this->_subdom_type;
	}
	
	/**
	 * Removew the www subdomain from host string.
	 * 
	 * @return type
	 */
	function getHost()
	{
		return preg_replace('/^(www|hubpreview)\./i', '', $this->host_raw);
	}

	static function loadWebRequest() {
		$RR = static::CreateRequest($_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);
		return $RR;
	}

    static function CreateRequest($HOST, $REQUEST_uri)
    {
        $RR = new RouterRequest();
        $RR->setHostString($HOST);
        $RR->setRequestURI($REQUEST_uri);
        return $RR;
    }
}
