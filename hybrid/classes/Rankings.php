<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of Rankings
 *
 * @author amado
 */

class Rankings
{
	function getQuery(){
		$db	=	Qube::Start()->GetDB();
		$q	=	new DBSelectQuery(Qube::GetPDO());
		$q->From('sapphire_rankings.rankings r')->Join('sapphire_rankings.sites s', 's.id = r.site_id');

return $q;		
	}

static function GetTopRankings(PointsSetBuilder $timecontext, $limit=5){
	
	$Rankings	=	new Rankings();
	
	return array('google' => $Rankings->queryMin('google', $timecontext, $limit)->Exec()->fetchAll(PDO::FETCH_KEY_PAIR),
				'bing' => $Rankings->queryMin('bing', $timecontext, $limit)->Exec()->fetchAll(PDO::FETCH_KEY_PAIR));
}

	function queryMin($searchEngine, PointsSetBuilder $p, $limit = NULL){
		$q	=	$this->getQuery();
		$q->Fields('r.keyword, min(r.' . $searchEngine . ') as rank');
		$q->Where('r.datetime ' . $p->getTimeRangeRestriction());
		$q->Where('r.' . $searchEngine . ' != 0 AND s.user_id = dashboard_userid()')
			->orderBy('rank ASC');
			$q->groupBy('r.keyword');
			if($limit)
				$q->Limit($limit);
		return $q;
	}

	function queryTop__depr($user_id, PointsSetBuilder $p, $searchEngine	=	'bing'){
		throw new Exception('invalid. set @dashboard_userid to change user');
		$q	=	$tihs->queryMin($searchEngine, $p);
			
			return $q;
	}

	function queryTopFromCID($cid, PointsSetBuilder $p, $searchEngine	=	'bing', $limit = 10){
		$q	=	$this->queryMin($searchEngine, $p, $limit);
		$q->Where('s.cid = %d', $cid);
			
			return $q;
	}
}

