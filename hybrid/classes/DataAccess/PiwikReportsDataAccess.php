<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PiwikReportsDataAccess
 *
 * @author amado
 */
class PiwikReportsDataAccess extends QubeDataAccess
{
	const STATSTBL	=	'piwikreports';

	/**
	 * Returns boolean indicating if stats exist for a given date.
	 *
	 * @param $datestr
	 * @param $user_id
	 * @return bool
	 */
	function hasDailyStat($datestr, $user_id)
	{
		$q = 'SELECT 1 FROM ' . self::STATSTBL . ' where user_ID = ' . $user_id . ' AND point_date = "' . $datestr . '" AND point_type = "DAILY"';
		return $this->query($q)->fetchColumn() == 1;
	}
	/**
	 * gives an array of stdobj with user_id, mindate, maxdate, created, range1, range2
	 * the objects represent the users
	 * @return array
	 */
	function getUsersForImportStats()
	{
		$query = 'SELECT u.id,
	min( r.point_date ) AS mindate,
	max( r.point_date ) AS maxdate,
	date(u.created) as created,
	concat(date(created), ",", min(r.point_date)) as range1,
	if(max(point_date) != curdate(),concat(max(r.point_date), ",", curdate()),"") as range2
FROM users u
LEFT JOIN piwikreports r ON ( r.user_ID = u.ID  and point_type = "DAY")
WHERE u.created > 0 AND u.last_login > "2015-01-01"
GROUP BY u.ID having mindate > created
ORDER BY `range1`  ASC';
		return $this->query($query)->fetchAll(PDO::FETCH_OBJ);
	}

        function deletemegetCampaignStatsChangeReport($cid, PointsSetBuilder $dateMagic, $user_id = null)
        {    
					$d4 =   $dateMagic->getEndDate();
					$d3 =   $dateMagic->getStartDate();
					$d2 =   $d3;
					$d1 =   $dateMagic->previousSet()->getStartDate();
#			echo $dstr1, $dstr2, $dstr3, $dstr4, $this->getQube()->query('select dashboard_userid()')->fetchColumn();

					$query	= DataTableCampaignsResult::getQueryCampaigns();

					$p  =   $this->getQube()->GetPDO()->prepare($query);
					$args = compact('d1', 'd2', 'd3', 'd4', 'cid');
					if($user_id)
						$args['user_id'] = $user_id;
					$p->execute($args);
					
					if($cid)
						return $p->fetch(PDO::FETCH_OBJ);
					
					return $p->fetchAll(PDO::FETCH_OBJ);					
        }
        
        function getCampaignsStatsQubePDOStatement(){
            
        }

	/**
	 * @todo add a way to toggle exclusion of inactive touchpoints.
	 *
	 * @param $user_ID
	 * @param $created_before
	 * @param $id_greater_than
	 * @param int $limit
	 * @return array
	 */
	function getUserTrackingIds($user_ID, $created_before, $id_greater_than, $limit = 100)
	{

		$q = 'SELECT tp.tracking_id, tp.ID as touchpoint_id,
		 	if(tp.touchpoint_type="BLOG","blog","hub") as touchpoint_type,
		 		if(tp.hub_ID!=0, tp.hub_ID, tp.blog_ID) as id,
		 		tp.user_ID as user_id, tp.campaign_ID as cid
		 		FROM sapphire_6qube.touchpoints_info tp
	inner join sapphire_analytics.analytics_site s on s.idsite = tp.tracking_id
		 		 WHERE tp.user_ID = :user_ID AND tp.tracking_id != 0 AND tp.created_date <= :datestr
		 		 AND (:idl = 0 OR tp.ID > :idl) ORDER BY tp.ID ASC LIMIT ' . $limit;
		//$statement = self::getPreparedStatement(__FUNCTION__, $limit);
		//if(!$statement)
		//{
			//$statement = $this->setPreparedQuery($q, __FUNCTION__, $limit);
		//}
		$statement = $this->getQube()->GetPDO('piwikdb')->prepare($q);
		$statement->execute(array('user_ID' => $user_ID, 'datestr' => $created_before, 'idl' => $id_greater_than));
		return $statement->fetchAll(PDO::FETCH_OBJ);
	}
		
	function getLeadTrackingIds($start, $end, $checktype, $checktypeid)
	{
		$query	=	'SELECT hex(piwikVisitorId) FROM leads WHERE date(created) BETWEEN :start AND :end AND piwikVisitorId != 0 ';
		$where	=	array('start' => $start, 'end' => $end);
		switch($checktype)
		{
			case 'cid':
				$where['cid'] = $checktypeid;
				$query .= ' AND cid = :cid';
				break;
			default:
				$where['tbl_id']	=	$checktypeid;
//				$where['tbl']	=	$checktype;
				$query .= ' AND hub_id = :tbl_id';
				break;
		}
		
		$stmt	=	$this->prepare($query);
		$stmt->execute($where);
		
		return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	}
	
	function getSearchEngineLeads($start, $end, $checktype, $checktypeid)
	{
		$visitorIds	=	$this->getLeadTrackingIds($start, $end, $checktype, $checktypeid);
	}
	
	function getSearchEngineVisits($start, $end, $checktype, $checktypeid)
	{
		$query	=	'SELECT searchengine, SUM(nb_visits) FROM piwikreports_se WHERE point_date BETWEEN :start AND :end AND point_type = "DAY" ';
		$where	=	array('start' => $start, 'end' => $end);
		switch($checktype)
		{
			case 'cid':
				$where['cid'] = $checktypeid;
				$query .= ' AND cid = :cid';
				break;
			default:
				$where['tbl_id']	=	$checktypeid;
				$where['tbl']	=	$checktype;
				$query .= ' AND tbl = :tbl AND tbl_id = :tbl_id';
				break;
		}
		
		$query .= ' GROUP BY searchengine';
		
		$stmt	=	$this->prepare($query);
		
		$stmt->execute($where);
		
		return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	}


        /**
	 * 
	 * @staticvar null $stmt
	 * @return PDOStatement
	 */
	function getSaveStatStatement()
	{
		return $stmt;
	}
	
	function SaveLeadsStat(StatPoint $obj)
	{
		static $stmt	=	NULL;
		if(!$stmt){
			$stmt	=	$this->getQube()->GetPDO()
					->prepare('INSERT INTO ' . self::STATSTBL . ' SET tbl = :tbl, 
						tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
										point_date = :point_date, point_type = :point_type,
										nb_leads = :point_value
								ON DUPLICATE KEY UPDATE nb_leads = :point_value');
		}
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value
				);
		$stmt->execute($data);
	}


	
	function SaveVisitsStat(StatPoint $obj)
	{
		static $stmt	=	NULL;
		if(!$stmt){
			$stmt	=	$this->getQube()->GetPDO()
					->prepare('INSERT INTO ' . self::STATSTBL . ' SET tbl = :tbl, 
						tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
										point_date = :point_date, point_type = :point_type,
										nb_visits = :point_value,
										bounce_count = :bounce_count
								ON DUPLICATE KEY UPDATE nb_visits = :point_value, bounce_count = :bounce_count');
		}
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'bounce_count' => $obj->bounce_count
				);
		$stmt->execute($data);
	}
	
	function getPreparedInsertStmt($obj, $tbl, $action = 'INSERT')
	{
		$updtCols	=	array();
		foreach(array_keys($obj) as $key)
		{
			$updtCols[]	=	"`$key` = :$key";
		}
		$q	=	$action . '	INTO ' . $tbl . ' SET ' . implode(', ', $updtCols);
		return $q;
	}
	
	function SaveReferralKeyword(array $data)
	{
		static $stmt	=	NULL;
		if(!$stmt)
		{
			$query	=	$this->getPreparedInsertStmt($data, 'piwikreports_keywords');
			$stmt	=	$this->prepare($query);
		}
		$stmt->execute($data);
		return $stmt;
	}

	function SaveSearchEngineLeadPoint($obj)
	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_se SET tbl = :tbl,
										tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
							point_date = :point_date, point_type = :point_type, nb_leads = :point_value, searchengine = :searchengine, bounce_count = :bounce_count
								ON DUPLICATE KEY UPDATE nb_leads = :point_value, bounce_count = :bounce_count');
		}
		
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'searchengine' => $obj->searchengine,
				'bounce_count' => $obj->bounce_count
		);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function SaveSearchEngineVisitPoint($obj)

	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_se SET tbl = :tbl,
										tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
							point_date = :point_date, point_type = :point_type, nb_visits = :point_value, searchengine = :searchengine, bounce_count = :bounce_count
								ON DUPLICATE KEY UPDATE nb_visits = :point_value, bounce_count = :bounce_count');
		}
		
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'searchengine' => $obj->searchengine,
				'bounce_count' => $obj->bounce_count
			);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function SaveReferralVisitPoint($obj)
	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_ref SET tbl = :tbl,
									tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
					point_date = :point_date, point_type = :point_type, nb_visits = :point_value, referrer = :referrer, bounce_count = :bounce_count
					ON DUPLICATE KEY UPDATE nb_visits = :point_value, bounce_count = :bounce_count');
		}
		
#		var_dump($obj);
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'referrer' => $obj->referrer,
				'bounce_count' => $obj->bounce_count
			);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function SaveSocialVisitPoint($obj)
	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_social SET tbl = :tbl,
									tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
					point_date = :point_date, point_type = :point_type, nb_visits = :point_value, network = :network, bounce_count = :bounce_count
						ON DUPLICATE KEY UPDATE nb_visits = :point_value, bounce_count = :bounce_count');
		}
		
#		var_dump($obj);
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'network' => $obj->referrer,
				'bounce_count' => $obj->bounce_count
			);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function SaveSocialLeadPoint($obj)
	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_social SET tbl = :tbl,
									tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
					point_date = :point_date, point_type = :point_type, nb_leads = :point_value, network = :network, bounce_count = :bounce_count
						ON DUPLICATE KEY UPDATE nb_leads = :point_value, bounce_count = :bounce_count');
		}
		
#		var_dump($obj);
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'network' => $obj->referrer,
				'bounce_count' => $obj->bounce_count
			);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function SaveReferralLeadPoint($obj)
	{	
		static $stmt    =       NULL;
		if(!$stmt){
				$stmt   =       $this->getQube()->GetPDO()
								->prepare('INSERT INTO ' . self::STATSTBL . '_ref SET tbl = :tbl,
									tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
					point_date = :point_date, point_type = :point_type, nb_leads = :point_value, referrer = :referrer, bounce_count = :bounce_count
						ON DUPLICATE KEY UPDATE nb_leads = :point_value, bounce_count = :bounce_count');
		}
		
#		var_dump($obj);
		$data	=	array('tbl' => $obj->tbl,
							'tbl_ID' => $obj->tbl_ID,
							'user_ID' => $obj->user_ID,
				'cid' => $obj->cid,
				'point_date' => $obj->point_date,
				'point_type' => $obj->point_type,
				'point_value' => $obj->point_value,
				'referrer' => $obj->referrer,
				'bounce_count' => $obj->bounce_count
			);

#		var_dump($data);

		$stmt->execute($data);
            return $stmt;
	}
	
	function _dailyStatsQuery()
	{
                static $stmt    =       NULL;
                if(!$stmt){
                        $stmt   =       $this->getQube()->GetPDO()
                                        ->prepare('INSERT INTO ' . self::STATSTBL . ' SET tbl = :tbl,
                                                tbl_ID = :tbl_ID, user_ID = :user_ID, cid = :cid,
                                                                                point_date = :point_date, point_type = :point_type, point_value = :point_value');
                }
                return $stmt;		
	}
	
	protected function _getDailyHubStats(PointsSetBuilder $PSB, $HUBID, $statcol = 'nb_visits')
	{
		$PDO	=	$this->getQube()->GetPDO();
		
		$query	=	'select unix_timestamp(r.point_date), sum(' . $statcol . ') as point_value from piwikreports r inner join hub h on (h.ID = r.tbl_ID)
							where r.point_date > date_sub(now(), INTERVAL ' . (int)$PSB->vc->interval . ' DAY) AND h.id = ' . (int)$HUBID . '
									group by point_date';
		
		$stmt	=	$PDO->query($query);
		return $stmt;
	}
	
	protected function _getMonthlyHubStats(PointsSetBuilder $PSB, $HUBID, $statcol = 'nb_visits')
	{
		$PDO	=	$this->getQube()->GetPDO();
		
		$query	=	"select unix_timestamp(date_format(r.point_date, \"%Y-%m-01\")), sum($statcol) as point_value from piwikreports r inner join hub h on (h.ID = r.tbl_ID)
									where r.point_date > date_sub(now(), INTERVAL " . (int) $PSB->vc->interval . ' MONTH) AND h.id = ' . (int)$HUBID . '
											group by year(r.point_date), month(r.point_date)';
		
		$stmt	=	$PDO->query($query);
		return $stmt;
	}
	
	protected function _getDailyUserGlobalStats(PointsSetBuilder $PSB, $UID, $cid = NULL)
	{
		$PDO	=	$this->getQube()->GetPDO();
		if($cid){
			$query	=	'select unix_timestamp(point_date), sum(nb_visits) as point_value from piwikreports r
							where r.point_date > date_sub(now(), INTERVAL ' . (int)$PSB->vc->interval . ' DAY) AND r.user_ID = ' . (int)$UID . ' AND cid = ' . $cid . '
									group by point_date';
		} else {
			
			$query	=	'select unix_timestamp(point_date), sum(nb_visits) as point_value from piwikreports r
							where r.point_date > date_sub(now(), INTERVAL ' . (int)$PSB->vc->interval . ' DAY) AND r.user_ID = ' . (int)$UID . '
									group by point_date';	
		}
		
		
		$stmt	=	$PDO->query($query);
		return $stmt;
	}
	
	protected function _getMonthlyUserGlobalStats(PointsSetBuilder $PSB, $user_ID)
	{
		$PDO	=	$this->getQube()->GetPDO();
		
		$query	=	'select unix_timestamp(date_format(r.point_date, "%Y-%m-01")), sum(nb_visits) as point_value
									from piwikreports r 
									where r.point_date > date_sub(now(), INTERVAL ' . (int) $PSB->vc->interval . ' MONTH) AND r.user_id = ' . (int)$user_ID . '
											group by year(r.point_date), month(r.point_date)';
		
		$stmt	=	$PDO->query($query);
		return $stmt;
	}

	public function getBlogStats(PointsSetBuilder $PSB, $BLOGID, $stat = 'nb_visits', $type = 'graph')
	{
		switch ($PSB->vc->period) {
			case 'DAY':
				$stmt = $this->_getDailyBlogStats($PSB, $BLOGID, $stat);
				break;
			case 'MONTH':
				$stmt = $this->_getMonthlyBlogStats($PSB, $BLOGID, $stat);
				break;
			case 'ALL':
				$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => 12, 'period' => 'MONTH')), $PSB->end);
				return $this->_getAllBlogStats($point, $BLOGID, $stat);
		}

		$points = $PSB->createDataSet($stmt, $type);
		return $points;
	}

	protected function _getDayKeywords(DateTime $DT)
	{}
	
	/**
	 * Queries the piwikreports table.
	 * 
	 * @param PointsSetBuilder $PSB
	 * @param type $cid
	 * @return type
	 */
	protected function _getDailyCampaignStat(PointsSetBuilder $PSB, $cid = 0, $statcol = 'nb_visit', $touchpointType = '')
	{
		$PDO	=	$this->getQube()->GetPDO();

//		$query	=	'select unix_timestamp(r.point_date), sum(nb_visits) as point_value from piwikreports r inner join hub h on (h.ID = r.tbl_ID)
//							where r.point_date > date_sub(now(), INTERVAL ' . (int)$PSB->vc->interval . ' DAY) AND h.cid = ' . (int)$cid . '
//									group by point_date';
		$join = '';
		$where = '';
		switch($touchpointType){
			case '':
				break;
			case 'WEBSITE':
			case 'LANDING':
			case 'PROFILE':
			case 'SOCIAL':
			case 'MOBILE':
				$join = "LEFT JOIN hub ON hub.ID = tbl_id AND tbl = 'hub'";
				$where = " AND touchpoint_type = '$touchpointType'";
				break;
			case 'BLOG':
				$join = "LEFT JOIN blogs ON blogs.ID = tbl_ID AND tbl = 'blog'";
				$where = " AND tbl = 'blog'";
				break;
		}

		$query	=	'select unix_timestamp(r.point_date), sum(' . $statcol . ") as point_value from piwikreports r $join
							where r.point_date > date_sub(now(), INTERVAL " . (int)$PSB->vc->interval . ' DAY) AND r.user_ID = ' . $this->getActionUser()->getID() . $where;
		
		if($cid) 
			$query .= ' AND r.cid = ' . (int)$cid;
		
		$query .= ' group by point_date';
		
		$stmt	=	$PDO->query($query);
		return $stmt;
	}
	
	protected function _getMonthlyCampaignStats(PointsSetBuilder $PSB, $cid, $colstat = 'nb_visits', $touchpointType = '')
	{
		$PDO	=	$this->getQube()->GetPDO();

		$join = '';
		$where = '';
		switch($touchpointType){
			case '':
				break;
			case 'WEBSITE':
			case 'LANDING':
			case 'SOCIAL':
			case 'PROFILE':
			case 'MOBILE':
				$join = "LEFT JOIN hub ON hub.ID = tbl_id AND tbl = 'hub'";
				$where = " AND touchpoint_type = '$touchpointType'";
				break;
			case 'BLOG':
				$join = "LEFT JOIN blogs ON blogs.ID = tbl_ID AND tbl = 'blog'";
				$where = " AND tbl = 'blog'";
				break;
		}

		$query	=	'select unix_timestamp(date_format(r.point_date, "%Y-%m-01")), sum(' . $colstat . ") as point_value from piwikreports r $join
									where r.point_date > date_sub(now(), INTERVAL " . (int) $PSB->vc->interval . ' MONTH) AND r.user_ID = ' . $this->getActionUser()->getID() . $where;

		if($cid)
			$query .= ' AND r.cid = ' . (int)$cid;



		$query .= ' group by year(r.point_date), month(r.point_date)';

		$stmt	=	$PDO->query($query);
		return $stmt;
	}
	
	function getCampaignStats(PointsSetBuilder $PSB, $CID, $stat = 'nb_visits', $type = 'chart', $touchpointType = '')
	{
//		$VC	=	new VarContainer(array('interval' => $interval, 'period' => $period));
//		$PSB	=	new PointsSetBuilder($VC);
		switch($PSB->vc->period)
		{
			case 'DAY':
				$stmt	= $this->_getDailyCampaignStat($PSB, $CID, $stat, $touchpointType);
				break;
			case 'MONTH':
				$stmt	= $this->_getMonthlyCampaignStats($PSB, $CID, $stat, $touchpointType);
				break;
			case 'ALL':
//				$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => 12, 'period' => 'MONTH')), $PSB->end);
				return $this->_getAllCampaignlStats($PSB, $CID, $stat, $type);

		}
		
		$points	=	$PSB->createDataSet($stmt, $type);
		
		return $points;
	}
	
	function getHubStats(PointsSetBuilder $PSB, $HUBID, $stat = 'nb_visits', $type = 'graph')
	{
		switch($PSB->vc->period)
		{
			case 'DAY':
				$stmt	=	$this->_getDailyHubStats($PSB, $HUBID, $stat);
				break;
			case 'MONTH':
				$stmt	=	$this->_getMonthlyHubStats($PSB, $HUBID, $stat);
				break;
			case 'ALL':
				$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => 12, 'period' => 'MONTH')), $PSB->end);
				return $this->_getAllHubStats($point, $HUBID, $stat);
		}
		
		$points	=	$PSB->createDataSet($stmt, $type);
		return $points;
	}
	
	function getUserGlobalStats(PointsSetBuilder $PSB, $UID, $type = 'graph', $cid = NULL)
	{
		switch($PSB->vc->period)
		{
			case 'DAY':
				$stmt	=	$this->_getDailyUserGlobalStats($PSB, $UID, $cid);
				break;
			case 'MONTH':
				$stmt	= $this->_getMonthlyUserGlobalStats($PSB, $UID);
			break;
			case 'ALL':
				return $this->_getAllUserGlobalStats($PSB, $UID, $type);
		}
		
		$points	=	$PSB->createDataSet($stmt, $type);
		
		return $points;
	}

    /**
     * Returns an array of STDOBJ containing information about the sites visited after a specified time period.
     *
     * @param string $where
     * @param string $DATESUB
     * @return array
     */
	function getVisitedHubs( $where = '', $DATESUB = '24 HOUR')
	{
        $pdo =  $this->getQube()->GetPDO('piwikdb');
		$q	=	'SELECT v.idsite as tracking_id, h.id, h.touchpoint_type, h.user_id, h.cid
			FROM sapphire_analytics.`analytics_log_visit` v
	inner join sapphire_6qube.6q_trackingids h on (h.tracking_id = v.idsite)
	WHERE v.visit_last_action_time > date_sub(now(), INTERVAL ' . $DATESUB . ') ' .
			($where ? ' AND (' . $where . ') ' : '') . ' group by v.idsite';


		echo $q;
		$ids	=	$pdo->query($q)->fetchAll(PDO::FETCH_OBJ);

	    return $ids;
	}
	
	function getLeadTrackingIdsLast24Hr()
	{
		$query	=	'SELECT hex(piwikVisitorId) FROM leads WHERE created > DATE_SUB(CURDATE(), INTERVAL 24 Hour) AND 
				piwikVisitorId != 0 ';
		
		return $this->query($query)->fetchAll(PDO::FETCH_COLUMN, 0);
	}
	
	function getLeadVisitorIds(array $tracking_ids, $interval = 1, $period = 'DAY')
	{
//		$Yesterday	=	date(PointsSetBuilder::MYSQLFORMAT, mktime(-24));
        $today = date('Y-m-d');
		$query	=	'SELECT lower(hex(l.piwikVisitorId)) FROM leads l 
				INNER JOIN hub h ON h.id = l.hub_id
				where l.spam = 0
				 AND l.created > DATE_SUB("' . $today . '", INTERVAL ' . $interval . ' ' . $period . ') AND
                    piwikVisitorId != ""
				AND h.tracking_id IN (' . join(', ', $tracking_ids) . ' )';
		
		return $this->query($query)->fetchAll(PDO::FETCH_COLUMN, 0);
	}
	
	function getTopPerformingKeywords(array $args)
	{
        //Definitive Args
        $args['user_id']	=	$this->getActionUser()->getID();
		$qOpts	=	self::getQueryOptions($args);
        $where	=	array();


		// If touchpoint type is filtering keyboards, that is other than blogs.
        if (isset($args['touchpoint_type']) && $args['touchpoint_type'] != 'BLOG' && $args['touchpoint_type'] != ''){
            $qstr	=	'SELECT concat(left(label, :strlen), if(length(label) > :strlen, "..", "")), sum(nb_visits) as nb_visits
						FROM piwikreports_keywords INNER JOIN hub ON (piwikreports_keywords.tbl_ID = id) WHERE %s'
                . ' GROUP BY label ORDER BY sum(nb_visits) DESC LIMIT :limit';
            if(isset($args['campaign_id']))
            {
                $where[]	=	'piwikreports_keywords.cid = :campaign_id';
            }
            $where[]    =   "piwikreports_keywords.tbl = 'hub'";
            $where[]    =   "hub.touchpoint_type = :touchpoint_type";
        }else{

            // If not being filtered by touchpoints.

            $qstr	=	'SELECT concat(left(label, :strlen), if(length(label) > :strlen, "..", "")), sum(nb_visits) as nb_visits
						FROM piwikreports_keywords WHERE %s'
                . ' GROUP BY label ORDER BY sum(nb_visits) DESC LIMIT :limit';
            // - If its filtered but only by BLOGS.
            if($args['touchpoint_type'] == 'BLOG'){
                $where[]    =   "tbl = 'blog'";
            }
            if(isset($args['campaign_id']))
            {
                $where[]	=	'cid = :campaign_id';
            }
        }
        // Definitive Wheres.
        $where[] = 'piwikreports_keywords.user_id = :user_id';
        $where[] =  'piwikreports_keywords.label != "Keyword not defined"';
        // Additional Wheres with conditions.

        if(!isset($args['limit']))
        {
            $args['limit']	=	5;
        }
        $args['strlen']	=	66;

        // PointSetBuilder.
        /* @var $psb PointsSetBuilder */
        $psb	=	$args['timecontext'];
        unset($args['timecontext']);
        if(!($psb->isalltime))
        {
            $args['d1']	=	$psb->getStartDate();
            $args['d2']	=	$psb->getEndDate();
            $where[]	=	'point_date BETWEEN :d1 AND :d2';
        }

        //Individual table filter
        if(isset($args['tbl']) && isset($args['tbl_ID'])){
            $where[] = 'tbl = :tbl AND tbl_ID = :tbl_ID';
        }


        //Query Do Process
		$q = sprintf($qstr, implode(' AND ', $where));
		$stmt	=	$this->prepare($q);
		self::callPreparedQuery($stmt, $args);
		if($qOpts['getStatement'])
		{
			return $stmt;
		}
		
		return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
	}

	function addSite(HubModel $hub){
		$name = urlencode($hub->name);
		$httphostkey = urlencode($hub->httphostkey);
		$token	=	Qube::get_settings('analytics_token_auth');
		$url = "http://6qube.mobi/index.php?module=API&method=SitesManager.addSite&format=JSON&token_auth=$token&siteName={$name}&urls[0]={$httphostkey}";
		$json = json_decode(file_get_contents($url));
		if(!$json || !$json->value){
			Qube::LogError('Unable to create json piwik id', $json, $url, $hub);
			return;
		}
		$id = $json->value;
		
		$q = "UPDATE hub SET tracking_id = $id WHERE id = {$hub->id}";
		$stmnt = $this->query($q);//getQube()->GetDB()->prepare($q);
//		$stmnt->execute();
		return $id;
	}


	protected function _getAllUserGlobalStats(PointsSetBuilder $PSB, $UID, $type = 'graph'){
		$date = $PSB->start;	//new DateTime($this->getOldestPiwikreportsDate(array('user_id' => $UID)));
		$interval = $date->diff(new DateTime());
		if($interval->y >= 1 || $interval->m < 3){
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->y * 12 + $interval->m, 'period' => 'MONTH')), $date);
			return $this->getUserGlobalStats($point, $UID, $type);
		}else{
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->days, 'period' => 'DAY')), $date);
			return $this->getUserGlobalStats($point, $UID, $type);
		}
	}

	protected function _getAllCampaignlStats(PointsSetBuilder $PSB, $CID, $stat = 'nb_visits', $type = 'graph'){
		$date = $PSB->start;	//new DateTime($this->getOldestPiwikreportsDate(array('cid' => $CID)));
		$interval = $date->diff(new DateTime());
		if($interval->y >= 1 || $interval->m < 3){
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->y * 12 + $interval->m, 'period' => 'MONTH')), $date);
			return $this->getCampaignStats($point, $CID, $stat, $type, $PSB->vc->getValue('touchpointType', ''));
		}else{
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->days, 'period' => 'DAY')), $date);
			return $this->getCampaignStats($point, $CID, $stat, $type, $PSB->vc->getValue('touchpointType', ''));
		}
	}

	protected function _getAllHubStats(PointsSetBuilder $PSB, $HUBID, $type = 'graph'){
		$date = $PSB->start; //$date = new DateTime($this->getOldestPiwikreportsDate(array('tbl_id' => $HUBID)));
		$interval = $date->diff(new DateTime());
		if($interval->y >= 1 || $interval->m < 3){
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->y * 12 + $interval->m, 'period' => 'MONTH')), $date);
			return $this->getHubStats($point, $HUBID, $type);
		}else{
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->days, 'period' => 'DAY')), $date);
			return $this->getHubStats($point, $HUBID, $type);
		}
	}

	public function getOldestPiwikreportsDate($data){
		$user_ID = $data['user_id'];
		$cid = $data['cid'];
		$tbl_id = $data['tbl_id'];

		$args = array();
		$PDO	=	$this->getQube()->GetPDO();

		$query	=	'SELECT min(point_date) FROM piwikreports ';
		$where = array();
		if($user_ID){
			$where[] = "user_ID = :user_id";
			$args['user_id'] = $user_ID;
		}

		if($cid){
			$where[] = "cid = :cid";
			$args['cid'] = $cid;
		}

		if($tbl_id){
			$where[] = "tbl_id = :tbl_id";
			$args['tbl_id'] = $tbl_id;
		}
		$query .= ' WHERE ' . join(' AND ', $where);
		$stmt	=	$PDO->prepare($query);
		$stmt->execute($args);
		return $stmt->fetchColumn();
	}

	public function _getAllBlogStats(PointsSetBuilder $PSB, $BLOGID, $type = 'graph'){
		$date = new DateTime($this->getOldestPiwikreportsDate(array('tbl_id' => $BLOGID)));
		$interval = $date->diff(new DateTime());
		if($interval->y >= 1 || $interval->m < 3){
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->y * 12 + $interval->m, 'period' => 'MONTH')), $date);
			return $this->getHubStats($point, $BLOGID, $type);
		}else{
			$point = new PointsSetBuilder(VarContainer::getContainer(array('interval' => $interval->days, 'period' => 'DAY')), $date);
			return $this->getHubStats($point, $BLOGID, $type);
		}
	}

	public function _getDailyBlogStats(PointsSetBuilder $PSB, $BLOGID, $statcol){
		$PDO	=	$this->getQube()->GetPDO();

		$query	=	'select unix_timestamp(r.point_date), sum(' . $statcol . ') as point_value from piwikreports r inner join blogs b on (b.ID = r.tbl_ID)
							where r.point_date > date_sub(now(), INTERVAL ' . (int)$PSB->vc->interval . ' DAY) AND b.id = ' . (int)$BLOGID . '
									group by point_date';

		$stmt	=	$PDO->query($query);
		return $stmt;
	}

	public function _getMonthlyBlogStats(PointsSetBuilder $PSB, $BLOGID, $statcol){
			$PDO	=	$this->getQube()->GetPDO();

			$query	=	"select unix_timestamp(date_format(r.point_date, \"%Y-%m-01\")), sum($statcol) as point_value from piwikreports r inner join blogs b on (b.ID = r.tbl_ID)
									where r.point_date > date_sub(now(), INTERVAL " . (int) $PSB->vc->interval . ' MONTH) AND b.id = ' . (int)$BLOGID . '
											group by year(r.point_date), month(r.point_date)';

			$stmt	=	$PDO->query($query);
			return $stmt;
	}

	public function saveApiRequest($params){
		$trackingIDs = explode(',', $params['idSite']);
		if(count($trackingIDs) == 1){
			$results[$params['idSite']] = json_decode($params['json']);
		}else{
			$results = json_decode($params['json']);
		}
		foreach($results as $trackingID => $site)
			foreach($site as $date => $json){

				$q = "REPLACE INTO piwik_apidata (apimethod, tracking_ID, datestr, json) VALUES(:apimethod, :tracking_id, :datestr, :json)";
				$stmt = $this->prepare($q);
				$stmt->execute(array(
					'apimethod' => $params['method'],
					'tracking_id' => $trackingID,
					'datestr' => $date,
					'json' => json_encode($json)
				));
			}
	}

	/**
	 * @param $tracking_id
	 * @param $periodDays
	 * @return int
	 */
	public function getTotalVisitsByTrackingId($tracking_id, $periodDays){

		$touchpoint = $this->query("SELECT * FROM (SELECT id hub_id, 0 blog_id, tracking_id, created FROM hub WHERE trashed = 0 union SELECT 0 hub_id, id blog_id, tracking_id, created FROM blogs WHERE trashed = 0) touchpoints WHERE tracking_id = $tracking_id ORDER BY created desc LIMIT 1")->fetchObject();

		if(!$touchpoint)
			return 0;

		$tbl = $touchpoint->hub_id ? 'hub' : 'blog';
		$tbl_id = $touchpoint->hub_id ? $touchpoint->hub_id : $touchpoint->blog_id;
		list($start_date, $end_date) = explode(',', $periodDays);
		$visits = $this->query("SELECT ifnull(SUM(nb_visits), 0) FROM piwikreports WHERE tbl = '$tbl' AND tbl_ID = $tbl_id AND point_date BETWEEN '$start_date' AND '$end_date'")->fetchColumn();

		return $visits;
	}

	function addBlogToPiwik(BlogModel $blog){
		$name = urlencode($blog->blog_title);
		$httphostkey = urlencode($blog->httphostkey);
		$token	=	Qube::get_settings('analytics_token_auth');
		$url = "http://6qube.mobi/index.php?module=API&method=SitesManager.addSite&format=JSON&token_auth=$token&siteName={$name}&urls[0]={$httphostkey}";
		$json = json_decode(file_get_contents($url));
		if(!$json || !$json->value){
			Qube::LogError('Unable to create json piwik id', $json, $url, $blog);
			return;
		}
		$id = $json->value;

		$q = "UPDATE blogs SET tracking_id = $id WHERE id = {$blog->id}";
		$stmnt = $this->query($q);//getQube()->GetDB()->prepare($q);
//		$stmnt->execute();
		return $id;
	}
}

