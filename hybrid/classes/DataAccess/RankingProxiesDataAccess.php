<?php

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/29/15
 * Time: 3:12 PM
 */
class RankingProxiesDataAccess extends QubeDataAccess{
	/**
	 * Called when a proxy couldn't connect
	 * @param string $proxy
	 * @param string $searchengine
	 * @return bool
	 * @throws QubeException
	 */
	public function incrementProxyTimeOutError($proxy, $searchengine){
		$error_timeout_count = $this->getErrorTimeoutCountField($searchengine);
		$total_attempts = $this->getTotalAttemptsField($searchengine);

		$q = "INSERT INTO ranking_proxys (proxy, $error_timeout_count, $total_attempts) VALUES (:proxy, 1, 1) " .
			"ON DUPLICATE KEY UPDATE $error_timeout_count = $error_timeout_count + 1, $total_attempts = $total_attempts + 1";
		$stmt = $this->prepare($q);

		return $stmt->execute(array('proxy' => $proxy));
	}

	/**
	 * Called when a proxy didn't get enough results for a searchengine
	 * @param string $proxy
	 * @param string $searchengine
	 * @return bool
	 * @throws QubeException
	 */
	public function incrementProxyKeywordError($proxy, $searchengine){
		$error_keyword_count = $this->getErrorKeywordCountField($searchengine);
		$total_attempts = $this->getTotalAttemptsField($searchengine);

		$q = "INSERT INTO ranking_proxys (proxy, $error_keyword_count, $total_attempts) VALUES (:proxy, 1, 1) " .
			"ON DUPLICATE KEY UPDATE $error_keyword_count = $error_keyword_count + 1, $total_attempts = $total_attempts + 1";
		$stmt = $this->prepare($q);

		return $stmt->execute(array('proxy' => $proxy));
	}

	/**
	 * Called always when a proxy is used
	 * @param string $proxy
	 * @param string $searchengine
	 * @return bool
	 * @throws QubeException
	 */
	public function incrementProxyAttempts($proxy, $searchengine){
		$total_attempts = $this->getTotalAttemptsField($searchengine);

		$q = "INSERT INTO ranking_proxys (proxy, $total_attempts) VALUES (:proxy, 1) " .
			"ON DUPLICATE KEY UPDATE $total_attempts = $total_attempts + 1";
		$stmt = $this->prepare($q);

		return $stmt->execute(array('proxy' => $proxy));
	}

	/**
	 * Get a random active proxy based in the response quality (timeouts and keywords)
	 * @param string $searchengine
	 * @return string
	 * @throws QubeException
	 */
	public function getPreferredProxy($searchengine){
		$total_attempts = $this->getTotalAttemptsField($searchengine);
		$error_timeout_count = $this->getErrorTimeoutCountField($searchengine);
		$error_keyword_count = $this->getErrorKeywordCountField($searchengine);

		$q = "SELECT proxy FROM ranking_proxys WHERE active = 1 ORDER BY IF($total_attempts != 0, ($total_attempts - $error_timeout_count * 0.75 - $error_keyword_count * 0.25) / $total_attempts, 1) * RAND() DESC LIMIT 1";
		$stmt = $this->prepare($q);
		$stmt->execute();
		return $stmt->fetchColumn(0);
	}

	/**
	 * Get a random active proxy
	 * @return string
	 */
	public function getRandomProxy(){
		$q = "SELECT proxy FROM ranking_proxys WHERE active = 1 ORDER BY id * RAND() LIMIT 1";
		$stmt = $this->prepare($q);
		$stmt->execute();
		return $stmt->fetchColumn(0);
	}

	/**
	 * Returns all proxies
	 * @return string[]
	 */
	public function getAllProxies(){
		return $this->query('SELECT proxy FROM ranking_proxys')->fetchAll(PDO::FETCH_COLUMN);
	}

	/**
	 * Returns all active proxies
	 * @return string[]
	 */
	function getActiveProxies(){
		return $this->query("SELECT proxy FROM ranking_proxys WHERE active = 1")->fetchAll(PDO::FETCH_COLUMN);
	}

	/**
	 * Set or unset active proxy, 1 means active
	 * @param string $proxy
	 * @param int $active
	 * @return int|mixed
	 */
	function setActiveProxy($proxy, $active = 1){
		return $this->getPDO()->exec("UPDATE ranking_proxys SET active = $active WHERE proxy = '$proxy'");
	}

	/**
	 * Set all proxies to the original state
	 * @return int|mixed
	 */
	public function initializeProxies(){
		return $this->getPDO()->exec(
			'UPDATE ranking_proxys SET ' .
				'error_keyword_count_google = 0, ' .
				'error_timeout_count_google = 0, ' .
				'total_attempts_google = 0, ' .
				'error_keyword_count_bing = 0, ' .
				'error_timeout_count_bing = 0, ' .
				'total_attempts_bing = 0'
		);
	}

	/**
	 * Set a proxy quality for searchengine based on ranks value
	 * @param string $proxy
	 * @param string $searchengine
	 * @param int $rank
	 * @return int|mixed
	 * @throws QubeException
	 */
	public function setUpSearchengineProxy($proxy, $searchengine, $rank){
		$error_keyword_count = $this->getErrorKeywordCountField($searchengine);
		$total_attempts = $this->getTotalAttemptsField($searchengine);

		return $this->getPDO()->exec("UPDATE ranking_proxys SET $error_keyword_count = $error_keyword_count + $rank, $total_attempts = $total_attempts + $rank WHERE proxy = '$proxy'");
	}

	/**
	 * Get the error_keyword_count_field for the searchengine specified
	 * @param string $searchengine
	 * @return string
	 * @throws QubeException
	 */
	private function getErrorKeywordCountField($searchengine){
		switch($searchengine){
			case 'google':
				$field = 'error_keyword_count_google';
				break;
			case 'bing':
				$field = 'error_keyword_count_bing';
				break;
			default:
				throw new QubeException("Searchengine $searchengine not supported");
		}

		return $field;
	}

	/**
	 * Get the timeout_count_field for the searchengine specified
	 * @param string $searchengine
	 * @return string
	 * @throws QubeException
	 */
	private function getErrorTimeoutCountField($searchengine){
		switch($searchengine){
			case 'google':
				$field = 'error_timeout_count_google';
				break;
			case 'bing':
				$field = 'error_timeout_count_bing';
				break;
			default:
				throw new QubeException("Searchengine $searchengine not supported");
		}
		return $field;
	}

	/**
	 * Get the total_attempts_field for the searchengine specified
	 * @param string $searchengine
	 * @return string
	 * @throws QubeException
	 */
	private function getTotalAttemptsField($searchengine){
		switch($searchengine){
			case 'google':
				$field = 'total_attempts_google';
				break;
			case 'bing':
				$field = 'total_attempts_bing';
				break;
			default:
				throw new QubeException("Searchengine $searchengine not supported");
		}
		return $field;
	}

	/**
	 * @param $proxy
	 * @return int
	 */
	public function updateLastUpdated($proxy)
	{
		return $this->query("UPDATE ranking_proxys SET last_updated = CURDATE() WHERE proxy = '$proxy'")->rowCount();
	}
}