<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 12/4/14
 * Time: 10:01 AM
 */
require_once(QUBEADMIN . 'inc/hub.class.php');

class WebsiteDataAccess extends QubeDataAccess{

    function PerformPageValuePropagation(PageModel $master, array $column_changes, $keepCustomVals = TRUE)
    {
        $master_page_id =   $master->getID();
        // check if there are slave pages
        $num_slaves = $this->query('SELECT count(*) FROM hub_page2 WHERE page_parent_id = ' . $master_page_id)->fetchColumn();
        if(0 == $num_slaves)
        {
            // no slaves
            return FALSE;
        }

        if(isset($column_changes['nav_parent_id']))
        {
            $newvalue = (int)$column_changes['nav_parent_id'];
//            case 'nav_parent_id':   // choose the page id belonging to the slave hub
                if($newvalue != 0)  // non-root nav
                {
                    // select the ids of slavepage (based on page_parent_id) and set them as the nav_id of the corresponding slave pages (based on hub_id)
                    $query = "update hub_page2 p, (select id, hub_id from hub_page2 where page_parent_id = $newvalue) k
                        set p.nav_parent_id = k.id
                        where p.page_parent_id = $master_page_id and p.hub_id = k.hub_id";
                    $this->query($query);
                    unset($column_changes['nav_parent_id']);
                }else{
//                    $query = 'UPDATE hub_page2 SET nav_parent_id = 0 WHERE '
                    $keepCustomVals =   false;  // skip original-check
                }
        }

        $key_changes    =   array_intersect($master->_getColumns(), array_keys($column_changes));
        $master_info = $this->getMasterPageInfo($master_page_id, $key_changes);

//        $master_keys    =   array_keys($column_changes);
        foreach($key_changes as $column)
        {
            $newvalue   =   $column_changes[$column];
            $original = $master_info->get($column);
            $args   =   array('hub_id' => $master_info->hub_id, 'newvalue' => $newvalue);
            $query = "UPDATE hub_page2 p inner join hub h on (h.id = p.hub_id)
                SET p.`$column` = :newvalue
                WHERE (h.hub_parent_id = :hub_id)
                AND (p.page_parent_id = $master_page_id)";
            if($keepCustomVals){
                $query .= ' AND p.`' . $column . '` = :originalvalue';
                $args['originalvalue']  =   $original;
            }
            $pstmt = $this->prepare($query);
            $pstmt->execute($args);
        }
        return TRUE;
    }

    /**
     * Returns basic page info including hub_id
     *
     * @return PageModel
     */
    function getMasterPageInfo($page_id, array $other = array())
    {
        if($other)
            $otherstr = count($other) > 1 ? ', `' . implode('`, `', $other) . '`' : ", `" . end($other) . "`";
        else $otherstr = '';

        $obj = $this->query("SELECT user_id, hub_id,
          nav_parent_id, `type`, `nav_order`, page_title, page_title_url{$otherstr}
           FROM hub_page2 WHERE id = {$page_id}")->fetchObject('PageModel');
        return $obj;
    }



    function CreateNewPageInSlaveHubs($page_id)
    {
        $page_info  =   $this->getMasterPageInfo($page_id);
        $isMultiSitePage = $this->isMultiSiteHub($page_info->hub_id);
        if(FALSE === $isMultiSitePage)
        {
            return false;
        }
        // loop through slave hubs
        $slaves = $this->query("SELECT id, user_id, user_parent_id, tracking_id, name
            FROM hub WHERE
            hub_parent_id = {$page_info->hub_id} AND id != {$page_info->hub_id}
            AND (user_parent_id = {$page_info->user_id} OR user_id = {$page_info->user_id})
            AND pages_version = 2
            ")->fetchAll(PDO::FETCH_OBJ);

        if(0 == count($slaves))
        {
            return false; // nothing to do!
        }

        $query = 'SELECT @nav_parent := 0, @nav_root := 0;';
        $prepared_navquery  =   $this->prepare($query);
        // if the page is a root page (no parent)
        if(0    ==  $page_info->nav_parent_id)
        {
            $prepared_navquery->execute();
        }
        $query = "INSERT INTO hub_page2
							(hub_id, type, nav_parent_id, nav_root_id, nav_order,
							user_id, user_parent_id, page_parent_id, page_title, page_title_url, created)
							VALUES
							(:hub_id, :page_type, @nav_parent, @nav_root, :page_order,
							 :user_id, :user_parent, :master_page, :page_title,
							 :page_title_url, NOW())";
        $prepared_insert = $this->prepare($query);
        $this->beginTransaction();
        foreach($slaves as $slave)
        {
            if(0    !=  $page_info->nav_parent_id)
            {
                $query = "SELECT @nav_parent := id, @nav_root := nav_root_id
                            FROM hub_page2
                          WHERE page_parent_id = {$page_info->nav_parent_id}
                            AND hub_id = {$slave->id}";
                if($this->query($query)->rowCount() == 0)
                {
                    // if we couldnt find the master page (nav) for the master nav, something
                        // is seriously screwed up.. lets use 0 values anyway (prepared_navquery)
                    $prepared_navquery->execute();
                    Qube::LogError('Error Creating Slave Page. Could Not Find Nav: ', $page_info, $page_id);
                }
            }
            $q_args =   array('hub_id' => $slave->id,
                    'page_type' => $page_info->type,
                    'page_order' => $page_info->nav_order,
                    'user_id' => $slave->user_id,
                    'user_parent' => $slave->user_parent_id,
                    'master_page' => $page_id,
                    'page_title' => $page_info->page_title,
                    'page_title_url' => $page_info->page_title_url);
            $prepared_insert->execute($q_args);
        }
        $this->commit();

        return TRUE;
    }

    /**
     *
     * @param $hub_ID
     * @return bool
     */
    function isMultiSiteHub($hub_ID)
    {
        $q = $this->query('SELECT multi_user FROM hub WHERE id = ' . (int)$hub_ID);
        //var_dump('SELECT multi_user FROM hub WHERE id = ' . (int)$hub_ID);
        return $q->fetchObject('WebsiteModel')->isMultiSite();
    }

	/**
	 * @param PageModel $page
	 * @return int
	 */
	public function getNavOrder($hub_id){
		$stmt = $this->prepare('SELECT nav_order FROM hub_page2 WHERE hub_id = :hub_id ORDER BY nav_order DESC LIMIT 1');
		$stmt->execute(array(
			'hub_id' => $hub_id
		));
		return intval($stmt->fetchColumn());
	}

	/**
	 * @param int $id
	 * @param string $touchpointType
	 * @return WebsiteModel
	 */
	public function getSite($id, $touchpointType = 'touchpoint'){
		$actionUser = $this->getActionUser();
		$query = "SELECT *, CONCAT_WS(',', keyword1, keyword2, keyword3, keyword4, keyword5, keyword6) as keywords FROM hub";
		$where = array('id = :id');
		$args = array('id' => $id);
		if(!$actionUser->isSystem()){
			if($actionUser->can("view_$touchpointType", $id)){
				$where[] = '(user_parent_id = :parent_id or user_id = :user_id)';
				$args['parent_id'] = $actionUser->getParentID();
			}else{
				$where[] = 'user_id = :user_id';
            }
            $args['user_id'] = $actionUser->getID();
        }

		if(!empty($where)){
			$query .= ' WHERE  ' . join(' AND ', $where);
		}
		$stmt = $this->prepare($query);

		$stmt->execute($args);

		return $stmt->fetchObject('WebsiteModel');
	}

	/**
	 * @param int $campaignID
	 * @param array() $options
	 * @return WebsiteModel[]
	 */
	public function getSites($campaignID, $options = array()){
		$actionUser = $this->getActionUser();
		$where = '';
		$orderBy = '';
		$qOpts	=	(object)self::getQueryOptions($options);
		$whereParts = array('cid = :cid');
//		$options = $options;
		$options['cid']	=	$campaignID;
//		if(TRUE){ // !$actionUser->isSystem()){
			if($actionUser->can('view_campaigns', $campaignID)){
				$whereParts[] = '(user_parent_id = :parent_id or user_id = :user_id)';
				$options['parent_id'] = $actionUser->getParentID();
			}else{
				$whereParts[] = 'user_id = :user_id';
            }
            $options['user_id'] = $actionUser->getID();
//        }
		if($qOpts->where){
			$whereParts[] = "(".strval($qOpts->where).")";
		}

		if($options['sSearch']){
			$whereParts[] = 'name LIKE :sSearch';
		}

		if(!empty($whereParts)){
			$where = 'WHERE  ' . join(' AND ', $whereParts);
		}

		if($options['orderBy'] && !empty($options['orderBy'])){
			$orderBy = 'ORDER BY ' . join(', ', $options['orderBy']) . ' DESC';
			unset($options['orderBy']);
		}

		if($options['bindParams'] && !empty($options['bindParams'])){
			$options = array_merge($options, $options['bindParams']);
			unset($options['bindParams']);
		}

		$query = "SELECT * FROM hub $where $orderBy {$qOpts->limitstr}";

		$stmt = $this->prepare($query);

		self::callPreparedQuery($stmt, $options);
//		$stmt->execute($args);

		if($qOpts->returnSingleObj){
			return $stmt->fetchObject('HubModel');
		}
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'WebsiteModel');
	}
	
	function getCampaignTouchPointCounts($campaign_id	=	NULL)
	{

		$fields = array(
			"(select count(*) FROM blogs WHERE user_id = c.user_id and cid = c.id AND trashed = 0) as BLOG",
			"ifnull(sum(`hub`.`touchpoint_type` = 'WEBSITE'), 0) AS `WEBSITE`",
			"ifnull(sum(`hub`.`touchpoint_type` = 'LANDING'), 0) AS `LANDING`",
			"ifnull(sum(`hub`.`touchpoint_type` = 'PROFILE'), 0) AS `PROFILE`",
			"ifnull(sum(`hub`.`touchpoint_type` = 'SOCIAL'), 0) AS `SOCIAL`",
			"ifnull(sum(`hub`.`touchpoint_type` = 'MOBILE'), 0) AS `MOBILE`"
		);
		$groupBy = "";

		$args	=	array(
				'user_id' => $this->getActionUser()->getID()
				);
		$where	=	array('c.user_id = :user_id');
		if($campaign_id)
		{
			$args['campaign_id']	=	$campaign_id;
			$where[] = 'c.id = :campaign_id';
			$fields[] = "c.id";
			$groupBy = "group by c.id";
		}

		$fields = join(', ', $fields);
		$groupQuery = "SELECT $fields from campaigns c left join hub on (hub.cid = c.id and hub.trashed = 0) where %s $groupBy";

		$q	=	sprintf("select x.*, x.LANDING + x.SOCIAL + x.PROFILE + x.WEBSITE + x.MOBILE + x.BLOG as TOTAL
					 from ($groupQuery) x", implode(' AND ', $where));
		$st = $this->prepare($q);
		
		$st->execute($args);
		return $st->fetch(PDO::FETCH_ASSOC);
	}

	static function getrole($site_id){
		$DA = new WebsiteDataAccess();
		$result = $DA->query("SELECT role_ID FROM 6q_roleperms WHERE object_ID = '$site_id'");
		return $result->fetchColumn();
	}

	static function getroles($site_id){
		$DA = new WebsiteDataAccess();
		$result = $DA->query("SELECT role_ID FROM 6q_roleperms WHERE object_ID = '$site_id'");
		$array = $result->fetchAll();
		foreach($array as $row){
			$r_array[] = $row[0];
		}
		return $r_array;
	}

	static function UpdateHubTheme($hub_id,$musite_id,$user_id, $owner_id){
		$hub_subclass = new Hub();
		$result = $hub_subclass->setupMultiUserHubV2($hub_id, $musite_id, $user_id, $owner_id);
		return $result['error'] != 1;
	}

	public function hasFormTagsInPageFields($fields, $hub_id)
	{
		$embedFields = join(', ', array_intersect(PageRowModel::getFormTagFields(), $fields));
		$q = "SELECT $embedFields FROM hub_page2 WHERE id = :page_id AND has_custom_form = 1";
		$stmt = $this->prepare($q);
		$stmt->execute(array('page_id' => $hub_id));
		$rowData = $stmt->fetch(PDO::FETCH_ASSOC);
		if($rowData && HubModel::hasEmbedForm($rowData)){
			return true;
		}

		return false;
	}

	public function hasTagsInPageFields($fields, $ID)
	{
		$embedFields = join(', ', array_intersect(PageRowModel::getParsableFields(), $fields));
		$q = "SELECT $embedFields FROM hub_page2 WHERE id = :page_id AND has_tags = 1";
		$stmt = $this->prepare($q);
		$stmt->execute(array('page_id' => $ID ));
		$rowData = $stmt->fetch(PDO::FETCH_ASSOC);
		if($rowData && HubModel::hasTags($rowData)){
			return true;
		}
		return false;
	}

	public function getPageParentLocks($page_id){
		$q = "SELECT multi_user_fields FROM hub_page2 WHERE id = (SELECT IF(page_parent_id = 0, id, page_parent_id) FROM hub_page2 WHERE id = :page_id)";
		$stmt = $this->prepare($q);
		$stmt->execute(array('page_id' => $page_id));
		/** @var PageModel $page */
		$page = $stmt->fetchObject('PageModel');

		return $page->getFieldLocker();
	}

	public function getNavParentLocks($page_parent_id){
		$q = "SELECT multi_user_fields FROM hub_page2 WHERE id = :page_parent_id";
		$stmt = $this->prepare($q);
		$stmt->execute(array('page_parent_id' => $page_parent_id));
		/** @var NavModel $page */
		$page = $stmt->fetchObject('NavModel');

		return $page->getFieldLocker();
	}

	/**
	 * Get a page using the page_id
	 * @param $page_id
	 * @return PageModel
	 */
	public function getPage($page_id){
		return $this->qube->queryObjects('PageModel', 'T',
			'T.id = %d', $page_id)->Exec()->fetch();
	}

//	public function getMuSites($user_id, $options = array()){
//		$args = array('user_id' => $user_id);
//		$where = array();
//
//		$dataTableArgs = isset($options['dataTableArgs']) ? $options['dataTableArgs'] : false;
//
//		$where[] = 'multi_user = 1';
//		$where[] = 'user_id = :user_id';
//
//		$limit = 'LIMIT 10';
//		if($dataTableArgs){
//			$where[] = "name LIKE CONCAT('%', :search, '%')";
//			$args['search'] = $dataTableArgs['sSearch'];
//
//			$limit = "LIMIT {$dataTableArgs['']}"
//		}
//
//		$where = implode(' AND ', $where);
//		$q = "SELECT name FROM hub WHERE $where $limit";
//	}
}