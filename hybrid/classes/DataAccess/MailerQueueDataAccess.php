<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Qube\Hybrid\DataAccess;

/**
 * Description of MailerQueueDataAccess
 *
 * @author sofus
 */
class MailerQueueDataAccess extends \QubeDataAccess {
	
//	protected $queries	=	array();
	protected function getQuery($data_type)
	{
		$fields	=	'q.data_type, q.q_id, q.data_id, q.created, q.date_sent, q.collab_id, ';
		$joins	=	'';
		$wherestr = '';
		switch($data_type)
		{
			case 'contact':
				$PROCESSOR_CLASS	=	'ContactNotifier';
				$joins = ' INNER JOIN contact_form f on (f.id = q.data_id) ';
				$fields .= ' f.name, f.email, f.phone, f.comments, f.website, f.page, ';
				$wherestr = ' AND q.created > "2014-11-16"';
				break;
			
			case 'lead':
		
				$PROCESSOR_CLASS	=	'LeadNotifier';
				$fields .='
        -- hub info related to form submission
        h.name as hub_name, ifnull(p.page_title, p2.page_title) as page_name,        
        -- get the form name
        n.name as form_name,
        -- basic lead submission info
        f.*,          ';
				
				$joins = '
					INNER JOIN leads     f ON ( f.id = q.data_id )
    INNER JOIN lead_forms n ON ( n.id = f.lead_form_id )
    LEFT JOIN hub        h ON (h.id = f.hub_id)
    LEFT JOIN hub_page2  p2 ON (p2.id = f.hub_page_id AND h.pages_version=2)
    LEFT JOIN hub_page   p ON (p.id = f.hub_page_id AND h.pages_version=1)
			';
				
		}
		$Query	= "SELECT $fields
        -- used for sender domain
        if(r.main_site <> '', ifnull(r.main_site,  'seedlogix.com'), 'seedlogix.com') as reseller_domain,
        -- collaborator info
        c.name as collab_name, c.email as collab_email
        
    FROM collab_email_queue q 
    INNER JOIN campaign_collab c ON ( c.id = q.collab_id )
		$joins
		LEFT JOIN users u ON ( u.id = f.user_id ) 
		LEFT JOIN resellers r ON ( ( (
                    u.parent_id =0
                    AND r.admin_user = u.id
                    )
                    OR r.admin_user = u.parent_id
			))
		WHERE date_sent = 0 AND data_type = '$data_type' AND c.trashed = 0 AND c.active = 1 and q_id > 5156 $wherestr
		ORDER BY q.q_id ASC
		LIMIT 50";
		
//		return $this->query($Query)->fetchAll(PDO::FETCH_CLASS, $PROCESSOR_CLASS);
		return $this->query($Query)->fetchAll(PDO::FETCH_CLASS, $PROCESSOR_CLASS, array($this));
	}
				
	/**
	 * 
	 * @return \LeadNotifier[]
	 * @return LeadNotifier[]
	 */
	function getQueuedLeadNotifications()
	{
		return $this->getQuery('lead');
	}			
	
	/**
	 * 
	 * @return \LeadNotifier[]
	 * @return LeadNotifier[]
	 */
	function getQueuedContactNotifications()
	{
		return $this->getQuery('contact');
	}
	
	/**
	 * 
	 * @staticvar type $updatestatement
	 * @param integer $q_id
	 * @param bool $sent
	 * @return bool
	 */
	function setSentStatus($q_id, $sent)
	{
		static $updatestatement	=	NULL;
		if(!$updatestatement){
			$updateq =   'UPDATE collab_email_queue SET date_sent = if(:sentdate =1, now(), 0) WHERE q_id = :q_id';
			$updatestatement    =   $this->getPDO('master')->prepare($updateq);
		}
		
		$sentdate	=	$sent ? 1 : 0;
		
		return $updatestatement->execute(array('q_id' => $q_id, 'sentdate' => $sentdate));
	}
	
}
