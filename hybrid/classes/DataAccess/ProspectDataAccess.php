<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProspectDataAccess
 *
 * @author sofus
 */
class ProspectDataAccess extends QubeDataAccess {
	
	/**
	 * gets all prospects
	 * params: cid, limit, limit_start, orderCol, orderDir, sSearch, getStatement, cols
	 * 
	 * @param array $args
	 * @return ProspectModel[]
	 */
	function getAccountProspects(array $args)
	{
		$user	=	$this->getActionUser();

		if(!isset($args['limit']))
		{
			$args['limit'] = 10;
		}

		$qOpts	=	self::getQueryOptions($args);
		$where	=	$qOpts['where'] ? $qOpts['where'] : DBWhereExpression::Create(array(
			'spampoints < 21',
			'p.trashed = 0'
			//'p.hub_ID != 0'
		));
		
		$args['user_id']	=	$user->getID();
		if($user->can('view_prospects') && UserModel::ADMIN_CANVIEWALL)
		{
			$args['user_parent_id']	=	$user->getParentID();
			$where->Where('(p.user_parent_ID = :user_parent_id OR p.user_ID = :user_id)');
		}else
			$where->Where('p.user_ID = :user_id');
		
		$cid	=	0;
		if(isset($args['cid']))
		{
			$cid	=	$args['cid'];
			$where->Where('p.cid = :cid');
		}

		if(isset($args['touchpoint_ID']))
		{
//			$cid	=	$args['hub_id'];
			$where->Where('p.touchpoint_ID = :touchpoint_ID');
		}
		
		if(isset($args['sSearch']))
		{
			if(is_numeric($args['sSearch']))
			{
				$where->Where('(p.ID = :sSearch or c.ID = :sSearch)');
			}else{
				$where->Where('(p.name LIKE :sSearch OR p.email LIKE :sSearch)');
			}
		}

		if(isset($args['form_id'])){
			$where->where('(p.form_ID = :form_id)');
		}

		/* @var $psb PointsSetBuilder */
		if(isset($args['timecontext'])){
			$psb = $args['timecontext'];
			unset($args['timecontext']);
			if(!($psb->isalltime))
			{
				$args['d1']	=	$psb->getStartDate();
				$args['d2']	=	$psb->getEndDate();
				$where->Where('DATE(p.created) BETWEEN :d1 AND :d2');
			}
		}

		$orderCol	=	'p.created';
		$orderDir	=	!empty($qOpts['orderDir']) ? $qOpts['orderDir'] : 'desc';
		
		if(isset($qOpts['orderCol'])){
			$orderCol	=	$qOpts['orderCol'];
			switch($orderCol)
			{
				case 'name':
					$orderCol	=	'p.name';
					break;
				case 'TOUCHPOINT_TYPE':
					$orderCol = 'tp.touchpoint_type';
					break;
				case 'grade':
				case 'email':
				case 'phone':
				case 'campaign_name':
				case 'tp.name':
				case 'p.phone':
				case 'p2.page_title':
				case 'p.created':
					break;
				default:
					$orderCol	=	'p.created';
			}
		}

		if(isset($qOpts['cols']) && !empty($qOpts['cols']))
		{
			$fieldnames	=	$qOpts['cols'];
			unset($args['cols']);
		}else{
			$fieldnames	=	'p.ID, grade, p.name, p.email, p.phone, p.source, c.name AS campaign_name, p.created, p.cid, p.contactform_ID, p.lead_ID, p.ip, p.searchengine, p.keywords, p.form_id, f.name form_name';
		}

		$joins	=	'';
		if(isset($args['TOUCHPOINT_TYPE']))
		{
			if(!CampaignController::getTouchPointTypes($args['TOUCHPOINT_TYPE']))
			{
				$args['TOUCHPOINT_TYPE']	=	'';
//				unset($args['TOUCHPOINT_TYPE']);
			}

			if(!$qOpts['isCount'])
				$fieldnames .= ', tp.touchpoint_type AS TP_TYPE, tp.touchpoint_name as TP_NAME, ifnull(p2.page_title, "") as TP_PAGE';

//			$tbl = 'hub';
			$on = 'tp.ID = p.touchpoint_ID';
			$where->Where('(:TOUCHPOINT_TYPE = "" OR tp.touchpoint_type = :TOUCHPOINT_TYPE)');
			$joins .= ' INNER JOIN touchpoints_info tp ON (' . $on . ') LEFT JOIN hub_page2 p2 ON (p2.ID = p.page_ID) ';
		}
		
//		$stmt	=	self::getPreparedStatement(__FUNCTION__, $user->getID(), $cid);
//		if(!$stmt){
		$qsyntax	=	'SELECT%s %s
				FROM prospects p INNER JOIN campaigns c ON (c.id = p.cid)
				 LEFT JOIN lead_forms f ON (p.form_ID = f.id) %s
				WHERE %s %s %s';
			$q	=	sprintf($qsyntax,
								$qOpts['CALC_FOUND_ROWS'], 	// cfr
								$fieldnames,			// fieldnames
								$joins,	// joins
									$where,	// where
				($orderCol ? ' ORDER BY ' . $orderCol . ' ' . $orderDir : ''),	// order by
							$qOpts['limitstr']
							);
			$stmt	=	$this->setPreparedQuery($q, __FUNCTION__, $user->getID(), $cid);
//		}

		return self::HandleQueryResults($stmt, $args, $qOpts, 'ProspectModel');
	}

	/**
	 * Returns prospect object with touchpoint & page info
	 *
	 * @param $prospect_ID
	 * @return mixed
	 */
	function getFullProspectInfo($prospect_ID)
	{
		$q = 'SELECT p.*, t.touchpoint_name, t.touchpoint_type, p2.page_title, coalesce(c.ID, 0) as contact_ID
 		from prospects p
 				left join touchpoints_info t on t.ID = p.touchpoint_ID
 				left join hub_page2 p2 on p2.ID = p.page_ID
 				left join contacts c on (c.prospect_ID = p.ID)
			where p.ID = %d AND p.user_ID = %d AND p.trashed = 0
			 LIMIT 1';

		return	$this->getQube()->squery($q, $prospect_ID, $this->getActionUser()->getID())
				->fetchObject('ProspectModel');
	}
	function getFullProspectInfoExternal($prospect_ID, $userid)
	{
		$q = 'SELECT p.*, t.touchpoint_name, t.touchpoint_type, p2.page_title, coalesce(c.ID, 0) as contact_ID
 		from prospects p
 				left join touchpoints_info t on t.ID = p.touchpoint_ID
 				left join hub_page2 p2 on p2.ID = p.page_ID
 				left join contacts c on (c.prospect_ID = p.ID)
			where p.ID = %d AND p.user_ID = %d AND p.trashed = 0
			 LIMIT 1';

		return	$this->getQube()->squery($q, $prospect_ID, $userid)
				->fetchObject('ProspectModel');
	}

	function addToContacts($prospect_ID, $accountID)
	{
		
		$pros = $this->getFullProspectInfo($prospect_ID);
		$pros->phone = preg_replace('/[^0-9]/', '', $pros->phone);
		
		$q = 'INSERT INTO contacts (user_id, cid, prospect_ID, account_id, first_name, last_name, email, phone, lead_id, created)
			SELECT user_ID, cid, ID, "'.$accountID.'", name, "", email, "'.$pros->phone.'", lead_ID, NOW() from prospects where ID = ' . (int)$prospect_ID;

		return $this->query($q)->rowCount();
	}
	
	function SaveFromLeadID($lead_ID , $phone = '', $args = array())
	{
		$executeArgs = array(
			'phone' => $phone,
		);

		$executeArgs = array_merge($executeArgs, array_intersect_key($args, $executeArgs));

		$q	=	"insert into prospects (ip, user_ID, user_parent_ID, cid,
    touchpoint_ID, page_ID, form_ID, searchengine, keywords,
    visitor_ID, lead_ID, contactform_ID, name, email, phone, created, spampoints, grade, hub_ID)
  select l.ip, l.user_id, l.parent_id, l.cid,
    if(l.touchpoint_id = 0, (select touchpoint_ID FROM hub WHERE id = l.hub_id), touchpoint_id), l.hub_page_id, l.lead_form_id, l.search_engine, l.keyword,
    l.piwikVisitorId, l.ID, 0, l.lead_name, l.lead_email, :phone, l.created, l.spam, l.grade, l.hub_id
    from leads l WHERE l.ID = " . (int)$lead_ID;

		$pdo = $this->getPDO('master');
		$statement = $pdo->prepare($q);
		$statement->execute($executeArgs);
		return $pdo->lastInsertId();
	}
	
	function SaveFromContactDataID($contactform_ID)
	{
		$q	=	'insert into prospects (ip, user_ID, user_parent_ID, cid,
    page_ID, form_ID, searchengine, keywords,
    visitor_ID, lead_ID, contactform_ID, name, email, phone, created, spampoints)
  select c.ip, c.user_id, c.parent_id, c.cid,
    0, 0, c.search_engine, c.keyword,
    "", 0, c.id, c.name, c.email, c.phone, c.created, c.spamstatus*100
    from contact_form c
    where c.type = "hub" AND c.ID = ' . (int)$contactform_ID;
		
		return $this->getPDO('master')->query($q);
	}
	
	function SaveContactForm(ContactDataModel $Contactform)
	{
		$this->getQube()->Save($Contactform);
		$this->SaveFromContactDataID($Contactform->getID());
	}
	
	function getCampaignEmailRecipients($site_ID, $campaign_ID)
	{
		$where	=	array();
		$where[]	=	"(T.scope = 'campaign' OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND r.site_id = $site_ID ) )";
		$where[]	=	"T.campaign_id = $campaign_ID";
		$where[]	=	'T.active = 1';
		$where[]	=	'T.trashed = "0000-00-00"';
		
		return CampaignCollab::Query($where)->Exec()->fetchAll();
	}
	
	function SaveEmailInQueue($data_id, $data_type, $collab_id)
	{
		$stmt	=	self::getPreparedStatement(__FUNCTION__, $data_type);
		if(!$stmt)
		{
			$q	=	'INSERT INTO collab_email_queue (data_id, data_type, collab_id)
							VALUES (?, ?, ?)';
			$stmt	=	$this->setPreparedQuery($q, __FUNCTION__, $data_type);
		}
		return $stmt->execute(array($data_id, $data_type, $collab_id));
	}

	function RetrieveNotes($data){
		$user_id = $this->getActionUser()->getID();
		$query = "SELECT * FROM notes WHERE user_id = :user_id AND contact_id = :contact_id AND cid = :cid AND trashed = 0";
		$statement = $this->prepare($query);
		$statement->execute(array ('user_id' => $user_id, 'contact_id' => $data['contact_id'], 'cid' => $data['cid']));
		return $statement->fetchAll(PDO::FETCH_CLASS,'NoteModel');
	}
	
	function RetrieveLeadNotes($data){
		$user_id = $this->getActionUser()->getID();
		$query = "SELECT * FROM notes WHERE user_id = :user_id AND lead_id = :lead_id AND cid = :cid AND trashed = 0";
		$statement = $this->prepare($query);
		$statement->execute(array ('user_id' => $user_id, 'lead_id' => $data['lead_id'], 'cid' => $data['cid']));
		return $statement->fetchAll(PDO::FETCH_CLASS,'NoteModel');
	}
	
	function getReturnURL($table, $type_id, $user_ID)
	{
		$where	=	array();
		$where[]	=	'`T`.table_name = ?';
		$where[]	=	'`T`.table_id = ?';
		$where[]	=	'`T`.sched = 0 AND `T`.active = 1 AND T.trashed = "0000-00-00"';
		$where[]	=	'(T.user_id = ? OR ? = 0)';
		
		$stmt	=	ResponderModel::Query($where)->Fields('return_url')->Limit(1)->Prepare();
		
		if(!$stmt->execute(array($table, $type_id, $user_ID, $user_ID)))
						return FALSE;
		
		return $stmt->fetchColumn();
	}

	function ValidateSubmissionSource($tbl, $tbl_ID, $col = '1')
	{
		$q	=	'SELECT ' . $col . ' FROM ' . $tbl . ' WHERE id = ' . (int)$tbl_ID;
		return $this->getPDO()->query($q)->fetchColumn();
	}

	public function getProspectPhotoByEmail($lead_email)
	{
		$api = new Services_FullContact_Person(Qube::get_settings('fullcontact_api_key'));
        try {
            $data = @$api->lookupByEmail($lead_email);
        }catch(Services_FullContact_Exception_NoCredit $e)
        {
            return null;
        }
		//-- Get Photo URL
		$user_photo_url = null;
		if(isset($data->photos)){
				$user_photo_url = $data->photos[0]->url;
			}
			return $user_photo_url;
	}

	public function saveProspectPhoto($photo, $user_id, $lead_email)
	{
//			$q = "UPDATE prospects SET photo = :photo WHERE id = :prospect_id";
//			$statement = $this->prepare($q);
//			$statement->execute(array ('photo' => $photo , 'prospect_id' => $prospectID));
        try {
            $path = UserModel::getStorage($user_id, TRUE, 'prospects');//' . $lead_email);
        } catch (QubeException $e) {
            Qube::LogError($e->getMessage());
            return false;
        }
		$success = copy($photo, $path . '/' . $lead_email);
        return $success;
	}

	public function getLastActivityProspect(ProspectModel $prospect, $all = false){
		if(!$prospect->visitor_ID || !$prospect->hub_ID){
            		return null;
        	}

		$piwikAPI = new PiwikReportingAPI();

		$q = "SELECT tracking_id FROM hub WHERE id = :hub_id";
 		$statement = $this->prepare($q);
		$statement->execute(array('hub_id' => $prospect->hub_ID));
		$idSite = $statement->fetchColumn();

		if(!$idSite){
			return null;
		}
		

		$result = $piwikAPI->liveGetVisitorProfile($idSite, $prospect->visitor_ID);
		
		if($all) {
			
			return $result;
			
		} else  {
			

			if(!empty($result->lastVisits) && !empty($result->lastVisits[0]->actionDetails)){
				$pageUrl = $result->lastVisits[0]->actionDetails[count($result->lastVisits[0]->actionDetails) - 1]->url;

				if(preg_match('/[^\/]\/([^\/]+)\/$/', $pageUrl, $pageTitleUrl)){
					$pageTitleUrl = $pageTitleUrl[1];

					$q = "SELECT * FROM hub_page2legacy WHERE user_id = :user_id AND hub_id = :hub_id AND page_title_url = :pageTitleUrl LIMIT 1";
					$statement = $this->prepare($q);
					$statement->execute(array('user_id' => $prospect->user_ID, 'hub_id' => $prospect->hub_ID, 'pageTitleUrl' => $pageTitleUrl));
					return $statement->fetchObject('PageModel');
				}elseif(preg_match('/^(?:http(?:s|):\/\/|)([^\/]+)\/$/', $pageUrl, $siteUrl)){
					$q = "SELECT * FROM hub WHERE id = :id";
					$statement = $this->prepare($q);
					$statement->execute(array('id' => $prospect->hub_ID));
					return $statement->fetchObject('WebsiteModel');
				}
			}
			return $result->lastVisits;
			
		}
	}

	/**
	 * @param $contactId
	 * @return ContactModel
	 */
	public function getContactById($contactId){
		$q = "SELECT c.*, CONCAT('{', GROUP_CONCAT(CONCAT('\"',l.label,'\":\"',l.valuestr,'\"')),'}') form_response FROM contacts c " .
			"LEFT JOIN leads_datax l ON c.lead_id = l.lead_ID " .
			"WHERE c.id = :contact_id";

		$statement = $this->prepare($q);

		$statement->execute(array('contact_id' => $contactId));
		$contact = $statement->fetchObject('ContactModel');
		
		if($contact && $contact->form_response){
			$contact->form_response = json_decode($contact->form_response, true);
		}

		return $contact;

	}
}
