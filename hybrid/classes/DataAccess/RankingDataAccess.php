<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/16/14
 * Time: 2:24 PM
 */

class RankingDataAccess extends  QubeDataAccess {

	/**
	 * @param DateTime $date
	 * @return bool
	 */
	public function saveDailyStatsResult($date = null){
		if(!$date){
			$date = new DateTime();
		}

		$ranking_dailystats_query = $this->getRanking_dailystats_query();

		$q = "REPLACE INTO ranking_dailystats
		(date, user_ID, campaign_ID, site_ID, searchengine, keyword_ID, ranking, pagenumber) ". $ranking_dailystats_query;

		$datestr	=		$date->format('Y-m-d');

	echo "Date: ", $datestr, "\n";
		$stmnt = $this->prepare($q);
		return $stmnt->execute(array("date" => $datestr));
	}

	/**
	 * @param int $siteID
	 * @param string $searchengine
	 * @param array $params
	 * @return stdClass[]
	 */
	public function getRankingDailyStatsChart($siteID, $searchengine, $params){
//		$interval = $params['interval'] - 1;
		$params['site_ID'] = $siteID;
		list($period, $interval) = $this->getIntervalPeriod($params);
		$interval--;

		$fields = array();
		$where = array();
		$group = array();
		$order = array();

		$fields[] = $period == "MONTH" ? "DATE_FORMAT(date, '%Y-%m-01') as date" : 'date';
		$fields[] = 'keyword_id';
		$fields[] = 'MIN(ranking) as rank';
		$fields[] = 'pagenumber';

		$where[] = 'ranking_keywords.site_ID = :site_ID';
		$where[] = 'searchengine = :searchengine';
		$where[] = "date >= DATE_SUB(CURRENT_DATE(), INTERVAL :interval $period)";
		if($period == "MONTH") $where[] = "(date = last_day(date) OR date = current_date())";

		$group[] = $period == "MONTH" ? "DATE_FORMAT(date, '%Y-%m-01')" : 'date';
		$group[] = "keyword_id";

		$order[] = "date";
		$order[] = "rank";

		$fields = implode(', ', $fields);
		$where = implode(' AND ', $where);
		$group = implode(', ', $group);
		$order = implode(', ', $order);


		$q = "SELECT $fields FROM ranking_keywords LEFT JOIN ranking_dailystats ON ranking_keywords.ID = ranking_dailystats.keyword_ID AND ranking_keywords.site_id = ranking_dailystats.site_ID AND ranking_dailystats.ranking != 0 WHERE $where GROUP BY $group ORDER BY $order";

//		$q = 'SELECT date, keyword_id, MIN(ranking) rank, pagenumber FROM '.
//    			'ranking_dailystats '.
//			 'WHERE '.
//				'site_ID = :site_ID AND searchengine = :searchengine '.
//        			"AND date >= DATE_SUB(CURRENT_DATE(), INTERVAL :interval $period) ".
//			 'GROUP BY date , keyword_id, pagenumber ' .
//			 'ORDER BY date, rank';

		$stmnt = $this->prepare($q);
		$stmnt->execute(array(
			'site_ID' => $siteID,
			'searchengine' => $searchengine,
			'interval' => $interval
		));
		$keywordsRank = $stmnt->fetchAll(PDO::FETCH_OBJ);

		$ranksTop10 = array();
		$ranksTop50 = array();
		$ranksElse = array();
		$maxKeywordsCountInADay = array();
		$startDate = new DateTime();
		if($period == "MONTH"){
			$startDate = date_create($startDate->format('Y-m-01'));
		}
		$startDate->sub(new DateInterval("P$interval{$period[0]}"));
		for($date = new DateTime(), $i = 0; $startDate <= $date; $startDate->add(new DateInterval("P1{$period[0]}")), $i++){
			$format = $startDate->format('Y-m-d');
			$ranksTop10[strval((strtotime($format . ' UTC')) * 1000)] = 0;
			$ranksTop50[strval((strtotime($format . ' UTC')) * 1000)] = 0;
			$ranksElse[strval((strtotime($format . ' UTC')) * 1000)] = 0;
			$maxKeywordsCountInADay[strval((strtotime($format . ' UTC')) * 1000)] = 0;
		}

		foreach($keywordsRank as $keywordRank){
			if($keywordRank->pagenumber <= 1){
				$ranksTop10[strval((strtotime($keywordRank->date . ' UTC')) * 1000)]++;
			}else if($keywordRank->pagenumber <= 5){
				$ranksTop50[strval((strtotime($keywordRank->date . ' UTC')) * 1000)]++;
			}else{
				$ranksElse[strval((strtotime($keywordRank->date . ' UTC')) * 1000)]++;
			}
			$maxKeywordsCountInADay[strval((strtotime($keywordRank->date . ' UTC')) * 1000)]++;
		}

		$ranksTop10Chart = array();
		$ranksTop50Chart = array();
		$ranksElseChart = array();
		foreach($ranksTop10 as $date => $ranks){
			$ranksTop10Chart[] = array($date, $ranksTop10[$date]);
			$ranksTop50Chart[] = array($date, $ranksTop50[$date]);
			$ranksElseChart[] = array($date, $ranksElse[$date]);
		}

		$data = array(
			array(
				'data' => $ranksElseChart,
				'bars' => array(
					'fillColor' => "#00aeef"/* 3rd: cyan */
				),
				'label' => 'Page Number +6',
				'color' => '#00aeef'/* 3rd: cyan */
			),
			array(
				'data' => $ranksTop50Chart,
				'bars' => array(
					'fillColor' => "#27bdbe"/* 2nd: teal */
				),
				'label' => 'Page Number 2-5',
				'color' => '#27bdbe'/* 2nd: teal */
			),
			array(
				'data' => $ranksTop10Chart,
				'bars' => array(
					'fillColor' => "#C57FE5"/* 1st: green */
				),
				'label' => 'Page Number 1',
				'color' => '#C57FE5'/* 1st: green */
			)
		);

		$chartOptions = $this->getRankingChartOptions($period);
		$chartOptions['yaxis']['tickSize'] = intval(ceil(max($maxKeywordsCountInADay) / 4));
		$return = array('data' => $data, 'chartOptions' => $chartOptions);
		return $return;
	}

	/**
	 * @param array $objs
	 * @param string $date
	 * @return bool
	 * @throws QubeException
	 */
	public function saveSearchResults2(array $objs, $date = null)
	{
		static $insertStmt	=	NULL;

		$searchengine = $objs['searchengine'];
		$pdo	=	$this->getPDO('master');

		if(!$date){
			$date = date('Y-m-d');
		}
		if(!$insertStmt)
		{
			$q	=	"INSERT INTO `ranking_dailyresults` (`ID`, `searchengine`, `date`, `position`, `pagenumber`, `keyword`, `title`, `url`, `domain`, `description`, `ts`) "
				. "VALUES (NULL, :searchengine, '$date', :position, :pagenumber, :keyword, :title, :url, :domain, :description, NULL)";
			$insertStmt	=	$pdo->prepare($q);
		}

		if(empty($objs))
			throw new QubeException('Nothing to Save.');

		if(!$pdo->beginTransaction()) throw new QubeException('Failed to beginTransaction');
		foreach($objs as $obj)
		{
			$insertStmt->execute((array)$obj);
			$updateStmt = $pdo->prepare('UPDATE ranking_keywords SET last_' . $obj['searchengine'] . ' = "' . $date . '" WHERE `keyword` = :keyword');
			$updateStmt->execute(array('keyword' => $obj['keyword']));
		}

		$result	=	$pdo->commit();
		return true;
	}

	public function saveSearchResults(array $objs, KeywordRanker $ranker, $searchengine)
	{
		static $insertStmt	=	NULL;
		static $updateStmt = array();
		
		$pdo	=	$this->getPDO('master');
		if(!$insertStmt)
		{
			$q	=	'INSERT INTO `ranking_dailyresults` (`ID`, `searchengine`, `date`, `position`, `pagenumber`, `keyword`, `title`, `url`, `domain`, `description`, `ts`) '
							. 'VALUES (NULL, :searchengine, CURDATE(), :position, :pagenumber, :keyword, :title, :url, :domain, :description, NULL)';
			$insertStmt	=	$pdo->prepare($q);
		}
		
		if(empty($objs))
			throw new QubeException('Nothing to Save.');
		
		if(!$pdo->beginTransaction()) throw new QubeException('Failed to beginTransaction');
		foreach($objs as $obj)
		{
			$insertStmt->execute((array)$obj);
		}
		if(!$updateStmt[$searchengine])
		{
			$updateStmt[$searchengine]	=	$pdo->prepare('UPDATE ranking_keywords SET last_' . $searchengine . ' = "' . date('Y-m-d') . '" WHERE `keyword` = :keyword');
		}
		$updateStmt[$searchengine]->execute(array('keyword' => $ranker->keyword));
		
		$result	=	$pdo->commit();
		return true;
	}
	
	public function getKeywordRankers($limit = 100, $offset = 0) {
		$todaydate	=	date('Y-m-d');
		
		$q	=	"select w.keyword, min(w.last_bing) as last_bing, min(w.last_google) as last_google"
						. " from ranking_keywords w "
						. "WHERE (last_bing != '$todaydate' OR last_google != '$todaydate') group by w.keyword LIMIT $offset, $limit";
		$stmt	=	$this->query($q);
		
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'KeywordRanker', array($this));
	}
	
	public function getTasks() {
		//$todaydate	=	date('Y-m-d');
		
		$q	=	"SELECT k.ID, k.keyword, k.site_id, s.hostname FROM ranking_keywords k, ranking_sites s WHERE s.id = k.site_id";
		$stmt	=	$this->query($q);
		
		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}
	
	public function updateGoogleTaskID($key_id, $task_id) {
		$todaydate	=	date('Y-m-d');
		
		$q	=	'UPDATE ranking_keywords SET google_task_id = "'.$task_id.'", last_google = "'.$todaydate.'" WHERE ID = "'.$key_id.'"';		
		$statement = $this->prepare($q);
		$statement->execute();
	}
	
	public function updateBingTaskID($key_id, $task_id) {
		$todaydate	=	date('Y-m-d');
		
		$q	=	'UPDATE ranking_keywords SET bing_task_id = "'.$task_id.'", last_bing = "'.$todaydate.'" WHERE ID = "'.$key_id.'"';
		$statement = $this->prepare($q);
		$statement->execute();
	}

	public function getRankingSites($user_id){
		$q = 'SELECT * FROM ranking_sites WHERE user_id = :user_id';

		$stmnt = $this->prepare($q);
		$stmnt->execute(array('user_id' => $user_id));
	}
	
	public function checkTaskDaily($user_id, $task_id){
		$todaydate	=	date('Y-m-d');
		$q = 'SELECT ID FROM ranking_dailystats WHERE user_ID = "'.$user_id.'" AND task_id = "'.$task_id.'" AND date = "'.$todaydate.'"';

		$stmnt = $this->prepare($q);
		$stmnt->execute(array('user_id' => $user_id));
	}
	
	public function saveRankingDailyStat($user_id, $cid, $site_id, $search, $key_id, $result_position, $page, $task_id, $result_url, $result_title, $result_snippet, $result_se_check_url){
		
		$todaydate	=	date('Y-m-d');
		$q = 'INSERT INTO ranking_dailystats (ID, date, user_ID, campaign_ID, site_ID,searchengine, keyword_ID, ranking, pagenumber, task_id, result_url, result_title, result_snippet, result_se_check_url) VALUES (NULL, "'.$todaydate.'", "'.$user_id.'", "'.$cid.'", "'.$site_id.'", "'.$search.'", "'.$key_id.'", "'.$result_position.'", "'.$page.'", "'.$task_id.'", "'.$result_url.'", "'.$result_title.'", "'.$result_snippet.'", "'.$result_se_check_url.'")';

		$statement = $this->prepare($q);
		$statement->execute();
	}
	
	static function getTopRankingKeywordsBySearchEngine(PointsSetBuilder $b, HybridBaseController $ctrl, $args = array())
	{
		$RaDA	=	static::FromController($ctrl);
if(isset($args['TOUCHPOINT_TYPE']) && trim($args['TOUCHPOINT_TYPE']) == '')
		unset($args['TOUCHPOINT_TYPE']);

			return array('google' => 
		$RaDA->getTopRankingKeywords(array('searchengine' => 'google', 'limit' => 5) + $args, $b),
					'bing' => 
		$RaDA->getTopRankingKeywords(array('searchengine' => 'bing', 'limit' => 5) + $args, $b)
				);
	}
	
	function getTopRankingKeywords($args	=	array(), PointsSetBuilder $PSB)
	{
		$actionuser	=	$this->getActionUser();
		
		$args['d1']	=	$PSB->getStartDate();
		$args['d2']	= $PSB->getEndDate();
		$args['user_ID']	=	$actionuser->getID();
		if(!isset($args['limit']))
		{
			$args['limit'] = 10;
		}
		$stmt	=	self::getPreparedStatement(__FUNCTION__, $actionuser->getID());
		if(!$stmt)
		{
			$q	=	'select w.*, ifnull(w.keyword, " Keyword ") as keyword, coalesce(r.ranking, 0) as ranking from ranking_keywords w
    	left join ( select s.keyword_ID, min(ranking) as ranking FROM ranking_dailystats as s where %s AND s.ranking != 0 group by s.keyword_ID ) as r
    		on (r.keyword_ID = w.ID)
    	right join ranking_sites x on (x.id = w.site_id)
    		%s
    	where %s order by ifnull(r.ranking, 100) asc'; //' limit 5';

			$whereStats	=	array();
			$mainWhere	=	array();
			$whereStats[]	=	'(:searchengine = "" OR s.searchengine = :searchengine)';
			$security_check	=	's.user_ID = :user_ID';
			if($actionuser->can('view_rankings') && UserModel::ADMIN_CANVIEWALL)	// DISABLED. user may only view their own keywords.
			{
				$security_check	.= ' or s.user_ID = :user_ID';
			}

			if(isset($args['campaign_ID']))
			{
				$whereStats[]	=	's.campaign_ID = :campaign_ID';
				$mainWhere[]	=	'w.campaign_ID = :campaign_ID';
			}
			if(isset($args['TOUCHPOINT_TYPE']))
			{
				$joins = ' INNER JOIN touchpoints_info i ON (i.ID = x.touchpoint_ID)';
				$mainWhere[]	=	'i.touchpoint_type = :TOUCHPOINT_TYPE';
				$mainWhere[]	=	'i.user_ID = :user_ID';

				if(isset($args['touchpoint_ID'])){
					$mainWhere[] = 'i.ID = :touchpoint_ID';
				}
//				unset($args['TOUCHPOINT_TYPE']);
			}else{
				// need to do a join with campaigns because keywords does not have a user_ID column
				$joins = ' INNER JOIN campaigns c on (c.ID = w.campaign_ID)';
				$mainWhere[]	=	'c.user_ID = :user_ID';
			}

			$whereStats[]	=	$security_check;
			$whereStats[]	=	's.date BETWEEN :d1 AND :d2';
			
			$q =	sprintf($q, implode(' AND ' , $whereStats), $joins, join(' AND ', $mainWhere));

			$q .= ' LIMIT :limit ';// . (int)$args['limit'];

			$stmt	=	$this->setPreparedQuery($q, __FUNCTION__, $actionuser->getID());
		}
		self::callPreparedQuery($stmt, $args);
		
		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}

	function getAllRankingData($data, $limit = 10, $offset = 0, $filter = null, $order = "id", $direction = "asc")
	{
		//rsites.*, ifnull(ti.hostname, rsites.hostname) as hostname, ti.touchpoint_name,
		$args['campaign_id'] = $data['campaign_id'];
		$args['user_id'] = $data['user_id'];
		$q = "SELECT SQL_CALC_FOUND_ROWS
					rsites.*, 
					ti.touchpoint_name,
					(SELECT
							COUNT(*)
						FROM
							ranking_keywords
						WHERE
							site_ID = rsites.ID) keywords,
					(SELECT
							COUNT(DISTINCT keyword_ID)
						FROM
							ranking_dailystats
						WHERE
							site_ID = rsites.ID
								AND searchengine = 'google'
								AND ranking <= 10
								AND ranking != 0
								AND date = CURRENT_DATE()) google_first,
					(SELECT
							COUNT(DISTINCT keyword_ID)
						FROM
							ranking_dailystats
						WHERE
							site_ID = rsites.ID
								AND searchengine = 'bing'
								AND ranking <= 10
								AND ranking != 0
								AND date = CURRENT_DATE()) bing_first
				FROM
					ranking_sites rsites left join touchpoints_info ti on (ti.ID = rsites.touchpoint_ID) ";
		$q .= "WHERE rsites.user_id = :user_id AND rsites.campaign_id = :campaign_id AND rsites.trashed = 0";
		//$args['curdate'] = date(PointsSetBuilder::MYSQLFORMAT);		// using hardcoded date for mysql-cache purposes.
		//ifnull(ti.hostname, rsites.hostname)
		if($filter){
			$q .= " AND (rsites.hostname LIKE CONCAT(CONCAT('%', :search),'%') OR rsites.ID = :search)";
			$args['search'] = $filter;
		}

		if($order){
			$newRow = $data['new_rows_ids'] ? 'rsites.id = ' . join(' DESC, rsites.id = ', $data['new_rows_ids']) . ' DESC, ' : '';
			unset($data['new_rows_ids']);
			$q .= " Order By $newRow $order $direction";
		}

		if(isset($data['limit'])){
			$limit = $data['limit'];
			$q .= " LIMIT $offset, $limit";
		}elseif($limit != null){
			$q .= " LIMIT $offset, $limit";
		}
		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args); // needs campaign_id and user_id
		return $stmn->fetchAll(PDO::FETCH_CLASS, 'RankingSiteModel');
	}
	
	function getAllRankingDataEmailReports($data, $order = "id", $direction = "asc")
	{
		$args['user_id'] = $data['user_id'];
		$q = "SELECT SQL_CALC_FOUND_ROWS
					rsites.*, ifnull(ti.hostname, rsites.hostname) as hostname,
					ti.touchpoint_name,
					(SELECT
							COUNT(*)
						FROM
							ranking_keywords
						WHERE
							site_ID = rsites.ID) keywords,
					(SELECT
							COUNT(DISTINCT keyword_ID)
						FROM
							ranking_dailystats
						WHERE
							site_ID = rsites.ID
								AND searchengine = 'google'
								AND ranking <= 10
								AND ranking != 0
								AND date = CURRENT_DATE()) google_first,
					(SELECT
							COUNT(DISTINCT keyword_ID)
						FROM
							ranking_dailystats
						WHERE
							site_ID = rsites.ID
								AND searchengine = 'bing'
								AND ranking <= 10
								AND ranking != 0
								AND date = CURRENT_DATE()) bing_first
				FROM
					ranking_sites rsites left join touchpoints_info ti on (ti.ID = rsites.touchpoint_ID) ";
		$q .= "WHERE rsites.user_id = :user_id AND rsites.trashed = 0";
		//$args['curdate'] = date(PointsSetBuilder::MYSQLFORMAT);		// using hardcoded date for mysql-cache purposes.


		if($order){
			$newRow = $data['new_rows_ids'] ? 'rsites.id = ' . join(' DESC, rsites.id = ', $data['new_rows_ids']) . ' DESC, ' : '';
			unset($data['new_rows_ids']);
			$q .= " Order By $newRow $order $direction";
		}


		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args); // needs campaign_id and user_id
		return $stmn->fetchAll(PDO::FETCH_CLASS, 'RankingSiteModel');
	}

	function getAllRankingCount($args, $limit = 10, $offset = 0, $filter = null, $order = "id", $direction = "asc")
	{
		$q = "select count(*) from ranking_sites
						WHERE user_id = :user_id AND campaign_id = :campaign_id and trashed = 0";
		
		// $args['curdate'] = date(PointsSetBuilder::MYSQLFORMAT);		// using hardcoded date for mysql-cache purposes.


		// if($limit != null){
		//	$q .= " LIMIT $offset, $limit";
		//}
		// $stmn->fetchAll(PDO::FETCH_OBJ);

		$stmn = $this->getQube()->GetDB()->prepare($q);
		unset($args['new_rows_ids']);
		$stmn->execute($args);
		$drt = $stmn->fetchColumn();
		return $drt;
	}

	function getAllKeywordData($data, $start = 10, $offset = 0, $filter = null, $order = "ranking_keywords.keyword", $direction = "asc")
	{
		//$data['period']=$data['PERIOD'];
		$siteID = $data['site_ID'];
		$campaignID = $data['campaign_id'];
		list($period) = $this->getIntervalPeriod($data);

		$where = array();

		$args = array('site_id' => $siteID, 'campaign_id' => $campaignID);

		$where[] = "ranking_keywords.campaign_id = :campaign_id";
		$where[] = "ranking_keywords.site_id = :site_id";
		if($filter){
			if(is_numeric($filter))
			{
				$where[] = '(ranking_keywords.ID = :search)';
			}else
				$where[] = "ranking_keywords.keyword LIKE CONCAT(CONCAT('%', :search),'%')";
			$args['search'] = $filter;
		}
		$where = "WHERE " . implode(' AND ', $where);

		$orderBy = "";
		if($order){
			$newRow = $data['new_rows_ids'] ? 'ID = ' . join(' DESC, ID = ', array_map('intval', $data['new_rows_ids'])) . ' DESC, ' : '';
			$orderBy .= " Order By $newRow $order $direction";
		}
		
		$limit = "";
		if(isset($data['limit'])){
			$dlimit = $data['limit'];
			$limit .= "";
		}elseif($start != null){
			$limit .= " LIMIT $offset, $start";
		}

		$latest_date_google = "ranking_keywords.last_google";
		$latest_date_bing = "ranking_keywords.last_bing";
		$change_date_google = "SUBDATE({$latest_date_google}, INTERVAL 1 {$period})";
		$change_date_bing = "SUBDATE({$latest_date_bing}, INTERVAL 1 {$period})";

		if($data['period'] == "MONTH"){
			$change_date_google = "LAST_DAY($change_date_google)";
			$change_date_bing = "LAST_DAY($change_date_bing)";
		}

		$q = "SELECT SQL_CALC_FOUND_ROWS " .
				"ranking_keywords.ID, " .
				"ranking_keywords.campaign_id, " .
				"ranking_keywords.site_id, " .
				"ranking_keywords.keyword, " .
				"ranking_keywords.created, " .
				$this->getRankingLastUpdated($this->getMinRankingQuery('google', $latest_date_google),$latest_date_google) . "AS rank_google, " .
				$this->getRankingLastUpdated($this->getMinRankingQuery('bing', $latest_date_bing), $latest_date_bing) . "AS rank_bing, " .
				$this->getRankingIntervalDate($this->getMinRankingQuery('google', $change_date_google), $latest_date_google) . ' - ' . $this->getRankingLastUpdated($this->getMinRankingQuery('google', $latest_date_google), $latest_date_google) . " AS google_change, " .
				$this->getRankingIntervalDate($this->getMinRankingQuery('bing', $change_date_bing), $latest_date_bing) . ' - ' . $this->getRankingLastUpdated($this->getMinRankingQuery('bing', $latest_date_bing), $latest_date_bing) . " AS bing_change " .
			"FROM ranking_keywords LEFT JOIN ranking_dailystats ON ranking_keywords.ID = ranking_dailystats.keyword_ID AND ranking_keywords.site_id = ranking_dailystats.site_ID " .
			"$where " .
			"GROUP BY ranking_keywords.ID, ranking_keywords.campaign_id, ranking_keywords.keyword, ranking_keywords.created " .
			"$orderBy $limit";

		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		return $stmn->fetchAll(PDO::FETCH_CLASS,'RankingKeywordModel');
	}
	
	function getAllKeywordDataEmailReports($data, $order = "ranking_keywords.keyword", $direction = "asc")
	{
//		$data['period']=$data['PERIOD'];
		$siteID = $data['site_ID'];
		$period = $data['period'];
		$where = array();

		$args = array('site_id' => $siteID);
		$where[] = "ranking_keywords.site_id = :site_id";
		if($filter){
			if(is_numeric($filter))
			{
				$where[] = '(ranking_keywords.ID = :search)';
			}else
				$where[] = "ranking_keywords.keyword LIKE CONCAT(CONCAT('%', :search),'%')";
			$args['search'] = $filter;
		}
		$where = "WHERE " . implode(' AND ', $where);

		$orderBy = "";
		if($order){
			$newRow = $data['new_rows_ids'] ? 'ID = ' . join(' DESC, ID = ', array_map('intval', $data['new_rows_ids'])) . ' DESC, ' : '';
			$orderBy .= " Order By $newRow $order $direction";
		}

		$limit = "";
		if($start != null){
			$limit .= " LIMIT $offset, $start";
		}

		$latest_date_google = "ranking_keywords.last_google";
		$latest_date_bing = "ranking_keywords.last_bing";
		$change_date_google = "SUBDATE({$latest_date_google}, INTERVAL 1 {$period})";
		$change_date_bing = "SUBDATE({$latest_date_bing}, INTERVAL 1 {$period})";

		if($data['period'] == "MONTH"){
			$change_date_google = "LAST_DAY($change_date_google)";
			$change_date_bing = "LAST_DAY($change_date_bing)";
		}

		$q = "SELECT SQL_CALC_FOUND_ROWS " .
				"ranking_keywords.ID, " .
				"ranking_keywords.campaign_id, " .
				"ranking_keywords.site_id, " .
				"ranking_keywords.keyword, " .
				"ranking_keywords.created, " .
				"ranking_dailystats.result_url, " .
			    "ranking_dailystats.searchengine, " .
			"ranking_dailystats.ranking, " .
			"ranking_dailystats.pagenumber, " .
			"ranking_dailystats.result_se_check_url, " .
				$this->getRankingLastUpdated($this->getMinRankingQuery('google', $latest_date_google),$latest_date_google) . "AS rank_google, " .
				$this->getRankingLastUpdated($this->getMinRankingQuery('bing', $latest_date_bing), $latest_date_bing) . "AS rank_bing, " .
				$this->getRankingIntervalDate($this->getMinRankingQuery('google', $change_date_google), $latest_date_google) . ' - ' . $this->getRankingLastUpdated($this->getMinRankingQuery('google', $latest_date_google), $latest_date_google) . " AS google_change, " .
				$this->getRankingIntervalDate($this->getMinRankingQuery('bing', $change_date_bing), $latest_date_bing) . ' - ' . $this->getRankingLastUpdated($this->getMinRankingQuery('bing', $latest_date_bing), $latest_date_bing) . " AS bing_change " .
			"FROM ranking_keywords LEFT JOIN ranking_dailystats ON ranking_keywords.ID = ranking_dailystats.keyword_ID AND ranking_keywords.site_id = ranking_dailystats.site_ID " .
			"$where " .
			"GROUP BY ranking_keywords.ID, ranking_keywords.campaign_id, ranking_keywords.keyword, ranking_keywords.created " .
			"$orderBy $limit";

		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		return $stmn->fetchAll(PDO::FETCH_CLASS,'RankingKeywordModel');
	}

	private function getRankingIntervalDate($getMinFunction, $date)
	{
		return "IFNULL($getMinFunction, IF($date = '0000-00-00', 0, NULL))";

	}

	private function getRankingLastUpdated($getMinFunction, $date)
	{
		return "IFNULL($getMinFunction, IF($date = '0000-00-00', 0, 101))";
	}

	private function getMinRankingQuery($searchengine, $date){
		return "MIN(IF(ranking_dailystats.searchengine = '$searchengine' AND ranking_dailystats.date = $date,ranking_dailystats.ranking, NULL))";
	}

	function getAllKeywordCount($data, $limit = 10, $offset = 0, $filter = null, $order = "ID", $direction = "asc")
	{
		$siteID = $data['site_ID'];
		$campaignID = $data['campaign_id'];
		$q = "select count(*) from ranking_keywords ";
		$q .= "WHERE site_id = :siteID AND campaign_id = :campaign_ID";
		$args = array('siteID' => $siteID, 'campaign_ID' => $campaignID);

		if($order) {
			$q .= " Order By $order $direction";
		}

		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		$drt = $stmn->fetchColumn();
		return $drt;
	}

	function getGoogleKeywordRanking($data){
		$site_ID = $data['site_id'];
		$user_ID = $data['user_id'];
		$query_tier = 1;
		switch($query_tier){
			case 1:
				$query =
					"SELECT * FROM ranking_dailystats LEFT JOIN ranking_keywords ON keyword_id = ranking_keywords.ID WHERE ranking_dailystats.campaign_ID = 7221 AND ranking < 6;";
				break;
			case 2:
		}
		execute:
		$statement = $this->getQube()->GetDB()->prepare($query);
		$statement->execute();
		$drt = $statement->fetchAll();
		$final_array = array('FirstTier' => Array(), 'SecondTier' => Array(), 'ThirdTier' => Array());
		foreach ($drt as $index => $atp )
		{
			$final_array[]= $atp;
			//$final_array[]= ;  or maybe $index, the last Alan's instruction he typed before he left
		}
		return $final_array;
	}

	/**
	 * @param string $period
	 * @return array
	 */
	public function getRankingChartOptions($period){
		$isMonth = $period == "MONTH";
		$chartOptions = array(
			'xaxis' => array(
				'mode' => "time",
				"timeformat" => "%b %d",
				"tickSize" => array(1, strtolower($period)),
				"tickLength" => 0
			),
			'yaxis' => array(
				'tickDecimals' => 0,
				'tickSize' => 1,
				'min' => 0
			),
			'grid' => array(
				'borderColor' => '#d4e9f4',
				'hoverable' => true,
				'clickable' => false,
				'borderWidth' => 1
			),
			'legend' => array(
				'labelBoxBorderColor' => 'none',
				'position' => 'ne'
			),
			'series' => array(
				'stack' => true,
				'shadowSize' => 1,
				'bars' => array(
					'show' => true,
					'barWidth' => ceil((24 * 60 * 60 * 1000 * ($isMonth ? 30 : 1)) / 3.5),
					'align' => "center",
					'fill' => true,
					'lineWidth' => 1
				)
			)
		);
		return $chartOptions;
	}

	private function getIntervalPeriod($params)
	{
		switch($params['period']){
			case 'DAY':
				return array($params['period'], $params['interval']);
			case 'MONTH':
				return array($params['period'], $params['interval']);
			case 'ALL':
				$site_ID = intval($params['site_ID']);
				$date = date_create_from_format('Y-m-d', $this->query("SELECT date FROM ranking_dailystats WHERE site_ID = $site_ID ORDER BY date ASC LIMIT 1")->fetchColumn());
				if(!$date) return array('DAY', 1);
				$diff = $date->diff(new DateTime());
				if($diff->days <= 7)
					return array('DAY', 7);
				elseif($diff->days <= 30)
					return array('DAY', 30);
				elseif($diff->m <= 6)
					return array('MONTH', 6);
				elseif($diff->m <= 12)
					return array('MONTH', 12);
				elseif($diff->y <= 6)
					return array('YEAR', 6);
				else
					return array('YEAR', 12);
			case null:
				throw new QubeException("Period param missing in RankingDataAccess");
			default:
				throw new QubeException("Period not supported {$params['period']}");
		}
	}

	/**
	 * @return string
	 */
	private function getRanking_dailystats_query()
	{
		$ranking_dailystats_query =
			'SELECT ranking_dailyresults.date, ' .
				'ranking_sites.user_id, ' .
				'ranking_sites.campaign_id, ' .
				'ranking_sites.ID site_ID, ' .
				'ranking_dailyresults.searchengine, ' .
				'ranking_keywords.ID keyword_ID, ' .
				'ranking_dailyresults.position, ranking_dailyresults.pagenumber ' .
			'FROM ranking_keywords ' .
				'INNER JOIN ' .
					'ranking_sites ON ranking_keywords.site_id = ranking_sites.ID ' .
				'LEFT JOIN ' .
					'6q_touchpoints ON ranking_sites.touchpoint_ID = 6q_touchpoints.ID AND ranking_sites.touchpoint_ID != 0 ' .
				'LEFT JOIN ' .
					'ranking_dailyresults ON ranking_dailyresults.keyword = ranking_keywords.keyword ' .
						'AND ((ranking_sites.hostname = ranking_dailyresults.domain AND ranking_sites.touchpoint_ID = 0) ' .
						'OR (6q_touchpoints.hostname = ranking_dailyresults.domain AND ranking_sites.touchpoint_ID != 0)) ' .
				'WHERE ranking_dailyresults.date = :date';
		return $ranking_dailystats_query;
	}

	public function getRankingDataDailyStats($date){
		$statement = $this->prepare($this->getRanking_dailystats_query());
		$statement->execute(array('date' => $date));
		return $statement->fetchAll();
	}

	public function saveDailyStatsResultFromArray($data){
		$insertsPerLoop = 1000;
		$countRows = count($data);
		$i = 0;

#		try{
			while($i < $countRows){
				$values = array();

				for($j = 1; $j <= $insertsPerLoop - 1 && $i < $countRows; $j++, $i++){
					$row = $data[$i];
					$values[] = "('{$row['date']}', {$row['user_id']}, {$row['campaign_id']}, {$row['site_ID']}, '{$row['searchengine']}', {$row['keyword_ID']}, {$row['position']}, {$row['pagenumber']})";
				}

#				$row = $data[$i++];
#				$values[] = "('{$row['date']}', {$row['user_id']}, {$row['campaign_id']}, {$row['site_ID']}, '{$row['searchengine']}', {$row['keyword_ID']}, {$row['position']}, {$row['pagenumber']})";

				$insert = "REPLACE INTO ranking_dailystats (date, user_ID, campaign_ID, site_ID, searchengine, keyword_ID, ranking, pagenumber) VALUES " . implode(',', $values);

				$statement = $this->prepare($insert);
				$statement->execute();
			}
#		}catch(Exception $e){
#		var_dump($e);
#			echo $insert;
#			return false;
#		}


		return true;
	}
}
