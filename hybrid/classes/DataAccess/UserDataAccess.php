<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataAccess
 *
 * @author amado
 */
class UserDataAccess extends QubeDataAccess {

    /**
     * 
     * @param int $user_ID
     * @return UserModel
     */
    function getUser($user_ID) {
        return $this->getQube()->queryObjects('UserModel', 'U', 'U.id = %d', $user_ID)->Exec()->fetch();
    }

    /**
     * Accepts multiple args
     * 
     * @param string $fields
     * @param type $fetch_arg1
     * @return UserModel[]
     */
    function getAllParentUsers($fields, $fetch_arg1) {
        $query = 'SELECT ' . $fields . ' FROM users u left join user_info i on (i.user_id = u.id)'
                . ' WHERE u.parent_id = 0 OR u.parent_id = u.id';

        $args = func_get_args();
        array_shift($args);
        return call_user_func_array(array($this->query($query), 'fetchAll'), $args);
    }

    /**
     * 
     * @param int|array $user_ID|$args
     * @return UserWithRolesModel
     */
    function getUserWithRoles($args) {
        if (is_numeric($args)) {
            $args = array('user_ID' => (int) $args);
        }
        if (!is_array($args)) {
            throw new QubeException('array');
        }
        $user_ID = $args['user_ID'];
        /* @var $UserWithRoles UserWithRolesModel */
        $Query = $this->getQube()->queryObjects('UserWithRolesModel')
                        ->Where('T.id = %d', $user_ID)
                        ->leftJoin('user_info info', 'info.user_id = T.id')->leftJoin('6q_userroles roles', 'roles.user_ID = T.id')->addFields('info.profile_photo, info.firstname,
                                ceil(rand()*10) as unread_msgs, (select 1 FROM user_info WHERE referral_id = T.id LIMIT 1) as has_referred_users, roles.role_ID');
        if (isset($args['reseller_fields'])) {
            $Query->leftJoin(array('resellers', 'r'), 'r.admin_user = if(T.parent_id=0,T.id,T.parent_id)')
                    ->addFields($args['reseller_fields']);
        }
        $UserWithRoles = $Query->Exec()->fetch();
        if ($UserWithRoles) {
            $query = $this->getQube()->queryObjects('QubePermissionModel')
                    ->rightJoin('6q_roleperms x', 'x.permission_ID = T.ID')
                    ->rightJoin(array('QubeRoleModel', 'r'), 'r.ID = x.role_ID')
                    ->rightJoin('6q_userroles rx', 'rx.role_ID = r.ID')
                    ->Where('rx.user_ID = %d', $user_ID)
                    ->Fields('T.flag, x.object_ID');

            $xpermissions = new PermissionsSet($UserWithRoles);

            $permissions = $query->Exec()->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_COLUMN);

            // set is_admin permission if there are any non-view permissions.
            foreach ($permissions as $key => $ids) {
                foreach ($ids as $id) {
                    $xpermissions->setPermission($key, $id);
                }
                continue;
                if (strpos($key, 'view_') !== 0 || $key == 'view_users') {
                    $this->permissions['is_admin'] = array(0);
                    break;
                }
            }

            $UserWithRoles->setPermissionsSet($xpermissions);

            $is_reseller = $UserWithRoles->isAdmin();
            $is_super = $UserWithRoles->super_user;

            if ($user_ID == 6610 || $UserWithRoles->username == 'system@sofus.mx') {
                $UserWithRoles->setPermission('is_system');
            } else
            // a reseller
            if ($is_reseller)
                $UserWithRoles->setPermission('is_reseller');
            else
            // still not sure what this means...
            if ($is_super)
                $UserWithRoles->setPermission('is_super');
            else
            // support
            if ($user_ID == 2845)  // support@6qube.com	@todo remove
                $UserWithRoles->setPermission('manage_spam');

            $UserWithRoles->renderPermissions();
        }
        return $UserWithRoles;
    }

    /**
     * @param $role_ID
     * @param array $permission_ids
     * @throws QubeException
     */
    function SaveRolePermissions($role_ID, array $permission_ids) {
        $pdo = $this->getPDO();

        $allpermissionsvalid = $this->getQube()->query('SELECT count(*) = ' . count($permission_ids) . ' FROM %s WHERE ID IN (%s) ', 'QubePermissionModel', join(', ', $permission_ids))->fetchColumn();
        if ($allpermissionsvalid == 0)
            throw new QubeException('invalid permissions');

        $pdo->query('DELETE FROM 6q_roleperms WHERE role_ID = ' . (int) $role_ID);
        foreach ($permission_ids as $p_id) {
            $pdo->query('INSERT INTO 6q_roleperms (role_ID, permission_ID) VALUES (' . $role_ID . ', ' . $p_id . ')');
        }
    }

    protected function queryRoles() {
        return $this->getQube()->queryObjects('QubeRoleModel') // Roles
                        ->leftJoin('6q_roleperms x', 'x.role_ID = T.ID') // Many-To-Many
                        ->leftJoin('(SELECT role_ID, count(*) n_users FROM 6q_userroles INNER JOIN users ON 6q_userroles.user_ID = users.ID AND users.canceled = 0  GROUP BY 6q_userroles.role_ID) count_roles', 'count_roles.role_ID = T.ID')
                        ->leftJoin(array('QubePermissionModel', 'p'), 'p.ID = x.permission_ID') // Permissions
                        ->addFields('group_concat(p.flag SEPARATOR ", ") as permissions_str, IFNULL(count_roles.n_users, 0) as role_users')
                        ->groupBy('T.ID');
    }

    function createDefaultSystemRoles($reseller_id) {
        // @todo assert that the user does not have any roles previously created
        $pdo = $this->getPDO();
        $pdo->beginTransaction();
        $q = 'SELECT ID, role from 6q_roles WHERE reseller_user = 0';
        $stmt = $this->query($q);
        $insertstmt = $this->prepare('INSERT IGNORE INTO 6q_roles SET reseller_user = :reseller_id, role = :rolename');
        $copyperms = $this->prepare(
                'insert into 6q_roleperms (role_ID, permission_ID, object_ID) '
                . 'select :newrole_ID, permission_ID, object_ID from 6q_roleperms where role_ID = :oldrole_ID'
        );

        while ($rolerow = $stmt->fetchObject()) {
            if (!$insertstmt->execute(array('reseller_id' => $reseller_id, 'rolename' => $rolerow->role)))
                throw new QubeException('FAiled to create role.');

            $insert_id = $pdo->lastInsertId();

            if (0 == $insert_id)
                continue; // if role exists, do not modify!!

            $copyperms->execute(array('newrole_ID' => $insert_id, 'oldrole_ID' => $rolerow->ID));
        }

        $pdo->commit();
    }

    /**
     * Returns a single or all roles for the reseller(based on getactionuser)
     * 
     * @param int $role_ID
     * @param string $filter
     * @return QubeRoleModel[]|QubeRoleModel
     */
    function getRoles($role_ID = 0, $filter = null, $offset = 0, $limit = null, $options = array()) {
        $is_system = $this->getActionUser()->can('is_system');
        $q = $this->queryRoles();
        $newRow = $options['new_rows_ids'] ? 'T.ID = ' . join(' DESC, T.ID = ', array_map('intval', $options['new_rows_ids'])) . ' DESC ' : '0 = 0';
        if ($is_system) {
            $q->leftJoin('user_info u', 'u.user_id = T.reseller_user')
                    ->addFields('if(T.reseller_user = 0, "*** Default Role ***",
										if(ifnull(u.company, "")="", concat("Unknown#", T.reseller_user), u.company)
									) as company, concat(T.reseller_user, "#", T.role) as role_hash')
                    ->orderBy("$newRow, T.reseller_user = 0 DESC, company ASC, T.role ASC");

            // in case we want to limit the results to a specific account's roles
            if ($filter && $filter['reseller_user'])
                $q->Where('T.reseller_user LIKE :reseller_user');
        }else {
            $q->Where('(T.reseller_user = %d)', $this->getActionUser()->getParentID());
            if ($newRow) {
                $q->orderBy($newRow);
            }
        }
        $q->calc_found_rows();

        if ($limit) {
            $q->Limit($offset, $limit);
        }
        if ($filter && $filter['search']) {
            $where = array('T.role LIKE CONCAT(CONCAT("%%",:search),"%%")');
            if ($is_system)
                $where[] = 'u.company LIKE concat("%%", :search, "%%")';
            $q->Where("(%s)", implode(' OR ', $where));
        }

        $params = $filter;

        if (!$role_ID) {
//			$q->Where('T.id = %d', $role_ID);
            $pstmt = $q->Prepare();
            if ($pstmt->execute($params))
                return $pstmt->fetchAll();

            return NULL;
        }
        // return single role
        return $q->Where('T.id = %d', $role_ID)->Exec()->fetch();
    }

    /**
     * 
     * @param type $sub_ID
     * @return boolVerify that a subscription exists in the db.
     * 
     * @return bool
     */
    function subscriptionExists($sub_ID) {
        return $this->query('SELECT 1 FROM subscriptions WHERE ID = ' . (int) $sub_ID)->rowCount() == 1;
    }

    function subscribeUser($subscription_ID, $user_ID, $expiration, $billing_ID = 0) {
        if (!$this->subscriptionExists($subscription_ID))
            return false;

        /**
         * v1.1 In this version, any user who has a subscription becomes a reseller.
         */
        $q = 'INSERT INTO account_limits SET user_ID = :user_ID, sub_ID = :sub_ID,
					billing_ref = :billing_ID, limits_json = (select limits_json FROM subscriptions where ID = :sub_ID),
					expiration_date = :expiration, type = \'SUB\'';

        $stmt = $this->prepare($q);

        $result = $stmt->execute(array('sub_ID' => $subscription_ID,
            'user_ID' => $user_ID, 'billing_ID' => $billing_ID,
            'expiration' => $expiration instanceof DateTime ? $expiration->format(PointsSetBuilder::MYSQLFORMAT) : $expiration)
        );

        if (!$result)
            return false;

//		return $this->recalculateTOTALLimits($user_ID, $expiration->format(PointsSetBuilder::MYSQLFORMAT));
        return $this->getPDO('master')->lastInsertId();
    }

    /**
     * Obtain non-expired non-canceled subscriptions.
     * if expiration_date = 0, then the subscription never expires.
     *
     * @param $user_ID
     * @param $expiration
     * @param int $sub_id
     * @return array
     * @throws QubeException
     */
    function getSubscriptionsForTOTAL($user_ID, $expiration, $sub_id = null) {
        $q = 'SELECT * FROM account_limits WHERE user_ID = :user_ID
 					AND (expiration_date = 0 OR expiration_date > :expiration)
 					AND cancelation_date = 0
 					AND sub_ID != 0
 					AND type =\'SUB\'
 					AND (:sub_id IS NULL OR sub_ID != :sub_id)'; // select all non-summary subscription rows
        $stmt = $this->prepare($q);

        if (false === $stmt->execute(array('user_ID' => $user_ID, 'expiration' => $expiration, 'sub_id' => $sub_id))) {
            throw new QubeException('Fail');
        }

        return $stmt->fetchAll(PDO::FETCH_CLASS, 'SubscriptionModel');
    }

    /**
     * c. find all non-canceled, non-expired plans+addons.
     * d. calculate the TOTAL limits and use the min(expiration_date) as the expiration_date of TOTAL
     *
     * @param $user_ID
     * @param $expiration
     * @param null $type
     * @return bool
     */
    function calculateTOTALForUser($user_ID, $expiration, $type = NULL) {
        $params = array('where' =>
            array('user_ID = :user_ID',
                'cancelation_date = 0',
                '(expiration_date = 0 OR expiration_date > :expiration)'));
        if ($type) {
            $params['where'][] = 'type = :type';
            $params['type'] = $type;
        } else {
            $params['where'][] = 'type != "TOTAL"';
        }
        $params['user_ID'] = $user_ID;
        $params['expiration'] = $expiration;

        $all_limits = $this->getLimitRow($params);

        if (count($all_limits) < 1)
            return false;

        $total_json = array();
        self::aggregateLimits($all_limits, $total_json);

        $params['cols'] = 'ifnull(min(expiration_date), "0000-00-00") as recalculation_date';
        $params['getStatement'] = TRUE;
        $params['where'][] = 'expiration_date != 0'; // hack, (expiration-date = 0 or expiration-date > :expiration) and expiration-date != 0
        $min_date = $this->getLimitRow($params)->fetchColumn();


        $total = array('expiration_date' => $min_date,
            'limits_json' => json_encode($total_json), 'user_ID' => $user_ID);

        $q = 'INSERT INTO account_limits
				SET user_ID = :user_ID,
					sub_ID = 0,
					expiration_date = :expiration_date,
					limits_json = :limits_json, type = \'TOTAL\'';
        $stmt = $this->prepare($q);
        return $stmt->execute($total);
    }

    /**
     * @param $user_ID
     * @return SubscriptionModel
     */
    function getUserUnExpiredTOTALLimit($user_ID) {
        $limit = $this->getLimitRow(array('user_ID' => $user_ID,
            'cols' => 'l.*, l.expiration_date = CURDATE() as expires_today, l.expiration_date = 0 as is_persistent',
            'where' => array('user_ID = :user_ID', 'type = "TOTAL"', '(expiration_date = 0 OR CURDATE() <= expiration_date)', 'cancelation_date = 0'),
            'returnSingleObj' => 'SubscriptionModel')
        );
        return $limit;
    }

    function getLimitRow(array $args = array()) {
        if (!isset($args['cols'])) {
            $args['cols'] = '*';
        }
        if (!isset($args['where'])) {
            $args['where'] = array('sub_ID' => 0, 'user_ID = :user_ID', 'type = "TOTAL"');
        }
//		$args['returnSingleObj']	=	'stdClass';
        $qOpts = self::getQueryOptions($args);
#		$where = $where ? $where : array('expiration_date > CURDATE()');

        $q = 'SELECT %s FROM account_limits l
					WHERE %s';
        $qstr = sprintf($q, $qOpts['cols'], $qOpts['where']);

        $stmt = $this->prepare($qstr);
//		$stmt->execute(array('user_ID' => $user_ID));//, 'expiration' => $expiration->format(PointsSetBuilder::MYSQLFORMAT)));
//		$rowcount = $stmt->rowCount();
//		if($rowcount > 1)
//			throw new QubeException('Found multiple Account Limits.');
//		elseif($rowcount == 0){
//			return null;
//		}
        return self::HandleQueryResults($stmt, $args, $qOpts, 'SubscriptionModel');
    }

    /**
     * Retrieve TOTAL limits
     *
     * @param $user_ID
     * @param null $expiration
     * @return mixed
     */
    function getAccountLimits($user_ID, $expiration = NULL) {
        if (!$expiration) {
            $expiration = date('Y-m-d');
        }

        $stmt = $this->getLimitRow(array('where' => array('sub_ID = 0 AND type = "TOTAL" AND user_ID = :user_ID AND (expiration_date > :expiration OR expiration_date = 0)'),
            'expiration' => $expiration, 'getStatement' => TRUE, 'cols' => 'limits_json', 'user_ID' => $user_ID));

        return json_decode($stmt->fetchColumn());
    }

    /**
     * adds up the limits from a set of subscriptions (or addons) to a json obj
     *
     * @param SubscriptionModel[] $subs
     * @param array $total
     */
    static function aggregateLimits(array $subscriptions, array &$json) {
        foreach ($subscriptions as $sub) {
            foreach ($sub->getLimits() as $key => $value) {
                $intv = (int) $value;
                $previous_value = isset($json[$key]) ? $json[$key] : 0;
                switch ($intv) {
                    case SubscriptionModel::UNLIMITED:
                        $json[$key] = SubscriptionModel::UNLIMITED;
                        break;
                    default:
                        $json[$key] = $previous_value == SubscriptionModel::UNLIMITED ? $previous_value : $previous_value + $intv;
                }
            }
        }
    }

    /**
     * b. verify that the existing TOTAL is not persistent.
     * c. save new calculated TOTAL
     *
     * @param $user_ID
     * @param DateTime $expiration
     * @return bool
     */
    function recalculateTOTALLimits($user_ID, DateTime $expiration) {
        $current_T = $this->getUserUnExpiredTOTALLimit($user_ID);
        if ($current_T !== FALSE && $current_T->is_persistent) {
            return true; // return success, but skip recalculation of limits
        }
        $pdo = $this->getPDO();
        $pdo->beginTransaction();
        $save_OK = $this->calculateTOTALForUser($user_ID, $expiration->format(PointsSetBuilder::MYSQLFORMAT));

        if (!$save_OK) {
            $pdo->rollBack();
            return FALSE;
        }

        if ($current_T)
            $this->DeletePreviousTotal($user_ID);

        $pdo->commit();

        return true;
    }

    function DeletePreviousTotal($user_ID) {
        $q = 'DELETE FROM account_limits
					WHERE user_ID = :user_ID
					AND type = \'TOTAL\' ORDER BY ID ASC limit 1';
        $stmt = $this->prepare($q);
        return $stmt->execute(array('user_ID' => $user_ID));
    }

    /**
     * Obtain non-expired, non-canceled addons.
     * expiration_date = 0 means a never-expiring addon.
     *
     * @param $user_ID
     * @param $expiration
     * @param null $addon_ID
     * @return array
     * @throws QubeException
     */
    function getTotalAddOns($user_ID, $expiration, $addon_ID = null) {
        $args = array('user_ID' => $user_ID, 'expiration' => $expiration);
        $q = 'SELECT * FROM account_limits
					WHERE user_ID = :user_ID
					AND (expiration_date = 0 OR expiration_date > :expiration)
					AND cancelation_date = 0
					AND sub_ID = 0
					AND type =\'ADDON\''; // select all summary subscription rows
        if ($addon_ID) {
            $q .= ' AND account_limits.id != :id';
            $args['id'] = $addon_ID;
        }
        $stmt = $this->prepare($q);

        if (false === $stmt->execute($args)) {
            throw new QubeException('Fail');
        }

        return $stmt->fetchAll(PDO::FETCH_CLASS, 'SubscriptionModel');
    }

    function getPermissions() {
//		$q = 'SELECT
//				  DISTINCT
//				  `6q_permissions`.ID,
//				  `6q_permissions`.flag
//				FROM `6q_roleperms`
//				  INNER JOIN `6q_permissions`
//					ON `6q_roleperms`.permission_ID = `6q_permissions`.ID
//				  INNER JOIN `6q_roles`
//					ON `6q_roleperms`.role_ID = `6q_roles`.ID';
        $q = ' SELECT ID, flag FROM 6q_permissions ORDER BY flag';
        $stmt = $this->prepare($q);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, 'QubePermissionModel');
    }

    function updateUserLastLogin(UserModel $User) {
        return $this->getQube()->query('UPDATE %s SET last_login = NOW() WHERE id = %d', $User, $User->getID());
    }

    function getCookieKey(UserModel $User) {
        return $this->getQube()
                        ->query('SELECT MD5(CONCAT( CONVERT( MD5( last_login ) USING latin1 ), password )) FROM %s WHERE id = %d', $User, $User->getID())
                        ->fetch(PDO::FETCH_COLUMN);
    }

    function CountRoles() {
        $resellerID = $this->getActionUser()->can('is_system') ? 0 : $this->getActionUser()->getID();
        $stmnt = $this->getQube()->getQubeDriver()->getDb()->prepare('SELECT COUNT(*) FROM 6q_roles WHERE  reseller_user = :reseller_user OR :reseller_user = 0');
        $stmnt->execute(array('reseller_user' => $resellerID));
        return $stmnt->fetchColumn();
    }

    function SaveRole($userID, $roleID) {
        $this->getQube()->GetDB()->beginTransaction();
        $this->query('DELETE FROM 6q_userroles WHERE user_ID = ' . $userID);
        $this->query('INSERT INTO 6q_userroles (user_ID, role_ID) VALUES (' . $userID . ', ' . $roleID . ' )');
        $this->getQube()->GetDB()->commit();
    }

    function CreateAccountAddOn($user_ID, $billing_ID, stdClass $limits_json, $expiration = null) {
        if (!$expiration)
            $expiration = new DateTime();
        $q = 'INSERT INTO account_limits SET user_ID = :user_ID, sub_ID = 0,
					billing_ref = :billing_ID, limits_json = :limits_json,
					expiration_date = :expiration, type = \'ADDON\'';

        $stmt = $this->prepare($q);

        $result = $stmt->execute(array(
            'user_ID' => $user_ID, 'billing_ID' => $billing_ID,
            'expiration' => PointsSetBuilder::firstDayNextMonth($expiration)->format(PointsSetBuilder::MYSQLFORMAT),
            'limits_json' => json_encode($limits_json))
        );


        $addon_ID = $this->getPDO()->lastInsertId();
        if (!$result)
            return false;

        $expiration = new DateTime();
        $return = $this->recalculateTOTALLimits($user_ID, $expiration);

        if (!$return)
            return false;


        return $addon_ID;
    }

    function getAllAccountLimits($args = array(), $limit = 10, $offset = 0, $filter = null, $order = "expiration_date", $direction = "desc") {
        $actionuser = $this->getActionUser();
        $where = array();

        $q = "SELECT sql_calc_found_rows l.id, username, type, expiration_date, u.created, subscriptions.name, l.user_ID,
				l.sub_ID, datediff(l.expiration_date, :curdate) as days_to_expiration 
				from account_limits l 
				inner join users u on u.id = l.user_ID ";

        if ($actionuser->isSystem()) {
            // can visualize any users.
        } elseif ($actionuser->can('view_users')) {
            // visualize only child users.
            $where[] = 'u.parent_id = ' . $actionuser->getParentID();
        } else {
            // can visualize only 'own' subscriptions. which are probably none.
            $where[] = 'u.id = ' . $actionuser->getID();
        }

        $q .= ' left join subscriptions on subscriptions.ID = l.sub_ID';

//		$args = array();
        $args['curdate'] = date(PointsSetBuilder::MYSQLFORMAT);  // using hardcoded date for mysql-cache purposes.

        if ($filter) {
            if (is_numeric($filter)) {
                $where[] = ' (l.user_ID = :search or l.sub_ID = :search)';
            } else
                $where[] = " (username LIKE CONCAT(CONCAT('%', :search),'%') OR l.id = :search)";

            $args['search'] = $filter;
        }

        // do not check permissions, let higher levels handle that stuff
        if (isset($args['user_id'])) {
            $where[] = 'l.user_ID = :user_id';
        }

        if ($where) {
            $q .= ' WHERE ' . join(' AND ', $where) . ' ';
        }

        if ($order) {
            $q .= " Order By $order $direction";
        }

        if ($limit != null) {
            $q .= " LIMIT $offset, $limit";
        }
        $stmn = $this->getQube()->GetDB()->prepare($q);
        $stmn->execute($args);
        return $stmn->fetchAll(PDO::FETCH_OBJ);
    }

    function CountAccountLimits() {
        $stmnt = $this->getQube()->GetDB()->prepare('SELECT COUNT(*) FROM account_limits');
        $stmnt->execute();
        return $stmnt->fetchColumn();
    }

    function RemoveAddon($user_ID, $ID) {
        $q = "UPDATE account_limits SET expiration_date = CURRENT_DATE() WHERE id = :id AND user_ID = :user_id AND account_limits.type = 'ADDON'";
        $args = array(
            'id' => $ID,
            'user_id' => $user_ID
        );

        $stmn = $this->getQube()->GetDB()->prepare($q);
        return $stmn->execute($args);
    }

    /**
     * @param $user_ID
     * @param DateTime $expiration
     * @param int $addon_ID
     * @param null $subscription_ID
     * @throws QubeException
     * @return array
     */
    public function SumActiveLimits($user_ID, DateTime $expiration, $addon_ID = null, $subscription_ID = null) {
        $json = array();
        /* @var $subscriptions SubscriptionModel[] */
        $subscriptions = $this->getSubscriptionsForTOTAL($user_ID, $expiration->format(PointsSetBuilder::MYSQLFORMAT), $subscription_ID);
        self::aggregateLimits($subscriptions, $subscriptions);

        /** @var SubscriptionModel[] $addons */
        $addons = $this->getTotalAddOns($user_ID, $expiration->format(PointsSetBuilder::MYSQLFORMAT), $addon_ID);
        self::aggregateLimits($addons, $subscriptions);
        return $json;
    }

    public function RemoveSubscription($user_ID, $ID) {
        $q = 'DELETE FROM account_limits WHERE user_ID = :user_ID AND sub_ID = :ID';
        $stmnt = $this->getQube()->GetDB()->prepare($q);
        $stmnt->execute(array(
            'user_ID' => $user_ID,
            'ID' => $ID
        ));
        return $this->recalculateTOTALLimits($user_ID, new DateTime());
    }

    public function GetAllSubscriptions() {
        $q = 'SELECT * FROM subscriptions';
        $smnt = $this->getQube()->GetDB()->prepare($q);
        $smnt->execute();
        return $smnt->fetchAll(PDO::FETCH_CLASS, 'SubscriptionModel');
    }

    /**
     * sets the cancelation_date = now()
     * recalculate limits
     *
     * @param $user_id
     * @param $subscription_id PROVIDE a negative ID to exlude it from cancelation
     * @return bool
     */
    public function CancelSubscription($user_id, $subscription_id) {
        $exclude = $subscription_id < 0;
        $subscription_id = $exclude ? -1 * $subscription_id : $subscription_id;

        $q = 'UPDATE account_limits SET cancelation_date = NOW()
				WHERE user_ID = :user_id AND ' .
                ($exclude ? ' id != :sub_ID ' : ' id = :sub_ID') .
                ' 	AND cancelation_date = 0
				AND `type` = "SUB"';
        $stmnt = $this->getQube()->GetDB()->prepare($q);
        $x1 = $stmnt->execute(array('user_id' => $user_id, 'sub_ID' => $subscription_id));

        return $x1 && $this->recalculateTOTALLimits($user_id, new DateTime());
    }

    /**
     * @param $user_ID
     * @throws QubeException
     */
    function hasPersistentTOTALLimits($user_ID) {
        throw new QubeException;
    }

    public function getMailboxId($user_id) {
        $q = "SELECT mailbox_id FROM phone_numbers WHERE user_id = :user_id";
        $stmt = $this->prepare($q);
        $stmt->execute(array('user_id' => $user_id));
        return $stmt->fetchColumn();
    }

    function UpdateLastUpload($file_path_rel) {
        $recent_time = time() + -1 * UserManagerController::UPLOADED_IF_MTIME;
        $AbsolutePath = UserModel::getAbsoluteUsersStorage() . $file_path_rel;
        if (!FileUtil::FileExists($AbsolutePath)) {
            throw new QubeException($AbsolutePath . ' does not exist!');
        }
        //-- Read Url File to see if its new
        $filetime = filemtime($AbsolutePath);
        if ($filetime < $recent_time) {
            return FALSE;
        }
        $query = "UPDATE users SET last_upload = NOW() WHERE id = :user_id or parent_id = :user_id";
        $statement = $this->prepare($query);
        $statement->execute(array('user_id' => $this->getActionUser()->getID()));
        return TRUE;
    }

    function getRole($user_id) {
        $q = "SELECT B.ID, B.role FROM 6q_userroles A LEFT JOIN 6q_roles B ON A.role_ID = B.ID WHERE A.user_id = :user_id";
        $stmt = $this->prepare($q);
        $stmt->execute(array('user_id' => $user_id));
        return $stmt->fetchAll();
    }

    function getUserInfo($user_id) {
        $q = "SELECT * FROM user_info WHERE user_id = :user_id";
        $stmt = $this->prepare($q);
        $stmt->execute(array('user_id' => $user_id));
        return $stmt->fetch();
    }

    function getUsersTable($user_id) {
        $q = "SELECT * FROM users WHERE id = :user_id";
        $stmt = $this->prepare($q);
        $stmt->execute(array('user_id' => $user_id));
        return $stmt->fetch();
    }

    function deleteRole($role_id, $new_role_id = false) {
        if (!$this->getActionUser()->can('edit_role', $role_id)) {
            return false;
        }
        $this->beginTransaction();
        try {
            if ($new_role_id) {
                $stmt = $this->prepare('UPDATE 6q_userroles SET role_ID = :newRole WHERE role_ID = :oldRole');
                $stmt->execute(array('oldRole' => $role_id, 'newRole' => $new_role_id));
                $update = $stmt->rowCount();
            }
            $stmt = $this->prepare("DELETE FROM 6q_roles WHERE ID = :roleID AND NOT EXISTS (SELECT ID FROM 6q_userroles WHERE role_ID = :roleID)");
            $stmt->execute(array('roleID' => $role_id));

            $this->commit();
            $update = $stmt->rowCount();
            return $update;
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Requires an array with the following data:
     * <ul>
     * 		<li>user_id</li>
     * 		<li>firstName</li>
     * 		<li>lastName</li>
     * 		<li>company</li>
     * 		<li>phone</li>
     * 		<li>emailAddress</li>
     * </ul>
     * @param array $data
     * @return bool
     */
    public function saveUserInfo($data) {
        $stmt = $this->prepare(
                'INSERT INTO user_info (user_id, firstname, lastname, company, phone, email) ' .
                'VALUES (:user_id, :firstName, :lastName, :company, :phone, :emailAddress)'
        );

        $stmt->execute($data);
        return $stmt->rowCount() > 0;
    }

}
