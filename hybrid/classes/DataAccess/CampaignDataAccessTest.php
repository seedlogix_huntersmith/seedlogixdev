<?php
/**
 * Created by PhpStorm.
 * User: alan
 * Date: 10/21/14
 * Time: 9:47 AM
 */

namespace hybrid\classes\DataAccess;

use Qube\Hybrid\DataAccess\CampaignDataAccess;

require_once TESTDIR . 'Qube_TestCaseIntegration.php';

class CampaignDataAccessTest extends \Qube_TestCaseIntegration
{


	public function testCampaignDataCount()
	{


		$datax = new CampaignDataAccess();

		$countresult = $datax->getAllTouchPointCount(array('user_id' => '6610', 'campaign_id' => '7221'));

		$this->assertEquals(1, $countresult);


	}

	public function testCampaignDataFetch()
	{


		$datax = new CampaignDataAccess();

		$countresult = $datax->getAllTouchPointData(array('user_id' => '6610', 'campaign_id' => '7221'), 10, 0, null, "id", "asc");


		$this->assertNotEmpty($countresult);


	}
}