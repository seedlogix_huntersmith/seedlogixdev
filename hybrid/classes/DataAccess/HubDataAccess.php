<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubDataAccess
 *
 * @author amado
 */
class HubDataAccess extends QubeDataAccess
{
	const STATSTBL	=	'piwikreports';

	function getPicValue($hub_ID, $column)
	{
		list($internalname, $column)	=	self::getColumnInfo($column);
		$q	=	'select ' . $column . ' FROM slides WHERE hub_id = :hub_id and internalname = :internalname';
		$pstmt	=	$this->prepare($q);
		$pstmt->execute(array('hub_id' => $hub_ID, 'internalname' => $internalname));
		return $pstmt->fetchColumn();
	}

    /**
     * a get Hub Themes
     *
     */
    function getHubThemes($selected_theme = 0, $touchpoint_type)
    {
		if($selected_theme == ''){$selected_theme = 0;}
        $site_themes = $this->getQube()->queryObjects('HubThemeModel')
//            ->Where('(user_class_access = %d OR user_class_access = 0)',
//                $this->getActionUser()->class)
			->Where("theme_type = '" . $touchpoint_type . "' AND hybrid = 1")
            ->Where('(user_id_access = %d OR user_id_access = 0)',
                $this->getActionUser()->getID())
            ->orderBy('id = ' . $selected_theme . ' DESC')->fields("id, name, ($selected_theme != 0 AND id = $selected_theme) as is_selected, thumbnail, 'regular' as type ")
            ->Exec()->fetchAll();
        return $site_themes;
    }
	function getHubSingleTheme($selected_theme = 0)
	{
		$site_themes = $this->getQube()->queryObjects('HubThemeModel')
			->Where('(user_class_access = %d OR user_class_access = 0)',
				$this->getActionUser()->class)
			->Where("id = " . $selected_theme)
//			->Where('(user_id_access = %d OR user_id_access = 0)', not sure what means user_id_access
//				$this->getActionUser()->getID())
			->orderBy('id = ' . $selected_theme . ' DESC')->fields("id, name, ($selected_theme != 0 AND id = $selected_theme) as is_selected, NULL as thumbnail, 'regular' as type ")
			->Exec()->fetchAll();

		if(!$site_themes)
			throw new QubeException("Theme $selected_theme not found");

		return $site_themes[0];
	}

    function setHubParent($child_ID, $parent_ID)
    {
        $check_CLASS = false;
        if ($check_CLASS) {
            $q = 'select user_id, multi_user_classes FROM hub
                      WHERE id = :child_ID AND user_id = :user_ID';
            $stmt = $this->prepare($q);

            $result = $stmt->execute(array('user_ID' => $this->getActionUser()->getID(),
                'child_ID' => $child_ID));
            if (FALSE === $result || $stmt->rowCount() != 1) {
                return false;
            }

            $parent_info = $stmt->fetchObject();
            $allowed_classes = explode('::', $parent_info->multi_user_classes);

            if (!$allowed_classes || !in_array($this->getActionUser()->class,
                    $allowed_classes)
            ) {
                return false;   // not allowed
            }
        }

        return $this->CopyParentHubValues($child_ID, $this->getActionUser()->getID(), $parent_ID, $this->getActionUser()->getParentID());
    }

    function CopyParentHubValues($child_ID, $child_user_ID, $parent_ID, $user_parent_id)
    {
        $pdo = $this->getPDO('master');
        $pdo->beginTransaction();
        $s = 'select * from hub where id = ' . (int)$parent_ID . ' AND user_id = ' . (int)$user_parent_id;
        $parent_row = $pdo->query($s)->fetch(PDO::FETCH_ASSOC);

        $excludeFields = array('id', 'id_duped', 'cid', 'user_id', 'user_id_created', 'user_parent_id', 'hub_parent_id',
            'tracking_id', 'name', 'domain', 'mobile_domain', 'website_url', 'google_webmaster',
            'google_tracking', 'seo_url', 'connect_blog_id', 'multi_user', 'multi_user_classes',
            'multi_user_fields', 'last_updated', 'created', 'httphostkey');

        $updatecols = array();
        $placeholders   =   array();
        foreach($parent_row as $col => $val)
        {
            if(in_array($col, $excludeFields)) continue;
            $placeholder = self::keyToPlaceHolder($col);
            $updatecols[]   =   " `$col` = :" . $placeholder;
            $placeholders[$placeholder] =   $val;
        }

        $update_q = 'UPDATE hub SET hub_parent_id = :parent_ID, ' . implode(", ", $updatecols) . '
            WHERE id = :child_ID AND user_id = :child_user_ID';

        $pstmt  =   $pdo->prepare($update_q);
        $placeholders['child_ID']    =   $child_ID;
        $placeholders['child_user_ID']  =   $child_user_ID;
        $placeholders['parent_ID']  =   $parent_ID;

        if(FALSE == $pstmt->execute($placeholders))
        {
            $pdo->rollBack();
            return false;
        }
		$this->CopyParentHubImages($parent_ID, $child_ID, $child_user_ID, $user_parent_id, $parent_row);
		$this->CopyParentHubPages($parent_ID, $child_ID, $child_user_ID, $user_parent_id);

		return $pdo->commit();
    }

	function CopyParentHubPages($parent_ID, $child_ID, $parent_user_ID, $child_user_ID, $child_nav_parent_ID = 0, $nav_parent_ID = 0, $nav_root_ID = 0)
	{
		$excludePageFields = array('id', 'hub_id', 'user_id', 'user_parent_id', 'nav_parent_id', 'page_parent_id',
			'nav_root_id', 'multi_user', 'multi_user_fields', 'last_edit', 'created');

		$prepared_stmt = NULL;
		//remove existing pages
		$query = "DELETE FROM hub_page2 WHERE hub_id = '".$child_ID."' AND user_id = '".$child_user_ID."'";
		$this->query($query);
		//copy MU pages (also copies page images)
//		$copyPages = $this->copyV2MUPages($child_ID, $parent_ID, $child_user_ID, $parent_user_ID);

		$query = "SELECT * FROM hub_page2 WHERE hub_id = '".$parent_ID."' AND user_id = '".$parent_user_ID."' AND trashed = '0000-00-00' AND nav_parent_id = $nav_parent_ID";
		$muPages = $this->query($query);

		$copy_images = array();
		while($row = $muPages->fetchObject()){
			$rowValues = array();
			if(NULL == $prepared_stmt) {
				$updateColumns = array();
				foreach ($row as $key => $value) {
					if (in_array($key, $excludePageFields)) continue;
					$column = self::keyToPlaceHolder($key);
					$updateColumns[] = "`$key` = :$column";
				}
				$updateColumns[] = "hub_id = $child_ID, user_ID = $child_user_ID, user_parent_id = $parent_user_ID, nav_parent_id = $child_nav_parent_ID, nav_root_id = $nav_root_ID,
					page_parent_id = $row->id, created = NOW()";
				$prepared_stmt	= $this->prepare('INSERT INTO hub_page2 SET ' . implode(', ', $updateColumns) );
			}
			foreach ($row as $key => $value) {
				if (in_array($key, $excludePageFields)) continue;
				$column = self::keyToPlaceHolder($key);
				$rowValues[$column]	=	$value;
			}
			$prepared_stmt->execute($rowValues);

			$cur_nav_ID = $this->getPDO('master')->lastInsertId();
			$is_main_section = $row->nav_root_id == $row->nav_parent_id;
			$cur_nav_root_ID = $is_main_section ? $cur_nav_ID : $this->getNavRoot($cur_nav_ID, $child_user_ID);
			if ($row->type == 'nav') {
				//if the item was a nav section, recurse through this function again with the proper vars
				if ($row->nav_root_id > 0) {
					//if the copied nav was a main section (has same parent_id as root_id)
					//use the newly created nav section's ID as its child pages' root ID
				}
				//recurse:
				$copy = $this->CopyParentHubPages($parent_ID, $child_ID, $parent_user_ID, $child_user_ID, $cur_nav_ID, $row->id, $cur_nav_root_ID);
				if (!$copy) {
					return false;
				}
			} else if ($row->type == 'page') {
				//make sure the page's nav_root_id is being set properly
//				if (!$root_ID) {
//					$query = "UPDATE hub_page2 SET nav_root_id = '" . $this->getNavRoot($cur_nav_ID, $child_user_ID) . "' WHERE id = '" . $cur_nav_ID . "' LIMIT 1";
//					$this->query($query);
//				}
				if ($row->page_photo) {
					$copy_images[]	=	$row->page_photo;
				}
			}
		}

		if (!empty($copy_images)) {
			$this->symlinkUserFiles($parent_user_ID, $child_user_ID, $copy_images, 'hub_page/');
		}
		return true;
	}

	function CopyParentHubImages($parent_ID, $child_ID, $child_user_ID, $user_parent_ID, $parent_row)
	{
		// replace child hub slides
		$this->query('DELETE FROM slides WHERE hub_id = ' . $child_ID);
		$this->query('INSERT INTO slides (hub_id, user_id, title, background, url, description, internalname)
            SELECT ' . $child_ID . ', ' . $child_user_ID . ', title, background, url, description, internalname
            FROM slides s
            WHERE s.hub_id = ' . $parent_ID);


		$parent_pic_names = $this->query('SELECT background FROM slides WHERE hub_ID = ' . $parent_ID)->fetchAll(PDO::FETCH_COLUMN, 0);

		$imgs = array('logo', 'bg_img');
		foreach($imgs as $key){
			if($parent_row[$key]	==	'') continue;
			$parent_pic_names	=	$parent_row[$key];
		}

		$this->symlinkUserFiles($user_parent_ID, $child_user_ID, $parent_pic_names, 'hub/');
	}

	function symlinkUserFiles($user_source_ID, $user_target_ID, $filenames, $path = '')
	{
		if($user_target_ID == $user_target_ID) return false;
		$sourceLoc = UserModel::getStorage($user_source_ID, true, $path);// . '/' . $path;
		$targetLoc = UserModel::getStorage($user_target_ID, true, $path);// . '/' . $path;

		$filesarr = (array)$filenames;

		foreach($filesarr as $basename)
		{
			FileUtil::CreateSymlink($sourceLoc . $basename, $targetLoc . $basename);
		}
	}
    /**
     * @return array
     */
    function getMuSitesAsThemes($selected_theme  = 0, $touchpoint_type = '')
    {
        $user_parent_id = $this->getActionUser()->getParentID();
        $q = "select id, name, ($selected_theme != 0 AND id = $selected_theme) as is_selected, 'mu' as type, NULL as thumbnail, id as hub_parent_id, user_id as owner
 				FROM hub
 				WHERE user_id = $user_parent_id AND multi_user = 1 AND trashed = 0 AND ( '$touchpoint_type' = '' OR touchpoint_type = '$touchpoint_type')
 				ORDER BY ($selected_theme!= 0 AND id = $selected_theme ) desc";
        $stmt = $this->query($q);
        return $stmt->fetchAll(PDO::FETCH_CLASS, 'HubThemeModel');
    }

    /**
     * inverse of getColumnInfo
     *
     * @param $internalname
     * @param $slidecol
     * @throws QubeException
     */
    static function convertSlideColToHubCol($internalname, $slidecol)
    {
        switch($internalname)
        {
            case 'one':
            case 'two':
            case 'three':
            case 'four':
            case 'five':
            case 'six':
            case 'seven':
            case 'eight':
            case 'nine':
            case 'ten':
                break;
            default:
                throw new QubeException();
        }

        $column=    '';
        switch($slidecol)
        {
            case 'description':
                $column=	'desc';
                break;
            case 'urlenabled':
            case 'title':
                $column=    $slidecol;
                break;
            case 'url':
                $column	=	'link';
                break;
            case 'background':
                $column	=	'';
                break;
            default:
                throw new QubeException();
        }
        return "pic_$internalname" . ($column ? "_$column" : "");
    }

	/**
	* accepts a column such as pic_one_title
    * returns array of (one, title) or (three, description) etc..
	**/
	static function getColumnInfo($column)
	{
		if(!preg_match('/^pic_(.+)$/', $column, $matches))	throw new QubeException();
		
		$split	=	explode('_', $matches[1]);
		list($internalname, $column)	= count($split) > 1 ? $split : array($split[0], '');

		switch($internalname)
		{
			case 'one':
			case 'two':
			case 'three':
			case 'four':
			case 'five':
			case 'six':
			case 'seven':
			case 'eight':
			case 'nine':
			case 'ten':
				break;
			default:
				throw new QubeException();
		}		

		
		switch($column)
		{
			case 'desc':
				$column=	'description';
				break;
            case 'urlenabled':
			case 'title':
				break;
			case 'link':
				$column	=	'url';
				break;
			case '':
				$column	=	'background';
				break;
			default:
				throw new QubeException();
		}

		return array($internalname, $column);
	}

	static function getColumnName($column)
	{
		switch($column)
		{
			case 'desc':
				$column=	'description';
				break;
			case 'title':
				break;
			case 'link':
				$column	=	'url';
				break;
			case '':
				$column	=	'background';
				break;
			default:
				throw new QubeException();
		}
		return $column;
	}
	
	function getPics($hub_ID, &$pics)
	{
		$q	=	"select concat('pic_', s.internalname) as internalname, s.title, 
						s.background as image, s.description as `desc`, s.url as link from slides s where s.hub_id = %d AND s.deleted = 0";
		
		$res	=	$this->getQube()->squery($q, $hub_ID);
		while($r	=	$res->fetchObject()){
				$pics[$r->internalname]	=	$r->image;
				$pics[$r->internalname	.	'_title']	=	$r->title;
				$pics[$r->internalname	.	'_desc']	=	$r->desc;
				$pics[$r->internalname	.	'_link']	=	$r->link;
		}
		return $res->rowCount();
	}
	
	function updatePic($hub_ID, $column, $value)
	{
		if(!preg_match('/^pic_(.+)$/', $column, $matches))	throw new QubeException();
		
		list($internalname, $column)	=	$this->getColumnInfo($column);
		
		$q	=	'INSERT INTO slides SET user_id = (select user_id from hub where id = :hub_id), hub_id = :hub_id, internalname=:internalname, ' . $column . ' = :value ON DUPLICATE KEY UPDATE ' . $column . ' = :value';
		$pstmt	=	$this->getQube()->GetPDO()->prepare($q);
		
		$pstmt->execute(array('internalname' => $internalname, 'value' => $value, 'hub_id' => $hub_ID));
	}

	static function findSiteTheme(HubModel $hub, array $muSites, array $hubthemes)
	{
		if($hub->hub_parent_id != 0 && count($muSites))
		{
			foreach($muSites AS $musite){
				if($musite->is_selected)
					return $musite;
			}
		}

		if($hub->theme != 0 && count($hubthemes))
		{
			foreach($hubthemes as $hubtheme){
				if($hubtheme->is_selected)
					return $hubtheme;
			}
		}
		return new HubThemeModel(array('is_selected' => 0, 'name' => 'None', 'id' => $hub->theme, 'type' => 0, 'thumbnail' => ''));
	}

	public function hasFormTagsInFields($fields, $hub_id)
	{
		$embedFields = join(', ', array_intersect(HubOutput::getEmbedFields(), $fields));
		$q = "SELECT $embedFields FROM hub WHERE id = :hub_id AND has_custom_form = 1";
		$stmt = $this->prepare($q);
		$stmt->execute(array('hub_id' => $hub_id));
		$rowData = $stmt->fetch(PDO::FETCH_ASSOC);
		if($rowData && HubModel::hasEmbedForm($rowData)){
			return true;
		}

		return false;
	}

	public function hasTagsInFields($fields, $ID)
	{
		$embedFields = join(', ', array_intersect(Hub::getParsableFields(), $fields));
		$q = "SELECT $embedFields FROM hub WHERE id = :hub_id AND has_tags = 1";
		$stmt = $this->prepare($q);
		$stmt->execute(array('hub_id' => $ID ));
		$rowData = $stmt->fetch(PDO::FETCH_ASSOC);
		if($rowData && HubModel::hasTags($rowData)){
			return true;
		}
		return false;
	}

    /**
     * Used to propagate a value from a master to the slave (child) hubs.
     * The action user for this method should be the master site user_id.
     *
     * @param WebsiteModel $master
     * @param $field
     * @param $value
     * @param int $keepCustomVals
     * @return bool
     * @throws QubeException
     */
    function UpdateChildHubsColumnValue(WebsiteModel $master, $field, $value, $keepCustomVals = 1, $changedVars)
    {
        if(!$master->isMultiSite() || $master->user_id != $this->getActionUserID()) return false;

        $ispic	=	preg_match('/^pic_/', $field);
		$isKeyword = preg_match('/keyword[0-9]/', $field);
        $master_id = $master->getID();

        if($ispic)
        {
            return $this->PropagateSiteSlideUpdate($field, $value, $keepCustomVals, $master_id);
        }


		if($isKeyword){
			$stmnt = $this->prepare('UPDATE hub SET `' . $field . '` = :newvalue WHERE concat(keyword1, keyword2, keyword3, keyword4, keyword5, keyword6) = concat(ifnull(:keyword1, \'\'), ifnull(:keyword2, \'\'), ifnull(:keyword3, \'\'), ifnull(:keyword4, \'\'), ifnull(:keyword5, \'\'), ifnull(:keyword6, \'\')) AND hub_parent_id = :hub_id AND (user_parent_id = :user_id OR user_id = :user_id)');
			return $stmnt->execute(array(
				'newvalue' => $value,
				'keyword1' => $master->get('keyword1'),
				'keyword2' => $master->get('keyword2'),
				'keyword3' => $master->get('keyword3'),
				'keyword4' => $master->get('keyword4'),
				'keyword5' => $master->get('keyword5'),
				'keyword6' => $master->get('keyword6'),
				'hub_id' => $master->getID(),
				'user_id' => $this->getActionUserID()
				));
		}

        // update child sites only.
        $query = 'UPDATE hub SET `' . $field . '` = :newvalue WHERE hub_parent_id = :hub_id AND (user_parent_id = :user_id OR user_id = :user_id)';

        // this updates columns where existing value is unchanged (value = original.)
        if($keepCustomVals){
            $query .= " AND `$field` = :originalvalue";
        }
        $originalvalue = $master->get($field);

        // todo update has_tags, has_custom_form

        $pstmt = $this->prepare($query);
        $params = array('hub_id' => $master_id, 'user_id' => $this->getActionUserID(), 'newvalue' => $value);
        if($keepCustomVals)
        {
            $params['originalvalue'] = $originalvalue;
        }
        return $pstmt->execute($params);
    }

	/**
	 * @param $hub_id int
	 * @return HubFieldLocks
	 */
	public function getWebsiteLocks($hub_id){
		$q = "SELECT multi_user_fields FROM hub WHERE id = (SELECT if(hub_parent_id=0,id,hub_parent_id) FROM hub WHERE id = :hub_id)";
		$stmt = $this->prepare($q);
		$stmt->execute(array('hub_id' => $hub_id));
		/** @var WebsiteModel $model */
		$model = $stmt->fetchObject('WebsiteModel');
		if(!$model)
			return false;
		return $model->getFieldLocker();
	}

    /**
     * Propagates update to sides based on pic_XXX_YYY column strings
     * @param $field
     * @param $keepCustomVals
     * @param $master_hub_id
     * @return array
     * @throws QubeException
     */
    public function PropagateSiteSlideUpdate($field, $value, $keepCustomVals, $master_hub_id)
    {
        $originalvalue = $this->getPicValue($master_hub_id, $field);
        $isNewPic = $originalvalue === false;
        // sets the slide values for the parent hub
//            $da->updatePic($master_id, $field, $value);

        list($internalname, $field) = HubDataAccess::getColumnInfo($field);

        if ($isNewPic) {
            // insert new slides for each child-site (hub)
            $query = "INSERT ignore INTO slides (user_id, hub_id, internalname, $field) " .
                'select user_id, id, "' . $internalname . '", :newvalue from hub WHERE hub_parent_id = :hub_id AND (user_parent_id = :user_id OR user_id = :user_id)';
        } else {
            // update child sites ONLY
            $query = "update slides s
                SET s.`$field` = :newvalue
                WHERE s.internalname = '$internalname' AND s.hub_id IN (SELECT id FROM hub WHERE hub_parent_id = :hub_id AND (user_parent_id = :user_id OR user_id = :user_id))";
            if ($keepCustomVals)
                $query .= ' AND s.`' . $field . '` = :originalvalue';
        }

        $pstmt = $this->prepare($query);
        $params = array('hub_id' => $master_hub_id, 'user_id' => $this->getActionUserID(), 'newvalue' => $value);
        if($keepCustomVals)
        {
            $params['originalvalue'] = $originalvalue;
        }
        return $pstmt->execute($params);
    }

    /**
     * Note: Duplicates slides but only copies the title, and none of the other columns.
     * @todo fix insert / select query to copy over all slide columns.
     *
     * @param $master_slide
     */
    function PropagateNewSiteSlide($master_slide)
    {
        $query  =   "insert into slides (hub_id, user_id, internalname, title)
      select sh.id, sh.user_id, ms.internalname, ms.title from hub sh inner join slides ms
	  on sh.hub_parent_id  = ms.hub_id
	  where ms.id = {$master_slide}";
        $stmt   =   $this->query($query);
        return $stmt->rowCount();
    }
}
