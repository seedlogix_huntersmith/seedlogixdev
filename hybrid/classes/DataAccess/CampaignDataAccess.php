<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\DataAccess;
use LeadFormModel;
USE \PDO;

/**
 * Description of CampaignDataAccess
 *
 * @author amado
 */
class CampaignDataAccess extends \QubeDataAccess {
	
	function RefreshCampaignTouchPointCounts($campaign_ID)
	{
		$q	=	'UPDATE campaigns c set c.total_touchpoints = (select count(*) from hub where cid = c.id) + (select count(*) from blogs where cid = c.id),
    c.trashed_touchpoints = (select count(*) from hub where cid = c.id and trashed != 0) + (select count(*) from blogs where cid = c.id and trashed != 0) where c.id = ' . (int)$campaign_ID;
		
		return $this->query($q)->rowCount();
	}
	
	function queryProspects()
        {
            
        }
	/**
	 * 
	 * @param type $campaign_ID
	 * @return CampaignModel
	 */
	function getCampaign($campaign_ID)
	{
		$action_user	=	$this->getActionUser();
		$stmt	=	$this->getQube()->queryObjects('CampaignModel')
                    ->Where('id = %d AND (user_id = %d OR user_parent_ID = %d)', $campaign_ID, $action_user->getID(), $action_user->getParentID());
		
		return $stmt->Exec()->fetch();
	}
	
	function getCampaignWithTouchpointTotals($campaign_ID)
	{
//		$query	=	"select x.*, x.LANDING + x.SOCIAL + x.WEBSITE + x.MOBILE + @blogcount as TOTAL from (
//		select c.id, (select count(*) FROM blogs WHERE user_id = c.user_id and cid = c.id AND trashed = 0) as BLOG, 
//	sum((`hub`.`touchpoint_type` = 'WEBSITE')) AS `WEBSITE`,
//	sum((`hub`.`touchpoint_type` = 'LANDING')) AS `LANDING`,
//	sum((`hub`.`touchpoint_type` = 'SOCIAL')) AS `SOCIAL`,
//	sum((`hub`.`touchpoint_type` = 'MOBILE')) AS `MOBILE`
//	from campaigns c left join hub on (hub.user_id = c.id and hub.cid = c.id and hub.trashed = 0)
//	where c.id	=	$campaign_ID
//	group by c.id ) x";
//		
//		$stmt	=	$this->query($query);
//		
//		return $stmt->fetchObject('CampaignModel');
//		
		$stmt	=	$this->getQube()->queryObjects('CampaignModel')
                    ->Where('id = %d AND (user_id = dashboard_userid() OR 1 = %d)', $campaign_ID, $this->getActionUser()->can('is_system'))
                    ->leftJoin('dashboard_touchpoints p', 'p.cid = T.id')
                    ->addFields('(select count(*) FROM blogs b WHERE b.trashed = 0 AND b.user_id = T.user_id and b.cid = T.id) as BLOG, p.*');

		
		$obj	=	$stmt->Exec()->fetch();
		
		return $obj;
	}

	function getAllTouchPointDatax($data, $limit = 10, $offset = 0, $filter = null, $order = "id", $direction = "asc")
	{
		function filter($type,$filter)
		{
			switch($type){
				case 'TOUCHPOINT':
					$qr = " AND (T.`name` LIKE CONCAT('%', :search,'%') OR T.`httphostkey` LIKE CONCAT('%', :search,'%')";
					if(is_numeric($filter)){$qr .= " OR T.`id` LIKE CONCAT('%', :search, '%')";}
					$qr .= ')';

					break;
				case 'BLOG':
					$qr = " AND (B.`blog_title` LIKE CONCAT(CONCAT('%', :search),'%')";
					if(is_numeric($filter)){$qr .= " OR B.`id` LIKE CONCAT('%', :search, '%')";}
					$qr .= ')';
					break;
			}
			return $qr;
		}



		list($interval, $period) = preg_split('/-/', \Cookie::getVal('chartconfig', '7-DAY'));
		switch ($period){
			case 'DAY':
			case 'MONTH':
				break;
			case 'ALL':
//				break;
			default:
				$period = 'DAY';
				$interval = 7;
				break;
		}
		$actionUser = $this->getActionUser();
		$userID = $actionUser->getID();
		$campaignID = $data['campaign_id'];
		$touchpoint_type = $data['touchpoint_type'];

		$whereUserLevel = 'user_id = :userID';
		$blogWhere = 'user_id = :userID';

		$args = array(
			'userID' => $userID,
			'campaign_ID' => $campaignID,
			'interval' => (int)$interval,
			'period' => $period,
			'limit' => (int)$interval * 2
		);
		if ($filter){$args['search'] = $filter;}

		//-- Check for Touchpoint Type --//
		switch ($touchpoint_type) {
			case 'BLOG':
				//--Replace Query to get blogs only--//
				$q = "SELECT SQL_CALC_FOUND_ROWS * FROM(
					$blogjoin
 			left join
			$statsjoin
stats on
stats.tbl_ID = B.id AND stats.tbl = 'blog'
 			WHERE B.trashed = 0 AND B.cid = :campaign_ID and (B.$blogWhere)" . ($filter ? filter('BLOG', $filter) : '').") Total LEFT JOIN
(SELECT
		tbl,
		tbl_ID,
		cid,
			SUM(IF(point_date < DATE_SUB(CURRENT_DATE, INTERVAL :interval $period), 0, nb_visits)) / SUM(IF(point_date < DATE_SUB(CURRENT_DATE, INTERVAL :interval $period), nb_visits, 0)) * 100 AS visits_changes
			FROM
			piwikreports WHERE point_date>DATE_SUB(current_date,interval :limit day) GROUP BY cid , tbl , tbl_ID) Cview ON tbl = 'blog'
			AND Total.id = Cview.tbl_ID";
				break;

			case 'WEBSITE':
			case 'MULTIUSER':
			case 'MOBILE':
			case 'SOCIAL':
			case 'PROFILE':
			case 'LANDING':
			$q = "SELECT * FROM(
			$hubjoin
left join
 $statsjoin
stats on
stats.tbl_ID = T.id AND stats.tbl = 'hub'
where T.trashed = 0 AND T.cid = :campaign_ID AND (T.$whereUserLevel) AND "
							. "T.`touchpoint_type` = '" . $touchpoint_type .  "'" . ($filter ? filter('TOUCHPOINT', $filter) : '')."
	) 
	Total 
	LEFT JOIN
$piwiksjoin ) Cview ON tbl = 'hub' AND Total.id = Cview.tbl_ID";
				break;

			case '':
				$q = "SELECT SQL_CALC_FOUND_ROWS * FROM(SELECT * FROM
						(SELECT T.id, T.name, T.touchpoint_type, T.created, ifnull(total_leads, 0) total_leads, ifnull(total_visits, 0) total_visits, ifnull(total_leads / total_visits * 100, 0) rate FROM hub as T
							left join
							(SELECT tbl, tbl_ID, user_ID, cid, ifnull(sum(nb_leads), 0) total_leads, ifnull(sum(nb_visits), 0) total_visits from piwikreports
								where cid = :campaign_ID
								group by tbl, tbl_ID, user_ID, cid) stats on
							stats.tbl_ID = T.id AND stats.tbl = 'hub'
							where T.trashed = 0 AND T.cid = :campaign_ID AND (T.$whereUserLevel) " . ($filter ? filter('TOUCHPOINT', $filter) : '') .
							" UNION ALL
							(SELECT B.`id`,B.`blog_title` as name,'BLOG' as touchpoint_type,B.`created`, ifnull(total_leads, 0) total_leads, ifnull(total_visits, 0) total_visits, ifnull(total_leads / total_visits * 100, 0) rate FROM blogs as B
							left join
								(SELECT tbl, tbl_ID, user_ID, cid, ifnull(sum(nb_leads), 0) total_leads, ifnull(sum(nb_visits), 0) total_visits from piwikreports
									where cid = :campaign_ID
									group by tbl, tbl_ID, user_ID, cid) stats on
								stats.tbl_ID = B.id AND stats.tbl = 'blog'
								WHERE B.trashed = 0 AND B.cid = :campaign_ID and B.$blogWhere " . ($filter ? filter('BLOG', $filter) : '') . ")) A)  Total LEFT JOIN
    							(SELECT
									tbl,
									tbl_ID,
									cid,
										SUM(IF(point_date < DATE_SUB(CURRENT_DATE, INTERVAL :interval $period), 0, nb_visits)) / SUM(IF(point_date < DATE_SUB(CURRENT_DATE, INTERVAL :interval $period), nb_visits, 0)) * 100 AS visits_changes
									FROM
										piwikreports WHERE point_date>DATE_SUB(current_date, INTERVAL :limit $period) GROUP BY cid , tbl , tbl_ID) Cview ON ((touchpoint_type != 'BLOG'AND tbl = 'hub') OR (touchpoint_type = 'BLOG'	AND tbl = 'blog'))
										AND Total.ID = Cview.tbl_ID";
				break;
		}


		if($order){
			$q .= " Order By $order $direction";
		}

		if($limit != null){
			$q .= " LIMIT $offset, $limit";
		}
		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		return $stmn->fetchAll(\PDO::FETCH_OBJ);
	}

	function getAllTouchPointCount($data)
	{
		$actionUser = $this->getActionUser();
		$userID = $data['user_id'];
		$campaignID = $data['campaign_id'];
		$touchpoint_type = $data['touchpoint_type'];
		$args = array('campaign_ID' => $campaignID);
		$websitesWhere = array('trashed = 0', 'cid = :campaign_ID');
		$blogWhere = array('trashed = 0', 'cid = :campaign_ID');

		switch($touchpoint_type){
			case 'WEBSITE':
			case 'MULTIUSER':
			case 'MOBILE':
			case 'PROFILE':
			case 'SOCIAL':
			case 'LANDING':
				if($actionUser->can("view_$touchpoint_type")){
					$websitesWhere[] = 'user_parent_id = :parentID';
					$args['parentID'] = $actionUser->getParentID();
				}else{
					$websitesWhere[] = 'user_id = :userID';
					$args['userID'] = $userID;
				}
				$websitesWhere[] = "touchpoint_type = '$touchpoint_type'";
			break;
			case 'BLOG':
				if($actionUser->can("view_BLOG")){
					$blogWhere[] = 'user_parent_id = :parentID';
					$args['parentID'] = $actionUser->getParentID();
				}else{
					$blogWhere[] = 'user_id = :userID';
					$args['userID'] = $userID;
				}

				break;
			case '':
				$touchpointsWhere = array();
				if($actionUser->can("view_WEBSITE")){
					$touchpointsWhere[] = "(touchpoint_type = 'WEBSITE' AND user_parent_id = :parentID)";
				}

				if($actionUser->can("view_LANDING")){
					$touchpointsWhere[] = "(touchpoint_type = 'LANDING' AND user_parent_id = :parentID)";
				}

				if($actionUser->can("view_PROFILE")){
					$touchpointsWhere[] = "(touchpoint_type = 'PROFILE' AND user_parent_id = :parentID)";
				}

				if($actionUser->can("view_MOBILE")){
					$touchpointsWhere[] = "(touchpoint_type = 'MOBILE' AND user_parent_id = :parentID)";
				}

				if($actionUser->can("view_SOCIAL")){
					$touchpointsWhere[] = "(touchpoint_type = 'SOCIAL' AND user_parent_id = :parentID)";
				}

				if(!empty($touchpointsWhere)){
					$websitesWhere[] = join(' OR ', $touchpointsWhere);
					$args['parentID'] = $actionUser->getParentID();
				}

				if($actionUser->can("view_BLOG")){
					$blogWhere[] = "user_parent_id = :parentID";
					$args['parentID'] = $actionUser->getParentID();
				}

				if(!isset($args['parentID'])){
					$args['userID'] = $userID;
				}
				break;
		}

		$websites = '0';
		$blogs = '0';
		if($touchpoint_type != 'BLOG' || $touchpoint_type == ''){
			$websitesWhere =  '(' . join(') AND (', $websitesWhere) . ')';
			$websites = "SELECT count(*) " .
				"FROM hub " .
				"WHERE $websitesWhere";
		}

		if($touchpoint_type == 'BLOG' || $touchpoint_type == '') {
			$blogWhere = '(' . join(') AND (', $blogWhere) . ')';
			$blogs = "SELECT count(*) " .
				"FROM blogs " .
				"WHERE $blogWhere";
		}


		$q = "SELECT (($websites) + ($blogs)) A ";
		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		$drt = $stmn->fetchColumn();
		return $drt;
	}

	/**
	 * @param array $options
	 * @return \WebsiteModel[]
	 */
	public function getCampaigns($options = array()){
		$actionUser = $this->getActionUser();
		$where = '';
		$orderBy = '';
		$qOpts	=	(object)self::getQueryOptions($options);
//		$options = array();
		if(TRUE){//!$actionUser->isSystem()){
			if($actionUser->can('view_campaign')){
				$whereParts[] = 'user_parent_id = :parent_id';
				$options['parent_id'] = $actionUser->getParentID();
			}else{
				$whereParts[] = 'user_id = :user_id';
				$options['user_id'] = $actionUser->getID();
			}
		}
		if(!empty($whereParts)){
			$where = 'WHERE  ' . join(' AND ', $whereParts);
		}

		if($options['orderBy'] && !empty($options['orderBy'])){
			$orderBy = 'ORDER BY ' . join(', ', $options['orderBy']) . ' DESC';
			unset($options['orderBy']);
		}

		if($options['bindParams'] && !empty($options['bindParams'])){
			$options = array_merge($options, $options['bindParams']);
			unset($options['bindParams']);
		}

		$query = "SELECT * FROM campaigns c $where $orderBy {$qOpts->limitstr}";
//echo $query;
		$stmt = $this->prepare($query);

		self::callPreparedQuery($stmt, $options);

		return $stmt->fetchAll(PDO::FETCH_CLASS, 'WebsiteModel');
	}

	public function getFormsByRoleAccess(array $args){
		$columns = "*";
		$countProspectsColumn = "(SELECT COUNT(ID) FROM prospects WHERE prospects.form_ID = lead_forms.ID)";
		$countRespondersColumn = "(SELECT  COUNT(ID) FROM auto_responders WHERE auto_responders.table_name = 'lead_forms' AND auto_responders.table_id = lead_forms.id)";

		$q = "SELECT $columns, $countProspectsColumn, $countRespondersColumn FROM lead_forms " .
			"LEFT JOIN " .
				"lead_forms_roles ON lead_forms.id = lead_forms_roles.lead_form_id " .
			"LEFT JOIN " .
				"6q_roles ON 6q_roles.ID = lead_forms_roles.role_id " .
			"LEFT JOIN " .
				"6q_userroles ON 6q_roles.ID = 6q_userroles.role_ID " .
			"WHERE " .
				"((lead_forms.user_id = :user_id AND lead_forms.cid = :cid) " .
				"OR (lead_forms.multi_user = 1 " .
					"AND (6q_userroles.user_ID = :user_id OR lead_forms.user_id = :user_id))) " .
				"AND lead_forms.trashed = 0";
	}
	/**
	 * Receives a [formID] Or [roleID]
	 *
	 * @param array $args
	 * @return array|bool
	 * The data returned depends of which parameters receives.<br/><br/>
	 * <p>[formID] Returns all roles specifying for each role if it is assigned to the form with the [formID].</p><br/>
	 * <p>Without [formID] will return all leadforms that the action user can access.</p><br/>
	 * <p>[Optional params] [cid] Campaign ID.</p><br/>
	 */
	public function getFormsWithRoles(array $args){
		$orderCol = 'created';
		$orderDir = 'ASC';
		$qOpts = $this->getQueryOptions($args);
		$actionUser = $this->getActionUser();
		$args['userID'] = $actionUser->getID();

		if(isset($args['formID'])){
			$cols = '6q_roles.*, lead_forms_roles.*, IF(lead_forms_roles.lead_form_id IS NULL, 0, 1) assigned';
			$q = "6q_roles LEFT JOIN lead_forms_roles ON 6q_roles.ID = lead_forms_roles.role_id LEFT JOIN lead_forms ON lead_forms.id = lead_forms_roles.lead_form_id";
			$where = array('(6q_roles.reseller_user = :userID)',
	  						'(lead_forms.id = :formID AND lead_forms.multi_user = 1 OR lead_forms_roles.lead_form_id IS NULL)');
		}else{
			$cols = 'DISTINCT lead_forms.*';
			$q = 'lead_forms LEFT JOIN lead_forms_roles ON lead_forms.id = lead_forms_roles.lead_form_id LEFT JOIN 6q_roles ON 6q_roles.id = lead_forms_roles.role_id';
			$where = array('(lead_forms.user_id = :userID OR (lead_forms_roles.role_id = (SELECT 6q_userroles.role_ID FROM 6q_userroles WHERE 6q_userroles.user_ID = :userID) AND lead_forms.multi_user = 1))', 'lead_forms.trashed = 0');
		}

		if(!$qOpts['isCount'] && $args['joins']){
			if(in_array('prospects', $args['joins'])){
				$q .= ' left join (select form_ID, count(*) as count1 from prospects
				WHERE user_id = :userID AND hub_ID != 0) cp ON (cp.form_ID = lead_forms.ID)';

				$cols .= ", coalesce(cp.count1, 0) as prospects_count";
			}

			if(in_array('autoresponders', $args['joins'])){
				$q .= '  left join (select table_id, count(*) as count1 FROM auto_responders
				WHERE table_name = "lead_forms" AND user_id = :userID AND trashed = "0000-00-00") cr ON (cr.table_id = lead_forms.ID)';

				$cols .= ', coalesce(cr.count1, 0) as responders_count';
			}

			unset($args['joins']);
		}

		if($qOpts['cols']){
			$cols = $qOpts['cols'];
		}

		if(isset($args['cid'])){
			$where[] = '(lead_forms.cid = :cid OR lead_forms.multi_user = 1)';
		}

		if(!$qOpts['isCount'] && (isset($args['sSearch']) || isset($qOpts['sSearch']))){
			$args['search'] = isset($args['sSearch']) ? $args['sSearch'] : $qOpts['sSearch'];
			$where[] = '(lead_forms.name LIKE :search)';
			$args['search'] = "%{$args['search']}%";
		}

		if(!$qOpts['isCount'] && $qOpts['orderDir']){
			$orderDir = $qOpts['orderDir'];
		}

		if(!$qOpts['isCount'] && $qOpts['orderCol']){
			$orderCol = $qOpts['orderCol'];
		}

		$where = join(' AND ', $where);
		$q = "SELECT {$qOpts['CALC_FOUND_ROWS']} $cols FROM $q WHERE $where ORDER BY $orderCol $orderDir {$qOpts['limitstr']}";
		$stmt = $this->prepare($q);
		return self::HandleQueryResults($stmt, $args, $qOpts, 'LeadFormModel');
	}


	function assignRoleToForm($formID, $roleID, $value){
		$user = $this->getActionUser();
		$args = array('formID' => $formID, 'roleID' => $roleID);

		if(!$user->isAdmin())
			return false;
		$securityWhere = array('id = :formID', 'multi_user = 1');
		if(!$user->isSystem()){
			$securityWhere[] = 'user_id = :userID';
			$args['userID'] = $user->getID();
		}

		$where = join(' AND ', $securityWhere);

		if($value)
			$stmt = $this->prepare("INSERT INTO lead_forms_roles SELECT :formID, :roleID from dual WHERE EXISTS(SELECT 1 FROM lead_forms WHERE $where)");
		else
			$stmt = $this->prepare("DELETE FROM lead_forms_roles WHERE lead_form_id = :formID AND role_id = :roleID AND EXISTS(SELECT 1 FROM lead_forms WHERE $where)");

		return $stmt->execute($args);
	}

	function SaveWebsitesCollabNotifications($collab_id, array $websites){
		$websites = array_filter($websites);
		//Formatting websites array (id, siteid1), (id, siteid2),... (id, siteidN)
		$websitesValues = implode(
			'), (',
			array_map(
				function($row) use ($collab_id){ return intval($collab_id) . ', ' . intval($row);},
				$websites
			)
		);
		$stmt = $this->prepare('DELETE FROM campaign_collab_siteref WHERE collab_id = :collab_id');
		$stmt->execute(array('collab_id' => $collab_id));
		if(!empty($websites)){
			$q = "INSERT INTO campaign_collab_siteref(collab_id, site_id) VALUES ($websitesValues)";
			$stmt = $this->query($q);
			return $stmt->rowCount();
		}

		return true;
	}

	/**
	 * @param array $args
	 * @return \CampaignCollab[]|\CampaignCollab|\QubeLogPDOStatement
	 */
	function getCollabs(array $args){
		$options = $this->getQueryOptions($args);

		$fields = $options['cols'] ? $options['cols'] : '*';

		$orderBy = $options['orderCol'] ? "ORDER BY {$options['orderCol']} {$options['orderDir']}" : '';

		$where = array();
		$where[] = 'campaign_id = :campaign_id';
		$where[] = 'trashed = 0';
		$where[] = $args['collab_id'] ? 'id = :collab_id' : '';
		$where[] = $args['sSearch'] ? '(name LIKE :sSearch OR email LIKE :sSearch)' : '';
		$where[] = $options['where'];
		$where = array_filter($where);
		$where = empty($where) ? '' : "WHERE " . join(' AND ', $where);

		$q = "SELECT {$options['CALC_FOUND_ROWS']} $fields FROM campaign_collab $where $orderBy {$options['limitstr']}";


		$stmt = $this->prepare($q);
		self::callPreparedQuery($stmt, $args);
		if($options['getStatement']){
			return $stmt;
		}

		require_once(QUBEADMIN . '/models/CampaignCollab.php');
		if($options['returnSingleObj']){
			return $stmt->fetchObject('CampaignCollab');
		}
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'CampaignCollab');
	}

	/**
	 * @param array $args
	 * @return \stdClass[]|\QubeLogPDOStatement
	 */
	function getCollabWebsitesReferences(array $args){
		$options = $this->getQueryOptions($args);

		$where = array();
		$where[] = "collab_id = :collab_id";
		$where[] = $args['site_id'] ? "site_id = :site_id" : '';
		$where[] = $options['where'] ? $options['where'] : '';
		$where = array_filter($where);
		$where = !empty($where) ? "WHERE " . join(' AND ', $where) : '';

		$q = "SELECT hub.id hub_ID, campaign_collab_siteref.*, hub.name as touchpoint_name, hub.httphostkey as hostname FROM campaign_collab_siteref LEFT JOIN hub ON campaign_collab_siteref.site_id = hub.id $where {$options['limitstr']}";

		$stmt = $this->prepare($q);
		self::callPreparedQuery($stmt, $args);
		if($options['getStatement']){
			return $stmt;
		}

		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}
}
