<?php

/*
 * To change this license header, choose License Headers in Project Properties.hub_page2
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qube\Hybrid\DataAccess;

/**
 * Description of AccountDataAccess
 *
 * @author amado
 */
class AccountDataAccess extends \QubeDataAccess {
	
	protected function getParentID()
	{
		return $this->getActionUser()->getParentID();
	}
	
        function getTotalCreatedUsers()
        {
            $parent_id  =   $this->getParentID();
            
            $q  =   'SELECT count(*) FROM users WHERE parent_id = ' . $parent_id . ' or id = ' . $parent_id;
            return $this->query($q)->fetchColumn();
        }
	
	function getTotalCreatedTP($tp_type)
	{
		$parent_id	=	$this->getActionUser()->getParentID();
		$q	=	'SELECT count(*) FROM hub WHERE touchpoint_type = :tp_type and (user_id = :user_id or user_parent_id = :user_id) AND trashed = 0';	// count only non-trashed
		
		$stmt	=	$this->prepare($q);
		
		$stmt->execute(array('tp_type' => $tp_type, 'user_id' => $parent_id));
		
		return $stmt->fetchColumn();
	}

    function getTotalCreatedBlogs(){

        $parent_id = $this->getActionUser()->getParentID();
        $q = 'SELECT COUNT(*) FROM blogs WHERE (user_id = :user_id or user_parent_id = :user_id) AND trashed = 0';

        $stmt = $this->prepare($q);

        $stmt->execute(array('user_id' => $parent_id));

        return $stmt->fetchColumn();
    }

    function getTotalCreatedKeywords(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM ranking_keywords " .
                "LEFT JOIN ranking_sites ON ranking_keywords.site_id = ranking_sites.ID " .
                "LEFT JOIN users ON ranking_sites.user_id = users.id " .
                "WHERE users.parent_id = :parent_id OR users.id = :parent_id";

        $stmt = $this->prepare($q);
        $stmt->execute(array('parent_id' => $parent_ID));
        return $stmt->fetchColumn();
    }


    function getTotalCreatedCampaigns(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM campaigns LEFT JOIN users ON campaigns.user_id = users.id WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedBlogposts(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM blog_post WHERE blog_post.user_parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedNavs(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM hub_page2 WHERE user_parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedBlogfeeds(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM blog_feedsmu LEFT JOIN users ON blog_feedsmu.user_ID = users.id WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedEmailers(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM Emailers";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedNetworkListings(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM 6q_listings LEFT JOIN campaigns ON 6q_listings.cid = campaigns.id LEFT JOIN users ON users.id = campaigns.user_id WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedLeadforms(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM lead_forms WHERE lead_forms.user_parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedResellers(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM Resellers";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedSlides(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM slides LEFT JOIN users ON users.id = slides.user_id WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedRoles(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM 6q_roles WHERE 6q_roles.reseller_user = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedKeywordCampaigns(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM ranking_sites LEFT JOIN users ON users.id = ranking_sites.user_id WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedReports(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM campaign_reports LEFT JOIN users ON users.id = campaign_reports.user_ID WHERE users.parent_id = :parent_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

    function getTotalCreatedPages(){
        $parent_ID = $this->getActionUser()->getParentID();
        $q = "SELECT COUNT(*) FROM hub_page2 WHERE hub_page2.user_parent_id = :user_id";
        $stmnt = $this->prepare($q);
        $stmnt->execute(array('parent_id' => $parent_ID));
        return $stmnt->fetchColumn();
    }

}
