<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProposalsDataAccess
 *
 * @author Jon Aguilar
 */
class ProposalsDataAccess extends QubeDataAccess {

	function getTotalProposal($account_id){
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT COUNT(*) FROM proposals WHERE account_id = :account_id AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('account_id' => $account_id));
        return $stmnt->fetchColumn();

    }

	
	function getProposalsValue($account_id){
		
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT id FROM proposals WHERE account_id = :account_id AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('account_id' => $account_id));
		
		$proposals = $stmnt->fetchAll();
		
		
			
			foreach($proposals as $prop){
				
		
				
				$pMod = ProposalModel::Fetch('id = %d', $prop['id']);
				$pa = new ProposalDataAccess();
				$plists = $pa->getProductsbyProposal($pMod, array('proid' => $prop['id']));
				
				if($plists){
					
				
				$priceonetime = array();
				$pricemonthly = array();
				$pricebiannual = array();
				$priceannual = array();
				foreach($plists as $plist){
				if($plist->type == 'One-Time') $priceonetime[] = $plist->price;
				if($plist->type == 'Monthly') $pricemonthly[] = $plist->price;
				if($plist->type == 'Bi-Annual') $pricebiannual[] = $plist->price;
				if($plist->type == 'Annual') $priceannual[] = $plist->price;
				}
				$ponetime += array_sum($priceonetime);
				$pmonthly += 12*array_sum($pricemonthly);
				$pricebiannual = 2*array_sum($pricebiannual);
				$pannual += array_sum($priceannual);
				
					
				}

			
			} return $totaldollar = number_format($ponetime+$pmonthly+$pricebiannual+$pannual,2);


    }
	
}
