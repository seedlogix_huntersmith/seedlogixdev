<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 *
 * Description of SalesDataAccess
 *
 * @author corfro
 */
class SalesDataAccess extends QubeDataAccess
{
    //current User ID
    protected $currentUserID = '';
    protected $salesTotalCommission = NULL;
    protected $isManager = NULL;
    protected $managerTotalSales = 0;

    //Ignited Agent class IDs
    protected $professionalID = "1217";
    protected $professionalPlusID = "1218";
    protected $managerClassID = "1219";
    protected $ignitedAgents = "1217,1218,1219";

    //Ignited Local class IDs
    protected $jumpTrack = "1167";
    protected $fastTrack = "1168";
    protected $AdvTrack = "1169";
    protected $proTrack = "1170";
    protected $proTrackPlus = "1171";
    protected $ignitedLocalIDs = "1167,1168,1169,1170,1171,983";

    protected $salesManagers = array();
    protected $salesProfessionals = array();
    protected $salesClients = array();

	/**
     * @param $id
     *
     * @return mixed
     *
     * @description Set the current Sales' User ID
     */
    function setSalesUserID($id)
    {
        try{
            return $this->currentUserID = $id;
        }
        catch(Exception $e) {
            return false;
        }
    }

    /**
     * @return string
     *
     * @description RETURN the current Sales' User ID
     */
    function getSalesUserID()
    {
        return $this->currentUserID;
    }

    function getIndirectSalesIDs($salesIDs)
    {
        return true;
    }

    function isAgent($userID)
    {
        if(!$userID)
            return false;
        return $this->checkClass($userID, 'agent');
    }

    # @description Check if userID is a Sales Manager
    function isManager($userID)
    {
        if(!$userID)
            return false;

        if($this->isManager)
        {
            return $this->isManager;
        }
        else
        {
            $this->isManager = $this->checkClass($userID, 'manager');
        }
        return $this->isManager;
    }

    # @description Check if userID is a Sales Professional/Professional+
    function isSalesProfessional($userID)
    {
        if(!$userID)
            return false;
        return $this->checkClass($userID, 'salesProfessional');
    }

	/**
     * @param        $array
     * @param string $sep
     *
     * @return string
     *
     * @description Convert Multidimensional array to CSV string
     */
    function array2str($array, $sep = ', ')
    {
        $str = '';
        if(is_array($array)) {
            if(count($array)) {
                foreach($array as $v) {
                    $str .= $v[0].$sep;
                }
                $str = substr($str, 0, -strlen($sep));
            }
        } else {
            $str .= $array;
        }

        return $str;
    }

	/**
     * @param        $userID
     * @param string $type
     *
     * @return bool|string
     *
     * @description Check if the user_ID provided matches a user_class
     */
    function checkClass($userID, $type = 'client')
    {
        if(!$userID)
            return false;

        switch($type)
        {
            case 'agent':
                $hayStack = $this->ignitedAgents;
                break;
            case 'manager':
                $hayStack = $this->managerClassID;
                break;
            case 'salesProfessional':
                $hayStack = $this->professionalID;
                break;
            case 'salesProfessionalPlus':
                $hayStack = $this->professionalPlusID;
                break;
            case 'products':
            default:
                $hayStack = $this->ignitedLocalIDs;
                break;
        }

        $q = "SELECT class FROM users WHERE id = $userID";
        $q = $this->array2str($this->query($q)->fetchColumn(), ',');
        return (strpos($hayStack, $q) !== false);
    }

    function getSalesPersonsName($userID)
    {
        if(!$userID)
            return false;

        $q = "SELECT CONCAT(firstname, ' ', lastname) AS fullname FROM user_info WHERE user_id = $userID";
        return addslashes ( $this->query($q)->fetchColumn() );
    }

	/**
     * @param $userID
     * @return array|string
     * @description GET user_IDs based on referral_ID
     */
    function getSalesClients($userID, $returnType = 'string')
    {
        if(!$userID)
            return false;

        if(is_array($userID)){
            $userID = $userID['id'];
        }

        $q1 = "SELECT user_id FROM user_info where referral_id = $userID";
        $results = $this->query($q1)->fetchAll();

        if($returnType == 'array'){
            return  $results;
        }
        $this->salesClients = $this->array2str($results, ',');
        return $this->salesClients;
    }

    function getSalesAgents($userID)
    {
        if(!$userID)
            return false;

        if(is_array($userID)){
            $userID = $userID['id'];
        }

        $q1 = "SELECT ui.user_id, u.class
                FROM user_info ui
                JOIN users u ON ui.user_id = u.id
                WHERE ui.referral_id = $userID
                AND u.class IN (".$this->ignitedAgents.")";

        if(!$q1)
            return false;
        return $this->query($q1)->fetchAll();
    }

    function getSalesTotal($userID, $range = NULL, $commission = 0.35)
    {
        if(!$userID)
            return false;

        if( $this->managerTotalSales )
            return $this->managerTotalSales;

        $multiCheck = "= $userID";

        $q1 = 'SELECT
                sum(rs.amount) as total
            FROM resellers_billing rs
            LEFT JOIN user_info ui ON rs.user_id = ui.user_id
            LEFT JOIN user_class uc ON rs.product_id = uc.id
            WHERE rs.user_id IN (SELECT user_id FROM user_info WHERE referral_id '.$multiCheck.')
			AND rs.success = 1';

        $q2 = '';
        $q = "{$q1}{$q2}";
        $this->salesTotalCommission = $this->query($q)->fetchColumn();
        $this->salesTotalCommission = floatval($this->salesTotalCommission) * floatval($commission);
        return $this->salesTotalCommission;
    }


    function getSales($userID, $range = NULL, $tier = NULL)
	{
        if(!$userID)
            return false;

        //If a Manager
        if( $tier != NULL )
        {
            switch($tier)
            {
                case '10%':
                    $commission = 10;
                    break;
                case '5%';
                    $commission = 5;
                    break;
                default:
                    $commission = intval($tier);
            }
        }
        //IF a Sales Professional
        else {
            $commission = 35;
        }
        $salesProfName = $this->getSalesPersonsName($userID);

        if(!$userID)
            return false;
        $q1 = "SELECT
                rs.product_id, rs.user_id, rs.transaction_type, rs.amount, rs.timestamp,
                uc.name,
                ui.company,
                CONCAT(ui.firstname, ' ', ui.lastname) AS company_fallback,
                '$salesProfName' AS 'sales_professional',
                '$commission' AS commission
            FROM resellers_billing rs
            LEFT JOIN user_info ui ON rs.user_id = ui.user_id
            LEFT JOIN user_class uc ON rs.product_id = uc.id
            WHERE rs.user_id IN (SELECT user_id FROM user_info WHERE referral_id = $userID)";

        $q2 = '';
        if( ($range) && (($range!='0') || ($range!='ALL')) )
        {
            // @todo change range to get from cookie
            list($interval, $period) = preg_split('/-/', $range);
            
            switch($period)
            {
                case 'YEAR':
                case 'MONTH':
                case 'DAY':
                    break;
                case 'ALL':
                case 'ALLTIME':
                    break;
                default:
                    $interval = 7;
                    $period = 'DAY';
                    break;
            }
            
            $q2 .= " AND resellers_billing.sched_date > SUBDATE(curdate(), INTERVAL $interval $period)";
        }
        $q2 .= " AND rs.transaction_id != 0";

        $q = "{$q1}{$q2}";
        return $this->query($q)->fetchAll();
	}

    function getManagerSales($userID)
    {
        if(!$userID)
            return false;

        $firstTierSales = $this->getSales($userID);
        $salesArray = $firstTierSales;
        $ssalesTotal = $this->getSalesTotal($userID, NULL, 0.35);

        //get second tier sales
        $secondTierSales = $this->getSalesClients($userID, 'array');

        foreach ($secondTierSales as $value)
        {
            $results = array_filter( (array)$this->getSales( $value['user_id'], NULL, '10%' ) );
            if( $results ) {
                $salesArray = array_merge($salesArray, $results);
                $ssalesTotal = $ssalesTotal + ($this->getSalesTotal($value['user_id'], NULL, 0.10));
            }

            $thirdTeirSales = $this->getSalesClients($value['user_id'], 'array');
            foreach ($thirdTeirSales as $values)
            {
                $childResults = array_filter( (array)$this->getSales( $values['user_id'], NULL, '5%' ) );
                if( $childResults ) {
                    $salesArray = array_merge($salesArray, $childResults);
                    $ssalesTotal = $ssalesTotal + ($this->getSalesTotal($values['user_id'], NULL, 0.05));
                }
            }
        }

        $this->managerTotalSales = $ssalesTotal;

        //return all results
        return $salesArray;
    }

	/**
     * @param $userID
     *
     * @return array
     */
    function getClientSold($userID)
    {
        if(!$userID)
            return false;

        if( is_array($userID) ){
            $ids = $this->getSalesClients($userID[0]);
        }
        else{
            $ids = $this->getSalesClients($userID);
        }

        if(!$ids)
            return false;

        $q2 = "SELECT
                ui.user_id,
                ui.company,
                uc.name,
                u.created,
                (SELECT CONCAT(firstname, ' ', lastname) FROM user_info as ui2 WHERE ui2.user_id = ui.referral_id) AS sales_professional
            FROM user_info ui
            JOIN users u ON u.id = ui.user_id
            JOIN user_class uc ON u.class = uc.id
            WHERE u.id IN ($ids)
            AND uc.id IN (".$this->ignitedLocalIDs.")
            AND u.access = 1";

        return $this->query($q2)->fetchAll();
    }

    function getClientSoldManager($userID)
    {
        if(!$userID)
            return false;

        //FIRST RUN
        $salesAgents = $this->getClientSold($userID);
        $clientsSoldArray = $salesAgents;

        //get second tier sales
        $secondTierAgents = $this->getSalesAgents($userID);

        foreach ($secondTierAgents as $value)
        {
            $results = array_filter( (array)$this->getClientSold( $value['user_id']) );
            if( $results ) {
                $clientsSoldArray = array_merge($clientsSoldArray, $results);
            }

            //if manager get their client sales
            if($value['class'] == $this->managerClassID)
            {
                $thirdTierAgents = $this->getSalesAgents($value['user_id']);
                foreach ($thirdTierAgents as $values)
                {
                    $childResults = array_filter( (array)$this->getClientSold( $values['user_id']) );
                    if( $childResults ) {
                        $clientsSoldArray = array_merge($clientsSoldArray, $childResults);
                    }
                }
            }

        }

        //return all results
        return $clientsSoldArray;

    }

	/**
     * @param $userID
     *
     * @return array
     */
    function getSalesTeam($userID)
    {
        if(!$userID)
            return false;

        $q1 = "SELECT user_id FROM user_info where referral_id = $userID";
        $q1 = $this->query($q1)->fetchAll();
        $ids = $this->array2str($q1, ',');
        if(!ids || $ids === '')
            return false;

        $q2 = "SELECT
                ui.user_id,
                ui.firstname,
                ui.lastname,
                uc.name,
                ui.phone,
                u.created,
                u.username
            FROM user_info ui
            JOIN users u ON u.id = ui.user_id
            JOIN user_class uc ON u.class = uc.id
            WHERE u.id IN ($ids)
            AND uc.id IN (".$this->ignitedAgents.")
            AND u.access = 1";

        return $this->query($q2)->fetchAll();
    }
}