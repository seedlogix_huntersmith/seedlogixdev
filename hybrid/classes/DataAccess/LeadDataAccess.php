<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of LeadDataAccess
 *
 * @author amado
 */
class LeadDataAccess extends QubeDataAccess
{
	protected $stmtreplace=	array();
	
	static function removeHostnameStorage($oldhostname)
	{
		$symlink	=	self::getHostnameDataPath($oldhostname, false);
		if(is_link($symlink))
			return unlink ($symlink);
		
		return false;
	}
	
	static function setUpHostnameStorage($hostname, $hub_ID, $reseller_ID)
	{
//		$domains_storage_path	=	""
		$relative		=	self::getHubStorage($reseller_ID, $hub_ID, true);
		$hub_storage	=	'../../../hubs-domains/' . $relative;	//self::getHubStorage($reseller_ID, $hub_ID, false);	// call to get relative path
		$symlink		=	self::getHostnameDataPath($hostname, true);
		// @todo remove, temporary, legacy compatibility code
		$subrelative_id	=	FileUtil::getIDPath('h', $hub_ID);
		@symlink($subrelative_id, QUBEROOT . 'hubs/domains/' . $reseller_ID . '/' . $hub_ID);
		// end todo
		return FileUtil::CreateSymlink($hub_storage, $symlink);
	}
	
	/**
	 * creates the {reseller}/h{xxx}/{xxx} storage directory for a hub.
	 * 
	 * @param type $reseller_ID
	 * @param type $hub_ID
	 * @return type
	 */
	static function getHubStorage($reseller_ID, $hub_ID, $createDir	=	false)
	{
		// reseller_ID is not processed into sub-dirs. only hub-ID is processed .
		
		$domaindir			= $reseller_ID . '/' . FileUtil::getIDPath('h', $hub_ID);
		
		// crete the hubs/domains/{reseller_ID}/hX/XXXX directory for the domain
		if($createDir){
			$domain_dataroot	=	QUBEROOT . 'hubs/domains';
			$fullpath	= realpath($domain_dataroot) . '/' . $domaindir;
			FileUtil::CreatePath($fullpath);
//			return $fullpath;
		}
		return $domaindir;
	}

	/**
	 * Creates the path (directories) do/ma/ for do/ma/domain.com (symlink)
	 * returns the relative symlink name (do/ma/domain.com)
	 * 
	 * @param type $httphostname
	 * @param type $createPath
	 * @return string
	 */
	static function getHostnameDataPath($httphostname, $createPath	=	false)
	{
		$numparts  =   preg_match('/^(..)(..).*$/', $httphostname, $matches);
		$prevdir    =   '';
		$curdir     =   '';
		for($i  =   0;  $i < 2; $i++)
		{
			$prevdir    .=  $matches[$i+1];
			$curdir     =   QUBEROOT . 'sitedata/' . $prevdir;
			if($createPath == true && !is_dir($curdir))
			{
				mkdir($curdir);
			}

			$prevdir .= '/';
		}

		return $curdir . '/' . $httphostname;
	}
	
	/**
	 * 
	 * @param type $key
	 * @param type $pk
	 * @param type $type valint | valstr
	 * @return type
	 */
	function getConfig($tag, $pk = 0, $type = 'valint')
	{
		return $this->getQube()
							->query("SELECT $type FROM 6q_meta WHERE PK = $pk AND `table` = 'system' AND col='$tag'")
							->fetchColumn();
	}
	
	/**
	 * Get an Associative array of values.
	 * 
	 * @param type $W specify col, and pk conditions
	 * @param type $pk
	 * @param type $type valint | valstr
	 * @return array
	 */
	function getConfigs(DBWhereExpression $W, $pk = 0, $type = 'valint')
	{
		return $this->getQube()
							->query("SELECT col, $type FROM 6q_meta WHERE " . $W->Where("`table` = 'system'"))
							->fetchAll(PDO::FETCH_KEY_PAIR);
	}
	
	/**
	 * 
	 * @param type $col
	 * @param type $value
	 * @param type $PK
	 * @param type $type
	 * @return type
	 */
	function saveConfig($col, $value, $PK = 0, $type = 'valint')
	{
		if(!$this->stmtreplace[$type]){
			$this->stmtreplace[$type]	=	$this->getQube()->GetPDO()
            ->prepare("REPLACE INTO 6q_meta SET $type = ?, PK = ?, `table` = 'system', col=?");
		}
		
		return $this->stmtreplace[$type]->execute(array($value, $PK, $col));
	}
	
	function doOptOut($id, $email, $type = NULL)
	{
		switch($type)
		{
			case 'lead':
				$query = 'update leads SET optout = 1 WHERE id = :id and lead_email = :email';
				break;
			case 'contact_form':
				$query = 'update contact_form SET optout = 1 WHERE id = :id and email = :email';
				break;
		}
		
		if(NULL === $type){
			return $this->doOptOut($id, $email, 'lead') + 
				$this->doOptOut($id, $email, 'contact_form');
		}else{
			$stmt	=	$this->prepare($query);
			$stmt->execute(array('id' => $id, 'email' => $email));
			
			return $stmt->rowCount();
		}
	}

	function getTrialSubscriptionID()//UserDataAccess $uda)
	{
		$trial_subscription = (int)$this->getConfig('TRIAL_SUBID');
		
//		if(!$uda->subscriptionExists($trial_subscription))
//		{
//			throw new QubeException('The trial subscription does not exist!');
//		}
		
		return $trial_subscription;
	
		$SubscriptionModel	=	SubscriptionModel::Fetch(array('ID' => $trial_subscription));
		return $SubscriptionModel;
	}
	
	function createSystemUser()
	{
		$dum	=	new UserWithRolesModel();
		$ps	=	new PermissionsSet($dum);
		$ps->setPermission('is_system');
		$dum->setPermissionsSet($ps);
		
		$dum->renderPermissions();
		return $dum;
	}
}
