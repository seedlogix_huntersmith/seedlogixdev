<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountsDataAccess
 *
 * @author Jon Aguilar
 */
require HYBRID_PATH . 'library/PHPMailer/src/Exception.php';
require HYBRID_PATH . 'library/PHPMailer/src/PHPMailer.php';
require HYBRID_PATH . 'library/PHPMailer/src/SMTP.php';

class EmailsDataAccess extends QubeDataAccess {
	
	function sendEmailer($sent, $subject, $body, $from, $from_name, $cc, $bcc){
			
		$mail = new PHPMailer(true); 
			 
			 try {
				//Server settings
				//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = 'express-relay.jangosmtp.net;express-relay.jangosmtp.net';  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = '6qube';                 // SMTP username
				$mail->Password = '927522WK';                           // SMTP password
				$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                    // TCP port to connect to

				//Recipients
				$mail->setFrom($from, $from_name);
				$mail->addAddress($sent);     // Add a recipient
				//$mail->addAddress('ellen@example.com');               // Name is optional
				$mail->addReplyTo($from, $from_name);
				if($cc) $mail->addCC($cc);
				if($bcc) $mail->addBCC($bcc);
				//$mail->addBCC($data['bcc_sent']);

				//Attachments
				//$mail->addAttachment(HYBRID_PATH . '/api/proposals/proposal-'.$cname.'.pdf');         // Add attachments
				//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $subject;
				$mail->Body    = $body;
				//$mail->AltBody = 'You have a new proposal!';

				$mail->send();
				 
				//return $error = false;
		
				 
			} catch (Exception $e) {
				 
				 return $error = true;
			}
		
	}
	
	function sendContactEmail($data){
		
		$eset = EmailSettingsModel::Fetch('cid = %d', $data['cid']);
		$key = 's$TS$EGU$NGdg,cg cxcxXMMMM ;;;{} (.)(.) ( Y )';
		$pss = base64_decode($eset->password);
		$iv = substr($pss, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

		$password = rtrim(
			mcrypt_decrypt(
				MCRYPT_RIJNDAEL_128,
				hash('sha256', $key, true),
				substr($pss, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
				MCRYPT_MODE_CBC,
				$iv
			),
			"\0"
		);
		
			
		$mail = new PHPMailer(true); 
		
			 
			 try {
				//Server settings
				//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = ''.$eset->host.';'.$eset->host.'';  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = $eset->username;                 // SMTP username
				$mail->Password = $password;                           // SMTP password
				$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                    // TCP port to connect to

				//Recipients
				$mail->setFrom($eset->username, $eset->name);
				$mail->addAddress($data['to_sent']);     // Add a recipient
				//$mail->addAddress('ellen@example.com');               // Name is optional
				$mail->addReplyTo($eset->username, $eset->name);
				if($data['cc_sent']) $mail->addCC($data['cc_sent']);
				if($data['bcc_sent']) $mail->addBCC($data['bcc_sent']);

				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $data['subject'];
				$mail->Body    = $data['body'];
				//$mail->AltBody = 'You have a new proposal!';

				$mail->send();
				//var_dump($mail);
				//return $error = false;
		
				 
			} catch (Exception $e) {
				 
				 //var_dump($e);
				 return $error = true;
			}
		
	}
		
	function sendProposalEmail($proposal, $Account, $Contact){
		
		$eset = EmailSettingsModel::Fetch('cid = %d', $Contact->cid);
		$boBrand = ResellerModel::Fetch('admin_user = %d', $Account->user_parent_id);
		$key = 's$TS$EGU$NGdg,cg cxcxXMMMM ;;;{} (.)(.) ( Y )';
		$pss = base64_decode($eset->password);
		$iv = substr($pss, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

		$password = rtrim(
			mcrypt_decrypt(
				MCRYPT_RIJNDAEL_128,
				hash('sha256', $key, true),
				substr($pss, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
				MCRYPT_MODE_CBC,
				$iv
			),
			"\0"
		);
		
		
		$mail = new PHPMailer(true); 
			 
			 try {
				//Server settings
				//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = ''.$eset->host.';'.$eset->host.'';  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = $eset->username;                 // SMTP username
				$mail->Password = $password;                           // SMTP password
				$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                   // TCP port to connect to

				//Recipients
				$mail->setFrom($eset->username, $eset->name);
				$mail->addAddress(''.$Contact->email.'', ''.$Contact->first_name.' '.$Contact->last_name.'');     // Add a recipient
				//$mail->addAddress('ellen@example.com');               // Name is optional
				$mail->addReplyTo($eset->username, $eset->name);
				//$mail->addCC('cc@example.com');
				//$mail->addBCC('bcc@example.com');
				 
				 $cname = preg_replace('/[^\w]/', '', $Account->name);
				 $cname = strtolower($cname);

				//Attachments
				$mail->addAttachment(HYBRID_PATH . '/api/proposals/proposal-'.$cname.'.pdf');         // Add attachments
				//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = 'New Proposal: '.$Account->name.' ';
				$mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>New Proposal Request</title>
<style type="text/css">
@media only screen and (max-width: 600px) {
*[class=mobiRmPadR] {
	padding-right: 0!important;
}
*[class=mobiStretch] {
	width: 100%!important;
}
*[class=banner300] {
	width: 280px!Important;
}
*[class=mobiReduceTopPad] {
	padding-top: 15px!important;
}
}
.clearBlueLinks a {
	color: #333333!important;
	text-decoration: none!important;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#eeeeee" style="background:#eeeeee;" xmlns="">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" align="center"><table class="mobiStretch" align="center" cellpadding="0" cellspacing="0" border="0" style="max-width:600px!important;">
        <tr>
          <td class="mobiStretch" valign="top" width="600" style="max-width:600px!important;"><table align="left" width="300" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td style="color:#ffffff;padding:14px 0 14px 10px;text-align:left;line-height:44px;vertical-align:middle;" valign="middle"><img src="https://'.$boBrand->proposal_url.'/users'.$boBrand->prop_logo.'" border="0" style="vertical-align: middle;" height="38"></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<table bgcolor="#eeeeee" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-spacing: 0;">
  <tr>
    <td valign="top" height="20">
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeee">
  <tr>
    <td bgcolor="#eeeeee"><table class="mobiStretch" bgcolor="#eeeeee" style="max-width:600px!important;" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td class="mobiStretch" valign="top" width="600" align="center" style="max-width:600px!important;"><table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeee">
              <tr>
                <td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeee">
                    <tr>
                      <td width="10">
                      <td style="max-width:580px; display:block;" bgcolor="#eeeeee" align="center"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                          <tr>
                            <td bgcolor="'.$boBrand->bg_clr.'" class="clearBlueLinks" style="border-top:1px solid #cccccc;font-size:18px!important;font-weight:bold;color:#FFFFFF;font-family:Arial, Helvetica, sans-serif;line-height:20px;padding:15px 20px;text-align:left;" valign="top">Hey '.$Contact->first_name.'! Your Proposal Is Attached...</td>
                          </tr>
                          <tr>
                            <td style="padding:5px 20px 0;" align="left"><font style="font-family:Arial,Helvetica,sans-serif; font-size:13px; color:#333333">Your proposal is ready and I have included it in this email.  If you have any questions about our proposal, please reach out directly to me.  If all looks good you can click on Accept Proposal on this email or inside the proposal. 
							<br><br>
							This proposal will expire on '.$proposal->expire_date.', so if you need more time to move forward, let\'s setup a call to discuss.  
							
								 <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                          <tr>
                            <td style=" padding-bottom: 16px; "><br />
                              <table border="0" cellspacing="0" cellpadding="0" height="48px">
                                <tr>
                                  <td width="88" style="width:88px"></td>
                                  <td style="font-family: Segoe UI Semibold,-apple-system,BlinkMacSystemFont,San Francisco,Roboto,Helvetica Neue,Arial "><div> 
                                      <!--[if mso]>
								<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" style="height:40px;width:170px;v-text-anchor:middle;" arcsize="7%" strokecolor="'.$boBrand->bg_clr.'" fillcolor="'.$boBrand->bg_clr.'">
								<v:shadow on="true" opacity="25%" type="perspective" offset=0pt,1pt/>
							<![endif]--> 
                                      <a href="https://'.$boBrand->proposal_url.'/?aid='.$Account->id.'&conid='.$Contact->id.'&proid='.$proposal->id.'" target="_blank" style=" text-decoration: none; color: #ffffff;">
                                      <table border="0" cellspacing="0" cellpadding="0" height="38px" align="center" valign="middle">
                                        <tr>
                                          <td width="164" bgcolor="'.$boBrand->bg_clr.'" style="width:164px;padding-top:2px;border-radius: 3px;box-shadow: 0 2px 4px -0.8px rgba(0, 0, 0, 0.25);text-decoration:none;font-family: Segoe UI Semibold,-apple-system,BlinkMacSystemFont,San Francisco,Roboto,Helvetica Neue,Arial; font-weight:600; " align="center" valign="middle"><a href="https://'.$boBrand->proposal_url.'/?aid='.$Account->id.'&conid='.$Contact->id.'&proid='.$proposal->id.'" target="_blank" style="font-family: Segoe UI Semibold,-apple-system,BlinkMacSystemFont,San Francisco,Roboto,Helvetica Neue,Arial; font-weight:600; font-size:15px; text-decoration: none; color: #ffffff;">&nbsp;&nbsp;Accept Proposal&nbsp; </a></td>
                                        </tr>
                                      </table>
                                      </a> 
                                      <!--[if mso]>        
			</v:roundrect>
		<![endif]--> 
                                    </div></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>

								
								<br><br>
								'.$eset->signa.'
<br />
<br />
<br />
                            </font></td>
                          </tr>
                        </table>
                       
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeee">
                          <tr>
                            <td style="max-width:580px;" width="580" height="15">
                          </tr>
                          <tr>
                            <td style="padding:10px 20px 50px;font-family:Arial,Helvetica,sans-serif; font-size:11px; color:#333333;text-align:left;" align="left">Add '.$eset->username.' to your address book to ensure delivery of my emails.</td>
                          </tr>
                          <tr>
                            <td style="max-width:580px;" width="580" height="50">
                          </tr>
                        </table></td>
                      <td width="10">
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
';
				//$mail->AltBody = 'You have a new proposal!';

				$mail->send();
				 
				 $pdo = $this->getQube()->GetPDO('master');
				 $pdo->query('UPDATE proposals SET sent = "1", last_updated = now() WHERE id = ' . $proposal->id);
				 
				 return $message = 'Proposal Sent to '.$Account->name.'';
		
				 
			} catch (Exception $e) {
				 
				 return $error = true;
			}
		
		
	}
	
}
