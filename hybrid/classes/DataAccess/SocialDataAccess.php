<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SocialDataAccess
 *
 * @author Jon Aguilar
 */
class SocialDataAccess extends QubeDataAccess {
	public function removeSocialSched($social_id, $post_id){
		$statement = $this->prepare('DELETE from social_sched WHERE social_id = :social_id AND post_id = :post_id');
		return $statement->execute(array('social_id' => $social_id, 'post_id' => $post_id));
	}
}
