<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 1/29/2015
 * Time: 2:32 AM
 */

class EmailBroadcastDataAccess extends QubeDataAccess {

    static function getOrderByString($qOptsObj, $defaultCol = 'created')
    {
		$newRow = '';
        $dir = $qOptsObj->orderDir;
        $col = empty($qOptsObj->orderCol) ? $defaultCol : $qOptsObj->orderCol;

        if(!in_array($col, LeadFormEmailerModel::dbColumns())){
            switch($col){
                case 'f.name':
                    break;
                default:
                    throw new QubeException('Unknown Column for LeadFormEmailerModel :' . $col);
                    break;
            }
        }
        else{
            $col = 'e.`' . $col .'`';
        }

		if($qOptsObj->new_rows_ids){
			$newRow = 'e.id = ' . join(' DESC, e.id = ', $qOptsObj->new_rows_ids) . ' DESC, ';
		}
        return " ORDER BY $newRow $col $dir";
    }

    /**
	 *
     * @param $args array <p>An array with the following values:</p>
	 * <ul>
	 * 	<li>campaign_ID</li>
	 * 	<li>sSearch</li>
	 * 	<li>id</li>
	 * 	<li>returnSingleObj</li>
	 * </ul>
     * @return LeadFormEmailerModel[]|LeadFormEmailerModel|PDOStatement
     * @throws QubeException
     */
    function getLeadBroadcasters($args)
    {
        $args['user_ID']    =   $this->getActionUser()->getID();
        $qOpts = (object)self::getQueryOptions($args, array('e.trashed' => '0000-00-00',
                'e.user_id = :user_ID'));
        $where = $qOpts->where;
        $fields = 'e.*, f.name as form_name, f.cid';
        $orderByString = self::getOrderByString($qOpts);
        $joins = 'inner join lead_forms f on (f.id = e.lead_form_id)';
        if($qOpts->cols){
            $fields = $qOpts->cols;
        }
        if(isset($args['campaign_ID'])) {
            $where->Where('e.campaign_id = :campaign_ID');
        }

        if(isset($args['sSearch'])){
            $where->Where('(e.name LIKE :sSearch OR f.name LIKE :sSearch ' . (is_numeric($args['sSearch']) ? "OR e.name = {$args['sSearch']}" : "") . ")");
        }

        $qformat = 'select %s %s from lead_forms_emailers e %s
          where %s
          %s %s';

        $qstr = sprintf(
            $qformat,
            $qOpts->CALC_FOUND_ROWS,
            $fields,
            $joins,
            $where, $orderByString, $qOpts->limitstr);

        $stmt = $this->prepare($qstr);
        self::callPreparedQuery($stmt, $args);
        return self::HandleQueryResults($stmt, $args, $qOpts, 'LeadFormEmailerModel');
    }

	/**
	 * @return LeadFormEmailerModel[]
	 */
	public function getPendingBroadcasts(){
		$q = "SELECT * FROM lead_forms_emailers " .
				"WHERE sched_mode = 'date' " .
				"AND active = 1 " .
				"AND trashed = 0 " .
				"AND str_to_date(concat(sched_date, ' ', sched_hour), '%m/%d/%Y %H') < NOW()";

		$filter = array(
			"from_field != ''",
			"subject != ''",
			"body != ''",
			"sched_date != ''"
		);

		$q .= " AND " . join(' AND ', $filter);

		$statement = $this->query($q);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_CLASS, 'LeadFormEmailerModel');
	}
}
