<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountsDataAccess
 *
 * @author Jon Aguilar
 */
class AccountsDataAccess extends QubeDataAccess {

	function getTotalContacts($account_id){
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT COUNT(*) FROM contacts WHERE account_id = :account_id AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('account_id' => $account_id));
        return $stmnt->fetchColumn();

    }
	
	function getTotalContactsByCID($cid){
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT COUNT(*) FROM contacts WHERE cid = :cid AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('cid' => $cid));
        return $stmnt->fetchColumn();

    }
	
	function getTotalLeadsByCID($cid){
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT COUNT(*) FROM prospects WHERE cid = :cid AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('cid' => $cid));
        return $stmnt->fetchColumn();

    }
	
	function getTotalAccountsByCID($cid){
		$pdo = Qube::Start()->GetDB('master');
        $q = "SELECT COUNT(*) FROM accounts WHERE cid = :cid AND trashed = '0000-00-00'";
        $stmnt = $pdo->prepare($q);
        $stmnt->execute(array('cid' => $cid));
        return $stmnt->fetchColumn();

    }

	
}
