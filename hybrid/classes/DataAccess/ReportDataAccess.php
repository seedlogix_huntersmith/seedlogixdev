<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/1/14
 * Time: 1:00 PM
 */

class ReportDataAccess extends QubeDataAccess{
    const N_ITEMS = 10;
    function getAllStatsDaily(DateTime $start, DateTime $end, $hub_id = null, $touchpoint_type = null){
        $q = "SELECT Day(leads.created) day, count(*) visits FROM sapphire_6qube.leads left join hub
                ON leads.hub_id = hub.id
                where leads.created between :start and :end
                and (:touchpoint_type IS NULL OR hub.touchpoint_type = :touchpoint_type)
                and (:hub_id IS NOT NULL OR leads.hub_ID = :hub_id)
                group by Date(leads.created)
                order by Date(leads.created)";

        $args = array(
            'start' => $start->format(PointsSetBuilder::MYSQLFORMAT),
            'end' => $end->format(PointsSetBuilder::MYSQLFORMAT),
            'hub_id' => $hub_id,
            'touchpoint_type' => $touchpoint_type
        );

        /** @var QubeLogPDOStatement $stmnt */
        $stmnt = $this->getQube()->GetDB()->prepare($q);
        $stmnt->execute($args);
        return $stmnt->fetchAll(PDO::FETCH_OBJ);
    }
	
	
    
//    function getLeadsGraphData($offset)
    function getLeadsGraphData(PointsSetBuilder $b, DBWhereExpression $W)
    {
        /*
        list($t1, $t2)  =   $this->db->query('SELECT @startdate := DATE_SUB(CURDATE(), INTERVAL ' . $interval . ' ' . $unit . '),
                                @end_date := CURDATE()')->fetch(PDO::FETCH_NUM);
        */
#        $t1 =   "-$interval $unit";
//        $t2 =   date('Y-m-d');
        $st =   $this->getLeadsCountStatement($b, $W);//$offset, $t2);
        
        return array('label' => 'Leads', 'data' => $b->createDataSet($st));
//            $this->createSetPoints($st, $offset, $t2, "Leads");
    }
	


    function getLeadsCountStatement(PointsSetBuilder $b, DBWhereExpression $W, $cid = NULL, $userid)    //&$t1, &$t2, $createpoints = false, $valuesonly = false)
    {/*
        $d1     =       date_create($t1);
        $d2     =       date_create($t2);
        $dfdays =       date_diff($d2, $d1)->days;

        if($dfdays > 31){
                        $this->point_increment='+1 month';
                        $d1     =       date('Y-m-01', $d1->getTimestamp());
                        $t1     =       strtotime($d1);//$d1->getTimestamp();
                        // set $t2 = first day of next month.
                        $t2     =   strtotime(date('Y-m-01', strtotime('+1 Month', $d2->getTimestamp())));

#var_dump($t1, $t2);
#                       echo $d1->format('Y-m-d');
#               var_dump($createpoints);
        }
        */
    if($cid)  {    
	$this->query('SET @datediffv	=	DATEDIFF( CURDATE(), "' . $b->start->format('Y-m-d')// self::getTs($t1, 'mysql')
                   . '")');
        $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.created, "%Y-%m-01"), date(t.created))) as date1, count(*) as value
                    FROM active_leads t ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                    'WHERE t.cid != 0 AND t.user_id = "'.$userid.'" AND t.spam = 0 AND t.cid = "'.$cid.'" AND 
                        (t.created ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                            ($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                     ' group by ' . // date(l.created)'
                            'IF( @datediffv  < 32, DATE( t.created ) , DATE_FORMAT( t.created,  "%Y-%m" ) )';
	} else {
		 
		$this->query('SET @datediffv	=	DATEDIFF( CURDATE(), "' . $b->start->format('Y-m-d')// self::getTs($t1, 'mysql')
                   . '")');
        $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.created, "%Y-%m-01"), date(t.created))) as date1, count(*) as value
                    FROM active_leads t ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                    'WHERE t.cid != 0 AND t.spam = 0 AND t.user_id = "'.$userid.'" AND 
                        (t.created ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                            ($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                     ' group by ' . // date(l.created)'
                            'IF( @datediffv  < 32, DATE( t.created ) , DATE_FORMAT( t.created,  "%Y-%m" ) )';
		//original query $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.created, "%Y-%m-01"), date(t.created))) as date1, count(*) as value
                  //  FROM active_leads t ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                  //  'WHERE t.cid != 0 AND t.user_id = dashboard_userid() AND 
                       // (t.created ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                           // ($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                    // ' group by ' . // date(l.created)'
                            //'IF( @datediffv  < 32, DATE( t.created ) , DATE_FORMAT( t.created,  "%Y-%m" ) )';
	 }
                            
#                            echo $q;        
        $st =   $this->query($q);
		
		//var_dump($st);
        
//        if($createpoints) throw new exception('diabled');
//                return $this->getGraphPoints ($st, $t1, $t2, $valuesonly);
        
        return $st;
    }
	
	function getCallsCountStatement(PointsSetBuilder $b, DBWhereExpression $W, $cid = NULL, $userid)    //&$t1, &$t2, $createpoints = false, $valuesonly = false)
    {/*
        $d1     =       date_create($t1);
        $d2     =       date_create($t2);
        $dfdays =       date_diff($d2, $d1)->days;

        if($dfdays > 31){
                        $this->point_increment='+1 month';
                        $d1     =       date('Y-m-01', $d1->getTimestamp());
                        $t1     =       strtotime($d1);//$d1->getTimestamp();
                        // set $t2 = first day of next month.
                        $t2     =   strtotime(date('Y-m-01', strtotime('+1 Month', $d2->getTimestamp())));

#var_dump($t1, $t2);
#                       echo $d1->format('Y-m-d');
#               var_dump($createpoints);
        }
        */
     if($cid)  {
	$this->query('SET @datediffv	=	DATEDIFF( CURDATE(), "' . $b->start->format('Y-m-d')// self::getTs($t1, 'mysql')
                   . '")');
        $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.date, "%Y-%m-01"), date(t.date))) as date1, count(*) as value
                    FROM phone_records t, phone_numbers n ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                    'WHERE t.cid != 0 AND t.user_id = "'.$userid.'" AND t.cid = "'.$cid.'" AND n.id = t.tracking_id AND n.type != "voip" AND 
                        (t.date ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                            ($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                     ' group by ' . // date(l.created)'
                            'IF( @datediffv  < 32, DATE( t.date ) , DATE_FORMAT( t.date,  "%Y-%m" ) )';		 
	 } else {
		 
		$this->query('SET @datediffv	=	DATEDIFF( CURDATE(), "' . $b->start->format('Y-m-d')// self::getTs($t1, 'mysql')
                   . '")');
        $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.date, "%Y-%m-01"), date(t.date))) as date1, count(*) as value
                    FROM phone_records t, phone_numbers n ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                    'WHERE t.cid != 0 AND t.user_id = "'.$userid.'" AND n.id = t.tracking_id AND n.type != "voip" AND 
                        (t.date ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                            ($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                     ' group by ' . // date(l.created)'
                            'IF( @datediffv  < 32, DATE( t.date ) , DATE_FORMAT( t.date,  "%Y-%m" ) )';	
		 
		 //original query $q  =   'SELECT unix_timestamp(if(@datediffv > 31, date_format(t.date, "%Y-%m-01"), date(t.date))) as date1, count(*) as value
                    //FROM phone_records t, phone_numbers n ' . /* inner join active_sites s ON (s.id = t.hub_id) */ 
                    //'WHERE t.cid != 0 AND t.user_id = dashboard_userid() AND n.id = t.tracking_id AND n.type != "voip" AND 
                        //(t.date ' . $b->getTimeRangeRestriction() . ')' . //self::getTs($t1, 'mysql') . 
                            //($W->isEmpty() ? '' : $W->FullClause(' and ') ) .
                     //' group by ' . // date(l.created)'
                            //'IF( @datediffv  < 32, DATE( t.date ) , DATE_FORMAT( t.date,  "%Y-%m" ) )';	
	 }
                            
#                            echo $q;        
        $st =   $this->query($q);
        
//        if($createpoints) throw new exception('diabled');
//                return $this->getGraphPoints ($st, $t1, $t2, $valuesonly);
        
        return $st;
    }

    
    /**
     * Required data: user_id, campaign_id
     *
     * @param string[] $args
     * @param int $limit
     * @param int $offset
     * @param string $filter
     * @param string $order
     * @param string $direction
     * @return ReportModel[] | ReportModel
     */
    function getAllReportData($args, $limit = 10, $offset = 0, $filter = null, $order = "id", $direction = "asc", $rID = 0)
    {
        $qOpts = self::getQueryOptions($args);

        $where = $qOpts['where'] ? $qOpts['where'] : DBWhereExpression::Create(
            array('user_id = :user_id') // requires user_id key in $args
        );
        $args['user_id']    =   $this->getActionUser()->getID();
        $fields = 'ID , name , type , touchpoint_type , created, campaign_ID, user_ID, start_date, end_date';
        $q = "select %s %s from campaign_reports WHERE %s %s %s ";
        if(isset($args['campaign_id'])):
            $where->Where('campaign_id = :campaign_id');
//            $q .= "WHERE user_ID = :userID AND campaign_ID = :campaign_ID";
//            $args = array('userID' => $userID, 'campaign_ID' => $campaignID);
        endif;
//        if ($rID){ $q .= " AND ID = :RID"; $args['RID']=$rID;}

        if($args['sSearch']){
            $where->Where('name LIKE :sSearch');
        }
        $orderBy = '';
        if($qOpts['orderCol'] || $order){
            // @todo security
            switch($order){
				case 'ID':
				case 'name':
				case 'type':
				case 'touchpoint_type':
				case 'created':
					$orderCol = $order;
					$orderDir = $direction;
                    break;
				default:
					$orderCol = 'name';
					$orderDir = 'ASC';

            }
			$newRow = $qOpts['new_rows_ids'] ? 'id = ' . join(' DESC, id = ', $qOpts['new_rows_ids']) . ' DESC, ' : '';
            $orderBy = " Order By $newRow $orderCol $orderDir";
        }
        $q = sprintf($q, $qOpts['calc_found_rows'],
                $qOpts['cols'] ? $qOpts['cols'] : $fields,
            $where, $orderBy, $qOpts['limitstr']);
        $stmn = $this->prepare($q);
        return self::HandleQueryResults($stmn, $args, $qOpts, 'ReportModel');
    }

    function getAllReportCount($data)
    {
        $userID = $data['user_id'];
        $campaignID = $data['campaign_id'];
		unset($data['newRowsIds']);
        $q = "select count(*) from campaign_reports ";
        if(isset($data['campaign_id'])):
            $q .= "WHERE user_ID = :userID AND campaign_ID = :campaign_ID";
            $args = array('userID' => $userID, 'campaign_ID' => $campaignID);
        else:
            $q .= "WHERE user_ID = :userID";
            $args = array('userID' => $userID);
        endif;
        $stmn = $this->getQube()->GetDB()->prepare($q);
        $stmn->execute($args);
        $drt = $stmn->fetchColumn();
        return $drt;
    }

    function getChartDataStructure($focus,$drt, $search_engines){
        $final_array = (object)array('Visits' => Array() , 'Leads' => Array());
//        $search_engines = Array();
        switch ($focus){
            case 'REFERRAL':
                $final_array = (object)$search_engines;
                foreach($drt as $atp){
                    if(array_key_exists($atp['referrer'],$search_engines)){
                        if(count((array)$final_array->{$atp['referrer']}) == 0){
                            $final_array->{$atp['referrer']}=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        $final_array->{$atp['referrer']}->Visits[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['visits'], 'date' => $atp['point_date']);
                        $final_array->{$atp['referrer']}->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['leads'], 'date' => $atp['point_date']);
                    }
                    else{
                        if(count((array)$final_array->Others) == 0){
                            $final_array->Others=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        if(!array_key_exists($atp['point_date'], $final_array->Others->Visits)){
                            $final_array->Others->Visits[$atp['point_date']] = (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                            $final_array->Others->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                        }
                        $final_array->Others->Visits[$atp['point_date']]->visits += $atp['visits'];
                        $final_array->Others->Leads[$atp['point_date']]->visits += $atp['leads'];
                    }
                }
                $final_array->Bar_Colors = Array('#009e63','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b');
                break;
            case 'ORGANIC':
                $final_array = (object)$search_engines;
                foreach($drt as $atp){
                    if(array_key_exists($atp['searchengine'],$search_engines)){
                        if(count((array)$final_array->{$atp['searchengine']}) == 0){
                            $final_array->{$atp['searchengine']}=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        $final_array->{$atp['searchengine']}->Visits[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['visits'], 'date' => $atp['point_date']);
                        $final_array->{$atp['searchengine']}->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['leads'], 'date' => $atp['point_date']);
                    }
                    else{
                        if(count((array)$final_array->Others) == 0){
                            $final_array->Others=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        if(!array_key_exists($atp['point_date'], $final_array->Others->Visits)){
                            $final_array->Others->Visits[$atp['point_date']] = (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                            $final_array->Others->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                        }
                        $final_array->Others->Visits[$atp['point_date']]->visits += $atp['visits'];
                        $final_array->Others->Leads[$atp['point_date']]->visits += $atp['leads'];
                    }
                }
                $final_array->Bar_Colors = Array('#009e63','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b');
                break;
            case 'SOCIAL':
                $final_array = (object)$search_engines;
                foreach($drt as $atp){
                    if(array_key_exists($atp['network'],$search_engines)){
                        if(count((array)$final_array->{$atp['network']}) == 0){
                            $final_array->{$atp['network']}=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        $final_array->{$atp['network']}->Visits[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['visits'], 'date' => $atp['point_date']);
                        $final_array->{$atp['network']}->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date']. ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['leads'], 'date' => $atp['point_date']);
                    }
                    else{
                        if(count((array)$final_array->Others) == 0){
                            $final_array->Others=(object)Array( 'Visits' => Array(),'Leads' => Array() );
                        }
                        if(!array_key_exists($atp['point_date'], $final_array->Others->Visits)){
                            $final_array->Others->Visits[$atp['point_date']] = (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                            $final_array->Others->Leads[$atp['point_date']]= (object)Array('timestamp' => (strtotime($atp['point_date'] . ' UTC')*1000), 'type' => 'visit', 'visits' => 0, 'date' => $atp['point_date']);
                        }
                        $final_array->Others->Visits[$atp['point_date']]->visits += $atp['visits'];
                        $final_array->Others->Leads[$atp['point_date']]->visits += $atp['leads'];
                    }
                }
                $final_array->Bar_Colors = Array('#009e63','#27bdbe','#00aeef','#1e4382','#612c5c','#e02a74','#ea413e','#ee6f23','#f4a41d','#faf06f','#4d555b');
                break;
            case '':
                $final_array = (object)array('Visits' => Array() , 'Leads' => Array());
                foreach ($drt as $index => $atp )
                {
                    $final_array->Visits[]= (object)Array('timestamp' => (strtotime($atp['point_date']. ' UTC')*1000), 'type' => 'visit', 'visits' => $atp['nb_visits']);
                    $final_array->Leads[]= (object)Array('timestamp' => (strtotime($atp['point_date']. ' UTC')*1000), 'type' => 'leads', 'visits' => $atp['nb_leads']);
                }
                break;
        }
        if ((bool)$final_array->Others) {
            $Others_Content = array_filter(get_object_vars($final_array->Others));
        }
        if((bool)$final_array->Others & empty($Others_Content)){
            unset($final_array->Others);}
        return $final_array;
    }

    function getWebsites(ReportModel $report, $filter){
		$args  = array('user_id' => $report->user_ID);
		$where ='';
		$tbl = 'hub';
		$tblval = 'hub';
		$union = '';
		$name = 'name';
		switch ($filter["touchpoint_type"]){
            case 'ALL':
				$whereCID = '';
				if($report->campaign_ID){
					$whereCID = 'cid = :CID AND';
				}
                $union = "UNION ALL SELECT blog_title name, 'blog' tbl, id, httphostkey from blogs WHERE user_id = :user_id AND $whereCID trashed = 0";
                break;
            case 'BLOGS':
                $tbl = 'blogs';
                $tblval = 'blog';
				$name = 'blog_title';
                break;
            case 'SOCIAL':
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'MOBILE':
                $where .=" AND touchpoint_type = '{$filter['touchpoint_type']}' ";
                break;
            default:
                throw new Exception("Not supported touchpoint type {$filter['touchpoint_type']}");

        }

		if($report->campaign_ID){
			$where .= ' AND cid = :CID';
			$args['CID'] = $report->campaign_ID;
		}

        if($filter['search']){
            $args['search'] = "%{$filter['search']}%";
            $where .= " AND ($name LIKE :search OR httphostkey LIKE :search)";
        }

        $q = "SELECT $name name, '$tblval' tbl, id, httphostkey FROM " . $tbl . " WHERE user_id = :user_id AND trashed = 0" .  $where . " $union";
        $stmn = $this->getQube()->GetDB()->prepare($q);
//        $stmn->execute($args= array('CID' => $report->campaign_ID));
		$stmn->execute($args);
        $drt = $stmn->fetchAll();
        $final_array = array();
        foreach ($drt as $index => $atp )
        {
            $final_array[]= Array('INDEX' => $index, 'filter' => $atp['tbl'] . '-' . $atp['id'], 'website' => $atp['httphostkey'] ? $atp['httphostkey'] : $atp['name']);
        }
        return $final_array;
    }

    function getChartPoints(ReportModel $report, $filter, $focus = '', $items = array()){
        $startDate = new DateTime();
		$endDate = new DateTime();
        switch($report->getType()){
            case 'DAILY':
                $interval = 'P1D';
                $startDate->sub(new DateInterval('P9D'));
				$chartPoints = $this->getChartPoints2($report, $filter, $focus, $items);
//                $chartPoints = $this->getDailyChartPoints($report, $filter, $focus, $items);
                break;
            case 'WEEKLY':
                $interval = 'P1W';
				$chartPoints = $this->getChartPoints2($report, $filter, $focus, $items);
//                $chartPoints = $this->getWeeklyChartPoints($report, $filter, $focus, $items);
                $startDate->setTimeStamp(strtotime('-10 week this monday', time()));
                break;

            CASE 'MONTHLY':
                $interval = 'P1M';
                PointsSetBuilder::firstDayCurrentMonth($startDate)->sub(new DateInterval('P9M'));
				$chartPoints = $this->getChartPoints2($report, $filter, $focus, $items);
//                $chartPoints = $this->getMonthlyChartPoints($report, $filter, $focus, $items);
                break;
            CASE 'YEARLY':
                $interval = 'P1Y';
                $startDate->modify(date('Y-01-01', $startDate->getTimestamp()))->sub(new DateInterval('P9Y'));
				$chartPoints = $this->getChartPoints2($report, $filter, $focus, $items);
//				$chartPoints = $this->getYearlyChartPoints($report, $filter, $focus, $items);
				break;
            default:
                throw new Exception("Report type {$report->type} not supported");

        }

		if($report->type == 'CUSTOM'){
			$endDate = new DateTime($report->start_date);
		}

		for($date = clone $endDate, $i = 0; $startDate <= $date; $startDate->add(new DateInterval($interval)), $i++){
            $format = $startDate->format('Y-m-d');
            if($focus != ''){
                foreach($items as $key => $item){
                    if(array_key_exists($key, $chartPoints) && $chartPoints->$key->Visits[$format]->timestamp != (strtotime($format . ' UTC')) * 1000){
						$a_visits = (array)$chartPoints->$key->Visits;
						$a_leads = (array)$chartPoints->$key->Leads;
                        array_splice($a_visits, $i, 0, array((object)Array('timestamp' => (strtotime($format . ' UTC')) * 1000, 'type' => 'visit', 'visits' => 0, 'date' => $format)));
                        array_splice($a_leads, $i, 0, array((object)Array('timestamp' => (strtotime($format . ' UTC')) * 1000, 'type' => 'visit', 'visits' => 0, 'date' => $format)));
                    }
                }
            }
            else{
                if(count($chartPoints->Visits) > $i - 1 && $chartPoints->Visits[$i]->timestamp != (strtotime($format . ' UTC')) * 1000){
					$a_visits = (array)$chartPoints->Visits;
					$a_leads = (array)$chartPoints->Leads;
                    array_splice($a_visits, $i, 0, array((object)Array('timestamp' => (strtotime($format . ' UTC')) * 1000, 'type' => 'visit', 'visits' => 0, 'date' => $format)));
                    array_splice($a_leads, $i, 0, array((object)Array('timestamp' => (strtotime($format . ' UTC')) * 1000, 'type' => 'visit', 'visits' => 0, 'date' => $format)));
                }
            }
        }
        return $chartPoints;
    }

//    static function getTableFromType($tp_type){
//        switch($tp_type){
//            case 'ALL':
//                return '';
//            case 'WEBSITE':
//                return 'hub';
//            case 'BLOGS':
//                return 'blogs';
//        }
//        return '';
//    }

	function getChartPoints2(ReportModel $report, $filter, $focus = '', $items = array()){
		$where = array();

		$table = $this->getTableByFocus($focus);

		$cols = $this->getFieldsByFocus($report, $focus);

		$join = $this->getJoinByTouchpointType($report->touchpoint_type);



		$where = $this->getWhere($report, $filter, $table, $where);

		$groupBy = $this->getGroupBy($report, $focus);

		$orderBy = $this->getOrderBy($report, $table, $focus);

		$q = "SELECT $cols FROM $table $join $where $groupBy $orderBy";

		$args = array('user_id' => $this->getActionUser()->getID());
		if($report->campaign_ID){
			$args['CID'] = $report->campaign_ID;
		}

		$stmn = $this->getQube()->GetDB()->prepare($q);
		$stmn->execute($args);
		$drt = $stmn->fetchAll();
		$final_array = $this->getChartDataStructure($focus,$drt, $items);
		switch($report->getType()){
			case 'DAILY':
				$Chart_Size = 1000 * 60 * 60 * 2;
				break;
			case 'WEEKLY':
				$Chart_Size = 1000 * 60 * 60 * 24 * 70 / (20 * count((array)$final_array) - 1);
				break;
			case 'MONTHLY':
				$Chart_Size = 1000 * 60 * 60 * 24 * 2;
				break;
			case 'YEARLY':
				$Chart_Size = 1000 * 60 * 60 * 24 * 30;
				break;
		}
		$final_array->Chart_Size = $Chart_Size;
		return $final_array;
	}


    function getSearchVisits(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_se', 'ifnull(sum(nb_visits), 0)');
    }

    function getSearchLeads(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_se', 'ifnull(sum(nb_leads), 0)');
    }

	function getSearchBounceVisits($report, $filter){
		return $this->getPiwikStats($report, $filter, 'piwikreports_se', 'ifnull(sum(bounce_count), 0)');
	}

    function getReferralVisits(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_ref', 'ifnull(sum(nb_visits), 0)');
    }

    function getReferralLeads(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_ref', 'ifnull(sum(nb_leads), 0)');
    }

	function getReferralBounceVisits($report, $filter){
		return $this->getPiwikStats($report, $filter, 'piwikreports_ref', 'ifnull(sum(bounce_count), 0)');
	}

    function getSocialVisits(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_social', 'ifnull(sum(nb_visits), 0)');
    }

    function getSocialLeads(ReportModel $report, $filter = ''){
        return $this->getPiwikStats($report, $filter, 'piwikreports_social', 'ifnull(sum(nb_leads), 0)');
    }

	function getSocialBounceVisits($report, $filter){
		return $this->getPiwikStats($report, $filter, 'piwikreports_social', 'ifnull(sum(bounce_count), 0)');
	}

    function getAllVisits(ReportModel $report, $filter = '', $table = 'piwikreports'){
        return $this->getPiwikStats($report, $filter, $table, 'ifnull(sum(nb_visits), 0)');
    }

    function getAllLeads(ReportModel $report, $filter = '', $table = 'piwikreports'){
        return $this->getPiwikStats($report, $filter, $table, 'ifnull(sum(nb_leads), 0)');
    }

	function getAllBounceVisits($report, $filter){
		return $this->getPiwikStats($report, $filter, 'piwikreports', 'ifnull(sum(bounce_count), 0)');
	}


	/* The following functions are used as functions adapters with DataAccessTableResult*///============================//
	function getDataTableReferralSearch($params){																		//
		return $this->getReferralSearch($params['report'], $params['filter'], false, $params['sSearch'], $params);		//
	}																													//
																														//
	function getDataTableOrganicSearch($params){																		//
		return $this->getOrganicSearch($params['report'], $params['filter'],false, $params['sSearch'], $params);		//
	}																													//
																														//
	function getDataTableSocialMediaSearch($params){																	//
		return $this->getSocialSearch($params['report'], $params['filter'],false, $params['sSearch'], $params);			//
	}																													//
	/* End functions adapters*///=======================================================================================//

	function getOrganicSearch(ReportModel $report, $filter = '', $total = false, $search = '', $queryOptions = array()){
        return $this->getPiwikStats($report, $filter, 'piwikreports_se',
            'ifnull(sum(nb_visits), 0) visits, ' .
			'ifnull(sum(nb_leads), 0) leads, ' .
			'ifnull(sum(bounce_count), 0) bounce_count,' .
			'FORMAT(IFNULL(SUM(nb_leads), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) rate, ' .
			'FORMAT(IFNULL(SUM(bounce_count), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) bounce_rate',
            $total ? '' : 'searchengine',
            $total,
			$search,
			$queryOptions
        );
    }

	/**
	 * @param ReportModel $report Report which data are obtained
	 * @param string $filter Filter data for a specific touchpoint with the following format TOUCHPOINT_TYPE-ID eg. hub-21342
	 * @param bool $total If true is passed, then will return a single row with data totalized
	 * @param string $search
	 * @param array $queryOptions
	 * @return array|string
	 */
    function getReferralSearch(ReportModel $report, $filter = '', $total = false, $search = '', $queryOptions = array()){
        return $this->getPiwikStats($report, $filter, 'piwikreports_ref',
            'ifnull(sum(nb_visits), 0) visits, ' .
			'ifnull(sum(nb_leads), 0) leads, ' .
			'ifnull(sum(bounce_count), 0) bounce_count,' .
			'FORMAT(IFNULL(SUM(nb_leads), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) rate, ' .
			'FORMAT(IFNULL(SUM(bounce_count), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) bounce_rate',
            $total ? '' : 'referrer',
            $total,
			$search,
			$queryOptions
        );
    }



    function getSocialSearch(ReportModel $report, $filter = '', $total = false, $search = '', $queryOptions = array()){
        return $this->getPiwikStats($report, $filter, 'piwikreports_social',
            'ifnull(sum(nb_visits), 0) visits, ' .
			'ifnull(sum(nb_leads), 0) leads, ' .
			'ifnull(sum(bounce_count), 0) bounce_count,' .
			'FORMAT(IFNULL(SUM(nb_leads), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) rate, ' .
			'FORMAT(IFNULL(SUM(bounce_count), 0) / IFNULL(SUM(nb_visits), 1) * 100, 2) bounce_rate',
            $total ? '' : 'network',
            $total,
			$search,
			$queryOptions
        );
    }
    private function getPiwikStats(ReportModel $report, $filter, $table, $columns, $groupBy = '', $total = false, $search = '', $queryOptions = array()){
        $campaignID = $report->campaign_ID;
        $userID = $report ->user_ID;
        $args = array('user_id' => $userID);


        $q = $this->getPiwikStatsQuery($table, $columns, $report, $filter, $groupBy, $search, $queryOptions);

        if($filter){
            $this->getFilterInfo($filter, $tableType, $typeID);
            $args['tbl'] = $tableType;
            $args['tbl_ID'] = $typeID;
        }
		if($campaignID){
			$args['cid'] = $campaignID;
		}
		if($search){
			$args['search'] = "%$search%";
		}
        $stmnt = $this->prepare($q);
        $stmnt->execute($args);

		if($queryOptions['getStatement']){
			return $stmnt;
		}

		if($groupBy || $total)
			return $stmnt->fetchAll(PDO::FETCH_OBJ);

		return $stmnt->fetchColumn();
    }

    private function getPiwikStatsQuery($table, $columns, ReportModel $report, $filter = '', $groupBy = '', $search = '', $queryOptions = array()){
		$isCount = $queryOptions['cols'] == "count(*)";
		$columns = $isCount ? "count(DISTINCT $groupBy)" : $columns;
		$where = array("$table.user_ID = :user_id");
		$where[] = $this->getRangeWhere($report);

		$join = '';
        switch($report->touchpoint_type){
            case 'WEBSITE':
            case 'LANDING':
            case 'MOBILE':
            case 'PROFILE':
            case 'SOCIAL':
                $join .= " LEFT JOIN hub ON tbl_id = hub.id";
                $where[] = "tbl = 'hub' AND hub.touchpoint_type = '{$report->touchpoint_type}'";
            break;
            case 'BLOGS':
                $join .= " LEFT JOIN blogs ON tbl_id = blogs.id";
                $where[] = "tbl = 'blog'";
                break;
            case 'ALL':
                break;
            default:
                throw new Exception("Not supported touchpoint type {$report->touchpoint_type}");
        }

        if($filter){
            $this->getFilterInfo($filter, $tableType, $typeID);
            $where[] = 'tbl = :tbl AND tbl_ID = :tbl_ID';
        }

		if($search){
			$where[] = "$groupBy LIKE :search";
		}
		if($report->campaign_ID)
			$where[] = "$table.cid = :cid";

		$groupByColumn = '';
		$orderBy = '';
		if($groupBy && !$isCount){
			$groupByColumn = ", $groupBy";
			$groupBy = "GROUP BY $groupBy";
		}else{
			$groupBy = '';
		}

		if($queryOptions['orderCol'] && !$isCount){
			switch($queryOptions['orderCol']){
				case 'referrer':
				case 'searchengine':
				case 'network':
				case 'visits':
				case 'leads':
				case 'rate':
				case 'bounce_rate':
					break;
				default:
					throw new Exception("Column '{$queryOptions['orderCol']}' not supported in order by");
			}
			$orderBy = "order by {$queryOptions['orderCol']} " . ($queryOptions['orderDir'] == 'desc' ? 'DESC' : 'ASC');
		}

		$limtStr = "";
		if($queryOptions['limit']){
			$limit = intval($queryOptions['limit']);
			$offset = intval($queryOptions['limit_start']);
			$limtStr = "LIMIT $offset, $limit";
		}
		if(!empty($where)){
			$where = array_filter($where);
			$where = "WHERE " . join(' AND ', $where);
		}
		return "SELECT SQL_CALC_FOUND_ROWS $columns $groupByColumn FROM $table $join $where $groupBy $orderBy $limtStr";
    }

    private function getFilterInfo($filter, &$type, &$typeID){
        if($filter)
            list($type, $typeID) = explode('-', $filter);
    }

	/**
	 * @param ReportModel $report
	 * @return string
	 * @throws Exception
	 */
	private function getRangeWhere(ReportModel $report)
	{
		switch ($report->type) {
			case 'YEARLY':
				return "year(point_date) > year(date_sub(current_date(), INTERVAL " . ReportDataAccess::N_ITEMS . " YEAR))";
				break;
			case 'WEEKLY':
				return "yearweek(point_date, 1) > yearweek(date_sub(current_date(), INTERVAL " . ReportDataAccess::N_ITEMS . " WEEK), 1)";
				break;
			case 'MONTHLY':
				return "point_date > date_format(date_sub(current_date(), INTERVAL " . (ReportDataAccess::N_ITEMS - 1) . " MONTH), '%Y-%m-01')";
				break;
			case 'DAILY':
				return "point_date > date_sub(current_date(), INTERVAL " . ReportDataAccess::N_ITEMS . " DAY)";
				break;
			case 'CUSTOM':
				return "point_date BETWEEN '{$report->start_date}' AND '{$report->end_date}'";
				break;
			default:
				throw new Exception("{$report->type} Not supported");
				break;
		}
	}

	/**
	 * @param $focus
	 * @return string
	 */
	private function getTableByFocus($focus)
	{
		switch ($focus) {
			case '':
				$table = 'piwikreports';
				break;
			case 'ORGANIC':
				$table = 'piwikreports_se';
				break;
			case 'SOCIAL':
				$table = 'piwikreports_social';
				break;
			case 'REFERRAL':
				$table = 'piwikreports_ref';
				break;
			default:
				throw new InvalidArgumentException("$focus type not supported");
		}
		return $table;
	}

	private function getJoinByTouchpointType($touchpoint_type)
	{
		switch($touchpoint_type){
			case 'WEBSITE':
			case 'LANDING':
			case 'MOBILE':
            case 'PROFILE':
			case 'SOCIAL':
				return "LEFT JOIN hub ON tbl_id = hub.id";
			case 'BLOGS':
				return  "LEFT JOIN blogs ON tbl_id = blogs.id";
			case 'ALL':
				return 'LEFT JOIN hub ON tbl_id = hub.id LEFT JOIN blogs ON tbl_id = blogs.id';
			default:
				throw new InvalidArgumentException("Not supported touchpoint type {$touchpoint_type}");
		}
	}

	private function getFieldsByFocus(ReportModel $report, $focus)
	{
		$fieldPointDate = $this->getPointDateField($report, $focus);
		switch($focus){
			case '':
				$fields = 'IFNULL(SUM(nb_leads), 0) as nb_leads, ' .
					'IFNULL(SUM(nb_visits), 0) as nb_visits, ' .
					"$fieldPointDate point_date";
				break;
			case 'ORGANIC':
				$column = "searchengine";
				break;
			case 'SOCIAL':
				$column = "network";
				break;
			case 'REFERRAL':
				$column = "referrer";
				break;
			default:
				throw new InvalidArgumentException("$focus type not supported");
		}

		if($focus != ''){
			$fieldData = $this->getFieldData($report, $column);
			$fields = "$fieldData as data, " .
				"$fieldPointDate point_date, " .
				"$column, " .
				"sum(nb_visits) visits, sum(nb_leads) leads";

		}
		return $fields;
	}

	private function getTouchpointTypeWhere($touchpoint_type)
	{
		switch($touchpoint_type){
			case 'WEBSITE':
			case 'LANDING':
			case 'MOBILE':
            case 'PROFILE':
			case 'SOCIAL':
				return "tbl = 'hub' AND hub.touchpoint_type = '{$touchpoint_type}' AND hub.trashed = 0";
			case 'BLOGS':
				return "tbl = 'blog' AND blogs.trashed = 0";
			case 'ALL':
				return "((tbl = 'hub' AND hub.trashed = 0) OR (tbl = 'blog' AND blogs.trashed = 0))";
			default:
				throw new InvalidArgumentException("Not supported touchpoint type {$touchpoint_type}");
		}
	}

	private function getWhereTouchpointFilter($filter)
	{
		if($filter){
			list($tbl, $table_id) = explode('-', $filter);
			return "tbl_ID = '$table_id'";
		}

		return false;
	}

	/**
	 * @param ReportModel $report
	 * @param $filter
	 * @param $table
	 * @return string
	 * @throws Exception
	 */
	private function getWhere(ReportModel $report, $filter, $table)
	{
		$where = array();

		if ($report->campaign_ID) {
			$where[] = "$table.cid = :CID";
		}
		$where[] = "$table.user_id = :user_id";

		$where[] = $this->getRangeWhere($report);

		$where[] = $this->getTouchpointTypeWhere($report->touchpoint_type);

		$where[] = $this->getWhereTouchpointFilter($filter);

		$where = array_filter($where);

		return "WHERE " . join(' AND ', $where);
	}

	private function getGroupBy(ReportModel $report, $focus)
	{
		switch($focus){
			case '':
				$group = $this->getPointDateField($report, $focus);
				return "GROUP BY $group";
			case 'ORGANIC':
			case 'SOCIAL':
			case 'REFERRAL':
				return 'GROUP BY data';
			default:
				throw new InvalidArgumentException("$focus type not supported");
		}
	}

	private function getFieldData(ReportModel $report, $column)
	{
		switch($report->getType()){
			case 'DAILY':
				return "CONCAT(point_date,'-',$column)";
			case 'WEEKLY':
				return "CONCAT(yearweek(point_date, 1) , $column)";
			case 'MONTHLY':
				return "CONCAT(year(point_date) , MONTH (point_date), $column)";
			case 'YEARLY':
				return "CONCAT(year(point_date) , '-' , $column)";
			default:
				throw new InvalidArgumentException("{$report->type} Not supported");
		}
	}

	private function getPointDateField(ReportModel $report, $column)
	{
		switch($report->getType()){
			case 'DAILY':
				return "point_date";
			case 'WEEKLY':
				return "STR_TO_DATE(CONCAT(yearweek(point_date),' Monday'), '%X%V %W')";
			case 'MONTHLY':
				return "CONCAT(year(point_date),'-',month(point_date),'-1')";
			case 'YEARLY':
				return "CONCAT(year(point_date),'-01-01')";
			default:
				throw new InvalidArgumentException("{$report->type} Not supported");
		}
	}

	private function getOrderBy($report, $table, $focus)
	{
		switch($focus){
			case '':
				return "ORDER BY $table.point_date";
			case 'ORGANIC':
			case 'SOCIAL':
			case 'REFERRAL':
				return 'ORDER BY visits DESC';
			default:
				throw new InvalidArgumentException("$focus type not supported");
		}
	}
}


