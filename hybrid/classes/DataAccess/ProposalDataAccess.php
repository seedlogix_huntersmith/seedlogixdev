<?php


class ProposalDataAccess extends QubeDataAccess {
	
	
	function getProductsbyProposal(ProposalModel $P, $params = array())
	{
		$query = 'SELECT * FROM products WHERE proposal_id = "'.$params['proid'].'" AND trashed = "00-00-0000" AND qty >= "1" ORDER BY sort ASC';


		$stmt = $this->prepare($query);
		$stmt->execute(array('proposal_id' => $P->getID()));
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'ProductsModel');
	}
	
	
}