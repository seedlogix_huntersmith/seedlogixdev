<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResponderDataAccess
 *
 * @author amado
 */
class ResponderDataAccess extends QubeDataAccess {
	
	protected $instances_tbl	=	'responder_instances';
	protected $responders_tbl	=	'auto_responders';
	
	function createMaster(ResponderModel $R)
	{
		if($R->isMaster())	return;
		$this->query('UPDATE auto_responders SET master_ID = id WHERE id = ' . $R->getID());
		$R->set('master_ID', $R->getID());
	}
	
	function getSlaveResponder($master_ID, $for_user_ID)
	{
		if(!$this->createSlaveResponder($master_ID, $for_user_ID))	return FALSE;
		$slave_ID	=	$this->getPDO()->lastInsertId();
		$Responder	=	new ResponderModel();
		$Responder->setID($slave_ID);
		return $this->refreshObject($Responder);
	}
	
	function createSlaveResponder($responder_ID, $for_user_ID)
	{	
		$q	=	"insert into {$this->responders_tbl} (
`user_id`,
`user_parent_id`, `master_ID`,
`cid`,
`type`,
`table_name`,
`table_id`,
`name`,
`active`,
`sched`,
`sched_mode`,
`from_field`,
`subject`,
`body`,
`return_url`,
`contact`,
`anti_spam`,
`optout_msg`,
`theme`,
`theme_id`,
`stream_blog_id`,
`logo`,
`bg_color`,
`links_color`,
`accent_color`,
`titles_color`,
`edit_region1`,
`call_action`,
`edit_region2`,
`trashed`) SELECT :user_ID,
`user_parent_id`, `id`,
`cid`,
`type`,
`table_name`,
`table_id`,
`name`,
`active`,
`sched`,
`sched_mode`,
`from_field`,
`subject`,
`body`,
`return_url`,
`contact`,
`anti_spam`,
`optout_msg`,
`theme`,
`theme_id`,
`stream_blog_id`,
`logo`,
`bg_color`,
`links_color`,
`accent_color`,
`titles_color`,
`edit_region1`,
`call_action`,
`edit_region2`,
`trashed` FROM {$this->responders_tbl} WHERE id=:responder_ID";

		$stmt	=	$this->prepare($q);
		return $stmt->execute(array('responder_ID' => $responder_ID, 'user_ID' => $for_user_ID));
	}	
	
	function addResponderToForm(ResponderModel $R, LeadFormModel $LF)
	{
		$q	=	"INSERT IGNORE INTO {$this->instances_tbl} (responder_ID, user_ID, table, table_ID) VALUES (:responder_ID, :user_ID, :table, :table_ID)";
		
		$stmt	=	$this->prepare($q);
		return $stmt->execute(array('responder_ID' => $R->getID(),
					'user_ID' => $LF->user_id,
					'table' => 'auto_responders',
					'table_ID' => $LF->getID()));
	}
	
	
	function getMasterResponder($responder_ID)
	{
		$stmt	=	$this->queryMasterResponders(DBWhereExpression::Create(array('id' => (int)$responder_ID)));
		return $stmt->fetchObject('ResponderModel');
	}
	
	function getResellerMasterResponders($reseller_ID)
	{
		$stmt	=	$this->queryMasterResponders(DBWhereExpression::Create('R.user_ID = :reseller_ID AND R.trashed = 0'), 
					array('reseller_ID' => $reseller_ID));
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'ResponderModel');
	}
	
	function getMasterLeadForms($args, $form_ID	=	0)
	{
		$QOpts = self::getQueryOptions($args);
		$cols = '*';
		$orderCol = 'name';
		$orderDir = 'ASC';
		$newRow = "";
		if($QOpts['cols']){
			$cols = $QOpts['cols'];
		}

		if(isset($QOpts['orderCol'])){
			switch($QOpts['orderCol']){
				case 'name':
					$orderCol = $QOpts['orderCol'];
					break;
			}
		}

		if(isset($QOpts['orderDir'])){
			$orderDir = $QOpts['orderDir'];
		}
		$where = array(
			'multi_user = 1',
			'trashed = 0',
			'user_ID = :reseller_ID'
		);

		if($form_ID)
			$where[] = 'id = ' . (int)$form_ID;

		if($args['sSearch']){
			$where[] = 'name LIKE :sSearch';
		}

		if($QOpts['new_rows_ids']){
			$newRow = 'id = ' . join(' DESC, id = ', $QOpts['new_rows_ids']) . ' DESC, ' ;
		}

		$where = 'WHERE ' . join(' AND ', $where);

		$query	=	"SELECT {$QOpts['CALC_FOUND_ROWS']} $cols FROM lead_forms $where ORDER BY $newRow $orderCol $orderDir {$QOpts['limitstr']}" ;

		$stmt	=	$this->prepare($query);

		self::callPreparedQuery($stmt, $args);

		if($QOpts['getStatement']){
			return $stmt;
		}

		if(!$form_ID)
			return $stmt->fetchAll(PDO::FETCH_CLASS, 'LeadFormModel');
		return $stmt->fetchObject('LeadFormModel');
	}
	
	
	/**
	 * 
	 * @return PDOStatement
	 */
	protected function queryMasterResponders(DBWhereExpression $where = NULL, $params	=	array())
	{
		if(!$where)
			$where	= DBWhereExpression::Create (array('R.trashed' => 0));
		
		$query	=	'select * FROM auto_responders R WHERE R.master_ID = R.id AND ' . $where;
		$stmt	=	$this->prepare($query);
		
		$stmt->execute($params);
		
		return $stmt;
	}
	
	/**
	 * 
	 * Gets Responders (Masters, Slaves, Normal) for a user
	 *
	 * @param type $user_ID
	 * @return ResponderModel[]
	 */
	function getUserResponders($user_ID)
	{
		/*
		 *  Description of query:
		 *	a. load responders belonging to the user which are not master responders (master_ID != id)
		 *		-	 this includes both user's normal responders (master_ID = 0) and slave responders (master_ID = ?)
		 *	b. load all master responders belonging to the reseller (G.user_ID = U.parent_id)
		 *			that do not  have a slave belonging to the user
		 *		-	this includes only reseller master responders that have not been customized (a slave does not exit)
		 */
		
		$q	=	"SELECT * FROM auto_responders R WHERE R.user_ID = :user_ID AND R.master_ID != R.id AND R.trashed = 0
	UNION
	SELECT G.* FROM auto_responders G inner join users U ON (U.id = :user_ID AND G.user_ID = U.parent_id)  
		left join auto_responders X on ( X.master_ID = G.master_ID AND X.id != G.id ) 
	WHERE G.master_ID = G.id AND G.trashed = 0 and isnull(X.id)";
		
		$stmt	=	$this->prepare($q);
		$stmt->execute(array('user_ID' => $user_ID));
		
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'ResponderModel');
	}
	
	function updateSlaveResponders(array $updatedata, ResponderModel $master)
	{
		$cols	= ResponderModel::getPropagateColumns();
		$updatekeys	= array_intersect(array_keys($updatedata), $cols);
		
		if(!$updatekeys)	return	0;
		
		$vals	=	array();
		
		$master_ID	=	$master->getID();
		
		foreach($updatekeys as $key) {
			$placeholders	=	array();
			$updatestr	=	"`$key`	=	:$key";
			$placeholders[$key]	=	$updatedata[$key];
			$placeholders['prevvalue']	=	$master->get($key);
			$q	=	"UPDATE auto_responders SET %s "
			. "WHERE `$key` = :prevvalue AND `master_ID` = $master_ID AND `master_ID` != `id`";
			$query	=	sprintf($q, $updatestr);
			
			$stmt	=	$this->prepare($query);
			$stmt->execute($placeholders);
		}
	}

	function getFormInstantResponders(array $qArgs)
	{
		$params = array('where' => $qArgs + array('R.sched' => 0, 'R.active' => 1, 'R.trashed' => '0000-00-00'));
		return $this->getResponders($params);
	}
	
	function getResponders(array $qArgs, array $params	=	array()) {
		$params['R_user_ID']	=	$qArgs['where']['R.user_ID'] ? $qArgs['where']['R.user_ID'] : $this->getActionUser()->getID();
		$qOpts	=	self::getQueryOptions($qArgs);
		$whereArr	=	isset($qOpts['where']) ? array($qOpts['where']) : array();
		$with_source	= isset($qArgs['with-source']);
		$searchconditions	=	'';
		if(isset($qArgs['sSearch']))
		{
//			$args['sSearch']	=	$qArgs['sSearch'];
			if(is_numeric($qArgs['sSearch']))
			{
				$searchconditions	= 'R.id = :sSearch OR R.table_id = :sSearch';
			}else{
				$searchconditions	= 'R.name LIKE :sSearch OR R.subject LIKE :sSearch';
				if($with_source) {
					$searchconditions .= ' or b.blog_title LIKE :sSearch or h.name LIKE :sSearch or f.name LIKE :sSearch OR h.httphostkey LIKE :sSearch';
				}
			}
			// add search donditions to where only if its not a with_source query
			if(!$with_source)
				$whereArr[]	=	'(' . $searchconditions . ')';
			else
				$searchconditions	=	str_replace('R.', 'u.', $searchconditions);

			$params['sSearch']	=	$qArgs['sSearch'];
		}
//		$wherestr	=	implode(' AND ', $whereArr);
		$wherestr	=	DBWhereExpression::Create($whereArr);
//		$args = array();
		/**
		 * First, Select all normal responders belonging to the user including modified slaves.
		 * THen, Union Select all master resopnders belonging to the parent that have not been modified by the user.
		 */
		$query = "SELECT * FROM auto_responders R WHERE $wherestr AND master_ID != id " . // AND user_id = :user_ID AND table_name = :table_name AND table_id = :table_id
							" UNION
				SELECT R.* FROM auto_responders R inner join users U ON (U.id = :R_user_ID AND U.parent_id = R.user_id)
						left join auto_responders X on ( X.master_ID = R.master_ID AND X.id != R.id ) 
					WHERE R.master_ID = R.id AND ( $wherestr ) and isnull(X.id) "; // AND R.table_name = :table_name AND R.table_id = :table_id ";
		if(isset($qArgs['join_mainsite']))
		{
			$query = sprintf('SELECT g.*, r.main_site, concat("/users/%s/hub_page/", g.logo) as site_logo FROM ( %s ) g ' .
						' LEFT JOIN resellers r ON (r.admin_user = if(g.user_parent_id=0,g.user_id,g.user_parent_id))',
						$query);
		}
		$fields = 'u.*';
		if($qOpts['cols'])
		{
			$fields	=	$qOpts['cols'];
		}
		$orderCol	=	'u.name';
		$orderDir	=	$qOpts['orderDir'];
		if($qOpts['orderCol'])
		{
			switch($qOpts['orderCol'])
			{
				case 'u.name':
				case 'h.touchpoint_type':
				case 'responder_context':
				case 'u.sched':
				case 'u.active':
					$orderCol	=	$qOpts['orderCol'];
					break;
			}
		}
		$newRow = $qOpts['new_rows_ids'] ? 'u.id = ' . join(' DESC, u.id = ', $qOpts['new_rows_ids']) . ' DESC, ' : '' ;
		$orderBy = $qOpts['isCount'] ? '' : ' ORDER BY ' . $newRow . ' ' . $orderCol . ' ' . $orderDir;

		if($with_source) {
			if(isset($qArgs['context_filter'])){
				$searchconditions = "coalesce(b.blog_title, h.name, f.name, '#Error. Does Not Exist.') = :context_filter";
			}
			$query = sprintf("SELECT%s $fields, coalesce(b.blog_title, h.name, f.name, '#Error. Does Not Exist.') as responder_context, h.touchpoint_type FROM ( %s ) u
						LEFT JOIN blogs b ON (u.table_name = 'blogs' AND b.id = u.table_id)
						LEFT JOIN hub h ON (u.table_name = 'hub' AND h.id = u.table_id)
						LEFT JOIN lead_forms f ON (u.table_name = 'lead_forms' AND f.id = u.table_id)
						%s%s%s
						", $qOpts['CALC_FOUND_ROWS'], $query, $searchconditions ? ' WHERE ' . $searchconditions : '',
							$orderBy, $qOpts['limitstr']);
		}
		$stmt		=	$this->prepare($query);
		unset($qArgs['with-source']);
		self::callPreparedQuery($stmt, $qArgs + $params);
		if($qOpts['getStatement'])
		{
			return $stmt;
		}
//		$statement	=	$PDO->query($query);
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'ResponderModel');
	}
	
	/**
	 * 
	 * Support for master / slaves not coded yet. returns only user responders.
	 * 
	 * @param int $lead_id
	 * @param int $limit
	 * @return ResponderModel[]
	 */
	function getLeadResponders($lead_id, $limit	= NULL)	//$form_id, $user_id, $limit = NULL)
	{
		$q	=	'select R.* from auto_responders R '
				. ' inner join leads l 
					on (l.lead_form_id = R.table_id and spam = 0)
					where R.table_name = "lead_forms" 
					AND R.active = 1 
					AND R.sched > 0 
					AND R.trashed = "0000-00-00 00:00:00" 
					AND (l.user_id = R.user_id or (l.parent_id = R.user_id AND R.master_ID = R.id))
					AND l.id = :lead_id';
		if($limit)
				$q	.=	' LIMIT ' . (int)$limit;
		
		$stmt	=	$this->prepare($q);
		$stmt->execute(array('lead_id' => $lead_id));
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'ResponderModel');
	}

	/**
	 * @param $responder_id
	 *
	 * @return ResponderModel
	 */
	public function getResponderById($responder_id){
		$statement = $this->prepare('SELECT * FROM auto_responders WHERE id = :id');
		$statement->execute(array('id' => $responder_id));

		return $statement->fetchObject('ResponderModel');
	}
}
