<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubDataAccess
 *
 * @author amado
 */
class BlogDataAccess extends QubeDataAccess {

	const STATSTBL = 'piwikreports';

	/**
	 * Set master_ID = 0, insert into master posts table.
	 * Perform duplication.
	 * 
	 * @staticvar null $stmt
	 * @param BlogPostModel $post
	 * @param type $feed_ID
	 */
	function addPostToFeed(BlogPostModel $post, $feed_ID) {
		static $stmt = NULL;
		$pdo = $this->getQube()->GetPDO('master');
		if (!$stmt) {
			$stmt = $pdo->prepare('INSERT IGNORE INTO blogfeed_masterposts SET feed_ID = :feed_ID, master_post_ID = :post_ID');
		}
		// insert into masterposts library
		if ($stmt->execute(array('post_ID' => $post->getID(), 'feed_ID' => $feed_ID)) && $stmt->rowCount() > 0) {
			$query = 'select blog_ID FROM blogfeed_subs WHERE feed_ID = ' . $feed_ID;
			$sstmt = $pdo->query($query);

			// add a copy of the post to each of the subscriber blogs
			while ($subscriber_blog_ID = $sstmt->fetchColumn()) {
				$this->copyBlogPost($post->getID(), $subscriber_blog_ID, $pdo);
				//$pdo->query('UPDATE blog_post SET master_ID = ' . $post->getID() . ', blog_id = ' . $subscriber_blog_ID . ' WHERE id = ' . $pdo->lastInsertId());
			}

//			if ($post->master_ID) {
//				if ($pdo->query('UPDATE blog_post SET master_ID = 0 WHERE id = ' . $post->getID())->rowCount()) {
//					$post->set('master_ID', 0);
//				}
//			}
			return true;
		}
		return false;
	}

	function isMasterPost($post_ID) {
		$m = $this->getQube()->GetPDO('master');
		return $m->query('SELECT 1 FROM blogfeed_masterposts WHERE master_post_ID=' . $post_ID)->fetchColumn();
	}

	/**
	 * Set the Feed subscriptions for a particular Blog.
	 * 
	 * @staticvar null $stmt
	 * @param type $subscriber_ID
	 * @param array $feed_IDs
	 */
	function setBlogSubscriptions($subscriber_ID, array $feed_IDs) {
		$pdo = $this->getQube()->GetPDO('master');
			
		$pdo->beginTransaction();

                //
                $pdo->query('DELETE FROM blogfeed_subs WHERE blog_ID = ' . $subscriber_ID . ' AND feed_ID NOT IN (' . join(', ', $feed_IDs) . ')');
                
                foreach($feed_IDs as $feed_ID){
                    $this->addBlogSubscription($subscriber_ID, $feed_ID);
                }                
		//
				
		$pdo->commit();

	}
    
	function copyFeedPostImagesToNewBlog($feed_ID, $blog_ID)
	{
		$q	=	'SELECT user_id FROM blogs WHERE id = ' . (int)$blog_ID;
		$slave_blog_user_id	=	$this->query($q)->fetchColumn();
		
		$slave_blog_storage	=	self::createBlogPostUserStorage($slave_blog_user_id);
		
		$q	=	'select user_id, photo from v_masterblogposts WHERE feed_ID = ' . (int)$feed_ID . ' and user_id != 0 AND photo != ""';
		$stmt	=	$this->query($q);
		
		while($postinfo	=	$stmt->fetchObject())
		{
			$masterpost_user_storage	=	BlogPostModel::getPhotoStorage($postinfo->user_id);
			FileUtil::CreateSymlink($masterpost_user_storage . DIRECTORY_SEPARATOR . $postinfo->photo, $slave_blog_storage . DIRECTORY_SEPARATOR . $postinfo->photo);
		}

		//This will trigger rsync
		$stmt = $this->prepare('UPDATE users SET last_upload = NOW() WHERE id = :id');
		$stmt->execute(array('id' => $slave_blog_user_id));

		return true;
	}
	
        function addBlogSubscription($blog_ID, $feed_ID)
        {
            $pdo    =   $this->getPDO();
            
            $pdo->query('INSERT IGNORE INTO blogfeed_subs (blog_ID, feed_ID) VALUES (' . (int)$blog_ID . ', ' . (int) $feed_ID . ')');
            $this->copyFeedPostsToBlog($feed_ID, $blog_ID, $pdo);
		$this->copyFeedPostImagesToNewBlog($feed_ID, $blog_ID);
			
			
        }
        
		/**
		 * Removes slave posts
		 * 
		 * @param int $blog_ID
		 * @param int $feed_ID
		 * @throws Exception
		 */
        function removeBlogSubscription($blog_ID, $feed_ID)
        {
            $pdo = $this->getPDO();
            if ($blog_ID == 0) throw new Exception();
			
            $query = 'DELETE s FROM blogfeed_masterposts as i
                        inner join blog_post s on (s.master_ID = i.master_post_ID)
                        inner join blog_post m on (m.id = i.master_post_ID)
                    where 
                        m.category = s.category 
                        and m.post_title = s.post_title 
                        and m.post_content = s.post_content 
                        and m.photo = s.photo
                        and m.tags = s.tags
                        and m.tags_url = s.tags_url
                        and m.post_desc = s.post_desc
                        and m.seo_title = s.seo_title 
                        AND i.feed_ID = ' . (int) $feed_ID . ' AND s.blog_ID = ' . (int) $blog_ID;

            //Uncomment the following 2 lines to enable delete slave posts
            $stmt = $pdo->query($query);
            $count = $stmt->rowCount();

            $stmt = $pdo->query('DELETE FROM blogfeed_subs WHERE blog_ID = ' . (int)$blog_ID . ' AND feed_ID = ' . (int) $feed_ID . '');
			return $stmt->rowCount();
			
			$q	=	'DELETE FROM blog_post WHERE blog_ID = ' . (int)$blog_ID . ' AND master_ID IN (SELECT master_post_ID FROM blogfeed_masterposts WHERE feed_ID = ' . (int)$feed_ID . ')';
			
			$pdo->query($q);
        }

	function copyFeedPostsToBlog($feed_ID, $subscriber_ID, $pdo) {

		// setup posts from feeds : copy master posts
		$pdo->query('INSERT IGNORE INTO blog_post (
type,
blog_id,
tracking_id,
user_id,
master_ID,
user_parent_id,
category,
category_url,
author,
author_url,
post_title,
post_title_url,
post_content,
photo,
tags,
tags_url,
post_desc,
seo_title,
views,
network,
publish_date,
trashed,
timestamp,
created)
						SELECT 
type,
' . $subscriber_ID . ',
tracking_id,
(select user_id from blogs b where b.id = ' . $subscriber_ID . '),
id,	-- "my" id (as the master)
user_parent_id,
category,
category_url,
author,
author_url,
post_title,
post_title_url,
post_content,
photo,
tags,
tags_url,
post_desc,
seo_title,
views,
network,
publish_date,
trashed,
timestamp,
created
FROM blog_post p where p.id in ( select master_post_ID FROM blogfeed_masterposts m WHERE m.feed_ID = ' . $feed_ID . ')');
	}

	/**
	 * Replicates a post
	 *
	 * @param int $copy_ID The id of the post to replicate
	 * @param int $receiver_blog_id The blog where to copy the post
	 * @param QubeLogPDO $pdo
	 */
	function copyBlogPost($copy_ID, $receiver_blog_id, $pdo) {
		$pdo->query("INSERT INTO blog_post (
type,
blog_id,
tracking_id,
user_id,
master_ID,
user_parent_id,
category,
category_url,
author,
author_url,
post_title,
post_title_url,
post_content,
photo,
tags,
tags_url,
post_desc,
seo_title,
views,
network,
publish_date,
trashed,
timestamp,
created) SELECT
type,
$receiver_blog_id,
tracking_id,
( SELECT user_id FROM blogs b where b.id = " . $receiver_blog_id . '),
id,	-- the id of the master
user_parent_id,
category,
category_url,
author,
author_url,
post_title,
post_title_url,
post_content,
photo,
tags,
tags_url,
post_desc,
seo_title,
views,
network,
publish_date,
trashed,
timestamp,
created FROM blog_post WHERE id = ' . $copy_ID);
	}

	/**
	 * "feed posts" and "master posts" are synonymous
	 * 
	 * @param int $user_ID
	 * @param bool|int $limit
	 * @return BlogPostModel[]
	 */
	function getUserMasterPosts($user_ID, $limit	=	false)
	{
		$q	=	'select p.id, p.post_title, F.name as feed_name, p.created as publish_date from blog_post p'
                        . '         left join blogfeed_masterposts m on (m.master_post_ID = p.id) '
                        . '         left join blog_feedsmu as F on F.ID = m.feed_ID'
                        . ' WHERE p.user_ID = ' . (int)$user_ID . ' AND (NOT ISNULL(F.id) or p.master_ID = p.id) ' . ($limit ? ' LIMIT ' . $limit : '');
#		echo ($q);
		$stmt	=	$this->getPDO()->query($q);
		
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}
	
	function getPosts(BlogModel $B, $params = array()) {
		$query = 'select SQL_CALC_FOUND_ROWS  p.*, ifnull(f.name, "None") as feed from blog_post p left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
	left join blogfeed_subs s on (s.feed_ID = m.feed_ID)
	left join blog_feedsmu f ON m.feed_ID = f.ID
	where p.blog_ID = :blog_ID and (isnull(s.blog_ID) or s.blog_ID = :blog_ID) AND p.trashed = 0 AND p.master_id != p.id
	ORDER BY ' . (isset($params['order']) ? $params['order'] : 'id ASC') .
			(isset($params['limit']) ? ' Limit ' . $params['limit'] : '');

		$stmt = $this->prepare($query);
		$stmt->execute(array('blog_ID' => $B->getID()));
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}


	function getPostsWeb(BlogModel $B, $params = array())
	{
		$query = 'select SQL_CALC_FOUND_ROWS  p.*, ifnull(f.name, "None") as feed from blog_post p left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
	left join blogfeed_subs s on (s.feed_ID = m.feed_ID)
	left join blog_feedsmu f ON m.feed_ID = f.ID
	where p.blog_ID = :blog_ID and (isnull(s.blog_ID) or s.blog_ID = :blog_ID) AND p.trashed = 0 AND p.master_id != p.id
	AND ( p.publish_date = 0 OR p.publish_date <= "' . date('Y-m-d') . '" )
	ORDER BY ' . (isset($params['order']) ? $params['order'] : 'id ASC') .
			(isset($params['limit']) ? ' Limit ' . $params['limit'] : '') .
			(isset($params['offset']) ? ' OFFSET ' . $params['offset'] : '');

		$stmt = $this->prepare($query);
		$stmt->execute(array('blog_ID' => $B->getID()));
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}
	
	function getConnectedPostsWeb($connectedID)
	{
        $query	=	'SELECT p.*, b.domain FROM blog_post p, blogs b WHERE p.user_id = 7 AND p.blog_id = b.id AND p.profile_connect = "'.$connectedID.'"';

        return $this->query($query)->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
    }
	
	/**
	 * Accepts a category_url parameter
	 * 
	 * @param BlogModel $B
	 * @param array $params
	 * @return BlogPostModel[]
	 */
	function getCategoryPosts(BlogModel $B, $params = array())
	{
		$query = 'select SQL_CALC_FOUND_ROWS  p.* from blog_post p left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
	left join blogfeed_subs s on (s.feed_ID = m.feed_ID)
	where p.blog_ID = :blog_ID and (isnull(s.blog_ID) or s.blog_ID = :blog_ID) 
		AND p.trashed = 0 
		AND p.category_url = :category_url
		AND ( p.publish_date = 0 OR p.publish_date <= "' . date('Y-m-d') . '" )
		ORDER BY ' . (isset($params['order']) ? $params['order'] : 'id ASC') .
			(isset($params['limit']) ? ' Limit ' . $params['limit'] : '');

		unset($params['limit'], $params['order']);
		
		$params['blog_ID']	=	$B->getID();
		$stmt = $this->prepare($query);
		
		$stmt->execute($params);
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}
	
	function getBlogCategories(BlogModel $B, $params = array())
	{
		$query	=	'SELECT distinct category, category_url  FROM `blog_post`
			WHERE trashed = 0 AND `blog_id` = ' . $B->getID().'
			ORDER BY ' . (isset($params['order']) ? $params['order'] : 'id ASC');

			unset($params['order']);

		return $this->query($query)->fetchAll(PDO::FETCH_KEY_PAIR);
	}

	function getBlogCategoriesWeb(BlogModel $B, $params = array())
	{
		$query	=	'SELECT distinct category, category_url  FROM `blog_post`
			WHERE trashed = 0 AND `blog_id` = ' . $B->getID().'
			AND ( publish_date = 0 OR publish_date <= "' . date('Y-m-d') . '" )
			ORDER BY ' . (isset($params['order']) ? $params['order'] : 'id ASC');

		unset($params['order']);

		return $this->query($query)->fetchAll(PDO::FETCH_KEY_PAIR);
	}

    function getMPCategories($userID,$feedID = null)
    {
        $query	=	'SELECT distinct category, category_url  FROM blog_post a LEFT JOIN blogfeed_masterposts b on (a.id = b.master_post_ID) WHERE a.trashed = 0 AND a.id = a.master_ID AND a.user_id = ' . $userID . ($feedID ? " AND b.feed_ID = " . $feedID : '');

        return $this->query($query)->fetchAll(PDO::FETCH_KEY_PAIR);
    }

	static function createBlogPostUserStorage($user_id)
	{
		// create the user blog photo storage if it does not exist..
		$user_storage	= BlogPostModel::getPhotoStorage($user_id);
		FileUtil::CreatePath($user_storage);
		return $user_storage;
	}
	
	
	function updateChildPosts(array $postdata, BlogPostModel $master) {
		$propagate = array('publish_date', 'post_title_url', 'category_url', 'post_title', 'post_content', 
			'tags', 'category', 'photo', 'post_desc', 'seo_title', 'tags_url', 'network');

		$updatingkeys = array_intersect(array_keys($postdata), $propagate);

		if (!$updatingkeys)
			return 0;

//		$cols	=	array();
		$vals = array();

		$pdo = $this->getQube()->GetPDO('master');

		$master_ID = $master->getID();
		// update 1 c at a time..		
		foreach ($updatingkeys as $key) {
			if($key	==	'photo')
			{
				// create simlinks to photos!
				$rest1	=	$pdo->query('SELECT DISTINCT user_id FROM blog_post WHERE user_id != 0 AND master_ID = ' . $master_ID);
				while($user_id	=	$rest1->fetchColumn())
				{
					$master_storage	= BlogPostModel::getPhotoStorage($master->user_id, false);
					$slave_storage	=	self::createBlogPostUserStorage($user_id);
					FileUtil::CreateSymlink($master_storage . DIRECTORY_SEPARATOR . $postdata[$key], $slave_storage . DIRECTORY_SEPARATOR . $postdata[$key]);
				}
			}
			$vals = array();
			$cols = "`$key` = :$key";
			$vals[$key] = $postdata[$key];
			$vals['prevvalue'] = $master->get($key);//	deb($vals);
			$q = "UPDATE blog_post SET %s WHERE `$key` = :prevvalue AND `master_ID` = $master_ID";
			$query = sprintf($q, $cols);

			$stmt = $pdo->prepare($query);
			$stmt->execute($vals);
		}

		return TRUE;
	}

	function doSaveBlog(array $data, $ID = 0, DashboardBaseController $ctrl) {
		$Validation = new ValidationStack;
		$HM = new BlogModel();
		$result = new IOResultInfo(); //array('error' => false);
		if (!$ID) {
			// initialize some shit for a new object
			// initialize some stuff
			if (!isset($data['name'])) {
				$data['name'] = 'New Blog';
			}
			if (!$data['user_ID'])
				$data['user_ID'] = $ctrl->getDataOwnerID();
		} else {
			$data['ID'] = $ID;
			unset($data['user_id']);		// security
		}

		if ($HM->SaveData($data, NULL, $Validation, NULL, DBWhereExpression::Create('id = %d AND user_id = dashboard_userid()', $ID), $result)) {

			$result['object'] = $HM;
			$result['message'] = 'BLog Saved.';
//            $result['row'] = $this->getController();
			return $result;
		}
		// @todo test uploads
		return $Validation;
	}
	
	function getPostsf(BlogModel $B, $params	=	array())
	{		
		extract($params);
		$query	=   "SELECT 
						id, 
						blog_id, 
						tracking_id, 
						user_id, 
						category, 
						category_url,
						author,
						author_url,
						post_title, 
						post_title_url, 
						post_content, 
						photo, 
						tags, 
						tags_url, 
						network, created,
						if(publish_date=0, created, publish_date) as publish_date_str
                            FROM blogfeed_subs s right join blog_post p on (p.feed_id = s.feed_ID) 
                            where (s.blog_ID = :blog_ID or p.blog_id = :blog_ID)
                                    AND category != '' 
					  AND post_title != '' 
					  AND post_content != '' 
					  AND tags != '' 
					  AND blog_id != 0
					  AND p.trashed = '0000-00-00' 
                                        AND ( p.publish_date = 0 OR p.publish_date <= '" . date('Y-m-d') . "' )";
		if(!$blog_disp) $query .= " AND tags != '' ";
		$query .= " AND blog_id != 0
				  AND blog_post.trashed = '0000-00-00' AND ( blog_post.publish_date = 0 OR blog_post.publish_date <= DATE(NOW()) ) ";
		if(!$blog_disp) $query .= " AND network = 1";
		if($city) $query .= " AND (SELECT blogs.city FROM blogs WHERE id = blog_post.blog_id) = '".$city."'";
		if($state) $query .= " AND (SELECT blogs.state FROM blogs WHERE id = blog_post.blog_id) = '".$state."'";
		if($category) $query .= " AND (SELECT blogs.category FROM blogs WHERE id = blog_post.blog_id) LIKE '%".$category."'";
		if($blog_id) $query .= " AND blog_id = ".$blog_id;
		if($user_id) $query .= " AND user_id = ".$user_id;
		if($cid) $query .= " AND blog_post.blog_id IN (SELECT id FROM blogs WHERE cid = ".$cid.")";
		if($parent && is_numeric($parent)) $query .= " AND (SELECT parent_id FROM users WHERE id = blog_post.user_id) = ".$parent;
                
                        
		$pdo	=	$this->getQube()->GetPDO();
		$stmt	=	$pdo->prepare($query);
		$stmt->execute(array('blog_ID' => $B->getID()));
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
        
        function clearFromFeeds($post_ID)
        {
            $q  =   'DELETE FROM blogfeed_masterposts where master_post_ID = ' . $post_ID;
            $this->getPDO()->query($q);
        }
	
	function getFeeds($args    =   array(), $whereQ = ''){

		$QOpts = self::getQueryOptions($args);
		$cols = '*';
		$orderCol = 'created';
		$orderDir = 'ASC';
		$newRow = "";
		if($QOpts['cols']){
			$cols = $QOpts['cols'];
		}

		if(isset($QOpts['orderCol'])){
			switch($QOpts['orderCol']){
				case 'name':
				case 'created':
					$orderCol = $QOpts['orderCol'];
					break;
			}
		}

		if(isset($QOpts['orderDir'])){
			$orderDir = $QOpts['orderDir'];
		}
		$where = array(
			'trashed = 0',
			'user_ID = :user_ID'
		);

		if($args['ID'])
			$where[] = 'ID = :ID';

		if($args['sSearch']) {
			$where[] = 'name LIKE :sSearch';
		}

		if($QOpts['new_rows_ids']){
			$newRow = "id = " . join(' DESC, id = ' , $QOpts['new_rows_ids']) . " DESC, ";
		}

		if($whereQ != ''){
			$where[] = "($whereQ)";
		}

		$where = 'WHERE ' . join(' AND ', $where);

		$query	=	"SELECT {$QOpts['CALC_FOUND_ROWS']} $cols FROM blog_feedsmu $where ORDER BY $newRow $orderCol $orderDir {$QOpts['limitstr']}" ;

		$stmt	=	$this->prepare($query);

		self::callPreparedQuery($stmt, $args);

		if($QOpts['getStatement']){
			return $stmt;
		}

		if(!$args['ID'])
			return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogFeedModel');
		return $stmt->fetchObject('BlogFeedModel');
	}
        
//        static function getOwnMasterPosts(){
//
//			$query = 'select SQL_CALC_FOUND_ROWS p.id, p.post_title, F.name, p.created as publish_date  from blog_post p left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
//					inner join blog_feedsmu as F on
//					F.ID = m.feed_ID WHERE p.id = p.master_ID';
//			return $query;
//        }

	function getOwnMasterPosts(array $args = array()){
		$qOpts	=	self::getQueryOptions($args);

		$where = array(
			'p.id = p.master_ID',
            "p.trashed = '0000-00-00'"
		);
        if(isset($args['feed_ID'])){
            $where[] = '(m.feed_ID = :feed_ID)';
        }
        if(isset($args['category']) && $args['category'] != ''){
            $where[] = '(p.category_url = :category)';
        }else{
            unset($args['category']);
        }
		$newRow = '';
		$orderDir = 'ASC';
		$orderCol = 'p.post_title';
		$cols = 'p.id, p.post_title, F.name, p.created as publish_date, p.user_id, p.blog_id, p.category';
		$limit = '';

		if(isset($args['sSearch'])){
			$where[] = '(p.post_title LIKE :sSearch OR F.name LIKE :sSearch)';
		}

		if(!$this->getActionUser()->isSystem()){
			$where[] = 'p.user_id = :user_id';
			$args['user_id'] = $this->getActionUser()->getID();
		}

		if(isset($args['limit_start']) && isset($args['limit'])){
			$limit = "LIMIT :limit_start , :limit";
		}


		if($qOpts['orderDir'])
			$orderDir = $qOpts['orderDir'];

		if($qOpts['orderCol'])
			$orderCol = $qOpts['orderCol'];

		if($qOpts['cols'])
			$cols = $qOpts['cols'];

		if($qOpts['new_rows_ids']){
			$newRow = "p.id = " . join(' desc, p.id = ', $qOpts['new_rows_ids']). " desc, ";
		}

		$where = join(' AND ', $where);
		$query = "SELECT {$qOpts['CALC_FOUND_ROWS']} $cols FROM " .
			"blog_post p " .
				"LEFT JOIN " .
			"blogfeed_masterposts m ON (m.master_post_ID = p.master_ID) " .
				"INNER JOIN " .
			"blog_feedsmu AS F ON F.ID = m.feed_ID " .
			"WHERE $where " .
				"ORDER BY $newRow $orderCol $orderDir " .
			$limit;

		$stmt = $this->prepare($query);
		self::callPreparedQuery($stmt, $args);
		if(isset($qOpts['getStatement']) && $qOpts['getStatement']){
			return $stmt;
		}
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}

	function getBlogFeeds($where = null){
		$params = array('user_ID' => $this->user_ID);
		if($where){
			$params['ID'] = $where;
		}
		$statement = $this->getQube()->GetPDO()->prepare('SELECT ID, name from blog_feedsmu where user_ID = :user_ID' . ($where ? ' AND ID = :ID' : '') );
		$statement->execute($params);
		if($where){
			return $statement->fetchObject('BlogFeedModel');
		}
		return $statement->fetchAll(PDO::FETCH_CLASS, 'BlogFeedModel');
	}

	function getResellerBlogFeeds($userID){

	}
	
	/**
	 * 
	 * @param int $post_id
	 * @param type $post_rows
	 * @return BlogPostModel
	 */
	function getSinglePost($post_id, &$post_rows)
	{
		if(!is_numeric($post_id)) $post_id = 0;
		$pdo	=	$this->getPDO('slave');
		
		$query = "SELECT blog_post.*,"
						. "if(publish_date=0, created, publish_date) as created"
						. " FROM blog_post WHERE id = ".(int)$post_id.""
						. " AND trashed = '0000-00-00'";
		
		$stmt = $pdo->query($query);
		$post_rows	=	$stmt->rowCount();
		
		return $stmt->fetchObject('BlogPostModel');
	}

	/**
	 * 
	 * @param type $blog_id
	 * @param type $limit
	 * @return \BlogPostModel[]
	 * @param type $blog_id
	 * @param type $limit
	 * @return \BlogPostModel[]
	 * @return BlogPostModel[]
	 */
	function getLatestPosts($blog_id, $limit)
	{
		$query = 'SELECT blog_post.*, httphostkey FROM blog_post INNER JOIN blogs
					  ON blog_id = blogs.id AND blog_post.trashed = 0 
					  where blogs.id = :blog_id AND blog_post.publish_date <= :date ORDER BY IF(blog_post.publish_date = 0, blog_post.created, blog_post.publish_date) DESC LIMIT :limit';

		$stmt	=	$this->prepare($query);
		$stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
		$stmt->bindParam(':blog_id', $blog_id, PDO::PARAM_INT);
		$stmt->bindParam(':date', date(PointsSetBuilder::MYSQLFORMAT), PDO::PARAM_STR);
		$stmt->execute();
		
		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}
	
	/**
	 * Return the total number of posts belonging to the reseller.
	 * 
	 * @param type $reseller_ID
	 */
	function getResellerPostCount($reseller_ID)			
	{
		$q	=	'SELECT count(*) FROM blog_post WHERE user_parent_id	=	' . (int)$reseller_ID;
		return $this->query($q)->fetchColumn();
	}
	
	/**
	 * @param $lead_id
	 * @param $limit
	 * @return BlogPostModel[]
	 */
	public function getLatestPostsByLead($lead_id, $limit){
		$query = 'SELECT blog_post.*, httphostkey FROM blog_post INNER JOIN blogs
					  ON blog_id = blogs.id AND blog_post.trashed = 0 INNER JOIN leads
					  ON leads.hub_id = blogs.connect_hub_id AND leads.trashed = 0
					  where leads.id = :lead_id ORDER BY IF(blog_post.publish_date = 0, blog_post.created, blog_post.publish_date) DESC LIMIT :limit';

		$pdo = $this->getPDO('slave');

		$statement = $pdo->prepare($query);
		$statement->bindValue('lead_id', intval($lead_id), PDO::PARAM_INT);
		$statement->bindValue('limit', intval($limit), PDO::PARAM_INT);

		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_CLASS, 'BlogPostModel');
	}
	
	/**
	 * returns affected rows
	 * 
	 * @param int $post_ID
	 * @return int
	 */
	public function DeletePost($post_ID)
	{
		$stmt	=	$this->query('DELETE FROM blog_post WHERE id = ' . $post_ID . ' LIMIT 1');
		return $stmt->rowCount();
	}

	public function getDatatablePosts(BlogModel $blog, VarContainer $dataTableParams, ThemeView $rowTemplate){

		$query = $this->getQube()->queryObjects('BlogPostModel', 'T', 'T.blog_id = %d AND T.trashed=0', $blog->getID())->calc_found_rows()
			->leftJoin('blogfeed_masterposts m', 'T.master_id = m.master_post_ID')
			->leftJoin('blog_feedsmu f', 'm.feed_ID = f.ID')
			->addFields('IFNULL(f.name, "None") as feed');
		$args = array();

		if($dataTableParams->checkValue('category', $category) && $category != ''){
			$query->Where('category = :category');
			$args['category'] = $category;
		}

		if($dataTableParams->checkValue('sSearch', $search) && $search != ''){
			$where = '(post_title like CONCAT(CONCAT(\'%\',:search),\'%\') OR T.id = :search';
			if(!isset($category) || $category == '')
				$where .= ' OR category like CONCAT(CONCAT(\'%\',:search),\'%\')';
			$where .= ')';
			$query->Where($where);
			$args['search'] = $search;
		}


		$dataTableParams->load(array('table' => 'blog-posts'));
		$DT = new DataTableResult($query, $dataTableParams);
		$DT->setRowCompiler($rowTemplate, 'post');
		return $DT->getArray($args);
	}
	
	function findPostByURLInfo(BlogModel $info, $category_post_path)
	{
		$pathinfo	=	explode('/', $category_post_path);
		if(count($pathinfo) != 2) return false;
		
		list($category_url, $post_url)	=	$pathinfo;
		
		
		$pdo	=	$this->getPDO('slave');
		
		$query = "SELECT blog_post.*,"
						. "if(publish_date=0, created, publish_date) as created"
						. " FROM blog_post WHERE blog_id = :blog_id AND category_url = :category_url AND post_title_url = :post_url
							AND trashed = '0000-00-00' AND (publish_date = 0 OR publish_date <= CURDATE()) LIMIT 1";
		
		$stmt = $pdo->prepare($query);
		
		$stmt->execute(array('category_url' => $category_url, 'post_url' => $post_url, 'blog_id' => $blog->getID()));
		
		return $stmt->fetchObject('BlogPostModel');
	}

	public function getFeedsWithSubscription($user_id, $blog_id){
		$q = "SELECT blog_feedsmu.*, if(blogfeed_subs.blog_ID IS NULL, 0, 1) AS subscribed " .
  				"FROM blog_feedsmu LEFT JOIN blogfeed_subs ON blog_feedsmu.ID = blogfeed_subs.feed_ID AND blogfeed_subs.blog_ID = :blog_ID  WHERE user_ID = :user_id";
		$stmn = $this->prepare($q);
		$stmn->execute(array('blog_ID' => $blog_id, 'user_id' => $user_id));
		return $stmn->fetchAll(PDO::FETCH_CLASS, 'BlogFeedModel');
	}

	public function getAllPostTagsByBlogId(BlogModel $blogModel){
		$q = "SELECT DISTINCT lower(trim(tags)) tags FROM blog_post
			LEFT JOIN blogs ON blogs.id = blog_post.blog_id
			WHERE blogs.id = :blog_id AND tags != ''";
		$stmt = $this->prepare($q);
		$stmt->execute(array('blog_id' => $blogModel->getID()));
		$tags = $stmt->fetchAll(PDO::FETCH_COLUMN);
		$finalTags = array();
		foreach($tags as $tag){
			$t = explode(',', $tag);
			foreach($t as $uniqueTag){
				if(!in_array(strtolower(trim($uniqueTag)), $finalTags)){
					$finalTags[] = strtolower(trim($uniqueTag));
				}
			}
		}
		return $finalTags;
	}

	/**
	 * @param $blogID
	 * @return BlogModel
	 */
	public function getBlog($blogID){
		$actionUser = $this->getActionUser();
		$query = "SELECT *, CONCAT_WS(',', keyword1, keyword2, keyword3, keyword4, keyword5, keyword6) as keywords FROM blogs";
		$where = array('id = :id');
		$args = array('id' => $blogID);
		if(!$actionUser->isSystem()){
			if($actionUser->can('view_BLOG', $blogID)){
				$where[] = 'user_parent_id = :parent_id';
				$args['parent_id'] = $actionUser->getParentID();
			}else{
				$where[] = 'user_id = :user_id';
				$args['user_id'] = $actionUser->getID();
			}
		}

		if(!empty($where)){
			$query .= ' WHERE  ' . join(' AND ', $where);
		}
		$stmt = $this->prepare($query);

		$stmt->execute($args);

		return $stmt->fetchObject('BlogModel');
	}

	/**
	 * @param int $campaignID
	 * @param array $options
	 * @return BlogModel[]
	 */
	public function getBlogs($campaignID, $options = array()){
		$actionUser = $this->getActionUser();
		$where = '';
		$orderBy = '';
		$whereParts = array('cid = :cid');
		$args = array('cid' => $campaignID);
		if(!$actionUser->isSystem()){
			if($actionUser->can('view_campaigns', $campaignID)){
				$whereParts[] = 'user_parent_id = :parent_id';
				$args['parent_id'] = $actionUser->getParentID();
			}else{
				$whereParts[] = 'user_id = :user_id';
				$args['user_id'] = $actionUser->getID();
			}
		}
		if(!empty($whereParts)){
			$where = 'WHERE  ' . join(' AND ', $whereParts);
		}

		if($options['orderBy'] && !empty($options['orderBy'])){
			$orderBy = 'ORDER BY ' . join(', ', $options['orderBy']) . ' DESC';
		}

		if($options['bindParams'] && !empty($options['bindParams'])){
			$args = array_merge($args, $options['bindParams']);
		}

		$query = "SELECT * FROM blogs $where $orderBy";

		$stmt = $this->prepare($query);

		$stmt->execute($args);

		return $stmt->fetchAll(PDO::FETCH_CLASS, 'BlogModel');
	}

	function connectBlogToWebsite($blog_id, $hub_id){
		$this->beginTransaction();
		// If it's connecting to website
		if($hub_id){
			$stmt = $this->prepare(
				'UPDATE blogs INNER JOIN hub ON hub.id = :hub_id SET ' .
					'blogs.website_url = hub.domain, ' .
					'blogs.company_name = hub.company_name, ' .
					'blogs.phone = hub.phone, ' .
					'blogs.address = hub.street, ' .
					'blogs.city = hub.city, ' .
					'blogs.state = hub.state, ' .
					'blogs.category = hub.category, ' .
					'blogs.logo = getImageUrl(hub.user_id, hub.logo), ' .
					'blogs.bg_img = getImageUrl(hub.user_id, hub.bg_img), ' .
					'blogs.bg_color = hub.bg_color, ' .
					'blogs.links_color = hub.links_color, ' .
					'blogs.theme = hub.theme, ' .
					'blogs.adv_css = hub.adv_css, ' .
					'blogs.slogan = hub.slogan ' .
				'WHERE blogs.id = :blog_id');

			if($stmt->execute(array('hub_id' => $hub_id, 'blog_id' => $blog_id))){

				$stmt = $this->prepare(
					'UPDATE hub INNER JOIN blogs ON blogs.id = :blog_id SET ' .
						'hub.connect_blog_id = blogs.id, ' .
						'hub.blog = blogs.domain ' .
					'WHERE hub.id = :hub_id');

				if($stmt->execute(array('hub_id' => $hub_id, 'blog_id' => $blog_id)))
					return $this->commit();

				else{
					$this->rollBack();
					return false;
				}
			}else{
				$this->rollBack();
				return false;
			}
		}else{
			//Disconnecting website
			$stmt = $this->prepare('UPDATE hub SET connect_blog_id = 0 WHERE id = (SELECT connect_hub_id FROM blogs WHERE id = :blog_id)');

			if($stmt->execute(array('blog_id' => $blog_id)))
				return $this->commit();
			else{
				$this->rollBack();
				return false;
			}
		}

	}

	static function findSiteTheme(BlogModel $blog, array $blogthemes)
	{
		if($blog->theme != 0 && count($blogthemes))
		{
			foreach($blogthemes as $blogtheme){
				if($blogtheme->is_selected)
					return $blogtheme;
			}
		}
		return new HubThemeModel(array('is_selected' => 0, 'name' => 'None', 'id' => $blog->theme, 'type' => 0, 'thumbnail' => ''));
	}

	/**
	 * @param $post_ID
	 * @return BlogPostModel
	 */
	public function getPost($post_ID){
		$where = '';
		if(!$this->getActionUser()->isSystem()){
			$where = 'user_id = %d';
			if($this->getActionUser()->can('view_BLOG'))
				$where .= ' OR EXISTS (SELECT 1 FROM blogs WHERE id = T.blog_id AND user_parent_id = %d)';

		}
		if(!empty($where)){
			$where = "AND ($where)";
		}
		return $this->qube->queryObjects('BlogPostModel')->Where("id = %d $where", $post_ID, $this->getActionUser()->getID(), $this->getActionUser()->getParentID())->Exec()->fetch();
	}
}
