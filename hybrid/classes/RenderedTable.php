<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of RenderedTable
 *
 * @author amado
 */

class RenderedTable {
    protected $rows	=	array();
    protected $defaultrow   =   array();
		
    function __construct($rows) {
        if($rows)
            $this->rows =   $rows;
    }
    
    function setDefaultRow($row){
        $this->defaultrow   =   $row;
    }
    
    function getRow($id, &$isdefault){
        
        if(isset($this->rows[$id]))
        {
            $isdefault  =   false;
            return $this->rows[$id];
        }
        
        $isdefault  =   true;
        return $this->defaultrow;
    }
		
		function getColumnTitles()
		{
			return array();
		}
		
		function buildRow($row, $datarow)
		{
			return (object) array_merge((array) $row, (array) $datarow);
		}
		
		function loadCachedRows(Template $View, $rowFilename, $objects, $objIDAttr, $objname = 'row')
		{
			$vars	=	array();
			$vars['statsTable']	=	$this;
			foreach($objects as $objN)
			{
				$isdefault	=	false;
				$dataRow	=	$this->getRow($objN->$objIDAttr, $isdefault);
				$vars[$objname]	=	$this->buildRow($objN, $dataRow);
				$vars['isdefault']	=	$isdefault;
				$View->getRow($rowFilename, $vars);
			}
		}
}
