<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LeadFormProcessor
 *
 * @author Amado
 */
class LeadFormProcessor extends QubeDataProcessor {

	/**
	 * @var LeadsDAO
	 */
	protected $DA = NULL;

	/**
	 * @var LeadFormModel
	 */
	protected $LeadForm = NULL;


	function setForm(LeadFormModel $form)
	{
		$this->LeadForm	=	$form;
		return $this;
	}

	function getForm()
	{
		return $this->LeadForm;
	}

	function setDA(LeadsDAO $DA)
	{
		$this->DA = $DA;
	}

	function getDA()
	{
		if(!$this->DA)
		{
			$this->setDA(LeadsDAO::create($this));
		}
		return $this->DA;
	}

	function __construct(Qube $q = NULL, $logmode = 0)
	{
		if(Qube::IS_DEV()) $logmode = $logmode | Logger::DOPRINT;
		parent::__construct($q, $logmode);
	}
	//put your code here

	static function ValidatePOST(VarContainer $V) {
		$Validation = new ValidationStack();

		foreach (array('formID', 'userID', 'hubID') as $required_num) {
			if (!$V->check('is_numeric', $required_num))
				$Validation->addError($required_num, 'Is not a number.');
		}

		return $Validation;
	}

	/**
	 * @deprecated
	 * @param VarContainer $values
	 * @param array $fieldslegacy
	 * @return ValidationStack
	 */
	static function ValidateFieldValues(VarContainer $values, array $fieldslegacy) {
		$VS = new ValidationStack();
		foreach ($fieldslegacy as $fk => $fdata) {
			$value = null;
			if (!$fdata['required'])
				continue;
			$postkey = Hub::fieldNameFromLabelStatic($fdata['label']);
			if (!$values->checkValue($postkey, $value) || trim($value) == '') {
				// if checkValue returns false, or $value is a whitespace string
				$VS->addError($postkey, "{$fdata['label']} is Required.");
			}
		}
		return $VS;
	}

	static function IsSpam($spam_points)
	{
		return $spam_points >= 50;
	}

	function collectLeadInputs(VarContainer $varContainer)
	{
		$form = $this->getForm();
		$inputs	=	new CollectedLeadInput();

		$inputs->setFormFields($form->getFields());
		$inputs->collectData($varContainer);

//		if($form->isLegacyForm())
//		{
//			// blah blah blah
//			return NULL;
//		}
		return $inputs;
	}

//	function getAssocDataValues()
//	{
//
//		foreach ($fieldData as $label => $field) {
//			$vardata = new VarContainer($field);
//			$assocLeadData[$vardata->label] = $vardata->value;
//		}
//	}
	function Execute(\VarContainer $vars) {
		$ValidPOST = self::ValidatePOST($vars);

		if ($ValidPOST->hasErrors())
			return $ValidPOST;
		
		$form_id = $vars->get('formID', 'intval');
		$user_id = $vars->get('userID', 'intval');
		$hub_id = $vars->get('hubID', 'intval');
		$touchpoint_id = $vars->get('touchpointID', 'intval');
		$hub_page_id	=	intval($vars->getValue('hubPageID', 0));
//		$page = $vars->getValue('page', '');
		$keyword = $vars->getValue('keyword', '');
		$search_engine = $vars->getValue('search_engine', '');

		$leadsdao	=	$this->getDA();
		$formx	=	$this->getDA()->getLeadForm($form_id);
        $form_redirect_url = $formx->getRedirectURL();
        $returnURL = $form_redirect_url;
        $this->setForm($formx);

        if($formx->isLegacyForm())
        {
            // process a legacy form..
//            $fields	=	$formx->getFields();
        }else{

        }
		$inputs = $this->collectLeadInputs($vars);

		/** @var LeadFormModel $form */
//		$form = $hub->getCustomForms($user_id, NULL, NULL, $form_id, NULL, 1); //;$hub->queryFetch($query);

//		if (!$form || !$form['data'])
//			return $ValidPOST->addError('form', 'Form does not exist. or Invalid Form Data.');

//		$fields = $hub->parseCustomFormFields($form['data']);

		// validate required fields
		$ValidRequiredValues = $inputs->validateRequiredFields();
		if ($ValidRequiredValues->hasErrors())
			return $ValidRequiredValues;

		$lead_email	=	$inputs->getEmailValue();
		$lead_name = $inputs->getNameValue();
        $lead_phone = $inputs->getPhoneValue();
//		$fieldData = LegacyFuncs::encodeLeadFormPostValues($fields, $hub, $continue, $firstName, $lastName, $lead_phone, $lead_name, $lead_email);
		
//		if (!$fieldData)
//			throw new QubeException('Unable to get fieldData');

		$is_previously_sent = $this->checkPreviouslySent($lead_email, $form_id);

//		$fromDomain	=	'6qube.com';
		
//		if (!$lead_name && ($firstName || $lastName))			$lead_name = $firstName . ' ' . $lastName;

		$dataString = LegacyFuncs::getFieldDataStringLegacy($inputs);
		$required_leadinfo	=	compact('lead_phone', 'user_id', 'form_id', 'hub_id', 'lead_email', 'lead_name', 'hub_page_id', 'dataString', 'search_engine', 'keyword', 'touchpoint_id');
		$required_leadinfo['remote_addr'] =	$_SERVER['REMOTE_ADDR'];
		$required_leadinfo['piwikVisitorId']	=	$vars->getValue('visitorId', '');
		$required_leadinfo['grade'] = $value = self::getLeadRating($inputs);
		
		
		
		$spam_points = $this->getSpamStatus($vars, $required_leadinfo, new SpamChecker());
		
		$required_leadinfo['is_spam']	=	$spam_points;
		
		$lead = NULL;
		
		//if($user_id==155569) var_dump($spam_points);
		
		$processable = (!$is_previously_sent && $spam_points != 100); //true;	//(!$is_previously_sent && $spam_points < 60);
		if (($form_id == 891) || $processable){	//(!$is_previously_sent && $spam_points < 60)) {
			
			
			$lead_ID = $leadsdao->SaveLead($required_leadinfo, $inputs);
			$ProspectDataAccess	=	new ProspectDataAccess();
			$prospectID = $ProspectDataAccess->SaveFromLeadID($lead_ID, $inputs->getPhoneValue());

			if($lead_email){
				$photo = $ProspectDataAccess->getProspectPhotoByEmail($lead_email);
				if($photo){
					$ProspectDataAccess->saveProspectPhoto($photo, $user_id, $lead_email);
				}
			}
			$required_leadinfo['lead_id']	=	$lead_ID;
//			$required_leadinfo['fromDomain']	=	$fromDomain;

			if (!self::IsSpam($spam_points)) {
				/**
				 * Send Notifications and Responses.
				 */
				$responderdao	= ResponderDataAccess::create($this);
				$lead = LeadModel::Fetch('ID = %d', $lead_ID);
				$userInfo	=	$leadsdao->getLeadUserInfo($lead);

				$Mailer	=	$this->getQube()->getMailer();
				$Mailer->setLogger($this);
				$this->ScheduleLeadNotifications($lead, $inputs, $Mailer, $responderdao, $userInfo, $leadsdao);
				$returnURL	=	$this->ProcessResponders($lead, $Mailer, $responderdao, $userInfo['domain']);
			}
		}
		
		if(!$returnURL && $form_redirect_url){
			$returnURL = $form_redirect_url;
		}
		
		if($returnURL)
		{	
			if(strpos($returnURL, '/?')) $returnURL = $returnURL.'&pid='.$lead_ID;
			else $returnURL = $returnURL.'?pid='.$lead_ID;
			$returnURL = $this->getReturnURL($returnURL, $inputs);
		}
		
		if($vars->exists('ajaxSubmit'))
		{
			// success is always 1, on validation error, a validationstack is returned.
			$result	=	array('redir' => $returnURL, 'success' => 1);
			return $result;
		}
			
		return $returnURL;
	}
	
	function getReturnURL($returnURL, CollectedLeadInput $inputs)
	{
		$r = new PlaceHolderParser($inputs->asPlaceHolderValues(), '((', '))', '[^\)]*?');
		$r->setReturnCALLBACK(function ($key, $val, $php){
			return urlencode($val);
		});
		return $r->evSTR($returnURL);
	}
	
	static function getLeadRating(CollectedLeadInput $data)
	{
		$fields = $data->getInputs();
		$fieldValues	=	array_map(function($input){
			return $input->value;
		}, $fields);
		
		$values_count	=	count(array_filter($fieldValues));
		$fields_count	=	count($fields);
		
		$empty_fields	=	$fields_count-$values_count;

		$value = 0;
		
		if($empty_fields == 0)
			$value = 100;
		
		else if($fields_count < 10)
			$value = 100-$empty_fields*10;
		
		else
			$value = ceil($values_count/$fields_count*100);

		return ceil($value / 10);
	}

	/**
	 * 
	 * @param ResponderModel[] $responders
	 * @param array $leadvars
	 * @param SendTo $responderRecipient
	 * @param array $formdataAssoc
	 * @return string the return url
	 */
    function SendInstantLeadResponderMails(array $responders, WebsiteModel $W, NotificationMailer $Mailer, LeadModel $lead, SendTo $responderRecipient, $fromDomain)
    {
        /**
         * Ideally the user should have only 1 responder configured for each form.
         * But let's loop just in case the have multiple responders.
         *
         */
        $returnURL = '';
        foreach ($responders as $Responder) {

            if ($Responder->canSendEmail() == FALSE) {
                continue;
            }

            $Smarty = new QubeSmarty();

//			$LeadResponse = $Responder->CreateResponse('lead_forms' == $Responder->table_name ? 'leads' : 'contact_form', $lead);
            $smartyvars = ResponderModel::loadUserInfo($lead->getID(), NULL, NULL);
            $LeadResponse = $Responder->ConfigureResponse(
                array(
                    'table' => 'lead_forms' == $Responder->table_name ? 'leads' : 'contact_form',
                    'lead' => $lead
                ),
                (object) array(
                    'main_site' => $fromDomain,
                    'responder_phone' => $W->phone
                ),
                $Smarty,
                array(),
                $smartyvars
            );

            if (!$Mailer->Send($LeadResponse, $responderRecipient)) {
                $this->logERROR("Failed. Lead Response Failed to %s", $responderRecipient);
                Qube::LogError($Mailer, $LeadResponse, $responderRecipient);
            } else {
                $this->logINFO("OK. Sent LeadResponse to %s", $responderRecipient);
            }

            //check/set return URL
            if ($Responder->return_url) {
                $returnURL = $Responder->return_url;
            }
        } //end if($hub->emailsRemaining($responder['user_id'])>0)
        return $returnURL;
    }

	function CreateCollabNotificationQueue(LeadsDAO $dao, $hubID, $lead_ID) {
		$collaborators = $dao->getHubCollaborators($hubID);
		$dao->CollabNotificationPush($lead_ID, $collaborators);
	}

	function SendLeadNotification(Mail $smtp, LeadModel $lead, CollectedLeadInput $inputs, array $userInfo) {
		$PDO = $this->getQube()->GetPDO();


		$query = "SELECT name FROM lead_forms WHERE id = '$lead->lead_form_id'";
		$formName = $PDO->query($query)->fetchColumn();

		//get hub info
		$query = "SELECT name, pages_version FROM hub WHERE id = '" . $lead->hub_id . "'";
		$hubInfo = $PDO->query($query)->fetch(PDO::FETCH_ASSOC);

		$fromDomain = $userInfo['domain'];
// end

		$from = new SendTo($userInfo['company'], 'no-reply@' . $fromDomain); //'no-reply@'.$fromDomain;

		$pageTitle = '';
		if ($lead->hub_page_id) {
						$query = sprintf("SELECT page_title FROM `%s` WHERE id = %d LIMIT 1", 
												$hubInfo['pages_version'] == 2 ? 'hub_page2' : 'hub_page',
												$lead->hub_page_id);
						$pageTitle = $PDO->query($query)->fetchColumn();
		}

		//email user with form response					
		$to = $userInfo['username'];
#					$to	=	'"amado" <info@sofus.mx>';
		$subject = 'NEW LEAD from ' . $formName;
		$message = LegacyFuncs::CreateLeadInfoEmailBody($inputs, $pageTitle, $lead->search_engine,
						$lead->keyword, $hubInfo['name']);
		$headers = array('From' => $from,
										'Reply-To' => $from,
										'To' => $to,
										'Subject' => $subject,
										'Content-Type' => 'text/html; charset=ISO-8859-1',
										'MIME-Version' => '1.0');
#					deb($to, $headers, $message);
		$time = microtime(true);
		//already declared in the CreateCollabNotificationQueue()????
		//$collaborators = $this->getDA()->getHubCollaborators($lead->hub_id);
		//$this->getDA()->CollabNotificationPush($lead->getID(), $collaborators);
		$mail = $smtp->send($to, $headers, $message);
		$this->showProgress(__FUNCTION__, $time, "%s s. Sending Email To %s", "%s", $to);
		return $mail;
	}

	function ScheduleLeadResponders($lead_id, array $responders) {

		$qube = $this->getQube();

		/* @var $user UserModel */
		/*
		$user = $qube->queryObjects('UserModel')->Where('T.id = %d', $userID)
										->leftJoin(array('user_class', 'c'), 'c.id = T.class')
										->addFields('c.email_limit')
										->Exec()->fetch();

		$user_has_email_limit = $user->hasEmailLimit();

		// @todo consider evaluating the limit in the cron msg dispatcher script?
		if ($user_has_email_limit && $user->email_limit <= $user->emails_sent) { // do nothing if msg is spam, or user reached email limit
			Qube::LogError('User has Reached Limit', $user);
			return;
		}
		// limit not reached.. continue
		*/
		
//		$user	=	new UserDataAccess();
//		$user	=	$user->getUserWithRoles($userID);
//		$dao	=	new ResponderDataAccess();
//		$dao->setActionUser($user);
//		$responders	=	$dao->getLeadResponders($lead_id, 0);
		/*
		$Query = $D->queryActiveRespondersWhere('T.table_name = "lead_forms" AND T.table_id = :form_id AND sched>0 AND T.trashed = "0000-00-00" AND T.user_id = :user_id');
#                                                    ->leftJoin(array('trash', 'tr'), 'tr.table_name = "auto_responders" AND tr.table_id = T.id');

		if ($user_has_email_limit)
			$Query->Limit($user->email_limit - $user->emails_sent);

		$statement = $Query->Prepare();

		if (!$statement->execute(array(':form_id' => $formID, ':user_id' => $userID))) {
			Qube::LogError('Failed to fetch responders.');
		}

		$responders = $statement->fetchAll();
		 * 
		 */
		foreach ($responders as $r) {
			// new
			$scheduled_response = new ScheduledResponse();
			$scheduled_response->_set('contactdata_table', 'leads');
			// end
			$scheduled_response->_set('responder_id', $r->id);
			$scheduled_response->_set('contactdata_id', $lead_id);
			$scheduled_response->setScheduledTime($r->sched, $r->sched_mode);

			$qube->Save($scheduled_response);
		}
	}

	/**
	 * 
	 * @param string $email
	 * @param int $formID
	 * @return boolean
	 */
	function checkPreviouslySent($email, $formID) {

		$PDO = $this->getQube()->GetDB();
		$stmt = $PDO->prepare('SELECT count(*) FROM leads WHERE lead_email = ? AND lead_form_id = ? AND created + INTERVAL 10 SECOND > NOW()');
//		$stmt = $PDO->prepare('SELECT count(*) FROM leads WHERE lead_email = ? AND data = ? AND lead_form_id = ? AND created + INTERVAL 10 SECOND > NOW()');
		$stmt->execute(array($email, $formID));
//		$stmt->execute(array($email, $dataString, $formID));

		$was_sent = ($stmt->fetchColumn() > 0);
		return $was_sent;
	}

	function getSpamStatus(VarContainer $postdata, $leadinfo, SpamChecker $SC) {
		if(Qube::IS_DEV()) return 0;
		$postarray = $postdata->getData();
		$datastring = implode("\n", $postarray);

		if($postdata->checkValue('spam_points', $spampoints)){
			$this->logINFO("Spam Points predefined: %d", $spampoints);
			return $spampoints;
}

		if($leadinfo['lead_email'] == 'john@hotmail.com')
		{
			$this->logINFO("Lead_Email in black list %s", $leadinfo['lead_email']);
			//var_dump('wtf lead email');
			return 100;
		}
		if($postdata->getValue('firstmiddlelastsurname', '') != '')
		{
			$this->logINFO("Honey pot triggered.");
			//var_dump('wtf honey pot');
			return 100;
		}

		if($SC->checkSpammerIP($leadinfo['remote_addr'], $this->getQube()))
		{
			$this->logINFO("Remote Addr is a spammer: %s, +%d=%d", $leadinfo['remote_addr'], 40, $points);
			//var_dump('wtf check spammer');
			return 100;
		}
		
		$points	=	0;
		if(mb_detect_encoding($datastring) != 'ASCII'){
			$this->logINFO("encoding != ascii +%d=%d", 20, $points);	
			$points += 20;
		}
		
		if($leadinfo['lead_phone'] != '')
		{
			if($leadinfo['lead_phone'] == '123456'){
				$this->logINFO("lead_phone %s, +%d=%d", $leadinfo['lead_phone'], 20, $points);

				$points += 20;
			}
			if(preg_match('/^[^a-z:]+$/', $leadinfo['lead_phone']) == 0)
			{
				$this->logINFO("lead_phone format fail %s +%d=%d", $leadinfo['lead_phone'], 10, $points);
				$points += 10;
			}
		}
		
		/*if(!$leadinfo['piwikVisitorId'] && !$leadinfo['search_engine'])
		{
			$this->logINFO("Missing Piwik Visitor ID");
			$points += 50;
		}*/
		$sender_email = $leadinfo['lead_email'];
		$sender_ip = $leadinfo['remote_addr'];
		require_once HYBRID_PATH . 'api/spam/check.php';
		/*$ch = curl_init();
		$curlConfig = array(
			CURLOPT_URL            => "https://api.cleantalk.org/?method_name=spam_check&auth_key=nu6a7ydenudu&email='.$sender_email.'&ip='.$sender_ip.'",
			CURLOPT_POST           => true,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $curlConfig);
		$result = curl_exec($ch);
		curl_close($ch);

		$ct_result = json_decode($result);

		//var_dump($ct_result->data);
		foreach($ct_result->data as $set){

			if($set->appears==1) $points += 100;
		}*/

		if($SC->checkMessage($datastring))
		{
			$this->logINFO("DataString contains spam words: %s, +%d=%d", $datastring, 50, $points);
			$points += 50;
		}
		return $points;
	}
	
	function ScheduleLeadNotifications(LeadModel $lead, CollectedLeadInput $inputs  , NotificationMailer $Mailer, ResponderDataAccess $responderdao, array $userInfo, LeadsDAO $leadsdao)
	{	
		// Send all Notifications.
//		$userdao	= UserDataAccess::create($this);
		
		$this->SendLeadNotification($Mailer->getMailer(), $lead, $inputs, $userInfo);
		$this->ScheduleLeadResponders(
					$lead->getID(),
					$responderdao->getLeadResponders($lead->getID())
				);//($user_id, $lead_ID, $form_id, $is_spam);
		$this->CreateCollabNotificationQueue($leadsdao, $lead->hub_id, $lead->getID());
	}
	
	function ProcessResponders(LeadModel $lead, NotificationMailer $Mailer, ResponderDataAccess $responderdao, $fromDomain)
	{
		$SendToSubmitter	=	new SendTo($lead->lead_email, $lead->lead_email);
		$responders = $responderdao->getFormInstantResponders(
				array('R.table_id' => $lead->lead_form_id,
						'R.table_name'	=> 'lead_forms',
						'R.user_ID' => $lead->user_id));

		$W = WebsiteModel::Fetch('id = %d', $lead->hub_id);
		return $this->SendInstantLeadResponderMails($responders, $W, $Mailer, $lead, $SendToSubmitter, $fromDomain);
	}
}
