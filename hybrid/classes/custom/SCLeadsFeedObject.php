<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class SCLeadsFeedObject extends FeedObject
{
    /*
    public $meta_title  =   '';
    public $meta_description  =   '';
    public $sub_heading  =   '';
    public $key_features  =   '';
    /**
     * Custom function (feed-item.php)
     */
    function getBullets()
    {
        return array();
    }
    
    /**
     * print a navigatoin menu given a hierarchy of classes.
     * this function is called by feednav class to print the navigation menu
     * 
     * @param type $CAT_HIERARCHY
     */
    function printNavMenu(&$CAT_HIERARCHY)
    {
        $this->printNavItem('Products', '/products', '/products/all-products', $CAT_HIERARCHY, 0);
    }
    
    function getRequiredTags() {
        return array('id', 'product_type', 'availability', 'item_group_id', 'description',
							'google_product_category', 'image_link', 'additional_image_link', 'dealer_sku',
                        'sub_heading', 'key_features', 'meta_title', 'meta_description');
    }

    function customSummary() {
        return array('sub_heading' => $this->sub_heading, 'key_features' => $this->key_features,
                        'meta_title'    => $this->meta_title,
                        'meta_description'  => $this->meta_description);
    }
    /**
     * custom internal implementation (specifically for appending /browse-asjekrjke/
     * 
     * @param type $TEXT
     * @param type $URL
     * @param type $REAL_URL
     * @param type $HIERARCHY
     * @param type $level
     */
    function printNavItem($TEXT, $URL, $REAL_URL, &$HIERARCHY, $level = 0)
    {
	$padding	=	str_repeat("\t", $level);
        echo $padding . '<li class="page', ($level == 0 ? '-' : '_') . 'item"><a href="' . $REAL_URL . '/" title="' . $TEXT . '">' . $TEXT . '</a>';

        if($HIERARCHY)
        {
            echo "\n\t", $padding, '<ul class="children">', "\n";
            foreach($HIERARCHY  as $key => $subnav)
            {
                $cururl =   $URL . '/' . $key;
                $this->printNavItem($subnav->CATEGORY->SHORT_NAME, $cururl, $cururl . ($level == 0 ? '/browse-' . $key : ''), $subnav->CHILD, $level+1);
            }
            echo $padding, "\t", '</ul>', "\n$padding";
        }
        echo '</li>', "\n";
    }
    
    /**
     * custom function for sorting the categories.
     * all categories not in the array will be sorted alphabetically
     * 
     * @return array
     */
    function getCategories_OrderedArray(){
	if($this->FEEDID == 6)
        return
                    array('Access Control',
					'Security Cameras',
					'Digital Video Recorders',
					'Hybrid Digital Video Recorders',
					'Network Video Recorders',
					'Video Management Software',
					'HD-SDI Security',
					'Accessories'
					);
	

	// else	(5 or other)
        return
                    array(

                        'HD-TVI Video Surveillance',
                        'Security Video Recorders',
                        'Security Cameras',

                    );

    }

    function appendMetaValuesToCategoryObjects(){
        return array('image_link');
    }
    
    /**
     * context: loop (feed-category.php)
     * conception: sql join (HubOutput.php)
     * keys: current_path (the path of current category);
     * 
     */
    function getPageUrl()
    {
        return '/' . $this->current_path . '/' . $this->createPageName() . '/';
    }
    
    /**
     * This function adds the secret sauce to url creation by adding the key to end of url.
     * 
     * context: loop (feed-category.php)
     * conception: sql join (HubOutput.php)
     * 
     * 
     * @return type
     */
    function createPageName()
    {
#        return preg_replace('/[\s\/\?\+-]+/', '-', strtolower($this->NAME)) . '--' . $this->KEY;
        return trim(preg_replace('/[^\w\.]+/', '-', strtolower($this->NAME)), '-') . '--' . $this->KEY;

    }


    function getOrderBy_Categories(){
		return 'order by sortval ASC, FULL_NAME ASC';
	}

	function getNavCategoriesWhere(DBWhereExpression $where){
		return $where;
#		return		$where->Where('sortval != 900');
	}

	function getSortColumn(){

# 	return 'order by FULL_NAME ASC';       
        $when   =   'CASE ';
        foreach($this->getCategories_OrderedArray() as $i => $cat){
            $when .=   "WHEN FULL_NAME LIKE 
		( SELECT CONCAT(FULL_NAME, '%') FROM feedobjects_categories WHERE SHORT_NAME LIKE '$cat' AND FEEDID=T.FEEDID LIMIT 1) THEN $i ";
        }

	return "$when ELSE 900 END as sortval";

#            return "order by $when ELSE 900 END ASC";
    }
        
    
    function removeATags($v){
        $v      = preg_replace('@<a[^>]*?>(.*?)<\/a>@i', '\1', $v);
        
        return $v;
                
#        $v      = strip_tags($v, '<p><div><h1><h2><ul><li><br>')
    }
    
    function set($name, $value){
        if($name	==	'id'){
            $name	=	'KEY';
            $value  =   str_replace('/', '.', $value);  // remove / from KEY
        }
            
        parent::set($name, trim($this->removeATags($value)));
    }

    /**
     * The real meat and potatoes. This function
     * converts the imported feed data into a page_row array.
     * 
     * The implementation depends on the theme's default.php file.
     * context: output
     * 
     * @return array
     */
    function getPageRow() {
        // important keys:
        // page_photo, page_photo_desc, page_title, page_navname, page_full_width, page_seo_desc, page_seo_title,
        // page_region, page_edit_2
#        var_dump($this);
        $page_photo =   $this->image_link;      // item image (feed)
        $page_region    =   $this->description; // item description (feed)
        $page_navname   =   $this->FULL_NAME;   // category full path (feed)
        $page_title     =   $this->NAME;    // item name (feed)
        
        return array('page_photo' => $page_photo,
                        'page_region'   => $page_region,
                        'page_navname'  => $page_navname,
                    'page_title'    =>  $page_title);
    }
    
    /**
     * Another esential function that processes the feed data to get a sanitized category path:
     * 
     * full/path/to/category and the $short_name which would be the real category name.
     * 
     * @return array [ [full/path/to/mycategory, My Category], ... ]
     */
    function getCategories() {
        $categories =   $this->get('product_type');
        $list   =   array();
        foreach((array)$categories as $category)
        {
            $category = str_replace(' > ', '/', $category);
            preg_match_all('#(?:[^/]+\s+/\s+[^/]+)*|[^/]+#', $category, $p);
            $short_namea =   array_filter($p[0]);    //explode('/', $category);
            $short_name =   array_pop($short_namea);
#            if(stripos($short_name, 'hdcc'))              		print_r($short_namea);
#  xdebug_break();
            $list[] =   array(preg_replace('/^shop-all-products\/(shop-by-brand\/)?/', '', parent::normalize_category($category)),
                    $short_name);
        }
        
        return $list;
    }
    
    
    
    /**
     * 
     * This function parses shortens it to 200 chars 
     * 
     * @param type $MODE
     * @return type
     */
    
    function getDescription($MODE = FeedObject::FOR_FULL) {
        if($MODE    ==  self::FOR_SUMMARY)
        {
            return substr(strip_tags(parent::getDescription($MODE)), 0, 200);
        }
        parent::getDescription($MODE);
    }
    
}
