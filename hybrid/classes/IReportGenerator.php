<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/30/14
 * Time: 2:02 PM
 */

interface IReportGenerator {
    public function getOrganicSearchStats();
    public function getReferralsStats();
    public function getSocialMediaStats();
    public function getPaidSearchStats();
    public function getDirectTrafficStats();
    public function getAllStatsDaily();
    public function getReportData(CampaignModel $camp, ReportModel $report);
} 