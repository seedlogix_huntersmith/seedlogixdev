<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
\ini_set('display_errors', true);
error_reporting(E_ALL ^ E_STRICT);

define('SECRET_KEY', "SEEDLOGIX-BEAST-01-01-2018");
define('SECRET_IV', "CHILANGO-FRIDAY");

//$account_number = '1112223333';

$encrypt_method = "AES-256-CBC";
$key = hash('sha256', SECRET_KEY);
$iv = substr(hash('sha256', SECRET_IV), 0, 16);

//$encrypted_account_number = openssl_encrypt($account_number, $encrypt_method, $key, 0, $iv);
//var_dump($encrypted_account_number);


//$decrypted_account_number = openssl_decrypt($encrypted_account_number, $encrypt_method, $key, 0, $iv);
//var_dump($decrypted_account_number);

$host = '127.0.0.1';
$dbname = 'sapphire_6qube';
$user = 'root';
$pass = 'seedlogix';

$dsn = "mysql:host=$host;dbname=$dbname";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
];
$pdo = new PDO($dsn, $user, $pass, $opt);


$sql = "SELECT baccount_number FROM user_info";
$stmt = $pdo->query($sql);
$result = $stmt->fetchAll();

try {
    $pdo->beginTransaction();
    $sql = "UPDATE user_info SET baccount_number = ?";
    $stmt = $pdo->prepare($sql);
    
    foreach ($result as $row) {
        $encrypted_account_number = openssl_encrypt($row->baccount_number, $encrypt_method, $key, 0, $iv);
        
        $stmt->execute([$encrypted_account_number]);
        
    }
    
    $pdo->commit();
    
} catch (Exception $ex) {
    $pdo->rollBack();
    throw $ex;
}
