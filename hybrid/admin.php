<?php

if ((!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") &&
    preg_match('/\.(test|local|dev)$/', $_SERVER['HTTP_HOST']) !== 1) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
/*
 * Author:     Jon Aguilar
 * License:
 */

#$_SERVER['QUBECONFIG']	=	'qube.pastephp.com';
require_once 'bootstrap.php';
require_once QUBEADMIN . 'library/Popcorn/BaseController.php';
require_once HYBRID_PATH . 'controllers/SecureController.php';

Qube::ForceMaster();

/*
 * 1. Load the Controller.
 *
 */
/*
  if(!Qube::IS_DEV()){
  header('Location: /');
  exit;
  }
 */

#$qube->Initialize();
//$Clr    =   (!isset($_GET['ctrl']) ? 'Campaign' : ucfirst(strtolower($_GET['ctrl'])));
$Clr = 'Campaign';
if (isset($_GET['ctrl'])) {
    $Clr = $_GET['ctrl'] == strtoupper($_GET['ctrl']) ? ucfirst(strtolower($_GET['ctrl'])) : ucfirst($_GET['ctrl']);
}
$action = (!isset($_GET['action']) ? '' : $_GET['action']);

$Launcher = new HybridControllerLauncher($Clr);
$Launcher->RunView($Launcher->getWebNamespace('admin'), $action);

