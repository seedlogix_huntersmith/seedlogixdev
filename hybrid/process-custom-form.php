<?php
/**
 * 4-27-2012
 * implemented queued responses
 * 
 */


/**
 * Based on original process-custom-form.amado.php
 * 
 */
if(!isset($_SERVER['QUBECONFIG']))  $_SERVER['QUBECONFIG']  =   'eric';    // use 6qube1 for router processing

#if($_SERVER['HTTP_HOST'] == 'pastephp.com')	var_dump($_POST);
if(count($_POST) == 0) die('Error. 2');
    // new
    require_once 'bootstrap.php';
    
    //include classes
    require_once(QUBEADMIN . 'inc/hub.class.php');
    
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';
    require_once QUBEPATH . '../models/Response.php';
    require_once QUBEPATH . '../classes/Notification.php';
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/NotificationMailer.php';
    require_once QUBEPATH . '../models/Responder.php';
    require_once QUBEPATH . '../models/CampaignCollab.php';
    require_once QUBEPATH . '../models/Website.php';
            require_once QUBEADMIN . 'classes/ResponseNotification.php';
            require_once QUBEADMIN . 'classes/LeadResponseNotification.php';
require_once QUBEPATH . '../classes/QubeSmarty.php';
        // end
    
Qube::ForceMaster();
$LFP	=	new LeadFormProcessor(Qube::Start());
$PostVars   = VarContainer::getContainer($_POST);

$result	=	$LFP->Execute($PostVars);

if(is_array($result))	// return ajax
{
	echo json_encode($result);
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript">



	<?php

    if($result instanceof ValidationStack)
    { ?>
		var errors	=	<?php echo json_encode($result->getErrors()); ?>;
		var msg = "The following errors ocurred: ";
		for ( var field in errors )
			msg += errors[field].join(", ");
		
		parent.alert(msg);
	<?php
}else{
	// result is the returnURL

	if($result)
	{
		?>
			window.parent.location.replace("<?= $result; ?>");
		<?php
	}else{
		?>
			alert('Your response was successfully sent!');
	<?php
	}
}

?>
</script>

</head>
<body></body>
</html>

<?php

exit;


$Mailer =   $qube->getMailer();
	$hub = new Hub();
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
        
	$smtp = Mail::factory('smtp', array ('host' => $host));
	
        $Validation =   LeadFormProcessor::Validate($PostVars);
	$probablySpam = $_POST['hpot'] ? true : false;
        
	$probablySpam = $_POST['hpot'] ? true : false;
        
        if(!$Validation->hasErrors())
        {
            $formID =   $PostVars->get('formID', 'intval');
            $userID =   $PostVars->get('userID', 'intval');
            $hubID  =   $PostVars->get('hubID', 'intval');
        }else{
#            Qube::LogError('Validation Failed', $Validation->getErrors());
		$probablySpam	=	true;
            $formID = is_numeric($_POST['formID']) ? (int)$_POST['formID'] : '';
            $userID = is_numeric($_POST['userID']) ? (int)$_POST['userID'] : '';
            $hubID = is_numeric($_POST['hubID']) ? $_POST['hubID'] : '';            
        }
        
	$hubPageID = is_numeric($_POST['hubPageID']) ? $_POST['hubPageID'] : '';
	$keyword = $hub->validateInject($_POST['keyword']);
	$search_engine = $hub->validateInject($_POST['search_engine']);
	$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
	if($ip && $hub->checkBlacklist($ip)) $blacklisted = true;

	if($formID && $userID && !$blacklisted && !$probablySpam){
		//$query = "SELECT data, options FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
		$form = $hub->getCustomForms($userID, NULL, NULL, $formID, NULL, 1);//;$hub->queryFetch($query);

		if($form['data']){
			$fields = $hub->parseCustomFormFields($form['data']);
			$continue = true;
			$fieldData = LegacyFuncs::encodeLeadFormPostValues($fields, $hub, $continue, $firstName, $lastName, $phone, $name);
			$i = 0;
			if(!$name) $name = $firstName.' '.$lastName;

			//now go through again to build array string for leads table
			if($fieldData && $continue){
				$dataString = '';
				foreach($fieldData as $field=>$data){
					$dataString .= $data['type'].'|-|'.$data['label'].'|-|'.$data['value'].'[==]';
				}
				//trim trailing [==]
				if(strlen($dataString)>=4) $dataString = substr($dataString, 0, -4);
				//sanitize data string
				$dataString = $hub->sanitizeInput($dataString);
				//gather/sanitize other stuff
				$page = $hub->sanitizeInput($_POST['page']);
				
#amado 2012-05-24 check the lead doesnt already exist!
				$was_sent = FALSE;
				
					$encoding   = mb_detect_encoding($dataString);
//echo $encoding;
				      $is_test = $email == 'test@test.com'; // @used below
				      $is_spam = $encoding != 'ASCII' || $phone == '123456' || SpamChecker::checkMessage(implode("\n", $_POST), new SystemDataAccess());
				    if(TRUE)	// 123456 will be considered spam
				    {
				      $stmt = Qube::GetPDO()->prepare('SELECT count(*) FROM leads WHERE lead_email = ? AND data = ? AND lead_form_id = ? AND created + INTERVAL 10 SECOND > NOW()');
				      $stmt->execute(array($email, $dataString, $formID));
				      
				      $was_sent = ($stmt->fetchColumn() > 0);
				      if($was_sent && !$is_spam) $success = TRUE;
				    }
#end
                                // 
				//build string to insert into leads table
				$query = "INSERT INTO leads 
						(user_id, parent_id, lead_form_id, cid, lead_email, lead_name, hub_id, hub_page_id, data, search_engine, keyword, ip, spam, created) 
						VALUES 
						('".$userID."', (select parent_id from users where id=" . $userID ."),'".$formID."', 
                                                    (select cid from hub where id = " . (int)$hubID . "), '".$email."', '" . $name . "', '" .$hubID."', '".
                                                $hubPageID."',	'".$dataString."', '".$search_engine."', '".$keyword."', '".$_SERVER['REMOTE_ADDR']."', " . ($is_spam ? 1 : 0) . ", NOW())";
                                $form_data  =   array();
				if(!$was_sent && $hub->query($query)){


                                    
                                    // new
                                        $contactdata_id = $hub->getLastId();
                                        
                                    if(TRUE)
                                    {
                                        try{
                                        $save_data_stmt = Qube::GetPDO()->prepare('INSERT INTO leads_data (lead_id, label, value) VALUES (?, ?, ?)');
                                        Qube::GetPDO()->beginTransaction();
                                        
                                        foreach($fieldData as $index => $data)
                                        {
                                                // @note stripslashes as necessary (enabled by php gpc magic quotes)
                                            $vardata = new VarContainer($data);
                                            $save_data_stmt->execute(array($contactdata_id, $vardata->label, $vardata->value));
                                            $form_data[$vardata->label] =   $vardata->value;
                                        }
                                        Qube::GetPDO()->commit();
                                        }Catch(PDOException $e)
                                        {
                                            Qube::Fatal($e);
                                        }
                                    }
                                    
                                        // @todo streamline this process so we query all instant and delayed autoresponders
                                        // for now, just schedule delayed responders
                                        // 
                                        
                                        $D = Qube::GetDriver();
                                        $qube   =   Qube::Start();
                                        /* @var $qube Qube */
                                        $user = $qube->queryObjects('UserModel')->Where('T.id = %d', $userID)
                                                    ->leftJoin(array('user_class', 'c'), 'c.id = T.class')
                                                    ->addFields('c.email_limit')
                                                    ->Exec()->fetch(); //   ueryO('id = %d', $userID);
                                        $user_has_email_limit = $user->hasEmailLimit();
                                        
                                        // @todo consider evaluating the limit in the cron msg dispatcher script?
                                        if($is_spam || ($user_has_email_limit && $user->email_limit <= $user->emails_sent)) // do nothing if msg is spam, or user reached email limit
                                        {
                                            // limit has been reached
                                            // echo 'limit: ' , $user->email_limit, ' sent: ', $user->emails_sent;
                                        }else{
                                            $Query = $D->queryActiveRespondersWhere('T.table_name = "lead_forms" AND T.table_id = :form_id AND sched>0 AND T.trashed = "0000-00-00" AND T.user_id = :user_id');
#                                                    ->leftJoin(array('trash', 'tr'), 'tr.table_name = "auto_responders" AND tr.table_id = T.id');

                                            if($user_has_email_limit)
                                                $Query->Limit($user->email_limit-$user->emails_sent);
                                            
                                            $statement = $Query->Prepare();
                                            
                                            if($statement->execute(array(':form_id' => $formID, ':user_id' => $userID)))
                                            {
                                                $responders = $statement->fetchAll();
//                                                deb($responders);
                                                foreach($responders as $r)
                                                {
                                                    // new
                                                    $scheduled_response = new ScheduledResponse();
                                                    $scheduled_response->_set('contactdata_table', 'leads');
                                                    // end
                                                    $scheduled_response->_set('responder_id', $r->id);
                                                    $scheduled_response->_set('contactdata_id', $contactdata_id);
                                                    $scheduled_response->setScheduledTime($r->sched, $r->sched_mode);
                                                    
                                                    $qube->Save($scheduled_response);
//                                                    $scheduled_response->Save();
                                                }
                                            }
                                        }
                                        
                                        // end
                                        
					$success = true;
					$key = $value = NULL; //reset these vars for use below
					//get user's info
                                        
                                        /*
                                         * Outer Joins to the Rescue!
                                         */
                                        if(!$is_spam){
                                        $usrq = Qube::GetPDO()->query('SELECT u.username, u.parent_id, u.super_user,
                                                IFNULL(r.main_site, "6qube.com") as domain,
                                                IFNULL(r.company, "6Qube") as company
                                                FROM `users` u LEFT JOIN resellers r ON ((u.parent_id = 0 AND r.admin_user = u.id) or 
                                                    r.admin_user = u.parent_id) WHERE u.id = ' . (int)$userID);
                                        
                                        $userInfo = $usrq->fetch(PDO::FETCH_ASSOC);
                                        $fromDomain = $userInfo['domain'];
					if(!$fromDomain) $fromDomain = '6qube.com';
                                        // end
                                        
					$query = "SELECT name FROM lead_forms WHERE id = '".$formID."'";
					$formInfo = $hub->queryFetch($query, NULL, 1);
					
					//get hub info
					$query = "SELECT name, pages_version FROM hub WHERE id = '".$hubID."'";
					$hubInfo = $hub->queryFetch($query);
					if($hubPageID){
						$pTable = 'hub_page';
						if($hubInfo['pages_version']==2) $pTable .= '2';
						$query = "SELECT page_title FROM `".$pTable."` WHERE id = '".$hubPageID."' LIMIT 1";
						if($pageInfo = $hub->queryFetch($query)) $pageTitle = $pageInfo['page_title'];
					}
					//email user with form response					
					$to = $is_test ? 'test@sofus.mx' : $userInfo['username'];
#					$to	=	'"amado" <info@sofus.mx>';
					$from = new SendTo($userInfo['company'], 'no-reply@' . $fromDomain);    //'no-reply@'.$fromDomain;
					$subject = 'NEW LEAD from '.$formInfo['name'];
					$message = LegacyFuncs::CreateLeadInfoEmailBody($fieldData, $pageTitle, $search_engine, $keyword, 'Failed');
					$headers = array('From' => $from,
								  'Reply-To' => $from,
								  'To' => $to,
								  'Subject' => $subject,
								  'Content-Type' => 'text/html; charset=ISO-8859-1',
								  'MIME-Version' => '1.0');
#					deb($to, $headers, $message);
					$mail = $smtp->send($to, $headers, $message);
                                }
                                        // MODIFIED TO WORK WITH MU FORMS (where form campaign id and collab campaign id are different) 
#            $collaborators  = $D->queryCampaignCollabWhere('(T.scope = "campaign" OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND
#            r.site_id = %d )) AND T.campaign_id = (SELECT cid FROM hub WHERE id = %d) AND T.active = 1 AND T.trashed = "0000-00-00"', $hubID, $hubID)->Exec()->fetchAll();                                        
                                        
            if(!$is_test && !$is_spam){
             $collaborators  = $D->queryCampaignCollabWhere('(
                 T.scope = "campaign"and T.campaign_id = (SELECT cid FROM hub WHERE id = %d)
                 ) OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND r.site_id = %d )', $hubID, $hubID)->Exec()->fetchAll();
           
            $collab_msg_qstmt = Qube::GetPDO()->prepare('INSERT INTO collab_email_queue (data_id, data_type, collab_id)
                    VALUES (?, ?, ?)');
            
                Qube::GetPDO()->beginTransaction();
                foreach($collaborators as $collab)
                {
                    // enqueue these messages
                        $collab_msg_qstmt->execute(array($contactdata_id, 'lead', $collab->id));
                    // end
                }
                Qube::GetPDO()->commit();
            }
                                        // @amado loop through collaborators
					$to = $from = $subject = $message = $headers = NULL; //reset vars for email below
					//check for autoresponders
					$query = "SELECT * FROM auto_responders WHERE table_name = 'lead_forms' AND table_id = '".$formID."'
							AND sched = 0 AND active = 1 
							AND auto_responders.trashed = '0000-00-00' AND user_id = " . (int)$userID;
					$responders = $hub->query($query);
					if($responders && $hub->numRows($responders)){
						while($r = $hub->fetchArray($responders)){
							//if instant, send now
							//before sending check user's remaining email sends
							if($hub->emailsRemaining($r['user_id'])>0){
                                                            
                                                            /* new shit */
                                                            
                                                            
        $D  =   Qube::GetDriver();
        $Smarty =   new QubeSmarty();
        $Responder  =   $D->getResponderWhere('id = ' . $r['id']);
        $W          =   $D->getWebsiteWhere('id = ' . $hubID);//(int)$Responder->table_id);

        $formdata   =   $form_data;
#        list($Response, $recipient) =   $Responder->createFormResponse((object)$formdata);

        list($Response, $recipient) =   $Responder->CreateResponse(
                    'lead_forms' == $Responder->table_name ? 'leads' : 'contact_form', (object)$formdata);
        $Responder->ConfigureResponse($Response, (object)array(
                'main_site' => $fromDomain, //$_SERVER['SERVER_NAME'],
                'responder_phone' => @$W->phone),
                $Smarty,
                $formdata);
        
        
        /* end new shit */
        $recipient  =   new SendTo($name, $email);
								Qube::dev_dump($Mailer);
        $Mailer->Send($Response, $recipient);
        /*
        //replace embed tags with values
        foreach($fieldData as $key=>$value){
                $searchTag = $hub->convertKeywordUrl($value['label'], 1, '_');
                $body = str_replace('[['.$searchTag.']]', $value['value'], $body);
                $subject = str_replace('[['.$searchTag.']]', $value['value'], $subject);
                unset($searchTag);
        }
			//					$headers = "From: ".$row2['from_field']."\r\n";
			//					$headers .= "Reply-To: ".$row2['from_field']."\r\n";
			//					$headers .= "MIME-Version: 1.0\r\n";
			//					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
								$headers = array ('From' => $from,
											   'Reply-To' => $from,
											   'To' => $to,
											   'Subject' => $subject,
											   'Content-Type' => 'text/html; charset=ISO-8859-1',
											   'MIME-Version' => '1.0');
								$mail2 = $smtp->send($to, $headers, $body);
								//if mail was sent update user's emails_sent
								if(!PEAR::isError($mail2)){
									$query = "UPDATE users SET emails_sent = emails_sent+1 
											WHERE id = '".$responder['user_id']."'";
									$hub->query($query);
								}
			//					mail($to, $subject, $body, $headers);
         * 
         */
								//check/set return URL
        if($r['return_url']) $redirUrl = $r['return_url'];
							} //end if($hub->emailsRemaining($responder['user_id'])>0)
						} //end while($responder = $hub->fetchArray($responders))
					} //end if($responders && mysqli_num_rows($responders))
				} //end if($hub->query($query)) [insert into lead_forms table]
			} //end if($fieldData)
		} //end if($form['data'])
		if($form['options'] && !$redirUrl){
			$options = explode('||', $form['options']);
			if($options[2]) $redirUrl = $options[2];
		}
	} //end if($formID && $userID && !$blacklisted)
	else {
		//validation error or IP blacklisted
	}
	
	if($_POST['ajaxSubmit']){
		$return = array();
		$return['success'] = $success ? 1 : 0;
		$return['redir'] = $redirUrl ? $redirUrl : '';
		echo json_encode($return);
	}
	else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
	<? if($success && $redirUrl){ ?>
	window.parent.location = '<?=$redirUrl?>';
	<? } else if($success && !$redirUrl){ ?>
	alert('Your response was successfully sent!');
	<? } else if(!$success){ 
                if($hub->errors):
                    ?>
                        alert('<?php echo addslashes($hub->listErrors("\n")); ?>');
                        <?php
                    else:
?>
	alert('Sorry, there was an error submitting your form.  Please try again.');
	<? 
        
            
                endif;
        } ?>
</script>
</head>
<body></body>
</html>
<?
	}

?>
