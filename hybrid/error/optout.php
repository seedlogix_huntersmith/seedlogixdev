
			<!-- Start Inner Page Container -->
			<div class="inner-container">
				
				<div class="two-third" style="width:100%">
					<!-- Breadcrumb-->
					<!-- Start Custom Page Content -->
					<?php
						echo '<h2>Email Marketing Opt-out</h2>';						
							
						if($optout_result instanceof ValidationStack)
						{
							if($optout_result->hasErrors())
							{
								echo join("<br />", $optout_result->getErrors('OptOut'));
							}else{
								echo "<b>You were successfully removed from our client's mailing list.</b>";
							}
						}
						else {
					?>
					Ensure the email address below is correct and click the "Opt-Out" button to stop receiving scheduled emails. </b>.<br />
							<form action="<?php echo htmlentities($_SERVER['REQUEST_URI'], ENT_QUOTES); ?>" method="POST">
								<input type="hidden" name="pid" value="<?php
									echo htmlentities($pid, ENT_QUOTES);
									?>" />
								<input type="text" name="email" size="40" value="<?php echo htmlentities($email, ENT_QUOTES); ?>" /><br />
								<button name="submit" type="submit" class="blackButton3"><span>Opt-Out</span></button>
							</form><br />
							<small><b>Note:</b> We do not send or schedule these emails.  The company you received an email from is solely responsible<br />for the content of their emails, and we do our best to make sure they follow the CAN-SPAM act guidelines.<br />If you believe this company is abusing our system please contact us at <a href="mailto:<?=$resellerInfo['support_email']?>"><?=$resellerInfo['support_email']?></a>.</small><br />
					<?
						}
					?>
					<br style="clear:both;" />
				</div>
				
			<?
				//COMMON LAST THIRD (CONTACT FORM)
				//include('inc/common_last_third.php');
			?>
			</div><!--End Inner Page Container-->