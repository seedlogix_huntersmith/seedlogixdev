<?php

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 6/4/15
 * Time: 1:28 PM
 */
class PrevieweditorController extends DashboardBaseController{

	function ExecuteAction($action = NULL)
	{
		switch($action){
			case 'SaveRegions':
				return $this->saveRegions(VarContainer::getContainer($_POST));
			default:
				throw new QubeException("Missing action in PreviewController");
		}
	}

	private function saveRegions(VarContainer $params)
	{
		$result = false;
		$params->set('html', PrevieweditorController::ParseHtmRegionslTags($params->get('pages')));
		if(isset($params->postid) && $params->postid && $post = BlogDataAccess::FromController($this)->getPost($params->postid)){

			$this->savePostRegions($post, $params);
		}elseif(isset($params->pageid) && $params->pageid && $page = WebsiteDataAccess::FromController($this)->getPage($params->pageid)){

			$this->savePageRegion($page, $params);
		}elseif(isset($params->bemailid) && $params->bemailid && $emailer = EmailBroadcastDataAccess::FromController($this)->getLeadBroadcasters(array('id' => $params->bemailid, 'where' => array('e.id = :id AND e.user_id = :user_ID AND e.trashed = 0'),
                'returnSingleObj' => 'LeadFormEmailerModel'))){

           $this->saveEmailRegion($emailer, $params);
		}
		elseif(isset($params->responderid) && $params->responderid && $responder = ResponderDataAccess::FromController($this)->getResponderById($params->responderid)){

			$this->saveResponderRegion($responder, $params);
		}
		elseif(isset($params->siteid) && $params->siteid && $site = WebsiteDataAccess::FromController($this)->getSite($params->siteid)){

			$this->saveSiteRegions($site, $params);
		}elseif(isset($params->blogid) && $params->blogid && $blog = BlogDataAccess::FromController($this)->getBlog($params->blogid)) {

			$this->saveBlogRegions($blog, $params);
		}


			if($result instanceof ValidationStack){
				return $this->returnValidationError($result); 
			}
			return $this->Redirect($this->getPreviewURL($params->getData()));

		}

    private function saveEmailRegion(LeadFormEmailerModel $emailer, VarContainer $C)
    {
        $MM = new ModelManager();
        $column = $C->get('column');
        switch($column){
            case 'body':
                break;
            default:
        }
        $data[$column] = $C->get('html');
        $MM->doSaveBroadcastEmailer($data, $emailer->getID(), EmailBroadcastDataAccess::FromController($this));
#	print_r($result);exit;
    }

	private function saveResponderRegion(ResponderModel $responder, VarContainer $C)
	{
		$MM = new ModelManager();
		$column = $C->get('column');
		switch($column){
			case 'body':
				break;
			default:
		}
		$data[$column] = $C->get('html');
		$MM->doSaveResponder($data, $responder->getID(), ResponderDataAccess::FromController($this));
#	print_r($result);exit;
	}

	private function saveBlogRegions(BlogModel $blog, VarContainer $C)
	{
		$MM = new ModelManager();
		$column = $C->get('column');
		switch($column){
			case 'blog_edit_1':
			case 'blog_edit_2':
			case 'blog_edit_3':
				break;
			default:
				throw new QubeException("Column $column not supported");
		}
		$data[$column] = $C->get('html');
		$MM->doSaveBlog($data, $blog->getID(), BlogDataAccess::FromController($this));
	}

	private function savePostRegions(BlogPostModel $post, VarContainer $C)
	{
		$MM = new ModelManager();
		$column = $C->get('column');
		switch($column){
			case 'content':
			case 'post_edit_1':
				break;
			default:
				throw new QubeException("Column $column not supported");

		}
		$data[$column] = $C->get('html');
		$MM->doSaveBlogPost($data, $post->getID(), BlogDataAccess::FromController($this));
	}

	private function savePageRegion(PageModel $page, VarContainer $C)
	{
		$MM = new ModelManager();
		$column = $C->get('column');
		switch($column){
			case 'page_edit_2':
			case 'page_edit_3':
			case 'page_region':
				break;
			default:
				throw new QubeException("Column $column not supported");
		}
		$data[$column] = $C->get('html');
		$data['touchpoint_type'] = $C->get('touchpoint');
		$data['hub_id'] = $page->hub_id;
		$data['id'] = $page->getID();
		$MM->doSavePage($data, WebsiteDataAccess::FromController($this));
	}

	private function saveSiteRegions(WebsiteModel $site, VarContainer $C)
	{
		$MM = new ModelManager();
		$column = $C->get('column');
		switch($column){
			case 'offerings':
			case 'overview':
			case 'photos':
				break;
			default:
				throw new QubeException("Column $column not supported");
		}

		$data[$column] = $C->get('html');
		$data['touchpoint_type'] = $site->touchpoint_type;
		$MM->doSaveWebsite($data, $site->getID(), HubDataAccess::FromController($this));
	}

	function getPreviewURL($params){
		if($params['postid']){
			$idParams = "blogid={$params['blogid']}&postid={$params['postid']}";
		}elseif($params['pageid']){
			$idParams = "pageid={$params['pageid']}&siteid={$params['siteid']}";
		}elseif($params['siteid']){
			$idParams = "siteid={$params['siteid']}";
		}elseif($params['blogid']) {
            $idParams = "blogid={$params['blogid']}";
        }elseif($params['bemailid']){
			$idParams = "bemailid={$params['bemailid']}";
		}elseif($params['responderid']){
			$idParams = "responderid={$params['responderid']}";
		}else{
			throw new QubeException("Missing param preview");
		}
		$fullscreen = isset($params['fullscreen']) ? $params['fullscreen'] : 'false';
		return "/themes/preview-editor/v1/index.php?$idParams" . '&themeid=' . $params['themeid'] . "&column={$params['column']}&touchpoint=" . $params['touchpoint'] . '&fullscreen=' . $fullscreen;
	}

	public static function ParseHtmRegionslTags(array $pages){
		require_once(HYBRID_PATH . 'themes/preview-editor/v1/themes/inc/simple_html_dom.php');
		$html = '';

		foreach($pages as $page=>$content ) {
			$html .= stripslashes($content);
		}

		$replaceValues = array('div[class^="slider_replace"]'=>'[[slider]]',
								'nav[class^=treplace]'=>'[[navtag]]',
								'nav[class^=1treplace]'=>'[[navtag1]]',
								'nav[class^=nav_replace]'=>'[[navtag]]',
								'nav[class^=1nav_replace]'=>'[[navtag1]]',
								'div[class^=nav_replace]'=>'[[navtag]]',
								'div[class^=1nav_replace]'=>'[[navtag1]]',
								'div[class^=logo_replace]'=>'[[logo]]',
								'div[class^=posts1]'=>'[[posts1]]',
								'ul[id="pcategories"]'=>'[[category1]]',
								'div[id="pcategories"]'=>'[[category1]]',
								'ul[id="pposts"]'=>'[[pposts1]]',
								'div[id="pposts"]'=>'[[pposts1]]',
								'div[id="btitle1"]'=>'[[btitle1]]',
								'div[id="btitle2"]'=>'[[btitle2]]',
								'div[id="ptitle1"]'=>'[[ptitle1]]',
								'div[id="posts1"]'=>'[[posts1]]',
								'div[id="post1"]'=>'[[post1]]',
								'div[id="custom1"]'=>'[[custom1]]',
								'div[id="custom2"]'=>'[[custom2]]',
								'div[id="custom3"]'=>'[[custom3]]',
								'div[id="custom4"]'=>'[[custom4]]',
								'div[id="custom5"]'=>'[[custom5]]',
								'div[id="custom6"]'=>'[[custom6]]',
								'div[id="custom7"]'=>'[[custom7]]',
								'div[id="custom8"]'=>'[[custom8]]',
								'div[id="custom9"]'=>'[[custom9]]',
								'div[id="custom10"]'=>'[[custom10]]');

		foreach($replaceValues as $replaceKeyword=>$replaceValue){

			$tagprep = str_get_html($html);

			$replace = $tagprep->find(''.$replaceKeyword.'', 0)->outertext;
			$html = str_replace($replace,$replaceValue,$tagprep);
			unset($replace);

		}

		return $html;
	}
}
