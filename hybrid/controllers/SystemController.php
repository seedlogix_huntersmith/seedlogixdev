<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
//        require_once QUBEADMIN . 'models/CampaignModel.php';        
//        require_once QUBEADMIN . 'models/HubModel.php';        
//        require_once QUBEADMIN . 'models/WebsiteModel.php';        
//        require_once QUBEADMIN . 'models/LeadModel.php';        
        require_once HYBRID_PATH . 'controllers/AdminBaseController.php';
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
       
class SystemController extends AdminBaseController
{
    protected $search_q =   NULL;
    
    /**
     * 
     * @param type $view
     * @param type $title
     * @param type $menu
     * @param array $active_classes
     * @param type $classes
     * @return ThemeView
     */
    function getPage($view, $title, $menu = 'system', $active_classes = array(), $classes = array()) {
        $active_classes[]   =   'system-nav-icon';
        
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }
    
    function QueryEvents(){
        return $this->q->queryObjects('HistoryEventModel')->orderBy('T.ID DESC');
    }
    
    function viewCronEvents(VarContainer $C){
        
        if($C->checkValue('CLEAR', $clearcron)){
            switch($clearcron){
                case 'proc_resellerema':
                case 'proc_collabemail':                    
                    $this->db->beginTransaction();
                    $this->query('SET @numcron = (SELECT count(*) FROM %s WHERE object = "cron" AND object_ID = 0 AND event = "%s" AND data = "")', 'HistoryEventModel', $clearcron);
                    $this->query('INSERT INTO %s (user_ID, object, event, data, value, context) VALUES(dashboard_userid(), "system", "cron_clear", "User: %s, IP: %s HAS CLEARED SYSTEM CRON. ", 
                                    @numcron, "%s")',
                                'HistoryEventModel', $this->User->username, $_SERVER['REMOTE_ADDR'], $clearcron);
                    $this->query('DELETE FROM %s WHERE object = "cron" AND object_ID = 0 AND event = "%s" AND data = ""', 'HistoryEventModel', $clearcron);
                    $this->db->commit();
                    break;
            }
        }
        
        $R  =   $this->QueryEvents()->Where('object = "cron" AND object_ID = 0')->Limit(40)->Exec();
        
        $Page   =   $this->getPage('system-cron', 'Cron Events');
        $Page->init('cron', $R->fetchAll());
        return $Page;
    }
    
    static function queryRoleObjects(Qube $q){
        return $q->queryObjects('QubeRoleModel')
                ->Where('reseller_user = 0');
    }
    
    function  viewRoles(){
        $uda = UserDataAccess::FromController($this);
        $roles	= $uda->getRoles();
				$Accounts = $uda->getAllParentUsers('u.id as id, ifnull(company, "Unknown Company") as company', PDO::FETCH_CLASS, 'UserModel');
        
        $P  =   $this->getPage('system-roles', "System User Roles");
				$P->init('MainAccounts', $Accounts);
        $P->init('Roles', $roles);
        $P->init('totalRoles', $uda->CountRoles());
        $P->init('AllPermissions', $uda->getPermissions());
        $P->init('controller', 'admin');
        return $P;
    }
    
    static function getAllPermissions(Qube $q){
        return $q->queryObjects('QubePermissionModel')->Exec();
    }
    
    function viewSystemEvents($status   =   '')
    {
        $Q  =   $this->QueryEvents()->Limit(10)->calc_found_rows();
        
            $Q->leftJoin(array('users', 'U'), 'U.id = T.user_ID')->addFields('coalesce(U.username, "System") as username');
            
        $R  =   $Q->Exec();
        $totalEvents = $this->query('SELECT FOUND_ROWS()')->fetchColumn();
        
        $V  =   $this->getPage('system-events', "System Events");
        
        $V->init('events', $R->fetchAll());
        $V->init('totalEvents', $totalEvents);
        return $V;
    }
    
		function viewSystemConfig()
		{
        if($deny = $this->Protect('is_system'))
                return $deny;
				
				$SDA	=	new SystemDataAccess($this->getQube());				
				
				$configsstr	=	$SDA->getConfigs(
							new DBWhereExpression()
							//DBWhereExpression::Create("col LIKE 'spam_%'")
							, 0, 'valstr');
				$configsint	=	$SDA->getConfigs(
							new DBWhereExpression()
							//DBWhereExpression::Create("col LIKE 'spam_%'")
							, 0, 'valint');
				
				$configs	=	$configsint + $configsstr;
				$P = $this->getPage('system-config', 'System Configuration');
				
				$P->init('sysconfig', $configs);
				return $P;
		}
		
    function viewResellers()
    {
        if($deny = $this->Protect('is_system'))
                return $deny;
        
        $Resellers  =   $this->queryResellers()
                    ->addFields('u.username, u.last_login, concat(i.firstname, " ", i.lastname) as full_name')
                    ->orderBy('id')
                    ->calc_found_rows()
                    ->Limit(10)
                    ->leftJoin('user_info i', 'i.user_id = u.id')->Exec()->fetchAll();

        $totalResllers = $this->query('SELECT FOUND_ROWS()')->fetchColumn();
        
        $Page   =   $this->getPage('system-resellers', 'Resellers');
        $Page->init('Resellers', $Resellers);
        $Page->init('totalResellers', $totalResllers);
        
        return $Page;
    }
    
    function queryResellers(){
        return $this->q->queryObjects('ResellerModel')
                    ->leftJoin(array('UserModel', 'u'), 'u.id = T.admin_user')
                    ->addFields('u.username');
    }
    /**
     * 
     * @param int $reseller_ID
     * @return ResellerModel
     */
    function getReseller($reseller_ID = null){
        $Reseller  =   $this->queryResellers()->Where('T.id = %d', $reseller_ID)
                    ->addFields('w.name as site_name')
                    ->leftJoin(array('WebsiteModel', 'w'), 'w.id = T.main_site_id')
                    ->Exec()->fetch();
        return $Reseller;
    }
    
    function viewReseller(ResellerModel $Reseller)
    {

        $Page = $this->getPage('system-reseller', 'Reseller');
		if($this->getUser()->can('is_system')){
            //changed getMainStorageURL to return path without /users/
			$Page->init('storagePath', $Reseller->getMainStorageURL());
		}
        $Page->init('reseller', $Reseller);

        return $Page;
    }
    
    function ajaxSaveReseller(array $data){
		if(!isset($data['reseller']))
				throw new QubeException('Security error.');
		
		//$ID	=	isset($data['reseller']['ID']) ? (int)$data['reseller']['ID'] : 0;
        $data['reseller']['id']	=	isset($_GET['ID']) ? (int)$_GET['ID'] : 0;
		
        $MM = new ModelManager();
        $result =   $MM->doSaveReseller($data['reseller'],  $data['reseller']['id'], QubeDataAccess::FromController($this));

        
        if($result instanceof ValidationStack){
            return $this->returnValidationError($result);
        }else{
            unset($this->Session->ResellerData);
        }
        
        return $this->getAjaxView($result);
    }

	function ajaxCreateNew($postdata, $id = 0)
	{
		if(isset($_POST['reseller'])){
			return $this->ajaxSaveReseller($postdata);
		}
		
//		$post		=	VarContainer::getContainer($postdata);
		
		if($this->isAJAX() && is_array($_POST['system']))
		{
			$SDA	= SystemDataAccess::FromController($this);
			$limit	=	50;
			foreach($_POST['system'] as $key => $val)
			{
				if(!preg_match('/^(valint|valstr)_(.*?)$/', $key, $m)) continue;
				
				// quick and dirty protection. @todo improve ?
				if(--$limit < 0)
					throw new QubeException('Fatal Limit Reached.');


				$SDA->saveConfig($m[2], $val, 0, $m[1]);
			}
			return $this->getAjaxView(array('error' => false));
		}
				
		if(isset($_POST['sub']))
		{
			$savedata	=	$postdata['sub'];
			$subMgr	=	new SubscriptionManager();
			$subDA	=	UserDataAccess::FromController($this);
			
			$result	=	$subMgr->saveSubscription($savedata, $id, $subDA);			
		}

        if ($result instanceof ValidationStack)
            return $this->returnValidationError($result);

        $result['params'] = $postdata;
        return $this->getAjaxView($result);
	}
	
    function ExecuteAction($action = NULL) {
        if($deny = $this->Protect('is_system'))
			return $deny;


        $this->setAdminCrumb($this->getCrumbsRoot()->addCrumbTxt('UserManager_list', 'Users'));
            
        switch ($action){
            case 'removeSubscription':
                return $this->removeSubscription($_GET['sub_id']);
			case 'systemconfig':
				return $this->viewSystemConfig();
						
            case 'save':
				return $this->ajaxCreateNew($_POST, isset($_GET['ID']) ? (int)$_GET['ID'] : 0);
				
            case 'ajaxResellers':
                return $this->ajaxResellers(VarContainer::getContainer($_POST));

			case 'ajaxAccount-limits':
				return $this->ajaxAccountLimits(VarContainer::getContainer($_POST));
            case 'reseller':
                return $this->viewReseller($this->getReseller($_GET['ID']));
                
            case 'resellers':
                return $this->viewResellers();
                
            case 'newrole':
                return $this->ajaxCreateNewRole(new VarContainer($_POST['role']));
                
            case 'cron':
                return $this->viewCronEvents(new VarContainer($_GET));
                
            case 'roles':
                return $this->viewRoles();
			case 'account-limits':
				return $this->viewAccountLimits();
            case 'debug':
                return $this->viewDebug();
            case 'ajaxSystemEvents':
                return $this->ajaxSystemEvents(VarContainer::getContainer($_POST));
				
			case 'subscriptions':
				return $this->viewSubscriptions();
        }
        
        return $this->viewSystemEvents();
    }
	
	function viewSubscriptions()
	{
		// trick for fetching into [key] => $obj array
		$subscriptions	=	array_map('reset', $this->getQube()->queryObjects('SubscriptionModel')->Exec()->fetchAll(PDO::FETCH_GROUP));
		
        $V  =   $this->getPage('system-subscriptionsettings', 'System Info');
		$V->init('SubscriptionPlans', $subscriptions);
        $V->init('userobj', $User); //        
        return $V;
		
        $this->getCrumbsRoot()->addCrumbTxt('admin_billing', 'Billing Profile');
		$Page	=	$this->getPage('billing-profile', 'Billing Profile');
		
		return $Page;
	}
    
    function viewDebug(){
        return $this->getPage('system-debug', 'Debug');
    }
    
    /**
     * This function is only available to System Admins, Support Admins, and Resellers
     * 
     * @param VarContainer $VC
     * @return type
     * @throws Exception
     */
    function ajaxCreateNewRole(VarContainer $VC){
      $MM		= new ModelManager();
			$uda	=	UserDataAccess::FromController($this);
			$result = $MM->doSaveRole($VC, $VC->getValue('ID', 0), $uda);
		
		
			if($result instanceof ValidationStack)
				return $this->returnValidationError($result);
		
		$newrole	=	$uda->getRoles($result->getID());
		
//		$ajax	=	array();		
//		$result['object'] =   $newrole;
		$result['is_update']  =   $VC->exists('ID') !== FALSE;
		$result['row']    =   $this->getView('rows/system-role', false, false)
			->init('Role', $newrole)->toString();
		
		return $this->getAjaxView($result);
    }
    
    function ajaxSystemEvents(VarContainer $C){
        $queryEvents = $this->QueryEvents()->calc_found_rows();
        $queryEvents->leftJoin(array('users', 'U'), 'U.id = T.user_ID')->addFields('coalesce(U.username, "System") as username');
        if($C->checkValue('sSearch', $search) && $search != ''){
            if(!is_numeric($search)){
                $queryEvents->Where('coalesce(U.username, "System") LIKE :search OR T.object LIKE :search OR T.Event LIKE :search');
                    $qparams[':search']  =   '%' . $search . '%';
            }else{
                $queryEvents->Where('T.id = :search');
                $qparams[':search']  =   $search;
            }
        }
        $C->load(array('table' => 'Events'));
        $DT = new DataTableResult($queryEvents, $C);
        $DT->setRowCompiler($this->getView('rows/system-events', false, false), 'event');
        
        return $this->getAjaxView($DT->getArray($qparams));
    }

    function ajaxResellers(VarContainer $C){
        $C->load(array('table' => 'Resellers'));
        $query = $this->queryResellers()
            ->addFields('u.username, u.last_login, concat(i.firstname, " ", i.lastname) as full_name')
            ->leftJoin('user_info i', 'i.user_id = u.id')
            ->calc_found_rows();
        $args = array();
        if($C->checkValue('sSearch', $search) && !empty($search)){
            $query->Where('T.id = :search OR
                           T.company LIKE CONCAT("%", :search, "%") OR
                           CONCAT(i.firstname, " ", i.lastname) LIKE CONCAT("%", :search, "%") OR
                           T.support_email LIKE CONCAT("%", :search, "%")'
            );
            $args['search'] = $search;
        }

        $DT = new DataTableResult($query, $C);
        $rowTemplate = $this->getView('rows/system-reseller',false, false);

        $DT->setRowCompiler($rowTemplate, 'Reseller');

        return $this->getAjaxView($DT->getArray($args));

    }

	function viewAccountLimits(){
		$sda= UserDataAccess::FromController($this);
		$totalLimits = $sda->CountAccountLimits();
		$limits = $sda->getAllAccountLimits();
		$V = $this->getPage('system-account-limits', 'Account Limits');
		$V->init('totalLimits', $totalLimits);
		$V->init('accounts', $limits);
		return $V;
	}

	function ajaxAccountLimits(VarContainer $C){
        $DT = new DataAccessTableResult(UserDataAccess::FromController($this), $C, 'AccountLimits', 'getAllAccountLimits', 'CountAccountLimits');
//		$DT =   new DataTableAccountLimitsResult(UserDataAccess::FromController($this), $C);
		$DT->setRowCompiler($this->getView('rows/system-account-limits', false, false), 'limit');

		return $this->getAjaxView($DT->getArray());
	}

    function removeSubscription($sub_id){
        //@todo amado
    }
}
