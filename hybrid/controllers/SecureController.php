<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once 'HybridBaseController.php';

/**
 * Base controlelr in charge of managing user authentication and session management.
 * 
 */
abstract class SecureController extends HybridBaseController
{
    
    protected $permissions  =   array();    
    
    /**
     *
     * set to true in preExecute() to skip loading of user data
     * 
     * @var bool
     */
    protected $no_queries   =   false;
    
    /**
     *
     * @var ThemeView
     */
    protected $_reject_redirect =   NULL;
            
    function hasPermission($p)
    {
        if($p   ==  'is_system')
            return $this->permissions[$p];
    }
    
    function Protect($permission, $redirect = 'Auth', $id = 0){
        if($permission == 'deny' || !$this->getUser()->can($permission, $id))
		{
			$this->onEvent('onPermissionDenied', $this->getUser(), $permission);
            return $this->ActionRedirect ($redirect);
		}
        return false;
    }
    
    function setPermission($p, $v)
    {
        $this->permissions[$p]   =   $v;
    }
    
    static function validateResellerHostname(Qube $q, $httphost)
    {
        static $prepare =   null;
        if(!$prepare)
            $prepare=   $q->queryObjects('BrandingModel')->Where('main_site = :host') // or :host LIKE concat("%.", main_site)')   //->Fields('id, admin_user')
                ->Prepare();
        
        if(!$prepare->execute(array('host' => $httphost)) || $prepare->rowCount() ==  0)
        {
            QubeEvent::Trigger('SecureController.HostNotFound', $httphost);
            return false;
        }
        
        // @todo enable hub login??
        // @todo redirect to reseller site based on hub?
        return $prepare->fetch();
    }
    
    protected function findReseller()
    {
        $User   =   $this->getUser();
        // if User is super_user > 0, resller = true. if User->reseller_client, true. 
//        $has_reseller    =   $this->User->reseller_client == 1 ;
        
        // @todo if is_super, use $_POST or $domain variables to fetch the reseller id
        
        // 1 -> (reseller admin)
        // super_user =2 -> second_validation
        // info: super_user = 3 -> suppoert user
        // 4 -> reseller support user. (support_user = true) parent_id = *reseller*
        // 5 -> fuck knows (same as 6)
        // 6 -> fuck knows
        
        $is_reseller    =   ($this->User->parent_id  ==  $this->User->id || $this->User->parent_id == 0);
        
        $admin_user_reseller        =   $is_reseller ? $this->User->id : $this->User->parent_id; // if regular user, get reseller id from their parent_id
            
        if($is_reseller){
            $is_super       =   $this->User->super_user;
            
            if($is_super > 0)
            {
                // @todo load admin_user somehow some way, and simulate being that user.. 
                if($is_super == 1)  // reseller flag
                {
                    $admin_user_reseller    =   $this->User->id;
                }
                if($is_super == 2)  // SYSEM user flag
                {
                    // @todo do second validation
                    // if no reseller id, login to main site
                    // if reseller id, simulate that user
                }
                if($is_super == 3)  // SYSTEM SUPPORT USER flag
                {
                    // if no reseller id, login to main site
                    // if reseller id, simulate that user
                }
                if($is_super > 3)   // crazy shit.. no idea here
                {
                    
                }
            }else{
                
                if($this->User->sixqube_client == 1)
                {
                    $admin_user_reseller    =   7;          // DEFAULT 6QUBE RESELLER USER.
                }else{
#			var_dump($this, $is_super, $this->User, $this->User->super_user);
                    Qube::Fatal ('Could Not Login User: (is_reseller=1, $is_super !> 0, sixqube_client != 1)', $this->User);
		}
            }
        }
        /*
        $Q  =   $this->q->queryObjects('BrandingModel')->Where('admin_user = %d', $admin_user_reseller);
        $this->ResellerData =   $Q->Exec()->fetch();
         * 
         * 
         */
    }    
    
    function loadPermissions(){
        if(!$this->User) throw new QubeException();
#        var_dump($this->User->can('lickmynuts'), $this->User->can('is_system'));
    }
    
    /**
     * 
     * @param type $token_ID
     * @return QubeTokenModel
     */
    private function getTokenObject($token_ID){
        $stmt   =   Qube::GetPDO()->prepare('SELECT "QubeTokenModel", t.* FROM 6qube_tokens t WHERE ID = ? 
                AND expiration > NOW() AND permission IN ("user_login", "user_temp") AND used = 0');
        if(!$stmt->execute(array($token_ID)) || $stmt->rowCount() != 1) return false;
        
        return $stmt->fetch(PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
    }
    
    private function acceptTokens($action){        
        if(isset($_GET['token']))
        {
            $token  =   $this->getTokenObject($_GET['token']);
            if($token){
                $this->processToken($token, $action);
            }
        }
    }

    /**
     * Return false for no error.
     * Return a view for an error.
     *
     * @return bool|HttpRedirectView
     */
    private function authenticate(){
        
        $fail   =   false;
        $redirect_arguments = array();
        
        try{
					if(!$this->User){
						if($this->Session->AccessUser instanceof UserWithRolesModel)	// try to load from session
						{
							$this->User	=	$this->Session->AccessUser;
						}elseif($this->loadUser())	// try to load from user_id
						{
							$this->Session->AccessUser	=	$this->User;
						}else{			// failed to load user
							$fail	=	true;
						}
					}
                    if($this->User && !$this->User->hasRole() && !$this->User->isAdmin() && !$this->User->isSystem()){
                        $fail = true;
                        $redirect_arguments['E_Msg'] = 'E10'; // Message to show up in login redirect, defined in AuthController $MessageArray.
                                                            //   It may be a predefined message, or a custom message. i.e. 'E10' or 'Error, Login Failed'
                    }
//            $fail   =   !$this->User && !$this->loadUser();
        }catch(QubeException $failerror){
            $fail   =   true;
        }
        
        if($fail){
            // redirect to auth_login
            $this->Session->login_redirect =   $this->Session->REQUEST_URI;
            return $this->ActionRedirect ('Auth_login', $redirect_arguments);
        }
        
        return false;
    }

    /**
     * Use logged in users parent_id to verify the admin's subscription status
     *
     * @return SubscriptionModel
     * @throws QubeException
     */
    function verifyUserAccountAccess()
    {
        $uda = UserDataAccess::FromController($this);
        $sm = $uda->getUserUnExpiredTOTALLimit($this->getUser()->getParentID());

        return $sm;
    }
        
    function preExecute($action) {
        // accept tokens before loading branding model (token may override branding)
        $this->acceptTokens($action);
        parent::preExecute($action);
        
        if($authfail = $this->authenticate())
                return $authfail;
                
        // user_id is NOT empty, therefore user is logged in and we can proceed.
        // leave data security up to sub controllers
        if($this->no_queries) return NULL;

        if($action != 'billing-subscription' && !$this->getUser()->isSystem()) {
            $r = $this->verifyUserAccountAccess();
            if (FALSE === $r) {
                return $this->ActionRedirect('Admin_billing-subscription');
            }
        }
        $this->loadPermissions();

        return NULL;
    }
    
    function setUserId($userID){
        $this->user_ID  =   $userID;
    }
    
    function loadResellerData($resellerID){
        $this->ResellerData =   $this->q->queryObjects('BrandingModel')
                    ->Where('admin_user = %d', $resellerID)->Exec()->fetch();
        
        $this->Session->ResellerData   =   $this->ResellerData;
    }
}
