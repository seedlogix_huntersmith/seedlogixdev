<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';        
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class SocialMediaController extends DashboardBaseController
{

function ViewDashboard()
{
  $V	=	$this->getView('socialMedia-dashboard');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_dashboard', "dashboard");
  return $V;
}

function ViewEditRegions()
{
  $V	=	$this->getView('socialMedia-editRegions');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_editRegions', "edit regions");
  return $V;
}
function ViewPost()
{
  $V	=	$this->getView('socialMedia-post');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_post', "post");
  return $V;
}
function ViewEdit()
{
  $V	=	$this->getView('socialMedias-edit');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_edit', "edit");
  return $V;
}
function ViewSeo()
{
  $V	=	$this->getView('socialMedia-seo');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_seo', "seo");
  return $V;
}
function ViewSettings()
{
  $V	=	$this->getView('socialMedia-settings');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_settings', "Settings");
  return $V;
}

function ViewHome()
{
  $V	=	$this->getView('socialMedia-home');
  $V->init('menu', 'socialMedia-dashboard');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_home', "Home");
  return $V;
}

function ViewSocial()
{
  $V	=	$this->getView('socialMedia-social');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_social', "Social");
  return $V;
}

function ViewTheme()
{
  $V	=	$this->getView('socialMedia-theme');
  $V->init('menu', 'socialMedia');
  $V->init('_title', 'SocialMedia Page');
  $this->getCrumbsRoot()->addCrumbTxt('SocialMedia_theme', "Theme");
  return $V;
}

function prepareView(ThemeView &$view){

	parent::prepareView($view);

        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.cid = @dashboard_campaignid');
        
        $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

        $view->init('ChartWidget', $ChartWidget);
            return $view;
}

    function ExecuteAction($action = NULL) {
        
        switch($action)
        {
		case 'dashboard':
		case 'editRegions':
		case 'post':
		case 'edit':
		case 'seo':
		case 'settings':
		case 'home':
		case 'social':
		case 'theme':
			$func	=	"View" . ucwords($action);
		return call_user_func(array($this, $func));	//$this-$func;

		break;		
        }        
    }
}
