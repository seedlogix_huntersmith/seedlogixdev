<?php

        require_once HYBRID_PATH . 'controllers/DashboardBaseController.php';

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
        
abstract class AdminBaseController extends DashboardBaseController
{        
    function getCrumbsRoot($is_campaign_report = true, $not_mu = true, $c_id = null, $c_name = null) {
        if(!$this->crumbs)
        $this->crumbs   =    new BreadCrumb('Admin_dashboard', 'Administration');
        return $this->crumbs;
    }
    
    function prepareView(\ThemeView &$view) {
        parent::prepareView($view);
    
        $template   =   $view->getTemplate();
        
        
        
        $crumb  =   $this->getCrumbsRoot()->getNextCrumb();
        
        if(!$crumb) $crumb  =   $this->crumbs;
        
				if($this->nav)
				{
					// @todo remove thos block/if
						throw new Exception('dEPRECATED!');
					foreach($this->nav as $navitem):
							$crumb->addChild($navitem[3], $navitem[2]);
					endforeach;        
				}
        //$view->init('navmenu', $this->nav); @todo remove $this-nav, being controlled in menus > admin.php
        return $view;
    }
    
    function setAdminCrumb(BreadCrumb $b)
    {
        return $b;
    }

}