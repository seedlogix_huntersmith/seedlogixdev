<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';        
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class BlogsController extends DashboardBaseController
{

function ViewDashboard()
{
  $V	=	$this->getView('blog-dashboard');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_dashboard', "dashboard");
  return $V;
}

function ViewEditRegions()
{
  $V	=	$this->getView('blog-editRegions');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_editRegions', "edit regions");
  return $V;
}
function ViewPost()
{
  $V	=	$this->getView('blog-post');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_post', "post");
  return $V;
}
function ViewEdit()
{
  $V	=	$this->getView('blogs-edit');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_edit', "edit");
  return $V;
}
function ViewSeo()
{
  $V	=	$this->getView('blog-seo');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_seo', "seo");
  return $V;
}
function ViewSettings()
{
  $V	=	$this->getView('blog-settings');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_settings', "Settings");
  return $V;
}

function ViewHome()
{
  $V	=	$this->getView('blogs-home');
  $V->init('menu', 'blog-dashboard');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_home', "Home");
  return $V;
}

function ViewSocial()
{
  $V	=	$this->getView('blog-social');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_social', "Social");
  return $V;
}

function ViewTheme()
{
  $V	=	$this->getView('blog-theme');
  $V->init('menu', 'blog');
  $V->init('_title', 'Blogs Page');
  $this->getCrumbsRoot()->addCrumbTxt('Blogs_theme', "Theme");
  return $V;
}

function prepareView(ThemeView &$view){

	parent::prepareView($view);

        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.cid = @dashboard_campaignid');
        
        $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

        $view->init('ChartWidget', $ChartWidget);
            return $view;
}

    function ExecuteAction($action = NULL) {
        
        switch($action)
        {
		case 'dashboard':
		case 'editRegions':
		case 'post':
		case 'edit':
		case 'seo':
		case 'settings':
		case 'home':
		case 'social':
		case 'theme':
			$func	=	"View" . ucwords($action);
		return call_user_func(array($this, $func));	//$this-$func;

		break;		
        }        
    }
}
