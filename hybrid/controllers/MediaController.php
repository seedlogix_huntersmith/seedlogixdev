<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';        
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class MediaController extends DashboardBaseController
{
    const EXTENSIONS    =   'gif,jpg,jpeg,png,bmp,swf,dcr,mov,qt,ram,rm,avi,mpg,mpeg,asf,flv';
    const binary_path   =   '../admin/js/tiny_mce/plugins/imagemanager/stream/index.php';//?cmd=im.thumb&path={0}/'
    const thumbnail_path    =   '/admin/js/tiny_mce/plugins/imagemanager/stream/index.php?cmd=im.thumb&path={0}/';
    
    protected $search_q =   NULL;
    
    function viewCampaignDashboard($campaign_ID)
    {
        $this->query('SET @dashboard_campaignid = ' . (int)$campaign_ID);
        
        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.cid = @dashboard_campaignid');
        
        $df =   $this->query('SELECT * FROM dashboard_sites_analytics_summary ORDER BY nb_visits DESC');
        $WebsiteAnalytics  =   $df->fetchAll(PDO::FETCH_OBJ);
        
        $df =   $this->query('SELECT * FROM dashboard_pages_analytics_summary ORDER BY convert_rate DESC LIMIT 5 ');
        $PagesAnalytics  =   $df->fetchAll(PDO::FETCH_OBJ);
        
            $campaign   = $this->q->queryObjects('CampaignModel')->Where('id = %d and user_id = dashboard_userid()', $campaign_ID)->Exec()->fetch();
            if($campaign === false)
            {
                var_dump($campaign);
                die('facuk off'); // @todo
            }
            
            $Websites    =   $this->q->queryObjects('WebsiteModel')->Where('cid = %d and user_id = dashboard_userid()', $campaign_ID)->Exec()->fetchAll();
            if($Websites === false)
            {
                var_dump($Website);
                die('fauck off');   // @todo
            }
            
            $V  = $this->getView('dashboard-websites');

            $V->init('graph_data', array($analytics->getVisitsGraphData('-20 DAY'),
                                    $analytics->getLeadsGraphData('-20 DAY')));
            
            // important to purge temporary table before adding quickstats
            $analytics->purgeTemporaryTable();            
            $this->addQuickStats($analytics);
            
            $this->getCrumbsRoot()->addCrumb($this->getCrumbsCampaigns((int)$campaign_ID));
            
            $V->init('prospects', $this->queryLeads('cid = @dashboard_campaignid')->orderBy('created DESC')
                                        ->Limit(20)->Exec()->fetchAll());
            
            
            $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

            $V->init('ChartWidget', $ChartWidget);
//            $V->init('reseller', $_SESSION['reseller']);
//            $V->init('resellerParentID', $_SESSION['parent_id']);
            $V->init('items', $C);
            $V->init('sites', $WebsiteAnalytics);
            $V->init('pages', $PagesAnalytics);
            $V->init('Campaign', $campaign);
            $V->init('_title', 'Campaign: ' . $campaign->name);
            $V->init('menu', 'campaign');
            return $V;
    }
        
    /**
     * returns an array of queries necessary to generate a temporary table with graph values (dashboard_tempvisits)
     * @param type $t1
     * @param type $t2
     * @param type $name
     * @param type $hub_where_str
     * @param PDO $db
     * @return string|int
     */
    function getArchiveQueries($t1, $t2, $name  =   'nb_visits', $hub_where_str)
    {
        $m1 =   strtotime(date('Y-m-1', $t1));
        $m2 =   strtotime(date('Y-m-t', $t2));
        
        $xt =   $m1;
//        $t2 =   strtotime('+1 MONTH', $t2); // add 1 month
        $mi =   0;
        $qs =   array('CREATE TEMPORARY TABLE dashboard_summary_trackingids as select tracking_id, id as hub_id from ' .
                $this->q->getTable('WebsiteModel') . ' WHERE ' . DBWhereExpression::Create($hub_where_str));
        
        do{
            $mi++;
            $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . date('Y_m', $xt);
            $alias  =   "a$mi";
            $q   =   ($mi == 1 ? ' CREATE TEMPORARY TABLE dashboard_tempvisits AS ' : ' INSERT INTO dashboard_tempvisits ') .
                        "SELECT date1, SUM(value) as value FROM $tbl WHERE idsite IN (SELECT tracking_id FROM dashboard_summary_trackingids) AND
                    name    =   '$name' AND date1 = date2 " . ($mi == 1 ? 
                        ' AND date1 >= FROM_UNIXTIME(' . $t1 . ') ' : '') . " GROUP BY date1";
            $qs[]   = $q;
            /*
            $q->Join(array($tbl, $alias),
                        "$alias.idsite = s.tracking_id AND $alias.name = '$name'");
             //             */
        }while(($xt = strtotime("+1 MONTH", $xt))    <  $m2);
        
#        $qs[]   =   str_replace('GROUP BY', ' AND array_pop($qs) 
#        $qs[]   =   'SELECT date1, SUM(value) FROM dashboard_temp GROUP BY date1';
        
        return $qs;
        
        $value_colm =   array();
        for($i = 1; $i <= $mi; $i++)
        {
            $value_colm[]   =   "coalesce(a$i.value, 0)";
        }
        
        $q->addFields(join('+', $value_colm) . ' as value');
        
        return $mi;
    }
    
    /**
     * Pass a Cid, hub id, and params (interval => 7, period => (day, month, week, year)
     * 
     * @param type $CID
     * @param type $HID
     * @param type $params
     * @return type
     */
    function ajaxGraphData($CID, $HID, $params)
    {
        if($CID > 0)
            $this->query('SET @dashboard_campaignid = ' . (int)$CID);
        
        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.user_id = @dashboard_userid AND (ISNULL(@dashboard_campaignid) OR t.cid = @dashboard_campaignid) ' . (empty($HID) ? '' : ' AND id =' . (int)$HID));
        
        $interval   =   max(1, (int)$params['interval']);
        $period       =   preg_match('/^(DAY|WEEK|MONTH|YEAR)$/', $params['period']) ? $params['period'] : 'DAY';
        
        $offset =   "-$interval $period";
        try{
            $ajax   =   array('params' => $params,
                                'data' => array($analytics->getVisitsGraphData($offset),
                                        $analytics->getLeadsGraphData($offset))
                                );

            setcookie('web_chartint', $interval);
            setcookie('web_chartunit', $period);

        }catch(PDOException $e)
        {
            $ajax=  array('error' => 'error');
        }
        return $this->getAjaxView($ajax);
    }
    
    function viewMainDashboard()
    {        
        $user_id    =   $this->user_ID;
        
        // GET CAMPAIGNS WITH CURRENT DAY VISITS / LEADS
        $campaigns   =   Qube::Start()->queryObjects('CampaignModel')
                            ->Fields('T.id, T.name, DATE_FORMAT(date_created, "%M %D, %Y") as created_str, 
                                    sum(ifnull(s.nb_visits, 0)) as nb_visits, sum(ifnull(s.numleads, 0)) as numleads, count(s.id) as nb_touchpoints')
                            ->Where('T.user_id = %d', $user_id)
                            ->leftJoin(array(Qube::PIWIKDB . '.dashboard_pages_analytics_summary', 's'), 's.cid = T.id')
                                ->groupBy('T.id')
                                ->asSubSelect('c')->Fields('c.id, c.name, c.created_str, c.nb_touchpoints, c.nb_visits, c.numleads, if(
                                        c.nb_visits > 0, c.numleads / c.nb_visits, c.numleads) as convert_rate')
                            ->Exec()->fetchAll();
                
        $V  =  $this->getView('dashboard-campaigns');
        
        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.user_id = @dashboard_userid');
        
            $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

            $V->init('ChartWidget', $ChartWidget);

//            var_dump($campaigns);
            $V->init('campaigns', $campaigns);
            $V->init('items', $C);
            $V->init('_title', 'Campaigns Dashboard');
            
            $analytics->purgeTemporaryTable();
            $this->addQuickStats($analytics);
                    
            $V->init('menu', 'home');
            
#        var_dump($V);
        return $V;
    }
    
    function ajaxCreateNew($params)
    {
        if(isset($params['website']))
        {
            if(TRUE){   //WebsiteModel::Validate($params['website'], $errors)){
                // @todo validation

                $params['website']['user_id']   =   $this->user_ID;
                $params['website']['user_parent_id']   =   $this->user_ID;  // @todo
                $site   =   $this->q->Save(new WebsiteModel, $params['website']);
                $site->actions  =   array('trash' => '#trash',
                                            'edit' => '#edit',
                                                'dashboard' => $this->ActionUrl('Website_website', array('ID' => $site->id)));
            
                return $this->getAjaxView(array('error' => 0, 'result'  => $site));
                
            }else
                $this->getAjaxView(array('error' => 'validation', 'validation' => $errors));
        }
        
        if(isset($params['campaign']))
        {
            // @todo validate
            
            $params['campaign']['user_id']  =   $this->user_ID;
            $params['campaign']['default_snapshot_id']  =   0;
            $campaign   =   $this->q->Save(new CampaignModel, $params['campaign']); //(object)$params['campaign'];
            
//            var_dump($campaign);
            $campaign->actions  =   array('trash' => '#trash',
                                            'edit' => '#edit',
                                                'dashboard' => $this->ActionUrl('Campaign_dashboard', array('ID' => $campaign->id)));
            
            return $this->getAjaxView(array('error' => 0, 'result' => $campaign));
        }
        
        return $this->getAjaxView($params);
    }
    
    function ExecuteAction($action = NULL) {
        throw new Exception("Media manager not supported");
        switch($action)
        {
            case 'search':
                    $this->search_q =   $_POST['q'];
                    return $this->getAjaxMediaItems();
                break;
            
            case 'new':
                return $this->ajaxCreateNew($_POST);
                
            case 'graphdata':
                return $this->ajaxGraphData(@$_GET['CID'], @$_GET['ID'], $_POST);
                
            case 'dashboard':   // campaign dashboard
                return $this->viewCampaignDashboard($_GET['ID']);
                break;
        }
        
        return $this->viewManager();
    }
    
    function getMediaItems($dir =   'hub', &$fp =   null)
    {
        $match_extensions =   '/^.*?(' . str_replace(',', '|', self::EXTENSIONS) . ')$/';
        
        $home_dir   =   $this->User->getStoragePath();
        $list   =   array();
        if($rHandle =   opendir($home_dir . '/' . $dir))
        {
            while(false !== ($file = readdir($rHandle)))
            {
                if($file == '.' || $file    ==  '..') continue;
                $fpath  =   $file;
                
                if(preg_match($match_extensions, $file) && $this->applySearch($file))
                {
                    $list[] =   $file;
#                    if($fp)                        $fp[$file]   =   $fpath;
                }
            }
        }
        
        return $list;
    }
    
    function getAjaxMediaItems()
    {
        $items  =   $this->getMediaItems();
        
        $response   =   array('items' => $items, 'thumbs' => self::thumbnail_path . '--FILE--');
        
        return $this->getAjaxView($response);
    }
    
    private function applySearch($file)
    {
        return !isset($this->search_q) || empty($this->search_q) || strpos($file, $this->search_q) !== false;
    }
    
    function viewManager()
    {
        $files  =   $this->getMediaItems();
        
        $V  =   $this->getView('mediamanager');
        $V->init('_title', 'Media Manager');
        $V->init('menu', 'media');
        $V->init('images', $files);
        
        return $V;
    }
}