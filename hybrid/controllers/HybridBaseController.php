<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

abstract class HybridBaseController extends BaseController
{
	/**
	 *
	 * @var Qube\Hybrid\Events\SystemEventDispatch
	 */
	protected $eventDispatch	=	NULL;
	
    protected $action =   NULL;
    
    /**
     *
     * @var BrandingModel
     */
    protected $ResellerData =   NULL;
    
    /**
     *
     * @var Qube
     */
    protected $q;
    
    /**
     *
     * @var PDO
     */
    protected $db;
    /**
     * The ID of the user who will have ownership of the data being managed.
     * This is different than session_user_ID for support admins (loggedin_user_id, session_user_ID) managing the reseller's account (user_ID).
     * This is also different for sub-users managing (viewing) their parent user's account.
     *
     * @var type 
     */
    protected $user_ID  =   NULL;
    /**
     * The Logged In User's ID.
     *
     * 
     * @var int
     */
    private $session_user_ID  =   NULL;
    
    /**
     *
     * @var UserWithRolesModel
     */
    protected $User     =   NULL;
    
    /**
     *
     * @var HybridBaseController
     */
    protected $ParentController =   NULL;
    
    
    /**
     *
     * @var Zend_Session_Namespace
     */
    protected $Session  =   NULL;

	/**
	 *
	 * @param string $page
	 * @param string $header
	 * @param string $footer
	 * @return ThemeView
	 */
    function getView($page, $header =   'dashboard.php', $footer   =   'dashboard.php') {
        $V  =   parent::getView($page, $header === false ? false : 'headers/' . $header, $footer === false ? false : 'footers/' . $footer);
        $V->init('_path', 'themes/default/');
        
        if($header !== false)
            $this->prepareView($V);
        return $V;
    }

    /**
     * turns a view into a string.
     *
     * @param $view
     * @return string
     */
    function getHtml($view, $vars)
    {
        return $this->getView($view, false, false)->init($vars, NULL)->toString();
    }
    
    function getLoggedInUserID(){
        return $this->session_user_ID;
    }
    
    function setLoggedInUserID($session_user_ID){
        $this->session_user_ID  =   $session_user_ID;
    }
    
    // this goes here temporarily
    /*
     * @return DBSelectQuery
     */
    function queryLeads($where_cond)
    {
        $Q  =   $this->q->queryObjects('LeadModel')
                ->Fields('lead_email, id, lead_name, DATE_FORMAT(created, "%M %D, %Y") as created');
        $args   = func_get_args();
        return call_user_func_array(array($Q, 'Where'), $args);
#                    ->leftJoin()
    }
    
    function preExecute($action) {
        $this->action   =   $action;
        
        $V  =   parent::preExecute($action);
        
        if($V)  return $V;
        
        if(!$this->ResellerData)
            $this->loadBrandingModel();        
    }
    
    protected function loadBrandingModel(){
        
        if(isset($this->Session->ResellerData) && $this->Session->ResellerData instanceof ResellerModel)
        {
            $this->ResellerData =   $this->Session->ResellerData;
        }else{
            $this->ResellerData = SecureController::validateResellerHostname($this->q, $this->Session->HTTP_HOST);
            $this->Session->ResellerData        =   $this->ResellerData;
        }
    }
    
    function getParam($key, $default){
        return empty($_POST[$key]) ? empty($_GET[$key]) ? $default : $_GET[$key] : $_POST[$key];
    }
    
    function __construct($parent_controller =   NULL) {
        
        if($parent_controller instanceof HybridBaseController)
        {
            $this->q    =   $parent_controller->getQube();
            $this->db   =   $parent_controller->db;
            $this->ParentController =   $parent_controller;
//            $this->setSessionUser($parent_controller->getUser());
            
            $this->loadSession($parent_controller->Session);
        }
        if($parent_controller instanceof Zend_Session_Namespace){
            $this->Initialize();
            $this->loadSession($parent_controller);
        }
				
    }
    
    function _action_input($inputid, $action)
    {
        echo '<input type="hidden" id="' . $inputid . '" value="' . $action . '" />';
    }
    
    function isAction($action)
    {
        return func_num_args() > 1 && in_array($this->action, func_get_args()) ? true : $this->action == $action;
    }
    
    function isParam($paramname, $paramvalue)
    {
        return @$_GET[$paramname]    ==  $paramvalue;
    }
		
		function isAJAX()
		{
			return @$_SERVER['HTTP_X_REQUESTED_WITH']	==	'XMLHttpRequest';
		}
    
    function _getaction()
    {
        return $this->action;
    }
    
    function prepareView(ThemeView &$view)
    {
//        parent::prepareView($view);
        $view->getTemplate()->func('isAction', array($this, 'isAction'))
                    ->func('action_input', array($this, '_action_input'))
                    ->func('getaction', array($this, '_getaction'))
                    ->func('isParam', array($this, 'isParam'))
                    ->func('hasAccess', array($this, 'hasPermission'))->init('controller', get_class($this));
        $view->init('User', $this->User);
        
        $view->init('Branding', $this->ResellerData);        
    }
    
    function _rewrite(BaseController $controller, $action, $params = array())
    {
        // action = {controller}_{action}
        @list($actionstr, $query)    =   explode('?', $action);
        list($controller,$action) = explode('_',$actionstr);
        //list($controller,$action) = explode('_',strtolower($actionstr));/* Lowercase everything for URL consistency */
        
        if($query)
        {
            parse_str($query, $qparams);
            $params =   array_merge($params, $qparams);
        }
        
        $params['action'] = $action;
        $params['ctrl']   = $controller;
        
        return 'admin.php?' . http_build_query($params);
        //return 'admin.php?' . http_build_query($params,'','&amp;');/* Encode all ampersands for HTML */
    }   
    
    /**
     * Query the internal Qube object
     * 
     * @param type $q
     * @param type $model
     * @return PDOStatement
     */
    function query($q, $model   =   false)
    {
        return call_user_func_array(array($this->getQube(), 'query'), func_get_args());
    }
    
	/**
	 * 
	 * @return Qube
	 */
    function getQube(){
        return $this->q ? $this->q : Qube::Start();
    }
    
    function processToken(QubeTokenModel $token, $action){
        
    }
    
    function log($action, $args = array())
    {
        switch($action)
        {
            case 'login':
// @disabled                    return new HistoryEvent($args['User']->id, 'User', 'login', $_SERVER);
                break;
        }
    }
    
    function cacheCheck($id, $lifetime){
        return self::getZendCache($lifetime)->load($id);
    }
    
    function cacheSave($data){
        return self::getZendCache()->save($data);
    }
    
    /**
     * 
     * @staticvar string $zendcache
     * @param type $lifetime
     * @return Zend_Cache_Core|Zend_Cache_Frontend
     */
    static function getZendCache($lifetime   =   30){
        static $zendcache   =   NULL;
        if(!$zendcache){
            $frontOpts  =   array('lifetime'    =>  $lifetime, 'automatic_serialization' => true);
            $backOpts   =   array('cache_dir'   =>  QUBEPATH . 'cache' . DIRECTORY_SEPARATOR);
            $zendcache  =   Zend_Cache::factory('Core', 'File', $frontOpts, $backOpts);
        }
        return $zendcache;
    }
    
    function getSession(){
        return $this->Session;
    }
    
    
    /**
     * 
     * Initialize user_ID, session_user_ID, and other objects
     */
    function Initialize() {
        $this->_themes_path =   HYBRID_PATH . '/themes/';
        $this->setRewriter(array($this, '_rewrite'));
        
        
        $this->q    =   Qube::Start();
        $this->db   =   $this->q->GetDB();
    }
    
    /**
     * 
     * @return UserWithRolesModel
     */
    function getUser(){
        if(!$this->User && !$this->loadUser()) throw new QubeException('Error loading user data');
        
        return $this->User;
    }
    
    /**
     * Load the Logged In User Object
     * 
     * 
     */    
    protected function loadUser()
    {
        if(!$this->getLoggedInUserID()) throw new QubeException('no logged in user found.');
		$uda	=	new UserDataAccess($this->getQube());
        $User =   $uda->getUserWithRoles($this->getLoggedInUserID());
        
            // laod the permissions for the session user.
        
        if(!$User) return false;
        $this->setSessionUser($User);
        $this->loadPermissions();
        /*
        
        if($this->User->parent_id   !=  0){     // logged-in user has a parent
            // manage the parent's account
            $this->user_ID  =   $this->User->parent_id; 
            
//             if($this->User->gparent_id !=  0){ // logged in user has a grand parent (session_user->parent->parent_id != 0) 
                    // this means we are a subuser, and we must re
  //           }
        }
        
        if($this->User->parent_id == 0){    // A reseller is logged in managing their own account
            $this->user_ID  =   $this->session_user_ID;
        }else{  // the logged in user has a parent (possibly a reseller, or possibly a normal user.)
            // check the User's parent's parent_id: if (gparent_id = 0 // parent->parent_id = 0 //, then session user could be a support user accessing the reseller's account. 
                // OR a NORMAL user accessing their own data.
            // if parent->parent_id != 0, then the current user has a grand-father (meaning there is a reseller->normaluser->subuser) relationship.
            // and the current user can only be a subuser.
            if($this->user->gparent_id = 0){
                
            }
        }
         * 
         */
  
        return true;
//        $this->Session->User   =   $this->User;
#	var_dump('loadUser:', $this->User);
    }
    
		function getReseller($reseller_ID = null)
		{
			return $this->ResellerData;
		}
		
    function getDataOwnerID(){
        if(!$this->user_ID){    $this->getUser();       }
        return $this->user_ID;
    }
    
    function setDataOwnerID($owner_ID){
        $this->user_ID  =   $owner_ID;
    }
    
    function loadSession(Zend_Session_Namespace $Session)
    {
        $this->Session  =   $Session;
				if($this->Session->AccessUser instanceof UserWithRolesModel && !defined('DISABLE_USERCACHE')){
					$this->setSessionUser($this->Session->AccessUser);
				}else
        if($this->Session->access_user_ID){  // throw new QubeException('Session user ID is invalid.');
            $this->setLoggedInUserID($this->Session->access_user_ID);
            $this->getDataOwnerID();    // call this to make sure user_ID is initialized!
        }
    }
		
		static function unsetSessionUser(Zend_Session_Namespace $Session)
		{
			unset($Session->access_user_ID);
			unset($Session->AccessUser);
		}
		
    function setSessionUser(UserWithRolesModel $User){
        $this->User =   $User;
				$this->Session->AccessUser	=	$User;
#				unset($this->Session->access_user_ID);
#        $this->setDataOwnerID($User->getParentID());
        $this->setLoggedInUserID($User->getID());

        // had to write this 3 times to make sense of it.. i think it makes sense now.. nice and easy
        if($User->hasParent() && $User->hasPermissions()){       // logged-in user has a parent and has permissions to access the parent's account.
            $this->setDataOwnerID($User->getParentID());
        }else
		$this->setDataOwnerID($User->getID());


				$this->getQube()->setDashboardUserID($this->getDataOwnerID());
            
        return $this;
    }
    
    static function RunAction($method, HybridBaseController $parent, $args)
    {
        $controller =   new static($parent);
        return call_user_func_array(array($controller, $method), $args);
    }
    
    abstract function loadPermissions();
	
	function onEvent($evstr)
	{
		$args	= func_get_args();
		$arg1	= array_shift($args);
		return call_user_func_array(array($this->eventDispatch, $arg1), $args);
	}
	
	function setSystemEventDispatch(Qube\Hybrid\Events\SystemEventDispatch $SED)
	{
		$this->eventDispatch	=	$SED;
		return $this;
	}
}

