<?php
/* 
 * Author:          Amado Martinez, amado@projectivemotion.com
 * Last Updated:	2017-11-16 @fernando
 */

class AuthController extends HybridBaseController //extends HybridBaseController
{
    const PASSSALT  =   UserModel::PASSSALT;
    const COOKIENAME    =   '6qubelogin';
    
    function getView($page, $header = 'header.php', $footer = 'footer.php') {
        return parent::getView($page, 'login.php', 'login.php');
    }
    
    function validEmail($email)
    {
        return preg_match('/.+@.+\..+/i',$email);
    }
    
    function validPass($passw)
    {
        return strlen(trim($passw)) >= 5; // at least 5 characters like 'amado' ??
    }
    
    function loadPermissions() {
        ;
    }
    
    /**
     * 
     * @param type $info
     * @return UserModel
     */
    function validateCredentials($info, $admin_user)
    {
        if(!(empty($info['login']))   || 
                empty($info['password']) ||
                !$this->validEmail($info['login'])  ||
                !$this->validPass($info['password']))
        {
            //@todo disable validation, uncomment lines below to enable
//            $q  =   $this->q->queryObjects('UserModel')->Where('(parent_id = ? OR parent_id = 0) AND username = ? AND MD5(CONCAT(MD5(?), ?)) = password')->Prepare();
//            if($q->execute(array($admin_user, $info['login'], $info['password'], self::PASSSALT)))
            $q  =   $this->q->queryObjects('UserModel')->Where('username = ? AND MD5(CONCAT(MD5(?), ?)) = password AND access = 1')->Prepare();
            if($q->execute(array($info['login'], $info['password'], self::PASSSALT)))
            {
                $User   =   $q->fetch();
                if($User) return $User;
            }
        }
        return false;
    }
    
    function validateCookieKey($ck, $admin_user)
    {
        //@todo not sure how secure this is if multiple people have same pass and 0000-00-- last login? 
        // @todo check signup password generator.
        
        list($id, $key) =   explode('/', $ck);
		//@todo comment the following line for preventing log in users out if his main site
        $admin_user = $id;

        $q  =   $this->q->queryObjects('UserModel')
                ->Where('(parent_id = ? OR parent_id = 0) AND id = ? AND ? = MD5(CONCAT( CONVERT( MD5( last_login ) USING latin1 ), password ))')->Prepare();
        if(!$q->execute(array($admin_user, $id, $key))) return false;
        
        return $q->fetch();
    }
    
    
    static function doLoginSessionUser(UserModel $User, HybridBaseController $Ctrl)
    {
        $NS =   $Ctrl->getSession();
		HybridBaseController::unsetSessionUser($NS);				
        $NS->access_user_ID    =   $User->getID();
		
		
		$uda	=	new UserDataAccess();
		$Ctrl->onEvent('onUserLogin', $User, $uda);

        
		$NS->isLoggedIn = true;
		$NS->userID = $User->getID();
		
		
        $Ctrl->log('login', array('User' => $User,));
		$uda->updateUserLastLogin($User);
        $cookie_key =   $uda->getCookieKey($User);

        setcookie(self::COOKIENAME, $User->getID() . '/' . $cookie_key, time()+7*60*60*24);
    }
    
    /**
     * 
     * @param type $is_invalid_cookie
     * @return UserModel
     */
    function getUserFromCookie(&$is_invalid_cookie, $admin_user)
    {
        require_once HYBRID_PATH . '/classes/ChartWidget.php';
        $User   =   false;
        // check cookie!
        $cookie_key =   Cookie::getVal(self::COOKIENAME, false);
        if($cookie_key)
        {
            $User=  $this->validateCookieKey($cookie_key, $admin_user);
            $is_invalid_cookie     =    $User === false;
        }        
        return $User;
    }
    
    function forgotPassword($login, BrandingModel $b){
        
        $qUser = $this->q->queryObjects('UserModel')->Where('username = ?')->Prepare();
        $qUser->execute(array($login));
        
        $User = $qUser->fetch();
        if (!$User)
            return false;

        $insertq = 'INSERT INTO 6qube_tokens (ID, permission, reseller, object_id, expiration, object_type) VALUES (?, "passwordreset", ?, ?, NOW() + INTERVAL 7 day, "USER")';
        $insertp = $this->db->prepare($insertq);
        $resetid = md5(uniqid());

        if (!$insertp->execute(array($resetid, $b->admin_user, $User->getID())))
            return false;

        //$resetlink = $b->getUrl($this->ActionUrl('Auth_reset', array('token' => $resetid)));
        $reset_link = $this->ActionUrl('Auth_reset', array('token' => $resetid));
        //$reseller_url = $this->ResellerData->main_site;
        $reseller_data = $this->ResellerData;
        
        
        $to = $User->username;
        $subject = "Reset Your SeedLogix Password";
        
        require HYBRID_PATH . '/themes/email_default/reset-password-email.php';
        $message = reset_password_email($reseller_data, $reset_link);
        
        // Standard Non-Decorated HTML Message (in case HTML Pretty Email doesn't work).
        /*$message = "<html><head><title>Reset Your SeedLogix Password</title></head>"
                . "<body>"
                . "<p>Hi,</p>"
                . "<p>We received a request to reset the password for your SeedLogix account. You can reset your password by "
                . "following the instructions below:</p>"
                . "<p></p>"
                . "<br />"
                . "<p>Click <a href='http://$reseller_url/$resetlink' target='_blank'>here</a> to reset your password.</p>"
                . "<p>Discarding this email will have no effect on your account, or password.</p>"
                . "<p>If you did not make an attempt to reset your password please <a href='tel:8774677091'>contact us immediately</a>, "
                . "and we'll be happy to help.</p>"
                . "<p></p>"
                . "<br />"
                . "<br />"
                . "<p>Thank you,</p>"
                . "<p>The SeedLogix Team"
                . "<br />"
                . "P: <a href='tel:8774677091'>877-467-7091</a><br />"
                . "E: <a href='mailto:support@seedlogix.com'>support@seedlogix.com</a></p>"
                . "</body>"
                . "</html>";*/
        
        // Set HTML Content-Type
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: <admin@seedlogix.com>" . "\r\n";
        
        //mail($User->username, 'Your password reset link', $resetlink);
        //mail($User->username, 'Your password reset link', $message);
        mail($to, $subject, $message, $headers);

        return $resetid;
    }
    
    function getResetPasswordPage(BrandingModel $reseller, QubeTokenModel $t){
        //var_dump("RP Page Init");
        $V  =   $this->getView('login-reset');
        if(isset($_POST['password']) && isset($_POST['password-confirm']))
        {
            $vs =   new ValidationStack;
            $update =   array('password' => $_POST['password']);
            
            if($_POST['password'] != $_POST['password-confirm'] || strlen(trim($_POST['password'])) < 5 || strlen(trim($_POST['password-confirm'])) < 5)
                    $vs->addError('password','<p>Please enter a valid password.</p>');
            
            UserModel::ValidateField('password', $update['password'], $vs);
            
            if(!$vs->hasErrors()){
                //I'm not sure why Qube would be an instance og HybridModel, causing fake db error. <- Hunter S.
                /*
                if($this->q->Save(new \UserModel, $update, 'id = %d', $t->getUserID()) instanceof HybridModel){
                    $t->Invalidate($this->q);
                    return $this->ActionRedirect ('Auth_login?reset=true');
                }
                $vs->addError('password', 'Database error ocurred');
                */
                if($this->q->Save(new \UserModel, $update, 'id = %d', $t->getUserID())){
                    $t->Invalidate($this->q);
                    //return $this->ActionRedirect('Auth_login');
                    return $this->ActionRedirect ('Auth_login?reset=true');
                } else{
                    $vs->addError('password','<p>Database error, please try again.</p>');
                }
            }
            
            $V->init('errors', $vs->getErrors());
        }
        
        $V->init('Branding', $reseller);
        $V->init('tokenstr', $t->ID);
        
        return $V;        
    }
    
    
    function ExecuteAction($action = NULL) {
        require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';          
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
        
        $failed =   false;
        
        $User   =   false;
        
        $reseller   = SecureController::validateResellerHostname($this->q, $_SERVER['HTTP_HOST']);
        
        $messages   =   array();
        $MessageArray = array(
            'E10' => '<p>Your account is not fully setup yet, we have notified your admin.</p>'
        );
        //Check GET messages, defined from $MessageArray, may be one predefined message or a custom message defined in GET['E_Msg'].
        if(isset($_GET['E_Msg'])){
            if(isset($MessageArray[$_GET['E_Msg']])){
                $messages[] =   (object)array('body' => $MessageArray[$_GET['E_Msg']], 'class'   => 'nFailure');
            }else{
                $messages[] =   (object)array('body' => '<p>' . $_GET['E_Msg'] . '</p>', 'class'   => 'nFailure');
            }
        }
        if(!$reseller)  
            return $this->Redirect('/?hybrid-admin-not-found');
        
        if ($action == 'logout') {
            unset($this->Session->access_user_ID);
            unset($this->Session->isLoggedIn);
            if (isset($this->Session->previous_user_id) && !empty($this->Session->previous_user_id)) {
                $new_user_id = array_pop($this->Session->previous_user_id);
                $User = $this->q->queryObjects('UserModel')->Where('id = %d', $new_user_id)->Exec()->fetch();
            } else {
                setcookie(self::COOKIENAME, '0/0', time() - 60 * 60 * 6);
                session_destroy();
            }
        } elseif ($action == 'reset') {
            $tokenstr = $_GET['token'];
            $q = $this->db->prepare('SELECT "QubeTokenModel", t.* FROM 6qube_tokens t WHERE ID = ? AND expiration > NOW() AND permission = "passwordreset" AND used = 0');
            $q->execute(array($tokenstr));

            /* @var $token QubeTokenModel */
            $token = $q->fetch(PDO::FETCH_CLASSTYPE | PDO::FETCH_CLASS);

            if ($token) {
                //$token->Invalidate($this->q);
                $this->Session->resettoken_user = $token->getUserID();
                return $this->getResetPasswordPage($reseller, $token);
            } else
                $messages[] = (object) array('body' => '<p>Invalid token, please try again.</p>');
        } else {

            // handle 'login' action

            $User = $this->getUserFromCookie($failed, $reseller->admin_user);

            if ($action == 'login' && !$User) {
                if (isset($_GET['reset']) && isset($this->Session->resettoken_user)) {
                    $messages[] = (object) array('body' => '<p class="success">Your password has been updated.</p>');
                    unset($this->Session->resettoken);
                }

                if (!empty($_POST['forgot']) && !empty($_POST['login']) && filter_var($_POST['login'], FILTER_VALIDATE_EMAIL) !== FALSE) {
                    $tokenstr = $this->forgotPassword($_POST['login'], $reseller);
                    /* Not necessary, the form submit & message are triggered thru js */
                    //$messages[] = (object) array('body' => '<p class="success">Check your inbox to reset your password.</p>');

                    //if($tokenstr)                        return $this->ActionRedirect ('Auth_reset', array('token' => $tokenstr));
                }

                if (isset($_POST['sl_submit'])) {
                    $User = $this->validateCredentials($_POST, $reseller->admin_user);
                    $failed = $User === false;
                }
            }
        }

        if($User){
            self::doLoginSessionUser($User, $this);

            if(isset($this->Session->login_redirect))
            {
                $redir  =   $this->Session->login_redirect;
                unset($this->Session->login_redirect);
                return $this->Redirect($redir);
            }
            return $this->ActionRedirect('Campaign_home');
        }

        
        $V  =   $this->getView('login');
        $V->init('Branding', $reseller);
        if($failed && !isset($_GET['E_Msg'])){
            $messages[] = (object)array('body' => '<p>Invalid credentials, please try again.</p>');
        }
        $V->init('access_denied', $failed);
        $V->init('messages', $messages);
        $User = new UserWithRolesModel();
        $User->setPermissionsSet(new PermissionsSet($User));
        $V->init('User', $User);
        
        return $V;
    }
}
