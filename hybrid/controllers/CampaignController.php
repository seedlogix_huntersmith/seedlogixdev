<?php
/*
 * Author:     Jon Aguilar
 * License:    
 */
use Qube\Hybrid\DataAccess\CampaignDataAccess;

require_once QUBEADMIN . 'inc/reseller.class.php';
require_once QUBEADMIN . 'inc/user_permissions.class.php';
require_once QUBEADMIN . 'models/CampaignModel.php';
require_once QUBEADMIN . 'models/HubModel.php';
require_once QUBEADMIN . 'models/WebsiteModel.php';
require_once QUBEADMIN . 'models/LeadModel.php';
require_once QUBEADMIN . 'models/ReportModel.php';
require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';

class CampaignController extends DashboardBaseController
{
	const chartCookieName	=	'chartconfig';
        public $cc_user_id, $cc_is_reports_only;

        function __construct() {

        }

        function getSites($tp_type, VarContainer $C, CampaignModel $campaign)
        {
            $data = DataTablePreProcessor::RunIt($this, $C)->getSites($campaign, $tp_type);
            //error_log($data, 3, "hunter-errors.log");
            return $this->getAjaxView($data);
        }

        function getTopPerformingSites($tp_type, VarContainer $C, CampaignModel $campaign)
        {
			//iSort_Col_0 value get from select
            $C->iDisplayLength = 5;
            $C->sSortDir_0 = 'DESC';
            $data = DataTablePreProcessor::RunIt($this, $C)->getSites($campaign, $tp_type);
            return $this->getAjaxView($data);
        }

	/**
	 * @param $touchpoint_type
	 * @param $campaign_ID
	 * @return DBSelectQuery
	 */
	function queryTouchPoints($touchpoint_type, $campaign_ID){

        $this->q->query('SET @lastMonday    =   DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY)');

        if($touchpoint_type === 'BLOG'){
            $q  =   $this->q->queryObjects('BlogModel')->addFields('blog_title as name, "BLOG" as touchpoint_type');
        }else{
            $q    =   $this->q->queryObjects('WebsiteModel');
            if($touchpoint_type)
                $q->Where('T.touchpoint_type    =   "%s"', $touchpoint_type);
        }
        $q
        ->Where('T.trashed = 0 AND T.cid = %d and (T.user_id = dashboard_userid() OR 1=%d)', $campaign_ID, $this->getUser()->can('is_system'))
        ->leftJoin('active_leads l', 'l.hub_id = T.id AND l.created > @lastMonday')
        ->addFields('count(l.id) as numleads, T.created as site_created_date')
        ->groupBy('T.id');

        return $q;
    }

    static function getTouchPointTypes($type    =   NULL)
		{
        static $types   = array(
                     'WEBSITE' => array('Website_dashboard?ID=%d','Websites',''),
                     'BLOG' => array('Blog_dashboard?ID=%d','Blogs',''),
                     'LANDING' => array('Landing_dashboard?ID=%d','Landing Pages',''),
                    'PROFILE' => array('Profile_dashboard?ID=%d','Profiles','')
                );

        if($type)   return isset($types[$type]) ? $types[$type] : NULL;

        return $types;
    }

    static function getTouchPointType($type)
    {
        return in_array($type, self::getTouchPointTypes()) ? $type : NULL;
    }

    function viewForms(CampaignModel $Campaign)
    {
		if($deny = $this->Protect('view_customforms'))
			return $deny;
        $default_sort = DataTableResult::getDefaultColumn('LeadForms');
        $data = DataTablePreProcessor::RunIt($this, array('calc_found_rows' => TRUE,
                    'limit' => 10, 'count_prospects' => true, 'count_responders' => TRUE, 'where' => array('trashed' => '0000-00-00'), 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort'])
        )->getLeadForms($Campaign);
        if($this->getUser()->can('crm_only')) $P  =  $this->getPage('campaign-forms','Forms','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P  =  $this->getPage('campaign-forms','Forms','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_forms?ID='.$Campaign->id,'Campaign Forms');
        $P->init('totalForms', $data->count);
        $P->init('forms', $data->objects);
        return $P;
    }

	function ajaxDataTableForms(CampaignModel $Campaign, VarContainer $C){
		/** @var CampaignDataAccess $CDA */
		$CDA = CampaignDataAccess::FromController($this);
        $C->table = 'LeadForms';
        $return = DataTablePreProcessor::RunIt($this,$C)->getLeadForms($Campaign,array('count_prospects' => TRUE,'count_responders' => TRUE,'where' => array('trashed' => '0000-00-00')));
/*
		$C->set('table', 'LeadForms');
		$return = DataTableResult::returnDataTableStructure(array($CDA, 'getFormsWithRoles'), array(
			    'cid' => $Campaign->id,
                'joins' => array(
                    'prospects', 'autoresponders'
                )
            ) +
			DataTableResult::getRequestArgs($C),
			$this->getView('rows/leadform', false, false), 'leadform', 'LeadFormModel');
*/

		return $this->getAjaxView($return);
	}

    function ajaxForms(CampaignModel $Campaign, VarContainer $C){
		$data = CampaignDataAccess::FromController($this)->getFormsWithRoles(array(
			'cid' => $Campaign->id,
			'search' => $C->get('search')
		));

        return $this->getAjaxView($data);
   }

    /**
     *
     * @param CampaignModel $Campaign
     * @param type $formid
     * @return LeadFormModel
     */
    function getLeadForm(CampaignModel $Campaign, $formid){
        return LeadsDAO::FromController($this)->getLeadForm($formid, $Campaign->getID());
    }

    /**
     *
     * @param CampaignModel $Campaign
     * @param type responderid
     * @return ResponderModel
     */
    function getResponder(CampaignModel $Campaign, $responderid){
        return $Campaign->queryObjects('ResponderModel', 'T', 'user_id = %d AND id = %d', $this->user_ID, $responderid)->Exec()->fetch();
    }

    function viewForm(CampaignModel $Campaign, LeadFormModel $form){
        $form->upgrade();
        $select_new = false;
        if(isset($_POST['field'])){
            $VS =   new ValidationStack();
            $result =   array();
            $newField   =   $form->addField($_POST['field'], $VS);
            if($newField){
                $result['row']  =   $this->getView('rows/leadform-field', false, false)
                        ->init('campaign_id', $Campaign->getID())
                        ->init('form', $form)->init('field', 0)
                        ->init('field', $newField)
                        ->toString();
                $result['message']  =   'New Field has been created.';
                return $this->getAjaxView($result);
            }else{
                return $this->returnValidationError($VS);
            }
        }


        $form_fields    =   $form->getFields();
        if($select_new)
            $_GET['field']  =   count($form_fields)-1;

        if($this->getUser()->can('crm_only')) $P  =  $this->getPage('campaign-form','Form','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P  =  $this->getPage('campaign-form','Form','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Lead Forms', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_form?ID='.$Campaign->id, $form->name, $Campaign->id);

        if(isset($_GET['field'])){
            $field_ID = $_GET['field'];
//            $P = $this->getPage('campaign-field','Form Field','campaign'/* $menu */);
//            $P->init('form', $form);
            $P->init('field_edit', LeadFormFieldModel::Query('id = %d AND form_ID = %d', $field_ID, $form->getID())->Exec()->fetch());
        }

        $uda = UserDataAccess::FromController($this);
        $roles	= $uda->getRoles(null, null, 0, 10);
        $P->init('roles', $roles);

        $P->init('form_fields', $form_fields);
        $P->init('form', $form);

        return $P;
    }

    function viewResponderEmailPreview(CampaignModel $Campaign, $responder_ID)
    {
        /**
         * 1. find a lead to use
         * 2.
         */
        /** @var ResponderModel $Responder */
        $Responder =	$Campaign->getResponder('T.id = %d', $responder_ID);
        $fromDomain = $this->getSession()->HTTP_HOST;

        $LDA = LeadsDAO::FromController($this);
        $lead = $LDA->getLatestLead();
        $Smarty = new QubeSmarty();
        $smartyvars = $Responder->toArray();
        $LeadResponse = $Responder->ConfigureResponse(
            array('table' => 'lead_forms' == $Responder->table_name ? 'leads' : 'contact_form', 'lead' => $lead),
            (object) array(
                'main_site' => $fromDomain,
                'responder_phone' => '#hub-phone#', 'site_logo' => $Responder->logo), $Smarty, array(),
            $smartyvars);

        $captureView = new ObCaptureView();
        echo $LeadResponse->getContent();
        return $captureView->endCapture();
    }
    function viewEmailerPreview(CampaignModel $Campaign, $responder_ID)
    {
        $DA = EmailBroadcastDataAccess::FromController($this);
        $Responder = $DA->getLeadBroadcasters(array('id' => $responder_ID, 'where' => array('e.id = :id AND e.user_id = :user_ID AND e.trashed = 0'),
            'returnSingleObj' => 'LeadFormEmailerModel'));
        echo $Responder->body;
        die();
    }

    function viewResponderSettings(CampaignModel $Campaign){

        $page = 'Auto-Responder Settings';

				$RID	=	(int)$_GET['R'];

				$R =	$Campaign->getResponder('T.id = %d', $RID);

				if(!$R)	// security
					return $this->SecurityError();

        $P =    $this->getPage('responder-settings',$page,'campaign'/* $menu */);
        $P->init('Responder', $R);
        if($this->getUser()->can('is_system')){
            $dataAccess = new UserDataAccess();
            $ownerUser = $dataAccess->getUser($Campaign->user_id);
            $P->init('storagePath', $ownerUser->getStorageUrl());
        }
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emailmarketing?ID='.$Campaign->id, 'Email Marketing', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_responder-settings?ID='.$Campaign->id, 'Auto-Responder Settings', $Campaign->id);
//		$P->init('responder');

        return $P;
    }

    function ajaxSendTestMail(CampaignModel $Campaign, VarContainer $VC){
        $RID	=	(int)$_GET['R'];

        $R = ResponderDataAccess::FromController($this)->getResponderById($RID);


        if(!$R)	// security
            return $this->SecurityError();
        $fakeLead = new LeadModel(NULL, array(
            '_data' => 'Data',
            'hub_name' => 'Hub test',
            'page_name' => 'Page test',
            'search_engine' => 'Search engine Test',
            'keyword' => 'Test',
            'ip' => '127.0.0.1',
			'name' => 'John Smith',
			'lead_name' => 'John Smith',
			'lead_email' => $VC->get('sendTo'),
			'phone' => '88888888'
        ));

        $notification = $R->getNotification(array('table' => 'leads', 'lead' => $fakeLead));
        //$notification->setSender(new SendTo('John Smith', 'send@test.com'));
        $email = $VC->get('sendTo');
        if($this->getQube()->getMailer()->Send($notification, new SendTo($email, $email))){
            return $this->getAjaxView(array('message' => 'Email sent'));
        }

        return $this->returnAjaxError("Fail Send email to $email");
    }

    function viewEmailBroadcast(CampaignModel $Campaign){

        $EBDA = EmailBroadcastDataAccess::FromController($this);
        $default_sort = DataTableResult::getDefaultColumn('Broadcast');
        $broadcasters = $EBDA
            ->getLeadBroadcasters(array('limit' => 10, 'campaign_ID' => $Campaign->getID(), 'calc_found_rows' => true, 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort']));
        $page = 'Email Broadcasts';
        $total_broadcasters = $EBDA->FOUND_ROWS();
        if($this->getUser()->can('crm_only')) $P =    $this->getPage('campaign-broadcast',$page,'campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P =    $this->getPage('campaign-broadcast',$page,'campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emailbroadcasts?ID='.$Campaign->id, 'Email Broadcasts', $Campaign->id);
        $lead_forms = CampaignDataAccess::FromController($this)->getFormsWithRoles(array(
            'cid' => $Campaign->id
        ));
        $P->init('lead_forms', $lead_forms);
        $P->init('broadcasters', $broadcasters);
        $P->init('total_broadcasters', $total_broadcasters);

        return $P;
    }

    function ajaxBroadcasters(CampaignModel $campaign, VarContainer $C){
        $C->table = 'Broadcast';
        $args = DataTableResult::getRequestArgs($C);
        $args['campaign_ID'] = $campaign->getID();
		/** @var EmailBroadcastDataAccess $EDA */
		$EDA = EmailBroadcastDataAccess::FromController($this);
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($EDA, 'getLeadBroadcasters'), $args, $this->getView('rows/campaign-broadcast', false, false), 'broadcaster', 'LeadFormEmailerModel'));
    }

		function SecurityError($redir = 'Campaign_dashboard'){
			// @todo log error
			return $this->ActionRedirect($redir);
		}

    function viewEmailerBroadcast(CampaignModel $Campaign, $broadcast_ID = 0){

        $DA = EmailBroadcastDataAccess::FromController($this);

        /** @var LeadFormEmailerModel $R */
        $R = $DA->getLeadBroadcasters(array('id' => $broadcast_ID, 'where' => array('e.id = :id AND e.user_id = :user_ID AND e.trashed = 0'),
                'returnSingleObj' => 'LeadFormEmailerModel'));

        if(!$R)	// security
            return $this->SecurityError();

        if(isset($_POST['sendTo']))
        {
            $Notifier = Qube::Start()->getMailer(); //new NotificationMailer());
//            $Notifier->addStdoutWriter();
            $Notification = $R->getNotification();
            $Notification->setPlaceHolders(array('name' => '*John Smith*'));
            $Notifier->Send($Notification, new SendTo(NULL, $_POST['sendTo']));
            return $this->getAjaxView(array(
				'error' => false,
				'message' => "Email sent"
			));
        }

        if(isset($_POST['viewleads']))
        {
            $Prospects = ProspectDataAccess::FromController($this)
                ->getAccountProspects(array('where' => 'p.email != "" AND p.spampoints = 0 AND p.form_ID = :form_ID AND p.trashed = 0 and p.user_id = :user_id',
                        'form_ID' => $R->lead_form_id, 'limit' => 20,
                    'cols' => 'p.name, p.email, p.ID'));
            return $this->returnAjaxData('prospects',
                    $this->getHtml('lists/broadcaster-prospects', array('prospects' => $Prospects))
                );
        }

        $page = 'Emailer Settings';

        $P = $this->getPage('emailer-settings',$page,'campaign'/* $menu */);
        $P->init('Broadcaster', $R);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emailbroadcasts?ID='.$Campaign->id, 'Email Broadcasts', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emailer-settings?ID='.$Campaign->id, 'Email Broadcast Settings', $Campaign->id);
//		$P->init('responder');

        return $P;
    }

    function viewEmailMarketing(CampaignModel $Campaign){
		if($deny = $this->Protect('view_emailmarketing'))
			return $deny;
        $default_sort = DataTableResult::getDefaultColumn('EmailMarketing');
        $PQ =   ResponderDataAccess::FromController($this)->getResponders(
            array('with-source' => TRUE, 'calc_found_rows' => TRUE, 'limit' => 10, 'where' =>
                    array('R.trashed' => '0000-00-00', 'R.cid' => $Campaign->getID()), 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort']
            )
        );
        $totalEmails = $this->query('SELECT FOUND_ROWS()')->fetchColumn();

        $lead_forms = CampaignDataAccess::FromController($this)->getFormsWithRoles(array(
            'cid' => $Campaign->id
        ));

        if($this->getUser()->can('crm_only')) $P =    $this->getPage('campaign-marketing','Email Marketing','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P =    $this->getPage('campaign-marketing','Email Marketing','campaign'/* $menu */);
        $P->init('Responders', $PQ);
        $P->init('lead_forms', $lead_forms);
        $P->init('totalEmails', $totalEmails);
        $P->init('Campaign', $Campaign);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emailmarketing?ID='.$Campaign->id, 'Email Marketing', $Campaign->id);

        return $P;
    }

    function ajaxEmailmarketing(CampaignModel $Campaign, VarContainer $C){
        $C->table = 'EmailMarketing';
        $args   =   DataTableResult::getRequestArgs($C);
        if($C->context != '') {
            $args['context_filter'] = $C->context;
        }
        $args['with-source']    =   TRUE;
        $args['where'] = array('R.trashed' => "0000-00-00", 'R.cid' => $Campaign->getID());
		/** @var ResponderDataAccess $rda */
        $rda    =   ResponderDataAccess::FromController($this);
        $dataTableData  =   DataTableResult::returnDataTableStructure(array($rda, 'getResponders'),
            $args, $this->getView('rows/campaign-marketing', false, false), 'Responder', 'ResponderModel');
        return $this->getAjaxView($dataTableData);
//        $PQ =   $Campaign->queryObjects('ResponderModel', 'T','user_id = %d AND trashed = 0', $this->user_ID)->calc_found_rows();
//        if($C->checkValue('sSearch', $search) && $search != ''){
//            if(!is_numeric($search)){
//                $PQ->Where('T.name LIKE :search');
//                    $qparams[':search']  =   '%' . $search . '%';
//            }else{
//                $PQ->Where('T.id = :search');
//                $qparams[':search']  =   $search;
//            }
//        }
    }

    /**
     * Some real magic going on here.
     * Passing a CampaignModel will return prospects for the particular campaign.
     * Passing Qube will query all campaigns.
     *
     * @param array $table
     * @param queriesObjects $Campaign
     * @return type
     */
    function queryProspects(queriesObjects $Campaign, VarContainer $C)
    {
        $cols = array('T.created', 'T.grade', 'T.name', 'T.email', 'T.phone', 'f.name', 'T.created');

        $Q  =   $Campaign->queryObjects('ProspectModel')
//                    ->Join(array('CampaignModel', 'c'), 'c.id = T.cid')
                    ->addFields('ifnull(f.name, "Contact Form") as form_name, if(T.name = "", "?", T.name) as name')
                    ->leftJoin(array('LeadFormModel', 'f'), 'f.id = T.form_id');


        /**
         * Defaults
         */
        $start  =   0;
        $length =   10;
        if($C->checkValue('iDisplayStart', $start)){
            $length =   min(100, $C->iDisplayLength);
        }
        $Q->Limit($start, $length);

        if($C->checkValue('form', $form_id)){
            $Q->Where('lead_form_id = %d', $form_id);
            $filters[]  =   'form';
        }

        if($C->checkValue('iSortCol_0', $colIndex) && isset($cols[$colIndex])){
            $Q->orderBy($cols[$colIndex] . ' ' . ($C->sSortDir_0 == 'asc' ? 'ASC' : 'DESC'));
        }else{
            $Q->orderBy('T.created DESC');
        }

        return $Q;
    }

    function ajaxProspects(VarContainer $C, CampaignModel $Campaign = NULL)
    {
        $cc_user_id = $this->getUser()->getID();
        $cc_is_reports_only = $this->getUser()->can('reports_only');

        if(!isset($C->alltime)){
            $timeContext = new PointsSetBuilder(self::getChartTimeContext($cc_user_id, $cc_is_reports_only));
            $C->timecontext = $timeContext;
        }
        $dataTableData  =   DataTablePreProcessor::RunIt($this, $C)
            ->getProspects($Campaign);
        return $this->getAjaxView($dataTableData);
    }

    function viewProspects(CampaignModel $Campaign, VarContainer $C){
		if($deny = $this->Protect('view_prospects'))
			return $deny;

		$PDA = ProspectDataAccess::FromController($this);
        $default_sort = DataTableResult::getDefaultColumn('ProspectsCampaign');
		$Prospects = $PDA->getAccountProspects(
			array('cid' => $Campaign->getID(), 'limit' => 10, 'calc_found_rows' => 1, 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort']));
        $iTotalProspects    =   $PDA->FOUND_ROWS();

        if($this->getUser()->can('crm_only')) $P  =   $this->getPage('campaign-prospects','Leads','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P  =   $this->getPage('campaign-prospects','Leads','campaign'/* $menu */);

        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_prospects?ID='.$Campaign->id, 'Leads', $Campaign->id);

		$P->init('AllProspects', $Prospects);
        $P->init('total_prospects_match', $iTotalProspects);

//        $P->init('ContactFormProspects', $CQ->Exec()->fetchAll());
//        $P->init('BlogFormProspects', $BQ->Exec()->fetchAll());
//        $P->init('NetworkProspects', $NQ->Exec()->fetchAll());
//        $P->init('BlogFormProspects', $BQ->Exec()->fetchAll());
//        $P->init('BlogFormProspects', $BQ->Exec()->fetchAll());

//        $P->init('filters', $filters);

        return $P;
    }

    function viewNetworks(CampaignModel $Campaign){
		//if($deny = $this->Protect('view_networks'))
			//return $deny;


        $P  =   $this->getPage('campaign-networks','Networks','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_networks?ID='.$Campaign->id, 'Network Listings', $Campaign->id);
        $P->init('total_users', 10);

		$ArticlesWidget	=	new NetworkArticlesWidget($this, array('campaign' => $Campaign));
        //$ArticlesWidget->appendToView($P, 'articles');

        return $P;
    }
    public function ajaxNetworks($Camp, VarContainer $C) {
        $query = $this->queryNetworkArticles($Camp)->calc_found_rows(true);
        if($C->checkValue('sSearch', $search) && $search != ''){
            if(!is_numeric($search)){
                $query->Where('T.name LIKE :search OR T.tbl LIKE :search');
                    $qparams[':search']  =   '%' . $search . '%';
            }else{
                $query->Where('T.tbl_id = :search');
                $qparams[':search']  =   $search;
            }
        }
        $C->load(array('table' => 'NetworksCampaign'));
        $DT = new DataTableResult($query, $C);
        $DT->setRowCompiler($this->getView('rows/network-any', false, false), 'item');
        return $this->getAjaxView($DT->getArray($qparams));

    }

    function viewNetwork(CampaignModel $Campaign){
        $Networks   =   $this->queryNetworkArticles($Campaign)->Exec()->fetchAll();

        $P  =   $this->getPage('campaign-network','Networks','networks'/* $menu */);
        $P->init('networks', $Networks);

        return $P;
    }

    function viewNetworkPressRelease(CampaignModel $Campaign, $press_ID){
        $PressArticle   =   $Campaign->getPressArticle('T.id = %d', $press_ID);

		$P  =   $this->getPage('network-press','Press Release','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_networks?ID='.$Campaign->id, 'Network Listings', $Campaign->id);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_network-press?press='.$press_ID.'&ID='.$Campaign->id, 'Press Release Edit', $Campaign->id);

		$PhotoUpload    =   new PhotoUploadWidget($this);
        $PhotoUpload->appendToView($P, 'photoupload');

        $P->init('pressarticle', $PressArticle);

        return $P;
    }

    function getListingPhotoGallery(){
			$slidet	=	new SlideModel();
			return array($slidet);
    }

    function viewNetworkListingInfo(CampaignModel $Campaign, $listing_ID, $viewt = ''){
		$Listing	=	$Campaign->getDirectoryArticle('T.id = %d', $listing_ID);
        $viewfiles = array('ss' => 'campaign-network-socialSpots', 'seo' => 'campaign-network-seo', 'er' => 'campaign-network-editRegions',
              'ph' => 'campaign-network-photoGallery');

        $view = isset($viewfiles[$viewt]) ? $viewfiles[$viewt] : 'network-listingInfo';

        $P = $this->getPage($view,'Networks','networks'/* $menu */);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_networks?ID='.$Campaign->id, 'Network Listings', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_network-listingInfo?article='.$listing_ID.'&ID='.$Campaign->id, 'Directory Listing Edit', $Campaign->id);

		$PhotoUpload = new PhotoUploadWidget($this);

        $P->init('dirarticle', $Listing);
		if($viewt	==	'ph')
			$P->init('slides', $this->getListingPhotoGallery ());

        $PhotoUpload->appendToView($P, 'photoupload');

        return $P;
    }

    function viewNetworkArticle(CampaignModel $Campaign, $article_ID){
			/* @var $Networks NetworkArticleModel */
        $Article   =   $Campaign->getNetworkArticle('T.id = %d AND trashed = 0', $article_ID);

        $P  =   $this->getPage('network-article','Article','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_networks?ID='.$Campaign->id, 'Network Listings', $Campaign->id);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_network-article?article='.$article_ID.'&ID='.$Campaign->id, 'Article Edit', $Campaign->id);

		$PhotoUpload    =   new PhotoUploadWidget($this);
        $PhotoUpload->appendToView($P, 'photoupload');

        $P->init('article', $Article);

        return $P;
    }


    function viewPhoneRecordings(CampaignModel $Campaign, Phone $phone, VarContainer $C){
        //$this->action_url('UserManager_ajaxList?status=' . htmlentities(@$user_status, ENT_QUOTES));
        $P = $this->getPage('campaign-phonecalls','Phone','campaign'/* $menu */);

        $req_url = 'https://6qube.callroi.com/api/v1.0/Mailbox/'.$C->mid.'/Messages/?format=json&RequestType=YearToDate&PageSize=80';
        $hCurl = curl_init();
        $creds_pass = 'testing';
        $creds = 'sixqube:'.$creds_pass;
        curl_setopt($hCurl, CURLOPT_URL, $req_url);
        curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
        curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);

        $json = curl_exec($hCurl);
        curl_close($hCurl);

//        echo $json; exit;
        $resp    =   json_decode($json);

        if(!$resp || $resp->ErrorCode){
            throw new Exception ('error');
        }

        $info   =   $resp->DataInfo;

        $data   =   array();
//        if($info->TotalResults){
//            foreach($resp['Data'] as $datarow){
//                $data[] =
//
//            }
//        }

        $P->init('mid', $C->mid);
        $P->init('info', $info);
        $P->init('Calls', $resp->Data);
        return $P;
    }

    function viewPhone(CampaignModel $Campaign, VarContainer $C){

		//  include the library
		require_once(QUBEADMIN . 'inc/phone.class.php');
		$phone = new Phone();

		if($C->checkValue('do', $subdo))
		{
			switch($subdo)
			{

				case 'reports':
					$P  =  $this->getPage('campaign-phonereports','Phone Reports','campaign'/* $menu */);

					//get call reports
					$callReport = $phone->getCallDetails($this->user_ID, $C->mid, $C->rqtype);

					$callReports = $callReport['Data'];
					$totalResults = $callReport['ResultSet']['TotalResults'];

					$P->init('callReports', $callReports);
					$P->init('totalResults', $totalResults);
					$P->init('phone', $phone);
					$P->init('rqtype', $C->rqtype);
					$P->init('mid', $C->mid);
					return $P;
					break;

				case 'rec':
					return $this->viewPhoneRecordings($Campaign, $phone, $C);
			}
		}

        //Default Campaigns phone view
		$P  =  $this->getPage('campaign-mailboxes','Phone','campaign'/* $menu */);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_phone?ID='.$Campaign->id, 'Phone', $Campaign->id);

		//get customerID
		$customer_id = $phone->getCustomerID($this->user_ID, $Campaign->getID());

		$custID = $customer_id['customer_id'];

		//get mailboxes
		$mailboxView = $custID ? $phone->getMailboxes($this->user_ID, $custID) : array();


		$mailboxNumbers = $phone->getMBXNumbers($this->user_ID);//$mailboxID);

        $P->init('mailboxView', $mailboxView);
        $P->init('custID', $custID);
        $P->init('customer_id', $customer_id);
        $P->init('phone', $phone);
        $P->init('mailboxNumbers', $mailboxNumbers);

        return $P;
    }

   // function viewPhoneRecords(CampaignModel $Campaign, VarContainer $C){
        //Include Phone library
        //require_once(QUBEADMIN . 'inc/phone.class.php');
        //$phoneRec = new Phone();
        //API Call
        //$callReportRec = $phoneRec->getCallDetails($this->user_ID, $C->mid, $C->rqtype);
        //$callReportsRec = $callReportRec['Data'];
        //$totalResultsRec = $callReportRec['ResultSet']['TotalResults'];
        //View and Variables
        //$P  =   $this->getPage('campaign-phone','Phone Records','campaign'/* $menu */);
       // $P->init('totalResults', $totalResultsRec);
        //$P->init('callReports', $callReportsRec);
       // $P->init('rqtype', $C->rqtype);
        //$P->init('mid', $C->mid);
        //$P->init('phone', $phoneRec);
        //breadcrumbs
        //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_phone?ID='.$Campaign->id, 'Phone', $Campaign->id);
        //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_phone-reports?ID='.$Campaign->id, 'Phone Reports',$Campaign->id);
        //return $P;
   // }

	function queryPhoneRecords(CampaignModel $Campaign, VarContainer $C){


        if($Campaign->id) $PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d and T.cid = %d', $this->User->getID(), $Campaign->id);
		else $PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID());
        return $PQ;
    }

    function viewPhoneRecords(CampaignModel $Campaign, VarContainer $C){
        //Include Phone library
        /*require_once(QUBEADMIN . 'inc/phone.class.php');
        $phoneRec = new Phone();
        $did = $phoneRec->getDID($Campaign->user_id, $Campaign->getID());
		$tid = $phoneRec->getTID($Campaign->user_id, $Campaign->getID());
        $blacklist = $phoneRec->blacklistCalls();
        $DID = $did['phone_num'];
		$TID = $tid['id'];
        $created = $did['date_created'];*/
        //API Call
        //$callReportRec = $phoneRec->getCallDetailsNew($DID,$C->rqtype);
        //$callReportsRec = $callReportRec['response']['objects'];
        //$totalResultsRec = $callReportRec['response']['meta']['total_count'];
		//NEW Setup here:
		//$callReportArchive = $phoneRec->getCallDetailsArchive($TID,$C->rqtype,$Campaign->getID(),$Campaign->user_id);
		//$totalResultsArchive = count($callReportArchive);

		$default_sort = DataTableResult::getDefaultColumn('PhoneRecords');
        $PQ =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
        $PQ->Limit(0, 10);
		$PQ->orderBy('T.date DESC');
        $PhoneRecords  =   $PQ->Exec()->fetchAll();
        $iTotalPhoneRecords    =   $this->queryColumn('SELECT FOUND_ROWS()');



        //View and Variables
        $Page  =   $this->getPage('campaign-phone', 'Phone Records', 'campaign');
		//NEW Initiate:
		$Page->init('totalRecords', $iTotalPhoneRecords);
		$Page->init('PhoneRecords', $PhoneRecords);
		//legacy
       /* $P->init('totalResults', $totalResultsRec);
        $P->init('callReports', $callReportsRec);
        $P->init('rqtype', $C->rqtype);
        $P->init('blacklist', $blacklist);
        $P->init('mid', $C->mid);
        $P->init('phone', $phoneRec);
        $P->init('did', $DID);
        $P->init('created', $created);*/
        //breadcrumbs
        //$this->getCrumbsRoot()->addCrumbTxt('campaign_phone?ID='.$Campaign->id, 'Phone', $Campaign->id);

        /*$m = date('m');
        $y = date('Y');
        $d = date('t');

        $this->getCrumbsRoot()->addCrumbTxt('campaign_phone-reports?ID='.$Campaign->id.'&rqtype='.$y.'-'.$m.'-01,'.$y.'-'.$m.'-'.$d.'', 'Phone Reports', $Campaign->id);
        return $P;*/

        return $Page;

    }

	function ajaxPhoneMainDashboard(VarContainer $C){

		$C->load(array('table' => 'PhoneRecords'));

		$d2 = date('Y-m-d', strtotime('-30 days'));
        //$qP =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
		$qP     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$qP ->Where('T.date >= "'.$d2.'"');
		//$qP->orderBy('T.date ASC');


        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.caller_id LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/phone-records', false, false), 'PhoneRecord');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);

	}


	function ajaxPhone(CampaignModel $Campaign = NULL, VarContainer $C){

		$C->load(array('table' => 'PhoneRecords'));


        $qP =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.caller_id LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/phone-records', false, false), 'PhoneRecord');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);


		/*require_once(QUBEADMIN . 'inc/phone.class.php');

        $requestType = $C->get('requestType');

		$SortyBy = $C->get('iSortCol_0');
        switch($SortyBy){
			case 0:
				$column = 'DateTime';
				break;
			case 1:
				$column = 'PhoneNumber';
				break;
			default:
				throw new InvalidArgumentException('Column not supported.');
		}

		$SortDir = $C->get('sSortDir_0');
		$iDisplaysStart = $C->get('iDisplayStart','intVal');
		$iDisplayslength = $C->get('iDisplayLength','intVal');
		//$currentPage = (int)($iDisplaysStart/$iDisplayslength+1);
        $currentPage = $iDisplaysStart+1;
		$phone = new Phone();
		$user_id = $this->getUser()->getID();
		$did = $C->get('did');
		$callReport = $phone->getCallDetailsNew($did, $requestType, $currentPage, $iDisplayslength);
		$finalData = array();

        $blacklist = $phone->blacklistCalls();

            $a = 0;
            foreach ($callReport['response']['objects'] as $row) {
                //$date = date('m/j/Y', preg_replace('/[^\d]/','', $row['DateTime'])/1000);
                //$link = str_replace('my.patlive.com', '6qube.callroi.com', $callReport['CurrentURL']);
                //$link = $this->stripPhoneNumber( $row['CallerId'] );
                if (in_array($row['from_number'], $blacklist)) {
                    $a++;
                } else {
                    $date = date('Y-m-d H:i:s', strtotime($row['end_time']));
                    $calltime = gmdate("H:i:s", $row['call_duration']);
                    $link = '<a class="buttonS bDefault phoneDownload" href="' . $row['call_uuid'] . '"><span class="iconb" data-icon="&#xe078;"></span><span>Download</span></a>';
                    $finalData[] = array($date, $row['from_number'], $row['to_number'], $calltime, $link);
                }
            }

		//$totalRecords = $callReport['ResultSet']['TotalResults'];
        $totalRecords = $callReport['response']['meta']['total_count'];
        $totalRecords = $totalRecords-$a;
		$echo = $C->get('sEcho');
		return $this->getAjaxView((object)array('iTotalRecords' => $totalRecords, 'aaData' => $finalData, 'iTotalDisplayRecords' => $totalRecords, 'sEcho' => $echo));*/
	}

    function queryContacts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   $Campaign->queryObjects('ContactModel', 'T', 'user_id = %d and trashed = 0', $this->User->getID());
        return $PQ;
    }

    function ajaxContacts(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Contacts'));
        $qP =   $this->queryContacts($Campaign, $C)->orderBy('T.id DESC')->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.first_name LIKE :search OR T.last_name LIKE :search OR T.email LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/campaign-contact', false, false), 'Contact');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

    function ajaxAccountContacts(CampaignModel $Campaign, VarContainer $C){

		$C->load(array('table' => 'Contacts'));
        $qP =   $this->queryContacts($Campaign, $C)->Limit(0, 10)->calc_found_rows();
		$qP->Where('(T.account_id = '.$_GET['aid'].')');


        $qparams    =   array();

        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.first_name LIKE :search OR T.last_name LIKE :search OR T.email LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);

        $DT->setRowCompiler($this->getView('rows/campaign-contact', false, false), 'Contact');

        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

    function viewContacts(CampaignModel $Campaign, VarContainer $C){
		if($deny = $this->Protect('view_contacts'))
			return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Contacts');
        $PQ =   $this->queryContacts($Campaign, $C)->orderBy($default_sort['Name'])->Limit(0, 10)->calc_found_rows();

        $Contacts  =   $PQ->Exec()->fetchAll();
        $iTotalContacts    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-contacts','Contacts','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_contacts?ID='.$Campaign->id, 'Contacts', $Campaign->id);
		$Page->init('Contacts', $Contacts);
        $Page->init('total_contacts_match', $iTotalContacts);
        return $Page;
    }

	function queryEmails(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('EmailsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

	function queryParentEmails(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('EmailsModel', 'T', 'T.user_id = %d and T.cid = 0 and T.account_id = 0 and T.trashed = 00-00-0000', $Campaign->user_parent_ID);
        return $PQ;
    }

    function ajaxEmails(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Emails'));
        $qP =   $this->queryEmails($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/emails', false, false), 'Email');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewEmails(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Emails');
        $PQ =   $this->queryEmails($Campaign, $C)->orderBy('T.created DESC')->Limit(0, 10)->calc_found_rows();

        $Emails  =   $PQ->Exec()->fetchAll();
        $iTotalEmails    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-emails','Emails','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_emails?ID='.$Campaign->id, 'Emails', $Campaign->id);
		$Page->init('Emails', $Emails);
        $Page->init('total_emails_match', $iTotalEmails);
        return $Page;
    }

	function viewEmail(CampaignModel $Campaign, VarContainer $C){

	    $v = $this->getPage('campaign-account','Account','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_accounts?ID='.$Campaign->id, 'Accounts', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_account?aid='.$_GET['aid'].'&ID='.$Campaign->id, 'Account Details', $Campaign->id);
		$account = AccountsModel::Fetch('id = %d', $_GET['aid']);

		$default_sort = DataTableResult::getDefaultColumn('Contacts');
        $PQ =   $this->queryContacts($Campaign, $C)->orderBy($default_sort['Name'])->calc_found_rows();
		$PQ->Where('(T.account_id = '.$_GET['aid'].')');

        $Contacts  =   $PQ->Exec()->fetchAll();
        $iTotalContacts    =   $this->queryColumn('SELECT FOUND_ROWS()');


        $QT =   $this->queryProposals($Campaign, $C)->orderBy('T.last_updated DESC')->Limit(0, 10)->calc_found_rows();
		$QT->Where('(T.account_id = '.$_GET['aid'].')');

        //var_dump($QT);
        $Proposals  =   $QT->Exec()->fetchAll();
        $iTotalProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');


		$v->init('account', $account);
		$v->init('Contacts', $Contacts);
        $v->init('total_contacts_match', $iTotalContacts);
		$v->init('Proposals', $Proposals);
        $v->init('total_proposals_match', $iTotalProposals);

		return $v;
    }
	function viewEmailSettings(CampaignModel $Campaign, VarContainer $C){

		if($this->getUser()->can('crm_only')) $v   =   $this->getPage('edit-email-settings','Email Settings','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $v   =   $this->getPage('edit-email-settings','Email Settings','campaign'/* $menu */);
		$this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_emailsettings?ID='.$Campaign->id, 'Edit Email Settings', $Campaign->id);
		$eset = EmailSettingsModel::Fetch('cid = %d', $_GET['ID']);


		if (!$eset) {
            $MM = new ModelManager();
			$parrr = array(
                'cid' => $Campaign->id);
			$esetNEW = $MM->doSaveEmailSetting($parrr, $parrr['id'], EmailSettingsDataAccess::FromController($this));
			header('Location: '.$_SERVER['REQUEST_URI']);
        }

		$v->init('eset', $eset);

		return $v;
    }
	function viewPhoneLine(CampaignModel $Campaign, VarContainer $C){

		if($this->getUser()->can('crm_only')) $v   =   $this->getPage('edit-phone','Phone Settings','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $v   =   $this->getPage('edit-phone','Phone Settings','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_phones?ID='.$Campaign->id, 'Phone Lines', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_phoneline?lid='.$_GET['lid'].'&ID='.$Campaign->id, 'Edit Phone Line', $Campaign->id);
		$phone = PhoneModel::Fetch('id = %d', $_GET['lid']);

		if($this->getUser()->can('is_system')){
			$dataAccess = new UserDataAccess();
			$ownerUser = $dataAccess->getUser($phone->user_id);
			$v->init('storagePath', $ownerUser->getStorageUrl());
		}

		$v->init('phone', $phone);

		return $v;
    }
	function queryPhones(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('PhoneModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }
    function ajaxPhones(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Phones'));
        $qP =   $this->queryPhones($Campaign, $C)->orderBy('T.date_created DESC')->Limit(0, 10)->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.phone_num LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/phones', false, false), 'Phone');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }
	function viewPhones(CampaignModel $Campaign, VarContainer $C){

        $default_sort = DataTableResult::getDefaultColumn('Phones');
        $PQ =   $this->queryPhones($Campaign, $C)->orderBy('T.date_created DESC')->Limit(0, 10)->calc_found_rows();

        $Phones  =   $PQ->Exec()->fetchAll();
        $iTotalPhones    =   $this->queryColumn('SELECT FOUND_ROWS()');

        if($this->getUser()->can('crm_only')) $Page   =   $this->getPage('campaign-phones','Phone Lines','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $Page   =   $this->getPage('campaign-phones','Phone Lines','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('campaign_phones?ID='.$Campaign->id, 'Phone Lines', $Campaign->id);
		$Page->init('Phones', $Phones);
        $Page->init('total_phones_match', $iTotalPhones);
        return $Page;
    }

	function queryNotes(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('NoteModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

	function queryAccounts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('AccountsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

    function ajaxAccounts(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Accounts'));
        $qP =   $this->queryAccounts($Campaign, $C)->orderBy('T.last_updated DESC')->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/accounts', false, false), 'Account');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewAccounts(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Accounts');
        $PQ =   $this->queryAccounts($Campaign, $C)->orderBy('T.last_updated DESC')->Limit(0, 10)->calc_found_rows();

        $Accounts  =   $PQ->Exec()->fetchAll();
        $iTotalAccounts    =   $this->queryColumn('SELECT FOUND_ROWS()');

        if($this->getUser()->can('crm_only'))  $Page   =   $this->getPage('campaign-accounts','Accounts','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $Page   =   $this->getPage('campaign-accounts','Accounts','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_accounts?ID='.$Campaign->id, 'Accounts', $Campaign->id);
		$Page->init('Accounts', $Accounts);
        $Page->init('total_accounts_match', $iTotalAccounts);
        return $Page;
    }

	function viewAccount(CampaignModel $Campaign, VarContainer $C){

	    if($this->getUser()->can('crm_only')) $v = $this->getPage('campaign-account','Account','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $v = $this->getPage('campaign-account','Account','campaign'/* $menu */);

        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_accounts?ID='.$Campaign->id, 'Accounts', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_account?aid='.$_GET['aid'].'&ID='.$Campaign->id, 'Account Details', $Campaign->id);
		$account = AccountsModel::Fetch('id = %d', $_GET['aid']);

		$default_sort = DataTableResult::getDefaultColumn('Contacts');
        $PQ =   $this->queryContacts($Campaign, $C)->orderBy($default_sort['Name'])->Limit(0, 10)->calc_found_rows();
		$PQ->Where('(T.account_id = '.$_GET['aid'].')');

        $Contacts  =   $PQ->Exec()->fetchAll();
        $iTotalContacts    =   $this->queryColumn('SELECT FOUND_ROWS()');


        $QT =   $this->queryProposals($Campaign, $C)->orderBy('T.last_updated DESC')->Limit(0, 10)->calc_found_rows();
		$QT->Where('(T.account_id = '.$_GET['aid'].')');

        //var_dump($QT);
        $Proposals  =   $QT->Exec()->fetchAll();
        $iTotalProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$TQ =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->Limit(0, 10)->calc_found_rows();
		$TQ->Where('(T.account_id = '.$_GET['aid'].' AND T.status != "Completed")');
        $Tasks  =   $TQ->Exec()->fetchAll();


		$TLQ =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->calc_found_rows();
		$TLQ->Where('(T.account_id = '.$_GET['aid'].')');
        $TimleineTasks  =   $TLQ->Exec()->fetchAll();
		$iTotalTasks    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$NQ =   $this->queryNotes($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$NQ->Where('(T.account_id = '.$_GET['aid'].')');
		$Notes  =   $NQ->Exec()->fetchAll();
		$iTotalNotes    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$EQ =   $this->queryEmails($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$EQ->Where('(T.account_id = '.$_GET['aid'].')');
		$Emails  =   $EQ->Exec()->fetchAll();
		$iTotalEmails    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$TM =   $this->queryTexts($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$TM->Where('(T.account_id = '.$_GET['aid'].')');
        $Texts  =   $TM->Exec()->fetchAll();
        $iTotalTexts    =   $this->queryColumn('SELECT FOUND_ROWS()');

		foreach($Contacts as $phonecalls){
			$phonecheck = '1'.$phonecalls->phone.'';
			$IC =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
			$IC->orderBy('T.date DESC');
			$IC->Where('(T.caller_id = "'.$phonecheck.'" AND T.direction = "inbound")');
			$InboundCalls	=	$IC->Exec()->fetchAll();

			$OC =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
			$OC->orderBy('T.date DESC');
			$OC->Where('(T.destination = "'.$phonecheck.'" AND T.direction = "outbound")');
			$OutboundCalls  =   $OC->Exec()->fetchAll();
			$iTotalOutboundCalls    +=   $this->queryColumn('SELECT FOUND_ROWS()');

			foreach($InboundCalls as $ICalls){
				$calltimeline .= json_encode($ICalls->getTimeLinePhone(NULL, $phonecalls->id)).',';
			}
			foreach($OutboundCalls as $OCalls){
				$calltimeline .= json_encode($OCalls->getTimeLinePhone(NULL, $phonecalls->id)).',';
			}

		}






		//$Elist =   $this->queryEmails($Campaign, $C)->Where('T.account_id = 0');
		$Elist =   $this->queryParentEmails($Campaign, $C);
		$EmailTemplates  =   $Elist->Exec()->fetchAll();
		//var_dump($Elist);

		if(isset($_POST['emailTemplate']))
        {
			$fields = array('first_name', 'last_name',
            'phone', 'address', 'city', 'state',
            'email', 'title' , 'name' , 'zip' , 'website', 'signa'
                    );
			$contact = ContactModel::Fetch('id = %d', $_POST['contactid']);
			$emailtemplate = EmailsModel::Fetch('id = %d', $_POST['email_template']);

			foreach($fields as $key)
				{
						if($key=='name' || $key=='address' || $key=='city' || $key=='state' || $key=='zip'|| $key=='website'){

							$emailtemplate->body = str_replace('[['.$key.']]', $account->$key, $emailtemplate->body);

						} else if($key=='signa'){

							$eset = EmailSettingsModel::Fetch('cid = %d', $Campaign->id);
							$emailtemplate->body = str_replace('[['.$key.']]', $eset->$key, $emailtemplate->body);

						} else $emailtemplate->body = str_replace('[['.$key.']]', $contact->$key, $emailtemplate->body);
						$emailtemplate->subject = str_replace('[['.$key.']]', $contact->$key, $emailtemplate->subject);
						unset($key);
				}

			$emailtemplate->body = utf8_encode($emailtemplate->body);

			return $this->getAjaxView(array(
					'error' => false,
					'template' => $emailtemplate->body,
					'subject' => $emailtemplate->subject,
				));
			//return $this->getAjaxView($emailtemplate);
		}


		$v->init('account', $account);
		$v->init('Contacts', $Contacts);
        $v->init('total_contacts_match', $iTotalContacts);
		$v->init('Notes', $Notes);
		$v->init('Emails', $Emails);
		$v->init('Tasks', $Tasks);
        $v->init('total_tasks_match', $iTotalTasks);
		$v->init('TimleineTasks', $TimleineTasks);
		$v->init('Proposals', $Proposals);
        $v->init('total_proposals_match', $iTotalProposals);
		$v->init('EmailTemplates', $EmailTemplates);
		$v->init('Texts', $Texts);
		$v->init('total_texts_match', $iTotalTexts);
		$v->init('calltimeline', $calltimeline);
		$v->init('total_phone_match', $iTotalOutboundCalls);
		$v->init('total_emails_match', $iTotalEmails);
		$v->init('total_notes_match', $iTotalNotes);

		return $v;
    }

	function queryProposals(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

    function ajaxProposals(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Proposals'));
        $qP =   $this->queryProposals($Campaign, $C)->orderBy('T.last_updated DESC')->calc_found_rows();
		$qP->Where('(T.account_id = '.$_GET['aid'].')');
		if(isset($_GET['contactid'])) $qP->Where('(T.contact_id = '.$_GET['contactid'].')');

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/proposals', false, false), 'Proposal');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewProposals(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Proposals');
        $PQ =   $this->queryProposals($Campaign, $C)->orderBy('T.last_updated DESC')->Limit(0, 10)->calc_found_rows();
		$PQ->Where('(T.account_id = '.$_GET['aid'].')');

        $Proposals  =   $PQ->Exec()->fetchAll();
        $iTotalProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');

        if($this->getUser()->can('crm_only')) $Page   =   $this->getPage('campaign-proposals','Proposals','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $Page   =   $this->getPage('campaign-proposals','Proposals','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_proposals?ID='.$Campaign->id, 'Proposals', $Campaign->id);
		$Page->init('Proposals', $Proposals);
        $Page->init('total_proposals_match', $iTotalProposals);
        return $Page;
    }

	function viewProposal(CampaignModel $Campaign, VarContainer $C){

	    if($this->getUser()->can('crm_only')) $v = $this->getPage('campaign-proposal','Proposal','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $v = $this->getPage('campaign-proposal','Proposal','campaign'/* $menu */);
		$proposal = ProposalModel::Fetch('id = %d', $_GET['proid']);

		$PQ =   $this->queryProducts($Campaign, $C)->orderBy('T.sort ASC')->Limit(0, 10)->calc_found_rows();
		$PQ->Where('(T.proposal_id = '.$_GET['proid'].')');

        $Products  =   $PQ->Exec()->fetchAll();
        $iTotalProducts    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$Plist =   $this->queryProducts($Campaign, $C)->Where('T.proposal_id = 0');
		$ProductList  =   $Plist->Exec()->fetchAll();

		$pa = new ProductsDataAccess();
		$parent_products = $pa->getProductsbyParent($Campaign->user_parent_ID);


        $CQ =   $this->queryContacts($Campaign, $C)->orderBy('T.first_name ASC')->calc_found_rows();
		$CQ->Where('(T.account_id = '.$proposal->account_id.')');

        $Contacts  =   $CQ->Exec()->fetchAll();

		$Contact = ContactModel::Fetch('id = %d', $proposal->contact_id);
		$Account = AccountsModel::Fetch('id = %d', $proposal->account_id);

		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_account?aid='.$proposal->account_id.'&ID='.$Campaign->id, $Account->name, $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_proposal?proid='.$_GET['proid'].'&ID='.$Campaign->id, 'Proposal Details', $Campaign->id);


		 if(isset($_POST['sendTo'])){

			 		$ER = new EmailsDataAccess();

					$ER->sendProposalEmail($proposal, $Account, $Contact);

			 		if($error != true) {
						return $this->getAjaxView(array(
							'error' => false,
							'message' => 'Proposal Sent Successfully'
						));
					} else {

						return $this->getAjaxView(array(
							'error' => true,
							'message' => 'Error Sending Proposal'
						));
					}
		 }


		$v->init('proposal', $proposal);
		$v->init('Contact', $Contact);
		$v->init('Contacts', $Contacts);
		$v->init('Products', $Products);
        $v->init('total_products_match', $iTotalProducts);
		$v->init('ProductList', $ProductList);
		$v->init('parent_products', $parent_products);

		return $v;
    }

	function queryProducts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('ProductsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }
    function ajaxProducts(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Products'));
        $qP =   $this->queryProducts($Campaign, $C)->orderBy('T.sort ASC')->calc_found_rows();
		$qP->Where('(T.proposal_id = '.$_GET['proid'].')');

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/products', false, false), 'Product');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewProducts(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Products');
        $PQ =   $this->queryProducts($Campaign, $C)->orderBy('T.sort ASC')->Limit(0, 10)->calc_found_rows();
		$PQ->Where('(T.proposal_id = '.$_GET['proid'].')');

        $Products  =   $PQ->Exec()->fetchAll();
        $iTotalProducts    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-products','Proposals','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_products?ID='.$Campaign->id, 'Products', $Campaign->id);
		$Page->init('Products', $Products);
        $Page->init('total_products_match', $iTotalProducts);
        return $Page;
    }

    function queryUsers(CampaignModel $Campaign, VarContainer $C, $status = null, $role_filter = null){
        $query = Qube::Start()->queryObjects('UserModel', 'T', 'T.parent_id = %d', $this->User->getParentID())
            ->addFields('(' . (int) $this->User->getID() . ' = T.id) as is_current_user')
            ->leftJoin(array('user_info', 'i'), 'i.user_id = T.id')
            ->addFields("i.*, T.parent_id, IF(ISNULL(i.firstname), 'Unknown', CONCAT(i.firstname, ' ', i.lastname)) as fullname, T.id, i.profile_photo");

        // Check user list status.
        $check_status = array(
            'active' => 'access = 1 and canceled = 0',
            'suspended' => 'access = 0 and canceled = 0',
            'canceled' => 'canceled = 1'
        );

        if ($status && isset($check_status[$status])) {
            $query->Where($check_status[$status]);
        }

        if (is_string($role_filter)) {
            $query->Where("r2.role = '" . strval($role_filter) . "'");
        }

        $query->leftJoin('6q_userroles r', 'r.user_ID = T.id');
        $query->leftJoin('6q_roles r2', 'r.role_ID = r2.id');
        $query->addFields("r.role_ID as role_id");
        $query->addFields("r2.role as role");

        $query->orderBy('fullname ASC');

        return $query;
    }

	function queryTexts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

	function queryTasks(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('TasksModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

    function ajaxTasks(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'Tasks'));
        $qP =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->calc_found_rows();
		if(isset($_GET['leadid'])){
			$qP->Where('(T.lead_id = "'.$_GET['leadid'].'" AND T.status != "Completed")');
		} else {
			$qP->Where('(T.account_id = "'.$_GET['aid'].'" AND T.status != "Completed")');
			if(isset($_GET['contactid'])) $qP->Where('(T.contact_id = '.$_GET['contactid'].')');
		}


        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/tasks', false, false), 'Task');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewTasks(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('Tasks');
        $PQ =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->Limit(0, 10)->calc_found_rows();
		$PQ->Where('(T.account_id = '.$_GET['aid'].')');

        $Tasks  =   $PQ->Exec()->fetchAll();
        $iTotalTasks    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-tasks','Tasks','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_tasks?ID='.$Campaign->id, 'Tasks', $Campaign->id);
		$Page->init('Tasks', $Tasks);
        $Page->init('total_tasks_match', $iTotalTasks);
        return $Page;
    }
	function ajaxCompleteTask(VarContainer $params){

		$task_id = $params->getValue("Task_ID");

		$pdo = $this->getQube()->GetPDO('master');
		$pdo->query('UPDATE tasks SET status = "Completed", last_updated = now() WHERE id = ' . $task_id);

		if($error != true) {
			return $this->getAjaxView(array(
				'error' => false,
				'message' => 'Task Is Now Complete'
			));
		} else {

			return $this->getAjaxView(array(
				'error' => true,
				'message' => 'Error Completing Task'
			));
		}

	}
	function querySocialAccounts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('SocialAccountsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

    function ajaxSocialAccounts(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'SocialAccounts'));
        $qP =   $this->querySocialAccounts($Campaign, $C)->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/social-accounts', false, false), 'SocialAccount');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

    function viewSocialAccounts(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('SocialAccounts');
        $PQ =   $this->querySocialAccounts($Campaign, $C)->calc_found_rows();

        $SocialAccounts  =   $PQ->Exec()->fetchAll();
        $iTotalSocialAccounts    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-social-accounts','Social Accounts','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Marketing Automation', $Campaign->id);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-accounts?ID='.$Campaign->id, 'Social Accounts', $Campaign->id);
		$Page->init('SocialAccounts', $SocialAccounts);
        $Page->init('total_socialaccounts_match', $iTotalSocialAccounts);
        return $Page;
    }

	function viewSocialAccount(CampaignModel $Campaign, VarContainer $C){

	    $v = $this->getPage('campaign-social-account','Social Account','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Marketing Automation', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-accounts?ID='.$Campaign->id, 'Social Accounts', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-account?aid='.$_GET['aid'].'&ID='.$Campaign->id, 'Social Account Details', $Campaign->id);
		$socialaccount = SocialAccountsModel::Fetch('id = %d', $_GET['aid']);
		$v->init('socialaccount', $socialaccount);
		return $v;
    }

	function querySocialPosts(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('SocialPostsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

	function ajaxSocialPosts(CampaignModel $Campaign, VarContainer $C){
		$C->load(array('table' => 'SocialPosts'));
        $qP =   $this->querySocialPosts($Campaign, $C)->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.post_title LIKE :search OR T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/social-posts', false, false), 'SocialPost');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }
	function viewSocialPosts(CampaignModel $Campaign, VarContainer $C){
		//if($deny = $this->Protect('view_contacts'))
			//return $deny;
        $default_sort = DataTableResult::getDefaultColumn('SocialPosts');
		/** @var DBSelectQuery $PQ */
        $PQ =   $this->querySocialPosts($Campaign, $C)->calc_found_rows();
		$PQ->leftJoin(array('SocialSchedModel', 'sched'), 'T.id = sched.post_id');
		$PQ->leftJoin(array('SocialAccountsModel', 'account'), 'account.id = sched.social_id');
		$PQ->addFields("group_concat(account.name SEPARATOR ', ') accounts");
		$PQ->groupBy('T.id');
		$PQ->orderBy('T.publish_date DESC');
        $PQ->Limit(0, 10);
        $SocialPosts  =   $PQ->Exec()->fetchAll();
        $iTotalSocialPosts    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-social-posts','Social Posts','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Marketing Automation', $Campaign->id);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-posts?ID='.$Campaign->id, 'Social Posts', $Campaign->id);
		$Page->init('SocialPosts', $SocialPosts);
        $Page->init('total_socialposts_match', $iTotalSocialPosts);
        return $Page;
    }

	function viewSocialPostDetails(CampaignModel $Campaign, VarContainer $C){
		$v = $this->getPage('campaign-social-post-details','Social Post Details','campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Marketing Automation', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-posts?ID='.$Campaign->id, 'Social Posts', $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-post-details?pid='.$_GET['pid'].'&ID='.$Campaign->id, 'Social Post Details', $Campaign->id);
		$socialpost = SocialPostsModel::Fetch('id = %d', $_GET['pid']);
		/** @var DBSelectQuery $SocialAccounts */
		$SocialAccounts  = $this->querySocialAccounts($Campaign, $C)->calc_found_rows();
		$SocialAccounts->leftJoin(array('SocialSchedModel', 'sched'), 'T.id = sched.social_id AND sched.post_id = ' . $C->get('pid', 'intval'))->addFields('if(sched.id IS NULL, 0, 1) active');
		$SocialAccounts->orderBy('T.id');
		$SAccounts  =   $SocialAccounts->Exec()->fetchAll();
		$v->init('SAccounts', $SAccounts);
		$v->init('socialpost', $socialpost);
		return $v;
	}

		//function viewSocialAccount(CampaignModel $Campaign){
		//$v = $this->getPage('campaign-social-accounts','Social Accounts','campaign'/* $menu */);
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-accounts?ID='.$Campaign->id, 'Social Accounts', $Campaign->id);
       // return $v;
   // }

	//function viewSocialPosts(CampaignModel $Campaign){
		//$v = $this->getPage('campaign-social-posts','Social Posts','campaign'/* $menu */);
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-posts?ID='.$Campaign->id, 'Social Posts', $Campaign->id);
       // return $v;
    //}
    function queryNetworkListings(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('NetworkListingsModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }
    function ajaxDirectoryListings(CampaignModel $Campaign, VarContainer $C){
        $C->load(array('table' => 'NetworkListings'));
        $qP =   $this->querySocialAccounts($Campaign, $C)->calc_found_rows();

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
            $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);
        $DT->setRowCompiler($this->getView('rows/network-listings', false, false), 'NetworkListing');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }
    function viewDirectoryListings(CampaignModel $Campaign, VarContainer $C){
        //if($deny = $this->Protect('view_contacts'))
        //return $deny;

        $PQ =   $this->queryNetworkListings($Campaign, $C)->calc_found_rows();

        $NetworkListings  =   $PQ->Exec()->fetchAll();
        $iTotalNetworkListings    =   $this->queryColumn('SELECT FOUND_ROWS()');

        $Page   =   $this->getPage('campaign-network-listings','Network Listings','network'/* $menu */);
        //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_forms?ID='.$Campaign->id, 'Marketing Automation', $Campaign->id);
        //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_social-accounts?ID='.$Campaign->id, 'Social Accounts', $Campaign->id);
        $Page->init('NetworkListings', $NetworkListings);
        $Page->init('total_networklistings_match', $iTotalNetworkListings);
        return $Page;

        return $Page;
    }

    function viewDirectoryListing(CampaignModel $Campaign, VarContainer $C){

        $v = $this->getPage('campaign-network-listing','Network Listing','network'/* $menu */);
        $networklisting = NetworkListingsModel::Fetch('id = %d', $_GET['lid']);
        $v->init('networklisting', $networklisting);
        return $v;
    }

    static function getChartTimeContext($user_id = null, $is_reports_only = null) {
        // Default 7-Day Time Context
        if (empty($user_id) && empty($is_reports_only)) {
            return LeadsVisitsChartData::getTimeContext(self::chartCookieName, '7-DAY');

        } else {
            if ($user_id == 6610 || !$is_reports_only) {
                return LeadsVisitsChartData::getTimeContext(self::chartCookieName, '7-DAY');

            } else if ($user_id != 6610 && $is_reports_only) {
                return LeadsVisitsChartData::getTimeContext(self::chartCookieName, '30-DAY');

            }
        }

        //return LeadsVisitsChartData::getTimeContext(self::chartCookieName, '7-DAY'); <- OLD
    }

    /**
     * Display a particular campaign's dashboard.
     *
     * @param type $campaign_ID
     * @return type
     */
    function viewCampaignDashboard(CampaignModel $Campaign, $touchpoint_type = ''){
        // sanitize touchpoint type
        $type_info  =   self::getTouchPointTypes($touchpoint_type);
        $touchpoint_type = ($touchpoint_type && $type_info) ? $touchpoint_type : NULL;
        if($touchpoint_type){
            $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_dashboard?ID='.$Campaign->id.'&pointtype='.$touchpoint_type,$type_info[1]);
        } else{
            $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name);
        }
        $cda = CampaignDataAccess::FromController($this);
//        $analytics  =   new DashboardAnalytics;
        $track  =   array('t.cid' =>  $Campaign->id);

        if($touchpoint_type)
            $track['touchpoint_type']   =   $touchpoint_type;
        else {
            $touchpoint_type    =   '';
        }
//        $analytics->prepareHubTrackingIds($track, '6q_trackingids');


        $V  = $this->getPage('dashboard-websites','Campaign Dashboard','campaign'/* $menu */);
        $campaign_ID    =   $Campaign->id;
        $Chart  =   new LeadsVisitsChartData(self::chartCookieName, '30-DAY', false, array('cid' => $campaign_ID, 'touchpointType' => $touchpoint_type,
                    'controller'    => $this));

        $timevars   =   VarContainer::getContainer($Chart->getConfig());
        $timecontext = new PointsSetBuilder($timevars, $this->getUser()->created);
        $V->init('kwranks', RankingDataAccess::FromController($this)->
            getTopRankingKeywordsBySearchEngine($timecontext, $this, array('campaign_ID' => $campaign_ID,
            'TOUCHPOINT_TYPE' => $touchpoint_type)));


        // NORMAL WEBSITES
        $Websites = TouchPointQuery::FromController($this)->ProcessQueryRequest(array(
            'orderCol' => 'created',
            'orderDir' => 'desc',
            'calc_found_rows' => 1,
            'campaign_id' => $campaign_ID,
            'touchpoint_type' => $touchpoint_type,
            'timecontext'	=> clone $timecontext,
            'limit' => 10,
            'limit_start' => 0
        ));
        $total_tp_sites = $cda->FOUND_ROWS();

        // TOP PERFORMING WEBSITES
        $TopWebsites = TouchPointQuery::FromController($this)->ProcessQueryRequest(array(
            'campaign_id' => $campaign_ID,
            'touchpoint_type' => $touchpoint_type,
            'timecontext'	=> clone $timecontext,
            'orderCol' => 'conversion',
            'orderDir' => 'DESC',
            'limit' => 5,
            'limit_start' => 0
        ));
        $total_top_sites    =   $cda->FOUND_ROWS();


        // TOP PERFORMING KEYWORDS (From Searc Engine Referrals)
        $args	=	array('timecontext' => $timecontext, 'campaign_id' => $campaign_ID);
        // If touchpoint type is selected, add to args.
        if($touchpoint_type){
            $args['touchpoint_type'] = $touchpoint_type;
        }

        $topPerformingKeywords	= PiwikReportsDataAccess::FromController($this)->getTopPerformingKeywords($args);
        $V->init('TopPerformingKeywords', $topPerformingKeywords);

        $V->init('chart',   $Chart);
        // $analytics->purgeTemporaryTable();

        $this->addQuickStats(NULL, $Campaign->id);

        $user = $this->getUser();

        if($touchpoint_type != 'BLOG' && $this->getUser()->can('view_prospects'))
        {
            $PDA = ProspectDataAccess::FromController($this);
			$prospects = $PDA->getAccountProspects(
				array('cid' => $campaign_ID, 'TOUCHPOINT_TYPE' => $touchpoint_type, 'limit' => 10, 'import' => true, 'orderDir' => 'DESC', 'calc_found_rows' => 1, 'timecontext' => $timecontext)
			);
			$V->init('prospects', $prospects);
            $V->init('totalProspects', $PDA->FOUND_ROWS());
        }



		$PR = PhonesDataAccess::FromController($this);
		$PhoneRecords = $PR->getCallsPast30days($this->User->getID(), $Campaign->id);

        $V->init('callRecordsChart',$PhoneRecords);
        $V->init('KeywordsWidget', new KeywordsWidget($Chart->getConfig(), $this->user_ID));

        // load sites
        $V->init('sites', $Websites);
        $V->init('total_tp_sites', $total_tp_sites);

        // load top performing sites
        $V->init('topsites', $TopWebsites);
        $V->init('total_top_sites', $total_top_sites);


//        $V->init('statsTable', new RenderedTable($this->getWebsitesStats($Campaign)));
        $V->init('touchpoint_type', $touchpoint_type);
        $V->init('touchpoint_type_info', $type_info);
        $timevars->campaign_ID = $campaign_ID;
        // $V->init('kwranks', Rankings::getTopRankings($timecontext);
        $V->init('selected_pointtype_label', $touchpoint_type == '' ? 'Touchpoints' : $type_info[1]);
        $V->init('campaign_id', $Campaign->id);
        $V->init('_title', 'Campaign: ' . $Campaign->name);
        $V->init('menu','campaign'/* $menu */);
        $V->init('touchPointTypes', self::getTouchPointTypes());
		$V->init('user' , $user);
        $V->init('showDashboardRange', true);
        $V->init('campaignVideo', true);
        return $V;
    }

    /**
     * returns an array of queries necessary to generate a temporary table with graph values (dashboard_tempvisits)
     * @param type $t1
     * @param type $t2
     * @param type $name
     * @param type $hub_where_str
     * @param PDO $db
     * @return string|int
     */
    function getArchiveQueries($t1, $t2, $name  =   'nb_visits', $hub_where_str)
    {
        $m1 =   strtotime(date('Y-m-1', $t1));
        $m2 =   strtotime(date('Y-m-t', $t2));

        $xt =   $m1;
//        $t2 =   strtotime('+1 MONTH', $t2); // add 1 month
        $mi =   0;
        $qs =   array('CREATE TEMPORARY TABLE dashboard_summary_trackingids as select tracking_id, id as hub_id from ' .
                $this->q->getTable('WebsiteModel') . ' WHERE ' . DBWhereExpression::Create($hub_where_str));

        do{
            $mi++;
            $tbl    = Qube::PIWIKDB . '.analytics_archive_numeric_' . date('Y_m', $xt);
            $alias  =   "a$mi";
            $q   =   ($mi == 1 ? ' CREATE TEMPORARY TABLE dashboard_tempvisits AS ' : ' INSERT INTO dashboard_tempvisits ') .
                        "SELECT date1, SUM(value) as value FROM $tbl WHERE idsite IN (SELECT tracking_id FROM dashboard_summary_trackingids) AND
                    name    =   '$name' AND date1 = date2 " . ($mi == 1 ?
                        ' AND date1 >= FROM_UNIXTIME(' . $t1 . ') ' : '') . " GROUP BY date1";
            $qs[]   = $q;
            /*
            $q->Join(array($tbl, $alias),
                        "$alias.idsite = s.tracking_id AND $alias.name = '$name'");
             //             */
        }while(($xt = strtotime("+1 MONTH", $xt))    <  $m2);

#        $qs[]   =   str_replace('GROUP BY', ' AND array_pop($qs)
#        $qs[]   =   'SELECT date1, SUM(value) FROM dashboard_temp GROUP BY date1';

        return $qs;

        $value_colm =   array();
        for($i = 1; $i <= $mi; $i++)
        {
            $value_colm[]   =   "coalesce(a$i.value, 0)";
        }

        $q->addFields(join('+', $value_colm) . ' as value');

        return $mi;
    }

    function getGraphData_Old() // this is too beautiful to delete
    {
#        var_dump($_SESSION);

        $reseller_id    =   $this->Session->user_id;//
#        echo $reseller_id, '--- id';
        $ids_concat    =   $this->q->queryObjects('HubModel')->Fields('GROUP_CONCAT(tracking_id)')->Where('user_parent_id = %d', $reseller_id)->Exec()->fetchColumn();

        $format     = 'xml';

        //'week' returns data for the week that contains the specified 'date'
        $period = 'day';
        $date='last7';

        $TOKEN	=	'b00ce9652e919198e1c4daf7bf5aed5a';		// ALWAYS define TOKEns and security shit in a variable or constant.
        $url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&period=".$period."&date=".$date."&idSite=".$ids_concat."&token_auth=$TOKEN&format=".$format;

#        echo $url, "<br />";
        $data   = file_get_contents($url);
        $o  = simplexml_load_string($data);
        $data=  array();
        $q  =   $this->q->queryObjects('LeadModel')->groupBy('DATE(created)')->Fields('UNIX_TIMESTAMP(DATE(created))*1000 as d, count(*) as sum')
                    ->Where('(user_id = %d OR parent_id = %d) AND DATE_SUB(NOW(), INTERVAL 7 DAY) < created', $reseller_id, $reseller_id);
        $leaddata   =   $q->Exec()->fetchAll(PDO::FETCH_NUM);
        list($ymax, $ymin)            =   $q->asSubSelect()->Fields('MAX(sum), MIN(sum)')->Exec()->fetch(PDO::FETCH_NUM);

#        echo "--$ymax\n";
#        echo $max;

        $points =   array();
        foreach($o->result as $r):
#            echo "::",$r['idSite'];
#                        $data[1*(string)$r['idSite']]   =   array();
        foreach($r->result as $k => $num):
#                if((string)$num)
#                    $data[1*(string)$r['idSite']][(string)$num['date']]   +=   1*(string)$num;
                    $tk     =   strtotime((string)$num['date'])*1000;
                    $data[$tk]   +=   1*(string)$num;

                    $points[$tk]    =   array($tk, $data[$tk]);
        endforeach;
        endforeach;

        $ymin=  count($data) ? min($ymin,
                    min($data)
                ) : $ymin;
        $ymax   =  count($data) ? max($ymax, max($data)) : $ymax;
        $keys   =   array_keys($data);
        $min    = array_shift($keys);
        $max    =   array_pop($keys);


#        echo $data;//

#        var_dump($data);
        return array($min, $max, $ymin, $ymax, array_values($points), $leaddata);

#        var_dump($ids);
    }

    function getWebsitesStats(CampaignModel $C){
        return false;
        return $this->cacheCheck('sitestats_' . $C->id, 60 * 15);
    }

    function getCampsStats($timekey =   ''){
        return getflag('disable_cache') ? false : $this->cacheCheck($this->user_ID . '_atycs_campsstats' . $timekey, 60 * 15); // refresh every 15 minutes!
    }

    function queryNetworkArticles(CampaignModel $c)
    {
        return $this->User->queryNetworkItemLegacys()->Where('cid = %d AND trashed = 0', $c->getID());
    }

    function queryAgentsByRole() {
        //$users = UserListWidget::queryUsers($this)->Where('T.parent_id = %d', '170005')->Exec()->fetch();
        //return $users;
        /*$query = $this->User->queryUsers('parent_id = 170005')
            ->orderBy('username');

        return $query;*/
    }

    function queryCampaigns()
    {
        $q = $this->User->queryCampaigns('trashed = 0')
            ->addFields('DATE_FORMAT(date_created, "%M %D, %Y") as created_str')
            ->orderBy('name');

        return $q;
    }

    /**
     * Returns the user landing view (dashboard-home).
     * protected by view_HOME, on access denies redirects to campaign_dashboard
     *
     * @return bool|HttpRedirectView|ThemeView
     */
    function viewHome()
    {
        if($deny = $this->Protect('view_HOME'))
        {
            return $this->ActionRedirect('Campaign_dashboard');
        }

        if($this->User->parent_id==6312) {
            #SuperCircuits dashboard
            $V = $this->getPage('dashboard-home-supercircuits','Home','campaign'/* $menu */,array('not-campaign','superCircuits-nav'));
        }
        else if($this->User->parent_id==48205) {
            #Sales Dashboard
            $V = $this->getPage('dashboard-home-sales','Sales Dashboard','campaign'/* $menu */,array('not-campaign','home-nav'));
        }
        else {
            #Sales Dashboard
            $V = $this->getPage('dashboard-home-default','Home','campaign'/* $menu */,array('not-campaign','default-nav'));
        }


        $salesData = new SalesDataAccess();
        $userID = $salesData->setSalesUserID($this->User->id);
        $salesproplus = $salesData->checkClass($this->User->id, $type = 'salesProfessionalPlus');
        $salespro = $salesData->checkClass($this->User->id, $type = 'salesProfessional');
        //IF SALES MANAGER
        if( $salesData->isManager($userID) )
        {
            $sales = $salesData->getManagerSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
            $clients = $salesData->getClientSoldManager( $userID );
            $team = $salesData->getSalesTeam( $userID );
            $V->init('teamData', $team);
        }
        //IF SALES PROFESSIONAL
        else
        {
            $sales = $salesData->getSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
            $clients = $salesData->getClientSold( $userID );
        }

        $V->init('saleData', $sales);
        $V->init('clientsData', $clients);
        $V->init('salesTotal', $salesTotal);
        $V->init('isManager', $salesData->isManager($userID));
        $V->init('isSalesProPlus', $salesproplus);
        $V->init('isSalesPro', $salespro);
        return $V;
    }

	function ajaxCampaigns(VarContainer $C){
		$C->load(array('table' => 'CRMCampaigns'));
        $qP =   Qube::Start()->queryObjects('CampaignModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
		$qP->Where('T.trashed = 0');

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);


        $DT->setRowCompiler($this->getView('rows/campaigns', false, false), 'Campaign');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function ajaxInboxCalls(VarContainer $C){
		$C->load(array('table' => 'InboxCalls'));
        $qP =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
		$qP->Where('T.direction = "inbound"');
		$qP->orderBy('T.date DESC');

        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.caller_id LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

        $DT =   new DataTableResult($qP, $C);


        $DT->setRowCompiler($this->getView('rows/inbox-calls', false, false), 'InboxCall');


        return $this->getAjaxView($DT->getArray($qparams));
//        $this->ajaxDataTableSet($C, $qP);
    }

	function viewInboxCalls()
    {
		$V = $this->getPage('inbox-calls','Calls Inbox','sales-crm'/* $menu */,array('not-campaign','crm-nav'));

		$default_sort = DataTableResult::getDefaultColumn('InboxCalls');
        $PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
		$PQ->Where('T.direction = "inbound" ORDER BY T.date DESC');
		$InboxCalls =   $PQ->Exec()->fetchAll();

        $iTotalPhoneRecords    =   $this->queryColumn('SELECT FOUND_ROWS()');

		//NEW Initiate:
		$V->init('totalRecords', $iTotalPhoneRecords);
		$V->init('InboxCalls', $InboxCalls);

		return $V;
	}

    function ajaxInboxTexts(VarContainer $C){
        $C->load(array('table' => 'InboxTexts')); 
        $qP =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
        $qP->Where('T.direction = "inbound"');
        $qP->orderBy('T.created DESC');
        
        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.from_text LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }
        
        $DT =   new DataTableResult($qP, $C);        

        $DT->setRowCompiler($this->getView('rows/inbox-texts', false, false), 'InboxText');
                
        return $this->getAjaxView($DT->getArray($qparams));
    }

    function viewInboxTexts()
    {
        $V = $this->getPage('inbox-texts','Texts Inbox','sales-crm'/* $menu */,array('not-campaign','crm-nav'));
        
        $default_sort = DataTableResult::getDefaultColumn('InboxTexts');
        $TQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
        $TQ->Where('T.direction = "inbound" ORDER BY T.created DESC');
        $InboxTexts  =   $TQ->Exec()->fetchAll();

        $iTotalTextRecords    =   $this->queryColumn('SELECT FOUND_ROWS()');

        //NEW Initiate:
        $V->init('totalRecords', $iTotalTextRecords);
        $V->init('InboxTexts', $InboxTexts);
        
        return $V;
    }

    function ajaxInboxVoicemails(VarContainer $C){
        $C->load(array('table' => 'InboxVoicemails'));
        $qP =   Qube::Start()->queryObjects('VoicemailModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
        $qP->orderBy('T.created DESC');
        
        $qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.created LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }
        
        $DT =   new DataTableResult($qP, $C);
        

        $DT->setRowCompiler($this->getView('rows/inbox-voicemails', false, false), 'InboxVoicemail');

                
        return $this->getAjaxView($DT->getArray($qparams));
    }
    function viewInboxVoicemails()
    {
        $V = $this->getPage('inbox-voicemails','Voicemails Inbox','sales-crm'/* $menu */,array('not-campaign','crm-nav'));
        
        $default_sort = DataTableResult::getDefaultColumn('InboxVoicemails');

        $VQ     =   Qube::Start()->queryObjects('VoicemailModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
        $VQ->orderBy('T.created DESC');
        $InboxVoicemails =   $VQ->Exec()->fetchAll();

        $iTotalVoiceRecords    =   $this->queryColumn('SELECT FOUND_ROWS()');

        //NEW Initiate:
        $V->init('totalRecords', $iTotalVoiceRecords);
        $V->init('InboxVoicemails', $InboxVoicemails);
        
        return $V;
    }
	function viewSalesFeed()
    {
		$V = $this->getPage('sales-feed','Sales Feed','sales-crm'/* $menu */,array('not-campaign','crm-nav'));

		$today = date('Y-m-d');
		$date = new DateTime($today);
		$date->modify('-3 days');
		$querydate = $date->format('Y-m-d');

		$datePlus = new DateTime($today);
		$datePlus->modify('+15 days');
		$querydatePlus = $datePlus->format('Y-m-d');

		$dateMinus = new DateTime($today);
		$dateMinus->modify('-15 days');
		$querydateMinus = $date->format('Y-m-d');

		//total texts
		$TQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$TQ->Where('T.direction = "inbound" AND T.created >= "'.$querydate.'" AND T.trashed = "0000-00-00" ORDER BY T.created DESC');
		$Texts  =   $TQ->Exec()->fetchAll();
		//total calls
		$PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$PQ->Where('T.direction = "inbound" AND T.date >= "'.$querydate.'" ORDER BY T.date DESC');
		$Calls =   $PQ->Exec()->fetchAll();

		//total voicemails
		$VQ     =   Qube::Start()->queryObjects('VoicemailModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$VQ->Where('T.created >= "'.$querydate.'" ORDER BY T.created DESC');
		$Voicemails =   $VQ->Exec()->fetchAll();

		//total proposal cards
		$PROQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$PROQ->Where('T.expire_date BETWEEN "'.$querydateMinus.'" AND "'.$querydatePlus.'" AND T.status = "Active" AND T.trashed = "0000-00-00" ORDER BY T.expire_date DESC');

		$Proposals =   $PROQ->Exec()->fetchAll();

		//total leads
		$LQ     =   Qube::Start()->queryObjects('ProspectModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 30)->calc_found_rows();
		$LQ->Where('T.trashed = 0 AND T.spampoints = 0 ORDER BY T.created DESC');
		$Leads  =   $LQ->Exec()->fetchAll();
        $iTotalLeads   =   $this->queryColumn('SELECT FOUND_ROWS()');


		$V->init('Texts', $Texts);
		$V->init('Calls', $Calls);
		$V->init('Voicemails', $Voicemails);
		$V->init('Proposals', $Proposals);
		$V->init('Leads', $Leads);
		$V->init('iTotalLeads', $iTotalLeads);

		return $V;
	}

	function viewSalesCRM()
    {

		$V = $this->getPage('sales-crm','CRM Dashboard','sales-crm'/* $menu */,array('not-campaign','crm-nav'));
        $rangeValid = false;
		if ($_GET['d']) {
            $reportDate = $_GET['d'];
            $date = DateTime::createFromFormat("Y-m-d", $_GET['d']);
            $year = $date->format("Y");
            $month = $date->format("m");
            $reportDM = $year.'-'.$month;
        }
		else {
			$reportDate = date('Y-m-01');
			$reportDM = date('Y-m');
		}

        // Range of dates. r is the second date.
        if ($_GET['d'] and $_GET['r'] and ($_GET['r'] != $_GET['d'])) {
            $rangeValid = true;
            $reportDate2 = $_GET['r'];
            $date2 = DateTime::createFromFormat("Y-m-d", $_GET['d']);
            $year = $date->format("Y");
            $month = $date->format("m");
            $reportDM2 = $year.'-'.$month;

            if ($date2 < $date) { // ordering Dates, customer can use any order
                $reportDate1    = $reportDate2;
                $reportDate2    = $reportDate;
                $reportDM1      = $reportDM2;
                $reportDM2      = $reportDM;
            }else{
                $reportDate1    = $reportDate;
                $reportDM1      = $reportDM;
            }
        }

		//outbound calls
		if($this->User->getID()==$this->User->parent_id) $PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$PQ->Where( (($rangeValid) ?  'T.date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.date >= "'.$reportDate.'"') .' AND T.direction = "outbound"');
		$OutBoundCalls  =   $PQ->Exec()->fetchAll();
		$iTotalOBCalls    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//inbound calls
		if($this->User->getID()==$this->User->parent_id) $IPQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $IPQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$IPQ->Where((($rangeValid) ?  'T.date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.date >= "'.$reportDate.'"') .' AND T.direction = "inbound"');
		$InboundCalls  =   $IPQ->Exec()->fetchAll();
		$iTotalIBCalls    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total talk time
		if($this->User->getID()==$this->User->parent_id) $iTotalIBTalkTime  =   $this->
					query('SELECT SUM(length) FROM phone_records WHERE user_parent_id = "'.$this->User->getID().'" AND '.(($rangeValid) ?  'date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'date >= "'.$reportDate.'"'))
				->fetchColumn();	
		else $iTotalIBTalkTime  =   $this->
					query('SELECT SUM(length) FROM phone_records WHERE user_id = "'.$this->User->getID().'" AND '.(($rangeValid) ? 'date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'date >= "'.$reportDate.'"'))
				->fetchColumn();
		//total emails
		if($this->User->getID()==$this->User->parent_id) $EQ     =   Qube::Start()->queryObjects('EmailsModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $EQ     =   Qube::Start()->queryObjects('EmailsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$EQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Emails  =   $EQ->Exec()->fetchAll();
		$iTotalEmails    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total texts
		if($this->User->getID()==$this->User->parent_id) $TQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $TQ     =   Qube::Start()->queryObjects('TextsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$TQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Texts  =   $TQ->Exec()->fetchAll();
		$iTotalTexts    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total notes
		if($this->User->getID()==$this->User->parent_id) $NQ     =   Qube::Start()->queryObjects('NoteModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $NQ     =   Qube::Start()->queryObjects('NoteModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$NQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Notes  =   $NQ->Exec()->fetchAll();
		$iTotalNotes    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total tasks
		if($this->User->getID()==$this->User->parent_id) $TSQ     =   Qube::Start()->queryObjects('TasksModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $TSQ     =   Qube::Start()->queryObjects('TasksModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$TSQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Tasks  =   $TSQ->Exec()->fetchAll();
		$iTotalTasks    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total proposals created
		if($this->User->getID()==$this->User->parent_id) $PRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $PRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$PRQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.expire_date LIKE "%'.$reportDM.'%" AND T.trashed = 0');
		$Proposals  =   $PRQ->Exec()->fetchAll();
		$iTotalProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total proposals won
		if($this->User->getID()==$this->User->parent_id) $CPRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $CPRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$CPRQ->Where((($rangeValid) ?  'T.accepted_date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.accepted_date LIKE "%'.$reportDM.'%"').' AND T.trashed = 0');
		$ClosedProposals  =   $CPRQ->Exec()->fetchAll();
		$iTotalClosedProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total proposals active
		if($this->User->getID()==$this->User->parent_id) $APRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $APRQ     =   Qube::Start()->queryObjects('ProposalModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$APRQ->Where((($rangeValid) ?  'T.expire_date BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.expire_date LIKE "%'.$reportDM.'%"').' AND T.trashed = 0');
		$ActiveProposals  =   $APRQ->Exec()->fetchAll();
		$iTotalActiveProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total leads
		if($this->User->getID()==$this->User->parent_id) $LQ     =   Qube::Start()->queryObjects('ProspectModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $LQ     =   Qube::Start()->queryObjects('ProspectModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$LQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Leads  =   $LQ->Exec()->fetchAll();
		$iTotalLeads   =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total Accounts
		if($this->User->getID()==$this->User->parent_id) $AQ     =   Qube::Start()->queryObjects('AccountsModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $AQ     =   Qube::Start()->queryObjects('AccountsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$AQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Accounts  =   $AQ->Exec()->fetchAll();
		$iTotalAccounts   =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total Contacts
		if($this->User->getID()==$this->User->parent_id) $CQ     =   Qube::Start()->queryObjects('ContactModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->calc_found_rows();
		else $CQ     =   Qube::Start()->queryObjects('ContactModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();
		$CQ->Where((($rangeValid) ?  'T.created BETWEEN "'.$reportDate1.'" AND "'.$reportDate2.'"' : 'T.created >= "'.$reportDate.'"') .' AND T.trashed = 0');
		$Contacts  =   $CQ->Exec()->fetchAll();
		$iTotalContacts   =   $this->queryColumn('SELECT FOUND_ROWS()');
		//total Campaigns
		$CPQ     =   Qube::Start()->queryObjects('CampaignModel', 'T', 'T.user_parent_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();$CPQ     =   Qube::Start()->queryObjects('CampaignModel', 'T', 'T.user_id = %d', $this->User->getID())->Limit(0, 10)->calc_found_rows();
		$CPQ->Where('T.trashed = 0');
		$Campaigns  =   $CPQ->Exec()->fetchAll();
		$iTotalCampaigns   =   $this->queryColumn('SELECT FOUND_ROWS()');
		
	

		$V->init('iTotalOBCalls', $iTotalOBCalls);
		$V->init('iTotalIBCalls', $iTotalIBCalls);
		$V->init('iTotalIBTalkTime', $iTotalIBTalkTime);
		$V->init('iTotalEmails', $iTotalEmails);
		$V->init('iTotalTexts', $iTotalTexts);
		$V->init('iTotalNotes', $iTotalNotes);
		$V->init('iTotalTasks', $iTotalTasks);
		$V->init('iTotalProposals', $iTotalProposals);
		$V->init('iTotalClosedProposals', $iTotalClosedProposals);
		$V->init('iTotalActiveProposals', $iTotalActiveProposals);
		$V->init('iTotalLeads', $iTotalLeads);
		$V->init('iTotalAccounts', $iTotalAccounts);
		$V->init('iTotalContacts', $iTotalContacts);
		$V->init('Proposals', $Proposals);
		$V->init('ClosedProposals', $ClosedProposals);
		$V->init('ActiveProposals', $ActiveProposals);
		$V->init('Leads', $Leads);
		$V->init('Campaigns', $Campaigns);
		$V->init('iTotalCampaigns', $iTotalCampaigns);
		return $V;
	}

    function viewResources()
    {
        if($deny = $this->Protect('view_HOME'))
        {
            return $this->ActionRedirect('Campaign_dashboard');
        }

        if($this->User->parent_id==6312) {
            $V  =  $this->getPage('resources-supercircuits','Resources','campaign'/* $menu */,array('not-campaign','superCircuits-nav'));
        }
        else if($this->User->parent_id==48205) {
            $V  =  $this->getPage('resources-home','Sales Resources','campaign'/* $menu */,array('not-campaign','home-nav'));
        }
		else {
            $V  =  $this->getPage('resources-home-default','Resources','campaign'/* $menu */);
        }

        $salesData = new SalesDataAccess();
        $userID = $salesData->setSalesUserID($this->User->id);
        $salesproplus = $salesData->checkClass($this->User->id, $type = 'salesProfessionalPlus');
        $salespro = $salesData->checkClass($this->User->id, $type = 'salesProfessional');

        //IF SALES MANAGER
        if( $salesData->isManager($userID) )
        {
            $sales = $salesData->getManagerSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
        }
        //IF SALES PROFESSIONAL
        else
        {
            $sales = $salesData->getSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
        }

        $V->init('saleData', $sales);
        $V->init('salesTotal', $salesTotal);
        $V->init('isManager', $salesData->isManager($userID));
        $V->init('isSalesProPlus', $salesproplus);
        $V->init('isSalesPro', $salespro);

        return $V;
    }

    function viewCRM()
    {
        if($deny = $this->Protect('view_HOME'))
        {
            return $this->ActionRedirect('Campaign_dashboard');
        }

        $V  =  $this->getPage('crm-sales','CRM','home-sales'/* $menu */,array('not-campaign','home-nav'));

        $salesData = new SalesDataAccess();
        $userID = $salesData->setSalesUserID($this->User->id);
        $salesproplus = $salesData->checkClass($this->User->id, $type = 'salesProfessionalPlus');
        $salespro = $salesData->checkClass($this->User->id, $type = 'salesProfessional');

        //IF SALES MANAGER
        if( $salesData->isManager($userID) )
        {
            $sales = $salesData->getManagerSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
        }
        //IF SALES PROFESSIONAL
        else
        {
            $sales = $salesData->getSales( $userID );
            $salesTotal = $salesData->getSalesTotal( $userID );
        }

        $V->init('saleData', $sales);
        $V->init('salesTotal', $salesTotal);
        $V->init('userid', $userID );
        $V->init('isManager', $salesData->isManager($userID));
        $V->init('isSalesProPlus', $salesproplus);
        $V->init('isSalesPro', $salespro);

        return $V;
    }

    function viewMainDashboard()
    {
        $user_id    =   $this->user_ID;

        // GET CAMPAIGNS WITH CURRENT DAY VISITS / LEADS
        $campaigns   =  $this->queryCampaigns()->calc_found_rows()->Limit(0, 10)->Exec()->fetchAll();
        // $total_camps    =   $this->queryColumn('SELECT FOUND_ROWS()');

        /*if($this->getUser()->getID() == 6610 || !$this->getUser()->can('reports_only')){
            $chartData      = new LeadsVisitsChartData('chartconfig','7-DAY',false,array('controller' => $this));
        } elseif($this->getUser()->getID() != 6610 && $this->getUser()->can('reports_only')){
            $chartData      = new LeadsVisitsChartData('chartconfig','30-DAY',false,array('controller' => $this));
        }*/

		$chartData      = new LeadsVisitsChartData('chartconfig','30-DAY',false,array('controller' => $this));

        $V  =  $this->getPage('dashboard-campaigns', 'Campaigns Dashboard', 'campaign');
        $timecontext = new PointsSetBuilder(VarContainer::getContainer($chartData->getConfig()));
        $Prospects	= ProspectDataAccess::FromController($this)->getAccountProspects(array(
            'calc_found_rows' => 1,
            'limit' => 10,
            'timecontext' => $timecontext
        ));
        $totalProspects = $this->queryColumn('SELECT FOUND_ROWS()');

		//load phone for 30 days
		$d2 = date('Y-m-d', strtotime('-30 days'));
		$default_sort = DataTableResult::getDefaultColumn('PhoneRecords');
		$PQ     =   Qube::Start()->queryObjects('PhoneRecordsModel', 'T', 'T.user_id = %d', $this->User->getID())->calc_found_rows();$PQ->Where('T.date >= "'.$d2.'"');
        $PQ->Limit(0, 10);
		$PQ->orderBy('T.date DESC');
        $PhoneRecords  =   $PQ->Exec()->fetchAll();
        $iTotalPhoneRecords    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$PR = PhonesDataAccess::FromController($this);
		$PCallRecords = $PR->getCallsPast30days($this->User->getID(), NULL);

		$DA = new UserDataAccess();
        $role = $DA->getRole($this->User->getID());


        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.user_id = @dashboard_userid', '6q_trackingids');

        $contextConfig	=	new VarContainer($chartData->getConfig());
        $contextConfig->ActionUser	=	$this->getUser();
        $timecontext = new PointsSetBuilder($contextConfig, $this->getUser()->created);

        $RDA = PiwikReportsDataAccess::FromController($this);

        $V->init('statsTable',	new AllCampaignsStatsTable($this));
        $topPerformingKeywords	= PiwikReportsDataAccess::FromController($this)
            ->getTopPerformingKeywords(array('timecontext' => $timecontext));
        $V->init('TopPerformingKeywords', $topPerformingKeywords);   //$panalytics->getAllKeywords());

        $V->init('kwranks', RankingDataAccess::getTopRankingKeywordsBySearchEngine($timecontext, $this));
        $V->init('chart', $chartData);
        $V->init('Prospects', $Prospects);
		$V->init('PhoneRecords', $PhoneRecords);
        $V->init('totalProspects', $totalProspects);
		$V->init('totalRecords', $iTotalPhoneRecords);
		$V->init('role', $role);
        $V->init('prospect_count', count($Prospects));
        $V->init('TopRankingKeywords', $RDA->getTopPerformingKeywords(compact('timecontext')));

			// here DTCampaignsResult takes a UserWithRolesModel and, if user has view_campaigns, displays all campaigns from the admin perspective
			// if he does not have view_campaigns, the user can still view his own campaigns.
        $table = new DataTableCampaignsResult($contextConfig);

//        $table->setRowCompiler($this->getView('rows/campaign-stats', false, false), 'camp');
        $campaignsTable = $table->getRowsData();

        $V->init('campaigns', $campaignsTable['campaigns']);
        $V->init('total_camps', $campaignsTable['totalDisplayRecords']);


        $V->init('touchpoints', $this->getTouchPointsStats());
        $this->addQuickStats($analytics);
        $V->init('viewChart', true);
        $V->init('showDashboardRange', true);
        $V->init('menu', 'home');
        $V->init('campaignsVideo', true);

        /* NEW: DASHBOARD PHONE RECORDS */

        // Include Phone library
        /*require_once(QUBEADMIN.'inc/phone.class.php');
        $phoneRec               = new Phone();
        $did                    = $phoneRec->getDID($this->getUser()->getID());
        $blacklist              = $phoneRec->blacklistCalls();
        $DID                    = $did['phone_num'];
        $created                = $did['date_created'];
        $y                      = date('Y');
        $m                      = date('m');
        $md                     = date('t');
        $d                      = date('d');
        $d30                    = date('Y-m-d',strtotime('-29 days'));
        $d7                     = date('Y-m-d',strtotime('-6 days'));
        $rqtypeChart            = $d30.','.$y.'-'.$m.'-'.$d;
        $rqtypeChart7           = $d7.','.$y.'-'.$m.'-'.$d;
        $rqtype                 = $y.'-'.$m.'-01,'.$y.'-'.$m.'-'.$md;
        // API Call Chart 30-DAY
        $callReportRecChart     = $phoneRec->getCallDetailsNew($DID,$rqtypeChart);
        $callReportsRecChart    = $callReportRecChart['response']['objects'];
        $totalResultsRecChart   = $callReportRecChart['response']['meta']['total_count'];
        // API Call Chart 7-DAY
        $callReportRecChart7    = $phoneRec->getCallDetailsNew($DID,$rqtypeChart7);
        $callReportsRecChart7   = $callReportRecChart7['response']['objects'];
        $totalResultsRecChart7  = $callReportRecChart7['response']['meta']['total_count'];
        // API Call Table
        $callReportRec          = $phoneRec->getCallDetailsNew($DID,$rqtype);
        $callReportsRec         = $callReportRec['response']['objects'];
        $totalResultsRec        = $callReportRec['response']['meta']['total_count'];*/
        // Variables
        //$V->init('totalResultsChart',$totalResultsRecChart);
		$V->init('totalResultsChart',$iTotalPhoneRecords);
        //$V->init('callReportsChart',$callReportsRecChart);
		//$jsonPhoneRecords = json_encode($PhoneRecords);
		//print_r($PhoneRecords);
		//print_r($callReportsRecChart);
		//$V->init('callReportsChart',$callReportsRecChart);
		$V->init('callReportsChart',$callReportsRecChart);
		$V->init('callRecordsChart',$PCallRecords);
        $V->init('rqtypeChart',$rqtypeChart);

        $V->init('totalResultsChart7',$totalResultsRecChart7);
        //$V->init('callReportsChart7',$callReportsRecChart7);
        $V->init('rqtypeChart7',$rqtypeChart7);

        $V->init('totalResults',$totalResultsRec);
        //$V->init('callReports',$callReportsRec);
        $V->init('rqtype',$rqtype);

        $V->init('blacklist',$blacklist);
        $V->init('phone',$phoneRec);
        $V->init('did',$DID);
        $V->init('created',$created);

        /* NEW: DASHBOARD PHONE RECORDS */

        return $V;
    }

    function ajaxDelete(VarContainer $params){

//        $stm = $this->getQube()->GetPDO()->prepare("SELECT cid FROM hub where id = @id");
//		$stm->execute(array("id" => $params->getValue("site_ID")));
//		$campaign_id = $stm->fetchColumn();
        $TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
        $result = $TrashMgr->TrashByParam($params, $message, true);
//        $touchpoints = $this->getTouchPointsStats($campaign_id);
        if(!isset($params->callback)){
            $params->callback = 'null';
        }
        return $this->getAjaxView(array('error' => $result ? 0 : 1, 'message' => $message, 'touchpoints' => $touchpoints, 'callback' => $params->get('callback') ));
    }

    function SaveFormField(array $data, $ID = 0){
        $FF =   new LeadFormFieldModel;
        $where  =   DBWhereExpression::Create('ID = %d AND user_ID = dashboard_userid()', $ID);
        $FF->set('ID', $ID);
        $data['ID'] =   $ID;
        unset($data['user_ID']);

        if(isset($data['options']) && is_array($data['options'])){
            $options    = LeadFormFieldModel::Query('ID = %d AND user_ID = dashboard_userid()', $ID)->Fields('options')->Exec()->fetchColumn();
            if($options)
                $options    =   unserialize($options);
            else
                $options    =   array();
            foreach($data['options'] as $index => $text){
                if(empty($text))
                    unset($options[$index]);
                else
                    $options[$index]  =   $text;
            }
            $data['options']    =   serialize(array_values($options));
        }
        $Validation =   new ValidationStack();

        $result =   array('error' => false, 'params' => $data);
        if(!$FF->SaveData($data, NULL, $Validation, NULL, $where, $result)){
            return $this->returnValidationError($Validation);
        }

        return $this->getAjaxView($result);
    }

    function ajaxCreateNew($params) {
        $ModelManager = new ModelManager();
        $result = array();

        $parrr = 0;
        if (isset($params['autoresponder'])) {
			$parrr = $params['autoresponder'];
			$result = $ModelManager->doSaveResponder($parrr, $parrr['id'], ResponderDataAccess::FromController($this));
        } elseif(isset($params['report'])){
            $parrr = $params['report'];
            $parrr['user_ID'] = $this->getUser()->getID();
            $result = $ModelManager->doSaveReport($parrr, $parrr['ID'], ReportDataAccess::FromController($this));
        } elseif(isset($params['ranking_site'])){
            $parrr = $params['ranking_site'];
            $parrr['user_ID'] = $this->getUser()->getID();
            $result = $ModelManager->doSaveRankingSite($parrr, RankingDataAccess::FromController($this));
        } elseif (isset($params['listing'])) {
            $parrr = $params['listing'];
//            $result =   $ModelManager->doSaveNetworkListing($params['listing'], (int)$params['listing']['id']);
            $result = $ModelManager->doSaveNetworkListing($params, 'listing', (int) $params['listing']['id'], QubeDataAccess::FromController($this));
        } elseif (isset($params['form'])) {
            $parrr = $params['form'];
			$parrr['user_id'] = $this->getUser()->getID();
			$parrr['cid'] = $params['campaign']['cid'];
//					$params['form']['cid']
            $result = $ModelManager->doSaveLeadForm($parrr, @$params['form']['ID'], CampaignDataAccess::FromController($this));
        } elseif (isset($params['field']))
        {
            $parrr = $params['field'];
            $result =   $ModelManager->doSaveLeadFormField($parrr, @$params['field']['ID'], LeadsDAO::FromController($this));
            if($result instanceof IOResultInfo)
            {
                $field = $result->getObject()->Refresh();
                $form = LeadFormModel::Query('id = %d', $field->form_ID)->Exec()->fetchObject('LeadFormModel');
                $result['row']  =   $this->getView('rows/leadform-field', false, false)
                    ->init('form', $form)
                    ->init('editable', true)
//                    ->init('form', $form) ->init('field', 0)
                    ->init('field', $field)
                    ->toString();
            }

        } elseif (isset($params['website'])) {
			$HDA  = new HubDataAccess();
			$HDA->setActionUser($this->getUser());
            $parrr = $params['website'];
			$result = $ModelManager->doSaveWebsite($params['website'], (int) $params['website']['id'], HubDataAccess::FromController($this));

        if ($result instanceof ValidationStack)
            return $this->returnValidationError($result);

			$result['touchpoints'] = $this->getTouchPointsStats((int) $parrr["cid"]);
		} elseif (isset($params['emailer'])) { // responder
            $parrr = $params['emailer'];
            $result = $ModelManager->doSaveEmailer($parrr, (int) $parrr['id'], QubeDataAccess::FromController($this));
        } elseif(isset($params['campaign'])){
			$parrr = $params['campaign'];
			$result = $ModelManager->doSaveCampaign($params['campaign'], (int) $params['campaign']['id'], QubeDataAccess::FromController($this));
		} elseif(isset($params['campaigncrm'])){
			$parrr = $params['campaigncrm'];
			$result = $ModelManager->doSaveCampaignCRM($params['campaigncrm'], (int) $params['campaigncrm']['id'], QubeDataAccess::FromController($this));
		}elseif(isset($params['keyword'])){
            $parrr = $params['keyword'];
        $result = $ModelManager->doSaveKeywords($params['keyword'], 0, RankingDataAccess::FromController($this));
//            $result = $this->ajaxSaveRankingKeywords($parrr);
        }elseif(isset($params['broadcast'])){
            $parrr = $params['broadcast'];
            $result = $ModelManager->doSaveBroadcastEmailer($parrr, $parrr['id'], EmailBroadcastDataAccess::FromController($this));
        }elseif(isset($params['contact'])){
			$parrr = $params['contact'];
			$result = $ModelManager->doSaveContact($parrr, $parrr['id'], ProspectDataAccess::FromController($this));
			if(!($result instanceof ValidationStack)) {

				$contact = ContactModel::Fetch('id = %d', $result['object']->id);
				$result['contacts'][] = $contact->getTimeLineContact();

            }
		} elseif (isset($params['proposal'])) {
            $parrr = $params['proposal'];
            $result = $ModelManager->doSaveProposal($parrr, $parrr['id'], ProposalsDataAccess::FromController($this));
			if(!($result instanceof ValidationStack)) {

				$proposal = ProposalModel::Fetch('id = %d', $result['object']->id);
				$result['proposals'][] = $proposal->getTimeLineProposal();

            }
		} elseif (isset($params['task'])) {
            $parrr = $params['task'];
            $result = $ModelManager->doSaveTask($parrr, $parrr['id'], TasksDataAccess::FromController($this));
			if(!($result instanceof ValidationStack)) {

				$task = TasksModel::Fetch('id = %d', $result['object']->id);
				$result['tasks'][] = $task->getTimeLineTask();

            }
		} elseif (isset($params['email'])) {
            $parrr = $params['email'];
            $result = $ModelManager->doSaveEmail($parrr, $parrr['id'], EmailsDataAccess::FromController($this));
			if(!($result instanceof ValidationStack)) {

				$email = EmailsModel::Fetch('id = %d', $result['object']->id);
				$result['emails'][] = $email->getTimeLineEmail($this->ActionUrl('Campaign_delete', array('Email_ID' => $email->id)));
            }
		} elseif (isset($params['prospect'])) {
            $parrr = $params['prospect'];
            $result = $ModelManager->doSaveProspect($parrr, $parrr['ID'], ProspectDataAccess::FromController($this));
		} elseif (isset($params['accounts'])) {
            $parrr = $params['accounts'];
            $result = $ModelManager->doSaveAccount($parrr, $parrr['id'], AccountsDataAccess::FromController($this));
		} elseif (isset($params['phoneline'])) {
            $parrr = $params['phoneline'];
            $result = $ModelManager->doSavePhoneLine($parrr, $parrr['id'], PhonesDataAccess::FromController($this));
		} elseif (isset($params['emailsettings'])) {
            $parrr = $params['emailsettings'];
            $result = $ModelManager->doSaveEmailSetting($parrr, $parrr['id'], EmailSettingsDataAccess::FromController($this));
		} elseif (isset($params['product'])) {
            $parrr = $params['product'];
            $result = $ModelManager->doSaveProduct($parrr, $parrr['id'], ProductsDataAccess::FromController($this));
		} elseif (isset($params['social_accounts'])) {
            $parrr = $params['social_accounts'];
            $result = $ModelManager->doSaveSocialAccount($parrr, $parrr['id'], SocialDataAccess::FromController($this));
		} elseif (isset($params['social_posts'])) {
                $parrr = $params['social_posts'];
                $result = $ModelManager->doSaveSocialPost($parrr, $parrr['id'], SocialDataAccess::FromController($this));
        } elseif (isset($params['social_sched'])) {
            if($params['social_sched']['post_date']=='0000-00-00') {
            } else {
                $parrr = $params['social_sched'];
                $result = $ModelManager->doSaveSocialSched($parrr, $parrr['id'], SocialDataAccess::FromController($this));
            }
        } elseif (isset($params['network_listings'])) {
            $parrr = $params['network_listings'];
            $result = $ModelManager->doSaveNetworkListing($parrr, $parrr['id'], NetworkDataAccess::FromController($this));
		}elseif(isset($params['note'])){
			$parrr = $params['note'];
			$result = $ModelManager->dosaveNote($parrr, $parrr['id'], QubeDataAccess::FromController($this));
            if(!($result instanceof ValidationStack)) {

				$note = NoteModel::Fetch('id = %d', $result['object']->id);
				$result['notes'][] = $note->getTimeLineNote($this->ActionUrl('Campaign_delete', array('note_ID' => $note->id)));
                //$notes = ProspectDataAccess::FromController($this)->RetrieveNotes(array('contact_id' => $result['object']->contact_id, 'cid' => $result['object']->cid));
                /** @var NoteModel $note */
               /* foreach ($notes as $note) {
                    $result['notes'][] = $note->getTimeLineNote($this->ActionUrl('Campaign_delete', array('note_ID' => $note->id)));
                }*/
            }
		}elseif(isset($params['collab'])){
            $parrr = $params['collab'];
            $result = $ModelManager->doSaveCollab($parrr, CampaignDataAccess::FromController($this));
        }


        if ($result instanceof ValidationStack)
            return $this->returnValidationError($result);

        $result['params'] = $parrr;
        return $this->getAjaxView($result);
    }

    /**
     * Returns a view for campaign controller, setting
     * $classes['campaign-nav'] =   'active',
     * and declares touchpointtypes view variable.
     *
     * @param string $view the basename of the view file ($view.php)
     * @param string $title the page title <title>$title</title>
     * @param string $menu the basename of the menu file (in menus/)


     * @param int $campaign_ID the campaign id to be used in the breadcrumbs
     * @param array $active_classes an array of identifiers. the identifiers can be used in the view, and will have a value of 'active'. example: class="<?= $classes[$identifier]; ?>"
     * @param array $classes an array of identifiers holding classname attributes. example: class="<?= $classes['myclass']; ?>" will print the value in the array provided to this function.
     * @return ThemeView
     */
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()) {

        if(!in_array('not-campaign', $active_classes))
            $active_classes[]   =   'campaign-nav';

        $P  =   parent::getPage($view, $title, $menu, $active_classes, $classes);
        $P->init('touchpointtypes', CampaignController::getTouchPointTypes());
        return $P;
    }

    function getCrumbsReports(ReportModel $current_report, $campaign_action, $home_action){
        $RDA = ReportDataAccess::FromController($this);
        $reports = $RDA->getAllReportData(array('user_id' => $this->getUser()->getID(), 'campaign_id' => $current_report->campaign_ID, 'orderCol' => "ID = {$current_report->ID} DESC, created", 'orderDir' => 'DESC'));

        $bread = new BreadCrumb(sprintf($home_action,$reports[0]->getID()),'Report: '.$reports[0]->name);
        for($i = 1; $i < count($reports); $i++){
            $r = $reports[$i];
            $child = new BreadCrumb(sprintf($campaign_action,$r->getID()),'Report: '.$r->name);
            $bread->addChild($child);
        }

        return $bread;
    }

    function viewDomains(){
        // @todo security
        $Hubs   =   $this->q->queryObjects('HubModel')
                    ->Where('user_id = %d AND httphostkey <> ""', $this->user_ID)
                    ->Exec();

        $P  =   $this->getPage('campaign-domains','Domains Manager','campaign'/* $menu */);
        $P->init('hubs', $Hubs->fetchAll());

        return $P;
    }

    function viewCrmIntegration(){
        $P  =   $this->getPage('campaign-crm','Crm Integration','campaign'/* $menu */);

        return $P;
    }

    function viewTrash(){
		if($deny = $this->Protect('view_trash'))
			return $deny;

        $P  =   $this->getPage('campaign-trash','Trash','campaign'/* $menu */);

        return $P;
    }

    function doUpdateSpamMessage($ID, $SETPART)
    {
        $r  =   $this->q->query('UPDATE %s SET ' . $SETPART . ' WHERE (user_id = dashboard_userid() OR parent_id = %d OR %d) AND id = %d',
            'ContactDataModel', $this->user_ID, $this->User->can('manage_spam'), $ID);
        return $r;
    }

    function doAjaxApproveSpamMessage($ID){
        if($q	=	$this->doUpdateSpamMessage($ID, 'spamstatus = 0'))
        {
            if($q->rowCount() == 1){
                $this->doProcessMessage($ID);
            }

            return $this->returnAjaxData ('approved', true);
        }
        return $this->returnAjaxError('Could not approve this message.');
    }

    function doAjaxDeleteSpamMessage($ID){
        if($this->doUpdateSpamMessage($ID, 'trashed = NOW()'))
            return $this->returnAjaxData ('deleted', true);

        return $this->returnAjaxError('Could not Delete this message.');
    }

    function doProcessMessage($ID){
        $proc   =   new ContactFormProcessor($this->q);

        /* @var $ContactForm ContactDataModel */
        $ContactForm    =   $this->q->queryObjects('ContactDataModel', 'T', 'id = %d', $ID)->Exec()->fetch();
        $user           = ContactFormProcessor::GetUserResellerInfoQuery($ContactForm->user_id)->Exec()->fetch();

//        $proc->DispatchNotifications($ContactForm, $user); // Disabled until mailer works.
    }

    function viewCampaignSpam($Campaign){/* @FER: BREADCRUMBS FIXED */
//        if($deny = $this->Protect('view_spam'))
//            return $deny;

        require_once HYBRID_PATH . '/models/ContactDataModel.php';
        $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_spam?ID='.$Campaign->id,'Spam');

        $approved   =   $deleted    =   false;
        if(isset($_GET['APPROVE']))
        {
            $approved   =   $this->doUpdateSpamMessage($_GET['APPROVE'], 'spamstatus = 0');
            if($approved->rowCount() == 1){
                $this->doProcessMessage($_GET['APPROVE']);
            }
        }else
            if(isset($_GET['DELETE']))
            {
                $deleted   =   $this->doUpdateSpamMessage($_GET['DELETE'], 'trashed = NOW()');
            }

        $wConfig    =   $_GET;
        $wConfig['max_text_length']    =   64;
        unset($wConfig['ID']);
        $wConfig['Campaign_ID'] = $Campaign->id;
        $W  =   new SpamWidget($this, $wConfig);

        $V  =   $this->getPage('campaign-spam','Campaign: '.$Campaign->name,'campaign'/* $menu */);

        if($approved)
            $V->init('spamaction', 'The message has been approved.');
        else
            if($deleted)
                $V->init('spamaction', 'The message has been deleted.');

        $W->initView($V);
        return $V;
    }

    function viewAjaxSpamMessage($ID){

        require_once HYBRID_PATH . '/models/ContactDataModel.php';

        $rq  =   $this->q->queryObjects('ContactDataModel')
            ->Where('spamstatus != 0 AND (user_id = %d OR parent_id = %d OR %d) AND id = %d',
                $this->user_ID, $this->user_ID, $this->User->can('manage_spam'), $ID)
            ->addFields('DAY(T.created) as day, substr(MONTHNAME(T.created), 1, 3) as monthname, YEAR(T.created) as year')
            ->Exec()
        ;
#        echo $rq;
        /*
        $rq =   $rq
                    ->Exec();
        */

        if(!$rq)
            return $this->returnAjaxError ('Could not load message');

        $message    =   $rq->fetch();

        return $this->returnAjaxData('message', $message);
    }

    function viewSettings(CampaignModel $Campaign){/* @FER: BREADCRUMBS FIXED */
        if($this->getUser()->can('crm_only')) $P  =   $this->getPage('campaign-settings','Campaign: '.$Campaign->name,'campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $P  =   $this->getPage('campaign-settings','Campaign: '.$Campaign->name,'campaign'/* $menu */);
        $this->getCrumbsRoot(true,true,$Campaign->id,$Campaign->name)->addCrumbTxt('Campaign_settings?ID='.$Campaign->id,'Email Notifications');
        $CDA = CampaignDataAccess::FromController($this);
        $defaultCollabColumn = DataTableResult::getDefaultColumn('CampaignCollabs');
        $Collabs = $CDA->getCollabs(array(
            'campaign_id' => $Campaign->getID(),
            'calc_found_rows' => true,
            'orderCol' => $defaultCollabColumn['Name'],
            'orderDir' => 'ASC',
            'limit' => 10
        ));
        $totalCollabs = $CDA->FOUND_ROWS();
        $preloaded_touchpoints  = DataTablePreProcessor::RunIt($this,
            array(
                'limit' => 100,
                'where' => array('active' => 1,
                    'hosthash <> ""', 'campaign_ID' => $Campaign->getID())
            ))->getTouchPoints();
        $P->init('websites', $preloaded_touchpoints);
        $P->init('collabs', $Collabs);
        $P->init('totalCollabs', $totalCollabs);
        return $P;
    }

    function viewNotificationEdit(CampaignModel $campaign, $collab_id){/* @FER: BREADCRUMBS FIXED */
        $P  =   $this->getPage('campaign-notification-edit','Campaign: '.$campaign->name,'campaign'/* $menu */);
		$CDA = CampaignDataAccess::FromController($this);
		$collab = $CDA->getCollabs(array(
			'returnSingleObj' => true,
			'campaign_id' => $campaign->getID(),
			'limit' => 1,
            'collab_id' => $collab_id
		));

		$websites = $CDA->getCollabWebsitesReferences(array(
			'collab_id' => $collab->getID()
		));
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_settings?ID='.$campaign->id,'Email Notifications: '.$campaign->name);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_edit-notification?ID='.$campaign->id.'&Collab='.$collab_id,'Response: '.$collab->name);

        $P->init('collab', $collab);
		$P->init('websites', $websites);
        return $P;
    }


    function viewRankings(CampaignModel $Campaign){
		if($deny = $this->Protect('view_rankings'))
			return $deny;

        $rda = new RankingDataAccess();

        $default_sort = DataTableResult::getDefaultColumn('RankingSites');

        $rankingSites = $rda->getAllRankingData(array(
            'user_id' => $Campaign->user_id,
            'campaign_id' => $Campaign->getID()
        ));

        $rankingSitesCount = $rda->getAllRankingCount(array(
            'user_id' => $Campaign->user_id,
            'campaign_id' => $Campaign->getID()
        ));

        $preloaded_touchpoints  = DataTablePreProcessor::RunIt($this,
            array(
                'limit' => 100,
                'where' => array('active' => 1,
                    'hosthash <> ""', 'campaign_ID' => $Campaign->getID())
            ))->getTouchPoints();

		$v = $this->getPage('campaign-rankings','Campaign Rankings','campaign'/* $menu */);
        $v->init('preloaded_touchpoints', $preloaded_touchpoints);
        $v->init('rankingSites', $rankingSites);
        $v->init('countRankingSites', $rankingSitesCount);
        $v->init('user_id', $Campaign->user_id);
        $v->init('campaign_id', $Campaign->getID());

		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_rankings?ID='.$Campaign->id, 'Campaign Rankings');
        return $v;
    }

	function viewRanking(CampaignModel $Campaign, $site_id){
        $ranking_data_access = new RankingDataAccess;
        $params = array();
        list($params['interval'], $params['period']) = explode('-', Cookie::getVal('chartconfig', '30-DAY'));
        $chartDataGoogle = $ranking_data_access->getRankingDailyStatsChart($site_id, 'google', $params);
        $chartDataBing = $ranking_data_access->getRankingDailyStatsChart($site_id, 'bing', $params);
        $v = $this->getPage('campaign-ranking','Campaign Ranking','campaign'/* $menu */);

        $default_sort = DataTableResult::getDefaultColumn('RankingKeywords');

        $v->init('rankingKeywords', RankingDataAccess::FromController($this)->getAllKeywordData(array(
            'campaign_id' => $Campaign->getID(),
            'site_ID' => $site_id, 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort'],
            'period' => $params['period'],
			'interval' => $params['interval']
        )));

        $v->init('countRankingKeywords', RankingDataAccess::FromController($this)->getAllKeywordCount(array(
            'campaign_id' => $Campaign->getID(),
            'site_ID' => $site_id, 'orderCol' => $default_sort['Name'], 'orderDir' => $default_sort['Sort']
        )));

        $v->init('chartDataGoogle', json_encode($chartDataGoogle['data']));
        $v->init('chartDataBing', json_encode($chartDataBing['data']));
        $v->init('chartOptions', json_encode($chartDataGoogle['chartOptions']));

        $v->init('site_id', $site_id);
		$v->init('showDashboardRange', true);
		$v->init('exportURL', $this->ActionUrl("piwikAnalytics_getCSVData?chart=RankingKeywords&limit=5000&period={$params['period']}&interval={$params['interval']}&sortcol=0&sortdir=ASC&site_ID=" . $site_id .'&campaign_id='.$Campaign->getID()));
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_rankings?ID='.$Campaign->id, 'Campaign Rankings', $Campaign->id);
        $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_ranking?ID='.$Campaign->id, 'Keyword Rankings', $Campaign->id);
		return $v;
    }

    function ViewReports(CampaignModel $Campaign){
        $rda = ReportDataAccess::FromController($this);
        $v = $this->getPage('campaign-reports','Campaign Reports','campaign'/* $menu */);
        $v->init('reports',$rda->getAllReportData(array('campaign_id'=>$Campaign->id ,'user_id'=>$Campaign->user_id)));
        $v->init('totalreports',$rda->getAllReportCount(array('campaign_id'=>$Campaign->id,'user_id'=>$Campaign->user_id)));
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID='.$Campaign->id,'Campaign Reports',$Campaign->id);
		return $v;
    }

	function viewReportsCampaign(CampaignModel $Campaign){
		$v = $this->getPage('reports-campaign','Campaign Reports','campaign'/* $menu */);
		return $v;
	}

    function ViewReport($report_ID){/* @FER: BREADCRUMBS FIXED */

		$report = ReportDataAccess::FromController($this)
			->getAllReportData(array('id' => $report_ID, 'returnSingleObj' => 'ReportModel',
				'where' => 'id = :id AND user_id = :user_id'));

		$cid = $report->campaign_ID;
        $DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $v = $this->getPage('campaign-report','Campaigns Report','campaign'/* $menu */);
		$piwikStats = $this->getPiwikReportStats($report, $DA, $filter);

        $crumbsReport = $this->getCrumbsReports($report,"Campaign_report?&report_ID=%d","Campaign_report?report_ID=%d");
		if($report->campaign_ID){
			$Campaign = $this->getCampaignObject($report->campaign_ID,CampaignDataAccess::FromController($this));
			$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID='.$cid,'Campaign Reports',$cid);
            $this->getCrumbsRoot(true,true,null,null)->addCrumb($crumbsReport);
			$v->init('Campaign',$Campaign);
		}else{
            //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports','Reports','ReportsFull');
            $this->getCrumbsRoot(false,true,null,null)->addCrumb($crumbsReport);
            $v->init('menu','home'/* $menu */);
        }

        $v->init('piwikStats', $piwikStats);
        $v->init('T_Report', $report);

		return $v;
    }

    function viewReportsFull(){
        $rda = ReportDataAccess::FromController($this);
        $v = $this->getPage('campaign-reports','Reports','campaign'/* $menu */);
        $v->init('reports', $rda->getAllReportData(array('user_id'=>$this->user_ID, 'campaign_id' => 0)));
        $v->init('totalreports',$rda->getAllReportCount(array('user_id'=>$this->user_ID, 'campaign_id' => 0)));
        //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID=','Reports','ReportsFull');/* Reports is top level, why does it need breadcrumbs?! */
        $v->init('menu','home'/* $menu */);
        return $v;
    }

    function ajaxReportsWebsites($report_id, $search){
        $RDA = ReportDataAccess::FromController($this);
        $report = $RDA->getAllReportData(array('id' => $report_id, 'returnSingleObj' => 'ReportModel',
            'where' => 'id = :id AND user_id = :user_id'));
        $websites = $RDA->getWebsites($report, array("touchpoint_type" => $report->touchpoint_type, "search" => $search));
        return $this->getAjaxView($websites);
    }

    /**
     * @param $report
     * @param $DA
     * @param $filter
     * @return array
     */
    private function getPiwikReportStats(ReportModel $report, ReportDataAccess $DA, $filter)
    {
//        $websites = $DA->getWebsites($report, array('touchpoint_type' => $report->touchpoint_type));
        $SearchVisits = $DA->getSearchVisits($report, $filter);
        $SearchLeads = $DA->getSearchLeads($report, $filter);
        $SearchBounce = $DA->getSearchBounceVisits($report, $filter);
        $ReferralVisits = $DA->getReferralVisits($report, $filter);
        $ReferralLeads = $DA->getReferralLeads($report, $filter);
		$ReferralBounce = $DA->getReferralBounceVisits($report, $filter);
        $SocialVisits = $DA->getSocialVisits($report, $filter);
        $SocialLeads = $DA->getSocialLeads($report, $filter);
		$SocialBounce = $DA->getSocialBounceVisits($report, $filter);
		$chartStats = $DA->getChartPoints($report, $filter);
		$allBounce = $DA->getAllBounceVisits($report, $filter);
		$allVisits = $DA->getAllVisits($report, $filter);
        $allLeads = $DA->getAllLeads($report, $filter);

        $searchRate = number_format($SearchVisits == 0 ? 0 : (floatval($SearchLeads) / floatval($SearchVisits) * 100), 2);
        $referralRate = number_format($ReferralVisits == 0 ? 0 : (floatval($ReferralLeads) / floatval($ReferralVisits) * 100), 2);
        $socialRate = number_format($SocialVisits == 0 ? 0 : (floatval($SocialLeads) / floatval($SocialVisits) * 100), 2);
		$searchBounceRate = number_format($SearchVisits == 0 ? 0 : (floatval($SearchBounce) / floatval($SearchVisits) * 100), 2);
		$referralBounceRate = number_format($ReferralVisits == 0 ? 0 : (floatval($ReferralBounce) / floatval($ReferralVisits) * 100), 2);
		$socialBounceRate = number_format($SocialVisits == 0 ? 0 : (floatval($SocialBounce) / floatval($SocialVisits) * 100), 2);
        $directVisits = $allVisits -  (intval($SearchVisits) + intval($ReferralVisits) + intval($SocialVisits));
        $directLeads = $allLeads -  (intval($SearchLeads) + intval($ReferralLeads) + intval($SocialLeads));
        $directBounce = $allBounce -  (intval($SearchBounce) + intval($ReferralBounce) + intval($SocialBounce));
        $totalVisits = $allVisits;
        $totalLeads = $allLeads;
		$totalBounce = $allBounce;
        $totalRate = number_format($totalVisits == 0 ? 0 : (floatval($totalLeads) / floatval($totalVisits) * 100), 2);
		$totalBounceRate = number_format($totalVisits == 0 ? 0 : (floatval($totalBounce) / floatval($totalVisits) * 100), 2);
        $directRate = number_format($directVisits == 0 ? 0 : (floatval($directLeads) / floatval($directVisits) * 100), 2);
        $directBounceRate = number_format($directVisits == 0 ? 0 : (floatval($directBounce) / floatval($directVisits) * 100), 2);



        $chartOptions = $report->getChartOptions();

        return (object) array(
            'chartOptions' => $chartOptions,
            'chartStats' => $chartStats,
            'tableStats' => (object)    array(
                'SearchVisits' => $SearchVisits,
                'SearchLeads' => $SearchLeads,
                'ReferralVisits' => $ReferralVisits,
                'ReferralLeads' => $ReferralLeads,
                'SocialVisits' => $SocialVisits,
                'SocialLeads' => $SocialLeads,
                'searchRate' => "$searchRate%",
                'referralRate' => "$referralRate%",
                'socialRate' => "$socialRate%",
                'totalVisits' => $totalVisits,
                'totalLeads' => $totalLeads,
                'totalRate' => "$totalRate%",
                'directVisits' => $directVisits,
                'directLeads' => $directLeads,
                'directRate' => "$directRate%",
				'searchBounceRate' => "$searchBounceRate%",
				'referralBounceRate' => "$referralBounceRate%",
				'socialBounceRate' => "$socialBounceRate%",
				'totalBounceRate' => "$totalBounceRate%",
				'directBounceRate' => "$directBounceRate%",
            )
        );
    }

    /**
     * @param ReportModel $report
     * @param ReportDataAccess $DA
     * @param $filter
     * @return object
     */
    private function getOrganicReportStats(ReportModel $report, ReportDataAccess $DA, $filter, $focus = ''){
		$data = array();
		$data['chartOptions'] = $report->getChartOptions();

		$dataTableOptions = array(
			'limit' => '10',
			'offset' => '0'
		);

		switch($focus){
            case 'ORGANIC':
                $organicSearch = $DA->getOrganicSearch($report, $filter, false, '', $dataTableOptions);
				$totalSearchRows = $DA->FOUND_ROWS();
                $itemsOrderByVisits = array();
                foreach(array_slice($organicSearch, 0, 4) as $item){
                    $itemsOrderByVisits[$item->searchengine] = (object) array();
                }

                $itemsOrderByVisits['Others'] = (object) array();
                $totalOrganicSearch = $DA->getOrganicSearch($report, $filter, true);
                $chartStats = $DA->getChartPoints($report, $filter, $focus, $itemsOrderByVisits);
//                foreach($organicSearch as &$row){
//                    $row = $this->getView("rows/report-organic", false, false)->init('stats', $row)->toString();
//                }

                $data['tableStats'] = (object)array(
					'organicSearch' => $organicSearch,
					'totalSearchRows' => $totalSearchRows,
					'totalOrganicSearch' => (object)$totalOrganicSearch[0]
				);

				$data['chartStats'] = $chartStats;

				break;
            case 'REFERRAL':
                $ReferralSearch = $DA->getReferralSearch($report, $filter, false, '', $dataTableOptions);
				$totalSearchRows = $DA->FOUND_ROWS();
                $itemsOrderByVisits = array();
                foreach(array_slice($ReferralSearch, 0, 4) as $item){
                    $itemsOrderByVisits[$item->referrer] = (object) array();
                }
                $itemsOrderByVisits['Others'] = (object) array();
                $totalReferralSearch = $DA->getReferralSearch($report, $filter, true);

                $chartStats = $DA->getChartPoints($report, $filter, $focus, $itemsOrderByVisits);

//                foreach($ReferralSearch as &$row){
//                    $row = $this->getView("rows/report-referrals", false, false)->init('stats', $row)->toString();
//                }

				$data['tableStats'] =(object)array(
					'ReferralSearch' =>  $ReferralSearch,
					'totalSearchRows' => $totalSearchRows,
					'totalReferralSearch' =>  $totalReferralSearch[0]
				);

				$data['chartStats'] = $chartStats;
				break;
            case 'SOCIAL':
                $socialSearch = $DA->getSocialSearch($report, $filter);
				$totalSearchRows = $DA->FOUND_ROWS();
                $itemsOrderByVisits = array();
                foreach(array_slice($socialSearch, 0, 4) as $item){
                    $itemsOrderByVisits[$item->network] = (object) array();
                }
                $itemsOrderByVisits['Others'] = (object) array();
                $totalSocialSearch = $DA->getSocialSearch($report, $filter, true);

                $chartStats = $DA->getChartPoints($report, $filter, $focus, $itemsOrderByVisits);

//                foreach($socialSearch as &$row){
//                    $row = $this->getView("rows/report-social", false, false)->init('stats', $row)->toString();
//                }

				$data['tableStats'] = (object)array(
					'socialSearch' => $socialSearch,
					'totalSearchRows' => $totalSearchRows,
					'totalSocialSearch' => $totalSocialSearch[0]
				);

				$data['chartStats'] = $chartStats;
				break;
			default:
				throw new Exception("Report $focus not supported");
        }

        return (object)$data;
    }

    function ViewChartData($rID, $filter = ''){
		$DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $report = $DA->getAllReportData(array('id' => $rID, 'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id'));
        $piwikStats = $this->getPiwikReportStats($report, $DA, $filter);
        return $this->getAjaxView($piwikStats);
    }

    function ViewOrganicReport($report_ID, $filter = ''){/* @FER: BREADCRUMBS FIXED */
        $DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $report = $DA->getAllReportData(array('id' => $report_ID, 'returnSingleObj' => 'ReportModel',
                        'where' => 'id = :id AND user_id = :user_id'));
        $v = $this->getPage('report-organic','Campaigns Report','campaign'/* $menu */);
        $T_report = $this->getOrganicReportStats($report, $DA, $filter, 'ORGANIC');
//        $websites = $DA->getWebsites($report, array("touchpoint_type" => $report->touchpoint_type));
        $crumbsReport = $this->getCrumbsReports($report,"Campaign_report-organic?&report_ID=%d","Campaign_report?report_ID=%d");
        if($report->campaign_ID){
            $cid = $report->campaign_ID;
            $Campaign = $this->getCampaignObject($report->campaign_ID,CampaignDataAccess::FromController($this));
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID='.$cid,'Campaign Reports',$cid);
            $this->getCrumbsRoot(true,true,null,null)->addCrumb($crumbsReport);
			$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-organic?report_ID={$report->ID}",'Source: Organic',$cid);
			$v->init('campaign_id',$Campaign->getID());

		}else{
            //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports','Reports','ReportsFull');
            $this->getCrumbsRoot(false,true,null,null)->addCrumb($crumbsReport);
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-organic?report_ID={$report->ID}",'Source: Organic','ReportsFull');
			$v->init('menu','home'/* $menu */);
		}
        $v->init('T_Report', $report);
        $v->init('chartOptions', $T_report->chartOptions);
        $v->init('tableStats', $T_report->tableStats);
        $v->init('Stats', $T_report->chartStats);
        return $v;
    }

    function ViewOrganicReportAjax($rID, $filter ='', $focus = '')
    {
        $DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $report = $DA->getAllReportData(array('id' => $rID, 'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id'));
        $T_report = $this->getOrganicReportStats($report, $DA, $filter, $focus);
        return $this->getAjaxView($T_report);
    }

    function ViewReferralsReport($rID, $filter = ''){/* @FER: BREADCRUMBS FIXED */
        $DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $v = $this->getPage('report-referrals','Campaigns Report','campaign'/* $menu */);
        $report = $DA->getAllReportData(array('id' => $rID, 'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id'));
        $T_report = $this->getOrganicReportStats($report, $DA, $filter, 'REFERRAL');
//        $websites = $DA->getWebsites($report, array("touchpoint_type" => $report->touchpoint_type));
        $crumbsReport = $this->getCrumbsReports($report,"Campaign_report-referrals?&report_ID=%d","Campaign_report?report_ID=%d");
        if($report->campaign_ID){
            $cid = $report->campaign_ID;
            $Campaign = $this->getCampaignObject($report->campaign_ID,CampaignDataAccess::FromController($this));
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID='.$cid,'Campaign Reports',$cid);
            $this->getCrumbsRoot(true,true,null,null)->addCrumb($crumbsReport);
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-referrals?report_ID={$report->ID}",'Source: Referrals',$cid);
            $v->init('campaign_id',$Campaign->getID());

        }else{
            //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports','Reports','ReportsFull');
            $this->getCrumbsRoot(false,true,null,null)->addCrumb($crumbsReport);
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-referrals?report_ID={$report->ID}",'Source: Referrals','ReportsFull');
            $v->init('menu','home'/* $menu */);
        }
        $v->init('T_Report', $report);
        $v->init('chartOptions', $T_report->chartOptions);
        $v->init('tableStats', $T_report->tableStats);
        $v->init('Stats', $T_report->chartStats);
        return $v;
    }

    function ViewSocialReport($rID, $filter = ''){/* @FER: BREADCRUMBS FIXED */
        $DA = ReportDataAccess::FromController($this); //--Create ReportDataAccess--//
        $v = $this->getPage('report-social','Campaigns Report','campaign'/* $menu */);
        $report = $DA->getAllReportData(array('id' => $rID, 'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id'));
        $T_report = $this->getOrganicReportStats($report, $DA, $filter, 'SOCIAL');
//        $websites = $DA->getWebsites($report, array("touchpoint_type" => $report->touchpoint_type));
        $crumbsReport = $this->getCrumbsReports($report,"Campaign_report-social?&report_ID=%d","Campaign_report?report_ID=%d");
        if($report->campaign_ID){
            $cid = $report->campaign_ID;
            $Campaign = $this->getCampaignObject($report->campaign_ID,CampaignDataAccess::FromController($this));
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports?ID='.$cid,'Campaign Reports',$cid);
            $this->getCrumbsRoot(true,true,null,null)->addCrumb($crumbsReport);
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-social?report_ID={$report->ID}",'Source: Social',$cid);
            $v->init('campaign_id',$Campaign->getID());

        }else{
            //$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('Campaign_reports','Reports','ReportsFull');
            $this->getCrumbsRoot(false,true,null,null)->addCrumb($crumbsReport);
            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt("Campaign_report-social?report_ID={$report->ID}",'Source: Social','ReportsFull');
            $v->init('menu','home'/* $menu */);
        }
        $v->init('T_Report', $report);
        $v->init('chartOptions', $T_report->chartOptions);
        $v->init('tableStats', $T_report->tableStats);
        $v->init('Stats', $T_report->chartStats);
        return $v;
    }

    function viewContact(CampaignModel $Campaign, VarContainer $C){

	    if($this->getUser()->can('crm_only')) $v = $this->getPage('campaign-contact','Contact','campaign'/* $menu */, array('not-campaign','crm-nav'));
		else $v = $this->getPage('campaign-contact','Contact','campaign'/* $menu */);


        $PDA = ProspectDataAccess::FromController($this);
        $contact = $PDA->getContactById($_GET['pid']);
        $notes = $PDA->RetrieveNotes(array ('contact_id' => $contact->id, 'cid' => $Campaign->getID()));
		$iTotalNotes    =   count($notes);

		$Account = AccountsModel::Fetch('id = %d', $contact->account_id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_account?aid='.$contact->account_id.'&ID='.$Campaign->id, $Account->name, $Campaign->id);
		$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('campaign_contact?ID='.$Campaign->id, 'Contact Details', $Campaign->id);

		$TQ =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->Limit(0, 10)->calc_found_rows();
		$TQ->Where('(T.contact_id = '.$contact->id.' AND T.status != "Completed")');
        $Tasks  =   $TQ->Exec()->fetchAll();

		$TLQ =   $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->calc_found_rows();
		$TLQ->Where('(T.contact_id = '.$contact->id.')');
        $TimleineTasks  =   $TLQ->Exec()->fetchAll();
		$iTotalTasks    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$QT =   $this->queryProposals($Campaign, $C)->orderBy('T.last_updated DESC')->Limit(0, 10)->calc_found_rows();
		$QT->Where('(T.contact_id = '.$contact->id.')');

        //var_dump($QT);
        $Proposals  =   $QT->Exec()->fetchAll();
        $iTotalProposals    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$is_contact = true;

		$phonecheck = '1'.$contact->phone.'';

		$EQ =   $this->queryEmails($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$EQ->Where('(T.contact_id = '.$contact->id.')');
		$Emails  =   $EQ->Exec()->fetchAll();
		$iTotalEmails    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$TM =   $this->queryTexts($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$TM->Where('(T.contact_id = '.$contact->id.')');
        $Texts  =   $TM->Exec()->fetchAll();
        $iTotalTexts    =   $this->queryColumn('SELECT FOUND_ROWS()');

		$AT =   $this->queryTexts($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
		$AT->Where('(T.from_text = "'.$phonecheck.'")');
        $AssociatedTexts  =   $AT->Exec()->fetchAll();

		$IC =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
		$IC->orderBy('T.date DESC');
		$IC->Where('(T.caller_id = "'.$phonecheck.'" AND T.direction = "inbound")');
		$InboundCalls  =   $IC->Exec()->fetchAll();

		$OC =   $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
		$OC->orderBy('T.date DESC');
		$OC->Where('(T.destination = "'.$phonecheck.'" AND T.direction = "outbound")');
		$OutboundCalls  =   $OC->Exec()->fetchAll();
		$iTotalOutboundCalls    =   $this->queryColumn('SELECT FOUND_ROWS()');



		//$Elist =   $this->queryEmails($Campaign, $C)->Where('T.account_id = 0');
		$Elist =   $this->queryParentEmails($Campaign, $C);
		$EmailTemplates  =   $Elist->Exec()->fetchAll();

		if(isset($_POST['emailTemplate']))
        {
			$fields = array('first_name', 'last_name',
            'phone', 'address', 'city', 'state',
            'email', 'title' , 'name' , 'zip' , 'website', 'signa'
                    );

			$emailtemplate = EmailsModel::Fetch('id = %d', $_POST['email_template']);

			foreach($fields as $key)
				{
						if($key=='name' || $key=='address' || $key=='city' || $key=='state' || $key=='zip'|| $key=='website'){

							$emailtemplate->body = str_replace('[['.$key.']]', $Account->$key, $emailtemplate->body);

						} else if($key=='signa'){

							$eset = EmailSettingsModel::Fetch('cid = %d', $Campaign->id);
							$emailtemplate->body = str_replace('[['.$key.']]', $eset->$key, $emailtemplate->body);

						} else $emailtemplate->body = str_replace('[['.$key.']]', $contact->$key, $emailtemplate->body);
						$emailtemplate->subject = str_replace('[['.$key.']]', $contact->$key, $emailtemplate->subject);
						unset($key);
				}

			$emailtemplate->body = utf8_encode($emailtemplate->body);

			return $this->getAjaxView(array(
					'error' => false,
					'template' => $emailtemplate->body,
					'subject' => $emailtemplate->subject,
				));
			//return $this->getAjaxView($emailtemplate);
		}

		if(isset($_POST['sendText']))
        {

			$phone = PhoneModel::Fetch('cid = "'.$Campaign->id.'" AND type = "voip"');
			$voip = $phone->phone_num;
			$ST = new VoipDataAccess();
			$sent = $ST->sendTextMessage($_POST['sendText'], $_POST['TextMessage'], $voip);

            if($sent == 'Sent') {

				$parrr = array(
					'cid' => $Campaign->getID(),
					'account_id' => "$contact->account_id",
					'contact_id' => "$contact->id",
					'to_sent' => $_POST['sendText'],
					'message' => $_POST['TextMessage']
				);

				$ModelManager = new ModelManager();
				$result = $ModelManager->doSaveText($parrr, NULL, VoipDataAccess::FromController($this));
				if(!($result instanceof ValidationStack)) {

					$text = TextsModel::Fetch('id = %d', $result['object']->id);
					$result['texts'][] = $text->getTimeLineText($this->ActionUrl('Campaign_delete', array('Text_ID' => $text->id)), NULL);
				}

				return $this->getAjaxView($result);

			} else {

				return $this->getAjaxView(array(
					'error' => true,
					'message' => 'Error Sending Text'
				));
			}

        }

		if($contact->prospect_ID){
		$Prospect   =   $PDA->getFullProspectInfo($contact->prospect_ID);
		$ProspectData   =   $Prospect->getProspectData($contact->prospect_ID);
		$all_activity = $PDA->getLastActivityProspect($Prospect, TRUE);
		$first_touch = $Prospect->getFirstTouch();
		}

		$v->init('first_touch', $first_touch);
		$v->init('account', $Account);
		$v->init('contact', $contact);
		$v->init('Tasks', $Tasks);
        $v->init('total_tasks_match', $iTotalTasks);
		$v->init('Texts', $Texts);
		$v->init('InboundCalls', $InboundCalls);
		$v->init('OutboundCalls', $OutboundCalls);
		$v->init('total_phone_match', $iTotalOutboundCalls);
		$v->init('AssociatedTexts', $AssociatedTexts);
        $v->init('total_texts_match', $iTotalTexts);
		$v->init('Proposals', $Proposals);
		$v->init('Prospect', $Prospect);
        $v->init('total_proposals_match', $iTotalProposals);
		$v->init('total_notes_match', $iTotalNotes);
		$v->init('notes', $notes);
		$v->init('all_activity', $all_activity);
		$v->init('is_contact', $is_contact);
		$v->init('TimleineTasks', $TimleineTasks);
		$v->init('Emails', $Emails);
		$v->init('clientstatus', $clientstatus);
		$v->init('total_emails_match', $iTotalEmails);
		$v->init('EmailTemplates', $EmailTemplates);
		return $v;
    }

	function prepareView(\ThemeView &$view) {
		parent::prepareView($view);
	}

    static function getTouchpointCtrlAction(TouchpointInfoModel $info, $action = 'dashboard', $etc = '')
    {
        return sprintf('%s_%s?ID=%d%s', ucfirst(strtolower($info->touchpoint_type)), $action, $info->blog_ID ? $info->blog_ID : $info->hub_ID, $etc);
    }

	function queryCampaignForms(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('LeadFormModel', 'T', 'T.user_id = %d and T.cid = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }

    function ViewProspect($pid, CampaignModel $Campaign, $action, VarContainer $C)
    {
        $pda = ProspectDataAccess::FromController($this);
        /** @var ProspectModel $Prospect */
        $Prospect = $pda->getFullProspectInfo($pid);
        //$site->queryProspects('ID = %d', $pid)->Exec()->fetch();
        //$Info   =   $Prospect->getData();
	    /* added by --Jon Aguilar-- ^ */
        $ProspectData = $Prospect->getProspectData($pid);
        $first_touch = $Prospect->getFirstTouch();
        $last_touch = $Prospect->getLastTouch();
        $last_activity = $pda->getLastActivityProspect($Prospect);
        $all_activity = $pda->getLastActivityProspect($Prospect, TRUE);

        $notes = $pda->RetrieveLeadNotes(array('lead_id' => $Prospect->id, 'cid' => $Campaign->getID()));
        $iTotalNotes = count($notes);

        if ($this->getUser()->can('crm_only')) {
            $page = $this->getPage('website-prospect', 'Lead Details', 'campaign'/* $menu */, array('not-campaign', 'crm-nav'));
        } else {
            $page = $this->getPage('website-prospect', 'Lead Details', 'campaign'/* $menu */);
        }

        $this->getCrumbsRoot(true, true, null, null)->addCrumbTxt('campaign_prospects?ID=' . $Campaign->getID(), 'Leads')
            ->addCrumbTxt('Campaign_prospect?pid=' . $pid . '&ID=' . $Campaign->getID(), 'Lead Details');

        $PQ = $this->queryAccounts($Campaign, $C)->orderBy('T.name DESC')->calc_found_rows();
        $Accounts = $PQ->Exec()->fetchAll();

        /* Transfer Leads: Query Campaigns */
        $Campaigns = $this->queryCampaigns()->Exec()->fetchAll();

        /* Transfer Leads: Query Agents by CRM Role */
        $CRMRoleAgents = $this->queryUsers($Campaign, $C, 'active')->calc_found_rows()->Exec()->fetchAll();

        // get last query for testing.
        $db = $this->db;
        $ls = $db::$last_statement;
        // end get last query for testing.

        // Fake Data
        /*$CRMRoleAgents = array(
            0 => (object) ['id' => 1, 'name' => 'Jon'],
            1 => (object) ['id' => 3, 'name' => 'Hunter']
        );*/
        /* End Transfer Leads: Query Agents by CRM Role */

        $LQ = $this->queryCampaignForms($Campaign, $C)->orderBy('T.name DESC')->calc_found_rows();
        $Forms = $LQ->Exec()->fetchAll();

        $TM = $this->queryTexts($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
        $TM->Where('(T.lead_id = ' . $Prospect->id . ')');
        $Texts = $TM->Exec()->fetchAll();
        $iTotalTexts = $this->queryColumn('SELECT FOUND_ROWS()');

        $phonecheck = '1' . $Prospect->phone . '';
        $AT = $this->queryTexts($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
        $AT->Where('(T.from_text = "' . $phonecheck . '")');
        $AssociatedTexts = $AT->Exec()->fetchAll();

        $IC = $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
        $IC->orderBy('T.date DESC');
        $IC->Where('(T.caller_id = "' . $phonecheck . '" AND T.direction = "inbound")');
        $InboundCalls = $IC->Exec()->fetchAll();

        $OC = $this->queryPhoneRecords($Campaign, $C)->calc_found_rows();
        $OC->orderBy('T.date DESC');
        $OC->Where('(T.destination = "' . $phonecheck . '" AND T.direction = "outbound")');
        $OutboundCalls = $OC->Exec()->fetchAll();
        $iTotalOutboundCalls = $this->queryColumn('SELECT FOUND_ROWS()');

        $TQ = $this->queryTasks($Campaign, $C)->orderBy('T.due_date DESC')->Limit(0, 10)->calc_found_rows();
        $TQ->Where('(T.lead_id = ' . $Prospect->id . ' AND T.status != "Completed")');
        $Tasks = $TQ->Exec()->fetchAll();

        $EQ = $this->queryEmails($Campaign, $C)->orderBy('T.created DESC')->calc_found_rows();
        $EQ->Where('(T.lead_id = ' . $Prospect->id . ')');
        $Emails = $EQ->Exec()->fetchAll();
        $iTotalEmails = $this->queryColumn('SELECT FOUND_ROWS()');

        $Elist = $this->queryParentEmails($Campaign, $C);
        $EmailTemplates = $Elist->Exec()->fetchAll();

        if (isset($_POST['emailTemplate'])) {
            $fields = array('first_name', 'name', 'signa'
            );

            $emailtemplate = EmailsModel::Fetch('id = %d', $_POST['email_template']);

            foreach ($fields as $key) {
                if ($key == 'first_name' || $key == 'name') {

                    if ($key == 'first_name') $field = 'name';
                    if ($key == 'name') $field = 'company';

                    $emailtemplate->body = str_replace('[[' . $key . ']]', $Prospect->$field, $emailtemplate->body);

                } else if ($key == 'signa') {

                    $eset = EmailSettingsModel::Fetch('cid = %d', $Campaign->id);
                    $emailtemplate->body = str_replace('[[' . $key . ']]', $eset->$key, $emailtemplate->body);

                } else $emailtemplate->body = str_replace('[[' . $key . ']]', $Prospect->$key, $emailtemplate->body);
                $emailtemplate->subject = str_replace('[[' . $key . ']]', $Prospect->$key, $emailtemplate->subject);
                unset($key);
            }

            $emailtemplate->body = utf8_encode($emailtemplate->body);

            return $this->getAjaxView(array(
                'error' => false,
                'template' => $emailtemplate->body,
                'subject' => $emailtemplate->subject,
            ));
            //return $this->getAjaxView($emailtemplate);
        }

        if (isset($_POST['sendText'])) {

            $phone = PhoneModel::Fetch('cid = "' . $Campaign->id . '" AND type = "voip"');
            $voip = $phone->phone_num;
            $ST = new VoipDataAccess();
            $sent = $ST->sendTextMessage($_POST['sendText'], $_POST['TextMessage'], $voip);

            if ($sent == 'Sent') {

                $parrr = array(
                    'cid' => $Campaign->getID(),
                    'lead_id' => "$Prospect->id",
                    'to_sent' => $_POST['sendText'],
                    'message' => $_POST['TextMessage']
                );

                $ModelManager = new ModelManager();
                $result = $ModelManager->doSaveText($parrr, NULL, VoipDataAccess::FromController($this));
                if (!($result instanceof ValidationStack)) {

                    $text = TextsModel::Fetch('id = %d', $result['object']->id);
                    $result['texts'][] = $text->getTimeLineTextLead($this->ActionUrl('Campaign_delete', array('Text_ID' => $text->id)), NULL);
                }

                return $this->getAjaxView($result);

            } else {

                return $this->getAjaxView(array(
                    'error' => true,
                    'message' => 'Error Sending Text'
                ));
            }

        }

        // Transfer lead to another agent.
        if ($action == 'transfer-lead-to-agent-crm') {
            if (!empty($_POST['agent_user_id']) && !empty($_POST['campaign_id'])) {
                $data = array(
                    'user_id' => $_POST['agent_user_id'],
                    'cid' => $_POST['campaign_id'],
                );

                $modelManager = new ModelManager();
                $result = $modelManager->doSaveContact($data, $Prospect->contact_ID, ProspectDataAccess::FromController($this), null, $data['user_id']);

                if ($result instanceof IOResultInfo) {
                    $field = $result->getObject()->Refresh();
                    $contact_id = $field->id;
                }

                // Model Manager will return
                if (!$result instanceof ValidationStack) {
                    return $this->getAjaxView(array(
                        'error' => false,
                        'message' => 'Contact transferred successfully!'
                    ));
                } else {
                    return $this->getAjaxView(array(
                        'error' => true,
                        'message' => 'Error transferring contact. Please try again.'
                    ));
                }

            }
        }


        if ($action == 'prospect-to-contact') {
            if ($_POST['account_name'] != '') {

                $MM = new ModelManager();
                $args = array('name' => $_POST['account_name'], 'cid' => $Campaign->getID());
                $result = $MM->doSaveAccount($args, NULL, AccountsDataAccess::FromController($this));
                if ($result instanceof IOResultInfo) {

                    $field = $result->getObject()->Refresh();
                    $account_id = $field->id;

                }

            } else {
                $account_id = $_POST['account_id'];
            }

            if ($Prospect->phone) {
                $pcheck = preg_replace('/[^0-9]/', '', $Prospect->phone);
                $Contact = ContactModel::Fetch('cid = "' . $Prospect->cid . '" AND phone = "' . $pcheck . '"');

                if ($Contact) {
                    $phoneMatch = true;
                }

            }


            if ($phoneMatch) {
                $error = true;
            } else {
                $pda->addToContacts($pid, $account_id);
            }

            //return $this->ActionRedirect('Campaign_prospect?ID=' . $Campaign->getID() . '&pid=' . (int)$pid);
            if ($error != true) {
                return $this->getAjaxView(array(
                    'error' => false,
                    'message' => 'Contact Added Successfully'
                ));
            } else {

                return $this->getAjaxView(array(
                    'error' => true,
                    'message' => 'Error Adding Contact'
                ));
            }

        }

        $Contact = ContactModel::Fetch('prospect_ID = %d', $pid);
        $Account = AccountsModel::Fetch('id = %d', $Contact->account_id);

        $page->init('Accounts', $Accounts);

        $page->init('CRMRoleAgents', $CRMRoleAgents); // Added by Hunter: 5/16/2019
        $page->init('Campaigns', $Campaigns);

        $page->init('Forms', $Forms);
        $page->init('Texts', $Texts);
        $page->init('AssociatedTexts', $AssociatedTexts);
        $page->init('total_texts_match', $iTotalTexts);
        $page->init('InboundCalls', $InboundCalls);
        $page->init('OutboundCalls', $OutboundCalls);
        $page->init('total_phone_match', $iTotalOutboundCalls);
        $page->init('Account', $Account);
        $page->init('Contact', $Contact);
        $page->init('Prospect', $Prospect);
        $page->init('ProspectData', $ProspectData);
        $page->init('first_touch', $first_touch);
        $page->init('last_touch', $last_touch);
        $page->init('last_activity', $last_activity);
        $page->init('all_activity', $all_activity);
        $page->init('Emails', $Emails);
        $page->init('total_emails_match', $iTotalEmails);
        $page->init('EmailTemplates', $EmailTemplates);
        $page->init('notes', $notes);
        $page->init('Tasks', $Tasks);
        $page->init('total_tasks_match', $iTotalTasks);

        return $page;
    }


    function RedirectToProspectPage($pid, CampaignModel $Camp)
    {
        $info   =   $this->query('select touchpoint_type, blog_ID, i.hub_ID from touchpoints_info i inner join prospects p on i.ID = p.touchpoint_ID and i.campaign_ID = p.cid
	            WHERE p.id = ' . (int)$pid . ' AND p.cid = ' . $Camp->getID())->fetchObject('TouchpointInfoModel');

        if(!$info)
        {
            return $this->ActionRedirect('');
        }
        return $this->ActionRedirect('Campaign_prospect?ID=' . $Camp->getID() . '&p=' . (int)$pid);
    }

    function doAction(CampaignModel $Campaign, $action){

        if($deny = $this->Protect('view_campaigns', 'Auth', $Campaign->getID()))
                        return $deny;
        switch($action)
        {
			case 'ajaxDataTableForms':
				return $this->ajaxDataTableForms($Campaign, VarContainer::getContainer($_POST));
			case 'sendBroadcast':
				return $this->sendBraodcast($Campaign, VarContainer::getContainer($_POST));
			case 'importLeads':
				return $this->importLeads($Campaign, VarContainer::getContainer($_POST + $_FILES));
            case 'ajaxCollabs':
                return $this->ajaxCollabs($Campaign, VarContainer::getContainer($_POST));
            case 'ajaxTouchpoints':
                return $this->ajaxTouchpoints($Campaign, VarContainer::getContainer($_POST));
            case 'send-test-responder';
                return $this->ajaxSendTestMail($Campaign, VarContainer::getContainer($_POST));
            case 'LeadConvert':
                return $this->ajaxLeadConvert($Campaign);
            case 'transfer-lead-to-agent-crm':
                return $this->ViewProspect((int)$_GET['pid'], $Campaign, $action, new VarContainer($_GET));
            case 'prospect-to-contact':
            case 'prospect':
                return $this->ViewProspect((int)$_GET['pid'], $Campaign, $action, new VarContainer($_GET));

            case 'ajaxBroadcasters':
                return $this->ajaxBroadcasters($Campaign, VarContainer::getContainer($_POST));
//            case 'prospectf':                return $this->RedirectToProspectPage($_GET['pid'], $Campaign);                break;

            case 'ajaxRankingKeywords':
                $C = VarContainer::getContainer($_POST);
                $C->load(array('site_ID' => $_GET['site_ID']));
                return $this->ajaxRankingKeywords($Campaign, $C);
            case 'approvespam':
                return $this->doAjaxApproveSpamMessage($_POST['MID']);
            case 'deletespam':
                return $this->doAjaxDeleteSpamMessage($_POST['MID']);
            case 'loadspam-msg':
                return $this->viewAjaxSpamMessage($_POST['MID']);
            case 'ajaxRankingSites':
                $C = VarContainer::getContainer($_POST);
                $C->load(array('user_id' => $Campaign->user_id));
                return $this->ajaxRankingSites($Campaign, $C);
            case 'ajaxEmailmarketing':
                $C = VarContainer::getContainer($_POST);
                $C->load(array('table' => 'EmailMarketing'));
                return $this->ajaxEmailmarketing($Campaign, $C);
            case 'ajaxNetworks':
                return $this->ajaxNetworks($Campaign, VarContainer::getContainer($_POST) );
            case 'ajaxReports':
                return $this->ajaxReports($Campaign, VarContainer::getContainer($_POST) );
            case 'ajaxDashboardProspects':
                $C = new VarContainer($_POST);
                $C->load(array('table' => 'ProspectsDashboardCampaign'));
                return $this->ajaxProspects($C, $Campaign);
            case 'ajaxCampaignProspects':
                $C = new VarContainer($_POST);
                if(isset($_GET['alltime'])){$C->alltime = true;}
                $C->load(array('table' => 'ProspectsCampaign'));
                return $this->ajaxProspects($C, $Campaign);
            case 'ajaxcontacts':
                return $this->ajaxContacts($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxaccountcontacts':
                return $this->ajaxAccountContacts($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxaccounts':
                return $this->ajaxAccounts($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxproposals':
                return $this->ajaxProposals($Campaign, VarContainer::getContainer($_POST));
			case 'ajaxtasks':
                return $this->ajaxTasks($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxemails':
                return $this->ajaxEmails($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxphones':
                return $this->ajaxPhones($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxproducts':
                return $this->ajaxProducts($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxsocialaccounts':
                return $this->ajaxSocialAccounts($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxPhone':
					return $this->ajaxPhone($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxPhoneMainDashboard':
					return $this->ajaxPhoneMainDashboard(VarContainer::getContainer($_POST));

            case 'ajaxnetworklistings':
                return $this->ajaxNetworkListings($Campaign, VarContainer::getContainer($_POST));

			case 'ajaxsocialposts':
                return $this->ajaxSocialPosts($Campaign, VarContainer::getContainer($_POST));

            case 'ajaxGetSites':
                $this->cc_user_id = $this->getUser()->getID();
                $this->cc_is_reports_only = $this->getUser()->can('reports_only');
                return $this->getSites(@$_GET['pointtype'], new VarContainer($_POST), $Campaign);

            case 'ajaxTopPerformingSites':
                //$this->cc_user_id = $this->getUser()->getID();
                //$this->cc_is_reports_only = $this->getUser()->can('reports_only');
                return $this->getTopPerformingSites(@$_GET['pointtype'], new VarContainer($_POST), $Campaign);

            case 'ajaxGetVisits':
                return $this->ajaxGetVisits($Campaign);

			case 'social-scheduler':
			case 'social-accounts':
				return $this->viewSocialAccounts($Campaign, new VarContainer($_GET));

			case 'social-account':
				return $this->viewSocialAccount($Campaign, new VarContainer($_GET));

			case 'social-posts':
				return $this->viewSocialPosts($Campaign, new VarContainer($_GET));

			case 'social-post-details':
				return $this->viewSocialPostDetails($Campaign, new VarContainer($_GET));

            case 'network-listings':
                return $this->viewDirectoryListings($Campaign, new VarContainer($_GET));
            case 'network-listing':
                return $this->viewDirectoryListing($Campaign, new VarContainer($_GET));
            case 'network-listing-seo':
                return $this->viewDirectoryListingSEO($Campaign, new VarContainer($_GET));
            case 'network-listing-slides':
                return $this->viewDirectoryListingSlides($Campaign, new VarContainer($_GET));
            case 'network-listing-social':
                return $this->viewDirectoryListingSocial($Campaign, new VarContainer($_GET));

            case 'spam':
                return $this->viewCampaignSpam($Campaign);

			case 'rankings':
                return $this->viewRankings($Campaign);

			case 'ranking':
                return $this->viewRanking($Campaign, $_GET['site_ID']);

			case 'reports-campaign':
				// @amado this is for the REPORTS_campaign - which was originally linked to a Reports controller, but doesnt't exist.
				return $this->viewReportsCampaign($Campaign);

			case 'report-campaign':
				// @amado this is for the REPORTS_campaign
				return $this->viewReportsCampaignDetails($Campaign);

            case 'reports':
                return $this->ViewReports($Campaign);

            case 'report':
                return $this->ViewReport($_GET['report_ID']);

//            case 'ajaxchartreport':
//                $r = $_GET['report_ID'];
//                $f = $_POST['filter'];
//                return $this->ViewChartData($Campaign,$r, $f);
//
//            case 'ajaxchartreportorganic':
//                $r = $_GET['report_ID'];
//                $f = $_POST['filter'];
//                return $this->ViewOrganicReportAjax($Campaign,$r, $f, 'ORGANIC');
//
//            case 'ajaxchartreportreferrals':
//                $r = $_GET['report_ID'];
//                $f = $_POST['filter'];
//                return $this->ViewOrganicReportAjax($Campaign,$r, $f, 'REFERRAL');
//
//            case 'ajaxchartreportsocial':
//                $r = $_GET['report_ID'];
//                $f = $_POST['filter'];
//                return $this->ViewOrganicReportAjax($Campaign,$r, $f, 'SOCIAL');

				// @todo rewrite save url for this shit
            case 'saveform':
                return $this->SaveForm($Campaign, $_POST, 'form');

            case 'forms':
                return $this->viewForms($Campaign);

            case 'form':
                if($form    =   $this->getLeadForm($Campaign, $_GET['form']))
                    return $this->viewForm($Campaign, $form);
                else throw new Exception('form does not exist');

            case 'emailmarketing':
                return $this->viewEmailMarketing($Campaign);

            case 'responder-preview':
                return $this->viewResponderEmailPreview($Campaign, (int)$_GET['R']);

            case 'emailer-preview':
                return $this->viewEmailerPreview($Campaign, (int)$_GET['R']);

            case 'responder':
            case 'responder-settings':
                return $this->viewResponderSettings($Campaign);

            case 'emailbroadcasts':
                return $this->viewEmailBroadcast($Campaign);

            case 'emailer-settings':
                return $this->viewEmailerBroadcast($Campaign, (int)$_GET['b']);

            case 'prospects':
				return $this->viewProspects($Campaign, new VarContainer($_GET));

            case 'networks':
                return $this->viewNetworks($Campaign);

             case 'network':
                return $this->viewNetwork($Campaign);

            case 'dirarticle-seoRegions':
            case 'dirarticle-photoGallery':
            case 'dirarticle-socialSpots':
            case 'dirarticle-seo':
            case 'network-listingInfo':
                return $this->viewNetworkListingInfo($Campaign, @$_GET['article'], @$_GET['view']);

            case 'network-press':
                return $this->viewNetworkPressRelease($Campaign, @$_GET['press']);

            case 'network-article':
                return $this->viewNetworkArticle($Campaign, @$_GET['article']);

            case 'phone':
                return $this->viewPhone($Campaign, new VarContainer($_GET));

            case 'phone-reports':
                return $this->viewPhoneRecords($Campaign, new VarContainer($_GET));

            case 'contacts':
                return $this->viewContacts($Campaign, new VarContainer($_GET));

            case 'contact':
                return $this->viewContact($Campaign, new VarContainer($_GET));

			case 'proposals':
                return $this->viewProposals($Campaign, new VarContainer($_GET));

			case 'proposal':
                return $this->viewProposal($Campaign, new VarContainer($_GET));

			case 'tasks':
                return $this->viewTasks($Campaign, new VarContainer($_GET));

			case 'accounts':
                return $this->viewAccounts($Campaign, new VarContainer($_GET));

			case 'phones':
                return $this->viewPhones($Campaign, new VarContainer($_GET));

			case 'phoneline':
                return $this->viewPhoneLine($Campaign, new VarContainer($_GET));
			case 'emailsettings':
                return $this->viewEmailSettings($Campaign, new VarContainer($_GET));

			case 'account':
                return $this->viewAccount($Campaign, new VarContainer($_GET));

			case 'emails':
                return $this->viewEmails($Campaign, new VarContainer($_GET));

			case 'email':
                return $this->viewEmail($Campaign, new VarContainer($_GET));

            case 'settings':
                return $this->viewSettings($Campaign);

            case 'edit-notification':
                return $this->viewNotificationEdit($Campaign, $_GET['Collab']);

            case 'crm':
                return $this->viewCrmIntegration();

            case 'domains':
                return $this->viewDomains();

            case 'dashboard':   // campaign dashboard
                return $this->viewCampaignDashboard($Campaign, @$_GET['pointtype']);
                break;
            case 'ajaxForms':
                return $this->ajaxForms($Campaign, VarContainer::getContainer($_POST));
				break;
        }
    }

    static function getCampaignObject($campaign_ID, CampaignDataAccess $cda){	// DashboardBaseController $q){
        $obj	=	$cda->getCampaign($campaign_ID);
		return $obj;
    }

    function ExecuteAction($action = NULL) {
        // @todo locate the campaign before showing a view

        if(!$this->getUser()->hasModuleAccess('Campaigns'))
        {
            // any action takes user home.
            if($this->getUser()->can('view_HOME'))


            switch($action)
            {
                case 'resources':
                    return $this->viewResources();
                    break;
                case 'crm':
                    return $this->viewCRM();
                    break;
                case 'landing':
                case 'home':
                    return $this->viewHome();

            }

            return $this->SecurityError('Auth_login');
        }

        if (isset($_GET['ID'])) {
            $campaign_ID = (int) $_GET['ID'];
			$cda	=	new CampaignDataAccess($this->getQube());
			$cda->setActionUser($this->getUser());
            $Campaign = $cda->getCampaign($campaign_ID);

            // @todo log error
            if (!$Campaign)
                die('Error');

            $this->query('SET @dashboard_campaignid = ' . $Campaign->id);
            $P = $this->doAction($Campaign, $action);

            if ($P) {
                $P->init('campaign_id', $Campaign->id);
                $P->init('Campaign', $Campaign);
				$P->init('touchpoints', $this->getTouchPointsStats($Campaign->id));
                return $P;
            } else {
                die('unknown action');
            }
        } else {
            // process actions that do not require a campaign
            switch ($action) {
				case 'exportLeads':
					return $this->exportLeads(VarContainer::getContainer($_GET));
				case 'downloadCVSFormExample':
					return $this->downloadCVSFormExample(VarContainer::getContainer($_GET));
				case 'ajaxSocialMedia':
					return $this->ajaxSocialMedia($_GET['R_ID'], $_POST);
				case 'ajaxReferrals':
					return $this->ajaxReferrals($_GET['R_ID'], $_POST);
				case 'ajaxOrganic':
					return $this->ajaxOrganic($_GET['R_ID'], $_POST);
				case 'ajaxRankingChart':
					return $this->ajaxRankingChart($_GET['site_ID'], $_POST);
                case 'ajaxReportsWebsites':
                    return $this->ajaxReportsWebsites($_GET['report_ID'], $_POST['search']);
                case 'resources':
                    return $this->viewResources();
                case 'crm':
                    return $this->viewCRM();
                case 'landing':
                    return $this->viewHome();
				case 'salescrm':
                    return $this->viewSalesCRM();
				case 'salesfeed':
                    return $this->viewSalesFeed();
				case 'inboxcalls':
                    return $this->viewInboxCalls();
                case 'inboxtexts':
                    return $this->viewInboxTexts();
                case 'inboxvoicemails':
                    return $this->viewInboxVoicemails();
				case 'ajaxinboxcalls':
                return $this->ajaxInboxCalls(VarContainer::getContainer($_POST));
                case 'ajaxinboxtexts':
                return $this->ajaxInboxTexts(VarContainer::getContainer($_POST));
                case 'ajaxinboxvoicemails':
                return $this->ajaxInboxVoicemails(VarContainer::getContainer($_POST));
				case 'ajaxcampaigns':
                return $this->ajaxCampaigns(VarContainer::getContainer($_POST));

                case 'ajaxPhoneRecording':
                    return $this->ajaxPhoneRecording(VarContainer::getContainer($_POST));

				case 'ajaxPhoneMainDashboard':
					return $this->ajaxPhoneMainDashboard(VarContainer::getContainer($_POST));

				case 'ajaxchartreport':
					$r = $_GET['report_ID'];
					$f = $_POST['filter'];
					return $this->ViewChartData($r, $f);

				case 'ajaxchartreportorganic':
					$r = $_GET['report_ID'];
					$f = $_POST['filter'];
					return $this->ViewOrganicReportAjax($r, $f, 'ORGANIC');

				case 'ajaxchartreportreferrals':
					$r = $_GET['report_ID'];
					$f = $_POST['filter'];
					return $this->ViewOrganicReportAjax($r, $f, 'REFERRAL');

				case 'ajaxchartreportsocial':
					$r = $_GET['report_ID'];
					$f = $_POST['filter'];
					return $this->ViewOrganicReportAjax($r, $f, 'SOCIAL');
                case 'report-organic':
                    return $this->ViewOrganicReport( $_GET['report_ID'], $_POST['filter']);
                case 'report-referrals':
                    return $this->ViewReferralsReport( $_GET['report_ID'], $_POST['filter']);
                case 'report-social':
                    return $this->ViewSocialReport( $_GET['report_ID'], $_POST['filter']);

                case 'report':
                    return $this->ViewReport($_GET['report_ID']);
				case 'reports':
					return $this->viewReportsFull();
                case 'savefield':
                    $_POST['field']['ID']   =   (int)$_GET['FID'];
                case 'save':
                	//// This function returns the message
                    return $this->ajaxCreateNew($_POST);

                case 'trash':
                    return $this->viewTrash();

//                case 'savetp':
//                    return $this->SaveWebsite((array)@$_POST['website']);

                case 'graphdata':
                    //return $this->ajaxGraphData(@$_GET['CID'], @$_GET['ID'], $_POST);

                case 'deletesite':
                    return $this->ajaxDeleteSite(new VarContainer($_GET));

                case 'delete':
                    return $this->ajaxDelete(new VarContainer($_GET));

				case 'complete-task':
                    return $this->ajaxCompleteTask(new VarContainer($_GET));

                case 'ajaxGetSites' :

                    return $this->getSites($tp_type, new VarContainer($_POST), $Camp);

                case 'ajaxProspects':

                    return $this->ajaxProspects(VarContainer::getContainer($_POST));

                case 'responderstatus':

                        return $this->ajaxResponderStatus($_GET['autoresponder_ID'],$_GET['active'],$_GET['name']);

                case 'ajaxReports':

                    return $this->ajaxReports(NULL, VarContainer::getContainer($_POST));
            }
        }

//        if($this->getUser()->can('view_HOME'))
//            return $this->viewHome();

        // main dashboard.. displaying all the campaigns
		if($this->getUser()->can('crm_only')) return $this->viewSalesCRM();
        else return $this->viewMainDashboard();
    }

    function getWebsiteStatsRow(WebsiteModel $W){
        $alldata    =   $this->getWebsitesStats($W->cid);

        $data   =   array();

        if(getflag('disable_cache') || $alldata === false   ||  !isset($alldata[$W->id]))
        {
            $atycs  =   new DashboardAnalytics;
            $atycs->prepareHubTrackingIds('id = ' . (int)$W->id, 'hub');

//            $data   =   $atycs->getChartData(2, 'WEEK', 'nb_visits', 'ValueArray');

            $data   =   $atycs->getChartData(new PointsSetBuilder(new VarContainer(array('interval' => 2,
                    'period'    => 'WEEk'))), 'nb_visits', 'ValueArray');
//            $row    =   new WebsiteStatsRow();
//            $row->render($data);

        }

        return $this->returnAjaxData('data', $data);

        if($alldata === false)
            $alldata    =   array();

        $alldata[$W->id]    =   $data;
    }

    function ajaxGetVisits(CampaignModel $Camp) {

        $alldata = $this->getCampsStats();

        if ($alldata === false || !isset($alldata[$Camp->id])) {

            $trackingids = $this->db->query('select group_concat(h.tracking_id) as trackingids
                from hub h where h.user_id = dashboard_userid() AND h.cid = ' . $Camp->id . ' AND tracking_id != 0 group by h.cid')
                    ->fetchColumn();

            $data = array('prevw' => 0,
                'curw' => 0, 'diff' => 'N/a');

            $db_data    =   array('numleads'    =>  0, 'nb_to`uchpoints'    =>  0);
            if ($trackingids != '') {

                $db =   Qube::Start();

                $chartparams    =   array('interval'    =>  7, 'period' => 'DAY');
                $ComparisonChart    =   new TableCompareChartsData('temp', NULL, false, array('controller' => $this));
                $ComparisonChart->compareLast2($this->query('SELECT '), DBWhereExpression::Create('s.idsite = tr.tracking_id'),
                            new PointsSetBuilder(VarContainer::getContainer($chartparams)));

                    $patycs =   new PiwikAnalyticsController();

                    $raw    =   $patycs->getVisits($trackingids);


                foreach ($raw as $siteid => $row) {
                    $data['prevw'] += $row[key($row)];
                    next($row);
                    $data['curw'] += $row[key($row)];
                }
            }
            $rowrender = new CampaignStatsRow();
            $rowrender->render($data + $db_data);
            $rowrender->campaign_id =   $Camp->id;

            if ($alldata === false)
                $alldata = array();

            $alldata[$Camp->id] = $rowrender->getRenderedValues();

            $this->cacheSave($alldata);
        }

        $returnvalues = array($alldata[$Camp->id]);
        $ajax = array('values' => $returnvalues, 'campaign_id' => $Camp->id);
        $view = $this->returnAjaxData('data', $ajax);

        return $view;
    }

    function ajaxDeleteSite(VarContainer $params){
		$message    =   'Error Ocurred';

        $stm = $this->q->GetPDO()->prepare("SELECT cid FROM hub where id = :id");
		$stm->execute(array("id" => $params->getValue("site_ID")));
		$campaign_id = $stm->fetchColumn();
		$TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
		$result = $TrashMgr->TrashByParam($params, $message, true);
        $touchpoints = $this->getTouchPointsStats($campaign_id);
		return $this->getAjaxView(array('error' => $result ? 0 : 1, 'message' => $message, 'touchpoints' => $touchpoints));
	}

	function ajaxResponderStatus($id,$active,$name){
		$MM = new ModelManager();

		$args = array('name' => $name,'active' => $active);

		$result = $MM->doSaveResponder($args,$id,ResponderDataAccess::FromController($this));

		return $this->getAjaxView($result);
	}

    function ajaxRankingSites(CampaignModel $campaign, VarContainer $C){
        $user_id = $C->getValue('user_id');
        $campaign_id = $campaign->getID();
        $DT = new DataAccessTableResult(RankingDataAccess::FromController($this), $C, 'RankingSites', 'getAllRankingData', 'getAllRankingCount');
        $rowTemplate = $this->getView('rows/campaign-rankings', false, false);
        $rowTemplate->init('Campaign', $campaign);
        $DT->setRowCompiler($rowTemplate, 'site');

        $datatableData = $DT->getArray(array('user_id' => $user_id, 'campaign_id' => $campaign_id, 'new_rows_ids' => $C->getValue('newRowsIds', false)));
        return $this->getAjaxView($datatableData);
    }

    function ajaxSaveRankingSite(CampaignModel $campaign, VarContainer $C){
        $MM = new ModelManager();
        $result = $MM->doSaveRankingSite($C->getData(), 0, RankingDataAccess::FromController($this));
        if($result instanceof ValidationStack){
            return $this->returnAjaxError($result);
        }
        return $this->getAjaxView($result);
    }

	function queryKeywords(CampaignModel $Campaign, VarContainer $C){
        $PQ     =   Qube::Start()->queryObjects('RankingKeywordModel', 'T', 'T.user_id = %d and T.campaign_id = %d and T.trashed = 00-00-0000', $this->User->getID(), $Campaign->id);
        return $PQ;
    }


    function ajaxRankingKeywords($campaign, VarContainer $C){
        $site_id = $C->getValue('site_ID');
        $campaign_id = $campaign->getID();
        $DT = new DataAccessTableResult(RankingDataAccess::FromController($this), $C, 'RankingKeywords', 'getAllKeywordData', 'getAllKeywordCount');
        $rowTemplate = $this->getView('rows/campaign-keywords', false, false);
        $rowTemplate->init('Campaign', $campaign);
        $DT->setRowCompiler($rowTemplate, 'keyword');
        return $this->getAjaxView($DT->getArray(array('site_ID' => $site_id, 'campaign_id' => $campaign_id, 'interval' => $C->get('interval', 'intval'), 'period' => $C->get('period'), 'new_rows_ids' => $C->getValue('newRowsIds', array()))));

		/*$C->load(array('table' => 'RankingKeywords'));
		$qP =   $this->queryKeywords($campaign, $C)->orderBy('T.keyword ASC')->calc_found_rows();
		$qP->Where('(T.site_id = '.$site_id.')');

		$qparams    =   array();
        if($C->checkValue('sSearch', $search) && $search != ''){
            $qP->Where('(T.name LIKE :search)');
                    $qparams[':search']  =   '%' . $search . '%';
        }

		$DT =   new DataTableResult($qP, $C);
		$DT->setRowCompiler($this->getView('rows/campaign-keywords', false, false), 'keyword');
		return $this->getAjaxView($DT->getArray($qparams));*/

    }

    function ajaxReports(CampaignModel $campaign=NULL, VarContainer $C){
        $DT = new DataAccessTableResult(ReportDataAccess::FromController($this), $C, 'CampaignReports', 'getAllReportData', 'getAllReportCount');
        $DT->setRowCompiler($this->getView('rows/campaign-reports-row', false, false), 'report');

        if (isset($campaign)):
            $user_id = $campaign->user_id;
            $campaign_id = $campaign->getID();
        else:
            $user_id = $this->user_ID;
            $campaign_id = 0;
        endif;
        return $this->getAjaxView($DT->getArray(array('user_id' => $user_id, 'campaign_id' => $campaign_id, 'newRowsIds' => $C->getValue('newRowsIds', array()))));
//        return $this->getAjaxView($DT->getArray(array('user_id' => $this->user_ID)));
    }

    function stripPhoneNumber($number){
        $number = str_replace('(', '', $number);
		$number = str_replace(')', '', $number);
		$number = str_replace('-', '', $number);
		return str_replace(' ', '', $number);
    }

    function ajaxPhoneRecording(VarContainer $C){

        require_once(QUBEADMIN . 'inc/phone.class.php');
        $phone = new Phone();

        $userID = $this->getUser()->getID();
        //$mid = $C->get('mid');
        $number = $C->get('number');

        //$recordings = $phone->getCallRecording($userID, $mid, $number);

        $recordings = $phone->getCallRecordingNew($userID, $number);

        $i = 1;
/*
        $finalData = array();
        foreach($recordings as $recording):
            $i++;
            $link = str_replace('my.patlive.com', '6qube.callroi.com', $recording['CurrentURL']);
            $finalData[] = array($i, $link, $userID, $mid, $number);
        endforeach;
*/
        //$this->Redirect($link);
        //return $this->getAjaxView((object)array('Link' => $link));
        return $this->getAjaxView($recordings);
    }

	/*function ajaxPhone(VarContainer $C){
		require_once(QUBEADMIN . 'inc/phone.class.php');

        $requestType = $C->get('requestType');

		$SortyBy = $C->get('iSortCol_0');
        switch($SortyBy){
			case 0:
				$column = 'DateTime';
				break;
			case 1:
				$column = 'PhoneNumber';
				break;
			default:
				throw new InvalidArgumentException('Column not supported.');
		}

		$SortDir = $C->get('sSortDir_0');
		$iDisplaysStart = $C->get('iDisplayStart','intVal');
		$iDisplayslength = $C->get('iDisplayLength','intVal');
		//$currentPage = (int)($iDisplaysStart/$iDisplayslength+1);
        $currentPage = $iDisplaysStart+1;
		$phone = new Phone();
		$user_id = $this->getUser()->getID();
		$did = $C->get('did');
		$callReport = $phone->getCallDetailsNew($did, $requestType, $currentPage, $iDisplayslength);
		$finalData = array();

        $blacklist = $phone->blacklistCalls();

            $a = 0;
            foreach ($callReport['response']['objects'] as $row) {
                //$date = date('m/j/Y', preg_replace('/[^\d]/','', $row['DateTime'])/1000);
                //$link = str_replace('my.patlive.com', '6qube.callroi.com', $callReport['CurrentURL']);
                //$link = $this->stripPhoneNumber( $row['CallerId'] );
                if (in_array($row['from_number'], $blacklist)) {
                    $a++;
                } else {
                    $date = date('Y-m-d H:i:s', strtotime($row['end_time']));
                    $calltime = gmdate("H:i:s", $row['call_duration']);
                    $link = '<a class="buttonS bDefault phoneDownload" href="' . $row['call_uuid'] . '"><span class="iconb" data-icon="&#xe078;"></span><span>Download</span></a>';
                    $finalData[] = array($date, $row['from_number'], $row['to_number'], $calltime, $link);
                }
            }

		//$totalRecords = $callReport['ResultSet']['TotalResults'];
        $totalRecords = $callReport['response']['meta']['total_count'];
        $totalRecords = $totalRecords-$a;
		$echo = $C->get('sEcho');
		return $this->getAjaxView((object)array('iTotalRecords' => $totalRecords, 'aaData' => $finalData, 'iTotalDisplayRecords' => $totalRecords, 'sEcho' => $echo));
	}*/

    function ajaxLeadConvert($Campaign){
        $form = $this->getLeadForm($Campaign, $_GET['LF_ID']);
        $V = $form->ConvertLeadForm();
        return $V->hasErrors() ? $this->returnValidationError($V) : $this->getAjaxView(array('error' => 0, 'message' => 'LeadForm Converted.'));
    }

    function ajaxTouchpoints(CampaignModel $Campaign, VarContainer $params){
        $type_filter = array();

        if($params->checkValue('params', $extraParams)){
            switch($extraParams['type']){
                case 'hub':
                    $type_filter[] = 'hub_id != 0';
                    break;
            }
        }

        $search = $params->get('search');
        $preloaded_touchpoints  = DataTablePreProcessor::RunIt($this,
            array(
                'limit' => 10,
                'cols' => 'ID touchpoint_ID, hostname, touchpoint_name, hub_ID',
                'where' => array_merge(array(
                    'active' => 1,
                    'campaign_ID' => $Campaign->getID(),
                    '(hostname like :search OR touchpoint_name like :search)'
                ), $type_filter),
                'search' => "%$search%"
            ))->getTouchPoints();

        return $this->getAjaxView($preloaded_touchpoints);
    }

    function ajaxCollabs(CampaignModel $Campaign, VarContainer $C){
        require_once('../admin/models/CampaignCollab.php');
        $C->table = 'CampaignCollabs';
        $args = array(
            'campaign_id' => $Campaign->getID()
        ) + DataTableResult::getRequestArgs($C);
		/** @var CampaignDataAccess $CDA */
		$CDA = CampaignDataAccess::FromController($this);
		$collabs = DataTableResult::returnDataTableStructure(array($CDA, 'getCollabs'), $args, $this->getView('rows/campaign-collab', false, false), 'collab', 'CampaignCollab');
        return $this->getAjaxView($collabs);
    }

	public function ajaxRankingChart($site_ID, $params){
        $RDA = RankingDataAccess::FromController($this);
		$chartDataGoogle = $RDA->getRankingDailyStatsChart($site_ID, 'google', $params);
		$chartDataBing = $RDA->getRankingDailyStatsChart($site_ID, 'bing', $params);
		return $this->getAjaxView(array(
			'chartDataGoogle' => $chartDataGoogle['data'],
			'chartDataBing' => $chartDataBing['data'],
			'chartOptions' => $chartDataGoogle['chartOptions']
		));
	}

	private function ajaxReferrals($R_ID, $Params)
	{
		/** @var ReportDataAccess $rda */
		$rda = ReportDataAccess::FromController($this);
		$Params['table'] = 'ReportReferrals';
		$args = DataTableResult::getRequestArgs(VarContainer::getContainer($Params));
		$report = $rda->getAllReportData(array(
			'id' => $R_ID,
			'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id',
			'user_id' => $this->getUser()->getID()
		));
		$args['report'] = $report;
		$args['filter'] = $Params['filter'];
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($rda, 'getDataTableReferralSearch'), $args, $this->getView('rows/report-referrals', false, false), 'stats', 'ReportModel'));
	}
	private function ajaxOrganic($R_ID, $Params)
	{
		/** @var ReportDataAccess $rda */
		$rda = ReportDataAccess::FromController($this);
		$Params['table'] = 'ReportOrganic';
		$args = DataTableResult::getRequestArgs(VarContainer::getContainer($Params));
		$report = $rda->getAllReportData(array(
			'id' => $R_ID,
			'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id',
			'user_id' => $this->getUser()->getID()
		));
		$args['report'] = $report;
		$args['filter'] = $Params['filter'];
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($rda, 'getDataTableOrganicSearch'), $args, $this->getView('rows/report-organic', false, false),'stats','ReportModel'));
	}

	private function ajaxSocialMedia($R_ID, $Params)
	{
		/** @var ReportDataAccess $rda */
		$rda = ReportDataAccess::FromController($this);
		$Params['table'] = 'ReportSocialMedia';
		$args = DataTableResult::getRequestArgs(VarContainer::getContainer($Params));
		$report = $rda->getAllReportData(array(
			'id' => $R_ID,
			'returnSingleObj' => 'ReportModel',
			'where' => 'id = :id AND user_id = :user_id',
			'user_id' => $this->getUser()->getID()
		));
		$args['report'] = $report;
		$args['filter'] = $Params['filter'];
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($rda, 'getDataTableSocialMediaSearch'), $args, $this->getView('rows/report-social', false, false),'stats','ReportModel'));
	}

	function importLeads(CampaignModel $Campaign, VarContainer $C){
		$form_id = $C->get('formID', 'intval');
		$leadFormProcessor = new LeadFormProcessor();
		$LeadsDataAccess = LeadsDAO::FromController($this);
		$ProspectDataAccess = ProspectDataAccess::FromController($this);
		$form = $LeadsDataAccess->getLeadForm($form_id);
		$leadFormProcessor->setForm($form);

		$file = $C->get('csvLeads');
		$csvFile = fopen($file['tmp_name'], 'r');
		$headers = fgetcsv($csvFile);
		$fields = $form->getFields();

		$lead_data = array(
			'user_id' => $this->getUser()->getID(),
			'form_id' => $form->getID(),
			'hub_id' => 0,
			'hub_page_id' => 0,
			'search_engine' => '',
			'keyword' => '',
			'remote_addr' => $_SERVER['REMOTE_ADDR'],
			'piwikVisitorId' => '',
			'is_spam' => 0,
			'cid' => $Campaign->getID()
		);

		$headers = array_map(function($header) use($fields){
			$result_field = array_values(array_filter($fields, function ($element) use ($header) {
				return $header == $element->label;
			}));
			return $result_field[0]->ID;
		}, $headers);

		while($row = @array_combine($headers, fgetcsv($csvFile))){
			$form->getFields();
			$inputValues = VarContainer::getContainer($row);
			$input = $leadFormProcessor->collectLeadInputs($inputValues);
			$lead_data['dataString'] = LegacyFuncs::getFieldDataStringLegacy($input);
            $lead_data['grade'] = LeadFormProcessor::getLeadRating($input);
			$leadID = $LeadsDataAccess->SaveLead($lead_data, $input);

			$ProspectDataAccess->SaveFromLeadID($leadID, $input->getPhoneValue());

			if($email = $input->getEmailValue()){
				$Photo = $ProspectDataAccess->getProspectPhotoByEmail($input->getEmailValue());
				if($Photo){
					$ProspectDataAccess->saveProspectPhoto($Photo, $this->getUser()->getID(), $input->getEmailValue());
				}
			}
		}

		return $this->getAjaxView(array('success' => true, 'errors' => false, 'message' => 'Import Leads Success'));
	}

	function downloadCVSFormExample(VarContainer $getContainer)
	{

		$form = LeadsDAO::FromController($this)->getLeadForm($getContainer->get('formID', 'intval'));
		$fields = $form->getFields();
		$headers = array_map(function(LeadFormFieldModel $item){ return $item->getLabel(); }, $fields);
		header('Content-type: text/csv');
		header("Content-Disposition: attachment;filename=\"Form Example {$form->name}.csv\"");
		$csvTmp = fopen('php://memory', 'w');
		fputcsv($csvTmp, $headers);
		fseek($csvTmp, 0);
		$result = stream_get_contents($csvTmp);
		return $this->getStringView($result);
	}

    function exportLeads(VarContainer $container){
		$LDA = LeadsDAO::FromController($this);
		$form = $LDA->getLeadForm($container->get('formID', 'intval'));
		$prospects = $LDA->getLeadsByFormId($form->getID());
		$csvTmp = fopen('php://memory', 'w');
		$fields = array_map(function($field){return $field->getLabel(); }, $form->getFields());
		fputcsv($csvTmp, $fields);
		foreach($prospects as $prospect){
			fputcsv($csvTmp, array_values($prospect->getData($LDA)));
		}
		fseek($csvTmp, 0);
		$result = stream_get_contents($csvTmp);
		header('Content-type: text/csv');
		header("Content-Disposition: attachment;filename=file.csv");
		return $this->getStringView($result);
    }

	function sendBraodcast(CampaignModel $Campaign, VarContainer $C)
	{
		require_once 'Mail.php';
		require_once QUBEADMIN . '/models/LeadData.php';
		$host = "relay.jangosmtp.net";
		$port = '25';
		$mail = new Mail();
        /** @var Mail_smtp $smtp */
		$smtp = $mail->factory('smtp',
			array ('host' => $host,
				'port' => $port,
				'auth' => false));


		$email = EmailBroadcastDataAccess::FromController($this)->getLeadBroadcasters(array(
			'id' => $C->get('b'),
			'returnSingleObj' => 'LeadFormEmailerModel',
			'where' => "e.id = :id AND e.sched_mode = 'now' AND e.active = 1 AND e.trashed = 0 AND e.user_ID = :user_ID"
		));

		$emailBody = $email->body;
		$emailBody .= "<br/><br/>" . $email->contact . "<br/>" . $email->anti_spam;
        $emailBody .= "[[UNSUBSCRIBE]]";


		$leads = LeadsDAO::FromController($this)->getLeadsByFormId($email->lead_form_id);
		$VS = new ValidationStack();

		if(empty($leads)){
			return $this->getAjaxView(array(
				'message' => "Everything went ok, but there wasn't leads to sent broadcast",
				'error' => false
			));
		}

		foreach($leads as $lead){
			$headers = array(
				'From' => $email->from_field,
				'To' => $lead->getEmailWithFormat(),
				'Subject' => stripslashes(str_replace("#name", $lead->name, $email->subject)),
				'Content-Type' => 'text/html; charset=ISO-8859-1',
				'MIME-Version' => '1.0',
				'Reply-To' => $email->from_field
			);

            $leadData = LeadData::getData(Qube::GetDriver(), $lead->getID());
            $parsedEmail = preg_replace_callback('/\[\[([^\]]+)\]\]/', function($matches) use ($leadData){
                if(isset($leadData[$matches[1]]))
                    return $leadData[$matches[1]]['value'];
                return $matches[0];
            }, $emailBody);

            //$main_site = LeadsDAO::FromController($this)->getLeadParentOptoutURL($lead->parent_id);
            $main_site = ResellerModel::Fetch('admin_user = %d', $lead->parent_id);

            $unsubscribe = '<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0" cellspacing="0" align="center" width="660">
						  <tbody>
							<tr>
							  <td valign="top" align="left"><p><br />
								  TO UNSUBSCRIBE: This email was sent to you because you filled out our contact form.
								   To remove yourself from our email program, please <a href="http://'.$main_site->main_site.'/?page=client-optout&email='.$lead->lead_email.'&pid='.$lead->id.'" target="_blank" style="color:#666666; text-decoration:underline;" >click here to UNSUBSCRIBE.</a></p></td>
							</tr>
						  </tbody>
						</table>';

			if($smtp->send($lead->getEmailWithFormat(), $headers, stripslashes(str_replace("[[UNSUBSCRIBE]]", $unsubscribe, $parsedEmail))) !== true){
				$VS->addError('email', "There was an error sending email to $lead->lead_email");
			}
		}

		if($VS->hasErrors())
			return $this->returnValidationError($VS);

		return $this->getAjaxView(array(
			'message' => 'Email Sent Successful',
			'error' => false
		));

	}
}

class Touchpoint
{
    protected $Controller;
    protected $Icon;
    protected $plural;
    function __construct($ctrl, $plural, $icon) {
        $this->Controller  =   $ctrl;
        $this->plural   =   $plural;
        $this->Icon =   $icon;
    }

    function getDashboardUrl(Template $V, WebsiteModel $site){
        return $V->action_url($this->ctrl . '_dashboard?ID=' . $site->id);
    }
}
