<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class WordpressImporterController extends SecureController //extends HybridBaseController
{
    const PASSSALT  =   'AA<,sd9dfgd-,d,.D>FG>FG:Ld=d=d0fgo3W#Msjsnxxfg';
    const COOKIENAME    =   '6qubelogin';
    
    function getView($page, $header = 'header.php', $footer = 'footer.php') {
        return parent::getView($page, 'login.php', 'login.php');
    }
    
    function downloadImage($full_url, $save_to_path){
#        static $paths   =   array();
        
        $basename   = basename($full_url);
        if(!preg_match('/(jpe?g|gif|png)$/i', $basename)) return false;
        
        // check if it exists
        if(file_exists($save_to_path . $basename))
                return $basename;
        // otherwise download it
        if(copy($full_url, $save_to_path . $basename))
                return $basename;
        
        return false;
    }
    
    function ProcessPostContent($content, $bloginfo, &$imagescount, &$photo){
        $matches    =   null;
        
        $images_reg =   array('/<a[^>]*?>\s*<img[^>]*?>\s*<\/a>/', '/<img.*?>/');
        
        $urls       =   array();        
        $imgs   =   array();
        

            $storagepath    =   '/users/' . $bloginfo->user_id . '/blog_post/';            
            $savetopath =   QUBEROOT . $storagepath;
            
            $imgct   =   0;
        foreach ($images_reg as $regxp):
            $imgc       =   preg_match_all($regxp, $content, $matches);

            $imgct += $imgc;
            if(!$imgc) continue; // no images found


            for($i = 0; $i < $imgc; $i++){
                $attrs  = preg_match("/src=(['\"]?)(http:\/\/[\s\S]*?)\\1/", $matches[0][$i], $attrv);
                $src    =   $attrv[2];
                $local_basename =   $this->downloadImage($src, $savetopath);
                if(!$local_basename){   // failed to download or find the image
                    $newattrs  =   'src="#url:' . $src . '"';
                }else{
                    $imagescount++;
                    $newattrs  =   'src="' . $storagepath . $local_basename . '"';
                    if($photo  ==  ''){
                        $photo =   $local_basename;
                        $content    =   str_replace($matches[0][$i], '', $content); // remove main image from post content
                    }

                    $append_i   =   $imgc+$i;   // push strings to end of replace array, replace all instances of the url with new url.
                    $matches[0][$append_i]  =   $src;

                    $urls[]     =   $src;
                    $imgs[]    = $storagepath . $local_basename;

                }
            }

        endforeach; 
        
        return $imgct ? str_replace($urls, $imgs, $content) : $content;
    }
        
    function createItem(SimplePie_Item $item, Blog $b, $bloginfo, &$importstats){
        $id =   $item->get_id();
        static $paths   =   array();
        
#        $imgscount  =    0;
        
        $info   =   array();
        $info[':user_id']   =   $this->user_ID;
        
        
        $info[':photo'] =   '';
        $info[':category']   =   empty($_POST['category'][$id]) ? $_POST['category_txt'][$id] : $_POST['category'][$id];
        $info[':date']   =   $_POST['dates'][$id];
        $info[':tags_url']    =   $_POST['tagsurl'][$id];
        $info[':tags']       =   $_POST['tags'][$id];
        $info[':post_title']      =   $_POST['title'][$id];
        $info[':category_url']  =   $b->convertKeywordUrl($info[':category'], 1);
        $info[':post_title_url']  =   $b->convertKeywordUrl($_POST['title'][$id], 1);
        $info[':post_content']   =   $this->ProcessPostContent($item->get_content(true), $bloginfo, $importstats['imgcounter'], $info[':photo']);
        
#        var_dump($imgs);
#        echo htmlentities($info[':post_content']);
        return $info;
    }
    
    function SaveFeed(SimplePie $pie, $bloginfo)
    {
     
        require_once QUBEADMIN . '/inc/blogs.class.php';
        
        $db =   Qube::GetPDO();
        $blog   =   new Blog();
                
        $pstmt  =   $db->prepare('INSERT INTO blog_post (blog_id, user_id, user_parent_id, tracking_id,
                category, category_url, post_title, post_title_url, post_content, tags, tags_url, photo, created)
                    VALUES (:blog_id, :user_id, :user_parent_id,
                            (select tracking_id FROM blogs WHERE id = :blog_id),
                            :category, :category_url, :post_title, :post_title_url, :post_content, :tags, :tags_url, :photo, str_to_date(:date, "%m/%d/%Y"))');
        
        $redir_insert   =   $db->prepare('REPLACE INTO 301_redir (ID, `TABLE`, REQUEST_URI, REDIRECT_URL) VALUES(?, "blog", ?, ?)');
        
        $imporstats =   array('imgcounter'  => 0, 'posts' => 0);
        $db->beginTransaction();
        foreach($pie->get_items() as $itempie):
            $item   =   $this->createItem($itempie, $blog, $bloginfo, $imporstats);            
            $item[':user_parent_id']    =   $bloginfo->user_parent_id;
            $item[':user_id']    =   $bloginfo->user_id;
            $item[':blog_id']   =   $bloginfo->id;
            
#            var_dump($item); exit;
            if($pstmt->execute($item)){
                    $imporstats['posts']++;
                    // save 301
                    $post_url   =   $itempie->get_link();
                    $path   =   parse_url($post_url, PHP_URL_PATH);
                    $query  =   parse_url($post_url, PHP_URL_QUERY);
                    
                    $uri    =   $path . ($query ? '?' . $query : '');
                    $redirto    =   '/' . $item[':category_url'] . '/' . $item[':post_title_url'] . '/';
                    
                    $redir_insert->execute(array($bloginfo->id, $uri, $redirto));
            }
        endforeach;
        
        $db->commit();
        
        return $imporstats;
    }
    
    function processToken(\QubeTokenModel $token, $action) {
        $token->doLoginUser($this);
    }    
    
    function ExecuteAction($action = NULL) {
        require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';          
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
        require HYBRID_PATH . '/library/simplepie/autoloader.php';
        
        $failed =   false;
        
        $User   =   false;
        
#        var_dump($this->ResellerData);
#        exit;/*
#        $reseller   = SecureController::validateResellerHostname($this->q, $this->ResellerData->main_site);
        
        if(!$this->ResellerData){
            return $this->Redirect('/?hybrid-admin-not-found');
        }
        
        
        $blog_id    =   (int)$this->getParam('blog_id', 0);
        if(!$blog_id)
            return $this->Redirect ('/?missing-parameter-in-request');
        
        $stmt   =   $this->query('SELECT id, user_id, if(user_parent_id=0,user_id,user_parent_id) as user_parent_id, blog_title FROM %s WHERE (user_id = %d OR user_parent_id = %d OR %d) AND id = %d',
                        'BlogModel', $this->user_ID, $this->user_ID, $this->hasPermission('is_system'), $blog_id);
        
        if(!$stmt) return false;
        $bloginfo   =   $stmt->fetch(PDO::FETCH_OBJ);
        
        #var_dump($bloginfo);
        
        $url    =   $this->getParam('url', '');
        
        $pie    =   null;
        if($url){
#            case 'url':
                $pie    =   new SimplePie();
                $cachedir   =   QUBEPATH . 'cache/simplepie';
                is_dir($cachedir) || mkdir($cachedir);
                $pie->set_cache_location($cachedir);
#			$this->pie->set_file(new SimplePie_File('dealer-product-feed.xml'));
                $pie->set_feed_url($url);
                $pie->init();
#                break;
        }
        
        $V  =   $this->getView('wp-import');
        $V->init('pie', $pie);
        $V->init('url', $url);
        $V->init('blog_id', $blog_id);
        $V->init('bloginfo', $bloginfo);
        
        if(isset($_POST['title'])){
            $stats    =   $this->SaveFeed($pie, $bloginfo);
            $V->init('stats', $stats);
#            $V->init('blog_title', $counter[0]);
#            var_dump($counter);
        }
        
#        VAR_DUMP($pie);
        
        return $V;
    }
}