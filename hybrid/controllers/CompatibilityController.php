<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of UserDataTransitioner
 *
 * @author amado
 */
class CompatibilityController extends DashboardBaseController {
    
    function ExecuteAction($action = NULL) {
        
        $args   =   VarContainer::getContainer($_POST);
        switch($action){
            case 'upload_form':
                return $this->doUploadForm($args);
            case 'check_domain':
                return $this->doCheckDomain($args);
        }
        
        return new BlankView();
    }
    
    /**
     * Adds a Domain!!
     * 
     * @param VarContainer $args
     * @return \ValidationStack
     * @throws QubeException
     */
    function doCheckDomain(VarContainer $args){
        $domain =   null;
        if(!$args->checkValue('domain', $domain)) throw new QubeException('Error');
        
        $DC =   WebsiteModel::GetDomainConfigFromHostInput($domain);
        if($DC instanceof ValidationStack) return $DC;
        
        $DS =   new DomainSetup();
        $DS->SetupRouter($DC);
    }
    
    function getStatus($table, $PK, $col)
    {
        return $this->q;
    }
    
    function ConvertHub(HubModel $hub){
        $convert_status =   $hub->getMeta('converted');
        if($convert_status  ==  1)  return;
        
        $hub->saveMeta('converted', 1);
#        return new BlankView();
    }
    
    function returnV1Ajax(array $arr, $error=   0){
        return $this->getAjaxView(array_merge(array('error' => $error), $arr));
    }
    
    function LegacyUpdateHub(VarContainer $p){
        if(!$p->checkValue('name', $name)) throw new QubeException('Missing name parameter');
        
        $value  =   $p->get('value');
	$HM	=	new HubModel($this->getQube(), array('id' => $p->get('id')));
        $Hub    =   $HM->Update(array($name => $value));
        if($Hub instanceof HubModel){
            $this->ConvertHub($Hub->Refresh());
            return array('message' => 'success', 'error' => 0);
        }
        
        throw new QubeException('failed');
    }

    function LegacyUpdatePage(VarContainer $p){
        if(!$p->checkValue('name', $col) || !preg_match('#^[\w_-]+$#', $col)) 
                        throw new QubeException('f');

        $q    =   $this->getQube();
        $Q	=   $q->GetPDO()->prepare('UPDATE hub_page2 SET `' . $col . '` = ? where id = ? AND user_id = ?');
        
        
        if(!$Q->execute(array($p->value, $p->id, $this->getDataOwnerID()))) throw new QubeException('failed');

//        $NewPage    =   PageModel::Fetch(array('oldtbl_ID' => $p->id));
        PageModel::ImportLegacyPage2($q, $p->id);
        
        return array('message' => 'success', 'error' => 0);
    }    
    
    function doUploadForm(VarContainer $p){
        
        if(!$p->checkValue('form', $table))    throw new QubeException('Method not supported.');
        
        $result =   array('message' => 'error ocurred Operation not defined', 'error' => 1);
        switch($table){
            case 'blog_post':
                $result =   $this->LegacyUpdateBlogPost($p);
                
            case 'hub':
                $result =   $this->LegacyUpdateHub($p);
                break;
            case 'hub_page2':
		$result =   $this->LegacyUpdatePage($p);
        }
        
        return $this->returnV1Ajax($result);
    }
    
    static function ProcessLegacyAdminRequest($_POST){
        
    }
    
    static function SetProspectGrades(){
        
    }
    //put your code here
}

?>
