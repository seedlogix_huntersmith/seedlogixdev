<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';

 require_once QUBEADMIN . 'models/HubModel.php';
 require_once QUBEADMIN . 'models/WebsiteModel.php';
 require_once QUBEADMIN . 'models/BlogModel.php';
 require_once HYBRID_PATH . 'models/SlideModel.php';
 CONST MU_SITE_PERMISSION = "view_theme";
#class 
 
class WebsiteController extends DashboardBaseController
{
    /**
     *
     * @var WebsiteModel
     */
    protected $site;
	protected $touchpointType = 'WEBSITE';
    function preExecute($action) {
        
#        if
#        $this->no_queries   =   ($action  ==  'save');
            
        $V  =   parent::preExecute($action);
        if($V) return $V;
        
#        $this->site =    $this->getSite($_GET['ID']);
        
        
#        if($this->site === false)
#            return $this->ActionRedirect ('Campaign_home'); // errror ocurred
        
        return NULL;
    }
    
    function getSitePreview(WebsiteModel $hub, $Theme = NULL, $path = '/'){
		$V  = $this->getPage('website-preview','Site Preview','campaign');/* $menu = 'campaign' */
		$preview_url = sprintf('http://hubpreview.6qube.com/i/%d/%d', $hub->user_id, $hub->getID());
		$V->init('preview_url', $preview_url);
		return $V;

        $themeID =  (isset($Theme)) ? (int)$Theme : $hub->theme;
//        $output =   new HubOutput($this->getQube());
        $hubrow =   HybridRouter::findHub($hub->httphostkey, $hub->id, $themeID);

//        $theme  =   $this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $themeID)->Exec()->fetch();

        $HR = new HybridRouter();
        $HR->setRouterRequest(RouterRequest::CreateRequest('', '/'));
//        $router_request = new HubURIRequest($this->getQube(), '/');

        //end quick edit
        return $HR->getResponder($hubrow)->getOutputView('/');

//        error_reporting(0);// disable errors
        $output->setHub($hubrow);
        $output->setTheme($theme);
        // black vodoo magic 
        $output->setVar('baseUrl', $this->ActionUrl('Website_preview?ID=' . $hub->id, array('theme' => $themeID)) . '&path=');
        // end
        return $output->getOutputView(htmlentities($path, ENT_QUOTES));
    }
    
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()) {
        $active_classes[]   =   'campaign-nav';
        
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }

	/* @FER: THIS IS HARDCORE SHIT & IT'S PRETTY AWESOME */
	function addSiteCrumbs(WebsiteModel $site,$action){
		$CDA		= \Qube\Hybrid\DataAccess\CampaignDataAccess::FromController($this);
		$CC			= new CampaignController();
		$c_id		= $site->cid;
		$c_name		= $CDA->getCampaign($c_id)->name;
		$t_id		= $site->id;
		$t_name		= $site->name;
		$t_type		= $site->getTouchpointType();
		$t_label1	= $CC->getTouchPointTypes($t_type);
		$t_label2	= $action == 'seo' ? 'SEO' : ($action == 'editRegions' ? 'Regions' : ($action == 'photos' ? 'Slider' : ucfirst($action)));
		$t_ctrl		= ucfirst(mb_strtolower($t_type));

		if($c_id == 99999){
			return parent::getCrumbsRoot(false,false,null,null)->addCrumbTxt($t_ctrl.'_'.$action.'?ID='.$t_id,$t_label2.': '.$t_name);
		} else{
			return parent::getCrumbsRoot(true,true,$c_id,$c_name)->addCrumbTxt('Campaign_dashboard?ID='.$c_id.'&pointtype='.$t_type,$t_label1[1])->addCrumbTxt($t_ctrl.'_'.$action.'?ID='.$t_id,$t_label2.': '.$t_name);
		}
	}

	/**
	 * @param $site_ID
	 * @return WebsiteModel
	 * @throws QubeException
	 */
	function getSite($site_ID)
    {
		return WebsiteDataAccess::FromController($this)->getSite($site_ID, $this->touchpointType);
    }

/*
    function getCrumbsSite($action_fmt, $current_ID, $cid)
    {
//        $sites  =   $this->q->queryObjects('WebsiteModel')->Where('cid = %d AND (user_id = @dashboard_userid OR %d)',
//                $cid, $this->getUser()->can('is_system'))
//                        ->Fields('id, name')->orderBy('id = ' . $current_ID . ' DESC')->Exec()->fetchAll(PDO::FETCH_ASSOC);
        $bread  =   NULL;
		$WDA = WebsiteDataAccess::FromController($this);
        // @todo fix this, this is doing a select *, it should only fetch name, and id to generate the links.
		$sites = $WDA->getSites($cid, array('orderBy' => array('id = :current_id'), 'limit' => 50,
				'bindParams' => array('current_id' => $current_ID))
		);
        foreach($sites as $site)
        {
            $temp   =   new BreadCrumb(sprintf($action_fmt, $site->getID()), $site->name);
            if(is_null($bread))
                $bread  =   $temp;
            else
                $bread->addChild ($temp);
        }
        return $bread;
        
    }
*/

	//view individual website
	function ViewDashboard(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		if($site->cid != 99999){
			if($deny = $this->Protect("view_{$site->touchpoint_type}", 'Auth', $site->getID()))
							return $deny;
			
			//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('website_social?ID=' . $this->site->id, 'SDashboard');

			$Chart  =   new LeadsVisitsChartData('sitechart', '7-DAY', false, array('id' => $site->id, 'controller' => $this, 'touchpointType' => strtolower($this->touchpointType)));
			$timevars   =   VarContainer::getContainer($Chart->getConfig());
			$timecontext = new PointsSetBuilder($timevars, $this->getUser()->created);
			
			if($this->getUser()->can('view_prospects'))
			{
	//			$leads  =   $site->queryProspects()->Limit(10)->calc_found_rows()->orderBy('created DESC')->Exec()->fetchAll();
				$leads = ProspectDataAccess::FromController($this)->getAccountProspects(array(
					'calc_found_rows' => 1,
					'limit' => 10,
					'touchpoint_ID' => $site->touchpoint_ID,
					'timecontext' => $timecontext
				));
				$totalProspects = $this->query('SELECT FOUND_ROWS()')->fetchColumn();		//$this->queryLeads('hub_id = %d', $site->id)->Exec()->fetchAll();
			}else{
				$leads = array();
				$totalProspects	=	0;
			}
			
			//$site->doTransitionPages(Qube::Start());
			
			$total_pages = $site->queryPages('trashed = 0')->Fields('count(*) as total')->Exec()->fetchColumn();
			
			#$site   =   $this->q->queryObjects('WebsiteModel')->Where('id = %d', $site_ID)->Exec()->fetch();
			//var_dump($leads);
				
			$analytics  =   new DashboardAnalytics;
			$analytics->prepareHubTrackingIds('t.id = ' . (int)$site->id, 'WebsiteModel');

			/* MU don't have a dashboard */
			$V = $this->getPage('website-dashboard',$site->name.' Dashboard','campaign');/* $menu = 'campaign' */

			$this->addSiteCrumbs($site,'dashboard');

			$V->init('kwranks', RankingDataAccess::FromController($this)->
			getTopRankingKeywordsBySearchEngine($timecontext, $this, array('campaign_ID' => $site->cid,
					'TOUCHPOINT_TYPE' => $site->touchpoint_type, 'touchpoint_ID' => $site->touchpoint_ID)));
			//$ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);
			
			//$V->init('ChartWidget', $ChartWidget);
			
			$V->init('chart',   $Chart);    
			
			$V->init('KeywordsWidget', new KeywordsWidget($Chart->getConfig(), $this->getDataOwnerID()));        
			$V->init('prospects', $leads);
			$V->init('totalProspects', $totalProspects);
			
			// can't believe this shit works. 
	//		$V->init('kwranks', Rankings::getTopRankings(new PointsSetBuilder(new VarContainer($Chart->getConfig()))));

			$args	=	array(
				'timecontext' => $timecontext,
				'campaign_id' => $site->cid,
				'tbl' => 'hub',
				'tbl_ID' => $site->id
			);

			$topPerformingKeywords	= PiwikReportsDataAccess::FromController($this)->getTopPerformingKeywords($args);
			$V->init('TopPerformingKeywords', $topPerformingKeywords);
			$V->init('total_pages', $total_pages);
			$V->init('_title', $site->name);
			$V->init('touchpoint_type', strtolower($site->touchpoint_type));
			$V->init('showDashboardRange', true);
			$V->init('campaignVideo', true);
			
			return $V;
			//return $this->getView('campaign-dashboard');
		} else{
			/* MU don't have a dashboard */
			die('Error');
		}
	}

	//view Settings
	function ViewSettings(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$this->addSiteCrumbs($site,'settings');

		/* MU Hack */
		$V = $this->getPage('website-settings',$site->name.' Settings',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$AvailableRoles = UserDataAccess::FromController($this)->getRoles(0, array('reseller_user' => $this->getUser()->getParentID()));
		$MU_Roles = WebsiteDataAccess::getroles($site->getID());
		$V->init('Roles', $AvailableRoles);
		$V->init('SelectedRoles', $MU_Roles);

		if($this->getUser()->can('is_system')){
			$dataAccess = new UserDataAccess();
			$ownerUser = $dataAccess->getUser($site->user_id);
			$V->init('storagePath', $ownerUser->getStorageUrl());
		}

		$V->init('locks', $site->getFieldLocker());
		if($site->hub_parent_id){
			$V->init('parent_locks', HubDataAccess::FromController($this)->getWebsiteLocks($site->hub_parent_id));
		}
        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
        $V->init('sitetheme', $themeModel);
		return $V;
	}
	
	//view Theme
	function ViewTheme(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		require_once HYBRID_PATH . '/models/HubThemeModel.php';

		/**
		 * a mu-theme = $site->hub_parent_id
		 * a regular theme = $site->theme
		 *
		 */
        $hda = HubDataAccess::FromController($this);
		$User = $hda->getActionUser();
        $MUSites = array();
        $FilteredMUSites    =   array();
		// seems like the table should use an index for this query but there is none at the moment.
		if($site->multi_user != 1){
			$MUSites  =   $hda->getMuSitesAsThemes($site->hub_parent_id, $this->touchpointType);
			foreach($MUSites as $MUSite){
				$roles = WebsiteDataAccess::getroles($MUSite->id);
				if(!$roles){
					$roles = array();
				}
				if(in_array($User->role_ID,$roles) || $User->getID() == $MUSite->owner){
					$FilteredMUSites[] = $MUSite;
				}
			}
		}
		$HubThemes	=	$hda->getHubThemes($site->hub_parent_id == 0 ? $site->theme : 0, $site->touchpoint_type);

		$siteTheme = HubDataAccess::FindSiteTheme($site, $MUSites, $HubThemes);
		$Blogs = $this->q->queryObjects('BlogModel')
					->Where('(user_id =  dashboard_userid() OR %d) AND cid = %d', 
								$this->getUser()->can('is_system'),   $site->cid)->Exec()->fetchAll();

		/* MU Hack */
		$V = $this->getPage('website-theme',$site->name.' Theme',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		if($this->getUser()->can('is_system')){
			$dataAccess = new UserDataAccess();
			$ownerUser = $dataAccess->getUser($site->user_id);
			$V->init('storagePath', $ownerUser->getStorageUrl());
		}
		
		$V->init('sitetheme', $siteTheme);
		$V->init('musites', $FilteredMUSites);
		$V->init('hubthemes', $HubThemes);
		$V->init('blogs', $Blogs);
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));

		$this->addSiteCrumbs($site,'theme');
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');

		$V->init('locks', $site->getFieldLocker());
		return $V;
	}
	
	//view SEO
	function ViewSEO(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$this->addSiteCrumbs($site,'seo');
		$locks = new HubFieldLocks($site->multi_user_fields);

		/* MU Hack */
		$V = $this->getPage('website-seo',$site->name.' SEO',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$V->init('locks', $locks);
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('site', $site);
		$themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();
		$V->init('sitetheme', $themeModel);
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
	  	return $V;
	}
	
	//view Edit Regions
	function ViewEditRegions(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$this->addSiteCrumbs($site,'editRegions');

        /* @var $themeModel HubThemeModel */
		$themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();

		/* MU Hack */
		$V = $this->getPage('website-editRegions',$site->name.' Regions',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$V->init('hubTheme', $themeModel);
        $V->init('fields', $themeModel->getEditFields($site));
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('locks', $site->getFieldLocker());
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
	  	return $V;
	}

    //view Theme Designer
    function ViewThemeDesigner(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$this->addSiteCrumbs($site,'themeDesigner');

        /* @var $themeModel HubThemeModel */
        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();

        $V  = $this->getPage('website-themeDesigner',$site->name.' edit regions','campaign');/* $menu = 'campaign' */
        $V->init('hubTheme', $themeModel);
        return $V;
    }

	//view Social
	function ViewSocial(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		/* MU Hack */
		$V = $this->getPage('website-social',$site->name.' Social',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$this->addSiteCrumbs($site,'social');
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('locks', $site->getFieldLocker());
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
	  	return $V;
	}

/*
	function doCreatePage(array $data, WebsiteModel $site)
	{
        throw new QubeException();
        $MM = f;
		$Page   =   $site->newPage() ;//->loadArray($pvars);
		$result =   array();
		$Validation   =   new ValidationStack();
		
		$nav_order   =   $this->query('SELECT nav_order FROM %s WHERE hub_id = %d ORDER BY nav_order DESC LIMIT 1', 'PageModel', $Page->hub_id)->fetch();
		$data['nav_order'] = $nav_order[0] + 1;
		
		if(!$ID && !PageModel::ValidatePage($data, $Validation)) return $Validation; // if creating a new row
		
		if($Page->SaveData($data, NULL, $Validation, NULL, DBWhereExpression::Create('id = %d AND user_id = dashboard_userid()', $ID), $result))
		{
			return $result;
		}
		
		return $Validation;
	}
*/

	function returnCreatePage(WebsiteModel $site, array $pvars, $actionData = array()){
		$MM = new ModelManager();
        $site_page_data = $site->getPageDataInfo();

//		$Page = $site->newPage();
		$pvars['touchpoint_type'] = $site->touchpoint_type;
		$result =   $MM->doSavePage( $site_page_data + $pvars, WebsiteDataAccess::FromController($this));
		//if($result instanceof ValidationStack)
		//	return $this->returnValidationError($result);
		$this->getSession()->validCreatePage=$result;
		if(empty($actionData)){
			return $this->ActionRedirect("{$this->touchpointType}_pages?ID=" . $site->id);
		}

		$action = $actionData['name'];
		$param = $actionData['param'];

		return $this->ActionRedirect("{$this->touchpointType}_$action", array($param => $pvars['nav_parent_id']));

	}

	function createNav(WebsiteModel $site, VarContainer $v, $actionData = array()){
		$data   =   $v->getData();
		$MM=new ModelManager();

		$nav_order   =   $this->query('SELECT nav_order FROM %s WHERE hub_id = %d ORDER BY nav_order DESC LIMIT 1', 'NavModel', $site->id)->fetch();
		$data['nav_order'] = $nav_order[0] + 1;

		//$Nav    =   $site->newNav()->loadArray($data);
		$data['hub_id']=$site->getID();
		// todo validate
		//$Nav->Save();
		$data['touchpoint_type'] = $site->touchpoint_type;
                
		$valid=$MM->doSaveNav($data,0,WebsiteDataAccess::FromController($this));

		//if($valid instanceof ValidationStack)
		$this->getSession()->validnav=$valid;

		if(empty($actionData)){
			return $this->ActionRedirect("{$this->touchpointType}_pages?ID=" . $site->id);
		}

		$action = $actionData['name'];
		$param = $actionData['param'];
		return $this->ActionRedirect("{$this->touchpointType}_$action", array($param => $data['nav_parent_id']));
	}

	/**
	 *
	 * @param string $title
     * @param WebsiteModel $site
	 * @return SlideModel
	 */
	private function createSlide($title  =   'New Slide', WebsiteModel $site)
	{
		$MM = new ModelManager();
		$Slides =   $site->getSlidePhotos();
		$result = array();
		
		$names = SlideModel::getPicsInternalNames();
		
		foreach($Slides as $Slide)
		{
		    if (($key = array_search($Slide->internalname, $names)) !== false)
                unset($names[array_search($Slide->internalname, $names)]);
		}
		
		if (count($names))
		{
		    $internalName = reset($names); // Get the first available name
		    $slideData  =   array('title' => $title, 'user_id' => $this->getUser()->getID(), 'hub_id' => $site->id, 'internalname' => $internalName);

		    $result = $MM->doSaveSlide($slideData, 0, HubDataAccess::FromController($this));
		}
		else
		{
		    $result['slide'] = 0;
		}
		
		return $result['slide'];
	}
	
	private function deleteSlide(WebsiteModel $site)
	{
	    if (isset($_GET['slide']))
	    {
			$trash = new TrashManager(SystemDataAccess::FromController($this));
			$return = $trash->TrashByParam(VarContainer::getContainer(array('slide_ID' => $_GET['slide'])), $message);
//	        $Slide   =   $this->q->queryObjects('SlideModel', 'T', 'T.ID = %d', $_GET['slide'])->Exec()->fetch();
//	        $this->query('UPDATE %s SET deleted = NOW() WHERE user_id = dashboard_userid() AND ID = %d', 'SlideModel', $Slide->ID);
	    }
	}
        
	//view Photos
	function ViewPhotos(WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$slides =   $site->getSlidePhotos();
		$dataAccess = new UserDataAccess();
		$ownerUser = $dataAccess->getUser($site->user_id);

		if(count($slides)   ==  0)
		{
			$slides[]  =   $this->createSlide('Slide 1', $site);
		}
		
		$MediaCtrl  =   new MediaController($this);

		/* MU Hack */
		$V = $this->getPage('website-photos',$site->name.' Slider',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$V->init('slides', $slides);
//		$V->init('thumbs', $MediaCtrl->getMediaItems());
		if($this->getUser()->can('is_system')){
			$V->init('storagePath', $ownerUser->getStorageUrl());
		}

		//@todo limit # of media items
		$this->addSiteCrumbs($site,'photos');
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('locks', $site->getFieldLocker());
		$V->init('site', $site);
        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();
        $V->init('sitetheme', $themeModel);
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
		return $V;
	}
		
	//view Pages
	function ViewPages(WebsiteModel $site, $Nodes){/* @FER: BREADCRUMBS FIXED */
		$this->addSiteCrumbs($site,'pages');

		/* MU Hack */
		$V = $this->getPage('website-pages',$site->name.' Pages',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		#$nodes = pages / navs
		#$Nodes = $site->getNodes();
		//Initialize Session variable of Validation Nav in Website

		if($this->getSession()->validnav){
			if($this->getSession()->validnav instanceof ValidationStack){
				$V->init('validNav',$this->getSession()->validnav->getErrors());
			}else{
				$V->init('verifyNav',$this->getSession()->validnav['message']);
			}
			unset($this->getSession()->validnav);
		}elseif($this->getSession()->delnav){
			$V->init('delNav',$this->getSession()->delnav);
			unset($this->getSession()->delnav);
		}

		if($this->getSession()->validCreatePage){
			if($this->getSession()->validCreatePage instanceof ValidationStack){
				$V->init('validCP',$this->getSession()->validCreatePage->getErrors());
			}else{
				$V->init('verifyCP',$this->getSession()->validCreatePage['message']);
			}
			unset($this->getSession()->validCreatePage);
		}elseif($this->getSession()->delpage){
			$V->init('delPage',$this->getSession()->delpage);
			unset($this->getSession()->delpage);
		}

		$V->init('Nodes', $Nodes);
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('locks', $site->getFieldLocker());
		$V->init('touchpoint_type', strtolower($site->touchpoint_type));
	  	return $V;
	}
	
	//view Pages Detail
	function ViewPagesDetail(PageModel $page){/* @FER: BREADCRUMBS FIXED */
		$WDA = WebsiteDataAccess::FromController($this);
		$site   =   $page->getSite();
		$page_locks = $page->Refresh('multi_user_fields')->getFieldLocker();

		/* MU Hack */
		$V = $this->getPage('website-pages-pageDetail',$site->name.' Page',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$this->addSiteCrumbs($site,'pages')->addCrumbTxt('Website_pagedetails?PID='.$page->id,'Page: '.$page->page_title);

		$V->init('locks', $page_locks);
		$V->init('page', $page);
		if($page->page_parent_id){
			$V->init('parent_hub_lock', $WDA->getPageParentLocks($page->page_parent_id));
		}

        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $site->theme)->Exec()->fetch();
        $V->init('sitetheme', $themeModel);
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('touchpoint_type',strtolower($site->touchpoint_type));
	  	return $V;
	}
	
    //view Pages Detail
	function ViewNavDetail(NavModel $page, WebsiteModel $site){/* @FER: BREADCRUMBS FIXED */
		$WDA = WebsiteDataAccess::FromController($this);

		$this->addSiteCrumbs($site,'pages')->addCrumbTxt('Website_pagedetails?NID='.$page->id,'Nav: '.$page->page_title);

		/* MU Hack */
		$V = $this->getPage('website-pages-navDetail',$site->name.' Nav',$site->cid != 99999 ? 'campaign' : 'admin');/* $menu = 'campaign' OR 'admin' */

		$V->init('locks', $page->Refresh('multi_user_fields')->getFieldLocker());
		if($page->page_parent_id){
			$V->init('parent_hub_lock', $WDA->getNavParentLocks($page->page_parent_id));
		}
		if (isset($_GET['NID'])) $Nodes = $page->getNodes();
		
		$V->init('Nodes', $Nodes);
		$V->init('page', $page);
		//IF IS MU change breadcrumbs
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		//$this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$V->init('site', $site);

		if($this->getSession()->validnav){
			if($this->getSession()->validnav instanceof ValidationStack){
				$V->init('validNav',$this->getSession()->validnav->getErrors());
			}else{
				$V->init('verifyNav',$this->getSession()->validnav['message']);
			}
			unset($this->getSession()->validnav);
		}elseif($this->getSession()->delnav){
			$V->init('delNav',$this->getSession()->delnav);
			unset($this->getSession()->delnav);
		}

		if($this->getSession()->validCreatePage){
			if($this->getSession()->validCreatePage instanceof ValidationStack){
				$V->init('validCP',$this->getSession()->validCreatePage->getErrors());
			}else{
				$V->init('verifyCP',$this->getSession()->validCreatePage['message']);
			}
			unset($this->getSession()->validCreatePage);
		}elseif($this->getSession()->delpage){
			$V->init('delPage',$this->getSession()->delpage);
			unset($this->getSession()->delpage);
		}

		$V->init('touchpoint_type',strtolower($site->touchpoint_type));
	  	return $V;
	}
    
    function ExecuteAction($action = NULL) {
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
        
        if(isset($_GET['ID'])){
            $site = $this->getSite($_GET['ID']);
            
			if(!$site) die('Error');
            
			$PageNav = $this->doAction($site, $action);
            
			if($PageNav) {
				$PageNav->init('site', $site);
				$PageNav->init('parent_hub_lock', $site->hub_parent_id ? HubDataAccess::FromController($this)->getWebsiteLocks($site->hub_parent_id) : false);
				$PageNav->init('can_edit_site', $this->getUser()->can("edit_{$this->touchpointType}", $site->getID()));
				$PageNav->init('multi_user', $site->isMultiSite());
			}
			return $PageNav;
        }
        
        switch($action){
            case 'moveup':
                return $this->doMoveUp($_GET['nid'], $_GET['node']);
            case 'movedown':
                return $this->doMoveDown($_GET['nid'], $_GET['node']);
        }
        
        if(isset($_GET['NID'])){
            /* @var $Nav NavModel */
            //$Nav    =   $this->q->queryObjects('NavModel', 'N', 'N.ID = %d AND trashed = 0', $_GET['NID'])->Exec()->fetch();
            $Nav    =   $this->q->queryObjects('NavModel', 'N', 'N.id = %d AND trashed = 0', $_GET['NID'])->Exec()->fetch();
			$site   =   $Nav->getSite();

            switch($action){
                case 'trashpage':
                    $parent_node    =   $Nav->nav_parent_id;
//                    $this->query('UPDATE %s SET trashed = NOW() WHERE user_id = dashboard_userid() AND id = %d', 'NavModel', $Nav->id);
					$TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
					$result = $TrashMgr->TrashByParam(VarContainer::getContainer($_GET  + array('touchpointType' => $this->touchpointType)), $message, true);
					$this->getSession()->delnav= $message;
                    //return $this->ActionRedirect('Website_pages?ID=' . $Nav->hub_id);
					return $parent_node ? $this->ActionRedirect('Website_pagedetails?NID=' . $parent_node) :
						$this->ActionRedirect('Website_pages?ID=' . $Nav->hub_id);
                    /*return $parent_node ? $this->ActionRedirect('Website_pagedetails?NID=' . $parent_node) :
                            $this->ActionRedirect('Website_pages?ID=' . $Nav->hub_ID);*/
                    /*$this->query('UPDATE %s SET trashed = NOW() WHERE user_ID = dashboard_userid() AND ID = %d', 'NavModel', $Nav->ID);
                    return $parent_node ? $this->ActionRedirect('Website_pagedetails?NID=' . $parent_node) :
                            $this->ActionRedirect('Website_pages?ID=' . $Nav->hub_ID);*/
                    
                case 'pagedetails': // yea, should be navdetails, but we're balls to the walls. btw
                    $PageModel   =   $this->q->queryObjects('NavModel', 'T','T.id = %d', $_GET['NID'])->Exec()->fetch();
                    $PageNav = $this->ViewNavDetail($PageModel, $site);

                    $PageNav->init('site', $site);
                    $PageNav->init('multi_user', $site->isMultiSite());
                    $PageNav->init('can_edit_site', $this->getUser()->can("edit_{$this->touchpointType}", $site->getID()));
                    return $PageNav;
            }
        }
        
        if(isset($_GET['PID'])){
            // @todo security
            /* @var $PageNav PageModel */
            $PageNav   =   $this->q->queryObjects('PageModel', 'T',
                        'T.id = %d', $_GET['PID'])->Exec()->fetch();
            if($PageNav) {
                $Page = $this->doPageAction($action, $PageNav);

                $site = $this->getSite($PageNav->hub_id);

                $Page->init('site', $site);
                $Page->init('multi_user', $site->isMultiSite());
                $Page->init('can_edit_site', $this->getUser()->can("edit_{$this->touchpointType}", $site->getID()));

                if ($Page)
                    return $Page;
            }
        }
        throw new Exception('Invalid Action');
    }
    
    function doPageAction($action, PageModel $Page){
         switch($action){
            case 'trashpage':
                $parent_node = $Page->nav_ID;
//                $this->query('UPDATE %s SET trashed = NOW() WHERE user_ID = dashboard_userid() AND ID = %d', 'PageModel', $Page->ID);
				$TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
				$result = $TrashMgr->TrashByParam(VarContainer::getContainer($_GET  + array('touchpointType' => $this->touchpointType)), $message, true);
				$this->getSession()->delnav= $message;
                return $parent_node ? $this->ActionRedirect("{$this->touchpointType}_pagedetails?NID=" . $parent_node) :
                        $this->ActionRedirect("{$this->touchpointType}_pages?ID=" . $Page->hub_id);
                
             case 'pagedetails':
                 return $this->ViewPagesDetail($Page);
             case 'savepage':
                 return $this->doSavePage($Page, $_POST['page']);
         }
         throw new Exception('Invalid Action.');
    }
    
    function doSortPages($site)
    {
        if (isset($_POST['website']['table']))
        {
            $tableOrder = $_POST['website']['table'];
            $table = explode(',', $tableOrder);
            $i = 0;
            
            $sql = "UPDATE hub_page2 SET nav_order = CASE id ";
            
            foreach ($table as $id) {
                $sql .= "WHEN $id THEN $i ";
                ++$i;
            }
            $sql .= "END WHERE id IN ($tableOrder)";
            
            $this->query($sql);
            
            return "Sorting successful.";
        }
        
        return "Error sorting pages.";
    }
    
    function doSavePage(PageModel $Page, array $data){
        // @todo unset user_id and other shit VALIDATE!
		$MM = new ModelManager();
        $site = $this->getSite($Page->hub_id);
		$data['id'] = $Page->getID();
		$data['touchpoint_type'] =  $site->touchpoint_type;
		$return = $MM->doSavePage($site->getPageDataInfo() + $data, WebsiteDataAccess::FromController($this));
		if($return instanceof ValidationStack){
			return $this->returnValidationError($return);
		}
		$return['params']	=	$data;
		return $this->getAjaxView($return);
    }
    
    function doMoveUp($nid, $type){
        $model  =   '';
        switch($type){
            case 'page':
                $model  =   'PageModel';
                /*$stmt   =   $this->query('SELECT hub_ID, sortk, nav_ID from %s where ID = %d ', $model, $nid);
                $nav_parent_col = 'nav_ID';*/
                $stmt   =   $this->query('SELECT id, hub_id, nav_order, nav_parent_id FROM %s WHERE id = %d ', $model, $nid);
                $nav_parent_col = 'nav_parent_id';
                break;
            case 'nav':
                $model  =   'NavModel';
                    /*$stmt   =   $this->query('select hub_ID, sortk, parent_ID from %s WHERE ID = %d', $model, $nid);
                                $nav_parent_col =   'parent_ID';*/
                    $stmt   =   $this->query('SELECT id, hub_id, nav_order, nav_parent_id FROM %s WHERE id = %d', $model, $nid);
                                $nav_parent_col =   'nav_parent_id';
                break;
            default:
                throw new Exception('fail');
        }
        
        // Validate
        list($id, $hub_id, $nav_order, $nav_parent_id) = $stmt->fetch(PDO::FETCH_NUM);
        
        $stmt   =   $this->query('SELECT id, nav_order FROM %s WHERE '.
        			'nav_order < %d AND hub_id = %d AND nav_parent_id = %d AND trashed = 0 ORDER BY nav_order DESC LIMIT 1',
                    $model, $nav_order, $hub_id, $nav_parent_id);
                    
        list($top_id, $top_nav_order) = $stmt->fetch(PDO::FETCH_NUM);
        
        // Switch the nav_orders of the above/below pair.
        $this->query('UPDATE %s SET nav_order = %d WHERE hub_id = %d AND id = %d ', $model, $top_nav_order, $hub_id, $id);
        $this->query('UPDATE %s SET nav_order = %d WHERE hub_id = %d AND id = %d ', $model, $nav_order, $hub_id, $top_id);
        
        return $this->ActionRedirect('Website_pages?ID=' .  $hub_id);
        
        // validate
        //list($hub_ID, $sortk, $parent_nav_ID) = $stmt->fetch(PDO::FETCH_NUM);
        
        // get node siblings
        //$stmt   =   $this->query('UPDATE %s SET sortk = sortk - 15 WHERE ID = %d', $model, $nid);
		#echo $stmt->rowCount();        
        
        /*$this->query('create temporary table sortvalues as select type, ID, sortk FROM
            6q_website_treenodes WHERE hub_ID = ' . $hub_ID . ' AND 
                parent_nav_ID = ' . $parent_nav_ID, false);*/
        
        
        // re-sort 
        /*$this->query('SET @F=0', FALSE);
        $Q = $this->query('UPDATE sortvalues SET sortk = (@F:=@F+1)*10 ' . 
//                    ' WHERE sortk > ' . ($sortk - 20 ) .
                ' ORDER BY sortk ASC', false);
        
        $this->query('update 6q_hubpages_tbl w, sortvalues v set w.sortk = v.sortk
            where v.type="page" AND w.ID=v.ID');
        $this->query('update 6q_navs_tbl w, sortvalues v set w.sortk = v.sortk
            where v.type="nav" AND w.ID=v.ID');
        return $this->ActionRedirect('Website_pages?ID=' .  $hub_ID);*/
    }
    
    function getWebsitesStats($campaign_ID){
        return $this->cacheCheck('sitestats_' . $campaign_ID, 60 * 15);
    }
    
    function getStats(WebsiteModel $W){
        $alldata    =   $this->getWebsitesStats($W->cid);

        $data   =   array();
        
        if(getflag('disable_cache') || $alldata === false   ||  !isset($alldata[$W->id]))
        {
            $atycs  =   new DashboardAnalytics;
            $atycs->prepareHubTrackingIds('id = ' . (int)$W->id, 'hub');    
            
            $points =    $atycs->getChartData(new PointsSetBuilder(new VarContainer(array('interval' => 1,
                'period'    =>  'WEEK'))), 'nb_visits', 'ValueArray');
            
//            $points   =   $atycs->getChartData(1, 'WEEK', 'nb_visits', 'ValueArray');
#		var_dump($points);
            $data=  array('prevw'   => $points[0], 'curw'   => $points[1]);
            
            $row    =   new WebsiteStatsRow();
            $row->render($data);
//            $row->id    =   $W->id;
            if($alldata === false)
                $alldata    =   array();
            
            $alldata[$W->id]    =   $row->getRenderedValues();
            $this->cacheSave($alldata);
        }
        
        $alldata[$W->id]->id  =   $W->id;
        $returnv    =   array($alldata[$W->id]);
        $ajax   =   array('values'  =>  $returnv, 'id'  => $W->id);
        
        return $this->returnAjaxData('data', $ajax);
        
        if($alldata === false)
            $alldata    =   array();
        
        $alldata[$W->id]    =   $data;
    }
    
    function doAction(WebsiteModel $site, $action){
    //load view
    switch($action)
    {
        case 'newnav':
            return $this->createNav($site, new VarContainer($_POST['nav']), $_POST['action']);
            
        case 'newpage':
            return !isset($_POST['page']) ? $this->ActionRedirect("{$this->touchpointType}_pages?ID=" . $site->getID()) : $this->returnCreatePage($site, $_POST['page'], $_POST['action']);
            
        case 'ajaxGetStats':
            return $this->getStats($site);
		case 'ajaxProspects':
			return $this->ajaxProspects($site, VarContainer::getContainer($_POST));
            
        // dashboards ...
        
            case 'dashboard':
                    return $this->ViewDashboard($site);
                
            case 'settings':
                    return $this->ViewSettings($site);
                    break;
            case 'theme':
                    return $this->ViewTheme($site);
                    break;
                
            case 'preview':
                    if (isset($_GET['theme']))
                         return $this->getSitePreview($site, (int)@$_GET['theme'], rtrim(@$_GET['path'], '/') . '/');
                    else return $this->getSitePreview($site);
                    break;
            
            case 'seo':
                    return $this->ViewSEO($site);
                    break;
                
        
            case 'social':
                    return $this->ViewSocial($site);
                    break;
                    // 
            case 'saveslide':
                return $this->doAjaxSaveSlide($_POST['slide'], $_GET);
            break;
            case 'save':
				if($_POST['website_theme_change']['changed'] == 'true' &&
					$this->doUpdateHubTheme($_GET['ID'],$_POST['website']['theme'],$this->getUser()->getID(),$_POST['website_theme_change']['owner'])){
					return $this->getAjaxView(array('error' => '', 'message' => 'Theme applied successfully!'));
				}elseif(isset($_POST['role_website'])){
					$this->doUpdateHubRole(key($_POST['role_website']['role']),$_GET['ID'],current($_POST['role_website']['role']));
					return $this->getAjaxView(array('error' => '', 'message' => 'Role modified sucessfully!'));
				}elseif(isset($_POST['website'])){
                    return $this->doAjaxSave($_POST['website'], $_GET['ID']);
				}
                else {
					throw new Exception('website is undeclared.');
				}
                
                break;
            
                    break;
            case 'editRegions':
                    return $this->ViewEditRegions($site);
                    break;

            case 'themeDesigner':
                return $this->ViewThemeDesigner($site);
                break;
                
            case 'createslide':
					$counter	= count($site->getSlidePhotos());
					$Slide		= $this->createSlide('Slide '.++$counter,$site);
                    return $Slide ? $this->Redirect($this->ActionUrl('Website_photos?ID=' . $site->id)  . '#slideanchor_' . $Slide->ID) : 
                                                   $this->Redirect($this->ActionUrl('Website_photos?ID=' . $site->id)  . '&maxPages=1');
                    break;
                    
            case 'deleteslide':
                    $result  =   $this->deleteSlide($site);
                    return $this->Redirect($this->ActionUrl('Website_photos?ID=' . $site->id));
                    break;
                
            case 'photos':
                    return $this->ViewPhotos($site);
                    break;
            case 'pages':
                    return $this->ViewPages($site, $site->getNodes(DBWhereExpression::Create('trashed = 0')));
                    break;
            case 'sortpages':
                    $result = $this->doSortPages($site);
                    return $this->Redirect($this->ActionUrl('Website_pages?ID=' . $site->id));
                    break;
            case 'loadDefault':
                    break;
      }
      
      return $this->ViewDashboard($site);
    }

	function doUpdateHubTheme($hub_id,$theme_id,$user_id, $owner_id){
		$DA = new WebsiteDataAccess();
		return $DA->UpdateHubTheme($hub_id,$theme_id,$user_id, $owner_id);
	}
	function doUpdateHubRole($role,$site_id,$flag){
		$DA = new WebsiteDataAccess();
		if($flag == 0){
			$DA->query("DELETE FROM 6q_roleperms WHERE object_ID = '$site_id' AND role_ID = '$role'");
		}else {
			$DA->query("INSERT INTO 6q_roleperms (role_ID,permission_ID,object_ID) VALUES('$role',(SELECT ID FROM 6q_permissions where flag = '" .MU_SITE_PERMISSION . "'),'$site_id')");
		}
	}
    function doAjaxSaveSlide($slidedata, $C)
    {
		$MM = new ModelManager();
		$slidedata['hub_id'] = $C['ID'];
		$slidedata['user_id'] = $this->getDataOwnerID();
        if(isset($slidedata['background']))
		    $slidedata['background']= str_replace("/".UserModel::USER_DATA_DIR,"",$slidedata['background']);
		$result = $MM->doSaveSlide($slidedata, $C['slide'], HubDataAccess::FromController($this));
		if($result instanceof ValidationStack){
			return $this->returnValidationError($result);
		}
        return $this->getAjaxView($result);
    }
    
    /**
     * 
     * @param type $object_vars
     * @param type $ID
     * @return type
     */
    function doAjaxSave($data, $ID)
    {
			$MM	=	new ModelManager();
			$data['touchpoint_type']	=	$this->touchpointType;
			$result	=	$MM->doSaveWebsite($data, $ID, HubDataAccess::FromController($this));
			
			if($result instanceof ValidationStack)
			{
				return $this->returnValidationError ($result);
			}
			
			return $this->getAjaxView($result);
			
//      
        $Validation =   new ValidationStack;
        $HM =   new WebsiteModel();
        $result =   array('error' => false);
        if(!$ID){
            // initialize some shit for a new object
        }else{
            $data['id']  =   $ID;
            unset($data['user_id']);    // security
        }
        
        if($HM->SaveData($data, NULL, $Validation, NULL, 
            DBWhereExpression::Create('id = %d AND user_id = dashboard_userid()', $ID), 
                $result)){
            if(isset($data['language'])){
                // on lang change
                $HM->Refresh()->onLangChange();
                $result['obj']  =   $HM;
            }
//                $result['row']  =   $this->getView('rows/leadform', false, false)
//                        ->init('leadform', $lf->Refresh())
//                        ->init('campaign_id', $Campaign->getID())
//                        ->toString();
                $result['message']  =   'New Website has been created.';
                return $this->getAjaxView($result);
        }
        // @todo test uploads
        return $this->returnValidationError($Validation);
        
        $object_vars    =   $data;
        // end
        $uploadsInfo =   new UploadsInfo();
        $keys   =   array_keys($object_vars);
        
        $errors =   new ValidationStack;
        $Validator  =   new Zend_Validate_NotEmpty();
        
        $uploadsInfo->accept('website', $errors, $this->User->getStoragePath('hub'));
        
//        $this->acceptUpload('website', $errors);
        
        if(!$uploadsInfo || !$uploadsInfo->hasFiles())
        {   
            foreach($object_vars as $key => $value)
            {
                switch($key)
                {
                    case 'bg_repeat':
                        $object_vars['bg_repeat']   =   $value == 1 ? 'repeat' : 'no-repeat';
                        break;
                    
                    case 'domain':
                        $urldomain    =   parse_url($value, PHP_URL_HOST);
                        if($urldomain === false ||  
                                !($ipaddr = filter_var(gethostbyname($urldomain), FILTER_VALIDATE_IP)))
                        {
                            $errors->addError($key, 'The domain url is invalid');
                        }
                        $object_vars[$key]  =   'http://' . $urldomain;
                        $object_vars['httphostkey'] =   $urldomain;
                        $result['message']      =   'Found Domain IP Address: ' . $ipaddr;
                        break;
                    
                    case 'keywords':    // special processing
                        $keys   =   explode(',', $value, 6);
                        // reset keyword1 .. keyword 6
    //                    $arr    =   array();
                        for($i = 1; $i <= 6; $i++)
                        {
                            $object_vars['keyword' . $i]    =   @$keys[$i-1];
                        }

                        unset($object_vars['keywords']);
                        continue;
                        break;                        
                }

                if(WebsiteModel::isRequired($key))/* && empty($value))
                {
                    $errors->addError($key, 'This field is required.');
                }   */
                {
                    if(!$Validator->isValid($value))
                    {
                        $errors->addErrors($key, $Validator->getMessages());
                    }
                }
            }
        }else{
            $result['uploads']      =   array();
            foreach($uploadsInfo->getFiles() as $key    =>  $file_info){
                $object_vars[$key]    =   $file_info['name'];   // filename$uploadsInfo->getFiles('logo', 'name');
                $result['uploads'][$key]    =   $this->User->getStorageUrl('hub', $object_vars[$key]);
            }            
//            $result['uploads']  =   array('logo' => );
        }
        
        $result['error']    =   $errors->getStatus();
        
        if(!$result['error'])
        {
            $this->q->Save(new WebsiteModel, $object_vars, 'id = %d AND user_id = dashboard_userid()', $ID);
        }else{
            $result['validation']   =   $errors->getErrors();
        }
        
//        sleep(2);
        // do validation!
        
        return $this->getAjaxView($result); //array('error' => $error_status));
    }
	
	function ajaxProspects(WebsiteModel $site, VarContainer $C){
		$C->timecontext = new PointsSetBuilder(LeadsVisitsChartData::getTimeContext('sitechart', '7-DAY'));
		$dataTableData  =   DataTablePreProcessor::RunIt($this, $C)
			->getProspects(NULL, $site->touchpoint_ID);
		return $this->getAjaxView($dataTableData);
//		$C->load(array('table' => 'ProspectsDashboardCampaign'));
//		$query = $site->queryProspects()->calc_found_rows();
//		$DT = new DataTableResult($query, $C);

//		$DT->setRowCompiler($this->getView('rows/Prospect', false, false), 'prospect');
//		return $this->getAjaxView($DT->getArray());
//		$cols = DataTableResult::getTableColumns('Prospects');
//		$keys = array_keys($cols);
//		return $this->getAjaxView(DataTableResult::returnDataTableStructure(ProspectDataAccess::FromController($this), 'getAccountProspects', array(
//				'limit_start' => max(0, (int)$C->getValue('iDisplayStart', 0)),
//				'limit' => min(100, (int)$C->getValue('iDisplayLength', 0)),
//				'sSearch' => $C->getValue('sSearch', ''),
//				'orderCol' => $cols[$keys[max(0, min(count($cols), (int)$C->getValue('iSortCol_0', 0)))]]['Name'],
//				'orderDir' => $C->getValue('sSortDir_0', 'ASC'),
//				'hub_id'  => $site->getID()
//			),
//			$this->getView('rows/Prospect', false, false), 'prospect', 'ProspectModel'));
	}
}
