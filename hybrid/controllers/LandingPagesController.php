<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
require_once QUBEADMIN . 'inc/user_permissions.class.php';        
require_once QUBEADMIN . 'models/CampaignModel.php';        
require_once QUBEADMIN . 'models/HubModel.php';        
require_once QUBEADMIN . 'models/WebsiteModel.php';        
require_once QUBEADMIN . 'models/LeadModel.php';        
require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class LandingPagesController extends DashboardBaseController
{

function ViewDashboard()
{
  $V	=	$this->getView('landingpages-dashboard');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->crumbs->addCrumbTxt('LandingPages_dashboard', "dashboard");
  return $V;
}

function ViewEditRegions()
{
  $V	=	$this->getView('landingpages-editRegions');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_editRegions', "edit regions");
  return $V;
}
function ViewPost()
{
  $V	=	$this->getView('landingpages-post');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_post', "post");
  return $V;
}
function ViewEdit()
{
  $V	=	$this->getView('landingpages-edit');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_edit', "edit");
  return $V;
}
function ViewSeo()
{
  $V	=	$this->getView('landingpages-seo');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_seo', "seo");
  return $V;
}
function ViewSettings()
{
  $V	=	$this->getView('landingpages-settings');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_settings', "Settings");
  return $V;
}

function ViewHome()
{
  $V	=	$this->getView('landingpages-home');
  $V->init('menu', 'landingPages-dashboard');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_home', "Home");
  return $V;
}

function ViewSocial()
{
  $V	=	$this->getView('landingpages-social');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_social', "Social");
  return $V;
}

function ViewTheme()
{
  $V	=	$this->getView('landingpages-theme');
  $V->init('menu', 'landingPages');
  $V->init('_title', 'Landing Pages');
  $this->getCrumbsRoot()->addCrumbTxt('LandingPages_theme', "Theme");
  return $V;
}

function prepareView(ThemeView &$view){

	parent::prepareView($view);

        $analytics  =   new DashboardAnalytics;
        $analytics->prepareHubTrackingIds('t.cid = @dashboard_campaignid');
        
        $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

        $view->init('ChartWidget', $ChartWidget);
            return $view;
}

    function ExecuteAction($action = NULL) {
        
        switch($action)
        {
		case 'dashboard':
		case 'editRegions':
		case 'post':
		case 'edit':
		case 'seo':
		case 'settings':
		case 'home':
		case 'social':
		case 'theme':
			$func	=	"View" . ucwords($action);
		return call_user_func(array($this, $func));	//$this-$func;

		break;		
        }        
    }
}
