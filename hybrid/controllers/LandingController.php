<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once HYBRID_PATH . 'controllers/BlogController.php';
 
class LandingController extends WebsiteController
{
	protected $touchpointType = 'LANDING';
}
