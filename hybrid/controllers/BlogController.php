<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';

 require_once QUBEADMIN . 'models/HubModel.php';             
 require_once QUBEADMIN . 'models/WebsiteModel.php';         
 require_once QUBEADMIN . 'models/BlogModel.php';        
 require_once HYBRID_PATH . 'models/SlideModel.php';       
 require_once HYBRID_PATH . 'controllers/CampaignController.php';
 
class BlogController extends DashboardBaseController
{
    
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()) {
        $active_classes[]   =   'campaign-nav';
        
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }
        
    function getBlog($blog_ID){
//        return $this->q->queryObjects('BlogModel')
//                ->Where('id = %d and (user_id = dashboard_userid() OR %d)',
//                            $blog_ID, $this->hasPermission('is_system'))
//                    ->addFields('CONCAT_WS(",", keyword1, keyword2, keyword3, keyword4, keyword5, keyword6) as keywords')
//                ->Exec()->fetch();
//#                ->addFie
        return BlogDataAccess::FromController($this)->getBlog($blog_ID);
    }
	
    /**
     * 
     * @param type $post_ID
     * @return BlogPostModel
     */
    function getPost($post_ID){
        $where = '';
        if(!$this->getUser()->isSystem()){
            $where = 'user_id = %d';
            if($this->getUser()->can('view_BLOG'))
                $where .= ' OR EXISTS (SELECT 1 FROM blogs WHERE id = T.blog_id AND user_parent_id = %d)';

        }
        if(!empty($where)){
            $where = "AND ($where)";
        }
        return $this->q->queryObjects('BlogPostModel')->Where("id = %d $where", $post_ID, $this->getUser()->getID(), $this->getUser()->getParentID())->Exec()->fetch();
    }
    
    /**
     * Add a Site (or blog) list after the last crumb
     * 
     * @param type $action_fmt
     * @param type $current_ID
     * @param type $cid
     * @return \BreadCrumb
     */
    function getCrumbsBlog($action_fmt, $current_ID, $cid)
    {
//        $sites  =   $this->q->queryObjects('BlogModel')->Where('cid = %d AND (user_id = dashboard_userid() OR %d)',
//                $cid, $this->hasPermission('is_system'))
//                        ->Fields('id, blog_title')->orderBy('id = ' . $current_ID . ' DESC')->Exec()->fetchAll(PDO::FETCH_ASSOC);
        $BDA = BlogDataAccess::FromController($this);
        $sites =$BDA->getBlogs($cid, array('orderBy' => array('id = :currentID'), 'bindParams' => array('currentID' => $current_ID)));
        $bread  =   NULL;
        foreach($sites as $site)
        {
            $temp   =   new BreadCrumb(sprintf($action_fmt, $site->getID()), $site->blog_title);
            if(is_null($bread))
                $bread  =   $temp;
            else
                $bread->addChild ($temp);
        }
        return $bread;
        
    }

    /* @FER: THIS IS HARDCORE SHIT & IT'S PRETTY AWESOME */
    function addBlogCrumbs(BlogModel $blog,$action){
		$CDA		= \Qube\Hybrid\DataAccess\CampaignDataAccess::FromController($this);
		$CC			= new CampaignController();
		$c_id		= $blog->cid;
		$c_name		= $CDA->getCampaign($c_id)->name;
		$b_id		= $blog->id;
		$b_name		= $blog->blog_title;
		$b_type		= 'BLOG';
		$b_label1	= $CC->getTouchPointTypes($b_type);
		$b_label2	= $action == 'seo' ? 'SEO' : ($action == 'editregions' ? 'Regions' : ucfirst($action));
		$b_ctrl		= ucfirst(mb_strtolower($b_type));

		if($c_id == 99999){
			return parent::getCrumbsRoot(false,false,null,null)->addCrumbTxt($b_ctrl.'_'.$action.'?ID='.$b_id,$b_label2.': '.$b_name);
		} else{
			return parent::getCrumbsRoot(true,true,$c_id,$c_name)->addCrumbTxt('Campaign_dashboard?ID='.$c_id.'&pointtype='.$b_type,$b_label1[1])->addCrumbTxt($b_ctrl.'_'.$action.'?ID='.$b_id,$b_label2.': '.$b_name);
		}
	}

	//view individual website
	function ViewDashboard(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
#            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('website_social?ID=' . $this->site->id, 'SDashboard');
		$Chart  =   new LeadsVisitsChartData('sitechart', '7-DAY', false, array('id' => $blog->id, 'controller' => $this, 'touchpointType' => strtolower('BLOG')));
			$timevars   =   VarContainer::getContainer($Chart->getConfig());
			$timecontext = new PointsSetBuilder($timevars, $this->getUser()->created);
			$leads = ProspectDataAccess::FromController($this)->getAccountProspects(array(
				'calc_found_rows' => 1,
				'limit' => 10,
				'touchpoint_ID' => $blog->touchpoint_ID,
				'timecontext' => $timecontext
			));
            $totalProspects = $this->query('SELECT FOUND_ROWS()')->fetchColumn();
            $analytics  =   new DashboardAnalytics;
            $analytics->prepareHubTrackingIds('t.id = ' . (int)$blog->id, 'BlogModel');
            
            $V  = $this->getPage('website-dashboard', 'Blog Dashboard', 'website');
            $this->addBlogCrumbs($blog,'dashboard');
            
//            $ChartWidget    = ChartWidget::fromCookie('web', 20, 'DAY')->loadData($analytics);

//            $V->init('ChartWidget', $ChartWidget);

        $Chart = new LeadsVisitsChartData('sitechart', '7-DAY', false, array('id' => $blog->id,
			'touchpointType' => 'blog',
            'controller' => $this));
        $timevars   =   VarContainer::getContainer($Chart->getConfig());
        $timecontext = new PointsSetBuilder($timevars, $this->getUser()->created);
            $args	=	array(
                'timecontext' => $timecontext,
                'campaign_id' => $blog->cid,
                'tbl' => 'blog',
                'tbl_ID' => $blog->id
            );

            $topPerformingKeywords	= PiwikReportsDataAccess::FromController($this)->getTopPerformingKeywords($args);
            $V->init('TopPerformingKeywords', $topPerformingKeywords);
			$V->init('showDashboardRange', true);
            $V->init('chart', $Chart);
            $V->init('site', $blog);
            $V->init('blog', $blog);
            $V->init('prospects', $leads);
            $V->init('totalProspects', $totalProspects);
            $V->init('campaign_id', $blog->cid);
//            $V->init('kwranks', Rankings::getTopRankings(new PointsSetBuilder(new VarContainer($Chart->getConfig()))));
        $V->init('kwranks', RankingDataAccess::FromController($this)->
        getTopRankingKeywordsBySearchEngine($timecontext, $this, array('campaign_ID' => $blog->cid,
                'TOUCHPOINT_TYPE' => 'BLOG', 'touchpoint_ID' => $blog->touchpoint_ID)));
            $V->init('_title', $blog->name);
            $V->init('menu','campaign');/* $menu = 'campaign' */
            $V->init('touchpoint_type','blog');
            
            return $V;
            //return $this->getView('campaign-dashboard');
	}
	
	//view Settings
	function ViewSettings(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
		$this->addBlogCrumbs($blog,'settings');

#            $this->getCrumbsRoot(true,true,null,null)->addCrumbTxt('blog_settings?ID=' . $blog->id, 'Settings');

		$V  = $this->getPage('blog-settings','Blog Settings','campaign');/* $menu = 'campaign' */
        $WDA = WebsiteDataAccess::FromController($this);
        $site = $WDA->GetSites($blog->cid,array('where' => array('id' => $blog->connect_hub_id), 'returnSingleObj' => true));
		$V->init('blog', $blog);
        $V->init('sites', $site);
		if($this->getUser()->can('is_system')){
			$DA = new UserDataAccess();
			$blogUser = $DA->getUser($blog->user_id);
			$V->init('storagePath', $blogUser->getStorageUrl());
		}
        $V->init('touchpoint_type','blog');

		return $V;
	}

    function ajaxSites(BlogModel $blog, VarContainer $params){
        $WDA = WebsiteDataAccess::FromController($this);
        $sites = $WDA->GetSites($blog->cid,array('sSearch' => $params->getValue('search', false)));
        return $this->getAjaxView($sites);
    }
	
	//view Theme
	function ViewTheme(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
		$this->addBlogCrumbs($blog,'theme');
		$V  = $this->getPage('blog-theme','Blog Theme','campaign');/* $menu = 'campaign' */
		$V->init('blog', $blog);
        $hda = HubDataAccess::FromController($this);
        $HubThemes	=	$hda->getHubThemes($blog->theme, 'BLOG');
        if($blog->connect_hub_id != 0){
            $connected_site = WebsiteDataAccess::FromController($this)->getSite($blog->connect_hub_id, 'BLOG');
            $MUTheme = $connected_site->theme;
            $HubThemes[]	=	$hda->getHubSingleTheme($MUTheme);
            $HubThemes[count($HubThemes)-1]->name =$HubThemes[count($HubThemes)-1]->name . "*";
        }
		$siteTheme = BlogDataAccess::findSiteTheme($blog, $HubThemes);
		$V->init('sitetheme', $siteTheme);
        $V->init('hubthemes', $HubThemes);
		if($this->getUser()->can('is_system')){
			$DA = new UserDataAccess();
			$blogUser = $DA->getUser($blog->user_id);
			$V->init('storagePath', $blogUser->getStorageUrl());
		}
        $V->init('touchpoint_type','blog');

		return $V;
	}
	
	//view SEO
	function ViewSEO(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
        $this->addBlogCrumbs($blog,'seo');
        $V  = $this->getPage('blog-seo','Blog SEO','campaign');/* $menu = 'campaign' */
        $V->init('blog', $blog);
        $V->init('touchpoint_type','blog');

	  	return $V;
	}
	
	//view Edit Regions
	function ViewEditRegions(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
        $this->addBlogCrumbs($blog,'editregions');

        /* @var $themeModel HubThemeModel */
        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $blog->theme)->Exec()->fetch();
        $fields = $themeModel->getBlogEditFields($blog);
		$V  = $this->getPage('blog-editregions','edit regions','campaign');/* $menu = 'campaign' */
			$V->init('blog', $blog);
        $V->init('fields', $fields);
        $V->init('blogTheme', $themeModel);
        $V->init('touchpoint_type','blog');

	  	return $V;
	}
	
	//view Social
	function ViewSocial(BlogModel $blog){/* @FER: BREADCRUMBS FIXED */
        $this->addBlogCrumbs($blog,'social');
        $V  = $this->getPage('blog-social','Blog Social','campaign');/* $menu = 'campaign' */
        $V->init('blog', $blog);
        $V->init('touchpoint_type','blog');

	  	return $V;
	}

	
	//view Pages
	function ViewPosts(BlogModel $blog,$category = null){/* @FER: BREADCRUMBS FIXED */
		//$categories =   $this->query('SELECT DISTINCT category FROM %s WHERE blog_id = %d AND user_id = %d and trashed = 0',
		//			'BlogPostModel', $blog->id, $this->user_ID)->fetchAll(PDO::FETCH_OBJ);

		$DA = new BlogDataAccess($this->getQube());
        $categories = $DA->getBlogCategories($blog);
        $default_sort = DataTableResult::getDefaultColumn('blog-posts');
        $posts = $DA->getPosts($blog, array('order' => $default_sort['Name'] . ' ' . $default_sort['Sort'], 'limit' => 10));
		$totalPosts = $this->query('SELECT FOUND_ROWS()')->fetchColumn();
		$UDA = new UserDataAccess();
		$feeds = $DA->getFeedsWithSubscription($UDA->getUser($blog->user_id)->getParentID(), $blog->getID());
//            $post_sel   =   $this->q->queryObjects('BlogPostModel')   `
//                        ->Where('blog_id = %d AND user_id = %d and trashed = 0', $blog->id,
//                                    $this->user_ID);
//
//            $posts_result   =   null;
//            if($category){
//                $posts_result    =   $post_sel->Where('category = ?')->Prepare();
//                $posts_result->execute(array($category));
//            }else{
//                $posts_result       =   $post_sel->Exec();
//            }

		$this->addBlogCrumbs($blog,'posts');

		$V  = $this->getPage('blog-posts','Blog Posts','campaign');/* $menu = 'campaign' */

        $V->init('feeds', $feeds);
		$V->init('categories', $categories);
		$V->init('posts', $posts);
		$V->init('blog', $blog);
		$V->init('totalPosts', $totalPosts);
        $V->init('touchpoint_type','blog');

		return $V;
	}
    
    // @amado @todo DELETE ENTIRE FUNCTION - only for testing
    function ViewPost2(BlogModel $blog){
        
        $V  = $this->getPage('blog-post','Blog Post: SAMPLE','campaign');/* $menu = 'campaign' */
                
        $V->init('blog', $blog);
                        
	  	return $V;
    }
        
    //view Pages
	function ViewPost(BlogModel $blog,BlogPostModel $post){/* @FER: BREADCRUMBS FIXED */
        $this->addBlogCrumbs($blog,'posts')->addCrumbTxt('Blog_post?ID='.$blog->id.'&post='.$post->id,'Post: '.$post->post_title);

        $themeModel	=	$this->q->queryObjects('HubThemeModel', 'T', 'id = %d', $blog->theme)->Exec()->fetch();

        $BDA = BlogDataAccess::FromController($this);
		$V  = $this->getPage('blog-post','Blog Post: '.htmlentities($post->post_title),'campaign');/* $menu = 'campaign' */
        $V->init('categories', $BDA->getBlogCategories($blog));
		$V->init('blog', $blog);
        $V->init('post', $post);
        $V->init('blogTheme', $themeModel);
        $V->init('touchpoint_type','blog');

	  	return $V;
	}
        
    function ExecuteAction($action = NULL) {
        require_once QUBEADMIN . 'models/CampaignModel.php';
        require_once QUBEADMIN . 'models/LeadModel.php';

        if(isset($_GET['ID']) && $_GET['ID'] != ''){
            $blog   =   $this->getBlog($_GET['ID']);
            if(!$blog) die('Error');
            $Page   =   $this->doAction($blog, $action);

            return $Page;
        }else{
            switch($action){
            case 'save':
                if(isset($_POST['post']) && $post = $this->getPost($_GET['post'])){
                    return $this->doAjaxSave($post, 'BlogPostModel', $_POST['post']);
                }


            }
        }
    }
     
    function doAction(BlogModel $blog, $action){
        if($deny = $this->Protect("view_BLOG", 'Auth', $blog->getID()))
            return $deny;
    //load view
    switch($action)
    {
        
        // dashboards ...

        case 'dashboard':
                return $this->ViewDashboard($blog);

        case 'ajaxSites':
            return $this->ajaxSites($blog, VarContainer::getContainer($_POST));
        case 'settings':
                return $this->ViewSettings($blog);
                break;

        case 'theme':
            return $this->ViewTheme($blog);
                break;

        case 'seo':
                return $this->ViewSEO($blog);
                break;

        case 'editregions':
                return $this->ViewEditRegions($blog);
                break;
        case 'social':
                return $this->ViewSocial($blog);
                break;

        case 'posts':
                return $this->ViewPosts($blog, isset($_GET['category']) ? $_GET['category'] : null);
                break;               
            
        case 'post':                
            $post   =   $this->getPost($_GET['post']);
            
            if($post){
                return $this->ViewPost($blog, $post);
                break;               
            }
            else{ //@amado @todo REMOVE ELSE - only used for testing since no blog post are in DB
                return $this->ViewPost2($blog);
                break;
            }
        
        // save!
        case 'save':
            if(isset($_POST['blog']))
                return $this->doAjaxSave($blog, 'BlogModel', $_POST['blog']);
            if(isset($_POST['post']) && $post = $this->getPost($_GET['post']))
                return $this->doAjaxSave($post, 'BlogPostModel', $_POST['post']);
            else
                throw new Exception('website is undeclared.');
            break;
            
        case 'saveslide':
            return $this->doAjaxSaveSlide($_POST['slide'], $_GET['slide']);
        break;
                    break;  //@amado @todo why is there two break;s here?
                
            case 'createslide':
                    $Slide  =   $this->createSlide();
                    return $this->Redirect($this->ActionUrl('Website_photos?ID=' . $this->site->id)  . '#slideanchor_' . $Slide->ID);
                    break;

		case 'createpost':
			return $this->createPost($blog, $_POST['post']);
		break;
		case 'trashpost':
		    return $this->ajaxDelete(VarContainer::getContainer($_GET));
		    break;
        case 'photos':
                return $this->ViewPhotos();
                break;
        case 'loadDefault':
                break;
		case 'ajaxPosts':
			return $this->ajaxPosts($blog, VarContainer::getContainer($_POST));
        case 'ajaxSubscribeFeeds':
                //error_log(json_encode($blog) . "\r\n" . json_encode($_POST['blogfeed']), 3, "errorlog_feeds.log");
                return $this->ajaxSubscribeFeeds($blog, $_POST['blogfeed']);
                break;
        case 'ajaxProspects':
            return $this->ajaxProspects($blog, VarContainer::getContainer($_POST));
      }
      
      return $this->Dashboard($_GET['ID']);
    }
        
    /**
     * 
     * @param type $arrkey
     * @param ValidationStack $errors
     * @param type $uploadDir
     * @return \UploadsInfo|boolean returns false if no files were uploaded, returns UploadsInfo otherwise (and populates validationstack)
     */
    function acceptUpload($arrkey, ValidationStack $errors, $uploadDir   =   'hub')
    {
        $upload = new Zend_File_Transfer();
        $files = $upload->getFileInfo();
        
        $uploadsInfo    =   new UploadsInfo;
        
        $result =   array();
        
        if(!empty($files))
        {
//            $this->findReseller();
//            ini_set('display_errors', false);
            
            // @todo change. this is dependent on some weird shit, see upload_file.php:23
            $destination    =   $this->User->getStoragePath($uploadDir);
            
            $upload->addValidator('IsImage', true);
            $upload->addValidator('NotExists', false, '.');
            $upload->setDestination($destination);
            
            foreach($files as $fkey => $file)
            {
                preg_match('/^' . $arrkey . '_(.*?)_$/', $fkey, $matches);
                $key    =  $matches[1];
                if(!$upload->isValid($fkey))
                {
//                    var_dump($matches, $fkey);
                    $errors->addErrors($key, $upload->getMessages());
                    $error_status   =   'validation';
                }else{
                // rename jpeg to jpg extensions
    //                if(preg_match('/jpeg$/i', $file['name']))                {
                        $newname    =   $destination . '/' . preg_replace(array('/[^\w\.-_]+/', '/jpeg$/'), array('_', 'jpg'), $file['name']);
                        $upload->addFilter('Rename', array('target' => $newname), $fkey);
    //                }

                    if(!$upload->receive($fkey))
                    {
                        $errors->addErrors($key, $upload->getMessages());
                    }  else {
    //                    echo 'fileinfo:'; var_dump($upload->getFileInfo($fkey), $fkey);
                        $uploadsInfo->accept($key, $upload->getFileInfo($fkey));
    //                    if(!is_array($result['uploads']))   $result['uploads']  =   array();
      //                  $result['uploads'][$key]   =   '/' . $uploadDir .'/' . $object_vars[$key];  //$this->ResellerData->getUrl($destination, $object_vars[$key]);
                    }
                }
            }
            //ini_set('display_errors', true);
            
            return $uploadsInfo;
        }
        
        return false;
    }

    function doAjaxSave(HybridModel $obj, $class, $object_vars)
    {
//        require_once HYBRID_LIB . 'Zend/Validate.php';
        
        $keys   =   array_keys($object_vars);
        
        $errors =   new ValidationStack;

        $Validator  =   new Zend_Validate_NotEmpty();
        
        $uploadsInfo  =   $this->acceptUpload('blog', $errors);
        
        if(!$uploadsInfo || !$uploadsInfo->hasFiles())
        {
            foreach($object_vars as $key => $value)
            {
                switch($key)
                {
                    case 'keywords':    // special processing
                        $keys   =   explode(',', $value, 6);
                        // reset keyword1 .. keyword 6
    //                    $arr    =   array();
                        for($i = 1; $i <= count($keys); $i++)
                        {
                            $object_vars['keyword' . $i]    =   $keys[$i-1];
                        }

                        for( ; $i <= 6; $i++){
                            $object_vars['keyword' . $i]    =   "";
                        }

                        unset($object_vars['keywords']);
                        continue;
                        break;
                }

                if(WebsiteModel::isRequired($key) && !empty($value))
                /*{
                    $errors->addError($key, 'This field is required.');
                }   */
                {
                    if(!$Validator->isValid($value))
                    {
                        $errors->addErrors($key, $Validator->getMessages());
                    }
                }
            }
        }
        
        $result['error']    =   $errors->getStatus();
        
        if(!$result['error'])
        {
            $MM = new ModelManager();
            switch($class){
                case 'BlogPostModel':
                    $object_vars['blog_id'] = $obj->blog_id;
                    $valid=$MM->doSaveBlogPost($object_vars, $obj->id, BlogDataAccess::FromController($this));
                    break;
                case 'BlogModel':
                    $valid = $MM->doSaveBlog($object_vars, $obj->id, BlogDataAccess::FromController($this));
                    break;
                default:
                    $this->q->Save(new $class, $object_vars, 'id = %d AND user_id = dashboard_userid()', $obj->id);
                    break;
            }
        }

        if($errors->hasErrors())
            return $this->returnValidationError($errors);

        if($valid instanceof ValidationStack){
            return $this->returnValidationError($valid);
        }

        // sleep(2);
        // do validation!
        return $this->getAjaxView($result); //array('error' => $error_status));
    }

	function ajaxPosts(BlogModel $blog, VarContainer $C){
		$DA = new BlogDataAccess();
		$DA->setActionUser($this->getUser());
		$rowTemplate = $this->getView('rows/blog-posts-row', false, false);
		$rowTemplate->init('blog', $blog);
		return $this->getAjaxView($DA->getDatatablePosts($blog, $C, $rowTemplate));
	}

	function createPost(BlogModel $blog, $data){
        $DA = new BlogDataAccess();
		$DA->setActionUser($this->getUser());
		$MM = new ModelManager();
		$data['blog_id'] = $blog->getID();
		$data['user_id'] = $blog->user_id;
		if(isset($data['category']) && $data['category'] == '_new')
			$data['category'] = $data['category_text'];
		$result = $MM->doSaveBlogPost($data, 0, $DA);

		if($result instanceof ValidationStack){
			return $this->returnValidationError($result);
		}

        $categories = $DA->getBlogCategories($blog);
        $result['blog_categories'] = array_keys($categories);

		return $this->getAjaxView($result);
	}
	
	function deletePost(BlogModel $blog, $post)
	{

        $this->query('UPDATE %s SET trashed = NOW() WHERE user_ID = dashboard_userid() AND id = %d', 'BlogPostModel', $post);
        return $this->ActionRedirect('Blog_posts?ID=' . $blog->id);
	}

    function ajaxSubscribeFeeds(BlogModel $blog, $blogfeed)
    {
        $BDA = BlogDataAccess::FromController($this);
        $feeds_ids = array_keys($blogfeed);
        //error_log("Feeds IDS: " . json_encode($feeds_ids) . "\r\n", 3, "errorlog_feeds.log");
        
        if($blogfeed[$feeds_ids[0]] == 1){
            //error_log("Add Blog Sub\r\n", 3, "errorlog_feeds.log");
            $BDA->addBlogSubscription($blog->getID(), $feeds_ids[0]);
            
        } else {
            //error_log("Remove Blog Sub\r\n", 3, "errorlog_feeds.log");
            $BDA->removeBlogSubscription($blog->getID(), $feeds_ids[0]);
            
        }
//        $BDA->setBlogSubscriptions($blog->getID(), $blogfeed);

        return $this->getAjaxView((object)array('error' => false));
    }

    function ajaxProspects(BlogModel $blog, VarContainer $C){
		$dataTableData = DataTablePreProcessor::RunIt($this, $C)
			->getProspects(NULL, $blog->touchpoint_ID);
		return $this->getAjaxView($dataTableData);
//        $cols = DataTableResult::getTableColumns('Prospects');
//        $keys = array_keys($cols);
//        return $this->getAjaxView(DataTableResult::returnDataTableStructure(ProspectDataAccess::FromController($this), 'getAccountProspects', array(
//                'limit_start' => max(0, (int)$C->getValue('iDisplayStart', 0)),
//                'limit' => min(100, (int)$C->getValue('iDisplayLength', 0)),
//                'sSearch' => $C->getValue('sSearch', ''),
//                'orderCol' => $cols[$keys[max(0, min(count($cols), (int)$C->getValue('iSortCol_0', 0)))]]['Name'],
//                'orderDir' => $C->getValue('sSortDir_0', 'ASC'),
//                'hub_id'  => $blog->getID()
//            ),
//            $this->getView('rows/Prospect', false, false), 'prospect', 'ProspectModel'));
    }

    function ajaxDelete(VarContainer $params){

        $TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
        $result = $TrashMgr->TrashByParam($params, $message, true);
        return $this->getAjaxView(array('error' => $result ? 0 : 1, 'message' => $message));
    }
}
