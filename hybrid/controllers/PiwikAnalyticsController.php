<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PiwikAnalytics
 *
 * @author amado
 */
class PiwikAnalyticsController extends DashboardBaseController {

    /**
     * 
     * @return PDOStatement
     */
    function xquery() {
        $q = Qube::Start();
        $args = func_get_args();
        return call_user_func_array(array($q, 'query'), $args);
    }

		/**
		 * Return the Top Performing Keywords.
		 * 
		 * @param VarContainer $C
		 * @return type
		 */
    function getKeywords(VarContainer $C) {
#		var_dump($C);
//        $offset = (int) $C->getValue('offset', 0);
        $length = 50;
        
        $cid    =   $C->getValue('ID');

                $context = new VarContainer();
                $context->interval = intval($_POST['interval']);
                $context->period = $_POST['period'];
				$args	=	array('timecontext' =>
							new PointsSetBuilder($context, $this->getUser()->created)
						);

				if($cid)
					$args['campaign_id']	=	$cid;
                if(isset($_GET['pointtype']))
                    $args['touchpoint_type'] = $_GET['pointtype'];
				
				$kwlist	= PiwikReportsDataAccess::FromController($this)->getTopPerformingKeywords($args);
				
        $rowhtml = array();
        foreach ($kwlist as $keyword => $count) {
            $rowhtml[] =
                            $this->getView('rows/keyword', false, false)
                            ->init('keyword', $keyword)
                            ->init('count', $count)->toString();
        }

        return array(
            'next' => false, //$nextoffset > $totalids ? false : $nextoffset,
            'total' => 5,	//(int) $totalids,
//	'ids' => $trackingids, 
            'length' => $length,
            'count' => count($kwlist),
            'rows' => implode("\n", $rowhtml),
            'keywords' => $kwlist);
    }

    function getKeywordsFromIds($ids, $date = 'last2', $period = 'week') {
#	echo "$date\n$period\n";
        set_time_limit(0);
        $request = $this->getPiwikApiRequest('
                                method=Referers.getKeywords
                                &idSite=' . $ids . '
                                &date=' . $date . '
                                &period=' . $period . '
                                &format=php&serialize=0
                                ');

        return $request->process();
    }

    function getAllKeywords() {
        $dt = $this->getKeywordsFromIds($this->getAllTrackingIds());

        return $this->TrimKeywords($dt, 10);

        $kw = array();
        foreach ($dt as $trackingid => $keywords) {
            foreach ($keywords as $kwi) {
                $kw[$kwi['label']] += $kwi['nb_visits'];
            }
        }
        arsort($kw);

        return $kw;

        var_dump($kw, $dt);

        $request = $this->getPiwikApiRequest('
                                method=Referers.getKeywords
                                &idSite=' . $ids . '
                                &date=today
                                &period=week
                                &format=php&serialize=0
                                &filter_limit=10');
        $dt = $request->process();

        var_dump($dt);
#        var_dump($request->process());
    }

    function flatRender($dt) {
        $renderer = new Piwik_DataTable_Renderer_Php();
        $renderer->setRenderSubTables(true);
        $renderer->setSerialize(false);
        $renderer->setTable($dt);
        $renderer->amado = 1; // enable patch

        $raw = $renderer->flatRender();

        return $raw;
    }

    /**
     * 
     * @return Piwik_API_Request
     */
    function getPiwikApiRequest($str) {

        $piwikpath = Qube::get_settings('PIWIK_PATH');
        define('PIWIK_INCLUDE_PATH', $piwikpath ? $piwikpath : realpath(HYBRID_PATH . '../../../piwik/public_html/'));

        define('PIWIK_USER_PATH', PIWIK_INCLUDE_PATH);
        define('PIWIK_ENABLE_DISPATCH', false);
        define('PIWIK_ENABLE_ERROR_HANDLER', false);
        define('PIWIK_ENABLE_SESSION_START', false);

        require_once PIWIK_INCLUDE_PATH . "/index.php";
        require_once PIWIK_INCLUDE_PATH . "/core/API/Request.php";
        require_ONCE PIWIK_INCLUDE_PATH . '/core/DataTable/Renderer/Php.php';
        Piwik_FrontController::getInstance()->init();


        $request = new Piwik_API_Request($str . '
                                &token_auth=b00ce9652e919198e1c4daf7bf5aed5a
        ');
        return $request;
    }

    function getChartPoints_api(VarContainer $v, &$data) {
        $interval = max(1, (int) $v->interval);
        $period = strtolower(preg_match('/^(DAY|WEEK|MONTH|YEAR)$/', $v->period) ? $v->period : 'DAY');

        $ids = $this->getAllTrackingIds();
#        $ids .= ',' . '5982,732';
        $request = $this->getPiwikApiRequest('
                                method=VisitsSummary.getVisits
                                &idSite=' . $ids . '
                                &date=last' . $interval . '
                                &period=' . $period . '
                                &format=php&serialize=0
                                &filter_limit=10');
        $dt = $request->process();

        if (@$dt['result'] == 'error') {
            $dt['error'] = $dt['message'];
            $data = $dt;
            return false;
        }
//        $dtm    =   $dt->mergeChildren();
//        $dtm2   =   $dtm->mergeChildren();
//            echo $dtm; exit;
        $MyDataTable = new Piwik_DataTable;
//        Header('Content-type: text/html', true);
        foreach ($dt as $trackingid => $tbls) {
            /**
             * This part is tricky. $tbls is an DataTable where each Row is a DataTable such as: [(DataTable_array for period A1 - A2), (DataTable_array for period B1 - B2), ... ]
             * calling MergeChildren will convert the datatable_array into Rows.. with columns such as: [ [ Label: A1 - A2, Value], [Label: B1 - B2, Value], ...]
             */
            $merged = $tbls->mergeChildren();
            $MyDataTable->addDataTable($merged);
        }
//echo '<pre>';        var_dump($dt);	echo $MyDataTable;
        $stamps = array();
        foreach ($MyDataTable->getRows() as $i => $Row) {
            $range_str = $Row->getColumn('label');
            $timestamp = strtotime($range_str);
            $value = $Row->getColumn(0);
            $row = compact('value', 'timestamp', 'range_str');
            $stamps[] = $timestamp;
            $data[] = $row;

//            $Row->setColumn('value', $Row->getColumn(0));
//            $Row->setColumn('timestamp', strtotime($range_str)); //reset($dt)->metadata[$range_str]['timestamp']);            
        }
        array_multisort($stamps, $data);
//        $data =   $this->flatRender($MyDataTable);
        return true;
    }

    function getLeadsVisitsPoints__depr(VarContainer $c, &$result) {

        $PointCreator = new PointsSetBuilder($c, $this->User->created);
//        $result =   array();
        if (!$this->getChartPoints($PointCreator, $result[0]))
            return false;
        if (!$this->getLeadsPoints($PointCreator, $result[1]))
            return false;

        $result[0] = array('label' => 'Visits', 'data' => $result[0]);

        return true;
    }

    function getVisits($trackingids) {

        $request = $this->getPiwikApiRequest('
                    method=VisitsSummary.getVisits
                    &idSite=' . $trackingids . '
                    &date=last2
                    &period=week
                    &format=original
                    &filter_limit=10');
        $dt = $request->process();

        return $this->flatRender($dt);
    }

		function getChart(VarContainer $vc)
		{
			if (!$vc->chart)
					throw new Exception();

			$config = $vc->getData();
			unset($config['chart']);

			$config['controller'] = $this;
			$config['ActionUser']	=	$this->getUser();
			
			$chart = new $vc->chart('chartconfig', NULL, false, $config);
			return $chart;
		}
		
		function getChartData(VarContainer $vc)
		{
			$chart = $this->getChart($vc);
			if ($chart->data === false)
					$chart->loadData(true);
			// refresh the cache
			return $chart->data;
		}

	/**
	 *
	 * @param VarContainer $vc
	 * @return ExportableTable
	 * @throws QubeException
	 */
    function getDataTableResult(VarContainer $vc) {
			$vc->ActionUser	=	$this->getUser();

			switch($vc->chart)
			{
				case 'CampaignTouchPoints':
					$ctr	=	new ExportableTableContainer();
					$ctr->setExportable(TouchPointQuery::FromController($this));
                                        
                                        $cc_user_id = $this->getUser()->getID();
                                        $cc_is_reports_only = $this->getUser()->can('reports_only');

					$ctr->setArgs(array(
						'campaign_id'	=> $vc->cid,
						'timecontext' => new PointsSetBuilder(CampaignController::getChartTimeContext($cc_user_id, $cc_is_reports_only), $this->getUser()->created),
						'rowVar' => 'site',
						'rowCompiler' => $this->getView('rows/website-stats-export', false, false)
					));
					return $ctr;
				case 'TableCompareChartsData':
                    $table = new DataTableCampaignsResult($vc);
                    $ctr = new ExportableTableContainer();

                    $ctr->setExportable($table);
                    return $ctr;

                case 'RankingSites':
                    $table = new DataAccessTableResult(RankingDataAccess::FromController($this), $vc, 'RankingSites', 'getAllRankingData', 'getAllRankingCount');
					$ctr = new ExportableTableContainer();

                    $ctr->setExportable($table);
                    $ctr->setArgs(array('FETCHMODE' => PDO::FETCH_OBJ, 'user_id' => $this->User->getID(), 'campaign_id' => $vc->cid, 'limit' => 5000));
                    return $ctr;
                case 'RankingKeywords':
                    $table = new DataAccessTableResult(RankingDataAccess::FromController($this), $vc, 'RankingKeywords', 'getAllKeywordData', 'getAllKeywordCount');
                    $ctr = new ExportableTableContainer();

                    $ctr->setExportable($table);
                    //$ctr->setArgs($vc->getData());
					//$ctr->setArgs(array('limit' => 5000, 'period' => 'DAY', 'interval' => 7));
					$ctr->setArgs($vc->getData());
                    return $ctr;
			}
			return NULL;
    }

	function getKeywordRankings(VarContainer $vc){
		$timesettings	=	new PointsSetBuilder($vc);

        $args = array();
        if($vc->getValue('campaign_ID', NULL))
        {
            $args['campaign_ID']    =   $vc->campaign_ID;
        }
        if($vc->checkValue('TOUCHPOINT_TYPE', $tp) && trim($tp) != '')
        {
            $args['TOUCHPOINT_TYPE']    =   $tp;
        }
//        $args['TOUCHPOINT_TYPE']    =   'BLOG';
		return RankingDataAccess::getTopRankingKeywordsBySearchEngine($timesettings, $this, $args);
	}

    function getCSVData(VarContainer $vc) {
        $data = $this->getDataTableResult($vc);
        return new CSVExportView($data);
    }

    function ExecuteAction($action = NULL) {
        switch ($action){
            case 'ajaxCampaigns':
                return $this->getAjaxView($this->CampaignsDataTable(VarContainer::getContainer($_POST)));
        }
        if (strpos($action, 'ajax') === 0) {
            $action = lcfirst(preg_replace('/^ajax/', '', $action));
            if (method_exists($this, $action)) {
                $vc = new VarContainer;
//var_dump($vc, $_POST);

//                $vc->load($_GET);
                $vc->load($_POST);

//                $data   =   array();
                $data = $this->$action($vc);
                /*
                  $data   =   $this->checkFnCache($action, $vc);

                  if($data === false)
                  {
                  if($result)
                  $this->cacheSave($data);
                  }
                 */
                Header('Content-Type: application/json', true);
                return $this->returnAjaxData('pw', $data, array('params' => $vc->getData()));
            }
        }
        switch ($action) {
            case 'getCSVData':
                $vc = new VarContainer($_GET);
                return $this->getCSVData($vc);
                break;
        }
    }
    
    function CampaignsDataTable(VarContainer $vc){
				$vc->ActionUser	=	$this->getUser();
				$vc->cid	=	0;
				$vc->isalltime = $dateMagic->isalltime;
				
        $table = new DataTableCampaignsResult($vc);
        $rowTemplate = $this->getView('rows/campaign-stats', false, false);
        $rowTemplate->init('User', $this->getUser());
        $table->setRowCompiler($rowTemplate, 'camp');
				
#        $args['user_id'] = $this->getUser()->getParentID();
        return $table->getArray();
    }
}