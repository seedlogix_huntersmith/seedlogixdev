<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';        
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class TemporaryController extends DashboardBaseController
{
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()) {
        $active_classes[]   =   'admin-nav';
        
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }
    
    function viewHome($ID)
    {
        // loads temporary-home.php from themes/default
        $V  =   $this->getView('temporary-home');
        
        // example vars
            $V->init('test', 'TEST');
            $V->init('ID', $ID);
            $V->init('data', array('data1', 'data2'));
            $V->init('menu', 'home');   // sets left menu to themes/default/home.php
            
            $V->init('_title', 'Page Title');
            
            // set up the navigation breadcrumbs
            $this->getCrumbsRoot()->addCrumbTxt('Temporary_edit', "Go to edit action")->addCrumbTxt('Temporary_edit?test=value"><h1>', 'You can chain breadrumbs.');
            // end
        return $V;
    }
    
    function viewEdit()
    {
        // loads temporary-home.php from themes/default
        $V  =   $this->getView('temporary-edit');
        
        // example vars
            $V->init('test', $_GET['test']);
            $V->init('data', array('data1', 'data2'));
            $V->init('menu', 'home');   // sets left menu to themes/default/home.php
            $V->init('_title', 'Page Title for Edit Page');
            $this->getCrumbsRoot()->addCrumbTxt('Temporary_home?ID=' . rand(), "Go home");
            // end
            
        return $V;
    }
    
    function ExecuteAction($action = NULL) {
        
        switch($action)
        {
            case 'home':
                return $this->viewHome($_GET['ID']);
                
            case 'edit':
                return $this->viewEdit();
                break;
        }        
    }
}