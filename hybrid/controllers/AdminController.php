<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
//\ini_set('display_errors', true);
//error_reporting(E_ALL ^ E_STRICT);

require_once QUBEADMIN . 'inc/reseller.class.php';
        require_once QUBEADMIN . 'inc/user_permissions.class.php';        
        require_once QUBEADMIN . 'models/CampaignModel.php';        
        require_once QUBEADMIN . 'models/HubModel.php';        
        require_once QUBEADMIN . 'models/WebsiteModel.php';        
        require_once QUBEADMIN . 'models/LeadModel.php';        
       require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';
        
class AdminController extends AdminBaseController
{
    protected $search_q =   NULL;
    
    /*
    function prepareView(\ThemeView &$view) {
        parent::prepareView($view);
        
        $view->init('_title', 'Administration');
        $view->init('menu', 'admin');
        
        return $view;
    }
    */
    
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()) {
        $active_classes[]   =   'admin-nav';
        
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }
    
    function viewNetworks(){
		if($deny = $this->Protect('view_networks'))
			return $deny;

        $default_sorting = DataTableResult::getDefaultColumn('Networks');
        $Networks   =   $this->User->queryNetworks()->orderBy($default_sorting['Name'])->Exec()->fetchAll();
        $iTotalNetworks    =   $this->queryColumn('SELECT FOUND_ROWS()');
        
		$this->getCrumbsRoot()->addCrumbTxt('admin-networks', 'Networks');
        
		$V  =   $this->getPage('admin-networks', 'Networks', 'admin');
        $V->init('networks', $Networks);
        $V->init('iTotalNetworks', $iTotalNetworks);
        return $V;
    }
    
    function SaveNetwork(array $data, $ID = 0){
        
        $result =   array();
        $Validation    =   new ValidationStack;
        $uploadInfo =   new UploadsInfo();
        
        if(!$ID && !NetworkModel::Validate($data, $Validation)) // if creating a new row
                return $this->returnValidationError ($Validation);
        
        $isupload   =   $uploadInfo->accept('network', $Validation, $this->User->getStoragePath('hub'));
        $object_vars    =   $data;
        
        if($uploadInfo->hasFiles()){
            
            $result['uploads']      =   array();
            foreach($uploadInfo->getFiles() as $key    =>  $file_info){
                $object_vars[$key]    =   $file_info['name'];   // filename$uploadsInfo->getFiles('logo', 'name');
                $result['uploads'][$key]    =   $this->User->getStorageUrl('hub', $object_vars[$key]);
            } 
        }
        
        $NM =   new NetworkModel();
        $update =   false;
        if($ID)
        {
            $update = DBWhereExpression::Create('id = %d AND admin_user = dashboard_userid()', $ID);
            $NM->set('id', (int)$ID);
        }else{
            // initialize some stuff
            if(!isset($object_vars['name'])){
                $object_vars['name']    =   'New ' . $object_vars['type'] . ' Network (' . $object_vars['httphostkey'] . ')';
            }
            
            $object_vars['admin_user'] =   $this->user_ID;
        }
        
        if(!$Validation->hasErrors())
        {
            $return =   $this->q->Save($NM, $object_vars, $update);
            
            if($return instanceof HybridModel){
                $result['row']  =   $this->getView('rows/network', false, false)
                        ->init('network', $NM->Refresh())->toString();
            }else{
                $result['error']    =   true;
                $result['message']  =   'Database Error';
            }
        }else{
            $result['validation']   =   $Validation->getErrors();
        }
        
        return $this->getAjaxView($result);
    }
    
    function viewNetwork($ID){
        $Network =   $this->User->queryNetworks('id = %d', $ID)->Exec()->fetch();
        
        $PhotoUpload    =   new PhotoUploadWidget($this);
        $V  =   $this->getPage('admin-network', $Network->name . ' Network', 'admin');
        
        $PhotoUpload->appendToView($V, 'photoupload');
        
        $V->init('network', $Network);
        
        return $V;
    }
    
    function viewAdminDashboard(){
        $this->getCrumbsRoot()->addCrumbTxt('Admin_dashboard', 'Dashboard');
        
        $V  =   $this->getPage('dashboard-admin', 'Administration', 'admin');
        $W  =   new \UserListWidget(new \UserManagerController($this),                
                    array('status' => '',
                            'roles' => isset($_GET['role']) ? $_GET['role'] : NULL,
                        'limit' => 40
                    ));
        $W->appendToView($V, 'users');
        
        $W  =   new SpamWidget($this,
                array('max_text_length' => 128));
        $W->initView($V);
		
		$date = date('Y-m-d');
		
		/*$comRevTotal  =   $this->
                    query('SELECT sum(amount) FROM resellers_billing WHERE user_parent_id =48205 AND success =1 AND timestamp BETWEEN "'.date('Y-m-01').'" AND "'.$date.'"')
                ->fetchColumn();
		
		$localUsers  =   $this->
                    query('SELECT 
							 class AS `package`,
							 count(*) AS locals
							FROM users
							WHERE (canceled != 1 AND access = 1 AND class = 1167)
							OR (canceled != 1 AND access = 1 AND class = 1168)
							OR (canceled != 1 AND access = 1 AND class = 1169)
							OR (canceled != 1 AND access = 1 AND class = 1170)
							OR (canceled != 1 AND access = 1 AND class = 1171)
							OR (canceled != 1 AND access = 1 AND class = 983 AND created BETWEEN "2018-01-01" AND "'.$date.'")
							GROUP BY class')
                ->fetchAll();
		
		$agentsRevenue  =   $this->
                    query('SELECT MONTHNAME(b.timestamp) AS month,
							sum(b.amount) AS revenue
							FROM resellers_billing b, users u, user_info i, user_info c, user_class p
							WHERE b.timestamp BETWEEN "2018-01-01" AND "'.$date.'"
							AND b.user_parent_id =48205
							AND b.success =1
							AND u.id = b.user_id
							AND i.user_id = b.user_id
							AND c.user_id = i.referral_id
							AND p.id = b.product_id
							AND i.referral_id !=48205
							GROUP BY YEAR(b.timestamp), MONTH(b.timestamp)')
                ->fetchAll();
		
		$corpRevenue  =   $this->
                    query('SELECT MONTHNAME(b.timestamp) AS month,
							sum(b.amount) AS revenue
							FROM resellers_billing b, users u, user_info i, user_info c, user_class p
							WHERE b.timestamp BETWEEN "2018-01-01" AND "'.$date.'"
							AND b.user_parent_id =48205
							AND b.success =1
							AND u.id = b.user_id
							AND i.user_id = b.user_id
							AND c.user_id = i.referral_id
							AND p.id = b.product_id
							AND i.referral_id =48205
							GROUP BY YEAR(b.timestamp), MONTH(b.timestamp)')
                ->fetchAll();
		
		$allRevenue  =   $this->
                    query('SELECT MONTHNAME(b.timestamp) AS month,
							sum(b.amount) AS revenue
							FROM resellers_billing b, users u, user_info i, user_info c, user_class p
							WHERE b.timestamp BETWEEN "2018-01-01" AND "'.$date.'"
							AND b.user_parent_id =48205
							AND b.success =1
							AND u.id = b.user_id
							AND i.user_id = b.user_id
							AND c.user_id = i.referral_id
							AND p.id = b.product_id
							GROUP BY YEAR(b.timestamp), MONTH(b.timestamp)')
                ->fetchAll();
		
		$agentsSP  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 count(*) AS agents
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1217
							GROUP BY YEAR(created), MONTH(created)')
                ->fetchAll();
		
		$agentsSPPlus  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 count(*) AS agents
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1218
							GROUP BY YEAR(created), MONTH(created)')
                ->fetchAll();
		
		$agentsMD  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 count(*) AS agents
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1219
							GROUP BY YEAR(created), MONTH(created)')
                ->fetchAll();
		
		$jumpTracks  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 created
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1167
							ORDER BY created asc')
                ->fetchAll();
		$fastTracks  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 created
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1168
							ORDER BY created asc')
                ->fetchAll();
		$advTracks  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 created
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1169
							ORDER BY created asc')
                ->fetchAll();
		$proTracks  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 created
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1170
							ORDER BY created asc')
                ->fetchAll();
		$mlliteTracks  =   $this->
                    query('SELECT 
							 MONTHNAME(created) AS `month`,
							 created
							FROM users
							WHERE created BETWEEN "2018-01-01" AND "'.$date.'" AND class = 1171
							ORDER BY created asc')
                ->fetchAll();*/

        
        $t_users  =   $this->
                    query('SELECT count(*) FROM users WHERE parent_id = dashboard_userid()')
                ->fetchColumn();

        $t_camps =   $this->
                query('SELECT count(*) FROM campaigns WHERE user_id IN (SELECT id FROM users WHERE parent_id = dashboard_userid())')
                ->fetchColumn();
        
        $t_tps    =     $this->
                    query('SELECT count(*) FROM hub WHERE user_parent_id = dashboard_userid()')
                ->fetchColumn();
        $roles = UserDataAccess::FromController($this)->getRoles();
        $V->init('roles', $roles);
        foreach($roles as $role){
            $role_list[$role->ID] = $role->role;
        }
        $V->init('role_list', $role_list);
		//$V->init('jumpTracks', $jumpTracks);
		//$V->init('fastTracks', $fastTracks);
		//$V->init('advTracks', $advTracks);
		//$V->init('proTracks', $proTracks);
		//$V->init('mlliteTracks', $mlliteTracks);
		//$V->init('allRevenue', $allRevenue);
		//$V->init('comRevTotal', $comRevTotal);
		//$V->init('localUsers', $localUsers);
		//$V->init('corpRevenue', $corpRevenue);
		//$V->init('agentsRevenue', $agentsRevenue);
		//$V->init('agentsSP', $agentsSP);
		//$V->init('agentsSPPlus', $agentsSPPlus);
		//$V->init('agentsMD', $agentsMD);
        $V->init('t_sites', $t_tps);
        $V->init('t_users', $t_users);
        $V->init('t_camps', $t_camps);
        return $V;
    }
    
    function doRestore($array){
        // find in trash table, 
        //  update trash status
        
        foreach($array as $tbl_id){
            list($tbl, $id) =   explode('-', $tbl_id);
            if(!in_array($tbl,  
                    array('hub', 'campaigns', 'hub_page', 'hub_page2', 'blogs', 'blog_post', 'articles', 'leads')))
                    throw new Exception('Invalid table ' . $tbl);
            
            // @TODO save event
            Qube::GetPDO()->query('UPDATE ' . $tbl . ' SET trashed = 0 WHERE user_id = dashboard_userid() AND id = ' . (int)$id);
        }
    }
    
    function viewTrash(){
		if($deny = $this->Protect('view_trash'))
			return $deny;
        $this->getCrumbsRoot()->addCrumbTxt('Admin_dashboard', 'Dashboard');
        $q  =   $this->q->query('SELECT `table`, id, name, trashed from 6q_alltrash');
        
        $V  =   $this->getPage('admin-trash', 'System Trash', 'admin');
        $V->init('pdoresult', $q);
        return $V;
    }
    
    function viewAjaxSpamMessage($ID){
        
        require_once HYBRID_PATH . '/models/ContactDataModel.php';
        
        $rq  =   $this->q->queryObjects('ContactDataModel')
                ->Where('spamstatus != 0 AND (user_id = %d OR parent_id = %d OR %d) AND id = %d', 
                        $this->user_ID, $this->user_ID, $this->User->can('manage_spam'), $ID)
                    ->addFields('DAY(T.created) as day, substr(MONTHNAME(T.created), 1, 3) as monthname, YEAR(T.created) as year')
                    ->Exec()
                ;
#        echo $rq;
        /*
        $rq =   $rq
                    ->Exec();
        */
        
        if(!$rq)
            return $this->returnAjaxError ('Could not load message');
        
        $message    =   $rq->fetch();
        
        return $this->returnAjaxData('message', $message);
    }
    
    function doUpdateSpamMessage($ID, $SETPART)
    {
        $r  =   $this->q->query('UPDATE %s SET ' . $SETPART . ' WHERE (user_id = dashboard_userid() OR parent_id = %d OR %d) AND id = %d',
                        'ContactDataModel', $this->user_ID, $this->User->can('manage_spam'), $ID);
        return $r;
    }
    
    function doAjaxApproveSpamMessage($ID){
        if($q	=	$this->doUpdateSpamMessage($ID, 'spamstatus = 0'))
{
		if($q->rowCount() == 1){
			$this->doProcessMessage($ID);
		}

            return $this->returnAjaxData ('approved', true);
}        
        return $this->returnAjaxError('Could not approve this message.');
    }
    
    function doAjaxDeleteSpamMessage($ID){        
        if($this->doUpdateSpamMessage($ID, 'trashed = NOW()'))
            return $this->returnAjaxData ('deleted', true);
        
        return $this->returnAjaxError('Could not Delete this message.');
    }
    
    function doProcessMessage($ID){
        $proc   =   new ContactFormProcessor($this->q);
        
        /* @var $ContactForm ContactDataModel */
        $ContactForm    =   $this->q->queryObjects('ContactDataModel', 'T', 'id = %d', $ID)->Exec()->fetch();
        $user           = ContactFormProcessor::GetUserResellerInfoQuery($ContactForm->user_id)->Exec()->fetch();
        
        $proc->DispatchNotifications($ContactForm, $user);
    }
    
    function viewSpam()
    {
		if($deny = $this->Protect('view_spam'))
			return $deny;

        require_once HYBRID_PATH . '/models/ContactDataModel.php';
        $this->getCrumbsRoot()->addCrumbTxt('Admin_spam', 'Spam');
        
        $approved   =   $deleted    =   false;
        // approve or decline a message
            if(isset($_GET['APPROVE']))
            {
                $approved   =   $this->doUpdateSpamMessage($_GET['APPROVE'], 'spamstatus = 0');
                if($approved->rowCount() == 1){
                    $this->doProcessMessage($_GET['APPROVE']);
                }
            }else
            if(isset($_GET['DELETE']))
            {
                $deleted   =   $this->doUpdateSpamMessage($_GET['DELETE'], 'trashed = NOW()');
            }
        // 
       
        $wConfig    =   $_GET;
        $wConfig['max_text_length']    =   256;
        $W  =   new SpamWidget($this, $wConfig);
        
        $V  =   $this->getPage('spam-dashboard', 'Spam', 'admin');
        
        if($approved)
                $V->init('spamaction', 'The message has been approved.');
        else
        if($deleted)
                $V->init('spamaction', 'The message has been deleted.');
        
        $W->initView($V);
        
        return $V;
    }
    
    function viewSpamDashboard()
    {
        require_once HYBRID_PATH . '/models/ContactDataModel.php';
        $this->getCrumbsRoot()->addCrumbTxt('Admin_spam', 'Spam');
        
        $approved   =   $deleted    =   false;
        // approve or decline a message
            if(isset($_GET['APPROVE']))
            {
                $approved   =   $this->doUpdateSpamMessage($_GET['APPROVE'], 'spamstatus = 0');
                if($approved->rowCount() == 1){
                    $this->doProcessMessage($_GET['APPROVE']);
                }
            }else
            if(isset($_GET['DELETE']))
            {
                $deleted   =   $this->doUpdateSpamMessage($_GET['DELETE'], 'trashed = NOW()');
            }
        // 
       
        $wConfig    =   $_GET;
        $wConfig['max_text_length']    =   256;
        $W  =   new SpamWidget($this, $wConfig);
        
        $V  =   $this->getPage('spam-dashboard', 'Spam', 'admin');
        
        if($approved)
                $V->init('spamaction', 'The message has been approved.');
        else
        if($deleted)
                $V->init('spamaction', 'The message has been deleted.');
        
        $W->initView($V);
        
        return $V;
    }
    
    function viewRankings(){
		if($deny = $this->Protect('view_rankings'))
			return $deny;

		$this->getCrumbsRoot()->addCrumbTxt('campaign-rankings', 'Rankings');
        return $this->getPage('campaign-rankings', "Campaign Rankings View", "admin");
    }
	
	function viewProducts(){
		if($deny = $this->Protect('view_products'))
			return $deny;

		$this->getCrumbsRoot()->addCrumbTxt('admin_products', 'Products');
		return $this->getPage('admin-products', 'Reseller Products', 'admin');
	}
	
	function viewProductSettings(){
		$this->getCrumbsRoot()->addCrumbTxt('admin_products', 'Products');
		$this->getCrumbsRoot()->addCrumbTxt('admin_product-settings', 'Product Settings');
		return $this->getPage('admin-product-settings','Reseller Product Settings','admin');	
	}
    
    // @todo @amado remove after testing
    function viewDevTest(UserModel $User){
        $V = $this->getPage('admin-dev-test','Dev Test', 'admin');
        $V->init('userobj', $User);      
        return $V;
    }
	
	function viewBillingHistory() {
		if($deny = $this->Protect('view_billing'))
			return $deny;

		$this->getCrumbsRoot()->addCrumbTxt('admin_billing', 'Billing History');
		return $this->getPage('billing-history', 'Billing History', 'admin');
	}
    
    function viewBillingProfile() {
		if($deny = $this->Protect('view_billing'))
			return $deny;

        $this->getCrumbsRoot()->addCrumbTxt('admin_billing', 'Billing Profile');
		return $this->getPage('billing-profile', 'Billing Profile', 'admin');
    }
    
    function viewBillingSubscription() {
//		if($deny = $this->Protect('view_billing'))			return $deny;

        $uda = UserDataAccess::FromController($this);
        $uda->recalculateTOTALLimits($this->getUser()->getID(), new DateTime());
        $this->getCrumbsRoot()->addCrumbTxt('admin_billing', 'Billing Profile');
		return $this->getPage('billing-subscription', 'Billing Subscription', 'admin');
    }
    
    function viewSettings() {
        if($deny = $this->Protect('view_settings'))
			return $deny;
        
        $this->getCrumbsRoot()->addCrumbTxt('admin_settings', 'Settings');
		return $this->getPage('admin-settings', 'Admin Settings', 'admin');
    }
	
	function viewBranding($id) {
		if($deny = $this->Protect('view_branding'))
			return $deny;

        $main_site = preg_replace('/[^\w\.]+/', '', $_SERVER['HTTP_HOST']);
		$this->getCrumbsRoot()->addCrumbTxt('admin_branding', 'Branding');
		//$Reseller  =   $this->q->queryObjects('ResellerModel')->Where('T.admin_user = %d AND main_site = "%s"', $id, $main_site)->Exec()->fetch();
		$Reseller  =   $this->q->queryObjects('ResellerModel')->Where('T.admin_user = %d', $id)->Exec()->fetch();
        if (!$Reseller && $this->getUser()->isAdmin()) {
            $MM = new ModelManager();
            
            $Reseller = $MM->doSaveReseller(array(
                'admin_user' => $this->getUser()->getID(), 
                'main_site' => $main_site), 
                    $this->getUser()->getID(), 
                    QubeDataAccess::FromController($this))->getObject();
            
            if (!($Reseller instanceof ValidationStack)) {
                unset($this->Session->ResellerData);
            }
        }
        $V = $this->getPage('admin-branding', 'Branding', 'admin');
		if($this->getUser()->can('is_system')){
			$V->init('storagePath', $Reseller->getMainStorageURL());
		}
		$V->init('Brand', $Reseller);
		return $V;
	}
	
	function viewMUWebsites() {
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MU Websites');
		
//		$q = $this->q->queryObjects('WebsiteModel')->Where('multi_user = 1')->orderBy('name')->calc_found_rows()->Limit(10);
//		$websites = $q->Exec()->fetchAll();
//		$totalWebsites = $this->query('SELECT FOUND_ROWS()')->fetchColumn();

        $TPQ = TouchPointQuery::FromController($this);
        $default_sorting = DataTableResult::getDefaultColumn('Multi-Sites');
        $websites = $TPQ->ProcessQueryRequest(array(
            'campaign_id' => TouchPointQuery::MULTISITE_CAMPAIGNID,
            'touchpoint_type' => 'TOUCHPOINT',
            'limit' => 10,
            'limit_start' => 0,
            'query-mode' => 'without-stats',
            'calc_found_rows' => true,
            'where' => array('multi_user = 1'),
            'orderCol' => $default_sorting['Name'],
            'orderDir' => $default_sorting['Sort']
        ));

        $totalWebsites = $TPQ->FOUND_ROWS();

		$v = $this->getPage('admin-mu-websites', 'Multi-User Hubs', 'admin');

        $touchpointsTypes = CampaignController::getTouchPointTypes();
        unset($touchpointsTypes['BLOG']);
        $v->init('touchPointTypes', $touchpointsTypes);
		$v->init('sites', $websites);
		$v->init('total_tp_sites', $totalWebsites);
		
		return $v;
	}
	
	function viewMUWebsiteSettings() {
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MU Websites');
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites-settings', 'MU Website Settings');
		$V = $this->getPage('website-settings', 'MU Site Settings', 'website');
		$V->init('isMU', true);
		return $V;
	}
	
	function viewMUForms() {
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-forms', 'MU Forms');
        $RDA = ResponderDataAccess::FromController($this);
        $default_sorting = DataTableResult::getDefaultColumn('LeadForms');
        $masterForms = $RDA->getMasterLeadForms(array(
            'reseller_ID' => $this->getUser()->getID(),
            'calc_found_rows' => true,
            'orderCol' => $default_sorting['Name'],
            'orderDir' => $default_sorting['Sort']
        ));
        $totalMasterForms = $RDA->FOUND_ROWS();
        $v = $this->getPage('admin-mu-forms', 'Multi-User Forms', 'admin');
        $v->init('masterForms', $masterForms);
        $v->init('totalMasterForms', $totalMasterForms);
        return $v;
	}

    function viewMUBlogs() {
        $this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
		$this->getCrumbsRoot()->addCrumbTxt('admin_mu-blogs', 'MU Blog Feeds');
        $da =   BlogDataAccess::FromController($this);
        $default_sorting = DataTableResult::getDefaultColumn('BlogFeeds');
        $feeds = $da->getFeeds(array('user_ID' => $this->getDataOwnerID(), 'calc_found_rows' => true, 'limit' => 10, 'orderCol' => $default_sorting['Name'], 'orderDir' => $default_sorting['Sort']), 'trashed = 0');
        $totalFeeds = $da->FOUND_ROWS();
		$V = $this->getPage('admin-mu-blogs', 'Multi-User Blog Feeds', 'admin');
		$V->init('totalFeeds', $totalFeeds);
		$V->init('feeds', $feeds);
		return $V;
    }
    // @todo updated function
    function viewMUBlogPosts(VarContainer $post) {
        $this->getCrumbsRoot()->addCrumbTxt('admin_mu-websites', 'MultiSite CMS');
        $this->getCrumbsRoot()->addCrumbTxt('admin_mu-blogs', 'MU Blog Feeds');
        $da =   BlogDataAccess::FromController($this);
        $args = array('calc_found_rows' => true , 'feed_ID' => $post->get('feed_ID'));
        $posts = $da->getOwnMasterPosts(DataTableResult::getRequestArgs(VarContainer::getContainer(array(
                'table' => 'masterposts',
                'iDisplayStart' => 0,
                'iDisplayLength' => 10
            ))) + $args);
        $totalPosts = $da->FOUND_ROWS();
        $feeds = $da->getFeeds(array('user_ID' => $this->getDataOwnerID()));
        $categories = $da->getMPCategories($this->getUser()->getID(),$post->get('feed_ID'));


        $V = $this->getPage('admin-mu-blogPosts', 'Multi-User Blog Feed Posts', 'admin');

        //
        $V->init('posts', $posts);
        $V->init('categories', $categories);
        $V->init('totalPosts', $totalPosts);
        $V->init('feeds', $feeds);
        //
        $V->init('menu', 'admin');
        //
        $V->init('feed',$post->get('feed_ID'));
        return $V;
    }
    // @todo update function
    function viewMUBlogPostDetails($post_id) {
        $BDA = BlogDataAccess::FromController($this);
        $post = $BDA->getSinglePost($post_id,$dummy);
        $V  = $this->getPage('blog-post', 'Blog Post: ' . htmlentities($post->post_title), 'blog');
        $V->init('menu', 'admin');
        //$V->init('categories', array_keys($BDA->getMPCategories($this->getUser()->getID())));
		$V->init('categories', $BDA->getMPCategories($this->getUser()->getID()));
//        $V->init('blog', $blog);
        $V->init('post', $post);
		//var_dump(array_keys($BDA->getMPCategories($this->getUser()->getID())));

        return $V;

//        $V = $this->getPage('admin-mu-blogPosts', 'Multi-User Blog Feed Post', 'admin');
//        return $V;
    }

    function viewMuForm($form_id){
        $select_new = false;
        $RDA = ResponderDataAccess::FromController($this);
        /** @var LeadFormModel $form */
        $form = $RDA->getMasterLeadForms(array(
            'reseller_ID' => $this->getUser()->getID()
        ), $form_id);
        $form->upgrade();
        $form_fields    =   $form->getFields();
        if($select_new)
            $_GET['field']  =   count($form_fields)-1;

        $P  =  $this->getPage('campaign-form', 'Multi-User Form', 'admin');

        if(isset($_GET['field'])){
            $field_ID = $_GET['field'];
            $P->init('field_edit', LeadFormFieldModel::Query('id = %d AND form_ID = %d', $field_ID, $form->getID())->Exec()->fetch());
        }

        $cda = \Qube\Hybrid\DataAccess\CampaignDataAccess::FromController($this);
        $roles	= $cda->getFormsWithRoles(array(
            'formID' => $form->getID()
        ));
        $P->init('roles', $roles);

        $P->init('form_fields', $form_fields);
        $P->init('form', $form);

        return $P;
    }
	
	function viewUsers() {
		/*if($deny = $this->Protect('is_admin'))
			return $deny;*/
		if($deny = $this->Protect('view_users'))
			return $deny;
		# @amado we need some way to dechipher if action is (users, users-suspended, users-canceled) and display the proper title for the view and get the correct list.
		# currently all three page links go to this view
                $V = $this->getPage('admin-users', 'User List', 'admin');
                $W  =   new \UserListWidget(new \UserManagerController($this),                
                    array(
                        'serverParams' => array('status' => 'active'),
                        'roles' => isset($_GET['role']) ? $_GET['role'] : NULL,
                        'limit' => 40));
                $W->appendToView($V, 'users');
        $roles = UserDataAccess::FromController($this)->getRoles();
        $V->init('roles', $roles);
        $V->init('company', $this->getUser()->getBindings());
        foreach($roles as $role){
            $role_list[$role->ID] = $role->role;
        }
        $V->init('role_list', $role_list);
				
		$this->getCrumbsRoot()->addCrumbTxt('admin_users', 'Users');
		return $V;	
	}

    function viewUsersSuspended(){
        /*if($deny = $this->Protect('is_admin'))
			return $deny;*/
		if($deny = $this->Protect('view_users'))
			return $deny;
        # @amado we need some way to dechipher if action is (users, users-suspended, users-canceled) and display the proper title for the view and get the correct list.
        # currently all three page links go to this view
        $V = $this->getPage('admin-users', 'User List', 'admin');
        $W  =   new \UserListWidget(new \UserManagerController($this),
            array(
                'roles' => isset($_GET['role']) ? $_GET['role'] : NULL,
                'limit' => 40,
                'serverParams' => array('status' => 'canceled')
            ));
        $W->appendToView($V, 'users');
        $V->init('roles', UserDataAccess::FromController($this)->getRoles());

        $this->getCrumbsRoot()->addCrumbTxt('admin_users', 'Users');
        return $V;
    }
	
    function doImpersonate($user_ID){
        if(!$this->User->can('impersonate', $user_ID)) throw new Exception();
        
        if(!isset($this->Session->previous_user_id))
            $this->Session->previous_user_id   =   array();
        
        $this->Session->login_redirect =   $_SERVER['HTTP_REFERER'];
        
        $this->Session->previous_user_id[]  =   $this->getUser()->getID();
        
//        $this->setLoggedInUserID($user_ID);
        
//				$User	=	$this->getQube()->queryObjects('UserModel')->Where('ID = %d', $user_ID)->Exec()->fetch();
		UserModel::CreateUserDirectory(QUBEROOT . 'users', $user_ID);
				$this->unsetSessionUser($this->getSession());
        // this line makes the switch to the new user account.
				$this->Session->access_user_ID	=	$user_ID;
				
//				$this->setSessionUser($User);
//        AuthController::doLoginSessionUser($User, $this);
        
        return $this->ActionRedirect('Campaign_home');
    }
    
    function ExecuteAction($action = NULL) {

        switch($action) {
            case 'doImpersonate':
                return $this->doImpersonate($_GET['ID']);
        }

        if($action != 'billing-subscription' && ($deny = $this->Protect('view_admin')))
			return $deny;

        switch($action)
        {
            case 'ajaxDeleteRole':
                return $this->ajaxDeleteRole(VarContainer::getContainer((isset($_POST['role']) ? $_POST['role'] : array('ID' => $_GET['ID']))));
            case 'ajaxSaveFeed':
                return $this->saveFeed($_POST['feed']);
            case 'ajaxMuFeeds':
                return $this->ajaxMuFeeds(VarContainer::getContainer($_POST));
            case 'form':
                return $this->viewMuForm($_GET['form']);
			case 'roles':
				return $this->viewRoles();
			case 'saveBranding':
				return $this->saveBranding($_GET['ID'], VarContainer::getContainer($_POST['branding']));
			case 'saveMUWebsite':
				return $this->saveMUWebsite(VarContainer::getContainer($_POST));
			case 'ajaxMUWebsites':
				return $this->ajaxMUWebsites(VarContainer::getContainer(($_POST)));
			case 'ajaxMUForms':
				return $this->ajaxMUForms(VarContainer::getContainer(($_POST)));
			case 'saveBlogFeeds':
				return $this->saveBlogFeeds(VarContainer::getContainer($_POST));
            case 'ajaxnetworks':
                return $this->ajaxNetworks(VarContainer::getContainer($_POST));
            case 'savenetwork':
                return $this->SaveNetwork((array)@$_POST['network'], @$_GET['ID']);
                
            case 'network':
                return $this->viewNetwork($_GET['ID']);
                
            case 'networks':
                return $this->viewNetworks();

            case 'rankings':
                return $this->viewRankings();
            
			case 'products':
				return $this->viewProducts();
			
			case 'product-settings':
				return $this->viewProductSettings();
			
			case 'billing':
				return $this->viewBillingHistory();
            case 'billing-profile':
                return $this->viewBillingProfile();
			case 'billing-subscription':
                return $this->viewBillingSubscription();
            
			case 'branding':
				return $this->viewBranding($_GET['ID']);
			
            case 'settings':
                return $this->viewSettings();
            
			case 'mu-websites':
				return $this->viewMUWebsites();
			
			case 'mu-websites-settings':
				return $this->viewMUWebsiteSettings();
			
			case 'mu-forms':
				return $this->viewMUForms();

            case 'mu-blogs':
                return $this->viewMUBlogs();
            case 'mu-blog-posts':
                return $this->viewMUBlogPosts(VarContainer::getContainer($_GET));
            case 'mu-blog-post-trash':
                $params = VarContainer::getContainer($_GET);
                $TrashMgr   =   new TrashManager(SystemDataAccess::FromController($this) ,$this->getQube());
                $result = $TrashMgr->TrashByParam($params, $message, true);
                return $this->getAjaxView(array('error' => $result ? 0 : 1, 'message' => $message));
            case 'mu-blog-post':
                return $this->viewMUBlogPostDetails(VarContainer::getContainer($_GET)->get('post'));
			case 'users':
				return $this->viewUsers();
			case 'users-suspended':
				return $this->viewUsersSuspended();
			case 'users-canceled':
				return $this->viewUsers();
			
            case 'spam-dashboard':
                return $this->viewSpamDashboard();
                // @amado @todo make this view (viewSpamDashboard) pull in sidebar
            case 'spam':
                return $this->viewSpam();
            case 'approvespam':
                return $this->doAjaxApproveSpamMessage($_POST['ID']);
            case 'deletespam':
                return $this->doAjaxDeleteSpamMessage($_POST['ID']);
            case 'loadspam-msg':
                return $this->viewAjaxSpamMessage($_POST['ID']); // the contact_form id
                
            case 'restore':
                $this->doRestore($_POST['trash']);
                return $this->ActionRedirect('Admin_trash');
                
            case 'systeminfo':
			case 'profile':
                return $this->viewProfile($action);
                
            case 'trash':
                return $this->viewTrash();
            
            case 'ajaxMuBlogs':
                $var = VarContainer::getContainer($_POST);
                return $this->ajaxMuBlogs($var);
			case 'newrole':
				return $this->ajaxCreateNewRole(VarContainer::getContainer($_POST['role']));

            case 'ajaxRoles':
                return $this->ajaxRoles(VarContainer::getContainer($_POST));


            
            // @todo @amado remove after testing
            case 'devtest':
                return $this->viewDevTest($this->getUser());

        }
        
        return $this->viewAdminDashboard();        
    }
    
    function viewProfile($viewstr){
        $UserManager = new UserManagerController($this);
        $User = UserListWidget::queryUsers($this)->Where('T.id = %d', $this->user_ID)->Exec()->fetch();
        $view = null;
        switch($viewstr){
            case 'systeminfo':
                $view   =   $this->viewSystemInfo($User);
                break;
            case 'products':
                $view   =   $UserManager->viewUserProducts($User);
                break;
            case 'billinginfo':
                $view   =   $UserManager->viewUserBillingInfo($User);
                break;
            default: // profile
                $view   =   $UserManager->viewUserDetails($User);
        }
        return $view->init('menu', 'admin');
    }
	
	function viewSystemInfo(UserModel $User) {
		$this->getCrumbsRoot()->addCrumbTxt('Admin_systeminfo?ID=' . $User->getID(), 'System Information');
        $V  =   $this->getPage('user-systeminfo', 'System Info', 'admin');
        $V->init('userobj', $User);      
        return $V;
	}
    
    function getView($page, $header = 'dashboard.php', $footer = 'dashboard.php') {
//        if($page    ==  'spam-dashboard')
//            return parent::getView($page, 'login.php', 'login.php');
        return parent::getView($page, $header, $footer);
    }
    
    function processToken(\QubeTokenModel $token, $action) {
        if($action  ==  'spam')
            $token->doLoginUser ($this);
    }
    
    function ajaxNetworks(VarContainer $C){
        $q = $this->User->queryNetworks()->calc_found_rows();
        $params = array();
        if($C->checkValue('sSearch', $search) && !empty($search)){
            $params['search'] = $search;
            $q->where('(name like CONCAT(CONCAT("%",:search),"%") or httphostkey like CONCAT(CONCAT("%",:search),"%") or type like CONCAT(CONCAT("%",:search),"%") OR id = :search)');
        }
//        $C->load(array('model' => 'NetworkModel'));
        $C->load(array('table' => 'Networks'));
        $DT = new DataTableResult($q,$C);
        $DT->setRowCompiler($this->getView('rows/network', false, false), 'network');
        return $this->getAjaxView($DT->getArray($params));
    }
    
    function ajaxMuBlogs(VarContainer $C){
        $C->load(array('table' => 'masterposts','feed_ID' => $_GET['feed_ID']));
        $arr = DataTableResult::getRequestArgs($C);
        $arr['feed_ID'] = $C->get('feed_ID');
        $arr['category'] = $C->get('category');
		/** @var BlogDataAccess $BDA */
		$BDA = BlogDataAccess::FromController($this);
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($BDA, 'getOwnMasterPosts'),
            $arr,
            $this->getView('rows/blog-muposts-row', false, false), 'post', 'BlogPostModel'));
    }

    function saveFeed($data){
        $DA = BlogDataAccess::FromController($this);
        $MM = new ModelManager();

        $result = $MM->doSaveBlogFeed($data, 0, $DA);

        if($result instanceof ValidationStack){
            return $this->returnValidationError($result);
        }

        return $this->getAjaxView($result);
    }

	function saveBlogFeeds(VarContainer $C) {
		$result = array();
		$post = $C->get('post');
		$DA = BlogDataAccess::FromController($this);
		$MM = new ModelManager();
		
		
		
		if($post['feedID'] == 0){
			$feed = $MM->doSaveBlogFeed(array('name' => $post['feed']), 0, $DA)->getObject();
		}else{
			$feed = $DA->getFeeds(array('ID' => $post['feedID'], 'user_ID' => $this->getDataOwnerID()));
		}
		
		$postModel = $MM->doSaveBlogPost(array('post_title' => $post['post_title'], 'blog_id' => 0, 'category' => $post['category'],
            'category_URL' => str_replace(" ", "-" , $post['category'])), 0, $DA)->getObject();
		$postModel = $MM->doSaveBlogPost(array('master_ID' => $postModel->getID()), $postModel->getID(), $DA)->getObject();
		$DA->addPostToFeed($postModel, $feed->getID());
		$result['message'] = "New post has been created " . $post['name'];
        $categories = $DA->getMPCategories($this->getUser()->getID(),$feed->getID());
        $result['blog_categories'] = array_keys($categories);
        $result['blog_categories_values'] = array_values($categories);
		$result['object'] = $postModel;
        $result['error'] = false;
		return $this->getAjaxView($result);
	}
	
	function ajaxMUWebsites(VarContainer $C){
		$C->load(array('table' => 'Multi-Sites'));
//		$query = $this->q->queryObjects('WebsiteModel', 'T', 'multi_user = 1')->calc_found_rows(true);
//
//		$args = array();
//		if($C->checkValue('sSearch', $search) && $search != ''){
//			$query->Where('name LIKE :search');
//			$args['search'] = '%' . $search . '%';
//		}
//
//		$DT = new DataTableResult($query, $C);
//		$DT->setRowCompiler($this->getView('rows/system-multisites', false, false), 'site');
//
//		return $this->getAjaxView($DT->getArray($args));
		/** @var TouchPointQuery $TouchPointDA */
        $TouchPointDA = TouchPointQuery::FromController($this);
        $args = array(
            'campaign_id' => TouchPointQuery::MULTISITE_CAMPAIGNID,
            'touchpoint_type' => 'TOUCHPOINT',
//            'limit' => 10,
//            'limit_start' => 0,
            'query-mode' => 'without-stats',
            'where' => array('multi_user = 1')
        );

        $args = $args + DataTableResult::getRequestArgs($C);
        return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($TouchPointDA, 'ProcessQueryRequest'), $args,
            $this->getView('rows/admin-multisites', false, false), 'site', 'stdClass'));
	}
	
	function saveMUWebsite(VarContainer $c){
		$MM = new ModelManager();
		
		$data = array(
//			'touchpoint_type' => 'WEBSITE',
			'multi_user' => 1, 
			'cid' => TouchPointQuery::MULTISITE_CAMPAIGNID
            )
				+ $c->get('website');
		
		$result = $MM->doSaveWebsite($data, $c->getValue('id'), HubDataAccess::FromController($this));
		
		if ($result instanceof ValidationStack){
            return $this->returnValidationError($result);
		}
		
		return $this->getAjaxView($result);
	}

	function saveBranding($id, VarContainer $data){
		$R  =   $this->q->queryObjects('ResellerModel')->Where('T.id = %d', $id)
			->Exec()->fetch();
		$MM = new ModelManager();

		$result =   $MM->doSaveReseller($data->getData() + array('id' => $R->getID(), 'admin_user' => $R->admin_user), $R->getID(), QubeDataAccess::FromController($this));

		if($result instanceof ValidationStack){
			return $this->returnValidationError($result);
		}else{
            unset($this->Session->ResellerData);
        }

		return $this->getAjaxView($result);
	}

	function ajaxCreateNewRole(VarContainer $VC){
		$MM		= new ModelManager();
		$uda	=	UserDataAccess::FromController($this);
		$result = $MM->doSaveRole($VC, $VC->getValue('ID', 0), $uda);


		if($result instanceof ValidationStack)
			return $this->returnValidationError($result);

		$newrole	=	$uda->getRoles($result->getID());

//		$ajax	=	array();
//		$result['object'] =   $newrole;
		$result['is_update']  =   $VC->exists('ID') !== FALSE && $VC->getValue('ID') != '';
		$result['row']    =   $this->getView('rows/system-role', false, false)
			->init('Role', $newrole)->toString();

		return $this->getAjaxView($result);
	}

	function  viewRoles(){
        $uda = UserDataAccess::FromController($this);
		$roles	= $uda->getRoles(0, null, 0, 10);
		$AllRoles	= $uda->getRoles();

		$P  =   $this->getPage('system-roles', "System User Roles", "admin");
		$P->init('Roles', $roles);
		$P->init('AllRoles', $AllRoles);
		$P->init('totalRoles', $uda->CountRoles());
		$P->init('AllPermissions', $uda->getPermissions());
		$P->init('controller', 'admin');
		return $P;
	}

    function ajaxRoles(VarContainer $C){
        $C->load(array('table' => 'Roles'));

//        $q = $this
//            ->getQube()->queryObjects('QubeRoleModel')
//            ->leftJoin('6q_roleperms x', 'x.role_ID = T.ID')
//            ->leftJoin(array('QubePermissionModel', 'p'), 'p.ID = x.permission_ID')
//            ->addFields('group_concat(p.flag SEPARATOR ", ") as permissions_str')
//            ->groupBy('T.ID')->Limit(10);
//
//        $qparams['reseller_user'] = $this->getUser()->getID();
//
//        if($C->checkValue('sSearch', $search) && $search != ''){
//            $q->Where("(T.flag LIKE CONCAT(CONCAT('%',:search),'%') OR id = :search)");
//            $qparams[':search']  = $search;
//        }
//
        $DT =   new DataTableRolesResult(UserDataAccess::FromController($this), $C);
        $DT->setRowCompiler($this->getView('rows/system-role', false, false), 'Role');

//
//
//        $result = $DT->getArray($qparams);
//        $stmn = $this->q->GetDB()->prepare('SELECT COUNT(*) FROM 6q_roles WHERE reseller_user = :reseller_user');
//        $stmn->execute( array('reseller_user' => $this->getUser()->getID()));
//        $total = $stmn->fetchColumn();
//        $result['iTotalRecords'] = $total;
//        $result['iTotalDisplayRecords'] = count($result['aaData']);

        return $this->getAjaxView($DT->getArray());
    }


    function ajaxMUForms(VarContainer $C){
        $C->load(array('table' => 'LeadForms'));
		/** @var ResponderDataAccess $TouchPointDA */
        $TouchPointDA = ResponderDataAccess::FromController($this);
        $args = array(
            'reseller_ID' => $this->getUser()->getID()
        );

        $args = $args + DataTableResult::getRequestArgs($C);
        return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($TouchPointDA, 'getMasterLeadForms'), $args,
            $this->getView('rows/admin-mu-forms', false, false), 'leadform', 'LeadFormModel'));
    }

    function ajaxMuFeeds(VarContainer $C){
        $C->load(array('table' => 'BlogFeeds'));
        $args = DataTableResult::getRequestArgs($C) + array(
                'user_ID' => $this->getUser()->getID()
            );
		/** @var BlogDataAccess $BDA */
		$BDA = BlogDataAccess::FromController($this);
		return $this->getAjaxView(DataTableResult::returnDataTableStructure(array($BDA, 'getFeeds'), $args,
            $this->getView('rows/feed-row', false, false), 'feed', 'BlogFeedModel'));
    }


    function ajaxDeleteRole(VarContainer $C){
        $return = array();
        $UDA = UserDataAccess::FromController($this);
        if($UDA->deleteRole($C->ID, $C->getValue('newRole', false))){
            $return['message'] = "Role has been deleted";
        }else{
            $VS = new ValidationStack();
            $VS->addError('Error', "Role cannot be deleted");
            return $this->returnValidationError($VS);
        }
        return $this->getAjaxView($return);
    }

    function viewLeadNotificacion(){

    }
}