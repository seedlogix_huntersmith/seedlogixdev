<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 */
require_once HYBRID_PATH . 'controllers/BlogController.php';

class SocialController extends WebsiteController{
	protected $touchpointType = 'SOCIAL';
}
