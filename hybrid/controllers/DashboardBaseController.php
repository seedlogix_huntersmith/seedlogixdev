<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once 'HybridBaseController.php';
require_once 'SecureController.php';
require_once HYBRID_PATH . 'classes/MiniQuickStat.php';
require_once HYBRID_PATH . 'classes/BreadCrumb.php';
require_once HYBRID_PATH . 'classes/ChartWidget.php';

/**
 * Main features of DashboardBaseController are:
 * - provides miniquick stats (the tiny last 7 day visits graphs)
 * - generate and initialize touchpoint stats in prepareView
 * 
 */

/**
 * Main features of UIHybrid Controller:
 * - Initializes the Breadcrumbs Chain with a root node ("Campaigns Dashboard") in preExecute($action) and postExecute
 * - provide isAction view function in prepareView
 * - initializes a User object for the logged in object in prepareView
 * 
 */
class DashboardBaseController extends SecureController
{
    protected $quickstats   =   array();
    
    /**
     *
     * @var BreadCrumb
     */
    protected   $crumbs =   NULL;
    
    function queryColumn($q){
        return $this->query($q, FALSE)->fetchColumn();
    }
       
    /**
     * 
     * @param type $view
     * @param type $title
     * @param type $menu
     * @param type $active_classes
     * @param string $classes
     * @return ThemeView
     */
    function getPage($view, $title, $menu, $active_classes = array(), $classes = array()){
        if($this->ParentController)
            return call_user_func_array (array($this->ParentController, 'getPage'),
            func_get_args ());
        
        $V  =   $this->getView($view);
        $V->init('_title', $title);
        $V->init('menu', $menu);
        // css magic .. pass a list of css classes to be active.
        // confusing.. need better way to explain this @todo
        // active_classes are keys that hold the css class "active"
        // in this way we can do echo $classes['my-key']; and it will print 'active' if my-key is passed in the array.
        
        
        foreach($active_classes as $classname){
            $classes[$classname]    =   'active';
        }
        $V->init('classes', $classes);
        return $V;
    }   
    
    function returnValidationError($vs){
        $v = NULL;
        $result = array('error' => 'validation', 'validation' => null);
        if($vs instanceof ValidationStack){
            $result['limit_reached'] = $vs->getlimit();
            $result['validation'] = $vs->getErrors();
        }
        else if(is_array($vs))
            $result['validation'] = $vs;
        else if(is_string($vs))
            $result['validation'] = array('Unknown' => array($vs));
        else
                throw new QubeException('Err.');

        return $this->getAjaxView($result);
    }
    
    function returnAjaxError($message, $moredata = array())
    {
        $result = array('id' => 1,
                        'action' => 'replace',
                        'container' => '#error',
                        'message' => $message,
                        'error' => true)+$moredata;
        
        return $this->getAjaxView($result);
    }
    
    function returnAjaxData($keyname, $data, $moredata    =   array())
    {
        $result =   array($keyname => $data,
                            'error' => false
                            ) + $moredata;
        return $this->getAjaxView($result);
    }

    function getCrumbsRoot($is_campaign_report = true, $not_mu = true, $c_id = null, $c_name = null)
    {/* Booleans added for Breadcrumbs consistency */
        if ($not_mu) {
            if ($is_campaign_report && !$this->crumbs) {
                $this->crumbs = new BreadCrumb('Campaign_home', 'Campaigns: Main Dashboard');
                if ($c_id && $c_name) {
                    $this->crumbs->addCrumbTxt('Campaign_dashboard?ID=' . $c_id, 'Campaign Dashboard: ' . $c_name);
                }
            } elseif (!$is_campaign_report && !$this->crumbs) {
                $this->crumbs = new BreadCrumb('Campaign_reports', 'Campaigns: Reports');
            }
        } else {
            $this->crumbs = new BreadCrumb('Admin_mu-websites', 'Admin: MU Touchpoints');
        }
        return $this->crumbs;
    }

    function postExecute($action, BaseView $View) {
        parent::postExecute($action, $View);
        
        if($this->crumbs)
            $View->init('breadcrumbs', $this->crumbs);

		if(!($View instanceof CSVExportView))
        	$View->init('quickstats', $this->quickstats);
    }
    

    /**
     * Add a Campaigns-list crumb to the root crumb.
     * 
     * @param type $current_ID
     * @return \BreadCrumb
     */
    function getCrumbsCampaigns($current_ID, $campaign_action = 'Campaign_dashboard?ID=%d', $campaign_name = 'Campaign Dashboard: %s')
    {
//        $camps  =   Qube::GetPDO()->query('select id, name from campaigns WHERE user_id = dashboard_userid() AND trashed = 0 ORDER BY id = ' . $current_ID . ' DESC')->fetchAll(PDO::FETCH_ASSOC);
        $CDA = \Qube\Hybrid\DataAccess\CampaignDataAccess::FromController($this);
        $camps = $CDA->getCampaigns(array(
            'limit' => 25,
            'orderBy' => array('id = :current_id desc, c.total_touchpoints'),
            'bindParams' => array('current_id' => $current_ID)
        ));
        $bread  =   NULL;
        if(!$camps)
        {
            $bread = new BreadCrumb('campaign_home', 'No Campaigns');
        }
        foreach($camps as $camp)
        {
            $temp   =   new BreadCrumb(sprintf($campaign_action, $camp->id), sprintf($campaign_name, $camp->name));
            if(is_null($bread))
                $bread  =   $temp;
            else
                $bread->addChild ($temp);
        }
        return $bread;
    }

		function findUsers($qstr)
		{
			$Q  =   $this->getSubusersQuery();
			if(preg_match('/^[0-9]+$/', $qstr))
			{
				return	$Q->Where('T.id = %d', $qstr)->Exec()->fetchall();
			}

			$prep	=	$Q->Where('(username LIKE :search OR 
								firstname LIKE :search OR
								lastname LIKE :search OR
								phone LIKE :search OR
								company LIKE :search OR
								T.id LIKE :search)')
								->Prepare();
				$prep->execute(array('search' => '%' . $qstr . '%'));
				return $prep->fetchAll();
		}
		
    function ExecuteAction($action = NULL) {
        
        if($action == 'user-search' && $this->User->can('view_users'))
        {
						$users	=	$this->findUsers($_POST['q']);
            
            $V  =   $this->getView('lists/subusers', false, false);
            $V->init('subusers', $users);
            
            return $V;
        }
				
				if(DataTableWidget::findWidget($_GET['w']))
				{
					$WClass	=	$_GET['w'] . 'Widget';
					$W	=	 new $WClass($this, $_POST);
					return $W->doAction($action);
				}
        
        return new BlankView();
    }
    
    function getLastWeekVisitors(PiwikReportsDataAccess $da, $cid = NULL)
    {
			$PB	= PointsSetBuilder::createFromOffset(30, 'DAY');
			return $da->getUserGlobalStats($PB, $this->getUser()->getID(), 'array', $cid);
#			$DA
//        return $a->getChartData(7, 'DAY', 'nb_visits', 'ValueArray');
//        return $a->getChartData(), 'nb_visits', 'ValueArray');
    }
    
    function getLastWeekLeads(DashboardAnalytics $a, $cid = NULL)
    {
#        return $a->getChartData(7, 'DAY', 'nb_visits');
		$RDA	=	new ReportDataAccess($this->q);
		
        $ref    =   '-30 DAY';
        $date   =   date('Y-m-d');
        $datemagic  =   new PointsSetBuilder(new VarContainer(array('interval' => 30,
                'period'    =>  'DAY')));
        $query  =   $RDA->getLeadsCountStatement($datemagic, new DBWhereExpression, $cid, $this->getUser()->getID());//$ref, $date, true, true);
        
        $data   =   $a->createValueArray($query, $datemagic);
        return $data;
    }
	
	function getLastWeekCalls(DashboardAnalytics $a, $cid = NULL)
    {
#        return $a->getChartData(7, 'DAY', 'nb_visits');
		$RDA	=	new ReportDataAccess($this->q);
		
        $ref    =   '-30 DAY';
        $date   =   date('Y-m-d');
        $datemagic  =   new PointsSetBuilder(new VarContainer(array('interval' => 30,
                'period'    =>  'DAY')));
        $query  =   $RDA->getCallsCountStatement($datemagic, new DBWhereExpression, $cid, $this->getUser()->getID());//$ref, $date, true, true);
		
		//var_dump($query);
        
        $data   =   $a->createValueArray($query, $datemagic);
		
        return $data;
    }
    
    /**
     * called by subclasses
     * 
     * @param DashboardAnalytics $analytics
     */
    function addQuickStats($analytics, $cid = NULL){
        $this->quickstats[] = new MiniQuickStat($this->getLastWeekVisitors(new PiwikReportsDataAccess, $cid),'Visits','30 DAY','sparkline');
        $this->quickstats[] = new MiniQuickStat($this->getLastWeekLeads(new DashboardAnalytics, $cid),'Leads','30 DAY','sparkline2');
		$this->quickstats[] = new MiniQuickStat($this->getLastWeekCalls(new DashboardAnalytics, $cid),'Calls','30 DAY','sparkline3');
		
    }
    
    
    /**
     * 
     * @return DBSqlQuery
     */
    private function getSubusersQuery(){
        $q = $this->q->queryObjects('UserModel')
                ->leftJoin('user_info i', 'i.user_id = T.id')
                ->Where('(%d = 1 AND T.parent_id = %d)  or (%d = 1) or (i.referral_id = %d)',
                        $this->getUser()->can('view_users'),
                        $this->getUser()->getParentID(),
                        $this->getUser()->isSystem(),
                        $this->getUser()->getID()
                )
                ->Limit(10)->orderBy('created DESC')->Fields('T.id, username, T.canceled, T.access, 0 as isreseller');
//        echo $q;
        return $q;
    }
    
    function prepareView(ThemeView &$view) {
        parent::prepareView($view);
        
        $view->init('subusers', $this->getSubusersQuery()
                            ->Exec()->fetchAll(PDO::FETCH_CLASS));
    }

    /**
     * @param null $campaignID
     * @return mixed
     */
    function getTouchPointsStats($campaignID = null)
    {
        // this is madness.. @todo use an enum or set type for hubs or an index table.
        /*
        $bindings           =   array('website' => 1,
                                        'multi_user' => 4,
                                                'mobile' => 3,
                                        'landing_page' => 2,
                                                'social' => 5);
        $names  =   array();
        foreach($bindings as $name => $themetype){
            $names[]    =   $themetype . ' THEN "' . $name . '"';
        }

        /*
        $st  =   $this->query('SELECT CASE req_theme_type WHEN ' . join(' WHEN ', $names) . ' ELSE "unknown" END as theme, count(*) as num
                        FROM hub WHERE (user_id = dashboard_userid()) AND (ISNULL(@dashboard_campaignid) OR cid = @dashboard_campaignid) AND trashed = 0 AND cid != 0 GROUP BY req_theme_type
                        UNION ALL SELECT "blog", count(*) FROM blogs WHERE (user_id = dashboard_userid()) AND (ISNULL(@dashboard_campaignid) OR cid = @dashboard_campaignid) AND trashed = 0 AND cid != 0');
        */

//        $st  =   $this->query('SELECT touchpoint_type, count(*) as num
//                        FROM hub WHERE (user_id = dashboard_userid()) AND (ISNULL(@dashboard_campaignid)
//                            OR cid = @dashboard_campaignid) AND trashed = 0 GROUP BY touchpoint_type
//                        UNION ALL SELECT "BLOG", count(*) FROM blogs WHERE (user_id = dashboard_userid())
//                            AND (ISNULL(@dashboard_campaignid) OR cid = @dashboard_campaignid) AND trashed = 0');


		$params = array("dashboard_campaignid" => $campaignID);
		$wda	= WebsiteDataAccess::FromController($this);
		
		return $wda->getCampaignTouchPointCounts($campaignID);
    }
    

	public function getStringView($string){
		$view = new PrintStringView();
		$view->init('var', $string);
		return $view;
	}
}
