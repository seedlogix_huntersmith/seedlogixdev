<?php

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('Account Type', 'SFDC ID', 'Account Number', 'Blog ID', 'User ID', 'Tracking ID', 'Company Name', 'Domain',
    'Phone', 'Address', 'City', 'State', 'Visits'));

$start = $_GET["start"];
$end = $_GET["end"];
$token = $_GET["token"];

if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $start) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $end) && $start != '' && $end != '' && $token = 'ahecSW2VAKxGBcZK2DhqZzB') {
    $proceed = TRUE;
} else {
    $proceed = FALSE;
}
if ($proceed) {


//include phone class
#require_once('/home/sapphire/production-code/2012-08-24/admin/inc/analytics.class.php');
    require '../bootstrap.php';
    Qube::AutoLoader();
    @session_start();
    require_once QUBEADMIN . 'inc/analytics.class.php';
    $stats = new Analytics();

//AND hub_parent_id = '7024'
//AND hub_parent_id = '8220'

    $query = "SELECT h.id, h.user_id, h.cid, h.tracking_id, h.domain, h.company_name, h.phone, h.address, h.city, h.state, u.account_number, u.api_id, r.role_ID FROM blogs h, user_info u, 6q_userroles r WHERE h.user_parent_id = '6312'
              AND h.trashed = '0000-00-00'
			  AND h.httphostkey != ''
              AND h.domain != '' AND tracking_id != 0
			  AND h.user_id = u.user_id
			  AND h.user_id = r.user_ID
              ORDER BY h.company_name ASC";    // temp 50235

    $hubs = $stats->query($query);

//instantiate blog class for blog section in right panel
//get analytics info
// loop over the rows, outputting them
//fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip'));

    while ($row = $stats->fetchArray($hubs)) {

        if ($row['role_ID'] == 51)
            $role_name = 'PowerLeads';
        if ($row['role_ID'] == 24)
            $role_name = 'PowerLeads Plus';

        //$query2 = "SELECT count(id) FROM leads WHERE hub_id = '" . $row['id'] . "' AND spam < 50 AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
        //$leads1 = $stats->queryFetch($query2);
        //$query3 = "SELECT count(id) FROM contact_form WHERE type_id = '" . $row['id'] . "' AND spamstatus = '0' AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
        //$leads2 = $stats->queryFetch($query3);
        //$numProspects = $leads1['count(id)'] + $leads2['count(id)'];


        if (TRUE) {
            $piwikDataAccess = new PiwikReportsDataAccess();
            $r0 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], '' . $start . ',' . $end . '');
//$r1	=		'';
            //$r1 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
//$r2	=		'';
            //$r2 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
        } else {
            $r0 = $r1 = $r2 = 'test';
        }

        fputcsv($output, array($role_name, $row['api_id'], $row['account_number'], $row['id'], $row['user_id'], $row['tracking_id'], $row['company_name'], $row['domain'], $row['phone'], $row['address'], $row['city'], $row['state'], $r0));
        flush();
//fputcsv($output, array($row['id'], $row['user_id'], $row['cid'], $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'], NULL, NULL, NULL, $numProspects));
    }
} else
    echo 'Access Denied';
