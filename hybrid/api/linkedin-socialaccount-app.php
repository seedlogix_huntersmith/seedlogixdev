<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Change system directory.
chdir(dirname(__FILE__));

// Load SeedLogix bootstrap.
require '../bootstrap.php';

// Load SeedLogix config and database.
Qube::ForceMaster();

session_start();

// Get blog social account id.
$social_account_id = !empty($_GET['aid']) ? $_GET['aid'] : $_SESSION['aid'];

if (empty($social_account_id)) {
    die("Could not get social account id!");
}

// Prepare the mysql query statement.
$stmt = Qube::GetPDO()->prepare(
        'SELECT s.id, r.linkedin_app_id, r.linkedin_app_secret, r.linkedin_code, r.main_site 
            FROM resellers r, social_accounts s, users u 
            WHERE s.id = ? 
            AND ((s.user_id != 0 AND r.admin_user = u.parent_id) OR r.admin_user = s.user_id)');

// Execute mysql query.
if (!$stmt->execute(array($social_account_id))) {
    // Failed query.
    die('Failed to get LinkedIn API information from database!');
    
}

// Fetch API information from database.
$operationinfo = $stmt->fetch(PDO::FETCH_ASSOC);

// Set LinkedIn app code.
/*if (empty($operationinfo['linkedin_code'])) {
    // Not sure if you can use the same code??? So will die for now...
    die('Failed to get LinkedIn API code from database!');
    
} else {
    // Set LinkedIn app code to reseller's value.
    $linkedin_app_code = $operationinfo['linkedin_code'];
    
}*/

// Set LinkedIn app id.
if (empty($operationinfo['linkedin_app_id'])) {
    // Set LinkedIn app id to SeedLogix defualt if reseller is empty.
    #$linkedin_app_id = '86djy22jquj41b'; // <- !!Eventually change to SeedLogix, using Hunter's account for testing.
    $linkedin_app_id = '86a8wviwxw9t3o'; // <- SeedLogix Live.
    
} else {
    // Set LinkedIn app id to reseller's value.
    $linkedin_app_id = $operationinfo['linkedin_app_id'];
    
}

// Set LinkedIn app secret.
if (!$operationinfo['linkedin_app_secret']) {
    // Set LinkedIn app secret to SeedLogix defualt if reseller is empty.
    #$linkedin_app_secret = 'VIUlKAzHyoBCFXdW'; // <- !!Eventually change to SeedLogix, using Hunter's account for testing.
    $linkedin_app_secret = 'SZEQwNoxSIDUa8fc'; // <- SeedLogix Live
    
} else {
    $linkedin_app_secret = $operationinfo['linkedin_app_secret'];
    
}

$reseller_host = 'login.seedlogix.com';
//$reseller_host = 'seedlogix.test';


// DO GET ACCESS TOKEN STUFF BELOW!!!
//session_start();

// OAuth 2 Control Flow
if (isset($_GET['error'])) {
    // LinkedIn returned an error
    die("LinkedIn Authentication Error <br />" . $_GET['error'] . ": " . $_GET['error_description'] . "\r\n");
    
} else if (isset($_GET['code'])) {
    // User authorized your application
    if ($_SESSION['state'] == $_GET['state']) {
        // Get token so you can make API calls
        
        // Set configuration parameters array to get LinkedIn access token.
        $params = array(
            'grant_type' => 'authorization_code', 
            'client_id' => $linkedin_app_id, 
            'client_secret' => $linkedin_app_secret, 
            'code' => $_GET['code'], // <- !!NEED TO ADD THIS TO RESELLER TABLE IN DATABASE .
            'redirect_uri' => 'http://' . $reseller_host . '/api/linkedin-socialaccount-app.php'
            //'redirect_uri' => 'http://' . $reseller_host . '/api/linkedin-blog-app.php'
        );
        //var_dump($params);
        
        $url = "https://www.linkedin.com/oauth/v2/accessToken";
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        
        $response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        // Error in curl request.
        if (!empty(curl_error($curl))) {
            die("Curl error: " . curl_error($curl));
        }
        
        //print($response);
        $token = json_decode($response);
        //var_dump($token);
        
        curl_close($curl);
        
        // Store access token and expiration time
        $_SESSION['access_token'] = $token->access_token;
        $_SESSION['expires_in'] = $token->expires_in;
        
        //var_dump($_SESSION); exit;
        
        // Get some basic info.
        $url = "https://api.linkedin.com/v1/people/~:(public-profile-url,last-name)?format=json";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("authorization: Bearer {$_SESSION['access_token']}"));

        $response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // Error in curl request.
        if (!empty(curl_error($curl))) {
            print("Curl error: " . curl_error($curl));
            exit;
        }

        $profile_response = json_decode($response);
        //var_dump($profile_response, $social_account_id, $params, $token->access_token); exit;

        curl_close($curl);
        
        $query = "UPDATE social_accounts "
                . "SET token = CONCAT(?, '\n', ?, '\n', ?, '\n', ?, '\n', ?, '\n', ?) "
                . "WHERE id = ?";
        $pdo = Qube::GetPDO()->prepare($query);
        
        // Add to concat field for URL profile id AND Company Name!!!
        if ($pdo->execute(array(urlencode($profile_response->publicProfileUrl), $profile_response->lastName, $params['grant_type'], $params['client_id'], $params['client_secret'], $token->access_token, $social_account_id))) {
            print "LinkedIn Publishing has been configured. Please <a href='javascript:void(close());'>close this window.</a>";
            
        } else {
            Qube::Fatal("Unable to update token in table 'social_accounts' .", $social_account_id);
                    
        }
        
    } else {
        // Cross Site Request Forgery attack? Or mixed up states?
        die ("Non-matching states found!<br />\r\n");
        
    }
    
} else { 
    if ((empty($_SESSION['expires_in'])) || (time() > $_SESSION['expires_in'])) {
        // Token has expired, clear the state
        print("Token expired!<br />");
        session_unset();
        
    }
    
    if (empty($_SESSION['access_token'])) {
        // (STEP 1) Start authorization process (if there is no session or session is expired).
        //print "Get authorization code!<br />\r\n";
        session_start();
        
        // Get blog social account id.
        $social_account_id = !empty($_GET['aid']) ? $_GET['aid'] : die("Could not get social account id!");
        
        // Store blog account id to session.
        $_SESSION['aid'] = $social_account_id;
        
        $params = array(
            'response_type' => "code", 
            'client_id' => $linkedin_app_id, 
            'redirect_uri' => "http://" . $reseller_host . "/api/linkedin-socialaccount-app.php", 
            'state' => uniqid("", true), 
            //'scope' => 
        );
        
        $url = "https://www.linkedin.com/oauth/v2/authorization?" . http_build_query($params);
        //var_dump($url);
        
        $_SESSION['state'] = $params['state'];

        header("Location: $url");
        exit;
    }
}


