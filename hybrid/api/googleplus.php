<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';

Qube::ForceMaster();

// @todo maybe use server:http_host in the query (where main_site = ? )
$bloghttphost    =   isset($_GET['bloghttphost']) ? $_GET['bloghttphost'] : $_SERVER['HTTP_HOST'];

$stmt=  Qube::GetPDO()->prepare('select b.id, r.twapp_id, r.twapp_secret, r.main_site from resellers r, blogs b where b.httphostkey = ?
        AND ((b.user_parent_id != 0 AND r.admin_user = b.user_parent_id) or r.admin_user = b.user_id)');


if(!$stmt->execute(array($bloghttphost))) die (' failed');

$operationinfo  =   $stmt->fetch(PDO::FETCH_ASSOC);

$config = array(
    'callbackUrl' => 'http://' . $operationinfo['main_site'] . '/api/twitter-blog-app.php?bloghttphost=' . urlencode($bloghttphost),
    'siteUrl' => 'http://api.twitter.com/oauth',
    'consumerKey' => $operationinfo['twapp_id'],    //'cZnMrZtUh9VvYgoHlfGUQ',
    'consumerSecret' => $operationinfo['twapp_secret'], //'VKAfHegB8NhFv2QmQfEosXzV6rvyfPtV7fHLUS4zc'
);

$consumer = new Zend_Oauth_Consumer($config);
$req_token_key  =   $operationinfo['id'] . '_twitter_request_token';

@session_start();

if(isset($_GET['oauth_token']))
{
    if(isset($_SESSION[$req_token_key]))
    {
        // exchange request token for access token
       $request_token  =   $_SESSION[$req_token_key];   
       $access_token    =   $consumer->getAccessToken($_GET, $request_token);
       
       unset($_SESSION[$req_token_key]);
       
	$oauth_token	=	$access_token->getParam('oauth_token');
	$oauth_secret	=	$access_token->getParam('oauth_token_secret');
	$user_id	=	$access_token->getParam('screen_name');

       $q   =   'UPDATE blogs SET twitter_accessid  =   concat(?, "\n", ?, "\n", ?) WHERE httphostkey =   ?';
       $p   =   Qube::GetPDO()->prepare($q);
       
       if($p->execute(array($oauth_token, $oauth_secret, $user_id, $bloghttphost)))
       {
           ?>
            Twitter Publishing has been configured. Please <a href="javascript:void(close());">close this window.</a>
<?php
       }else
           Qube::Fatal('Unable to update blog twitter_accessid', $bloghttphost);
       
    }else{
        Qube::Fatal('Invalid request . oauth_token provided but session key is undefined.');
    }
}elseif(count($_GET) == 1)
{
    // ok.. fetch requet token and execute redirect.
    $reqtoken = $consumer->getRequestToken();
    $_SESSION[$req_token_key]   =   $reqtoken;
    $consumer->redirect();
    
}else{
    
    Qube::Fatal('Unexpected GET arguments.');
    
}


