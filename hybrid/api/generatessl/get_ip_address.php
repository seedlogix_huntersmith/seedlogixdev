<?php
// sl = seedlogix
$sl_ip_list = [
    '50.22.60.10',
    '50.23.225.224',
    '50.23.225.225',
    '50.23.225.228',
    '50.23.225.229',
    '50.23.225.230',
    '50.97.164.248',
    '50.97.78.208',
    '50.97.78.209',
    '50.97.78.210',
    '50.97.78.211',
    '50.97.78.212',
    '50.97.78.213',
    '50.97.78.214',
    '50.97.78.215',
];

$ip_match = false;
$data = array();

if (!empty($_POST['url_address'])) {
    $hostname = $_POST['url_address'];
    $ip = gethostbyname($hostname);
    $data['hostname'] = $hostname;
    $data['ip'] = $ip;
    if ($ip != $hostname && $ip != '127.0.0.1') {
        // Returned an ip address. Check to see if it points to SeedLogix.
        foreach($sl_ip_list as $sl_ip) {
            if ($sl_ip == $ip) {
                $ip_match = true;
                break;
            }
        }

        if ($ip_match) {
            // Looks like it points to our SL server.
            $data['STATUS'] = 'SUCCESS';
            $data['IP-ADDRESS'] = $ip;
        } else {
            // Hostname has not been pointed to our server yet.
            $data['STATUS'] = 'FAILED';
            $data['REASON'] = 'Host has not been pointed to our server yet.';
        }

    } else {
        // Hostname ip not found.
        $data['STATUS'] = 'FAILED';
        $data['REASON'] = 'Hostname not found.';
    }
} else {
    // No form entry (for 'url_address')...
    // Hostname ip not found.
    $data['STATUS'] = 'FAILED';
    $data['REASON'] = 'Hostname empty. No URL has been typed in.';
}

echo json_encode($data);