<?php
define('QUBE_NOLAUNCH', 1);
require_once '/home/sapphire/production-code/sofus-unstable/hybrid/bootstrap.php';
Qube::AutoLoader();

require_once( '/home/sapphire/production-code/sofus-unstable/hybrid/library/facebook-php-sdk/src/facebook.php' );

	$config = array(

		'appId' => '1604862766397665',

		'secret' => '649610b9a32a251610ffd0418caeac6a',

		'fileUpload' => false, // optional

		'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps

	);

$facebook = new Facebook( $config );

require_once('/home/sapphire/data/hubs-themes/socialmashup/oauth/twitteroauth.php'); 

$consumer_key = trim('7n4KT8rtdiM1sxc7HPFTvGyrI');
$consumer_secret = trim('DF2UQS4roV6MYcD1OXK407nG9ZAJMRFC0gnDtpuAunoi0Mfd0j');
$oauth_access_token = trim('3241730810-8PxQoOtanQVdynQRKL4IZTY1xz4wnL1n6YLWOcf');
$oauth_access_token_secret = trim('wtxzUfJX3TAJlOIxhoSHxPL8RGCEOa3CpA2trWLLGCaJM');

$auth = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);

$auth->decode_json = FALSE;
$trest = 'statuses/user_timeline';

function usersQuery($parent_id){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.users u');
	$select->Fields('u.id');
	$select->Where('u.parent_id = %d', $parent_id);
	$select->Where('u.id != %d', 65149);
	//$select->Where('u.id = %d', 6526);
	$select->Where('EXISTS (SELECT 1 FROM ranking_sites k WHERE k.user_id = u.id)');

	$stmt	=	$select->Exec();
	
	$userlist	=	array();
	while($row	=	$stmt->fetch()){
		$userlist[$row['id']]	=	$row;
	}

	return $userlist;
}

function sitesQuery($user_id){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.hub h, sapphire_6qube.ranking_sites s');
	$select->Fields('h.connect_blog_id, s.hostname');
	$select->Where('s.user_id = %d', $user_id);
	$select->Where('h.httphostkey = s.hostname');

	$stmt	=	$select->Exec();
	
	$sitelist	=	array();
	while($row	=	$stmt->fetch()){
		$sitelist[$row['connect_blog_id']]	=	$row;
	}

	return $sitelist;
}

function blogsQuery($blogid){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.blogs');
	$select->Fields('httphostkey, facebook_id, twitter_accessid');
	$select->Where('id = %d', $blogid);

	$stmt	=	$select->Exec();
	return $stmt->fetch();
}

function statsQuery($user_id){
	$start = date('Y-m-d', strtotime('-7 days'));
	$end = date("Y-m-d");
	
	$pdo =	Qube::GetPDO();
	$sql = "SELECT SUM(nb_visits) AS Total FROM sapphire_6qube.piwikreports_social WHERE user_id = '".$user_id."' AND ts BETWEEN '".$start."' AND '".$end."'"; 
	$result = $pdo->prepare($sql); 
	$result->execute(); 
	$statstotal = $result->fetchColumn(); 
	
	return $statstotal;
}

function bPostsQuery($blogid){
	$start = date('Y-m-d', strtotime('-7 days'));
	$end = date("Y-m-d");
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.blog_post');
	$select->Fields('id, post_title, publish_date, photo, category_url, post_title_url');
	$select->Where('blog_id = %d', $blogid);
	$select->Where("publish_date BETWEEN '".$start."' AND '".$end."'");

	$stmt	=	$select->Exec();
	
	$postlist	=	array();
	while($row	=	$stmt->fetch()){
		$postlist[$row['id']]	=	$row;
	}

	return $postlist;
}

$parent_id = 6312;
$imageURL = 'https://powerleads.biz/users';
$from = 'admin@powerleads.biz';
$from_name = 'PowerLeads Leads Program';
$cc = 'george.farley@observint.com';
$bcc = 'mark.farley@observint.com';

$users = usersQuery($parent_id);
$start = date('Y-m-d', strtotime('-7 days'));
$end = date("Y-m-d");

foreach($users as $user){
	
	$user_id = $user['id'];
	$userinfo = new UserDataAccess();
	$ui = $userinfo->getUserInfo($user_id);
	$usersT = $userinfo->getUser($user_id);
	$to = $usersT->username;
	//$to = 'admin@6qube.com';

	$company = $ui['company'];
	echo 'Starting reports for '.$company . PHP_EOL;
	
	$sites = sitesQuery($user_id);
	foreach ($sites as $site) {

		echo 'Starting with the following connected blog = '.$site['connect_blog_id'] . PHP_EOL;
		
		$bdata = blogsQuery($site['connect_blog_id']);
		if($bdata['facebook_id']){
			list( $page_id, $pagename, $token ) = explode( "\n", $bdata['facebook_id'] );
			$page_id = preg_replace('/[^0-9]/', '', $page_id);
			try {
			$fbposts = $facebook->api('/'.$page_id.'/feed?since='.$start.'&until=now&limit=20');
			} catch ( FacebookApiException $e ) {
				
			}
			$fbcount = count($fbposts['data']);
			echo $fbcount.' Facebook Posts' . PHP_EOL;
			
		}
		if($bdata['twitter_accessid']){
			list($o_token, $o_secret, $o_screenname)    =   explode("\n", $bdata['twitter_accessid']);
			$tr	=	array('token'	=> $o_token,    'secret'    => $o_secret);
			
			$tparams = array(
								'screen_name' => $o_screenname,
								'count' => 50
								);
			$content = $auth->get( $trest, $tparams );
			$feed = json_decode($content);
			//var_dump($feed);
			$tcount = 0;
			foreach ( $feed as $data ) {
				if($data->created_at){
					$data->created_at = strtotime($data->created_at);
					$data->created_at = date('Y-m-d', $data->created_at);
					if($data->created_at >= $start) $tcount += 1;
				}
				
			}
			echo $tcount.' Twitter Posts' . PHP_EOL;
			
		}
		$totalSocialPosts = $fbcount+$tcount;
		echo $totalSocialPosts.' Total Social Posts' . PHP_EOL;
		
		$posts = bPostsQuery($site['connect_blog_id']);
		$totalBlogPosts = count($posts);
		echo $totalBlogPosts.' Total Blog Posts' . PHP_EOL;
		
		foreach ($posts as $post) {
		$blogposts .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
  <tr>
    <td valign="top" id="templateBody" style="background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 0;padding-bottom: 9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnImageBlockOuter">
          <tr>
            <td valign="top" style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner"><table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <tbody>
                  <tr>
                    <td class="mcnImageContent" valign="top" style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img align="center" alt="" src="'.$imageURL.''.$post['photo'].'" width="600" style="max-width: 1000px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage"> </td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
        </tbody>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnTextBlockOuter">
          <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> 
              
              <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
              
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                <tbody>
                  <tr>
                    <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;"><div style="text-align: center;"><span style="font-size:20px"><strong>'.$post['post_title'].'</strong></span></div></td>
                  </tr>
                </tbody>
              </table>
              
              <!--[if mso]>
				</td>
				<![endif]--> 
              
              <!--[if mso]>
				</tr>
				</table>
				<![endif]--></td>
          </tr>
        </tbody>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnTextBlockOuter">
          <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> 
              
              <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]--><!--[if mso]>
				</td>
				<![endif]--> 
              
              <!--[if mso]>
				</tr>
				</table>
				<![endif]--></td>
          </tr>
        </tbody>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnButtonBlockOuter">
          <tr>
            <td style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top" align="center" class="mcnButtonBlockInner"><table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 1px;background-color: #A5C559;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <tbody>
                  <tr>
                    <td align="center" valign="middle" class="mcnButtonContent" style="font-family: &quot;Source Sans Pro&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;font-size: 24px;padding: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><a class="mcnButton " title="READ MORE" href="http://'.$bdata['httphostkey'].'/'.$post['category_url'].'/'.$post['post_title_url'].'/" target="_blank" style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">VIEW POST</a></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
        </tbody>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnTextBlockOuter">
          <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> 
              
              <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]--><!--[if mso]>
				</td>
				<![endif]--> 
              
              <!--[if mso]>
				</tr>
				</table>
				<![endif]--></td>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td valign="top" id="templateFooter" style="background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <tbody class="mcnTextBlockOuter">
          <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> 
              
              <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]--><!--[if mso]>
				</td>
				<![endif]--> 
              
              <!--[if mso]>
				</tr>
				</table>
				<![endif]--></td>
          </tr>
        </tbody>
      </table></td>
  </tr>
</table>';
		}
		//var_dump($posts);
		
	}
	$statsTotal = statsQuery($user_id);
	echo $statsTotal.' Visited your site from Social' . PHP_EOL;
	
	if(!$totalBlogPosts){
		
	} else {
	echo 'Emailing reports to '.$to . PHP_EOL;
	///email report
	include 'inc/email_template_marketing.php';
	$ER = new EmailsDataAccess();
	$ER->sendEmailer($to, 'Weekly Marketing Report for '.$company.'', $body, $from, $from_name, $cc, $bcc);
	}
	
	$blogposts = '';
}




/*function get_largest_img($url) {
  libxml_use_internal_errors(true);
  $html = file_get_contents($url);
  $dom = new DOMDocument();
  $dom->loadHTML($html);
  $imgs = $dom->getElementsByTagname('img');
  $largest = "";
  $largest_area = 0;
  foreach ($imgs as $img) {
    $img_url = $img->getAttribute("src");
    if (empty($img_url)){
       continue;
    }
    if (substr( $img_url, 0, 2 ) === "//"){
       $img_url = "http:" . $img_url;
    } else if (substr( $img_url, 0, 1 ) === "/"){
       $img_url = $url . $img_url;
    }
    $size = getimagesize($img_url);
    if (is_suitable($size)) {
      $width = $size[0];
      $height = $size[1];
      if ($width*$height > $largest_area) {
        $largest = $img_url;
        $largest_area = $width*$height;
      }
    }
  }
  return $largest;
}
function is_suitable($size) {
  // no image for some reason
  if (is_null($size)) {
    return false;
  }
  $width = $size[0];
  $height = $size[1];
  // long images are uninteresting (eg. they can be advert banners)
  if ($width/$height > 10 || $width/$height < 0.1) {
    return false;
  }
  return true;
}*/


/*$start = strtotime('-7 days');

require_once( '/home/sapphire/production-code/dev-trunk-svn/hybrid/library/facebook-php-sdk/src/facebook.php' );

	$config = array(

		'appId' => '1604862766397665',

		'secret' => '649610b9a32a251610ffd0418caeac6a',

		'fileUpload' => false, // optional

		'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps

	);

$facebook = new Facebook( $config );

$fbid[ 'facebook_id' ] = '1462247487414561
ignitedLOCAL
EAAWznTsauOEBAHVOONIIbsOHdyIoxGoViBmTkyervew5ImeJXhNA5COWL89g8KYwBHELdkqr4lNk5fzaF1MKq9lZCM5aRNmty1mLN8lWkO1gZB5AZAuXSW0g5uJAzEkCABi2qZAxyOMt1YzFjWXtX1Ma0u4xsMTCkmyNlmheIwZDZD';

list( $page_id, $pagename, $token ) = explode( "\n", $fbid[ 'facebook_id' ] );

$page_id = preg_replace('/[^0-9]/', '', $page_id);

$fbposts = $facebook->api('/'.$page_id.'/feed?since='.$start.'&until=now&limit=20');*/


//var_dump($fbposts['data']);

/*foreach($fbposts['data'] as $fbpost){
	
	echo $fbpost['created_time'];
	preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $fbpost['message'], $match);

	//var_dump($match[0][0]);
	
	$url = $match[0][0];

	libxml_use_internal_errors(true);
	$doc = new DomDocument();
	$doc->loadHTML(file_get_contents($url));
	$xpath = new DOMXPath($doc);*/
	//$query = '//*/meta[starts-with(@property, \'og:image\')]';
	/*$metas = $xpath->query($query);

	foreach ($metas as $meta) {
		if($meta) $noImageSearch = true;
		if($meta->getAttribute('property')=='og:image:secure_url') $imageurl = $meta->getAttribute('content');
		elseif($meta->getAttribute('property')=='og:image') $imageurl = $meta->getAttribute('content');
		echo $imageurl . PHP_EOL;
	}
	
	if(!$noImageSearch){
	
		$largest = get_largest_img($url);
		$embed = empty($largest) ? $imagesnippet = false : $imagesnippet =  true;
		if($imagesnippet) echo $largest . PHP_EOL;

	}
	
}*/





?>