<?php
define('QUBE_NOLAUNCH', 1);
require_once '/home/sapphire/production-code/sofus-unstable/hybrid/bootstrap.php';
Qube::AutoLoader();

function usersQuery($parent_id){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.users u');
	$select->Fields('u.id');
	$select->Where('u.parent_id = %d', $parent_id);
	$select->Where('EXISTS (SELECT 1 FROM ranking_keywords k WHERE k.user_id = u.id)');

	$stmt	=	$select->Exec();
	
	$userlist	=	array();
	while($row	=	$stmt->fetch()){
		$userlist[$row['id']]	=	$row;
	}

	return $userlist;
}
function websitesQuery($user_id){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.hub');
	$select->Fields('tracking_id');
	$select->Where('user_id = %d', $user_id);
	$select->Where(' httphostkey != "" ');
	$select->Where(' domain != "" ');
	$select->Where(' trashed = "0000-00-00" ');
	$select->Where(' tracking_id != 0 ');
	$select->Where(' cid != "99999" ');

	$stmt	=	$select->Exec();
	
	$sitelist	=	array();
	while($row	=	$stmt->fetch()){
		$sitelist[$row['tracking_id']]	=	$row;
	}

	return $sitelist;
}
function blogsQuery($user_id){
	$pdo =	Qube::GetPDO();
	$select	=	new DBSelectQuery($pdo);
	$select->From('sapphire_6qube.blogs');
	$select->Fields('tracking_id');
	$select->Where('user_id = %d', $user_id);
	$select->Where(' httphostkey != "" ');
	$select->Where(' domain != "" ');
	$select->Where(' trashed = "0000-00-00" ');
	$select->Where(' tracking_id != 0 ');

	$stmt	=	$select->Exec();
	
	$sitelist	=	array();
	while($row	=	$stmt->fetch()){
		$bloglist[$row['tracking_id']]	=	$row;
	}

	return $bloglist;
}
function getTrackingVisits($tracking_id){
	//$start = date('Y-m-d', strtotime('-30 days'));
	//$end = date("Y-m-d");
	$start = '2018-12-01';
	$end = '2018-12-31';
	
	$piwikDataAccess = new PiwikReportsDataAccess();
    $totalSiteVisits += $piwikDataAccess->getTotalVisitsByTrackingId($tracking_id, '' . $start . ',' . $end . '');
	
	return $totalSiteVisits;
}
function phoneQuery($user_id){
	$start = date('Y-m-d', strtotime('-30 days'));
	$end = date("Y-m-d");
	$start = '2018-12-01';
	$end = '2018-12-31';
	
	$pdo =	Qube::GetPDO();
	$sql = "SELECT count(*) FROM sapphire_6qube.phone_records WHERE user_id = '".$user_id."' AND date BETWEEN '".$start."' AND '".$end."'"; 
	$result = $pdo->prepare($sql); 
	$result->execute(); 
	$number_of_rows = $result->fetchColumn(); 
	
	return $number_of_rows;
}

function leadsQuery($user_id){
	//$start = date('Y-m-d', strtotime('-30 days'));
	//$end = date("Y-m-d");
	$start = '2018-12-01';
	$end = '2018-12-31';
	
	$pdo =	Qube::GetPDO();
	$sql = "SELECT count(*) FROM sapphire_6qube.leads WHERE user_id = '".$user_id."' AND spam < 50 AND created BETWEEN '".$start."' AND '".$end."'"; 
	$result = $pdo->prepare($sql); 
	$result->execute(); 
	$number_of_rows = $result->fetchColumn(); 
	
	return $number_of_rows;
}
function rankingSitesQuery($user_id){
	
		$rda = new RankingDataAccess();
		$rankingSites = $rda->getAllRankingDataEmailReports(array(
            'user_id' => $user_id
        ));
		return $rankingSites;


}

$parent_id = 6312;
$period = 'DAY';
$interval = 30;
$loginURL = 'https://powerleads.biz';
$start = '2018-12-01';
$end = '2018-12-31';
$from = 'admin@powerleads.biz';
$from_name = 'PowerLeads Leads Program';
$cc = 'george.farley@observint.com';
$bcc = 'mark.farley@observint.com';

$users = usersQuery($parent_id);

foreach($users as $user){
	
	$user_id = $user['id'];
	$userinfo = new UserDataAccess();
	$ui = $userinfo->getUserInfo($user_id);
	$usersT = $userinfo->getUser($user_id);
	//$to = $usersT->username;
	$to = 'admin@6qube.com';

	$company = $ui['company'];
	echo 'Starting reports for '.$company . PHP_EOL;

		

		$rk = new RankingDataAccess();

		$sites = websitesQuery($user_id);
		$blogs = blogsQuery($user_id);
		$calls = phoneQuery($user_id);
		$leads = leadsQuery($user_id);
		$rankings = rankingSitesQuery($user_id);
		
		$totalSiteVisits = 0;
		$totalBlogVisits = 0;
		$totalVisits = 0;
		foreach ($sites as $site) {

			$siteVisits = getTrackingVisits($site['tracking_id']);
			$totalSiteVisits += $siteVisits;
			$totalVisits += $siteVisits; 
			echo 'Running site analytics for tracking_id '.$site['tracking_id'],' - Visits for id = '.$siteVisits . PHP_EOL;
		}
		foreach ($blogs as $blog) {

			$blogVisits = getTrackingVisits($blog['tracking_id']);
			$totalBlogVisits += $blogVisits;
			$totalVisits += $blogVisits; 
			echo 'Running blog analytics for tracking_id '.$blog['tracking_id'],' - Visits for id = '.$blogVisits . PHP_EOL;
		}
		echo 'Total Website Visits = '.$totalSiteVisits . PHP_EOL;
		echo 'Total Blog Visits = '.$totalBlogVisits . PHP_EOL;
		echo $totalVisits.' PEOPLE FOUND YOUR BUSINESS ONLINE' . PHP_EOL;
		echo $calls.' people called' . PHP_EOL;
		echo $leads.' submitted leads' . PHP_EOL;

		foreach ($rankings as $rank) {

			echo 'The following ranking_sites will be included: '.$rank->hostname . PHP_EOL;
			echo 'Number of 1st Page of Google: '.$rank->google_first . PHP_EOL;
			echo 'Number of 1st Page of Bing: '.$rank->bing_first . PHP_EOL;

			$kData = $rk->getAllKeywordDataEmailReports(array(
					'site_ID' => $rank->ID, 
					'period' => $period,
					'interval' => $interval
				));

			$num_one_spots = 0;
				$top_three_spots = 0;
				$top_20_spots = 0;
				$top_100_spots = 0;

			foreach($kData as $keyword){




				if($keyword->rank_google == 1) $num_one_spots++;
				if($keyword->rank_google > 1 && $keyword->rank_google <= 3) $top_three_spots++;
				if($keyword->rank_google > 10 && $keyword->rank_google <= 20) $top_20_spots++;
				if($keyword->rank_google > 20 && $keyword->rank_google <= 99) $top_100_spots++;

						echo $keyword->keyword.' - Google Rank '.(isset($google_rank_sing) ? $google_rank_sing : '').$keyword->rank_google.' - Change:'.(isset($google_change_sign) ? $google_change_sign : '').$keyword->google_change.' Result URL: '.$keyword->result_url.' Search Engine: '.$keyword->searchengine.' Search Engine: '.$keyword->pagenumber . PHP_EOL . PHP_EOL;


			}

				echo $num_one_spots. PHP_EOL;
				echo $top_three_spots. PHP_EOL;
				echo $top_20_spots. PHP_EOL;
				echo $top_100_spots. PHP_EOL;


		}
		echo 'Emailing reports to '.$to . PHP_EOL;
		///email report
		include 'inc/email_template.php';
		$ER = new EmailsDataAccess();
		$ER->sendEmailer($to, 'Monthly Activity Report for '.$company.'', $body, $from, $from_name, $cc, $bcc);

}
?>