<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

\ini_set('display_errors', true);
error_reporting(E_ALL ^ E_STRICT);

require '../../bootstrap.php';

Qube::AutoLoader();
@session_start();

require_once QUBEADMIN . 'inc/analytics.class.php';

$stats = new Analytics();
$query = "SELECT h.id, h.user_id, h.cid, h.tracking_id, h.domain, h.company_name, h.phone, h.address, h.city, h.state, u.account_number, u.api_id, r.role_ID "
        . "FROM blogs h, user_info u, 6q_userroles r "
        . "WHERE h.user_parent_id = '6312' "
        . "AND h.trashed = '0000-00-00' "
        . "AND h.httphostkey != '' "
        . "AND h.domain != '' "
        . "AND tracking_id != 0 "
        . "AND h.user_id = u.user_id "
        . "AND h.user_id = r.user_ID "
        . "ORDER BY h.company_name ASC";

$hubs = $stats->query($query);

var_dump($stats->numRows($hubs));




// Output CSV file
$date_time = date("Y-m-d");
$time = time();
$file = "test_reports/{$date_time}_{$time}_bv-report.csv";
//print($file);

// Create file pointer to the output stream.
@$csv_file_handler = fopen($file, 'w');

// Output the column headings.
fputcsv($csv_file_handler, 
        array(
            'Account Type', 
            'SFDC ID', 
            'Account Number', 
            'Blog ID', 
            'User ID', 
            'Tracking ID', 
            'Company Name', 
            'Domain',
            'Phone', 
            'Address', 
            'City', 
            'State', 
            'Visits')
        );

fclose($csv_file_handler);