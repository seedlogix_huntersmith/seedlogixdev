<?php
    // put your code here
    //ini_set('display_errors', false);
    //error_reporting(E_ALL ^ E_STRICT);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>SeedLogix Reports</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="jquery-ui-1.12.1/jquery-ui.min.css" />
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    </head>
    <?php
        @$type = $_GET["type"];
        @$start = $_GET["start"];
        @$end = $_GET["end"];
        @$token = $_GET["token"];
        @$generate = $_GET["gen"];
        @$admin = $_GET["admin"];
    ?>
    <body>
        <div class="container">
            <?php if ($type === "blog-visit" || empty($type)) : ?>
            <h3>Blog Visit Reports List <a href="index.php?token=<?php echo $token ?><?php echo !empty($type) ? "&type={$type}" : null ?>" class="btn btn-default pull-right"><i class="glyphicon glyphicon-refresh"></i> Refresh</a></h3>
            <?php elseif ($type === "visit") : ?>
            <h3>Visit Reports List <a href="index.php?token=<?php echo $token ?><?php echo !empty($type) ? "&type={$type}" : null ?>" class="btn btn-default pull-right"><i class="glyphicon glyphicon-refresh"></i> Refresh</a></h3>
            <?php endif ?>
            
            <hr />
            
            <?php if ($token === 'ahecSW2VAKxGBcZK2DhqZzB'): ?>
            <script>
                $(function() {
                    var date_format = "yy-mm-dd";
                    var start_date = $("#startDate").val();
                    var end_date = $("#endDate").val();
                    
                    $("#startDate").datepicker({dateFormat: date_format});
                    $("#endDate").datepicker({dateFormat: date_format});
                    
                    $("#startDate").on('change', function() {
                        start_date = $(this).val();
                        //console.log(start_date);
                    });
                    
                    $("#endDate").on('change', function() {
                        end_date = $(this).val();
                        //console.log(end_date);
                    });
                    
                    $("#generateReport").on('click', function() {
                        <?php if ($type === "blog-visit" || empty($type)) : ?>
                        var url = "index.php?type=blog-visit&start=" + start_date + "&end=" + end_date + "&token=ahecSW2VAKxGBcZK2DhqZzB&gen=1";
                        <?php elseif ($type === "visit") : ?>
                        var url = "index.php?type=visit&start=" + start_date + "&end=" + end_date + "&token=ahecSW2VAKxGBcZK2DhqZzB&gen=1";
                        
                        if ($("#adminSwitch").prop('checked')) {
                            url += "&admin=1";
                        }
                        <?php endif ?>
                            console.log(url);
                            window.location.href = url;
                    });
                });
            </script>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="reportType">Report Type:</label>
                        <select class="form-control" id="reportType" onchange="location = this.value;">
                            <option selected="selected">-- Choose Report Type --</option>
                            <option value="index.php?type=blog-visit&token=ahecSW2VAKxGBcZK2DhqZzB">Blog Visits</option>
                            <option value="index.php?type=visit&token=ahecSW2VAKxGBcZK2DhqZzB">Visits</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="startDate">Start Date:</label>
                        <input class="form-control" type="text" id="startDate" value="<?php echo empty($start) ? date("Y-m-d") : $start ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="endDate">End Date:</label>
                        <input class="form-control" type="text" id="endDate" value="<?php echo empty($end) ? date("Y-m-d") : $end ?>" />
                    </div>
                </div>
            </div>
            
            <?php if ($type === "visit") : ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="endDate">Admin:</label>
                        <input id="adminSwitch" data-toggle="toggle" type="checkbox" />
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    <?php if (!empty($admin)) : ?>
                    $('#adminSwitch').bootstrapToggle('on');
                    <?php endif ?>
                    $('#adminSwitch').change(function() {
                        /*if ($(this).prop('checked')) {
                            console.log('admin on');
                        } else {
                            console.log('admin off');
                        }*/
                    });
                });
            </script>
            <?php endif ?>
            
            <br />
            
            <div class="row">
                <div class="col-md-6"><button id="generateReport" class="btn btn-success">Generate Report</button></div>
            </div>
            
            <hr />
            
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Reports</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        
                        if ($type === "blog-visit" || empty($type)) {
                            $dir = 'blog_visit_reports';
                        
                        } else if ($type === "visit") {
                            $dir = 'visit_reports';
                            
                        }
                        
                        $reports = scandir($dir);
                        $reports_reversed = array_reverse($reports);
                        //print_r($reports);
                        //var_dump($dir);
                    ?>
                    <?php foreach ($reports_reversed as $report) : ?>
                        <?php if ($report != "." && $report != ".." && $report != ".svn"): ?>
                        <tr>
                            <td><?php echo $report ?></td>
                            <td><a class="btn btn-xs btn-primary" href="<?php echo "{$dir}/{$report}" ?>" download="<?php echo $report ?>">Download Report</a></td>
                        </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
            <?php
            
                if ($generate === "1") {
                    require '../../bootstrap.php';

                    Qube::AutoLoader();
                    @session_start();

                    require_once QUBEADMIN . 'inc/analytics.class.php';

                    if ($type === "blog-visit" || empty($type)) {
                        // Output CSV file
                        $date_time = date("Y-m-d");
                        $time = time();
                        $file = "blog_visit_reports/{$date_time}_{$time}_{$start}_{$end}bv-report.csv";
                        //print($file);

                        // Create file pointer to the output stream.
                        @$csv_file_handler = fopen($file, 'w');

                        // Output the column headings.
                        fputcsv($csv_file_handler, 
                                array(
                                    'Account Type', 
                                    'NetSuite ID', 
                                    'Blog ID', 
                                    'User ID', 
                                    'Tracking ID', 
                                    'Company Name', 
                                    'Domain',
                                    'Phone', 
                                    'Address', 
                                    'City', 
                                    'State', 
                                    'Visits')
                                );

                        $stats = new Analytics();
                        $query = "SELECT h.id, h.user_id, h.cid, h.tracking_id, h.domain, h.company_name, h.phone, h.address, h.city, h.state, u.account_number, u.api_id, r.role_ID "
                                . "FROM blogs h, user_info u, 6q_userroles r "
                                . "WHERE h.user_parent_id = '6312' "
                                . "AND h.trashed = '0000-00-00' "
                                . "AND h.httphostkey != '' "
                                . "AND h.domain != '' "
                                . "AND tracking_id != 0 "
                                . "AND h.user_id = u.user_id "
                                . "AND h.user_id = r.user_ID "
                                . "ORDER BY h.company_name ASC";

                        $hubs = $stats->query($query);

                        while ($row = $stats->fetchArray($hubs)) {
                            if ($row['role_ID'] == 51) {
                                $role_name = 'PowerLeads';

                            } else if ($row['role_ID'] == 24) {
                                $role_name = 'PowerLeads Plus';

                            } else if ($row['role_ID'] == 722) {
                                $role_name = 'PowerLeads Plus';

                            }

                            if (true) {
                                $piwikDataAccess = new PiwikReportsDataAccess();
                                $r0 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], '' . $start . ',' . $end . '');

                            } else {
                                 $r0 = $r1 = $r2 = 'test';

                            }

                            fputcsv($csv_file_handler, 
                                    array(
                                        $role_name, 
                                        $row['api_id'],  
                                        $row['id'], 
                                        $row['user_id'], 
                                        $row['tracking_id'], 
                                        $row['company_name'], 
                                        $row['domain'], 
                                        $row['phone'], 
                                        $row['address'], 
                                        $row['city'], 
                                        $row['state'], 
                                        $r0)
                                    );
                            flush();
                        }
                        
                        fclose($csv_file_handler);
                        
                    } else if ($type === "visit") {
                        $is_dev = false;
                        
                        // Output CSV file
                        $date_time = date("Y-m-d");
                        $time = time();
                        $file = "visit_reports/{$date_time}_{$time}_{$start}_{$end}_v-report.csv";
                        //print($file);

                        // Create file pointer to the output stream.
                        @$csv_file_handler = fopen($file, 'w');
                        
                        $stats = new Analytics();
                        
                        if (empty($admin)) {
                            $admin = "0";
                        }
                        
                        //var_dump($admin);
                        // output the column headings
                        if ($admin === "1") {
                            fputcsv($csv_file_handler, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'City', 'State', 'Visits', 'Leads'));
                            
                            $query = "SELECT id, user_id, cid, name, tracking_id, domain, company_name, phone, city, state "
                                . "FROM hub "
                                . "WHERE user_id = '6312' "
                                . "AND trashed = '0000-00-00' "
                                . "AND hub_parent_id != '18953' "
                                . "AND hub_parent_id != '23395' "
                                . "AND hub_parent_id != '8015' "
                                . "AND hub_parent_id != '18951' "
                                . "AND hub_parent_id != '18953' "
                                . "AND hub_parent_id != '18952' "
                                . "AND hub_parent_id != '22671' "
                                . "AND domain != 'http://feed1.powerleads.biz' "
                                . "AND domain != 'http://feed2.powerleads.biz' "
                                . "AND domain != 'http://feed3.powerleads.biz' "
                                . "AND domain != 'http://feed4.powerleads.biz' "
                                . "AND domain != 'http://www.alibidealer.com' "
                                . "AND hub_parent_id != 0 "
                                . "AND httphostkey != '' "
                                . "AND domain != '' "
                                . "AND tracking_id != 0 "
                                . "AND cid != '99999' "
                                . "ORDER BY name ASC";
                            
                        } else {
                            fputcsv($csv_file_handler, array('Account Type', 'NetSuite ID', 'Website ID', 'User ID', 'Tracking ID', 'Theme',
                                'Domain', 'Creation Date', 'Logo', 'Company Name', 'First Name', 'Last Name', 'Email', 'Phone', 'Address', 'City', 'State', 'Zip', 'Visits', 'Leads'));
                            
                            $query = "SELECT h.id, h.user_id, h.cid, h.name, h.tracking_id, h.hub_parent_id, h.domain, h.company_name, h.phone, h.street, h.city, h.state, h.zip, h.created, h.logo, u.account_number, u.profile_photo, u.api_id, u.firstname, u.lastname, i.username, r.role_ID
                                FROM hub h, user_info u, 6q_userroles r, users i 
                                WHERE h.user_parent_id = '6312'
                                AND h.trashed = '0000-00-00'
                                AND h.hub_parent_id != '18953'
                                AND h.hub_parent_id != '23395'
                                AND h.hub_parent_id != '8015'
                                AND h.hub_parent_id != '18951'
                                AND h.hub_parent_id != '18953'
                                AND h.hub_parent_id != '18952'
                                AND h.hub_parent_id != '22671'
                                AND h.domain != 'http://feed1.powerleads.biz'
                                AND h.domain != 'http://feed2.powerleads.biz'
                                AND h.domain != 'http://feed3.powerleads.biz'
                                AND h.domain != 'http://feed4.powerleads.biz'
                                AND h.domain != 'http://www.alibidealer.com'
                                AND h.hub_parent_id != 0
                                AND h.httphostkey != ''
                                AND h.domain != ''
                                AND tracking_id != 0
                                AND h.cid != '99999'
                                AND h.user_id = u.user_id
                                AND h.user_id = r.user_ID AND h.user_id = i.id
                                ORDER BY h.name ASC";
                        }
                        
                        $hubs = $stats->query($query);
                        
                        //instantiate blog class for blog section in right panel
                        //get analytics info

                        // loop over the rows, outputting them
                        //fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip'));
                        
                        while ($row = $stats->fetchArray($hubs)) {
                            $query2 = "SELECT count(id) "
                                . "FROM leads "
                                . "WHERE hub_id = '{$row['id']}' "
                                . "AND spam < 50 "
                                . "AND created < '{$end} 23:59:59' "
                                . "AND created > '{$start} 00:00:00'";
                            
                            $leads1 = $stats->queryFetch($query2);
                            
                            $query3 = "SELECT count(id) "
                                . "FROM contact_form "
                                . "WHERE type_id = '{$row['id']}' "
                                . "AND spamstatus = '0' "
                                . "AND created < '{$end} 23:59:59' "
                                . "AND created > '{$start} 00:00:00'";
                                
                            $leads2 = $stats->queryFetch($query3);
                            
                            $numProspects = $leads1['count(id)'] + $leads2['count(id)'];
                            
                            if ($row['role_ID'] == 51) {
                                $role_name = 'PowerLeads';
                                
                            }
                            
                            if ($row['role_ID'] == 24) { 
                                $role_name = 'PowerLeads Plus';
                                
                            }
							
							if ($row['role_ID'] == 722) { 
                                $role_name = 'PowerLeads Custom';
                                
                            }
                            
                            if ($row['hub_parent_id'] == 108931) {
                                if ($row['user_id'] == 6312) { 
                                    $theme = 'Theme 1 - SC Branded';
                                    
                                } else if (strpos($row['domain'], 'alibidealer.com')) {
                                    $theme = 'Alibi Dealer';
                                    
                                } else { 
                                    $theme = 'Theme 1';
                                    
                                }
                            }
                            
                            if ($row['hub_parent_id'] == 116695) {
                                if ($row['user_id'] == 6312) { 
                                    $theme = 'Theme 2 - SC Branded';
                                    
                                } else { 
                                    $theme = 'Theme 2';
                                    
                                }
                            }
                            
                            if ($row['hub_parent_id'] == 117342) {
                                if ($row['user_id'] == 6312) { 
                                    $role_name = 'Theme 3 - SC Branded';
                                    
                                }
                                
                                $theme = 'Theme 3';
                                
                            }
                            
                            if ($row['hub_parent_id'] == 20561) { 
                                $theme = 'SC Branded';
                                
                            }
                            
                            if ($row['logo']) { 
                                $logo = 'Yes';
                                
                            } else if ($row['profile_photo']) {
                                $logo = 'Yes';
                                
                            } else {
                                $logo = 'No';
                                
                            }
                            
                            if ($is_dev == false) {
                                $piwikDataAccess = new PiwikReportsDataAccess();
                                $r0 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
                                
                            } else {
                                $r0 = $r1 = $r2 = 'test';
                                
                            }
                            
                            if ($admin == 1) {
                                fputcsv($csv_file_handler, array(
                                    $row['id'], 
                                    $row['user_id'], 
                                    $row['tracking_id'], 
                                    $row['company_name'], 
                                    $row['domain'], 
                                    $row['phone'], 
                                    $row['city'], 
                                    $row['state'],
                                    $r0,
                                    $numProspects));
                                
                            } else {
								

									
                                fputcsv($csv_file_handler, array(
                                    $role_name, 
                                    $row['api_id'],  
                                    $row['id'], 
                                    $row['user_id'], 
                                    $row['tracking_id'], 
                                    $theme, 
									$row['domain'], 
									$row['created'],
                                    $logo, 
                                    $row['company_name'], 
                                    $row['firstname'], 
									$row['lastname'], 
									$row['username'], 
                                    $row['phone'], 
                                    $row['street'], 
                                    $row['city'], 
                                    $row['state'], 
                                    $row['zip'],
                                    $r0,
                                    $numProspects));
                                
                            }
                            
                            flush();
                            
                        }
                        
                        fclose($csv_file_handler);
                    }
                } 
             
            ?>
            <?php else : ?>
            <div class="alert alert-danger">
                <h4 style="margin-bottom: 0;">You do not have permission to access 'Blog Visit Reports'.</h4>
            </div>
            <?php endif ?>
        </div>
    </body>
</html>
