<?php 
$body = '<!doctype html public "- / /w3c / /dtd xhtml 1.0 strict / /en" "http: / /www.w3.org /tr /xhtml1 /dtd /xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="target-densitydpi=device-dpi">
<meta name="format-detection" content="telephone=no">
<meta name="x-apple-disable-message-reformatting">
<title>My Business</title>
<style type="text/css">
.apple-links-blue a {
	color: #1155cc !important;
	text-decoration: none !important;
	border: none !important;
}
a.blue-links {
	color: #1155cc !important;
	text-decoration: none !important;
	border: none !important;
}
.apple-links-grey a {
	color: #444444 !important;
	text-decoration: none !important;
	border: none !important;
}
a.grey-links {
	color: #444444 !important;
	text-decoration: none !important;
	border: none !important;
}
/* Force Hotmail to display emails at full width */
.ExternalClass {
	width: 100%;
}
/* Force Hotmail to display normal line spacing. */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}
#backgroundTable {
	margin: 0;
	padding: 0;
	width: 100% !important;
	line-height: 100% !important;
}
/* Hotmail symbol fix */
.symbolfix img {
	width: 10px !important;
	height: 10px !important;
}
a[href^=tel] {
	text-decoration: none;
}

@media only screen and (-webkit-min-device-pixel-ratio : 1.5), only screen and (min-device-pixel-ratio : 1.5) {
}

@media only screen and (-webkit-min-device-pixel-ratio: 2) {
}

@media only screen and (-webkit-device-pixel-ratio:.75) {
}

@media only screen and (-webkit-device-pixel-ratio:1) {
}

@media only screen and (-webkit-device-pixel-ratio:1.5) {
}
<!--
[if IEMobile 7]>  <![endif]
-->
</style>
<style type="text/css">
.yahoo {
	display: none !important;
}
</style>

<!--[if (lte mso 7)|(gte mso 9)]>
      <style type="text/css">
        #outlook {
           table-width: fixed;
         width:600px !important;
          
        }
        .outlooktd {
          width:600px !important;
          
        }
        
        
      </style>
    <![endif]-->

<!--[if gte mso 9]>
      <style type="text/css">
          <xml>
            <o:OfficeDocumentSettings>
              <o:AllowPNG/>
              <o:PixelsPerInch>96</o:PixelsPerInch>
           </o:OfficeDocumentSettings>
          </xml>
      </style>
    <![endif]-->
<style type="text/css">
@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (min-device-width: 374px) and (max-device-width: 376px), only screen and (min-device-width: 413px) and (max-device-width: 415px), only screen and (min-device-width: 319px) and (max-device-width: 321px), only screen and (device-width: 375px) and (orientation: portrait), only screen and (device-width: 414px) and (orientation: portrait), only screen and (device-width: 360px) {
.copy.company-name {
	padding: 0 20px 30px 20px !important
}
.desktop {
	display: none !important;
	height: 0 !important
}
.mobile {
	display: block !important
}
.keywords-table {
	-premailer-align: center !important;
	width: 300px !important
}
.keywords-table .copy.subheadline {
	-premailer-align: center !important;
	font-size: 16px !important;
	line-height: 24px !important;
	text-align: center !important;
	width: 300px !important;
	padding: 0 !important
}
.keywords-table .copy.p {
	-premailer-align: center !important;
	font-size: 12px !important;
	line-height: 20px !important;
	padding: 5px 0 19px 0 !important;
	text-align: center !important;
	width: 300px !important
}
.keywords-table .white-divider img {
	width: 150px !important;
	height: 2px !important;
	display: inline-block !important
}
.insights-table-keywords {
	max-width: 424px !important
}
.insights-table-keywords .copy.insights-icon {
	vertical-align: top !important
}
.insights-table-keywords .copy.insights-icon img {
	vertical-align: top !important
}
.insights-table-keywords .copy.copy-insights-keywords {
	font-weight: 400 !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait) {
section .display-on-mobile {
	display: block !important;
	visibility: visible !important
}
section tr.display-on-mobile {
	display: table-row !important
}
section td.display-on-mobile {
	display: table-cell !important
}
section .display-on-desktop {
	display: none !important;
	visibility: hidden !important
}
section td.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section table.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section td.mobile {
	display: block !important
}
section table.mobile {
	display: block !important
}
section img.mobile {
	display: block !important;
	height: auto !important
}
section .inner {
	padding: 10px !important
}
section #logo {
	height: 26px !important;
	padding: 30px 0 20px 0 !important;
	-premailer-align: center !important
}
section #logo img {
	height: 26px !important
}
section .mobile-empty {
	-premailer-height: 10 !important;
	height: 10px !important
}
section .mobile-empty.mobile-no-height {
	height: 0 !important
}
section .mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
section .mobile-empty-bg.mobile-no-height {
	height: 0 !important
}
section .one-column table {
	width: 100% !important
}
section .copy.company-name {
	font-size: 34px !important;
	line-height: 40px !important
}
section .copy.company-name .not-a-link {
	color: #9b9b9b !important;
	text-decoration: none !important
}
section .copy.company-name a {
	color: #9b9b9b !important;
	cursor: text !important;
	text-decoration: none !important
}
section .copy.views-mobile {
	font-size: 24px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
section .copy.copy-insights-mobile {
	display: none !important;
	visibility: hidden !important
}
section .copy.copy-insights-mobile-display {
	display: block !important;
	visibility: visible !important
}
section .copy.copy-insights {
	font-weight: 400 !important
}
section .copy.quote {
	font-weight: 400 !important
}
section .yahoo {
	display: none !important
}
.inner {
	padding: 10px !important
}
.mobile-empty {
	-premailer-height: 30 !important;
	height: 30px !important
}
.mobile-empty.mobile-no-height {
	height: 0 !important
}
.mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
.mobile-empty-bg.mobile-no-height {
	height: 0 !important
}
.copy.padding-icon-right {
	padding: 0 5px 0 0 !important
}
.copy.copy-insights {
	font-weight: 400 !important
}
.copy.copy-insights.padding-icon-right {
	width: 40px !important
}
.copy.views-mobile {
	font-size: 22px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
.copy.quote {
	font-weight: 400 !important
}
.copy.insights-icon {
	vertical-align: top !important
}
.copy.insights-icon img {
	vertical-align: text-top !important
}
#footer-table {
	width: 480px !important;
	-premeailer-align: center !important
}
#ios-gmail-fix {
	display: none !important
}
.fix-for-gmail {
	display: none !important
}
.inbox-fix {
	-premailer-width: 480 !important
}
.inbox-fix td {
	-premailer-width: 80 !important
}
.inbox-fix td img {
	height: 1px !important;
	width: 80px !important
}
}

@media only screen and (device-width: 360px) and (orientation: portrait) {
section .display-on-mobile {
	display: block !important;
	visibility: visible !important
}
section tr.display-on-mobile {
	display: table-row !important
}
section td.display-on-mobile {
	display: table-cell !important
}
section .display-on-desktop {
	display: none !important;
	visibility: hidden !important
}
section .inner {
	padding: 10px !important
}
section td.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section table.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section td.mobile {
	display: block !important
}
section table.mobile {
	display: block !important
}
section img.mobile {
	display: block !important;
	height: auto !important
}
section .one-column table {
	width: 100% !important
}
section #logo {
	height: 26px !important;
	padding: 30px 0 20px 0 !important;
	-premailer-align: center !important
}
section #logo img {
	height: 26px !important
}
section .mobile-empty {
	-premailer-height: 10 !important;
	height: 10px !important
}
section .mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
section .copy.company-name {
	font-size: 34px !important;
	line-height: 40px !important
}
section .copy.views-mobile {
	font-size: 24px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
section .copy.copy-insights.padding-icon-right {
	width: 40px !important
}
section .copy.copy-insights-mobile {
	display: none !important;
	visibility: hidden !important
}
section .copy.copy-insights-mobile-display {
	display: block !important;
	visibility: visible !important
}
section .copy.copy-insights {
	font-weight: 400 !important
}
section .copy.quote {
	font-weight: 400 !important
}
section .yahoo {
	display: none !important
}
.inner {
	padding: 10px !important
}
.mobile-empty {
	-premailer-height: 30 !important;
	height: 30px !important
}
.mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
.copy.padding-icon-right {
	padding: 0 5px 0 0 !important
}
.copy.copy-insights {
	font-weight: 400 !important
}
.copy.views-mobile {
	font-size: 22px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
.copy.quote {
	font-weight: 400 !important
}
.copy.insights-icon {
	vertical-align: top !important
}
.copy.insights-icon img {
	vertical-align: text-top !important
}
.copy.footer-table {
	-premailer-width: 100% !important;
	width: 440px !important
}
.copy.footer-table-inside {
	width: 440px !important
}
.fix-for-gmail {
	display: none !important
}
#footer-table {
	width: 480px !important;
	-premeailer-align: center !important
}
.inbox-fix {
	-premailer-width: 480 !important
}
.inbox-fix td {
	-premailer-width: 80 !important
}
.inbox-fix td img {
	height: 1px !important;
	width: 80px !important
}
#ios-gmail-fix {
	display: none !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (min-device-width: 374px) and (max-device-width: 376px), only screen and (min-device-width: 413px) and (max-device-width: 415px), only screen and (min-device-width: 319px) and (max-device-width: 321px), only screen and (device-width: 375px) and (orientation: portrait), only screen and (device-width: 414px) and (orientation: portrait), only screen and (device-width: 360px) {
.awx-td-actions-3-stat .insights-section-default-3-stat .copy.copy-insights-awx {
	min-width: 230px !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (device-width: 360px) and (orientation: portrait) {
div > u + .body .insights-section-awx-3-stat {
	width: 240px !important
}
div > u + .body .insights-section-awx-2-stat {
	width: 240px !important
}
.insights-section-awx-3-stat, .insights-section-awx-2-stat {
	width: 285px !important
}
.copy.copy-insights-awx {
	font-weight: 300 !important
}
.display-desktop {
	display: none !important
}
div > u + .body .display-inbox {
	display: none !important
}
.display-inbox {
	display: block !important
}
div > u + .body .display-gmail {
	display: none !important
}
.display-gmail {
	display: none !important
}
}
</style>
</head>
<body class="body" style="background-color:#ffffff; font-size:0; line-height:0; margin:0; padding:0" bgcolor="#ffffff">
<div style="clear: both;display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;">Blog Posts & Social Posts ('.$start.' - '.$end.')</div>
<section>
  <style>
        
      </style>
  <table width="100%" align="center" class="main-container" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
    <tr>
      <td><!--[if gte mso 9]>
      <table id="outlook" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
        <tr>
          <td class="outlooktd" align="center">
      <![endif]-->
        
        <table class="email-container" dir="ltr" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; width:100%" align="center" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td><table id="masthead" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; background-color:#ffffff; margin:0 auto; padding:0; width:100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                <tr>
                  <td id="logo" align="center" style="-premailer-align:center; height:40px; padding:20px 0 10px 0" height="40"><img src="https://powerleads.biz/users/u6/6312/smaller_logo.png" height="40" alt="PowerLeads" title="PowerLeads" border="0" style="border:0; display:block; height:40px; image-rendering:auto; mso-line-height-rule:exactly"></td>
                </tr>
                <tr>
                  <td class="copy company-name" align="center" width="100%" style="-premailer-align:center; color:#9b9b9b; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:40px; font-weight:normal; line-height:48px; padding:0 0 10px 0; padding-bottom:10px; text-align:center"><span class="not-a-link" style="color:#9b9b9b; text-decoration:none">'.$company.'</span></td>
                </tr>
              </table>
              <table bgcolor="#ffffff" width="100%" class="actions-table" dir="ltr" style="-premailer-cellpadding:0; -premailer-cellspacing:0; border-top:2px solid #f5f5f5; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="-premailer-cellpadding:0; -premailer-cellspacing:0; background-color:#ffffff; margin:0 auto; max-width:600px; min-width:420px; padding:0" bgcolor="#ffffff">
                      <tr>
                        <td class="empty mobile-empty " height="25" colspan="1" style="font-size:0; line-height:25px; mso-line-height-rule:exactly">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="copy headline" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; line-height:24px; padding:0 0 3px 0; text-align:center">POSTS TO YOUR BLOG & SOCIAL PLATFORMS LAST WEEK</td>
                      </tr>
                      <tr>
                        <td><table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td class="empty copy empty-line" style="-premailer-align:center; -premeailer-height:5; font-size:5px; line-height:5px; mso-line-height-rule:exactly" align="center">&nbsp;</td>
                              <td class="copy dark-gray-line-desktop dark-gray-line-mobile" style="-premailer-align:center; -premailer-width:100; -premeailer-height:5; background-color:#9b9b9b; color:#9b9b9b; font-size:5px; line-height:5px" align="center" width="100" bgcolor="#9b9b9b">&nbsp;</td>
                              <td class="empty copy empty-line" style="-premailer-align:center; -premeailer-height:5; font-size:5px; line-height:5px; mso-line-height-rule:exactly" align="center">&nbsp;</td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center">
                      <tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center"><!--[if (gte mso 9)|(IE)]>
              <table width="100%">
                <tr>
                  <td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$totalBlogPosts.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center"> Blog Posts</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>   
								</table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
              </td><td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$totalSocialPosts.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center"> Social Posts</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>
                                    
                                  </table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
                  </td>
                </tr>
              </table>
            <![endif]--></td>
                      </tr>
					<tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center"><!--[if (gte mso 9)|(IE)]>
              <table width="100%">
                <tr>
                  <td width="50%" valign="top">
            <![endif]--><!--[if (gte mso 9)|(IE)]>
                  </td>
                </tr>
              </table>
            <![endif]--></td>
                      </tr>
                      <tr>
                        <td align="center" valign="top">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td class="empty mobile-empty " height="28" colspan="1" style="font-size:0; line-height:28px; mso-line-height-rule:exactly">&nbsp;</td>
                </tr>
              </table>

            </td>
          </tr>
        </table>

      <!--[if mso ]>
            </td>
          </tr>
        </table>
        <![endif]--></td>
    </tr>
  </table>
</section>
<center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
                        <!-- BEGIN TEMPLATE // -->
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                        <![endif]-->
                        '.$blogposts.'
						
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
				<tr>
                        <td align="center" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:14px; font-style:italic; line-height:24px; padding:0 0 3px 0; text-align:center">NOTE:  This weekly content marketing activity is specifically designed to help your site rank for online search of targeted keywords in your territory.</td>
                      </tr>
            </table>
	 
	 
</center>	
	
	<div style="clear: both;display: block;width: 100%;height: 30px;"><br/>
  <br/>
</div>
</body></html>
';
?>