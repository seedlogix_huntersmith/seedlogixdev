<?php 
$body = '<!doctype html public "- / /w3c / /dtd xhtml 1.0 strict / /en" "http: / /www.w3.org /tr /xhtml1 /dtd /xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="target-densitydpi=device-dpi">
<meta name="format-detection" content="telephone=no">
<meta name="x-apple-disable-message-reformatting">
<title>My Business</title>
<style type="text/css">
.apple-links-blue a {
	color: #1155cc !important;
	text-decoration: none !important;
	border: none !important;
}
a.blue-links {
	color: #1155cc !important;
	text-decoration: none !important;
	border: none !important;
}
.apple-links-grey a {
	color: #444444 !important;
	text-decoration: none !important;
	border: none !important;
}
a.grey-links {
	color: #444444 !important;
	text-decoration: none !important;
	border: none !important;
}
/* Force Hotmail to display emails at full width */
.ExternalClass {
	width: 100%;
}
/* Force Hotmail to display normal line spacing. */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}
#backgroundTable {
	margin: 0;
	padding: 0;
	width: 100% !important;
	line-height: 100% !important;
}
/* Hotmail symbol fix */
.symbolfix img {
	width: 10px !important;
	height: 10px !important;
}
a[href^=tel] {
	text-decoration: none;
}

@media only screen and (-webkit-min-device-pixel-ratio : 1.5), only screen and (min-device-pixel-ratio : 1.5) {
}

@media only screen and (-webkit-min-device-pixel-ratio: 2) {
}

@media only screen and (-webkit-device-pixel-ratio:.75) {
}

@media only screen and (-webkit-device-pixel-ratio:1) {
}

@media only screen and (-webkit-device-pixel-ratio:1.5) {
}
<!--
[if IEMobile 7]>  <![endif]
-->
</style>
<style type="text/css">
.yahoo {
	display: none !important;
}
</style>

<!--[if (lte mso 7)|(gte mso 9)]>
      <style type="text/css">
        #outlook {
           table-width: fixed;
         width:600px !important;
          
        }
        .outlooktd {
          width:600px !important;
          
        }
        
        
      </style>
    <![endif]-->

<!--[if gte mso 9]>
      <style type="text/css">
          <xml>
            <o:OfficeDocumentSettings>
              <o:AllowPNG/>
              <o:PixelsPerInch>96</o:PixelsPerInch>
           </o:OfficeDocumentSettings>
          </xml>
      </style>
    <![endif]-->
<style type="text/css">
@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (min-device-width: 374px) and (max-device-width: 376px), only screen and (min-device-width: 413px) and (max-device-width: 415px), only screen and (min-device-width: 319px) and (max-device-width: 321px), only screen and (device-width: 375px) and (orientation: portrait), only screen and (device-width: 414px) and (orientation: portrait), only screen and (device-width: 360px) {
.copy.company-name {
	padding: 0 20px 30px 20px !important
}
.desktop {
	display: none !important;
	height: 0 !important
}
.mobile {
	display: block !important
}
.keywords-table {
	-premailer-align: center !important;
	width: 300px !important
}
.keywords-table .copy.subheadline {
	-premailer-align: center !important;
	font-size: 16px !important;
	line-height: 24px !important;
	text-align: center !important;
	width: 300px !important;
	padding: 0 !important
}
.keywords-table .copy.p {
	-premailer-align: center !important;
	font-size: 12px !important;
	line-height: 20px !important;
	padding: 5px 0 19px 0 !important;
	text-align: center !important;
	width: 300px !important
}
.keywords-table .white-divider img {
	width: 150px !important;
	height: 2px !important;
	display: inline-block !important
}
.insights-table-keywords {
	max-width: 424px !important
}
.insights-table-keywords .copy.insights-icon {
	vertical-align: top !important
}
.insights-table-keywords .copy.insights-icon img {
	vertical-align: top !important
}
.insights-table-keywords .copy.copy-insights-keywords {
	font-weight: 400 !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait) {
section .display-on-mobile {
	display: block !important;
	visibility: visible !important
}
section tr.display-on-mobile {
	display: table-row !important
}
section td.display-on-mobile {
	display: table-cell !important
}
section .display-on-desktop {
	display: none !important;
	visibility: hidden !important
}
section td.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section table.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section td.mobile {
	display: block !important
}
section table.mobile {
	display: block !important
}
section img.mobile {
	display: block !important;
	height: auto !important
}
section .inner {
	padding: 10px !important
}
section #logo {
	height: 26px !important;
	padding: 30px 0 20px 0 !important;
	-premailer-align: center !important
}
section #logo img {
	height: 26px !important
}
section .mobile-empty {
	-premailer-height: 10 !important;
	height: 10px !important
}
section .mobile-empty.mobile-no-height {
	height: 0 !important
}
section .mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
section .mobile-empty-bg.mobile-no-height {
	height: 0 !important
}
section .one-column table {
	width: 100% !important
}
section .copy.company-name {
	font-size: 34px !important;
	line-height: 40px !important
}
section .copy.company-name .not-a-link {
	color: #9b9b9b !important;
	text-decoration: none !important
}
section .copy.company-name a {
	color: #9b9b9b !important;
	cursor: text !important;
	text-decoration: none !important
}
section .copy.views-mobile {
	font-size: 24px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
section .copy.copy-insights-mobile {
	display: none !important;
	visibility: hidden !important
}
section .copy.copy-insights-mobile-display {
	display: block !important;
	visibility: visible !important
}
section .copy.copy-insights {
	font-weight: 400 !important
}
section .copy.quote {
	font-weight: 400 !important
}
section .yahoo {
	display: none !important
}
.inner {
	padding: 10px !important
}
.mobile-empty {
	-premailer-height: 30 !important;
	height: 30px !important
}
.mobile-empty.mobile-no-height {
	height: 0 !important
}
.mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
.mobile-empty-bg.mobile-no-height {
	height: 0 !important
}
.copy.padding-icon-right {
	padding: 0 5px 0 0 !important
}
.copy.copy-insights {
	font-weight: 400 !important
}
.copy.copy-insights.padding-icon-right {
	width: 40px !important
}
.copy.views-mobile {
	font-size: 22px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
.copy.quote {
	font-weight: 400 !important
}
.copy.insights-icon {
	vertical-align: top !important
}
.copy.insights-icon img {
	vertical-align: text-top !important
}
#footer-table {
	width: 480px !important;
	-premeailer-align: center !important
}
#ios-gmail-fix {
	display: none !important
}
.fix-for-gmail {
	display: none !important
}
.inbox-fix {
	-premailer-width: 480 !important
}
.inbox-fix td {
	-premailer-width: 80 !important
}
.inbox-fix td img {
	height: 1px !important;
	width: 80px !important
}
}

@media only screen and (device-width: 360px) and (orientation: portrait) {
section .display-on-mobile {
	display: block !important;
	visibility: visible !important
}
section tr.display-on-mobile {
	display: table-row !important
}
section td.display-on-mobile {
	display: table-cell !important
}
section .display-on-desktop {
	display: none !important;
	visibility: hidden !important
}
section .inner {
	padding: 10px !important
}
section td.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section table.desktop {
	display: none !important;
	height: 0 !important;
	font-size: 0 !important;
	line-height: 0 !important
}
section td.mobile {
	display: block !important
}
section table.mobile {
	display: block !important
}
section img.mobile {
	display: block !important;
	height: auto !important
}
section .one-column table {
	width: 100% !important
}
section #logo {
	height: 26px !important;
	padding: 30px 0 20px 0 !important;
	-premailer-align: center !important
}
section #logo img {
	height: 26px !important
}
section .mobile-empty {
	-premailer-height: 10 !important;
	height: 10px !important
}
section .mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
section .copy.company-name {
	font-size: 34px !important;
	line-height: 40px !important
}
section .copy.views-mobile {
	font-size: 24px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
section .copy.copy-insights.padding-icon-right {
	width: 40px !important
}
section .copy.copy-insights-mobile {
	display: none !important;
	visibility: hidden !important
}
section .copy.copy-insights-mobile-display {
	display: block !important;
	visibility: visible !important
}
section .copy.copy-insights {
	font-weight: 400 !important
}
section .copy.quote {
	font-weight: 400 !important
}
section .yahoo {
	display: none !important
}
.inner {
	padding: 10px !important
}
.mobile-empty {
	-premailer-height: 30 !important;
	height: 30px !important
}
.mobile-empty-bg {
	-premailer-height: 30px !important;
	height: 30px !important
}
.copy.padding-icon-right {
	padding: 0 5px 0 0 !important
}
.copy.copy-insights {
	font-weight: 400 !important
}
.copy.views-mobile {
	font-size: 22px !important;
	line-height: 28px !important;
	font-weight: 400 !important;
	text-align: center !important
}
.copy.quote {
	font-weight: 400 !important
}
.copy.insights-icon {
	vertical-align: top !important
}
.copy.insights-icon img {
	vertical-align: text-top !important
}
.copy.footer-table {
	-premailer-width: 100% !important;
	width: 440px !important
}
.copy.footer-table-inside {
	width: 440px !important
}
.fix-for-gmail {
	display: none !important
}
#footer-table {
	width: 480px !important;
	-premeailer-align: center !important
}
.inbox-fix {
	-premailer-width: 480 !important
}
.inbox-fix td {
	-premailer-width: 80 !important
}
.inbox-fix td img {
	height: 1px !important;
	width: 80px !important
}
#ios-gmail-fix {
	display: none !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (min-device-width: 374px) and (max-device-width: 376px), only screen and (min-device-width: 413px) and (max-device-width: 415px), only screen and (min-device-width: 319px) and (max-device-width: 321px), only screen and (device-width: 375px) and (orientation: portrait), only screen and (device-width: 414px) and (orientation: portrait), only screen and (device-width: 360px) {
.awx-td-actions-3-stat .insights-section-default-3-stat .copy.copy-insights-awx {
	min-width: 230px !important
}
}

@media only screen and (device-width: 412px) and (orientation: portrait), only screen and (device-width: 360px) and (orientation: portrait) {
div > u + .body .insights-section-awx-3-stat {
	width: 240px !important
}
div > u + .body .insights-section-awx-2-stat {
	width: 240px !important
}
.insights-section-awx-3-stat, .insights-section-awx-2-stat {
	width: 285px !important
}
.copy.copy-insights-awx {
	font-weight: 300 !important
}
.display-desktop {
	display: none !important
}
div > u + .body .display-inbox {
	display: none !important
}
.display-inbox {
	display: block !important
}
div > u + .body .display-gmail {
	display: none !important
}
.display-gmail {
	display: none !important
}
}
</style>
</head>
<body class="body" style="background-color:#ffffff; font-size:0; line-height:0; margin:0; padding:0" bgcolor="#ffffff">
<div style="clear: both;display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;">Visits, Calls, Leads & Rankings ('.$start.' - '.$end.')</div>
<section>
  <style>
        
      </style>
  <table width="100%" align="center" class="main-container" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
    <tr>
      <td><!--[if gte mso 9]>
      <table id="outlook" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
        <tr>
          <td class="outlooktd" align="center">
      <![endif]-->
        
        <table class="email-container" dir="ltr" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; width:100%" align="center" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td><table id="masthead" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; background-color:#ffffff; margin:0 auto; padding:0; width:100%" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                <tr>
                  <td id="logo" align="center" style="-premailer-align:center; height:40px; padding:20px 0 10px 0" height="40"><img src="https://powerleads.biz/users/u6/6312/smaller_logo.png" height="40" alt="PowerLeads" title="PowerLeads" border="0" style="border:0; display:block; height:40px; image-rendering:auto; mso-line-height-rule:exactly"></td>
                </tr>
                <tr>
                  <td class="copy company-name" align="center" width="100%" style="-premailer-align:center; color:#9b9b9b; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:40px; font-weight:normal; line-height:48px; padding:0 0 10px 0; padding-bottom:10px; text-align:center"><span class="not-a-link" style="color:#9b9b9b; text-decoration:none">'.$company.'</span></td>
                </tr>
              </table>
              <table bgcolor="#ffffff" width="100%" class="actions-table" dir="ltr" style="-premailer-cellpadding:0; -premailer-cellspacing:0; border-top:2px solid #f5f5f5; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="-premailer-cellpadding:0; -premailer-cellspacing:0; background-color:#ffffff; margin:0 auto; max-width:600px; min-width:420px; padding:0" bgcolor="#ffffff">
                      <tr>
                        <td class="empty mobile-empty " height="25" colspan="1" style="font-size:0; line-height:25px; mso-line-height-rule:exactly">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="copy headline" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:700; line-height:24px; padding:0 0 3px 0; text-align:center"><span class="blue-text" style="color:#4a90e2">'.$totalVisits.'</span> PEOPLE FOUND YOUR BUSINESS ONLINE LAST MONTH</td>
                      </tr>
                      <tr>
                        <td><table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td class="empty copy empty-line" style="-premailer-align:center; -premeailer-height:5; font-size:5px; line-height:5px; mso-line-height-rule:exactly" align="center">&nbsp;</td>
                              <td class="copy dark-gray-line-desktop dark-gray-line-mobile" style="-premailer-align:center; -premailer-width:100; -premeailer-height:5; background-color:#9b9b9b; color:#9b9b9b; font-size:5px; line-height:5px" align="center" width="100" bgcolor="#9b9b9b">&nbsp;</td>
                              <td class="empty copy empty-line" style="-premailer-align:center; -premeailer-height:5; font-size:5px; line-height:5px; mso-line-height-rule:exactly" align="center">&nbsp;</td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center">
                      <tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center"><!--[if (gte mso 9)|(IE)]>
              <table width="100%">
                <tr>
                  <td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$totalSiteVisits.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center"> visited your website</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>   
								</table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
              </td><td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$totalBlogVisits.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center"> visited your blog</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>
                                    
                                  </table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
                  </td>
                </tr>
              </table>
            <![endif]--></td>
                      </tr>
					<tr>
                        <td class="two-column" style="font-size:0; text-align:center" align="center"><!--[if (gte mso 9)|(IE)]>
              <table width="100%">
                <tr>
                  <td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$calls.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center">people called</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>
                                   
                                  </table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
              </td><td width="50%" valign="top">
            <![endif]-->
                          
                          <div class="column" style="display:inline-block; max-width:280px; min-width:240px; text-align:center; vertical-align:middle; width:100%" align="center" valign="middle" width="100%">
                            <table width="100%" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="inner" style="padding:10px"><table class="mso-centering" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" align="center" cellpadding="0" cellspacing="0" valign="center">
                                    <tr>
                                      <td class="copy blue-stats-sm" align="center" style="-premailer-align:center; color:#4a90e2; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:30px; font-weight:400; line-height:35px"> '.$leads.' </td>
                                    </tr>
                                    <tr>
                                      <td class="copy copy-actions" align="center" dir="ltr" style="-premailer-align:center; color:#444444; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:21px; padding:5px 0 0 0; text-align:center"> submitted leads</td>
                                    </tr>
                                    <tr>
                                      <td class="empty mobile-empty " height="5" colspan="1" style="font-size:0; line-height:5px; mso-line-height-rule:exactly">&nbsp;</td>
                                    </tr>
                                   
                                  </table></td>
                              </tr>
                            </table>
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]>
                  </td>
                </tr>
              </table>
            <![endif]--></td>
                      </tr>
                      <tr>
                        <td align="center" valign="top"><table class="mso-centering" width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="-premailer-align:center; -premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0; vertical-align:center" valign="center">
                            <tr>
                              <td class="empty mobile-empty " height="20" colspan="1" style="font-size:0; line-height:20px; mso-line-height-rule:exactly">&nbsp;</td>
                            </tr>
                            <tr>
                              <td valign="middle" style="vertical-align: middle;" class="cta-mobile-width cta-desktop-3-stat"><table align="center" style="-premailer-cellpadding:0; -premailer-cellspacing:0; margin:0 auto; padding:0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td align="center" class="copy text-link" dir="ltr" style="-premailer-align:center; color:#2962ff; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:24px; margin:0 auto; padding:0; text-align:center"><a href="'.$loginURL.'" style="border:0; color:#2962ff; text-decoration:none"> Login to view report</a></td>
                                    <td width="10">&nbsp;</td>
                                    <td class="copy text-link arrow-img ltr" style="-premailer-align:center; color:#2962ff; font-family:&quot;Roboto&quot;, Helvetica, Arial, sans-serif; font-size:18px; font-weight:400; line-height:24px; margin:0 auto; padding:0; text-align:center" align="center"><a href="'.$loginURL.'" style="border:0; color:#2962ff; text-decoration:none"> <img src="https://6qube.com/hubs/themes/includes/img/blue_reports_arrow.png" width="20" alt="" style="border:0; display:block; image-rendering:auto; mso-line-height-rule:exactly"> </a></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td class="empty mobile-empty " height="28" colspan="1" style="font-size:0; line-height:28px; mso-line-height-rule:exactly">&nbsp;</td>
                </tr>
              </table>

            </td>
          </tr>
        </table>

      <!--[if mso ]>
            </td>
          </tr>
        </table>
        <![endif]--></td>
    </tr>
  </table>
</section>
				
<div style="margin:5px auto;"></div>
';
	

foreach ($rankings as $rank) {	
	$kData = $rk->getAllKeywordDataEmailReports(array(
            'site_ID' => $rank->ID, 
            'period' => $period,
			'interval' => $interval
        ));
$body .='
<!--Start stats-->
<table width="50%" border="0" cellpadding="0" cellspacing="0" dir="ltr" style="/*table-layout:fixed;border-bottom: 1px solid #EDEDED;*/">
  <tbody>
    <tr>
      <td rowspan="1" colspan="4" style="width: 455px; max-width: 455px; min-width: 455px; border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;padding: 5px 17px;font: 600 15px/28px \'Open Sans\', sans-serif;background-color: #F4F4F6;color: #30363F;">Current Google Rankings</td>
    </tr>
    <tr style="color: #53667B; background-color: #FBFBFD;">
      <!-- LEFT -->
      <!-- / LEFT -->
      <!-- RIGHT -->
      <td style="color: #53667B; background-color: #FFFFFF;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;">Rank</td>
      <!-- / RIGHT -->
    </tr>
    <!-- RIGHT -->
    <tr>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;color:#8FB900;"><span style="color:#8FB900;font: 14px/18px \'Open Sans\', sans-serif;">&#9632;</span> #1</td>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;color:#8FB900;">'.$num_one_spots.'</td>
      <td style="color: #53667B; background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;">&nbsp;</td>
    </tr>
    <!-- / RIGHT -->
    <tr style="color: #53667B; background-color: #FFFFFF;">
      <!-- LEFT -->
      <!-- / LEFT -->
      <!-- RIGHT -->
      <td style="background-color: #FFFFFF;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;color:#01A2D0;"><span style="color:#01A2D0;font: 14px/18px \'Open Sans\', sans-serif;">&#9632;</span> Top 3</td>
      <td style="background-color: #FFFFFF;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;color:#01A2D0;">'.$top_three_spots.'</td>
      <td style="color: #53667B; background-color: #FFFFFF;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;">&nbsp;</td>
      <!-- / RIGHT -->
    </tr>
    <!-- RIGHT -->
    <tr>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;color:#FFC000;"><span style="color:#FFC000;font: 14px/18px \'Open Sans\', sans-serif;">&#9632;</span> Top 10</td>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;color:#FFC000;">'.$rank->google_first.'</td>
      <td style="color: #53667B; background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;">&nbsp;</td>
    </tr>
    <!-- / RIGHT -->
    <tr style="color: #53667B; background-color: #FBFBFD;">
      <!-- LEFT -->
      <!-- / LEFT -->
      <!-- RIGHT -->
      <td style="background-color: #FFFFFF;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;color:#F48500;"><span style="color:#F48500;font: 14px/18px \'Open Sans\', sans-serif;">&#9632;</span> Top 20</td>
      <td style="background-color: #FFFFFF;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;color:#F48500;">'.$top_20_spots.'</td>
      <td style="color: #53667B; background-color: #FFFFFF;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;">&nbsp;</td>
      <!-- / RIGHT -->
    </tr>
    <!-- RIGHT -->
    <tr>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: left;border-top: 1px solid #EDEDED;border-bottom: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 17px;color:#C9310A;"><span style="color:#C9310A;font: 14px/18px \'Open Sans\', sans-serif;">&#9632;</span> Top 100</td>
      <td style="background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-bottom: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;color:#C9310A;">'.$top_100_spots.'</td>
      <td style="color: #53667B; background-color: #FBFBFD;vertical-align:middle; text-align: center;border-top: 1px solid #EDEDED;border-right: 1px solid #EDEDED;border-bottom: 1px solid #EDEDED;font: 14px/18px \'Open Sans\', sans-serif;padding: 5px 0px;">&nbsp;</td>
    </tr>
    <!-- / RIGHT -->
  </tbody>
</table>

		<!--End stats-->

<div style="clear: both;display: block;width: 100%;height: 30px;"><br/><br/></div>


<!-- URL BOX -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" dir="ltr">
  <tr>
    <td style="border: 1px solid #EDEDED;"><h3 style="display: inline-block;margin:17px; margin-left:20px;"> <a href="http://'.$rank->hostname.'" style="color: #FF5F00">'.$rank->hostname.'</a> </h3>
      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
        <thead>
          <tr style="background-color: #F4F4F6; color: #30363F;" align="center">
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED;padding: 4px 5px; width: 45px;min-width: 45px;">Change</th>
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED;padding: 4px 5px; width: 125px;min-width: 120px;" align="left">Keyword </th>
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px; width: 200px;  min-width: 110px;" align="left">Ranking URL</th>
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px; width: 120px; min-width: 70px;">Engine</th>
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px; width: 50px; min-width: 40px;">Ranking</th>
            <th style="border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px;  width: 50px; min-width: 40px;">Page</th>
          </tr>
        </thead>
        <tbody>
          ';
	
	///start keyword data table.
	
	foreach($kData as $keyword){
		
		

		
		$classGoogle = $keyword->google_change >= 0 ? $changecolor = 'green' : $changecolor = 'red'; 
		
		if($keyword->rank_google==0){
			$keyword->rank_google = '+100';
			$keyword->pagenumber = '+10';
		}
		if($keyword->google_change == NULL) $keyword->google_change = 0;
		  
 $body .='<tr style="color: #53667B; background-color: #FBFBFD" align="center">
 <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED;padding: 4px 5px;"	align="center"><span style="color:'.$changecolor.'">'.(isset($google_change_sign) ? $google_change_sign : '').$keyword->google_change.'</span></td>
            <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED;padding: 4px 5px;"	align="left">'.$keyword->keyword.'</td>
            <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px;" align="left">'.$keyword->result_url.'</td>
            <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px;"><a href="'.$keyword->result_se_check_url.'">Google.com</a></td>
            <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px;">'.(isset($google_rank_sing) ? $google_rank_sing : '').$keyword->rank_google.'</td>
            <td style="word-wrap:break-word;border-top: 1px solid #EDEDED;border-right:1px solid #EDEDED; padding: 4px 5px;">'.$keyword->pagenumber.'</td> </tr>';
	}
	
	///end keyword data table.
			
 $body .='        
        </tbody>
      </table></td>
  </tr>
</table>';
}
 $body .='<div style="clear: both;display: block;width: 100%;height: 30px;"><br/>
  <br/>
</div>
</body></html>';
?>

