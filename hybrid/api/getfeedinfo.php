<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';
require HYBRID_PATH . '/library/simplepie/autoloader.php';
require HYBRID_PATH . '/classes/FeedObject.php';
require HYBRID_PATH . '/classes/FeedObjects.php';
require HYBRID_PATH . 'classes/custom/SCLeadsFeedObject.php';

$import	=	new FeedObjects('https://www.supercircuits.com/media/xmlFeedExport/google/dealer-product-feed.xml');
$parseObject	=	new SCLeadsFeedObject;

$reporter =   new FeedReporter($import);
$reportinfo =   $reporter->getItemsSummary($parseObject, (object)(array('ID' => 0)));

echo json_encode($reportinfo);


