<?php    
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

\ini_set('display_errors', true);
error_reporting(E_ALL ^ E_STRICT);
define('APP_NAME', "GOTO WEBINAR - LIVE"); // APPLICATION NAME
//define('CLIENT_ID', "arITMLvHqh4ca7DgDbmwDrJNSUiR9gj4"); // API KEY
//define('CLIENT_SECRET', "J9CGbRAyLLIdZije"); // API SECRET
define('CLIENT_ID', "aUO5zWIk7Z6jGX5H2oIIpIsLcVi6769c"); // API KEY
define('CLIENT_SECRET', "GpmihxrY6wzIOhyk"); // API SECRET
//define('CALLBACK_URL', "https://control.ignitedlocal.com/api/gotowebinar/webinars_v2.php"); // LIVE
define('CALLBACK_URL', "http://seedlogix.test/api/gotowebinar/webinars_v2.php"); // DEV

$application_name = APP_NAME;

// Database Settings.
/*$host = "127.0.0.1";
$dbname = "sapphire_6qube";
$user = "sapphire_ignited";
$pass = "QTvCG4#R,]#9)'8*#Ueb";*/

$host = '127.0.0.1';
$dbname = 'sapphire_6qube';
$user = 'root';
$pass = 'seedlogix';

$dsn = "mysql:host=$host;dbname=$dbname";
$opt = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
];
$pdo = new PDO($dsn, $user, $pass, $opt);

$client_id = CLIENT_ID;
$sql = "SELECT refresh_token, organizer_key, expiration_time "
        . "FROM api_settings "
        . "WHERE client_id = '$client_id' AND token_used != 1 "
        . "ORDER BY id DESC LIMIT 1";

$stmt = $pdo->query($sql);
$result = $stmt->fetch();
//var_dump($result);

// If row is found in database, set the refresh token and organizer key.
if ($result !== false) {
    $row = $result;
    $refresh_token = $row->refresh_token;
    $organizer_key = $row->organizer_key;
    $expiration_time = $row->expiration_time;

}

// If '$refresh_token' is set and not expired, go ahead and grab access token and pull upcoming webinars.
if (!empty($refresh_token) && ((int)$expiration_time > (int)time())) {
    $params = array(
        'grant_type' => 'refresh_token',
        'refresh_token' => $refresh_token
    );
    
    $url = "https://api.getgo.com/oauth/v2/token";
    $base64_client_id_and_secret = base64_encode(CLIENT_ID . ':' . CLIENT_SECRET);
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/x-www-form-urlencoded", 
        "Accept: application/json", 
        "Authorization: Basic $base64_client_id_and_secret"));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        
        curl_close($curl);
        exit;
        
    }
    
    curl_close($curl);
    
    // JSON decode the response.
    $json_response = json_decode($response);
    //var_dump($json_response);
    
    if (isset($json_response->access_token)) {
        $access_token = $json_response->access_token;

        $webinars = getUpcomingWebinars($access_token, $organizer_key);

        // Show webinar schedule if they exist.
        if (!empty($webinars) && $webinars[0]->webinarKey) {
            displayWebinars($webinars);
            
            $sql = "UPDATE api_settings SET token_used = 1 "
                    . "WHERE client_id = '$client_id' AND refresh_token = '$refresh_token'";
            
            $stmt = $pdo->query($sql);
            
            // Insert New Token
            $refresh_token = $json_response->refresh_token;
            $organizer_key = $json_response->organizer_key;
            $expiration_time = strtotime(date('Y-m-d H:i:s', strtotime('+30 days')));
            $date = date('Y-m-d H:i:s');

            $client_id = CLIENT_ID;

            // Insert token and key into database.
            $sql = "INSERT INTO api_settings "
                    . "(client_id, application_name, refresh_token, organizer_key, expiration_time, created_at) "
                    . "VALUES "
                    . "('$client_id', '$application_name', '$refresh_token', '$organizer_key', '$expiration_time', '$date')";
            $pdo->query($sql);
            
        } else {
            print "No upcoming webinars to display.";
            exit;

        }
    } 

// If no refresh token is available or refresh token is expired, lets get the authorization code. 
} else if (!isset($_GET['code'])) {
    $params = array(
        'response_type' => "code", 
        'client_id' => CLIENT_ID, 
        'redirect_uri' => CALLBACK_URL, 
        //'state' => uniqid("", true), 
        //'scope' => 
    );
    
    $url = "https://api.getgo.com/oauth/v2/authorize?" . http_build_query($params);
    
    header("Location: $url");
    exit;

// Use authorization code to get a new refresh token. Since we get an access code, let's pull the webinar info while we are at it!
} else {
    $params = array(
        'grant_type' => 'authorization_code', 
        //'client_id' => CLIENT_ID, 
        //'client_secret' => CLIENT_SECRET, 
        'code' => $_GET['code'], 
        'redirect_uri' => CALLBACK_URL
    );
    
    $url = "https://api.getgo.com/oauth/v2/token";
    $base64_client_id_and_secret = base64_encode(CLIENT_ID . ':' . CLIENT_SECRET);
    //var_dump($base64_client_id_and_secret);
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/x-www-form-urlencoded", 
        "Accept: application/json", 
        "Authorization: Basic $base64_client_id_and_secret"));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        
        curl_close($curl);
        exit;
        
    }
    
    curl_close($curl);
    
    // JSON decode the response.
    $json_response = json_decode($response);
    //var_dump($json_response);
    
    if (isset($json_response->access_token)) {
        $access_token = $json_response->access_token;
        //var_dump($access_token);
        
        $refresh_token = $json_response->refresh_token;
        $organizer_key = $json_response->organizer_key;
        $expiration_time = strtotime(date('Y-m-d H:i:s', strtotime('+30 days')));
        $date = date('Y-m-d H:i:s');
        
        $client_id = CLIENT_ID;
        
        // Insert token and key into database.
        $sql = "INSERT INTO api_settings "
                . "(client_id, application_name, refresh_token, organizer_key, expiration_time, created_at) "
                . "VALUES "
                . "('$client_id', '$application_name', '$refresh_token', '$organizer_key', '$expiration_time', '$date')";
        $pdo->query($sql);
        
        $webinars = getUpcomingWebinars($access_token, $organizer_key);
        //var_dump($webinars);
        
        // Show webinar schedule if they exist.
        if (!empty($webinars) && $webinars[0]->webinarKey) {
            displayWebinars($webinars);
            
        } else {
            print "No upcoming webinars to display.";
            exit;
            
        }
        
    } else {
        print "Connection to GoTo Webinar failed! Please try again later.";
        exit;
        
    } 
}

// Function to display webinars.
function displayWebinars($webinars) {
    htmlStyle(); // Inject CSS styles
    
    foreach ($webinars as $webinar) {
        ?>
        <div class="container">
            <div class="title"><?php echo $webinar->subject ?></div>
            <?php
            foreach ($webinar->times as $webinar_time) {
                $date = new DateTime($webinar_time->startTime); 
                $date->setTimezone(new DateTimeZone($webinar->timeZone));

                ?><div class="date"><?php echo $date->format("l jS F Y \@ g:ia \C\S\T") ?></div>
            <?php } ?>
            <div class="link">
                <a href="<?php echo $webinar->registrationUrl ?>" target="_blank"><?php echo $webinar->registrationUrl ?></a>
            </div>
        </div>
        <?php
    }
}

function htmlStyle() {
    ?>
    <style type="text/css">
        .link {
            text-align: left;
            color: #999;
            font: 12px Tahoma, Geneva, sans-serif;
            margin: 10px 0 10px 0;
        }
        .title, .title *{
            color: #3083A3;
            text-decoration: none;
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        }

        .date {
            font: 12px Tahoma, Geneva, sans-serif;
        }
        .container {
            color: #07AD00;
            text-decoration: none;
            margin-bottom: 10px;
            border: 1px solid #DDD;
            padding: 10px;
        }
    </style>
    <?php
}

// Function to grab webinars post CURL.
function getUpcomingWebinars($access_token, $organizer_key) {
    //$organizer_key = ORGANIZER_KEY;
    //var_dump($organizer_key);
    
    // GoToMeeting Webinar URL
    $url = "https://api.getgo.com/G2W/rest/organizers/$organizer_key/upcomingWebinars";

    // Run CURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array( 
        "Accept: application/json", 
        "Authorization: $access_token"));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        exit;
    }

    $profile_response = json_decode($response);
    //var_dump($profile_response);

    curl_close($curl);
    
    return $profile_response;
}

