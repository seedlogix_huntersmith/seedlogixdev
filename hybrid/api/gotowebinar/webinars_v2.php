<?php    
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//ini_set('display_errors', true);
//error_reporting(E_ALL ^ E_STRICT);
error_reporting(0);

$mode = "LIVE"; // DEV

define('ENV', $mode);
define('APP_NAME', "GOTO WEBINAR - LIVE"); // APPLICATION NAME
define('CLIENT_ID', "aUO5zWIk7Z6jGX5H2oIIpIsLcVi6769c"); // API KEY SeedLOGIX
define('CLIENT_SECRET', "GpmihxrY6wzIOhyk"); // API SECRET SeedLOGIX

                        
if (ENV === 'LIVE') {
    define('CALLBACK_URL', "https://control.ignitedlocal.com/api/gotowebinar/webinars_v2.php"); // LIVE
    
    // Database Settings.
    $host = "127.0.0.1";
    $dbname = "sapphire_6qube";
    $user = "sapphire_ignited";
    $pass = "QTvCG4#R,]#9)'8*#Ueb";
    
} else if (ENV === 'DEV') {
    define('CALLBACK_URL', "http://seedlogix.test/api/gotowebinar/webinars_v2.php"); // DEV
    
    $host = '127.0.0.1';
    $dbname = 'sapphire_6qube';
    $user = 'root';
    $pass = 'seedlogix';
    
}

$application_name = APP_NAME;

/* NEW: Get API return info by using grant_type = password instead of authorization code, so we don't have to login anymore! */
/*$params = array(
    'grant_type' => 'password',
    'user_id' => 'admin@seedlogix.com', 
    'password' => '$eed1ogix', 
    'client_id' => CLIENT_ID, 
    //'redirect_uri' => CALLBACK_URL, 
    //'state' => uniqid("", true), 
    //'scope' => 
);*/

$params = array(
    'grant_type' => 'password',
    'user_id' => 'admin@ignitedlocal.com', 
    'password' => '$eed1ogiX', 
    'client_id' => CLIENT_ID, 
    //'redirect_uri' => CALLBACK_URL, 
    //'state' => uniqid("", true), 
    //'scope' => 
);

$url = "https://api.getgo.com/oauth/access_token";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/x-www-form-urlencoded", 
    "Accept: application/json"));

$response = curl_exec($curl);
$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

// Error in curl request.
/* if (!empty(curl_error($curl))) {
    print("Curl error: " . curl_error($curl));

    curl_close($curl);
    exit;

}*/

curl_close($curl);

// JSON decode the response.
$json_response = json_decode($response);
//var_dump($json_response);
?>

<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>GoToWebinar Webinars Schedule</title>

<link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
<link href="/themes/default/css/styles.css" rel="stylesheet" type="text/css">
<link href="/themes/default/css/hybrid.css" rel="stylesheet" type="text/css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.4.1.js"></script>
<script src="/themes/default/js/files/functions.js"></script>

</head>
<body class="gtw">

<?php
if (isset($json_response->access_token)) {
    $access_token = $json_response->access_token;
    $organizer_key = $json_response->organizer_key;
    //var_dump($access_token);

    $webinars = getUpcomingWebinars($access_token, $organizer_key);
	$pastWebinars = getPastWebinars($access_token, $organizer_key);
    //var_dump($webinars);

    // Show webinar schedule if they exist.
    if (!empty($webinars) && (array)$webinars[0]->webinarKey) {
        displayWebinars($webinars);
		displayWebinars($pastWebinars,true);

    } else{
?>

    <div class="fluid"><div class="grid12"><p><strong class="red">No GoToWebinar information available.</strong></p></div></div>

<?php
        exit;
    }
} else{
?>

    <div class="fluid"><div class="grid12"><p><strong class="red">GoToWebinar connection error, please try again later.</strong></p></div></div>

<?php
    exit;
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.6.1/iframeResizer.contentWindow.min.js"></script>
</body>
</html>

<?php
function displayWebinars($webinars,$is_past = false){
?>

    <div class="fluid">
        <div class="grid12">
            <p><strong class="green"><?= $is_past ? 'Past Webinar Replays' : 'Current Webinars'; ?></strong></p>

<?php
    $filtered = array_filter($webinars,function($w){
        return $w->subject == 'ignitedAGENT Sales Training';
    });
    if($is_past){
        $counter = 0;
        $length = count($filtered);
    }
    foreach($filtered as $webinar){
        if($is_past){
            ++$counter;
        }
?>

            <div class="grid3"<?= $is_past && $counter == $length ? ' data-iframe-height' : ''; ?>>
                <div class="gtw_title"><strong class="cyan"><?= $webinar->subject; ?></strong></div>

<?php
        foreach($webinar->times as $webinar_time){
            $date = new DateTime($webinar_time->startTime);
            $date->setTimezone(new DateTimeZone($webinar->timeZone));
?>

                <div class="gtw_date"><?= $date->format('D, M d, Y \@ h:i A T'); ?></div>

<?php
        }
?>

                <div class="gtw_link"><a href="https://attendee.gotowebinar.com/register/<?= $webinar->webinarKey; ?>" class="buttonS f_cr _blank">Register</a></div>
            </div>

<?php
    }
?>

        </div>
    </div>

<?php
}

// Function to grab webinars post CURL.
function getUpcomingWebinars($access_token, $organizer_key) {
    //$organizer_key = ORGANIZER_KEY;
    //var_dump($organizer_key);
    
    // GoToMeeting Webinar URL
    $url = "https://api.getgo.com/G2W/rest/organizers/$organizer_key/upcomingWebinars";

    // Run CURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array( 
        "Accept: application/json", 
        "Authorization: $access_token"));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    /*if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        exit;
    }*/

    $profile_response = json_decode($response);
    //var_dump($profile_response);

    curl_close($curl);
    
    return $profile_response;
}

function getPastWebinars($access_token, $organizer_key) {
    //$organizer_key = ORGANIZER_KEY;
    //var_dump($organizer_key);
    $datenow = date('Y-m-d');

    // GoToMeeting Webinar URL
    $url = "https://api.getgo.com/G2W/rest/organizers/$organizer_key/historicalWebinars?fromTime=2018-01-01T10:00:00Z&toTime=".$datenow."T10:00:00Z";
  
    // Run CURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Accept: application/json",
        "Authorization: $access_token"));

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    // Error in curl request.
    /*if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        exit;
    }*/

    $profile_response = json_decode($response);
    //var_dump($profile_response);

    curl_close($curl);

    return $profile_response;
}
?>