<?php

chdir(dirname(__FILE__));

require '../bootstrap.php';
@session_start();

Qube::ForceMaster();

require_once HYBRID_PATH . 'library/ChargeBee/ChargeBee.php';

$aid = $_GET['aid'];
$account = AccountsModel::Fetch('id = %d', $aid);

$conid = $_GET['conid'];
$contact = ContactModel::Fetch('id = %d', $conid);

$proid = $_GET['proid'];
$proposal = ProposalModel::Fetch('id = %d', $proid);

$time = strtotime($proposal->created);
$proposal->created = date('m-d-y',$time);

$expire = strtotime($proposal->expire_date);
$proposal->expire_date = date('m-d-y',$expire);

$pa = new ProposalDataAccess();
$products = $pa->getProductsbyProposal($proposal, array('proid' => $proid));

$boBrand = ResellerModel::Fetch('admin_user = %d', $account->user_parent_id);


require_once HYBRID_PATH . 'library/html-pdf/phpinvoice.php';
$invoice = new phpinvoice("decentblue","A4","$");
$logo = HYBRID_PATH . 'users'.$boBrand->prop_logo;
/* Header Settings */
$invoice->setLogo($logo);
$invoice->setType($account->name);
$invoice->setColor($boBrand->bg_clr);
$invoice->setReference($contact->first_name.' '.$contact->last_name);//customer name here.
$invoice->setDate($proposal->created);
$invoice->setDue($proposal->expire_date);
$invoice->setFrom(array($boBrand->company,$boBrand->address,$boBrand->city.', '.$boBrand->state.' '.$boBrand->zip,$boBrand->phone));
$invoice->setTo(array($account->address,$account->city.', '.$account->state.' '.$account->zip,$account->phone,'<a href="'.$account->url.'">'.$account->url.'</a>'));

/* Adding Items in table */
$addons = array();
foreach($products as $product){
	
$invoice->addItem($product->name,$product->s_desc,$product->qty,0,$product->price,$product->discount,$product->type);

}


$invoice->setProposalLink('https://ignitedco.com/?aid='.$aid.'&conid='.$conid.'&proid='.$proid.'');
/* Add totals */
// Make sure to add  "$invoice->items_total" first before adding other "addTotal()"
$invoice->addTotal("Sub Total",		$invoice->items_total);
//$invoice->addTotal("VAT 10%", 	 	$invoice->GetPercentage(10));
//$invoice->addTotal("Discount 10%", 	$invoice->GetPercentage(10),false,true);
//$invoice->addTotal("Shipment", 		"100");
$invoice->addTotal("Grand Total",	 	$invoice->GetGrandTotal(),true);

/* Set badge */ 
//$invoice->addBadge("Invoice Copy");
/* Add title */
$invoice->addTitle('This Proposal Will Expire On <span style="color:#EC320A">'.$proposal->expire_date.'</span> - Please Follow Link Below To Accept');
/* Add Paragraph */
$invoice->addParagraph('');
/* Set footer note */
$invoice->setFooternote($boBrand->company);

/* Render */
$cname = preg_replace('/[^\w]/', '', $account->name);
$cname = strtolower($cname);

//$invoice->render('proposals/proposal-'.$cname.'.pdf','F'); 
$invoice->render('proposals/proposal-'.$cname.'.pdf','I');
/* I => Display on browser, D => Force Download, F => local path save, S => return document path */

?>