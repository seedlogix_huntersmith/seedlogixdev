<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';
@session_start();

Qube::ForceMaster();

/**
	1. Load reseller app id
	2. Redirect to facebook dialog.
	3. exchange oauth token for long-lived token
	4. request /me/accounts and get list of pages
	6. save the longtoken into $_SESSION['token_$blogid']	=	$longlivetoken;
	5. present pages to user and have them choose the page, passing pageid and blogid in $_GET
	6. receive the pageid and blogid values
	7. load the $longlivetoken from the session array
	8. fetch  /me/accounts and identify $pageid
	9. save $pageid/$pagetoken into DB
	6. happy dance
*/
$sitehttphost    =   isset($_GET['sitehttphost']) ? $_GET['sitehttphost'] : $_GET['state'];

$stmt=  Qube::GetPDO()->prepare('select b.id, fbapp_id, fbapp_secret, r.main_site, r.fbapp_url from resellers r, hub b where 
	b.httphostkey = ?  AND ((b.user_parent_id != 0 AND r.admin_user = b.user_parent_id) or r.admin_user = b.user_id)');


function redirect($appid, $redirect_uri, $state)
{    
	$url	='http://www.facebook.com/dialog/oauth/?client_id=' . $appid . 
                    '&redirect_uri=' . urlencode($redirect_uri) . 
			'&state='.$state.'&scope=' . urlencode('manage_pages,publish_pages');
	header('Location: ' . $url);
	exit;
}

function get_longlivedtoken($app_id, $app_secret, $token)
{
    $token_url  =   'https://graph.facebook.com/oauth/access_token?client_id=' . $app_id . 
			'&client_secret=' . $app_secret . '&grant_type=fb_exchange_token&fb_exchange_token=' . $token;
	$response	=	file_get_contents($token_url);

#	echo "Long Token: $result<br/>";
	#$params	=	null;
	#parse_str($response, $params);

	$json = json_decode($response);

	#var_dump($params);
	#return $params['access_token'];
	return $json->access_token;
}

function get_token($app_id, $redirect_uri, $app_secret, $code, &$error)
{
    $token_url  =   'https://graph.facebook.com/oauth/access_token?' .
                'client_id=' . $app_id . '&redirect_uri=' . urlencode($redirect_uri) .
                '&client_secret=' . $app_secret . '&code=' . $code;

    $response   = file_get_contents($token_url);

	
#var_dump($response, $token_url);
	#var_dump(json_decode($response));
    #$params =   null;
    #parse_str($response, $params);

	#var_dump($params);

	$json = json_decode($response);

	#$short_token	=	$params['access_token'];
	$short_token	=	$json->access_token;

	#var_dump($short_token);

	if(!$short_token){
		$info	=	json_decode($response);
		$error	=	$info->error->message;
		return false;
	}

#echo 'token:', $short_token;
#return $short_token;
#	Qube::LogError('Short Token:', $short_token);

	return get_longlivedtoken($app_id, $app_secret, $short_token);
}

function save_facebookid($page_id, $pagename, $access_token, $sitehttphost)
{
    // @todo change facebook_id column to facebook_accessid so it makes more sense.
        $q      =       'UPDATE hub SET facebook_id = CONCAT(?,"\n",?,"\n",?) WHERE httphostkey     =       ?';
        $prepared       =       Qube::GetPDO()->prepare($q);
        if($prepared->execute(array($page_id, $pagename, $access_token, $sitehttphost)))
	{
		echo 'OK! You selected: ', $pagename, '. <a href="javascript:void(close());">Close this window</a>. or Choose a different page.';
#exit;
	}//, $bloghttphost));
}

if(!$stmt->execute(array($sitehttphost))) die (' failed');

$info	=	$stmt->fetch(PDO::FETCH_ASSOC);

$siteid	=	$info['id'];
if(!$info['fbapp_id']) $fbappid = '1604862766397665';
else $fbappid = '1604862766397665';
if(!$info['fbapp_secret']) $fbappsecret = '649610b9a32a251610ffd0418caeac6a';
else $fbappsecret = '649610b9a32a251610ffd0418caeac6a';//stmt->fetchColumn();
if(!$info['fbapp_url']) $reseller_host = 'login.seedlogix.com';
else $reseller_host = 'login.seedlogix.com';

#echo $reseller_host; var_dump($info); exit;
$redirect_uri   =   'https://' . $reseller_host . '/api/facebook-website-app.php';
$state = $sitehttphost;


$pageid	=	null;
if(isset($_GET['pageid']))
{
	$pageid	=	$_GET['pageid'];

#	echo "blogid: $blogid;"; var_dump($_SESSION);
	if(!isset($_SESSION['token_' . $blogid])) Qube::Fatal('Token is not inigialized in session');
	$token	=	$_SESSION['token_' . $blogid];

}elseif(!isset($_GET['code']))

	redirect($fbappid, $redirect_uri, $state);

else{
	// code is defined, but pageid is not

	$token  =   get_token($fbappid, $redirect_uri, $fbappsecret, $_GET['code'], $errormsg);

	if($token === false){
		echo 'Error ocurred: ' . $errormsg;

		//Qube::Fatal('A fatal message ocurred: ' . $errormsg, $reseller_host);
	}
	$_SESSION['token_' . $blogid]	=	$token;
}


$usr	=	json_decode(file_get_contents('https://graph.facebook.com/me?access_token=' . $token.'&limit=5000'));
#var_dump($usr, $token);
$graph_url  =   'https://graph.facebook.com/me/accounts?access_token=' . $token.'&limit=5000';

$user = json_decode(file_get_contents($graph_url));

#Qube::LogError('User accounts: ', $user, $_SESSION, $_GET, $token, $_POST);


#var_dump($user);
if($user && !$user->error){

	if($pageid){	// find and save the page if one has been selected

		foreach($user->data as $page): 
			if($page->id	==	$pageid)
				save_facebookid($pageid, $page->name, $page->access_token, $sitehttphost);
#			echo "Page: $page->name/ $page->id<br />";	// $page->access_token;
		endforeach;
	}


	?>
	<h1>Choose a page to connect your website to</h1>
	<ul><?php foreach($user->data as $page): ?><li>
	<a href='facebook-website-app.php?sitehttphost=<?php echo $sitehttphost; ?>&pageid=<?php echo $page->id; ?>'><?php echo htmlentities($page->name); ?></a></li>
	<?php endforeach; ?></ul>
<script>
//parent.window.document.forms.blogs.facebook_app_id.value = "wats up!";
//window.close();
</script>
<?php
#	echo 'OK!';
}else{

	die(' an error ocurred. Check Facebook App Secret.');
	unset($_SESSION['token_' . $siteid]);
}

