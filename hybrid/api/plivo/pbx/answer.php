<?php
require 'include/config.php';

## Assign Variables from incoming call
$did = $_REQUEST['To']; 
$CallStatus = $_REQUEST['CallStatus'];

$r = new Response(); 			//Create the Plivo response object

if($CallStatus=='completed'){
	
	$p = new RestAPI($conf_pilvo_authid, $conf_plivo_authtoken);
	
	$CallID = $_REQUEST['CallUUID'];
	$CallDuration = $_REQUEST['Duration'];
	$CallStartTime = $_REQUEST['StartTime'];
	$CallFrom = $_REQUEST['From'];
	$CallTo = $_REQUEST['To'];
	
	$stmt = $pdo->prepare('SELECT id, cid, user_id, user_parent_id FROM phone_numbers WHERE phone_num = :did');
	$stmt->execute(array('did' => $did));
	$user = $stmt->fetch();
	
	$params2 = array(
						'call_uuid' => ''.$CallID.''
					);
	$response2 =$p->get_recordings($params2);
	
	$query = "INSERT INTO phone_records 
				(cid, user_id, user_parent_id, tracking_id, direction, call_uuid, caller_id, destination, length, recording, date)
				VALUES 
				('".$user['cid']."', '".$user['user_id']."', '".$user['user_parent_id']."', '".$user['id']."', 'inbound','".$CallID."', 
				'".$CallFrom."', '".$CallTo."', '".$CallDuration."', '".$response2['response']['objects'][0]['recording_url']."', '".$CallStartTime."')";
	$result = $pdo->query($query);
	
} else {
	## Select the DID from the DB, if it's a user redirect.
	$stmt = $pdo->prepare('SELECT id, type FROM phone_numbers WHERE phone_num = :did');
	$stmt->execute(array('did' => $did));
	$user = $stmt->fetch();

	$from_number = $_REQUEST['From'];
	$blacklist = array('12169206413', '16016188876', '16014150673', '18887895999', '17182873762', '16044090872', '12318934583', '15149695508', '19545055388', '18887895999', '5123378905');

	if (in_array($from_number, $blacklist)) {

		$params = array('reason' => 'rejected');
		$r->addHangup($params);

	} else {
		if (!$user) {
			//Do Default
			$body = ''.$pbx_url.'/' . $conf_default_route["module"] . '.php?id=' . $conf_default_route["id"];
			$attributes = array();
			$r->addRedirect($body, $attributes);
		} else if($user["type"]=='ivr') {
			//Redirect to user
			$body = ''.$pbx_url.'/' . $conf_default_route["module"] . '.php?id=' . $conf_default_route["id"].'&userid='.$user["id"].'';
			$attributes = array();
			$r->addRedirect($body, $attributes);
		/*} else if($user["id"]==151) {
			//Redirect to user
			$body = ''.$pbx_url.'/' . $conf_default_route["module"] . '.php?id=' . $conf_default_route["id"].'&userid='.$user["id"].'';
			$attributes = array();
			$r->addRedirect($body, $attributes);*/
		} else {
			//Redirect to user
			$body = ''.$pbx_url.'/user.php?id=' . $user["id"].'&cid='.$from_number.'';
			$attributes = array();
			$r->addRedirect($body, $attributes);
		}
	}
	
}
header('Content-type: application/xml'); // Plivo is expecting XML
echo $r->toXML(); 				//return the response XML
?>