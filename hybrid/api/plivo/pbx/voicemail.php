<?php
require 'include/config.php';

$id = $_REQUEST['id'];
$cid = $_REQUEST['cid'];
$CallID = $_REQUEST['CallUUID'];
$r = new Response();


if ($id){
	
		$stmt = $pdo->prepare('SELECT greeting FROM phone_numbers WHERE id = :id');
		$stmt->execute(array('id' => $id));
		$greeting = $stmt->fetch();
		if ($greeting['greeting']){
			$r->addPlay('https://jonaguilar.com/users'.$greeting['greeting']);
		}else{
			## NO personalized greeting
			$r->addSpeak('Leave your message after the tone');
		}
	## No extension, can't leave a message
}
	/*$r->addRecord(array('action' => ''.$pbx_url.'/vm-confirm-input.php?id=' . $id .'&cid='.$cid,
						   'method' => 'GET',
						   'maxLength' => '60',
						   'finishOnKey' => '#',
						   'playBeep' => 'true'));
	$r->addSpeak('Recording not received');*/

    $record_params = array(
        'action'=> ''.$pbx_url.'/vm-action.php?id=' . $id .'&cid='.$cid, # Submit the result of the record to this URL
        'method' => 'GET', # HTTP method to submit the action URL
        'maxLength'=> '60', # Maximum number of seconds to record 
        'transcriptionType' => 'auto', # The type of transcription required
        'transcriptionUrl' => ''.$pbx_url.'/transcription.php?CallUUID='.$CallID.'', # The URL where the transcription while be sent from Plivo
		'playBeep' => 'true',
        'transcriptionMethod' => 'GET' # The method used to invoke transcriptionUrl 
    );
    $r->addRecord($record_params);

	//$r->addSpeak('Recording not received');
	//$r->addRedirect(''.$pbx_url.'/voicemail.php?id=' . $id, array('method' => 'GET'));
	header('content-type: text/xml');
	echo($r->toXML());

?>