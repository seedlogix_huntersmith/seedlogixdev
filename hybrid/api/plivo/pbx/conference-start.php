<?php
require 'include/plivo.php';
require 'include/config.php';
require 'include/database.php';

$pin = $_REQUEST['Digits'];

$stmt = $db->prepare('SELECT room,role FROM Conference WHERE pin = :pin');
$stmt->execute(array('pin' => $pin));
$room = $stmt->fetch();

$r = new Response();

if($room){
	##Valid PIN
	if($room['role'] == 'leader'){
		##Room Leader
		$attributes = array(
			'startConferenceOnEnter' => 'true',
			'endConferenceOnExit' => 'true',
			'enterSound' => 'beep:1',
			'exitSound' => 'beep:2',
		);
	}else{
		## Participant
		$attributes = array(
			'startConferenceOnEnter' => 'false',
			'waitSound' => 'http://' . $_SERVER["SERVER_NAME"] . '/PBX/hold.php',
			'enterSound' => 'beep:1',
			'exitSound' => 'beep:2',
		);
	}
	##Enter Room
	$r->addConference($room['room'], $attributes);
}else{
	##Invalid PIN
	$r->addSpeak('Invalid PIN');
	$r->addRedirect('http://' . $_SERVER["SERVER_NAME"] . '/PBX/conference.php');
}



header('Content-Type: text/xml');
echo($r->toXML());
?>