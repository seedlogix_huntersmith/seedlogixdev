<?php
require 'include/config.php';

if(substr($_REQUEST['From'], 0,4) == "sip:"){
	$searchSIP = true;
	$voip = $_REQUEST['From'];
	$stmt = $pdo->prepare('SELECT id, cid, user_id, user_parent_id, phone_num FROM phone_numbers WHERE sip = :did');
	$stmt->execute(array('did' => $voip));
	$user = $stmt->fetch();
	$stmt2 = $pdo->prepare('SELECT firstname, lastname FROM user_info WHERE user_id = :uid');
	$stmt2->execute(array('uid' => $user['user_id']));
	$userInfo = $stmt2->fetch();	
	$cname = $userInfo['firstname'].' '.$userInfo['lastname'];
} else {
	$voip = $_REQUEST['To'];
}
	


$dst = $_REQUEST['ForwardTo'];
if(! $dst)
    $dst = $_REQUEST['To'];
$src = $_REQUEST['CLID'];
if(! $src)
    $src = $_REQUEST['From'] or "";
//$cname = $_REQUEST['CallerName'] or "";
$hangup = $_REQUEST['HangupCause'];
$dial_music = $_REQUEST['DialMusic'];
$disable_call = $_REQUEST['DisableCall'];

$CallStatus = $_REQUEST['CallStatus'];
    
$r = new Response();

if($CallStatus=='completed'){
	
	$p = new RestAPI($conf_pilvo_authid, $conf_plivo_authtoken);
	
	$CallID = $_REQUEST['CallUUID'];
	$CallDuration = $_REQUEST['Duration'];
	$CallStartTime = $_REQUEST['StartTime'];
	$CallTo = $_REQUEST['To'];
	
	if($searchSIP) $stmt = $pdo->prepare('SELECT id, cid, user_id, user_parent_id, phone_num FROM phone_numbers WHERE sip = :did');
	else $stmt = $pdo->prepare('SELECT id, cid, user_id, user_parent_id, phone_num FROM phone_numbers WHERE phone_num = :did');
	$stmt->execute(array('did' => $voip));
	$user = $stmt->fetch();
	
	if($searchSIP) $CallFrom = $user['phone_num'];
	else $CallFrom = $_REQUEST['From'];
	
	$params2 = array(
						'call_uuid' => ''.$CallID.''
					);
	$response2 =$p->get_recordings($params2);
	
	$query = "INSERT INTO phone_records 
				(cid, user_id, user_parent_id, tracking_id, direction, call_uuid, caller_id, destination, length, recording, date)
				VALUES 
				('".$user['cid']."', '".$user['user_id']."', '".$user['user_parent_id']."', '".$user['id']."', 'outbound','".$CallID."', 
				'".$CallFrom."', '".$CallTo."', '".$CallDuration."', '".$response2['response']['objects'][0]['recording_url']."', '".$CallStartTime."')";
	$result = $pdo->query($query);	
	
} else {
	
		if($dst) {
			$dial_params = array();
			//if($src)
			$dial_params['callerId'] = $user['phone_num'];
			//if($cname)
			$dial_params['callerName'] = $cname;
			
			if(substr($dst, 0,4) == "sip:")
				$is_sip_user = TRUE;
			else
				$is_sip_user = FALSE;
			if($is_sip_user and in_array($disable_call, array("all", "sip"))) {
				$r->addHangup(array("reason" => "busy"));
			} elseif (! $is_sip_user and in_array($disable_call, array("all", "number"))) {
				$r->addHangup(array("reason" => "busy"));
			} else {
				$record_params = array(
				'action'=> ''.$pbx_url.'/record_action.php', # Submit the result of the record to this URL
				'method' => 'GET', # HTTP method to submit the action URL
				'maxLength'=> '5400', # Maximum number of seconds to record 
				//'transcriptionType' => 'auto', # The type of transcription required
				//'transcriptionUrl' => ''.$pbx_url.'/transcription.php', # The URL where the transcription while be sent from Plivo
				'redirect' => 'false', 
				'startOnDialAnswer' => 'true', 
				//'transcriptionMethod' => 'GET' # The method used to invoke transcriptionUrl 
			);

				if($dial_music)  {
					$r->addRecord($record_params);
					$dial_params["dialMusic"] = $dial_music;
					$d = $r->addDial($dial_params);
				} else
					$r->addRecord($record_params);
					$d = $r->addDial($dial_params);
				if($is_sip_user)
					$d->addUser($dst);
				else
					$d->addNumber($dst);
			} 
		 } else {
			 $r->addHangup();
		 }
}

header("Content-Type: text/xml");
echo($r->toXML());
?>