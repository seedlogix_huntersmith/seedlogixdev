<?php
require 'include/plivo.php';
require 'include/config.php';

$body = 'Please enter your conference PIN';
$attributes = array ('loop' => 2,);
$getdigitattributes = array (
	'action'=> 'http://' . $_SERVER["SERVER_NAME"] . '/PBX/conference-start.php',
	);

$r = new Response();
$g = $r->addGetDigits($getdigitattributes);
$g->addSpeak($body,$attributes);
$r->addSpeak("Input not received",array('language' => 'en-US', 'voice' => 'WOMAN'));

header('Content-Type: text/xml');
echo($r->toXML());

?>