<?php
require 'include/plivo.php';
require 'include/config.php';
require 'include/database.php';

$body = 'You may dial an extension at any time.';
$attributes = array('language' => 'en-US', 'voice' => 'WOMAN');
$getdigitattributes = array (
					'action'=> 'http://' . $_SERVER["SERVER_NAME"] . '/PBX/user.php',
					);

$r = new Response();
$g = $r->addGetDigits($getdigitattributes);
$g->addSpeak($body,$attributes);

## Select the user from the DB, if it's a user redirect.
$users = $db->query('SELECT id, name FROM Users');
foreach ($users as $row){
	$text = 'For ' . $row['name'] . ', Dial ' . $row['id'];
	$g->addSpeak($text,$attributes);
	$g->addWait(array('length' => 1));
}

$r->addSpeak("Input not received", $attributes);
echo($r->toXML());

?>