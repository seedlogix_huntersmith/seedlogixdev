<?php
require 'include/config.php';

$digits = $_REQUEST['Digits'];
$userid = $_REQUEST['userid'];

//var_dump($userid);

$stmt = $pdo->prepare('SELECT id, tree_num, tree_digit FROM ivr_tree WHERE ivr_id = :id');
$stmt->execute(array('id' => $userid));
$ivr_tree	=	array();
while($row	=	$stmt->fetch()){
	$ivr_tree[$row['id']]	=	$row;
}
$conf_ivr_menu = array();
//var_dump($ivr_tree);	
foreach($ivr_tree as $itree){
	
	  
  $conf_ivr_menu[$itree['tree_digit']] = array(
  				  'action' => 'user',
  				  'id' => $itree['tree_num'],);
}




$r = new Response();

$option = $conf_ivr_menu[$digits];

if ($option['action']){
	##Digits is valid option
	$url = ''.$pbx_url.'/' . $option['action'] . '.php';
	if ($option['id']){
		$url .= '?id=' . $option['id'];
	}
	$r->addRedirect($url);
}else{
	$errorSpeak = 'You have entered an invalid '.$option['action'].' by pressing '.$digits.'.';
	##Digits isn't valid option
	$r->addSpeak($errorSpeak);
	$r->addRedirect(''.$pbx_url.'/ivr.php');
}

header("Content-Type: text/xml");
echo($r->toXML());				  
?>