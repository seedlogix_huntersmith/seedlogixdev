<?php
require 'include/config.php';

$r = new Response();
$id = $_REQUEST['id'];
$cid = $_REQUEST['cid'];
$record_url = $_REQUEST['RecordUrl'];

$getdigits = $r->addGetDigits(
					array(
						'action' => 'https://' . $_SERVER["SERVER_NAME"]
						.'/api/plivo/pbx/vm-action.php?id=' .$id
						.'&cid='.$cid.'&RecordUrl=' . $record_url,
						'method' => 'GET'));
$getdigits->addSpeak('Press 8 to hear your anything');
$getdigits->addSpeak('Press 2 to start over');
$getdigits->addSpeak('Press 5 to finish call');

header('content-type: text/xml');
echo($r->toXML());

?>