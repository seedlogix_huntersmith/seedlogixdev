<?php

//Establish our DB connection
try {
    $db = new PDO("mysql:host=$conf_db_hostname;dbname=$conf_db_database", $conf_db_username, $conf_db_password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}

?>