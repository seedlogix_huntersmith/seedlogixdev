<?php
require 'include/plivo.php';
require 'include/config.php';
require 'include/database.php';


    $p = new RestAPI($conf_pilvo_authid, $conf_plivo_authtoken);
    
    //$response = $p->get_cdrs();
    //print_r ($response);

    // Filtering the records
    $params = array(
				//'end_time__gt' => '2016-04-28 04:00', # Filter out calls according to the time of completion. gte stands for greater than or equal.
				//'end_time__lt' => '2016-04-28 05:00', # Filter out calls according to the time of completion. lte stands for less than or equal.
				//'call_direction' => 'inbound', # Filter the results by call direction. The valid inputs are inbound
				# and outbound
				//'from_number' => '1111111111', # Filter the results by the number from where the call originated
				'call_uuid' => 'bf46002e-f3b3-11e6-aef7-a314a9bd6c84', # Filter the results by the number to which the call was made
				//'limit' => '20', # The number of results per page
				//'offset' => '0' # The number of value items by which the results should be offset
			);
    $response =$p->get_recordings($params);
    print_r ($response);
?>