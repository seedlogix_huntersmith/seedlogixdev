<?php
require 'include/plivo.php';
require 'include/config.php';
require 'include/database.php';

$gid = $_REQUEST['id'];	

$r = new Response();

if($gid){
	## Select the group from the DB
	$stmt = $db->prepare('SELECT number FROM Groups WHERE group = :gid');
	$stmt->execute(array('gid' => $gid));
	$members = $stmt->fetch();

	$dialattributes = array('timeout' => '20',
							'confirmSound' => 'http://' . $_SERVER["SERVER_NAME"] . '/PBX/static/confirm.xml',
							'confirmKey' => '1',
							'dialMusic' => $conf_hold_music,
							);
	$d = $r->addDial($dialattributes);
	foreach($members as $agent){
	$d->addNumber($agent['number']);		
	}
	$r->addSpeak('Sorry, no one is available to take your call');
	$r->addRedirect('http://' . $_SERVER["SERVER_NAME"] . '/PBX/' . $conf_default_route["module"] . '.php?id=' . $conf_default_route["id"]);
} else{
	//no ring group number!
	$r->addHangup()
}

header('Content-type: application/xml'); // Plivo is expecting XML
echo $r->toXML(); 				//return the response XML
?>