<?php
require 'include/config.php';

if ($_REQUEST['Digits']) {
	$id = $_REQUEST['Digits'];
} elseif ($_REQUEST['id']){
	$id = $_REQUEST['id'];	
	$cid = $_REQUEST['cid'];
}
$r = new Response(); 			//Create the Plivo response object

if($id > 0){
	## Select the user from the DB, if it's a user redirect.
	$stmt = $pdo->prepare('SELECT cell,sip FROM phone_numbers WHERE id = :id');
	$stmt->execute(array('id' => $id));
	$user = $stmt->fetch();
	
	
	$record_params = array(
        'action'=> ''.$pbx_url.'/record_action.php', # Submit the result of the record to this URL
        'method' => 'GET', # HTTP method to submit the action URL
        'maxLength'=> '1800', # Maximum number of seconds to record 
        //'transcriptionType' => 'auto', # The type of transcription required
        //'transcriptionUrl' => ''.$pbx_url.'/transcription.php', # The URL where the transcription while be sent from Plivo
		'redirect' => 'false', 
		'startOnDialAnswer' => 'true', 
        //'transcriptionMethod' => 'GET' # The method used to invoke transcriptionUrl 
    );
	//$dialattributes = array('timeout' => '20',
							//'confirmSound' => 'http://' . $_SERVER["SERVER_NAME"] . '/PBX/static/confirm.xml',
							//'confirmKey' => '1',
							//);
	if($user['cell']) $dialattributes = array('timeout' => '50');
	else $dialattributes = array('timeout' => '20');
	$r->addRecord($record_params);
	$d = $r->addDial($dialattributes);
	if($user['cell']) $d->addNumber($user['cell']);
	$d->addUser($user['sip']);
	$r->addRedirect(''.$pbx_url.'/voicemail.php?id=' . $id.'&cid='.$cid.'');
} else{
	## We don't know who to connect to, let's find out
	$greeting = 'Please enter an extension.';
	$attributes = array('loop' => 2,
						'language' => 'en-US',
						'voice' => 'WOMAN');
	$getdigitsattributes = array('action' => ''.$pbx_url.'/user.php',);
	$g = $r->addGetDigits($getdigitsattributes);
	$g->addSpeak($greeting,$attributes);
	$r->addSpeak("I did not catch that, sorry.",array('language' => 'en-US', 'voice' => 'WOMAN'));
}

header('Content-type: application/xml'); // Plivo is expecting XML
echo $r->toXML(); 				//return the response XML
?>