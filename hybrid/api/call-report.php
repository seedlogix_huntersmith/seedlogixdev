<?php

// output headers so that the file is downloaded rather than displayed
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=data.csv');

// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

// output the column headings
	fputcsv($output, array('SFDC ID', 'Account Number', 'User ID', 'Tracking Number', 'Company Name', 'Calls'));

$start = $_GET["start"];
$end = $_GET["end"];
$token = $_GET["token"];

if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$start) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$end) && $start != '' && $end != '' && $token = 'ahecSW2VAKxGBcZK2DhqZzB')
{
	if($start < '2017-04-09') $proceed = FALSE;
	else $proceed = TRUE;
}else{
	$proceed = FALSE;
}
if($proceed) {

	$is_dev = false;
//include phone class
#require_once('/home/sapphire/production-code/2012-08-24/admin/inc/analytics.class.php');
	require '../bootstrap.php';
	Qube::AutoLoader();
	@session_start();
	require_once(QUBEADMIN . 'inc/phone.class.php');
	$phoneRec = new Phone();

//AND hub_parent_id = '7024'
//AND hub_parent_id = '8220'

	$query = "SELECT u.api_id, u.account_number, u.user_id, u.company, p.phone_num FROM user_info u, phone_numbers p WHERE p.user_parent_id =
'6312'
			  AND p.user_id = u.user_id
              ORDER BY u.company ASC";    // temp 50235

	$results = $phoneRec->query($query);

//instantiate blog class for blog section in right panel
//get analytics info

// loop over the rows, outputting them
//fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip'));

	$blacklist = $phoneRec->blacklistCalls();

	while ($row = $phoneRec->fetchArray($results)) {

		//$query2 = "SELECT count(id) FROM leads WHERE hub_id = '" . $row['id'] . "' AND spam < 50 AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
		//$leads1 = $stats->queryFetch($query2);
		//$query3 = "SELECT count(id) FROM contact_form WHERE type_id = '" . $row['id'] . "' AND spamstatus = '0' AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
		//$leads2 = $stats->queryFetch($query3);

		//$numProspects = $leads1['count(id)'] + $leads2['count(id)'];


		if ($is_dev==false) {


			$callReportRec = $phoneRec->getCallDetailsNew($row['phone_num'], $start.','.$end);

			$r0 = $callReportRec['response']['meta']['total_count'];

			//$r0 = $callReportRec['response']['meta']['total_count'];

			//$piwikDataAccess = new PiwikReportsDataAccess();
			//$r0 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
//$r1	=		'';
			//$r1 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
//$r2	=		'';
			//$r2 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
		} else {
			$r0 = $r1 = $r2 = 'test';
		}

		fputcsv($output, array($row['api_id'], $row['account_number'], $row['user_id'], $row['phone_num'], $row['company'], $r0));
		flush();
//fputcsv($output, array($row['id'], $row['user_id'], $row['cid'], $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'], NULL, NULL, NULL, $numProspects));

	}

} else echo 'No data availble.';
