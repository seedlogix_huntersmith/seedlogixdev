<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
// Change system directory.
chdir(dirname(__FILE__));

// Load SeedLogix bootstrap.
require '../bootstrap.php';

// Load SeedLogix config and database.
Qube::ForceMaster();

function getOrganizationInfo($id) {
    $url = 'https://api.linkedin.com/v2/organizations/' . $id;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("authorization: Bearer {$_SESSION['access_token']}"));

    $response = curl_exec($curl);

    // Error in curl request.
    if (!empty(curl_error($curl))) {
        print("Curl error: " . curl_error($curl));
        exit;
    }

    $data = json_decode($response);

    if ($data->status >= 400) {
    	return null;
    } else {
	    return $data->localizedName;
    }

}


if(isset($_GET['pageid'])) {
					
		$query = "UPDATE blogs "
			. "SET linkedin_accessid = CONCAT(?, '\n', ?) "
			. "WHERE httphostkey = ?";
		$pdo = Qube::GetPDO()->prepare($query);

		// Add to concat field for URL profile id AND Company Name!!!
		if ($pdo->execute(array($_GET['pageid'], $_GET['token'], $_GET['bloghttphost']))) {
			print "LinkedIn Publishing has been configured. Please <a href='javascript:void(close());'>close this window.</a>";

		} else {
			Qube::Fatal("Unable to update linkedin_accessid in table 'blog' .", $_GET['bloghttphost']);

		}

} else {

    session_start();

    // Get blog http hostname.
    //$bloghttphost = !empty($_GET['bloghttphost']) ? $_GET['bloghttphost'] : $_SERVER['HTTP_HOST'];
    $bloghttphost = !empty($_GET['bloghttphost']) ? $_GET['bloghttphost'] : $_SESSION['bloghttphost'];


    if (empty($bloghttphost)) {
        die("Could not get blog http host!");
    }

    // Prepare the mysql query statement.
    $stmt = Qube::GetPDO()->prepare(
            'SELECT b.id, r.linkedin_app_id, r.linkedin_app_secret, r.linkedin_code, r.main_site 
                FROM resellers r, blogs b 
                WHERE b.httphostkey = ? 
                AND ((b.user_parent_id != 0 AND r.admin_user = b.user_parent_id) OR r.admin_user = b.user_id)');

    // Execute mysql query.
    if (!$stmt->execute(array($bloghttphost))) {
        // Failed query.
        die('Failed to get LinkedIn API information from database!');

    }

    // Fetch API information from database.
    $operationinfo = $stmt->fetch(PDO::FETCH_ASSOC);

    // Set LinkedIn app code.
    /*if (empty($operationinfo['linkedin_code'])) {
        // Not sure if you can use the same code??? So will die for now...
        die('Failed to get LinkedIn API code from database!');

    } else {
        // Set LinkedIn app code to reseller's value.
        $linkedin_app_code = $operationinfo['linkedin_code'];

    }*/

    // Set LinkedIn app id.
    if (empty($operationinfo['linkedin_app_id'])) {
        // Set LinkedIn app id to SeedLogix default if reseller is empty.
        //$linkedin_app_id = '86djy22jquj41b'; // <- !!Eventually change to SeedLogix, using Hunter's account for testing.
        $linkedin_app_id = '7861cxkfwwx8ph'; // <- SeedLogix Live.

    } else {
        // Set LinkedIn app id to reseller's value.
        $linkedin_app_id = $operationinfo['linkedin_app_id'];

    }

    // Set LinkedIn app secret.
    if (!$operationinfo['linkedin_app_secret']) {
        // Set LinkedIn app secret to SeedLogix default if reseller is empty.
        //$linkedin_app_secret = 'VIUlKAzHyoBCFXdW'; // <- !!Eventually change to SeedLogix, using Hunter's account for testing.
        $linkedin_app_secret = 'Vtq1188rf2p8lhIY'; // <- SeedLogix Live

    } else {
        $linkedin_app_secret = $operationinfo['linkedin_app_secret'];

    }

    $reseller_host = 'login.seedlogix.com';
    //$reseller_host = 'seedlogix.dev';


    // DO GET ACCESS TOKEN STUFF BELOW!!!
    session_start();

    // OAuth 2 Control Flow
    if (isset($_GET['error'])) {
        // LinkedIn returned an error
        die("LinkedIn Authentication Error <br />" . $_GET['error'] . ": " . $_GET['error_description'] . "\r\n");

    } else if (isset($_GET['code'])) {
        // User authorized your application
        if ($_SESSION['state'] == $_GET['state']) {
            // Get token so you can make API calls

            // Set configuration parameters array to get LinkedIn access token.
            $params = array(
                'grant_type' => 'authorization_code',
                'client_id' => $linkedin_app_id,
                'client_secret' => $linkedin_app_secret,
                'code' => $_GET['code'], // <- !!NEED TO ADD THIS TO RESELLER TABLE IN DATABASE .
                'redirect_uri' => 'http://' . $reseller_host . '/api/linkedin-blog-app.php?bloghttphost=' . urlencode($bloghttphost)
            );
            //var_dump($params); exit;

            $url = "https://www.linkedin.com/oauth/v2/accessToken";

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

            $response = curl_exec($curl);
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            // Error in curl request.
            if (!empty(curl_error($curl))) {
                die("Curl error: " . curl_error($curl));
            }

            //print($response);
            $token = json_decode($response);

            //curl_close($curl); exit;

            // Store access token and expiration time
            $_SESSION['access_token'] = $token->access_token;
            $_SESSION['expires_in'] = $token->expires_in;

            // Get some basic info.
	          $url = 'https://api.linkedin.com/v2/organizationalEntityAcls?q=roleAssignee';

	          $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("authorization: Bearer {$_SESSION['access_token']}"));

            $response = curl_exec($curl);
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            // Error in curl request.
            if (!empty(curl_error($curl))) {
                print("Curl error: " . curl_error($curl));
                exit;
            }

            $profile_response = json_decode($response);

            $organizationIds = [];
            foreach ($profile_response->elements as $element) {
                list(,,,$id) = explode(':', $element->organizationalTarget);
                array_push($organizationIds, $id);
            }

            curl_close($curl);

            if ($profile_response->status >= 400) {
                echo "An error occurred: " . $profile_response->message;
                exit;
            }

            echo '<h1>Choose a page to publish your blog posts to</h1>
                    <ul>';

            foreach($organizationIds as $id) {
                $organization = getOrganizationInfo($id);
                if ($organization !== null) {
	                echo '<li>
        <a href="linkedin-blog-app.php?bloghttphost=' . $bloghttphost . '&pageid=' . $id . '&token=' . $token->access_token . '">' . htmlentities($organization) . '</a></li>';
                }
            }

            echo '</ul>';



            /*$query = "UPDATE blogs "
                    . "SET linkedin_accessid = CONCAT(?, '\n', ?, '\n', ?, '\n', ?, '\n', ?, '\n', ?) "
                    . "WHERE httphostkey = ?";
            $pdo = Qube::GetPDO()->prepare($query);

            // Add to concat field for URL profile id AND Company Name!!!
            if ($pdo->execute(array(urlencode($profile_response->publicProfileUrl), $profile_response->lastName, $params['grant_type'], $params['client_id'], $params['client_secret'], $token->access_token, $bloghttphost))) {
                print "LinkedIn Publishing has been configured. Please <a href='javascript:void(close());'>close this window.</a>";

            } else {
                Qube::Fatal("Unable to update linkedin_accessid in table 'blog' .", $bloghttphost);

            }*/

        } else {
            // Cross Site Request Forgery attack? Or mixed up states?
            die ("Non-matching states found!<br />\r\n");

        }

    }  else {
        /*if ((empty($_SESSION['expires_in'])) || (time() > $_SESSION['expires_in'])) {
            // Token has expired, clear the state
            print("Token expired!<br />");
            session_unset();

        }*/


        if (empty($_SESSION['access_token'])) {
            // (STEP 1) Start authorization process (if there is no session or session is expired).
            //print "Get authorization code!<br />\r\n";

            session_start();
            $bloghttphost = !empty($_GET['bloghttphost']) ? $_GET['bloghttphost'] : $_SERVER['HTTP_HOST'];

            // Store blog http host to session.
            $_SESSION['bloghttphost'] = $bloghttphost;

            $params = array(
                'response_type' => "code",
                'client_id' => $linkedin_app_id,
                'redirect_uri' => "https://" . $reseller_host . "/api/linkedin-blog-app.php",
                'state' => uniqid("", true),
                'scope' => "rw_organization_admin"
            );

            $url = "https://www.linkedin.com/oauth/v2/authorization?" . http_build_query($params);
            //var_dump($url);

            $_SESSION['state'] = $params['state'];

            header("Location: $url");
            exit;
        }
    }

} //end if page id.
