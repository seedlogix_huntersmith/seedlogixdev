<?php


// output headers so that the file is downloaded rather than displayed
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=data.csv');

// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

$admin = $_GET["admin"];

// output the column headings
if($admin==1) {
	fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'City', 'State', 'Visits', 'Leads'));
} else {
	fputcsv($output, array('Account Type', 'SFDC ID', 'Account Number', 'Website ID', 'User ID', 'Tracking ID', 'Theme',
		'Logo', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip', 'Visits', 'Leads'));
}

$start = $_GET["start"];
$end = $_GET["end"];
$token = $_GET["token"];

if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$start) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$end) && $start != '' && $end != '' && $token = 'ahecSW2VAKxGBcZK2DhqZzB')
{
	$proceed = TRUE;
}else{
	$proceed = FALSE;
}
if($proceed) {

$is_dev = false;
//include phone class
#require_once('/home/sapphire/production-code/2012-08-24/admin/inc/analytics.class.php');
	require '../bootstrap.php';
	Qube::AutoLoader();
	@session_start();
	require_once QUBEADMIN . 'inc/analytics.class.php';
	$stats = new Analytics();

//AND hub_parent_id = '7024'
//AND hub_parent_id = '8220'

	if($admin==1) {

		$query = "SELECT id, user_id, cid, name, tracking_id, domain, company_name, phone, city, state FROM hub WHERE user_id = '6312'
              AND trashed = '0000-00-00'
			  AND hub_parent_id != '18953'
			  AND hub_parent_id != '23395'
			  AND hub_parent_id != '47868'
			  AND hub_parent_id != '8015'
			  AND hub_parent_id != '18951'
			  AND hub_parent_id != '18953'
			  AND hub_parent_id != '18952'
			  AND hub_parent_id != '22671'
			  AND domain != 'http://feed1.powerleads.biz'
			  AND domain != 'http://feed2.powerleads.biz'
			  AND domain != 'http://feed3.powerleads.biz'
			  AND domain != 'http://feed4.powerleads.biz'
			  AND domain != 'http://www.alibidealer.com'
			  AND hub_parent_id != 0
			  AND httphostkey != ''
              AND domain != '' AND tracking_id != 0
              AND cid != '99999'
              ORDER BY name ASC";    // temp 50235

		$hubs = $stats->query($query);

	} else {
		$query = "SELECT h.id, h.user_id, h.cid, h.name, h.tracking_id, h.hub_parent_id, h.domain, h.company_name, h.phone, h.street, h
.city, h.state, h.zip, h.created, h.logo, u.account_number, u.profile_photo, u.api_id, r.role_ID FROM hub h, user_info u, 6q_userroles r WHERE h.user_parent_id = '6312'
              AND h.trashed = '0000-00-00'
			  AND h.hub_parent_id != '18953'
			  AND h.hub_parent_id != '23395'
			  AND h.hub_parent_id != '47868'
			  AND h.hub_parent_id != '8015'
			  AND h.hub_parent_id != '18951'
			  AND h.hub_parent_id != '18953'
			  AND h.hub_parent_id != '18952'
			  AND h.hub_parent_id != '22671'
			  AND h.domain != 'http://feed1.powerleads.biz'
			  AND h.domain != 'http://feed2.powerleads.biz'
			  AND h.domain != 'http://feed3.powerleads.biz'
			  AND h.domain != 'http://feed4.powerleads.biz'
			  AND h.hub_parent_id != 0
			  AND h.httphostkey != ''
              AND h.domain != '' AND tracking_id != 0
              AND h.cid != '99999'
			  AND h.user_id = u.user_id
			  AND h.user_id = r.user_ID
              ORDER BY h.name ASC";    // temp 50235

		$hubs = $stats->query($query);

	}
//instantiate blog class for blog section in right panel
//get analytics info

// loop over the rows, outputting them
//fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip'));

	while ($row = $stats->fetchArray($hubs)) {

		$query2 = "SELECT count(id) FROM leads WHERE hub_id = '" . $row['id'] . "' AND spam < 50 AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
		$leads1 = $stats->queryFetch($query2);
		$query3 = "SELECT count(id) FROM contact_form WHERE type_id = '" . $row['id'] . "' AND spamstatus = '0' AND created < '".$end." 23:59:59' AND created > '".$start." 00:00:00'";
		$leads2 = $stats->queryFetch($query3);

		$numProspects = $leads1['count(id)'] + $leads2['count(id)'];

		if($row['role_ID']==51) $role_name = 'PowerLeads';
		if($row['role_ID']==24) $role_name = 'PowerLeads Plus';
		if($row['hub_parent_id']==108931){
			if($row['user_id']==6312) $theme = 'Theme 1 - SC Branded';
			else if(strpos($row['domain'], 'alibidealer.com')){
				$theme = 'Alibi Dealer';
			}
			else $theme = 'Theme 1';

		}
		if($row['hub_parent_id']==116695){
			if($row['user_id']==6312) $theme = 'Theme 2 - SC Branded';
			else $theme = 'Theme 2';
		}
		if($row['hub_parent_id']==117342) {
			if($row['user_id']==6312) $role_name = 'Theme 3 - SC Branded';
			$theme = 'Theme 3';
		}
		if($row['hub_parent_id']==20561) $theme = 'SC Branded';

		if($row['logo']) $logo = 'Yes';
		else if($row['profile_photo']) $logo = 'Yes';
		else $logo = 'No';

		if ($is_dev==false) {
			$piwikDataAccess = new PiwikReportsDataAccess();
			$r0 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
//$r1	=		'';
			//$r1 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
//$r2	=		'';
			//$r2 = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], ''.$start.','.$end.'');
		} else {
			$r0 = $r1 = $r2 = 'test';
		}

		if($admin==1){
			fputcsv($output, array($row['id'], $row['user_id'], $row['tracking_id'], $row['company_name'], $row['domain'], $row['phone'], $row['city'], $row['state'],
				$r0,
				$numProspects));

		} else {
			fputcsv($output, array($role_name, $row['api_id'], $row['account_number'], $row['id'], $row['user_id'], $row['tracking_id'], $theme, $logo, $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'],
				$r0,
				$numProspects));
		}
		flush();
//fputcsv($output, array($row['id'], $row['user_id'], $row['cid'], $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'], NULL, NULL, NULL, $numProspects));

	}

} else echo 'Access Denied';
