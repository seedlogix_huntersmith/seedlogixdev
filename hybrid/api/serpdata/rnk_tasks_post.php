<?php
//You can download this file from here https://api.dataforseo.com/_examples/php/_php_RestClient.zip
require('RestClient.php');

$api_url = 'https://api.dataforseo.com/';
try {
	//Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/
	$client = new RestClient($api_url, null, 'login', 'password');
} catch (RestClientException $e) {
	echo "\n";
	print "HTTP code: {$e->getHttpCode()}\n";
	print "Error code: {$e->getCode()}\n";
	print "Message: {$e->getMessage()}\n";
	print  $e->getTraceAsString();
	echo "\n";
	exit();
}


$post_array = array();

//for example, some data selection cycle for tasks
for ($i = 0; $i < 3; $i++) {

	// example #1 - simplest
	// you set only URL of website and searchengine URL.
	// This search engine URL string will be converted (at our side, I writing this just only to show you
	// different possibilities to use it) to our internal parameters (see example #3 for details):
	// "se_id", "loc_id", "key_id" ( actual and fresh list are located here: "se_id":
	// https://api.dataforseo.com/v2/cmn_se , "loc_id": https://api.dataforseo.com/v2/cmn_locations )
	// If task set is success, this *_id will be returned in results: 'v2/rnk_tasks_post' so you can use it.
	// The task set can fail, if you will set not-existent searchengine, for example.
	// Disadvantages: You cannot work with "map pack", "maps", "mobile"
	$my_unq_id = mt_rand(0,30000000); //your unique ID (like you DB "id" field. type 'string'). will be returned with all results
	$post_array[$my_unq_id] = array(
	"priority" => 1,
	"site" => "dataforseo.com",
	"url" => "https://www.google.co.uk/search?q=seo%20data%20api&hl=en&gl=GB&uule=w+CAIQIFISCXXeIa8LoNhHEZkq1d1aOpZS"
	);

	// example #2 - more powerful than #1, but more simple and not so powerful as example #3
	// All parametes you will set as text data.
	// All data will be converted (at our side, I writing this just only to show you different possibilities to
	// use it) to our internal parameters (see example #3 for details): "se_id", "loc_id", "key_id" ( actual and
	// fresh list are located here: "se_id": https://api.dataforseo.com/v2/cmn_se ,
	// "loc_id": https://api.dataforseo.com/v2/cmn_locations )
	// If task set is success, this *_id will be returned in results: 'v2/rnk_tasks_post' so you can use it.
	// The task set can fail, if you will set not-existent searchengine, for example.
	// Disadvantages: time to convert data to our internal parametes
	$my_unq_id = mt_rand(0,30000000); //your unique ID (like you DB "id" field. type 'string'). will be returned with all results
	$post_array[$my_unq_id."##2"] = array(
	"priority" => 1,
	"site" => "dataforseo.com",
	"se_name" => "google.co.uk",
	"se_language" => "English",
	"loc_name_canonical"=> "London,England,United Kingdom",
	"key" => mb_convert_encoding("seo data api", "UTF-8")
	//,"pingback_url" => "http://your-domain.com/pingback_url_example.php?task_id=$task_id" //see pingback_url_example.php script
	);

	// example #3 - most powerful and fastest. All parameters you will set up in our internal format.
	// Actual and fresh list are located here: "se_id": https://api.dataforseo.com/v2/cmn_se ,
	// "loc_id": https://api.dataforseo.com/v2/cmn_locations
	$my_unq_id = mt_rand(0,30000000); //your unique ID (like you DB "id" field. type 'string'). will be returned with all results
	$post_array[$my_unq_id."##4"] = array(
	"priority" => 1,
	"site" => "dataforseo.com",
	"se_id" => 22,
	"loc_id" => null,
	"key_id" => 62845222
	//,"postback_url" => "http://your-domain.com/postback_url_example.php" //see postback_url_example.php script
	);

	//This example has a cycle of up to 3 elements, but in the case of large count of tasks - POST by 100 elements
	if (count($post_array) > 99) {
		try {
			// POST /v2/rnk_tasks_post/$data
			// $tasks_data must by array with key 'data'
			$task_post_result = $client->post('/v2/rnk_tasks_post', array('data' => $post_array));
			print_r($task_post_result);

			//do something with post results

			$post_array = array();
		} catch (RestClientException $e) {
			echo "\n";
			print "HTTP code: {$e->getHttpCode()}\n";
			print "Error code: {$e->getCode()}\n";
			print "Message: {$e->getMessage()}\n";
			print  $e->getTraceAsString();
			echo "\n";
		}
	}
}

if (count($post_array) > 0) {
	try {
		// POST /v2/rnk_tasks_post/$data
		// $tasks_data must by array with key 'data'
		$task_post_result = $client->post('/v2/rnk_tasks_post', array('data' => $post_array));
		print_r($task_post_result);

		//do something with post results

	} catch (RestClientException $e) {
		echo "\n";
		print "HTTP code: {$e->getHttpCode()}\n";
		print "Error code: {$e->getCode()}\n";
		print "Message: {$e->getMessage()}\n";
		print  $e->getTraceAsString();
		echo "\n";
	}
}

$client = null;
?>