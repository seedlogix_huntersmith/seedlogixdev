<?php
//You can download this file from here https://api.dataforseo.com/_examples/php/_php_RestClient.zip
require('RestClient.php');

$api_url = 'https://api.dataforseo.com/';
try {
	//Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/
	$client = new RestClient($api_url, null, 'admin@ignitedlocal.com', 'BeOwFfmMZ8Sx2tQV');
} catch (RestClientException $e) {
	echo "\n";
	print "HTTP code: {$e->getHttpCode()}\n";
	print "Error code: {$e->getCode()}\n";
	print "Message: {$e->getMessage()}\n";
	print  $e->getTraceAsString();
	echo "\n";
	exit();
}

/*
#1 - get ALL ready results
recommended use of getting results:
run this script by cron with 10-60 streams, every minute with random delay 0-30 sec.
usleep(mt_rand(0,30000000));
*/



/*
#2 - get one result by task_id
*/
try {

	// GET /api/v1/tasks_get/$task_id
	$task_get_result = $client->get('v2/rnk_tasks_get/2757878687');
	print_r($task_get_result);
	echo $task_get_result['results']['organic'][0]['task_id'].'<br>';
	echo $task_get_result['results']['organic'][0]['result_position'];

	//do something with result

} catch (RestClientException $e) {
	echo "\n";
	print "HTTP code: {$e->getHttpCode()}\n";
	print "Error code: {$e->getCode()}\n";
	print "Message: {$e->getMessage()}\n";
	print  $e->getTraceAsString();
	echo "\n";
}

$client = null;
?>