<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 * 
 * - includes the 6QUBE CORE
 * - if invoked via command line, sets SERVER_NAME, HTTP_HOST, AND REQUEST_URI values based on cli parameters
 * 
 */

defined('HYBRID_PATH') || define('HYBRID_PATH', dirname(__FILE__) . '/');
define('HYBRID_LIB', HYBRID_PATH . 'library/');
define('HYBRID_CDN', HYBRID_PATH . '../smarty/cdn/');
define('APP_CONTROLLERS', HYBRID_PATH . 'controllers/');
define('PIWIK_VERSION', 2);

set_include_path(get_include_path() . PATH_SEPARATOR . HYBRID_LIB);

class HubNotFoundException extends Exception {
    
}

//if(defined('QUBE_NOLAUNCH')){
//    require_once dirname(__FILE__) . '/../admin/6qube/core.php';
//    return; // end initialization
//}

define('IS_HYBRID_CLI', !isset($_SERVER['REQUEST_METHOD']) && !defined('QUBECONFIG') && ($_SERVER['argc'] > 0) && $_SERVER['argv'][1] != '--bootstrap'); // bypass cli processing on phpstorm/phpunit execution

require_once HYBRID_PATH . '/../admin/6qube/core.php';

// Important. USING HTTP_HOST . We do not want to relly on SERVER_NAME because that is virtual-host config dependent. 
// ENABLE CONSOLE EXECUTION / EMULATION.
if (IS_HYBRID_CLI) {
    Qube::AutoLoader();

    // DEFINE WHICH config file/server TO use
    $_SERVER['SERVER_NAME'] = $_SERVER['HOSTNAME'];

    $cliopts = array('hostname|h=s' => 'The hostname to be defined for determining the configuration file to use.',
        'req|rq=s' => 'The request uri to be simulated and used for reporting in Qube::FatalError',
        'debug|deb=i' => 'Debug, 0 = false, 1 = TRUE',
        'nobanner|i' => 'Hide Banner Help.');

    if (function_exists('get_cli_args')) {
        $cliopts += get_cli_args();
    }

    $Zopts = new Zend_Console_Getopt($cliopts);

#	var_dump($cliopts, $cliopts->hostname);	exit;

    function welcome($Zopts) {
        if ($Zopts->hostname)
            $_SERVER['SERVER_NAME'] = $Zopts->hostname;
        if ($Zopts->debug)
            $_COOKIE['amado'] = md5('amado');

        $configname = Qube::get_configname();
        $hostname = $_SERVER['HOSTNAME'];

        if (!$Zopts->nobanner) {
            printf("Welcome CLI User.\nHOSTNAME: %s\nSERVER_NAME: %s\nIS_DEV: %d\nConfigname: %s\n\n", $hostname, $_SERVER['SERVER_NAME'], Qube::IS_DEV(), $configname);

            echo $Zopts->getUsageMessage(), "\n\n";
        }

        if ($configname === FALSE) {
            echo "Exiting. Bad configname.";
            exit;
        }
    }

    try {
        welcome($Zopts);

        if ($Zopts->hostname) {
            $_SERVER['HTTP_HOST'] = $Zopts->hostname;
            $_SERVER['REQUEST_URI'] = $Zopts->cli;
        } else {
            $_SERVER['HTTP_HOST'] = '';
            $_SERVER['REQUEST_URI'] = '/cli/' . $argv[0];
        }
    } catch (Zend_Console_Getopt_Exception $e) {
        
    }
}

#require_once HYBRID_PATH . 'classes/QubeEvent.php';
// support for SIMHOST
//@session_start();
if (!defined('QUBE_NOLAUNCH')) {
    QubeEvent::Bind('Router.SessionStart', function ($EV_KEY, $EV_NAME, RouterRequest $RR) {
        if (in_array($RR->getHostString(), array('secure.6qube.com', 'secure-url.co')) || Qube::IS_DEV()) {
            if (isset($_GET['SIMHOST']))
                $_SESSION['SIMHOST'] = $_GET['SIMHOST'];

            if (isset($_SESSION['SIMHOST']))
                $RR->setHostString($_SESSION['SIMHOST']);

            else if (!Qube::IS_DEV())
                die('<h1>Secure Browsing by Secure URL</h1>');
        }

        //Search Engine
        if (!isset($_SESSION['searchEngine'])) {
            if (isset($_SERVER['HTTP_REFERER'])) {
                preg_match('/http(?:s|):\/\/([^\/]*).*/', $_SERVER['HTTP_REFERER'], $searchEngineDomain);
                $searchengine = strtolower($searchEngineDomain[1]);

                if (strpos($searchengine, 'bing') !== false) {
                    $_SESSION['searchEngine'] = 'Bing';
                    preg_match('/(?:\?|\&)q\=([^&]*)/', $_SERVER['HTTP_REFERER'], $keyword);
                    $_SESSION['seKeyword'] = urldecode($keyword[1] ? $keyword[1] : '');
                } elseif (strpos($searchengine, 'google')) {
                    $_SESSION['searchEngine'] = 'Google';

                    //Not sure how obtain keywords from google
                    preg_match('/(?:\?|\&)q\=([^&]*)/', $_SERVER['HTTP_REFERER'], $keyword);
                    $_SESSION['seKeyword'] = urldecode($keyword[1] ? $keyword[1] : '');
                }
            }
        }
    });
}

#register_shutdown_function('qube_shutdown');

function qube_shutdown() {
    $error = error_get_last();

    if ($error !== NULL && in_array($error['type'], array(
                E_ERROR, E_PARSE, E_USER_ERROR, E_USER_WARNING, E_RECOVERABLE_ERROR
            ))) {
        $errno = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr = $error["message"];
        Qube::Fatal('fatal error', $error);
    }
}
