<?php
//\ini_set('display_errors', true);
//error_reporting(E_ALL);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var_dump($_POST, $_FILES);

// Create a new folder if non existent.
if (!empty($_POST['user_id'])) {
    $user_folder_id = $_POST['user_id'];
    
} else {
    $user_folder_id = "missing_id";
    
}

$target_dir = "../csv_uploads/$user_folder_id/";
if (!is_dir($target_dir)) {
    mkdir($target_dir);
}

$file_name = basename($_FILES["fileToUpload"]["name"]);

$csvFileType = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

$file_name_ext = implode('.', explode('.', $file_name, -1));

$new_file_name = "{$file_name_ext}_{$user_folder_id}.csv";

$target_file = $target_dir . $new_file_name;

// Check if csv file is a actual csv
if(isset($_POST["csv_upload"])) {
    //$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($csvFileType != "csv") {
        echo "Only CSV files can be uploaded.";
        //exit;
        
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file '" . basename($_FILES["fileToUpload"]["name"]) . "' has has been uploaded.";
            
        } else {
            echo "Sorry, there was an error uploading your file.";
            
        }
        
        //exit;
    }
}
