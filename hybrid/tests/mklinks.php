<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';


if(isset($argc) || $_GET['amado'] != md5('amado')) die('Call via HTTP with passwd');

        function sitedatalinkpath($valuenowww)
        {
            $numparts  =   preg_match('/^(..)(..).*$/', $valuenowww, $matches);
		if(!$numparts) return false;
            $prevdir    =   '';
            $curdir     =   '';
            for($i  =   0;  $i < 2; $i++)
            {
                $prevdir    .=  $matches[$i+1];
                $curdir     =   QUBEROOT . 'sitedata/' . $prevdir;
#		$curdir	=	QUBEROOT . 'sitedata/' . $r
                if(!is_dir($curdir))
                {
                    mkdir($curdir);
                }

                $prevdir .= '/';
            }
            
            return $curdir . '/' . $valuenowww;
        }

$q  =   Qube::Start();
$Q  =   $q->queryObjects('HubModel', 'H')->Where('httphostkey != "" AND trashed = 0')->Exec();

/* @var $reseller ResellerModel */
while($hub =   $Q->fetch())
{
	echo $hub->id, ' ';
	$lnkpath	=	sitedatalinkpath($hub->httphostkey);

	if($lnkpath	=== false){
		echo 'Invalid! '; continue;
}
	if(is_link($lnkpath)){
		 echo ' :)';
		continue;
}else{
		if(symlink('../../../hubs-domains/' . $hub->user_id . '/' . $hub->id, $lnkpath))
		echo ' :D';
		else
			echo ' :(';
}
echo "\n";
		
}
