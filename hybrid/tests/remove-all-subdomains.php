<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';

$name   =   $argv[5];   //'bbc.co.uk';    //

if($argv[8] != 'confirmed')
{
    echo ' Usage: script.php .. .. .. SERVERALIAS HOST USER PASS confirmed';
    
    var_dump($argv);
    exit;
}

#require_once QUBEADMI . 'inc/cpanel.xmlapi.php';
require_once HYBRID_PATH . 'classes/DomainConfig.php';

$xmlapi =   new KennedyXmlApi();	#LincolnXmlApi();	#new xmlapi($argv[4], $argv[6], $argv[7]);

$q  =   Qube::Start();
$Q  =   $q->queryObjects('HubModel', 'H')->Where('httphostkey != "" AND trashed = 0 and lincoln_domain	=	0')->Exec();

$domains	=	array();
$have_wild	=	array();

/* @var $reseller ResellerModel */
while($hub =   $Q->fetch())
{
	$subname	=	$hub->httphostkey;
	$name			=	preg_replace('/^.*?([^\.]+.[^\.]+)$/', '\1', $subname);

	if(in_array($name, $domains)){
#		echo "$subname already processed as $name\n";
		continue;
	}

	$domains[]	=	$name;

#continue;	// create star subdomain FIRST
	echo "Processing $name\n";
	$result  =   $xmlapi->api2_query($argv[6], "SubDomain", "listsubdomains",
                array('regex' => $name));

	$backup_subd_fname	=	'subdomains-' . $name . '.xml';

	if(!file_exists($backup_subd_fname))		$result->asXML($backup_subd_fname);	//'subdomains-' . $name . '.xml');

if(@$argv[9]	==	'saveonly')
{
	echo 'Save only. Please confirm file exists: ', $backup_subd_fname, "\n";
	continue;
}

#echo "Deleting: ";
#var_dump($result->data);

foreach($result->data as $info)
{
    if(!preg_match('/^([^\.]*)\.' . $name . '$/', $info->domain, $match)) continue;
            
    $sub    =   $match[1];
    if($sub == '*')
    {
        echo 'Skipping *', "\n";
	$have_wild[]	=	$name;
        continue;
    }
    
    $basedir = (string)$info->basedir;
    
#    if(!preg_match('/^public_html\/hubs\/domains\/[0-9]+/', $basedir))		#6qube2 path
#    if(!preg_match('/^public_html\/6qube\.com\/hubs\/domains\/[0-9]+/', $basedir)) #6qube1 hubs path

	$subdomaintype	=	'unknown';
	if(preg_match('/^public_html\/6qube\.com\/blogs\/domains\/[0-9]+/', $basedir))
		$subdomaintype	=	'blog';
	if($basedir	==	'public_html/6qube.com/press')
		$subdomaintype	=	'press';
	if($basedir	==	'public_html/6qube.com/blogs')
		$subdomaintype	=	'blogs';
	if($basedir	==	'public_html/6qube.com/search')
		$subdomaintype	=	'search';
	if($basedir	==	'public_html/6qube.com/articles')
		$subdomaintype	=	'articles';
	if($basedir	==	'public_html/6qube.com/browse')
		$subdomaintype	=	'browse';
	if($basedir	==	'public_html/6qube.com/hubs')
		$subdomaintype	=	'hubs';
	if($basedir	==	'public_html/6qube.com/local')
		$subdomaintype	=	'local';

        echo "$info->domain Basedir: $basedir Type: $subdomaintype\n";
	
	if($subdomaintype	==	'unknown'){
        continue;
    }
    
    echo $info->domain, ":sub=", $match[1], ".. Deleting: ";
    
    $result2    =    $xmlapi->api2_query($argv[6], "SubDomain", "delsubdomain",
                            array('domain' => (string)$info->domain)
            );
    
    echo 'result = ', $result2->data->result, "\n";
    
#    usleep(500000);
    
}
}

echo "Done deleting subdomains! Now Setting up Wildcard Subdomains:";


foreach($domains as $checkit)
{
	$domain	=	$checkit;
	if(in_array($checkit, $have_wild)) continue;

echo "\nSetting up *.$domain : ";
	$domainconfig	=	new DomainConfig($domain);	
	try{
		$result	=	$domainconfig->setupWildcardHOST($xmlapi, false);
	}catch(HybridWebsiteSetupFailureException $e){
		echo "Fatal Error: Maybe $domain does not exist? ";
		echo $e->getMessage();
continue;
		exit;
	}

echo $result ? '1' : '0';

echo "\n";
}


echo "ALL DONE!";
