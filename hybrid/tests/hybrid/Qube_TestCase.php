<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * 
 * Initializes Qube Class loader and other cool shit! (but not the db)
 * 
 */
abstract class Qube_TestCase extends PHPUnit_Framework_TestCase {
    
    /**
     *
     * @var Qube
     */
    protected static $qubeTEST =   NULL;
    
    public static function setUpBeforeClass() {
        
#			if(!isset($_SERVER['QUBECONFIG']))
        $_SERVER['QUBECONFIG']  =   EMPTYDBCONFIG;

        self::$qubeTEST =   Qube::Start();
    }
    
		static function truncateTables(array $tables)
		{
			return false;
			self::$qubeTEST->query('SET FOREIGN_KEY_CHECKS = 0');
			foreach($tables as $tblname)
			{
				
				self::$qubeTEST->query('TRUNCATE TABLE ' . $tblname);		
			}
			self::$qubeTEST->query('SET FOREIGN_KEY_CHECKS = 1');
		}
		
    public static function getTestDataAsArgsArray($filename, $varname =   'data'){
        require TESTDATAFILES . $filename;
        
        $args   =   array();
        foreach($$varname as $row){
            $args[] =   array($row);
        }
        
        return $args;
        
        $xa  =     array_map('func_get_args', $map);
        $xb  =   array_map('array_pop', $xa);
        
//        var_dump($xb);exit;
//        echo 'test';
        return $xb;
    }
    
		static function runSQLFile($filename)
		{
			self::$qubeTEST->query( file_get_contents(//HYBRID_PATH .'tests/' . 
								TESTDATAFILES . $filename));
		}
}
