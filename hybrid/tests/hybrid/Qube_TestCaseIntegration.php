<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require TESTDIR . 'Qube_TestCase.php';
/**
 * 
 * Initializes Qube Class loader and other cool shit! (but not the db)
 * 
 */
abstract class Qube_TestCaseIntegration extends Qube_TestCase {
    
	/**
	 *
	 * @var UserWithRolesModel
	 */
	protected static $systemAccount		=	NULL;
	
	
	/**
	 * @var ModelManager
	 */
	private $mm	=	NULL;
	
	/**
	 * 
	 * @return ModelManager
	 */
	protected function getModelManager()
	{
		if(!$this->mm)
		{
			$this->mm	=	new ModelManager();
		}
		
		return $this->mm;
	}
	
	function createSystemUserMock($id	=	0)
	{
//		if(!self::$systemAccount)
//		{
			self::$systemAccount	=	$this->createUserMock(array('is_system' => true));
			self::$systemAccount->expects($this->any())->method('getID')
						->will($this->returnValue($id));
            self::$systemAccount->username = "Admin";
//		}
		return self::$systemAccount;
	}
	
	/**
	 * 
	 * @param array $permissions
	 * @return UserWithRolesModel
	 */
	function createUserMock(array $permissions)
	{
		$Mock	=	$this->getMock('UserWithRolesModel');
        if(isset($permissions['is_system'])){
            $Mock->expects($this->any())->method('can')->will($this->returnValue(true));
			$Mock->expects($this->any())->method('getID')->will($this->returnValue(7));
        }else{
            $p = array();

            foreach($permissions as $permission => $result)
            {
//			$Mock->expects($this->any())->method('can')
//				->with($this->equalTo($permission))
//				->will($this->returnValue($result));
                $p[] = array($permission, 0, $result);


            }
                $Mock->expects($this->any())->method('can')->will($this->returnValueMap($p));
        }

        return $Mock;
	}
	
	/**
	 * 
	 * @param type $data
	 * @param type $roles
	 * @return UserModel
	 */
	function AssertCreateUser($data, UserWithRolesModel $actionUser)
	{
		$uda	=	new UserDataAccess();
		$uda->setActionUser($actionUser);
		
		// create db user
		$result	=	$this->getModelManager()->doSaveUser($data, 0, $uda);//$this->systemUser);
		$this->assertInstanceOf('IOResultInfo', $result);
		$user	=	$result->getObject();		
		$this->assertInstanceOf('UserModel', $user);
		
		return $user;
	}
	
	function AssertCreateSubscription(array $sub, array $limits, SubscriptionManager $subMgr)
	{
		$uda	=	new UserDataAccess();
		$uda->setActionUser(self::$systemAccount);
		
		$return	=	$subMgr->saveSubscription($sub, 0, $uda);
		$this->assertInstanceOf('IOResultInfo', $return);
		
		/* @var $obj SubscriptionModel */
		$obj	=	$return->getObject();
//		$obj->set('price', 50.00);
		
 		foreach($limits as $type => $limit)
		{
			$obj->setLimit($type, $limit);
		}
		$obj->Save();
		
		
		return $obj;
	}
	
	/**
	 * 
	 * @param UserModel $user
	 * @return ResellerModel
	 */
	function AssertCreateReseller(UserModel $user)
	{
		// create db reseller
		$data	=	array('admin_user' => $user->getID(), 'company' => 'Sofus Test');
        $da = new QubeDataAccess();
        $da->setActionUser($this->createSystemUserMock());
		$result	=	$this->getModelManager()->doSaveReseller($data, 0, $da);

		$reseller	=	$result->getObject();
		$this->assertInstanceOf('ResellerModel', $reseller);

		return $reseller;
	}
}
