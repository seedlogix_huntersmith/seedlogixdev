<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require TESTDIR . 'Qube_TestCase.php';
/**
 * Description of MUAutoResponderIntegrationTest
 *
 * @author amado
 */
class MUAutoResponderIntegrationTest extends Qube_TestCase {
	

    /** @var UserWithRolesModel  */
	protected static $resellerAccount	=	NULL;
	
	/**
	 *
	 * @var UserWithRolesModel
	 */
	protected $normalUser	=	NULL;
	
	
	/**
	 *
	 * @var LeadFormModel
	 */
	protected static $LeadForm	=	NULL;
	
	/**
	 *
	 * @var ResponderDataAccess
	 */
	protected $da	=	NULL;
	protected static $user_ID	=	NULL;
	
	protected static $reseller_ID	=	NULL;
	
	static function tearDownAfterClass() {
		static::truncateTables(array('auto_responders', 'users', 'resellers', 'lead_forms', '6q_roles', 'hub', 'blogs'));
	}
	
	protected function setUp()
	{
		$uda	=	new UserDataAccess(self::$qubeTEST);
		
		if(self::$user_ID)
		{
			$this->normalUser	=	$uda->getUserWithRoles(self::$user_ID);
			
			foreach(array('create_responder', 'edit_leadform') as $permission)
				$this->normalUser->setPermission($permission);
		}
//		$this->systemUser->expects($this->)
//		$this->getModelManager();
		
//		$this->da	=	new ResponderDataAccess(self::$qubeTEST);
//		$this->mm = new ModelManager($DC);
	}
	
	/**
	 * 
	 * @return ResellerModel
	 */
	function testCreateReseller()
	{
		$ResellerLogin	=	$this->AssertCreateUser(array('username' => 'reseller@test.com',
					'user_edit_id' => 0,
					'parent_user' => self::$resellerAccount), $this->createSystemUserMock());


		$subManager = new SubscriptionManager();
		$profesional_limits	=	array('TOUCHPOINTS' => 5,
			'KEYWORDS' => 100,
			'EMAILS'	=> 12000,
			'KEYWORD'	=> 100,
			'USERS'		=> 5	//SubscriptionModel::UNLIMITED
		);

		$sub	=	array('name' => 'My first subscription package', 'price' => 1.1);

		$obj	=	$this->AssertCreateSubscription($sub, $profesional_limits, $subManager);


		$uda	=	new UserDataAccess(self::$qubeTEST);
        $uda->setActionUser($this->createSystemUserMock());
		$resellerModel	=	$this->AssertCreateReseller($ResellerLogin);

		$result = $subManager->subscribeUser($obj->getID(), $resellerModel->admin_user, 0, $uda);

		self::$resellerAccount	=	$uda->getUserWithRoles($resellerModel->admin_user);
		return $resellerModel;
	}
	
	/**
	 * 
	 * @depends testCreateReseller
	 * @return UserModel
	 */
	function testCreateNormalUser(ResellerModel $RM)
	{
		$this->da->setActionUser(self::$resellerAccount);
		
		$user	=	$this->AssertCreateUser(array(
				'username' => 'user@test.com',
				'parent_id'	=> $RM->admin_user
				), self::$resellerAccount
			);

		self::$user_ID	=	$user->getID();
		self::$qubeTEST->setDashboardUserID(self::$user_ID);
		
		return $user;
	}
	
	/**
	 * t
	 * 
	 * @depends testCreateReseller
	 * @depends testCreateNormalUser
	 * @return LeadFormModel
	 */
	function testCreateMUForm(ResellerModel $R)
	{
        $reseller = clone self::$resellerAccount;
        $reseller->setPermissionsSet(new PermissionsSet($reseller));
        $reseller->setPermission('create_leadform');
		$this->da->setActionUser($reseller);

		$IO	=	$this->mm->doSaveLeadForm(array('name' => "my new leadform", 'user_id' => $R->admin_user, 'multi_user' => 1), 0, $this->da);
        $this->assertInstanceOf('IOResultInfo', $IO);
		$this->assertInstanceOf('LeadFormModel', $IO->getObject());
		self::$LeadForm	=	$IO->getObject();
		
		$forms	=	$this->da->getMasterLeadForms($R->admin_user);
		$this->assertEquals($forms[0]->getID(), $IO->getID());

		return $IO->getObject();
	}
	
	/**
	 * 
	 * @depends testCreateMUForm
	 * @return ResponderModel
	 */
	function testCreateMUResponder(LeadFormModel $LF)
	{
        $reseller = clone self::$resellerAccount;
        $reseller->setPermission('create_responder', true);
        $this->da->setActionUser($reseller);

        $this->AssertMasterResponders(0);
		$result	=	$this->mm->doSaveResponder(array('name' => 'MASTER responder name', 
					'multi_user' => 1,
					'active' => 1, 'sched' => 0,
					'user_id' => $LF->user_id, 'user_parent_ID' => $LF->user_id,
//					'table_name'	=> 'lead_forms',
//					'table_id' => $LF->getID()
				), 0, $this->da);

        $this->assertInstanceOf('IOResultInfo', $result);
		$master	=	$result->getObject();
		$this->assertInstanceOf('ResponderModel', $master);
		
		$this->mm->doSaveResponder(array('mu-form' => $LF->getID()), $result->getID(), $this->da);
		
		$updated_master	=	$master->Refresh('table_name, table_id', PDO::FETCH_ASSOC);
		
		$this->assertEquals($updated_master['table_id'], $LF->getID());
		
		
		$this->da->createMaster($master);
		$this->AssertMasterResponders(1);
		
		$responder	=	$this->da->getMasterResponder($master->getID());
		$this->assertInstanceOf('ResponderModel', $responder);
		return $master;
	}
	
	/**
	 * Tests that a Reseller's Master Responder is returned with the normal user's respodners
	 * 
	 * @depends testCreateMUForm
	 * @depends testCreateMUResponder
	 */
	function testCreateNormalResponder(LeadFormModel $MUForm)
	{
		$this->da->setActionUser($this->normalUser);
		$this->AssertResponderCount(1);
		
		// check that the user responder is created.
		$normal	=	$this->mm->doSaveResponder(array('name' => 'normal responder name', 
					'sched' => 0, 'active' => 1, 'table_name' => 'lead_forms', 'table_id' => $MUForm->getID(),
					'user_id' => $this->normalUser->getID(), 'user_parent_ID' => $MUForm->user_id), 0, $this->da);
		$this->assertInstanceOf('IOResultInfo', $normal);
		$this->AssertResponderCount(2);
	}
	
	/** 
	 * Assert that the responder count for a user is incremented after a reseller creates a master.
	 * 
	 * @depends testCreateMUResponder
	 * @depends testCreateNormalUser
	 * @return ResponderModel
	 */
	function testCreateSlave(ResponderModel $R, UserModel $UM)
	{
		$this->da->setActionUser(self::$resellerAccount);
		$slave	=	$this->da->getSlaveResponder($R->getID(), $UM->getID());
		$this->assertInstanceOf('ResponderModel', $slave);
		return $slave;
	}
	
	/**
	 * Check that a modification to a master is propagated to the slaves.
	 * 
	 * @depends testCreateMUResponder
	 * @depends testCreateSlave
	 */
	function testModifyMaster(ResponderModel $Master, ResponderModel $slave)
	{
		$this->da->setActionUser(self::$resellerAccount);
		$modified_mastername	=	'mODIFIED MASTER NAME';
//		$this->da	=	
		$normal	=	$this->mm->doSaveResponder(array('name' => $modified_mastername), $Master->getID(), $this->da);
		
		$slave_name	=	$slave->Refresh('name', PDO::FETCH_COLUMN);
		$this->assertEquals($slave_name, $modified_mastername);
		
		return $Master;
	}
	
	/**
	 * 
	 * @depends testModifyMaster
	 * @depends testCreateSlave
	 */
	function testModifySlave(ResponderModel $Master, ResponderModel $slave)
	{
		$this->da->setActionUser(self::$resellerAccount);
		$result	=	$this->mm->doSaveResponder(array('name' => 'Slave 1'), $slave->getID(), $this->da);
		$this->assertInstanceOf('IOResultInfo', $result);
		$this->assertInstanceOf('ResponderModel', $result->getObject());
		return $slave;
	}
	
	/**
	 * Check that a modification to a master does not ovewrite a custom value
	 * 
	 * @depends testCreateMUResponder
	 * @depends testModifySlave
	 */
	function testModifyMaster2(ResponderModel $Master, ResponderModel $slave)
	{
		$this->da->setActionUser(self::$resellerAccount);
		$modified_mastername	=	'testModifyMaster2';
		$normal	=	$this->mm->doSaveResponder(array('name' => $modified_mastername), $Master->getID(), $this->da);
		
		$slave_name	=	$slave->Refresh('name', PDO::FETCH_COLUMN);
		$this->assertNotEquals($slave_name, $modified_mastername);
		
		return $Master;
	}
	
	/**
	 * a - After creating a slave (customized responder), trash the Master and ensure the slave is unaffected
	 * b - Trash the slave, and test responder count (1 normal responder remains)
	 * 
	 * @depends testModifyMaster2
	 * @depends testCreateSlave
	 */
	function testTrash(ResponderModel $Master, ResponderModel $Slave)
	{
		$this->AssertResponderCount(2);
		$Master->Trash(self::$qubeTEST);	//$this->da, $this->mm);
		$this->AssertResponderCount(2);
		$Slave->Trash(self::$qubeTEST);
		$this->AssertResponderCount(1);
		$this->AssertMasterResponders(0);
	}

	function testCreateEmailer(){

    }




	/**
	 *
	 * @param int $expected
	 * @return ResponderModel[]
	 */
	function AssertResponderCount($expected)
	{
		$form_id	=	self::$LeadForm->getID();
		$responders	=	$this->da->getFormInstantResponders(
			array('table_id' => $form_id,
				'user_ID'	=> self::$user_ID,
				'table_name'	=> 'lead_forms')
		);
		
		$this->assertCount($expected, $responders);
		
		$this->AssertProcessForm(self::$LeadForm, $responders, $expected);
		return $responders;
	}
	
	function AssertProcessForm(LeadFormModel $LFM, $responders, $responder_count)
	{
		$logger	=	$this->getMock('ProcessLog');
		$logger->expects($this->exactly($responder_count))
				->method('writeLog')
				->with(
					$this->anything(), 
					$this->equalTo(ProcessLog::INFO)
				);
		
		$submitter	=	new SendTo("Amado Martinez", "martinez@amadomartinez.mx");
		
		$LFP	=	new LeadFormProcessor(self::$qubeTEST);
		$LFP->setLogger($logger);
		
		$webModel	=	$this->getMock('WebsiteModel');
		$Mailer	=	self::$qubeTEST->getMailer(TRUE);
		$leadvars		=	array(
				'lead_name'	=> 'Jack Mosley',
				'lead_email'	=>	'jack@mosley.mx',
				'lead_id'	=>	7);
		
		$LFP->SendInstantLeadResponderMails($responders, $webModel, $Mailer, 
						$leadvars, $submitter, $leadvars);
	}
	
	function AssertMasterResponders($expected)
	{
		$R	=	$this->da->getResellerMasterResponders(self::$resellerAccount->getID());
		$this->assertCount($expected, $R);
	}
}
