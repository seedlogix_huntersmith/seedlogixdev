<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/23/14
 * Time: 12:00 PM
 */


require TESTDIR . 'Qube_TestCaseIntegration.php';
class PermissionsTest extends Qube_TestCaseIntegration {

	/**
	 * @var UserDataAccess
	 */
	protected $uda;

	protected function setUp()
	{
		$this->uda	=	new UserDataAccess(self::$qubeTEST);
		$this->mm = new ModelManager(new DashboardBaseController());
	}

	static function tearDownAfterClass() {
		static::truncateTables(array('auto_responders', 'users', 'resellers', 'lead_forms', '6q_roles', 'hub', 'blogs'));
	}

	function testCreateReseller()
	{
		$ResellerLogin	=	$this->AssertCreateUser(array('username' => 'reseller@test.com',
			'user_edit_id' => 0), $this->createSystemUserMock());


		$subManager = new SubscriptionManager();
		$profesional_limits	=	array('TOUCHPOINTS' => 5,
			'KEYWORDS' => 100,
			'EMAILS'	=> 12000,
			'KEYWORD'	=> 100,
			'USERS'		=> 5	//SubscriptionModel::UNLIMITED
		);

		$sub	=	array('name' => 'My first subscription package', 'price' => 1.1);

		$obj	=	$this->AssertCreateSubscription($sub, $profesional_limits, $subManager);


		$uda	=	new UserDataAccess(self::$qubeTEST);
		$uda->setActionUser($this->createSystemUserMock());
		$resellerModel	=	$this->AssertCreateReseller($ResellerLogin);

		$result = $subManager->subscribeUser($obj->getID(), $resellerModel->admin_user, 0, $uda);
		return $resellerModel;
	}

	/**
	 *
	 * @depends testCreateReseller
	 * @return UserModel
	 */
	function testCreateNormalUser(ResellerModel $RM)
	{
		$user	=	$this->AssertCreateUser(array(
				'username' => 'user@test.com',
				'parent_id'	=> $RM->admin_user
			), $this->uda->getUserWithRoles($RM->admin_user)
		);

		return $user;
	}

	/**
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 *
	 * @return CampaignModel
	 */
	function testCreateCampaign(UserModel $user){
		$uda = new UserDataAccess();

		$usr = clone $uda->getUserWithRoles($user->getID());
		$usr->setPermissionsSet(new PermissionsSet($usr));

		$dataCampaign = array(
			'name' => 'Test campaign',
		);

		$da = new \Qube\Hybrid\DataAccess\CampaignDataAccess();
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveCampaign($dataCampaign, 0, $da);

		$this->assertInstanceOf("ValidationStack", $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_campaigns');
		$result = $this->getModelManager()->doSaveCampaign($dataCampaign, 0, $da);
		$this->assertInstanceOf("CampaignModel", $result['object']);

		/** @var CampaignModel $campaign */
		$campaign = $result['object'];

		$result = $this->getModelManager()->doSaveCampaign($dataCampaign, (int)$campaign->getID(), $da);
		$this->assertInstanceOf("ValidationStack", $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('edit_campaigns');
		$result = $this->getModelManager()->doSaveCampaign($dataCampaign, (int)$campaign->getID(), $da);
		$this->assertInstanceOf("CampaignModel", $result['object']);



		return $campaign;
	}

	/**
	 * @depends testCreateCampaign
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 * @param CampaignModel $campaign
	 * @return WebsiteModel
	 */
	function testCreateWebsite(CampaignModel $campaign, UserModel $user){
		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'WEBSITE',
			'blog_title' => 'test website',
			'cid' => $campaign->getID()
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$usr = $dau->getUserWithRoles($user->getID());
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_WEBSITE');
		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('HubModel', $result['object']);
			$blog = $result['object'];
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());


		$websiteData['httphostkey'] = 'blog.provensites.com';
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$sda = new SystemDataAccess();
		$sda->setActionUser($usr);
		$trash = new TrashManager($sda);

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertFalse($trashed);

		$usr->setPermission('edit_WEBSITE');
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('HubModel', $result['object']);
		$blog = $result['object'];

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertTrue($trashed);

		return $blog;
	}

	/**
	 * @depends testCreateCampaign
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 * @param CampaignModel $campaign
	 * @return WebsiteModel
	 */
	function testCreateLanding(CampaignModel $campaign, UserModel $user){
		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'LANDING',
			'blog_title' => 'test landing',
			'cid' => $campaign->getID()
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$usr = $dau->getUserWithRoles($user->getID());
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_LANDING');
		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('HubModel', $result['object']);
			$blog = $result['object'];
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());


		$websiteData['httphostkey'] = 'blog.provensites.com';
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$sda = new SystemDataAccess();
		$sda->setActionUser($usr);
		$trash = new TrashManager($sda);

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertFalse($trashed);

		$usr->setPermission('edit_LANDING');
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('HubModel', $result['object']);
		$blog = $result['object'];

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertTrue($trashed);

		return $blog;
	}

	/**
	 * @depends testCreateCampaign
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 * @param CampaignModel $campaign
	 * @return WebsiteModel
	 */
	function testCreateSocial(CampaignModel $campaign, UserModel $user){
		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'SOCIAL',
			'blog_title' => 'test social',
			'cid' => $campaign->getID()
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$usr = $dau->getUserWithRoles($user->getID());
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_SOCIAL');
		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('HubModel', $result['object']);
			$blog = $result['object'];
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());


		$websiteData['httphostkey'] = 'blog.provensites.com';
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$sda = new SystemDataAccess();
		$sda->setActionUser($usr);
		$trash = new TrashManager($sda);

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertFalse($trashed);

		$usr->setPermission('edit_SOCIAL');
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('HubModel', $result['object']);
		$blog = $result['object'];

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertTrue($trashed);

		return $blog;
	}

	/**
	 * @depends testCreateCampaign
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 * @param CampaignModel $campaign
	 * @return WebsiteModel
	 */
	function testCreateMobile(CampaignModel $campaign, UserModel $user){
		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'MOBILE',
			'blog_title' => 'test mobile',
			'cid' => $campaign->getID()
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$usr = $dau->getUserWithRoles($user->getID());
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_LANDING');
		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('HubModel', $result['object']);
			$blog = $result['object'];
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());


		$websiteData['httphostkey'] = 'blog.provensites.com';
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$sda = new SystemDataAccess();
		$sda->setActionUser($usr);
		$trash = new TrashManager($sda);

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertFalse($trashed);

		$usr->setPermission('edit_MOBILE');
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('HubModel', $result['object']);
		$blog = $result['object'];

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('site_ID' => $blog->getID())), $message, true);
		$this->assertTrue($trashed);

		return $blog;
	}

	/**
	 * @depends testCreateCampaign
	 * @depends testCreateNormalUser
	 * @param UserModel $user
	 * @param CampaignModel $campaign
	 * @return BlogModel
	 */
	function testCreateBlog(CampaignModel $campaign, UserModel $user){
		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'BLOG',
			'blog_title' => 'test blog'
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$usr = $dau->getUserWithRoles($user->getID());
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_BLOG');
		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('BlogModel', $result['object']);
			$blog = $result['object'];
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());


		$websiteData['httphostkey'] = 'blog.provensites.com';
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());
		$sda = new SystemDataAccess();
		$sda->setActionUser($usr);
		$trash = new TrashManager($sda);

		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('blog_ID' => $blog->getID())), $message, true);
		$this->assertFalse($trashed);

		$usr->setPermission('edit_BLOG');
		$result = $this->getModelManager()->doSaveWebsite($websiteData, $blog->getID(), $da);
		$this->assertInstanceOf('BlogModel', $result['object']);
		$blog = $result['object'];


		$trashed = $trash->TrashByParam(VarContainer::getContainer(array('blog_ID' => $blog->getID())), $message, true);
		$this->assertTrue($trashed);

		return $blog;

	}
	/**
	 * @param UserModel $user
	 * @param BlogModel $blog
	 * @depends testCreateNormalUser
	 * @depends testCreateBlog
	 */
	function testCreatePostBlog(UserModel $user, BlogModel $blog){
		$data = array(
			'name' => 'New post',
			'blog_id' => $blog->getID(),
			'user_id' => $user->getID()
		);

		$uda = new UserDataAccess();
		$da = new BlogDataAccess();
		$usr = $uda->getUserWithRoles($user->getID());
		$usr->setPermissionsSet(new PermissionsSet($usr));
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveBlogPost($data, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied(), 'mensaje derror');

		$usr->setPermission('create_blogposts');
		$result = $this->getModelManager()->doSaveBlogPost($data, 0, $da);
		$this->assertInstanceOf('BlogPostModel', $result['object']);

		$post = $result['object'];

		$result = $this->getModelManager()->doSaveBlogPost($data, $post->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('edit_blogpost');
		$result = $this->getModelManager()->doSaveBlogPost($data, 0, $da);
		$this->assertInstanceOf('BlogPostModel', $result['object']);

	}


	/**
	 * @param UserModel $user
	 * @depends testCreateNormalUser
	 */
	function testCreateBlogFeed(UserModel $user){
		$data = array(
			'name' => 'test_feed','assertInstanceOfassertInstanceOf',
			'user_id' => $user->getID()
		);
		$uda = new UserDataAccess();
		$da = new BlogDataAccess();

		$usr = $uda->getUserWithRoles($user->getID());
		$usr->setPermissionsSet(new PermissionsSet($usr));
		$da->setActionUser($usr);

		$result = $this->getModelManager()->doSaveBlogFeed($data, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('create_blogfeed');
		$result = $this->getModelManager()->doSaveBlogFeed($data, 0, $da);
		$this->assertInstanceOf('BlogFeedModel', $result['object']);

		$blogfeed = $result['object'];

		$result = $this->getModelManager()->doSaveBlogFeed($data, $blogfeed->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr->setPermission('edit_blogfeed');
		$result = $this->getModelManager()->doSaveBlogFeed($data, $blogfeed->getID(), $da);
		$this->assertInstanceOf('BlogFeedModel' ,$result['object']);
	}

	/**
	 * @param ResellerModel $reseller
	 * @depends testCreateReseller
	 * @depends testCreateNormalUser
	 */
	function testDoSaveRole(ResellerModel $reseller, UserModel $normalUser){
		$data = array(
			'role' => 'test role',
			'permissions' => array(
				'1' => 1,
				'2' => 1,
				'3' => 1,
				'4' => 1,
				'5' => 1,
				'6' => 1,
				'10' => 1,
			)
		);

		$uda = new UserDataAccess();

		$usr = $uda->getUserWithRoles($normalUser->getID());
		$uda->setActionUser($usr);
		$result = $this->getModelManager()->doSaveRole(VarContainer::getContainer($data), null, $uda);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$usr = $uda->getUserWithRoles($reseller->admin_user);
		$uda->setActionUser($usr);
		$result = $this->getModelManager()->doSaveRole(VarContainer::getContainer($data), null, $uda);

		$this->assertInstanceOf('IOResultInfo', $result);
	}

}
 