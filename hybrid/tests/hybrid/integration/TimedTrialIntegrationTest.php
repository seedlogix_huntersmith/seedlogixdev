<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 9/22/14
 * Time: 9:40 AM
 */
require TESTDIR . 'Qube_TestCaseIntegration.php';
class TimedTrialIntegrationTest extends Qube_TestCaseIntegration {

    /**
     * @var UserModel
     */
    protected $systemUser;

    /**
     * @var ResellerModel
     */
    protected $resellerAccount = null;

    /**
     * @var ModelManager
     */
    protected $modelManager;

    /**
     * @var UserDataAccess
     */
    protected $uda;

	/**
	 * @var CampaignModel
	 */
	protected static $campaign;

    public static function tearDownAfterClass()
    {
        static::truncateTables(array('auto_responders', 'users', 'resellers', 'lead_forms', '6q_roles', 'hub', 'blogs', 'account_limits', 'subscriptions', 'campaigns'));
    }


    public function setUp(){
        $this->resellerAccount = null;
        $this->uda = new UserDataAccess(self::$qubeTEST);
        $this->modelManager = new ModelManager();
        $this->systemUser = $this->createSystemUserMock();

    }

	protected function tearDown()
	{
		$this->uda->setActionUser($this->createSystemUserMock());
		$sub = new SubscriptionManager();
		$sub->removeAddon(1, 1, $this->uda);
	}

	/**
	 * Create a User and Subscribe Him 
	 * 
	 * @return ResellerModel
	 */
	public function testCreateUser(){
        $ResellerLogin	=	$this->AssertCreateUser(array('username' => 'reseller@test.com',
            'user_edit_id' => 0,
            'parent_user' => 0), self::$systemAccount);

        $subManager = new SubscriptionManager();
        $profesional_limits	=	array('TOUCHPOINTS' => 5,
            'KEYWORDS' => 100,
            'EMAILS'	=> 12000,
            'USERS'		=> 5	//SubscriptionModel::UNLIMITED
        );

        $sub	=	array('name' => 'My first subscription package', 'price' => 1.1);

        $obj	=	$this->AssertCreateSubscription($sub, $profesional_limits, $subManager);

        $this->uda->setActionUser($this->createSystemUserMock());
        $resellerModel	=	$this->AssertCreateReseller($ResellerLogin);

        $result = $subManager->subscribeUser($obj->getID(), $resellerModel->admin_user, 0, $this->uda);

		self::$campaign = new CampaignModel(self::$qubeTEST, array(
			'user_id' => $resellerModel->admin_user,
			'name' => 'Test campaign', 'ID' => 123
		));

//		self::$campaign = self::$campaign->Refresh('ID');
        return $resellerModel;
    }

    /**
     * @param ResellerModel $reseller
     * @depends testCreateUser
     */
    public function testSubscriptionExpired(ResellerModel $reseller){
        $this->uda->setActionUser($this->uda->getUserWithRoles($reseller->admin_user));
        $result = $this->uda->getAccountLimits($reseller->admin_user);

		$this->assertInstanceOf('stdClass', $result);

		$date = new DateTime();
		$result = $this->uda->getAccountLimits($reseller->admin_user, $date->add(new DateInterval('P12M')));
		$this->assertNull($result);
    }

    /**
	 * Add 5 TOUCHPOINTS to the Subscription (10 touchpoints)
	 * 
     * @param ResellerModel $reseller
     * @depends testCreateUser
     */
    public function testCreateAddOn($reseller)
	{
        $addon = (object)array(
            'TOUCHPOINTS' => 10,
            'KEYWORDS' => 10
        );

		$addon_ID	=	$this->AssertCreateAddon($reseller, $addon);
		
		$limits = $this->uda->getAccountLimits($reseller->admin_user);
		$this->assertInstanceOf('stdClass', $limits);
		
		$this->assertTrue($limits->TOUCHPOINTS == 15);
		
		return $addon_ID;
    }


	/**
	 * After testCreateAddon (TOUCHPOINTS = 15)
	 *	-	Attempt to remove the addon.
	 * 
	 * @param type $reseller
	 * @depends testCreateAddOn
	 * @depends testCreateUser
	 */
	public function testRemoveAddon($addon_ID, $resellerModel){
		
		$user = $this->uda->getUserWithRoles($resellerModel->admin_user);
		$user->setPermission('create_WEBSITE');
		
		$da = new QubeDataAccess();
		$da->setActionUser($user);
		
		$websites	=	array();
		// create 15 touchpoints
		for($i = 0; $i < 15; $i++)
		{
			$websiteData = array(
				'httphostkey' => 'asdas' . $i . '@sasd.com',
				'touchpoint_type' => 'WEBSITE',
				'blog_title' => 'test website' . $i,
				'cid' => self::$campaign->getID()
			);			
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('IOResultInfo', $result);
			$this->assertInstanceOf('WebsiteModel', $result->getObject());
			$websites[]		=	$result->getObject();
		}
		
		$result	=	$user->can('remove_addon', $addon_ID, new ModelManager());
		$this->assertFalse($result);
		
		for($i	=	0;	$i	<	10; $i++)
		{
			$websites[$i]->Trash(self::$qubeTEST);
		}
		
		$result	=	$user->can('remove_addon', $addon_ID, new ModelManager());
		$this->assertTrue($result);
	}

	/**
	 * @depends testCreateUser
	 * @param ResellerModel $resellerModel
	 */
	public function testLimitsBlog(ResellerModel $resellerModel){
		$user = $this->uda->getUserWithRoles($resellerModel->admin_user);


		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'BLOG',
			'blog_title' => 'test blog'
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$user->setPermission('create_BLOG');
		$da->setActionUser($user);


		for($i = 0; $i < 15; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('IOResultInfo', $result);
			$this->assertInstanceOf('BlogModel', $result->getObject());
		}
		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		$this->AssertCreateAddon($resellerModel, array('TOUCHPOINTS' =>  5));
		$da->setActionUser($dau->getUserWithRoles($resellerModel->admin_user));

		for($i = 0; $i < 5; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('IOResultInfo', $result);
			$this->assertInstanceOf('BlogModel', $result->getObject());
		}

		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

	}

	/**
	 * @depends testCreateUser
	 * @param ResellerModel $resellerModel
	 */
	public function testLimitsWebsite(ResellerModel $resellerModel){
		$user = $this->uda->getUserWithRoles($resellerModel->admin_user);



		$websiteData = array(
			'httphostkey' => 'asdas@sasd.com',
			'touchpoint_type' => 'WEBSITE',
			'blog_title' => 'test website',
			'cid' => self::$campaign->getID()
		);

		$da = new QubeDataAccess();
		$dau = new UserDataAccess();
		$user->setPermission('create_WEBSITE');
		$da->setActionUser($user);


		for($i = 0; $i < 15; $i++){
			$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
			$this->assertInstanceOf('IOResultInfo', $result);
			$this->assertInstanceOf('WebsiteModel', $result->getObject());
		}
		$result = $this->getModelManager()->doSaveWebsite($websiteData, 0, $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

	}
	/**
	 * @depends testCreateUser
	 * @depends testCreateAddOn
	 * @depends testLimitsBlog
	 * @param $user ResellerModel
	 */
	public function testRemoveSubscription(ResellerModel $user){
		$da = new UserDataAccess();
		$date = new DateTime();
		$actionUser = $da->getUserWithRoles($user->admin_user);
		$da->setActionUser($actionUser);
		$sm = new SubscriptionManager();

		//Subscription to downgrade
		$new_subscription = $this->AssertCreateSubscription(array(
			'name' => 'My first subscription package', 'price' => 1.1,
		), array(
			'TOUCHPOINTS' => 1,
			'KEYWORDS' => 2000
		), $sm);

		//Get actual subscription
		$subscription_id = $da->getSubscriptionsForTOTAL($user->admin_user, $date->format(PointsSetBuilder::MYSQLFORMAT));

		$result = $sm->changeSubscription($user->admin_user, $subscription_id[0]->getID(), $new_subscription->getID(), $da);
		$this->assertInstanceOf('ValidationStack', $result);
		$this->assertTrue($result->isDenied());

		/** @var WebsiteModel[] $websites */
		$websites = $da->getUser($user->admin_user)->queryObjects('WebsiteModel')->Exec()->fetchAll();

		/** @var BlogModel[] $blogs */
		$blogs = $da->getUser($user->admin_user)->queryObjects('BlogModel')->Exec()->fetchAll();

		foreach($blogs as $blog){
			$blog->Trash(self::$qubeTEST);
		}

		foreach($websites as $website){
			$website->Trash(self::$qubeTEST);
		}

		$result = $sm->changeSubscription($user->admin_user, $subscription_id[0]->getID(), $new_subscription->getID(), $da);
		$this->assertInstanceOf('IOResultInfo', $result);
	}



	/**
	 * @param $reseller
	 * @param $addon
	 * @return array
	 */
	protected function AssertCreateAddon($reseller, $addon)
	{
		$sm = new SubscriptionManager();
		$da = new UserDataAccess();
		$da->setActionUser($da->getUserWithRoles($reseller->admin_user));

		$result = $sm->subscribeAddOn($reseller->admin_user, 0, (object)$addon, $da);
		$this->assertNotInstanceOf('ValidationStack', $result);

		return (int)$result;
	}
}
 