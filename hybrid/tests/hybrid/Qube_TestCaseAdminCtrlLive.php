<?php

require TESTDIR . 'Qube_TestCaseLive.php';

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * 
 * Initializes Qube Class loader and other cool shit! (but not the db)
 * 
 */
abstract class Qube_TestCaseAdminCtrlLive extends Qube_TestCaseLive {
    
    /**
     *
     * @var Qube
     */
    protected static $qubeTEST =   NULL;
    
    protected function setUpController(DashboardBaseController $Ctrl, $user_ID){
        $Session    =   HybridControllerLauncher::getWebNamespace('UNIT_TEST');
        $Session->access_user_ID    =   $user_ID;
        $Ctrl->Initialize();
        $Ctrl->loadSession($Session);
        return $Ctrl;
    }
    
    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        
        Zend_Session::$_unitTestEnabled = true;
        Zend_Session::setSaveHandler(new DummySessionSaver());  // dont save shit
        
        $_SERVER['QUBECONFIG']  =   'gravedigger';

        self::$qubeTEST =   Qube::Start();
    }
    
    public static function getTestDataAsArgsArray($filename, $varname =   'data'){
        require TESTDATAFILES . $filename;
        
        $args   =   array();
        foreach($$varname as $row){
            $args[] =   array($row);
        }
        
        return $args;
        
        $xa  =     array_map('func_get_args', $map);
        $xb  =   array_map('array_pop', $xa);
        
//        var_dump($xb);exit;
//        echo 'test';
        return $xb;
    }
    
}
