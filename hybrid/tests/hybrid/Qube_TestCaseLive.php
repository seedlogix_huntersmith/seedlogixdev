<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * 
 * Initializes Qube Class loader and other cool shit! (but not the db)
 * 
 */
abstract class Qube_TestCaseLive extends PHPUnit_Framework_TestCase {
    
    /**
     *
     * @var Qube
     */
    protected static $qubeTEST =   NULL;
    
    public static function setUpBeforeClass() {
        self::$qubeTEST =   Qube::Start();
    }
    
    public static function getTestDataAsArgsArray($filename, $varname =   'data'){
        require TESTDATAFILES . $filename;
        
        $args   =   array();
        foreach($$varname as $row){
            $args[] =   array($row);
        }
        
        return $args;
        
        $xa  =     array_map('func_get_args', $map);
        $xb  =   array_map('array_pop', $xa);
        
//        var_dump($xb);exit;
//        echo 'test';
        return $xb;
    }
    
}
