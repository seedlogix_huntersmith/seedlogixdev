<?php

require_once TESTDIR . 'Qube_TestCaseLive.php';

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2013-12-25 at 03:27:07.
 */
class WebsiteModelTest extends Qube_TestCaseLive {

    /**
     * @var WebsiteModel
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new WebsiteModel(Qube_TestCaseLive::$qubeTEST, array(
			'touchpoint_type' => 'WEBSITE',
			'cid' => '7221',
			'type' => '2',
			'user_id' => '6610',
			'user_parent_id' => '6610',
			'name' => '(my test website)',
			'multi_user' => '1',
			'page_version' => '1'
		));
    }
    
    public function testSave() {
		$r = $this->object->Save();
		$this->assertInstanceOf('WebsiteModel', $r);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers WebsiteModel::Validate
     * @todo   Implement testValidate().
     */
    public function testValidate() {
        $W      =   $this->object;
		$VS = new ValidationStack();
		$var = array(
			'name' => 'website',
			'cid' => '6610'
		);
        $W->Validate($var, $VS);
		$this->assertInstanceOf('WebsiteModel', $W);
    }

    /**
     * @covers WebsiteModel::isRequired
     * @todo   Implement testIsRequired().
     */
    public function testIsRequired() {
        $website = $this->object;
		$r = $website->isRequired('name');
		$this->assertTrue($r);
    }

    /**
     * @covers WebsiteModel::getBindings
     * @todo   Implement testGetBindings().
     */
    public function testGetBindings() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete('This test has not been implemented yet');

    }

    /**
     * @covers WebsiteModel::getRootNavs
     * @todo   Implement testGetRootNavs().
     */
    public function testGetRootNavs() {
        $website = $this->object;
		var_dump($website->getRootNavs());
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers WebsiteModel::getNodes
     * @todo   Implement testGetNodes().
     */
    public function testGetNodes() {
        $website = $this->object;
		var_dump($website->getNodes());
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers WebsiteModel::newPage
     * @todo   Implement testNewPage().
     */
    public function testNewPage() {
		$website = $this->object;
		$r = $website->newPage();

		$this->assertInstanceOf('PageModel', $r);
    }

    /**
     * @covers WebsiteModel::newNav
     * @todo   Implement testNewNav().
     */
    public function testNewNav() {
		$result = $this->object->newNav();

		$this->assertInstanceOf('NavModel', $result);
    }

    /**
     * @covers WebsiteModel::_getDeleteClause
     * @todo   Implement test_getDeleteClause().
     */
    public function test_getDeleteClause() {
        // Remove the following lines when you implement this test.

		$this->markTestIncomplete('_getDeleteClause not implemented yet');
    }
    
    public function test_getStatus() {
        $this->markTestIncomplete(
			'This test has not been implemented yet.'
		);
    }

}
