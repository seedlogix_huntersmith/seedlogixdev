<?php

require_once TESTDIR . 'Qube_TestCaseLive.php';
/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-04-23 at 12:50:33.
 */
class HubRowModelTest extends Qube_TestCaseLive {

	/**
	 * need to do an integration test before attempting to do this test.
	 * 
	 * @covers HubRowModel::ParseHubRow
	 * @todo   Implement testParseHubRow().
	 */
	public function fix_testParseHubRow() {
		$HRM	=	new HubRowModel(array('city' => 'austin', 'state' => 'TX', 'slogan' => 'Hello from ((city)) ((state))!', 'site_title' => 'The Slogan Is ((slogan))',
							'meta_description' => '((incitycommastate))', 'edit_region_9' => '((my-first-navigation-children))', 'id' => 10997));
		HubRowModel::ParseHubRow($HRM);
		$this->assertEquals('Hello from austin TX!', $HRM['slogan']);
		$this->assertEquals('The Slogan Is Hello from austin TX!', $HRM['site_title']);
		$this->assertStringMatchesFormat('%S<ul><li>%a', $HRM['edit_region_9']);	// check nav creation
	}

}
