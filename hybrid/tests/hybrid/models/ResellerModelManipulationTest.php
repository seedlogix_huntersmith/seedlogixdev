<?php

require TESTDIR . 'Qube_TestCaseLive.php';


/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class ResellerModelManipulationTest extends Qube_TestCaseLive
{
    protected $qubeMOCK =   NULL;
    
    protected function setUp(){
//        $this->qubeMOCK
    }
    /**
     * 
     * @dataProvider ProvideValidData
     */
    function testSaveDataValid($data){
        $Reseller  =   new ResellerModel(self::$qubeTEST);
        $misc   =   array('reseller' => $data);
        $result =   array();
        
        $this->assertTrue(FALSE !== $Reseller->SaveData($misc, 'reseller', new ValidationStack(), NULL, FALSE, $result));
    }
    
    /**
     * 
     * @dataProvider ProvideValidData
     */
    function testSaveDataInValid($data){
        $Reseller  =   new ResellerModel(self::$qubeTEST);
        $misc   =   array('reseller' => $data);
        $result =   array();
        
        $this->assertTrue(FALSE === $Reseller->SaveData($misc, 'reseller', new ValidationStack(), NULL, FALSE, $result));
    }
    
    
    function ProvideValidData(){
        return self::getTestDataAsArgsArray('resellers_valid.php', 'resellers');
    }
    
}

