<?php

require_once TESTDIR . 'Qube_TestCase.php';

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:
 */


class ResellerModelTest extends Qube_TestCase
{

    /**
     * @dataProvider Provider
     */
	public function testHasFacebookApp(ResellerModel $pastePHP){
		$Reseller   =   new ResellerModel;
		$this->assertFalse($Reseller->hasFacebookApp());

		$this->assertTrue($pastePHP->hasFacebookApp());
	}


    /**
     * @dataProvider Provider
     */
	public function testHasTwitterApp(ResellerModel $pastePHP){
		$Reseller   =   new ResellerModel();
		$this->assertFalse($Reseller->hasTwitterApp());

		$this->assertTrue($pastePHP->hasTwitterApp());
	}

    /**
     * @dataProvider Provider
     */
    public function notestGetTwitterApp(ResellerModel $pastePHP){
        $Twitter    =   $pastePHP->getTwitter("100720299-h7oyiE4chH1NZlJmVriH1Bd3MiI5bVX4yebSKfxh\n0HzzXOAlGKpG0gPoyhInaeNUQs727ct5eS3olBOtQY\namadocodes"); //getTwitter not working;
        $Response   =   $Twitter->accountVerifyCredentials();
        $this->assertTrue($Response->isSuccess());
        $User   =   $Twitter->usersShow($Twitter->getUsername());
        $this->assertTrue($User->isSuccess());
        $Response   =   $Twitter->statusesUpdate("API TEST FROM GRAVEDIGGER! " . rand());
        $this->assertTrue($Response->isSuccess());
    }

    /**
     * @dataProvider Provider
     */
    public function testGetAdmin(ResellerModel $pastePHP){
			$DA	=	$this->getMock('UserDataAccess');
			$DA->expects($this->any())->method('getUser')->will($this->returnValue(new UserModel));
			
        $admin = $pastePHP->getAdmin($DA);
        $this->assertInstanceOf('UserModel', $admin);
    }

    /**
     * @dataProvider Provider
     */
    public function testGetMainStorage(ResellerModel $resller){
        $storage = $resller->getMainStorage();
        $this->assertEquals(realpath(QUBEROOT) . '/users/u2/2996/brand', $storage);

    }

    /**
     * @dataProvider Provider
     */
    public function testGetMainStorageURL(ResellerModel $reseller){
        $storage = $reseller->getMainStorageURL();
        $this->assertEquals($storage, 'users/u2/2996/brand/');
    }

    /**
     * @dataProvider Provider
     */
    public function testGetFacebook(ResellerModel $reseller){
        require_once HYBRID_LIB . 'facebook-php-sdk/src/facebook.php';
				session_id(1);	// disable facebook's ssession_start() call
        $facebook = $reseller->getFacebook();
        $this->assertInstanceOf('Facebook', $facebook);
    }

    public function Provider(){
			Qube::AutoLoader();
			return array(array(new ResellerModel(self::$qubeTEST, array('name' => '', 'twitter' => '', 'admin_user' => 2996,
					'fbapp_id' => 'fjkjk', 'fbapp_secret' => 'fjkljk', 'twapp_secret' => 'jkjk', 'twapp_id' => 91918
						))));
    }
}


