<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class NavModelTest extends PHPUnit_Framework_TestCase
{
    
    /**
     * @dataProvider NavModelProvider
     */
    function testGetDefaultSite(NavModel $Nav){
        // just make sure the functions dont trigger any errors..
         $Nav->getSite();
         $Nav->getNodes();
         $Nav->getNavs();
         
//        $this->assertTrue($Nav->getSite() !== FALSE);
    }
    
    function NavModelProvider(){
        $set    =   array();
        $howmany    =   10;
        $NavModelRandom =   Qube::Start()->queryObjects('NavModel', 'N', 'trashed = 0')->orderBy('rand()')->Limit($howmany)->Exec();
        
//        var_dump($NavModelRandom);
        
        while($NavModel = $NavModelRandom->fetch())
                $set[]  =   array($NavModel);
        
        return $set;
    }
}

