<?php
require TESTDIR . 'Qube_TestCaseLive.php';
/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-03-11 at 04:27:25.
 */
class BlogPostModelTest extends Qube_TestCaseLive {

    /**
     * @var BlogPostModel
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new BlogPostModel;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers BlogPostModel::__getColumns
     * @todo   Implement test__getColumns().
     */
    public function test__getColumns() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers BlogPostModel::_getDeleteClause
     * @todo   Implement test_getDeleteClause().
     */
    public function test_getDeleteClause() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers BlogPostModel::getPostURL
     * @todo   Implement testGetPostURL().
     */
    public function testGetPostURL() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}
