<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once TESTDIR . 'Qube_TestCase.php';

/**
 * Description of SubscriptionModelTest
 *
 * @author amado
 */
class SubscriptionModelTest extends Qube_TestCase {

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * 
	 * @return SubscriptionModel
	 */
	public function testConstruct()
	{
		$Sub	=	new SubscriptionModel();
		return $Sub;
	}
	
	/**
	 * 
	 * @depends testConstruct
	 */
	public function testSetLimits(SubscriptionModel $SM)
	{
		$SM->setLimit('TOUCHPOINTS', 5);
		$this->assertEquals($SM->limits->TOUCHPOINTS, 5);
		
		$SM->set('limit_TOUCHPOINTS', 15);
		$this->assertEquals($SM->limits->TOUCHPOINTS, 15);
		
		$limitsarray	=	array('TOUCHPOINTS'=> 15);
		
		$this->assertEquals($SM->get('limits_json'), json_encode($limitsarray));
	}
	
	
}
