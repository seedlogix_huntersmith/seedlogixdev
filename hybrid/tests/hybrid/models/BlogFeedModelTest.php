<?php

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-07-14 at 08:19:59.
 */
class BlogFeedModelTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var BlogFeedModel
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new BlogFeedModel;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers BlogFeedModel::__getColumns
	 * @todo   Implement test__getColumns().
	 */
	public function test__getColumns() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers BlogFeedModel::_getColumns
	 * @todo   Implement test_getColumns().
	 */
	public function test_getColumns() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

}
