<?php

require TESTDIR . 'Qube_TestCaseLive.php';

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class SocialMonitorTest extends Qube_TestCaseLive
{
    /**
     * @dataProvider MonitorProvider
     */
    public function testTwitterQuery(SocialMonitor $Monitor){
        $Q  =   $Monitor->queryTwitterBlogPosts(self::$qubeTEST);
        
        $this->assertInstanceOf('PDOStatement', $Q->Exec());
    }   
    
    /**
     * @dataProvider MonitorProvider
     */
    public function testFacebookQuery(SocialMonitor $Monitor){
        $Q  =   $Monitor->queryFacebookBlogPosts(self::$qubeTEST);
        
        $this->assertInstanceOf('PDOStatement', $Q->Exec());
    }
    
    function MonitorProvider(){
        Qube::Start();  // init class loader and other cool shit
        return array(array(new SocialMonitor()));
    }
}


