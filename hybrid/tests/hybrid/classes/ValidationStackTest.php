<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class ValidationStackTest extends PHPUnit_Framework_TestCase
{
    function testEmailValidate(){
     $invalidEmails	=	array('invalid', 'invalid@', 'awesome.com', '#email', '');
	 
	 $validEmails	=	array('$invalid?@my.com', 'valid@awesome.com', 'boss@sofus.mx');
	 
	 foreach($invalidEmails as $emailstr)
	 {
		$validator	=	new ValidationStack();
		$this->assertFalse($validator->Validate('Email', $emailstr, new \Zend\Validator\EmailAddress), $emailstr);
	 }
	 
	 foreach($validEmails as $emailstr)
	 {
		 $this->assertTrue($validator->Validate('Email', $emailstr, new \Zend\Validator\EmailAddress), $emailstr);
	 }
	 
    }
		
    protected function setUp() {
    		Qube::Start();
    }
    
    
}
