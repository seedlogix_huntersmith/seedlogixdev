<?php

require TESTDIR . 'Qube_TestCaseLive.php';

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-04-30 at 11:39:18.
 */
class FeedDataAccessTest extends Qube_TestCaseLive {

	/**
	 * @var FeedDataAccess
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new FeedDataAccess(self::$qubeTEST);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers FeedDataAccess::createFeedNavMenu
	 * @todo   Implement testCreateFeedNavMenu().
	 * @dataProvider FeedTestDP()
	 */
	public function testCreateFeedNavMenu($classname, $feed_id) {
		require_once HYBRID_PATH . 'classes/FeedObject.php';
		require_once HYBRID_PATH . 'classes/custom/' . $classname . '.php';

		$feedobj	=	new $classname;
		$feedobj->FEEDID	=	$feed_id;
		
		$menu	=	array();
		$this->object->createFeedNavMenu($menu, $feedobj);
		$this->assertNotEmpty($menu);
	}
	
	function FeedTestDP()
	{
		return array(array('SCLeadsFeedObject', 1));
	}
}
