<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class SocialStatsChartDataTest extends PHPUnit_Framework_TestCase
{
    public function testFetchDataSetLikes(){
        $config =   array('interval' => 0, 'period' => 'ALL',
                'controller'    => new PiwikAnalyticsController());
        $SocialChart   =   new SocialStatsChartData('chartconfig', NULL, false, $config);
        $stats  =   $SocialChart->fetchDataSetLikes();
//        var_dump($stats);
        $this->assertNotEmpty($stats);
    }   
}


