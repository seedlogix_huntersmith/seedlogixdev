<?php

require_once TESTDIR . 'Qube_TestCase.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactFormProcessorTest
 *
 * @author sofus
 */
class ContactFormProcessorTest extends Qube_TestCase {
	
	function getFormData(array $arr)
	{
		$CF	=	new ContactDataModel();
		$CF->loadArray($arr);
		return $CF;
	}
	
	function getUser(array $data)
	{
		$arra	=	array('main_site' => 'phpunit-test.com', 'username' => 'user@phpunit-test.com') + $data;
		
		$user	=	new UserWithRolesModel();
		$user->loadArray($arra);
		return $user;
	}
	
	function getProcessor()
	{
		return new ContactFormProcessor();
	}
	
	function testProcessContactFormNOTSPAM()
	{
		$p = $this->getProcessor();
		
		$form	=	$this->getFormData(array('type_id' => 199, 'cid' => 700));
		$form->spamstatus = 0;

		Qube::ClassLoader('ProspectDataAccess');
		$mockDA	=	$this->getMock('ProspectDataAccess');
		$mockDA->expects($this->once())->method('getCampaignEmailRecipients')
							->with(199, 700)
						->will($this->returnValue(array()));
		$p->setProspectDA($mockDA);
		
//		$logger	=	$this->getMock('Logger');
//		$logger->expects($this->once())
//				->method('logDEBUG')
//			->with(
////				$this->anything(),
//				$this->equalTo(Zend\Log\Logger::DEBUG)
//			);

		$Mailer	=	$this->getMockBuilder('NotificationMailer')->disableOriginalConstructor()->getMock();
		$Mailer->expects($this->once())
			->method('Send');
		$p->setMailer($Mailer);

		$uda	=	$this->getMock('UserDataAccess');
		$uda->expects($this->once())
			->method('getUserWithRoles')
			->will($this->returnValue($this->getUser(
				array('reseller_company' => 'phpunit-company')))
			);

		$rda	=	$this->getMock('ResponderDataAccess');
		$rda->expects($this->once())
			->method('getFormInstantResponders')
			->will($this->returnValue(array()));


		$p->ProcessContactForm($form, $uda, $rda);
	}
}
