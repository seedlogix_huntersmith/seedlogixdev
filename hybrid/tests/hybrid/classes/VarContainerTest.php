<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class VarContainerTest extends PHPUnit_Framework_TestCase
{
    
    function testCheck()
    {
    	$data	=	array('myvar' => "9999");
        $V	=	new VarContainer($data);
        	$this->assertTrue($V->check('is_numeric', 'myvar'));
    }

    function testEmpty()
    {
    	$data	=	array('myvar' => "9999");
        $V	=	new VarContainer($data);
        	$this->assertFalse($V->isEmpty('myvar'));
    }


    function testCheckValue(){
     
    	$data	=	array('myvar' => "9999");
        $V	=	new VarContainer($data);
        $this->assertFalse($V->checkValue('notset', $set));
        $this->assertTrue($V->checkValue('myvar', $myvar));
        $this->assertEquals($myvar, "9999");
    }
		
    function testChecksFail(){
    
			// test string val
    	$data	=	array('myvar' => "9999", 'notint' => 'test');
        $V	=	new VarContainer($data);
				$this->assertFalse($V->check('intval', 'notint', 'myvvar'));
				
				// test zero val
    	$data	=	array('myvar' => "9999", 'emptyint' => 0);
        $V2	=	new VarContainer($data);
				$this->assertFalse($V2->check('intval', 'myvvar', 'emptyint'));
				
				// test empty val
    	$data	=	array('myvar' => "9999", 'emptyint' => '');
        $V3	=	new VarContainer($data);
				$this->assertFalse($V3->check('intval', 'myvvar', 'emptyint'));
				
				// test unset val
    	$data	=	array('myvar' => "9999");
        $V3	=	new VarContainer($data);
				$this->assertFalse($V3->check('intval', 'myvar', 'notset'));
				
				
				// test quotes
    	$data	=	array('myvar' => "'9999'", 'yesint' => 7777);
        $V4	=	new VarContainer($data);
				$this->assertFalse($V4->check('intval', 'yesint', 'myvar'));
    }
    
    function testChecksPass(){
     
    	$data	=	array('myvar' => "9999", 'yesint' => 7777);
        $V	=	new VarContainer($data);
				$this->assertTrue($V->check('intval', 'yesint', 'myvar'));
				
    }
		
    protected function setUp() {
    		Qube::Start();
    }
    
    
}
