<?php
/**
 * Created by PhpStorm.
 * User: alan
 * Date: 10/21/14
 * Time: 9:47 AM
 */

namespace hybrid\classes\DataAccess;

require_once TESTDIR . 'Qube_TestCaseIntegration.php';

class RankingDataAccessTest extends \Qube_TestCaseIntegration {
	
	/**
	 *
	 * @var RankingDataAccess
	 */
	protected $dataX	=	NULL;

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	static function tearDownAfterClass() {
		self::truncateTables(array('ranking_sites', 'ranking_keywords'));
	}
	
	static function setUpBeforeClass() {
		parent::setUpBeforeClass();
		self::runSQLFile('rankings_insertonly.sql');
	}
	
	protected function setUp()
	{
		$this->dataX	=	new \RankingDataAccess(self::$qubeTEST);
	}
	
	function testGetKeywords()
	{
		
		
	}
	
    public function testRankingDataCount(){

        $countresult = $this->dataX->getAllRankingCount(array('user_id'=>'6610','campaign_id'=>'7221'),10,0,null,"id","asc",array('user_id'=>'6610','campaign_id'=>'7221'));

        $this->assertEquals(3,$countresult);

    }

    public function testRankingDataFetch(){


        $datax = new \RankingDataAccess();

        $countresult = $datax->getAllRankingData(array('user_id'=>'6610','campaign_id'=>'7221'),10,0,null,"id","asc");


        $this->assertCount(3, $countresult);


    }

    public function testDoSaveKeywordCampaign(){
        $MM = new \ModelManager();
        $dateTime = new \DateTime();
        $user = $this->createSystemUserMock();
//        $rda = new \RankingDataAccess();
        $this->dataX->setActionUser($user);
        $data = array(
            'user_ID' => 6610,
            'ts' => $dateTime->format(\PointsSetBuilder::MYSQLFORMAT),
            'created' => $dateTime->format(\PointsSetBuilder::MYSQLFORMAT),
            'campaign_id'  => 7221,
            'hostname' => 'test'
        ));
        $result = $MM->doSaveRankingSite($C, $this->dataX);
        $this->assertInstanceOf('IOResultInfo', $result);
        $this->assertInstanceOf('RankingSiteModel', $result->getObject());
        $this->assertTrue($result->getObject()->getID() != 0);
    }

    public function testDoSaveKeywords()
    {
        $datax = new \ModelManager();
        $countresult = new \IOResultInfo();
        $uda	=	new \UserDataAccess(self::$qubeTEST);
        $uda->setActionUser(\Qube_TestCaseIntegration::CreateSystemUserMock());

        $countresult = $datax->doSaveKeywords(array(
            'campaign_ID'=>'6225',
            'site_ID'=>'7241',
            'keywords'=>'key1\nkey2\nkey3\nlastoda'
        ),0,$uda);

        $this->assertEquals('Keywords Saved',$countresult['message']);
    }

    public function testDoSaveKeyword()
    {
        $datax = new \ModelManager();
        $countresult = new \IOResultInfo();
        $uda	=	new \UserDataAccess(self::$qubeTEST);
        $uda->setActionUser(\Qube_TestCaseIntegration::CreateSystemUserMock());

        $countresult = $datax->doSaveKeywords(array(
            'campaign_ID'=>'6225',
            'site_ID'=>'7241',
            'keywords'=>'thiscanel',
            'ID' => '3',
        ),3,$uda);

        $this->assertEquals('Keywords Saved',$countresult['message']);
    }


}
