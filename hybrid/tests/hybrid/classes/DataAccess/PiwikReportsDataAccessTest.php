<?php //

require TESTDIR . 'Qube_TestCaseLive.php';

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-06-09 at 11:10:05.
 */
class PiwikReportsDataAccessTest extends Qube_TestCaseLive {

	/**
	 * @var PiwikReportsDataAccess
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new PiwikReportsDataAccess;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers PiwikReportsDataAccess::getSaveStatStatement
	 * @todo   Implement testGetSaveStatStatement().
	 */
	public function testGetSaveStatStatement() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers PiwikReportsDataAccess::SaveStat
	 * @todo   Implement testSaveStat().
	 */
	public function testSaveStat() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers PiwikReportsDataAccess::_dailyStatsQuery
	 * @todo   Implement test_dailyStatsQuery().
	 */
	public function test_dailyStatsQuery() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers PiwikReportsDataAccess::_getDailyCampaignStats
	 * @todo   Implement test_getDailyCampaignStats().
	 */
	public function test_getDailyCampaignStats() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers PiwikReportsDataAccess::getUserGlobalStats
	 * @todo   Implement testGetCampaignsStats().
	 */
	public function testGetUserGlobalStats() {
		
		$VC	= VarContainer::getContainer(array('interval' => 7, 'period' => 'MONTH'));
		
		$stats	=	$this->object->getUserGlobalStats(new PointsSetBuilder($VC), 11691);
		
		$this->assertTrue(is_array($stats));
		
		$VCD	= VarContainer::getContainer(array('interval' => 7, 'period' => 'DAY'));
		
		$stats2	=	$this->object->getUserGlobalStats(new PointsSetBuilder($VCD), 11691);
		
		$this->assertTrue(is_array($stats2));
	}

	
	/**
	 * @covers PiwikReportsDataAccess::getCampaignsStats
	 * @todo   Implement testGetCampaignsStats().
	 */
	public function testGetCampaignsStatsDAY() {
		
		$VC	= VarContainer::getContainer(array('interval' => 7, 'period' => 'DAY'));
		
		$stats	=	$this->object->getCampaignStats(new PointsSetBuilder($VC), 11691);
		
		$this->assertTrue(is_array($stats));
	}
	
	/**
	 * @covers PiwikReportsDataAccess::getVisitedHubs
	 * @todo   Implement testGetHubsVisitedLast24hr().
	 */
	public function testGetHubsVisitedLast24hr() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
						'This test has not been implemented yet.'
		);
	}

	public function testGetTrackingID(){
		$piwik = new PiwikReportsDataAccess();
		$hub = new HubModel(self::$qubeTEST, array(
			'name' => 'Test site',
			'httphostkey' => 'testsite.com'
		));
		$result = $piwik->addSite($hub);
	}

}
