<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/2/14
 * Time: 8:31 AM
 */

require TESTDIR . 'Qube_TestCaseIntegration.php';

class ReportDataAccessTest extends Qube_TestCaseIntegration {
	
    /** @var  PiwikReportsDataAccess */
    private $prda = NULL;

    /** @var  PiwikReportsDataAccess */
    private $ps = NULL;

    public function setUp(){
        $this->prda = new PiwikReportsDataAccess();
		$this->ps	=	new PointsSetBuilder(VarContainer::getContainer(array(
			'interval' => 7,
			'period' => 'DAY'
		)));
    }

	function testGetSearchEngineVisits()
	{

		$stuff	=	$this->prda->getSearchEngineVisits(
				$this->ps->getStartDate(),
				$this->ps->getEndDate(), 'cid', 271);

		$this->assertNotNull($stuff);

		return $stuff;
	}

	/**
	 *
	 * @depends testGetSearchEngineVisits
	 * @param type $searchengines
	 */
	function testGetSearchEngineLeads($searchengines)
	{
		$leadtrackinngids	=	$this->prda->getLeadTrackingIds(
				$this->ps->getStartDate(),
				$this->ps->getEndDate(), 'cid', 7221);

		$this->assertNotNull($leadtrackinngids);
	}

    public function gtestGetAllStatsDaily(){
        $start = new DateTime('2012-01-01');
        $end = new DateTime('2000-02-01');


        $result = $this->rda->getAllStatsDaily($start ,$end);

    }

	public function testSaveReport(){


		$datax = new \ModelManager();


		$uda	=	new \UserDataAccess(self::$qubeTEST);
		$uda->setActionUser(\Qube_TestCaseIntegration::CreateSystemUserMock());

		$countresult = $datax->DoSaveReport(array('name'=>'reportname',
			'user_ID'=>'6610','campaign_ID'=>'7221','type'=>'DAYLY','touchpoint_type'=>'WEBSITE'
		),0,$uda);

		$this->assertInstanceof('IOResultInfo',$countresult);
		$this->assertInstanceOf('ReportModel', $countresult->getObject());
		$this->assertTrue($countresult->getObject()->getID()!=0);

	}

	public function testReportCount(){


		$datax = new \ReportDataAccess();

		$countresult = $datax->getAllReportCount(array('user_id'=>'6610','campaign_id'=>'7221'),10,0,null,"id","asc");

		$this->assertGreaterThan(0,$countresult);


	}

	public function testReportDataFetch(){


		$datax = new \ReportDataAccess();

		$countresult = $datax->getAllReportData(array('user_id'=>'6610','campaign_id'=>'7221'),10,0,null,"id","asc");


		$this->assertNotEmpty($countresult);


	}

	public function testGetSearchVisits(){
		$rda = new ReportDataAccess();
		$report = new ReportModel(self::$qubeTEST, array(
			'ID' => 1,
			'type' => 'WEEKLY',
			'campaign_ID' => 271,
			'user_ID' => 235
		));
		$results = $rda->getSearchVisits($report, '');
	}

	function testGetReferrals(){
		$rda = new ReportDataAccess(self::$qubeTEST);
		$report = new ReportModel(self::$qubeTEST ,array(
			'ID' => 102,
			'type' => 'DAILY',
			'campaign_ID' => 39629,
			'user_ID' => 6610,
			'touchpoint_type' => 'WEBSITE'
		));
		$results = $rda->getReferralSearch($report, '', false, '', array(
			'orderBy' => 'visits',
			'sortDir' => 'desc',
			'limit' => '10',
			'offset' => '0'
		));
	}

}
 