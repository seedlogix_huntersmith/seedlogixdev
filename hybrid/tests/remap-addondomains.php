<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';
require_once HYBRID_PATH . 'classes/DomainConfig.php';

$argc < 5 && die('Arguments are serverconfig path httphost lincoln|kennedy');

$router_dir	=	'production-code/dev-trunk/hybrid';

if($argv[4] == 'lincoln'){
	$xmlapi =   new LincolnXmlApi();	
	$rootdomain	=	'6qube.mobi';
	$matchhubs	=	'#^public_html/hubs/domains/[0-9]+/([0-9]+)$#';
}elseif($argv[4]	==	'kennedy'){
	$xmlapi	=	new KennedyXmlApi();
	$rootdomain	=	'bluejagweb.com';
	$matchhubs	=	'#^public_html/6qube\.com/hubs/domains/[0-9]+/([0-9]+)$#';
}else{
	die('invalid server ');
}

	$result  =   $xmlapi->api2_query(null, "SubDomain", "listsubdomains",
				array('regex' => $rootdomain));

	$backup_subd_fname	=	'addons-' . get_class($xmlapi) . '.xml';
	if(!file_exists($backup_subd_fname)){
		$result->asXML($backup_subd_fname);
		var_dump($result);
		exit;
	}

	$pdo	=	Qube::GetPDO();

	foreach($result->data as $addon):
		echo "Processing $addon->domain ($addon->basedir , $addon->domainkey, $addon->subdomain)";

		$basedir	=	(string)$addon->basedir;
		$addonkey	=	(string)$addon->domainkey;
		if(preg_match($matchhubs, $basedir, $id))
		{
			$id	=	$id[1];	// get the hub id based on path
			$stmt	=	$pdo->query('SELECT httphostkey FROM hub WHERE id = ' . $id );
			
			echo "	*Is Hub = yes.	";
			if($stmt->rowCount() == 0)
			{
				echo " Does not exist in db: $id\n";
				continue;
			}
			$httphostkey	=	$stmt->fetchColumn();
			if($httphostkey	 !=	'')
			{
				echo "httphostkey = $httphostkey";				
			}else{
				echo 'httphostkey = blank';
				continue;
			}
			
			$delresult	=	$xmlapi->api2_query(null, 'SubDomain', 'delsubdomain',
							array('domain' => (string)$addon->domain));
			if($delresult->data->result == '0')
			{
				var_dump($delresult);exit;
			}
			
			$addresult	=	$xmlapi->api2_query(null, 'SubDomain', 'addsubdomain',
							array('dir' => $router_dir,
								'domain'	=> (string)$addon->subdomain,
								'rootdomain'	=> (string)$addon->rootdomain));

			if($addresult->data->result == '0'){
			var_dump($addresult);			exit;			
			}
		}else echo " Skipping..";

		echo "\n";
		

	endforeach;
exit;

#    if(!preg_match('/^public_html\/hubs\/domains\/[0-9]+/', $basedir))		#6qube2 path
#    if(!preg_match('/^public_html\/6qube\.com\/hubs\/domains\/[0-9]+/', $basedir)) #6qube1 hubs path

	$subdomaintype	=	'unknown';
	if(preg_match('/^public_html\/6qube\.com\/blogs\/domains\/[0-9]+/', $basedir))
		$subdomaintype	=	'blog';
	if($basedir	==	'public_html/6qube.com/press')
		$subdomaintype	=	'press';
	if($basedir	==	'public_html/6qube.com/blogs')
		$subdomaintype	=	'blogs';
	if($basedir	==	'public_html/6qube.com/search')
		$subdomaintype	=	'search';
	if($basedir	==	'public_html/6qube.com/articles')
		$subdomaintype	=	'articles';
	if($basedir	==	'public_html/6qube.com/browse')
		$subdomaintype	=	'browse';
	if($basedir	==	'public_html/6qube.com/hubs')
		$subdomaintype	=	'hubs';
	if($basedir	==	'public_html/6qube.com/local')
		$subdomaintype	=	'local';

        echo "$info->domain Basedir: $basedir Type: $subdomaintype\n";
	
	if($subdomaintype	==	'unknown')
	        continue;
    
    echo $info->domain, ":sub=", $match[1], ".. Deleting: ";
    
    $result2    =    $xmlapi->api2_query($argv[6], "SubDomain", "delsubdomain",
                            array('domain' => (string)$info->domain)
            );
    
    echo 'result = ', $result2->data->result, "\n";
    
#    usleep(500000);
   

echo "Done deleting subdomains! Now Setting up Wildcard Subdomains:";


