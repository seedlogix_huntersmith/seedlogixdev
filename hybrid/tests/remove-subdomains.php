<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';

$name   =   $argv[5];   //'bbc.co.uk';    //simplyrankings.net';

if($argv[8] != 'confirmed')
{
    echo ' Usage: script.php .. .. .. SERVER.6qube.com HOST USER PASS confirmed';
    
    var_dump($argv);
    exit;
}

require_once QUBEADMIN . 'inc/cpanel.xmlapi.php';

$xmlapi =   new xmlapi($argv[4], $argv[6], $argv[7]);

$result  =   $xmlapi->api2_query($argv[6], "SubDomain", "listsubdomains",
                array('regex' => $name));

$backup_subd_fname	=	'subdomains-' . $name . '.xml';

if(!file_exists($backup_subd_fname))
	$result->asXML($backup_subd_fname);	//'subdomains-' . $name . '.xml');

if($argv[9]	==	'saveonly')
{
	echo 'Save only. Please confirm file exists: ', $backup_subd_fname, "\n";
exit;
}

echo "Deleting: ";
var_dump($result->data);

foreach($result->data as $info)
{
    if(!preg_match('/^([^\.]*)\.' . $name . '$/', $info->domain, $match)) continue;
            
    $sub    =   $match[1];
    if($sub == '*')
    {
        echo 'wats up!';
        continue;
    }
    
    $basedir = (string)$info->basedir;
    
    if(!preg_match('/^public_html\/hubs\/domains\/[0-9]+/', $basedir))
    {
        echo "Skipping: $info->domain Basedir: $basedir\n";
        continue;
    }
    
    echo $info->domain, ":", $match[1], ".. Deleting: ";
    
    $result2    =    $xmlapi->api2_query($argv[6], "SubDomain", "delsubdomain",
                            array('domain' => (string)$info->domain)
            );
    
    echo $result2->data->result, "\n";
    
#    var_dump($result);
    usleep(500000);
#    sleep(4);
    
}
