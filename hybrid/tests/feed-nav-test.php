<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';
require HYBRID_PATH . '/library/simplepie/autoloader.php';
require HYBRID_PATH . '/classes/FeedObject.php';
require HYBRID_PATH . '/classes/FeedNav.php';
require HYBRID_PATH . '/classes/FeedObjects.php';

require HYBRID_PATH . '/classes/custom/SCLeadsFeedObject.php';



$db =   Qube::Start()->GetDB('slave');
$feednav    =   new FeedNav($db, 1, 'SCLeadsFeedObject', 'products');

$f  =   $feednav->getArray();

$feednav->printNav();
