<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

//phpinfo();
define('QUBE_NOLAUNCH', true);
require dirname(__FILE__) . '/../bootstrap.php';

// for admin controllers
require_once QUBEADMIN . 'library/Popcorn/BaseController.php';
require_once QUBEADMIN . 'classes/QubeLogPDO.php';

// start the qube class loader for test skeleton generator.
if(basename($_SERVER['argv'][0]) === 'phpunit-skelgen')
    Qube::Start();
