<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';

$q  =   Qube::Start();
$Q  =   $q->queryObjects('ResellerModel', 'M')->Exec();

/* @var $reseller ResellerModel */
while($reseller =   $Q->fetch())
{
    if(empty($reseller->main_site)) continue;
    
    $_SERVER['HTTP_HOST']    =   $reseller->main_site;
    $_SERVER['REQUEST_URI'] =   '/';
    
    try{
    include '../router.php';
    }Catch(HubNotFoundException $e)
    {
    }
    Catch(Exception $e)
    {
        echo "\n\nFailed:\n\n", $e->getMessage();
    }
    echo "Done with: ", $_SERVER['HTTP_HOST'], "\n";
}
