<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';

$q  =   Qube::Start();
/* @var $q Qube */
$Qt  =   $q->queryObjects('ResellerModel', 'M')->Exec();

var_dump($Qt);


$p  =   $q->GetDB()->query('create temporary table hellootemp as select 2 AS a, 3 as b, 9 as c');

try{
$r  =   $q->GetDB()->query('SELECT * FROM hellootemp')->fetchAll(PDO::FETCH_ASSOC);

var_dump($r);
}catch(PDOException $e)
{
    
    echo 'Exception: ', $e->getMessage(), "\n\n";
}

echo ' now selecting from master';

$r  =   $q->GetDB('master')->query('SELECT * FROM hellootemp')->fetchAll(PDO::FETCH_ASSOC);

var_dump($r);

$p  =   $q->GetDB('master')->prepare('INSERT INTO hellootemp VALUES (?, ? , ?)');

$M  =   $q->GetDB('master');

$M->beginTransaction();
$p->execute(array(5, 6, 7));
$M->commit();
        


echo 'committed. now reselecting:';
$r  =   $q->GetDB('master')->query('SELECT * FROM hellootemp')->fetchAll(PDO::FETCH_ASSOC);

var_dump($r);

$f  =   Qube::GetPDO();

var_dump($f, $M, $q->GetDB());