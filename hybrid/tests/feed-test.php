<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

chdir(dirname(__FILE__));

require '../bootstrap.php';
require HYBRID_PATH . '/library/simplepie/autoloader.php';
require HYBRID_PATH . '/classes/FeedObject.php';
require HYBRID_PATH . '/classes/FeedNav.php';
require HYBRID_PATH . '/classes/FeedObjects.php';



$db =   Qube::Start()->GetDB('slave');
$Q  =   $db->query('SELECT ID, user_ID, root, classname, url, pagename, key_match FROM  feeds');


while($feed =   $Q->fetchObject())
{
	echo "Processing Feed: $feed->ID / $feed->classname / $feed->url / $feed->root\n";
	
    $classname  =   $feed->classname;
    require_once HYBRID_PATH . 'classes/custom/' . $classname . '.php';
    if(!class_exists($classname)) throw new Exception('failed');
    
    /** @var FeedObject  */
    $parserObject   =   new $classname;
    
#    $nav  =   new FeedNav($db, $feed->ID, $parserObject);    //array('products' => (object)(array('CATEGORY' => (OBJECT)array('FULL_NAME'))));
#    $nav->printNav();
    
#    continue;
    $importer   =   new FeedObjects($feed->url);
    
    
    
    $importer->SaveAll($parserObject, $feed);
    $db->query('UPDATE feeds SET last_import = NOW() where ID = ' . (int)$feed->ID);
}

#var_dump($reportinfo);
exit;
asciiarray($reportinfo->items);
#echo json_encode($reportinfo);


