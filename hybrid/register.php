<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require_once 'bootstrap.php';
require_once HYBRID_PATH . 'classes/HybridRouter.php';

// Check for post if old form.
if (!empty($_POST)) {
    $signup_vars = $_POST; // For backwards compatibility.

// Session used for in between 'activate.php' in cloudmu.  
} else if (!empty($_SESSION['signup_post_vars'])) {
    session_start();
    
    $signup_vars = $_SESSION['signup_post_vars'];
    unset($_SESSION['signup_post_vars']);
    
}

$PostContainer	= VarContainer::getContainer($signup_vars);

function SendWelcomeEmail(UserModel $User, Qube $q)
{
	$emailSender	=	$q->getMailer();
	$Welcome	=	new WelcomeEmail($User);
	$Welcome->setSender(new SendTo('SeedLogix', 'support@seedlogix.com'));
	$sent	=	$emailSender->Send($Welcome, $User->asRecipient());
	if(false === $sent)
		return $emailSender->getError();

	return true;
}

function ValidateRegistrationPost(VarContainer $P)
{
//	$firstName	=	trim($P->checkValue('firstName', ''));
//	$lastName	=	trim($P->checkValue('lastName', ''));
//	$emailAddress	=	trim($P->checkValue('emailAddress', ''));
//	$company	=	trim($P->checkValue('company', ''));
//	$phone	=	trim($P->checkValue('phone', ''));
	
	$VS	=	new ValidationStack();
	foreach(array('firstName', 'lastName', 'emailAddress', 'phone') as $key)
	{
		$val	=	$P->getValue($key, '');
		if(strlen(trim($val)) < 3)
		{
			$VS->addError($key, 'This field must be longer than you specified.');
		}
	}
	
	// @todo validate email address
	if(FALSE)
	{
		$VS->addError('emailAddress', 'Provide a valid email address.');
	}
	
	return $VS;
}

function PrintValidation(ValidationStack $Validation)
{
	$obj	=	$Validation->getErrors();
	$response	=	array('success' => false, 'validation' => $obj);
	echo json_encode($response);
	exit;
}

$Validation	= ValidateRegistrationPost($PostContainer);

if($Validation->hasErrors())
	PrintValidation ($Validation);

// 
// Do Registration
//

$SDA	=	new SystemDataAccess();
$SubMgr	=	new SubscriptionManager();
$MM	=	new ModelManager();

function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

$data	=	array('username' => $PostContainer->emailAddress, 'password' => generatePassword(), 'role' => false);
$data['userinfo'] = array(
	'firstName' => $PostContainer->firstName,
	'lastName' => $PostContainer->lastName,
	'emailAddress' => $PostContainer->emailAddress,
	'company' => $PostContainer->company,
	'phone' => $PostContainer->phone
);
$result	=	$SubMgr->doTrialSubscriptionRegistration($data, $SDA, $MM);


if($result instanceof ValidationStack)
	PrintValidation ($result);

$response	=	array('success' => true);

$response['emailsent']	=	SendWelcomeEmail($result->getObject(), Qube::Start());
echo json_encode($response);
