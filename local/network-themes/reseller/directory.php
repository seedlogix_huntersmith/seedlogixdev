<?
	if($_GET['id']){
		require_once(QUBEADMIN . 'inc/directory.class.php');
		$dir = new Dir();
		$dir_id = $_GET['id'];
	}
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	
	if($siteInfo['category']) $siteInfoCategory = $siteInfo['category'];
	else $siteInfoCategory = $siteInfo['industry'];
	
	$result = $dir->getDirectories(NULL, NULL, $dir_id, NULL, NULL, $siteInfoCategory);
	if($dir->getDirectoryRows()){
		$row = $dir->fetchArray($result);
		//Number of items displayed on right bar
		$rightLimit = 4;
		//URL to user's directory images
		$userImgUrl = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/directory/';
		//Default link
		$dfltLink = $row['website_url'] ? $row['website_url'] : '#';
		//Page title
		$title = $row['company_name'].' featured on '.$resellerCompany.' | '.$row['keyword_one'];
		//Meta description
		$meta_desc = $row['display_info'];
		
		if($row['photo'] == "") {
			$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
		} else {
			$icon = $userImgUrl.$row['photo'];
		}
		
		$a = explode("->", $row['category']);
		$category = ucwords(trim($a[1])); 
		$city = $row['city'];
		$state = $row['state'];
		
		
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>

			<div class="inner-container">
				<div id="innerpage-title">
					<? if($siteInfo['remove_search']){ ?>
                <?=$siteInfo['edit_region_7']?>
				<? } else { ?>
                 	<? if($siteInfo['network_name']){ ?>
                        <h1><?=$siteInfo['network_name']?></h1>
                    <? } else { ?>
                        <h1>Local Business Directory</h1>
                    <?php } ?>
                        <!-- Search-->
                        <? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
                <?php } ?>
                </div>
                
                <br class="clear" />
                <br class="clear" />
				
				<!-- Start Two Third -->
				<div class="two-third">
				<?=$siteInfo['edit_region_1']?>
					<!-- Breadcrumb -->
					<div id="breadcrumb">
						You Are Here: <a title="<? if($siteInfo['network_name']){ ?><?=$siteInfo['network_name']?><? } else { ?>Local Yellow Pages Directory by <?=$resellerCompany?> Local<?php } ?>" href="http://<?=$site?>"><? if($siteInfo['network_name']){ ?><?=$siteInfo['network_name']?><? } else { ?>Local<?php } ?></a> <? if($siteInfo['city_focus']){ ?><? } else { ?>| <?=$dir->getCityLink($row['city'], $row['state'], $site, NULL, true, $row['city'].' yellow pages | '.$resellerCompany.' Local Directory')?> | <?=$dir->getCatLink($row['city'], $row['state'], $category, $site, $row['company_name'].' | '.$row['keyword_one'].' | '.$row['keyword_two'])?><?php } ?> | <?=$row['company_name']?>
					</div>
					
					<!-- Start Listing Content -->
					<div class="blog-post">
						<!-- Title and Keywords -->
						<div class="post-title">
							<h3><a href="<?=$dfltLink?>"><?=$row['company_name']?></a></h3>
							<span class="clear">
								Tags <a href="<?=$dfltLink?>" title="<?=$row['keyword_one']?> | <?=$row['company_name']?>" class="category_name" rel="nofollow"><?=$row['keyword_one']?></a>
							<? if($row['keyword_two']){ ?>
								, <a href="<?=$dfltLink?>" title="<?=$row['keyword_two']?> | <?=$row['company_name']?>" class="category_name" rel="nofollow"><?=$row['keyword_two']?></a>
							<? } ?>
							<? if($row['keyword_three']){ ?>
								, <a href="<?=$dfltLink?>" title="<?=$row['keyword_three']?> | <?=$row['company_name']?>" class="category_name" rel="nofollow"><?=$row['keyword_three']?></a></span>
							<? } ?>
						</div>
						<br style="clear:both;" />
						
						<!-- Listing Image -->
						<div class="blog2-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['company_name']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
							</div>
						</div>
						
						<!-- Address -->
						<div class="add-container">
							<p class="address"><?=$row['street']?></p>
							<p class="city"><?=$row['city']?>, <?=$row['state']?> <?=$row['zip']?></p>
							<p class="phone" style="width:400px;"><?=$dir->formatPhone($row['phone'])?></p>
							<p><a href="<?=$dfltLink?>" <? if($row['photo'] == ""){ ?>rel="nofollow"<? } ?> class="button_size4"><span>View Website</span></a></p>
						</div>
						
						<!-- Social -->
						<div class="rightDir">
						<? if($row['twitter']){ ?>
							<a href="<?=$row['twitter']?>" title="Twitter" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/twitter.png" alt="Twitter" />
							</a>
						<? } ?>
						<? if($row['facebook']){ ?>
							<a href="<?=$row['facebook']?>" title="Facebook" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/facebook.png" alt="Facebook" />
							</a>
						<? } ?>
						<? if($row['myspace']){ ?>
							<a href="<?=$row['myspace']?>" title="Myspace" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/myspace.png" alt="Myspace" />
							</a>
						<? } ?>
						<? if($row['youtube']){ ?>
							<a href="<?=$row['youtube']?>" title="YouTube" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/youtube.png" alt="Youtube" />
							</a>
						<? } ?>
						<? if($row['blog']){ ?>
							<a href="<?=$row['blog']?>" title="Blog" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/blogger.png" alt="Blog" />
							</a>
						<? } ?>
						<? if($row['linkedin']){ ?>
							<a href="<?=$row['linkedin']?>" title="LinkedIn" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/linkedin.png" alt="LinkedIn" />
							</a>
						<? } ?>
						<? if($row['technorati']){ ?>
							<a href="<?=$row['technorati']?>" title="Technorati" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/technorati.png" alt="Technorati" />
							</a>
						<? } ?>
						<? if($row['diigo']){ ?>
							<a href="<?=$row['diigo']?>" title="Diigo" target="_blank">
							<img src="<?=$themeInfo['themeUrl2']?>images/icons/diigo.png" alt="Diigo" />
							</a>
						<? } ?>
						</div>
						<br class="clear" />
					</div>
					
					<!-- Listing Description -->
					<div class="slogan">
						<p><?=$row['display_info']?></p>
					</div>
					<br class="clear" />
					
					<!-- Listing Overview -->
					<div class="blog-post">
						<div class="dirIcon">
							<img src="<?=$themeInfo['themeUrl2']?>images/backgrounds/home.png" alt="" />
						</div>
						<!-- Keywords one and two -->
						<div class="post-title">
							<h3><?=$row['company_name']?> Overview</h3>
							<span class="clear">Tags 
								<a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_one']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_one']?></a>
								<? if($row['keyword_two']){ ?>
								 | <a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_two']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_two']?></a>
								<? } ?>
							</span>
						</div>
						<p>
						<?
							if($row['company_spot']) echo $row['company_spot'];
							else echo "This listing doesn't have an overview yet.";
						?>
						</p>
						<br class="clear" />
					</div> 
					
                    
					<!-- Listing Photos -->
					<div class="blog-post">
						<div class="dirIcon">
							<img src="<?=$themeInfo['themeUrl2']?>images/backgrounds/photos.png" alt="" />
						</div>
						<div class="post-title">
							<h3><?=$row['company_name']?> Photos</h3>
							<!-- Keywords three and four -->
							<span class="clear">Tags 
							<? if($row['keyword_three']){ ?>
								<a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_three']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_three']?></a>
							<? } ?>
							<? if($row['keyword_four']){ ?>
								 | <a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_four']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_four']?></a>
							<? } ?>
							</span>
						</div>
						
						<!-- Images -->
						<?
						if($row['pic_one']){
							//loop through images one through six
							$nums = array('one', 'two', 'three', 'four', 'five', 'six');
							for($i = 0; $i<=6; $i++){
								if($row['pic_'.$nums[$i]]){ 
						?>
							<!-- Image <?=$nums[$i]?> -->
							<div class="blog3-post-img">
								<div class="fade-img" lang="zoom-icon">
									<a href="<?=$userImgUrl.$row['pic_'.$nums[$i]]?>" rel="prettyPhoto[gallery2column]" title="<?=$row['pic_'.$nums[$i].'_title']?>">
									<img src="<?=$userImgUrl.$row['pic_'.$nums[$i]]?>"  width="165px" alt="<?=$row['pic_'.$nums[$i].'_title']?>" />
									</a>
								</div>
							</div>
						<?
								}
							}
						}
						else {
							//if no images
							echo "This listing doesn't have any photos yet.";
						}
						?>
						<!-- End Images -->
						<br class="clear" />
					</div>
					
					<!-- Listing Videos -->
					<div class="blog-post">
						<div class="dirIcon">
							<img src="<?=$themeInfo['themeUrl2']?>images/backgrounds/movie.png" alt="" />
						</div>
						<div class="post-title">
							<h3><?=$row['company_name']?> Videos</h3>
							<!-- Keywords five and six -->
							<span class="clear">Tags 
							<? if($row['keyword_five']){ ?>
								<a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_five']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_five']?></a>
							<? } ?>
							<? if($row['keyword_six']){ ?>
								 | <a href="<?=$dfltLink?>" title="<?=$row['company_name']?> | <?=$row['keyword_six']?> | <?=$row['city']?>, <?=$row['state']?>" rel="nofollow"><?=$row['keyword_six']?></a>
							<? } ?>
							</span>
						</div>
						<p>
						<?
							if($row['video_spot']) echo $row['video_spot'];
							else echo "This listing doesn't have any videos yet.";
						?>
						</p>
						<br class="clear" />
					</div>
					<? if($siteInfo['remove_contact']){ ?>
                
				<? } else { ?>
					<!-- Contact form -->
					<div class="blog-post">
						<div class="post-title">
							<h3>Contact <?=$row['company_name']?></h3>
						</div>
                        
						<!--Start Contact Form-->
						<div id="contact-form">	
						<div id="contactdiv" style="clear:both;">
							<div id="msg"></div>
							<form action="http://<?=$siteInfo['reseller_main_site']?>/process-form.php" method="POST" name="contact">
								<label>Name:*</label>
								<input name="name" value="" type="text" class="input1 rounded3" />
								<label>Email:*</label>
								<input name="email" value="" type="text" class="input1 rounded3" />
								<label>Phone:*</label>
								<input name="phone" value="" type="text" class="input1 rounded3" />
								<label>Message:</label>
								<textarea name="comments" cols="" rows="" class="input2 rounded3"></textarea>
								<br style="clear:both;" />
								<!-- hidden variables -->
								<input type="hidden" name="lead_source_id" value="<?=$row['company_name']?>" />
								<input type="hidden" name="user_id" value="<?=$row['user_id']?>" />
								<input type="hidden" name="type_id" value="<?=$row['id']?>" />
								<input type="hidden" name="type" value="direc" />
								<input type="hidden" name="domain" value="<?='http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" />
								<p class="left">
									<button name="submit" type="submit" class="blackButton3"><span>SUBMIT</span></button>
								</p>
							</form>
						</div>
						</div>
						<!-- End Contact Form -->
                        
						<br class="clear" />
					</div>
                    <?php } ?>
					<?=$siteInfo['edit_region_4']?>
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
                            	<div class="sidebar-advertise">
                                <? if($siteInfo['edit_region_9']){ ?>
							<?=$siteInfo['edit_region_9']?>
                            <? } else { ?>
                            <?=$siteInfo['edit_region_2']?>
                            <?php } ?>
							</div>
								<!-- Map -->
								<h2>Come Visit Us!</h2>
								<div class="sidebar-advertise">
									<iframe class="iframe" src="http://6qube.com/google-map.php?addy=<?=urlencode($row['street'])?>&city=<?=urlencode($row['city'])?>&state=<?=urlencode($row['state'])?>&company=<?=urlencode($row['company_name'])?>&phone=<?=urlencode($row['phone'])?>&width=265&height=350&logo=<?=urlencode($icon)?>" frameborder="0" scrolling="no" width="265" height="350"></iframe>
								</div>
								
                             <? if($siteInfo['parent'] == "0"){ ?>
                            <? } else { ?>
								<!-- Hubs -->
								<h2>Our HUBs</h2>
								<div class="sidebar-advertise">
								<?php
								if($results = $dir->getHubsForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($hub_row = $dir->fetchArray($hubs)){
										if(!$hub_row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$hub_row['user_id'].'/hub/'.$hub_row['logo'];
								?>
										<li><a href="<?=$hub_row['domain']?>" title="<?=$hub_row['company_name']?> | <?=$hub_row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo "This user hasn't created any hubs.";
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Press -->
								<h2>Our Press Releases</h2>
								<ul>
								<?php
								if($results = $dir->getPressForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$press = $results['results'];
									while($press_row = $dir->fetchArray($press)){
										$a = explode("->", $press_row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
													$dir->convertKeywordUrl($pressCategory).
													$dir->convertKeywordUrl($press_row['headline']).$press_row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title=""><?=$press_row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't written any press releases.";
								?>
								</ul>
								<!-- End Press -->
								
								<!-- Blog Posts -->
								<h2>Our Blog Posts</h2>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $dir->getPostsForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$posts = $results['results'];
									while($post_row = $dir->fetchArray($posts)){
										$postUrl = $post_row['domain']."/".$dir->convertKeywordUrl($post_row['category']).
					 								$dir->convertPressUrl($post_row['post_title'], $post_row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$post_row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't written any blog posts.";
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->							
								
								<!-- Articles -->
								<h2>Our Articles</h2>
								<ul>
								<?php
								if($results = $dir->getArticlesForNetwork($rightLimit, $offset, NULL, NULL, NULL, NULL, 
																$row['user_id'], $row['cid'])){
									$articles = $results['results'];
									while($art_row = $dir->fetchArray($articles)){
										$a = explode("->", $art_row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$dir->convertKeywordUrl($articleCategory).
													$dir->convertKeywordUrl($art_row['headline']).$art_row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title=""><?=$art_row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo "This user hasn't written any articles.";
								?>
								</ul>
								<!-- End Articles -->
                                <?php } ?>
								<div class="sidebar-advertise">
							<? if($siteInfo['edit_region_10']){ ?>
							<?=$siteInfo['edit_region_10']?>
                            <? } else { ?>
                            <?=$siteInfo['edit_region_3']?>
                            <?php } ?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div> <!-- End One Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		$customTracking = $row['tracking_id'];
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<?
} //end if($dir->getDirectoryRows())
else {
	
	header ('HTTP/1.1 301 Moved Permanently');
  	header ('Location: '.$siteInfo['domain']);
}
?>