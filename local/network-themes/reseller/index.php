<?php

//if($args = $_GET['args']){

//include(QUBEROOT . 'search/network-themes/'.$themeInfo['path'].'/index.php');

//echo 'nothing';
										
//} else {
	
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$dispListings = true;
	$continue = true;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		if($siteInfo['login_url']){
		$dflt_noImage = 'http://'.$siteInfo['login_url'].'/img/no_image.jpg';
		} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
		}
	}
	
	
	
	//title and meta description -- gets changed if viewing city or category
	if($siteInfo['seo_title']){
		$title = $siteInfo['seo_title'];
	}
	else $title = "".$resellerCompany." Local Business Directory";
	if($siteInfo['seo_desc']){
		$meta_desc = $siteInfo['seo_desc'];
	}
	else $meta_desc = "".$resellerCompany." Local Business Directory, Local Business Listings, Local Yellow Pages Directory, Local Business White Pages.";

	
	
	require_once(QUBEADMIN . 'inc/directory.class.php');
	$dir = new Dir();
	
	if($siteInfo['city_focus']) $city = $siteInfo['city_focus'];
	if($siteInfo['state_focus']) $state = $siteInfo['state_focus'];
	if($siteInfo['category']) $siteInfoCategory = $siteInfo['category'];
	else $siteInfoCategory = $siteInfo['industry'];
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);

                if($_GET['location']) $args_array[0]        =       $_GET['location'];
				if($_GET['keyword']){
				
				$keyword     =       $_GET['keyword'];
				$mainLimit = 200;
				
				}

		
		if($args_array[0]=="cities"){
			//echo "cities";
			$dispListings = false;
			$dispCities = true;
			$title = "".$resellerCompany." | Browse Local Businesses by City";
			$meta_desc = "Find Local Businesses by browsing ".$resellerCompany." City Search.";
		}
		
		//else if($args_array[0]=="service" && $args_array[1]){
			//echo "cities";
			//$dispListings = true;
			//$service = true;
			//$currPage = $args_array[2];
			//$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			//$category = $dir->validateText($args_array[1]);
			//$b = explode("-", $category);
			//$b = implode(" ", $b);
			//$category = ucwords(trim($b));
			//$mainLimit = 25;
			
		//}
		
		else if(is_numeric($args_array[0])){
			//echo "all listings - page ".$args_array[0];
			$dispCities = true;
			$currPage = $args_array[0];
			$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			if($siteInfo['seo_title']){
				$title = $siteInfo['seo_title']." | Page ".$currPage;
			}
			else $title .= " | Page ".$currPage;
			$meta_desc .= " Page ".$currPage." of results.";
		}
		else if($city = $dir->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $dir->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $dir->majorCity($city);
				$majorCity = true;
			}
			$cityLink = $dir->getCityLink($city, $state, $site, NULL, true);			
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//echo "$city." listings, page #".$args_array[1];
					$dispCats = true;
					$currPage = $args_array[1];
					$mainOffset = ($mainLimit*$currPage)-$mainLimit;
					$title = $city.', '.$state.' '.$resellerCompany.' Business Directory | Page '.$currPage;
					$meta_desc = 'Find '.$city.' '.$state.' Business Listings on '.$resellerCompany.' in '.$city.' '.$dir->getStateFromAbv($state).'. Page '.$currPage.' of results.';
				}
				else if($args_array[1]=="categories"){
					//echo $city." Categories";
					$dispListings = false;
					$dispCities = false;
					$dispCats = true;
					$title = $city.', '.$state.' '.$resellerCompany.' Business Categories | All Business Categories';
					$meta_desc = 'Find Local Businesses in '.$city.', '.$state.' by browsing '.$resellerCompany.' Local All Categories Display of '.$city.' '.$dir->getStateFromAbv($state).' Yellow Pages';
				}
				else if($category = $dir->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							if($args_array[2]<20){
								//echo $city." ".$category."s, page ".$args_array[2];
								//if third argument is numeric it could be either
								//a listing's id or pagination for a city's category.
								//assume there won't be more than 20 pages for any cat
								//(and there aren't any listings with id<20)
								$currPage = $args_array[2];
								$mainOffset = ($mainLimit*$currPage)-$mainLimit;
								$title = $resellerCompany.' | '.$category.' in '.$city.
										' | Page '.$currPage;
								$meta_desc = $resellerCompany.' '.$category.' Directory for '.$city.' '.$state.'.  '.$category.' in '.$city.' '.$dir->getStateFromAbv($state).'. Page '.$currPage.' of results.';
							}
							else {
								$continue = false;
								$dir_id = $args_array[2];
								$category = '';
								$city = '';
								
include(QUBEROOT . 
'local/network-themes/'.
										$themeInfo['path'].'/directory.php');
							}
						}
					}
					else {
						//echo $city." ".$category."s";\
						if($siteInfo['citycat_seo_title']){
						$title = $siteInfo['citycat_seo_title'];
						}	
						else $title = $resellerCompany.' | '.$category.' in '.$city;
						if($siteInfo['citycat_seo_desc']){
						$meta_desc = $siteInfo['citycat_seo_desc'];
						}	
						else $meta_desc = $resellerCompany.' '.$category.' Directory for '.$city.' '.$state.'.  '.$category.' in '.$city.' '.$dir->getStateFromAbv($state);
						
					}
				}
			} //end if($args_array[1])
			else {
				//echo "All ".$city." listings";
				$dispCities = false;
				$dispCats = true;
				
				if($siteInfo['city_seo_title']){
				$title = $siteInfo['city_seo_title'];
				}	
				else $title = $city.', '.$state.' Local Business Directory | '.$city.' Business Listings';
				
				if($siteInfo['city_seo_desc']){
				$meta_desc = $siteInfo['city_seo_desc'];
				}	
				else $meta_desc = 'Find '.$city.' '.$state.' Business Listings on '.$resellerCompany.' in '.$city.' '.$dir->getStateFromAbv($state);
			}
				
				
		}
					
					
					
					
	} //end if($args = $_GET['args'])
	else {
		$dispCities = true;
	}
	
if($continue){
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
                <? if($siteInfo['remove_search']){ ?>
                <?=$siteInfo['edit_region_7']?>
				<? } else { ?>
                 	<? if($siteInfo['network_name']){ ?>
                        <h1><?=$siteInfo['network_name']?></h1>
                    <? } else { ?>
                        <h1>Local Business Directory</h1>
                    <?php } ?>
                        <!-- Search-->
                        <? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
                <?php } ?>
				</div>
                
                <br class="clear" />
                <br class="clear" />
				
				<!-- Start Two Third -->
				<div class="two-third">

				<?=$siteInfo['edit_region_1']?>
					<!-- Header Title -->
					<?php
					if($dispListings){
						if($category) 
							echo '<h1>Local '.$category.' business listings for '.$cityLink.'</h1>';
						else if($city) 
							if($siteInfo['city_focus']){

							}
							else echo '<h1><a href="http://'.$site.'">Local business listings</a> for '.ucwords($city).', '.$state.'</h1>';
					}
					?>
					
					<!-- Breadcrumb -->
					<div id="breadcrumb">
					<?
					if($dispListings){
						if($dispCities) {
							if($siteInfo['city_focus']){

							}
							else $dir->displayCitiesLocal(20, $site, $resellerID, $siteInfoCategory, $state); 
						}
						else if($dispCats) $dir->displayCategoriesLocal($city, $state, 10, $site, $resellerID, $siteInfoCategory);
					}
					?>
					</div>
                    
			
                    
            
            
                    <!-- Start Display of Listings -->
                    <?php
                    if($dispListings){
				
                        if($results = $dir->getListingsForNetwork($mainLimit, $mainOffset, $city, $state, $category, $resellerID, NULL, NULL, $keyword, $siteInfoCategory, $siteInfo['class_only'], NULL, ture, $siteInfo['featured_class'])){
                            $numListings = $results['numResults'];
                            $listings = $results['results'];
                            while($row = $dir->fetchArray($listings)){
                               if($siteInfo['link_type']){
								$z = explode("->", $row['category']);
								$catLink = $z[1];
								} else {
								$catLink = $row['keyword_one'];	
								}
								
                                $listingUrl = 'http://'.$site.'/'.$dir->convertKeywordUrl($catLink).
                                            $dir->convertKeywordUrl($row['company_name']).$row['id'].'/';
                            
                                if(!$row['photo']) 
                                    $icon = $dflt_noImage;
                                else 
                                    $icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/directory/'.$row['photo'];
                                
                                $lastUpdated = date("F jS, Y", strtotime($row['last_edit']));
                                $createdMo = date("M", strtotime($row['created']));
                                $createdDay = date("j", strtotime($row['created']));
								if($row['basic_listing']) $class = 'basic';
								if($row['class_id']!=0) $classid = 'style'.$row['class_id'];
								else $classid = '';
                        ?>
                            <!-- Start Listing -->
                            <div class="blog-post <?=$classid?> <?=$class?>">
                            
                            <? if(!$row['basic_listing']){ ?>
                                <!-- Date -->
                                <div class="post-date">
                                    <span class="post-month"><?=$createdMo?></span><br />
                                    <span class="post-day"><?=$createdDay?></span>
                                </div>
                             <? } ?>   
                                <!-- Title -->
                                <div class="post-title">
                                    <h3><a href="<?=$listingUrl?>"><?=$row['company_name']?></a></h3>
                                    <span class="clear">
                                        Tags: <a href="<?=$listingUrl?>" title="<?=$row['keyword_one']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_one']?></a>
                                    <? if($row['keyword_two']){ ?>
                                        , <a href="<?=$listingUrl?>" title="<?=$row['keyword_two']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_two']?></a>
                                    <? } ?>
                                    <? if($row['keyword_three']){ ?>
                                        , <a href="<?=$listingUrl?>" title="<?=$row['keyword_three']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_three']?></a>
                                    <? } ?>
                                    </span>
                                </div>		
                                <br style="clear:both;" />
                                
                 				<? if(!$row['basic_listing']){ ?>
                                <!-- Image -->
                                <div class="blog2-post-img">
                                    <div class="fade-img" lang="zoom-icon">
                                        <a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['company_name']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
                                    </div>
                                </div>
                 				<? } ?>
                                
                                <? if(!$row['basic_listing']){ ?>
                                <!-- Details -->
                                <div class="displayPhone"><?=$dir->formatPhone($row['phone'])?></div>
                                
                                        <p class="date"><i>Last Updated: <br/><?=$lastUpdated?></i></p>							
                                        <?=$dir->shortenSummary($row['display_info'], 250)?><br />
                                        <a href="<?=$listingUrl?>" title="Local Yellow Pages Directory by <?=$resellerCompany?> Presents <?=$row['company_name']?>">Visit <?=$row['company_name']?> Local Profile</a></p>
                                <br class="clear" />
                                
                                 <div class="add-container">
                                    <p class="address"><?=$row['street']?></p>
                                    <p class="city"><?=$row['city']?>, <?=$row['state']?> <?=$row['zip']?></p>
                                </div>
                                
                                <div class="right">
                                    <a href="<?=$listingUrl?>" class="button_size4"><span>View Listing</span></a>
                                </div>		
                                
                                <? } else { ?>
                                
                                 <!-- Details -->
                                <div class="displayPhone" style="float:left;padding-right:15px;"><?=$dir->formatPhone($row['phone'])?></div>	

                                 <div class="add-container">
                                    <p class="address"><?=$row['street']?></p>
                                    <p class="city"><?=$row['city']?>, <?=$row['state']?> <?=$row['zip']?></p>
                                </div>
 
                                 <div class="right">
                                    <a href="<?=$listingUrl?>" class="button_size4"><span>View Listing</span></a>
                                </div>
                                
                       
                                
                                <? } ?>
                                
                               	
                            </div> 
                            <!-- End Listing -->
                        <? 
                            } //end while($row = $dir->fetchArray($listings))
                        ?>
                            <!-- End Display of Listings -->
                            <br clear="all" />
                         
                   			 
                        <?
                        } //end if($listings = $dir->getListingsForNetwork()
                        else echo '';
                    } //end if($dispListings)
                    else {
                        if($dispCities){
                            echo '<h1>All cities with <a href="http://'.$site.'">local business listings</a></h1>';
                            $dir->displayCitiesLocal(NULL, $site, $resellerID, $siteInfoCategory, $state);
                        }
                        else if($dispCats){
                            echo '<h1>All <a href="http://'.$site.'">local business listings</a> categories for '.$cityLink.'</h1>';
                            $dir->displayCategoriesLocal($city, $state, NULL, $site, $resellerID, $siteInfoCategory);
                        }
                    }
                    ?>
                    
                    
                   
                        
          <? include('inc/pagination.php'); ?>
          
          	<? if($city && !$category && !$args_array[1]=="categories"){ ?>
			<?=$siteInfo['edit_region_18']?>
            <? } else if($category){ ?>
            <?=$siteInfo['edit_region_19']?>
            <? } else { ?>
            <?=$siteInfo['edit_region_4']?>
            <?php } ?>
            
            
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
                           
                           <? if($city && !$category){ ?>
                            
                            	<? if($siteInfo['edit_region_11']){ ?>
								<?=$siteInfo['edit_region_11']?>
                                <? } else { ?>
                                <?=$siteInfo['edit_region_2']?>
                                <?php } ?>
                                
                            <? } else if($category){ ?>
                            
                            	<? if($siteInfo['edit_region_13']){ ?>
								<?=$siteInfo['edit_region_13']?>
                                <? } else { ?>
                                <?=$siteInfo['edit_region_2']?>
                                <?php } ?>
                            
                            <? } else { ?>
                            
								<?=$siteInfo['edit_region_2']?>
                            
                            <?php } ?>
                           
							</div>
                            <? if($siteInfo['parent'] == "0"){ ?>
                            <? } else { ?>
								<!-- Hubs -->
                                <? if($siteInfo['hubs_network_name']){ ?>
								<h2><?=$siteInfo['hubs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local HUBs</h2>
                                 <?php } ?>
								<div class="sidebar-advertise">
								<?php
								if($results = $dir->getHubsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($row = $dir->fetchArray($hubs)){
										if(!$row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$row['user_id'].'/hub/'.$row['logo'];
								?>
										<li><a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo 'No HUBs found.';
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Press -->
								 <? if($siteInfo['press_network_name']){ ?>
								<h2><?=$siteInfo['press_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Press Releases</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $dir->getPressForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$press = $results['results'];
									while($row = $dir->fetchArray($press)){
										$a = explode("->", $row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
													$dir->convertKeywordUrl($pressCategory).
													$dir->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No press releases found.';
								?>
								</ul>
								<!-- End Press -->
								
								<!-- Blog Posts -->
								 <? if($siteInfo['blogs_network_name']){ ?>
								<h2><?=$siteInfo['blogs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Blog Posts</h2>
                                 <?php } ?>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $dir->getPostsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$posts = $results['results'];
									while($row = $dir->fetchArray($posts)){
										$postUrl = $row['domain']."/".$dir->convertKeywordUrl($row['category']).
					 								$dir->convertPressUrl($row['post_title'], $row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo 'No blog posts found.';
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->							
								
								<!-- Articles -->
								 <? if($siteInfo['articles_network_name']){ ?>
								<h2><?=$siteInfo['articles_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Articles</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $dir->getArticlesForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$press = $results['results'];
									while($row = $dir->fetchArray($press)){
										$a = explode("->", $row['category']);
										$articleCategory = $a[1];
										$articleUrl = 'http://articles.'.$domain.'/'.
													$dir->convertKeywordUrl($articleCategory).
													$dir->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$articleUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No articles found.';
								?>
								</ul>
								<!-- End Articles -->
                            <?php } ?>
								<div class="sidebar-advertise">
                                
                            <? if($city && !$category){ ?>
                            
                            	<? if($siteInfo['edit_region_12']){ ?>
								<?=$siteInfo['edit_region_12']?>
                                <? } else { ?>
                                <?=$siteInfo['edit_region_3']?>
                                <?php } ?>
                            
                            <? } else if($category){ ?>
                            
                            	<? if($siteInfo['edit_region_14']){ ?>
								<?=$siteInfo['edit_region_14']?>
                                <? } else { ?>
                                <?=$siteInfo['edit_region_3']?>
                                <?php } ?>
                            
                            <? } else { ?>
                            
								<?=$siteInfo['edit_region_3']?>
                            
                            <?php } ?>
                            
             
							</div>
							
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div>
				<!-- End Last Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<? } //end if($continue) ?>
<? // } //end if($continue) ?>