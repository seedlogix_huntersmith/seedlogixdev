<?
require_once '/home/sapphire/production-code/dev-trunk/hybrid/bootstrap.php';
session_start();

$site = strtolower($_SERVER['SERVER_NAME']);
$a = explode('.', $site);
$domain = $a[count($a)-2].'.'.$a[count($a)-1];

if($domain!='6qube.com'){
	throw new Exception('Invalid.');
}
else {
		require_once(QUBEADMIN . 'inc/directory.class.php');
		$dir = new Dir();

	if($_GET['id']){
		$dir_id = $_GET['id'];
	}
	$directory_id = $dir_id; //$dir_id is set in index.php, which pulls this page in through include()
	$result = $dir->getDirectories(NULL, NULL, $directory_id); //primes directory results, and is needed for getDirectoryRows()
	if($dir->getDirectoryRows()) { //checks to see if the user has any directories
		$row = $dir->fetchArray($result);
		//How many results do you want per heading?
		$limit = 1;
		$rightLimit = 4;
		//do not touch
		$offset = 0;
		
		if($row['photo'] == "") {
			$icon = 'http://6qube.com/img/no_image.jpg';//6qube_cube.png';
		} else {
			$icon = 'http://6qube.com/users/'.$row['user_id'].'/directory/'.$row['photo'];
		}
		
		$a = explode("->", $row['category']);
		$category = $a[1];	
		if($category=='plumbing contractors' || $category=='plumbing repair'){
			$backlinkCategory = 'Plumbers';
		} else if($category=='air conditioning service'){
			$backlinkCategory = 'Air Conditioning';
		} else if($category=='landscape contractors' || $category=='lawn care'){
			$backlinkCategory = 'Landscaping';
		} else if($category=='termite treatment' || $category=='commercial pest control'){
			$backlinkCategory = 'Exterminators';
		} else if($category=='fire restoration' || $category=='mold removal'){
			$backlinkCategory = 'Emergency Restoration';
		} else if($category=='carpet and rug cleaners'){
			$backlinkCategory = 'Carpet Cleaning';
		} else if($category=='kitchen remodeling'){
			$backlinkCategory = 'Remodeling Contractors';
		} else if($category=='electrical contractors'){
			$backlinkCategory = 'Electricians';
		}
		else $backlinkCategory = $category;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="google-site-verification" content="Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y" />
<title>
<?=$row['company_name']?> <?=html_entity_decode('&raquo;')?> <?=$row['city'].', '.$row['state'].' '.$row['zip']?> | Local Business Network
</title>
<meta content="<?=$dir->shortenSummary($row['display_info'], 150)?>" name="description" />
<meta content="INDEX,FOLLOW" name="Robots" />
<meta content="2 Days" name="Revisit-after" />
<link rel="sitemap" href="http://local.6qube.com/sitemap.xml" type="application/xml" />
<meta property="fb:app_id" content="<?=$blog_data['facebook_app_id']?>" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="http://local.6qube.com/js/jquery.min.js"></script>


<!-- ALL jQuery Tools. jQuery library -->

<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.tools.js" type="text/javascript" charset="utf-8"></script>
<!-- End Load -->

<!-- Load Jquery Easing -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.easing.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.css-transform.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.css-rotate-scale.js" type="text/javascript"></script>
<!-- End Load -->

<script src="http://6qube.com/hubs/themes/6qube/_include/js/cufon.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/fonts/arials.font.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/fonts/zurich.font.js" type="text/javascript"></script>

<!-- Load Pretty Photo -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.prettyPhoto.css" type="text/css" media="screen" />
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<!-- End Load -->
<link rel="stylesheet" type="text/css" href="http://6qube.com/js/jquery.fancybox/jquery.fancybox.css" media="screen" />
<script type="text/javascript" src="http://6qube.com/js/jquery.fancybox/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="http://6qube.com/js/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("a[rel^='portfolio']").fancybox(
		{'zoomSpeedIn': 300,'zoomSpeedOut': 200,'overlayShow': false,'zoomOpacity': true ,'overlayShow':true}
	);
});
$(function(){ 
	$('input[title!=""]').hint();	
});
</script>

<!-- Load SWFObject, used for video embedding -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/swfobject.js" type="text/javascript" charset="utf-8"></script>
<!-- End Load -->

<!-- Load Captify -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.captify.js" type="text/javascript"></script>
<!-- End Load -->

<!-- Load Bubble Tip -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.bubbletip.js" type="text/javascript"></script>
<link href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.bubbletip.css" rel="stylesheet" type="text/css" />

<!--[if IE]>
	    <link href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.bubbletip-ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
<!-- End Load -->

<!-- Load Roundies -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/browserdetect.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/roundies.js" type="text/javascript"></script>
<!-- End Load -->

<!-- Load Jquery Twitter -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.tweet.js" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
<!-- End Load -->
<script type="text/javascript"> 	var geocoder;

  var map;
  var company ="<?=$row['company_name']?>";
  var phone ="<?=$row['phone']?>";
  var address ="<?=$row['street']?><br/><?=$row['city'].', '.$row['state'].' '.$row['zip']?>";
  var logo ="<?=$icon?>";
  
</script>
<!-- Load some small custom scripts -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/custom.js" type="text/javascript"></script>
<!-- End Load -->

<!-- Load Main Stylesheets and Default Color and Style -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style-6qube.css" type="text/css" media="screen" title="orange"/>

<!-- Load Main Enhancements Stylesheet border radius, transparency, shadows and such things -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style-enhance.css" type="text/css" media="screen" />
<!-- End Load -->

<!-- Load IE6 Stylesheet -->
<!--[if IE 6]>
        <link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/ie6.css" type="text/css" media="screen" />
    <![endif]-->
<!-- End Load -->

<!-- Load IE7 Stylesheet -->
<!--[if IE 7]>
        <link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/ie7.css" type="text/css" media="screen" />
    <![endif]-->
<!-- End Load -->

<!-- Load PNG Fix older IE Versions -->
<!--[if lt IE 7]>
        <script type="text/javascript" src="http://6qube.com/hubs/themes/6qube/_include/js/pngfix.js"></script>
        <script type="text/javascript">DD_belatedPNG.fix('*');</script>
    <![endif]-->
<!-- End Load -->


<link rel="stylesheet" href="http://local.6qube.com/css/listing-styles.css" type="text/css" media="screen" title="orange"/>

<!-- Place this tag in the <head> of your document -->
<link href="https://plus.google.com/107689484959897238109" rel="publisher" />
<!-- Place this render call where appropriate -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34745846-1']);
  _gaq.push(['_setDomainName', '6qube.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">



$(window).load(function() {




	// load hidden map

	loadHiddenMap();

	

});

</script>
</head>
<body>

<!-- start top -->
<div class="top">
  <div class="full-width rel">
    <div class="one-third">
      <div class="logoBox"><a href="http://local.6qube.com" title="Local Business Directory" ><img src="http://local.6qube.com/images/local-logo2.png" /></a></div>
      <div class="logo"></div>
    </div>
    <div class="two-third">
      <div class="rel">
        <div class="abs utility"> Create <a href="http://6qubedirectory.com" title="Free Business Listing" >Free Business Listing</a> <a href="http://facebook.com/6qube"><img src="http://6qube.com/hubs/themes/6qube/_include/images/facebook.png" alt="Follow Us On Facebook"  /></a> <a href="http://twitter.com/6qube"><img src="http://6qube.com/hubs/themes/6qube/_include/images/twitter.png" alt="Follow Us On Twitter"  /></a> 
          <!-- Place this tag where you want the badge to render --> 
          <a href="https://plus.google.com/107689484959897238109?prsrc=3" style="text-decoration:none;"><img src="https://ssl.gstatic.com/images/icons/gplus-32.png" alt="" style="border:0;width:20px;height:20px;margin:2px 2px 0 0;"/></a> <a href="http://login.6qube.com"><img src="http://6qube.com/hubs/themes/6qube/_include/images/login.png" alt="Login to 6Qube" /></a>
          <div class="clear"></div>
        </div>
      </div>
      <div class="mainmenu">
        <ul id="topnav">
          <li class="active"><a href="http://local.6qube.com" title="Local Business Network"><span>Local</span></a>
            <div class="sub">
              <ul>
              </ul>
            </div>
          </li>
          <li><a href="http://hubs.6qube.com" title="Local Website Directory"><span>Websites</span></a>
            <div class="sub">
              <ul>
                
              </ul>
            </div>
          </li>
          <li><a href="http://blogs.6qube.com" title="Local Blog Directory"><span>Blogs</span></a>
            <div class="sub">
              <ul>
               
              </ul>
            </div>
          </li>
          <li><a href="http://press.6qube.com" title="Local Press Directory"><span>Press</span></a>
            <div class="sub">
              <ul>
               
              </ul>
            </div>
          </li>
          <li><a href="http://articles.6qube.com" title="Local Articles Directory"><span>Articles</span></a>
            <div class="sub">
              <ul>
               
              </ul>
            </div>
          </li>
         
        </ul>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<!-- end top -->

<!-- start middle -->
<div class="middle">
<!--
<div class="search">

<div class="full-width" >
<div class="searchbox">
<form method="get" name="search" id="search-form" action="http://search.6qube.com/index.php">
				
				
	<input type="text" onblur="javascript:if(this.value==''){this.value='Keyword';}" onfocus="javascript:if(this.value=='Keyword'){this.value='';}" value="Keyword" name="business" />
    <input  type="text" onblur="javascript:if(this.value==''){this.value='Location';}" onfocus="javascript:if(this.value=='Location'){this.value='';}" value="Location" name="location" />
<input type="submit" value="Search Now" id="submit" class="btn-big-action-fixed" />
					</form>
</div>
</div>

</div>
-->
<div id="hidden_map"></div>
  <!-- Container of the hidden map -->
<div class="inner">
	<!-- IE 6 Needs this -->
	<div class="full-width">
    <div class="outer-rounded-box-bold">
                        <div class="simple-rounded-box">
						You Are Here: <a title="Local Business Network | Local Yellow Pages Directory" href="http://local.6qube.com">Local</a> | <?=$dir->getCityLink($row['city'], $row['state'], 'local', NULL, true, $row['city'].' Yellow Pages | Local Business Network')?> | <?=$dir->getCatLink($row['city'], $row['state'], $category, 'local', $row['company_name'])?> | <?=$row['company_name'] ?> <!--| <a href="http://www.amazon.com/gp/search?ie=UTF8&camp=1789&creative=9325&index=aps&keywords=<?=$category ?>&linkCode=ur2&tag=generalstore0ac-20"><?=$row['keyword_one'] ?></a>-->
                        
                        <?php
						$musiteids = "SELECT group_concat(id) as ids FROM hub WHERE (user_id = '2996' AND multi_user = '1' AND cid = '99999' AND trashed = '0000-00-00')
OR (user_id = '6312' AND multi_user = '1' AND cid = '99999' AND trashed = '0000-00-00')
OR (user_id = '100967' AND multi_user = '1' AND cid = '99999' AND trashed = '0000-00-00')";

						$muids = $dir->fetchColumn($musiteids);    
						$mucitylist = "SELECT domain, company_name FROM hub WHERE city = '".$row['city']."' AND state = '".$row['state']."' AND domain != '' AND
category LIKE '%".$backlinkCategory."%' AND hub_parent_id IN (".$muids.") AND hub.trashed = '0000-00-00' group by
hub_parent_id";
						        //'.$row['keyword_one'].'
								 $mucities = $dir->query($mucitylist);
                        if($mucities) echo '<br><br><small>Find More '.$row['city'].' '.$dir->getCatLink($row['city'], $row['state'], $category, 'local', $row['company_name']).' Companies';
								   while($cityview = $dir->fetchArray($mucities)){
									  echo ' :: <a href="'.$cityview['domain'].'">'.$cityview['company_name'].'</a>';
									}
                        if($mucities) echo '</small>';

						
						?>
                        
                        </div>
                        </div>
                        <br />
                        
                        <!--<div class="alignleft" style="padding-bottom:25px">
     <script type="text/javascript">
       	amzn_assoc_ad_type = "banner";
	amzn_assoc_marketplace = "amazon";
	amzn_assoc_region = "US";
	amzn_assoc_placement = "assoc_banner_placement_default";
	amzn_assoc_banner_type = "ez";
	amzn_assoc_p = "13";
	amzn_assoc_width = "620";
	amzn_assoc_height = "60";
	amzn_assoc_tracking_id = "refurbishedne-20";
	amzn_assoc_linkid = "837d7f4f6ec92d01a251ad42cf45d76f";
     </script>
     <script src="//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1"></script>
    </div>-->
		<div >
			<!--Start Blogs Container Blogs Container Bottom-->
			<h1><?=$row['company_name']?></h1> 			
            <img src="<?=$icon?>" class="bloglistimg" alt="<?=$row['company_name']?> | <?=$row['keyword_one']?>" width="200px" /> 
            <h2><?=$dir->formatPhone($row['phone'])?></h2>
            <p><?=$row['street']?><br/>
			   <?=$row['city'].', '.$row['state'].' '.$row['zip']?>
               
                <div class="socialList">

							<?php if($row['twitter']){ ?>
							<a <a href="<?=$row['twitter']?>" id="twitter" title="<?=$row['company_name']?> Twitter Spot | <?=$row['keyword_one']?>" target="_blank" ><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-05.png" width="32" height="32" alt="<?=$row['company_name']?> Twitter Spot | <?=$row['keyword_one']?>" /></a>
                            <?php } ?>
                            <?php if($row['facebook']){ ?>
							<a href="<?=$row['facebook']?>" id="facebook" title="<?=$row['company_name']?> Facebook Spot | <?=$row['keyword_two']?>" target="_blank"><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-14.png" width="32" height="32" alt="<?=$row['company_name']?> Facebook Spot | <?=$row['keyword_two']?>" /></a>
                            <?php } ?>
                            <?php if($row['youtube']){ ?>
							<a href="<?=$row['youtube']?>" id="youtube" title="<?=$row['company_name']?> YouTube Spot | <?=$row['keyword_three']?>" target="_blank"><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-18.png" width="32" height="32" alt="<?=$row['company_name']?> YouTube Spot | <?=$row['keyword_three']?>" /></a>
                            <?php } ?>
                            <?php if($row['myspace']){ ?>
							<a href="<?=$row['myspace']?>" id="myspace" title="<?=$row['company_name']?> MySpace Spot | <?=$row['keyword_four']?>" target="_blank"><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-04.png" width="32" height="32" alt="<?=$row['company_name']?> MySpace Spot | <?=$row['keyword_four']?>" /></a>
                            <?php } ?>
                            <?php if($row['linkedin']){ ?>
							<a href="<?=$row['linkedin']?>" id="linkedin" title="<?=$row['company_name']?> LinkedIn Spot | <?=$row['keyword_six']?>" target="_blank"><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-34.png" width="32" height="32" alt="<?=$row['company_name']?> LinkedIn Spot | <?=$row['keyword_six']?>" /></a>
                            <?php } ?>
                            <?php if($row['technorati']){ ?>
							<a href="<?=$row['technorati']?>" id="technorati" title="<?=$row['company_name']?> Technorati Spot | <?=$row['keyword_five']?>" target="_blank"><img src="http://hubs.6qube.com/themes/featured/images/icons/social/social_balloon-16.png" width="32" height="32" alt="<?=$row['company_name']?> Technorati Spot | <?=$row['keyword_five']?>" /></a>
                            <?php } ?>

                        </div>   
               </p> 
              
            <h4><a title="<?=$row['company_name']?>" href="<?=$row['website_url']?>">Visit Website</a></h4>
            <blockquote>
            <?=$dir->shortenSummary($row['display_info'], 250)?>
            </blockquote>
            <br /> 
            
                      
		</div>

		<div class="clear"></div>
<br />
       
	</div>
     <div class="gradient-up-with-border">
        <div class="full-width pt20 pb20">
        <? if($row['pic_one']){?>
        <?php
		$dir->displayDirectoryPhotos($row);
		?>
        <div class="clear"></div>

        <?php } ?>
        <div class="two-third">
        <h2><?=$row['company_name'] ?> Overview</h2>

             
              <p><? if($row['company_spot']){
					echo $row['company_spot'];
				} else {
					echo ''.$row['company_name'].' has not added a detailed overview yet about '.$dir->getCatLink($row['city'], $row['state'], $category, 'local', $row['company_name']).' in '.$row['city'].', '.$row['state'].'.';
				}
				?> </p>
                
                
        
        </div>
        <div class="one-third">
       
<!--<div class="alignleft" style="padding-bottom:25px">
     <script type="text/javascript">
       	amzn_assoc_ad_type = "banner";
	amzn_assoc_marketplace = "amazon";
	amzn_assoc_region = "US";
	amzn_assoc_placement = "assoc_banner_placement_default";
	amzn_assoc_campaigns = "holsetforget2016";
	amzn_assoc_banner_type = "setandforget";
	amzn_assoc_p = "12";
	amzn_assoc_width = "300";
	amzn_assoc_height = "250";
	amzn_assoc_tracking_id = "generalstore0ac-20";
	amzn_assoc_linkid = "aac6c00d00e0612c2fc6966dc9505876";
     </script>
     <script src="//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1"></script>
    </div>-->

        <!--<h2 style="text-align:center;">Our Websites</h2>
			<div class="outer-rounded-box">
				<div class="inner-rounded-box-filled-grey clearfix">
					
							<div id="hub_display">
                            <?php
							// display the 4 newest HUBs
							require_once(QUBEADMIN . 'inc/hub.class.php');
							$hub = new Hub();
							$hub->displayHubs($rightLimit, $offset, 3, $row['user_id'], NULL, NULL, NULL, $row['cid']);
							?>
                            </div>
				
				</div>
			</div>
            <br />
            
            <h2 style="text-align:center;">Our Press Releases</h2>
			<div class="outer-rounded-box">
				<div class="inner-rounded-box-filled-grey clearfix">
					
							<div id="hub_display">
                           <?php
							// display the 4 newest Press Release titles (shortened)
							require_once(QUBEADMIN . 'inc/press.class.php');
							$press = new Press();
							$press->displayPress($rightLimit, $offset, NULL, $row['user_id'], NULL, NULL, NULL, $row['cid']);
							?>
                            </div>
				
				</div>
			</div>
            <br />
            
             <h2 style="text-align:center;">Our Blog Posts</h2>
			<div class="outer-rounded-box">
				<div class="inner-rounded-box-filled-grey clearfix">
					
							<div id="hub_display">
                          <?php
						// display the 4 newest Blog post titles (shortened)
						require_once(QUBEADMIN . 'inc/blogs.class.php');
						$blog = new Blog();
						$blog->displayPost($rightLimit, $offset, NULL, NULL, NULL, 
									    NULL, NULL, NULL, $row['user_id'], $row['cid']);
						?>
                            </div>
				
				</div>
			</div>
            <br />
            
            <h2 style="text-align:center;">Our Blog Posts</h2>
			<div class="outer-rounded-box">
				<div class="inner-rounded-box-filled-grey clearfix">
					
							<div id="hub_display">
                         <?php
						// display the 4 newest Article titles (shortened)
						require_once(QUBEADMIN . 'inc/articles.class.php');
						$articles = new Articles();
						$articles->displayArticles($rightLimit, $offset, NULL, $row['user_id'],
											  NULL, NULL, NULL, $row['cid']);
						?>
                            </div>
				
				</div>
			</div>
            <br />-->

            
            
		</div>
        <div class="clear"></div>
         <br /><br />
                        <div class="outer-rounded-box-bold">
                        <div class="simple-rounded-box">
						You Are Here: <a title="Local Business Network | Local Yellow Pages Directory" href="http://local.6qube.com">Local</a> | <?=$dir->getCityLink($row['city'], $row['state'], 'local', NULL, true, $row['city'].' yellow pages | 6Qube Local Directory')?> | <?=$dir->getCatLink($row['city'], $row['state'], $category, 'local', $row['company_name'].' | '.$row['keyword_one'])?> | <?=$row['company_name'] ?>
                        </div>
                        </div>
                        
        </div>
        </div>
        <div class="clear"></div>
        
       
</div>
</div>
<!-- end middle -->
<?
	//COMMON FOOTER
	include('inc/footer.php');
?>
<?
	}
	else{ //if no directories
		echo 'This Directory Not Found.';
	}
}
?>