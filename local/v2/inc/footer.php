<!--FOOTER SECTION-->
	<footer id="colophon" class="site-footer clearfix">
		<div id="quaternary" class="sidebar-container " role="complementary">
			<div class="sidebar-inner">
				<div class="widget-area clearfix">
					<div id="azh_widget-2" class="widget widget_azh_widget">
						<div data-section="section">
							<div class="container">
								<div class="row">
									<div class="col-sm-4 col-md-3 foot-logo"> <img src="https://6qube.com/blogs/themes/6qube2/images/6qube-logo.png" alt="logo">
										<p class="hasimg">6Qube - Local Business Network</p>
										<p class="hasimg"></p>
									</div>
									

									
									
									<div class="col-sm-4 col-md-4">
										<h4>Popular Services</h4>
										<ul class="two-columns">
											<?php foreach($topcatories as $topcat){ 
											$a = explode("->", $topcat['category']);
											$topcat['category'] = $a[1];
											$topcat['category'] = ucwords($topcat['category']);
										?>
										<li><a href="#"><?=$topcat['category']?></a></li>
										<?php } ?>
										<li><a href="/category/"> More Categories...</a> </li>
										</ul>
									</div>
									<div class="col-sm-4 col-md-4">
										<h4>Popular Cities</h4>
										<ul class="two-columns">
											<?php foreach($topcities as $topcity){ ?>
										<li><a href="<?=$v2->getCityLink($topcity['city'], $topcity['state']);?>"><?=$topcity['city']?></a></li>
										<?php } ?>
										<li><a href="/cities/"> More Cities...</a> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div data-section="section" class="foot-sec2">
							<div class="container">
								<div class="row">
									
									<div class="col-sm-5 foot-social">
										<h4>Follow us</h4>
										<ul>
											<li><a href="https://www.facebook.com/6qube"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
											<li><a href="https://twitter.com/6qube"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
											<li><a href="https://www.linkedin.com/company/6qube/"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .widget-area -->
			</div>
			<!-- .sidebar-inner -->
		</div>
		<!-- #quaternary -->
	</footer>
	<!--COPY RIGHTS-->
	<section class="copy">
		<div class="container">
			<p>copyrights &copy; <?php echo date("Y"); ?> 6Qube Local Business Network. &nbsp;&nbsp;All rights reserved. </p>
		</div>
	</section>
	
<a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
	<!--SCRIPT FILES-->
	<script src="/v2/js/jquery.min.js"></script>
	<script src="/v2/js/bootstrap.js" type="text/javascript"></script>
	<script src="/v2/js/materialize.min.js" type="text/javascript"></script>
	<script src="/v2/js/custom.js"></script>
</body>
</html>