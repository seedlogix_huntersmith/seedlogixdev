<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
	<title><?=$title?></title>
	<meta content="<?=$meta_desc?>" name="description" />
	<meta content="INDEX,FOLLOW" name="Robots" />
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
<link rel="apple-touch-icon" sizes="72x72" href="https://6qube.com/blogs/themes/6qube2/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://6qube.com/blogs/themes/6qube2/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://6qube.com/blogs/themes/6qube2/images/favicon-16x16.png">
<link rel="manifest" href="https://6qube.com/blogs/themes/6qube2/images/site.webmanifest">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#ffffff">
	
	<meta name="google-site-verification" content="Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y" />

	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="/v2/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="/v2/css/materialize.css" rel="stylesheet">
	<link href="/v2/css/style.css" rel="stylesheet">
	<link href="/v2/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="/v2/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="/v2/js/html5shiv.js"></script>
	<script src="/v2/js/respond.min.js"></script>
	<![endif]-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34745846-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34745846-3');
</script>

 <?php
switch ($category) {
    case "Fire Restoration":
        echo "<style>.dir-alp { background: #BBC0C6 url('/v2/images/catbg/fire-restoration.jpg') no-repeat center top !important;background-size: cover!important;background-attachment: fixed!important;}</style>";
        break;
	case "Cleaning Services":
        echo "<style>.dir-alp { background: #BBC0C6 url('/v2/images/catbg/cleaning-services.jpg') no-repeat center top !important;background-size: cover!important;background-attachment: fixed!important;}</style>";
        break;
    default:
        echo "";
}
?> 	
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	<section class="bottomMenu dir-il-top-fix">
		<div class="container top-search-main">
			<div class="row">
				<div class="ts-menu">
					<!--SECTION: LOGO-->
					<div class="ts-menu-1">
						<a href="/"><img src="/v2/images/box-logo.jpg" alt=""> </a>
					</div>
					<!--SECTION: BROWSE CATEGORY(NOTE:IT'S HIDE ON MOBILE & TABLET VIEW)-->
					<div class="ts-menu-2"><a href="#" class="t-bb">Explore <i class="fa fa-angle-down" aria-hidden="true"></i></a>
						<!--SECTION: BROWSE CATEGORY-->
						<div class="cat-menu cat-menu-1">
							<div class="dz-menu">
								<div class="dz-menu-inn">
									<h4>Top Cities</h4>
									<ul>
										<?php foreach($topcities as $topcity){ ?>
										<li><a href="<?=$v2->getCityLink($topcity['city'], $topcity['state']);?>"><?=$topcity['city']?></a></li>
										<?php } ?>
										<li><a href="/cities/"> More Cities...</a> </li>
									</ul>
								</div>
								<div class="dz-menu-inn">
									<h4>Top Categories</h4>
									<ul>
										<?php foreach($topcatories as $topcat){ 
											$a = explode("->", $topcat['category']);
											$topcat['category'] = $a[1];
										?>
										<li><a href="/category/<?=$dir->convertKeywordUrl($topcat['category'])?>"><?=$topcat['category']?></a></li>
										<?php } ?>
										<li><a href="/category/"> More Categories...</a> </li>
									</ul>
								</div>
								<div class="dz-menu-inn-long">
									<h4>Top Blog Posts</h4>
									<ul>
										<?php foreach($topposts as $toppost){ ?>
										<li><a href="https://<?=$toppost['httphostkey']?>/<?=$toppost['category_url']?>/<?=$toppost['post_title_url']?>/"><?=$toppost['post_title']?></a></li>
										<?php } ?>
										<li><a href="https://6qube.com"> More Posts...</a> </li>
										
									</ul>
								</div>
							
								
							</div>
							
						</div>
					</div>
					<!--SECTION: SEARCH BOX-->
					<div class="ts-menu-3">
						<div class="">
							<form class="tourz-search-form tourz-top-search-form" method="post" action="/search/">
								<div class="input-field">
									<input type="text" id="top-select-city" name="citysearch">
									<label for="top-select-city">Enter city</label>
								</div>
								<div class="input-field">
									<input type="text" id="top-select-search" name="categorysearch">
									<label for="top-select-search" class="search-hotel-type">Search your services like hotel, resorts, events and more</label>
								</div>
								<div class="input-field">
									 <button type="submit" name="action" class="waves-effect waves-light tourz-top-sear-btn"></button> </div>
							</form>
						</div>
					</div>
					<!--SECTION: REGISTER,SIGNIN AND ADD YOUR BUSINESS-->
					<div class="ts-menu-4">
						<div class="v3-top-ri">
							<ul>
								<li><a href="http://6qube.com/admin/" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
								<li><a href="http://6qubedirectory.com" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li>
							</ul>
						</div>
					</div>
					<!--MOBILE MENU ICON:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="ts-menu-5"><span><i class="fa fa-bars" aria-hidden="true"></i></span> </div>
					<!--MOBILE MENU CONTAINER:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="mob-right-nav" data-wow-duration="0.5s">
						<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
						<h5>Business</h5>
						<ul class="mob-menu-icon">
							<li><a href="http://6qubedirectory.com">Add Business</a> </li>
							<li><a href="http://6qubedirectory.com">Register</a> </li>
							<li><a href="lhttp://6qube.com/admin/">Sign In</a> </li>
						</ul>
						<h5>Top Categories</h5>
						<ul>
							<?php foreach($topcatories as $topcat){ 
											$a = explode("->", $topcat['category']);
											$topcat['category'] = $a[1];
										?>
										<li><a href="/category/<?=$dir->convertKeywordUrl($topcat['category'])?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?=$topcat['category']?></a></li>
										<?php } ?>
										<li><a href="/category/"><i class="fa fa-angle-right" aria-hidden="true"></i> More Categories...</a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>