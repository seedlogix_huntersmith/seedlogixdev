$(function() {
  $('footer a').tooltip({html: true});
  
  $('.variation a').click(function(){
    var href= $(this).attr('href');
    if (href.indexOf('gmap') != -1) {
      gmap = href.match(/gmap=(.*)/)[1];

      try {
        _gaq.push(['_trackEvent', 'change-map', gmap]); 
      } catch(err){}
 
      setTimeout(function() {
        document.location.href = href;
      }, 100);
    } // if switch link
  });
});