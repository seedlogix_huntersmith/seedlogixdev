<?php
require_once QUBEADMIN . '6qube/core.php';
require_once QUBEADMIN . 'inc/local.class.php';
class Version2 extends Local{
	
	
	/**********************************************************************/
	// function: Display Cities
	// displays cities for Local pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesLocal($limit = '', $setCat = '', $state = ''){

		
		$query = "SELECT id, COUNT(type), city, state FROM directory";
		$query .= " WHERE `trashed` = '0000-00-00'";
		$query .= " AND `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `city` NOT REGEXP '[0-9]'
				AND `city` NOT LIKE '%,%'
				AND `city` NOT LIKE '% - %'
				AND `city` NOT LIKE '%(%'
				AND `state` != ''
				AND `state` != '0'
				AND `state` != 'US'
				AND `state` != 'CN'
				 AND `state` != 'AB'
				 AND `state` != 'BC'
				 AND `state` != 'MB'
				 AND `state` != 'NB'
				 AND `state` != 'NF'
				 AND `state` != 'NT'
				 AND `state` != 'NS'
				 AND `state` != 'NU'
				 AND `state` != 'ON'
				 AND `state` != 'PE'
				 AND `state` != 'QC'
				 AND `state` != 'SK'
				 AND `state` != 'YT'
				AND `state` != 'AU'
				AND `state` != 'ACT'
				AND `state` != 'NSW'
				AND `state` != 'NTT'
				AND `state` != 'QLD'
				AND `state` != 'SA'
				AND `state` != 'TAS'
				AND `state` != 'VIC'
				AND `state` != 'WAT'
				AND `state` != 'VI'
				AND `category` != '0'
				AND `category` != ''
				AND `website_url` != ''
				AND `photo` != ''
				AND `display_info` != ''";	
		if($setCat) $query .= " AND category LIKE '%".$setCat."%'";
		if($state) $query .= " AND state = '".$state."'";
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		 
		$result = $this->query($query);
		
		$rows	=	array();
		while($row = $this->fetchArray($result)){
			$rows[$row['id']]	=	$row;
		}
		
		return $rows;


		 
	}	
	function displayAllCities(){
		$query = "SELECT id, city, state 
				FROM directory
				WHERE trashed = '0000-00-00'
				AND company_name != ''
				AND phone != ''
				AND street != ''
				AND city != ''
				AND `city` NOT REGEXP '[0-9]'
				AND `city` NOT LIKE '%,%'
				AND `city` NOT LIKE '% - %'
				AND `city` NOT LIKE '%(%'
				AND state != ''
				AND state != '0'
				AND `state` != 'US'
				AND `state` != 'CN'
				 AND `state` != 'AB'
				 AND `state` != 'BC'
				 AND `state` != 'MB'
				 AND `state` != 'NB'
				 AND `state` != 'NF'
				 AND `state` != 'NT'
				 AND `state` != 'NS'
				 AND `state` != 'NU'
				 AND `state` != 'ON'
				 AND `state` != 'PE'
				 AND `state` != 'QC'
				 AND `state` != 'SK'
				 AND `state` != 'YT'
				AND `state` != 'AU'
				AND `state` != 'ACT'
				AND `state` != 'NSW'
				AND `state` != 'NTT'
				AND `state` != 'QLD'
				AND `state` != 'SA'
				AND `state` != 'TAS'
				AND `state` != 'VIC'
				AND `state` != 'WAT'
				AND `state` != 'VI'
				AND category != '0'
				AND category != ''
				AND website_url != ''
				AND photo != ''
				AND display_info != ''
                ORDER BY state asc, city asc";
		$result = $this->query($query);
		while($row = $this->fetchArray($result)){
			
			$rows[$row['id']]	=	$row;
			
		}
		
		/*while($row = $this->fetchArray($result)){
			$states[$row['state']][$row['city']]	=	$row;
		}*/
		
		
		return $rows;
		
	}
	function getGeoData($city, $state){
		$query = "SELECT * FROM `geo_data` WHERE `city` LIKE '".$city."' AND `state_id` LIKE '".$state."'";
		$result = $this->query($query);
		
		$row = $this->fetchArray($result);
		
		return $row;
	}
	
	function displayNetworkSites($baseid, $city, $state){

		
		$query = "SELECT httphostkey, ssl_on, category, company_name, phone, street, city, state, zip, logo FROM hub";
		$query .= " WHERE `trashed` = '0000-00-00'";
		$query .= " AND `company_name` != ''
				AND `phone` != ''
				AND `logo` != ''
				AND `category` != ''
				AND `city` = '".$city."'
				AND `state` = '".$state."'
				AND httphostkey NOT LIKE '%alibipartners.com%'
				AND `hub_parent_id` = '".$baseid."'";	
		 
		$result = $this->query($query);
		
		$rows	=	array();
		while($row = $this->fetchArray($result)){
			$rows[$row['httphostkey']]	=	$row;
		}
		
		return $rows;


		 
	}
		/**********************************************************************/
	// function: Display Categories
	// displays categories for Local pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesLocal($limit = '', $city, $state, $setCat = ''){

		
		$query = "SELECT id, COUNT(type), category, city, state FROM directory";
		$query .= " WHERE `trashed` = '0000-00-00'";
		$query .= " AND `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `city` NOT REGEXP '[0-9]'
				AND `city` NOT LIKE '%,%'
				AND `city` NOT LIKE '% - %'
				AND `city` NOT LIKE '%(%'
				AND `state` != ''
				AND `state` != '0'
				AND `state` != 'US'
				AND `state` != 'CN'
				 AND `state` != 'AB'
				 AND `state` != 'BC'
				 AND `state` != 'MB'
				 AND `state` != 'NB'
				 AND `state` != 'NF'
				 AND `state` != 'NT'
				 AND `state` != 'NS'
				 AND `state` != 'NU'
				 AND `state` != 'ON'
				 AND `state` != 'PE'
				 AND `state` != 'QC'
				 AND `state` != 'SK'
				 AND `state` != 'YT'
				AND `state` != 'AU'
				AND `state` != 'ACT'
				AND `state` != 'NSW'
				AND `state` != 'NTT'
				AND `state` != 'QLD'
				AND `state` != 'SA'
				AND `state` != 'TAS'
				AND `state` != 'VIC'
				AND `state` != 'WAT'
				AND `state` != 'VI'
				AND `category` != '0'
				AND `category` != ''
				AND `website_url` != ''
				AND `display_info` != ''
				AND `photo` != ''";
		if($city && $state) $query .= " AND city = '".$city."'";
		if($city && $state) $query .= " AND state = '".$state."'";
		if($setCat) $query .= " AND category LIKE '%".$setCat."%'";
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		$result = $this->query($query);
		
		$rows	=	array();
		while($row = $this->fetchArray($result)){
			$rows[$row['id']]	=	$row;
		}
		
		return $rows;

	}
	
		/**********************************************************************/
	// function: Get City Link
	// returns html for a link to a city's page in a network
	//
	// Accepted Input: city, state, network, major city bold, include state for major
	/**********************************************************************/
	function getCityLink($city, $state){ //
		///////////////////////////////////////////////////////////////////////
		$city = ucwords($city); //capitalize first letter(s) if not already
		if($major = $this->majorCity($city)){
			if($major == strtoupper($state)){
				if($catall){
					$link = '/'.$this->convertKeywordUrl($city, true).'/';	
				} else {
					$link = '/'.$this->convertKeywordUrl($city, true).'/';
				}
			}
			else {
				if($catall){
					$link = '/'.$this->convertKeywordUrl($city, true).
					  '-'.strtolower($state).'/';	
				} else {
					$link = '/'.$this->convertKeywordUrl($city, true).
						  '-'.strtolower($state).'/';
				}
			}
		}
		else {
			if($catall){
				$link = '/'.$this->convertKeywordUrl($city, true).
				  '-'.strtolower($state).'/';	
			} else {
				$link = '/'.$this->convertKeywordUrl($city,true).'-'.
						strtolower($state).'/';
			}
		}
			
		return $link;
	}
	
	/**********************************************************************/
	// function: Display Blog Post (NEW)
	// outputs a set of Blog Post results in new format
	//
	// Accepted Input: Query Limit and Query Offset
	/**********************************************************************/	 
	function displayPost($limit = '', $count, $blogid){

			if($blogid)	{
				
				$query = "SELECT 
						p.id, 
						p.blog_id, 
						p.tracking_id, 
						p.user_id, 
						p.category, 
						p.category_url, 
						p.post_title, 
						p.post_title_url, 
						p.post_content, 
						p.publish_date, 
						p.photo, 
						p.tags, 
						p.tags_url, 
						p.network, p.created,
						b.httphostkey
					FROM 
						blog_post p, blogs b
					WHERE 
						 p.master_ID = 0
						 AND p.category != '' 
						 AND p.post_title != '' 
						 AND p.post_content != '' 
						 AND p.blog_id = '".$blogid."'
						 AND p.trashed = '0000-00-00' 
						 AND p.user_id = '7'
						 AND p.blog_id = b.id
						 AND p.user_id = b.user_id
					ORDER BY 
						p.publish_date DESC";
				if($limit) $query .= " LIMIT ".$limit;
				
			} else {
			//Query DB for all data matching specific criteria
			$query = "SELECT 
						p.id, 
						p.blog_id, 
						p.tracking_id, 
						p.user_id, 
						p.category, 
						p.category_url, 
						p.post_title, 
						p.post_title_url, 
						p.post_content, 
						p.photo, 
						p.tags, 
						p.tags_url, 
						p.network, p.created,
						b.httphostkey
					FROM 
						blog_post p, blogs b
					WHERE 
						 p.master_ID = 0
						 AND p.category != '' 
						 AND p.post_title != '' 
						 AND p.post_content != '' 
						 AND p.blog_id != 0
						 AND p.trashed = '0000-00-00' 
						 AND p.publish_date <= CURDATE()
						 AND p.user_id = '7'
						 AND p.blog_id = b.id
						 AND p.user_id = b.user_id
					ORDER BY 
						p.publish_date DESC";
				if($limit) $query .= " LIMIT ".$limit;
				///AND p.publish_date BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE()
				
			}
				$result = $this->query($query); //query the DB

					
					
					$rows	=	array();
					while($row = $this->fetchArray($result)){
						$rows[$row['id']]	=	$row;
					}

					return $rows;


		}	

		 /**********************************************************************/
	 // function: Display Directories (NEW)
	 // outputs a set of directory results in the new format
	 //
	 // Accepted Input: Query Limit and Query Offset
	 /**********************************************************************/	 
	 function displayDirectories($limit = '', $offset = '', $city = '', $state = '', $category = '', $count){

			if($count) $query = "SELECT 
									count(id)
								 FROM 
									`directory`"; 
			else $query = "SELECT id, 
					user_id, 
					company_name, 
					phone, 
					street, 
					city, 
					state, 
					zip, 
					category, 
					photo, 
					website_url, 
					display_info, 
					company_spot, 
					keyword_one, 
					keyword_one_link, 
					keyword_two, 
					keyword_two_link, 
					keyword_three, 
					keyword_three_link, 
					keyword_four, 
					keyword_four_link, 
					keyword_five, 
					keyword_five_link, 
					keyword_six, 
					keyword_six_link, 
					twitter ,
					facebook, 
					myspace, 
					youtube, 
					diigo, 
					technorati, 
					linkedin, 
					blog, 
					pic_one, 
					last_edit,
					created 
				FROM 
					`directory` "; 
			$query .= "WHERE `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `city` NOT REGEXP '[0-9]'
				AND `city` NOT LIKE '%,%'
				AND `city` NOT LIKE '% - %'
				AND `city` NOT LIKE '%(%'
				AND `state` != ''
				AND `state` != '0'
				AND `state` != 'US'
				AND `state` != 'CN'
				 AND `state` != 'AB'
				 AND `state` != 'BC'
				 AND `state` != 'MB'
				 AND `state` != 'NB'
				 AND `state` != 'NF'
				 AND `state` != 'NT'
				 AND `state` != 'NS'
				 AND `state` != 'NU'
				 AND `state` != 'ON'
				 AND `state` != 'PE'
				 AND `state` != 'QC'
				 AND `state` != 'SK'
				 AND `state` != 'YT'
				AND `state` != 'AU'
				AND `state` != 'ACT'
				AND `state` != 'NSW'
				AND `state` != 'NTT'
				AND `state` != 'QLD'
				AND `state` != 'SA'
				AND `state` != 'TAS'
				AND `state` != 'VIC'
				AND `state` != 'WAT'
				AND `state` != 'VI'
				AND `category` != ''
				AND `category` != '0' 
				AND `website_url` != ''
				AND `display_info` != ''
				AND `trashed` = '0000-00-00'
			";
			if($city) $query .= " AND city = '".$city."'";
			if($state) $query .= " AND state = '".$state."'";
			if($category) $query .= " AND category LIKE '%".$category."%'";
			if(!$count) $query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
		 
		 	$result = $this->query($query); //query the DB
		
			if($count){
					
				return  $this->fetchColumn($result); 

			} else {


				$rows	=	array();
				while($row = $this->fetchArray($result)){
					$rows[$row['id']]	=	$row;
				}

				return $rows;


			}


	}
	function displaySitemapDir($offset){

			$query = "SELECT id, 
					company_name,
					city, 
					state
				FROM 
					`directory`
				WHERE `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `city` NOT REGEXP '[0-9]'
				AND `city` NOT LIKE '%,%'
				AND `city` NOT LIKE '% - %'
				AND `city` NOT LIKE '%(%'
				AND `state` != ''
				AND `state` != '0'
				AND `state` != 'US'
				AND `state` != 'CN'
				 AND `state` != 'AB'
				 AND `state` != 'BC'
				 AND `state` != 'MB'
				 AND `state` != 'NB'
				 AND `state` != 'NF'
				 AND `state` != 'NT'
				 AND `state` != 'NS'
				 AND `state` != 'NU'
				 AND `state` != 'ON'
				 AND `state` != 'PE'
				 AND `state` != 'QC'
				 AND `state` != 'SK'
				 AND `state` != 'YT'
				AND `state` != 'AU'
				AND `state` != 'ACT'
				AND `state` != 'NSW'
				AND `state` != 'NTT'
				AND `state` != 'QLD'
				AND `state` != 'SA'
				AND `state` != 'TAS'
				AND `state` != 'VIC'
				AND `state` != 'WAT'
				AND `state` != 'VI'
				AND `category` != ''
				AND `category` != '0' 
				AND `website_url` != ''
				AND `display_info` != ''
				AND `trashed` = '0000-00-00'
				ORDER BY id ASC
				LIMIT 25000
			";
		
			if($offset) $query .= " OFFSET ".$offset;

		 
		 	$result = $this->query($query); //query the DB



				$rows	=	array();
				while($row = $this->fetchArray($result)){
					$rows[$row['id']]	=	$row;
				}

				return $rows;



	}
	
		/**********************************************************************/
	// function: Calculate and return percentage of directory listing completed
	// returns a number, 0 - 100 depending on level of directory listing completion
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function getListingCompletion($row){
		$percentComplete = 0;
		
		if($row['company_name'] && $row['phone'] && $row['street'] && $row['city'] && $row['state'] && $row['category'] && $row['website_url'] && $row['display_info'] && $row['keyword_one']){
			$percentComplete = 25;
			
			if($row['company_spot']){
				$percentComplete = 50;	
				
				if($row['pic_one']){
					$percentComplete = 75;
					
					if($row['keyword_one'] && $row['keyword_one_link'] && $row['keyword_two'] && $row['keyword_two_link'] && $row['keyword_three'] && $row['keyword_three_link'] && $row['keyword_four'] && $row['keyword_four_link'] && $row['keyword_five'] && $row['keyword_five_link'] && $row['keyword_six'] && $row['keyword_six_link']){
						$percentComplete = 95;
						
						if($row['twitter'] || $row['facebook'] || $row['myspace'] || $row['youtube'] || $row['diigo'] || $row['technorati'] || $row['linkedin'] || $row['blog']){
							$percentComplete = 100;
						}
						
					}
					
				}
				
			}
				
		}
		
		return $percentComplete;
	}
	
}
?>