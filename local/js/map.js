//////////////////////////////////////////////////////////////////////////

// HIDDEN MAP FUNCTION

//////////////////////////////////////////////////////////////////////////

	function loadHiddenMap() {
	
	// change this coordinates latitude,longitude - use this tool to get coordinates http://itouchmap.com/latlong.html
	geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(32.769843,-117.135542);
    var myOptions = {
		scrollwheel:false,
      zoom: 15,
      center: latlng,
    mapTypeControl: true,
   mapTypeControlOptions: {

			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,

        	position: google.maps.ControlPosition.RIGHT_CENTER },
    navigationControl: true,

		navigationControlOptions: {

			style: google.maps.NavigationControlStyle.SMALL,

        	position: google.maps.ControlPosition.LEFT_CENTER},

		mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("hidden_map"), myOptions);
	
    if (geocoder) {
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

            var infowindow = new google.maps.InfoWindow(
                { content: '<b>'+address+'</b>',
                  size: new google.maps.Size(150,50)
                });
    
            var marker = new google.maps.Marker({
                position: results[0].geometry.location,
                map: map, 
                title:address
            }); 
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });

          } else {
            alert("No results found");
          }
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
      });
    }

}