// JavaScript Document
$(document).ready(function() {
	//############################################
	// TEXT INPUT AUTO SUBMIT
	//############################################
	$(".jquery_form input, .jquery_form select").blur(function() {
		$elm = $(this);
		$elm.removeClass("error");
		$elm.removeClass("approved");
		$.getJSON("upload_form.php", { name: $(this).attr('name'), value: $(this).attr('value'), form: $('.jquery_form').attr('name') },
			function(data){
				if(data.error == 0) {
					$elm.find(' + p').html('');
					$elm.addClass("approved");
				}
				else {
					$elm.find(' + p').html(data.message);
					$elm.addClass("error");
				}
		});
	});
	//############################################
	// Newsletter
	//############################################
	$('#newsletter_submit').click(function(){
		$(this).parent().submit(); //submit the form
		$('#uploadIFrame').load(function(){ //get contents of iframe you submitted the form to
			JSONFile = "data = "+$(this).contents().text(); //grabbing contents
			eval(JSONFile);				
			if(data.result == 'true'){
				$('#newsletter').replaceWith('<p><strong style="color:#fff;">Success</strong><br />Your have joined the 6qube newsletter!</p>');
			}
		});
	});
	//############################################
	// Contact Form
	//############################################
	$('#request-more-info-submit_btn').click(function(){
		$("#consultation").submit(); //submit the form
		$('#uploadIFrame').load(function(){ //get contents of iframe you submitted the form to
			JSONFile = "data = "+$(this).contents().text(); //grabbing contents
			eval(JSONFile);				
			if(data.result == 'true'){
				$('#error_cnt').empty();
				$('#request-more-info-form').replaceWith('<p class="sms"><strong>Success</strong><br />Your information was recieved. We will contact you shortly!</p>');
			}
			else{
				$('#error_cnt').empty();
				$('#error_cnt').replaceWith('<div id="error_cnt">'+data.status+'</div>');
				$('input[title!=""]').hint();
			}
		});
	});
	//############################################
	// FILE INPUT AUTO SUBMIT
	//############################################
	$('.upload_form .test_file').change(function(){
		$elm = $(this).parent().attr('id');
		$(this).parent().submit();
		$('#uploadIFrame').load(function(){
			JSONFile = "data = "+$(this).contents().text();
			eval(JSONFile);
								
			if(data.result == 'true'){
				//$('.pic').remove();
				$("#"+$elm+" + img")
				.attr('src', data.file)
				.addClass('pic');
				//.insertAfter($elm);
			}
		});
	});
	//############################################
	// Browse Categories
	//############################################
	$('.autosubmit').change(function(){
		$('#browse').submit();
	});
//close $(document).ready(function()
});