<?php
	$addy = urldecode($_GET['addy']);
	$city = urldecode($_GET['city']);
	$state = urldecode($_GET['state']);
	$zip = urldecode($_GET['zip']);
	$logo = urldecode($_GET['logo']);
	$company = urldecode($_GET['company']);
	$width = urldecode($_GET['width']);
	$height = urldecode($_GET['height']);
	$phone = urldecode($_GET['phone']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>&FTAG_6QUBECONAME& Map & Directions </title>
<script type='text/javascript' src='http://www.google.com/jsapi?key=ABQIAAAAoPP3Ubl2HT3wEIEGZY37WhTpWj-3-2S2wmjUlN3iEmuBdvsYpRTY0svMpH73Ca_SHdqwq_y1mUK5Cw'></script>
<script type='text/javascript'>
var gmarkers = [];
function myclick(i){
	if(gmarkers[i]){
		GEvent.trigger(gmarkers[i], 'click');
	}
}
function importanceOrder(marker, b){
	return marker.importance * -1;
}
function createMarker(point, number, html){
	var icon = new GIcon(G_DEFAULT_ICON);
	if(number > 0 && number <= 10 && false){
		icon.image = 'http://www.6qubehub.com/6qube-hub/images/markericon_'+number+'.png';
	} else {
		icon.image = 'http://6qube.com/img/markericon.png';
	}
	icon.shadow = 'http://www.6qubehub.com/6qube-hub/images/shadow.png';
	icon.iconSize = new GSize(58.0, 78.0);
	icon.shadowSize = new GSize(58.0, 29.0);
	icon.iconAnchor = new GPoint(10.0, 29.0);
	icon.infoWindowAnchor = new GPoint(12.0, 14.0);
	var marker = new google.maps.Marker(point, {icon:icon, zIndexProcess:importanceOrder});
	GEvent.addListener(marker, 'click', function() { marker.openInfoWindowHtml(html); });
	gmarkers[number] = marker;
	return marker;
}
google.load('maps', '2.x');
function initialize(){
	var geo = new GClientGeocoder();
	var map = new google.maps.Map2(document.getElementById('map_canvas'));
	map.addControl(new GSmallMapControl());
	map.addControl(new GScaleControl());
	map.setCenter(new GLatLng(0,0),1);
	var bounds = new GLatLngBounds();
	geo.getLatLng('<?=$addy?> <?=$city?>, <?=$state?> <?=$zip?>', function(point){
		if(point){
			var marker = createMarker(point, 1, "<div class='viewMapInfo'><img src='<?=$logo?>' alt='<?=$company?>' title='<?=$company?>' border='0' /><br style='clear:both;' /><h1><?=$company?></h1><p><?=$phone?><br /><?=$addy?><br /><?=$city?>, <?=$state?>, <?=$zip?></p></div><br class='clear' />");
			marker.importance = 1;
			map.addOverlay(marker);
			bounds.extend(point);
			map.setZoom(map.getBoundsZoomLevel(bounds)-7);
			map.setCenter(bounds.getCenter());
		}
	});
}
google.setOnLoadCallback(initialize);</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
/**
* Google Map
*
* @section		detail
* @subsection	google-maps
*/
	
		.viewMapInfo
		{ padding: 10px; }
		
		.viewMapInfo img
		{ background: #FFF; background-position:center center; width:125px; height:93px; border: 1px solid #CCC; float: left; margin: 0 10px 10px 0; padding: 2px; }
		
		.viewMapInfo h1
		{ color: #000; font-size: 14px; }
		
		.viewMapInfo p
		{ font-size: 11px; }
		
		.viewMapInfo .googleRating img
		{ border: 0; margin: 0; float: none; }
		
		#map span { font-size: 8px; }
		
/**
* Google Map
*
* @section		detail
* @subsection	google-maps
*/
-->
-->
-->
</style></head>
<body onload="initialize()" onunload="GUnload()">
<div id="map_canvas" style="WIDTH: <?=$width?>px; HEIGHT: <?=$height?>px">&nbsp;</div>
</body>
</html>
