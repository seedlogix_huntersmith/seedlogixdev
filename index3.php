<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>6Qube Directory | Free Business Listing | Free Business Website | Free Business Tools </title>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAy-r1CaE6GQ0PT8bJtxJk8hQBwVSsGjV4Kb2VNM4d2d_Qe8idNhRZs1m1h7o6A4uBdr48ENclNJsgxQ"
    type="text/javascript">
  </script>
<script type="text/javascript">

    var map;
    var geocoder = null;
    var addressMarker;
    var addresses = [
    "259 Housefinch Loop, Liberty Hill, TX",
    "1906 Jojoba,Cedar Park, TX",
    "13740 Research Blvd., Suite D4, Austin, TX"];
    var numGeocoded = 0;
 
    function geocodeAll() {
      if (numGeocoded < addresses.length) {
        geocoder.getLocations(addresses[numGeocoded], addressResolved);
      }
    }
 
   function addressResolved(response) {
     var delay = 0;
     if (response.Status.code == 620) {
       // Too fast, try again, with a small pause
       delay = 500;
     } else {
       if (response.Status.code == 200) {
         place = response.Placemark[0];
         point = new GLatLng(place.Point.coordinates[1],
                             place.Point.coordinates[0]);
         marker = new GMarker(point);
         map.addOverlay(marker);
       }
       numGeocoded += 1;
     }
     window.setTimeout(geocodeAll, delay);
   }
 
    function load() {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(30.533285,-97.719269), 10);
 
        geocoder = new GClientGeocoder();
        geocoder.setCache(null);
        window.setTimeout(geocodeAll, 50);
      }
    }
 
    </script>


</head>
<body onload="load()" onunload="GUnload()">
<div id="map" style="width: 100%; height: 400px"></div>
</body>
</html>