<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//$row array is set in parent document inc/forms/edit_whole_network.php
	//this page is included if it's detected that a 'whole' type network is not setup yet
		
	require_once('inc/reseller.class.php');
	$reseller = new Reseller();
	
	if($_SESSION['reseller']){
?>
<h1>Reseller Network Setup:<br />
<?=$row['name']?></h1>
<br />
Enter the domain you would like to use as the network's main website below.<br />
<b>The network's sites will be set up as subdomains on this domain (<i>ie</i> http://local.mydomain.com)</b><br /><br />
<b>Note:</b> The domain must already be in our system for a Hub.<br />
If you're trying to set up this network on a new domain first create a Hub with the domain.<br /><br /><br />

<form class="jquery_form">
	<ul>
		<li>
			<label>Company/Display Name:</label>
			<div style="color:#888;font-size:12px;padding-bottom:5px;">Can be changed later</div>
			<input type="text" id="new-network-company" name="company" class="no-upload" value="" />
		</li>
		<li>
			<label>Domain Name:</label>
			<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>www.domain.com</i></div>
			<input type="text" id="new-network-domain" name="domain" class="no-upload" value="" /><br /><br />
		</li>
		<li>
			<input type="hidden" id="new-network-id" name="networkID" value="<?=$row['id']?>" />
			<input type="image" id="network-setup-sbmt" src="img/create-button/<?=$_SESSION['thm_buttons-clr']?>.png" style="width:116px;height:35px;background:none;border:none;margin-left:-10px;" />	
		</li>
	</ul>
</form><br />

<br style="clear:both;" />
<? } ?>