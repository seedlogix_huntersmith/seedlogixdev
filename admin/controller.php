<?php

require_once dirname(__FILE__) . '/../hybrid/bootstrap.php';//6qube/core.php';

$Clr    =   NULL;

if(!isset($_GET['ctrl']))
    $_GET['ctrl']      =   'Notifications';

if(isset($_GET['ctrl']))
{
    $controller         =   preg_replace('/[^a-zA-Z]+/', '', $_GET['ctrl']);
    $controller_path    =   QUBEPATH . '../classes/controllers/' . $controller . 'Admin.php';
    if(file_exists($controller_path))
    {
        $controller .=   'AdminController';
        require_once $controller_path;
        $Clr            =   new $controller;
    }
}else
{
    Qube::Fatal('Controller does not exist: ', $_GET, $_POST);
    exit;
}


//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once('./inc/auth.php');

try{
    $Clr->Execute($_GET['action'])->display();
}catch(Exception $e)
{
    Qube::Fatal($e);
}
