<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/hub.class.php');

//create new instance of class
$connector = new Hub();
$_SESSION['autoresponder_section'] = "hub";

//check limits
if($_SESSION['user_limits']['responder_limit']==-1)
	$respondersRemaining = "Unlimited";
else {
	//get number of directory autoresponders for this campaign
	$numResponders = $connector->getAutoResponders(NULL, 'hub', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
	$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
	if($respondersRemaining<0) $respondersRemaining = 0;
}
$emailsRemaining = $connector->emailsRemaining($_SESSION['user_id']);
if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
else $emailsRemaining = number_format($emailsRemaining);

$thisPage = "websiteauto";
include('./inc/forms/auto_nav.php');

//content
if($_GET['reseller_site']) echo '<h1>'.$_SESSION['main_site'].' Auto Responders</h1>';
else echo '<h1>Website Auto-Responders</h1>';
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<?php
if($id || $_GET['reseller_site']){
	include('./inc/forms/website_autoresponder.php');
}
else {
	echo $connector->userHubsDropDown($_SESSION['user_id'], 'website_autoresponder', $_SESSION['campaign_id'], 1);
?>
<br style="clear:both;" />
<div id="email-settings" style="width:100%;">
	<div id="ajax-select">
		<p>Use the drop-down above to select which hub you'd like to configure auto responders for.</p><br />
		<p>With this optional setting you can create scheduled auto-response emails to be sent to prospects<br />after they fill out the contact form on your hub.</p><br /><br />
		<p>You have <strong><?=$respondersRemaining?></strong> auto-responders remaining for this campaign.</p><br />
		<p>You have <strong><?=$emailsRemaining?></strong> outgoing emails remaining for this month.</p>
	</div>
</div>
<? } ?>

<br style="clear:both;" /><br />