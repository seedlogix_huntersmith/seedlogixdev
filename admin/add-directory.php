<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/directory.class.php');
	$directory = new Dir();
	
	$newDir = $directory->validateText($_POST['name'], 'Directory Name'); //validate directory name
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	$query = "SELECT class FROM users WHERE id = ".$_SESSION['user_id'];
	$row = $directory->queryFetch($query);
	
	if($directory->foundErrors()){ //check if there were validation errors
		//$error = $directory->listErrors('<br />'); //list the errors
	}
	else{ //if no errors
		$message = $directory->addNewDirectory($_SESSION['user_id'], $newDir, $_SESSION['campaign_id'], 
										$_SESSION['user_limits']['directory_limit'], NULL, $parent_id, NULL, $row['class']);
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_directory .container';
		$response['success']['message'] = $message;
	}
		
	echo json_encode($response);
?>