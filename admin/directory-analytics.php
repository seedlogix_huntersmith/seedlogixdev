<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/directory.class.php');
	
	//create new instance of class
	$connector = new Dir();
	
	//content
	echo '<h1>Analytics</h1>';
	
	echo $connector->userDirsDropDown($_SESSION['user_id'], 'directory_analytics_view'); //(user_id, selected, select name)
	
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop down box above to select which Directory's analytics you would like to view.</p>
</div>
