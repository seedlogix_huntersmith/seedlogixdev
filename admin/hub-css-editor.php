<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

if(!$_POST){
	////////////////////////////////////////////////
	//Display css settings/editing page			  //
	////////////////////////////////////////////////
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$query = "SELECT user_id, user_id_created, name, domain, custom_css_file, theme FROM hub WHERE id = '".$id."'";
	$hubInfo = $hub->queryFetch($query, NULL, 1);
	//get the css
	if($hubInfo['custom_css_file']){
		//determine which user id to use
		$uid = $hubInfo['user_id_created'] ? $hubInfo['user_id_created'] : $hubInfo['user_id']; 
		$basePath = QUBEROOT . 'hubs/domains/'.$uid.'/'.$id.'/';
		//existing custom file
		if(@is_file($basePath.'style.css')){
			$dispCSS .= @file_get_contents($basePath.'style.css');
		}
		else $dispCSS = '/* Couldn\'t load existing css file. CSS entered in this space will still be included. */';
	}
	else {
		$basePath = QUBEROOT . 'hubs/themes/';
		//get the theme's css file for customization
		$query = "SELECT path, css_file_path FROM themes WHERE id = '".$hubInfo['theme']."'";
		$themeInfo = $hub->queryFetch($query, NULL, 1);
		if($hubInfo['theme_color']){
			$themeCSSFile = str_replace('[color]', $themeInfo['color'], $themeInfo['css_file_path']);
		}
		else $themeCSSFile = $themeInfo['css_file_path'];
		if(@is_file($basePath.$themeInfo['path'].'/'.$themeCSSFile)){
			$dispCSS = 
'/* Customizing file '.$themeCSSFile.' */

'; //new line intentional
$dispCSS .= @file_get_contents($basePath.$themeInfo['path'].'/'.$themeCSSFile);
		}
		else $dispCSS = '/* Couldn\'t load existing css file. CSS entered in this space will still be included. */';
	}
	
	
?>
<script type="text/javascript">
$(function(){
	$('#submit-editor').die();
	$('#submit-editor').live('click', function(){
		var code = $('#css_code').val();
		var hid = '<?=$id?>';
		var params = { 'css_code': code, 'hub_id': hid, 'action': 'build' };
		$.post('hub-css-editor.php', params, function(data){
			if(data.success){
				$("#editor-notify").css('color', '#0C0').html('CSS file successfully updated!');
			}
			else {
				$("#editor-notify").css('color', '#C00').html('An error occured:<br />'+data.message);
			}
		}, 'json');
		return false;
	});
	
	$('#revert-css').die();
	$('#revert-css').live('click', function(){
		var hid = '<?=$id?>';
		var params = { 'hub_id': hid, 'action': 'revert' };
		$.post('hub-css-editor.php', params, function(data){
			if(data.success==1){
				$("#editor-notify").css('color', '#0C0').html('CSS file successfully reverted!');
			}
			else {
				$("#editor-notify").css('color', '#C00').html('An error occured:<br />'+data.message);
			}
		}, 'json');
		return false;
	});
});
</script>
<h1>Hub CSS File Editor</h1>
<a href="hub.php?id=<?=$id?>&form=theme"><img src="img/back-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a>

<h2><?=$hubInfo['name']?></h2>
<p>
Below is the current contents of the main CSS file for this hub.<br />
Make any changes you'd like, then click the Submit button at the bottom.
</p>
<br />
<div id="editor-notify" style="font-weight:bold;"></div>
<br />
<form class="jquery_form"><ul>
	<li>
		<label>CSS Code</label>
		<textarea id="css_code" rows="40" style="width:97%" class="no-upload"><?=$dispCSS?></textarea>
	</li>
</ul></form><br />
<a href="#" id="submit-editor"><img src="img/submit-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a><br /><br />

<? if($hubInfo['custom_css_file']){ ?>
<a href="<?=trim($hubInfo['domain'], '/')?>/style.css" target="_blank" class="dflt-link">View the current CSS file</a><br />
<a href="#" id="revert-css" class="dflt-link">Revert the CSS file to the last saved version</a><br />
<? } ?>

<br style="clear:both;" />
<?
} //end if(!$_POST)
else {
	////////////////////////////////////////////////
	// Sitemapping script if form has been posted //
	////////////////////////////////////////////////
	$id = is_numeric($_POST['hub_id']) ? $_POST['hub_id'] : 0;
	$uid = $_SESSION['user_id'];
	$result['success'] = 0;
	//make sure hub belongs to the user
	$h = $hub->queryFetch("SELECT user_id, user_id_created, custom_css_file FROM hub WHERE id = '".$id."'");
	if($h['user_id']==$uid){
		$uid = $h['user_id_created'] ? $h['user_id_created'] : $h['user_id']; //use user_id if user_id_created blank
		$base = QUBEROOT . 'hubs/domains/'.$uid.'/'.$id.'/';
		if($_POST['action']=='build'){
			$code = $_POST['css_code'];
			//delete the last revert file if it exists
			if(@is_file($base.'style_revert.css')) @unlink($base.'style_revert.css');
			//create revert file (if sitemap already exists)
			if(@is_file($base.'style.css')){
				@copy($base.'style.css', $base.'style_revert.css');
				@unlink($base.'style.css');
			}
			//now create the file
			if($f = @fopen($base.'style.css', 'w')){
				if(@fwrite($f, stripslashes($code))!==false){
					//Success!
					$result['success'] = 1;
					//update custom_css_file boolean if needed
					if(!$h['custom_css_file']){
						$query = "UPDATE hub SET custom_css_file = 1 WHERE id = '".$id."' LIMIT 1";
						$hub->query($query);
					}
				}
				else $result['message'] = 'Error writing to the file.';
				@fclose($f);
			}
			else $result['message'] = 'Error opening/creating the file.';
		} //end if($_POST['action']=='build')
		else if($_POST['action']=='revert'){
			//delete the current sitemap and restore the sitemap_revert.xml (if one exists)
			if(@is_file($base.'style_revert.css')){
				@unlink($base.'style.css');
				if(@copy($base.'style_revert.css', $base.'style.css')){
					@unlink($base.'style_revert.css');
					$result['success'] = 1;
				}
				else $result['message'] = 'Error restoring the previous file.  Please contact support for manual correction.';
			}
			else if(@is_file($base.'style.css')){
				//if css file was only edited once and they want to revert back to theme default
				@unlink($base.'style.css');
				$query = "UPDATE hub SET custom_css_file = 0 WHERE id = '".$id."' LIMIT 1";
				$hub->query($query);
				$result['success'] = 1;
			}
			else $result['message'] = 'No previous file to revert to.';
		}
		else $result['message'] = 'No action specified.';
	}
	else $result['message'] = 'Validation error.';
	
	echo json_encode($result);
}
?>