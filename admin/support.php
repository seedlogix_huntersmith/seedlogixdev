<? 
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	require_once('./inc/db_connector.php');
	$db = new DbConnector();
	
	if($_SESSION['theme'] && !$_SESSION['admin_regular']){
		if($_SESSION['reseller']) $id = $_SESSION['login_uid'];
		else $id = $_SESSION['parent_id'];
		
		$query = "SELECT `support-page` FROM resellers WHERE admin_user = ".$id;
		$result = $db->queryFetch($query);
		
		echo $result['support-page'];
	}
	else {
?>

<h1>6Qube Support</h1>
<p>6Qube Support Representatives are here to help.  If you experience any problems or just need someone to tell you how to do something, please use the contact information below.  </p>
<h2>Phone Support</h2>
<p>877-570-5005</p>
<h2>Phone Support Hours</h2>
<p>9AM to 5PM Monday through Friday</p>
<h2>Email Support</h2>
<p><a href="mailto:support@6qube.com" class="external dflt-link" target="_new" >support@6qube.com</a></p>
<? } ?>