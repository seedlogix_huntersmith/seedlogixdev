<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');

if($_SESSION['reseller'] || $_SESSION['admin']){
	$display = $_GET['display'] ? $_GET['display'] : 'current';
	$bold = 'style="font-weight:bold;"';
	echo 
	'<h1>User Communications</h1>';
	echo '<a href="reseller-emailers.php?display=current" class="dflt-link" ';
	if($display=='current') echo $bold;
	echo '>Current Emailers</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.
		'<a href="reseller-emailers.php?display=history" class="dflt-link"';
	if($display=='history') echo $bold;
	echo '>Sending History</a><br /><br />';
	if($display=='current') echo '<p>Here you can send or schedule emails to groups of users or all users.</p>';
	else echo '<p>Here you can view a history of emails that were sent to more than one user.</p>';
	
	if($display=='current')
		include(QUBEADMIN . 'inc/forms/reseller_emailers.php');
	else 
		include(QUBEADMIN . 'inc/forms/reseller_emailers_history.php');
}
?>