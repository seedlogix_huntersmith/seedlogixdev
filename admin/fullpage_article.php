<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/articles.class.php');
	$articles = new Articles();
	
	if($_SESSION['theme']) $previewUrl = 'http://articles.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://articles.6qube.com/';
	$articles_count = 0;
	//$articles_limit = $articles->getLimits($_SESSION['user_id'], "article");\
	$articles_limit = $_SESSION['user_limits']['article_limit'];
	
	$result = $articles->getArticles($_SESSION['user_id']); //primes Articles results, and is needed for getArticlesRows()
		if($articles_count = $articles->getArticlesRows()) { //checks to see if the user has any directories
			if($articles_limit==-1) $remaining = "Unlimited";
			else $remaining = $articles_limit - $articles_count;
			echo '<div id="dash"><div id="fullpage_article" class="dash-container"><div class="container">';
			while($row = $articles->fetchArray($result)){ //if so loop through and display links to each Articles
				$tracking = $row['tracking_id'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'articles.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="articles.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=articles&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$articles->ANAL_getGraphImg('article', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="articles.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Articles remaining.';
			if(!$_SESSION['theme']){
				if(($articles_limit-$articles_count)<=0 && $articles_limit!=-1) echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
		}
		else{ //if no Articles
			if($articles_limit==-1) $articles_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_article" class="dash-container"><div class="container">';

			if($articles_limit==0 && $articles_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Articles remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Article.  You have <strong>'.$articles_limit.'</strong> remaining.';

			echo '</div></div></div>';
		}
	
?>
