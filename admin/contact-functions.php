<?php
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

if(!$action = $_POST['action']) $action = $_GET['action'];

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

//initiate the class
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

if($action == "addNewContact"){
	$user_id = $_SESSION['user_id'];
	$cid = $_POST['cid'];
	$firstName = $reseller->validateText($_POST['first_name'], 'First Name');
	$lastName = $reseller->validateText($_POST['last_name'], 'Last Name');
	$phone = $reseller->validateNumber($_POST['phone'], 'Phone', true);
	$company = $reseller->validateText($_POST['company'], 'Company Name', true);
	$address = $reseller->validateText($_POST['address'], 'Address', true);
	$address2 = $reseller->validateText($_POST['address2'], 'Address 2', true);
	$city = $reseller->validateText($_POST['city'], 'City', true);
	$state = $reseller->validateText($_POST['state'], 'State', true);
	$zip = $reseller->validateNumber($_POST['zip'], 'Zip Code', true);
	$website = $reseller->validateText($_POST['website'], 'Website', true);
	$title = $reseller->validateText($_POST['title'], 'Title', true);
	$reports_to = $reseller->validateText($_POST['reports_to'], 'Reports To', true);
	
	if($reseller->foundErrors()){ //check to see if there were any errors
			$response['action'] = 'errors';
			$response['errors'] = $reseller->listErrors('<br />'); //if so then store the errors
		}
		
	else {
		$query = "INSERT INTO contacts 
				(user_id, cid, firstname, lastname, phone, company, address, address2, city, state, zip, website, title, reports_to, created) 
				VALUES 
				('".$user_id."', '".$cid."', '".$firstName."', '".$lastName."', '".$phone."', '".$company."', '".$address."', '".$address2."', '".$city."', '".$state."', 
				'".$zip."', '".$website."', '".$title."', '".$reports_to."', now())";
		$result = $reseller->query($query);
		
	}// end first else
	if(!$response['action']){
		$response['action'] = 'redir';
		$response['url'] = 'contacts.php';
	}
	echo json_encode($response);
} //end if($action == "addNewReseller" || $action == "addNewUser")
	
?>