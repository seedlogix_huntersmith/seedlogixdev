<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/blogs.class.php');
	$blog = new Blog();
	
	isset($_POST['title']) ? $title = $blog->sanitizeInput($_POST['title']) : $title = "";
	isset($_POST['blog_id']) ? $id = $_POST['blog_id'] : $id = "";
	isset($_POST['user_id']) ? $user_id = $_POST['user_id'] : $user_id = $_SESSION['user_id'];
	isset($_POST['category']) ? $category = $blog->sanitizeInput($_POST['category']) : $category = "";
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	if($title){
		$post = array('post_title'=>$title, 'category'=>$category);
		
		$post_id = $blog->addNewPost($id, $post, $user_id, $parent_id);
		
		if($post_id){
			$response['error'] = 0;
			$response['post_id'] = $post_id;
//			if($_POST['v2_cat']){
//				$newCatUrl = $blog->convertKeywordUrl($post['category'], 1);
//				$response['html'] = 
//				'<div class="item2">
//					<div class="icons">
//						<a href="#" id="delete-cat" rel="'.$newCatUrl.'" class="delete"><span>Delete</span></a>
//					</div>
//					<img src="img/category-button';
//				if($_SESSION['theme']) $response['html'] .= '/'.$_SESSION['thm_buttons-clr'];
//				$response['html'] .= '.png" class="blogPost" />
//					<a href="blog.php?form=add_blog_post2&id='.$id.'&cat='.$newCatUrl.'">'.$category.'</a>
//				</div>';
//			}
//			if($_POST['v2_post']){
//				$response['html'] = 
//				'<div class="item2">
//					<div class="icons">
//						<a href="blog.php?form=post2&id='.$id.'&postID='.$post_id.'" class="edit"><span>Edit</span></a>
//						<a href="#" id="delete-post" rel="'.$post_id.'" class="delete"><span>Delete</span></a>
//					</div>
//					<img src="img/category-button';
//				if($_SESSION['theme']) $response['html'] .= '/'.$_SESSION['thm_buttons-clr'];
//				$response['html'] .= '.png" class="blogPost" />
//					<a href="blog.php?form=add_blog_post2&id='.$id.'&postID='.$post_id.'">'.$title.'</a>
//				</div>';
//			}
		} else {
			$response['error'] = 1;
			$response['message'] = 'Sorry, there was an error creating your post.';
		}
	}

	echo json_encode($response);
?>