<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//get action to perform from either POST or GET
if(!$action = $_POST['action']) $action = $_GET['action'];

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
if($_SESSION['reseller'] || $_SESSION['admin'] || $_SESSION['user_manager']) $continue = true;
//initiate the class
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
if($continue){	
	if($action == "updateAllowedUsers"){
		$update = $_POST['checked'];
		$id = is_numeric($_POST['id']) ? $_POST['id'] : '';
		$usermanager = is_numeric($_POST['users_access']) ? $_POST['users_access'] : '';
		$updateField = "manage_user_id";
		$updateTable = "users";
		$userField = "parent_id";
		
		$allow = $update ? 1 : 0;
		
		if($usermanager && $id){
			//first get the colon-separated array from the table
			$query = "SELECT `".$updateField."` FROM `".$updateTable."` WHERE id = '".$id."' 
					AND `".$userField."` = '".$_SESSION['login_uid']."'";
			$a = $reseller->queryFetch($query);
			$removed = $false;
			//now go through and add or remove the class
			if($a[$updateField]){
				$allowed = explode('::', $a[$updateField]);
				foreach($allowed as $key=>$value){
					if($value == $usermanager){
						$allowed[$key] = NULL;
						$removed = true;
					}
				}
				if(!$removed) $allowed[] = $usermanager;
			}
			else $allowed = array($usermanager);
			
			//build string
			if($allowed){
				$string = '';
				foreach($allowed as $key=>$value){
					if($value) $string .= $value.'::';
				}
				//trim trailing '::'
				$string = substr($string, 0, -2);
			}
			
			//update table row
			$query = "UPDATE `".$updateTable."` SET `".$updateField."` = '".$string."' WHERE id = '".$id."' 
					AND `".$userField."` = '".$_SESSION['login_uid']."'";
			if($reseller->query($query) ){
				$response['action'] = 'success';
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'Error updating.';
		}
		
		echo json_encode($response);
	}

	//***************************************************************************
	//if manage user is trying to change users (become another)
	//***************************************************************************//
	else if($action == "manageUser"){
		$login_uid = $_SESSION['login_uid'];
		$new_id = $_POST['new_id'];
		
		//make sure user is either admin or parent_id of new user
		$continue = true;
		
		
		if($continue){
			$query = "SELECT id, username, class, reseller_client, sixqube_client FROM users WHERE id = '".$new_id."'";
			$row = $reseller->queryFetch($query);
			if($row){
				
				/* USER INFO TABLE */
				$userInfoQuery = "SELECT firstname, lastname FROM user_info WHERE user_id = '".$row['id']."'";
				$userInfoRow = $reseller->queryFetch($userInfoQuery);
				
				$_SESSION['user_fullName'] = $userInfoRow['firstname'].' '.$userInfoRow['lastname'];
				
				$_SESSION['user_id'] = $row['id'];
				$_SESSION['user'] = $row['username'];
				$_SESSION['user_class'] = $row['class'];
				$_SESSION['user_limits'] = $reseller->getLimits($row['id'], NULL, $row['class']);
				if($row['reseller_client']==1){
					$_SESSION['reseller_client'] = true;
					$_SESSION['parent_id'] = $reseller->getParentID($row['id']);
				}
				else $_SESSION['reseller_client'] = false;
				if($row['sixqube_client']==1){
					$_SESSION['sixqube_client'] = true;
					$_SESSION['parent_id'] = 7;
				}
				else $_SESSION['sixqube_client'] = false;
				
				$response['action'] = 'redir';
				$response['url'] = './';
			}
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'There was an error loading this user\'s information.  Refresh and try again or contact support for help.';
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'You are not authorized to manage this user.';
		}
		
		echo json_encode($response);
	} //end else if($action == "changeUser")

} //end if($continue)
?>