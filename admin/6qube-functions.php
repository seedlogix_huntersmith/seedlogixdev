<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');

//get action to perform from either POST or GET
if(!$action = $_POST['action']) $action = $_GET['action'];

if($action!='addNewBillingProfile' && $action!='updateBillingProfile'){
	//authorize Access to page
	require_once('inc/auth.php');
	$continue = true;
}
else {
	$salt = '4a0fBMMDDR(Cc,,c.bh07890';
	if($_POST['sec'] == md5($_POST['uid'].$salt)) $continue = true;
}

require_once(QUBEADMIN . 'inc/reseller.class.php');
require_once(QUBEADMIN . 'inc/Authnet.class.php');
$reseller = new Reseller();

//***************************************************************************
//if a user is trying to add a new billing profile to an ANet CIM
//***************************************************************************//
if($action == "addNewBillingProfile" || $action == "updateBillingProfile"){	
	$uid = ($_POST['uid'] && is_numeric($_POST['uid'])) ? $_POST['uid'] : '';
	//get info about user
	$query = "SELECT parent_id, billing_id, billing_id2, username, class FROM users WHERE id = '".$uid."'";
	$user = $reseller->queryFetch($query);
	$anet = new Authnet();
	//check for billing profile
	if(!$user['billing_id'] || $user['billing_id']<2000){
		if($user['billing_id'] && $user['billing_id']<2000){
			//echo 'debug: old billing profile found...<br />';
			//if user has an account in the old WHMCS system check to see if they have any products set up
			$postfields["action"] = "getclientsproducts";
			$postfields["clientid"] = "$uid";
			$results = $reseller->billAPI($postfields, 1);
			$xmlArray = $reseller->xml2array($results);
			if($xmlArray['whmcsapi']['totalresults']>0){
				//echo 'debug: products found...<br />';
				//user has products set up and should be manually switched over.  notify user and email 6Qube
				$to = 'admin@6qube.com';
				$from = 'admin@6qube.com';
				$subject = 'Attention Required: Manual 6Qube User Upgrade';
				$message = 
				'User #'.$uid.' recently tried to upgrade their account, but after checking with WHMCS it appears they may have products/services set up.<br /><br />
				You should <a href="https://6qube.com/billing/admin/">log in to WHMCS</a> and check the user\'s account status -- Their billing ID is #'.$user['billing_id'].'.  If they don\'t have any current services it\'s safe to delete their account in WHMCS and then email the user at <a href="mailto:'.$user['username'].'">'.$user['username'].'</a> to let them know they can now proceed with upgrading their account.';
				$reseller->sendMail($to, $subject, $message, NULL, $from, NULL, 1);
				$continue = false;
			}
			else {
				//echo 'debug: no products found, deleting account...<br />';
				//user has no products set up - delete account
				$postfields["action"] = "deleteclient";
				$postfields["clientid"] = "$uid";
				$results = $reseller->billAPI($postfields);
				//remove their billing ID from users table
				$query = "UPDATE users SET billing_id = 0 WHERE id = '".$uid."' LIMIT 1";
				$reseller->query($query);
				$continue = true;
				$bypass = 1;
			}
		} //end if($user['billing_id']<2000)
		else {
			$continue = true;
			$bypass = 0;
		}
		if($continue){
			//echo 'debug: attempting CIM profile setup...<br />';
			//if user is old, create a new profile for them under our CIM
			$newCIM = $anet->setupCIMProfile($uid, NULL, NULL, NULL, 1, $bypass);
			//var_dump($newCIM);
			$user['billing_id'] = $newCIM['profile_id'];
		}
	}
	else $continue = true;
	
	if($uid && $user['billing_id'] && $continue){
		//echo 'debug: successful, attempting payment profile setup...<br />';
		//set params to send to function
		//note CVV isn't sent -- it's just for placebo security on billing profile form
		$params = array('firstname' => $_POST['firstname'],
					 'lastname' => $_POST['lastname'],
					 'company' => $_POST['company'],
					 'address' => $_POST['address'],
					 'city' => $_POST['city'],
					 'state' => $_POST['state'],
					 'zip' => $_POST['zip'], 
					 'phone' => $_POST['phone'],
					 'cardnumber' => $_POST['cardnumber'],
					 'expmonth' => $_POST['expMonth'],
					 'expyear' => $_POST['expYear']);
		if($action=='addNewBillingProfile')
			$profile = $anet->setupPaymentProfile($uid, $user['billing_id'], $params, $apiLogin, $apiAuth, 1);
		else if($action=='updateBillingProfile')
			$profile = $anet->updatePaymentProfile($uid, $user['billing_id'], $user['billing_id2'], $params, $apiLogin, $apiAuth,1);
		
		if($profile['success'] && $action=='addNewBillingProfile'){
			echo '<h1>Billing Profile Successfully Created</h1>';
			//check to see if user has any items that should be charged right now
			if($charges = $reseller->chargesDueOnSetup($uid, 1)){
				$chargeAmount = $charges['totalAmount'];
				$monthly = $charges['monthlyCharge']['amount'];
				$pid = $charges['productID'];
				echo 'Attempting to bill you for $'.$chargeAmount.'...<br />';
				
				//attempt to charge new payment profile
				//no need to include rid, pid, etc because charge isn't logged
				$chargeUser = $anet->chargeCIM($uid, NULL, $pid, $chargeAmount, 'initial', 0, NULL, $apiLogin, $apiAuth);
				if($chargeUser['success']){
					//charge was successful, schedule next month's
					$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
					$nextSched = $reseller->scheduleTransaction(NULL, $uid, NULL, $pid, 'monthlySubscription', 
														$monthly, NULL, 'Scheduled monthly billing for '.
														date('F', strtotime($nextBillDate)), $nextBillDate, 1, 
														NULL, NULL, 1);
					
					//update the table rows
					$query = "UPDATE sixqube_billing 
							SET transaction_id = '".$chargeUser['message']."', method_identifier = '".$profile['payment_profile_id']."', success = 1, charge_on_setup = 0, timestamp = now() 
							WHERE user_id = '".$uid."' AND success = 0 AND charge_on_setup = 1";
					$reseller->query($query);
					//notify user
					echo '<b>Billing was successful!</b><br />Thank you for your payment, your account is now active.<br />Monthly billing has been scheduled to occur on the same day of each month following.<br />';
				} //end if($chargeUser['success'])
				else {
					//billing failed - reschedule for 2 days
					$nextAttempt = date('Y-m-d G:i:s', strtotime('+2 days'));
					$query = "UPDATE sixqube_billing 
							SET method_identifier = '".$chargeUser['profile_id']."', success = 0, sched = 1, 
								sched_date = '".$nextAttempt."',  sched_reattempt = 1, 
								charge_on_setup = 0, timestamp = now() 
							WHERE user_id = '".$uid."' AND success = 0 AND charge_on_setup = 1";
					$reseller->query($query);
					//notify user
					echo '<b>Sorry, there was a billing error:</b><br />'.$chargeUser['message'].'<br /><br />A reattempt has been scheduled to occur in 2 days.  Please correct the problem before then or contact support if you need assistance.<br />';
				}
			} //end if($charges = $reseller->chargesDueOnSetup($uid))
			else if($_POST['upgradeClass'] && ($_POST['upgradeClass']!=$user['class'])){
				//if the user is upgrading or changing to another class
				echo 'Attempting to upgrade your account...<br />';
				$upgradeClass = $_POST['upgradeClass'];
				//get info about class
				$query = "SELECT setup_price, monthly_price FROM user_class WHERE id = '".$upgradeClass."' AND admin_user = 7";
				$product = $reseller->queryFetch($query);
				if($product){
					$pid = $upgradeClass;
					$monthly = $product['monthly_price'];
					$setup = $product['setup_price'];
					$chargeAmount = 0.00;
					if($setup>0) $chargeAmount += $setup;
					if($monthly>0) $chargeAmount += $monthly;
					if($chargeAmount>0){
						//attempt charge
						$chargeUser = $anet->chargeCIM($uid, NULL, $pid, $chargeAmount, 'initial', 0, NULL, $apiLogin, $apiAuth);
						if($chargeUser['success']){
							//charge was successful, log it
							$query = "INSERT INTO sixqube_billing 
									(user_id, product_id, transaction_id, amount, method_identifier, success, note, timestamp)
									VALUES
									('".$uid."', '".$pid."', '".$chargeUser['message']."', '".$chargeAmount."', 
									'".$chargeUser['profile_id']."', 1, 'Initial setup and first month.', now())";
							$reseller->query($query);
							//schedule next month's
							$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
							$nextSched = $reseller->scheduleTransaction(NULL, $uid, NULL, $pid, 'monthlySubscription', 
																$monthly, NULL, 'Scheduled monthly billing for '.
																date('F', strtotime($nextBillDate)), $nextBillDate, 1, 
																NULL, NULL, 1);
							$update = true;
							echo 'Success!  Your new billing profile was successfully charged $'.$chargeAmount.' and your account has been upgraded.<br />';
							echo 'Billing has been scheduled to occur on the same day of each month following.';
						}
						else {
							//billing failed, notify user
							echo '<strong>Sorry, the billing attempt failed:</strong><br />';
							echo $chargeUser['message'].'<br />';
							echo 'Please correct the problem if possible and try again, or contact support if it persists.';
						}
					} //end if($chargeAmount>0)
					else {
						//no charge for account upgrade
						$update = true;
						echo 'Success!';
					}
					
					if($update){
						//update user table row
						$query = "UPDATE users SET parent_id = 7, access = 1, canceled = 0, class = '".$upgradeClass."', 
											  sixqube_client = 1, signup_ip = '".$_SERVER['REMOTE_ADDR']."', 
											  upgraded = now() 
								WHERE id = '".$uid."' LIMIT 1";
						$reseller->query($query);
					}
				} //end if($product)
				else {
					echo 'Sorry, an error occured: We couldn\'t find the account type you\'re trying to upgrade to.<br />';
					echo 'Please try again or contact support if the problem persists.';
				}
			} //end else if($_POST['upgradeClass'] && ($_POST['upgradeClass']!=$user['class']))
			echo '<br /><h2>You may now close this window</h2>';
			if($upgradeClass) echo '<h2>Note: You must log out before the account changes will take effect!</h2>';
		} //end if($profile['success'] && $action=='addNewBillingProfile')
		else if($profile['success'] && $action=='updateBillingProfile'){
			//if user was updating their profile and it was successful
			echo '<h1>Profile Successfully Updated</h1><br />
				 <h2>You may now close this window</h2>';
		}
		else {
			echo '<h1>An Error Occured</h1>';
			echo $profile['message'];
			echo '<br /><br /><b>Please try again or contact support if the problem persists</b>';
		}
	} //end if($uid && $a['billing_id'])
	else if(!$continue){
		echo '<h1>Manual Upgrade Required</h1>';
		echo 'Our records show that you may have one or more products currently set up in our old billing system, so a manual upgrade by our staff is required.  Our engineers have been notified and you will receive a follow-up shortly confirming that you can continue with the upgrade process.  We apologize for the inconvenience.<br /><br />';
		echo '<strong>No information was stored and your card was not charged.</strong>';
	}
	else {
		echo '<h1>An Error Occured</h1>';
		echo 'Please try again or contact support.<br />';
		echo 'No information was stored and your card was not charged.';
	}
}
?>