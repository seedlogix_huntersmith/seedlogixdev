<?php
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once('./inc/auth.php');
//require db connection
require_once('inc/domains.class.php');
$d = new Domains();
//validate domain string
$domain = $d->validateDomain($_GET['domain'], 1, 1, 0);
if(is_array($domain)){
	echo '<h1>Domain Error</h1>';
	echo 'Sorry, there was an error validating the domain.<br />
		Please try again or contact support if the problem persists.';
}
else {
	//first check if user is authorized (if the domain is for a hub that matches their UID)
	if($d->validateDomainOwnership($_SESSION['user_id'], $domain)) $continue = true;
	
	if($continue){
		$domainArray = $d->breakUpURL($domain);
		$domain = $domainArray['name'].'.'.$domainArray['tld'];
		
		echo '<h1>Email accounts for '.$domain.':</h1>';
		
		if($_GET['newAddr']){
			echo '<div style="background:url(\'img/undo-bar';
			if($_SESSION['theme']) echo '/'.$_SESSION['thm_undobar-clr'];
			echo '.png\') no-repeat;color:#fff;font-size:12px;width:437px;height:27px;"><p style="padding:4px;"><strong>Account created! Use temporary password <span style="font-size:15px;">'.$_GET['t'].'</span> to log in.</strong>';
			echo '</p></div>';
		}
		
		if($_GET['lincoln']){
			//delete the domain over on the lincoln server
			$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
			$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
			$secHash = md5($domain.$salt);
			$secHash = md5($secHash.'getEmails'.$pepper);
			//makes the hash only good if it was submitted today:
			$secHash = md5($secHash.date('d'));
			$data = file_get_contents('http://50.97.78.210/secure/setup_email.php?domain='.$domain.'&mode=getEmails&hash='.$secHash);
			$emails = json_decode($data, true);
		} else {
			$emails = $d->getEmailAddresses($domain);
		}
		if(is_array($emails)){
			$numEmails = count($emails['address']);
			echo '<br /><a href="http://'.$domain.'/webmail/" target="_blank" class="dflt-link"><strong>Click here to access your webmail or manage an email account.</strong></a><br /><br />';
			echo 'On the login page enter the <em>full email address</em> as your user name.<br /><br />';
			echo "If you've forgotten an email account password click the 'Reset Password' link next to it<br />and you'll be given a temporary password to log in with.<br /><br />";
			echo 'For <strong>POP3, SMTP and IMAP</strong> use the server: <em>mail.'.$domain.'</em><br /><br />';
			//list email accounts:
			foreach($emails['address'] as $key=>$value){
				echo '&bull;&nbsp;<strong>'.$value.'</strong>'.
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				if($value==$_GET['newAddr']){
					echo '<span style="font-size:11px;">Temporary password (please <a href="http://'.$domain.'/webmail/" target="_blank" class="dflt-link">log in</a> and change!): <strong>'.$_GET['t'].'</strong></span>';
				} else {
					echo '<span style="font-size:10px;"><a href="#" class="resetEmailPass dflt-link';
					if($_GET['lincoln']) echo ' lincolnDomain';
					echo '" rel="'.$domain.'" title="'.$value.'">[Reset Password]</a></span>';
				}
				echo '<br />';
			}
			echo '<br /><br /><br /><br />';
		}
		else {
			echo '<strong>'.$emails.'</strong>';
			$numEmails = 0;
		}
		//$numEmailsRemaining = 5-$numEmails;
		//if($numEmailsRemaining < 0) $numEmailsRemaining = 0;
		//echo '<br />You have <strong>'.$numEmailsRemaining.'</strong> email accounts remaining for this domain.<br /><br />';
		//if($numEmailsRemaining>0){
			echo '<h2>Create a new email account for '.$domain.':</h2>';
			echo '<div id="createDir" style="margin-top:-10px;">
					<form method="post" action="add-email.php" class="ajax">
					<input type="text" name="account" value="" style="width:250px;" maxlength="50" />
					<input type="text" name="domain" value="@'.$domain.'" style="width:250px;" readonly />';
			if($_GET['lincoln']) echo '<input type="hidden" name="lincoln" value="1" />';
			echo   '<input type="submit" value="Create New Email" name="submit" />
					</form>
				</div>';
		//}
		echo '<br /><br /><br />';
		echo '<strong><a href="#" class="installGoogleApps dflt-link';
		if($_GET['lincoln']) echo ' lincolnDomain';
		echo '" rel="'.$domain.'">Click here to install Google Apps support for this domain.</a></strong><br /><br />';
		echo 'This is only for businesses with Google Apps set up.  It will cause all mail to be sent to<br />your Google Apps accounts rather than the webmail above.<br /><br />';
		echo 'You\'ll be able to access your Google Apps Mail at <strong>email.'.$domain.'</strong> after changing the appropriate settings in your Google Apps management settings.  <a href="http://www.google.com/support/a/bin/answer.py?answer=53340" target="_blank" class="dflt-link">Click here</a> for information on changing the settings.';
		echo '<br style="clear:both" />';
	}
	else {
		echo '<h1>Domain Error</h1>';
		echo 'Sorry, there was an error validating your ownership of the domain.<br />
		Please try again or contact support if the problem persists.';
	}
}
?>