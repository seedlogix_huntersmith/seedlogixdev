<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/reseller.class.php');
	
	//create new instance of class
	$reseller = new Reseller();
	
	//content
	echo '<h1>Analytics</h1>';
	echo $reseller->networkSitesDropdown($_SESSION['login_uid'], 'network_analytics_view');
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop-down above to select which network site's analytics to view.</p>
</div>