<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//require db connection
	require_once('inc/local.class.php');
	$trash = new Local();
	
	if(!$_GET['admin']){
?>
<h1>Trash Can</h1>
<h2>Items in the trash can are permanently deleted 30 days after being trashed.</h2>
<div id="trash">
	<h2>Directories</h2>
	<?=$trash->displayTrash('directory', $_SESSION['user_id']);?>
	<h2>Hubs</h2>
	<?=$trash->displayTrash('hub', $_SESSION['user_id']);?>
	<h2>Hub Pages</h2>
	<?=$trash->displayTrash('hub_page', $_SESSION['user_id']);?>
	<h2>Hub Pages (V2)</h2>
	<?=$trash->displayTrash('hub_page2', $_SESSION['user_id']);?>
	<h2>Press Releases</h2>
	<?=$trash->displayTrash('press_release', $_SESSION['user_id']);?>
	<h2>Blogs</h2>
	<?=$trash->displayTrash('blogs', $_SESSION['user_id']);?>
	<h2>Blog Posts</h2>
	<?=$trash->displayTrash('blog_post', $_SESSION['user_id']);?>
	<h2>Articles</h2>
	<?=$trash->displayTrash('articles', $_SESSION['user_id']);?>
	<h2>Prospects</h2>
	<?=$trash->displayTrash('contact_form', $_SESSION['user_id']);?>
	<h2>Autoresponders</h2>
	<?=$trash->displayTrash('auto_responders', $_SESSION['user_id']);?>
	<h2>Custom Forms</h2>
	<?=$trash->displayTrash('lead_forms', $_SESSION['user_id']);?>
	<h2>Form Responses</h2>
	<?=$trash->displayTrash('leads', $_SESSION['user_id']);?>
	<h2>Email Marketing</h2>
	<?=$trash->displayTrash('lead_forms_emailers', $_SESSION['user_id']);?>
        <h2>Email Responses</h2>
        <?php
        
                require_once '6qube/core.php';
                require_once QUBEPATH . '../models/CampaignCollab.php';
                require_once QUBEPATH . '../library/Popcorn/BaseController.php';
                require_once QUBEPATH . '../classes/controllers/NotificationsAdmin.php';
                
                $item = Qube::GetDriver()->getCampaignCollabsWhere('trashed != \'0000-00-00\' AND campaign_id=%d', $_SESSION['campaign_id']);
                
                $ctrlr = new NotificationsAdminController;
                
                if(!count($item))                    
			echo '<div class="trash-item"><p class="title">None</p></div>';
                
                foreach($item as $collaborator){

            ?>
            <div class="trash-item">
                    <p class="title"><?php echo htmlentities($collaborator->name); ?></p>
                    <p class="desc"><?php echo htmlentities($collaborator->email); ?></p>
                    <p class="permdelete">
                            <a href="<?php echo $ctrlr->ActionUrl('Notifications_deletepermanent',
                                array('id' => $collaborator->id)); ?>" class="ajax dflt-link" title="responses email">[Delete Forever]</a>
                    </p>&nbsp;&nbsp;&nbsp;
                    <p class="restore"><a href="<?php echo $ctrlr->ActionUrl('Notifications_restore',
                                array('id' => $collaborator->id)); ?>" class="ajax dflt-link" title="response email">[Restore]</a></p>
            </div>     
        <?php
        
            }
        ?>
	<br style="clear:both" />
</div>
<? } else { ?>
<h1>Admin Trash Can</h1>
<h2>Items in the trash can are permanently deleted 30 days after being trashed.</h2>
<div id="trash">
	<h2>Multi-User Hubs</h2>
	<?=$trash->displayTrash('hub', $_SESSION['login_uid'], 1);?>
	<h2>Multi-User Hub Pages</h2>
	<?=$trash->displayTrash('hub_page2', $_SESSION['login_uid'], 1);?>
	<h2>Emailers</h2>
	<?=$trash->displayTrash('resellers_emailers', $_SESSION['login_uid']);?>
	<h2>Main Site Prospects</h2>
	<?=$trash->displayTrash('contact_form', $_SESSION['login_uid'], 1);?>
	<h2>Network Sites</h2>
	<?=$trash->displayTrash('resellers_network', $_SESSION['login_uid']);?>
	<? if($_SESSION['thm_custom-backoffice']){ ?>
	<h2>Custom Back Office Pages</h2>
	<?=$trash->displayTrash('resellers_backoffice', $_SESSION['login_uid']);?>
	<? } ?>
	<br style="clear:both" />
</div>
<? } ?>