<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/articles.class.php');
	
	//create new instance of class
	$articles = new Articles();
	
	if($_SESSION['theme']) $previewUrl = 'http://articles.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://articles.6qube.com/';
	$articles_count = 0;
	//$articles_limit = $articles->getLimits($_SESSION['user_id'], "article");
	$articles_limit = $_SESSION['user_limits']['article_limit'];
	if($articles_limit==-1 || $articles_limit==NULL) $remaining = "Unlimited";
	
	//content
	echo '<h1>Articles</h1>';
	/***************************************************
	IF FORM INFO IS SUBMITTED
	***************************************************/
	if(isset($_POST['headline'])){
		$update = $articles->updateArticles($_POST);	
		if($update){
			echo $update;
			$x = articleDisplay(1);
		}
		else{
			include('./inc/forms/add-article.php');
		}
	}
	/***************************************************
	IF ID OR FORM in URL
	***************************************************/
	else if($_GET){
		$_SESSION['current_id'] = $_GET['id'];
		include('./inc/forms/add-article.php');
	}
	// WHEN THEY FIRST COME TO THE PAGE
	else{
		$result = $articles->getArticles($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']); //primes Articles results, and is needed for getArticlesRows()
		$articles_count = $articles->getArticlesRows();
		echo '<div id="createDir">';
		if(!$_SESSION['theme']){
			echo '<div class="watch-video"><ul><li><a href="#campVideo" class="watch_video trainingvideo" >Watch Video</a></li></ul></div>';
		}
		echo '<form method="post" action="add-article.php" class="ajax"><input type="text" name="name" ';
		if(!$remaining && ($articles_limit==0 || (($articles_limit-$articles_count)<=0))) echo 'readonly';
		echo ' /><input type="submit" value="Create New Article" name="submit" /></form></div>';
		if(!$_SESSION['theme']){
			echo '<div style="display: none;"><div id="campVideo" style="width:640px;height:480px;overflow:auto;"><a href="http://6qube.com/videos/training/directory-overview.flv" style="display:block;width:640px;height:480px"id="player"> </a><script>flowplayer("player", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");</script></div></div>';
		}
		echo '<div id="message_cnt"></div>';
		
		if($_POST && !$switch){
			$newArticles = $articles->validateText($_POST['name'], 'articles', 'Article'); //validate press name
			if($articles->foundErrors()){ //check if there were validation errors
				echo '<p class="error"><strong>Errors were found</strong><br />'. $articles->listErrors('<br />') .'</p>'; //list the errors
			}
			else{ //if no errors
				$articles->addNewArticles($_SESSION['user_id'], $newArticles, $_SESSION['campaign_id'], $articles_limit); //add new hub
			}
		}
		
		if($articles_count) { //checks to see if the user has any directories
			if(!$remaining) $remaining = $articles_limit - $articles_count;
			$remaining = $remaining<0 ? 0 : $remaining;
			echo '<div id="dash"><div id="fullpage_article" class="dash-container"><div class="container">';
			while($row = $articles->fetchArray($result)){ //if so loop through and display links to each Articles
				$tracking = $row['tracking_id'];
				$cid = $row['cid'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'articles.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="articles.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=articles&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$articles->ANAL_getGraphImg('article', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="articles.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Articles remaining.';
			if(!$_SESSION['theme']){
				if(($articles_limit-$articles_count)<=0 && $articles_limit!=-1 && $articles_limit!="Unlimited") echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
			if($_SESSION['reseller']){
				echo '<br /><br />
					<a href="reseller-transfer-items.php?mode=section&mode2=articles&cid='.$cid.'" class="dflt-link">Transfer one or more articles to another user</a>';
			}
		}
		else{ //if no articles
			if($articles_limit==-1 || $articles_limit==NULL) $articles_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_article" class="dash-container"><div class="container">';
			if($articles_limit==0 && $articles_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Articles remaining.';
				if(!$_SESSION['theme']){
					echo ' <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Article.  You have <strong>'.$articles_limit.'</strong> remaining.';
			echo '</div></div></div>';
		}
	}
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>