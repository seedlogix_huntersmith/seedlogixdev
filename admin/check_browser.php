<?php
	$browser = $_GET['browser'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Elements Login by 6Qube - Browser Checker</title>
<style type="text/css">
	* {margin:0px; padding:0px;}
	#wrapper {  margin:auto; width:100%; }
	p { font-family:Georgia, "Times New Roman", Times, serif; font-size:16px; color:#333333; text-align: center; padding:10px; }
	img {
		border: none;	
	}
</style>
</head>
<body>
<div id="wrapper">
<div class="logo" align="center"><img src="http://hubs.6qube.com/themes/elements/images/logo.png"  />
</div>
<p>We have determined your web browser to be:<br />
<strong><?=$browser?></strong></p>
<p>We <strong>strongly</strong> suggest that you use Mozilla's Firefox browser for this application, or you may experience errors and/or glitches.  We are currently working on making 6Qube Elements 100% compatible with all browsers.</p>
<p>Mozilla Firefox is a free web browser that is very fast, safe, secure, and standards-compliant.  Click the image below to be taken to Mozilla Firefox's website:<br />
<a href="http://www.mozilla.com/en-US/firefox/firefox.html" target="_blank"><img src="http://www.mozilla.com/img/tignish/about/logo/download/logo-wordmark-preview.png" /></a></p>
</div>
</body>
</html>