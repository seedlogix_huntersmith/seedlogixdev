<?php

  require_once('./inc/local.class.php');

  $campaign = new Local();

  $cid = $_POST['campaignSelector'];
  $type = $_POST['itemTypeSelector'];
  
  $continue = true;
  $i = 1;
 
 $selected_items = $_POST['item'];
 
 if(!empty($selected_items) && is_array($selected_items)){
	 foreach($selected_items as $item){
		$success = $campaign->transferToAnotherCamp($type, $item, $cid);
	 } 
 }
 else
 	$error_message = "none selected";
 
  if($success){
  	echo "success";
  }
  else if($error_message){
  	echo $error_message;
  }
  else
  	echo "transfer failed";

?>