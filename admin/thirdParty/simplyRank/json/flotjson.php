<?php
require_once('/home/sapphire/public_html/6qube.com/admin/inc/rankings.class.php');
$rankings = new Rankings();

$keyword=$_POST['keyword'];
$the_main_site_id=$_POST['the_main_site_id'];

$mergedData = array();
 
$pdo    =   $rankings->pdo;
$pq     =   $pdo->prepare("SELECT datetime,google,google_url FROM sapphire_rankings.rankings WHERE keyword=? AND site_id=?");

//Get the first set of data you want to graph from the database
$result=    $pq->execute(array($keyword, $the_main_site_id));
 
//loop through the first set of data and pull out the values we want, then format
while($r = $pq->fetch(PDO::FETCH_ASSOC))
{
	$dt = strtotime($r['datetime']);
    $x = $dt*1000;
	if($r['google']==0){
    $y = NULL;
	} else {
    $y = $r['google'];
	}
    $data1[] = array ($x, $y);
	$url[] = $r['google_url'];
	$date[] = date("F j, Y", $dt);
}
 
//send our data values to $mergedData, add in your custom label and color
$mergedData[] =  array('label' => "Google" , 'data' => $data1, 'color' => '#2f84d3', 'url' => $url, 'date' => $date);
 

$pqbing     =   $pdo->prepare("SELECT datetime,google,google_url FROM sapphire_rankings.rankings WHERE keyword=? AND site_id=?");

//Get the second set of data you want to graph from the database
$result=$pqbing->execute(array($keyword, $the_main_site_id));
 
//loop through the second set of data and pull out the values we want, then format
while($r = $pqbing->fetch(PDO::FETCH_ASSOC))
{
	$dt = strtotime($r['datetime']);
    $x = $dt*1000;
	if($r['bing']==0){
    $y = NULL;
	} else {
    $y = $r['bing'];
	}
    $data2[] = array ($x, $y);
	$url[] = $r['bing_url'];
	$date[] = date("F j, Y", $dt);
}
 
//send our data values to $mergedData, add in your custom label and color
$mergedData[] = array('label' => "Bing" , 'data' => $data2, 'color' => '#2f2f2f', 'url' => $url, 'date' => $date);
 
//now we can JSON encode our data
echo json_encode($mergedData);
?>