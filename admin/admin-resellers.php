<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/reseller.class.php');
	$reseller = new Reseller();
	
	$offset = $_GET['offset'] ? $_GET['offset'] : 0;
	
	//content
	echo '<h1>Resellers</h1>';
	
	echo '<div id="createDir">
			<form method="post" class="ajax" action="inc/forms/add-reseller.php">
				<input type="text" name="name" value="Company Name" />
				<input type="submit" value="Create New Reseller" name="submit" />
			</form>
		</div><br style="clear:both;" /><br />';

	echo $reseller->displayResellers('admin-resellers.php', 20, $offset);
		
	echo '<br /><p>&nbsp;</p>';
?>