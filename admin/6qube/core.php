<?php

// QUBEPATH => directory of this core file and system config files.
define('QUBEPATH', dirname(__FILE__) . '/');
//var_dump(QUBEPATH);

defined('HYBRID_PATH') || (defined('QUBEPATH') && define('HYBRID_PATH', 
	realpath(QUBEPATH . '/../../hybrid') . '/')) || define('HYBRID_PATH',
		'/home/sapphire/production-code/dev-trunk-svn/hybrid/');
//var_dump(HYBRID_PATH);
defined('HYBRID_LIB') || define('HYBRID_LIB', HYBRID_PATH . 'library/');

// QUBEADMIN    =>  PATH TO ADMIN DIRECTORY
define('QUBEADMIN', QUBEPATH . '../');

// QUBEROOT     => PATH TO 6QUBE "DOCUMENT" ROOT
define('QUBEROOT', QUBEADMIN . '../');

require_once QUBEPATH . 'functions.php';    
require_once QUBEADMIN . 'library/CandyDB/CandyDB.php';
require_once QUBEADMIN . 'library/Popcorn/App.php';
require_once QUBEADMIN . 'classes/QubeDriver.php';
require_once QUBEADMIN . 'library/Popcorn/DBObject.php';
require_once HYBRID_LIB . 'fullcontact-api-php-master/Services/FullContact.php';
#require_once QUBEPATH . '../classes/PreparedDbObject.php';
require_once QUBEADMIN . 'classes/Qube.php';
#require_once QUBEADMIN . 'models/User.php';


// if nolaunch, do not initialize following constats  @todo dangerous I know
#if(defined('QUBE_NOLAUNCH')) return;

#if(Qube::IS_DEV())    require_once QUBEPATH . 'debugger.php';

/**
 * Notes:
 * 
 * Use constants. Do not use $qube object
 * 
 */
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

#if(strpos(getcwd(), '2012-08-24/admin') === FALSE) chdir(QUBEPATH);

#echo '1';
