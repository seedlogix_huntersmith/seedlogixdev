<?php

function tracelog($msg, $return = NULL)
{   
    static $log = array();
    
    if(is_null($msg))
    {
        var_dump($log);
    }
    
    if(empty($log))
    {
        register_shutdown_function('tracelog', NULL);
    }
    $bt = debug_backtrace();
    $data = array_shift($bt);
    
    $msg = 'File: ' . $data['file'] . '#' . $data['line'] . "\n". $msg;
    $log[] = $msg;
    
    return $return;
}

function asciiarray($itemlist, $maxlength=60){
    $item1  = array_shift($itemlist);
    array_unshift($itemlist, $item1);
    
    $keys   =   array_keys($item1);
    $lengths    =   array();
    foreach($itemlist as $row)
    {
        foreach($keys as $ki => $key)
        {
            $lengths[$ki]   =   max(strlen($key), min(max(strlen($row[$key]), $lengths[$ki]), $maxlength));
        }
    }
    $table = new Zend_Text_Table(array('columnWidths' => $lengths));
    $table->appendRow($keys);
    foreach($itemlist as $row)
        $table->appendRow ($row);
    
    echo $table;
}