<?php /* Smarty version Smarty-3.1.8, created on 2018-03-12 20:58:48
         compiled from "/vagrant/admin/6qube/../../emails/themes/corporate/theme.html" */ ?>
<?php /*%%SmartyHeaderCode:5584430385aa6ea08a52a18-11197292%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4f847773960988ef85f26142459cecdcdcb3586' => 
    array (
      0 => '/vagrant/admin/6qube/../../emails/themes/corporate/theme.html',
      1 => 1508181086,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5584430385aa6ea08a52a18-11197292',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'links_color' => 0,
    'titles_color' => 0,
    'bg_color' => 0,
    'themepath' => 0,
    'accent_color' => 0,
    'logo' => 0,
    'site_root' => 0,
    'edit_region2' => 0,
    'email' => 0,
    'call_action' => 0,
    'edit_region1' => 0,
    'blog_stream_id' => 0,
    'blogposts' => 0,
    'post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5aa6ea09154664_53179831',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5aa6ea09154664_53179831')) {function content_5aa6ea09154664_53179831($_smarty_tpl) {?><?php if (!is_callable('smarty_function_load_blogposts')) include '/vagrant/admin/library/Smarty-3.1.8/libs/plugins/function.load_blogposts.php';
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
  
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">

  <style type="text/css">
	body { margin:0;padding:0; } 
	.bodytbl { margin:0;padding:0;-webkit-text-size-adjust:none; } 
	.issue { font-size:18px;letter-spacing:-0.03em;text-shadow:#919191 -1px -1px 0px;-moz-text-shadow:#919191 -1px -1px 0px;-webkit-text-shadow:#919191 -1px -1px 0px; } 
	table { font-family:Helvetica, Arial, sans-serif;font-size:12px;color:#787878; } 
	div { line-height:22px;color:#787878; } 
	img { display:block; } 
	td,tr { padding:0; } 
	ul { margin-top:24px;margin-bottom:24px; } 
	li { list-style:disc;line-height:24px; }  
	a { color: <?php echo $_smarty_tpl->tpl_vars['links_color']->value;?>
;text-decoration:none;padding:2px 0px; } 
	.contenttbl { background-color:#F4F4F4; } 
	.border { background-color:#f8f8f8;line-height:6px; font-size:6px; } 
	.h div{ font-family:Georgia,Times,serif;font-size:24px;color:<?php echo $_smarty_tpl->tpl_vars['titles_color']->value;?>
;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:24px;text-shadow:#ddd 1px 1px 0px;-moz-text-shadow:#ddd 1px 1px 0px;-webkit-text-shadow:#ddd 1px 1px 0px; } 
	.h1 div { font-family:Georgia,Times,serif;font-size:30px;color:<?php echo $_smarty_tpl->tpl_vars['titles_color']->value;?>
;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px; text-align:center; } 
	.h2 div { font-family:Georgia,Times,serif;font-size:24px;color:<?php echo $_smarty_tpl->tpl_vars['titles_color']->value;?>
;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px; } 
	.tagline div { font-family:Helvetica,Arial,sans-serif; line-height:12px;font-size:12px;color:#FFFFFE;text-shadow:#919191 -1px -1px 0px;-moz-text-shadow:#919191 -1px -1px 0px;-webkit-text-shadow:#919191 -1px -1px 0px; } 
	.quote div { font-family:Georgia,Times,serif;font-style:italic;color:#EEEEEE;margin-top:12px;margin-bottom:12px;font-size:20px;line-height:26px;letter-spacing:-0.04em;text-shadow:#919191 -1px -1px 0px;-moz-text-shadow:#919191 -1px -1px 0px;-webkit-text-shadow:#919191 -1px -1px 0px; } 
	.invert div, .invert .h { color:#FFFFFE; } 
	.invert div a { color:<?php echo $_smarty_tpl->tpl_vars['links_color']->value;?>
; } 
	.small div { font-size:10px; line-height:16px; } 
	.head a { color:<?php echo $_smarty_tpl->tpl_vars['links_color']->value;?>
; } 
	.btn { margin-top:10px;display:block; } 
	.btn img,.social img { display:inline; } 
	div.preheader { line-height:1px;font-size:1px;height:1px;color:#B1B2A8;display:none!important; } 
  </style>
</head>
<body>
    
<table class="bodytbl" bgcolor="<?php echo $_smarty_tpl->tpl_vars['bg_color']->value;?>
" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td align="center">
      <table cellpadding="0" cellspacing="0" width="560">
        <tbody>
          <tr>
            <td align="center" height="15" valign="top">
            <div class="preheader"><!-- PREHEADER --></div>
            <div class="small head">
            
            <a name="top"></a></div>
            </td>
          </tr>
        </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td width="12">&nbsp;</td>
            <td align="center" valign="bottom"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/top.png" alt="" title="" border="0" height="18" width="600"></td>
            <td width="12">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td colspan="5" class="border" height="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td align="right" valign="top" width="8"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/left.png" alt="" title="" border="0" height="220" width="8"></td>
            <td class="border" width="6">&nbsp;</td>
            <td class="contenttbl" width="10">&nbsp;</td>
            <td class="contenttbl" align="left" valign="top"><!-- CONTENT start -->
            <table cellpadding="0" cellspacing="0" width="552">
              <tbody>
                <tr>
                  <td colspan="4" height="10">&nbsp;</td>
                </tr>
                <tr bgcolor="<?php echo $_smarty_tpl->tpl_vars['accent_color']->value;?>
">
                  <td height="72" width="16">&nbsp;</td>
                  <td width="260"><?php if ($_smarty_tpl->tpl_vars['logo']->value){?>
                      <img src="<?php echo $_smarty_tpl->tpl_vars['site_root']->value;?>
<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
" alt="" title="" style="max-width: 260px;" border="0" height="40">
                  <?php }?></td>
                  <td class="invert" align="right" width="260">
                  <div class="issue">
                  <div><?php echo $_smarty_tpl->tpl_vars['edit_region2']->value;?>
</div>
                  </div>
                  </td>
                  <td width="16">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="4" height="16">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="4" align="left"> <?php echo $_smarty_tpl->tpl_vars['email']->value['subject'];?>

                      
                      <?php echo $_smarty_tpl->tpl_vars['email']->value['body'];?>
 <?php echo $_smarty_tpl->tpl_vars['email']->value['from_field'];?>
 </td>
                </tr>
              </tbody>
            </table>
<!-- CONTENT end --> </td>
            <td class="contenttbl" width="10">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
            <td align="left" valign="top" width="8"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/right.png" alt="" title="" border="0" height="220" width="8"></td>
          </tr>
        </tbody>
      </table>
<!-- ~ header block ends here --><!-- Quotation start ~quote~ -->
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td width="8">&nbsp;</td>
            <td class="border" valign="bottom" width="6"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_top_border.png" alt="" title="" border="0" height="8" width="6"></td>
            <td align="center" bgcolor="#f4f4f4" valign="bottom"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_top.png" alt="" title="" border="0" height="8" width="572"></td>
            <td class="border" valign="bottom" width="6"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_top_border.png" alt="" title="" border="0" height="8" width="6"></td>
            <td width="8">&nbsp;</td>
          </tr>
          <tr bgcolor="<?php echo $_smarty_tpl->tpl_vars['accent_color']->value;?>
">
            <td colspan="5" align="center" valign="bottom"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_bg.png" alt="" title="" border="0" height="6" width="600"></td>
          </tr>
          <tr bgcolor="<?php echo $_smarty_tpl->tpl_vars['accent_color']->value;?>
">
            <td colspan="5" align="center">
            <div class="quote">
            <div><?php echo $_smarty_tpl->tpl_vars['call_action']->value;?>
</div>
            </div>
            </td>
          </tr>
          <tr bgcolor="<?php echo $_smarty_tpl->tpl_vars['accent_color']->value;?>
">
            <td colspan="5" align="center" valign="top"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_bg.png" alt="" title="" border="0" height="6" width="600"></td>
          </tr>
          <tr>
            <td width="8">&nbsp;</td>
            <td class="border" valign="top" width="6"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_bottom_border.png" alt="" title="" border="0" height="8" width="6"></td>
            <td align="center" bgcolor="#f4f4f4" valign="top"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_bottom.png" alt="" title="" border="0" height="8" width="572"></td>
            <td class="border" valign="top" width="6"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/sep_bottom_border.png" alt="" title="" border="0" height="8" width="6"></td>
            <td width="8">&nbsp;</td>
          </tr>
        </tbody>
      </table>
<!-- Quotation end ~quote~ -->
      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td class="border" height="23" width="6">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td valign="top" width="552"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/separator.gif" alt="" title="" border="0" height="2" width="552"></td>
            <td width="10">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
<!-- 1/2 Floating Image Left start ~1_2_f_l~ -->
      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td class="border" width="6">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td align="center" valign="top">
            <table cellpadding="0" cellspacing="0" width="552">
              <tbody>
                <tr>
<!-- CONTENT start --> <td align="left" valign="top"> <?php echo $_smarty_tpl->tpl_vars['edit_region1']->value;?>
 </td>
<!-- CONTENT end --> </tr>
              </tbody>
            </table>
            </td>
            <td width="10">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
          <tr>
            <td class="border" width="6">&nbsp;</td>
            <td colspan="3" height="16">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>


      <?php if (isset($_smarty_tpl->tpl_vars['blog_stream_id']->value)&&$_smarty_tpl->tpl_vars['blog_stream_id']->value>0){?>
      <?php echo smarty_function_load_blogposts(array('blog_id'=>$_smarty_tpl->tpl_vars['blog_stream_id']->value,'limit'=>10),$_smarty_tpl);?>

      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td class="border" width="6">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td align="center" valign="top">
            <table cellpadding="0" cellspacing="0" width="552">
              <tbody>
                <tr>
<!-- CONTENT start --> <td align="left" valign="top"> 
                    <h2>Blog Stream (Recent Posts)</h2>
                    <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['blogposts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value){
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
                    <?php echo $_smarty_tpl->tpl_vars['post']->value['post_title'];?>
 <br />
                    <?php } ?>
                    </td>
<!-- CONTENT end --> </tr>
              </tbody>
            </table>
            </td>
            <td width="10">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
          <tr>
            <td class="border" width="6">&nbsp;</td>
            <td colspan="3" height="16">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <?php }?>
      
<!-- Footer start -->
      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td class="border" height="23" width="6">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td valign="middle" width="552"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/separator.gif" alt="" title="" border="0" height="2" width="552"></td>
            <td width="10">&nbsp;</td>
            <td class="border" width="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td align="right" valign="bottom" width="8"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/bottom_left.png" alt="" title="" border="0" height="20" width="8"></td>
            <td class="border" width="6">&nbsp;</td>
            <td bgcolor="#f4f4f4" valign="top"> <br>
            </td>
            <td class="border" width="6">&nbsp;</td>
            <td align="left" valign="bottom" width="8"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/bottom_right.png" alt="" title="" border="0" height="20" width="8"></td>
          </tr>
        </tbody>
      </table>
      <table class="contenttbl" cellpadding="0" cellspacing="0" width="584">
        <tbody>
          <tr>
            <td colspan="5" class="border" height="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td width="12">&nbsp;</td>
            <td align="center" valign="bottom"><img src="<?php echo $_smarty_tpl->tpl_vars['themepath']->value;?>
/corporate/bottom.png" alt="" title="" border="0" height="18" width="600"></td>
            <td width="12">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" width="600">
        <tbody>
          <tr>
            <td height="24">&nbsp;</td>
          </tr>
        </tbody>
      </table>
<!--  Footer end --> </td>
    </tr>
  </tbody>
</table>
    <!-- blolg stream -->
</body></html>
<?php }} ?>