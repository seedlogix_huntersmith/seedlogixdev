<?php

    //MYSQL Login Info for 'gravedigger' development server

    
    $settings['dbhost'] = 'localhost';
    $settings['dbusername'] = 'root';
    $settings['dbpassword'] = 'seedlogix';
    $settings['dbname'] = 'sapphire_6qube';
    
    $settings['dbslaves'] = array(
                        array('dbhost'   =>  'localhost',
                                'dbusername'    => 'root',
                                 'dbpassword'   => 'seedlogix',
                                 'dbname'   => 'sapphire_6qube')
                            ,);
    
    //Sub-domains. (be sure NOT to use a trailing forward slash "/")
		$settings['directory_url'] = "http://local.{$settings['domain']}";
		$settings['hub_url'] = "http://hubs.{$settings['domain']}";
		$settings['press_url'] = "http://press.{$settings['domain']}";
		$settings['articles_url'] = "http://articles.{$settings['domain']}";
		$settings['blog_url'] = "http://blogs.{$settings['domain']}";
		
    

		$settings['display_count'] = 3;
// server storage paths and urls

    $settings['USERS_STORAGE'] = '/home/seedlogix/web/6qube/users';
    
    
    #IMPORTANT: 6QUBE_STORAGE_URL MUST ___! NOT !___ CONTAIN A TRAILING SLASH
    #REASON:    TEMPLATES ADD THE SLASH THEMSELVES TO THE URL
    $settings['6qube_storage_url']  =   '';
    $settings['6qube_home_url']         =   '/';
   
    
    $settings['dblog']  =   isset($_GET['dblog']) ? 1 : 0;
    $settings['dblogfile']  =   QUBEROOT . 'logs/' . date('Ymd-His') . '.sql';
    
    

    // debug (test) config
    $settings['QUBE_ERROR_REPORTING']   = getflag('error_report') ? getflag('error_report') : E_ALL & ~E_STRICT & ~E_NOTICE; //E_ALL | E_STRICT | E_WARNING | E_NOTICE;
    $settings['QUBE_DISPLAY_ERRORS']    = 1;//getflag('disp_errors') ? 0 : 1;
    //$settings['QUBE_TEST_SITE'] =   'pastephp.com';
    $settings['QUBE_TEST_SITE'] =   'seedlogix.test/hybrid';
    $settings['IS_DEV'] =   1;
    //$settings['IS_DEV'] =   1;
    $settings['IS_SIMHOST'] =   0;
    //$settings['IS_SIMHOST'] =   1;
    $settings['PIWIK_PATH']    =   '/home/amado/web/piwik';
		$settings['CONFIGFILE']	=	__FILE__;
		// flags
		$settings['_disable_cache']	=	true;
		
    // end test config
   ini_set('xdebug.var_display_max_depth', 10 );