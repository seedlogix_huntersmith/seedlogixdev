<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * use this function to return the static $connector object to be used globally
 * this is to avoid creating multiple connections
 * 
 * @staticvar null $connector
 * @return \DbConnector 
 */
function get_connector()
{
    static $connector = NULL;
    
    if(is_null($connector))
    {
        $connector  =    new DbConnector;
    }
    
    return $connector;
}

function getflag($flag){
		if(isset($_COOKIE[$flag]))
			return $_COOKIE[$flag];
		//var_dump(Qube::get_settings('_' . $flag));
		return Qube::get_settings('_' . $flag);
}

/**
 * this ctually returns a subdomain along with the domain (excluding www subs)
 * 
 * @param type $key
 * @return string
 */
function get_domain($key    =   'SERVER_NAME')
{
    $server = strtolower($_SERVER[$key]);
    preg_match('/([^\.]*?)\.?([a-z0-9-_]+\.?[a-z0-9-_]+)$/', $server, $matches);

    $site   =   $matches[2];
    if($matches[1] != '' && $matches[1] != 'www')
            $site	=	$matches[1] . '.' . $matches[2];
    
    return $site;
}

function extract_column($twoDarray, $key)
{
    $values =   array();
    foreach($twoDarray as $rI   =>  $row)
    {
        if(!array_key_exists($key, $row)) continue;
        $values[$rI]    =   $row[$key];
    }
    return $values;
}


function sitedatalinkpath($hostname, $datadir =   null, $checkonly    =   false)
{
    if(!$datadir)
            $datadir    =   QUBEROOT . 'sitedata/';
    
    $numparts  =   preg_match('/^(..)(..).*$/', $hostname, $matches);
    $prevdir    =   '';
    $curdir     =   '';
    for($i  =   0;  $i < 2; $i++)
    {
        $prevdir    .=  $matches[$i+1];
        $curdir     =   $datadir . $prevdir;
        if(!is_dir($curdir) && !$checkonly)
        {
            mkdir($curdir);
        }

        $prevdir .= '/';
    }

    return $curdir . '/' . $hostname;
}

function is_amadodevlocal(){
    return @$_SERVER['QUBECONFIG']  == 'gravedigger' || $_SERVER['QUBECONFIG']  ==  '12k' || $_SERVER['QUBECONFIG']  ==  'eric';
}

function piwik_inception(){
    return is_amadodevlocal() ? '2013-01-01' : '2010-01-01';
}

function array_to_css_style($array, $defaultValues = array()){
    if(isset($array))
        $array = array_merge($defaultValues, $array);
    else
        $array = $defaultValues;

    $array = join('; ', array_map(function($v, $k){
        return "$k: $v";
    }, $array, array_keys($array)));

    return $array;
}
