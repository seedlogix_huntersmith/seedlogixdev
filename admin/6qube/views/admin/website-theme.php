<!-- LEFT SIDE NAV -->
<div class="mainNav">
	<!-- Main nav -->
	<ul class="nav">
		<li><a title="" href="<?php echo $this->action_url('Website_settings', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/dashboard.png"><span>Settings</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_theme', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/ui.png"><span>Theme</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_seo', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/forms.png"><span>SEO</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_editRegions', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/messages.png"><span>Edit Regions</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_photos', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/statistics.png"><span>Photos</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_social', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/tables.png"><span>Social</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_pages', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/other.png"><span>Pages</span></a></li>
	</ul>
</div>

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

<h1>{Website} Theme</h1>
<br class="clear" /><br />



		<div class="fluid">
				
				<div class="widget grid6">       
                    <ul class="tabs">
                        <li class="activeTab"><a href="#tabb3">Website Themes</a></li>
                        <li class=""><a href="#tabb4">Base Themes</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content" id="tabb3" style="display: none;">
                         <!-- Website Themes -->            
                        	<div class="gallery">
               <ul>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li class="selected"><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
               </ul> 
           </div>

                        </div>
                        <div class="tab_content" id="tabb4" style="display: block;">
                        <!-- Base Themes -->
                        <div class="gallery">
               <ul>
                    
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
                    <li><a class="lightbox" title="" href="images/big.png"><img alt="" width="115" src="http://6qube.com/hubs/themes/thumbnails/corporate.jpg"></a>
                        <div class="actions" style="display: none;">
                            <div class="galleryActionsContainer">
                            	<a class="edit" title="preview" href="#"><img alt="" src="images/icons/update.png"></a>
                            	<a class="remove" title="remove" href="#"><img alt="" src="images/icons/delete.png"></a>
                            </div>
                        </div>
                    </li>
               </ul> 
           </div>

                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
			
        
                    <!-- section -->
                    <div class="widget grid6">
                        <div class="whead"><h6>Connect To A Blog</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid12">
                        	<p>Select one of your blogs to synchronize it with this hub. This will change the blog's theme and settings to match this hub.</p>
							<p>If you make changes to your hub's settings you will have to reconnect it.</p>
							<p>A blog can only be connected to one hub at a time.</p>
							</div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                            <div class="grid1"><label>Blog:</label></div>
                            <div class="grid10 noSearch">
                            <select name="select2" class="select">
                                <option value="opt1">Usual select box with dropdown styling</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                                <option value="opt4">Option 4</option>
                                <option value="opt5">Option 5</option>
                                <option value="opt6">Option 6</option>
                                <option value="opt7">Option 7</option>
                                <option value="opt8">Option 8</option>
                            </select>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div><!-- //section -->
                                           
                    <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Design Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Links Color:</label></div>
                        	<div class="grid9"><div class="cPicker" id="cPicker1"><div style="background-color: #e62e90"></div><span>Choose color...</span></div></div><div class="clear"></div>
                    	</div>
						<div class="formRow">
                        	<div class="grid3"><label>Background picker:</label></div>
                        	<div class="grid9"><div class="cPicker" id="cPicker2"><div style="background-color: #e62e90"></div><span>Choose color...</span></div></div><div class="clear"></div>
                   		</div>
                   		<div class="formRow">
                            <div class="grid3"><label>Background Image:</label> </div>
                            <div class="grid5">
                                <div class="uploader" id="uniform-fileInput">
                                	<input type="file" id="fileInput" class="fileInput" size="24" style="opacity: 0;">
                                	<span class="filename" style="-moz-user-select: none;">No file selected</span>
                                	<span class="action" style="-moz-user-select: none;">Choose File</span>
                                </div>
                            </div>
                            <div class="grid4 enabled_disabled">
                                <div class="floatL mr10"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                   		<div class="formRow">
                        	<div class="grid3"><label>Background Image Repeat:</label></div>
                        	<div class="grid9 yes_no">
                                <div class="floatL mr10"><input type="checkbox" id="check6" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                   		</div>
                    </div><!-- //section -->
         </div> <!-- //end fluid -->
         <div class="fluid">           
                    <!-- section -->
                    <div class="widget grid6 ">
                        <div class="whead"><h6>Advanced Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid12"><label>Advanced CSS:</label></div>
                        	<div class="grid9">
                        		<form><textarea id="code" name="code">
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
}</textarea></form>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
                    <!-- section -->
                    <div class="widget grid6 ">
                        <div class="whead"><h6>Additional Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid12"><label>Use Custom Form or Your Own Autoresponder:</label></div>
                        	<div class="grid9">
                        		<form><textarea id="code2" name="code2">
<form action="custom-form.php">
	<input type="text" name="name" />
	<input  type="submit" value="submit" />
</form></textarea></form>
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
			</div> <!-- //end fluid -->

<!-- //end content container -->
</div>
<!-- //end content container -->


<script type="text/javascript">
	
	//===== Image gallery control buttons =====//
	$(".gallery ul li").hover(
		function() { $(this).children(".actions").show("fade", 200); },
		function() { $(this).children(".actions").hide("fade", 200); }
	);	
	
	//===== Tooltips =====//
	$('.tipN').tipsy({gravity: 'n',fade: true, html:true});
	
	
	//===== Tabs =====//
	$.fn.contentTabs = function(){ 
	
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};
	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	//===== Color picker =====//
	$('#cPicker1').ColorPicker({
		color: '#e62e90',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#cPicker1 div').css('backgroundColor', '#' + hex);
		}
	});
	
	$('#cPicker2').ColorPicker({
		color: '#e62e90',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#cPicker2 div').css('backgroundColor', '#' + hex);
		}
	});
	
	$('#flatPicker').ColorPicker({flat: true});
	
	
	//===== iButtons =====//
	$('.enabled_disabled :checkbox, .enabled_disabled :radio').iButton({
		labelOn: "Enabled",
		labelOff: "Disabled",
		enableDrag: false
	});
	
	$('.yes_no :checkbox, .yes_no :radio').iButton({
		labelOn: "Repeat",
		labelOff: "No Repeat",
		enableDrag: false
	});
	
	//===== Chosen plugin =====//
	$(".select").chosen();
	
	//===== Style Form Elements =====//
	$("select").uniform();
	
	//===== Code Mirror / Syntax highlight =====//
	var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true
      });
    
    var editor2 = CodeMirror.fromTextArea(document.getElementById("code2"), {
        lineNumbers: true
      });
			
</script>
	