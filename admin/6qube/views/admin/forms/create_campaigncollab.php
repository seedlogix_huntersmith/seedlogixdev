<?php $is_scope_campaign = ($collab->scope == CampaignCollab::SCOPE_CAMPAIGN); ?>
<ul id="error"></ul>

<form name="create_campaign" class="ajax" method="post" action="<?php echo $this->action_url('Notifications_save'); ?>">
<input type="hidden" name="campaigncollab[id]" value="<?php echo $collab->id; ?>" />
<input type="hidden" name="campaigncollab[campaign_id]" value="<?php echo $_SESSION['campaign_id']; ?>" />

    <h2>Name</h2>
    <input type="text" name="campaigncollab[name]" onmousemove="focus()" value="<?php
        $this->p($collab->name); ?>" />
    <br clear="all" />

    <h2>Email Address</h2>
    <input type="text" name="campaigncollab[email]" onmousemove="focus()" value="<?php
        $this->p($collab->email); ?>"/>
    <br clear="all" />

    <h2>Scope</h2>
    <?php /* */ ?>
    <fieldset style="width:20%; float:left;">
        <label><input type="radio" class="cc_scope" name="campaigncollab[scope]" value="all" <?php
            $this->checked($is_scope_campaign); ?>/> All Websites (Campaign Level)</label>
        <label><input type="radio" class="cc_scope" name="campaigncollab[scope]" value="" <?php
            $this->checked(!$is_scope_campaign); ?>/> Specific websites.</label>
    </fieldset>
    <? /* / ?>
    
    <select class="uniformselect" name="ff" class="cc_scope" title="Scope">
        <optgroup label="Campaign Level">
            <option value="">All websites.</option>
        </optgroup>
        <optgroup label="Website Level">
            <?php foreach($websites as $website): ?>
            <option value="<?php echo $website->id; ?>" selected=""><?php
                echo $website->name; ?>
            </option>
            <?php endforeach; ?>
        </optgroup>
    </select>
    <?php /* */ ?>
    <fieldset id="scope_sites" style="<?php
        if($is_scope_campaign): ?>display:none;<?php endif;
            ?>float:left; width:75%;">
        <?php   // deb($website_active); 
                foreach($websites as $website): ?>
        <label><input type="checkbox" name="campaigncollab[sites][]" value="<?php
            echo $website->id; ?>" id="checkbox_site_<?php echo $website->id; ?>"
            <?php $this->checked(@$website_active[$website->id]['is_active'] == 1); ?>>
            <?php
                echo $website->name;
            ?>
        </label>
        <?php endforeach; ?>
    </fieldset>    
    
    <LABEL><input type="checkbox" value="1" name="campaigncollab[active]" <?php
	echo ($collab->active != 0 ? ' checked="checked"' : ''); ?>/> Active</label>

    <br clear="all" />

    <input type="submit" name="submit" value="Save" />
</form>

<script type="text/javascript">
    $(".uniformselect").uniform();
    $('.cc_scope').change(function (){
       var siteid = $(this).val();
       
//       console.log(siteid);
       
        $(this).parent().siblings().before($(this).parent());
        
       if(siteid == 'all')
       {
           $('#scope_sites').fadeOut();
           return;
       }
       $('#scope_sites').fadeIn();
       $('#scope_sites input:checkbox').removeAttr('checked');
       $('#checkbox_site_' + siteid).attr('checked', 'checked');
       
    });
</script>

<style>
    fieldset {
        border: none;
    }
    </style>
