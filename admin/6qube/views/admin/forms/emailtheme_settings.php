<?php
    $editor =   0;
    foreach ($ThemeDefinition as $varname => $input) {
        
        $value  =   htmlentities(isset($ThemeConfig[$varname]) ? $ThemeConfig[$varname] : @$input['default'], ENT_QUOTES);
        
        switch (@$input['type']) {
            case 'blog-select':
                
                //include classes
        	require_once QUBEPATH . '../inc/blogs.class.php';
                
                ?><li><label><?php echo $input['name']; ?> | <?php echo $input['description']; ?></label>
                    <?php
                        $PDO    =   Qube::GetPDO();
                        $Q      =   $PDO->query('SELECT * FROM blogs WHERE trashed = "0000-00-00" AND cid = ' . (int)$_SESSION['campaign_id'] . ' AND user_id = ' . 
                                        (int)$_SESSION['user_id']);
                        $blogs  =   $Q->fetchAll();
                        ?><select name="theme[<?php echo $varname; ?>]" size="1" class="updateme">
                            <option value="0">Please choose an option</option>
                            <?php
                        foreach($blogs as $blog):
                            ?>
                            <option value="<?php echo $blog['id']; ?>" <?php if($blog['id'] == $value) echo ' selected="selected"'; ?>><?php echo htmlentities($blog['blog_title']); ?></option>
                            <?php endforeach; ?>
                            </select>                    
                </li>
                <?php
                
                break;
            case 'html':
                $editor++;
                ?><li>
                    <label><?php echo $input['name']; ?></label>
                    <a href="#" id="<?php echo $editor; ?>" class="tinymce"><span>Rich Text</span></a>			
                    <textarea name="theme[<?php echo $varname; ?>]" rows="6" id="edtr<?php echo $editor; ?>" class="approved updateme"><?php
                        echo $value;
                    ?></textarea>
                    <br style="clear:left">
                </li>
                <?php
                break;

            case 'logo':
                ?></ul></form>
                <form enctype="multipart/form-data" method="POST" target="uploadIFrame" action="<?php echo $this->action_url('EmailTheme_upload', array('id' => $Responder->id,
                        'varname' => $varname)); ?>">
                    <label><?php echo $input['name']; ?> | <span style="color:#666; font-size:75%"><?php echo $input['description']; ?></span></label>
								<input type="file" class="upload_logo" name="theme[<?php echo $varname; ?>]" />
                                                                
                    <input type="submit" class="hideMe" value="Upload!">
<br />
                    <img id="bg_img_prvw" class="img_cnt" src="<?php if(!$value) echo 'img/no-photo.jpg'; else echo $value; ?>" style="max-width:300px;max-height:300px;"  /></form>
                <form class="ctrlr_form jquery_form" class="upload_form"><ul>
                <?php
                break;
            case 'color':
                ?><li>
                    <label><?php echo $input['name']; ?></label>
                    <div style="color:#888;font-size:12px; padding-bottom:5px;"><?php echo @$input['description']; ?></div>
                    <input type="text" name="theme[<?php echo $varname; ?>]" value="<?php echo $value; ?>" class="approved updateme">
                </li>
                <?php
                break;
            
            case 'text':
                ?><li>
                    <label><?php echo $input['name']; ?></label>
                    <div style="color:#888;font-size:12px; padding-bottom:5px;"><?php echo @$input['description']; ?></div>
                    <input type="text" name="theme[<?php echo $varname; ?>]" value="<?php echo $value; ?>" class="approved updateme">
                </li>
                <?php
                break;
            default:
        }
    }
    
    