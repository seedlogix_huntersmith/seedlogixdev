<script type="text/javascript">
$(function(){
	
	$(".uniformselect, input[type=file]").uniform();
	
	$(".theme-inline-open").fancybox({
		'width'			: '100%',
		'height'			: '100%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	
	$(".previewEmail").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	
});
</script>
<?php
    $this->getNav('autoresponders.php');
?>
<br />
<a href="<?php echo $this->action_url('EmailTheme_inline', array('id' => $Responder->id)); ?>"
    class="greyButton theme-inline-open" style="float:right;margin-bottom:15px;">Inline Editor</a>
    
<h1>Select a Theme</h1>
<style>
    .theme-thumbnails li{
        display: block; float:left;       
        padding: 0 10px;
    }
    .theme-thumbnails li input {
        display: none;
    }
    .theme-thumbnails .current {
        background: #f0c040;
    }
    .theme-thumbnails {
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    
    $(function (){
        $('.upload_logo').change(function (){
            var f   =   this.form;
            $('#wait').show();                    
            f.submit();
            
            $('#uploadIFrame').load(function(){
                $('#wait').hide();
                var src =   ($('img', $(this).contents()).attr('src'));
                $('img', f).attr('src', src);
            });
        });
        
        $('.theme-thumbnailsf a').click(function (){
            return confirm('Are you sure? This will reset the color properties of the current theme.')
        });
    
        $('.ctrlr_form .updateme').blur(function (){
            var senddata=   {};
            senddata[this.name] =   this.value;

            var elm =   $(this);
            $(this).removeClass('approved').removeClass('error');
    //        return;

            $.post('controller.php?ctrl=EmailTheme&action=save&id=<?php echo $Responder->id; ?>', senddata,
                function (data){
                    data    =   $.parseJSON(data);
                    if(!data.success)
                    {
                        elm.addClass('error');
                    }else{
                        elm.addClass('approved');
                    }
                    if(data.action == 'replace')
                    {
                        $(data.container).html(data.message);
                    }
                });
        });
    });
</script>
<form class="ctrlr_form jquery_form" action=""><ul>
        <li><ul class="theme-thumbnails">
                <?php
                
                foreach ($themes as $themeid => $theme) {
                        if(!@$theme['active']) continue;
                        
                    ?><li<?php if ($themeid == $Responder->theme):
                        ?> class="current"<?php endif;
                    ?>><a href="<?php echo $this->action_url('EmailTheme_settheme', array('id' => $Responder->id,
                                'theme' => $themeid)); ?>">
                            <img src="<?php echo Qube::EMAIL_THEMES_URL . $theme['path'] . '/' . $theme['thumbnail'];
                    ?>" width="80" height="80" /></a><br />
                        <p><?php echo $themeid; ?></p>
                    </li>
                                 <?php
                             }
                             ?></ul></li>

        <?php
        $this->getForm('emailtheme_settings.php');
        ?></ul></form>

<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>