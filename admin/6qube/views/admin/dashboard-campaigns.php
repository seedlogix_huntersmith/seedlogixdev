<h1 class="left">Campaigns Dashboard</h1>

<ul class="ov_boxes right">
	<li>
		<div class="p_line_up p_canvas">
			<span class="sparkline">3,5,9,7,12,8,16</span>
		</div>
		<div class="ov_text">
			<strong>2304</strong>
			Total visitors (last week)
		</div>
	</li>
	<li>
		<div class="p_line_down p_canvas">
			<span class="sparkline">20,16,14,18,15,14,14,13,12,10,10,8</span>
		</div>
		<div class="ov_text">
			<strong>30240</strong>
			Total Leads (last week)
		</div>
	</li>
</ul>

<br class="clear" /><br />

<!-- #### Graph Drop Down Options #### -->

<br class="clear" />

<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads for all campaigns</h6>
		<div class="right">
<span>Show the last</span>
<select name="select2">
	<option value="opt2">2</option>
	<option value="opt3">3</option>
	<option value="opt4">4</option>
	<option value="opt5">5</option>
	<option value="opt6">6</option>
	<option value="opt7">7</option>
	<option value="opt8">8</option>
	<option value="opt8">9</option>
	<option value="opt8">10</option>
</select>

<select name="select3">
	<option value="opt2">Days</option>
	<option value="opt3">Weeks</option>
	<option value="opt4">Months</option>
	<option value="opt5">Years</option>
</select>
</div>
		<div class="clear"></div>
	</div>
	<div class="body"><div class="chart"></div></div>
</div>

<!-- ##### Success Note ##### -->
<div class="nNote nSuccess" style="visibility:hidden">
	<p>Success message! hoooraaay!!!!</p>
</div>

<!-- ##### CAMPAIGNS TABLE ##### -->           
<div class="widget">
            <div class="whead">
            	<h6>Campaigns</h6>
            	<div class="buttonS bLightBlue right dynRight" id="exportCampaign">Export</div>
            	<div class="buttonS bGreen right dynRight" id="createCampaign">Create Campagin</div>
            	<div class="clear"></div>
            </div>
            <div id="dyn" class="hiddenpars">
                <div class="tOptions" title="Options"><img srcf="images/icons/options" alt="" /></div>
                <table cellpadding="0" cellspacing="0" border="0" class="dTable" id="dynamic">
                <thead>
                <tr>
                	<th>Campaign Name <span class="sorting" style="display: block;"></span></th>
                	<th>Total Visits</th>
                	<th>Total Leads</th>
                	<th>Conversion Rate</th>
                	<th>Touchpoints</th>
                	<th>Campaign Creation Date</th>
                	<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr class="gradeA">
                	<td><a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">6Qube.com</a></td>
                	<td class="center">234</td>
                	<td class="center">34</td>
                	<td class="center">4%</td>
                	<td class="center">4</td>
                	<td class="center">January 1st, 2013</td>
                	<td class="center">
                		<a class="tipN" href="#" original-title="Edit Campaign">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Campaign">
                			<span class="icos-cross"></span>
                		</a>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td><a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">Corfro.com</a></td>
                	<td class="center">4545</td>
                	<td class="center">87</td>
                	<td class="center">5%</td>
                	<td class="center">4</td>
                	<td class="center">January 3rd, 2013</td>
                	<td class="center">
                		<a class="tipN" href="#" original-title="Edit Campaign">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Campaign">
                			<span class="icos-cross"></span>
                		</a>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td><a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">Google.com</a></td>
                	<td class="center">54</td>
                	<td class="center">12</td>
               		<td class="center">80%</td>
               		<td class="center">8</td>
                	<td class="center">January 5th, 2013</td>
                	<td class="center">
                		<a class="tipN" href="#" original-title="Edit Campaign">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Campaign">
                			<span class="icos-cross"></span>
                		</a>
                	</td>
                </tr>
                </tbody>
                </table> 
            </div>
            <div class="clear"></div> 
        </div>
<br class="clear"><br /><br />

<!-- #### rankings data #### -->
<div class="fluid">
	<div class="widget grid12">
    	<div class="whead">
    		<h6>Total Touchpoints</h6><div class="clear"></div>
    	</div>
        <div class="body">
        	<h1 class="center">Total Touchpoint Volume</h1>
            <p class="center">for Aug 14, 2012</p>
            <br class="clear" /><br />
            <div class="span3special">
         		<h4>Websites</h4>
         		<span class="value">2</span>
         	</div>
         	<div class="span3special">
         		<h4>Blogs</h4>
         		<span class="value">3</span>
        	</div>
         	<div class="span3special">
         		<h4>Landing Pages</h4>
         		<span class="value">6</span>
       		</div>
       		<div class="span3special">
         		<h4>Socail Sites</h4>
          		<span class="value">5</span>
          	</div>
          	<div class="span3special">
         		<h4>Mobile Sites</h4>
          		<span class="value">7</span>
          	</div>
          	<br class="clear" /><br />	
		</div>
	</div>
</div>
<br class="clear" /><br />
<!-- Create Campaign POPUP -->
<div id="mwCreateCamp" class="SQmodalWindowContainer" title="Create New Campaign">
	<p class="validateTips" style="display:none">All form fields are required.</p>
	<form id="createCampForm" action="add_campaign.php">
	<fieldset>
		<label for="campaign_name">Campaign Name</label>
		<input type="text" name="campaign_name" id="campaign_name" class="text ui-widget-content ui-corner-all" />
		<input type="hidden" value="-1" name="camp_limit">
	</fieldset>
	</form>
</div>

<!-- Export Modal POPUP -->
<div id="mwExport" class="SQmodalWindowContainer" title="Export Campaign Data">
	Export CSV
	
	<div class="divider"><span></span></div>
	
	<p class="validateTips" style="display:none">All form fields are required.</p>
	<form id="exportCampForm" action="export_campaign.php">
	<fieldset>
		<label for="export_to">To</label>
		<input type="text" name="campaign_name" id="campaign_name" class="text ui-widget-content ui-corner-all" />
		
		<label for="export_from">From</label>
		<input type="text" name="campaign_name" id="campaign_name" class="text ui-widget-content ui-corner-all" />
		
		<label for="export_msg">Textarea:</label>
		<textarea placeholder="This textarea is elastic" class="auto" name="textarea" cols="" rows="8" style="overflow: hidden; height: 122px;"></textarea>
	</fieldset>
	</form>
</div>

<!-- End HTML -->
<?
	/*
		resellerID
		period { day, week, month, year, range }
		date {standard format = YYYY-MM-DD. magic keywords = today or yesterday.}
		format {xml, json, html, php}
	*/
	$data_array	= $camp->ANAL_getVisitsForCampaign($resellerParentID, 'lastWeek');	
?>

<script type="text/javascript">
	
	//===== Create Dashboard Graph =====//
	createDashBoard(<?=json_encode($data_array[0]);?>, <?=json_encode($data_array[1]);?> );
	
	
	//===== Dynamic data table =====//
	oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});
	
	//===== Dynamic table toolbars =====//		
	$('#dyn .tOptions').click(function () {
		$('#dyn .tablePars').slideToggle(200);
	});	
	
	$('#dyn2 .tOptions').click(function () {
		$('#dyn2 .tablePars').slideToggle(200);
	});	
	
	$('.tOptions').click(function () {
		$(this).toggleClass("act");
	});
	
	
	//===== Tooltips =====//
	$('.tipN').tipsy({gravity: 'n',fade: true, html:true});
	
	
	//===== Style Form Elements =====//
	$("select").uniform();
	
	
	//===== Create Campaign Form =====//
	var name = $( "#campaign_name" ),
		allFields = $( [] ).add( name ),
		tips = $( ".validateTips" );
	
	function updateTips( t )
	{
		
		tips.text( t ).addClass( "ui-state-highlight" ).show();

		setTimeout(function()
		{
			tips.removeClass( "ui-state-highlight", 1500 );
		}, 500 );
		
	}
	
	function checkLength( o, n, min, max )
	{
		if ( o.val().length > max || o.val().length < min )
		{
			o.addClass( "ui-state-error" );
			updateTips( "Length of " + n + " must be between " + min + " and " + max + "." );
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function checkRegexp( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}
	
	//===== Export Campaign Form =====//
	var exFrom = $( "#export_from" ),
		exTo = $( "#export_to" ),
		exMessage = $( "#export_msg" ),
		allFields = $( [] ).add( name ),
		tips = $( ".validateTips" );
	
	//===== Export Dialog =====//
	$( "#mwExport" ).dialog({
		autoOpen: false,
		height: 500,
		width: 550,
		modal: true,
		buttons: {
			"Export Campaign Data": function() {
				var bValid = true;
				allFields.removeClass( "ui-state-error" );
				
				/***********
				// VALIDATION CHECKS - NOT SURE ON THE RULES 
				
				bValid = bValid && checkLength( name, "Campaign Name", 3, 30 );
				
				bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Campaign Name may consist of a-z, 0-9, underscores, and begin with a letter." );
				***********/
					if ( bValid )
				{
					var campForm = $('#createCampForm');
					
					params = campForm.serialize();
					
					$.post(campForm.attr("action"), params, function(data)
					{
						json = $.parseJSON(data);
			
						//SUCCESS
						if(json.success)
						{
							$('#campTable').text(json.success.message);
							//$(json.success.container).find(".item:first").slideDown();
							
						}
						//ERROR
						else if(json.error.message)
						{
							if(json.error.type = 'displayMessage')
							{
								$(json.error.container).html(json.error.message);
								$(json.error.container).show();
							}	
						}
						else
						{
							alert('There was an error creating your item.\nPlease reload the page and try again.  Contact support if the problem persists.');
						}
					});
											
					$( this ).dialog( "close" );
					}
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	
	$( "#exportCampaign" ).click(function() {
		$( "#mwExport" ).dialog( "open" );
	});	
			
	//===== Create Campaign Dialog =====//
		$( "#mwCreateCamp" ).dialog({
			autoOpen: false,
			height: 200,
			width: 550,
			modal: true,
			buttons: {
				"Create a Campaign": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );
					
					/***********
					// VALIDATION CHECKS - NOT SURE ON THE RULES 
					
					bValid = bValid && checkLength( name, "Campaign Name", 3, 30 );
					
					bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Campaign Name may consist of a-z, 0-9, underscores, and begin with a letter." );
					***********/

					if ( bValid )
					{
						var campForm = $('#createCampForm');
						
						params = campForm.serialize();
						
						$.post(campForm.attr("action"), params, function(data)
						{
							json = $.parseJSON(data);
				
							//SUCCESS
							if(json.success)
							{
								$('#campTable').text(json.success.message);
								//$(json.success.container).find(".item:first").slideDown();
								
							}
							//ERROR
							else if(json.error.message)
							{
								if(json.error.type = 'displayMessage')
								{
									$(json.error.container).html(json.error.message);
									$(json.error.container).show();
								}	
							}
							else
							{
								alert('There was an error creating your item.\nPlease reload the page and try again.  Contact support if the problem persists.');
							}
						});
						
						$( this ).dialog( "close" );
					}
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#createCampaign" ).click(function() {
				$( "#mwCreateCamp" ).dialog( "open" );
		});
		

</script>
