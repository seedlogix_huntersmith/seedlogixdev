<!-- LEFT SIDE NAV -->
<div class="mainNav">
	<!-- Main nav -->
	<ul class="nav">
		<li><a title="" href="<?php echo $this->action_url('Website_settings', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/dashboard.png"><span>Settings</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_theme', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/ui.png"><span>Theme</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_seo', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/forms.png"><span>SEO</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_editRegions', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/messages.png"><span>Edit Regions</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_photos', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/statistics.png"><span>Photos</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_social', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/tables.png"><span>Social</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_pages', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/other.png"><span>Pages</span></a></li>
	</ul>
</div>

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->


<h1>{Website} Settings</h1>

<br class="clear" /><br />

			<div class="fluid">
                    <!-- section -->
                    <div class="widget grid6">
                        <div class="whead"><h6>Main Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Company Name:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Phone Number:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Address:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>City:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>State:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Zip Code:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                                           
                    <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Brand Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Company Logo:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Slogan/Tagline:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Website Description:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
                    <!-- section -->
                    <div class="widget grid6 widgetpadding">
                        <div class="whead"><h6>Domain Settings</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Domain:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
			</div> <!-- //end fluid -->
	
<!-- //end content container -->
</div>
<!-- //end content container -->