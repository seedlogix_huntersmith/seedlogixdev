<?php

    // load file from navs/ directory
    $this->getNav('campaign-collabs.php');
    


    foreach($items as $collab):
        
        ?>
<ul class="prospects" title="contact" id="prospect5157">
		<li class="name" style="width:28%;"><?php $this->p($collab->name); ?></li>
		<li class="email" style="width:22%;"><i><?php $this->p($collab->email); ?></i></li>
		<li class="email" style="width:20%;"><i><?php echo $collab->sites; ?></i></li>
		<li class="phone" style="width:10%;"><i><?php echo $collab->active; ?></i></li>
		<li class="details" style="width:5%;"><a href="<?php
                
                    echo $this->action_url('Notifications_edit', array('id' => $collab->id));
                    
                ?>"><img src="img/v3/view-details.png" border="0"></a></li>
                
		<li class="delete" style="width:5%;"><a href="<?php
                
                    echo $this->action_url('Notifications_delete', array('id' => $collab->id));
                    
                ?>" class="delete rmParent"
                                       rel="">
                        <img src="img/v3/x-mark.png" border="0" /></a></li>
	</ul>
<?php endforeach; ?>