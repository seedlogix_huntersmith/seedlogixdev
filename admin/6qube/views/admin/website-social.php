<!-- LEFT SIDE NAV -->
<div class="mainNav">
	<!-- Main nav -->
	<ul class="nav">
		<li><a title="" href="<?php echo $this->action_url('Website_settings', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/dashboard.png"><span>Settings</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_theme', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/ui.png"><span>Theme</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_seo', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/forms.png"><span>SEO</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_editRegions', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/messages.png"><span>Edit Regions</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_photos', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/statistics.png"><span>Photos</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_social', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/tables.png"><span>Social</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_pages', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/other.png"><span>Pages</span></a></li>
	</ul>
</div>

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

<h1>{Website} Social Networking</h1>
<br class="clear" /><br />

			<div class="fluid">
			 <!-- section -->               
                    <div class="widget grid6">
                        <div class="whead"><h6>Social Networking Connections</h6><div class="clear"></div></div>
                        <div class="formRow">
                        	<div class="grid3"><label>Your Blog URL:</label></div>
                        	<div class="grid9"><input type="text" name="blog" placeholder="http://www.myblog.com"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>twitter username:</label></div>
                        	<div class="grid9"><input type="text" name="twitter" placeholder="@6qube"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>Facebook:</label></div>
                        	<div class="grid9"><input type="text" name="facebook" placeholder="http://www.facebook.com/6qube"></div>
                        	<div class="clear"></div>
                    	</div>
                    	<div class="formRow">
                        	<div class="grid3"><label>linkedIn:</label></div>
                        	<div class="grid9"><input type="text" name="linkedin" placeholder="www.linkedin.com/in/jonathanaguilar"></div>
                        	<div class="clear"></div>
                    	</div>
                    </div><!-- //section -->
                    
			</div> <!-- //end fluid -->

<!-- //end content container -->
</div>
<!-- //end content container -->