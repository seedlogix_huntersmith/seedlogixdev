<!-- LEFT SIDE NAV -->
<div class="mainNav">
	<!-- Main nav -->
	<ul class="nav">
		<li><a title="" href="<?php echo $this->action_url('Website_settings', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/dashboard.png"><span>Settings</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_theme', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/ui.png"><span>Theme</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_seo', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/forms.png"><span>SEO</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_editRegions', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/messages.png"><span>Edit Regions</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_photos', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/statistics.png"><span>Photos</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_social', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/tables.png"><span>Social</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_pages', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/other.png"><span>Pages</span></a></li>
	</ul>
</div>

<!-- //start content container -->
<div id="twoColContentArea" class="editRegions">
<!-- //start content container -->

<h1>{Website} Photos</h1>
<br class="clear" /><br />

<h2>Example of New Photo layout</h2>

<h2>Example of Old Photo layout</h2>
<br class="clear" /><br />
	
	<div class="fluid">
				
				<div class="widget grid6">       
                    <div class="whead"><h6>Image Info</h6><div class="clear"></div></div>
                        
                        <div class="formRow">
                        	<div class="grid3"><label>Photo Title:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                        	<div class="grid3"><label>Photo Link:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                            <div class="grid3"><label>Background Image:</label> </div>
                            <div class="grid5">
                                <div class="uploader" id="uniform-fileInput">
                                	<input type="file" id="fileInput" class="fileInput" size="24" style="opacity: 0;">
                                	<span class="filename" style="-moz-user-select: none;">No file selected</span>
                                	<span class="action" style="-moz-user-select: none;">Choose File</span>
                                </div>
                            </div>
                            <div class="grid4 enabled_disabled">
                                <div class="floatL mr10"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                        </div>
				</div>
				
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb1">Image Description <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb2">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb1" style="display: block;">
                        	<form><textarea id="code1" name="code">
<p>Just a little <strong>description</strong> about my photo.</p></textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb2" style="display: none;">
                        	<textarea id="editor1" name="editor" rows="" cols="16">Just a little description about my photo.</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
	</div>			
	
	<div class="divider"><span></span></div> <br />
	
	<div class="fluid">
				
				<div class="widget grid6">       
                    <div class="whead"><h6>Image Info</h6><div class="clear"></div></div>
                        
                        <div class="formRow">
                        	<div class="grid3"><label>Photo Title:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                        	<div class="grid3"><label>Photo Link:</label></div>
                        	<div class="grid9"><input type="text" name="regular"></div>
                        	<div class="clear"></div>
                    	</div>
                    	
                    	<div class="formRow">
                            <div class="grid3"><label>Background Image:</label> </div>
                            <div class="grid5">
                                <div class="uploader" id="uniform-fileInput">
                                	<input type="file" id="fileInput" class="fileInput" size="24" style="opacity: 0;">
                                	<span class="filename" style="-moz-user-select: none;">No file selected</span>
                                	<span class="action" style="-moz-user-select: none;">Choose File</span>
                                </div>
                            </div>
                            <div class="grid4 enabled_disabled">
                                <div class="floatL mr10"><input type="checkbox" id="check4" checked="checked" name="chbox" /></div>
                            </div>
                            <div class="clear"></div>
                        </div>
				</div>
				
				<div class="widget grid6">       
                    <ul class="tabs hand opened inactive" id="opened">
                        <li class="activeTab"><a href="#tabb3">Image Description <em>{Source Code}</em></a></li>
                        <li class=""><a href="#tabb4">Word Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabb3" style="display: block;">
                        	<form><textarea id="code2" name="code">
<p>Just a little <strong>description</strong> about my photo.</p></textarea></form>
                        
                        	
                        </div>
                        <div class="tab_content" id="tabb4" style="display: none;">
                        	<textarea id="editor3" name="editor" rows="" cols="16">Just a little description about my photo.</textarea>
                        </div>
                    </div>	
                    <div class="clear"></div>		 
                </div>
				
	</div>
	
<!-- photo 1 -->



<!-- photo 2 -->


<!-- //end content container -->
</div>
<!-- //end content container -->


<script type="text/javascript">
	
	//===== Tabs =====//
	$.fn.contentTabs = function(){ 
	
		$(this).find(".tab_content").hide(); //Hide all content
		$(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
		$(this).find(".tab_content:first").show(); //Show first tab content
	
		$("ul.tabs li").click(function() {
			$(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
			$(this).addClass("activeTab"); //Add "active" class to selected tab
			$(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
			var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
			$(activeTab).show(); //Fade in the active content
			return false;
		});
	
	};
	$("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
	
	//===== Collapsible elements management =====//
	

	$('.opened').collapsible({
		defaultOpen: 'opened,toggleOpened',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	$('.closed').collapsible({
		defaultOpen: '',
		cssOpen: 'inactive',
		cssClose: 'normal',
		speed: 200
	});
	
	//===== iButtons =====//
	$('.enabled_disabled :checkbox, .enabled_disabled :radio').iButton({
		labelOn: "Enabled",
		labelOff: "Disabled",
		enableDrag: false
	});
	
	//===== WYSIWYG editor =====//
	$("#editor1").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true,
		controls:     // controls to add to the toolbar
                        "bold italic underline strikethrough subscript superscript | font size " +
                        "style | color highlight removeformat | bullets numbering | outdent " +
                        "indent | alignleft center alignright justify | undo redo | " +
                        "rule image link unlink | cut copy paste pastetext"
	});
	
	$("#editor2").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true,
		controls:     // controls to add to the toolbar
                        "bold italic underline strikethrough subscript superscript | font size " +
                        "style | color highlight removeformat | bullets numbering | outdent " +
                        "indent | alignleft center alignright justify | undo redo | " +
                        "rule image link unlink | cut copy paste pastetext"
	});
/*	
	$("#editor3").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor4").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor5").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor6").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor7").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor8").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor9").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
	
	$("#editor10").cleditor({
		width:"100%", 
		height:"250px",
		bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
		useCSS:true
	});
*/	
	//===== Code Mirror / Syntax highlight =====//
	var editor = CodeMirror.fromTextArea(document.getElementById("code1"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code2"), {
        lineNumbers: true
      });
/*      
    var editor = CodeMirror.fromTextArea(document.getElementById("code3"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code4"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code5"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code6"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code7"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code8"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code9"), {
        lineNumbers: true
      });
      
    var editor = CodeMirror.fromTextArea(document.getElementById("code10"), {
        lineNumbers: true
      });  
*/                    				
</script>