<!-- CONTENT AREA -->
<h1 class="left">{Campaign Name}</h1>

<ul class="ov_boxes right">
	<li>
		<div class="p_line_up p_canvas">
			<span class="sparkline">3,5,9,7,12,8,16</span>
		</div>
		<div class="ov_text">
			<strong>2304</strong>
			Total visitors (last week)
		</div>
	</li>
	<li>
		<div class="p_line_down p_canvas">
			<span class="sparkline">20,16,14,18,15,14,14,13,12,10,10,8</span>
		</div>
		<div class="ov_text">
			<strong>30240</strong>
			Total Leads (last week)
		</div>
	</li>
</ul>

<br class="clear" /><br />

<!-- #### Graph Drop Down Options #### -->

<br class="clear" />

<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads</h6>
		<div class="right">
<span>Show the last</span>
<select name="select2">
	<option value="opt2">2</option>
	<option value="opt3">3</option>
	<option value="opt4">4</option>
	<option value="opt5">5</option>
	<option value="opt6">6</option>
	<option value="opt7">7</option>
	<option value="opt8">8</option>
	<option value="opt8">9</option>
	<option value="opt8">10</option>
</select>

<select name="select3">
	<option value="opt2">Days</option>
	<option value="opt3">Weeks</option>
	<option value="opt4">Months</option>
	<option value="opt5">Years</option>
</select>
</div>
		<div class="clear"></div>
	</div>
	<div class="body"><div class="chart"></div></div>
</div>

<br class="clear" /> <br />

<!-- #### Top 5 Sites Table #### -->
<div class="widget">
            <div class="whead">
            	<h6>Top Performing Touchpoints</h6>
            	<div class="right">
            		<span>Show top 5 for </span>
            		<select name="select2">
            			<option value="opt3">Conversion Rate</option>
						<option value="opt1">Total Visits</option>
						<option value="opt2">Total Leads</option>
					</select>
				</div>
            	<div class="clear"></div>
            </div>
            
            <table width="100%" cellspacing="0" cellpadding="0" class="tDefault">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Type</td>
                        <td>Total Visits</td>
                        <td>Total Leads</td>
                        <td>Total Conversions</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <tr class="topPerforming">
                        <td><a href="#">corfro.com</a></td>
                        <td>Hub</td>
                        <td class="center">234</td>
                        <td class="center">34</td>
                        <td class="center"><strong>90</strong></td>
                        <td class="center">
                        	<a class="tipN" href="#" original-title="View Touchpoint">
                				<span class="icos-create2"></span>
                			</a>
                		</td>
                    </tr>
                    <tr>
                        <td><a href="#">google.com</a></td>
                        <td>Hub</td>
                        <td class="center">234</td>
                        <td class="center">34</td>
                        <td class="center"><strong>30</strong></td>
                        <td class="center">
                        	<a class="tipN" href="#" original-title="View Touchpoint">
                				<span class="icos-create2"></span>
                			</a>
                		</td>
                    </tr>
                    <tr>
                        <td><a href="#">6qube</a></td>
                        <td>Social</td>
                        <td class="center">234</td>
                        <td class="center">34</td>
                        <td class="center"><strong>30</strong></td>
                        <td class="center">
                        	<a class="tipN" href="#" original-title="View Touchpoint">
                				<span class="icos-create2"></span>
                			</a>
                		</td>
                    </tr>
                    <tr>
                        <td><a href="#">6qube</a></td>
                        <td>Blog</td>
                        <td class="center">234</td>
                        <td class="center">34</td>
                        <td class="center"><strong>30</strong></td>
                        <td class="center">
                        	<a class="tipN" href="#" original-title="View Touchpoint">
                				<span class="icos-create2"></span>
                			</a>
                		</td>
                    </tr>
                    <tr>
                        <td class="noBorderB"><a href="#">6qube</a></td>
                        <td class="noBorderB">Blog</td>
                        <td class="noBorderB center">234</td>
                        <td class="noBorderB center">34</td>
                        <td class="noBorderB center"><strong>30</strong></td>
                        <td class="center">
                        	<a class="tipN" href="#" original-title="View Touchpoint">
                				<span class="icos-create2"></span>
                			</a>
                		</td>
                    </tr>
                </tbody>
            </table>
        </div>

<br class="clear"><br />

<!-- ##### CAMPAIGNS TABLE ##### -->          
<div class="widget">
            <div class="whead">
            	<h6>Latest Prospects (last 30 days)</h6>
            	<div class="clear"></div>
            </div>
            <div id="dyn" class="hiddenpars">
                <div class="tOptions" title="Options"><img srcf="images/icons/options" alt="" /></div>
                <table cellpadding="0" cellspacing="0" border="0" class="dTable prospTable" id="dynamic">
                <thead>
                <tr>
                	<th>Name <span class="sorting" style="display: block;"></span></th>
                	<th>Email Address</th>
                	<th>Phone Number</th>
                	<th>Touchpoint Type</th>
                	<th>Touchpoint Name</th>
                	<th>Touchpoint Page</th>
                	<th>Date</th>
                	<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                <tr class="gradeA">
                	<td>
                		<img src="http://6qube.com/admin/img/v3/prospects-icon.png" class="prospect-image" />
                		<a href="<?php echo $this->action_url('Dashboard_campaign', array('id' => '#ID')); ?>">John Doe</a>
                	</td>
                	<td class="center"><span class="prospect-data">john.doe@gmail.com</span></td>
                	<td class="center"><span class="prospect-data">512-456-5455</span></td>
                	<td class="center"><span class="prospect-data">Website</span></td>
                	<td class="center"><span class="prospect-data">corfro.com</span></td>
                	<td class="center"><span class="prospect-data">index.html</span></td>
                	<td class="center"><span class="prospect-data">January 1st, 2013</span></td>
                	<td class="center">
                		<span class="prospect-data">
                		<a class="tipN" href="#" original-title="View Prospect">
                			<span class="icos-document"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Edit Prospect">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Prospect">
                			<span class="icos-cross"></span>
                		</a>
                		</span>
                	</td>
                </tr>
                </tbody>
                </table> 
            </div>
            <div class="clear"></div> 
        </div>


<br class="clear"><br />

<!-- #### rankings data #### -->
<div class="fluid">
	<div class="widget grid12">
    	<div class="whead">
    		<h6>Total Touchpoints for {campaign name}</h6><div class="clear"></div>
    	</div>
        <div class="body">
        	<h1 class="center">Total Touchpoint Volume</h1>
            <br class="clear" /><br />
            <div class="span3special">
         		<h4>Websites</h4>
         		<span class="value">2</span>
         	</div>
         	<div class="span3special">
         		<h4>Blogs</h4>
         		<span class="value">3</span>
        	</div>
         	<div class="span3special">
         		<h4>Landing Pages</h4>
         		<span class="value">6</span>
       		</div>
       		<div class="span3special">
         		<h4>Socail Sites</h4>
          		<span class="value">5</span>
          	</div>
          	<div class="span3special">
         		<h4>Mobile Sites</h4>
          		<span class="value">7</span>
          	</div>
          	<br class="clear" /><br />	
		</div>
	</div>
</div>
<br class="clear" /><br />
<!-- End HTML -->
<?
	/*
		resellerID
		period { day, week, month, year, range }
		date {standard format = YYYY-MM-DD. magic keywords = today or yesterday.}
		format {xml, json, html, php}
	*/
?>

<script type="text/javascript">
	
	//===== Create Dashboard Graph =====//
	createDashBoard(<?=json_encode($data_array[0]);?>, <?=json_encode($data_array[1]);?> );
	
	
	//===== Tooltips =====//
	$('.tipN').tipsy({gravity: 'n',fade: true, html:true});
	
	//===== Dynamic data table =====//
	oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});
	
	//===== Dynamic table toolbars =====//		
	$('#dyn .tOptions').click(function () {
		$('#dyn .tablePars').slideToggle(200);
	});	
	
	$('#dyn2 .tOptions').click(function () {
		$('#dyn2 .tablePars').slideToggle(200);
	});	
	
	$('.tOptions').click(function () {
		$(this).toggleClass("act");
	});
	
	//===== Style Form Elements =====//
	$("select").uniform();
		
	//==== Spark lines ====//
	$('.sparkline').sparkline('html', {
    	type: 'line',
    	width: '100',
    	height: '30'
    });
	
</script>