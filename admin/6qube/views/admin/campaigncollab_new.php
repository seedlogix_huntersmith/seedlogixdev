<?php

    // load file from navs/ directory
    $this->getNav('campaign-collabs.php');
    
?>

<div id="camps">
    
    <div class="icon"><img src="img/v3/campaign-icon.png" width="64" /></div>
	<h1>
            Create a Response Email
	</h1>
	<div class="clear"></div>
        
        <?php
            
            $this->getForm('create_campaigncollab.php');
        
        ?>        
</div>
