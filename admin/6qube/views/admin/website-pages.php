<!-- LEFT SIDE NAV -->
<div class="mainNav">
	<!-- Main nav -->
	<ul class="nav">
		<li><a title="" href="<?php echo $this->action_url('Website_settings', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/dashboard.png"><span>Settings</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_theme', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/ui.png"><span>Theme</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_seo', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/forms.png"><span>SEO</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_editRegions', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/messages.png"><span>Edit Regions</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_photos', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/statistics.png"><span>Photos</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_social', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/tables.png"><span>Social</span></a></li>
		<li><a title="" href="<?php echo $this->action_url('Website_pages', array('id' => '#ID')); ?>"><img alt="" src="images/icons/mainnav/other.png"><span>Pages</span></a></li>
	</ul>
</div>

<!-- //start content container -->
<div id="twoColContentArea">
<!-- //start content container -->

<h1>{Website} Pages</h1>


<!-- //end content container -->
</div>
<!-- //end content container -->