<h1><?php echo $campaign_name; ?> Settings</h1>

<?php
    $curr = array();
    
    $curr[$_GET['action']] = 'class="current"';
?>
<div id="side_nav">
    <ul id="subform-nav">
        <li class="first">
            <a href="<?php
                echo $this->action_url('Notifications_scopecampaign');
                ?>" <?php            
            echo @$curr['scopecampaign']; ?>>Campaign Level</a>
        </li>
        <li>
            <a href="<?php
echo $this->action_url('Notifications_scopewebsite');
?>" <?php
    echo @$curr['scopewebsite']; ?>>Website Level</a>
        </li>
        <li class="last">
            <a href="<?php
echo $this->action_url('Notifications_settings');
?>" <?php
    echo @$curr['settings']; ?>>Create a new Response Email </a>
        </li>
    </ul>
</div>
<br style="clear:left;"  /><br />
