<?php
/**
 * this file is included in reseller/reseller_site.php and in hubs/hub.php (both of those files should be combined into one. didnt want to copy/paste this entire block.)
 * so I am putting it here until further notice.
 * 
 */
if(isset($_GET['page']))
    {

    switch($_GET['page'])
    {
        case 'client-optout':
            require_once 'admin/6qube/core.php';
            require_once QUBEPATH . '../classes/VarContainer.php';
            require_once QUBEPATH . '../classes/QubeDataProcessor.php';
            require_once QUBEPATH . '../classes/ContactFormProcessor.php';
            require_once QUBEPATH . '../models/Response.php';
            require_once QUBEPATH . '../classes/Notification.php';
            require_once QUBEPATH . '../classes/NotificationMailer.php';
            require_once QUBEPATH . '../models/Responder.php';
            require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';
            require_once QUBEPATH . '../classes/OptOutProcessor.php';                    

            $optout_result = OptoutProcessor::OPTOUT_NONE;

            if(isset($_GET['result']))
                OptoutProcessor::setResult($_GET['result']);
            else
                OptoutProcessor::setResult(0);

            if($_POST){
                    $email = $hub->validateEmail($_POST['email']);
                    if($email){
                            if($_POST['pid'] && is_numeric($_POST['pid']))
                            {
                                //@note updating both tables because we're not sure which table the contact info came from.
                                //@todo improve
                                
                                $D = Qube::GetDriver();
                                $Q = new DBUpdateQuery(Qube::GetPDO());
                                $Q2 = new DBUpdateQuery(Qube::GetPDO());
                                
                                // ridiculous.. can't even reuse the same query because of differences in table structure (email vs lead_email)
                                
                                $Q->Table('contact_form')->Set('optout = 1')->Where('id = ? AND email = ?');
                                $Q2->Table('leads')->Set('optout = 1')->Where('id = ? AND lead_email = ?');
                                
                                $stmt = $Q->Prepare();                                
                                $stmt2 = $Q2->Prepare();
                                
                                $result1 = $stmt->execute(array($_POST['pid'], $email));
                                $result2 = $stmt2->execute(array($_POST['pid'], $email));
                                
                                if($result1 || $result2)
                                {
                                    $optout_result = OptoutProcessor::OPTOUT_OK;
//                                            echo '<b>You were successfully removed from our client\'s mailing list.</b>';
                                }else
                                    $optout_result = OptoutProcessor::OPTOUT_FAILED;
//                                            else echo '<b>Internal error.</b>  Try the opt-out link in the email again or <a href="mailto:support@6qube.com">email us</a> to be manually removed.  We apologize for the inconvenience.<br />';
                            }else
                                    $optout_result = OptoutProcessor::OPTOUT_INVALID;
//                                    else echo '<b>Internal error.</b>  Try the opt-out link in the email again or <a href="mailto:support@6qube.com">email us</a> to be manually removed.<br />';
                    }
                        else $optout_result = OptoutProcessor::OPTOUT_INVALID;

//                            else echo '<b>Valid email address is required!</b><br />';

                    Header('Location: /?page=client-optout&result=' . $optout_result . '&pid=' . (int)$_REQUEST['pid']);
                    exit;
            }

            require_once OptoutProcessor::getIncludeFile($themepath); // improvised fallback mechanism. ideally we need a templating engine!
            exit;
            // end client-optout
            break;                
    }
}
