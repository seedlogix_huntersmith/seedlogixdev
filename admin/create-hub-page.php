<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/hub.class.php');
	$hub = new Hub();
	
	$title = isset($_POST['title']) ? $hub->sanitizeInput($_POST['title']) : "";
	$id = isset($_POST['hub_id']) ? $_POST['hub_id'] : "";
	$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";
	$multi_user = isset($_POST['multi_user']) ? 1 : 0;
	$class = isset($_POST['inputClass']) ? $_POST['inputClass'] : "";
	if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	else $parent_id = $_SESSION['parent_id'];
	
	if($title){
		$page = array('page_title'=>$title, 'inc_contact'=>1, 'nav'=>1);
		
		$page_id = $hub->addNewPage($id, $page, $user_id, $parent_id);
		//if user is a reseller adding a new page to a multi-user hub theme
		if($multi_user){
			//get theme ID from class
			$a = array();
			preg_match('/-[0-9]{1,4}n/', $class, $a);
			$themeID = substr($a[0], 1, -1);
			$result = $hub->pushNewMultiUserHubPage($themeID, $title, $_SESSION['login_uid'], $page_id);
		}
		
		if((!$multi_user && $page_id) || ($result && $page_id)){
			$response['error'] = 0;	
			$response['page_id'] = $page_id;
		} else {
			$response['error'] = 1;
			$response['message'] = '';//'error: '.mysqli_error();
			//if(!$result1) $response['message'] .= '\nresult1 error';
			//if(!$result2) $response['message'] .= '\nresult2 error: '.$result2;
			//if(!$page_id) $response['message'] .= '\npage_id error';
		}
	}

	echo json_encode($response);
?>