//Please do not delete this file, it will be used to add phone numbers once PATLive updates their API

<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/auth.php');
	require_once('./inc/local.class.php');
	$p = new Local();
	
	$cid = $_SESSION['campaign_id'];	
	$query = "SELECT * FROM phone_numbers WHERE cid = '".$cid."'";
	
	$result = $p->query($query);
	if($p->numRows($result)){
?>
	<div id="side_nav">
		<ul id="subform-nav">
			<li class="first">
				<a href="phone_view_call_report.php">Call Reporting</a>
			</li>
			<li class="last">
				<a href="#">Settings</a>
			</li>		
		</ul>
	</div>
<?php
    } 
	else{
?>

<h1>Add Phone Number</h1>

<script type="text/javascript">
    $('.uniformselect').uniform();
    $('#state_select').parent().hide();
    $('#city_select').parent().hide();
	$('#createPhone').delegate('#area_code_type', 'change', function(){
		if($('#area_code_type').attr("selectedIndex") == 0){
    		return false;
    	}
		$('#wait').show();		
		$('#state_select').empty();
		$('#toll_free_select').empty();
		$('#available_numbers').empty();
		var areaCodeType = $('#area_code_type').val();
		if(areaCodeType == "Local"){
			$('#state_select').parent().show();
			$('#toll_free_select').parent().hide();
			$('#state_select').prev().text('Select an State');
		}
		else if (areaCodeType == "Toll Free"){
			$('#state_select').parent().hide();
			$('#city_select').parent().hide();
			$('#toll_free_select').parent().show();	
			$('#toll_free_select').prev().text('Select an Area Code');
			$('#toll_free_select').append('<option>Select an Area Code</option>');	
		}
		$.ajax({
			type:'GET',
			url: 'phone_auth.php?area_code_type=' + areaCodeType,
			success: function(d){
				var myJSON = $.parseJSON(d);
				var items = myJSON.Data;
				$.each(items, function(i){
					if(areaCodeType == 'Local'){							
						var curItem = items[i].StateName;
						var curItemAbbr = items[i].StateAbbreviation;
						var domNode = $('<option value="' + curItemAbbr + '">' + curItem + '</option>');
						$('#state_select').append(domNode);
						$(domNode).data(items[i]);
					}
					else{							
						var curItem = items[i].AreaCode;
						var domNode = $('<option>' + curItem + '</option>');
						$('#toll_free_select').append(domNode);
						$(domNode).data(items[i]);
					}
				});

				$('#wait').hide();
			}
		}); 
	});
	
	$('#createPhone').delegate('#toll_free_select', 'change', function(){
		if($('#area_code_type').attr("selectedIndex") == 0){
    		return false;
    	}
    	$('#available_numbers').empty();
    	$('#wait').show();
		var tfAreaCode = $('#toll_free_select').val();
		$.ajax({
			type: 'GET',
			url: 'phone_auth.php?tf_area_code=' + tfAreaCode,
			success: function(d){
				var myJSON = $.parseJSON(d);
				if(!myJSON.Data[0]){
					$('#available_numbers').prev().text('No Numbers Available');
				}
				else{
					$('#available_numbers').prev().text('Select a Number');
					$('#available_numbers').append('<option>Select a Number</option>');					
					var items = myJSON.Data;				
					$.each(items, function(i){				
						var curItem = items[i].PhoneNumberFormatted;
						var phoneNum = items[i].PhoneNumber;
						var domNode = $('<option value="' + phoneNum + '">' + curItem + '</option>');
						$('#available_numbers').append(domNode);
						$(domNode).data(items[i]);						
					});
				}
				$('#wait').hide();
			}
		});	
	});
	
	$('#createPhone').delegate('#state_select', 'change', function(selectedItem){
		if($('#state_select').attr("selectedIndex") == 0){
    		return false;
    	}
    	$('#wait').show();
    	$('#city_select').empty();
    	$('#city_select').parent().show();
    	$('#city_select').prev().text('Select a City');
    	var stateAbbr = $('#state_select').val();
    	$.ajax({
			type: 'GET',
			url: 'phone_auth.php?state_abbr=' + stateAbbr,
			success: function(d){
				var myJSON = $.parseJSON(d);
				var items = myJSON.Data;				
				$.each(items, function(i){				
					var curItem = items[i].City;
					var curItemNPA = items[i].NPA;
					var domNode = $('<option>' + curItem + ' (' + curItemNPA + ')</option>');
					$('#city_select').append(domNode);
					$(domNode).data(items[i]);						
				});
				$('#wait').hide();
			}
		});	
	});
	
	$('#createPhone').delegate('#city_select', 'change', function(selectedItem){
		if($('#state_select').attr("selectedIndex") == 0){
    		return false;
    	}
    	$('#wait').show();
    	$('#available_numbers').empty();    	
    	var curStateAbbr = $('#state_select option:selected').data().StateAbbreviation;
    	var cityNPAID = $('#city_select option:selected').data().CityNPAId;
    	$.ajax({
			type: 'GET',
			url: 'phone_auth.php?city_npa_id=' + cityNPAID + '&cur_state_abbr=' + curStateAbbr,
			success: function(d){
				var myJSON = $.parseJSON(d);
				if(!myJSON.Data[0]){
					$('#available_numbers').prev().text('No Numbers Available');
				}
				else{
					$('#available_numbers').prev().text('Select a Number');
					$('#available_numbers').append('<option>Select a Number</option>');
					//$('#available_numbers').attr('selectedIndex', 1);
					var items = myJSON.Data;				
					$.each(items, function(i){				
						var curItem = items[i].PhoneNumberFormatted;
						var phoneNum = items[i].PhoneNumber;
						var domNode = $('<option value="' + phoneNum + '">' + curItem + '</option>');
						$('#available_numbers').append(domNode);
						$(domNode).data(items[i]);						
					});
				}
				$('#wait').hide();
			}
		});	
	});
					
	$('#createDir').delegate('#createPhone', 'submit', function(event){
		event.stopPropagation();
    	
		var phoneNumName = $('#pnum_name').val();
		if(phoneNumName.length === 0){
			alert('Please enter a name.');
			return false;
		}
		
		if(!phoneNumName.match(/^[a-z0-9]+$/i)){
			alert('Please enter only alphanumeric characters for the name.');
			return false;
		}
						
		var a = $('#available_numbers').attr("selectedIndex");
		if(a === 0 || a === -1){
    		alert('Please select a phone number');
    		return false;
    	}

		var phoneNumForward = $('#forwardNumber').val();
		if (phoneNumForward.match(/^[2-9]\d{2}-\d{3}-\d{4}$/)){
		    phoneNumForward = phoneNumForward.replace('-', '').replace('-','');
		}
		else{
			alert('Please enter the phone number in the required format.');
			return false;
		}
		//return false;
		//debugger;
		
		var phoneNumID = $('#available_numbers option:selected').data().PhoneNumberId;
		var phoneNum = $('#available_numbers option:selected').data().PhoneNumber;
		
		$.ajax({
			type:'POST',
			url:'phone_create.php',
			data: {
				p_name: phoneNumName,
				p_id: phoneNumID,
				p_num: phoneNum,
				p_forward: phoneNumForward 
			},
			success: function(d){
				$('#ajax-load').html(d);
			}
		});
		return false;		
	});
	
	$('#testServices').live('click', function(){
		$.ajax({
			type: 'GET',
			url: 'phone_auth.php?services=true',
			success: function(d){
				
			}
		})
	});
	
</script>
<div id="createDir">
	<form id="createPhone" action="" class="ajax" method="post">
		<label for="pnum_name">Name</label>
		<input type="text" name="name" id="pnum_name" class="pnum_name"><br />
		
		<select id="area_code_type" class="uniformselect">
			<option>Select Type of Area Code</option>
			<option>Toll Free</option>
			<option>Local</option>			
		</select>
		
		<select id="toll_free_select" class="uniformselect"></select>
		
		<select id="state_select" class="uniformselect"></select>	
		
		<select id="city_select" class="uniformselect"></select>	
		
		<select id="available_numbers" class="uniformselect"></select>
		
		<label for="forwardNumber">Forwarding Number (format 000-000-0000):</label>
		<input type="text" id="forwardNumber" name="forwardNumber"/>
		<br />
		
		<input type="button" id="testServices" value="Test Services" />
		
		<input type="submit" name="submit" value="Create a new phone number"><br />
	</form>
</div>

<?php
}
?>