<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
		
	//authorize Access to page
	require_once('./inc/auth.php');
	
	//iniate the page class
	require_once('./inc/directory.class.php');
	$directory = new Dir();
	
	if($_SESSION['theme']) $previewUrl = 'http://local.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://local.6qube.com/';
	
	$current_id = $_GET['id'];
	$dir_count = 0;
	//$dir_limit = $directory->getLimits($_SESSION['user_id'], "directory");
	$dir_limit = $_SESSION['user_limits']['directory_limit'];
	if($dir_limit==-1 || $dir_limit==NULL) $remaining = "Unlimited";
	
	//content
	$anal = new Analytics;
	echo '<h1>Directory Listings</h1>';
	if(isset($_POST['company_spot'])){ //if they submitted the form for the edit regions
		$company_spot = $directory->validateInject($_POST['company_spot'], 'Company Spot');
		$video_spot = $directory->validateInject($_POST['video_spot'], 'Video Spot');
		$query = "
			UPDATE
				`directory`
			SET
				company_spot = '{$company_spot}',
				video_spot = '{$video_spot}'
			WHERE
				id = '{$_SESSION[current_id]}';
		";
		$directory->query($query);	
	}
	
	if($_GET){
		if($_GET['form'])
			$form = $_GET['form'];
		else
			$form = 'listings';
			
		$_SESSION['current_id'] = $_GET['id'];
		include('./inc/forms/add_listing_'.$form.'.php');
	}
	
	else{
		$result = $directory->getDirectories($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']); //primes directory results, and is needed for getDirectoryRows()
		$dir_count = $directory->getDirectoryRows();
		echo '<div id="createDir">';
		if(!$_SESSION['theme']){
			echo '<div class="watch-video"><ul><li><a href="#campVideo" class="watch_video trainingvideo" >Watch Video</a></li></ul></div>';
		}
		echo '<form method="post" class="ajax" action="add-directory.php"><input type="text" name="name" ';
		if(!$remaining && ($dir_limit==0 || (($dir_limit-$dir_count)<=0))) echo 'readonly';
		echo '/><input type="submit" value="Create New Directory" name="submit" /></form></div>';
		if(!$_SESSION['theme']){
			echo '<div style="display: none;"><div id="campVideo" style="width:640px;height:480px;overflow:auto;"><a href="http://6qube.com/videos/training/directory-overview.flv" style="display:block;width:640px;height:480px"id="player"> </a><script>flowplayer("player", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");</script></div></div>';
		}
		echo '<div id="message_cnt"></div>';
		
		if($_POST){
			$newDir = $directory->validateText($_POST['name'], 'Directory Name'); //validate directory name
			if($directory->foundErrors()){ //check if there were validation errors
				echo '<p class="error"><strong>Errors were found</strong>'. $directory->listErrors('<br />') .'</p>'; //list the errors
			}
			else{ //if no errors
				$directory->addNewDirectory($_SESSION['user_id'], $newDir, $_SESSION['campaign_id'], $dir_limit); //add new directory
			}
		}
		
		if($dir_count){ //checks to see if the user has any directories
			if(!$remaining) $remaining = $dir_limit - $dir_count;
			$remaining = $remaining<0 ? 0 : $remaining;
			echo '<div id="dash"><div id="fullpage_directory" class="dash-container"><div class="container">';
			while($row = $directory->fetchArray($result)){ //if so loop through and display links to each directory
				$tracking = $row['tracking_id'];
				$cid = $row['cid'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'directory.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="directory.php?form=listings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=directory&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$directory->ANAL_getGraphImg('directory', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="directory.php?form=listings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Directory Listings remaining.';
			if(!$_SESSION['theme']){
				if(($dir_limit-$dir_count)<=0 && $dir_limit!=-1 && $remaining!="Unlimited") echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
			if($_SESSION['reseller']){
				echo '<br /><br />
					<a href="reseller-transfer-items.php?mode=section&mode2=directory&cid='.$cid.'" class="dflt-link">Transfer one or more directories to another user</a>';
			}
		} //end if($dir_count)
		
		else{ //if no directories
			if($dir_limit==-1 || $dir_limit==NULL) $dir_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_directory" class="dash-container"><div class="container">';
			if($dir_limit==0 && $dir_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Directory Listings remaining.';
				if(!$_SESSION['theme']){
					echo '<a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Business Listing.  You have <strong>'.$dir_limit.'</strong> remaining.';
			echo '</div></div></div>';
		} //end else {
			
	} //end else {
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>