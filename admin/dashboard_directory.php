<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//require db connection
	require_once('inc/directory.class.php');
	$connector = new Dir();
	
	//get page settings
	$settings = $connector->getSettings();
	
	//set page variables 
	$display_count = $settings['display_count'];
	$dir_count = 0;
	//$dir_limit = $connector->getLimits($_SESSION['user_id'], "directory");
	$dir_limit = $_SESSION['user_limits']['directory_limit'];
	if($dir_limit==-1 || $dir_limit=="") $dir_limit = "Unlimited";
	
	//Directory
	$result = $connector->getDirectories($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']);
	if($dir_count = $connector->getDirectoryRows()){
		//$result = $connector->getDirectories($_SESSION['user_id'], $display_count, NULL, $cID);
		while($row = $connector->fetchArray($result)){
			if($row['id'] != $past){
				$directory_title[] = $connector->directoryDashUpdate($row['id'], $row['name'], $row['tracking_id'], 'block');
				$past = $row['id'];
			}
		}			
	}
?>

	<div class="headline">
		<h2>Directory Listings:</h2>
		<!--<p>You currently have <span class="bold"><?=$dir_count?></span> listings out of <span class="bold"><?=$dir_limit?></span> available.</p>-->
		<a href="directory.php"><p>New Directory</p></a>
	</div>
	<?
		if($dir_count){
			echo '<div class="container">';
			foreach($directory_title as $key => $value){
				echo $value;
			}
		
			echo '</div>';
		}
		else {
			echo '<div class="container">None.</div>';
		}
	?>