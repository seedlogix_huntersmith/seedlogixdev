<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//iniate the page class
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	
	//get page settings
	$settings = $local->getSettings();
	
	//Auto responders
	$results = $local->getAutoResponders(NULL, $_SESSION['autoresponder_section'], $_SESSION['user_id']);
	$ar_count = $results ? $local->numRows($results) : 0;
	
	if($ar_count){ //checks to see if the user has any autoresponders
		echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
		while($row = $local->fetchArray($results)){ //if so loop through and display links to each directory
			echo '<div class="item">
					<div class="icons">
						<a href="inc/forms/'.$row['table_name'].'_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
						<a href="#" class="delete" rel="table=auto_responders&id='.$row['id'].'"><span>Delete</span></a>
					</div>';
				echo '<img src="http://'.$_SESSION['main_site'].'/admin/img/autoresponder';
				if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
				echo '.png" class="graph trigger" />';
				echo '<a href="inc/forms/'.$row['table_name'].'_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
				</div>';
		}
	}
	
	else{ //if no autoresponders
		echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
		echo 'Use the form above to create a new auto responder.';
		echo '</div></div></div>';
	}
?>
