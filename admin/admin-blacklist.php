<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/validator.php');
$db = new Validator();

if($_SESSION['admin']){
	if($_POST){
		//if the page is being POSTed to don't display stuff
		if($_POST['action']=='add'){
			$addIP = $db->sanitizeInput($_POST['ip']);
			$query = "INSERT INTO blacklist (ip, reason) VALUES ('".$addIP."', 'manual_admin')";
			if($db->query($query)){
				$return['success']['id'] = 1;
				$return['success']['action'] = 'load';
				$return['success']['page'] = 'admin-blacklist.php';
			}
			else $return['success'] = 0;
		}
		else if($_POST['action']=='del' && is_numeric($_POST['id'])){
			$query = "DELETE FROM blacklist WHERE id = '".$_POST['id']."'";
			if($db->query($query)) $return['success'] = 1;
			else $return['success'] = 0;
		}
		
		echo json_encode($return);
	}
	else {
		$query = "SELECT * FROM blacklist ORDER BY id DESC";
		if($blacklist = $db->query($query)){
			$numBlacklisted = $db->numRows($blacklist);
		}
?>
<h1>System-wide Blacklisted IP Addresses</h1>
<div id="createDir">
	<form method="post" class="ajax" action="admin-blacklist.php">
		<input type="text" name="ip" />
		<input type="hidden" name="action" value="add" />
		<input type="submit" value="Blacklist IP" name="submit" />
	</form>
</div>
<br />

<?
if($numBlacklisted){ 
	echo 
	'<br /><p>Click the X next to an IP to remove it.</p><br />
	<table style="width:100%;text-align:left;">
		<tr>
			<th>&nbsp;</th>
			<th>IP</th>
			<th>Reason</th>
			<th>User <small>(if applicable)</small></th>
			<th>Date/Time Added</th>
		</tr>';
	while($row = $db->fetchArray($blacklist)){
		$r = $row['reason'];
		if($r=='spam') $reason = 'Spam';
		if($r=='login_attempts') $reason = '>10 Login Attempts';
		if($r=='manual_admin') $reason = 'Manually Entered';
		echo 
		'<tr id="bl_'.$row['id'].'">
			<td><a href="#" class="delFromBlacklist" rel="'.$row['id'].'">[X]</a></td>
			<td>'.$row['ip'].'</td>
			<td>'.$reason.'</td>
			<td>'.$row['username'].'</td>
			<td>'.date('F jS, Y g:ia', strtotime($row['added'])).'</td>
		</tr>';
	}
	echo '</table>';
} else { ?>
<p>No IP addresses are currently blacklisted.</p>
<? } ?>


<br style="clear:both;" />
<? }} ?>