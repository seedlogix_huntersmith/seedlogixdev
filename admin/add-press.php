<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/press.class.php');
	$press = new Press();
	
	$newPress = $press->validateText($_POST['name'], 'Press Name'); //validate directory name
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	if($press->foundErrors()){ //check if there were validation errors
		//$error = $press->listErrors('<br />'); //list the errors
	}
	else { //if no errors
		$message = $press->addNewPress($_SESSION['user_id'], $newPress, $_SESSION['campaign_id'], 
								$_SESSION['user_limits']['press_limit'], NULL, $parent_id);
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_press .container';
		$response['success']['message'] = $message;
	}

	echo json_encode($response);
?>