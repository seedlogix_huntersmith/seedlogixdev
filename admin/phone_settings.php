<?php
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/auth.php');
	require_once('./inc/local.class.php');
	$p = new Local();	
	
	$user_id = $_SESSION['user_id'];
	$query = "SELECT mailbox_id FROM phone_numbers WHERE user_id = '".$user_id."'";
	$result = $p->queryFetch($query);
	$mailbox_id = $result['mailbox_id'];
	
	$mid = $_GET['mid'];

?>
<script type="text/javascript">
  var curInputVal;

  $('#editArea').delegate('.change', 'click', function(){
  	var inputEl = $(this).prev();
  	curInputVal = inputEl.val();
  	$(this).prev().removeAttr('disabled').removeClass('noBorder').select();
    $(this).hide();
    $(this).next().show();
    $(this).next().next().show();
    return false;
  });
  
  $('#editArea').delegate('.cancel', 'click', function(){
  	var inputEl = $(this).prev().prev().prev();
  	inputEl.attr('disabled', 'true').addClass('noBorder');
    $(this).hide();
    $(this).prev().hide();
    $(this).prev().prev().show();
    inputEl.val(curInputVal);
    return false;
  });	
  
  $('#editArea').delegate('.save', 'click', function(){
  	var formToSubmit = $(this).parent().attr('id');
  	var curInputID = $(this).prev().prev().attr('id');
  	$('#wait').show();
  	$.ajax({
  		type: "POST",
  		url: 'phone_settings_save.php',
  		dataType: 'html',
  		data: $('#' + formToSubmit).serialize(),
  		success: function(data){
  			$('#wait').hide();
  			if(data == 'ok'){
				 $('#' + curInputID).attr('disabled', 'true').addClass('noBorder');
				 $('#' + curInputID).next().next().hide();
				 $('#' + curInputID).next().next().next().hide();
				 $('#' + curInputID).next().show();	
  				 $('.successDiv').text('Update was successful.').css('color', 'green');  				
				 $('.successDiv').fadeIn(5000, function(){
					 $(this).fadeOut(2000);
				 });			 
  			}
  			else if(data == 'not valid'){
  				$('.successDiv').text('That is not a valid mailbox id, please contact the support team.').css('color', 'red');  				
  				$('.successDiv').fadeIn(5000, function(){
  					$(this).fadeOut(2000);
  				});
  			}
  			else{
  				$('.successDiv').text('There was a problem with your request, please contact the support team.').css('color', 'red');  				
  				$('.successDiv').fadeIn(5000, function(){
  					$(this).fadeOut(2000);
  				});
  			}
  			
  		}
  		
  	});
  	return false;
  });
	
</script>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="phone_temp.php">Call Services</a>
		</li>
        <li>
			<a href="phone_view_call_details.php?mid=<?=$mid?>&rqtype=MonthToDate">Call Details</a>
		</li>
        <li>
			<a  href="phone_view_call_report.php?mid=<?=$mid?>">Call Recordings</a>
		</li>
		<li class="last">
			<a class="current" href="phone_settings.php?mid=<?=$mid?>">Settings</a>
		</li>		
	</ul>
</div>
<div id="editArea">
	<form id="changeMIDForm" action="" method="post">
		<label for="updateMid">Mailbox ID:</label>
		<input type="text" id="updateMid" name="mid" value="<? echo $mailbox_id ?>" class="noBorder" disabled	/>
		<a class="change" href="#">Change</a>
		<a class="save" href="#" style="display:none">Save</a>
		<a class="cancel" href="#" style="display:none">Cancel</a>
		<input type="hidden" name="xyz" value="<? echo $user_id ?>">
		<span class="successDiv" style="display:none"></span>
	</form>
</div>
