<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('inc/auth.php');
	
	//DB connections
	require_once('inc/local.class.php');
	//require_once 'Mail.php';
	$connector = new Local();
	
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
	$port = '25';
	$smtp = Mail::factory('smtp',
				array ('host' => $host,
					  'port' => $port, 
					  'auth' => false));
	
	$prospectName = $_POST['name'];
	$contactFormID = $_POST['id'];
	$emailSubject = str_replace("#name", $prospectName, $_POST['emailsubject']);
	$emailBody = str_replace("#name", $prospectName, $_POST['emailbody']);
	
	if($emailBody=="") {
		$data['error'] = 1;
		$data['message'] = 'Message field cannot be blank.';
	}
	else {
		//check to see if user is emailing all prospects
		if(strpbrk($contactFormID, ",")){ //if $contactFormID has a comma
			$contactFormID2 = explode(",", $contactFormID); //turn it into an array
			$numIDs = count($contactFormID2);
		}
		
		//set up for() loop 
		if($contactFormID2){ //if user is emailing all prospects
			$iterations = $numIDs; //set number of times to loop through email script
		} else {
			$iterations = 1; //set number of times to loop through email script
			$contactFormID2[0] = $contactFormID; //add the single ID to an array at key 0 to work with loop
		}
		$errorNum = 0;
		$emailsSent = 0;
		$duplicates = 0;
		$emails = array();
		$errors = array();
		//get sending company's info
		$userId = $_SESSION['user_id'];
		$query = "SELECT username FROM users WHERE id = '".$userId."'";
		$a = $connector->queryFetch($query);
		$fromEmail = $a['username'];
		$query = "SELECT company FROM user_info WHERE user_id = '".$userId."'";
		$b = $connector->queryFetch($query);
		$fromCompany = $b['company'];
		//check email sending limit
		$emailsRemaining = $connector->emailsRemaining($userId);
		for($i=0; $i<$iterations; $i++){ //loop through email script $iterations number of times
			if($emailsRemaining>0){
				//get row from contact_form table with information
				$query = "SELECT email 
						FROM contact_form 
						WHERE id = '".$contactFormID2[$i]."'
						AND trashed = '0000-00-00' ";
				$row = $connector->queryFetch($query);
				
				//make sure contact hasn't been emailed already during this run
				if(!in_array($row['email'], $emails)){
					
					//set up email to send
					$sendTo = $row['email'];
					$sendFrom = '"'.$fromCompany.'" <'.$fromEmail.'>';
					$subject = stripslashes($emailSubject);
					$body = stripslashes($emailBody);
					
					//$headers = "From: ".$sendFrom."\r\n";
					//$headers .= "Reply-To: ".$row2['username']."\r\n";
					//$headers .= "MIME-Version: 1.0\r\n";
					//$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$headers = array ('From' => $sendFrom,
								   'Reply-To' => $fromEmail,
								   'To' => $sendTo,
								   'Subject' => $subject,
								   'Content-Type' => 'text/html; charset=ISO-8859-1',
								   'MIME-Version' => '1.0');
					
					//send email
					//$mailed = mail($sendTo, $subject, $body, $headers);
					$mailed = $smtp->send($sendTo, $headers, $body);
					
					//if($mailed){
					if(!PEAR::isError($mailed)){
						$emails[$emailsSent] = $row['email']; //add email to array of emails
						$emailsSent++;
						
						//mark prospect as responded to
						$query = "UPDATE contact_form 
								SET responded = 1 
								WHERE id = '".$contactFormID2[$i]."'";
						$result = $connector->query($query);
						
						//update user's total number of emails sent this month
						$query = "UPDATE users SET emails_sent = emails_sent+1 WHERE id = '".$userId."'";
						$connector->query($query);
						$emailsRemaining--;
					} else {
						$data['error'] = 1;
						$errors[$errorNum] = "ID: ".$contactFormID2[$i]."\nTo: ".$sendTo."\nFrom: ".$sendFrom."\n";
						$errorNum++;
					}
					
					//if(!$mailed){
	//					$data['error'] = 1;
	//					$errors[$errorNum] = "ID: ".$contactFormID2[$i]."\nTo: ".$sendTo."\nFrom: ".$sendFrom."\n";
	//					$errorNum++;
	//				} else {
	//					$emails[$emailsSent] = $row['email']; //add email to array of emails
	//					$emailsSent++;
	//					
	//					//mark prospect as responded to
	//					$query = "
	//							UPDATE contact_form 
	//							SET responded=1 
	//							WHERE id={$row['id']}
	//							";
	//					$result = $connector->query($query);
	//				}
				}//end if(!in_array())
				else { $duplicates++; }
			} //end if($emailsRemaining > 0)
			else {
				$data['error'] = 1;
				if($emails){
					$sentSuccessfully = "\nHowever the following people were emailed successfully:\n";
					foreach($emails as $key=>$value){
						$sentSuccessfully .= $value.'\n';
					}
				}
				$errors[] = "You've reached your account's monthly limit of outgoing emails.".$sentSuccessfully;
			}
		}//end for() loop
		
		if($data['error']==1){
			$data['message'] = "One or more emails were not sent.  Detailed info:\n";
			for($j=0; $j<count($errors); $j++){
				$data['message'] .= $errors[$j];
			}
		} else {
			$data['error'] = 0;
			$data['message'] = $emailsSent." Message(s) sent successfully!";
			if($duplicates>0){
				if($duplicates==1){
					$data['message'] .= "\n".$duplicates." duplicate email was not sent";
				} else {
					$data['message'] .= "\n".$duplicates." duplicate emails were not sent";
				}
			}
		}
	
	}
	
	echo json_encode($data);
?>