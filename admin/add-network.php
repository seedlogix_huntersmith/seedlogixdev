<?php
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize Access to page
require_once('inc/auth.php');

//iniate the page class
require_once('inc/reseller.class.php');
$reseller = new Reseller();

$type = $_POST['type'];
	
if($type=='setupWhole'){ //this is called from reseller-network-setup.php when Create button is clicked
	$company = $reseller->validateText($_POST['company'], 'Company Name');
	$networkID = $_POST['networkID'];
	$domain = $_POST['domain'];
	//make sure $domain is in the right format
	if(strpos($domain, 'http://')!==false) $domain = substr($domain, 7);
	$a = explode('.', $domain);
	if(count($a)>2){
		$domain = $a[count($a)-2].'.'.$a[count($a)-1];
	}
	
	$setup = $reseller->setupWholeNetworkSites($_SESSION['login_uid'], $networkID, $domain, $company);
	if($setup['success']){
		$response['success'] = 1;
	} else {
		$response['success'] = 0;
		$response['message'] = 'There was a problem setting up this network.  Please try again or contact support.';
		if($_SESSION['login_uid']==496)
			$response['message'] .= '<br />'.$setup['errors'];
	}
}
else { //otherwise user is clicking the Create button in a Networks section of Resellers
	$name = $reseller->validateText($_POST['name'], 'Network Name');
	if($newSite = $reseller->setupNetworkSite($_SESSION['login_uid'], $name, $type)){
		$tracking_id = $newSite['tracking_id'];
		$id = $newSite['id'];
			
		$html = '<div class="item" style="display:none;">
				<div class="icons">
				<!--<a href="directory.php?id='.$id.'" target="_new" class="view" rev="directory"><span>View</span></a>-->
				<a href="networks.php?type='.$type.'&id='.$id.'" class="edit" title="Edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=resellers_network&id='.$id.'&type='.$type.'"><span>Delete</span></a>
			</div>
			<img src="'.$reseller->ANAL_getGraphImg('network', $id, $tracking_id, $_SESSION['login_uid']).'" class="graph trigger" />
			<a href="networks.php?type='.$type.'&id='.$id.'" title="Edit">'.$name.'</a></div>';
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_network_'.$type.' .container';
		$response['success']['message'] = $html;
	}
}

echo json_encode($response);
?>