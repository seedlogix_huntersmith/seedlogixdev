<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/db_connector.php';
class Validator extends DbConnector {
	var $errors = NULL;
	
	/**********************************************************************/
	// function: VALIDATE TEXT
	// returns: text ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: String user input - String description
	/**********************************************************************/	 
	 function validateText($input, $description = '', $blankOK = ''){
	 	if($description) $descriptionLC = strtolower($description);
		$input = DbConnector::sanitizeInput($input); //strip tags and single quotes (for injection attacks)
		if($input != '' || $blankOK){ //if the input is not blank then return the clean input
			if($descriptionLC=='phone'){
				//remove non-numeric characters
				$pattern = '/[^0-9]/';
				$input = preg_replace($pattern, '', $input);
				if(strlen($input)>=10 && is_numeric($input)) return $input;
				else{
					$this->errors[] = 'Invalid phone number';
					return false;
				}
			}
			else if($descriptionLC=='listing keyword'){
				$a = explode(',', $input);
				return $a[0];
			}
			else return $input; //returns new input value
		}
		else { //if the input IS blank then throw an error
			$this->errors[] = $description .' was left blank'; //sets the error to be displayed later
			return false; //returns false if used in any if statements
		}
	 }
	 
	/**********************************************************************/
	// function: VALIDATE NUMERICAL INPUT
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: INT user input - String description
	/**********************************************************************/	 
     function validateNumber($input, $description = ''){
	 	if($input = $this->validateText($input, $description)){ //strips tags and checks if blank
			$specialChars = array('(', ')', '-', '.', ' ');
			$input = str_replace($specialChars, '', $input); //strip "(" ")" "-" "." from input (example (512).565-5677 -> 5125655677)
			if(is_numeric($input)){ //checks to see if only numeric values exist
				return $input; //the value is numeric, return the number
			}
			else {
				$this->errors[] = $description.' must be numeric'; //value not numeric! Add error description to list of errors
				return false; //return false
			}
		}
	}
	
	/**********************************************************************/
	// function: VALIDATE EMAIL address
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input - String description
	/**********************************************************************/	 
	function validateEmail($input, $description = ''){
		//check to see if blank and strips tags
		if($input = $this->validateText($input, $description)){
			//check if elements of an email exist blah@something.com
			if(preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})$/i", $input)){
				return $input; //if so return the input
			}
			else { //if the email is not valid throw an error
				$this->errors[] = 'Invalid email address'; //email not valid, add to error list
				return false; //returns false
			}
		}
	}
	
	/**********************************************************************/
	// function: VALIDATE PASSWORD
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input - String description - Start String Length - Stop String Length
	/**********************************************************************/	 
	function validatePassword($input, $description, $start, $stop){
		if($input = $this->validateText($input, $description)){ //checks to see if blank and strips tags
			$notAllowed = array('password', 'Password', 'admin', 'pass', 'password1', '123', '1234', '12345', 
							'123456', '12345678', 'abc123', 'iloveyou', 'jesus', 'letmein', 
							'111', '1111', '11111', '111111',
							'222', '2222', '22222', '222222',
							'333', '3333', '33333', '333333',
							'444', '4444', '44444', '444444',
							'555', '5555', '55555', '555555',
							'666', '6666', '66666', '666666',
							'777', '7777', '77777', '777777',
							'888', '8888', '88888', '888888',
							'999', '9999', '99999', '999999');
			if(!in_array($input, $notAllowed)){
				$wordCount = strlen($input); //grabs the string length
				if(($wordCount >= $start) && ($wordCount <= $stop)){ //check to see if hte input in between the specified limit
					return $input;
				}
				else {
					$this->errors[] = $description." must be between ".$start." and ".$stop." characters";
					return false;
				}
			}
			else {
				$this->errors[] = "Password is not strong enough"; //add errors to list
				return false; //returns false
			}
		}
	}
	
	/**********************************************************************/
	// function: VALIDATE USERNAME (checks duplicates)
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input - String description
	/**********************************************************************/	 
	function validateUserId($input, $description = ''){
		if($input = $this->validateText($input, $description)){ //checks to see if blank and strips tags
			if($input = $this->validateEmail($input, $description)){ //checks to see if email is valid
				$query = "SELECT username FROM users WHERE username = '".$input."'";
				$result = $this->queryFetch($query);
				if($result){ //if username is found, throw an error
					$this->errors[] = 'The email address you entered is already in use';
					return false;
				}
				else { //if no matches are found
					return $input; //return the input
				}
			}
		}
	}
	
	/**********************************************************************/
	// function: VALIDATE USERNAME (checks duplicates)
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input - String description
	/**********************************************************************/	 
	function validateRecruiterId($input, $description = ''){
		if($input = $this->validateText($input, $description)){ //checks to see if blank and strips tags
			if($input = $this->validateEmail($input, $description)){ //checks to see if email is valid
				$query = "SELECT username FROM users WHERE username = '".$input."'";
				$result = $this->queryFetch($query);
				if(!$result){ //if username is found, throw an error
					$this->errors[] = 'Recruiter ID Wrong: The email address does not contain a user';
					return false;
				}
				else { //if no matches are found
					return $input; //return the input
				}
			}
		}
	}
	
	/**********************************************************************/
	// function: VALIDATE CATEGORY
	// returns: true if input != '' or 0
	//
	// Accepted Input: String user input - String description
	/**********************************************************************/	 
	 function validateCategory($input){
	 
		$input = DbConnector::sanitizeInput($input); //strip tags and single quotes (for injection attacks)
		if($input!='0' && $input!=''){ //if the input is not blank then return the clean input
			return $input; //returns new input value
		}
		else { //if the input IS blank then throw an error
			$this->errors[] = 'Category was left blank'; //sets the error to be displayed later
			return false; //returns false if used in any if statements
		}
	 }
	
	/**********************************************************************/
	// function: Strip Tags and prevent SQL injection
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input
	/**********************************************************************/	 
	function validateInject($input)
	{
		$input = DbConnector::sanitizeInput($input); 
		return $input;
	}
	
	/**********************************************************************/
	// function: Strip Tags and prevent SQL injection
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input
	/**********************************************************************/	 
	function validateUrl($input)
	{
		$input = DbConnector::sanitizeInput($input); //strip tags and single quotes (for injection attacks)
		if($input){
			if(strpbrk($input, '@;\'"\\{}[]!$^*()`')===false && $input!='http://'){
				if(substr($input, 0, 7)!='http://' && substr($input, 0, 8)!='https://') $input = 'http://'.$input;
				return $input;
			}
			else {
				$this->errors[] = 'Invalid URL';
				return false;
			}
		}
		else {
			$this->errors[] = 'URL was left blank';
			return false;
		}
	}
	
	/**********************************************************************/
	// function: FIND ERRORS
	// returns: true or false
	//
	// Accepted Input: NONE
	/**********************************************************************/	 
	function foundErrors() {
		if (count($this->errors) > 0){ //checks if any errors have been added
			return count($this->errors); //if so return true
		} else { //if not
			return false; //return false
		}
	}
	/**********************************************************************/
	// function: LIST ERRORS
	// returns: list of errors seperated for user inputted delimiter LOL SP
	//
	// Accepted Input: DELIMITER
	/**********************************************************************/	 
	function listErrors($delim = ''){
		if($this->errors) return implode($delim,$this->errors);
	}
	
	/**********************************************************************/
	// function: LIST FORM ERRORS
	// returns: list of errors with html
	//
	// Accepted Input: N/A
	/**********************************************************************/	 
	
	function listFormErrors(){
		return '
			<div class="formError">
    			<strong>There were errors in the form:</strong>
        		<br />
        		'.implode('<br />',$this->errors).'
    		</div>
		';
	}
	
	/**********************************************************************/
	// function: VALIDATE Subdomain (checks duplicates)
	// returns: input ready to be inserted into the db (strips tags and double quotes)
	//
	// Accepted Input: STRING user input - String description
	/**********************************************************************/	 
	function validateSubDomainUserEntry($input){
		if($input){ //checks to see if blank and strips tags
			if(strpbrk($input, '@;\'"\\{}[]!$^*()`& ')){ //checks to see if any crazy characters in user entry is valid
			
				$this->errors[] = 'Invalid URL - No (@;\'"\\{}[]!$^*()`& ) Characters Allowed';
			    return false;
				
			} else {
				
				$query = "SELECT domain FROM hub WHERE domain = 'http://".$input."'";
				$result = $this->queryFetch($query);
				if($result){ //if domain is found, throw an error
					$this->errors[] = 'The domain you entered is already in use';
					return false;
				}
				else { //if no matches are found
					return $input; //return the input
				}
				
			}
		}
		else {
			$this->errors[] = 'URL was left blank';
			return false;
		}
	}	
	
	/**********************************************************************/
	// function: VALIDATE DOMAIN NAME
	// returns: error if not valid domain name (http://site.tld)
	//
	// Accepted Input: STRING (domain)
	/**********************************************************************/
	
	function validateDomain($domain, $removeHTTP = 0, $returnArray = 1, $stripSub = 1){
		$validTLDs = array();
		
		if($removeHTTP){
			if(substr($domain, 0, 7)=='http://') $domain = substr($domain, 7);
			else if(substr($domain, 0, 8)=='https://') $domain = substr($domain, 8);
			
			//strip out all slashes - *updated saturday june 23 2012
			$domain = str_replace('/', '', $domain);
		}
		
		if(strpbrk($domain, '.')){
			$domainArray = explode('.', $domain);
			if(count($domainArray)==2){
				if(strpbrk($domainArray[0], '@,;:\'"/\\{}[]+=_!#$%^&*()`~') 
				|| strpbrk($domainArray[1], '@,;:\'"/\\{}[]+=_!#$%^&*()`~')){
					if($returnArray){
						$return['error'] = 1;
						$return['message'] = "Domain contains invalid characters";	
					}
					else $return = '';
				} else {
					$return = $domain;
				}
			}
			else { //if domain contains subdomains OR has a multi-part TLD like .co.jp
				if($domainArray[count($domainArray)-2]=='co' || $domainArray[count($domainArray)-2]=='com'){
					$tld = $domainArray[count($domainArray)-2].'.'.$domainArray[count($domainArray)-1];
					$site = $domainArray[count($domainArray)-3];
				}
				else {
					$tld = $domainArray[count($domainArray)-1];
					$site = $domainArray[count($domainArray)-2];
				}
				if(strpbrk($site, '@,;:\'"/\\{}[]+=_!#$%^&*()`~') 
				|| strpbrk($tld, '@,;:\'"/\\{}[]+=_!#$%^&*()`~')){
					if($returnArray){
						$return['error'] = 1;
						$return['message'] = "Domain contains invalid characters";
					}
					else $return = '';
				} else {
					if($stripSub){
						$return = $site.".".$tld; //return just site.tld
					}
					else $return = $domain;
				}
			}
			return $return;
		} //end if(strpbrk($domain, '.'))
		else {
			if($returnArray){
				$return['error'] = 1;
				$return['message'] = "Invalid domain: ".$domain;
			}
			else $return = '';
			return $return;
		}
		
	}
		
	
		
	/*
	*	Created by Cory Frosch  june 24th 2012
	*	validates a domain specific to the rules needed for simplyrank.com
	**/
	function validateDomainforRankings($domain)
	{
		preg_match('@^(?:http://)?([^/]+)@i', $domain, $matches);
		$host = $matches[1];

		// get last two segments of host name
		preg_match('/[^.]+\.[^.]+$/', $host, $matches);
		return $matches[0];
	}
	
	/**********************************************************************/
	// function: PARSE XML STRING INTO ARRAY
	/**********************************************************************/
	function xml2array($contents, $get_attributes=1, $priority = 'tag'){ 
		if(!$contents) return array(); 
	
		if(!function_exists('xml_parser_create')){ 
			//print "'xml_parser_create()' function not found!"; 
			return array(); 
		} 
		
		//Get the XML parser of PHP - PHP must have this module for the parser to work 
		$parser = xml_parser_create(''); 
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); 
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1); 
		xml_parse_into_struct($parser, trim($contents), $xml_values); 
		xml_parser_free($parser);
		
		if(!$xml_values) return;
		
		//Initializations 
		$xml_array = array(); 
		$parents = array(); 
		$opened_tags = array(); 
		$arr = array();
		
		$current = &$xml_array;
		
		//Go through the tags. 
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array 
		foreach($xml_values as $data){ 
			unset($attributes,$value);//Remove existing values, or there will be trouble 
			
			//This command will extract these variables into the foreach scope 
			// tag(string), type(string), level(int), attributes(array). 
			extract($data);//We could use the array by itself, but this cooler. 
			
			$result = array(); 
			$attributes_data = array();
			
			if(isset($value)){
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}
			
			//Set the attributes too. 
			if(isset($attributes) and $get_attributes){
				foreach($attributes as $attr => $val){
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}
			
			//See tag status and do the needed.
			if($type == "open"){//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))){ //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
						$repeated_tag_index[$tag.'_'.$level] = 1;
						$current = &$current[$tag];
					} else { //There was another element with the same tag name
						if(isset($current[$tag][0])){//If there is a 0th element it is already an array
							$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
							$repeated_tag_index[$tag.'_'.$level]++;
						} else {//This section will make the value an array if multiple tags with the same name appear together
							$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
							$repeated_tag_index[$tag.'_'.$level] = 2;
							
							if(isset($current[$tag.'_attr'])){ //The attribute of the last(0th) tag must be moved as well
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
						}
						$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
						$current = &$current[$tag][$last_item_index];
					}
			
			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])){ //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])){//If it is already an array...
						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
					
						if($priority == 'tag' and $get_attributes and $attributes_data){
						    $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;
					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes){
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							if($attributes_data){
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}
			} elseif($type == 'close') { //End of tag '</tag>'
				  $current = &$parent[$level-1];
			}
		}
	
		return($xml_array);
	} //end xml2array function
	
	//check an IP address to see if it has been blacklisted
	//returns true if so
	function checkBlacklist($ip){
		if($ip = $this->validateText($ip)){
			$query = "SELECT count(id) FROM blacklist WHERE ip = '".$ip."'";
			$a = $this->queryFetch($query);
			if($a['count(id)']) return true;
			else return false;
		}
		else return false;
	}
	
	//blacklist an IP
	function blacklistIP($ip, $username = '', $reason = ''){
		$username = $this->validateText($username);
		$reason = $this->validateText($reason);
		if($ip = $this->validateText($ip)){
			$query = "INSERT INTO blacklist (ip, username, reason) 
					VALUES ('".$ip."', '".$username."', '".$reason."')";
			$this->query($query);
		}
	}
}
?>