<?php
	session_start();
         require_once( dirname(__FILE__) . '/../6qube/core.php');
	
	require_once(QUBEADMIN . 'inc/auth.php');
	require_once(QUBEADMIN . 'inc/local.class.php');
	$connector = new Local();
	
	//get page settings
	$settings = $connector->getSettings();
	
	//set campaign ID
	if(isset($_GET['CID'])){
		$cID = is_numeric($_GET['CID']) ? $_GET['CID'] : '';
		$_SESSION['campaign_id'] = $cID;
	}
	else{
		$cID = $_SESSION['campaign_id'];
	}
	$connector->setCampaignID($cID);
	//get campaign name
	$query = "SELECT name, default_snapshot_id FROM campaigns WHERE id = '".$cID."'";
	$result = $connector->queryFetch($query, NULL, 1);
	$cName = $result['name'];
	$graphID = $result['default_snapshot_id'] ? $result['default_snapshot_id'] : $connector->getHubOrDirID($cID);
	if(!$graphID) $graphID = 26;
	
	//set page variables 
	$display_count = $settings['display_count'];
	$dir_count = $hub_count = $press_count = $blog_count = $art_count = 0;
?>
<script type="text/javascript">
$(".uniformselect").uniform();
$('#changeGraph').die();
$('#changeGraph').live('change', function(){
	var newID = $(this).attr('value');
	$.get('inc/analytics_graph.php?id='+newID, function(data){
		$('#trafficGraph').html(data);
	});
});
$('#setDefaultGraph').die();
$('#setDefaultGraph').live('click', function(){
	var newDefault = $('#changeGraph').attr('value');
	var cid = '<?=$cID?>';
	$.get('inc/analytics_graph.php?id='+newDefault+'&cid='+cid+'&setDefault=1', function(data){});
	return false;
});
$('#renameCamp').die();
$('#renameCamp').live('click', function(){
	var campName = $(this).attr('title');
	$('#renameCamp').hide();
	$('#campaign-name').html('<form name="campaigns" class="jquery_form" id="<?=$cID?>"><ul><li><input type="text" name="name" style="width:430px;" value="'+campName+'" /></li></ul></form><br /><br />');
	return false;
});
</script>
<a href="#" id="renameCamp" class="dflt-link" title="<?=$cName?>" rel="<?=$cID?>" style="float:right;">Rename Campaign</a>
<div id="campaign-name"><h1 style="width:100%;font-size:2em;"><?=$cName?> - Dashboard</h1></div>

<? 
	//require_once('inc/analytics.class.php');
	//$anal = new Analytics();
	//$anal_id = 1;//$anal->ANAL_getAnalyticsId('directory', 1) 
?>	
		<div id="trafficGraph">
		<!--[if !IE]> -->
		<object type="application/x-shockwave-flash"
 		 data="http://6qube.com/analytics/libs/open-flash-chart/open-flash-chart.swf?piwik=0.9" width="100%" height="150" style="visibility: visible; " bgcolor="#FFFFFF">
		<!-- <![endif]-->

		<!--[if IE]>
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
		  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
		  width="100%" height="150" bgcolor="#FFFFFF" style="visibility: visible; ">
		  <param name="movie" value="http://6qube.com/analytics/libs/open-flash-chart/open-flash-chart.swf?piwik=0.9" />
		<!-->
		  <param name="allowScriptAccess" value="always">
			<param name="wmode" value="transparent">
			<param name="flashvars" id="flashvars" value="data-file=http%3A//6qube.com/analytics/index.php%3Fmodule%3DVisitsSummary%26action%3DgetEvolutionGraph%26columns%5B%5D%3Dnb_visits%26idSite%3D<?=$graphID?>%26period%3Dday%26date%3Dlast30%26viewDataTable%3DgenerateDataChartEvolution&id=VisitsSummarygetEvolutionGraphChart_swf&loading=Loading...">
		
		  <p>Flash player must be installed to view this content.</p>
		</object>
		<!-- <![endif]-->
		</div>
		View snapshot for:
		<?=$connector->displaySnapshotSelector($cID, $_SESSION['user_id'], $graphID)?>
		<p style="margin-top:-15px;"><a href="#" id="setDefaultGraph" class="dflt-link">Set as default</a></p>
<div id="message_cnt"></div>
<!-- NEW DASHBOARD -->
<div id="dash">
	
	<!-- Directory -->
	<div id="dashboard_directory" class="dash-container">
		<? include("dashboard_directory.php"); ?>
	</div><br />
	<!-- End Directory -->
	<!-- HUBs -->
	<div id="dashboard_hub" class="dash-container">
		<? include("dashboard_hub.php"); ?>
	</div><br />
	<!-- END HUBs -->
	<!-- press -->
	<div id="dashboard_press" class="dash-container">
		<? include("dashboard_press.php"); ?>
	</div><br />
	<!-- END press -->
	<!-- Blog Post -->
	<div id="dashboard_blog" class="dash-container">
		<? include("dashboard_blog.php"); ?>
	</div><br />
	<!-- END Blog Post -->
	<!-- Articles -->
	<div id="dashboard_article" class="dash-container">
		<? include("dashboard_article.php"); ?>
	</div>
	<!-- END Articles -->
	
	<? if($_SESSION['reseller']){ ?>
	<br /><br />
	<!-- <a href="reseller-transfer-items.php?mode=campaign&cid=<?=$cID?>" class="dflt-link">Transfer this entire campaign to another user</a> -->
	<? } ?>
		
</div>
<!-- END NEW DASHBOARD -->


<br style="clear:both" />
<br />
