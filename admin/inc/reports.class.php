<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Reports extends Local {
	var $counter = 0;
	
	/******************************************************
	// Display prospects
	/******************************************************/	
	function displayContactFormLeads($user_id = '', $type = '', $format = '', $limit = '', $offset = '', 
							  $page = '', $type_id = '', $exclude = '', $countLimit = '', $parent_id='', $cid=''){

		$numProspects = $this->getProspects($user_id, $type, $countLimit, NULL, true, $type_id, $exclude, NULL, $parent_id, $cid);
		
		if($numProspects){
			$prospects = $this->getProspects($user_id, $type, $limit, $offset, NULL, $type_id, $exclude, $limited, $parent_id, $cid);

				if($limit && ($numProspects > $limit)){
					//pagination
					$numPages = ceil($numProspects/$limit);
					if($offset>0) $currPage = ($offset/$limit)+1;
					else $currPage = 1;
					echo '<p style="font-weight:bold;">';
					if($currPage > 1){
						echo '<a title="Start of Prospects" href="'.$page.'?offset=0';
						echo '"><img src="img/v3/nav-start-icon.jpg" border="0" /></a>&nbsp;';
						echo '<a title="Previous Set of Prospects" href="'.$page.'?offset='.($offset-$limit);
						echo '"><img src="img/v3/nav-previous-icon.jpg" border="0" /></a> ';
					}
					if($currPage>1 && $currPage<$numPages){
						echo '';
					}
					if($currPage < $numPages){
						echo ' <a title="Next Set of Prospects" href="'.$page.'?offset='.($offset + $limit);
						echo '"><img src="img/v3/nav-next-icon.jpg" border="0" /></a>';
						echo ' <a title="End of Prospects" href="'.$page.'?offset='.(($numPages*$limit)-$limit);
						echo '"><img src="img/v3/nav-end-icon.jpg" border="0" /></a>';
					}
					echo '</p><br />';
				}
				while($row = $this->fetchArray($prospects)){
					$dt1 = date("F j, Y", strtotime($row['timestamp']));
					$dt2 = date("g:ia", strtotime($row['timestamp']));
					if($row['type']=='hub' || $row['type']=='blogs')
						$itemInfo = '<b>Website:</b> <a href="'.$row['website'].'" target="_blank" class="dflt-link">'.$row['website'].'</a>';
					else{
						if($row['website']!='' && $row['website']!='/') $link = $row['website']; else $link = '#';
						$itemInfo = '<b>Listing:</b> <a href="'.$link.'" target="_blank" class="dflt-link">'.$row['page'].'</a>';
					}
					
					
				
						$html .= '<ul class="prospects" title="prospect" id="prospect'.$row['id'].'">
								<li class="name">'.$row['name'].'</li>
								<li class="email"><a href="'.$row['email'].'" class="email-prospect" id="'.$row['id'].'" name="'.$row['name'].'" rel="prospect">'.$row['email'].'</a></li>
								<li class="phone">'.$row['phone'].'</li>
								<li class="date">'.$dt1.'</li>
								<div class="iconWrap">
								<li class="details"><a href="#" title="View Details" class="prospect-details" rel="'.$row['id'].'"><img src="img/v3/view-details.png" border="0" /></a></li>
								<li class="delete"><a href="#" title="Delete" class="delete" rel="table=contact_form&id='.$row['id'].'"><img src="img/v3/x-mark.png" border="0" /></a></li>
								</div>
								<li class="comment" id="prospect-details-'.$row['id'].'">
									<p style="font-style:italic;"><small><b>'.$dt1.'</b> at <b>'.$dt2.'</b> from <b>'.$row['ip'].'</b></small></p>
									<p id="'.$row['id'].'-comment">'.$row['comments'].'</p><br />
								
									<p>
									<b>Prospect id:</b> '.$row['id'].'<br />
									<b>User id:</b> '.$row['user_id'].'<br />
									<b>Type/id:</b> '.$row['type'].' #'.$row['type_id'].' - '.$row['page'].'<br />
									<b>Website:</b> '.$row['website'].'<br />
									<b>Responded:</b> '.$row['responded'].'<br />
									<b>Opted out:</b> '.$row['optout'].'<br />
									<b>Search Engine:</b> '.$row['search_engine'].'<br />
									<b>Keyword:</b> '.$row['keyword'].'<br />
									</p>
								</li>
							</ul>';
					
					
				}
				
			
			echo $html;
		} //end if($numProspecs) 
		else {
			echo "There are currently no prospects to display.<br /><br />";	
		}
	}

	/******************************************************
	// Get Custom Form Prospects.  
	/******************************************************/
	function getAllCustomLeads($user_id='', $limit='', $offset='', $count='', $parent_id=''){
		if($count){
			$query = "SELECT count(id)  
					FROM leads 
					WHERE trashed = '0000-00-00' ";
			if($user_id) $query .= " AND user_id = '".$user_id."'";
			if($parent_id) $query .= " AND parent_id = '".$parent_id."'";
			if($limit) $query .= " LIMIT ".$limit;
			$result = $this->queryFetch($query);
			return $result['count(id)'];
		}
		else {
			$query = "SELECT * ";
			$query .= "FROM leads 
						WHERE trashed = '0000-00-00' ";
			if($user_id) $query .= " AND user_id = '".$user_id."'";
			if($parent_id) $query .= " AND parent_id = '".$parent_id."'";
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			$result = $this->query($query);
			
			return $result;
		}
	}
	 
}
?>