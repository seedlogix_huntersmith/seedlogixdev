<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Contacts extends Local {
	var $counter = 0;
	
	 
	//get contacts
	function getContacts($uid, $cid = '', $id = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//build query
			if($id) $query = "SELECT * ";
			else $query = "SELECT id, first_name, last_name, email, phone, address, address2, city, state, zip, website, title, company, reports_to ";
			$query .= " FROM contacts WHERE user_id = '".$uid."' 
					 AND trashed = '0000-00-00' ";
			if($cid) $query .= " AND cid = '".$cid."' ";
			if($id){
				$query .= " AND id = '".$id."' ";
				$query .= " ORDER BY first_name ASC";
				$return = $this->queryFetch($query, NULL, 1);
			}
			else {
				$query .= " ORDER BY first_name ASC";
				if($result = $this->query($query)){
					$forms = array();
					while($row = $this->fetchArray($result, 1)){
						$views[$row['id']] = $row;
					}
					$return = $views;
				}
				else $return = false;
			}
			
			//return final data!
			return $return;
		}
		else return false;
	}
	//add a new contact
	function addNewContact($uid, $cid, $first, $last){
		$first = $this->validateText($first);
		$last = $this->validateText($last);
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		if($first && $last && $uid && $cid){
			$query = "INSERT INTO contacts 
					(user_id, cid, first_name, last_name, created) 
					VALUES
					('".$uid."', '".$cid."', '".$first."', '".$last."', now())";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	//add a new note
	function addNewNote($uid, $cid, $contactID, $subject){
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		$contactID = is_numeric($contactID) ? $contactID : 0;
		$subject = $this->validateText($subject);
		
		if($uid && $cid && $contactID && $subject){
			$query = "INSERT INTO notes 
					(contact_id, user_id, cid, subject, created) 
					VALUES
					('".$contactID."', '".$uid."', '".$cid."', '".$subject."', now())";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	//get notes
	function getNotes($uid, $cid = '', $contactID = '', $id = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		$contactID = is_numeric($contactID) ? $contactID : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//build query
			if($id) $query = "SELECT * ";
			else $query = "SELECT id, subject, type, note, last_updated ";
			$query .= " FROM notes WHERE contact_id = '".$contactID."' 
					 AND trashed = '0000-00-00' ";
			if($cid) $query .= " AND cid = '".$cid."' ";
			if($id){
				$query .= " AND id = '".$id."' ";
				$return = $this->queryFetch($query, NULL, 1);
			}
			else {
				$query .= " ORDER BY last_updated DESC";
				if($result = $this->query($query)){
					$forms = array();
					while($row = $this->fetchArray($result, 1)){
						$notesView[$row['id']] = $row;
					}
					$return = $notesView;
				}
				else $return = false;
			}
			
			//return final data!
			return $return;
		}
		else return false;
	}
	//push Lead to Contact
	function addNewLeadtoContact($uid, $cid, $first, $last, $lead_data = '', $lead_email = ''){
		$first = $this->validateText($first);
		$last = $this->validateText($last);
		$lead_data = $this->validateText($lead_data);
		$lead_email = $this->validateText($lead_email);
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		if($first && $last && $uid && $cid && $lead_data){
			$query = "INSERT INTO contacts 
					(user_id, cid, first_name, last_name, email, lead_data, created) 
					VALUES
					('".$uid."', '".$cid."', '".$first."', '".$last."', '".$lead_email."', '".$lead_data."', now())";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	//add a SugarCRM Account
	function addSugarAccount($uid, $cid, $sugar_username){
		$first = $this->validateText($sugar_username);
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		if($uid && $cid && $sugar_username){
			$query = "INSERT INTO sugarcrm
					(user_id, cid, sugar_username, created) 
					VALUES
					('".$uid."', '".$cid."', '".$sugar_username."', now())";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	//get Sugar Settings
	function getSugarSettings($uid, $cid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($cid){
			//build query
			$query = "SELECT * FROM sugarcrm WHERE user_id = '".$uid."' 
		    AND cid = '".$cid."'";
			$return = $this->queryFetch($query, NULL, 1);
			
			//return final data!
			return $return;
		}
		else return false;
	}
	//add a Salesforce Account
	function addSalesforceAccount($uid, $cid, $salesforce_username){
		$first = $this->validateText($salesforce_username);
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		if($uid && $cid && $salesforce_username){
			$query = "INSERT INTO salesforce
					(user_id, cid, salesforce_username, created) 
					VALUES
					('".$uid."', '".$cid."', '".$salesforce_username."', now())";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	//get Salesforce Settings
	function getSalesforceSettings($uid, $cid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($cid){
			//build query
			$query = "SELECT * FROM salesforce WHERE user_id = '".$uid."' 
		    AND cid = '".$cid."'";
			$return = $this->queryFetch($query, NULL, 1);
			
			//return final data!
			return $return;
		}
		else return false;
	}
}
?>