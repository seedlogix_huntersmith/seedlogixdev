<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Articles extends Local {

	var $article_id = 0;
	var $article_rows = 0;
	 
	/**********************************************************************/
	// function: Get Articles
	// returns all Articles, or Articles for specified user
	//
	// Accepted Input: User ID and Query Limit
	/**********************************************************************/
	function getArticles($user_id = '', $limit = '', $article_id = '', $cid = '', $count = '', $limited = ''){
		if($count) $query = "SELECT id FROM articles";
		else if($limited) $query = "SELECT id, cid, user_id, thumb_id, category, headline, summary, created FROM articles";
		else $query = "SELECT * FROM articles";
		if($user_id) $query .= " WHERE user_id = '$user_id'";
		else if($article_id) $query .= " WHERE id = '$article_id'";
		if($cid) $query .= " AND cid = '$cid'";
		$query .= " AND trashed = '0000-00-00' ";
		$query .= " ORDER BY timestamp DESC";
		if($limit) $query .= " LIMIT $limit";
		$result = DbConnector::query($query);
		$this->article_rows = $this->numRows($result);
		return $result;
	}
	 
	function userArticlesDropDown($user_id, $name, $cid = ''){
		$result = $this->getArticles($user_id, NULL, NULL, $cid);
		
		if($this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$articleArray[$row['id']] = $row['name'];
			}
			return $this->buildSelect($name, $articleArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
		}
		else
			return 'No articles created yet.';	
	
	}
	 
	/**********************************************************************/
	// function: Add New Articles
	// adds a new Articles to the DB, assumes you validate input before passing
	//
	// Accepted Input: User ID and Articles Name
	/**********************************************************************/
	function addNewArticles($user_id, $name, $cid, $art_limit = '', $bypass = '', $parent_id = ''){
		if(!$art_limit && !$bypass) $art_limit = $this->getLimits($user_id, 'article');
		if(!$bypass){
			$arts = $this->getArticles($user_id, NULL, NULL, $cid, true);
			$artcount = $arts ? $this->numRows($arts) : 0;
		}
		if(!$parent_id) $parent_id = '0';
		
		if($art_limit==-1 || ($art_limit - $artcount)>0 || $bypass){
			$query = "
				INSERT INTO 
					articles (
						cid,
						user_id, 
						user_parent_id, 
						name, 
						created
						) 
					VALUES (
						'".$cid."',
						'".$user_id."',
						'".$parent_id."',
						'".$name."', 
						NOW()
						)
			";
			$result = $this->query($query);
			if($result):
				$id = $this->getLastId();
				$this->ANAL_addSite('articles', $id, 'Article'.$id);
				return $this->articleDashUpdate($id, $name);
			else:
				return false;
			endif;
		}
		else return false;
	}
	 
	 
	/**********************************************************************/
	// function: Display Articles (NEW)
	// outputs a set of Articles results in new format
	//
	// Accepted Input: Query Limit and Query Offset
	/**********************************************************************/	 
	function displayArticles($limit = '', $offset = '', $main = 0, $user = '', $city = '', $state = '',
							 $category = '', $cid = '', $domain = '', $parent = ''){
		if(!$domain) $domain = 'articles.6qube.com';
		if($parent){
			//get parent site (for imgs)
			$query = "SELECT main_site FROM resellers WHERE admin_user = '".$parent."'";
			$a = $this->queryFetch($query);
			$parent_site = $a['main_site'];
		}
		// INCORRECT!! USE count(*)
		//Query DB for total number of articles that fit criteria.  Only select id to save time
	 	$query = "SELECT count(*) FROM articles ";
		$query .= " WHERE trashed = '0000-00-00'
				  AND headline != '' 
				  AND summary != '' 
				  AND body != '' 
				  AND body != '' 
				  AND city != '' 
				  AND state != '' 
				  AND category != '' 
				  AND category != '0' 
				  AND user_id != 105 
				  AND network = 1 ";
		if($user) $query .= " AND user_id = '".$user."'";
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($cid) $query .= " AND cid = '".$cid."'";
		if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = press_release.user_id) = '".$parent."'";
		$result = $this->query($query); //query the DB
                
#wrong	 	if($result) $this->article_rows = $this->numRows($result); //set $dir_rows
/* right */     if($result) $this->article_rows = $this->fetchColumn($result); //set $dir_rows

		if($this->article_rows){
			//Query DB for all data matching specific criteria
			$query = "SELECT * FROM articles ";
			$query .= " WHERE trashed = '0000-00-00' 
					  AND headline != '' 
					  AND summary != '' 
					  AND body != '' 
					  AND city != '' 
					  AND state != '' 
					  AND category != '' 
					  AND category != '0' 
					  AND user_id != 105 
					  AND network = 1 ";
			if($user) $query .= " AND user_id = '".$user."'";
			if($city) $query .= " AND city = '".$city."'";
			if($state) $query .= " AND state = '".$state."'";
			if($cid) $query .= " AND cid = '".$cid."'";
			if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = press_release.user_id) = '".$parent."'";
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			$result = $this->query($query); //query the DB
			
			while($row = $this->fetchArray($result)){ //if so loop through and display links to each Articles
				$html .= Local::displayArticle($row, $main, $domain, $parent_site);
			}
			if($main==1) { echo $html; }
			else { echo "<div class=\"blog_right_links\"><ul id=\"popular_posts\">".$html."</ul></div>"; }
			if($main) Local::displayNav3($domain, $this->article_rows, $limit, $offset, $city, $state, $category, $parent_site);
			else echo '<div style="text-align:center;"><a href="http://'.$domain.'" class="neutral-grey" >
					 More Articles</a></div>';
		}
		else { //if no directories
			if($user){
				echo "This user hasn't created any articles yet.";
			} else {
				echo 'None.';
			}
		}
	}
	/**********************************************************************/
	// function: Update Articles
	// updates a press release
	//
	// Accepted Input: POST Array
	/**********************************************************************/	 	
	function updateArticles(){
		$settings = $this->getSettings();
		$filepath = $settings['articles_img_dir'];
		// Collect form variables and Validate
		$action = $_POST['action'];
		$user_id = $_SESSION['user'];
		$id = $_POST['id'];
		$thumb_id = Validator::validateFile($_FILES['thumbnail']['name'], 'Thumbnail'); //$_FILES['thumbnail']['name'];
		$headline = Validator::validateText($_POST['headline'], 'Headline');
		$summary = Validator::validateText($_POST['summary'], 'Summary');
		$body = Validator::validateText($_POST['body'], 'News Body');
		$industry = Validator::validateText($_POST['industry'], 'Industry');
		$country = $_POST['country'];
		$keywords = $_POST['keywords'];
		$website = $_POST['website'];
		$author = Validator::validateText($_POST['author'], 'Author');
		$email = Validator::validateText($_POST['email'], 'Email');
		$contact = $_POST['contact'];
		$date = date("Y-m-d");
		// Check if any errors were found
		if( Validator::foundErrors() ) {
			echo '<p class="error"><strong>Errors were found</strong><br />'. Validator::listErrors('<br />') .'</p>';
			return false;
		}
		else{
			$img_directory = QUBEROOT . 'users/' . UserModel::GetUserSubDirectory($_SESSION['user_id']).'/articles/';
			// Upload Files
			if($thumb_id)
				move_uploaded_file($_FILES['thumbnail']['tmp_name'], $img_directory.$thumb_id);
			// Insert or Update DataBase
			$query = "
				UPDATE
					articles
				SET
			";
			if($thumb_id)
				$query.= "thumb_id = '$thumb_id', ";
			$query.= "
				headline = '{$headline}',
				summary = '{$summary}',
			";
			$query.= "body='$body', ";
			$query.= "industry='$industry', ";
			$query.= "country='$country', ";
			$query.= "keywords='$keywords', ";
			$query.= "website='$website', ";
			$query.= "author='$author', ";
			$query.= "email='$email', ";
			$query.= "contact='$contact', ";
			$query.= "timestamp='$date' ";
			$query .= "WHERE id = {$id}";
			$result = DbConnector::query($query);
			if($result){	
				$message = '<p class="success"><strong>Success!</strong><br />Your Article has been successfully edited.';
				$message.= '<br />You can view your article at the following URL: ';
				$title = explode(" ", stripslashes($headline));
				$title = implode("-", $title);
				$message.= '<a href="'.$settings['articles_url'].$title.'-'.$id.'.htm" target="_new">'.$title.'-'.$id.'.htm</a></p>';
				$this->ANAL_updateSite('articles', $id, $website);
				return $message;
			}
		}
	}
	
	/**********************************************************************/
	// function: Display Cities
	// displays cities for HUBs pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesArticles($limit = '', $domain = '', $reseller = '', $setCat = '', $state = ''){
		if(!$domain) $domain = 'articles.6qube.com';
		
		$query = "SELECT id, COUNT(type), city, state FROM articles";
		$query .= " WHERE trashed = '0000-00-00' 
				  AND headline != '' 
				  AND summary != '' 
				  AND body != '' 
				  AND city != '' 
				  AND state != '' 
				  AND category != '' 
				  AND category != '0' 
				  AND user_id != 105 
				  AND network = 1 ";
		if($reseller) $query .= " AND `user_parent_id` = '".$reseller."'";
		if($setCat) $query .= " AND category LIKE '%".$setCat."'";
		if($state) $query .= " AND state = '".$state."'";
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		 
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$html .= $this->getCityLink($row['city'], $row['state'], $domain, true);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit) $html .= ' <a href="http://'.$domain.'/cities/">more cities...</a> ';
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No cities found.";
		 
	}
	
	/**********************************************************************/
	// function: Display Categories
	// displays categories for Local pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesArticles($city, $state, $limit = '', $domain = '', $reseller = ''){
		if(!$domain) $domain = 'articles.6qube.com';
		
		$query = "SELECT id, COUNT(type), state, category FROM articles";
		$query .= " WHERE trashed = '0000-00-00' 
				  AND headline != '' 
				  AND summary != '' 
				  AND body != '' 
				  AND city != '' 
				  AND state != '' 
				  AND category != '' 
				  AND category != '0' 
				  AND user_id != 105 
				  AND network = 1 
				  AND city = '".$city."'
				  AND state = '".$state."'";
		if($reseller) $query .= " AND `user_parent_id` = '".$reseller."'";
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		if($result = $this->query($query)) $numResults = $this->numRows($result);
		if($numResults){
			while($row = $this->fetchArray($result)){
				$a = explode("->", $row['category']);
				$category = $a[1];
				$html .= $this->getCatLink($city, $state, $category, $domain);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit && $numResults>=$limit) 
				$html .= ' <a href="http://'.$domain.'/'.$this->convertKeywordUrl($city).'categories/">more categories...</a> ';
			else
				$html = substr($html, 0, count($html)-3); //remove trailing |
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No categories found.";
	}
	
	/**********************************************************************/
	// function: Return Latest mysqli_NUM_ROWS
	// returns the last mysqli_num_rows result for latest Articles Query 
	//
	// Accepted Input: none
	/**********************************************************************/
	function getArticlesRows(){ return $this->article_rows; }

}
?>