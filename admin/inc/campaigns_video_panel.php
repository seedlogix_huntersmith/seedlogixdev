	<div class="block">
		<h3>Learning 6Qube</h3>
		<h4>Training Videos</h4>
		<ul>
			<li><a href="#demoVideo" class="trainingvideo">elements walk-through</a></li>
			<div style="display:none;">
				<div id="demoVideo" style="width:640px;height:385px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://blip.tv/file/get/Elementsby6qube-ElementsBy6QubeLocalInternetMarketingSoftwareOverview649.flv"  style="display:block;width:640px;height:385px" id="demoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("demoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    

              
			<li><a href="#hubVideo" class="trainingvideo" >creating a website</a></li>
			<div style="display: none;">
				<div id="hubVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-overview.flv" style="display:block;width:640px;height:480px" id="hubplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    		  
			<li><a href="#pagesVideo" class="trainingvideo" >creating a page</a></li>
			<div style="display: none;">
				<div id="pagesVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/pages-overview.flv" style="display:block;width:640px;height:480px" id="pagesplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("pagesplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#blogVideo" class="trainingvideo" >creating a blog</a></li>
			<div style="display: none;">
				<div id="blogVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blogs-overview.flv" style="display:block;width:640px;height:480px" id="blogplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#postVideo" class="trainingvideo" >creating a post</a></li>
			<div style="display: none;">
				<div id="postVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/posts-overview.flv" style="display:block;width:640px;height:480px" id="postplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("postplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#dirVideo" class="trainingvideo" >creating a directory listing</a></li>
			<div style="display: none;">
				<div id="dirVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-overview.flv" style="display:block;width:640px;height:480px" id="dirplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("dirplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#pressVideo" class="trainingvideo" >creating a press release</a></li>
			<div style="display: none;">
				<div id="pressVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/press-overview.flv" style="display:block;width:640px;height:480px" id="pressplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("pressplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#articlesVideo" class="trainingvideo" >creating an article</a></li>
			<div style="display: none;">
				<div id="articlesVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/articles-overview.flv" style="display:block;width:640px;height:480px" id="articlesplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("articlesplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
    
			<li><a href="#domainsVideo" class="trainingvideo" >adding a domain</a></li>
			<div style="display: none;">
				<div id="domainsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/domains-overview.flv" style="display:block;width:640px;height:480px" id="domainsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("domainsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!-- /block -->