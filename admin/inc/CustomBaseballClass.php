<?php

#echo dirname(__FILE__);

#require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';

#var_dump(Qube::get_settings());

class Baseball {
	
	
	function Baseball(){
		
		$this->pdo	=	Qube::Start()->GetDB('master');
		
		return; 
		
	}
	
	//get events list
	function getEvents($userid, $eventid, $calendar = FALSE){

			//build query
			$select	=	new DBSelectQuery($this->pdo);
			$select->From('sapphire_ignitedplayer.events');
			$select->Fields('*');
			
			// the WHERE function is incremental. So you only need to check for 1 variable at a time to add new conditions

			// $userid is always required
			$select->Where('user_id = %d', $userid);

			// $event id is conditional
			if($eventid)
				$select->Where('id = %d', $eventid);

			// if not calendar, show upcoming events!			
			if(!$calendar)
				$select->Where('date >= CURDATE()');

			$select->orderBy(' date asc');

#try{
			$stmt	=	$select->Exec();

			if($eventid)	// return single item
				return $stmt->fetch();
				
			// else return an array
			
			$elist	=	array();
			while($row	=	$stmt->fetch()){
			$elist[$row['id']]	=	$row;
			}
		
			return $elist;

	}
	
	
	
	

	
	
	
}

