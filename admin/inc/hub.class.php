<?php
if(defined('QUBEPATH'))
{
require_once QUBEADMIN . 'inc/local.class.php';
}else{
    require_once(dirname(__FILE__) . '/../6qube/core.php');
    require_once(dirname(__FILE__) . '/local.class.php');
}
class Hub extends Local {
	var $hub_id = 0;
	var $hub_rows = 0;
	var $counter = 0;
	 
	/**********************************************************************/
	// function: Get Hubs
	// returns all hubs, or hubs for specified user
	//
	// Accepted Input: User ID and Query Limit
	/**********************************************************************/
	function getHubs($user_id='',$limit='',$hub_id='',$cid='',$count='',$theme_type='',$dashboard='',$mu='',$offset='',$search=''){
		if(!$theme_type){
			if($count) $query = "SELECT count(id) FROM hub";
			else if($dashboard) $query = "SELECT id, id_duped, cid, user_id, tracking_id, name, theme, req_theme_type FROM hub";
			else $query = "SELECT * FROM hub";
#deprecated			$query .= " WHERE hub.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub')";
			$query .= " WHERE trashed = '0000-00-00' ";	// is not trash
			if($user_id) $query .= " AND user_id = '".$user_id."'";
			else if($hub_id) $query .= " AND id = '".$hub_id."'";
			if($cid) $query .= " AND cid = '".$cid."'";
			if($mu) $query .= " AND multi_user = 1";
			if($search) $query .= " AND name LIKE '%".$search."%' ";
			$query .= " ORDER BY last_updated DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			
			if(@$_GET['qdebug'] == md5('amado'))
			{
			  echo 'hub::getHubs args: ' . print_r(func_get_args(), true), '<br /> Query: ', $query, '<br />';
			}
			if($count){
				$result = $this->queryFetch($query);
				return $result['count(id)'];
			}
			else {
				if($result = $this->query($query)) $this->hub_rows = $this->numRows($result);
				return $result;
			}
		}
		else {
			if($count) $query = "SELECT count(hub.id) FROM hub ";
			else $query = "SELECT hub.id, hub.user_id, hub.tracking_id, hub.name FROM hub ";
			$query .= " WHERE (hub.req_theme_type = '".$theme_type."' ";
			$query .= " OR ((SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '".$theme_type."'))";
#amado			$query .= " AND hub.id NOT IN (SELECT trash.table_id FROM trash WHERE trash.table_name = 'hub')";
			$query .= " AND hub.trashed = '0000-00-00' ";	// hub trash status
			if($user_id) $query .= " AND hub.user_id = '".$user_id."'";
			if($hub_id) $query .= " AND hub.id = '".$hub_id."'";
			if($cid) $query .= " AND hub.cid = '".$cid."'";
			if($mu) $query .= " AND hub.multi_user = 1";
			if(!$count){
				$query .= " ORDER BY last_updated DESC";
				if($limit) $query .= " LIMIT ".$limit;
				if($offset) $query .= " OFFSET ".$offset;
			}
			
			if(@$_GET['qdebug'] == md5('amado'))
			{
			  echo 'hub::getHubs args: ' . print_r(func_get_args(), true), '<br /> Query: ', $query, '<br />';
			}
			
			if($count){
				$result = $this->queryFetch($query);
				return $result['count(hub.id)'];
			}
			else return $this->query($query);
		}
	}
		
	//returns all pages for specified hub ID
	function getHubPage($hub_id='', $user_id='', $check_nav='', $limited='', $v2='', $type='', $navParent=0, $noArray=''){
		if(!$v2){
			$table = 'hub_page';
			$navField = 'nav';
		}
		else {
			$table = 'hub_page2';
			$navField = 'inc_nav';
		}
		//build query
		if($limited){
			if(!$v2) $query = "SELECT id, page_title, page_navname, allow_delete, nav";
			else $query = "SELECT id, type, nav_parent_id, nav_order, page_title, page_title_url, 
							allow_delete, inc_nav, default_page, outbound_url, outbound_target";
			$query .= " FROM ".$table;
		}
		else $query = "SELECT * FROM ".$table;
		if($hub_id) $query .= " WHERE ".$table.".hub_id = '".$hub_id."' ";
		else if($user_id) $query .= " WHERE ".$table.".hub_id IN (SELECT id FROM hub WHERE user_id = '".$user_id."') ";
		if($check_nav && !$v2) $query .= " AND ".$table.".".$navField." = 1 ";
		if($v2 && $type) $query .= " AND hub_page2.type = '".$type."' ";
		if($v2) $query .= " AND hub_page2.nav_parent_id = '".$navParent."' ";
		$query .= " AND ".$table.".trashed = '0000-00-00' ";
		//$query .= " AND `".$table."`.hub_id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub' AND table_id = ".$table.".hub_id) ";
		if($v2){
			if($type) $query .= " ORDER BY nav_order ASC, id ASC";
			else $query .= " ORDER BY nav_order ASC, id DESC ";
		}
		else $query .= " ORDER BY created ASC, id ASC";
		
		if(@$_GET['qdebug'] == md5('amado'))
		{
		  echo 'Query: getHubPage ', $query, '<br />';
		}
		
		//query for results
		$result = $this->query($query);
		//set number of rows
		if($result){
			$this->page_rows += $this->numRows($result);
			//if both navs and pages are being pulled, return an array
			if($v2 && !$type){
				$return = array();
				while($row = $this->fetchArray($result, 1)){
					if($row['type']=='nav'){
						if($noArray) $return[] = $row;
						else {
							//if type is a nav, get sub-pages (this is iterative)
							$navPages = $this->getHubPage($hub_id, $user_id, $check_nav, $limited, 1, NULL, $row['id']);
							$return2 = $navPages;
							array_unshift($return2, $row);
							$return[] = $return2;
						}
					}
					else $return[] = $row;
				}
			}
			else $return = $result;
		}
		else $return = false;
		//return query results
		return $return;
	}
	
	//returns a single hub page
	function getSinglePage($page_id, $v2 = ''){
		$page_id = is_numeric($page_id) ? $page_id : 0;
		$table = 'hub_page';
		if($v2) $table .= '2';
		$query = "SELECT * FROM ".$table." WHERE id = '".$page_id."'";
		if($row = $this->queryFetch($query, NULL, 1)){
			$this->page_rows = 1;
			return $row;
		}
		else return false;
	}
	
	//returns a single hub page from the title and hub_id
	function getSinglePageByTitle($title,$hub_id,$hub_row='',$referrerID='',$v2='',
								  $parentNav='',$searchEngine='',$seKeyword='',$rootNav=''){
		$title = $this->sanitizeInput($title);
		$parentNav = $this->sanitizeInput($parentNav);
		$hub_id = is_numeric($hub_id) ? $hub_id : '';
		$table = 'hub_page';
		if($v2) $table .= '2';
		$titleField = $v2 ? 'page_title_url' : 'page_title';
		if($title && $hub_id){
			$query = "SELECT p1.* FROM ".$table." p1 WHERE p1.".$titleField." = '".$title."'";
			$query .= " AND p1.hub_id = '".$hub_id."'";
			$query .=
			" AND p1.trashed = '0000-00-00' ";
			if($v2){
				$query .= ' and type = "page"';
				if($parentNav)
					$query .= " AND (SELECT p2.page_title_url FROM hub_page2 p2 WHERE p2.id = p1.nav_parent_id) = '".$parentNav."' ";
				if($rootNav)
					$query .= " AND (SELECT p3.page_title_url FROM hub_page2 p3 WHERE p3.id = p1.nav_root_id) = '".$rootNav."' ";
			}
			$query .= " ORDER BY last_edit DESC LIMIT 1";
#deb($query);
			if($result = $this->queryFetch($query, NULL, 1)){
				//look for and replace form embed codes
				if($result['has_custom_form']){
					// Add new custom form embed fields to the array below:
					$embedFields = array('page_region', 'page_edit_2');
					$embedForms = $this->embedForms($embedFields, $result, $result['user_id'], $hub_id, $referrerID, $result['id'], $_SESSION['searchEngine'], $_SESSION['seKeyword']);
					$result = $embedForms['row'];
					$result['hasCustomForm'] = true;
					//////////////////////////////////////////////////////////////
				}
				//look for and replace tags
				if($result['has_tags']){
					if(!$hub_row){
						//get field values if not included in the function call
						$query = "SELECT city, state, company_name, street, phone, zip, slogan, domain, coupon_offer, 
										 google_webmaster, google_tracking, keyword1, keyword2, keyword3, keyword4, keyword5
										 keyword6, overview, offerings, photos, videos, events, edit_region_6, edit_region_7,
										 edit_region_8, edit_region_9, edit_region_10, logo, user_id
								 FROM hub WHERE id = '".$hub_id."'";
						$hub_row = $this->queryFetch($query, NULL, 1);
					}
					$searchFields = array('page_region', 'page_keywords', 'page_seo_desc', 'page_photo_desc', 'page_edit_2', 'page_seo_title', 'page_edit_3', 'page_photo_desc');
					$userinfo = new UserDataAccess();
					$ui = $userinfo->getUserInfo($hub_row['user_id']);
					
					if($hub_row['profile_override']==1) {
						$replaceValues = array('city'=>$ui['biz_city'], 'state'=>($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])),
												'citycommastate'=>$ui['biz_city']. ($ui['biz_city'] && $ui['biz_state'] ? ', ' : '').($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])),
												'company'=>$hub_row['company_name'], 'address'=>$hub_row['street'], 
												'phone'=>$hub_row['phone'], 'zip'=>$hub_row['zip'], 
												'slogan'=>$hub_row['slogan'], 'domain'=>$hub_row['domain'], 
												'coupon'=>$hub_row['coupon_offer'], 'google_webmaster'=>$hub_row['google_webmaster'], 
												'google_tracking'=>$hub_row['google_tracking'], 'keyword1'=>$hub_row['keyword1'], 
												'keyword2'=>$hub_row['keyword2'], 'keyword3'=>$hub_row['keyword3'], 
												'keyword4'=>$hub_row['keyword4'], 'keyword5'=>$hub_row['keyword5'], 
												'keyword6'=>$hub_row['keyword6'], 'edit1'=>$hub_row['overview'], 
												'edit2'=>$hub_row['offerings'], 'edit3'=>$hub_row['photos'], 
												'edit4'=>$hub_row['videos'], 'edit5'=>$hub_row['events'],
												'youtube'=>$hub_row['youtube'], 'myspace'=>$hub_row['myspace'], 'linkedin'=>$hub_row['linkedin'], 'blog'=>$hub_row['blog'],
												'google'=>$hub_row['google_local'], 'twitter'=>$hub_row['twitter'], 'facebook'=>$hub_row['facebook'],
												'googleplaces'=>$hub_row['google_places'], 'instagram'=>$hub_row['instagram'], 'pinterest'=>$hub_row['pinterest'],
												'edit6'=>$hub_row['edit_region_6'], 'edit7'=>$hub_row['edit_region_7'], 
												'edit8'=>$hub_row['edit_region_8'], 'edit9'=>$hub_row['edit_region_9'], 
												'edit10'=>$hub_row['edit_region_10'],
												'custom1'=>$hub_row['keyword1'], 'custom2'=>$hub_row['keyword2'], 'custom3'=>$hub_row['keyword3'],
												'custom4'=>$hub_row['keyword4'], 'custom5'=>$hub_row['keyword5'], 'custom6'=>$hub_row['keyword6'],
												'custom7'=>$hub_row['button_1'], 'custom8'=>$hub_row['button1_link'], 'custom9'=>$hub_row['button_2'],
												'custom10'=>$hub_row['button2_link'], 'custom11'=>$hub_row['button_3'], 'custom12'=>$hub_row['button3_link'], 'custom13'=>$hub_row['legacy_gallery_link'],
												'custom14'=>$hub_row['form_box_color'], 'custom15'=>$hub_row['full_home'], 'custom16'=>$hub_row['remove_font'], 'custom17'=>$hub_row['gallery_page'],
												'custom18'=>$hub_row['slider_option'], 'custom19'=>$hub_row['display_page'], 'custom20'=>$hub_row['remove_contact'],
												'userid'=>$hub_row['user_id'], 'logo'=>$hub_row['logo'], 'logopath'=>'/users/'.UserModel::GetUserSubDirectory($hub_row['user_id']).'/hub/'.$hub_row['logo'].'',
												'profilepath'=>'/users/'.UserModel::GetUserSubDirectory($hub_row['user_id']).'/hub/'.$hub_row['profile_photo'].'',
												'profilesrc'=>'/users'.$hub_row['profile_photo'].'',
												'logosrc'=>'/users'.$hub_row['logo'].'',
												'cityorfullstate'=>($ui['biz_city'] && $hub_row['state'] ? $ui['biz_city'] : HubRowModel::getFullState($hub_row['state'])),
												'citylowercase'=>trim(strtolower(str_replace(' ', '', $hub_row['city']))),
												'incitycommastate'=>($ui['biz_city'] || $ui['biz_state'] ? 'in ' : '').$ui['biz_city'].($ui['biz_city'] && $ui['biz_state'] ? ', ' : '').($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])),
												'fullstate' => HubRowModel::getFullState($ui['biz_state']),
												'navname-children'=>'');
					} else {
						$replaceValues = array('city'=>($hub_row['city_url'] ? $hub_row['city_url'] : ($hub_row['city'] && $hub_row['state'] ? $hub_row['city'] : HubRowModel::getFullState($hub_row['state']))), 'state'=>($hub_row['city_url'] ? '' : ($hub_row['city'] && $hub_row['state'] ? $hub_row['state'] : HubRowModel::getFullState($hub_row['state']))),
											'citycommastate'=>($hub_row['city_url'] ? $hub_row['city_url'] : $hub_row['city']. ($hub_row['city'] && $hub_row['state'] ? ', ' : '').($hub_row['city'] && $hub_row['state'] ? $hub_row['state'] : HubRowModel::getFullState($hub_row['state']))),
											'company'=>$hub_row['company_name'], 'address'=>$hub_row['street'], 
											'phone'=>$hub_row['phone'], 'zip'=>$hub_row['zip'], 
											'slogan'=>$hub_row['slogan'], 'domain'=>$hub_row['domain'], 
											'coupon'=>$hub_row['coupon_offer'], 'google_webmaster'=>$hub_row['google_webmaster'], 
											'google_tracking'=>$hub_row['google_tracking'], 'keyword1'=>$hub_row['keyword1'], 
											'keyword2'=>$hub_row['keyword2'], 'keyword3'=>$hub_row['keyword3'], 
											'keyword4'=>$hub_row['keyword4'], 'keyword5'=>$hub_row['keyword5'], 
											'keyword6'=>$hub_row['keyword6'], 'edit1'=>$hub_row['overview'], 
											'edit2'=>$hub_row['offerings'], 'edit3'=>$hub_row['photos'], 
											'edit4'=>$hub_row['videos'], 'edit5'=>$hub_row['events'],
											'youtube'=>$hub_row['youtube'], 'myspace'=>$hub_row['myspace'], 'linkedin'=>$hub_row['linkedin'], 'blog'=>$hub_row['blog'],
											'google'=>$hub_row['google_local'], 'twitter'=>$hub_row['twitter'], 'facebook'=>$hub_row['facebook'],
											'googleplaces'=>$hub_row['google_places'], 'instagram'=>$hub_row['instagram'], 'pinterest'=>$hub_row['pinterest'],
											'edit6'=>$hub_row['edit_region_6'], 'edit7'=>$hub_row['edit_region_7'], 
											'edit8'=>$hub_row['edit_region_8'], 'edit9'=>$hub_row['edit_region_9'], 
											'edit10'=>$hub_row['edit_region_10'],
											'custom1'=>$hub_row['keyword1'], 'custom2'=>$hub_row['keyword2'], 'custom3'=>$hub_row['keyword3'],
											'custom4'=>$hub_row['keyword4'], 'custom5'=>$hub_row['keyword5'], 'custom6'=>$hub_row['keyword6'],
											'custom7'=>$hub_row['button_1'], 'custom8'=>$hub_row['button1_link'], 'custom9'=>$hub_row['button_2'],
											'custom10'=>$hub_row['button2_link'], 'custom11'=>$hub_row['button_3'], 'custom12'=>$hub_row['button3_link'], 'custom13'=>$hub_row['legacy_gallery_link'],
											'custom14'=>$hub_row['form_box_color'], 'custom15'=>$hub_row['full_home'], 'custom16'=>$hub_row['remove_font'], 'custom17'=>$hub_row['gallery_page'],
											'custom18'=>$hub_row['slider_option'], 'custom19'=>$hub_row['display_page'], 'custom20'=>$hub_row['remove_contact'],
											'userid'=>$hub_row['user_id'], 'logo'=>$hub_row['logo'], 'logopath'=>'/users/'.UserModel::GetUserSubDirectory($hub_row['user_id']).'/hub/'.$hub_row['logo'].'',
							  				'profilepath'=>'/users/'.UserModel::GetUserSubDirectory($hub_row['user_id']).'/hub/'.$hub_row['profile_photo'].'',
											'profilesrc'=>'/users'.$hub_row['profile_photo'].'',
											'logosrc'=>'/users'.$hub_row['logo'].'',
											'cityorfullstate'=>($hub_row['city_url'] ? $hub_row['city_url'] : ($hub_row['city'] && $hub_row['state'] ? $hub_row['city'] : HubRowModel::getFullState($hub_row['state']))),
											'citylowercase'=>trim(strtolower(str_replace(' ', '', $hub_row['city']))),
											'incitycommastate'=>($hub_row['city_url'] ? $hub_row['city_url'] : ($hub_row['city'] || $hub_row['state'] ? 'in ' : '').$hub_row['city'].($hub_row['city'] && $hub_row['state'] ? ', ' : '').($hub_row['city'] && $hub_row['state'] ? $hub_row['state'] : HubRowModel::getFullState($hub_row['state']))),
											'fullstate' => HubRowModel::getFullState($hub_row['state']),
											'navname-children'=>'');
					}
					$result = $this->replaceTags($result, $searchFields, $replaceValues);
				}
				return $result;
			}
			else return false;
		}
		else return false;
	}
	
	//returns a select dropdown of a user's hubs
	function userHubsDropDown($user_id, $name, $cid = '', $theme = ''){
		$result = $this->getHubs($user_id, NULL, NULL, $cid, NULL, $theme);
		
		if($this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$hubArray[$row['id']] = $row['name'];
			}
			return $this->buildSelect($name, $hubArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
		}
		else
			return 'No Hubs created yet';		
	}
	
	//returns a select dropdown of a user's custom forms
	function userCustomFormsDropDown($user_id, $selectName){
		if(is_numeric($user_id)){
			$query = "SELECT id, name FROM lead_forms WHERE user_id = '".$user_id."'";
			$result = $this->query($query);
			
			if($this->numRows($result)){
				while($row = $this->fetchArray($result)){
					$formsArray[$row['id']] = $row['name'];
				}
				return $this->buildSelect($name, $formsArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
			}
			else
				return 'No forms created yet';	
		}
		else return 'Validation error.';
	}
	
	/**********************************************************************/
	// function: Add New Hub
	// adds a new hub to the DB, assumes you validate input before passing
	//
	// Accepted Input: User ID and Hub Name
	/**********************************************************************/
	function addNewHub($user_id, $name, $cid, $req_theme_type = '', $dupe = '', $reseller_site = '', $parent_id = '', $mu = 0){
		if(!$parent_id) $parent_id = '0';
		if($req_theme_type==4 || $mu){
			if($mu){
				$noAnal = true;
				$pagesVersion = 2;
			}
			else {
				//if user is a reseller creating a multi-user hub
				//get a multi-user theme to create the hub with
				$query = "SELECT id FROM themes WHERE type = 4 AND (user_id_access = '".$parent_id."' ";
				$query .= " OR (user_class_access = 0 AND user_id_access = 0))";
				$query .= " ORDER BY user_id_access DESC, control_hub ASC LIMIT 1";
				$a = $this->queryFetch($query);
				$theme = $a['id'];
				$noAnal = true;
				$pagesVersion = 1;
			}
		}
		else if(($req_theme_type==2) || ($req_theme_type==3) || ($req_theme_type==5)){
			//if user is a user creating a hub that's required to be a landing page or social page
			//get a default theme with that type
			$query = "SELECT id FROM themes WHERE type = '".$req_theme_type."' AND ((user_id_access = '".$user_id."' ";
			$query .= " OR (user_class_access = (SELECT users.class FROM users WHERE users.id = '".$user_id."'))";
			$query .= " OR (user_class_access = 0 AND user_id_access = 0))) ";
			$query .= " ORDER BY user_id_access ASC, user_class_access ASC LIMIT 1";
			$a = $this->queryFetch($query);
			$theme = $a['id'];
		}
		if(!$theme) $theme = 78;
		if(!$pagesVersion) $pagesVersion = 2;
		
		$query = "INSERT INTO `hub` 
				(cid, user_id, user_id_created, user_parent_id, name, theme, req_theme_type, pages_version, multi_user, created)
				VALUES 
				('".$cid."', '".$user_id."', '".$user_id."', (select if(parent_id=0,id,parent_id) from users where id = $user_id ), '".$name."', 
				'".$theme."', '".$req_theme_type."', ".$pagesVersion.", '".$mu."', NOW())";
		$result = $this->query($query);
		
		if($result){
                        $id =   $this->getLastId();
			if($req_theme_type==4){
				//if user is a reseller creating a multi-user hub
				//update theme row's control_hub field with new ID
				$query = "UPDATE themes SET control_hub = '".$id."' WHERE id = '".$theme."' 
						AND control_hub = 0 AND user_id_access = '".$user_id."'";
				$this->query($query);
			}
			$old = umask(0); //temporarily escalate CHMOD permissions
			/*if(!$reseller_site){
				$settings = $this->getSettings();
				$user_dir = $settings['site_root']."hubs/domains/".$user_id;
				$user_hub = $settings['site_root']."hubs/domains/".$user_id."/".$id;
				
				if(is_dir($user_dir)){ //check to see if user directory exist
					if(is_dir($user_hub)){ //checks to see if users hub directory exist
						if(!file_exists($user_hub.'/index.php')){ //checks to see if an index file exist
							$copy = true;
						}
						else{
							echo 'File already in place.';
						}
					}
					else{
						mkdir($user_hub, 0777);
						$copy = true;
					}	
				}
				else{ 
					mkdir($user_dir, 0777);
					mkdir($user_hub, 0777);
					$copy = true;
				}
				if($copy){
					copy($settings['site_root']."hubs/domains/index-copy.php", $user_hub.'/index.php');
					copy($settings['site_root']."hubs/domains/htaccess.txt", $user_hub.'/.htaccess');
					copy($settings['site_root']."hubs/domains/process-custom-form.copy.php", $user_hub.'/process-custom-form.php');
				}
			} //end if(!$reseller_site) */
			
			if(!$noAnal) $this->ANAL_addSite('hub', $id, 'Hub'.$id);
			umask($old); //set CHMOD permissions back
			
			if(!$dupe) return $this->hubDashUpdate($id, $name, $user_id);
			else return $id;
		} //end if($result)
		else return false;
	}
	
	/***********************************************************
	// FUNCTION: Add New Post
	//  Adds a new Blog Post into the 'blog_post' table and
	//  installs a tracking id with Piwik
	//  
	/***********************************************************/
	function addNewPage($hub_id, $page, $user_id = '', $parent_id = '', $v2 = ''){
		if(!$user_id){
			//$hub = $this->getHubs(NULL, NULL, $hub_id);
			$query = "SELECT name, user_id, tracking_id FROM hub WHERE id = ".$hub_id;
			$hub = $this->queryFetch($query);
			$user_id = $hub['user_id'];
			$hub_name = addslashes($hub['name']);
			$hub_tracking = $hub['tracking_id'];
		}
		else {
			$query = "SELECT name, tracking_id FROM hub WHERE id = '".$hub_id."'";
			$result = $this->queryFetch($query);
			$hub_name = addslashes($result['name']);
			$hub_tracking = $result['tracking_id'];
		}
		if(!$parent_id) $parent_id = '0';
		$table = 'hub_page';
		if($v2){
			$table .= '2';
			//get nav_root_id
			if($page['nav_parent_id']){
				$navRoot = $this->getNavRoot($page['nav_parent_id'], $user_id);
			}
		}
		if(!$page['nav']) $page['nav'] = $page['inc_nav'];
		if(!$page['trashed']) $page['trashed'] = '0000-00-00';
		
	 	$query = "
			INSERT INTO
				`".$table."` (
					hub_id,
					user_id,
					user_parent_id,";
		if(!$v2) $query .= 
					"tracking_id,
					hub_name,
					page_navname,
					nav,";
		else $query .= 
					"type, 
					 nav_parent_id, 
					 nav_root_id, 
					 nav_order,
					 page_title_url,
					 inc_nav,";
		$query .=  "page_title,
					page_region,
					page_edit_2, 
					page_keywords,
					page_seo_title,
					page_seo_desc,
					page_photo,
					page_photo_desc, 
					page_full_width, 
					inc_contact, 
					has_custom_form, 
					has_tags, 
					trashed, 
					created
				)
			VALUES(
				".$hub_id.",
				".$user_id.",
				".$parent_id.",";
		if(!$v2) $query .= 
				"'".$hub_tracking."',
				'".$hub_name."',
				'".$page['page_navname']."',";
		else $query .= 
				"'".$page['type']."',
				 '".$page['nav_parent_id']."',
				 '".$navRoot."', 
				 '".$page['nav_order']."',
				 '".$this->convertKeywordFolder($page['page_title'])."',";
		$query .= "'".$page['nav']."', 
				'".$this->sanitizeInput($page['page_title'])."',
				'".$this->sanitizeInput($page['page_region'])."',
				'".$this->sanitizeInput($page['page_edit_2'])."',
				'".$this->sanitizeInput($page['page_keywords'])."',
				'".$this->sanitizeInput($page['page_seo_title'])."',
				'".$this->sanitizeInput($page['page_seo_desc'])."',
				'".$page['page_photo']."', 
				'".$this->sanitizeInput($page['page_photo_desc'])."', 
				'".$page['page_full_width']."', 
				'".$page['inc_contact']."', 
				'".$page['has_custom_form']."', 
				'".$page['has_tags']."', 
				'".$page['trashed']."', 
				NOW()
			)
		";
		$result = $this->query($query);
		
		if($result) {
			$id = $this->getLastId();
			return $id;
		} else {
			return false;
		}
	}
	
	function editPage($page){
		$query = "UPDATE hub_page SET page_navname = '$page[page_navname]', page_title = '$page[page_title]', page_region = '$page[page_region]', page_keywords = '$page[page_keywords]', page_photo = '$page[page_photo]' WHERE id = '$page[edit]'";
		$result = DbConnector::query($query);
		if($result)
			return '<p>Hub Page Updated!</p>';
		else
			return false;
	}
	
	/**********************************************************************/
	// function: Display Hubs (NEW)
	// outputs a set of hub results with new formatting
	//
	// Accepted Input: Query Limit and Query Offset
	/**********************************************************************/	 
	function displayHubs($limit = '', $offset = '', $main = 0, $user = '', $city = '', $state = '',
					 	 $category = '', $cid = '', $domain = '', $parent = '', $parent_company = ''){
		if(!$domain) $domain = 'hubs.6qube.com';
		if($parent){
			//get parent site (for imgs)
			$query = "SELECT main_site FROM resellers WHERE admin_user = ".$parent;
			$a = $this->queryFetch($query);
			$parent_site = $a['main_site'];
		}
                /*
		// ::: COMPLETE FAIL!! USE COUNT(*)!!
		//Query DB for total number of listings that fit criteria.  Only select id to save time
		$query = "
				SELECT id FROM `hub`
				WHERE `domain` != ''
				AND `category` != ''
				AND `category` != '0'
				AND `company_name` != ''
				AND `city` != ''
				AND `state` != ''
				AND `logo` != ''
				AND `description` != ''
				AND `cid` != 99999";
				
#				AND hub.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub') 

		$query .= "	AND trashed = '0000-00-00'
				AND user_id != 105
				";
				
		if($user) $query .= " AND user_id = ".$user; 
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($cid) $query .= " AND cid = ".$cid;
		if($category) $query .= " AND category LIKE '%".$category."'";
		if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = hub.user_id) = ".$parent;
                */
		
                
			//Query DB for data matching specific criteria
			//only pull fields that are user
			$query = "
					SELECT 
						id, 
						user_id, 
						domain, 
						category, 
						company_name, 
						phone, 
						street, 
						city, 
						state, 
						zip, 
						slogan, 
						logo, 
						description, 
						site_title, 
						meta_keywords, 
						meta_description, 
						keyword1, 
						keyword2, 
						keyword3, 
						keyword4, 
						keyword5, 
						keyword6, 
						twitter, 
						facebook, 
						myspace, 
						youtube, 
						diigo, 
						technorati, 
						linkedin, 
						blog, 
						has_tags, 
						created
					FROM 
						`hub`
					WHERE `domain` != ''
					AND `category` != ''
					AND `category` != '0'
					AND `company_name` != ''
					AND `city` != ''
					AND `state` != ''
					AND `logo` != ''
					AND `description` != ''
					AND `cid` != 99999 ";
#					AND hub.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub')
			  $query .= "	AND trashed = '0000-00-00' 
					AND user_id != 105
				";
				
			if($user) $query .= " AND user_id = ".$user; 
			if($city) $query .= " AND city = '".$city."'";
			if($state) $query .= " AND state = '".$state."'";
			if($cid) $query .= " AND cid = ".$cid;
			if($category) $query .= " AND category LIKE '%".$category."'";
			if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = hub.user_id) = ".$parent;
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			$result = $this->query($query);
			
                $this->hub_rows =    $this->numRows($result);
		
		if($this->hub_rows){
			while($row = $this->fetchArray($result)){
				if($row['has_tags']){
					//replace tags if the hub has any
					$searchFields = array('company_name', 'description', 'keyword1', 'keyword2', 'keyword3');
					$replaceValues = array('city'=>$row['city'], 'state'=>$row['state'],  'citycommastate'=>$hub_row['city'].', '.$hub_row['state'],
										  'company'=>$row['company_name'], 'address'=>$row['street'], 'phone'=>$row['phone'], 'zip'=>$row['zip'], 
										  'slogan'=>$row['slogan'], 'keyword1'=>$row['keyword1'], 'keyword2'=>$row['keyword2'],
										  'keyword3'=>$row['keyword3'], 'keyword4'=>$row['keyword4'], 
										  'keyword5'=>$row['keyword5'], 'keyword6'=>$row['keyword6'],
										  'citylowercase'=>trim(strtolower(str_replace(' ', '', $row['city']))),
							   			  'incitycommastate'=>'in '.$row['city'].', '.$row['state'],);
					$row = $this->replaceTags($row, $searchFields, $replaceValues);
				}
				$html .= Local::displayHub($row, $main, $parent_site);
			}
			if($main==1){ echo $html; }
			else if($main==3){
			echo "<div id=\"blog_adv\">".$html."</div>";	
			}
			else { echo "<div id=\"blog_adv\"><ul>".$html."</ul></div>"; }
			if($main && $main!=3) Local::displayNav3($domain, $this->hub_rows, $limit, $offset, $city, $state, $category, $parent_site);
			else echo '<br style="clear:both;" /><div style="text-align:center;">
						<a href="http://'.$domain.'" class="neutral-grey" >More Websites</a></div>';
		}
		else { //if no hubs
			if($user){
				echo "This user hasn't created any hubs yet.";
			} else {
				echo 'None.';
			}
		}
	}
	
	/**********************************************************************/
	// function: Calculate and return percentage of HUB completed
	// returns a number, 0 - 100 depending on level of HUB completion
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function getHubCompletion($row){
		$percentComplete = 0;
		
		if($row['phone'] && $row['city'] && $row['state'] && $row['category'] && $row['domain'] && $row['keyword1']){
			$percentComplete = 25;
			
			if($row['meta_description'] && $row['meta_keywords'] && $row['site_title']){
				$percentComplete = 50;	
				
				if($row['pic_one'] || 1){
					$percentComplete = 75;
					
					if($row['keyword2'] && $row['keyword3'] && $row['keyword4'] && $row['keyword5'] && $row['keyword6']){
						$percentComplete = 95;
						
						if($row['twitter'] || $row['facebook'] || $row['myspace'] || $row['youtube'] || $row['diigo'] || $row['technorati'] || $row['linkedin'] || $row['blog']){
							$percentComplete = 100;
						}
						
					}
					
				}
				
			}
				
		}
		
		return $percentComplete;
	}
	
	/**********************************************************************/
	// function: Display Cities
	// displays cities for HUBs pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesHubs($limit = '', $domain = '', $reseller = ''){
		if(!$domain) $domain = 'hubs.6qube.com';
		
		$query = "SELECT id, COUNT(type), city, state FROM hub";
#amado		$query .= " WHERE hub.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub')";
		$query .= " WHERE trashed = '0000-00-00'";
		$query .= " AND domain != ''  
				AND `company_name` != ''
				AND `category` != ''
				AND `category` != '0'
				AND `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `state` != ''
				AND `slogan` != ''
				AND `description` != ''
				AND user_id != 105 
				";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		 
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$html .= $this->getCityLink($row['city'], $row['state'], $domain, true);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit) $html .= ' <a href="http://'.$domain.'/cities/">more cities...</a> ';
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No cities found.";
	}
	
	/**********************************************************************/
	// function: Display Categories
	// displays categories for Hubs pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesHubs($city, $state, $limit = '', $domain = '', $reseller = ''){	
		if(!$domain) $domain = 'hubs.6qube.com';
		
		$query = "SELECT id, COUNT(type), state, category FROM hub";
#amado		$query .= " WHERE hub.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'hub')";
		$query .= " WHERE trashed = '0000-00-00'";
		$query .= " AND domain != ''  
				AND `company_name` != ''
				AND `category` != ''
				AND `category` != '0'
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `state` != ''
				AND `slogan` != ''
				AND `description` != ''
				AND `user_id` != 105";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		$query .= " AND city = '".$city."'";
		$query .= " AND state = '".$state."'";
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		if($result = $this->query($query)) $numResults = $this->numRows($result);
		if($numResults){
			while($row = $this->fetchArray($result)){
				$a = explode("->", $row['category']);
				$category = $a[1];
				if($category){
					$html .= $this->getCatLink($city, $state, $category, $domain);
					if($limit) $html .= ' | ';
					else $html .= '<br />';
				}
			}
			if($limit && $numResults>=$limit) 
				$html .= ' <a href="'.$this->getCityLink($city, $state, $domain, NULL, NULL, NULL, true).'categories/">more categories...</a> ';
			else
				$html = substr($html, 0, count($html)-3); //remove trailing |
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No categories found.";
	}
	
	//Function to check how many duplicates of a hub a campaign has
	function getCampaignHubDupes($hub_id = '', $cid){
		$query = "SELECT count(id) FROM hub";
		if($hub_id) $query .= " WHERE id_duped = '".$hub_id."'";
		else $query .= " WHERE id_duped != 0";
		$query .= " AND cid = '".$cid."'";
		
		$result = $this->queryFetch($query);
		return $result['count(id)'];
	}
	
	//Function to get number of campaign's landing pages
	function getCampaignLandings($cid){
		$query = "SELECT count(hub.id) FROM hub
				WHERE hub.cid = '".$cid."' AND (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = 2";
		$result = $this->queryFetch($query);
		return $result['count(hub.id)'];
	}
	
	///////////////////////////////////////////////////////
	// MULTI-USER HUB FUNCTIONS
	///////////////////////////////////////////////////////
	//Check if a hub theme is multi-user
	function isMultiUserTheme($themeID = '', $hubID = ''){
		if($themeID && is_numeric($themeID)){
			$query = "SELECT type FROM themes WHERE id = '".$themeID."'";
			$result = $this->queryFetch($query);
			if($result['type']==4) return true;
			else return false;
		}
	}
	
	//Update a just-created hub row with presets from a multi-user theme
	//OR update a MU Hub Theme and its control hub
	function setupMultiUserHub($hubID, $themeID, $uid){
		//get theme info
		$query = "SELECT user_class2_access, user_id_access, control_hub FROM themes WHERE id = '".$themeID."'";
		$themeInfo = $this->queryFetch($query);
		if(($hubID != $themeInfo['control_hub']) && ($themeInfo['control_hub'] != 0)){
			//get user's class
			if($uid != $themeInfo['user_id_access']){
				$query = "SELECT parent_id, class FROM users WHERE id = '".$uid."'";
				$userInfo = $this->queryFetch($query);
			}
			else $userInfo['parent_id'] = $uid; //if user is the reseller making their own copy of the MU Hub
			//get hub info
			$query = "SELECT tracking_id, name, theme FROM hub WHERE id = '".$hubID."' AND user_id = '".$uid."'";
			$hubInfo = $this->queryFetch($query);
			//check to make sure user has access to this theme
			if(($themeInfo['user_class2_access']==$userInfo['class']) || 
			   ($uid==$themeInfo['user_id_access'] && $hubID!=$themeInfo['control_hub'])){
				//set up fields
				$noCopyFields = array('id', 'id_duped', 'cid', 'type', 'user_id', 'user_id_created', 'user_parent_id', 'tracking_id', 
								  'name', 'domain', 'connect_blog_id', 'req_theme_type', 'views', 'email_id', 
								  'last_updated', 'created', 'touchpoint_ID');	// oops
				$query = "SELECT * FROM hub WHERE id = '".$themeInfo['control_hub']."' AND user_id = '".$userInfo['parent_id']."'";
				$copyHub = $this->queryFetch($query);
				$fields = array();
				foreach($copyHub as $key=>$value){
					if(!in_array($key, $noCopyFields) && $value!='') $fields[$key] = $this->sanitizeInput($value);
				}
				if($fields){
					$query = "UPDATE hub SET ";
					//add each field to update
					foreach($fields as $key=>$value){
						if(!is_numeric($key)) $query .= " `".$key."`  = '".$value."', ";
					}
					//remove last ", "
					$query = substr($query, 0, -2);
					$query .= " WHERE id = '".$hubID."' AND user_id = '".$uid."' LIMIT 1";
					$this->query($query);
#					 echo mysqli_error();
					//copy images over from parent's hub images folder
					$parentLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($userInfo['parent_id']).'/hub/';
					$userLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($uid).'/hub/';
					$imgs = array('logo', 'bg_img', 'pic_one', 'pic_two', 'pic_three', 'pic_four', 'pic_five', 
								'pic_six', 'pic_seven', 'pic_eight', 'pic_nine', 'pic_ten');
					foreach($imgs as $key=>$value){
						if($fields[$value]!=''){
							//if there is a preset value for this image, copy it over
							copy($parentLoc.$fields[$value], $userLoc.$fields[$value]);
						}
					}
				}
				$noCopyFields = NULL;
				//set up pages
				$query = "SELECT * FROM hub_page WHERE hub_id = '".$themeInfo['control_hub']."' 
						AND user_id = '".$userInfo['parent_id']."' ORDER BY created ASC, id ASC";
				$copyPages = $this->query($query);
				if($copyPages && $this->numRows($copyPages)){
					//first delete any existing pages
					$query = "DELETE FROM hub_page WHERE hub_id = '".$hubID."' AND user_id = '".$uid."'";
					$this->query($query);
					while($pageRow = $this->fetchArray($copyPages)){
						//loop through pages
						//add each one
						$query = "INSERT INTO hub_page 
								(hub_id, tracking_id, user_id, user_parent_id, page_parent_id, hub_name, 
								 page_title, page_navname, page_region, page_keywords, page_seo_title, 
								 page_seo_desc, page_photo, page_photo_desc, page_edit_2, inc_contact, 
								 allow_delete, nav, has_custom_form, has_tags, trashed, created)
								VALUES 
								('".$hubID."', '".$hubInfo['tracking_id']."', '".$uid."', '".$userInfo['parent_id']."', '".$pageRow['id']."', '".$hubInfo['name']."', '".$this->sanitizeInput($pageRow['page_title'])."', '".$this->sanitizeInput($pageRow['page_navname'])."', '".$this->sanitizeInput($pageRow['page_region'])."', '".$this->sanitizeInput($pageRow['page_keywords'])."', '".$this->sanitizeInput($pageRow['page_seo_title'])."', '".$this->sanitizeInput($pageRow['page_seo_desc'])."', '".$this->sanitizeInput($pageRow['page_photo'])."', '".$this->sanitizeInput($pageRow['page_photo_desc'])."', '".$this->sanitizeInput($pageRow['page_edit_2'])."', '".$pageRow['inc_contact']."', '".$pageRow['allow_delete']."', '".$pageRow['nav']."', '".$pageRow['has_custom_form']."', 
								'".$pageRow['has_tags']."', '".$pageRow['trashed']."', NOW())";
						if($this->query($query)) $newPageID = $this->getLastId();
						//copy images over from parent's hub images folder
						$parentLoc2 = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($userInfo['parent_id']).'/hub_page/';
						$userLoc2 = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($uid).'/hub_page/';
						if($pageRow['page_photo']){
							//if there is a preset value for this image, copy it over
							copy($parentLoc2.$pageRow['page_photo'], $userLoc2.$pageRow['page_photo']);
						}
						//check to see if the parent page is in the trash
						//$query = "SELECT count(id) FROM hub_page WHERE table_name = 'hub_page' AND table_id = '".$pageRow['id']."'";
						//$a = $this->queryFetch($query);
						//if($a['count(id)']){
						//	//if so put this new page in the trash too
						//	$query = "INSERT INTO trash (table_name, table_id, user_id)
						//			VALUES ('hub_page', '".$newPageID."', '".$uid."')";
						//	$this->query($query);
						//}
					} //end while($pageRow = $this->fetchArray($copyPages))
				} //end if($copyPages && $this->numRows($copyPages))
			} //end if(($themeInfo['user_class2_access']==$userInfo['class']) ...
		} //end if(($hubID != $themeInfo['control_hub']) && ($themeInfo['control_hub']!=0))
		else {
			//If user is a reseller who just changed the theme on a Master MU Hub
			$query = "UPDATE themes SET control_hub = '".$hubID."' WHERE id = '".$themeID."' AND user_id_access = '".$uid."'";
			if($this->query($query)){
				//if successful removed the hub as control_hub from any other MU Themes
				$query = "UPDATE themes SET control_hub = 0 WHERE control_hub = '".$hubID."' 
						AND id != '".$themeID."' AND user_id_access = '".$uid."'";
				$this->query($query);
			}
		}
	}
	
	//Push new field value to all hubs using specified multi-user theme
	function pushMultiUserHubValue($themeID, $field, $value, $uid, $table = '', $keepCustomVals = 1, $pageID = ''){
		if(is_array($value)){
			list($valuestr, $value)	=	$value;
		}else
			$valuestr	=	$value;
		if(!$table) $table = 'hub';
		if($table=='hub') $idField = 'id';
		else if($table=='hub_page') $idField = 'hub_id';
		//make sure theme is multi-user and the user has permission
		$query = "SELECT type, user_id_access, control_hub FROM themes WHERE id = '".$themeID."'";
		$themeInfo = $this->queryFetch($query);
		if(($themeInfo['type']==4) && ($themeInfo['user_id_access']==$uid)){
			//get the old value to check for user customized fields
			$query = "SELECT `".$field."` FROM `".$table."` WHERE `".$idField."` = '".$themeInfo['control_hub']."'";
			if($table=='hub_page') $query .= " AND id = '".$pageID."'";
			$a = $this->queryFetch($query);
			$originalVal = $this->sanitizeInput($a[$field]);
			//if checks are passed, run query
			//first check to see if the value contains a form embed or tag code
			if(preg_match("/\|@form-(.*?)@\|/", $value)){
				$otherUpdate = ", has_custom_form = 1 ";
			}
			if(preg_match("/(\(\((.*?)\)\))/", $value)){
				$otherUpdate .= ", has_tags = 1 ";
			}
			$query = "UPDATE `".$table."` SET `".$field."` = '".$value."'".$otherUpdate;
			if($table=='hub')
				$query .= " WHERE theme = '".$themeID."' ";
			else if($table=='hub_page'){
				$query .= " WHERE (SELECT hub.theme FROM hub WHERE hub.id = hub_page.hub_id) = '".$themeID."' ";
				$query .= " AND (page_parent_id = '".$pageID."' OR id = '".$pageID."') ";
			}
			$query .= " AND (user_parent_id = '".$uid."' OR user_id = '".$uid."') ";
			if($keepCustomVals){
				$query .= " AND `".$field."` = '".$originalVal."'";
			}
			if($this->query($query)) return true;
			else return false;//'bad query';
		}
		else return false;//'no access';
	}
	
	//Push new hub page or hub page value to all hubs using specified multi-user theme
	//updated to work with V2 MU Hubs
	function pushNewMultiUserHubPage($themeID, $title, $uid, $pageID, $v2 = '', $pageNavParent='', $pageOrder='', $pageType=''){
		$table = 'hub_page';
		$pageInfoFields = 'has_custom_form, has_tags';
		if($v2){
			$table .= '2';
			$pageInfoFields .= ', type, nav_parent_id, order';
			//make sure theme is multi-user and the user has permission
			$query = "SELECT user_id, multi_user FROM hub WHERE id = '".$themeID."'";
			$hubInfo = $this->queryFetch($query, NULL, 1);
			if($hubInfo['multi_user'] && ($hubInfo['user_id']==$uid)) $continue = true;
		}
		else {
			//make sure theme is multi-user and the user has permission
			$query = "SELECT type, user_id_access, control_hub FROM themes WHERE id = '".$themeID."'";
			$themeInfo = $this->queryFetch($query);
			$query = "SELECT ".$pageInfoFields." FROM `".$table."` WHERE id = '".$pageID."'";
			$pageInfo = $this->queryFetch($query);
			if(($themeInfo['type']==4) && ($themeInfo['user_id_access']==$uid)) $continue = true;
		}
		if($continue){
			//if checks are passed, run query
			//if user is adding a new page to the theme
			//get all hubs with the theme
			if($v2){
				$query = "SELECT id, user_id FROM hub WHERE hub_parent_id = '".$themeID."' 
						AND (user_parent_id = '".$uid."' OR user_id = '".$uid."') AND id != '".$themeID."' 
						AND pages_version = 2";
			}
			else {
				$query = "SELECT id, user_id, tracking_id, name FROM hub WHERE theme = '".$themeID."' 
						AND (user_parent_id = '".$uid."' OR user_id = '".$uid."') AND id != '".$themeInfo['control_hub']."'";
			}
			$hubs = $this->query($query);
			if($hubs){
				$errors = 0;
				while($row = $this->fetchArray($hubs, 1)){
					//loop through applicable hubs and create the new page for each
					if($v2){
						//figure out which nav section this page should go under
						if($pageNavParent){
							//get this hub's nav section ID that has the parent hub's page_parent_id
							$query = "SELECT id, nav_root_id FROM hub_page2 
									  WHERE page_parent_id = '".$pageNavParent."' AND hub_id = '".$row['id']."'";
							$newPageInfo = $this->queryFetch($query, NULL, 1);
							$navParentID = $newPageInfo['id'];
							$navRootID = $newPageInfo['nav_root_id'] ? $newPageInfo['nav_root_id'] : $newPageInfo['id'];
						}
						else $navParentID = $navRootID = 0;
						$query = 
							"INSERT INTO `".$table."` 
							(hub_id, type, nav_parent_id, nav_root_id, nav_order, 
							user_id, user_parent_id, page_parent_id, page_title, page_title_url, created)
							VALUES 
							('".$row['id']."', '".$pageType."', '".$navParentID."', '".$navRootID."', '".$pageOrder."', 
							 '".$row['user_id']."', '".$uid."', '".$pageID."', '".$title."', 
							 '".$this->convertKeywordFolder($title)."', NOW())";
					}
					else {
						$query .= 
							"INSERT INTO `".$table."` 
							(hub_id, tracking_id, hub_name, user_id, user_parent_id, page_parent_id, 
							page_title, has_custom_form, has_tags, created)
							VALUES 
							('".$row['id']."', '".$row['tracking_id']."', '".$row['name']."', '".$row['user_id']."', 
							 '".$uid."', '".$pageID."', '".$title."', '".$pageInfo['has_custom_form']."', 
							 '".$pageInfo['has_tags']."', NOW())";
					}
					if(!$this->query($query)) $errors++;
				}
				if(!$errors) return true;
				else return false;
			}
			else return false;
		}
		else return false;
	}
	
	static function cpImageToUserDir($fromUserID, $toUserID, $filename)
	{
			//copy images over from parent's hub images folder
			$parentLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($fromUserID).'/hub/';
			$userLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($toUserID).'/hub/';
			return	@copy($parentLoc.$filename, $userLoc.$filename);
	}
	
	//V2 function to set up a new copy of a multi-user hub
	function setupMultiUserHubV2($newHubID, $muHubID, $userID, $parentID){
		$return['error'] = 1;
		//get info on the MU Hub to be copied
		$query = "SELECT * FROM hub WHERE id = '".$muHubID."' AND user_id = '".$parentID."'";
//echo 1;
		if($muHub = $this->queryFetch($query, NULL, 1)){
			//hub fields not to copy
			Qube::dev_dump("In MU HUB");
			$excludeFields = array('id', 'id_duped', 'cid', 'user_id', 'user_id_created', 'user_parent_id', 'hub_parent_id',
								   'tracking_id', 'name', 'domain', 'mobile_domain', 'website_url', 'seo_url', 'connect_blog_id', 'ssl_on', 'multi_user', 'multi_user_classes',
								   'multi_user_fields', 'last_updated', 'created', 'httphostkey');
			//copy ovesr all non-excluded hub row values
			$query = 
			"UPDATE hub SET ";
			foreach($muHub as $key=>$value){
				//loop through all non-excluded fields
				if(!in_array($key, $excludeFields)) $query .= " `".$key."` = '".addslashes($value)."', ";
			}
			$query .= 
			" hub_parent_id = '".$muHubID."' 
			  WHERE id = '".$newHubID."' AND user_id = '".$userID."' LIMIT 1";

			Qube::dev_dump($query);
			if($this->query($query)){
				//main update successful
				

				// update slides
				$this->query('DELETE FROM slides WHERE hub_id = ' . $newHubID);
				$this->query('INSERT INTO slides (hub_id, user_id, title, background, url, description, internalname) ' .
					'select ' . $newHubID . ', ' . $userID . ', title, background, url, description, internalname FROM slides s WHERE s.hub_id = ' . $muHubID);
				//

				//copy images over from parent's hub images folder
				$parentLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($parentID).'/hub/';
				$userLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($userID).'/hub/';
				$imgs = array('logo', 'bg_img', 'pic_one', 'pic_two', 'pic_three', 'pic_four', 'pic_five', 
							'pic_six', 'pic_seven', 'pic_eight', 'pic_nine', 'pic_ten');
				foreach($imgs as $key=>$value){
					if($muHub[$value]!=''){
						//if there is a preset value for this image, copy it over
						@copy($parentLoc.$muHub[$value], $userLoc.$muHub[$value]);
					}
				}
				//remove existing pages
				$query = "DELETE FROM hub_page2 WHERE hub_id = '".$newHubID."' AND user_id = '".$userID."'";
				$this->query($query);
				//copy MU pages (also copies page images)
				$copyPages = $this->copyV2MUPages($newHubID, $muHubID, $userID, $parentID);
				if($e = mysqli_error($this->getLastLink())) $return['message'] = $e;
				if($copyPages['success']) $return['error'] = 0;
				else $return['message'] = $copyPages['message'];
			}
			else $return['message'] = 'Error copying hub: '.mysqli_error($this->getLastLink());
		}
		else $return['message'] = 'Error getting parent hub to copy.';
		Qube::dev_dump($return);
		return $return;
	}
	//Helper function to copy V2 pages for MU Hubs
	function copyV2MUPages($newHubID, $muHubID, $userID, $parentID, $navParentID = '', $muNavParentID = '', $navRootID = ''){
		$return['success'] = 0;
		$errors = 0;
		//get items to copy
		$query = "SELECT * FROM hub_page2 WHERE hub_id = '".$muHubID."' AND user_id = '".$parentID."' ";
		$query .= " AND hub_page2.trashed = '0000-00-00' ";
		if($muNavParentID) $query .= " AND nav_parent_id = '".$muNavParentID."' ";
		else $query .= " AND nav_parent_id = 0 ";
		if($muPages = $this->query($query)){
			while($row = $this->fetchArray($muPages, 1)){
				$excludePageFields = array('id', 'hub_id', 'user_id', 'user_parent_id', 'nav_parent_id', 'page_parent_id',
										   'nav_root_id', 'multi_user', 'multi_user_fields', 'last_edit', 'created');
				$query = "INSERT INTO hub_page2 (";
				foreach($row as $key=>$value){
					if(!in_array($key, $excludePageFields)) $query .= $key.", ";
				}
				$query .= 
				"hub_id, user_id, user_parent_id, nav_parent_id, nav_root_id, page_parent_id, created)
				VALUES (";
				foreach($row as $key=>$value){
					if(!in_array($key, $excludePageFields)) $query .= "'".addslashes($value)."', ";
				}
				$query .= 
				"'".$newHubID."', '".$userID."', '".$parentID."', '".$navParentID."', '".$navRootID."', '".$row['id']."', NOW())";
				if($this->query($query)){
					$newID = $this->getLastId();
					if($row['type']=='nav'){
						//if the item was a nav section, recurse through this function again with the proper vars
						if($row['nav_root_id']>0){
							//if the copied nav was a main section (has same parent_id as root_id)
							//use the newly created nav section's ID as its child pages' root ID
							if($row['nav_root_id']==$row['nav_parent_id']) $rootID = $newID;
							//otherwise it must be a sub-nav, get this sub-nav's root ID
							else $rootID = $this->getNavRoot($newID, $userID);
						}
						//recurse:
						$copy = $this->copyV2MUPages($newHubID, $muHubID, $userID, $parentID, $newID, $row['id'], $rootID);
						if(!$copy['success']){
							$return['message'] .= "Error copying a sub-page or nav: ".$copy['message'];
							$errors++;
						}
					}
					else if($row['type']=='page'){
						//make sure the page's nav_root_id is being set properly
						if(!$rootID){
							$query = "UPDATE hub_page2 SET nav_root_id = '".$this->getNavRoot($newID, $userID)."' WHERE id = '".$newID."' LIMIT 1";
							$this->query($query);
						}
						//copy images over from parent's hub images folder
						$parentLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($parentID).'/hub_page/';
						$userLoc = QUBEROOT . 'users/' .UserModel::GetUserSubDirectory($userID).'/hub_page/';
						if($parentLoc != $userLoc && $row['page_photo']){
							//if there is a preset value for this image, copy it over
							if(!@copy($parentLoc.$row['page_photo'], $userLoc.$row['page_photo'])){
								Qube::dev_dump(array("Parent" => $parentLoc, "user" => $userLoc));
								$return['message'] .= "Error copying a page's photo.\n";
								$errors++;
							}
						}
					}
				}
				else $return['message'] .= "Error copying a page/nav: ".mysqli_error($this->getLastLink())."\n";
				$newID = $rootID = NULL; //clear vars for next iteration
			} //end while($row = $this->fetchArray($muPages, 1))
			if(!$errors) $return['success'] = true;
		} //end if($muPages = $this->query($query))
		else $return['message'] = "Error getting pages to copy: ".mysqli_error($this->getLastLink());
		return $return;
	}
	
	//Push new field value to all hubs using specified multi-user theme
	function pushMultiUserHubValueV2($hubID, $field, $value, $uid, $table = '', $keepCustomVals = 1, $pageID = ''){
		if(is_array($value)){
			list($valuestr, $value)	=	$value;
		}else
			$valuestr	=	$value;

		if(!$table) $table = 'hub';
		$idField = ($table=='hub_page2') ? 'hub_id' : 'id';
		//make sure theme is multi-user and the user has permission
		$query = "SELECT user_id, multi_user FROM hub WHERE id = '".$hubID."'";
		$hubInfo = $this->queryFetch($query, NULL, 1);
			$da	=	NULL; #echo $field;
		if(($hubInfo['multi_user']) && ($hubInfo['user_id']==$uid)){
			
			$ispic	=	preg_match('/^pic_/', $field);
			$isNewPic	=	false;

			if($ispic)
			{
				$da	=	new HubDataAccess();
				$originalVal	=	$da->getPicValue($hubID, $field);
				$isNewPic	=	$originalVal	===	false;
				$da->updatePic($hubID, $field, $valuestr);
			}else{
				//get the old value to check for user customized fields
				$query = "SELECT `".$field."` FROM `".$table."` WHERE `".$idField."` = '".$hubID."'";
				if($table=='hub_page2') $query .= " AND id = '".$pageID."'";
				$a = $this->queryFetch($query);
				$originalVal = $this->sanitizeInput($a[$field]);
			}
			//if checks are passed, run query
			//first check to see if the value contains a form embed or tag code
			if(preg_match("/\|@form-(.*?)@\|/", $value)){
				$otherUpdate = ", has_custom_form = 1 ";
			}
			if(preg_match("/(\(\((.*?)\)\))/", $value)){
				$otherUpdate .= ", has_tags = 1 ";
			}
			if($table=='hub_page2' && $field=='page_title'){
				$otherUpdate .= ", page_title_url = '".$this->convertKeywordFolder($value)."' ";
			}
			//if the field being updated is a nav section's default_page on a mu hub
			if($table=='hub_page2' && $field=='default_page' && $value>0){
				//update child pages with their proper IDs
				$query = "SELECT id, nav_parent_id FROM hub_page2 WHERE page_parent_id = '".$value."'";
				if($childPages = $this->query($query)){
					while($row = $this->fetchArray($childPages, 1)){
						$query = "UPDATE hub_page2 SET default_page = '".$row['id']."' 
								  WHERE id = '".$row['nav_parent_id']."' LIMIT 1";
						$this->query($query);
					}
				}
				//also update the control hub
				$query = "UPDATE hub_page2 SET default_page = '".$value."' WHERE id = '".$pageID."' LIMIT 1";
			}
			//if the field is a mu hub page's parent_nav_id
			else if($table=='hub_page2' && $field=='nav_parent_id'){
				//update child pages with their proper IDs
				if($value>0) $query = "SELECT id FROM hub_page2 WHERE page_parent_id = '".$value."'";
				else $query = "SELECT id FROM hub_page2 WHERE page_parent_id = '".$pageID."'";
				if($childPages = $this->query($query)){
					while($row = $this->fetchArray($childPages, 1)){
						if($value>0){
							$query = "UPDATE hub_page2 SET nav_parent_id = '".$row['id']."' 
									  WHERE page_parent_id = '".$pageID."' LIMIT 1";
						}
						else $query = "UPDATE hub_page2 SET nav_parent_id = 0 WHERE id = '".$row['id']."' LIMIT 1";
						$this->query($query);
					}
				}
				//also update the control hub
				$query = "UPDATE hub_page2 SET nav_parent_id = '".$value."' WHERE id = '".$pageID."' LIMIT 1";
			}
			//otherwise do the update
			else {
				if($ispic)
				{
					list($internalname, $xolumn)	=	HubDataAccess::getColumnInfo($field);

					if($isNewPic)
					{
						$query	=	"INSERT ignore INTO slides (user_id, hub_id, internalname, $xolumn) ".
							'select user_id, id, "' . $internalname . '", "' . $value . '" from hub WHERE hub_parent_id = ' . $hubID;
							
					}else{			
						$query="update slides s,  (select user_id, hub_id, internalname, $xolumn from slides where hub_id = $hubID AND internalname='$internalname') as t SET s.`$xolumn` = '$value' 
WHERE s.internalname = '$internalname' AND s.hub_id IN (SELECT id FROM hub WHERE hub_parent_id = $hubID)" ;
						if($keepCustomVals)
							$query .= ' AND s.`' . $xolumn . '` = "' . $originalVal . '"';
					}					
				}else{
/*
				$query = "UPDATE `".$table."` SET `".$field."` = '".$value."'".$otherUpdate;
				if($table=='hub')
					$query .= " WHERE id = '".$hubID."' OR hub_parent_id = '".$hubID."'";
				else if($table=='hub_page2'){
					$query .= " WHERE (hub_id = '".$hubID."' OR 
								(SELECT hub.hub_parent_id FROM hub WHERE hub.id = hub_page2.hub_id) = '".$hubID."') ";
					$query .= " AND (page_parent_id = '".$pageID."' OR id = '".$pageID."') ";
				}
				$query .= " AND (user_parent_id = '".$uid."' OR user_id = '".$uid."') ";
				if($keepCustomVals){
					$query .= " AND `".$field."` = '".addslashes($originalVal)."'";
				}
*/
#                               $query = "UPDATE `".$table."` SET `".$field."` = '".$value."'".$otherUpdate;
                                if($table=='hub'){
                                        $query = "UPDATE `".$table."` p SET `".$field."` = '".$value."'".$otherUpdate;
                                        $query .= " WHERE id = '".$hubID."' OR hub_parent_id = '".$hubID."'";
                                }else if($table=='hub_page2'){ /*
UPDATE `hub_page2` p inner join hub  h  on (h.id = p.hub_id)
        SET p.`page_region` = 'city never sleep'
        WHERE (p.hub_id = '103333' OR h.hub_parent_id = '103333')  AND (p.page_parent_id = '1802473' OR p.id = '1802473')
 AND (p.user_parent_id = '6610' OR p.user_id = '6610')  AND p.`page_region` = '';
*/
                                        $query = "UPDATE `hub_page2` p inner join hub h on (h.id = p.hub_id) SET `".$field."` = '".$value."'".$otherUpdate;

                                        $query .= " WHERE (p.hub_id = '".$hubID."' OR h.hub_parent_id = '".$hubID."') ";
                                        $query .= " AND (p.page_parent_id = '".$pageID."' OR p.id = '".$pageID."') ";
                                }
                                $query .= " AND (p.user_parent_id = '".$uid."' OR p.user_id = '".$uid."') ";
                                if($keepCustomVals){
                                        $query .= " AND p.`".$field."` = '".$originalVal."'";
                                }

#		echo $query; exit;
					}// end else isset da
			}
			if($this->query($query))
			{
				if($ispic){
				$qsel	=	'SELECT DISTINCT user_id FROM hub WHERE hub_parent_id=' . $hubID . ' AND user_id != ' . $uid;
				$ret	=	$this->query($qsel);
				while($child_user_id	=	$this->fetchColumn($ret))
				{
					self::cpImageToUserDir($uid, $child_user_id, $value);
#					echo 'Updating .. ', $child_user_id, "\n";
				}
				}
				return true;
			}
			else return false;
		}
		else return false;
	}
	
	//get a hub's theme type
	function getThemeType($hubID){
		$query = "SELECT themes.type AS type FROM themes WHERE themes.id = (SELECT hub.theme FROM hub WHERE hub.id = '".$hubID."')";
		$result = $this->queryFetch($query);
		if($result){
			return $result['type'];
		}
	}
	
	//get a user's custom forms
	function getCustomForms($uid, $rid = '', $cid = '', $id = '', $multiUser = '', $process = '', $hasEmail = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$rid = is_numeric($rid) ? $rid : 0;
		$cid = is_numeric($cid) ? $cid : 0;
		$mu = $multiUser ? 1 : 0;
		if($mu) $cid = NULL;
		//authorize
		if($uid==$rid){
			$isReseller = true;
			$continue = true;
		}
		else {
			$query = "SELECT parent_id, class FROM users WHERE id = '".$uid."'";
			$a = $this->queryFetch($query, NULL, 1);
			if($process){ //if this function is being called by the process-custom-form script
				if($a['parent_id']){
					$rid = $a['parent_id'];
					$continue = true;
				}
				else $continue = true;
			}
			else {
				if($rid==$a['parent_id'] || !$a['parent_id']) $continue = true;
			}
		}
		
		if($continue){
			//build query
			if($id) $query = "SELECT * ";
			else $query = "SELECT id, name, timestamp ";
			$query .= " FROM lead_forms WHERE user_id = '".$uid."' 
					 AND lead_forms.trashed = '0000-00-00' ";
			if($mu) $query .= " AND multi_user = 1 ";
			else if(!$id && !$isReseller) $query .= " AND multi_user = 0 ";
			if($cid && $isReseller) $query .= " AND (cid = '".$cid."' OR cid = 99999)";
			else if($cid) $query .= " AND cid = '".$cid."' ";
			if($hasEmail) $query .= " AND (data LIKE 'email|-|%' OR data LIKE '%[==]email|-|%') ";
			if($id){
				$query .= " AND id = '".$id."' ";
				$return = $this->queryFetch($query, NULL, 1);
#	mail('error.6qube@sofus.mx', 'data', print_r(Qube::get_settings(), true));
			}
			else {
				if($result = $this->query($query)){
					$forms = array();
					while($row = $this->fetchArray($result, 1)){
						$forms[$row['id']] = $row;
					}
					$return = $forms;
				}
				else $return = false;
			}
			//if user is a reseller client check to see if the reseller has any MU forms they have access to
			if($rid && !$mu && !$isReseller){
				if($id && $return){
					//if the user is trying to get the full row of a non-MU form, do nothing
				}
				else {
					if($id) $query = "SELECT * ";
					else $query = "SELECT id, name, timestamp ";
					$query .= " FROM lead_forms WHERE user_id = '".$rid."' 
							 AND lead_forms.trashed = '0000-00-00'   
							 AND multi_user = 1 
							 AND no_access_classes NOT LIKE '".$a['class']."::%' 
							 AND no_access_classes NOT LIKE '%::".$a['class']."::%' 
							 AND no_access_classes NOT LIKE '%::".$a['class']."' ";
					if($id){
						$query .= " AND id = '".$id."' ";
						$return = $this->queryFetch($query, NULL, 1);
					}
					else {
						if($result2 = $this->query($query)){
							if(!$forms) $forms = array();
							while($row2 = $this->fetchArray($result2, 1)){
								$forms[$row2['id']] = $row2;
							}
							$return = $forms;
						}
					}
				}
			}
			
			//return final data!
			return $return;
		}
		else return false;
	}
	
	//add a new custom form
	function addNewCustomForm($name, $uid, $rid, $cid, $multiUser = ''){
		$name = $this->validateText($name);
		$uid = is_numeric($uid) ? $uid : '';
		$rid = is_numeric($rid) ? $rid : 0;
		$cid = is_numeric($cid) ? $cid : 0;
		$mu = $multiUser ? 1 : 0;
		if($mu) $cid = 99999;
		
		if($name && $uid){
			$query = "INSERT INTO lead_forms 
					(user_id, user_parent_id, cid, name, options, multi_user) 
					VALUES
					('".$uid."', '".$rid."', '".$cid."', '".$name."', '1||Submit||', '".$mu."')";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	
	//parse a text array of a form's fields into a PHP array
	function parseCustomFormFields($data){
            return self::parseCustomFormFieldsStatic($data);
        }
        
	//parse a text array of a form's fields into a PHP array
	static function parseCustomFormFieldsStatic($data){
		if($data){
			$returnData = array();
			$i = 0;
			if($fields = explode('[==]', trim($data, '[==]'))){
				foreach($fields as $field=>$values){
					$params = explode('|-|', $values);
					$returnData[$i]['type'] = $params[0];
					//shared params
					if($returnData[$i]['type']!='name'){
						$returnData[$i]['label'] = $params[1];
						$returnData[$i]['label_note'] = $params[2];
						$returnData[$i]['required'] = $params[3];
					}
					//input-specific params
					if($returnData[$i]['type']=='text'){
						$params2 = explode('|=|', $params[4]);
						$returnData[$i]['default'] = $params2[0];
						$returnData[$i]['numsOnly'] = $params2[1];
						$returnData[$i]['size'] = $params2[2];
					}
					else if($returnData[$i]['type']=='textarea'){
						$params2 = explode('|=|', $params[4]);
						//$params3 = explode('|+|', $params[5]);
						$returnData[$i]['default'] = $params2[0];
						$returnData[$i]['size'] = $params2[1];
						//$returnData[$i]['width'] = $params3[0];
						//$returnData[$i]['height'] = $params3[1];
					}
					else if($returnData[$i]['type']=='select'){
						if($params2 = explode('|=|', $params[4])){
							$returnData[$i]['options'] = array();
							$params3 = array();
							$j = 0;
							foreach($params2 as $key=>$value){
								$params3[$j] = explode('|+|', $value);
								$returnData[$i]['options'][$j]['key'] = $params3[$j][0];
								$returnData[$i]['options'][$j]['value'] = $params3[$j][1];
								$j++;
							}
						}
					}
					else if($returnData[$i]['type']=='checkbox'){
						$returnData[$i]['checked'] = $params[4];
					}
					else if($returnData[$i]['type']=='hidden'){
						$returnData[$i]['default'] = $params[4];
					}
					else if($returnData[$i]['type']=='email'){
						$params2 = explode('|=|', $params[4]);
						$returnData[$i]['default'] = $params2[0];
						$returnData[$i]['size'] = $params2[1];
					}
					else if($returnData[$i]['type']=='money'){
						$returnData[$i]['default'] = $params[4];
					}
					else if($returnData[$i]['type']=='date'){
						$params2 = explode('|=|', $params[4]);
						$returnData[$i]['default'] = $params2[0];
						if($returnData[$i]['default']=='custom'){
							$params3 = explode('|+|', $params2[1]);
							$returnData[$i]['defaultM'] = $params3[0];
							$returnData[$i]['defaultD'] = $params3[1];
							$returnData[$i]['defaultY'] = $params3[2];
						}
					}
					else if($returnData[$i]['type']=='name'){
						$params2 = explode('|=|', $params[1]);
						$returnData[$i]['label1'] = $params2[0];
						$returnData[$i]['label2'] = $params2[1];
						$params3 = explode('|=|', $params[2]);
						$returnData[$i]['default1'] = $params3[0];
						$returnData[$i]['default2'] = $params3[1];
						$params4 = explode('|=|', $params[3]);
						$returnData[$i]['required1'] = $params4[0];
						$returnData[$i]['required2'] = $params4[1];
					}
					$i++;
					$params = $params2 = $params3 = $params4 = NULL;
				} //end foreach($fields as $field=>$params)
				return $returnData;
			} //end if($fields = explode('[==]', trim($data, '[==]')))
			else return false;
		}
		else return false;
	}
	
	//get a user's custom form responses
	function getCustomFormLeads($uid, $formID = '', $leadID = '', $limit = '', $cid = '', $hubID = ''){
		if(is_numeric($uid)){
			if($leadID) $query = "SELECT * ";
			else $query = "SELECT id, lead_form_id, hub_id, data, search_engine, keyword, created ";
			$query .= " FROM leads WHERE user_id = '".$uid."' ";
			if($leadID && is_numeric($leadID)) $query .= " AND id = '".$leadID."' ";
			if($formID && is_numeric($formID)) $query .= " AND lead_form_id = '".$formID."' ";
			if($cid && is_numeric($cid)) $query .= " AND (SELECT lead_forms.cid FROM lead_forms WHERE lead_forms.id = leads.lead_form_id) = '".$cid."' ";
			if($hubID && is_numeric($hubID)) $query .= " AND hub_id = '".$hubID."' ";
			$query .= " ORDER BY id DESC ";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			
			if($leadID) $result = $this->queryFetch($query);
			else $result = $this->query($query);
			
			return $result;
		}
	}
	// deprecated function
        function embedForms($fields, $row, $userID, $hubID = '', $referrerID = '', $pageID = '', $searchEngine = '', $seKeyword = ''){
            $hub    =   Qube::Start()->queryObjects('HubModel')->Where('id = %d', $hubID)->Exec()->fetch();
            return self::embedFormsStatic($fields, $row, $userID, $hub, $referrerID, $pageID, $searchEngine, $seKeyword);
        }
	
	//scan provided row/fields for form embed codes and replace with the form HTML
	static function embedFormsStatic($fields, $row, $userID, HubModel $hub, $referrerID = '', $pageID = '', $searchEngine = '', $seKeyword = ''){
		$return = array('hasCustomForm' => false);  // always initialize variables.
//		$form_ids	=	array();
		if($userID && $fields && is_array($fields)){
			foreach($fields as $key=>$field) {
				if (is_string($row[$field])) {
					$a = array();
					if (preg_match_all("/\|@form-(.*?)@\|/", $row[$field], $a)) {
						//for each form embed (now supports multiple on a page)
						if ($a[1]) {
							foreach ($a[1] as $key2 => $formID) {
									$formData = FormOutput::getFormOutput($userID, $formID, $hub, $searchEngine, $seKeyword, $pageID, $referrerID);

									//replace embed code with form html
									$row[$field] = str_replace('|@form-'.$formID.'@|', $formData, $row[$field]);
									$return['hasCustomForm'] = true;

									$formData = NULL;
							}
						}
					}
				}
				$formID = $formData = $a = NULL;
			}
		}
		$return['row'] = $row;
		return $return;
	}
	
	//quickly check to see if a hub has any pages with custom forms
	function checkPagesForForms($hubID, $v2 = ''){
		$table = $v2 ? 'hub_page2' : 'hub_page';
		if(is_numeric($hubID)){
			$query = "SELECT count(id) FROM ".$table." WHERE hub_id = '".$hubID."' AND has_custom_form = 1
					 AND ".$table.".trashed = '0000-00-00'";
			$result = $this->queryFetch($query, NULL, 1);
			if($result['count(id)']) return true;
			else return false;
		}
	}
	
	//sanitize a custom form field's label name for use as the input's name attribute
	function fieldNameFromLabel($input){
            return self::fieldNameFromLabelStatic($input);
        }
        
	//sanitize a custom form field's label name for use as the input's name attribute
	static function fieldNameFromLabelStatic($input){
		$output = strtolower($input);
		$output = str_replace(' ', '', $output);
		$output = Local::stripNonAlphaNum($output);
		
		return $output;
	}
	
	//get a custom form respondent's name/email from the data string
	function leadInfoFromData($dataString = '', $leadID = '', $returnAll = ''){
		if(!$dataString && is_numeric($leadID)){
			$query = "SELECT data FROM leads WHERE id = '".$leadID."'";
			$a = $this->queryFetch($query, NULL, 1);
			$dataString = $a['data'];
		}
		$fields = explode('[==]', $dataString);
		if($fields && is_array($fields)){
			foreach($fields as $field=>$data){
				if(substr($data, 0, 6)=='name_1'){
					$fieldData = explode('|-|', $data);
					$firstName = $fieldData[2];
				}
				else if(substr($data, 0, 6)=='name_2'){
					$fieldData = explode('|-|', $data);
					$lastName = $fieldData[2];
				}
				else if(substr($data, 0, 4)=='text' && substr($data, 8, 12)=='Name'){
					$fieldData = explode('|-|', $data);
					$name = $fieldData[2];
				}
				else if(substr($data, 0, 5)=='email'){
					$fieldData = explode('|-|', $data);
					$email = $fieldData[2];
				}
				
				if($returnAll){
					if(!$return['fields']) $return['fields'] = array();
					if(!$fieldData) $fieldData = explode('|-|', $data);
					if(!$return['fields'][$fieldData[1]]) $return['fields'][$fieldData[1]] = $fieldData[2];
				}
				$fieldData = NULL;
			}
		}
		if($name) $return['name'] = $name;
		else $return['name'] = $firstName.' '.$lastName;
		$return['email'] = $email;
		
		return $return;
	}
	
	//get a user's custom form emailers
	function getFormEmailers($userID, $formID = '', $emailerID = ''){
		$userID = is_numeric($userID) ? $userID : '';
		$formID = is_numeric($formID) ? $formID : '';
		$emailerID = is_numeric($emailerID) ? $emailerID : '';
		
		if($userID){
			if($emailerID) $query = "SELECT * ";
			else $query = "SELECT id, name ";
			$query .= " FROM lead_forms_emailers WHERE user_id = '".$userID."' 
			  AND lead_forms_emailers.trashed = '0000-00-00'";
			if($formID) $query .= " AND lead_form_id = '".$formID."' ";
			if($emailerID){
				$query .= " AND id = '".$emailerID."' ";
				$return = $this->queryFetch($query, NULL, 1);
			}
			else {
				$result = $this->query($query);
				if($result && $this->numRows($result)) $return = $result;
			}
			if($return) return $return;
			else return false;
		}
		else return false;
	}
	
	//add a new custom form mailer
	function addNewFormEmailer($userID, $formID, $emailerName){
		$userID = is_numeric($userID) ? $userID : '';
		$formID = is_numeric($formID) ? $formID : '';
		
		if($userID && $formID){
			$query = "INSERT INTO lead_forms_emailers 
					(user_id, lead_form_id, name, sched_date) 
					VALUES 
					('".$userID."', '".$formID."', '".$emailerName."', '".date('m/d/Y')."')";
			if($this->query($query)){
				return $this->getLastId();
			}
			else return false;
		}
		else return false;
	}
	
	//parse a hub's req_theme_type value (could be a single number or multiple, comma-separated)
	//returns array with value(s)
	function parseReqThemeType($type = '', $hubID = ''){
		//moved function to Local class
		return parseHubReqThemeType($type, $hubID);
	}
	
	//Pages V2: get a nav section's highest order num
	function getMaxOrderNum($navID, $hubID){
		$query = "SELECT count(id), MAX(nav_order) as highest FROM hub_page2 
				  WHERE nav_parent_id = '".$navID."' AND hub_id = '".$hubID."'";
		$a = $this->queryFetch($query, NULL, 1);
		if($a['count(id)']){
			return $a['highest'];
		}
		else return false;
	}

	//Pages V2: output the nav/pages onto the site
	function dispV2Nav($thisNav, $parentTitle='', $active='', $customWraps='', $iterate='', $footer='', $urlBase='/'){
            
	if($_GET['amado']) var_dump(func_get_args());
            if(!$urlBase)   $urlBase    =   $this->baseUrl; // defined in HubOutput::printRequest
            $is_section =   array_key_exists(0, $thisNav);

            $aClass = isset($customWraps['aClass']) ? $customWraps['aClass'] : '';
            $aClassActive = isset($customWraps['aClassActive']) ? $customWraps['aClassActive'] : 'active';
            $liClassActive = isset($customWraps['liClassActive']) ? $customWraps['liClassActive'] : '';
            
		// the FULL path to the na, or page. (with navs included)
/*
		$obj_path_full	=	$is_section ? (!empty($thisNav[0]['nav_url']) ? 
								$thisNav[0]['nav_url'] : '') : 
					(@$thisNav['page_path']);
  */

		$obj_path_full	=	$is_section ? 
						(!empty($thisNav[0]['nav_url']) ? $thisNav[0]['nav_url'] : $thisNav[0]['page_title_url'] . '/') : 
					(@$thisNav['page_path']);
          
            if($is_section || !$thisNav['outbound_url']){
                    if($parentTitle) $parentTitle .= '/';
			// if obj_path_full, dont convert keyword or other shit.
                    $navBase = $parentTitle && !$obj_path_full ? $this->convertKeywordFolder($parentTitle,0) :	$obj_path_full; //convert parent nav/folder title
            }
	$urlBase	=	rtrim($urlBase, '/') . '/';	// ADD SLASH

            
            $thisTitle = !$is_section && $thisNav['page_title'] ? $thisNav['page_title'] : $thisNav[0]['page_title'];
            $thisUrl = !$is_section && $thisNav['outbound_url'] ? $thisNav['outbound_url'] : $urlBase.$navBase;

            if($iterate){
                    //clear selected vars if this is a sub-iteration
                    if(!empty($customWraps['aOuterIterate'])) $customWraps['aOuter1'] = $customWraps['aOuter2'] = '';
                    if(!empty($customWraps['aInnerIterate'])) $customWraps['aInner1'] = $customWraps['aInner1'] = '';
            }
            
		if($customWraps){
			//build page/nav link element
			$a = $customWraps['aOuter1'].'<a href="'.$thisUrl.'" title="'.$thisTitle.'" class="';
			if($thisTitle==$active) $a .= $aClassActive.'"';
			else if($aClass)  $a .= $aClass.'"';
			else $a = substr($a, 0, -8);
			if($thisNav['outbound_url']) $a .= ' target="'.$thisNav['outbound_target'].'"';
			if($customWraps['aExtra']) $a .= ' '.$customWraps['aExtra'].'>'.$customWraps['aInner1'];
			else if($customWraps['aExtraDrop'] && $thisNav[0]['type']=='nav') $a .= ' '.$customWraps['aExtraDrop'].'>'.$customWraps['aInner1'];
			else $a .= '>'.$customWraps['aInner1'];
			$a2 = $customWraps['aInner2'].'</a>'.$customWraps['aOuter2'];
			//other elements
			$ul = $customWraps['outer1'] ? $customWraps['outer1'] : '<ul>';
			if($iterate && $customWraps['outer1Sub']) $ul = $customWraps['outer1Sub'];
			$ul2 = $customWraps['outer2'] ? $customWraps['outer2'] : '</ul>';
			if($iterate && $customWraps['outer2Sub']) $ul2 = $customWraps['outer2Sub'];
			if($customWraps['inner1']) $li = $customWraps['inner1'];
			else if($customWraps['inner1Drop'] && $thisNav[0]['type']=='nav') $li = $customWraps['inner1Drop'];
			else $li = '<li>';
			if($iterate && $customWraps['inner1Sub']) $li = $customWraps['inner1Sub'];
			if($liClassActive && ($thisTitle==$active)) $li = $this->insertClassIntoElement($li, $liClassActive);
			$li2 = $customWraps['inner2'] ? $customWraps['inner2'] : '</li>';
			if($iterate && $customWraps['inner2Sub']) $li2 = $customWraps['inner2Sub'];
		}
		else {
			//default values
			$a = '<a href="'.$thisUrl.'" title="'.$thisTitle.'"';
			if(!$is_section && $thisNav['outbound_url']) $a .= ' target="'.$thisNav['outbound_target'].'"';
			if($thisTitle==$active) $a .= ' class="active"';
			$a .= '>'; $a2 = '</a>';
			$ul = '<ul>'; $ul2 = '</ul>';
			$li = '<li>'; $li2 = '</li>';
		}

            if($is_section)          // process a $thisNav which consists of a list of (parent, child, child)
            {
                //must be nav section, iterate through it
                // initializing some variables
                $noUrl  =   ($thisNav[0]['default_page']==-1);
                $defaultPageTitle   =   '';
		$default_page_key	=	1;	// the first page in the list.
                
                foreach($thisNav as $key=>$value){
                        if($thisNav[0]['default_page'] && !$noUrl){
                                if($value['id']==$thisNav[0]['default_page']){
				$default_page_key	=	$key;
				$defaultPageTitle = $value['page_title_url'];
				$thisNav['outbound_url']	=	$value['outbound_url'];
                        	}
                        }
						// if the enumerated page has inc_nav != 0, then it is included in the menu, so set hasVisiblePages = true
                        if($key>0 && $value['inc_nav']) $hasVisiblePages = true;
                }
                if(!$defaultPageTitle) $defaultPageTitle = $thisNav[1]['page_title_url'];
                if($noUrl) $navLink = str_replace($thisUrl, '#', $a);
                else {
                        if(!$thisNav['outbound_url']){	// meaning no default page.. 
#		echo '<!-- ', print_r($thisNav), ' outboundurl', $default_page_key, ' ', $thisUrl, '-', $thisNav[$default_page_key]['page_title_url'], " $urlBase navBase: $navBase is:$is_section, obj_path_Full: ", $obj_path_full, " navUrl: ", $thisNav[0]['nav_url'], "oburl ", $thisNav[0]['outbound_url'], " \n -->";
				$navLink	=	str_replace($thisUrl, 
					HubLegacyWrapper::GetPageURLLegacy_NavType($thisNav[$default_page_key], $urlBase . $navBase
//		HubLegacyWrapper::GetPageURLLegacy_NavType($thisNav[0], $urlBase.$navBase)
	),
						$a);

/*
                                $navLink = str_replace('#0', (
					$obj_path_full ? $obj_path_full : 
						$thisNav[0]['page_title_url'].'/').$defaultPageTitle.'/', $a);
/* */
                        }   
                        else{
echo '<!-- replaced -->';
					// replace the href value with the default page url
				$navLink = str_replace($thisUrl, $thisNav['outbound_url'], $a);
                }
                }
#echo '<!-- issection: ', $is_section, ' ', 'inc_nav:', $thisNav[0]['inc_nav'], ',', $footer, ',', $hasVisiblePages, ' -->'; 
            
            /*
		$customWraps['aInner1'] =       $customWraps['aInner1Sub'];
       $customWraps['aInner2'] =       $customWraps['aInner2Sub'];

                if(!$footer && $hasVisiblePages && $thisNav[0]['inc_nav']){
                        echo $li.$navLink.$thisNav[0]['page_title'].$a2.$ul;
                        // end old
                        */
                                        if(!$footer && $hasVisiblePages && $thisNav[0]['inc_nav']){
#			echo '<!-- amado -->';
                        echo $li.$navLink.$thisNav[0]['page_title'].$a2.$ul;
	$customWraps['aInner1']	=	$customWraps['aInner1Sub'];
	$customWraps['aInner2']	=	$customWraps['aInner2Sub'];


                        foreach($thisNav as $key=>$value){
                                //iterate through nav children
                                if($key>0)
                                $this->dispV2Nav($value, $parentTitle.$thisNav[0]['page_title'], $active, $customWraps, 1, NULL, $urlBase);
                        }
                        echo $ul2.$li2;
                }
                //only output nav name and link to first page for footer
                else if($hasVisiblePages && $thisNav[0]['inc_nav']) echo $li.$navLink.$thisNav[0]['page_title'].$a2.$li2;                
            }else
		//output html
		if($thisNav['page_title_url'] && $thisNav['inc_nav']){ //must be a page
			if(!$thisNav['outbound_url']){
				$url = $thisNav['page_title_url'].'/';
				$a = str_replace($thisUrl, HubLegacyWrapper::GetPageURLLegacy_NavType($thisNav, $thisUrl), $a);
			}
			echo $li.$a.$thisNav['page_title'].$a2.$li2;
		}
	}
	

	//Pages V2: output the nav/pages onto the site
	function dispV2Nav_old($thisNav, $parentTitle='', $active='', $customWraps='', $iterate='', $footer='', $urlBase=''){
		if(!$thisNav['outbound_url']){
			if($parentTitle) $parentTitle .= '/';
			if($urlBase && substr($urlBase, -1)=='/') $urlBase = substr($urlBase, 0, -1); //remove trailing slash
			$navBase = $parentTitle ? $this->convertKeywordFolder($parentTitle,0) : ''; //convert parent nav/folder title
		}
		$thisTitle = $thisNav['page_title'] ? $thisNav['page_title'] : $thisNav[0]['page_title'];
		$thisUrl = $thisNav['outbound_url'] ? $thisNav['outbound_url'] : $urlBase.'/'.$navBase.'#0';
		$aClass = isset($customWraps['aClass']) ? $customWraps['aClass'] : '';
		$aClassActive = isset($customWraps['aClassActive']) ? $customWraps['aClassActive'] : 'active';
		$liClassActive = isset($customWraps['liClassActive']) ? $customWraps['liClassActive'] : '';
		if($iterate){
			//clear selected vars if this is a sub-iteration
			if(!$customWraps['aOuterIterate']) $customWraps['aOuter1'] = $customWraps['aOuter2'] = '';
			if(!$customWraps['aInnerIterate']) $customWraps['aInner1'] = $customWraps['aInner1'] = '';
		}
		if($customWraps){
			//build page/nav link element
			$a = $customWraps['aOuter1'].'<a href="'.$thisUrl.'" title="'.$thisTitle.'" class="';
			if($thisTitle==$active) $a .= $aClassActive.'"';
			else if($aClass)  $a .= $aClass.'"';
			else $a = substr($a, 0, -8);
			if($thisNav['outbound_url']) $a .= ' target="'.$thisNav['outbound_target'].'"';
			if($customWraps['aExtra']) $a .= ' '.$customWraps['aExtra'].'>'.$customWraps['aInner1'];
			else $a .= '>'.$customWraps['aInner1'];
			$a2 = $customWraps['aInner2'].'</a>'.$customWraps['aOuter2'];
			//other elements
			$ul = $customWraps['outer1'] ? $customWraps['outer1'] : '<ul>';
			if($iterate && $customWraps['outer1Sub']) $ul = $customWraps['outer1Sub'];
			$ul2 = $customWraps['outer2'] ? $customWraps['outer2'] : '</ul>';
			if($iterate && $customWraps['outer2Sub']) $ul2 = $customWraps['outer2Sub'];
			if($customWraps['inner1']) $li = $customWraps['inner1'];
			else $li = '<li>';
			if($iterate && $customWraps['inner1Sub']) $li = $customWraps['inner1Sub'];
			if($liClassActive && ($thisTitle==$active)) $li = $this->insertClassIntoElement($li, $liClassActive);
			$li2 = $customWraps['inner2'] ? $customWraps['inner2'] : '</li>';
			if($iterate && $customWraps['inner2Sub']) $li2 = $customWraps['inner2Sub'];
		}
		else {
			//default values
			$a = '<a href="'.$thisUrl.'" title="'.$thisTitle.'"';
			if($thisNav['outbound_url']) $a .= ' target="'.$thisNav['outbound_target'].'"';
			if($thisTitle==$active) $a .= ' class="active"';
			$a .= '>'; $a2 = '</a>';
			$ul = '<ul>'; $ul2 = '</ul>';
			$li = '<li>'; $li2 = '</li>';
		}
		//output html
		if($thisNav['page_title_url'] && $thisNav['inc_nav']){ //must be a page
			if(!$thisNav['outbound_url']){
				$url = $thisNav['page_title_url'].'/';//$this->convertKeywordFolder($thisNav['page_title']).'/';
				$a = str_replace('#0', $url, $a);
			}
			echo $li.$a.$thisNav['page_title'].$a2.$li2;
		}
		else { //must be nav section, iterate through it
			if($thisNav[0]['default_page']==-1) $noUrl = true;
			foreach($thisNav as $key=>$value){
				if($thisNav[0]['default_page'] && !$noUrl){
					if($value['id']==$thisNav[0]['default_page']) $defaultPageTitle = $value['page_title_url'];
				}
				if($key>0 && $value['inc_nav']) $hasVisiblePages = true;
			}
			if(!$defaultPageTitle) $defaultPageTitle = $thisNav[1]['page_title_url'];
			if($noUrl) $navLink = str_replace($urlBase.'/'.$navBase.'#0', '#', $a);
			else {
				if(!$thisNav['outbound_url']){
					$navLink = str_replace('#0', $thisNav[0]['page_title_url'].'/'.$defaultPageTitle.'/', $a);
				}
				else $navLink = $a;
			}
			if(!$footer && $hasVisiblePages && $thisNav[0]['inc_nav']){
				echo $li.$navLink.$thisNav[0]['page_title'].$a2.$ul;
				foreach($thisNav as $key=>$value){
					//iterate through nav children
					if($key>0)
					$this->dispV2Nav($value, $parentTitle.$thisNav[0]['page_title'], $active, $customWraps, 1, NULL, $urlBase);
				}
				echo $ul2.$li2;
			}
			//only output nav name and link to first page for footer
			else if($hasVisiblePages && $thisNav[0]['inc_nav']) echo $li.$navLink.$thisNav[0]['page_title'].$a2.$li2;
		}
	}
	
	//get a V2 page's nav_root_id
	function getNavRoot($navID, $uid){
		if(is_numeric($navID)){
			$query = "SELECT id, nav_parent_id, nav_root_id FROM hub_page2 WHERE id = '".$navID."' AND user_id  = '".$uid."' LIMIT 1";
			$a = $this->queryFetch($query, NULL, 1);
			if($a['nav_parent_id']>0) $return = $this->getNavRoot($a['nav_parent_id'], $uid);
			else $return = $a['id'];
			return $return;
		}
		else return false;
	}
	
	//get a V2 page's nav root title
	function getNavRootName($rootID, $hubID){
		$query = "SELECT page_title FROM hub_page2 WHERE id = '".$rootID."' AND hub_id = '".$hubID."'";
		$result = $this->queryFetch($query, NULL, 1);
		return $result['page_title'];
	}
	
	
	
	static function getParsableFields()
	{
		return array('slogan', 'site_title', 'meta_description', 'meta_keywords', 'google_webmaster', 
							  'keyword1', 'keyword2', 'keyword3', 'keyword4',  'company_name',
			'keyword5', 'keyword6', 'coupon_offer', 'overview', 'offerings', 'adv_css',
							  'youtube', 'myspace', 'linkedin', 'google_local', 'twitter', 'facebook', 
							  'photos', 'videos', 'events', 'edit_region_6', 'pic_one_title', 'pic_one_desc', 
							  'pic_two_title', 'pic_two_desc', 'pic_three_title', 'pic_three_desc', 
							  'pic_four_title', 'pic_four_desc', 'pic_five_title', 'pic_five_desc', 
							  'pic_six_title', 'pic_six_desc', 'pic_seven_title', 'pic_eight_title', 
							  'edit_region_7', 'edit_region_8', 'edit_region_9', 'edit_region_10', 'edit_region_11', 'edit_region_12', 
							  'edit_region_13', 'edit_region_14', 'edit_region_15', 'add_autoresponder');
	}
	
	static function getTagReplacementValues($row)
	{
		$da = new BlogDataAccess();
		$posts	=	$da->getLatestPosts($row['connect_blog_id'], 5);

		$userinfo = new UserDataAccess();
		$ui = $userinfo->getUserInfo($row['user_id']);

		if($row['profile_override']==1) {
			return array('city' => ($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_city'] : HubRowModel::getFullState($ui['biz_state'])), 'state' => ($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])), 'citycommastate' => $ui['biz_city'] . ($ui['biz_city'] && $ui['biz_state'] ? ', ' : '') . ($ui['biz_city'] && $row['state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])),
				'company' => $ui['biz_company_name'], 'address' => $ui['biz_address'], 'phone' =>
					$ui['biz_phone_field'], 'zip' =>
					$ui['biz_zip'],
				'slogan' => $row['slogan'], 'customblock' => $row['slogan'], 'customblock1' => $row['coupon_offer'],
				'domain' => $row['domain'], 'coupon' => $row['coupon_offer'],
				'google_webmaster' => $row['google_webmaster'], 'google_tracking' => $row['google_tracking'],
				'youtube' => $row['youtube'], 'myspace' => $row['myspace'], 'linkedin' => $row['linkedin'], 'blog' => $row['blog'],
				'google' => $row['google_local'], 'twitter' => $row['twitter'], 'facebook' => $row['facebook'],
				'googleplaces' => $row['google_places'], 'instagram' => $row['instagram'], 'pinterest' => $row['pinterest'],
				'keyword1' => $row['keyword1'], 'keyword2' => $row['keyword2'], 'keyword3' => $row['keyword3'],
				'keyword4' => $row['keyword4'], 'keyword5' => $row['keyword5'], 'keyword6' => $row['keyword6'],
				'custom1' => $row['keyword1'], 'custom2' => $row['keyword2'], 'custom3' => $row['keyword3'],
				'custom4' => $row['keyword4'], 'custom5' => $row['keyword5'], 'custom6' => $row['keyword6'],
				'custom7' => $row['button_1'], 'custom8' => $row['button1_link'], 'custom9' => $row['button_2'],
				'custom10' => $row['button2_link'], 'custom11' => $row['button_3'], 'custom12' => $row['button3_link'], 'custom13' => $row['legacy_gallery_link'],
				'custom14' => $row['form_box_color'], 'custom15' => $row['full_home'], 'custom16' => $row['remove_font'], 'custom17' => $row['gallery_page'],
				'custom18' => $row['slider_option'], 'custom19' => $row['display_page'], 'custom20' => $row['remove_contact'],
				'edit1' => $row['overview'], 'edit2' => $row['offerings'], 'edit3' => $row['photos'],
				'edit4' => $row['videos'], 'edit5' => $row['events'], 'edit6' => $row['edit_region_6'],
				'edit7' => $row['edit_region_7'], 'edit8' => $row['edit_region_8'], 'edit9' => $row['edit_region_9'],
				'userid' => $row['user_id'], 'logo' => $row['logo'], 'logopath' => '/users/' . UserModel::GetUserSubDirectory($row['user_id']) . '/hub/' . $row['logo'] . '',
				'profilepath' => '/users/' . UserModel::GetUserSubDirectory($row['user_id']) . '/hub/' . $row['profile_photo'] . '',
				'profilesrc' => '/users' . $row['profile_photo'] . '',
				'profilefname' => $ui['firstname'],
				'profilelname' => $ui['lastname'],
				'logosrc' => '/users' . $row['logo'] . '',
				'bgsrc' => '/users' . $row['bg_img'] . '',
				'edit10' => $row['edit_region_10'], 'cityurl' => $row['city_url'], 'rotatephone' => '<span id="rotate"></span>',
				'rotatephone2' => '<span id="rotate2"></span>',
				'cityorfullstate' => ($ui['biz_city'] && $row['state'] ? $ui['biz_city'] : HubRowModel::getFullState($row['state'])),
				'citylowercase' => trim(strtolower(str_replace(' ', '', $ui['biz_city']))),
				'incitycommastate' => ($ui['biz_city'] || $ui['biz_state'] ? 'in ' : '') . $ui['biz_city'] . ($ui['biz_city'] && $ui['biz_state'] ? ', ' : '') . ($ui['biz_city'] && $ui['biz_state'] ? $ui['biz_state'] : HubRowModel::getFullState($ui['biz_state'])),
				'fullstate' => HubRowModel::getFullState($ui['biz_state']),
				'blogfeed' => BlogModel::getHTMLPosts($posts),
				'navname-children' => '', 'blogURL' => HubRowModel::getBlogURL($row));
		} else {
			return array('city' => ($row['city_url'] ? $row['city_url'] : ($row['city'] && $row['state'] ? $row['city'] : HubRowModel::getFullState($row['state']))), 'state' => ($row['city_url'] ? '' : ($row['city'] && $row['state'] ? $row['state'] : HubRowModel::getFullState($row['state']))), 'citycommastate' => ($row['city_url'] ? $row['city_url'] : $row['city'] . ($row['city'] && $row['state'] ? ', ' : '') . ($row['city'] && $row['state'] ? $row['state'] : HubRowModel::getFullState($row['state']))),
				'company' => $row['company_name'], 'address' => $row['street'], 'phone' => $row['phone'], 'zip' => $row['zip'],
				'slogan' => $row['slogan'], 'customblock' => $row['slogan'], 'customblock1' => $row['coupon_offer'],
				'domain' => $row['domain'], 'coupon' => $row['coupon_offer'],
				'google_webmaster' => $row['google_webmaster'], 'google_tracking' => $row['google_tracking'],
				'youtube' => $row['youtube'], 'myspace' => $row['myspace'], 'linkedin' => $row['linkedin'], 'blog' => $row['blog'],
				'google' => $row['google_local'], 'twitter' => $row['twitter'], 'facebook' => $row['facebook'],
				'googleplaces' => $row['google_places'], 'instagram' => $row['instagram'], 'pinterest' => $row['pinterest'],
				'keyword1' => $row['keyword1'], 'keyword2' => $row['keyword2'], 'keyword3' => $row['keyword3'],
				'keyword4' => $row['keyword4'], 'keyword5' => $row['keyword5'], 'keyword6' => $row['keyword6'],
				'custom1' => $row['keyword1'], 'custom2' => $row['keyword2'], 'custom3' => $row['keyword3'],
				'custom4' => $row['keyword4'], 'custom5' => $row['keyword5'], 'custom6' => $row['keyword6'],
				'custom7' => $row['button_1'], 'custom8' => $row['button1_link'], 'custom9' => $row['button_2'],
				'custom10' => $row['button2_link'], 'custom11' => $row['button_3'], 'custom12' => $row['button3_link'], 'custom13' => $row['legacy_gallery_link'],
				'custom14' => $row['form_box_color'], 'custom15' => $row['full_home'], 'custom16' => $row['remove_font'], 'custom17' => $row['gallery_page'],
				'custom18' => $row['slider_option'], 'custom19' => $row['display_page'], 'custom20' => $row['remove_contact'],
				'edit1' => $row['overview'], 'edit2' => $row['offerings'], 'edit3' => $row['photos'],
				'edit4' => $row['videos'], 'edit5' => $row['events'], 'edit6' => $row['edit_region_6'],
				'edit7' => $row['edit_region_7'], 'edit8' => $row['edit_region_8'], 'edit9' => $row['edit_region_9'],
				'userid' => $row['user_id'], 'logo' => $row['logo'], 'logopath' => '/users/' . UserModel::GetUserSubDirectory($row['user_id']) . '/hub/' . $row['logo'] . '',
				'profilepath' => '/users/' . UserModel::GetUserSubDirectory($row['user_id']) . '/hub/' . $row['profile_photo'] . '',
				'profilesrc' => '/users' . $row['profile_photo'] . '',
				'profilefname' => $ui['firstname'],
				'profilelname' => $ui['lastname'],
				'logosrc' => '/users' . $row['logo'] . '',
				'bgsrc' => '/users' . $row['bg_img'] . '',
				'edit10' => $row['edit_region_10'], 'cityurl' => $row['city_url'], 'rotatephone' => '<span id="rotate"></span>',
				'rotatephone2' => '<span id="rotate2"></span>',
				'cityorfullstate' => ($row['city_url'] ? $row['city_url'] : ($row['city'] && $row['state'] ? $row['city'] : HubRowModel::getFullState($row['state']))),
				'citylowercase' => trim(strtolower(str_replace(' ', '', $row['city']))),
				'incitycommastate' => ($row['city_url'] ? $row['city_url'] : ($row['city'] || $row['state'] ? 'in ' : '') . $row['city'] . ($row['city'] && $row['state'] ? ', ' : '') . ($row['city'] && $row['state'] ? $row['state'] : HubRowModel::getFullState($row['state']))),
				'fullstate' => HubRowModel::getFullState($row['state']),
				'blogfeed' => BlogModel::getHTMLPosts($posts),
				'navname-children' => '', 'blogURL' => HubRowModel::getBlogURL($row));
		}
	}
	
	static function ReplaceNavigationTags(&$row, $searchField, $hub)
	{
				//check to see if there were any tags that require regex to find them
				$navPagesArray = array();
				if($navPagesList=preg_match_all('/\(\(([a-z|0-9|\-|\=]{1,120})-children\)\)/i', $row[$searchField], $navPagesArray)){
					//added 6/9/12 by Reese.  Searches through and finds custom tags like ((navname-children))
					//replaces it with UL of pages under that navname
					//figure out if this is being included on a hub page or just somewhere else on the hub:
					$hubID = $row['hub_id'] ? $row['hub_id'] : $row['id'];
					foreach($navPagesArray[1] as $key=>$value){
						//value is going to be an array.  $key=0 will be the full string, like ((navname-children))
						//$key=1 will be just the 'navname' string.
						//So $navPagesArray[0][0] = '((navname-children))' and $navPagesArray[1][0] = 'navname'
						//check if the tag includes a reference to a specific nav section
						//like ((nav-parent=nav-section-children))
						$b = explode('=', $value);
						if(count($b)>1){
							$navParent = $b[0];
							$navName = $b[1];
						} else {
							$navParent	=	'';
							$navName = $b[0];
						}
						$currPageID	=	-1;
						if($row['type']=='page') $currPageID = $row['id'];
#Qube::Fatal('Temporarily Down', get_class($this), debug_backtrace());
						$row[$searchField] = str_replace('(('.$value.'-children))', 
							Hub::buildNavPagesList($hub, $navName, $hubID, $navParent, $currPageID), $row[$searchField]);
					}
				}
				unset($navPagesList, $navPagesArray, $b, $navParent, $navName);		
	}
	
	//find and replace custom tags
	function replaceTags($row, $searchFields = '', $replaceValues = ''){
		// FIELDS TO LOOK THROUGH FOR TAGS:
		if(!$searchFields){
			//these are the hub search fields by default
			//if it's a hub page being searched through, custom $searchFields array is passed
		$searchFields = self::getParsableFields();
		}
		// TAGS TO LOOK FOR AND THE VALUE TO REPLACE THEM WITH:
		if(!$replaceValues){
		$replaceValues = self::getTagReplacementValues($row);
		}
		// DO IT:
		foreach($searchFields as $searchFieldKey=>$searchField){
			if(preg_match('/\(\((.*)\)\)/i', $row[$searchField])){ //regex to check if this field has tag(s)
				foreach($replaceValues as $replaceKeyword=>$replaceValueRaw){
					if(is_callable($replaceValueRaw))
					{
						$replaceValue	=	call_user_func($replaceValueRaw, $searchField, $replaceKeyword, $row);
					}else
						$replaceValue	=	$replaceValueRaw;
					
					if($replaceKeyword=='citycommastate' || $replaceKeyword=='incitycommastate'){
						$a = explode(',', trim($replaceValue));
						if($replaceKeyword=='incitycommastate') $replace1 = (trim($a[0])!='in') ? $a[0] : '';
						else $replace1 = trim($a[0]) ? $a[0] : '';
						$replace2 = trim($a[1]) ? $a[1] : '';
						if($replace1 && $replace2) $replaceValue = $replace1.', '.$replace2;
						else if($replace1) $replaceValue = $replace1;
						else if($replace2) $replaceValue = $replace2;
						else $replaceValue = '';
						$row[$searchField] = str_replace('(('.$replaceKeyword.'))', $replaceValue, $row[$searchField]);
					} else {
						$row[$searchField] = str_replace('(('.$replaceKeyword.'))', $replaceValue, $row[$searchField]);
					}
					unset($a, $replace1, $replace2, $replaceValue);
				}
				
				self::ReplaceNavigationTags($row, $searchField, $this);
			} //end if(preg_match('/\(\((.*)\)\)/i', $row[$searchField]))
		}
		
		return $row;
	}
	
	//helper function for replaceTags() -- when someone uses the ((navname-children)) custom tag
	//return UL code to be included
	static function buildNavPagesList($hub, $navName, $hubID, $specParent = '', $currPageID = ''){
//		$hub = $this;
		$navName = $hub->sanitizeInput($navName);
		$output = '';
		//if a parent nav section is specified, get its ID
		if($specParent){
			$parentInfo = $hub->queryFetch("SELECT id FROM hub_page2 WHERE page_title_url = '".$specParent."' AND hub_id = '".$hubID."' AND type='nav' AND trashed = '0000-00-00'");
		}
		//get the nav section's ID
		$query = "SELECT id, page_title_url FROM hub_page2 WHERE type = 'nav' AND page_title_url = '".$navName."' AND hub_id = '".$hubID."' AND trashed = '0000-00-00'";
		if($specParent && $parentInfo['id']) $query .= " AND nav_parent_id = '".$parentInfo['id']."' ";
		if($navInfo = $hub->queryFetch($query, NULL, 1)){
			//get all the pages under it
			if($navPages = $hub->query("SELECT id, type, nav_parent_id, nav_root_id, page_title, page_title_url FROM hub_page2 WHERE nav_parent_id =
'".$navInfo['id']."' AND inc_contact = '1' AND trashed = '0000-00-00' ORDER BY nav_order ASC")){
				$output = '<ul>';
				while($thisNav = $hub->fetchArray($navPages, 1)){
					if(($thisNav['nav_root_id']!=$thisNav['nav_parent_id']) && $thisNav['nav_root_id']!=0){
						//if this is a sub-nav, get the root nav's info
						$navInfo2 = $hub->queryFetch("SELECT page_title_url FROM hub_page2 WHERE id = '".$thisNav['nav_root_id']."'", NULL, 1);
						$rootNavNameURL = '/'.$navInfo2['page_title_url'];
					}
					else $rootNavNameURL = '';
					
					if($thisNav['type']=='nav'){
						//uhg
					}
					else {
						if($currPageID==$thisNav['id']) $output .= '<li class="current">';
						else $output .= '<li>';
						if($currPageID==$thisNav['id']) $output .= '<a class="current" href="'.$rootNavNameURL.'/'.$navInfo['page_title_url'].'/'.$thisNav['page_title_url'].'/">'.$thisNav['page_title'].'</a></li>';
						else $output .= '<a href="'.$rootNavNameURL.'/'.$navInfo['page_title_url'].'/'.$thisNav['page_title_url'].'/">'.$thisNav['page_title'].'</a></li>';
					}
				}
				$output .= '</ul>';
			}
			
			return $output;
		}
		else return false;
	}

	/**********************************************************************/
	// function: Return Latest mysqli_NUM_ROWS
	// returns the last mysqli_num_rows result for latest Hub Query 
	//
	// Accepted Input: none
	/**********************************************************************/
	function getHubRows(){ return $this->hub_rows; }
	function getPageRows(){ return $this->page_rows; }
	
}
?>
