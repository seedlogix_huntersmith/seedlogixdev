<?php
	session_start();
         require_once( dirname(__FILE__) . '/../6qube/core.php');
	
	require_once(QUBEADMIN . 'inc/auth.php');
	require_once(QUBEADMIN . 'inc/blogs.class.php');
	
	$camp = new Blog(); //instantiate blog class for blog section in right panel
	$camp_limit = $_SESSION['user_limits']['campaign_limit'];//$camp->getLimits($_SESSION['user_id'], "campaign");
	//limit and offset
	$limit = 10;
	$offset = is_numeric($_GET['offset']) ? $_GET['offset'] : 0;
	$results = $camp->getCampaigns($_SESSION['user_id'], NULL, $limit, $offset);
	$totalNumCamps = $camp->getCampaigns($_SESSION['user_id'], NULL, NULL, NULL, 1);
	$page = 'campaigns.php';
	$continue = true;
	$html = '';
	
	if($results && $totalNumCamps){
		if($camp_limit==-1) $remaining = "Unlimited";
		else $remaining = $camp_limit - $totalNumCamps;
		
		while($row = $camp->fetchArray($results)){
			$html .= $camp->campaignDashUpdate2($row);
		}
	}
	else if($camp_limit==-1) $remaining = "Unlimited";
	
	if($camp_limit==0 || (($camp_limit-$totalNumCamps)<=0 && $camp_limit!=-1)) $readonly = "readonly";
	
	//check to see if user is a reseller client that hasn't had billing set up yet
	if($_SESSION['reseller_client']){
		if($_SESSION['payments']=='authorize' || $_SESSION['payments']=='6qube'){
			if($_SESSION['payments']=='authorize'){
				//if reseller is using their own merchant account make sure the API info is there
				$query = "SELECT payments_acct, payments_acct2 FROM resellers WHERE admin_user = '".$_SESSION['parent_id']."'";
				$a = $camp->queryFetch($query);
				if($a['payments_acct'] && $a['payments_acct2']) $isSetUp = true;
			}
			if($_SESSION['payments']=='6qube' || $isSetUp){
				$query = "SELECT billing_id2 FROM users WHERE id = '".$_SESSION['user_id']."'";
				$b = $camp->queryFetch($query);
				if(!$b['billing_id2']){
					//make sure they're not a free user
					if(!$camp->isFreeAccount($_SESSION['user_class'])){
						//override classes
						$overrideClass = array(144);
						$overrideUsers = array(3359);
						$overrideUsers = array(3760);
						$overrideUsers = array(4486);
						if(!in_array($_SESSION['user_class'], $overrideClass) && !in_array($_SESSION['user_id'], $overrideUsers)){
							$continue = false;
							include(QUBEADMIN . 'account_unpaid.php');
						}
					}
				}
			}
		}
		else if($_SESSION['payments']=='paypal'){
			//////////////////////////
			// PAYPAL GOES HERE
			//////////////////////////
		}
	} //end if($_SESSION['reseller_client'])
	else if($_SESSION['user_class']==421){
		//If user is the new 30-day trial class, check how much time they have left
		$dispDaysRemaining = 1;
		$daysRemaining = '0';
		$query = "SELECT created FROM users WHERE id = '".$_SESSION['user_id']."'";
		$userInfo = $camp->queryFetch($query);
		$timeSinceCreation = time()-strtotime($userInfo['created']);
		if($timeSinceCreation<=2592000){ //make sure it has been less than 30 days
			$daysRemaining = 30-(ceil($timeSinceCreation/86400)); //divide by number of days and round up
		}
	}
if($continue){
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<? } ?>
<script type="text/javascript">
$('.graph').die();
$('.graph').live('click', function(){
	var elm = $(this);
	$.get('inc/analytics_graph.php?getSparkline=1&filename='+$(this).attr('src'), function(data){
		elm.attr('src', data);
	});
});



$('#searchBox').focus();
$('#searchBox').die();
$('#searchBox').live('keyup', function(){
	var searchVal = $(this).attr('value');
	$('#wait').show();
	$.get('search.php?action=searchCampaigns&limit=10&user_id=<?=$_SESSION['user_id']?>&search='+searchVal, function(data){
		$('#wait').hide();
		$('#campaigns').html(data);
	});
});
<? if(!$_SESSION['theme']){ ?>
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition': 'inside',
		'transitionIn': 'none',
		'transitionOut': 'none'
	});
	
//	createDashBoard();
	
});
<? } else if($_SESSION['thm_camp-pnl']){ ?>
$(function(){ $('.custom-block a').addClass('dflt-link'); });
<? } ?>
</script>
<div id="camps">
<? if($v3){ ?>
<!-- ##### Main Dashboard Chart ##### -->
<div class="widget chartWrapper">
	<div class="whead">
		<h6>Total visits and leads for all campaigns</h6>
		<div class="right">
<span>Show the last</span>
<select name="select2">
	<option value="opt2">2</option>
	<option value="opt3">3</option>
	<option value="opt4">4</option>
	<option value="opt5">5</option>
	<option value="opt6">6</option>
	<option value="opt7">7</option>
	<option value="opt8">8</option>
	<option value="opt8">9</option>
	<option value="opt8">10</option>
</select>

<select name="select3">
	<option value="opt2">Days</option>
	<option value="opt3">Weeks</option>
	<option value="opt4">Months</option>
	<option value="opt5">Years</option>
</select>
</div>
		<div class="clear"></div>
	</div>
	<div class="body"><div class="chart"></div></div>
</div>
<? } ?>
	<? //check to see if user has hit their email sending limit
	$emailsRemaining = $camp->emailsRemaining($_SESSION['user_id']);
	if($emailsRemaining==0 && ($_SESSION['user_limits']['email_limit']!=0)){
	?>
	<div style="width:100%;background-color:#F99;color:#000;font-weight:bold;padding:10px;">
		You've reached your monthly limit of <?=number_format($_SESSION['user_limits']['email_limit'])?> outgoing emails.
	</div>
	<? } ?>
	<? if($dispDaysRemaining){ ?>
	<div style="width:100%;background-color:#CCC;color:#000;font-weight:bold;padding:10px;">
		You have <?=$daysRemaining?> days remaining of your free 30-day trial
	</div>
    <br />
	<? } ?>
	<? if($_SESSION['theme']) echo '<h2>'.$_SESSION['thm_welcome-msg'].'</h2>'; ?>
    <div class="form-icon-wrapper">
    <div class="icon"><img src="img/v3/campaign-icon.png" width="64" /></div>
	<h1>
		<? if($_SESSION['reports_user']){ ?>Select Campaign<? } else { ?><? if(!$camp_count) echo 'Step 1 - '; ?>Create a campaign<? } ?>
	</h1>
    </div>
	<div class="clear"></div>
    <? if(!$_SESSION['reports_user']){ ?>
	<form name="create_campaign" class="ajax" method="post" action="add_campaign.php">
		<input type="text" name="campaign_name" onmousemove="focus()" <?=$readonly?> />
		<input type="hidden" name="camp_limit" value="<?=$camp_limit?>" />
		<input type="submit" name="submit" value="Create Campaign" />
	</form>
	
    <? if(!$_SESSION['theme']){ ?>
	<div class="watch-video"><ul><li><a href="#campVideo" class="trainingvideo">Watch Video</a></li></ul></div>
	<? } ?>
    
	<? if(!$_SESSION['theme']){ ?>
	<div style="display:none;">
        <div id="campVideo" style="width:1000px;height:590px;overflow:auto;">
            <!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
            <a href="http://6qube.mobi/videos/training/create-campaign.flv" style="display:block;width:1000px;height:590px" id="demoplayer"> </a>
            <!-- this will install flowplayer inside previous A- tag. -->
            <script>
                flowplayer("demoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
            </script>
        </div>
    </div>
	<? } ?>
	<br />
	<div class="clear"></div>
	<h2>or Select Campaign</h2><? if($totalNumCamps > 20){ ?><div class="searchboxes"><label>Start typing to search:</label> <input class="search" type="text" id="searchBox" value="" /></div><br /><br /><? } ?>
    
    <? } ?><!-- ends Reports User Limitations -->
    
	<div id="campaigns">
		<? //get campaigns
		if($html){
			if($v3){
			echo '<!-- ##### CAMPAIGNS TABLE ##### -->           
<div class="widget">
            <div class="whead">
            	<h6>Campaigns</h6>
            	<div class="buttonS bLightBlue right dynRight" id="exportCampaign">Export</div>
            	<div class="buttonS bGreen right dynRight" id="createCampaign">Create Campagin</div>
            	<div class="clear"></div>
            </div>
            <div id="dyn" class="hiddenpars">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tDefault" id="dynamic">
                <thead>
                <tr>
                	<td>Campaign Name <span class="sorting" style="display: block;"></span></td>
                	<td>Total Visits</td>
                	<td>Total Leads</td>
                	<td>Conversion Rate</td>
                	<td>Touchpoints</td>
                	<td>Campaign Creation Date</td>
                	<td>Action</td>
                </tr>
                </thead>
                <tbody>';
			echo $html;
			echo '</tbody>
                </table> 
            </div>
			<div class="clear"></div> 
		</div>
<br class="clear"><br /><br />';
			} else {
			echo '<div id="dash"><div id="dashboard_campaign" class="camp-container"><div class="container">';
			echo $html;
			echo '</div></div>';
			}
			if($totalNumCamps){
				if($limit && ($totalNumCamps > $limit)){
				//pagination
					$numPages = ceil($totalNumCamps/$limit);
					if($offset>0) $currPage = ($offset/$limit)+1;
					else $currPage = 1;
					echo '<p style="font-weight:bold;">';
					if($currPage > 1){
						echo '<a title="Start of Campaigns" href="'.$page.'?offset=0';
						echo '"><img src="img/v3/nav-start-icon.jpg" border="0" /></a>&nbsp;';
						echo '<a title="Previous Set of Campaigns" href="'.$page.'?offset='.($offset-$limit);
						echo '"><img src="img/v3/nav-previous-icon.jpg" border="0" /></a> ';
					}
					if($currPage < $numPages){
						echo ' <a title="Next Set of Campaigns" href="'.$page.'?offset='.($offset+$limit);
						echo '"><img src="img/v3/nav-next-icon.jpg" border="0" /></a>';
						echo ' <a title="End of Campaigns" href="'.$page.'?offset='.(($numPages*$limit)-$limit);
						echo '"><img src="img/v3/nav-end-icon.jpg" border="0" /></a>';
					}
					echo '</p><br />';
				}
			}
			if(!$_SESSION['reports_user']){
				if($remaining<=0 && $remaining!="Unlimited") 
					echo "You have <strong>0</strong> campaigns remaining.  Upgrade your account to access this feature.";
				else echo "You have <strong>".$remaining."</strong> campaigns remaining.";
				echo '</div>';
			}
			
		} else { ?>
		<div id="dash"><div id="dashboard_campaign" class="camp-container"><div class="container">
		<p>No campaigns created yet. Create a campaign to get started!  You have <strong><?=$remaining?></strong> remaining.</p>
		</div></div></div>
		<? } ?>
	</div><!-- /campaigns -->
	<br />
	    <div class="clear"></div>
        <div class="bottomContent">
	
   
    </div><!-- /bottomContent -->
    <div class="clear"></div>
</div><!-- /camps -->
<? } //end if($continue) ?>

<br style="clear:both;" /><br />
<?
	/*
		resellerID
		period { day, week, month, year, range }
		date {standard format = YYYY-MM-DD. magic keywords = today or yesterday.}
		format {xml, json, html, php}
	*/
	$data_array	= $camp->ANAL_getVisitsForCampaign($resellerParentID, 'lastWeek');		
?>
<? if($_SESSION['user_id']==7){ ?>
<script type="text/javascript">

	//===== Create Dashboard Graph =====//
	createDashBoard(<?=json_encode($data_array[0]);?>, <?=json_encode($data_array[1]);?>);
	
	//===== Dynamic data table =====//
	oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});
	
	//===== Dynamic table toolbars =====//		
	$('#dyn .tOptions').click(function () {
		$('#dyn .tablePars').slideToggle(200);
	});	
	
	$('#dyn2 .tOptions').click(function () {
		$('#dyn2 .tablePars').slideToggle(200);
	});	
	
	$('.tOptions').click(function () {
		$(this).toggleClass("act");
	});

		

</script>
<? } ?>