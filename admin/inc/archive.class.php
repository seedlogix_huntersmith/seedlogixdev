<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Archive extends Local {
	 
	 /**********************************************************************/
	 // function: Display Directory Archive
	 // returns a list, with pagination, of all months that have Directory 
	 // listings (that have required fields filled out)
	 //
	 // Accepted Input: Query Limit Query Offset
	 /**********************************************************************/
	 
	 function displayDirArchive($limit = '', $offset = ''){
		
		$query = "
	 		SELECT count(*) FROM `directory` 
			WHERE `company_name` != ''
			AND `phone` != ''
			AND`street` != ''
			AND `city` != ''
			AND `state` != ''
			AND `category` != ''
			AND `website_url` != ''
			AND `display_info` != ''
			AND `keyword_one` != ''
			AND directory.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'directory')
	 	";
		$result = $this->query($query);
		$count = $this->fetchArray($result);
		$this->dir_rows = $count['count(*)'];
	 	//$this->dir_rows = $this->numRows($result); //set $dir_rows

		$settings = $this->getSettings();

		if($this->dir_rows){
			
			$lastMo = '';
			$n = 0;
			
			for($i = 1; $i <= $limit; $i++){
				
				$query = "
					SELECT * FROM `directory` 
					WHERE `company_name` != ''
					AND `phone` != ''
					AND`street` != ''
					AND `city` != ''
					AND `state` != ''
					AND `category` != ''
					AND `website_url` != ''
					AND `display_info` != ''
					AND `keyword_one` != ''
					AND directory.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'directory')
				";
				$query .= " ORDER BY created DESC";
				if($limit) $query .= " LIMIT $limit";
				if($offset) $query .= " OFFSET ".($offset+($limit*n));
				$result = $this->query($query);
				
				while($row = $this->fetchArray($result)){ //if so loop through and display links to each
					$thisMo = date("F Y", strtotime($row['created']));
					
					if($n <= $limit){
						if($thisMo != $lastMo){
							$html .= $this->displayArchive($row);
							$n++;
						}
					}
					
					$lastMo = $thisMo;
				}
			}
			
			echo "<div class=\"blog_right_links\"><ul>".$html."</ul></div>";
			$this->displayNav2($this->dir_rows, $limit, $offset, 'archive');
		}
		else //if no directories
			echo 'You have 0 Directories.';
	 }

}
?>