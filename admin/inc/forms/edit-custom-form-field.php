<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize
require_once(QUBEADMIN . 'inc/auth.php');
//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
if($id){
	$form = $hub->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], $id, $multiUser);
	$fields = $hub->parseCustomFormFields($form['data']);
	$type = $fields[$fieldID]['type'];
	if($type=='select'){
		$numOptions = count($fields[$fieldID]['options']);
	}
	$h = ' style="display:none;"';
	$multiUser = $_GET['multi_user'] ? 1 : 0;
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
	$('#newFormFieldType').live('change', function(){
		var type = $(this).attr('value');
		$('.fieldOptions').each(function(){
			if($(this).hasClass('current')) $(this).removeClass('current');
			if($(this).attr('id')!=type+'_options') $(this).css('display', 'none');
			else {
				$(this).slideDown('fast');
				$(this).addClass('current');
			}
		});
	});
	$('#addSelectOption').die();
	$('#addSelectOption').live('click', function(){
		var currOptNum = parseInt($('#newSelectOptions').attr('rel'));
		var newOptNum = currOptNum+1;
		var currHTML = $('#newSelectOptions').html();
		var html = '<div id="newSelectOption'+newOptNum+'"><div style="display:inline-block;"><label>Value</label><input type="text" name="option_'+newOptNum+'_value" value="" class="no-upload" style="width:250px;" /></div>&nbsp;<div style="display:inline-block;"><label>Display Text</label><input type="text" name="option_'+newOptNum+'_text" value="" class="no-upload" style="width:450px;" /></div>&nbsp;&nbsp;<a href="#" class="delNewSelectOption">[x]</a></div>';
		
		$('#newSelectOptions').html(currHTML+html).attr('rel', newOptNum);
		
		return false;
	});
	$('.delNewSelectOption').live('click', function(){
		$(this).parent().slideUp().html('');
		
		return false;
	});
	$('.dateDefault').live('change', function(){
		if($(this).attr('value')=='custom'){
			$('#customDefaultDate').slideDown();
		}
		else $('#customDefaultDate').css('display', 'none');
	});
	$('.nameFieldLabel').live('change', function(){
		var label = $(this).attr('name');
		$('#name_'+label).html($(this).attr('value'));
	});
	$('#editFormField').die();
	$('#editFormField').live('click', function(){
		$('.fieldOptions').each(function(){
			if(!$(this).hasClass('current')) $(this).html('');
		});
		
		var params = $('#editFormFieldForm').serialize();
		//alert(params);
		$.post('update-custom-form.php', params, function(data){
			var json = $.parseJSON(data);
			if(json.success == 1){
				$('#wait').show();
				$.get(json.refreshUrl, function(data){
					$("#ajax-load").html(data);
					$('#wait').hide();
				});
			}
			else {
				alert(json.message);
			}
		});
		
		return false;
	});
});
</script>
<h1>Custom Form:</h1>
<h2 style="color:#000;margin-top:-10px;"><?=$form['name']?></h2>
<h1>Editing Field:</h1>
<h2 style="color:#000;margin-top:-10px;"><?=$fields[$fieldID]['label']?></h2>
<a href="hub-forms.php?mode=edit&id=<?=$id?><? if($multiUser) echo '&multi_user=1'; ?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Forms" /></a>
<br /><br />
<strong>Field type:</strong>
<select class="uniformselect no-upload" id="newFormFieldType">
	<optgroup label="Basic">
		<option value="text" <? if($type=='text') echo 'selected'; ?>>Text Input</option>
		<option value="textarea" <? if($type=='textarea') echo 'selected'; ?>>Text Area</option>
		<option value="select" <? if($type=='select') echo 'selected'; ?>>Drop-down</option>
		<option value="checkbox" <? if($type=='checkbox') echo 'selected'; ?>>Checkbox</option>
		<option value="hidden" <? if($type=='hidden') echo 'selected'; ?>>Hidden</option>
	</optgroup>
	<optgroup label="Presets">
		<option value="email" <? if($type=='email') echo 'selected'; ?>>Email Address</option>
		<option value="money" <? if($type=='money') echo 'selected'; ?>>Money Amount</option>
		<option value="date" <? if($type=='date') echo 'selected'; ?>>Date</option>
		<option value="name" <? if($type=='name') echo 'selected'; ?>>Name</option>
		<option value="states">US States</option>
	</optgroup>
	<optgroup label="Non-Input">
		<option value="section" <? if($type=='section') echo 'selected'; ?>>Section Header</option>
	</optgroup>
</select>
<div id="formFieldOptions">
	<form class="jquery_form" id="editFormFieldForm">
		<strong>Field Options:</strong><br />
		<!-- TEXT INPUT -->
		<div id="text_options" class="<? if($type=='text') echo 'current '; ?>fieldOptions"<? if($type!='text') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="<?=$fields[$fieldID]['default']?>" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> /><br /><br />
				<label style="display:inline;float:left;padding:0;padding-right:10px;">Numbers only?</label>&nbsp;<input type="checkbox" name="numsOnly" style="display:inline;" <? if($fields[$fieldID]['numsOnly']) echo 'checked'; ?> />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" <? if($fields[$fieldID]['size']=='large') echo 'selected'; ?>>Large</option>
					<option value="medium" <? if($fields[$fieldID]['size']=='medium') echo 'selected'; ?>>Medium</option>
					<option value="small" <? if($fields[$fieldID]['size']=='small') echo 'selected'; ?>>Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="text" />
		</ul></div>
		<!-- END TEXT INPUT -->
		<!-- TEXT AREA -->
		<div id="textarea_options" class="<? if($type=='textarea') echo 'current '; ?>fieldOptions"<? if($type!='textarea') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="<?=$fields[$fieldID]['default']?>" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" <? if($fields[$fieldID]['size']=='large') echo 'selected'; ?>>Large</option>
					<option value="medium" <? if($fields[$fieldID]['size']=='medium') echo 'selected'; ?>>Medium</option>
					<option value="small" <? if($fields[$fieldID]['size']=='small') echo 'selected'; ?>>Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="textarea" />
		</ul></div>
		<!-- END TEXT AREA -->
		<!-- DROPDOWN -->
		<div id="select_options" class="<? if($type=='select') echo 'current '; ?>fieldOptions"<? if($type!='select') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> />
			</li>
			<li>
				<label><strong>Options</strong></label>
				<div id="newSelectOptions" rel="<?=$numOptions?>" style="margin-top:-10px;">
					<?
					if($fields[$fieldID]['options']){
						foreach($fields[$fieldID]['options'] as $key=>$value){
					?>
					<div id="newSelectOption<?=$key?>">
						<div style="display:inline-block;">
							<label>Value</label>
							<input type="text" name="option_<?=$key?>_value" value="<?=$value['key']?>" class="no-upload" style="width:250px;" />
						</div>
						<div style="display:inline-block;">
							<label>Display Text</label>
							<input type="text" name="option_<?=$key?>_text" value="<?=$value['value']?>" class="no-upload" style="width:450px;" />
						</div>&nbsp;
						<a href="#" class="delNewSelectOption">[x]</a>
					</div>
					<?
						} //end foreach
					} //end if($fields[$fieldID]['options'])
					?>
				</div>
				<a href="#" class="dflt-link" id="addSelectOption">[+] Add Another</a><br /><br />
			</li>
			<input type="hidden" name="fieldType" value="select" />
		</ul></div>
		<!-- END DROPDOWN -->
		<!-- CHECKBOX -->
		<div id="checkbox_options" class="<? if($type=='checkbox') echo 'current '; ?>fieldOptions"<? if($type!='checkbox') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:83px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> /><br /><br />
				<label style="display:inline;float:left;padding:0;padding-right:10px;">Checked By Default?</label>&nbsp;<input type="checkbox" name="checked" style="display:inline;" <? if($fields[$fieldID]['checked']) echo 'checked'; ?> />
			</li>
			<input type="hidden" name="fieldType" value="checkbox" />
		</ul></div>
		<!-- END CHECKBOX -->
		<!-- HIDDEN -->
		<div id="hidden_options" class="<? if($type=='hidden') echo 'current '; ?>fieldOptions"<? if($type!='hidden') echo $h; ?>><ul>
			<li><label>Name</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Value</label><input type="text" name="default" value="<?=$fields[$fieldID]['default']?>" class="no-upload" /></li>
		</ul></div>
		<!-- END HIDDEN -->
		<!-- EMAIL ADDRESS -->
		<div id="email_options" class="<? if($type=='email') echo 'current '; ?>fieldOptions"<? if($type!='email') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="<?=$fields[$fieldID]['default']?>" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" <? if($fields[$fieldID]['size']=='large') echo 'selected'; ?>>Large</option>
					<option value="medium" <? if($fields[$fieldID]['size']=='medium') echo 'selected'; ?>>Medium</option>
					<option value="small" <? if($fields[$fieldID]['size']=='small') echo 'selected'; ?>>Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="email" />
		</ul></div>
		<!-- END EMAIL ADDRESS -->
		<!-- MONEY AMOUNT -->
		<div id="money_options" class="<? if($type=='money') echo 'current '; ?>fieldOptions"<? if($type!='money') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li>
			<li><label>Default Value</label>$<input type="text" name="default" value="<?=$fields[$fieldID]['default']?>" class="no-upload" style="width:75px;" /></li>
			<br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> />
			</li>
			<input type="hidden" name="fieldType" value="money" />
		</ul></div>
		<!-- END MONEY AMOUNT -->
		<!-- DATE -->
		<div id="date_options" class="<? if($type=='date') echo 'current '; ?>fieldOptions"<? if($type!='date') echo $h; ?>><ul>
			<li><label>Label</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="<?=$fields[$fieldID]['label_note']?>" class="no-upload" /></li>
			<li>
				<label>Default Value</label>
				<select name="default" class="uniformselect no-upload dateDefault">
					<option value="" <? if(!$fields[$fieldID]['default']) echo 'selected'; ?>>Blank</option>
					<option value="today" <? if($fields[$fieldID]['default']=='today') echo 'selected'; ?>>Current Date</option>
					<option value="custom" <? if($fields[$fieldID]['default']=='custom') echo 'selected'; ?>>Custom</option>
				</select>
			</li>
			<li id="customDefaultDate" style="margin-top:-10px;<? if($fields[$fieldID]['default']!='custom') echo 'display:none;'; ?>">
				<label>Custom Default Value</label>
				<div style="display:inline-block;">
					<input type="text" name="default_M" value="<?=$fields[$fieldID]['defaultM']?>" class="no-upload" style="width:50px;" maxlength="2" />
				</div>
				/
				<div style="display:inline-block;">
					<input type="text" name="default_D" value="<?=$fields[$fieldID]['defaultD']?>" class="no-upload" style="width:50px;" maxlength="2" />
				</div>
				/
				<div style="display:inline-block;">
					<input type="text" name="default_Y" value="<?=$fields[$fieldID]['defaultY']?>" class="no-upload" style="width:75px;" maxlength="4" />
				</div>
				<br /><br />
			</li>
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" <? if($fields[$fieldID]['required']) echo 'checked'; ?> />
			</li>
			<input type="hidden" name="fieldType" value="date" />
		</ul></div>
		<!-- END DATE -->
		<!-- NAME -->
		<div id="name_options" class="<? if($type=='name') echo 'current '; ?>fieldOptions"<? if($type!='name') echo $h; ?>><ul>
			<li><label>Label 1</label><input type="text" name="label1" value="<?=$fields[$fieldID]['label1']?>" class="no-upload nameFieldLabel" /></li>
			<li><label>Label 2</label><input type="text" name="label2" value="<?=$fields[$fieldID]['label2']?>" class="no-upload nameFieldLabel" /></li>
			<li>
				<label><strong>Default Value</strong></label>
				<div style="display:inline-block;margin-top:-15px;">
					<label id="name_label1">First Name</label>
					<input type="text" name="default1" value="<?=$fields[$fieldID]['default1']?>" class="no-upload" style="width:250px;" />
				</div>
				&nbsp;
				<div style="display:inline-block;margin-top:-15px;">
					<label id="name_label2">Last Name</label>
					<input type="text" name="default2" value="<?=$fields[$fieldID]['default2']?>" class="no-upload" style="width:250px;" />
				</div>
			</li>
			<li>
				<label>Required?</label>
				<select name="required" class="uniformselect no-upload">
					<option value="no" <? if(!$fields[$fieldID]['required1'] && !$fields[$fieldID]['required2']) echo 'selected'; ?>>Neither</option>
					<option value="field1" <? if($fields[$fieldID]['required1'] && !$fields[$fieldID]['required2']) echo 'selected'; ?>>Field 1</option>
					<option value="field2"<? if(!$fields[$fieldID]['required1'] && $fields[$fieldID]['required2']) echo 'selected'; ?>>Field 2</option>
					<option value="both"<? if($fields[$fieldID]['required1'] && $fields[$fieldID]['required2']) echo 'selected'; ?>>Both</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="name" />
		</ul></div>
		<!-- END NAME -->
		<!-- US STATES -->
		<div id="states_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<input type="hidden" name="fieldType" value="states" />
		</ul></div>
		<!-- END US STATES -->
		<!-- SECTION -->
		<div id="section_options" class="fieldOptions <? if($type=='section') echo 'current '; ?>"<? if($type!='section') echo $h; ?>><ul>
			<li><label>Text</label><input type="text" name="label" value="<?=$fields[$fieldID]['label']?>" class="no-upload" /></li>
			<input type="hidden" name="fieldType" value="section" />
		</ul></div>
		<!-- END SECTION -->
	<strong>Autoresponder Embed Tag:</strong><br />
	Use this tag to embed this field's submitted value in your autoresponders<br />
	<input type="text" value="[[<?= htmlentities($fields[$fieldID]['label'], ENT_QUOTES); ?>]]" readonly />
	<br /><br />
		
		<br />
		<input type="hidden" name="formID" value="<?=$id?>" />
		<input type="hidden" name="fieldID" value="<?=$fieldID?>" />
		<input type="hidden" name="userID" value="<?=$_SESSION['user_id']?>" />
		<input type="hidden" name="multi_user" value="<?=$multiUser?>" />
		<input type="hidden" name="action" value="editFormField" />
        <?php if($_SESSION['thm_buttons-clr']) {?><a href="#" id="editFormField"><img src="img/submit-button/<?=$_SESSION['thm_buttons-clr']?>.png" /></a><? } else { ?><a href="#" id="editFormField" class="greyButton" >Save Setting</a><?php } ?>
	</form>
</div>
<? } ?>