<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

if(!$id){
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$id = $_GET['id'];
}
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
$row = $local->queryFetch($query);

if($row['setup']!=1){
	include(QUBEADMIN . 'reseller-network-setup.php');
}
else {	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "global";
include('edit_whole_network_nav.php');
?>
<div id="form_test">
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Global Additional Settings</h2>
	Editing these settings will copy them to all sites in the network.<br />
	For instance, if you add your phone number this would be displayed on each site as well. 
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Free Account Link</label>
				<input class="update-whole-network" type="text" name="free_link" value="<?=$row['free_link']?>" />
			</li>
			<li>
				<label>Phone</label>
				<input class="update-whole-network" type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Twitter URL</label>
				<input class="update-whole-network" type="text" name="twitter" value="<?=$row['twitter']?>" />
			</li>
			<li>
				<label>Facebook URL</label>
				<input class="update-whole-network" type="text" name="facebook" value="<?=$row['facebook']?>" />
			</li>
			<li>
				<label>YouTube URL</label>
				<input class="update-whole-network" type="text" name="youtube" value="<?=$row['youtube']?>" />
			</li>
			<li>
				<label>LinkedIn URL</label>
				<input class="update-whole-network" type="text" name="linkedin" value="<?=$row['linkedin']?>" />
			</li>
			<li>
				<label>Blog URL</label>
				<input class="update-whole-network" type="text" name="blog" value="<?=$row['blog']?>" />
			</li>
			
		</ul>
	</form><br />
	

	
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_global_nav.php');
?>

	
						
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<?
	} else echo "Unable to display this page."; 
}
?>