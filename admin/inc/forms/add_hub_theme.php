<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, id_duped, user_id, user_id_created, hub_parent_id, name, company_name, city, connect_blog_id, video_script, add_autoresponder, display_page, remove_contact, affiliate_info, signup_name, signup_link, button_1, button1_link, button_2, button2_link, button_3, button3_link, bg_repeat, slider_option, call_action_submit, domain, adv_css, alt_bg, bg_img, bg_color, gallery_page, form_type, links_color, footer_text_color, theme_color, extra_links_color, header_text_color, display_diy, display_pricing, legacy_gallery_link, full_home, display_network, header_links_color, facebook_app_id, facebook_app_secret, coupon_offer2, footer_links_color, form_box_color, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields  
			FROM hub WHERE id = '".$_SESSION['current_id']."' ";
	$row = $hub->queryFetch($query);
	$query = "SELECT css_file_path, type, user_class2_fields, pages, training_link, thumbnail, custom_css_support, theme_color_options, slider_options FROM themes WHERE id = '".$row['theme']."'";
	$themeInfo = $hub->queryFetch($query);

	//create theme color array
	$themecolors = explode('::', $themeInfo['theme_color_options']);
	//create slider options array
	$slideroptions = explode('::', $themeInfo['slider_options']);
	
	if($row['user_id']==$_SESSION['user_id']){
		require_once QUBEADMIN . 'inc/blogs.class.php';
		$blog = new Blog();
		if($reseller_site){
			$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub/';
			$themeType = 'hub';
			$blogCampaign = NULL;
		} else {
			$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub/';
			$themeType = 'hub';
			$blogCampaign = $_SESSION['campaign_id'];
		}
		//get field info from hub row for locks
		if($row['hub_parent_id'] || $row['multi_user']){
			if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
			else {
				//if hub is a V2 MU Hub copy get parent hub info
				$query = "SELECT theme, multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentHubInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentHubInfo['multi_user_fields'];
			}
		}
		
		if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
		else $dflt_noImg = 'img/no-photo.jpg';
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if(($row['req_theme_type']==4 && $_SESSION['reseller']) ||
			($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
			$addClass = ' multiUser-'.$muHubID.'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		   ($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
			$multi_user2 = true;
			//figure out which fields to lock
			if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
			if($fieldsInfo = json_decode($fieldsInfoData, true)){
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
						return ' readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$("a.single_image").fancybox();
	
	<?php if($row['facebook_app_id']) {?>
	$(".addpages").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'		: '75%',
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "theme";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - Theme <?=$_SESSION['globalMUpid']?></h2>
	
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Layout Theme</label>
                <? if($themeInfo['thumbnail']){ ?>
                <br />
                <a href="http://<?=$_SESSION['main_site']?>/hubs/themes/thumbnails/<?=$themeInfo['thumbnail']?>" class="single_image"><img src="http://<?=$_SESSION['main_site']?>/hubs/themes/thumbnails/<?=$themeInfo['thumbnail']?>" width="115" style="float:left; padding-right:15px;" border="0" /></a>
                <? } ?>
				<?
				if(!$row['id_duped']){
					if($row['hub_parent_id'] && ($parentHubInfo['theme']==$row['theme'])) $selected = 'hub'.$row['hub_parent_id'];
					else $selected = $row['theme'];
					$hub->displayThemes($_SESSION['user_id'], $selected, $themeType, $_SESSION['user_class'], 
										NULL, $row['req_theme_type'], $_SESSION['parent_id'], $multi_user, $_SESSION['globalMUpid']);
				}
				else {
					$query = "SELECT name FROM themes WHERE id = '".$row['theme']."'";
					$themeInfo = $hub->queryFetch($query);
					$query = "SELECT name FROM hub WHERE id = '".$row['id_duped']."' ";
					$dupedHubInfo = $hub->queryFetch($query);
					echo $themeInfo['name'].'<br />';
					echo '<small>(locked to the theme of your <b>'.$dupedHubInfo['name'].'</b> hub)</small>';
				}
				?>
				<? if(!$_SESSION['theme'] && $themeInfo['training_link']){ ?>
				<br />
				<a href="<?=$themeInfo['training_link']?>" class="external" target="_new">View <?=$themeInfo['name']?> Training Site</a>
				<? } ?>
			</li>
            
		<? if(!$multi_user){ ?>
			<? if(!checkLock('allow_blog')){ ?>
			<li <? if($themeInfo['thumbnail']){ ?> style="float:left; width:350px"<? } ?>>
				<label>Connect To A Blog</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Select one of your blogs to synchronize it with this hub.  This will change the blog's theme and settings to match this hub.<br /><br />If you make changes to your hub's settings you will have to reconnect it.<br /><br />A blog can only be connected to one hub at a time.</div>
				<?=$blog->userBlogsDropDown($_SESSION['user_id'], 'connect_blog_id', $blogCampaign, TRUE, $row['connect_blog_id']);?>
			</li><br />
			<? } ?>
		<? } else { ?>
			<li>
				<label>Allow Blog Syncing?</label>
				<select name="lock_allow_blog" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Yes</option>
					<option value="1" <?=checkLock('allow_blog',1)?>>No</option>
				</select>
			</li>
		<? } ?>
        	<?php if($row['theme'] == "24" || $row['theme'] == "104"){ ?>
			<li>
				<label>Header Layout</label>
				<? if($multi_user){ ?>
				<select name="lock_theme_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="theme_color" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="theme_color" title="select" class="uniformselect<?=checkLock('theme_color')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['theme_color'] == "1") echo 'selected="yes"'; ?>>Navigation Top</option>
					<option value="2" <? if($row['theme_color'] == "2") echo 'selected="yes"'; ?>>Logo & Header Banner</option>
                    <option value="0" <? if($row['theme_color'] == "0") echo 'selected="yes"'; ?>>Default Layout</option>
				</select>
			</li>
			<?php } ?>
            
            <?php if($row['theme'] == "115"){ ?>
			<li>
				<label>Theme Color</label>
				<select name="theme_color" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="dark" <? if($row['theme_color'] == "dark") echo 'selected="yes"'; ?>>Dark</option>
                    <option value="light" <? if($row['theme_color'] == "light") echo 'selected="yes"'; ?>>light</option>
				</select>
			</li>
            <li>
				<label>Header Bar Color</label>
				<? if($multi_user){ ?>
				<select name="lock_header_text_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('header_text_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('header_text_color')?> name="header_text_color" value="<?=$row['header_text_color']?>" class="<?=$addClass?>" />
			</li>
			<?php } ?>
            
             <?php if($row['theme'] == "11253"){ ?>
            <li>
				<label>Theme Bar Color</label>
				<? if($multi_user){ ?>
				<select name="lock_header_text_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('header_text_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('header_text_color')?> name="header_text_color" value="<?=$row['header_text_color']?>" class="<?=$addClass?>" />
			</li>
			<?php } ?>

			<?php if($themeInfo['theme_color_options']){
					echo '<li>
				<label>Theme Color</label>
				<select name="theme_color" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>';
					foreach($themecolors as $value){
						if($row['theme_color'] == $value) $selectcolor = 'selected="yes"';
						echo '
						<option value="'.$value.'" '.$selectcolor.'>'.$value.'</option>';
					}
					echo '</select>
			</li>';
					
				}
			?>
            
        	<?php if($row['theme'] == "36"){ ?>
			<li>
				<label>Theme Color</label>
				<select name="theme_color" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="grey" <? if($row['theme_color'] == "grey") echo 'selected="yes"'; ?>>Grey</option>
                    <option value="green" <? if($row['theme_color'] == "green") echo 'selected="yes"'; ?>>Green</option>
                    <option value="blue" <? if($row['theme_color'] == "blue") echo 'selected="yes"'; ?>>Blue</option>
                    <option value="bronze" <? if($row['theme_color'] == "bronze") echo 'selected="yes"'; ?>>Bronze</option>
                    <option value="darkred" <? if($row['theme_color'] == "darkred") echo 'selected="yes"'; ?>>Dark Red</option>
                    <option value="lightblue" <? if($row['theme_color'] == "lightblue") echo 'selected="yes"'; ?>>Light Blue</option>
                    <option value="orange" <? if($row['theme_color'] == "orange") echo 'selected="yes"'; ?>>Orange</option>
                    <option value="pink" <? if($row['theme_color'] == "pink") echo 'selected="yes"'; ?>>Pink</option>
                    <option value="0" <? if($row['theme_color'] == "0") echo 'selected="yes"'; ?>>Default</option>
				</select>
			</li>
			<?php } ?>
			<?php if($row['theme'] == "50"){ ?>
			<li>
				<label>Theme Color</label>
				<select name="theme_color" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="custom" <? if($row['theme_color'] == "custom") echo 'selected="yes"'; ?>>Custom</option>
                <option value="amethyst" <? if($row['theme_color'] == "amethyst") echo 'selected="yes"'; ?>>Amethyst</option>
				<option value="azure" <? if($row['theme_color'] == "azure") echo 'selected="yes"'; ?>>Azure</option>
				<option value="camouflage" <? if($row['theme_color'] == "camouflage") echo 'selected="yes"'; ?>>Camouflage</option>
				<option value="cerulean" <? if($row['theme_color'] == "cerulean") echo 'selected="yes"'; ?>>Cerulean</option>
				<option value="charcoal" <? if($row['theme_color'] == "carcoal") echo 'selected="yes"'; ?>>Charcoal</option>
				<option value="coffee" <? if($row['theme_color'] == "coffee") echo 'selected="yes"'; ?>>Coffee</option>
				<option value="contrast" <? if($row['theme_color'] == "contrast") echo 'selected="yes"'; ?>>Contrast</option>
				<option value="emerald" <? if($row['theme_color'] == "emerald") echo 'selected="yes"'; ?>>Emerald</option>
				<option value="indigo" <? if($row['theme_color'] == "indigo") echo 'selected="yes"'; ?>>Indigo</option>
				<option value="midnight" <? if($row['theme_color'] == "midnight") echo 'selected="yes"'; ?>>Midnight</option>
				</select>
			</li>
			<?php } ?>
            <?php if($row['theme'] == "122" || $row['theme'] == "126"){ ?>
			<li>
				<label>Theme Color</label>
				<select name="theme_color" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="black" <? if($row['theme_color'] == "black") echo 'selected="yes"'; ?>>Black</option>
                    <option value="green" <? if($row['theme_color'] == "green") echo 'selected="yes"'; ?>>Green</option>
                    <option value="blue" <? if($row['theme_color'] == "blue") echo 'selected="yes"'; ?>>Blue</option>
                    <option value="red" <? if($row['theme_color'] == "red") echo 'selected="yes"'; ?>>Red</option>
				</select>
			</li>
			<?php } ?>
 			<?php if($row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "87" || $row['theme'] == "102" || $row['theme'] == "107"){ ?>
             <li>
				<label>Button Colors</label>
				<? if($multi_user){ ?>
				<select name="lock_theme_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="theme_color" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="theme_color" title="select" class="uniformselect<?=checkLock('theme_color')?>">
				<? } ?>
                <option value="0">Please choose an option</option>
				<option value="blue" <? if($row['theme_color'] == "blue") echo 'selected="yes"'; ?>>Blue</option>
				<option value="deeppink" <? if($row['theme_color'] == "deeppink") echo 'selected="yes"'; ?>>Deep Pink</option>
                <option value="green" <? if($row['theme_color'] == "green") echo 'selected="yes"'; ?>>Green</option>
				<option value="orange" <? if($row['theme_color'] == "orange") echo 'selected="yes"'; ?>>Orange</option>
				</select>
			</li>
			<?php } ?>
		
		    <?php if($themeInfo['type'] == "5") {?>
			<li>
				<label>Facebook App ID</label>
				<? if($multi_user){ ?>
				<select name="lock_facebook_app_id" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('facebook_app_id',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;"><a class="external" href="http://www.facebook.com/developers/#!/developers/createapp.php" target="_new">Click Here to Setup Facebook App</a></div>
				<input type="text" <?=checkLock('facebook_app_id')?> name="facebook_app_id" value="<?=$row['facebook_app_id']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Facebook App Secret</label>
				<? if($multi_user){ ?>
				<select name="lock_facebook_app_secret" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('facebook_app_secret',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;"><a class="external" href="http://www.facebook.com/developers/#!/developers/createapp.php" target="_new">Click Here to Setup Facebook App</a></div>
				<input type="text" <?=checkLock('facebook_app_secret')?> name="facebook_app_secret" value="<?=$row['facebook_app_secret']?>" class="<?=$addClass?>" />
			</li>
            <?php if($row['facebook_app_id']) {?>
             <a href="https://www.facebook.com/dialog/pagetab?app_id=<?=$row['facebook_app_id']?>&display=popup&next=<?php if($row['domain']) {?><?=$row['domain']?><? } else { ?>http://<?=$_SESSION['main_site']?>/hubs/domains/4997/3245/<? } ?>"  class="greyButton" style="margin-top:15px;" class="external" target="_new" >Add To Page</a>
            <? } ?>
			
            <li>
				<label>Require Like Page</label>
				<? if($multi_user){ ?>
				<select name="lock_form_type" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="form_type" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="form_type" title="select" class="uniformselect<?=checkLock('form_type')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="2" <? if($row['form_type'] == "2") echo 'selected="yes"'; ?>>Yes</option>
					<option value="1" <? if($row['form_type'] == "1") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
            <? } ?>
            
            
		
			<li>
				<label>Background Color</label>
				<? if($multi_user){ ?>
				<select name="lock_bg_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('bg_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('bg_color')?> name="bg_color" value="<?=$row['bg_color']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Links Color</label>
				<? if($multi_user){ ?>
				<select name="lock_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('links_color')?> name="links_color" value="<?=$row['links_color']?>" class="<?=$addClass?>" />
			</li>
           
			
		</ul>
	</form>
	<form id="bg_img" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Background Image</label>
		<? if($multi_user){ ?>
		<select name="lock_bg_img" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('bg_img',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('bg_img')){ ?>
		<? if($row['bg_img']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=theme&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_bg_img').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="bg_img" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form><img id="bg_img_prvw" class="img_cnt" src="<? if($row['bg_img']) echo $img_url.$row['bg_img'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;"  />
    
    <?php if($row['theme'] == "11252" || $row['theme'] == "11253" || $row['theme'] == "11266"){ ?> 
    <form id="alt_bg" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Inside BG Image</label>
		<? if($multi_user){ ?>
		<select name="lock_alt_bg" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('alt_bg',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('alt_bg')){ ?>
		<? if($row['alt_bg']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=theme&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_alt_bg').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="alt_bg" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form><img id="alt_bg_prvw" class="img_cnt" src="<? if($row['alt_bg']) echo $img_url.$row['alt_bg'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;"  />
    <? } ?>
    
    
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
             
             <li>
				<label>Background Image Repeat</label>
				<? if($multi_user){ ?>
				<select name="lock_bg_repeat" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="bg_repeat" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="bg_repeat" title="select" class="uniformselect <?=checkLock('bg_repeat')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="repeat-x" <? if($row['bg_repeat']=='repeat-x') echo 'selected="yes"'; ?>>Horizonal</option>
                    <option value="repeat-y" <? if($row['bg_repeat']=='repeat-y') echo 'selected="yes"'; ?>>Vertical</option>
                    <option value="repeat" <? if($row['bg_repeat']=='repeat') echo 'selected="yes"'; ?>>Both</option>
                    <option value="no-repeat" <? if($row['bg_repeat']=='no-repeat') echo 'selected="yes"'; ?>>No Repeat</option>
				</select>
			</li>
            
            <?php if($row['theme'] == "24" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "104"){ ?>     
			<li>
				<label>Header Text Color</label>
				<? if($multi_user){ ?>
				<select name="lock_header_text_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('header_text_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('header_text_color')?> name="header_text_color" value="<?=$row['header_text_color']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] == "24" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98"  || $row['theme'] == "104"){ ?>  
			<li>
				<label>Header Links Color</label>
				<? if($multi_user){ ?>
				<select name="lock_header_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('header_links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('header_links_color')?> name="header_links_color" value="<?=$row['header_links_color']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] == "11253"){ ?> 
            <li>
				<label>Header Bar Color</label>
				<? if($multi_user){ ?>
				<select name="lock_header_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('header_links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('header_links_color')?> name="header_links_color" value="<?=$row['header_links_color']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] == "24" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "104" || $row['theme'] == "110" || $row['theme'] == "11250" || $row['theme'] == "11252"){ ?> 
			<li>
				<label>Footer Text Color</label>
				<? if($multi_user){ ?>
				<select name="lock_footer_text_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('footer_text_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('footer_text_color')?> name="footer_text_color" value="<?=$row['footer_text_color']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
             <?php if($row['theme'] == "24" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79"  || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "11250" || $row['theme'] == "11252"){ ?> 
			<li>
				<label>Footer Links Color</label>
				<? if($multi_user){ ?>
				<select name="lock_footer_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('footer_links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('footer_links_color')?> name="footer_links_color" value="<?=$row['footer_links_color']?>" class="<?=$addClass?>" />
			</li>
			<?php } ?>
            
            <?php if($row['theme'] == "11253"){ ?> 
            <li>
				<label>Footer Bar Color</label>
				<? if($multi_user){ ?>
				<select name="lock_footer_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('footer_links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('footer_links_color')?> name="footer_links_color" value="<?=$row['footer_links_color']?>" class="<?=$addClass?>" />
			</li>
            
            <?php } ?>
            
            
            <?php if($row['theme'] == "24" || $row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "107" || $row['theme'] == "110"){ ?>
			<li>
				<label><?php if($row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "102" || $row['theme'] == "107"){ ?>Slider Text Color<?php } ?><?php if($row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98"){ ?>Arrow Color<?php } ?><?php if($row['theme'] == "24" || $row['theme'] == "101" || $row['theme'] == "104" || $row['theme'] == "110"){ ?>Footer BG Color<?php } ?></label>
				<? if($multi_user){ ?>
				<select name="lock_extra_links_color" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('extra_links_color',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" <?=checkLock('extra_links_color')?> name="extra_links_color" value="<?=$row['extra_links_color']?>" class="<?=$addClass?>" />
			</li>
			<?php } ?>
            
            
            <?php if($row['theme'] == "35" || $row['theme'] == "11250" || $row['theme'] == "11252" || $row['theme'] == "11253"){ ?>
			<li>
				<label>Form Box Color</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" name="form_box_color" value="<?=$row['form_box_color']?>" />
			</li>
			<?php } ?>
            
            <?php if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "36" || $row['theme'] == "50" || $row['theme'] == "57" || $row['theme'] == "88" || $row['theme'] == "101" || $row['theme'] == "110" || $row['theme'] == "111"){ ?>
            <li>
				<label>Full Width Home Page?</label>
				<? if($multi_user){ ?>
				<select name="lock_full_home" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="full_home" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="full_home" title="select" class="uniformselect<?=checkLock('full_home')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="0" <? if($row['full_home'] == "0") echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['full_home'] == "1") echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
            <?php } ?>
            
            <?php if($row['theme'] == "37"){ ?>
            <li>
				<label>Two Column Home Page?</label>
				<? if($multi_user){ ?>
				<select name="lock_full_home" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="full_home" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="full_home" title="select" class="uniformselect<?=checkLock('full_home')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="0" <? if($row['full_home'] == "0") echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['full_home'] == "1") echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
            <?php } ?>
			
			<?php if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "84" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "107"){ ?>
            <li>
				<label>Remove Slider?</label>
				<? if($multi_user){ ?>
				<select name="lock_gallery_page" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="gallery_page" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="gallery_page" title="select" class="uniformselect<?=checkLock('gallery_page')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['gallery_page'] == "1") echo 'selected="yes"'; ?>>No</option>
					<option value="2" <? if($row['gallery_page'] == "2") echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
            <?php } ?>
            <?php if($row['theme'] == "50"){ ?>
			<li>
				<label>Raise Opt-in Box</label>
				<select name="gallery_page" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['gallery_page'] == "1") echo 'selected="yes"'; ?> >Yes</option>
					<option value="2" <? if($row['gallery_page'] == "2") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<?php } ?>
            <?php if($row['theme'] == "27"){ ?>  
			<li>
				<label>Display Form on Home Page</label>
				<select name="gallery_page" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['gallery_page'] == "1") echo 'selected="yes"'; ?> >Yes</option>
					<option value="2" <? if($row['gallery_page'] == "2") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<? } ?>
			<?php if($themeInfo['slider_options']){
					echo '<li>
				<label>Slider Options</label>
				<select name="slider_option" class="uniformselect" title="select">
				<option value="0">Please choose an option</option>';
					foreach($slideroptions as $value){
						$options = explode('|', $value);
						echo '
						<option value="'.$options[0].'" '.$sliderselect.'>'.$options[1].'</option>';
					}
					echo '</select>
			</li>';
					
				}
			?>
            <?php if($row['theme'] == "24" || $row['theme'] == "104"){ ?> 
            <li>
				<label>Slider Type</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Wide</option>
					<option value="0" <? if($row['slider_option'] == "0") echo 'selected="yes"'; ?>>Thin</option>
				</select>
			</li>
            <? } ?>
             <?php if($row['theme'] == "22"){ ?> 
            <li>
				<label>Slider Type</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Wide</option>
					<option value="0" <? if($row['slider_option'] == "0") echo 'selected="yes"'; ?>>Default</option>
				</select>
			</li>
            <? } ?>
             <?php if($row['theme'] == "5" || $row['theme'] == "8"){ ?> 
            <li>
				<label>Slider Type</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Short</option>
                    <option value="2" <? if($row['slider_option'] == "2") echo 'selected="yes"'; ?>>Short Without Mid Images</option>
					<option value="0" <? if($row['slider_option'] == "0") echo 'selected="yes"'; ?>>Default</option>
                    <option value="3" <? if($row['slider_option'] == "3") echo 'selected="yes"'; ?>>Default Without Mid Images</option>
				</select>
			</li>
            <? } ?>
             <?php if($row['theme'] == "101" || $row['theme'] == "110"){ ?> 
            <li>
				<label>Slider Type</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Coin Slider</option>
					<option value="2" <? if($row['slider_option'] == "2") echo 'selected="yes"'; ?>>Slide Out</option>
                    <option value="3" <? if($row['slider_option'] == "3") echo 'selected="yes"'; ?>>Advanced Slider</option>
                    <option value="4" <? if($row['slider_option'] == "4") echo 'selected="yes"'; ?>>Slide Show</option>
                    <option value="5" <? if($row['slider_option'] == "5") echo 'selected="yes"'; ?>>Curved Slider</option>
                    <option value="6" <? if($row['slider_option'] == "6") echo 'selected="yes"'; ?>>No Slider</option>
				</select>
			</li>
            <? } ?>
            <?php if($row['theme'] == "32" || $row['theme'] == "111"){ ?> 
            <li>
				<label>Slider Type</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="0" <? if($row['slider_option'] == "0") echo 'selected="yes"'; ?>>Accordion Slider</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Nivo Slider</option>
                    <option value="2" <? if($row['slider_option'] == "2") echo 'selected="yes"'; ?>>BXSlider</option>
                    <option value="3" <? if($row['slider_option'] == "3") echo 'selected="yes"'; ?>>No Slider</option>
                    <option value="4" <? if($row['slider_option'] == "4") echo 'selected="yes"'; ?>>Zooming Slider</option>
				</select>
			</li>
            <? } ?>
             <?php if($row['theme'] == "50"){ ?> 
            <li>
				<label>Remove Slider?</label>
				<? if($multi_user){ ?>
				<select name="lock_slider_option" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="slider_option" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="slider_option" title="select" class="uniformselect<?=checkLock('slider_option')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="0" <? if($row['slider_option'] == "0") echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['slider_option'] == "1") echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
            <? } ?>
            
            <?php if($row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "50" || $row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104"){ ?>  
            <li>
				<label>Remove Default Contact Page</label>
				<? if($multi_user){ ?>
				<select name="lock_remove_contact" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="remove_contact" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="remove_contact" title="select" class="uniformselect<?=checkLock('remove_contact')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['remove_contact'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['remove_contact'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<? } ?>
            
            
            <?php if($row['theme'] == "83" || $row['theme'] == "84" || $row['theme'] == "89"){ ?>
			<li>
				<label>Remove Contact Widget Box?</label>
				<select name="display_page" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['display_page'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['display_page'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<?php } ?>
            
            
            
            <?php if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "32"){ ?>
			<li>
				<label><?php if($row['theme'] == "5" || $row['theme'] == "8"){ ?>Move Logo to Left?<?php } ?><?php if($row['theme'] == "32"){ ?>Move Logo to Top?<?php } ?></label>
				
				<select name="form_type" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['form_type'] == "1") echo 'selected="yes"'; ?> >Yes</option>
					<option value="2" <? if($row['form_type'] == "2") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<?php } ?>
            
            
            <?php if($row['theme'] == "27" || $row['theme'] == "29" || $row['theme'] == "64") {?>
			<?php if($row['theme'] == "27" || $row['theme'] == "29") {?>
			<li>
				<label>Affiliate Information</label>
				<input type="text" name="affiliate_info" value="<?=$row['affiliate_info']?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] == "27") {?>
			<li>
				<label>Sign-Up/Buy Now Name</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Buy Now</i> or <i>Sign Up</i></div>
				<input type="text" name="signup_name" value="<?=$row['signup_name']?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] == "27" || $row['theme'] == "64") {?>
            <li>
				<label><?php if($row['theme'] == "27") {?>Sign-Up/Buy Now Link<?php } ?><?php if($row['theme'] == "64") {?>Home Page Info Link<?php } ?></label>
				<? if($multi_user){ ?>
				<select name="lock_signup_link" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('signup_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>http://</i> or <i>http://www.</i></div>
				<input type="text" <?=checkLock('signup_link')?> name="signup_link" value="<?=$row['signup_link']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
			<?php } ?>
            
            
            
            <?php if($row['theme'] == "27" || $row['theme'] == "50" || $row['theme'] == "63" || $row['theme'] == "69") {?>
            <li>
				<label>Button 1 Name</label>
				<? if($multi_user){ ?>
				<select name="lock_button_1" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button_1',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>More Info</i> or <i>Learn More</i></div>
				<input type="text" <?=checkLock('button_1')?> name="button_1" value="<?=$row['button_1']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Button 1 Link</label>
				<? if($multi_user){ ?>
				<select name="lock_button1_link" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button1_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>http://</i> or <i>http://www.</i></div>
				<input type="text" <?=checkLock('button1_link')?> name="button1_link" value="<?=$row['button1_link']?>" class="<?=$addClass?>" />
			</li>
             <li>
				<label>Button 2 Name</label>
				<? if($multi_user){ ?>
				<select name="lock_button_2" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button_2',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>More Info</i> or <i>Learn More</i></div>
				<input type="text" <?=checkLock('button_2')?> name="button_2" value="<?=$row['button_2']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Button 2 Link</label>
				<? if($multi_user){ ?>
				<select name="lock_button2_link" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button2_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>http://</i> or <i>http://www.</i></div>
				<input type="text" <?=checkLock('button2_link')?> name="button2_link" value="<?=$row['button2_link']?>" class="<?=$addClass?>" />
			</li>
            <?php if($row['theme'] == "27" || $row['theme'] == "63" || $row['theme'] == "69") {?>
             <li>
				<label>Button 3 Name</label>
				<? if($multi_user){ ?>
				<select name="lock_button_3" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button_3',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>More Info</i> or <i>Learn More</i></div>
				<input type="text" <?=checkLock('button_3')?> name="button_3" value="<?=$row['button_3']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Button 3 Link</label>
				<? if($multi_user){ ?>
				<select name="lock_button3_link" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('button3_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>http://</i> or <i>http://www.</i></div>
				<input type="text" <?=checkLock('button3_link')?> name="button3_link" value="<?=$row['button3_link']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
            <?php } ?>
            
            
            <?php if($row['theme'] == "27" || $row['theme'] == "29") {?>
            <li>
				<label>Call To Action Submit</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Free Info</i> or <i>Get Access Now</i></div>
				<input type="text" name="call_action_submit" value="<?=$row['call_action_submit']?>" />
			</li>
			<?php } ?>
            
            
            
            <li>
				<label>Use Custom Form or Your Own Autoresponder</label>
				<? if($multi_user){ ?>
				<select name="lock_add_autoresponder" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('add_autoresponder',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<textarea name="add_autoresponder" <?=checkLock('add_autoresponder')?> rows="6" class="<?=$addClass?>"><?=htmlentities($row['add_autoresponder'])?></textarea>
				<br style="clear:left" />
			</li>
            
            
           
            <?php if($row['theme'] == "24" || $row['theme'] == "27" || $row['theme'] == "29" || $row['theme'] == "46" || $row['theme'] == "50" || $row['theme'] == "57" || $row['theme'] == "63" || $row['theme'] == "69" || $row['theme'] == "85" || $row['theme'] == "98" || $row['theme'] == "104") {?>
            
			<li>
				<label>Video Script</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;"><?php if($row['theme'] == "24"){ ?>Required Size: 575 W X 340 H<?php } ?><?php if($row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "98"){ ?>MAX Width: 600px<?php } ?><?php if($row['theme'] == "46" || $row['theme'] == "63" || $row['theme'] == "69"){ ?>Required Size: 397 W X 250 H<?php } ?><?php if($row['theme'] == "50"){ ?>Required Size: 486 W X 284 H<?php } ?><?php if($row['theme'] == "27" || $row['theme'] == "29"){ ?>Required Size: 445 W X 364 H<?php } ?></div>
				<textarea name="video_script" rows="6"><?=$row['video_script']?></textarea><br style="clear:left;" />
			</li>
			
			<?php } ?>
            
            <?php if($row['theme'] == "22" || $row['theme'] == "50"){ ?>
            <li>
				<label>Remove JS Font?</label>
				<? if($multi_user){ ?>
				<select name="lock_remove_font" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="remove_font" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="remove_font" title="select" class="uniformselect<?=checkLock('remove_font')?>">
				<? } ?>
                	<option value="0">Please choose an option</option>
					<option value="0" <? if($row['remove_font'] == "0") echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['remove_font'] == "1") echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
            <?php } ?>
			
			
			<li>
				<label>Advanced CSS<? if(!$_SESSION['theme']){ ?> | <span style="color:#666; font-size:75%"><a href="#advcssVideo" class="trainingvideo">Watch Video</a></span><? } ?></label>
				<? if($multi_user){ ?>
				<select name="lock_adv_css" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('adv_css',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<textarea name="adv_css" <?=checkLock('adv_css')?> rows="6" class="<?=$addClass?>"><?=htmlentities($row['adv_css'])?></textarea>
				<br style="clear:left" />
			</li>
			<? if(!$_SESSION['theme']){ ?>
			<div style="display: none;">
				<div id="advcssVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/advcss-overview.flv" style="display:block;width:640px;height:480px" id="advcssplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("advcssplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<? } ?>
            <?php if($row['theme'] == "8" || $row['theme'] == "32" || $row['theme'] == "36"){ ?>  
			<li>
				<label>Display Gallery Page</label>
				<select name="legacy_gallery_link" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['legacy_gallery_link'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['legacy_gallery_link'] == "0") echo 'selected="no"'; ?>>No</option>
				</select>
			</li>
			<? } ?>
			<li style="display:none;">
			<input type="text" name="bg_img" value="" id="clear_bg_img" />
			</li>
            <li style="display:none;">
			<input type="text" name="alt_bg" value="" id="clear_alt_bg" />
			</li>
            </ul>
        </form>
        <?php if($row['theme'] == "17") {?>
        <form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
            <ul>
                <li>
                    <label>Form Type</label>
                    <select name="form_type" class="uniformselect" title="select">
                        <option value="0">Please choose an option</option>
                        <option value="1">Apartment Search</option>
                        <option value="2">New Home Search</option>
                        <option value="3">Foreclosure Search</option>
                    </select>
                </li>
            </ul>
        </form>
        <?php } ?>
        <?php if($row['theme'] == "38") {?>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Display DIY Pricing</label>
				<select name="display_diy" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['display_diy'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['display_diy'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
            <li>
				<label>Display Services Pricing</label>
				<select name="display_pricing" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['display_pricing'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['display_pricing'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<li>
				<label>Display Network Links</label>
				<select name="display_network" class="uniformselect" title="select">
					<option value="0">Please choose an option</option>
					<option value="1" <? if($row['display_network'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['display_network'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
            <li>
				<label>Display Default Contact Page</label>
			
				<select name="remove_contact" title="select" class="uniformselect">
		
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['remove_contact'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['remove_contact'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
		</ul>
	</form>
	<?php } ?>
			
			
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>HUB Theme Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#hubthemeVideo" class="trainingvideo">HUB Theme Video</a>
			</li>
			<div style="display: none;">
				<div id="hubthemeVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-theme-overview.flv" style="display:block;width:640px;height:480px" id="hubthemeplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubthemeplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
			<? if($themeInfo['css_file_path'] && $themeInfo['custom_css_support']){ ?>
			<li>
				<a href="hub-css-editor.php?id=<?=$_SESSION['current_id']?>">Edit Main CSS File</a>
			</li>
			<? } ?>
		</ul>
		 <? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>							
</div>
<!--End APP Right Panel-->
<br /><br style="clear:both;" />
<!-- hidden iframe -->
<iframe style="display:none;" height="1" width="1" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>
