<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	
	//authorize Access to page
	require_once('../auth.php');
	
	//initiate the class
	require_once('../reseller.class.php');
	$reseller = new Reseller();
	
	$name = $reseller->validateText($_POST['name']);
	$a = explode(" ", strtolower($name));
	$codename = implode('', $a);
	$admin_user = $_SESSION['login_uid'];
	
	if($_SESSION['thm_no_billing']){
	$dropdown = '<option value="670">Regular User</option>
		<option value="463">Membership User</option>
		<option value="668">User Level Manager</option>
		<option value="27">Network User</option>';
	}
	else {
	$dropdown = '<option value="463">Membership User</option>
		<option value="668">User Level Manager</option>
		<option value="27">Network User</option>
		<option value="28">Professional</option>
		<option value="30">Advanced</option>
		<option value="188">Corporate</option>
		<option value="189">Enterprise</option>
		<option value="882">ignitedLOCAL FastTrack</option>
		<option value="883">ignitedLOCAL AdvTrack</option>
		<option value="884">ignitedLOCAL ProTrack</option>
		<option value="885">ignitedLOCAL ProTrack +</option>';
	}
	$html = '
	<script type="text/javascript">
	$(function(){
		$(".uniformselect").uniform();
	});
	</script>
	<div id="form_test">
	<h1>Add New Product</h1>
	<form action="reseller-functions.php" class="jquery_form new_product" method="post">
		<h2>Product Info</h2>
		<label>Product Name</label>
		<input type="text" name="name" size="30" value="'.$name.'" class="no-upload" /><br /><br />
		<label>Code Name</label>
		<span style="color:#666;font-size:75%;">A short, lowercase, one-word name like "freelisting" or "contractorlisting"</span>
		<input type="text" name="type" value="'.$codename.'" class="no-upload" /><br /><br />
		<label>Maps To Product</label>
		<span style="color:#666;font-size:75%;">This sets the limits and wholesale rate if not a Free Product.</span>
		<select name="maps_to" title="select" class="uniformselect" class="no-upload" >
		<option value="">Please choose an account type...</option>
		'.$dropdown.'
		</select>
		<label>Description</label>
		<textarea name="description" rows="8" cols="55" id="edtr1" class="no-upload"></textarea>
		
		<br style="clear:both" /><br />
		<input type="hidden" name="action" value="addNewProduct" class="no-upload" />
		<input type="submit" value="Create Product" name="submit" />
	</form>
	</div>
	<br style="clear:both" />';
	
	$response['success']['id'] = '1';
	$response['success']['container'] = '#ajax-load';
	$response['success']['action'] = "replace";
	$response['success']['message'] = $html;
	
	echo json_encode($response);
?>