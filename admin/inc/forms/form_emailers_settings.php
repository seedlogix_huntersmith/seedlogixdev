<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$id = $_GET['id'];

$row = $hub->getFormEmailers($_SESSION['user_id'], NULL, $id);

$createdDisp = date('M jS', strtotime($row['created']));
$createdDisp .= ' at ';
$createdDisp .= date('g:ia', strtotime($row['created']));
$createdDisp .= ' CST';

if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
$('#schedMode').live('change', function(){
	if($(this).attr('value')=='date'){
		$('.schedDate').slideDown();
	}
	else {
		$('.schedDate').slideUp();
	}
});
</script> 
    
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/form_emailers_settings.php?id=<?=$id?>" class="current">Settings</a></li>
		<li><a href="inc/forms/form_emailers_email.php?id=<?=$id?>">Email</a></li>
		<li class="last"><a href="inc/forms/form_emailers.php?formID=<?=$row['lead_form_id']?>">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />
<br />
<h1>Emailer Settings</h1>
<div style="color:#333;"><strong>Tip:</strong> To send this email right now set the schedule to "Send Now" and Active to "Yes".<br />On the Email tab page you will have an option to send the email immediately.</div>
<form name="lead_forms_emailers" class="jquery_form" id="<?=$row['id']?>">
	<ul>
		<li><label>Name:</label><input type="text" name="name" value="<?=$row['name']?>" /></li><br />
		<li>
			<label>Schedule:</label>
			<select name="sched_mode" title="select" class="uniformselect" id="schedMode">
				<option value="date" <? if($row['sched_mode']=="date") echo 'selected'; ?>>Specify Date/Time:</option>
				<option value="now" <? if($row['sched_mode']=="now") echo 'selected'; ?>>Send Now</option>
			</select>
		</li>
		<li class="schedDate" style="margin-top:-10px;<? if($row['sched_mode']!="date") echo 'display:none;'; ?>">
			<label>Date <small>(mm/dd/yyyy, eg 01/01/2012)</small></label>
			<input type="text" name="sched_date" value="<?=$row['sched_date']?>" style="width:100px;" />
		</li>
		<li class="schedDate"<? if($row['sched_mode']!="date") echo ' style="display:none;"'; ?>>
			<label>Time</label>
			<select name="sched_hour" title="select" class="uniformselect">
				<option value="00"<? if($row['sched_hour']=="00") echo ' selected'; ?>>12am</option>
				<option value="01"<? if($row['sched_hour']=="01") echo ' selected'; ?>>1am</option>
				<option value="02"<? if($row['sched_hour']=="02") echo ' selected'; ?>>2am</option>
				<option value="03"<? if($row['sched_hour']=="03") echo ' selected'; ?>>3am</option>
				<option value="04"<? if($row['sched_hour']=="04") echo ' selected'; ?>>4am</option>
				<option value="05"<? if($row['sched_hour']=="05") echo ' selected'; ?>>5am</option>
				<option value="06"<? if($row['sched_hour']=="06") echo ' selected'; ?>>6am</option>
				<option value="07"<? if($row['sched_hour']=="07") echo ' selected'; ?>>7am</option>
				<option value="08"<? if($row['sched_hour']=="08") echo ' selected'; ?>>8am</option>
				<option value="09"<? if($row['sched_hour']=="09") echo ' selected'; ?>>9am</option>
				<option value="10"<? if($row['sched_hour']=="10") echo ' selected'; ?>>10am</option>
				<option value="11"<? if($row['sched_hour']=="11") echo ' selected'; ?>>11am</option>
				<option value="12"<? if($row['sched_hour']=="12") echo ' selected'; ?>>12pm</option>
				<option value="13"<? if($row['sched_hour']=="13") echo ' selected'; ?>>1pm</option>
				<option value="14"<? if($row['sched_hour']=="14") echo ' selected'; ?>>2pm</option>
				<option value="15"<? if($row['sched_hour']=="15") echo ' selected'; ?>>3pm</option>
				<option value="16"<? if($row['sched_hour']=="16") echo ' selected'; ?>>4pm</option>
				<option value="17"<? if($row['sched_hour']=="17") echo ' selected'; ?>>5pm</option>
				<option value="18"<? if($row['sched_hour']=="18") echo ' selected'; ?>>6pm</option>
				<option value="19"<? if($row['sched_hour']=="19") echo ' selected'; ?>>7pm</option>
				<option value="20"<? if($row['sched_hour']=="20") echo ' selected'; ?>>8pm</option>
				<option value="21"<? if($row['sched_hour']=="21") echo ' selected'; ?>>9pm</option>
				<option value="22"<? if($row['sched_hour']=="22") echo ' selected'; ?>>10pm</option>
				<option value="23"<? if($row['sched_hour']=="23") echo ' selected'; ?>>11pm</option>
			</select>
			<small>Current server date/time: <?=date('m/d/y g:ia').' CST';?></small>
		</li>
		<li>
			<label>Emailer Active?</label>
			<select name="active" title="select" class="uniformselect">
				<option value="1" <? if($row['active']==1) echo 'selected="yes"'; ?>>Yes</option>
				<option value="0" <? if($row['active']==0) echo 'selected="yes"'; ?>>No</option>
			</select>
		</li>
	</ul>
</form><br />
<? } else echo "Not authorized."; ?>