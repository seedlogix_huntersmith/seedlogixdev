<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

if(1 || $_SESSION['user_id'] == 6610)   // LIVE!!
{
    require 'forms2_autoresponder.amado.php';
    exit;
}

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

if($_GET['reseller_site']){
	$_SESSION['current_id'] = $_SESSION['main_site_id'];
	$resellerSite = true;
}
else $_SESSION['current_id'] = $_GET['id'];

$forms = $hub->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], $_SESSION['current_id'], NULL, NULL, 1);

$results = $hub->getAutoResponders($_SESSION['current_id'], 'lead_forms', $_SESSION['user_id']);
$ar_count = $results ? $hub->numRows($results) : 0;

//check responder limits for type/campaign
if($_SESSION['user_limits']['responder_limit']==-1)
	$respondersRemaining = "Unlimited";
else {
	$numResponders = $hub->getAutoResponders(NULL, 'hub', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
	$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
	if($respondersRemaining<0) $respondersRemaining = 0;
}
//email limits
$emailsRemaining = $hub->emailsRemaining($_SESSION['user_id']);
if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
else $emailsRemaining = number_format($emailsRemaining);

if(!$resellerSite || ($resellerSite && $_SESSION['reseller'])){
	if($_GET['form']){
		$form = $_GET['form'];
		include('autoresponse-'.$form.'.php');
	}
	else {
		$thisPage = "formsauto";
		include('auto_nav.php');
		echo '<h1>'.$forms['name'].' Responders</h1>';
		echo '<div id="createDir">
				<form method="post" class="ajax" action="add-autoresponder2.php">
					<input type="hidden" name="table_id" value="'.$_SESSION['current_id'].'" />
					<input type="hidden" name="table_name" value="lead_forms" />
					<input type="hidden" name="type" value="forms2" />
					<input type="text" name="name" ';
					if((is_numeric($respondersRemaining) && $respondersRemaining==0)) 
					echo 'readonly';
		echo			' />
					<input type="submit" value="Create New Email" name="submit" />
				</form>
			</div>';
		
		if($ar_count){ //if user has any autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			while($row = $hub->fetchArray($results)){ //loop through and display links to each
				echo '<div class="item3">
						<div class="icons">
							<a href="inc/forms/forms-autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete rmParent" rel="table=auto_responders&id='.$row['id'].'"><span>Delete</span></a>
						</div>';
					echo '<img src="img/field-email.png" class="icon" />';
					echo '<div class="name"><a href="inc/forms/forms-autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a></div>
						  <div class="type"><a href="inc/forms/forms-autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched'].'</a></div>
						  <div class="date"><a href="inc/forms/forms-autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched_mode'].'</a></div>
					</div>';
			}
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
		
		else{ //if no autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			echo 'Use the form above to create a new auto responder.<br />';
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> hub auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
	}
}
?>