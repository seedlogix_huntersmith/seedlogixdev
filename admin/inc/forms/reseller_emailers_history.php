<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
if($_SESSION['reseller'] || $_SESSION['admin']){
	if(!$_GET['id'] && !$_GET['popHistoryID']){
		if($emailers = $reseller->getResellerEmailers($_SESSION['login_uid'], NULL, 1)){
			$numEmailers = count($emailers);
		}
		echo '<br /><br />';
		if($numEmailers){ //if user has sent any emailers
			echo '<div id="dash"><div id="fullpage_reseller-emailers" class="dash-container"><div class="container">';
			foreach($emailers as $key=>$row){ //loop through and display links to each
				$dateTimeSent = date('F jS, Y', strtotime($row['sent'])).' at '.date('g:ia', strtotime($row['sent']));
				$a = explode('::', $row['sent_to']);
				$numRecips = count($a);
				if($numRecips>1){
					if(strlen($row['name'])>=51){
						$dispName = substr($row['name'], 0, 51).'...';
					}
					else $dispName = $row['name'];
					echo '<div class="item2">';
						echo '<img src="img/field-email.png" class="icon" />';
						echo '<a href="inc/forms/reseller_emailers_history.php?id='.$key.'">'.$dispName.
							' &nbsp;&nbsp;&nbsp;('.$dateTimeSent.' -- '.$numRecips.' recipients)</a>
						</div>';
				}
				$dateTimeSent = $numRecips = $dispName = $a = NULL;
			}
			echo '</div></div></div>';
		}
		
		else { //if no history
			echo '<div id="dash"><div id="fullpage_emailers" class="dash-container"><div class="container">';
			echo 'No sending history to display.<br />';
			echo '</div></div></div>';
		}
	} //end if(!$_GET['id'])
	else if($_GET['popHistoryID']){
		//if user has clicked the link to open a popup of the full sent-to history
		$id = is_numeric($_GET['popHistoryID']) ? $_GET['popHistoryID'] : '';
		if($id){
			$query = "SELECT sent_to FROM resellers_emailers_history 
					WHERE id = '".$id."' AND admin_user = '".$_SESSION['login_uid']."'";
			if($data = $reseller->queryFetch($query)){
				if($a = explode('::', $data['sent_to'])){
					foreach($a as $key=>$value){
						$query = "SELECT username FROM users WHERE id = '".$value."'";
						$b = $reseller->queryFetch($query);
						//echo '#'.$value.' - '.$b['username'].'<br />';
						echo $b['username'].'<br />';
						$b = NULL;
					}
				}
			}
		}
	}
	else {
		$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
		if($id){
			$query = "SELECT * FROM resellers_emailers_history WHERE id = '".$id."' AND admin_user = '".$_SESSION['login_uid']."'";
			$row = $reseller->queryFetch($query);
			$dateTimeSent = '<strong>'.date('F jS, Y', strtotime($row['sent'])).'</strong>
						 at <strong>'.date('g:ia', strtotime($row['sent'])).'</strong>';
			if($row['from_field']){
				$fromField = str_replace('<', '&lt;', stripslashes($row['from_field']));
				$fromField = str_replace('>', '&gt;', $fromField);
				$fromField = str_replace('" ', '', $fromField);
				$fromField = str_replace('"', '', $fromField);
			}
			$backImg = 'img/v3/nav-previous-icon.jpg';
			//sent users
			$sentArray = explode('::', $row['sent_to']);
			$numSent = count($sentArray);
			for($i=0; $i<24; $i++){
				if($sentArray[$i]){
					$query = "SELECT username FROM users WHERE id = '".$sentArray[$i]."'";
					$a = $reseller->queryFetch($query);
					$dispSent .= '#'.$sentArray[$i].' - '.$a['username'].'<br />';
					$a = NULL;
				}
			}
			if($numSent>25){
				$dispSent .= '<br /><strong>'.($numSent-25).'</strong> more not shown.  <a href="#" class="dflt-link no-ajax" id="popEmailerHistory" rel="'.$row['id'].'">Click here</a> for a full list.';
			}
?>
	<h1><?=$row['name']?></h1>
	<a href="reseller-emailers.php?display=history" class="dflt-link"><img src="<?=$backImg?>" alt="Back" /></a>
	<form class="jquery_form">
		<ul>
			<li>
				<label>Sent:</label>
				<?=$dateTimeSent?>
				<br />
			</li>
			<li>
				<label>Sent To:</label>
				<?=$dispSent?>
				<br /><br />
			</li>
			<li>
				<label>Subject:</label>
				<input type="text" name="subject" value="<?=stripslashes($row['subject'])?>" class="no-upload" />
			</li>
			<li>
				<label>From Field:</label>
				<input type="text" name="from_field" value="<?=$fromField?>" class="no-upload" />
			</li>
			<li>
				<label>Message:</label>
				<a href="#" id="AR1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="body" rows="15" cols="80" id="edtrAR1" class="no-upload"><?=stripslashes($row['body'])?></textarea>
			</li>
			<? if($row['contact']){ ?>
			<li>
				<label>Business Name/Address:</label>
				<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="contact" rows="5" cols="80" id="edtr2" class="no-upload"><?=stripslashes($row['contact'])?></textarea>
			</li>
			<? } ?>
			<? if($row['anti_spam']){ ?>
			<li>
				<label>Anti-Spam Message:</label>
				<textarea name="anti_spam" rows="4" cols="80" id="edtr3" class="no-upload"><?=stripslashes($row['anti_spam'])?></textarea>
			</li>
			<? } ?>
		</ul>
	</form>
	<br style="clear:both;" />
<?
		}
	}
} //end if($_SESSION['reseller'])
?>