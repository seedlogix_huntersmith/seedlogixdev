<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	$query = "SELECT id, user_id, user_id_created, blog_title, seo_title, seo_desc, seo_keywords, keyword1, keyword2, keyword3, keyword4, keyword5, keyword6, posts_v2 FROM blogs WHERE id = '".$_SESSION['current_id']."'";
	$row = $blog->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript">
$(function(){
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "seo";
	$postsV2 = $row['posts_v2'] ? 1 : 0;
	include('add_blog_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['blog_title']?> - SEO</h2>
	<form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Title</label>
				<input type="text" name="seo_title" value="<?=$row['seo_title']?>" />
			</li>
			<li>
				<label>Meta Description</label>
				<input type="text" name="seo_desc" value="<?=$row['seo_desc']?>" />
			</li>
			<li>
				<label>Meta Keywords</label>
				<input type="text" name="seo_keywords" value="<?=$row['seo_keywords']?>" />
			</li>
			<li>
				<label>Keyword 1</label>
				<input type="text" name="keyword1" value="<?=$row['keyword1']?>" />
			</li>
			<li>
				<label>Keyword 2</label>
				<input type="text" name="keyword2" value="<?=$row['keyword2']?>" />
			</li>
			<li>
				<label>Keyword 3</label>
				<input type="text" name="keyword3" value="<?=$row['keyword3']?>" />
			</li>
			<li>
				<label>Keyword 4</label>
				<input type="text" name="keyword4" value="<?=$row['keyword4']?>" />
			</li>
			<li>
				<label>Keyword 5</label>
				<input type="text" name="keyword5" value="<?=$row['keyword5']?>" />
			</li>
			<li>
				<label>Keyword 6</label>
				<input type="text" name="keyword6" value="<?=$row['keyword6']?>" />
			</li>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>SEO Settings Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogseoVideo" class="trainingvideo">Search Engine Optimization Video</a>
			</li>
			<div style="display:none;">
				<div id="blogseoVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blogs-seo-overview.flv" style="display:block;width:640px;height:480px" id="blogseoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogseoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="external" target="_new" href="http://6qube.com/blog-preview.php?id=<?=$_SESSION['user_id']?>&bid=<?=$row['id']?>"><img src="http://6qube.com/admin/img/preview-button.png" border="0" /></a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="https://adwords.google.com/select/KeywordToolExternal" class="external" target="_new">Google Keyword Tool</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
			<a class="external" target="_new" href="http://6qube.com/blog-preview.php?id=<?=$row['user_id_created']?>&bid=<?=$row['id']?>"><img src="http://6qube.com/admin/img/preview-button/<?=$_SESSION['thm_buttons-clr']?>.png" border="0" /></a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<? } else echo "Error displaying page."; ?>