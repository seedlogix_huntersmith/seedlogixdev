<?php
	$a = 'class="current"';
	$b = 'class="last"';
?>
<style type="text/css">
	#subform-nav li { background:url('img/sub-nav-form-bg/<?=$_SESSION['thm_nav3-bg']?>.png') repeat-x; }
	#subform-nav li.first { background:url('img/sub-nav-form-left/<?=$_SESSION['thm_nav3-bg']?>.png') no-repeat top left; }
	#subform-nav li.last { background:url('img/sub-nav-form-right/<?=$_SESSION['thm_nav3-bg']?>.png') no-repeat top right; }
	#subform-nav a:hover, #subform-nav a.current { color:#<?=$_SESSION['thm_nav3-links2']?>; background:url('img/sub-form-nav-hover-bg/<?=$_SESSION['thm_nav3-bg2']?>.png'); }
	#subform-nav a { color:#<?=$_SESSION['thm_nav3-links']?>; }
</style>

<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/edit-theme2.php" <? if(!$frm) echo $a; ?>>Header/Body</a></li>
		<li><a href="inc/forms/edit-theme-navs2.php" <? if($frm=='navs') echo $a; ?>>Navigation</a></li>
		<li><a href="inc/forms/edit-theme-content2.php" <? if($frm=='content') echo $a; ?>>Content</a></li>
		<li <? if(!$_SESSION['main_site']) echo $b; ?>><a href="inc/forms/edit-theme-support.php" <? if($frm=='support') echo $a; ?>>Support/Users</a></li>
		<? if($_SESSION['main_site']){ ?>
		<li <?=$b?>><a href="inc/forms/edit-theme-login.php" <? if($frm=='login') echo $a; ?>>Login</a></li>
		<? } ?>
	</ul>
</div>