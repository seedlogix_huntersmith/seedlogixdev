<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

if($_GET['reseller_site']){
	$_SESSION['current_id'] = $_SESSION['main_site_id'];
	$resellerSite = true;
}
else $_SESSION['current_id'] = $_GET['id'];

$results = $hub->getAutoResponders($_SESSION['current_id'], 'hub', $_SESSION['user_id']);
$ar_count = $results ? $hub->numRows($results) : 0;

//check responder limits for type/campaign
if($_SESSION['user_limits']['responder_limit']==-1)
	$respondersRemaining = "Unlimited";
else {
	$numResponders = $hub->getAutoResponders(NULL, 'hub', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
	$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
	if($respondersRemaining<0) $respondersRemaining = 0;
}
//email limits
$emailsRemaining = $hub->emailsRemaining($_SESSION['user_id']);
if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
else $emailsRemaining = number_format($emailsRemaining);

if(!$resellerSite || ($resellerSite && $_SESSION['reseller'])){
	if($_GET['form']){
		$form = $_GET['form'];
		include('autoresponse_'.$form.'.php');
	}
	else{
		echo '<div id="createDir">
				<form method="post" class="ajax" action="add-autoresponder.php">
					<input type="hidden" name="table_id" value="'.$_SESSION['current_id'].'" />
					<input type="hidden" name="table_name" value="hub" />
					<input type="text" name="name" ';
					if((is_numeric($respondersRemaining) && $respondersRemaining==0)) 
					echo 'readonly';
		echo			' />
					<input type="submit" value="Create New Autoresponder" name="submit" />
				</form>
			</div>';
		
		if($ar_count){ //if user has any autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			while($row = $hub->fetchArray($results)){ //loop through and display links to each
				echo '<div class="item">
						<div class="icons">
							<a href="inc/forms/hub_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete" rel="table=auto_responders&id='.$row['id'].'"><span>Delete</span></a>
						</div>';
					echo '<img src="img/autoresponder';
					if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
					echo '.png" class="graph trigger" />';
					echo '<a href="inc/forms/hub_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
					</div>';
			}
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> hub auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
		
		else{ //if no autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			echo 'Use the form above to create a new auto responder.<br />';
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> hub auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
	}
}
?>