<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/local.class.php');
$local = new Local();
$parent_id = $_GET['id'];
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

//get site info
$query = "SELECT 
			id, admin_user, name, domain, company, logo, favicon, bg_color,  
			`header-bg-img`, `footer-bg-img`, `nav-link-hover-clr`, `no_image`, theme, adv_css
		FROM resellers_network 
		WHERE parent = ".$parent_id." AND type = 'press' LIMIT 1";
$row = $local->queryFetch($query);

//prepare domain value for Google note below
$a = explode('.', $row['domain']);
$domain2 = $a[count($a)-2].'.'.$a[count($a)-1];

$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
$themeInfo = $local->queryFetch($query);
	
if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "press";
include('edit_whole_network_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Press Site Settings</h2>
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<li>
				<label>Company/Display Name</label>
				<input type="text" name="company" value="<?=$row['company']?>" />
			</li>
			<li>
				<label>Domain</label>
				<a href="<?=$row['domain']?>" class="view dflt-link" target="_blank"><?=$row['domain']?></a>
			</li><br />
			<li style="margin-top:-10px;">
				<label>Background Color</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>#ff0000</i> or <i>red</i></div>
				<input type="text" name="bg_color" value="<?=$row['bg_color']?>" />
			</li>
			<li>
				<label>Navigation Link Hover Color</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>#ff0000</i> or <i>red</i></div>
				<input type="text" name="nav-link-hover-clr" value="<?=$row['nav-link-hover-clr']?>" />
			</li>
		</ul>
	</form><br />
	
	<!-- Logo -->
	<form id="logo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Site Logo | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['logo_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['logo_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="logo" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="logo_upload_prvw" class="img_cnt" src="<? if($row['logo']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['logo'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<!-- Favicon -->
	<form id="favicon_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Favicon | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a> | <span style="color:#666; font-size:75%;">max 64w x 64h image in .png format</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="favicon" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="favicon_upload_prvw" class="img_cnt" src="<? if($row['favicon']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['favicon'];  else echo $dflt_noImg; ?>" style="width:64px;" /><br />
	
	<!-- Header BG Img -->
	<form id="header-bg-img_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Header BG Image | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['header_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['header_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="header-bg-img" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="header-bg-img_upload_prvw" class="img_cnt" src="<? if($row['header-bg-img']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['header-bg-img'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<!-- Footer BG Img -->
	<form id="footer-bg-img_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Footer BG Image | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['footer_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['footer_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="footer-bg-img" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="footer-bg-img_upload_prvw" class="img_cnt" src="<? if($row['footer-bg-img']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['footer-bg-img'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<!-- No Image Img -->
	<form id="no_image_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>'No Image' Image | <span style="color:#666; font-size:75%;">Default image displayed if an item doesn't have one</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="no_image" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="no_image_upload_prvw" class="img_cnt" src="<? if($row['no_image']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['no_image'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			
			<li>
				<label>Advanced CSS</label>
			
				<textarea name="adv_css" rows="6" ><?=htmlentities($row['adv_css'])?></textarea>
				<br style="clear:left" />
			</li>
			
		</ul>
	</form><br />

</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_press_nav.php');
?>	
						
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>