<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	$query="SELECT id, user_id, user_id_created, blog_title, slogan, domain, website_url, category, company_name, phone, address, city, state, logo, coupon_offer, google_webmaster, posts_v2, theme, lincoln_domain FROM blogs WHERE id = '".$_SESSION['current_id']."'";
	$row = $blog->queryFetch($query);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if($row['posts_v2']!=1 && $blog->isThemeV2($row['theme'])){ ?>
	$('#upgradeBlog').die();
	$('#upgradeBlog').live('click', function(){
		if($('#upgradeBlogInfo').css('display')=='none'){ $('#upgradeBlogInfo').slideDown(); }
	});
	
	$('#upgradeBlogFinal').die();
	$('#upgradeBlogFinal').live('click', function(){
		if($('#upgradeBlogInfo').css('display')!='none'){ $('#upgradeBlogInfo').slideUp(); }
		var blogID = <?=$_SESSION['current_id']?>;
		var params = { 'blog_id': blogID };
		$.post('upgrade-blog.php', params, function(data){
			if(data.success == 1){
				alert('Upgrade was successful!');
				$('#wait').show();
				$.get('blog.php?form=settings&id='+blogID, function(data){
					$("#ajax-load").html(data);
				});
			}
			else {
				$('#ajax-load').prepend('<b>'+data.message+'</b>');
			}
		}, 'json');
	});
	<? } ?>
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'		: '75%',
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "settings";
	$postsV2 = $row['posts_v2'] ? 1 : 0;
	include('add_blog_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['blog_title']?> - Settings</h2>
	<form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Blog Title</label>
				<input type="text" name="blog_title" value="<?=$row['blog_title']?>" />
			</li>
			<li>
				<label>Slogan</label>
				<textarea name="slogan" rows="3"><?=$row['slogan']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Blog Domain Name</label>
				<div id="domainerror" style="color:#888;font-size:12px; padding-bottom:10px;">Example: http://blog.domain.com (recommended) or http://www.domainblog.com<br /><strong>Important:</strong> If you are using a subdomain (like blog.domain.com), make sure you create a HUB with the domain (domain.com) first.</div>
				<input type="text" id="domain" name="domain" class="website-url" value="<?=$row['domain']?>" title="blog" />
			</li>
			<li>
				<label>Website URL</label>
				<div style="color:#888;font-size:12px; padding-bottom:10px;">The URL of your company's main website.</div>
				<input type="text" name="website_url" value="<?=$row['website_url']?>" id="website_url" />
			</li>
			<li>
				<label>Business Category</label>
				<?= $blog->displayCategories($row['category']); ?>
			</li>
			<li>
				<label>Business Name</label>
				<input type="text" name="company_name" value="<?=$row['company_name']?>" />
			</li>
			<li>
				<label>Phone Number</label>
				<input type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Address</label>
				<input type="text" name="address" value="<?=$row['address']?>" />
			</li>
			<li>
				<label>City</label>
				<input type="text" name="city" value="<?=$row['city']?>" />
			</li>
			<li>
				<label>State</label>
				<?=$blog->stateSelect('state', $row['state'])?>
			</li>
		</ul>
	</form>
	
	<form id="logo" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Logo | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a> | <span style="color:#666; font-size:75%">Recommended Size: 330w x 130h</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="blogs" />
		<input type="hidden" name="fname" value="logo" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['logo']) echo 'http://'.$_SESSION['main_site'].'/users/'.$row['user_id'].'/blogs/'.$row['logo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Coupon Offer</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="coupon_offer" rows="6" id="edtr1"><?=$row['coupon_offer']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Google Webmaster Tools</label>
				<textarea name="google_webmaster" rows="2"><?=$row['google_webmaster']?></textarea>
			</li>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Blog Settings Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogsettingsVideo" class="trainingvideo">Blog Settings Video</a>
			</li>
			<div style="display:none;">
				<div id="blogsettingsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blogs-settings-overview.flv" style="display:block;width:640px;height:480px" id="blogsettingsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogsettingsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<li>
				<a href="#buydomainsVideo" class="trainingvideo">How to Buy a Domain Name</a>
			</li>
			<div style="display:none;">
				<div id="buydomainsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/buydomains-overview.flv" style="display:block;width:640px;height:480px" id="buydomainsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("buydomainsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<li>
				<a href="#domainsVideo" class="trainingvideo">How to Add a Domain Name?</a>
			</li>
			<div style="display:none;">
				<div id="domainsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/domains-overview.flv" style="display:block;width:640px;height:480px" id="domainsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("domainsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a class="external" target="_new" href="http://www.google.com/webmasters/tools/" >Google Webmaster Tools</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new" >Resize or Crop Images</a>
			</li>
		</ul>
		<div align="center">
			<a class="helpLinks" target="_blank" href="http://<?=$_SESSION['main_site']?>/blog-preview.php?uid=<?=$row['user_id_created']?>&bid=<?=$row['id']?>"><img src="img/preview-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a>
			<? if(($row['posts_v2']!=1) && $blog->isThemeV2($row['theme'])){ ?>
			<a id="upgradeBlog" href="#" rel="<?=$_SESSION['current_id']?>"><img src="img/upgrade-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a>
			<div id="upgradeBlogInfo" style="display:none;text-align:left;">
				<br /><br />
				<strong>Upgrade Information:</strong><br />
				Upgrading your blog will change all the URLs to be more SEO-friendly.<br /><br />
				For example an old URL like <em>blog.com/category/post-title-123.htm</em> will now be <em>blog.com/category/post-title/</em>.  This upgrade is not recommended for older or well-established blogs because it will adversely affect search engine indexing of your site for some time.  Old links around the web will continue to load the same page.<br /><br />
				The upgrade cannot be undone.  We also recommend not changing your blog's theme once you've upgraded because not all themes support the new version.<br /><br />
				<a href="#" id="upgradeBlogFinal" class="dflt-link">Click here if you understand<br />and wish to continue.</a>
			</div>
			<? } ?>
		</div>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>