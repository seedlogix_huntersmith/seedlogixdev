<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, name, company_name, phone, street, city, state, zip, category, website_url, display_info, photo FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$row = $directory->queryFetch($query);
	
	$query2 = "SELECT class FROM users WHERE id = '".$_SESSION['user_id']."' ";
	$row2 = $directory->queryFetch($query2);
	
	$query3 = "SELECT industry, category FROM user_class WHERE id = '".$row2['class']."' ";
	$row3 = $directory->queryFetch($query3);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'				: '75%',
		'height'				: '75%',
		'autoScale'			: false,
		'transitionIn'			: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'		: 'inside',
		'transitionIn'			: 'none',
		'transitionOut'		: 'none'
	});
	<? } ?>
});
</script>

<!-- FORM NAVIGATION -->
<?php
	$thisPage = "listings";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />

<div id="form_test">
	<h2><?=$row['name']?> - Listing Information</h2>
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Business Name</label>
				<input type="text" class="company-name-dir" name="company_name" value="<?=$row['company_name']?>" />
			</li>
			<li>
				<label>Phone</label>
				<input type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Street Address</label>
				<input type="text" name="street" value="<?=$row['street']?>" />
			</li>
			<li>
				<label>City</label>
				<input type="text" name="city" value="<?=$row['city']?>"/>
			</li>
			<li>
				<label>State</label>
				<?=$directory->stateSelect('state', $row['state'])?>
			</li>
			<li>
				<label>Zip Code</label>
				<input type="text" name="zip" value="<?=$row['zip']?>"/>
			</li>
			<? if(!$row3['category']){ ?>
			<li>
				<label>Business Category</label>
				<?php echo $directory->displayCategories($row['category'], NULL, $row3['industry']);?>
			</li>
			<? } ?>
			<li>
				<label>Website URL</label>
				<input type="text" name="website_url" value="<?=$row['website_url']?>" />
			</li>
			<li>
				<label>Business Listing Description</label>
				<!--<input type="text" name="display_info" value="<?=$row['display_info']?>" />-->
				<textarea name="display_info" rows="6" cols="40"><?=$row['display_info']?></textarea>
			</li>
		</ul>
	</form> 
	
	<form id="logo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Business Logo</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="photo" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['photo']) echo 'http://'.$_SESSION['main_site'].'/users/'.$row['user_id'].'/directory/'.$row['photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Listing Info Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#dirlistingVideo" class="trainingvideo">Listing Information Video</a>
			</li>
			<div style="display:none;">
				<div id="dirlistingVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-listings-overview.flv" style="display:block;width:640px;height:480px" id="dirlistingplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("dirlistingplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
	</div> <!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.google.com/local/add" class="external" target="_new">Google Places</a>
			</li>
			<li>
				<a href="https://ssl.bing.com/listings/ListingCenter.aspx" class="external" target="_new">Bing Local</a>
			</li>
			<li>
				<a href="http://listings.local.yahoo.com/" class="external" target="_new">Yahoo Local</a>
			</li>
		</ul>
		<? if(!$_SESSION['theme']){ ?>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
		<? } ?>
	</div> <!--End APP Right Links-->
	<?	
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>							
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />

<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>