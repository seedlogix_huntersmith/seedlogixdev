<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, user_id_created, blog_title, blog_edit_1, blog_edit_2, blog_edit_3, theme, posts_v2 FROM blogs WHERE id = '".$_SESSION['current_id']."'";
	$row = $blog->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
		$query = "SELECT blog_region1_title, blog_region2_title, blog_region3_title FROM themes WHERE id = '".$row['theme']."'";
		$themeInfo = $blog->queryFetch($query);
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript">
$(function(){
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'		: '75%',
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});	
});
</script>
<? } ?>
<script type="text/javascript">
<!--
// this tells jquery to run the function below once the DOM is read
$(document).ready(function() {
// choose text for the show/hide link
var showText="Show";
var hideText="Hide";
// append show/hide links to the element directly preceding the element with a class of "toggle"
$(".toggle").prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');
// hide all of the elements with a class of 'toggle'
$('.toggle').hide();
// capture clicks on the toggle links
$('a.toggleLink').click(function() {
// change the link depending on whether the element is shown or hidden
if ($(this).html()==showText) {
$(this).html(hideText);
}
else {
$(this).html(showText);
}
// toggle the display
$(this).parent().next('.toggle').toggle('slow');
// return false so any link destination is not followed
return false;
});
});
//-->
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "edit";
	$postsV2 = $row['posts_v2'] ? 1 : 0;
	include('add_blog_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['blog_title']?> - Edit Regions</h2>
    <form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
		<? 
		
		if($themeInfo['blog_region1_title']){
		?>	
			
            	<label><?=$themeInfo['blog_region1_title']?></label>
				<li class="toggle" >
                  
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea name="blog_edit_1"  rows="20" cols="60" id="edtr1" ><?=$row['blog_edit_1']?></textarea>
                 
				</li>
		
            <? if($themeInfo['blog_region2_title']){ ?>
            	<label><?=$themeInfo['blog_region2_title']?></label>
				<li class="toggle" >
                  
					
					<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
					<textarea name="blog_edit_2"  rows="20" cols="60" id="edtr2" ><?=$row['blog_edit_2']?></textarea>
                 
				</li>
			<? } ?>
            
            <? if($themeInfo['blog_region3_title']){ ?>
            	<label><?=$themeInfo['blog_region3_title']?></label>
				<li class="toggle" >
                  
					
					<a href="#" id="3" class="tinymce"><span>Rich Text</span></a>
					<textarea name="blog_edit_3"  rows="20" cols="60" id="edtr3" ><?=$row['blog_edit_3']?></textarea>
                 
				</li>
			<? } ?>
			
		<? } ?>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Blog Settings Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogsocialVideo" class="trainingvideo">Social Network Overview</a>
			</li>
			<div style="display:none;">
				<div id="blogsocialVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blogs-social-overview.flv" style="display:block;width:640px;height:480px" id="blogsocialplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogsocialplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="helpLinks" href="http://6qube.com/blog-preview.php?id=<?=$_SESSION['user_id']?>&bid=<?=$row['id']?>"><img src="http://6qube.com/admin/img/preview-button.png" border="0" /></a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.facebook.com/pages/create.php?campaign_id=372931622610&placement=pghm&extra_1=0" class="external" target="_new">Create a Facebook Business Page</a>
			</li>
			<li>
				<a href="https://twitter.com/signup" class="external" target="_new">Create a Twitter Account</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
			<a class="helpLinks" href="http://6qube.com/blog-preview.php?id=<?=$row['user_id_created']?>&bid=<?=$row['id']?>"><img src="http://6qube.com/admin/img/preview-button/<?=$_SESSION['thm_buttons-clr']?>.png" border="0" /></a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo 'Error displaying page.'; ?>