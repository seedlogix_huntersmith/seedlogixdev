<?php
$isdev  =   $_SESSION['user_id']    ==  6610 || $_SESSION['user_id'] ==  6312;
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
if($_POST['action']=='changePageOrder'){
	$return['success'] = 0;
	//if user has clicked the Up or Down icon on a page/nav
	$pageID = is_numeric($_POST['pageID']) ? $_POST['pageID'] : '';
	$currOrder = is_numeric($_POST['currOrder']) ? $_POST['currOrder'] : '';
	$mu = $_POST['multi_user'] ? true : false;
	$move = $_POST['move'];
	$change = $move=='Down' ? 1 : -1;
	$newOrder = $currOrder + $change;
	//get existing item info
	$query = "SELECT hub_id, nav_parent_id FROM hub_page2 WHERE id = '".$pageID."' AND user_id = '".$_SESSION['user_id']."'";
	$pageInfo = $hub->queryFetch($query);
	if($pageID && isset($currOrder) && $pageInfo){
		//get id we're replacing with
		$query = "SELECT id FROM hub_page2 WHERE nav_order = '".$newOrder."' 
				  AND hub_id = '".$pageInfo['hub_id']."' AND nav_parent_id = '".$pageInfo['nav_parent_id']."'";
		$a = $hub->queryFetch($query);
		//adjust existing higher or lower ordered items
		$query = "UPDATE hub_page2 SET nav_order = ";
		if($change<0) $query .= "nav_order+1";
		else $query .= "nav_order-1"; 
		$query .= " WHERE id = '".$a['id']."'";
		if($mu) $query .= " OR page_parent_id = '".$a['id']."'"; //adjust for mu hubs too
		//update this one
		$query2 = "UPDATE hub_page2 SET nav_order = ";
		if($change<0) $query2 .= "nav_order-1";
		else $query2 .= "nav_order+1"; 
		$query2 .= " WHERE id = '".$pageID."'";
		if($mu) $query2 .= " OR page_parent_id = '".$pageID."'"; //adjust for mu hubs too
		if($hub->query($query) && $hub->query($query2)){
			$return['success'] = 1;
			//$return['message'] = 'thisID/neworder: '.$pageID.'/'.$newOrder.', oldID/neworder: '.$a['id'].'/'.$currOrder;
		}
		//else $return['message'] = '\n'.mysqli_error();
	}
	
	echo json_encode($return);
}
else {
	$hub_id = is_numeric($_GET['id']) ? $_GET['id'] : 0;
	$nav_parent_id = is_numeric($_GET['p']) ? $_GET['p'] : 0;
	$page_id = is_numeric($_GET['p2']) ? $_GET['p2'] : '';
	//misc display vars
	$sel = 'selected="selected"';
	
	//get hub info and make sure the hub belongs to the user
	$query = "SELECT has_feedobjects, user_id, hub_parent_id, domain, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields FROM hub WHERE id = '".$hub_id."'";
	$hubInfo = $hub->queryFetch($query, NULL, 1);
	if($hubInfo['user_id']==$_SESSION['user_id']){
		//get theme info
		$query = "SELECT user_class2_fields, allow_subnavs FROM themes WHERE id = '".$hubInfo['theme']."'";
		$themeInfo = $hub->queryFetch($query, NULL, 1);
		
		if($page_id){
			//If editing a page
			$page = $hub->getSinglePage($page_id, 1);
			$dispForm = 'page';
		}
		else {
			//Get pages/nav sections
			$pages = $hub->getHubPage($hub_id, NULL, NULL, 1, 1, NULL, $nav_parent_id, 1);
			$dispForm = 'list';
			//check to see if this theme allows sub-navs
			$defaultNewType = 'page';
			$dispNewNavOption = true;
			if($nav_parent_id>0 && !$themeInfo['allow_subnavs']){
				$defaultNewType = 'page';
				$dispNewNavOption = false;
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$('#new-page-form').die();
	$('#new-page-form').live('submit', function(event){
		event.stopPropagation();
		var params = $(this).serialize();
		$.post('create-hub-page2.php', params, function(data){
			if(data.error===0){
				$('#new-title').attr('value', '');
				$('#wait').show();
				$.get('hub.php?form=pages2&id=<?=$hub_id?>&p=<?=$nav_parent_id?>', function(data2){
					$('#wait').hide();
					$('#ajax-load').html(data2);
				});
			}
			else {
				$('#errors').html(data.message);
			}
		}, 'json');
		return false;
	});
	
	$('.changeNewType').die();
	$('.changeNewType').live('click', function(){
		var type = $(this).attr('rel');
		$('#newType').attr('value', type);
		$('.changeNewType').css('font-weight', 'normal').removeClass('current');
		$(this).css('font-weight', 'bold').addClass('current');
		return false;
	});
	
	$('.moveField').die();
	$('.moveField').live('click', function(){
		var pageID = $(this).attr('rel');
		var move = $(this).attr('title');
		var currOrder = $(this).attr('name');
		var params = {'pageID': pageID, 'move': move, 'currOrder':currOrder, 'action':'changePageOrder'};
		<? if($hubInfo['multi_user']){ ?>
		params.multi_user = 1;
		<? } ?>
		$.post('inc/forms/add_hub_pages2.php', params, function(data){
			if(data.success==1){
				$('#wait').show();
				$.get('hub.php?form=pages2&id=<?=$hub_id?>&p=<?=$nav_parent_id?>', function(data2){
					$('#wait').hide();
					$('#ajax-load').html(data2);
				});
			}
			else {
				alert('Sorry, something went wrong trying to move this.  Please try again.'+data.message);
			}
		}, 'json');
		return false;
	});
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'		: '75%',
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "pages";
	$parentID = $_SESSION['current_id'];
	$hasPages = true;
	$pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<? if(!$page){ ?>
<? if(!$pages && !$nav_parent_id && $allowCreate){ ?>
<h2>Get started</h2>
<p>
	Welcome to the Hub page builder.<br />
	To get started, below you can either create a new navigation section (like <em>Services</em>)<br />
	or you can create a standalone page (like <em>About Us</em>).
</p>
<? } ?>
<?
if($hubInfo['multi_user'] || $hubInfo['hub_parent_id']){
	//if hub is a multi-user parent or child hub figure out whether to display the Create form or not
	if($hubInfo['multi_user']) $hubFieldsInfoData = $hubInfo['multi_user_fields'];
	else if($hubInfo['hub_parent_id']){
		$query = "SELECT multi_user_fields FROM hub WHERE id = '".$hubInfo['hub_parent_id']."' 
				  AND user_id = '".$_SESSION['parent_id']."'";
		$parentHubInfo = $hub->queryFetch($query, NULL, 1);
		$hubFieldsInfoData = $parentHubInfo['multi_user_fields'];
	}
	$hubFieldsInfo = json_decode($hubFieldsInfoData, true);
	if($hubFieldsInfo['allow_pages']['lock']) $allowCreate = false;
	else $allowCreate = true;
}
else $allowCreate = true;

// feed code
        $db =   Qube::GetPDO();
        $res    =    $db->query('SELECT ID, url FROM feeds WHERE user_ID = ' . (int)$_SESSION['user_id']);
        $default_feed   =   (int)$hubInfo['has_feedobjects'];

if($allowCreate || $hubInfo['multi_user']){
?>
<div id="createDir">
                    <?php if($isdev):
                        if($nav_parent_id == 0 && $res->rowCount()): 
                            ?><div><form class="jquery_form" name="hub" id="<?php echo $hub_id; ?>"><label>Page Feed(s): </label> <?php
                            while($feed =   $res->fetchObject()){
                                ?><label><input type="radio" <?php if($default_feed == $feed->ID) echo 'checked="checked" '; ?>name="has_feedobjects" value="<?php echo $feed->ID; ?>" /> <?php echo $feed->url; ?> <br/></label>
                                <?php
                            }
                            ?><label><input type="radio" name="has_feedobjects" value="0" <?php if($default_feed == 0) echo ' checked="checked"'; ?>/> Disabled. </label>
                                </form>
                            </div>
                            <?php
                        endif; ?><?php 
                    endif; ?>
	<form class="ajax bypass" id="new-page-form">
		<div id="errors"></div>
		<? if($dispNewNavOption){ ?>
		<label>Create a new:</label>
		<a href="#" class="dflt-link changeNewType current no-ajax" style="font-weight:bold;" rel="page">Page</a>
		&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="#" class="dflt-link changeNewType no-ajax" rel="nav">Nav Section</a>
		<br /><br />
		<? } else { ?>
		<label>Create a new page:</label>
		<? } ?>
		<input type="text" name="title" value="" id="new-title" />
		<input type="hidden" name="type" id="newType" value="<?=$defaultNewType?>" />
		<input type="hidden" name="hubID" value="<?=$hub_id?>" />
		<input type="hidden" name="nav_parent_id" id="nav_parent_id" value="<?=$nav_parent_id?>" />
		<? if($hubInfo['multi_user']){ ?>
		<input type="hidden" name="multi_user" value="1" />
		<? } ?>
		<input type="submit" value="Create" name="submit" />
	</form>
</div>
<? if($hubInfo['multi_user'] && !$nav_parent_id){ ?>
	<form name="hub" class="jquery_form" id="<?=$hub_id?>"><ul>
		<li>
			<label>Allow New Page Creation?</label>
			<select name="lock_allow_pages" title="select" class="uniformselect multiUser-<?=$hub_id?>n'">
				<option value="0" <? if($allowCreate) echo $sel; ?>>Yes</option>
				<option value="1" <? if(!$allowCreate) echo $sel; ?>>No</option>
			</select><div style="margin-top:-10px;"></div>
		</li>
	</ul></form>
<?
	} //end if($hubInfo['multi_user'])
} //end if($allowCreate || $hubInfo['multi_user'])
else {
?>
<p><strong>New page creation has been disabled for this hub.</strong></p><br />
<?
	}
}
?>
<?
	if($dispForm=='list'){
//////////////////////////////////////////////////////////////
// Edit Nav Section											//
//////////////////////////////////////////////////////////////
?>
<div id="message_cnt" style="display:none;"></div>
<div id="dash"><div id="fullpage_pages2" class="dash-container"><div class="container">
<?
	if(is_array($pages)){
		$link = 'hub.php?form=pages2&id='.$hub_id;
		//get the highest nav order num
		$highest = $hub->getMaxOrderNum($nav_parent_id, $hub_id);
		if($nav_parent_id && $nav_parent_id>0){
			//get parent parent id
			$query = "SELECT nav_parent_id, page_parent_id, page_title, multi_user_fields, default_page 
					  FROM hub_page2 WHERE id = '".$nav_parent_id."' LIMIT 1";
			$parent = $hub->queryFetch($query, NULL, 1);
			$img = '<img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Pages" />';
			
			echo '<a href="'.$link.'&p='.$parent['nav_parent_id'].'">'.$img.'</a><br />';
			
			//Multi-user stuff
			if($hubInfo['multi_user']) $fieldsInfoData = $parent['multi_user_fields'];
			else if($hubInfo['hub_parent_id']){
				//if page is part of a V2 MU Hub copy get parent page info
				$query = "SELECT multi_user_fields FROM hub_page2 WHERE id = '".$parent['page_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentPageInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentPageInfo['multi_user_fields'];
			}
			$lockedFields = array('');
			//if user is a reseller or admin editing a multi-user hub
			if($hubInfo['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin'])){
				//multi-user hub
				$multi_user = $multi_user2 = $_SESSION['multi_user_hub'] = 1;
				$addClass = ' muPage multiUser-'.$hub_id.'n';
			}
			else $_SESSION['multi_user_hub'] = $multi_user = NULL;
			//if user is a reseller/admin or client editing a multi-user hub
			if($hubInfo['hub_parent_id']) $multi_user2 = true;
			
			if($multi_user || $multi_user2){
				//figure out which fields to lock
				if($fieldsInfo = json_decode($fieldsInfoData, true)){
					foreach($fieldsInfo as $key=>$val){
						if($fieldsInfo[$key]['lock']==1)
							$lockedFields[] = $key;
					}
				}
			}
			
			function checkLock($field, $admin = ''){
				global $lockedFields, $multi_user2;
				//bypass for non-multi-user hubs
				if($multi_user2){
					//otherwise check if field is in lockedFields array
					if(in_array($field, $lockedFields)){
						if($admin){
							return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
						}
						else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
							return 'readonly'; //else if so and user is a client, lock field
						}
					}
				}
			}
?>
	<form name="hub_page2" class="jquery_form" id="<?=$nav_parent_id?>"><ul>
		<li style="margin-top:15px;">
			<label>Nav Section Title</label>
			<? if($multi_user){ ?>
			<select name="lock_page_title" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_title',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<input type="text" <?=checkLock('page_title')?> name="page_title" value="<?=$parent['page_title']?>" style="margin-left:0;font-size:24px;width:300px;" class="<?=$addClass?>" />
		</li>
		<? if($pages){ ?>
		<li>
			<label>Default Page</label>
			<? if($multi_user){ ?>
			<select name="lock_default_page" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('default_page',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="default_page" title="select" class="uniformselect<?=$addClass.' '.checkLock('default_page')?>">
				<option value="-1" <? if($parent['default_page']==-1) echo $sel; ?>>None</option>
			<?
			foreach($pages as $type=>$page){
				if($page['type']=='page'){
			?>
				<option value="<?=$page['id']?>" <? if($parent['default_page']==$page['id']) echo $sel; ?>><?=$page['page_title']?></option>
			<? }} ?>
			</select>
		</li>
		<? } ?>
	</ul></form>
<?
		}
		foreach($pages as $type=>$page){
			$typeImg = ($page['type']=='nav') ? 'nav-section' : 'page';
			$thisLink = $link.'&p=';
			if($page['type']=='page') $thisLink .= $page['nav_parent_id'].'&p2='.$page['id'];
			else $thisLink .= $page['id'];
			echo '<div class="item2" id="page'.$page['id'].'">
			<div class="icons" style="width:140px;">';
			if($page['nav_order']>0){
				echo
				'<a href="#" class="moveField" name="'.$page['nav_order'].'" title="Up" rel="'.$page['id'].'" style="margin:0 2px;padding:0px;display:inline;float:left;"><img src="img/up-arrow-icon.gif" alt="Move up" border="0" /></a>';
			}
			if($page['nav_order']<$highest){
				echo 
				'<a href="#" class="moveField" name="'.$page['nav_order'].'" title="Down" rel="'.$page['id'].'" style="margin:0 2px;padding:0px;display:inline;float:left;"><img src="img/down-arrow-icon.gif" alt="Move down" border="0" /></a>';
			}
			echo
				'<a href="'.$thisLink.'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete rmParent" rel="table=hub_page2&id='.$page['id'].'" onclick="javascript:$(this).parent().parent().fadeOut();"><span>Delete</span></a>
			</div>';
			echo '<img src="img/'.$typeImg.'.png" class="icon" />';
					
			echo '<a href="'.$thisLink.'">'.$page['page_title'].'</a></div>';
			
			//clear vars
			$typeImg = $thisLink = NULL;
		}
	}
?>
</div></div></div>
<?
} else if($dispForm=='page'){
//////////////////////////////////////////////////////////////
// Edit Single Page											//
//////////////////////////////////////////////////////////////
	$backLink = 'hub.php?form=pages2&id='.$hub_id.'&p='.$page['nav_parent_id'];
?>
<br /><a href="<?=$backLink?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Pages" /></a><br />
<h2><?=$page['page_title']?></h2>
<div id="form_test" style="width:100%;">
	<?php
	if($hubInfo['domain']){
		if($page['nav_parent_id']){
			//get nav parents for url
			$pageNavUrl = '';
			$getParentID = $page['nav_parent_id'];
			$done = false;
			while(!$done){
				$query = "SELECT nav_parent_id, page_title_url FROM hub_page2 WHERE id = '".$getParentID."'";
				if($result = $hub->queryFetch($query)){
					$pageNavUrl = $result['page_title_url'].'/'.$pageNavUrl;
					if($result['nav_parent_id']) $getParentID = $result['nav_parent_id'];
					else $done = true;
				}
				else $done = true;
			}
		}
		//display page's URL
		if(substr($hubInfo['domain'], -1) != "/") $hubInfo['domain'] .= "/"; //check/add trailing slash
		$pageURL = $hubInfo['domain'].$pageNavUrl.$page['page_title_url'].'/';
		echo '<span style="font-size:14px;color:#666;">Page URL:<br />';
		echo '<a href="'.$pageURL.'" target="_blank" class="view dflt-link"><i>'.$pageURL.'</i></a></span>';
	}
	//page image base url
	
	$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub_page/';
	
	//default 'no image uploaded' image
	$dflt_noImg = $_SESSION['thm_dflt-no-img'] ? $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'] : 'img/no-photo.jpg';
	//get all of this hub's nav section names/ids for dropdown
	$navs = array(0=>'Nav Root');
	$query = "SELECT id, page_title FROM hub_page2 WHERE hub_id = '".$hub_id."' 
			  AND user_id = '".$_SESSION['user_id']."' AND type='nav' ORDER BY id ASC";
	if($navresult = $hub->query($query)){
		while($navrow = $hub->fetchArray($navresult, 1)){
			$navs[$navrow['id']] = $navrow['page_title'];
		}
	}
	
	//Multi-user stuff
	if($hubInfo['multi_user']) $fieldsInfoData = $page['multi_user_fields'];
	else if($hubInfo['hub_parent_id']){
		//if page is part of a V2 MU Hub copy get parent page info
		$query = "SELECT multi_user_fields FROM hub_page2 WHERE id = '".$page['page_parent_id']."' 
				  AND user_id = '".$_SESSION['parent_id']."'";
		$parentPageInfo = $hub->queryFetch($query, NULL, 1);
		$fieldsInfoData = $parentPageInfo['multi_user_fields'];
	}
	$lockedFields = array('');
	//if user is a reseller or admin editing a multi-user hub
	if($hubInfo['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin'])){
		//multi-user hub
		$multi_user = $multi_user2 = $_SESSION['multi_user_hub'] = 1;
		$muHubID = $hubInfo['multi_user'] ? $hub_id : $hubInfo['theme'];
		$addClass = ' muPage multiUser-'.$muHubID.'n';
	}
	else $_SESSION['multi_user_hub'] = $multi_user = NULL;
	//if user is a reseller or client editing a multi-user hub
	if($hubInfo['hub_parent_id']) $multi_user2 = true;
	
	if($multi_user || $multi_user2){
		//figure out which fields to lock
		if($fieldsInfo = json_decode($fieldsInfoData, true)){
			foreach($fieldsInfo as $key=>$val){
				if($fieldsInfo[$key]['lock']==1)
					$lockedFields[] = $key;
			}
		}
	}
	
	function checkLock($field, $admin = ''){
		global $lockedFields, $multi_user2;
		//bypass for non-multi-user hubs
		if($multi_user2){
			//otherwise check if field is in lockedFields array
			if(in_array($field, $lockedFields)){
				if($admin){
					return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
				}
				else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
					return 'readonly'; //else if so and user is a client, lock field
				}
			}
		}
	}
	?>
	<br /><br />
	<form name="hub_page2" class="jquery_form" id="<?=$page['id']?>"><ul>
		<li>
			<label>Parent Nav Section</label>
			<? if($multi_user){ ?>
			<select name="lock_nav_parent_id" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('nav_parent_id',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="nav_parent_id" title="select" class="uniformselect<?=$addClass.' '.checkLock('nav_parent_id')?>">
			<? foreach($navs as $id=>$title){ ?>
				<option value="<?=$id?>" <? if($page['nav_parent_id']==$id) echo 'selected'; ?>><?=$title?></option>
			<? } ?>
			</select>
		</li>
		<li>
			<label>Title</label>
			<? if($multi_user){ ?>
			<select name="lock_page_title" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_title',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<input type="text" <?=checkLock('page_title')?> name="page_title" value="<?=$page['page_title']?>" class="<?=$addClass?>" />
		</li>
		<li>
			<label>SEO Title (optional)</label>
			<? if($multi_user){ ?>
			<select name="lock_page_seo_title" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_seo_title',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<input type="text" <?=checkLock('page_seo_title')?> name="page_seo_title" value="<?=$page['page_seo_title']?>" class="<?=$addClass?>" />
		</li>
		<li>
			<label>SEO Page Description</label>
			<? if($multi_user){ ?>
			<select name="lock_page_seo_desc" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_seo_desc',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<input type="text" <?=checkLock('page_seo_desc')?> name="page_seo_desc" value="<?=$page['page_seo_desc']?>" class="<?=$addClass?>" />
		</li>
		<li>
			<label>Keywords</label>
			<? if($multi_user){ ?>
			<select name="lock_page_keywords" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_keywords',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<div style="color:#888;font-size:12px; padding-bottom:5px;">Comma-separated. Example: <i>Austin Flooring, California Sun Rooms, ...</i> (up to six)</div>
			<input type="text" <?=checkLock('page_keywords')?> name="page_keywords" value="<?=$page['page_keywords']?>" class="<?=$addClass?>" />
		</li>
		<li>
			<label>Page Layout</label>
			<? if($multi_user){ ?>
			<select name="lock_page_region" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_region',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<? if(!checkLock('page_region')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
			<textarea name="page_region" <?=checkLock('page_region')?> rows="40" id="edtr1" style="width:97%" class="<?=$addClass?>"><?=htmlentities($page['page_region'])?></textarea>
		</li>
		<li>
			<label>Include Contact Form?</label>
			<? if($multi_user){ ?>
			<select name="lock_inc_contact" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('inc_contact',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="inc_contact" title="select" class="uniformselect<?=$addClass.' '.checkLock('inc_contact')?>">
				<option value="1" <? if($page['inc_contact']) echo $sel; ?>>Yes</option>
				<option value="0" <? if(!$page['inc_contact']) echo $sel; ?>>No</option>
			</select>
		</li>
		<li>
			<label>Include In Navigation Links?</label>
			<? if($multi_user){ ?>
			<select name="lock_inc_nav" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('inc_nav',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="inc_nav" title="select" class="uniformselect<?=$addClass.' '.checkLock('inc_contact')?>">
				<option value="1" <? if($page['inc_nav']) echo $sel; ?>>Yes</option>
				<option value="0" <? if(!$page['inc_nav']) echo $sel; ?>>No</option>
			</select>
		</li>
		<li>
			<label>Full Width Page?</label>
			<? if($multi_user){ ?>
			<select name="lock_page_full_width" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_full_width',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="page_full_width" title="select" class="uniformselect<?=$addClass.' '.checkLock('page_full_width')?>">
				<option value="1" <? if($page['page_full_width']) echo $sel; ?>>Yes</option>
				<option value="0" <? if(!$page['page_full_width']) echo $sel; ?>>No</option>
			</select>
		</li>
        <? if($hubInfo['theme'] == "120") {?>
        <li>
			<label>Page Level Slogan</label>
			<? if($multi_user){ ?>
			<select name="lock_page_edit_3" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_edit_3',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<textarea name="page_edit_3" <?=checkLock('page_edit_3')?> rows="10" class="<?=$addClass?>"><?=$page['page_edit_3']?></textarea>
			<br style="clear:left" />
		</li>
        <? } ?>
		<li>
			<label>Column Edit Region</label>
			<? if($multi_user){ ?>
			<select name="lock_page_edit_2" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_edit_2',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
            <? if(!checkLock('page_edit_2')){ ?><a href="#" id="2" class="tinymce"><span>Rich Text</span></a><? } ?>
			<textarea name="page_edit_2" <?=checkLock('page_edit_2')?> rows="10" id="edtr2" class="<?=$addClass?>"><?=$page['page_edit_2']?></textarea>
			<br style="clear:left" />
		</li>
		
	</ul></form>
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo</label>
		<? if($multi_user){ ?>
		<select name="lock_page_photo" title="select" class="uniformselect pagesLock imgLock<?=$addClass?>" rel="<?=$page_id?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('page_photo',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('page_photo')){ ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub_page2" />
		<input type="hidden" name="fname" value="page_photo" />
		<input type="hidden" name="img_id" value="<?=$page['id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
		<br style="clear:both;" />
	</form>
	<img id="img_one_prvw" class="img_cnt" src="<? if($page['page_photo']) echo $img_url.$page['page_photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	<form name="hub_page2" class="jquery_form" id="<?=$page['id']?>"><ul>
		<li>
			<label>Photo Description</label>
			<? if($multi_user){ ?>
			<select name="lock_page_photo_desc" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_photo_desc',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<div style="color:#888;font-size:12px; padding-bottom:5px;">Optional: Set the alt tag for the image on your page.</div>
			<input type="text" name="page_photo_desc" <?=checkLock('page_photo_desc')?> value="<?=$page['page_photo_desc']?>" class="<?=$addClass?>" />
		</li>
        <li>
			<label>Page Level Advanced CSS</label>
			<? if($multi_user){ ?>
			<select name="lock_page_adv_css" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('page_adv_css',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<textarea name="page_adv_css" <?=checkLock('page_adv_css')?> rows="10" class="<?=$addClass?>"><?=$page['page_adv_css']?></textarea>
			<br style="clear:left" />
		</li>
		<li>
			<label>Outbound Link</label>
			<? if($multi_user){ ?>
			<select name="lock_outbound_url" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('outbound_url',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<div style="color:#888;font-size:12px; padding-bottom:5px;">Optional: You can set this field to make this page link out to another URL when clicked.</div>
			<input type="text" name="outbound_url" <?=checkLock('outbound_url')?> value="<?=$page['outbound_url']?>" class="<?=$addClass?>" />
		</li>
		<li>
			<label>Outbound Link Target</label>
			<? if($multi_user){ ?>
			<select name="lock_outbound_target" title="select" class="uniformselect pagesLock<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('outbound_target',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="outbound_target" title="select" class="uniformselect<?=$addClass.' '.checkLock('outbound_target')?>">
				<option value="_blank" <? if($page['outbound_target']=='_blank') echo 'selected'; ?>>New Window/Tab</option>
				<option value="_self" <? if($page['outbound_target']=='_self') echo 'selected'; ?>>Same Window/Tab</option>
			</select>
		</li>
	</ul></form>
</div>
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } ?>
<br style="clear:left;" />
<?
	} //end if($hubInfo['user_id']==$_SESSION['user_id'])
} //end first else [no posted action]
?>