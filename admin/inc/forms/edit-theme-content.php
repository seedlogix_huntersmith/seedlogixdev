<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	//require_once(QUBEADMIN . 'inc/reseller.class.php');
	//create new instance of class
	//$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['reseller']){
		//list-bg-clr
		$a = explode('-', $_SESSION['thm_list-bg-clr']);
		$listBgClr1 = $a[0];
		$listBgClr2 = $a[1];
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = 'content';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" />
	<h1>Branding - Content:</h1>
	<h2>Text</h2>
	<form class="jquery_form">
	<label>Welcome Message</label>
	&nbsp;<input type="text" class="edit-theme-text no-upload" name="welcome-msg" value="<?=$_SESSION['thm_welcome-msg']?>" />
	<!<br />
	<label>Default Link Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="dflt-link-clr" value="<?=$_SESSION['thm_dflt-link-clr']?>" />
	<br />
	&nbsp;&nbsp;&nbsp;<a href="inc/forms/edit-theme-content.php" class="dflt-link">Sample default link</a>
	</form><br />
	<label>Campaigns Page - Custom Panel</label>
	<select class="edit-theme-select uniformselect" name="camp-pnl">
		<option value="1" <? if($_SESSION['thm_camp-pnl']==1) echo 'selected="yes"'; ?>>Enabled</option>
		<option value="0" <? if($_SESSION['thm_camp-pnl']==0) echo 'selected="yes"'; ?>>Disabled</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
	<label>Campaigns Page - Custom Panel Title</label>
	&nbsp;<input type="text" class="edit-theme-text no-upload" name="camp-pnl-title" value="<?=$_SESSION['thm_camp-pnl-title']?>" /><br /><br />
	<label>Campaigns Page - Custom Panel Content</label>
	<a href="#" id="1" class="tinymce"><span>Rich Text</span></a><textarea name="camp-pnl-html" rows="5" cols="50" id="edtr1" class="edit-theme-textarea no-upload"><?=stripslashes($_SESSION['thm_camp-pnl-html']);?></textarea>
	<br />
	<label>Campaigns Page - Panel Border Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="camp-pnl-brdr-clr" value="<?=$_SESSION['thm_camp-pnl-brdr-clr'];?>" /><br /><br />
	<div style="margin-left:15px;text-align:center;width:200px;height:50px;border:1px solid #<?=$_SESSION['thm_camp-pnl-brdr-clr']?>;"><br />Sample</div>
	</form><br />
	<label>Campaigns Page - Blog Section</label>
	<select class="edit-theme-select uniformselect" name="camp-blog-disp">
		<option value="1" <? if($_SESSION['thm_camp-blog-disp']==1) echo 'selected="yes"'; ?>>Enabled</option>
		<option value="0" <? if($_SESSION['thm_camp-blog-disp']==0) echo 'selected="yes"'; ?>>Disabled</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
	<label>Campaigns Page - Blog Section - Blog ID</label>
	&nbsp;<input type="text" class="edit-theme-text no-upload" name="camp-blog-id" value="<?=$_SESSION['thm_camp-blog-id']?>" /><br /><br />
	</form><br style="clear:both;" />
	
	<h2>Images</h2>
	<label>Buttons Color</label>
	<select class="edit-theme-select uniformselect" name="buttons-clr">
		<option value="black" <? if($_SESSION['thm_buttons-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_buttons-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($_SESSION['thm_buttons-clr']=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($_SESSION['thm_buttons-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_buttons-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_buttons-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_buttons-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
	</select>
	<img src="img/preview-button/<?=$_SESSION['thm_buttons-clr']?>.png" /><br />
	<label>Panel Title Bar Color</label>
	<select class="edit-theme-select uniformselect" name="titlebar-clr">
		<option value="black" <? if($_SESSION['thm_titlebar-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_titlebar-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($_SESSION['thm_titlebar-clr']=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($_SESSION['thm_titlebar-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_titlebar-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_titlebar-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_titlebar-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
	</select>
	<img src="img/right-panel-title-bg/<?=$_SESSION['thm_titlebar-clr']?>.png" /><br />
	<label>New Item Button Color</label>
	<select class="edit-theme-select uniformselect" name="newitem-clr">
		<option value="black" <? if($_SESSION['thm_newitem-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_newitem-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="green" <? if($_SESSION['thm_newitem-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_newitem-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_newitem-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_newitem-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($_SESSION['thm_newitem-clr']=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<img src="img/add-new/<?=$_SESSION['thm_newitem-clr']?>.gif" /><br />
	<label>Person Icon Color</label>
	<select class="edit-theme-select uniformselect" name="person-clr">
		<option value="black" <? if($_SESSION['thm_person-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_person-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($_SESSION['thm_person-clr']=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($_SESSION['thm_person-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_person-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_person-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_person-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
	</select>
	<img src="img/prospect-icon/<?=$_SESSION['thm_person-clr']?>.png" /><br />
	<label>Undo Bar Color</label>
	<select class="edit-theme-select uniformselect" name="undobar-clr">
		<option value="black" <? if($_SESSION['thm_undobar-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_undobar-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($_SESSION['thm_undobar-clr']=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($_SESSION['thm_undobar-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_undobar-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_undobar-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_undobar-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($_SESSION['thm_undobar-clr']=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<img src="img/undo-bar/<?=$_SESSION['thm_undobar-clr']?>.png" /><br /><br />
	<br />
	<form id="dflt-no-img" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
        <label>'No Photo Uploaded' Image | <span style="color:#666; font-size:75%">Displayed when a user hasn't uploaded a photo yet</span></label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="themeDfltNoImg" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="dflt-no-img_prvw" class="img_cnt" src="<? if($_SESSION['thm_dflt-no-img']) echo $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];  else echo 'img/no-photo.jpg'; ?>" style="max-width:300px;max-height:300px;" />
	<br /><br />
	<h2>Forms</h2>
	<label>Text Input Background Color</label>
	<select class="edit-theme-select uniformselect" name="form-input-clr">
		<option value="black" <? if($_SESSION['thm_form-input-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_form-input-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="green" <? if($_SESSION['thm_form-input-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="purple" <? if($_SESSION['thm_form-input-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_form-input-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($_SESSION['thm_form-input-clr']=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
	<label>Text Input Border Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="form-input-brdr-clr" value="<?=$_SESSION['thm_form-input-brdr-clr']?>" />
	<br />
	<label>Label Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="form-lbl-clr" value="<?=$_SESSION['thm_form-lbl-clr']?>" />
	<br />
	<label>Image Preview Border Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="form-img-brdr-clr" value="<?=$_SESSION['thm_form-img-brdr-clr']?>" />
	<br />
	<img class="img_cnt" src="<? if($_SESSION['thm_dflt-no-img']) echo $_SESSION['themedir'].$_SESSION['thm_dflt-no-img']; else echo 'img/no-photo.jpg'; ?>" style="width:200px;" /><br />
	</form><br />
	<label>Right Panel Links Bullet/Link Color</label>
	<select class="edit-theme-select uniformselect" name="app-right-link">
		<option value="black_7e7e7e" <? if($_SESSION['thm_app-right-link']=="black_7e7e7e") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue_6598ad" <? if($_SESSION['thm_app-right-link']=="blue_6598ad") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown_a47857" <? if($_SESSION['thm_app-right-link']=="brown_a47857") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green_6aae68" <? if($_SESSION['thm_app-right-link']=="green_6aae68") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange_e9a031" <? if($_SESSION['thm_app-right-link']=="orange_e9a031") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple_ac5d87" <? if($_SESSION['thm_app-right-link']=="purple_ac5d87") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red_d05048" <? if($_SESSION['thm_app-right-link']=="red_d05048") echo 'selected="yes"'; ?>>Red</option>
	</select>
	<label>Right Panel Links Bullet/Link Color - Hover</label>
	<select class="edit-theme-select uniformselect" name="app-right-link2">
		<option value="black_7e7e7e" <? if($_SESSION['thm_app-right-link2']=="black_7e7e7e") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue_6598ad" <? if($_SESSION['thm_app-right-link2']=="blue_6598ad") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown_a47857" <? if($_SESSION['thm_app-right-link2']=="brown_a47857") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green_6aae68" <? if($_SESSION['thm_app-right-link2']=="green_6aae68") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange_e9a031" <? if($_SESSION['thm_app-right-link2']=="orange_e9a031") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple_ac5d87" <? if($_SESSION['thm_app-right-link2']=="purple_ac5d87") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red_d05048" <? if($_SESSION['thm_app-right-link2']=="red_d05048") echo 'selected="yes"'; ?>>Red</option>
	</select>
	<div class="app_right_links">
		<ul>
			<li><a href="http://dfougndfgfdg.com">Sample Link</a></li>
		</ul>
	</div>
	<br />
	
	<h2>Lists</h2>
	<label>List Item Background Color</label>
	<select class="edit-theme-select uniformselect" name="list-bg-clr">
		<option value="black-<?=$listBgClr2?>" <? if($listBgClr1=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue-<?=$listBgClr2?>" <? if($listBgClr1=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown-<?=$listBgClr2?>" <? if($listBgClr1=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green-<?=$listBgClr2?>" <? if($listBgClr1=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange-<?=$listBgClr2?>" <? if($listBgClr1=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple-<?=$listBgClr2?>" <? if($listBgClr1=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red-<?=$listBgClr2?>" <? if($listBgClr1=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white-<?=$listBgClr2?>" <? if($listBgClr1=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<label style="margin-top:-10px;">List Item Background Color - Hover</label>
	<select class="edit-theme-select uniformselect" name="list-bg-clr">
		<option value="<?=$listBgClr1?>-black" <? if($listBgClr2=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="<?=$listBgClr1?>-blue" <? if($listBgClr2=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="<?=$listBgClr1?>-brown" <? if($listBgClr2=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="<?=$listBgClr1?>-green" <? if($listBgClr2=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="<?=$listBgClr1?>-orange" <? if($listBgClr2=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="<?=$listBgClr1?>-purple" <? if($listBgClr2=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="<?=$listBgClr1?>-red" <? if($listBgClr2=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="<?=$listBgClr1?>-white" <? if($listBgClr2=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
	<label>List Item Link Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="list-link-clr" value="<?=$_SESSION['thm_list-link-clr']?>" />
	</form>
	<div id="dash"><div class="dash-container"><div class="container">
	<div class="item">
		<div class="icons">
			<a href="inc/forms/edit-theme-content.php" class="view"><span>View</span></a>
			<a href="inc/forms/edit-theme-content.php" class="edit"><span>Edit</span></a>
			<a class="delete"><span>Delete</span></a>
		</div>
		<img src="http://6qube.com/users/105/graph_imgs/campaign_95_362.png" class="graph trigger" />
		<a href="inc/forms/edit-theme-content.php">Sample Item</a>
	</div>
	</div></div></div>
	<br /><br />
	
	<h2>Loading Box</h2>
	<label>Animation Color</label>
	<select class="edit-theme-select uniformselect" name="wait-anim-clr">
		<option value="black" <? if($_SESSION['thm_wait-anim-clr']=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($_SESSION['thm_wait-anim-clr']=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($_SESSION['thm_wait-anim-clr']=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($_SESSION['thm_wait-anim-clr']=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($_SESSION['thm_wait-anim-clr']=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($_SESSION['thm_wait-anim-clr']=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($_SESSION['thm_wait-anim-clr']=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($_SESSION['thm_wait-anim-clr']=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
	<label>Box Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="wait-box-clr" value="<?=$_SESSION['thm_wait-box-clr']?>" />
	<br />
	<label>Box Border Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="wait-brdr-clr" value="<?=$_SESSION['thm_wait-brdr-clr']?>" />
	<br />
	</form><br />
	<div style="width:300px;height:66px;background-color:#<?=$_SESSION['thm_wait-box-clr']?>;text-align:center;padding-top:34px;border:1px solid #<?=$_SESSION['thm_wait-brdr-clr']?>;"><img src="img/ajax-loader/<?=$_SESSION['thm_wait-anim-clr']?>.gif" alt="Loading..." /></div>
	<br /><br />
	<!-- hidden iframe -->
	<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>