<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	//require_once(QUBEADMIN . 'inc/reseller.class.php');
	//create new instance of class
	//$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['reseller']){
		//Left nav
		//nav-bg
		$a = explode('-', $_SESSION['thm_nav-bg']);
		$leftNavColor1 = $a[0];
		$leftNavColor2 = $a[1];
		//nav-bg2
		$a = explode('_', $_SESSION['thm_nav-bg2']);
		$leftNavColor3 = $a[0];
		$leftNavColor3_hex = $a[1];
		
		//Top nav
		//nav2-bg
		$topNavColor1 = $_SESSION['thm_nav2-bg'];
		$topNavColor2 = $_SESSION['thm_nav2-bg2'];
		
		//Inner navs
		$contentNavColor1 = $_SESSION['thm_nav3-bg'];
		$contentNavColor2 = $_SESSION['thm_nav3-bg2'];
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = 'navs';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" /><br />
	<h1>Branding - Navigation:</h1>
	
	
	<form class="jquery_form">
    <label>Left Navigation Bar Active Tab Color</label>
	<input type="text" class="edit-theme-text no-upload" name="left_nav_active" value="<?=$_SESSION['thm_left_nav_active']?>" />
	<br />
    <label>Left Navigation Active Tab Link Color</label>
	<input type="text" class="edit-theme-text no-upload" name="nav-links" value="<?=$_SESSION['thm_nav-links']?>" />
	<br />
    <label>Left Navigation Slide-down Color</label>
	<input type="text" class="edit-theme-text no-upload" name="left_nav_slide" value="<?=$_SESSION['thm_left_nav_slide']?>" />
	<br />
    <label>Left Navigation Slide-down Links Color</label>
	<input type="text" class="edit-theme-text no-upload" name="nav-links2" value="<?=$_SESSION['thm_nav-links2']?>" />
	<br />
	<label>Left Navigation Inactive Tab Link Color</label>
	<input type="text" class="edit-theme-text no-upload" name="nav-links3" value="<?=$_SESSION['thm_nav-links3']?>" />
	<br />
    <label>Left Navigation Shadow Color <span style="color:#666; font-size:75%">(leave blank for none)</span></label>
	<input type="text" class="edit-theme-text no-upload" name="nav-links4" value="<?=$_SESSION['thm_nav-links4']?>" />
	<br />
	</form><br />
	<br /><br />

<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>