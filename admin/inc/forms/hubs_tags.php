<small>
	<strong>Hint:</strong><br />
	&bull; If you plan on replicating your efforts for SEO, simply optimize your site with these tags and it will pull data from the fields in your HUB setttings automatically. <strong>((city))</strong>  <a href="#" class="dflt-link" id="showCustomPageTags">Click here to see available tags</a>.<br />
	<div id="availableTags" style="display:none;line-height:25px;">
    <br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((city))</strong> - City (<em>Austin Landscaping</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((state))</strong> - State (<em>TX Landscaping Services</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((address))</strong> - Address<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((zip))</strong>  - Zip Code<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((phone))</strong>  - Phone<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((domain))</strong>  - Domain<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword1))</strong>  - Keyword 1<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword2)</strong> ) - Keyword 2<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword3))</strong>  - Keyword 3<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword4))</strong>  - Keyword 4<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword5))</strong>  - Keyword 5<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((keyword6))</strong>  - Keyword 6<br />
	</div>
	
	<br />
    <?php if($thisPage == "pages"){ ?>
    &bull; These tags will not work in the main Page Title, you must use the SEO Title if you plan on using one of these tags in the site title.
    <? } ?>
    </small>
