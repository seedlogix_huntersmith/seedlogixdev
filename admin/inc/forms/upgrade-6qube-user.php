<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<h1>Upgrade Account</h1>
<p>
	Considering upgrading to a full account?  We have a wide range of account types and packages available to suit any business' needs.  Click on either of the links below to learn more and compare, then come back and make your upgrade selection from the dropdown
</p><br />
<h2>DIY 6Qube Elements Accounts</h2>
<p>
	<strong>Full access to the 6Qube Elements Organic Marketing Suite.</strong><br />
	For the DIY-types out there we offer a range of accounts with full access to the same cutting edge Organic Marketing Suite our experts use.  The 6Qube Elements system is designed for users of all skill levels and comes complete<br />with how-to videos.<br />
	<a href="http://elements.6qube.com/internet-marketing-tools-signup/" target="_blank" class="dflt-link">Click here to learn more about our DIY accounts</a><br /><br />
	<select name="upgrade_class" title="select" class="uniformselect" id="elements-account">
		<option value="">Please choose an account type...</option>
        <? if($_SESSION['login_uid']==4044){ ?>
        <option value="406">Legacy - $35/mo</option>
        <? } ?>
		<option value="179">Professional - $50/mo</option>
		<option value="180">Advanced &nbsp;&nbsp;&nbsp;&nbsp;- $99/mo</option>
		<option value="181">Corporate &nbsp;&nbsp;&nbsp;&nbsp;- $199/mo</option>
		<option value="182">Enterprise &nbsp;&nbsp;&nbsp;- $499/mo</option>
	</select>
	<a href="#" id="upgrade6QubeUser" class="dflt-link" rel="<?=$_SESSION['user_id']?>" title="elements-account"><img src="img/upgrade-button.png" alt="Upgrade now!" /></a>
</p>
<br />
<h2>Full Service Internet Marketing Packages By 6Qube</h2>
<p>
	<strong>Full access to the 6Qube Elements Organic Marketing Suite and a full, custom, targeted internet marketing campaign managed by our experts.</strong><br />
	Over the course of your initial 30 days our Search Engine Optimization experts will configure all your touch points including your custom website. Starting on day 60 each package comes with a 1st Page Placement on Google Guarantee! There are no contracts and you can cancel anytime.<br />
	<a href="http://6qube.com/local-internet-marketing-packages/" target="_blank" class="dflt-link">Click here to learn more about our full service packages</a><br /><br />
	<!--
	<select name="upgrade_class" title="select" class="uniformselect" id="marketing-package">
		<option value="">Please choose a package...</option>
		<option value="185">ProTrack</option>
		<option value="184">AdvTrack</option>
		<option value="183">FastTrack</option>
	</select>
	<a href="#" id="upgrade6QubeUser" class="dflt-link" rel="<?=$_SESSION['user_id']?>" title="marketing-package"><img src="img/upgrade-button.png" alt="Upgrade now!" /></a>
	-->
	<span style="color:#666;"><em>Temporarily unavailable, will be back shortly.</em></span>
</p>