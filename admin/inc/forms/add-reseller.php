<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	
	//authorize Access to page
	require_once('../auth.php');
	
	//initiate the class
	require_once('../reseller.class.php');
	$reseller = new Reseller();
	
	$company = $reseller->validateText($_POST['name']);
	
	$html = '
	<div id="form_test">
	<h1>Add new reseller</h1>
	<form action="reseller-functions.php" class="jquery_form new_account" method="post">
		<label>Email Address</label><input type="text" name="email" size="30" value="" class="no-upload" /><br />
		<div style="display:inline-block;margin-top:5px;">
		<label style="display:inline;">Password</label><br /><br />
		<input type="password" name="password" value="" style="width:214px;display:inline;" class="no-upload" />
		</div>&nbsp;
		<div style="display:inline-block;">
		<label style="display:inline;">Re-Enter Password</label><br /><br />
		<input type="password" name="password2" value="" style="width:213px;display:inline;" class="no-upload" />
		</div>
		<label>First Name</label><input type="text" name="first_name" value="" class="no-upload" /><br />
		<label>Last Name</label><input type="text" name="last_name" value="" class="no-upload" /><br />
		<label>Phone Number</label><input type="text" name="phone" value="" class="no-upload" /><br />
		<label>Company Name</label><input type="text" name="company" value="'.$company.'" class="no-upload" /><br />
		<label>Address</label><input type="text" name="address" value="" class="no-upload" /><br />
		<label>Address 2 - <small>(optional)</small> </label><input type="text" name="address2" value="" class="no-upload" /><br />
		<label>City</label><input type="text" name="city" value="" class="no-upload" /><br />
		<label>State</label><input type="text" name="state" size="2" maxlength="2" value="" class="no-upload" /><br />
		<label>Zip Code</label><input type="text" name="zip" size="5" maxlength="5" value="" class="no-upload" /><br /><br />
		<label>Payment Processor</label>
		<select name="payments" class="no-upload">
			<option value="6qube">6Qube</option>
			<option value="paypal">PayPal</option>
			<option value="authorize">Authorize.net</option>
		</select><br />
		<label>Active?</label>
		<select name="active" class="no-upload">
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
		<br style="clear:both" /><br />
		<input type="hidden" name="action" value="addNewReseller" />
		<input type="image" src="http://hubs.6qube.com/themes/elements/images/start-button.png" style="width:108px;height:33px;padding:0;background:none;border:none;" />
	</form>
	</div>
	<br style="clear:both" />';
	
	$response['success']['id'] = '1';
	$response['success']['container'] = '#ajax-load';
	$response['success']['action'] = "replace";
	$response['success']['message'] = $html;
	
	echo json_encode($response);
?>