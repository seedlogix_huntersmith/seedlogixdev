<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT 
				id, user_id, user_id_created, hub_parent_id, name, company_name, city, overview, offerings, photos, videos, events, edit_region_6, edit_region_7, edit_region_8, edit_region_9, edit_region_10, edit_region_11, edit_region_12, edit_region_13, edit_region_14, edit_region_15, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields 
			FROM hub WHERE id = '".$_SESSION['current_id']."'";
	$row = $hub->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
		//get field info from hub row for locks
		if($row['hub_parent_id'] || $row['multi_user']){
			if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
			else {
				//if hub is a V2 MU Hub copy get parent hub info
				$query = "SELECT multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentHubInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentHubInfo['multi_user_fields'];
			}
		}
		//get current theme info
		$query = "SELECT 
					user_class2_fields, pages, region1_title, region2_title, region3_title, region4_title, 
					region5_title, region6_title, region7_title, region8_title, region9_title, region10_title, 
					region11_title, region12_title, region13_title, region14_title, region15_title
				FROM themes WHERE id = '".$row['theme']."'";
		$themeInfo = $hub->queryFetch($query);
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if(($row['req_theme_type']==4 && $_SESSION['reseller']) ||
			($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
			$addClass = ' multiUser-'.$muHubID.'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		   ($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
			$multi_user2 = true;
			//figure out which fields to lock
			if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
			if($fieldsInfo = json_decode($fieldsInfoData, true)){
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
						return ' readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<script type="text/javascript">
<!--
// this tells jquery to run the function below once the DOM is read
$(document).ready(function() {

// choose text for the show/hide link
var showText="Show";
var hideText="Hide";

// append show/hide links to the element directly preceding the element with a class of "toggle"
$(".toggle").prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

// hide all of the elements with a class of 'toggle'
$('.toggle').hide();

// capture clicks on the toggle links
$('a.toggleLink').click(function() {

// change the link depending on whether the element is shown or hidden
if ($(this).html()==showText) {
$(this).html(hideText);
}
else {
$(this).html(showText);
}

// toggle the display
$(this).parent().next('.toggle').toggle('slow');

// return false so any link destination is not followed
return false;

});
});

//-->
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "edit";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - Edit Regions</h2>
    <?php
include('hubs_tags.php');
?>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
		<? 
		//$customs = array(1, 4, 5, 6, 7, 8, 21, 22, 23, 24, 27, 30, 32, 33, 34, 35, 36, 37, 38, 39, 42, 43, 44, 45, 46, 47, 48, 50, 54, 56);
		//if(in_array($row['theme'], $customs)){
		if($themeInfo['region1_title']){
		?>	
			<? //if($themeInfo['region1_title']){ ?>
            	<label><?=$themeInfo['region1_title']?></label>
				<li class="toggle" >
                  
					<? if($multi_user){ ?>
					<select name="lock_overview" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('overview',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('overview')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="overview" <?=checkLock('overview')?> rows="20" cols="60" id="edtr1" class="<?=$addClass?>"><?=$row['overview']?></textarea>
                 
				</li>
			<? //} ?>
			<? if($themeInfo['region2_title']){ ?>
            <label><?=$themeInfo['region2_title']?></label>
				<li class="toggle">
					
                 
					<? if($multi_user){ ?>
					<select name="lock_offerings" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('offerings',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('offerings')){ ?><a href="#" id="2" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="offerings" <?=checkLock('offerings')?> rows="20" cols="60" id="edtr2" class="<?=$addClass?>"><?=$row['offerings']?></textarea>
                 
				</li>
			<? } ?>
			<? if($themeInfo['region3_title']){ ?>
            <label><?=$themeInfo['region3_title']?></label>
				<li class="toggle">
					
                    
					<? if($multi_user){ ?>
					<select name="lock_photos" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('photos',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('photos')){ ?><a href="#" id="3" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="photos" <?=checkLock('photos')?> rows="20" cols="60" id="edtr3" class="<?=$addClass?>"><?=$row['photos']?></textarea>
                  
				</li>
			<? } ?>
			<? if($themeInfo['region4_title']){ ?>
            <label><?=$themeInfo['region4_title']?></label>
				<li class="toggle">
					
                 
					<? if($multi_user){ ?>
					<select name="lock_videos" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('videos',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('videos')){ ?><a href="#" id="4" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="videos" <?=checkLock('videos')?> rows="20" cols="60" id="edtr4" class="<?=$addClass?>"><?=$row['videos']?></textarea>
                 
				</li>
			<? } ?>
			<? if($themeInfo['region5_title']){ ?>
            <label><?=$themeInfo['region5_title']?></label>
				<li class="toggle">
					
               
					<? if($multi_user){ ?>
					<select name="lock_events" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('events',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('events')){ ?><a href="#" id="5" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="events" <?=checkLock('events')?> rows="20" cols="60" id="edtr5" class="<?=$addClass?>"><?=$row['events']?></textarea>
                  
				</li>
			<? } ?>
			<? if($themeInfo['region6_title']){ ?>
            <label><?=$themeInfo['region6_title']?></label>
				<li class="toggle">
					
                
					<? if($multi_user){ ?>
					<select name="lock_edit_region_6" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_6',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_6')){ ?><a href="#" id="6" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_6" <?=checkLock('edit_region_6')?> rows="20" cols="60" id="edtr6" class="<?=$addClass?>"><?=$row['edit_region_6']?></textarea>
				</li>
			<? } ?>
			<? if($themeInfo['region7_title']){ ?>
            <label><?=$themeInfo['region7_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_7" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_7',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_7')){ ?><a href="#" id="7" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_7" <?=checkLock('edit_region_7')?> rows="20" cols="60" id="edtr7" class="<?=$addClass?>"><?=$row['edit_region_7']?></textarea>
				</li>
			<? } ?>
			<? if($themeInfo['region8_title']){ ?>
            <label><?=$themeInfo['region8_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_8" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_8',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_8')){ ?><a href="#" id="8" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_8" <?=checkLock('edit_region_8')?> rows="20" cols="60" id="edtr8" class="<?=$addClass?>"><?=$row['edit_region_8']?></textarea>
				</li>
			<? } ?>
			<? if($themeInfo['region9_title']){ ?>
            <label><?=$themeInfo['region9_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_9" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_9',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_9')){ ?><a href="#" id="9" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_9" <?=checkLock('edit_region_9')?> rows="20" cols="60" id="edtr9" class="<?=$addClass?>"><?=$row['edit_region_9']?></textarea>
				</li>
			<? } ?>
			<? if($themeInfo['region10_title']){ ?>
            <label><?=$themeInfo['region10_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_10" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_10',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_10')){ ?><a href="#" id="10" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_10" <?=checkLock('edit_region_10')?> rows="20" cols="60" id="edtr10" class="<?=$addClass?>"><?=$row['edit_region_10']?></textarea>
				</li>
			<? } ?>
            <? if($themeInfo['region11_title']){ ?>
            <label><?=$themeInfo['region11_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_11" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_11',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_11')){ ?><a href="#" id="11" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_11" <?=checkLock('edit_region_11')?> rows="20" cols="60" id="edtr11" class="<?=$addClass?>"><?=$row['edit_region_11']?></textarea>
				</li>
			<? } ?>
            <? if($themeInfo['region12_title']){ ?>
            <label><?=$themeInfo['region12_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_12" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_12',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_12')){ ?><a href="#" id="12" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_12" <?=checkLock('edit_region_12')?> rows="20" cols="60" id="edtr12" class="<?=$addClass?>"><?=$row['edit_region_12']?></textarea>
				</li>
			<? } ?>
            <? if($themeInfo['region13_title']){ ?>
            <label><?=$themeInfo['region13_title']?></label>
				<li class="toggle">
					
					<? if($multi_user){ ?>
					<select name="lock_edit_region_13" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('edit_region_13',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<? if(!checkLock('edit_region_13')){ ?><a href="#" id="13" class="tinymce"><span>Rich Text</span></a><? } ?>
					<textarea name="edit_region_13" <?=checkLock('edit_region_13')?> rows="20" cols="60" id="edtr13" class="<?=$addClass?>"><?=$row['edit_region_13']?></textarea>
				</li>
			<? } ?>
            
           
           
		<? } else { ?>
			<li>
				<label>Edit Region #1</label>
				<? if($multi_user){ ?>
				<select name="lock_overview" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('overview',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('overview')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="overview" <?=checkLock('overview')?> rows="20" cols="60" id="edtr1" class="<?=$addClass?>"><?=$row['overview']?></textarea>
			</li>
			<li>
				<label>Edit Region #2</label>
				<? if($multi_user){ ?>
				<select name="lock_offerings" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('offerings',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('offerings')){ ?><a href="#" id="2" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="offerings" <?=checkLock('offerings')?> rows="20" cols="60" id="edtr2" class="<?=$addClass?>"><?=$row['offerings']?></textarea>
			</li>
			<li>
				<label>Edit Region #3</label>
				<? if($multi_user){ ?>
				<select name="lock_photos" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('photos',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('photos')){ ?><a href="#" id="3" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="photos" <?=checkLock('photos')?> rows="20" cols="60" id="edtr3" class="<?=$addClass?>"><?=$row['photos']?></textarea>
			</li>
			<li>
				<label>Edit Region #4</label>
				<? if($multi_user){ ?>
				<select name="lock_videos" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('videos',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('videos')){ ?><a href="#" id="4" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="videos" <?=checkLock('videos')?> rows="20" cols="60" id="edtr4" class="<?=$addClass?>"><?=$row['videos']?></textarea>
			</li>
			<li>
				<label>Edit Region #5</label>
				<? if($multi_user){ ?>
				<select name="lock_events" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('events',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('events')){ ?><a href="#" id="5" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="events" <?=checkLock('events')?> rows="20" cols="60" id="edtr5" class="<?=$addClass?>"><?=$row['events']?></textarea>
			</li>
		<? } ?>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Edit Regions Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#hubeditVideo" class="trainingvideo">Edit Regions Video</a>
			</li>
			<div style="display:none;">
				<div id="hubeditVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-edit-overview.flv" style="display:block;width:640px;height:480px" id="hubeditplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubeditplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<li>
				<a href="#addVideo" class="trainingvideo">How to Add Videos?</a>
			</li>
			<div style="display:none;">
				<div id="addVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/addvideo-overview.flv" style="display:block;width:640px;height:480px" id="addvideoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("addvideoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div><!--End APP Right Links-->
	<? } ?>
                               
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="external" target="_new">Common HTML Tags</a>
			</li>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new" >Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>									
</div><!--End APP Right Panel-->
<br style="clear:both;" />
<? } else echo "Error displaying page."; ?>