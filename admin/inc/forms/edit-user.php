<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
//create new instance of class
$reseller = new Reseller();
$id = $_GET['id'];
$id = is_numeric($id) ? $id : '';
$user = $reseller->getUsers(NULL, $id);
//get users able to manage
$query = "SELECT manage_user_id FROM users WHERE id = '".$user['id']."'";
$row = $reseller->queryFetch($query);
//get user_manage names
$allowed = explode('::', $row['manage_user_id']);
$usermanagers = array();
$query2 = "SELECT id, user_id, firstname, lastname FROM user_info WHERE parent_id = '".$_SESSION['login_uid']."' 
				AND user_manager = 1";
$b = $reseller->query($query2);
while($c = $reseller->fetchArray($b)){
	$usermanagers[$c['user_id']] = $c['firstname'];
}
if(($_SESSION['reseller'] && (($user['parent_id']==$_SESSION['user_id']) || ($user['id']==$_SESSION['login_uid']))) 
 || $_SESSION['admin']){
	if($user['super_user'] == 1){
		$isReseller = true;
		if($user['id']==$_SESSION['login_uid']){
			$isSelf = true;
		}
	}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file], input:checkbox").uniform();
	$('.AllowedUsersCheckbox').die();
	$('.AllowedUsersCheckbox').live('click', function(){
		var checked = $(this).attr('value');
		var id = $(this).parent().parent().parent().parent().parent().attr('id');
		var thisUser = $(this).attr('name');
		
		params = { 'action': 'updateAllowedUsers', 'checked': checked, 'id': id, 'users_access': thisUser};
		$.post('user-permissions-functions.php', params, function(data){
			if(data.action == 'errors'){
				alert(data.errors);
			}
		}, 'json');
	});
});
</script> 
    
<?
//section navigation
$thisPage = 'basics';
include('edit-user-nav.php');
?>
<br style="clear:left;" />
<br />
<h1>User #<?=$id?> Info: Basic</h1>
<form name="users" class="jquery_form" id="<?=$user['id']?>">
	<ul>
		<li>
			<label>Email:</label>
			<input type="text" name="username" value="<?=$user['username']?>" />
		</li>
		<li>
			<label>Password:</label>
			<input type="password" name="password" value="<?=$user['password']?>" />
		</li>
		<? if(!$isSelf){ ?>
		<li>
			<label>Access:</label>
			<select name="access" title="select" class="uniformselect">
				<option value="1" <? if($user['access']==1) echo 'selected="yes"'; ?>>Yes</option>
				<option value="0" <? if($user['access']==0) echo 'selected="yes"'; ?>>No</option>
			</select>
		</li>
		<li>
			<label>Canceled:</label>
			<select name="canceled" title="select" class="uniformselect">
				<option value="1" <? if($user['canceled']==1) echo 'selected="yes"'; ?>>Yes</option>
				<option value="0" <? if($user['canceled']==0) echo 'selected="yes"'; ?>>No</option>
			</select>
		</li>
		<?
		if($_SESSION['reseller']){ 
			$query = "SELECT name FROM user_class WHERE id = '".$user['class']."'";
			$productInfo = $reseller->queryFetch($query);
			
		?>
		<li>
			<label>Product</label>
			<?=$productInfo['name']?>
			<?
			if($reseller->isFreeAccount($user['class'])){
				echo '<br /><br />';
				echo $reseller->displayResellerProducts($_SESSION['login_uid'], 'dropdownWithOptions', NULL, true, 'upgradeClass');
				echo '<a href="#" class="upgrade-user" rel="'.$user['id'].'">';
				echo '<img src="img/upgrade-button';
				if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
				echo '.png" style="border:none;margin-top:-15px;" /></a>';
			}
			?>
		</li>
		<? } ?>
        
        
        <br />
        <li>
        	<label>Select User Managers</label>
       	 <? foreach($usermanagers as $key=>$value){ ?>
				<input type="checkbox" name="<?=$key?>" class="AllowedUsersCheckbox no-upload" <? if(in_array($key, $allowed)) echo 'checked'; ?> />&nbsp; <?=$value?><br /><br />
				<? } ?>
        </li>
       
        
		<? } ?>
		<br />
		<? if($_SESSION['admin'] && !$_SESSION['support_user']){ ?>
			<h2>6Qube Admin Options:</h2>
			<? if(!$isReseller){ ?>
			<li>
				<label>Parent ID:</label>
				<input type="text" name="parent_id" value="<?=$user['parent_id']?>" />
			</li>
			<? } else { ?>
			<li>
				<label>Client Access:</label>
				<select name="client_access" title="select" class="uniformselect">
					<option value="0" <? if($user['client_access']==0) echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($user['client_access']==1) echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
			<? } ?>
			<li>
				<label>Class:</label>
				<input type="text" name="class" value="<?=$user['class']?>" />
			</li>
			<li>
				<label>User Type:</label>
				<select name="super_user" title="select" class="uniformselect">
					<option value="0" <? if($user['super_user']==0) echo 'selected="yes"'; ?>>User</option>
					<option value="1" <? if($user['super_user']==1) echo 'selected="yes"'; ?>>Reseller</option>
					<option value="2" <? if($user['super_user']==2) echo 'selected="yes"'; ?>>6Qube Admin</option>
				</select>
			</li>
		<? } ?>
	</ul>
</form><br style="clear:both;" />
<? } else echo "Not authorized."; ?>