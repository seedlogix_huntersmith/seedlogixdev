<?php 
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	$curr = 'class="current"';
	if($thisPage!='global') $id = $parent_id;
?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="inc/forms/edit_whole_network.php?id=<?=$id?>" <? if($thisPage=="global") echo $curr; ?>>Global</a>
		</li>
		<li>
			<a href="inc/forms/edit_whole_network_local.php?id=<?=$id?>" <? if($thisPage=="local") echo $curr; ?>>Local</a>
		</li>
		<li>
			<a href="inc/forms/edit_whole_network_hubs.php?id=<?=$id?>" <? if($thisPage=="hubs") echo $curr; ?>>Hubs</a>
		</li>
		<li>
			<a href="inc/forms/edit_whole_network_blogs.php?id=<?=$id?>" <? if($thisPage=="blogs") echo $curr; ?>>Blogs</a>
		</li>
		<li>
			<a href="inc/forms/edit_whole_network_articles.php?id=<?=$id?>" <? if($thisPage=="articles") echo $curr; ?>>Articles</a>
		</li>
		<li>
			<a href="inc/forms/edit_whole_network_press.php?id=<?=$id?>" <? if($thisPage=="press") echo $curr; ?>>Press</a>
		</li>
		<li class="last">
			<a href="inc/forms/edit_whole_network_search.php?id=<?=$id?>" <? if($thisPage=="search") echo $curr; ?>>Search</a>
		</li>
		<!--
		<li class="last">
			<a href="inc/forms/edit_whole_network_browse.php?id=<?=$id?>" <? if($thisPage=="browse") echo $curr; ?>>Browse</a>
		</li>
		-->
	</ul>
</div>
<br style="clear:both;" /><br />