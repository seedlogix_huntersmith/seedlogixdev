<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, name, company_name, city, keyword_one, keyword_one_link, keyword_two, keyword_two_link, keyword_three, keyword_three_link, keyword_four, keyword_four_link, keyword_five, keyword_five_link, keyword_six, keyword_six_link FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$row = $directory->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript">
$(function(){
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>

<!-- FORM NAVIGATION -->
<?php
	$thisPage = "seo";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />


<div id="form_test">
	<h2><?=$row['name']?> - SEO</h2>
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Keyword #1</label>
				<input type="text" class="keyword-1" name="keyword_one" value="<?=$row['keyword_one']?>" />
			</li>
			<li>
				<label>Keyword #1 URL</label>
				<input type="text" name="keyword_one_link" value="<?=$row['keyword_one_link']?>" />
			</li>
			<li>
				<label>Keyword #2</label>
				<input type="text" name="keyword_two" value="<?=$row['keyword_two']?>" />
			</li>
			<li>
				<label>Keyword #2 URL</label>
				<input type="text" name="keyword_two_link" value="<?=$row['keyword_two_link']?>" />
			</li>
			<li>
				<label>Keyword #3</label>
				<input type="text" name="keyword_three" value="<?=$row['keyword_three']?>" />
			</li>
			<li>
				<label>Keyword #3 URL</label>
				<input type="text" name="keyword_three_link" value="<?=$row['keyword_three_link']?>" />
			</li>
			<li>
				<label>Keyword #4</label>
				<input type="text" name="keyword_four" value="<?=$row['keyword_four']?>" />
			</li>
			<li>
				<label>Keyword #4 URL</label>
				<input type="text" name="keyword_four_link" value="<?=$row['keyword_four_link']?>" />
			</li>
			<li>
				<label>Keyword #5</label>
				<input type="text" name="keyword_five" value="<?=$row['keyword_five']?>" />
			</li>
			<li>
				<label>Keyword #5 URL</label>
				<input type="text" name="keyword_five_link" value="<?=$row['keyword_five_link']?>" />
			</li>
			<li>
				<label>Keyword #6</label>
				<input type="text" name="keyword_six" value="<?=$row['keyword_six']?>" />
			</li>
			<li>
				<label>Keyword #6 URL</label>
				<input type="text" name="keyword_six_link" value="<?=$row['keyword_six_link']?>" />
			</li>
		</ul>
	</form>
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>SEO Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#dirseoVideo" class="trainingvideo">Search Engine Optimization Video</a>
			</li>
			<div style="display:none;">
				<div id="dirseoVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-seo-overview.flv" style="display:block;width:640px;height:480px" id="dirseoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("dirseoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="https://adwords.google.com/select/KeywordToolExternal" class="external" target="_new">Google Keyword Tool</a></li>
		</ul>
		<? if(!$_SESSION['theme']){ ?>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br/>
<? } else echo "Error displaying page."; ?>