<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
if($_GET) $id = $_GET['id'];
else $id = $_POST['id'];
$id = is_numeric($id) ? $id : '';
$row = $hub->getFormEmailers($_SESSION['user_id'], NULL, $id);
if($_POST['sendTest']){
	$to = $_POST['user_email'];
	$subject = str_replace("#name", "John Doe", $row['subject']);
	$subject = str_replace('#company', 'Company, Inc.', $subject);
	$message = stripslashes($row['body']);
	$message = str_replace("#name", "John Doe", $message);
	$message = str_replace("#company", "Company, Inc.", $message);
	$message .= "<br /><br />".$row['contact'];
	$message .= "<br />".$row['anti_spam'];
	
	$send = $hub->sendMail($to, $subject, $message, NULL, $row['from_field'], true, true);
	if($send['success']) echo '<script type="text/javascript">alert("Test email sent successfully to '.$to.'.");</script>';
	else echo '<script type="text/javascript">alert("Error sending test email: \n'.$send['errors'].'");</script>';
}
else if($_POST['sendNow'] || $_GET['getList']==1){
	//if reseller or admin is sending an instant email
	//OR they clicked the "see a list of people" link
	$isInclude = true;
	//determine the send settings to construct user queries
	//query ids
	$query = "SELECT id, lead_email FROM leads 
			WHERE lead_form_id = '".$row['lead_form_id']."' 
			AND optout != 1 AND lead_email != ''";
	if($b = $hub->query($query)){
		if($hub->numRows($b)){
			//set up user ids to pass to email_users script
			$leads = '';
			$leadsEmails = array();
			while($c = $hub->fetchArray($b)){
				if(!in_array($c['lead_email'], $leadsEmails)){
					$leads .= $c['id'].',';
					$leadsEmails[] = $c['lead_email'];
				}
			}
			//if($id==30) echo $leadID;
			if($_POST['sendNow']){
				//set up other email vars to pass
				$emailerID = $id;
				$emailSubject = $row['subject'];
				$emailBody = $row['body'];
				$emailBody .= '<br /><br />'.$row['contact'].'<br />'.$row['anti_spam'];
				$leadID = $leads;
				//FROM info
				$customFrom = $row['from_field'];
				//include script
				include_once(QUBEADMIN . 'email_leads.php');
			}
			else {
				//output a list of users who will be emailed
				$leadsArray = explode(',', $leads);
				$i = 0;
				if($leadsArray){
					$emails = array();
					foreach($leadsArray as $key=>$value){
						if($i<=50){
							//get email address
							$query = "SELECT lead_email FROM leads WHERE id = '".$value."'";
							$d = $hub->queryFetch($query);
							if($d['lead_email'] && !in_array(strtolower($d['lead_email']), $emails)){
								//list each email address
								echo $d['lead_email'].'<br />';
								$i++;
								$emails[] = strtolower($d['lead_email']); //prevent duplicate emails from being listed
							}
						}
						else $limitReached = true;
						$d = NULL;
					}
					if($row['sched_mode']=='date'){
						echo '<strong>Plus any future leads</strong><br />';
					}
					if($limitReached){
						echo '<strong>'.(count($leadsArray)-50).' more not shown...</strong>';
					}
				}
				else echo 
				'<script type="text/javascript">alert("No leads were found that matched your sending criteria.");</script>';
			}
		}
		//if no users were found for criteria:
		else echo '<script type="text/javascript">alert("No leads were found that matched your sending criteria.");</script>';
	}
	//if no users were found for criteria:
	else echo '<script type="text/javascript">alert("No leads were found that matched your sending criteria.");</script>';
}
else {
	if($row['user_id']==$_SESSION['user_id']){
		if($row['from_field']){
			$fromField = str_replace('<', '&lt;', $row['from_field']);
			$fromField = str_replace('>', '&gt;', $fromField);
			$fromField = str_replace('" ', '', $fromField);
			$fromField = str_replace('"', '', $fromField);
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
$('#sendtoEmailList').die();
$('#sendtoEmailList').live('click', function(){
	var emailerID = $(this).attr('rel');
	if($('#sendToEmailListDiv').css('display')=='none'){
		$.get('inc/forms/form_emailers_email.php?getList=1&id='+emailerID, function(data){
			$('#sendToEmailListDiv').html(data).slideDown();
			$('#sendtoEmailList').html('[-] Click here to hide list');
		});
	}
	else {
		$('#sendToEmailListDiv').slideUp();
		$('#sendtoEmailList').html('[+] Click here to show a list of leads who will be emailed');
	}
});
</script>
    
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/form_emailers_settings.php?id=<?=$id?>">Settings</a></li>
		<li><a href="inc/forms/form_emailers_email.php?id=<?=$id?>" class="current">Email</a></li>
		<li class="last"><a href="inc/forms/form_emailers.php?formID=<?=$row['lead_form_id']?>">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />
<br />
	<h1>Emailer Message</h1>
	<form name="lead_forms_emailers" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<li>
				<label>To:</label>
				<a href="#" id="sendtoEmailList" class="dflt-link" rel="<?=$id?>">[+] Click here to show a list of leads who will be emailed</a>
				<div id="sendToEmailListDiv" style="display:none;margin-top:5px;" name="0"></div>
			</li>
			<li>
				<label>From:</label>
				<span style="color:#666; font-size:75%"><strong>Must</strong> be in this format: <strong>Company Name &lt;email@company.com&gt;</strong></span><br />
				<input type="text" name="from_field" value="<?=$fromField?>" />
			</li>
			<li><label>Subject:</label><input type="text" name="subject" value="<?=$row['subject']?>" /></li>
			<li>
				<label>Message:</label>
				<a href="#" id="AR1" class="tinymce"><span>Rich Text</span></a>
				<? if($_SESSION['user_class']==5){ ?>
					<? if($templates && $local->numRows($templates)){
						echo '<select class="ar-templates no-upload">';
						echo '<option value="">Or choose a template...</option>';
						while($temprow = $local->fetchArray($templates)){
							echo '<option value="'.$temprow['id'].'">'.$temprow['theme_name'].'</option>';
						}
						echo '</select>';
					} ?>
				<? } ?>
				<textarea name="body" rows="15" cols="80" id="edtrAR1"><?=$row['body']?></textarea><br style="clear:left" />
			</li>
			<span style="font-size:9.5px;"><strong>Hint:</strong> You can use <strong>"#name"</strong> (without quotes) in the body or subject and it will automatically be<br />replaced with the prospect's name when the email is sent.</span><br />
			<br />
			<span style="color:#000; font-size:75%; font-weight:bold;">The following fields are required for the email to be sent, due to the CAN-SPAM act.</span><br />Feel free to customize them.<br />
			<li>
				<label>Business Name/Address:</label>
				<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="contact" rows="5" cols="80" id="edtr2"><?=stripslashes($row['contact'])?></textarea>
				<br style="clear:left;" />
			</li>
			<li>
				<label>Anti-Spam Message:</label>
				<textarea name="anti_spam" rows="3" cols="80" id="edtr3"><?=$row['anti_spam']?></textarea>
				<br style="clear:left;" />
			</li>
		</ul>
	</form><br />
     <div id="createDir">
	<form class="jquery_form" action="inc/forms/form_emailers_email.php" method="POST" target="hiddenIframe">
		<label>Send Test Email:</label>
		<span style="color:#666; font-size:75%">Enter your email below and click submit to be sent a test copy of this email.<br /></span><br />
		<input type="text" value="<?=$_SESSION['user']?>" name="user_email" style="width:350px;" />
		<input type="hidden" value="<?=$id?>" name="id" />
		<input type="submit" value="Send Test Email" name="sendTest"  />
	</form>
    </div>
	<? if($row['sched_mode']=='now' && $row['active']=='1'){ ?>
	<br /><br /><br />
	<form class="jquery_form" action="inc/forms/form_emailers_email.php" method="POST" target="hiddenIframe">
		<label>Send Email Now:</label>
		<span style="color:#666; font-size:75%"><b>We strongly recommend sending yourself a test copy before<br />broadcasting this message out to your leads.</b></span><br /><br />
		<input type="hidden" value="<?=$id?>" name="id" />
		<input type="hidden" value="1" name="sendNow" />
		<input type="submit" value="Launch Broadcast" name="sendNow"  onclick="javascript:$(this).slideUp();" />
	</form>
    <br /><br /><br />
	<? } ?>
	<br /><br style="clear:both;" />
	<iframe name="hiddenIframe" id="hiddenIframe" src="testemail.php" style="display:none;width:1px;height:1px;"></iframe>
	<? } else echo "Not authorized."; ?>
<? } ?>