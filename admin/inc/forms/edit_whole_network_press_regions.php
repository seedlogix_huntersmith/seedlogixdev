<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/local.class.php');
$local = new Local();
$parent_id = $_GET['id'];
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

//get site info
$query = "SELECT 
			id, admin_user, name, domain, edit_region_1, edit_region_2, edit_region_3, 
			edit_region_4, edit_region_5, edit_region_6, theme
		FROM resellers_network 
		WHERE parent = ".$parent_id." AND type = 'press' LIMIT 1";
$row = $local->queryFetch($query);

//prepare domain value for Google note below
$a = explode('.', $row['domain']);
$domain2 = $a[count($a)-2].'.'.$a[count($a)-1];

$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
$themeInfo = $local->queryFetch($query);
	
if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "press";
include('edit_whole_network_nav.php');
?>
<br style="clear:left;" />
<div id="form_test" >
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Press Edit Regions</h2>
	<br />
	<?php
include('edit_whole_network_tags.php');
?>
	
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<h2>Edit Regions:</h2>
			
			<li>
					<label><?=$themeInfo['region1_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_1"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_1']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region2_title']?></label>
					
					<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_2"  rows="20" cols="60" id="edtr2" ><?=$row['edit_region_2']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region3_title']?></label>
					
					<a href="#" id="3" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_3"  rows="20" cols="60" id="edtr3" ><?=$row['edit_region_3']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region4_title']?></label>
					
					<a href="#" id="4" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_4"  rows="20" cols="60" id="edtr4" ><?=$row['edit_region_4']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region5_title']?></label>
					
					<a href="#" id="5" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_5"  rows="20" cols="60" id="edtr5" ><?=$row['edit_region_5']?></textarea>
				</li>
                <li>
					<label><?=$themeInfo['region6_title']?></label>
					
					<a href="#" id="6" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_6"  rows="20" cols="60" id="edtr6" ><?=$row['edit_region_6']?></textarea>
				</li>
			
		</ul>
	</form><br />
	
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_press_nav.php');
?>
	
						
</div>
<!--End APP Right Panel-->

<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>