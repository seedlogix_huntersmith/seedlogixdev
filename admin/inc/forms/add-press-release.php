<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT * FROM press_release WHERE id = '".$_SESSION['current_id']."'";
	$row = $press->queryFetch($query);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();

	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>  

<div id="message-container"></div>
<div id="form_test">
	<h2>Editing:<br /><?=$press->shortenSummary($row['name'], 45);?></h2>
	<form class="jquery_form" name="press_release" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Include in network?</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Change this to "Yes" when you're ready to publish this press release to our network.</div>
				<select name="network" title="select" class="uniformselect">
					<option value="0" <? if(!$row['network']) echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['network']) echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
			<li>
				<label>Title</label>
				<input type="text" class="press-title" name="headline" value="<?=$row['headline']?>" />
			</li>
			<li>
				<label>Summary</label>
				<input type="text" name="summary" value="<?=$row['summary']?>" />
			</li>
			<li>
				<label>News body</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="body" rows="15" cols="80" id="edtr1"><?=$row['body']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Industry</label>
				<?php echo $press->displayCategories($row['category']); ?>
			</li>
			<!--<li>
				<label>Country</label>
				<select name="country" id="country" title="select" class="uniformselect">
					<option value="1">United States of America</option>
					<option value="2">Canada</option>
					<option value="3">United Kingdom of Great Britain &amp; N. Ireland</option>
				</select>
			</li>-->
			<li>
				<label>Keywords</label>
				<input type="text" name="keywords" value="<?=$row['keywords']?>" />
			</li>
			<li>
				<label>Website URL</label>
				<input type="text" name="website" value="<?=$row['website']?>" />
			</li>
			<li>
				<label>Company Name / Author</label>
				<input type="text" name="author" value="<?=$row['author']?>" />
			</li>
			<li>
				<label>Email</label>
				<input type="text" name="email" value="<?=$row['email']?>" />
			</li><br />
			<li>
				<label>About The Author, Postal Address, Phone Number, Fax, etc.</label>
				<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="contact" rows="5" cols="80" id="edtr2"><?=$row['contact']?></textarea>
				<br style="clear:left" />
			</li>
			<br />
			<small>City and State aren't shown on your press release but will help us categorize it.</small>
			<br />
			<li>
				<label>City</label>
				<input type="text" name="city" value="<?=$row['city']?>" />
			</li>
			<li>
				<label>State</label>
				<?=$press->stateSelect('state', $row['state'])?>
			</li>
		</ul>
	</form>
	
	<form id="logo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Business Logo</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="press_release" />
		<input type="hidden" name="fname" value="thumb_id" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['thumb_id']) echo 'http://'.$_SESSION['main_site'].'/users/'.$row['user_id'].'/press_release/'.$row['thumb_id'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
</div>
    
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Press Release Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#pressaddVideo" class="trainingvideo">Press Release Overview Video</a>
			</li>
			<div style="display:none;">
				<div id="pressaddVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/press-settings-overview.flv" style="display:block;width:640px;height:480px" id="pressaddplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("pressaddplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="helpLinks" href="http://press.6qube.com/press.php?id=<?=$_SESSION['current_id']?>&uid=<?=$row['user_id']?>"><img src="http://6qube.com/admin/img/preview-button.png" border="0" /></a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.free-press-release.com" class="external" target="_new">FreePressRelease.com</a>
			</li>
			<li>
				<a href="http://www.prlog.org/" class="external" target="_new">PRLog.com</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
			<a class="helpLinks" target="_blank" href="http://press.<?=$_SESSION['main_site']?>/press.php?id=<?=$_SESSION['current_id']?>&uid=<?=$row['user_id']?>"><img src="img/preview-button/<?=$_SESSION['thm_buttons-clr']?>.png" border="0" /></a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br/>

<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>