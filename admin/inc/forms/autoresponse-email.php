<?php
	session_start();
	define('QUBE_NOLAUNCH', 1);
         require_once( dirname(__FILE__) . '/../../../hybrid/bootstrap.php');
#         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/local.class.php');
	
	//create new instance of class
	$local = new Local();
	
	if($_GET) $id = $_GET['id'];
	else $id = $_POST['id'];
	
	$ar = $local->getAutoResponders(NULL, NULL, NULL, $id, 'email');
	$row = $local->fetchArray($ar);
	
	//get theme id
	$table = $row['table_id'];
	$query = "SELECT theme FROM hub WHERE id = '".$table."' LIMIT 1";	
	$theme = $local->queryFetch($query);	
	$themeID = $theme['theme'];
	
	//get theme type
	$query = "SELECT type FROM themes WHERE id = '".$themeID."' LIMIT 1";	
	$themeType = $local->queryFetch($query);	
		
	if($_POST['sendTest']){
            
            chdir(QUBEADMIN);
            $_GET['id']     =   $id;
            $_GET['ctrl']   =   'EmailTheme';
            $_GET['action'] =   'test';
            $to = $_POST['user_email'];
            require_once QUBEADMIN . 'classes/ResponseNotification.php';
            require_once QUBEADMIN . 'classes/LeadResponseNotification.php';
            require_once QUBEADMIN . 'controller.php';
            
            if(TRUE) echo '<script type="text/javascript">alert("Test email sent successfully to '.$to.'.");</script>';
            else echo '<script type="text/javascript">alert("Error sending test email");</script>';
                
            exit;
            
		$subject = str_replace("#name", "John Doe", $row['subject']);
		$message = stripslashes($row['body']);
		$message = str_replace("#name", "John Doe", $message);
		if($row['sched']>0){
			$message .= '<br style="clear: both;">
<hr>
<table border="0" cellspacing="0" width="100%" cellpadding="5">
  <tr>
    <td><font face="arial,verdana" size="1"> <a href="#">Click to view this email in a browser</a> <br/>
      <br/>
      If you no longer wish to receive these emails, please reply to this message with "Unsubscribe" in the subject line or simply click on the following link: <a href="http://6qube.com/?page=client-optout&email='.$to.'&pid=0" target="_blank">'.$row['optout_msg'].'</a> </font> </td>
  </tr>
</table>
<hr>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td><font face="arial,verdana" size="1"> '.$row['contact'].'
      <p> <a href="#" target="_blank">Read</a> 6Qube\'s marketing policy. </p>
      </font> </td>
    <td align="right"><a href="http://6qube.com/features/email-marketing/" target="_blank"> <img border="0" alt="Try Email Marketing with 6Qube" src="http://6qube.com/emails/images/try-now.png"> </a> </td>
  </tr>
</table>
<img alt="" src=""/>';
		}
		
		$send = $local->sendMail($to, $subject, $message, NULL, $row['from_field'], true, true);
		if($send['success']) echo '<script type="text/javascript">alert("Test email sent successfully to '.$to.'.");</script>';
		else echo '<script type="text/javascript">alert("Error sending test email: \n'.$send['errors'].'");</script>';
	}
	else {
		if($themeType['type']=="1"){ 
		$back_page = "inc/forms/website_autoresponder.php";
		$mode = "nav";
		}
		else if($themeType['type']=="2"){ 
		$back_page = "inc/forms/landing_autoresponder.php";
		$mode = "nav";
		}
		else if($themeType['type']=="5"){ 
		$back_page = "inc/forms/social_autoresponder.php";
		$mode = "nav";
		}
		else if($row['table_name']=="directory") $back_page = "directory-autoresponders.php";
		else if($row['table_name']=="press_release") $back_page = "press-autoresponders.php";
		else if($row['table_name']=="blogs"){ 
		$back_page = "inc/forms/blog_autoresponder.php";
		$mode = "nav";
		}
		else if($row['table_name']=="articles") $back_page = "articles-autoresponders.php";
		else if($row['table_name']=="lead_forms") $back_page = "inc/forms/forms2_autoresponder.php";
		if($row['user_id']==$_SESSION['user_id']){
			if($row['from_field']){
				$fromField = str_replace('<', '&lt;', $row['from_field']);
				$fromField = str_replace('>', '&gt;', $fromField);
				$fromField = str_replace('" ', '', $fromField);
				$fromField = str_replace('"', '', $fromField);
			}
?>
<script type="text/javascript">
$(function(){
	
	$(".uniformselect, input[type=file]").uniform();
	
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	
	$(".previewEmail").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	
});
</script>
    
<?php if($row['user_id'] == 6610): 
    echo '<pre>';
//        var_dump($row);
        echo '</pre>';
    ?>

<?php endif; ?>
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
           <li class="first"><a href="inc/forms/autoresponse-settings.php?id=<?=$id?>" >Settings</a></li>
        <li><a href="inc/forms/autoresponse-email.php?id=<?=$id?>" class="current">Email</a></li>
        <li><a href="inc/forms/autoresponse-theme.php?id=<?=$id?>">Theme</a></li>
        <li class="last"><a href="<?=$back_page?>?id=<?=$row['table_id']?>&mode=<?=$mode?>">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />
<br />
	<h1>Autoresponder Email</h1>
        <!--
        <div>We are currently working on improving this feature. We apologize for any inconvenience that may be cause during this process.
            Click <a href="javascript:;" onclick="$('#temporary_div').fadeIn();">here</a> to view the page anyway.</div>
        -->
        <div id="temporary_div">
    <form name="auto_responders" class="jquery_form" id="<?=$row['id']?>">
        <ul>
            <li><label>From:</label>
            <span style="color:#666; font-size:75%"><strong>Must</strong> be in this format: <strong>Company Name &lt;email@company.com&gt;</strong></span><br />
            <input type="text" name="from_field" value="<?=$fromField?>" /></li>
            <li><label>Subject:</label><input type="text" name="subject" value="<?=$row['subject']?>" /></li>
            <li>
            <?php if(!$row['theme']){ ?><label>Message:</label>
		  <a href="#" id="AR1" class="tinymce"><span>Rich Text</span></a>
          <? }else{ ?>
                  <label>Template Main Section</label>
                  <?php } ?>
          <a href="email-preview.php?id=<?=$id?>" class="greyButton helpLinks" style="float:right;margin-bottom:15px;" >Preview</a>
            
		  
		  <textarea name="body" rows="15" cols="80" id="edtrAR1"><?=$row['body']?></textarea><br style="clear:left" /></li>
            <span style="font-size:9.5px;">
				<strong>Hint:</strong> You can use "#name" (without quotes) in the body or subject and it will automatically be replaced with the prospect's name when the email is sent.
				<? if($row['table_name']=="lead_forms"){ ?><br />
				You can also use tags to embed the submitted values for your custom form fields.  Go to the custom form editor and click on a field to get its embed tag, which looks like <strong>[[form_field]]</strong>.
				<? } ?>
			</span><br />
            <? if($row['sched']==0) { ?>
            <li><label>Return URL:</label>
            <span style="color:#666; font-size:75%">Optional: Specify the URL a prospect is taken to after submitting the contact form</span><br />
            <input type="text" name="return_url" value="<?=$row['return_url']?>" /></li>
            <? } else { ?>
            <br /><span style="color:#000; font-size:75%; font-weight:bold;">The following fields are required for the email to be sent, due to the CAN-SPAM act.</span><br />Feel free to customize them.<br />
            <li><label>Business Name/Address:</label>
            <a href="#" id="2" class="tinymce"><span>Rich Text</span></a><textarea name="contact" rows="5" cols="80" id="edtr2"><?=stripslashes($row['contact'])?></textarea><br style="clear:left;" />
            <li><label>Anti-Spam Message:</label>
            <textarea name="anti_spam" rows="3" cols="80" id="edtr3"><?=$row['anti_spam']?></textarea><br style="clear:left;" /></li>
            <li><label>Opt-out Message:</label>
            <input type="text" name="optout_msg" value="<?=$row['optout_msg']?>" /></li>
            <? } ?>
        </ul>
    </form><br />
    <div id="createDir">
	<form class="jquery_form" action="inc/forms/autoresponse-email.php" method="POST" target="hiddenIframe">
    	<label>Send test email:</label>
        <span style="color:#666; font-size:75%">Enter your email below and click submit to be sent a test copy of this email.<br /><b>Only click the button once.</b></span><br /><br />
        
    	<input type="text" value="<?=$_SESSION['user']?>" name="user_email" style="width:350px;" />
	<input type="hidden" value="<?=$id?>" name="id" />
    <input type="submit" value="Send Test Email" name="sendTest"  />
    </form>
    </div>
    <iframe name="hiddenIframe" id="hiddenIframe" src="testemail.php" style="display:none;width:1px;height:1px;"></iframe>
</div>
	<? } else echo "Not authorized."; ?>
<? } ?>
