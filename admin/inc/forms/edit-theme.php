<?
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	//require_once(QUBEADMIN . 'inc/reseller.class.php');
	//create new instance of class
	//$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	$logoWidth = $_SESSION['thm_custom-backoffice'] ? '500w' : '565w';
	
	if($_SESSION['reseller']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = '';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" />
	<h1>Branding - Header/Body:</h1>
	<form id="theme_bgImg" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
    
        <label>Background Image</label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="themeBgImg" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="theme_bgImg_prvw" class="img_cnt" src="<? if($_SESSION['thm_bg-img']) echo $_SESSION['themedir'].$_SESSION['thm_bg-img'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	<label>Background Image Repeat</label>
	<select class="edit-theme-select uniformselect" name="bg-img-rpt">
		<option value="repeat-x" <? if($_SESSION['thm_bg-img-rpt']=='repeat-x') echo 'selected="yes"'; ?>>Horizonal</option>
		<option value="repeat-y" <? if($_SESSION['thm_bg-img-rpt']=='repeat-y') echo 'selected="yes"'; ?>>Vertical</option>
		<option value="repeat" <? if($_SESSION['thm_bg-img-rpt']=='repeat') echo 'selected="yes"'; ?>>Both</option>
		<option value="no-repeat" <? if($_SESSION['thm_bg-img-rpt']=='no-repeat') echo 'selected="yes"'; ?>>None</option>
	</select>
	<form class="jquery_form" style="margin-top:-10px;">
    <label>Custom CSS File</label>
	<input type="text" class="edit-theme-text no-upload" name="css_link" value="<?=$_SESSION['thm_css_link']?>" />
	<br />
	<label>Background Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="bg-clr" value="<?=$_SESSION['thm_bg-clr']?>" />
	<br /><br /><br />
	<label>User/Admin Bar Text Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="userbar-text" value="<?=$_SESSION['thm_userbar-text']?>" />
	<br />
	<label>User/Admin Bar Links Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="userbar-links" value="<?=$_SESSION['thm_userbar-links']?>" />
	<br />
	<label>Admin Bar Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="adminbar-clr" value="<?=$_SESSION['thm_adminbar-clr']?>" />
	<br />
	</form>
	<label>User Bar Color</label>
	<select class="edit-theme-select uniformselect" name="userbar-clr">
		<option value="black" <? if($_SESSION['thm_userbar-clr']=='black') echo 'selected="yes"'; ?>>Black</option>
		<option value="brown" <? if($_SESSION['thm_userbar-clr']=='brown') echo 'selected="yes"'; ?>>Brown</option>
		<option value="maroon" <? if($_SESSION['thm_userbar-clr']=='maroon') echo 'selected="yes"'; ?>>Maroon</option>
		<option value="navy" <? if($_SESSION['thm_userbar-clr']=='navy') echo 'selected="yes"'; ?>>Navy</option>
		<option value="olive" <? if($_SESSION['thm_userbar-clr']=='olive') echo 'selected="yes"'; ?>>Olive</option>
		<option value="plum" <? if($_SESSION['thm_userbar-clr']=='plum') echo 'selected="yes"'; ?>>Plum</option>
		<option value="rust" <? if($_SESSION['thm_userbar-clr']=='rust') echo 'selected="yes"'; ?>>Rust</option>
		<option value="white" <? if($_SESSION['thm_userbar-clr']=='white') echo 'selected="yes"'; ?>>White</option>
	</select>
	<form id="theme_logo" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
        <label>Logo | <span style="color:#666; font-size:75%">max <?=$logoWidth?> x 150h image in .png or .gif format</span></label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="themeLogo" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="theme_logo_prvw" class="img_cnt" src="<? if($_SESSION['thm_logo']) echo $_SESSION['themedir'].$_SESSION['thm_logo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	<br /><br />
	<form id="theme_favicon" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
        <label>Fav Icon | <span style="color:#666; font-size:75%">max 64w x 64h image in .png format</span></label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="themeFavicon" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="theme_favicon_prvw" class="img_cnt" src="<? if($_SESSION['thm_favicon']) echo $_SESSION['themedir'].$_SESSION['thm_favicon'];  else echo 'img/spacer.gif'; ?>" style="width:64px;height:64px;" />
	<br />
<br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>