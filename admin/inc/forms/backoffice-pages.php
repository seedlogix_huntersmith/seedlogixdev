<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

if($_SESSION['reseller']){
	$mode = isset($_GET['mode']) ? $_GET['mode'] : 'navs';
	$resellerID = $_SESSION['login_uid'];
	
	if($mode=='navs'){
		//get reseller's nav sections
		$query = "SELECT id, name FROM resellers_backoffice WHERE admin_user = '".$resellerID."' 
				AND type = 'nav' ORDER BY `order` ASC";
		if($navs = $reseller->query($query)) $numNavs = $reseller->numRows($navs);
?>
<h1>Custom Back Office Pages - <small>Nav Sections</small></h1>

<div id="createDir">
<form method="post" class="ajax" action="add-backoffice-nav.php">
	<input type="text" name="name" />
	<input type="submit" value="Create New Nav Section" name="submit" />
</form>
</div>

<div id="message_cnt"></div>

<div id="dash"><div id="fullpage_backoffice" class="dash-container"><div class="container">
<?
	if($numNavs){ 
		while($row = $reseller->fetchArray($navs)){
			echo '<div class="item">
			<div class="icons">
				<a href="backoffice-pages.php?mode=pages&n='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=resellers_backoffice&id='.$row['id'].'"><span>Delete</span></a>
			</div>
			<img src="http://'.$_SESSION['main_site'].'/admin/img/nav-section';
			if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
			echo	'.png" class="blogPost" />';
			
			echo '<a href="backoffice-pages.php?mode=pages&n='.$row['id'].'">'.$row['name'].'</a></div>';
		}
	}
	else {
		echo 'Use the form above to create a new custom back office nav section.';
	}
?>
</div></div></div>
<?
	} //end if($mode=='navs')
	else if($mode=='pages'){
		$navSection = $_GET['n'];
		$query = "SELECT id, name, order FROM resellers_backoffice WHERE type = 'page' 
				AND admin_user = '".$resellerID."' AND parent = '".$navSection."' ORDER BY `order` ASC";
		$pages = $reseller->query($query);
?>
<h1>Custom Back Office Pages - <small>Pages</small></h1>

<div id="createDir">
<form method="post" class="ajax" action="add-backoffice-page.php">
	<input type="text" name="name" />
	<input type="hidden" name="nav_section" value="<?=$navSection?>" />
	<input type="submit" value="Create New Page" name="submit" />
</form>
</div>

<div id="message_cnt"></div>

<div id="dash"><div id="fullpage_backoffice" class="dash-container"><div class="container">
<?
	if($numNavs){ 
		while($row = $reseller->fetchArray($pages)){
			echo '<div class="item">
			<div class="icons">
				<a href="inc/forms/edit-backoffice-page.php?id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=resellers_backoffice&id='.$row['id'].'"><span>Delete</span></a>
			</div>
			<img src="http://'.$_SESSION['main_site'].'/admin/img/page';
			if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
			echo	'.png" class="blogPost" />';
			
			echo '<a href="inc/forms/edit-backoffice-page.php?id='.$row['id'].'">'.$row['name'].'</a></div>';
		}
	}
	else {
		echo 'Use the form above to create a new custom back office page.';
	}
?>
</div></div></div>
<?
	} //end if($mode=='pages')
} //end if($_SESSION['reseller'])
?>