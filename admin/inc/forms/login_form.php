<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	isset($_SESSION['first_visit']) ? $_SESSION['first_visit'] = 0 : $_SESSION['first_visit'] = 1;
	
	require_once('inc/phpsniff/phpSniff.class.php');
	
	// initialize some vars
	$GET_VARS = isset($_GET) ? $_GET : $HTTP_GET_VARS;
	$POST_VARS = isset($_POST) ? $_GET : $HTTP_POST_VARS;
	if(!isset($GET_VARS['UA'])) $GET_VARS['UA'] = '';
	if(!isset($GET_VARS['cc'])) $GET_VARS['cc'] = '';
	if(!isset($GET_VARS['dl'])) $GET_VARS['dl'] = '';
	if(!isset($GET_VARS['am'])) $GET_VARS['am'] = '';
	$sniffer_settings = array('check_cookies'=>$GET_VARS['cc'],
						 'default_language'=>$GET_VARS['dl'],
						 'allow_masquerading'=>$GET_VARS['am']);
	$client = new phpSniff($GET_VARS['UA'],$sniffer_settings);
	
	$browser = $client->get_property('browser');
	
	if($browser=="ie") $browser = "Internet Explorer";
	else if($browser=="fx" || $browser=="mz") $browser = "Firefox";
	else if($browser=="sf"){
		if(strpos($client->get_property('ua'), "chrome")) $browser = "Chrome";
		else $browser = "Safari";
	}
	else if($browser=="op") $browser = "Opera";
	else $browser = "Unknown";
	
	//if($browser!="Firefox" && $_SESSION['first_visit']==1){
		//$displayAlert = true;
	//}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head profile="http://gmpg.org/xfn/11">
<title>Login to 6Qube</title>
<meta content="Login to 6Qube's internet marketing software." name="description" />
<meta name="google-site-verification" content="Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<!-- End Load -->
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript" src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	

	function megaHoverOver(){
		$(this).find(".sub").stop().fadeTo('fast', 1).show();
			
		//Calculate width of all ul's
		(function($) { 
			jQuery.fn.calcSubWidth = function() {
				rowWidth = 0;
				//Calculate row
				$(this).find("ul").each(function() {					
					rowWidth += $(this).width(); 
				});	
			};
		})(jQuery); 
		
		if ( $(this).find(".row").length > 0 ) { //If row exists...
			var biggestRow = 0;	
			//Calculate each row
			$(this).find(".row").each(function() {							   
				$(this).calcSubWidth();
				//Find biggest row
				if(rowWidth > biggestRow) {
					biggestRow = rowWidth;
				}
			});
			//Set width
			$(this).find(".sub").css({'width' :biggestRow});
			$(this).find(".row:last").css({'margin':'0'});
			
		} else { //If row does not exist...
			
			$(this).calcSubWidth();
			//Set Width
			$(this).find(".sub").css({'width' : rowWidth});
			
		}
	}
	
	function megaHoverOut(){ 
	  $(this).find(".sub").stop().fadeTo('fast', 0, function() {
		  $(this).hide(); 
	  });
	}


	var config = {    
		 sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)    
		 interval: 100, // number = milliseconds for onMouseOver polling interval    
		 over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
		 timeout: 500, // number = milliseconds delay before onMouseOut    
		 out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
	};

	$("ul#topnav li .sub").css({'opacity':'0'});
	$("ul#topnav li").hoverIntent(config);



});

</script>

<!-- for IE6 i'm sorry but there is too much wrong with it, needs warning at least, you can disable it by delething this load. -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.badBrowser.js" type="text/javascript" charset="utf-8"></script>
<!-- End Load -->
<!-- ALL jQuery Tools. jQuery library -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.tools.js" type="text/javascript" charset="utf-8"></script>
<!-- End Load -->
<!-- Load Jquery Easing -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.easing.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.css-transform.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.css-rotate-scale.js" type="text/javascript"></script>
<!-- End Load -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/cufon.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/fonts/arials.font.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/fonts/zurich.font.js" type="text/javascript"></script>
<!-- Load Pretty Photo -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.prettyPhoto.css" type="text/css" media="screen" />
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<!-- End Load -->
<!-- Load SWFObject, used for video embedding -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/swfobject.js" type="text/javascript" charset="utf-8"></script>
<!-- End Load -->
<!-- Load Captify -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.captify.js" type="text/javascript"></script>
<!-- End Load -->
<!-- Load Bubble Tip -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.bubbletip.js" type="text/javascript"></script>
<link href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.bubbletip.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
	    <link href="http://6qube.com/hubs/themes/6qube/_include/css/jquery.bubbletip-ie.css" rel="stylesheet" type="text/css" />
	<![endif]-->
<!-- End Load -->
<!-- Load Roundies -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/browserdetect.js" type="text/javascript"></script>
<script src="http://6qube.com/hubs/themes/6qube/_include/js/roundies.js" type="text/javascript"></script>
<!-- End Load -->
<!-- Load Jquery Twitter -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/jquery.tweet.js" type="text/javascript"></script>
<!-- End Load -->
<!-- Load some small custom scripts -->
<script src="http://6qube.com/hubs/themes/6qube/_include/js/custom.js" type="text/javascript"></script>
<!-- End Load -->
<!-- Load Main Stylesheets and Default Color and Style -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style-6qube.css" type="text/css" media="screen" title="orange"/>
<!-- Load Main Enhancements Stylesheet border radius, transparency, shadows and such things -->
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/style-enhance.css" type="text/css" media="screen" />
<!-- End Load -->
<!-- Load IE6 Stylesheet -->
<!--[if IE 6]>
        <link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/ie6.css" type="text/css" media="screen" />
    <![endif]-->
<!-- End Load -->
<!-- Load IE7 Stylesheet -->
<!--[if IE 7]>
        <link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/_include/css/ie7.css" type="text/css" media="screen" />
    <![endif]-->
<!-- End Load -->
<!-- Load PNG Fix older IE Versions -->
<!--[if lt IE 7]>
        <script type="text/javascript" src="http://6qube.com/hubs/themes/6qube/_include/js/pngfix.js"></script>
        <script type="text/javascript">DD_belatedPNG.fix('*');</script>
    <![endif]-->
<!-- End Load -->
<!-- start load cycle slideshow -->
<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$('#name').focus();
	$('#login-box h5').click( function() {
		$('#login').hide();
		content = $('#forgot-password').html();
		$('#login').replaceWith(content);
		
	});
	<? if($displayAlert){ ?>
	<!--$.fancybox({ width: 660, height:450,
		'autoScale'		: true,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe',
		'content'           : 'check_browser.php?browser=<?=$browser?>'
	});-->
	<? } ?>
	//close $(document).ready(function()
});
</script>
<style type="text/css">
.firefox { width:221px; float:left; text-align:center; margin-left:115px; }
.social { padding:0 15px 0 0; float:left; }
.social img { float:left; }

</style>
<link rel="stylesheet" href="http://6qube.com/hubs/themes/6qube/css/custom_form.css" />
<!-- Place this tag in the <head> of your document -->
<link href="https://plus.google.com/107689484959897238109" rel="publisher" />
</head>
<body>
<!-- start top -->
<div class="top">
  <div class="full-width rel">
    <div class="one-third">
      <div class="logoBox"><a href="http://6qube.com" title="Inbound Marketing | Marketing Automation | 6Qube" ><img src="https://6qube.com/hubs/themes/6qube/_include/images/6qube_cube.png" /></a></div>
      <div class="logo"></div>

    </div>
    <div class="two-third">
      <div class="rel">
    
        <div class="abs utility">
        
          <div class="icon-to-left"><img src="http://6qube.com/hubs/themes/6qube/_include/images/icons/smallphone.png" alt="" /></div>
          Give us a Call: <strong>1-877-570-5005</strong> or <a href="/contact/">E-mail Us</a> Directly. 
          <!-- Place this tag where you want the +1 button to render -->




          <a href="http://facebook.com/6qube"><img src="http://6qube.com/hubs/themes/6qube/_include/images/facebook.png" alt="Follow Us On Facebook"  /></a> 
          <a href="http://twitter.com/6qube"><img src="http://6qube.com/hubs/themes/6qube/_include/images/twitter.png" alt="Follow Us On Twitter"  /></a>
          <!-- Place this tag where you want the badge to render -->
<a href="https://plus.google.com/107689484959897238109?prsrc=3" style="text-decoration:none;"><img src="https://ssl.gstatic.com/images/icons/gplus-32.png" alt="" style="border:0;width:20px;height:20px;margin:2px 2px 0 0;"/></a>
           <a href="http://login.6qube.com"><img src="http://6qube.com/hubs/themes/6qube/_include/images/login.png" alt="Login to 6Qube" /></a>
          
          <div class="clear"></div>
        </div>
      </div>
      <div class="mainmenu">
        <ul id="topnav">
          <li><a href="/"><span>Home</span></a> </li>
          <li><a href="/solutions/marketing-automation-software/" title="Solutions"><span>Solutions</span></a>
            <div class="sub">
              <ul>
                <li><a href="/solutions/marketing-automation-software/" title="Marketing Automation Software">Marketing Automation Software</span></a></li>
                <li><a href="/solutions/inbound-marketing-software/" title="Inbound Marketing Software">Inbound Marketing Software</span></a></li>
                <li><a href="/solutions/internet-marketing-services/" title="Internet Marketing Services">Internet Marketing Services</span></a></li>
                <li><a href="/solutions/social-media-solutions/" title="Social Media Solutions">Social Media Solutions</span></a></li>
                <li><a href="/solutions/multiple-website-management/" title="Multiple Website Management">Multiple Website Management</span></a></li>
                <li><a href="/solutions/marketing-service-providers/" title="Marketing Service Providers">Marketing Service Providers</span></a></li>
              </ul>
            </div>
          </li>
          <li><a href="/services/local-internet-marketing/" title="Services"><span>Services</span></a>
            <div class="sub">
              <ul>
                <li><a href="/services/local-internet-marketing/" title="Local Internet Marketing">Local Internet Marketing</span></a></li>
                <li><a href="/services/national-internet-marketing/" title="National Internet Marketing">National Internet Marketing</span></a></li>
                <li><a href="/services/search-engine-optimization/" title="Search Engine Optimization">Search Engine Optimization</span></a></li>
                <li><a href="/services/pay-per-click-management/" title="Pay Per Click Management">Pay Per Click Management</span></a></li>
                <li><a href="/services/social-media-marketing/" title="Social Media Marketing">Social Media Marketing</span></a></li>
                <li><a href="/services/ecommerce-marketing/" title="eCommerce Marketing">eCommerce Marketing</span></a></li>
                <li><a href="/services/seo-website-design/" title="SEO Website Design">SEO Website Design</span></a></li>
                <li><a href="/services/seo-website-copy/" title="SEO Website Copy">SEO Website Copy</span></a></li>
              </ul>
            </div>
          </li>
          <li><a href="#" title="Features"><span>Features</span></a>
            <div class="sub">
              <ul>
                <li><a href="/features/content-management-system/" title="Content Management System">Content Management System</span></a></li>
                <li><a href="/features/business-blogging-software/" title="Business Blogging Software">Business Blogging Software</span></a></li>
                <li><a href="/features/landing-page-software/" title="Landing Page Software">Landing Page Software</span></a></li>
                <li><a href="/features/microsites/" title="Microsites">Microsites</span></a></li>
                <li><a href="/features/seo-platform/" title="SEO Platform">SEO Platform</span></a></li>
                <li><a href="/features/advanced-analytics/" title="Advanced Analytics">Advanced Analytics</span></a></li>
                <li><a href="/features/custom-form-builder/" title="Custom Form Builder">Custom Form Builder</span></a></li>
                <li><a href="/features/email-marketing/" title="Email Marketing">Email Marketing</span></a></li>
                <li><a href="/features/prospect-management/" title="Prospect Management">Prospect Management</span></a></li>
                <li><a href="/features/facebook-page-builder/" title="Facebook Page Builder">Facebook Page Builder</span></a></li>
                <li><a href="/features/call-tracking/" title="Call Tracking">Call Tracking</span></a></li>
                <li><a href="/features/website-replication-software/" title="Website Replication Software">Website Replication Software</span></a></li>
                <li><a href="/features/domain-management/" title="Domain Management">Domain Management</span></a></li>
                <li><a href="/features/multi-site-cms/" title="Multi Site CMS">Multi Site CMS</span></a></li>
                <li><a href="/features/multi-site-seo/" title="Multi Site SEO">Multi Site SEO</span></a></li>
              </ul>
            </div>
          </li>
          <li><a href="/pricing/inbound-marketing-software/" title="Pricing"><span>Pricing</span></a>
            <div class="sub">
              <ul>
                <li><a href="/pricing/inbound-marketing-software/" title="Inbound Marketing Software">Inbound Marketing Software</span></a></li>
                <li><a href="/pricing/marketing-automation-software/" title="Marketing Automation Software">Marketing Automation Software</span></a></li>
              </ul>
            </div>
          </li>
          <li ><a href="/free-trial/"><span>Free Trial</span></a> </li>
        </ul>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<!-- end top -->
<!-- start middle -->
<div class="middle">
<br />
    <!-- start header -->
    <div class="header">
      <!-- start slideshow -->
      <div id="slideshow">
      
        <!-- start slide -->
        <div class="slide header-image-right gradient-down-color">
          <div class="full-width">
            <div class="one-half pt10">
              <h1>Internet Marketing Software</h1>
              <h3 class="mb20">Managing Your Online Presence In One Place</h3>
              <div class="faded-to-right">
                <!--  -->
              </div>
              <div class="icon-to-left"><img src="http://6qube.com/hubs/themes/6qube/_include/images/control.png" width="64" height="64" alt="Complete Online Control" /></div>
              <h4>Complete Online Control</h4>
              <p class="limit">Website CMS, Blogging Platform, Micro-site Builder, Landing Page Creator and Social Media Marketing Systems all managed and built inside the same account.</p>
              <div class="faded-to-right">
                <!--  -->
              </div>
              <div class="icon-to-left"><img src="http://6qube.com/hubs/themes/6qube/_include/images/icons/tools.png" width="64" height="64" alt="Innovative Marketing Tools" /></div>
              <h4>Innovative Marketing Tools</h4>
              <p class="limit">Custom Lead Form Builder, Search Engine Optimization Engine, Email Marketing Platform, Website Replication Manager, Call Tracking & Recording, and much more.</p>
              <div class="faded-to-right">
                <!--  -->
              </div>
              <div class="action-buttons clearfix"> <a href="/pricing/inbound-marketing-software/" title="Inbound Marketing Software" class="btn-big-action-fixed"><span><img src="http://6qube.com/hubs/themes/6qube/_include/images/icons/button-arrow.png" width="30" height="30" /> Start Free Trial</span></a> <a href="/solutions/inbound-marketing-software/" title="Inbound Marketing Software" class="btn-big-neutral-fixed"><span>Learn More</span></a> </div>
              <div class="clear"></div>
            </div>
            <div class="one-half pt20">
            <?
//Messages TO DISPLAY
if(isset($_GET['action'])){
	switch($_GET['action']){
		case 'password': $msg = 'Your password was sent to your email address.'; break;
		case 'password-error': $msg = "We couldn't send your password to your email address.<br />Please contact 6Qube."; break;
		case 'username-error': $msg = "We couldn't find your username in our system."; break;
		case 'admin-account': $msg = "For security reasons we can't email password<br />reset instructions to admins.
								<br />Please contact support."; break;
	}
}
if(isset($_GET['error_code'])){
	switch ($_GET['error_code']):
		case 1: $msg = 'Your Account has not been activated.<br />Please check your email for the activation link.'; break;
		case 2: $msg = 'Username or password were incorrect.'; break;
		case 4: $msg = "Access to your account has been disabled."; break;
		case 'x': 
		$msg = "For your protection we've disabled access<br />to your account.<br />
			   Please contact us to verify your information<br />and have it re-enabled.<br />";
		$msg .= '<br />Email: support@6qube.com';
		$msg .= '<br />Phone: 877-570-5005';
		break;
	endswitch;
}
$errorDiv = $msg;		
?>
<br />
            <h2>Login to 6Qube</h2>
                    <div class="outer-rounded-box-bold">
                        <div class="simple-rounded-box">
						<div id="login-box">
						<div id="login" style="margin-top:-10px;">
						<? if($_GET['error_code']!='x'){ ?>
            
							<div id="error" style="color:black;font-size:12px;font-weight:bold;"><?=$errorDiv?></div>
							<form action="validate_login.php" method="post" name="login" class="custom-form" >
                            <ul>
                  				<li>
								<label>Email Address:</label>
								<input class="input1" type="text" name="username" id="name" style="width:370px;" /></li>
                                <li>
								<label>Password:</label>
								<input class="input1" type="password" name="password" style="width:370px;" /></li>
								<!-- send current page for redirect back -->
								<input type="hidden" name="page" value="./" />
								<input type="hidden" name="browser" value="<?=$browser?>" />
								<!-- submit button -->
                                <br />
                                 <li class="submit-li" style="float:left;">
                                <input type="submit" value="Login to 6Qube" id="submit" />
                                  </li>
                                  
                                  <h5 style="cursor:pointer; float:right; width:200px;">Forgot your password?</h5>
                                  
                                </ul>
                                
                                
                                
                            <div class="clear"></div>
								
								
							</form>
						<? } else { ?>
						<div style="color:black;font-size:14px;font-weight:bold;"><?=$errorDiv?></div>
						<? } ?>
						</div>	
						</div><!-- /login-box -->
						<div id="forgot-password" style="display:none;">
							
							<form action="retrieve_password.php" method="post" name="login" class="custom-form" >
                            <ul>
                    
                  				<li>
								<label>Email Address:</label>
								<input class="input1" type="text" name="username" style="width:370px;" />
								</li>
                                
								<!-- send current page for redirect back -->
								<input type="hidden" name="page" value="index.php" />
								<input type="hidden" name="parent_site" value="6Qube.com" />
								 <br />
                                 <li class="submit-li" style="float:left;">
                                <input type="submit" value="Send Password" id="submit" />
                                  </li>
                                  
                                </ul>
                                
                                <div class="clear"></div>
							</form>
						</div><!-- /forgot-password -->

                        </div>
                    </div>
           
                    
  
  <div class="firefox" >6Qube works best with<br /><a href="http://www.mozilla.com/en-US/firefox/firefox.html" target="_blank"><img src="http://6qube.com/images/firefox-logo.png" alt="Mozilla Firefox" border="0" /></a></div>
  
        </div>
            <div class="clear"></div>
          </div>
        </div>
        <!-- end slide -->
     
      </div>
      <!-- end slideshow -->
    </div>
    <!-- end header -->

    
  
</div>
<!-- end middle  -->
<!-- start footer -->
<div class="footer">
  <div class="footer-big">
    <div class="full-width">
      <div class="one-fourth footer-panel">
        <h3>Our Solutions</h3>
        <ul>
          <li><a href="/solutions/marketing-automation-software/">Marketing Automation</a></li>
          <li><a href="/solutions/inbound-marketing-software/">Inbound Marketing Software</a></li>
          <li><a href="/solutions/internet-marketing-services/">Internet Marketing Services</a></li>
          <li><a href="/solutions/social-media-solutions/">Social Media Solutions</a></li>
          <li><a href="/solutions/multiple-website-management/">Multiple Website Management</a></li>
          <li><a href="/solutions/marketing-service-providers/">Marketing Service Providers</a></li>
        </ul>
      </div>
      <div class="one-fourth footer-panel">
        <h3>Latest Tweets</h3>
        <div class="tweet">
          <!--  -->
        </div>
      </div>
      <!--  <div class="one-fourth footer-panel">
               <h3>From Our Blog</h3>
               
            </div>-->
      <div class="one-fourth footer-panel">
        <h3>Socialize With Us</h3>
        <ul class="social">
          <li><a href="http://twitter.com/6qube" class="twitter">Twitter</a></li>
          <li class="last"><a href="http://facebook.com/6qube" class="facebook">Facebook</a></li>
          <li><a href="http://www.linkedin.com/company/6qube" class="linkedin">LinkedIn</a></li>
          <li class="last"><a href="http://www.youtube.com/user/6qube/" class="youtube">YouTube</a></li>
          <li>
            <!-- &nbsp; -->
          </li>
          <li class="last">
            <!-- &nbsp; -->
          </li>
        </ul>
        <div class="clear"></div>
        <div class="rel mt10"> </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="footer-small">
    <div class="full-width">
      <div class="one-half left">&copy; 2011 6Qube. All rights reserved.</div>
      <div class="one-half right"></div>
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- end footer -->
<!-- execute cufon for IE flickering -->
<script type="text/javascript"> Cufon.now(); </script>
<!-- end cufon execute -->

</body>
</html>
