<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$uid = $_SESSION['login_uid'];
	$type = $_GET['type'];
	$id = $_GET['id'];
	
	$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
	$row = $local->queryFetch($query);
	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "main";
include('edit_network_site_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h1>Edit Network Site - <?=$row['name']?></h1>
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Company Name</label>
				<input type="text" name="company" value="<?=$row['company']?>" />
			</li>
            <li>
				<label>Network Name</label>
                <div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Local Business Directory</i> or <i>Realtor Directory</i></div>
				<input type="text" name="network_name" value="<?=$row['network_name']?>" />
			</li>
			<li>
				<label>Domain</label>
				<div id="domainerror" style="color:#888;font-size:12px; padding-bottom:5px;">Example: http://www.domain.com</div>
				<input type="text" class="website-url" id="domain" name="domain" value="<?=$row['domain']?>" title="network_site_<?=$type?>" />
			</li>
            
			<li>
				<label>Layout Theme</label>
				<? $local->displayThemes($_SESSION['login_uid'], $row['theme'], 'network_site', $_SESSION['user_class']); ?>
			</li>
		
            <li>
				<label>Category Only Network (Leave Blank if All Categories)</label>
                <select name="industry" title="select" class="uniformselect">
					<option value="0">Please Choose An Industry</option>
                    <option value="Automotive->" <? if($row['industry'] == "Automotive->") echo 'selected="yes"'; ?> >Automotive</option>
					<option value="Business Services->" <? if($row['industry'] == "Business Services->") echo 'selected="yes"'; ?> >Business Services</option>
                    <option value="Community and Education->" <? if($row['industry'] == "Community and Education->") echo 'selected="yes"'; ?> >Community and Education</option>
                    <option value="Computers and Electronics->" <? if($row['industry'] == "Computers and Electronics->") echo 'selected="yes"'; ?> >Computers and Electronics</option>
                    <option value="Entertainment->" <? if($row['industry'] == "Entertainment->") echo 'selected="yes"'; ?> >Entertainment</option>
					<option value="Finance->" <? if($row['industry'] == "Finance->") echo 'selected="yes"'; ?> >Finance</option>
                    <option value="Food and Dining->" <? if($row['industry'] == "Food and Dining->") echo 'selected="yes"'; ?> >Food and Dining</option>
                    <option value="Health and Personal Care->" <? if($row['industry'] == "Health and Personal Care->") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Healthcare->" <? if($row['industry'] == "Healthcare->") echo 'selected="yes"'; ?> >Healthcare</option>
                    <option value="Home Based Business->" <? if($row['industry'] == "Home Based Business->") echo 'selected="yes"'; ?> >Home Based Business</option>
                    <option value="Home Improvement->" <? if($row['industry'] == "Home Improvement->") echo 'selected="yes"'; ?> >Home Improvement</option>
                    <option value="Health and Personal Care->" <? if($row['industry'] == "Health and Personal Care->") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Legal->" <? if($row['industry'] == "Legal->") echo 'selected="yes"'; ?> >Legal</option>
                    <option value="Photography->" <? if($row['industry'] == "Photography->") echo 'selected="yes"'; ?> >Photography</option>
                    <option value="Real Estate->" <? if($row['industry'] == "Real Estate->") echo 'selected="yes"'; ?> >Real Estate</option>
                    <option value="Security->" <? if($row['industry'] == "Security->") echo 'selected="yes"'; ?> >Security</option>
                    <option value="Shopping->" <? if($row['industry'] == "Shopping->") echo 'selected="yes"'; ?> >Shopping</option>
                    <option value="Travel and Recreation->" <? if($row['industry'] == "Travel and Recreation->") echo 'selected="yes"'; ?> >Travel and Recreation</option>
                    <option value="Web Services->" <? if($row['industry'] == "Web Services->") echo 'selected="yes"'; ?> >Web Services</option>
				</select>
			</li>
		
              <li>
				<label>Single Sub-Category Only Network (Leave Blank if All Sub-Categories)</label>
				<?php echo $local->displayCategories($row['category']);?>
			</li>
            <li>
				<label>Featured Class (These Listing Show Above)</label>
				<?php echo $reseller->displayResellerProducts($_SESSION['login_uid'], 'dropdown', $row['featured_class'], NULL, NULL, 'featured_class'); ?>
			</li>
            <li>
				<label>User Class Only (Leave Blank if All Classes)</label>
				<?php echo $reseller->displayResellerProducts($_SESSION['login_uid'], 'dropdown', $row['class_only'], NULL, NULL, 'class_only'); ?>
			</li>
             <li>
				<label>City Focus (Leave Blank if All Cities)</label>
				<input type="text" name="city_focus" value="<?=$row['city_focus']?>" />
			</li>
			<li>
				<label>State Focus (Use without City for All Listing in State)</label>
				<?=$reseller->stateSelect('state_focus', $row['state_focus'])?>
			</li>
            <li>
				<label>Remove Search Option</label>
				<select name="remove_search" title="select" class="uniformselect">
					<option value="0">No</option>
					<option value="1" <? if($row['remove_search'] == "1") echo 'selected="yes"'; ?> >Yes</option>
				</select>
			</li>
            <li>
				<label>Remove Contact Form on Listing</label>
				<select name="remove_contact" title="select" class="uniformselect">
					<option value="0">No</option>
					<option value="1" <? if($row['remove_contact'] == "1") echo 'selected="yes"'; ?> >Yes</option>
				</select>
			</li>
         
             <li>
				<label>Background Color</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>#ff0000</i> or <i>red</i></div>
				<input type="text" name="bg_color" value="<?=$row['bg_color']?>" />
			</li>
            <li>
				<label>Navigation Link Hover Color</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>#ff0000</i> or <i>red</i></div>
				<input type="text" name="nav-link-hover-clr" value="<?=$row['nav-link-hover-clr']?>" />
			</li>
		</ul>
	</form>
	
	<form id="logo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Site Logo | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['logo_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['logo_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="logo" />
		<input type="hidden" name="img_id" value="<?=$id?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="logo_upload_prvw" class="img_cnt" src="<? if($row['logo']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['logo'];  else echo 'img/no-photo.jpg'; ?>" style="width:200px;" /><br />
    
    <!-- Favicon -->
	<form id="favicon_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Favicon | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a> | <span style="color:#666; font-size:75%;">max 64w x 64h image in .png format</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="favicon" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="favicon_upload_prvw" class="img_cnt" src="<? if($row['favicon']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['favicon'];  else echo $dflt_noImg; ?>" style="width:64px;" /><br />
	
	<!-- Header BG Img -->
	<form id="header-bg-img_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Header BG Image | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['header_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['header_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="header-bg-img" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="header-bg-img_upload_prvw" class="img_cnt" src="<? if($row['header-bg-img']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['header-bg-img'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<!-- Footer BG Img -->
	<form id="footer-bg-img_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Footer BG Image | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop Logo</a><? if($themeInfo['footer_size']){ ?> | <span style="color:#666; font-size:75%;"><?=$themeInfo['footer_size']?></span><? } ?></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="footer-bg-img" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="footer-bg-img_upload_prvw" class="img_cnt" src="<? if($row['footer-bg-img']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['footer-bg-img'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<!-- No Image Img -->
	<form id="no_image_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>'No Image' Image | <span style="color:#666; font-size:75%;">Default image displayed if an item doesn't have one</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="resellers_network" />
		<input type="hidden" name="fname" value="no_image" />
		<input type="hidden" name="img_id" value="<?=$row['id']?>" />
		<input type="hidden" name="reseller_network" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="no_image_upload_prvw" class="img_cnt" src="<? if($row['no_image']) echo '/reseller/'.$_SESSION['login_uid'].'/'.$row['no_image'];  else echo $dflt_noImg; ?>" style="width:200px;" /><br />
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			
			<li>
				<label>Advanced CSS</label>
			
				<textarea name="adv_css" rows="6" ><?=htmlentities($row['adv_css'])?></textarea>
				<br style="clear:left" />
			</li>
		</ul>
	</form><br />
	
</div>
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>