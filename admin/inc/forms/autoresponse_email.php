<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/local.class.php');
	
	//create new instance of class
	$local = new Local();
	
	if($_GET) $id = $_GET['id'];
	else $id = $_POST['id'];
	
	$ar = $local->getAutoResponders(NULL, NULL, NULL, $id, 'email');
	$row = $local->fetchArray($ar);
	
	if($_POST['sendTest']){
		$to = $_POST['user_email'];
		$subject = str_replace("#name", "John Doe", $row['subject']);
		$message = stripslashes($row['body']);
		$message = str_replace("#name", "John Doe", $message);
		if($row['sched']>0){
			$message .= "<br /><br />".$row['contact'];
			$message .= "<br />".$row['anti_spam'];
			$message .= '<br /><a href="http://6qube.com/?page=client-optout&email='.$to.'&pid=0" target="_blank">'.$row['optout_msg'].'</a>';
		}
		
		$send = $local->sendMail($to, $subject, $message, NULL, $row['from_field'], true, true);
		if($send['success']) echo '<script type="text/javascript">alert("Test email sent successfully to '.$to.'.");</script>';
		else echo '<script type="text/javascript">alert("Error sending test email: \n'.$send['errors'].'");</script>';
	}
	else {
		if($row['table_name']=="directory") $back_page = "directory-autoresponders.php";
		else if($row['table_name']=="hub") $back_page = "hub-autoresponders.php";
		else if($row['table_name']=="press_release") $back_page = "press-autoresponders.php";
		else if($row['table_name']=="blogs") $back_page = "blogs-autoresponders.php";
		else if($row['table_name']=="articles") $back_page = "articles-autoresponders.php";
	else if($row['table_name']=="lead_forms") $back_page = "inc/forms/forms_autoresponder.php";
		if($row['user_id']==$_SESSION['user_id']){
			//Get email templates
			//$query = "SELECT * FROM auto_responder_themes";
			//$templates = $local->query($query);
			if($row['from_field']){
				$fromField = str_replace('<', '&lt;', $row['from_field']);
				$fromField = str_replace('>', '&gt;', $fromField);
				$fromField = str_replace('" ', '', $fromField);
				$fromField = str_replace('"', '', $fromField);
			}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	
});
</script>
    
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/autoresponse_settings.php?id=<?=$id?>">Settings</a></li>
		<li><a href="inc/forms/autoresponse_email.php?id=<?=$id?>" class="current">Email</a></li>
        <li class="last"><a href="<?=$back_page?>?id=<?=$row['table_id']?>">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />

	<h1>Autoresponder Email</h1>
    <form name="auto_responders" class="jquery_form" id="<?=$row['id']?>">
        <ul>
            <li><label>From:</label>
            <span style="color:#666; font-size:75%"><strong>Must</strong> be in this format: <strong>Company Name &lt;email@company.com&gt;</strong></span><br />
            <input type="text" name="from_field" value="<?=$fromField?>" /></li>
            <li><label>Subject:</label><input type="text" name="subject" value="<?=$row['subject']?>" /></li>
            <li><label>Message:</label>
		  <a href="#" id="AR1" class="tinymce"><span>Rich Text</span></a>
		  <? if($_SESSION['user_class']==5){ ?>
		  <? if($templates && $local->numRows($templates)){
			echo '<select class="ar-templates no-upload">';
			echo '<option value="">Or choose a template...</option>';
			while($temprow = $local->fetchArray($templates)){
				echo '<option value="'.$temprow['id'].'">'.$temprow['theme_name'].'</option>';
			}
			echo '</select>';
		  } ?>
		  <? } ?>
		  <textarea name="body" rows="15" cols="80" id="edtrAR1"><?=$row['body']?></textarea><br style="clear:left" /></li>
            <span style="font-size:9.5px;"><strong>Hint:</strong> You can use "#name" (without quotes) in the body or subject and it will automatically be<br />replaced with the prospect's name when the email is sent.</span><br />
            <? if($row['sched']==0) { ?>
            <li><label>Return URL:</label>
            <span style="color:#666; font-size:75%">Optional: Specify the URL a prospect is taken to after submitting the contact form</span><br />
            <input type="text" name="return_url" value="<?=$row['return_url']?>" /></li>
            <? } else { ?>
            <br /><span style="color:#000; font-size:75%; font-weight:bold;">The following fields are required for the email to be sent, due to the CAN-SPAM act.</span><br />Feel free to customize them.<br />
            <li><label>Business Name/Address:</label>
            <a href="#" id="2" class="tinymce"><span>Rich Text</span></a><textarea name="contact" rows="5" cols="80" id="edtr2"><?=stripslashes($row['contact'])?></textarea><br style="clear:left;" />
            <li><label>Anti-Spam Message:</label>
            <textarea name="anti_spam" rows="3" cols="80" id="edtr3"><?=$row['anti_spam']?></textarea><br style="clear:left;" /></li>
            <li><label>Opt-out Message:</label>
            <input type="text" name="optout_msg" value="<?=$row['optout_msg']?>" /></li>
            <? } ?>
        </ul>
    </form><br />
	<form class="jquery_form" action="inc/forms/autoresponse_email.php" method="POST" target="hiddenIframe">
    	<label>Send test email:</label>
        <span style="color:#666; font-size:75%">Enter your email below and click submit to be sent a test copy of this email.<br /><b>Only click the button once.</b></span><br />
    	<input type="text" value="<?=$_SESSION['user']?>" name="user_email" style="width:350px;" />
	<input type="hidden" value="<?=$id?>" name="id" />
	<input type="submit" value="Send Test Email" name="sendTest" style="width:150px;" />
    </form>
    <iframe name="hiddenIframe" id="hiddenIframe" src="testemail.php" style="display:none;width:1px;height:1px;"></iframe>
	<? } else echo "Not authorized."; ?>
<? } ?>
