<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, httphostkey, user_id, user_id_created, hub_parent_id, name, domain, company_name, city, site_title, meta_keywords, meta_description, keyword1, keyword2, keyword3, keyword4, keyword5, keyword6, seo_url, seo_url_title, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields, www_options, lincoln_domain FROM hub WHERE id = '".$_SESSION['current_id']."' ";
	$row = $hub->queryFetch($query);
	$query = "SELECT user_class2_fields, pages FROM themes WHERE id = '".$row['theme']."'";
	$themeInfo = $hub->queryFetch($query);
	$sel = 'selected="selected"';
	if($row['user_id']==$_SESSION['user_id']){
		//get field info from hub row for locks
		if($row['hub_parent_id'] || $row['multi_user']){
			if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
			else {
				//if hub is a V2 MU Hub copy get parent hub info
				$query = "SELECT multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentHubInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentHubInfo['multi_user_fields'];
			}
		}
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if(($row['req_theme_type']==4 && $_SESSION['reseller']) ||
			($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
			$addClass = ' multiUser-'.$muHubID.'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		   ($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
			$multi_user2 = true;
			//figure out which fields to lock
			if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
			if($fieldsInfo = json_decode($fieldsInfoData, true)){
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
						return ' readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "seo";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - SEO</h2>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
	<? if($row['domain']    && (preg_match('/^www\./i', $row['httphostkey']) ||  substr_count($row['httphostkey'], '.') ==  1)){ ?>
	<ul>
		<li>
			<label>WWW Options</label>
			<? if($multi_user){ ?>
			<select name="lock_www_options" title="select" class="uniformselect<?=$addClass?>">
				<option value="0">Unlocked</option>
				<option value="1" <?=checkLock('www_options',1)?>>Locked</option>
			</select><div style="margin-top:-10px;"></div>
			<? } ?>
			<select name="www_options" title="select" class="uniformselect<?=$addClass.' '.checkLock('www_options')?>">
				<option value="both" <? if($row['www_options']=='both') echo $sel; ?>>Allow both (www and non-www)</option>
				<option value="www_only" <? if($row['www_options']=='www_only') echo $sel; ?>>www only</option>
				<option value="no_www" <? if($row['www_options']=='no_www') echo $sel; ?>>no www</option>
			</select>
		</li>
	</ul>
	<? } ?>
	<?php include('hubs_tags.php'); ?>
	
		<ul>
			<li>
				<label>Site Title</label>
				<? if($multi_user){ ?>
				<select name="lock_site_title" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('site_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('site_title')?> name="site_title" value="<?=$row['site_title']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Meta Keywords</label>
				<? if($multi_user){ ?>
				<select name="lock_meta_keywords" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('meta_keywords',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('meta_keywords')?> name="meta_keywords" value="<?=$row['meta_keywords']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Meta Description</label>
				<? if($multi_user){ ?>
				<select name="lock_meta_description" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('meta_description',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('meta_description')?> name="meta_description" value="<?=$row['meta_description']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 1</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword1" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword1',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword1')?> name="keyword1" value="<?=$row['keyword1']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 2</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword2" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword2',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword2')?> name="keyword2" value="<?=$row['keyword2']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 3</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword3" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword3',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword3')?> name="keyword3" value="<?=$row['keyword3']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 4</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword4" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword4',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword4')?> name="keyword4" value="<?=$row['keyword4']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 5</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword5" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword5',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword5')?> name="keyword5" value="<?=$row['keyword5']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Keyword 6</label>
				<? if($multi_user){ ?>
				<select name="lock_keyword6" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('keyword6',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('keyword6')?> name="keyword6" value="<?=$row['keyword6']?>" class="<?=$addClass?>" />
			</li>          
			<?php if($row['theme'] == "5") {?>
			<li>
				<label>SEO URL (Target URL for Backlinking)</label>
				<input type="text" name="seo_url" value="<?=$row['seo_url']?>" />
			</li>
			<li>
				<label>SEO URL Anchor Tag Title</label>
				<input type="text" name="seo_url_title" value="<?=$row['seo_url_title']?>" />
			</li>
			<?php } ?>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>SEO Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#hubseoVideo" class="trainingvideo" >Search Engine Optimization Video</a>
			</li>
			<div style="display:none;">
				<div id="hubseoVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-seo-overview.flv" style="display:block;width:640px;height:480px" id="hubseoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubseoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="https://adwords.google.com/select/KeywordToolExternal" class="external" target="_new" >Google Keyword Tool</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<? } else echo "Error displaying page."; ?>