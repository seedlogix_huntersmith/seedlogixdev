<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

if($_SESSION['reseller']){
	$pageID = $_GET['id'];
	$query = "SELECT parent, name, data, nav, list_order, allow_class FROM resellers_backoffice 
			WHERE admin_user = '".$_SESSION['login_uid']."' AND id = '".$pageID."'";
	$page = $reseller->queryFetch($query);
	//get nav sections
	$query = "SELECT id, name FROM resellers_backoffice 
			WHERE type = 'nav' AND admin_user = '".$_SESSION['login_uid']."'";
	if($a = $reseller->query($query)){
		$navs = array();
		while($b = $reseller->fetchArray($a)){
			$navs[$b['id']] = $b['name'];
		}
		$navName = $navs[$page['parent']];
		//allowed classes for this page
		$allowed = explode('::', $page['allow_class']);
		//get user class names
		$classes = array();
		$query = "SELECT id, name FROM user_class WHERE admin_user = '".$_SESSION['login_uid']."'";
		$c = $reseller->query($query);
		while($d = $reseller->fetchArray($c)){
			$classes[$d['id']] = $d['name'];
		}
	}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
});
</script>
<a href="reseller-backoffice-pages.php?mode=pages&n=<?=$page['parent']?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Pages" /></a><br /><br />
<h1>Edit Custom Back Office Page</h1>
<h2><?=$page['name']?></h2>
<br /><br />

<div id="form_test" style="width:100%;">
	<form name="resellers_backoffice" class="jquery_form" id="<?=$pageID?>">
		<ul>
			<li>
				<label>Page Name</label>
				<input type="text" name="name" value="<?=$page['name']?>"  />
			</li><br />
			<li>
				<label>Nav Section</label>
				<select name="parent" title="select" class="uniformselect">
				<?
				foreach($navs as $key=>$value){
					if($key==$page['parent']) $s = ' selected="selected"';
					echo '<option value="'.$key.'"'.$s.'>'.$value.'</option>';
					$s = NULL;
				}
				?>
				</select>
			</li>
			<li>
				<label>List Order <span style="color:#666; font-size:75%;">0 - 999</span></label>
				<input type="text" name="list_order" value="<?=$page['list_order']?>" maxlength="3" />
			</li><br />
			<li>
				<label>Show Link In Navigation? <span style="color:#666; font-size:75%;">(You can still link to it if not)</span></label>
				<select name="nav" title="select" class="uniformselect">
					<option value="1" <? if($page['nav']) echo 'selected="selected"'; ?>>Yes</option>
					<option value="0" <? if(!$page['nav']) echo 'selected="selected"'; ?>>No</option>
				</select>
			</li>
			<li style="line-height:23px;">
				<label>Allowed User Types</label>
				<? foreach($classes as $key=>$value){ ?>
				<input type="checkbox" name="<?=$key?>" rel="<?=$pageID?>" class="updateCheckbox" <? if(in_array($key, $allowed)) echo 'checked'; ?> />&nbsp; <?=$value?><br />
				<? } ?>
			</li><br />
			<li>
				<label>Page Layout</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="data" rows="40" id="edtr1" style="width:100%"><?=$page['data']?></textarea>
			</li>
		</ul>
	</form><br style="clear:both;" />
	<small>
	<strong>Hints:</strong><br />
	&bull; You can link to another page with the tag format <strong>[[Other Page Name]]</strong>.<br />
	&bull; You can link to another page with custom link text using the tag format <strong>[[Other Page Name||Link Text]]</strong>.<br />
	&bull; You can insert a user's information with the tag format <strong>((name))</strong>.  <a href="#" class="dflt-link" id="showCustomPageTags">Click here to see available tags</a>.<br />
	<div id="availableTags" style="display:none;line-height:25px;">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((name))</strong> - The user's full name (<em>John Doe</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((firstname))</strong> - The user's first name (<em>John</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((lastname))</strong> - The user's last name (<em>Doe</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((company))</strong> - The user's company name (<em>Doe Enterprises</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((email))</strong> - The user's email address (<em>johndoe@email.com</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((phone))</strong> - The user's formatted phone number (<em>(800) 555-1234</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((fulladdress))</strong> - The user's full address with line breaks<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((address))</strong> - The user's street address (123 Maple St.)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((address2))</strong> - The user's suite or apartment number (Suite 333)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((city))</strong> - The user's city (Austin)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((state))</strong> - The user's state (TX)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((zip))</strong> - The user's zip code (12345)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((id))</strong> - The user's system ID (1234)<br />
	</div>
	</small>
</div>
<br style="clear:both;" />
<? } else echo 'Not authorized.'; ?>