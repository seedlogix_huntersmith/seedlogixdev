<?php
session_start();

require_once( dirname(__FILE__) . '/../../6qube/core.php');

if(isset($_SESSION['views']))
	$_SESSION['views']=$_SESSION['views']+1;
else
	$_SESSION['views']=1;

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/rankings.class.php');
$rankings = new Rankings();

if ($id) {
    $row = $rankings->getRankings($_SESSION['user_id'], $_SESSION['campaign_id'], $id);
}

$date = strtotime($row['keywordupdate']);

if ($row['user_id'] == $_SESSION['user_id']) {
?>

<!-- simply rank css -->
<link rel="stylesheet" href="thirdParty/simplyRank/css/bootstrap.min.css" />
<link rel="stylesheet" href="thirdParty/simplyRank/css/jquery.qtip.css" type="text/css" />
<link rel="stylesheet" href="thirdParty/simplyRank/css/bootstrap-responsive.css" />
<link rel="stylesheet" href="thirdParty/simplyRank/css/style.css" />


	<script src="thirdParty/simplyRank/scripts/jquery-1.7.2.min.js"></script>


	<script src="thirdParty/simplyRank/scripts/bootstrap.min.js"></script>
	<script src="thirdParty/simplyRank/scripts/jquery.qtip.js"></script>
    <script src="thirdParty/simplyRank/scripts/jquery.dataTables.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="flot/excanvas.min.js"></script><![endif]-->
	<script src="thirdParty/simplyRank/scripts/jquery.flot.js"></script>
    <script src="thirdParty/simplyRank/scripts/jquery.flot.resize.js"></script>
    <script src="thirdParty/simplyRank/scripts/jquery.flot.pie.js"></script>
	<script src="thirdParty/simplyRank/scripts/modernizr-2.5.3-respond-1.1.0.min.js"></script>
	
	
	

	
	

    <script type="text/javascript">
	(function ($) {
	
		$.fn.extend({
			//pass the options variable to the function
			confirmModal: function (options) {
				var html = '<div class="modal" id="confirmContainer"><div class="modal-header"><a class="close" data-dismiss="modal">?</a>' +
				'<h3>#Heading#</h3></div><div class="modal-body">' +
				'#Body#</div><div class="modal-footer">' +
				'<a class="btn btn-primary external" id="confirmYesBtn">Confirm</a>' +
				'<a class="btn external" data-dismiss="modal">Close</a></div></div>';
	
				var defaults = {
					heading: 'Please confirm',
					body:'Body contents',
					callback : null
				};
	
				var options = $.extend(defaults, options);
				html = html.replace('#Heading#',options.heading).replace('#Body#',options.body);
				$(this).html(html);
				$(this).modal('show');
				var context = $(this);
				$('#confirmYesBtn',this).click(function(){
					if(options.callback!=null)
						options.callback();
					$(context).modal('hide');
				});
			}
		});
	})(jQuery);
	
	(function ($) {
		$.fn.extend({
			//pass the options variable to the function
			regModal: function (options) {
				var html = '<div class="modal" id="confirmContainer"><div class="modal-header"><a class="close" data-dismiss="modal">?</a>' +
				'<h3>#Heading#</h3></div><div class="modal-body">' +
				'#Body#</div><div class="modal-footer">' +
				'<a class="btn" data-dismiss="modal">Close</a></div></div>';
	
				var defaults = {
					heading: 'Please confirm',
					body:'Body contents',
					callback : null
				};
	
				var options = $.extend(defaults, options);
				html = html.replace('#Heading#',options.heading).replace('#Body#',options.body);
				$(this).html(html);
				$(this).modal('show');
				var context = $(this);
			}
		});
	})(jQuery);
	
	
	bootstrap_alert = function() {}
	
	bootstrap_alert.warning = function(message) {
		$('#alert_placeholder').html('<div class="alert"><a class="close" data-dismiss="alert">?</a><span>'+message+'</span></div>')
	}
	
	
  
	
</script>
<div id="confirmDiv"></div>


<a href="rankings.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Rankings" /></a>
<a href="#addKeywords" class="greyButton addKeywords external" id="addKeywords" style="float:right;margin-top:15px;" >Add Keywords</a>

<div style="display: none;">
	<div id="addKeywordsc" style="width:640px;height:350px;overflow:auto;">
		<div id="createDir">
			<h3>Enter Keywords</h3><br />
			<form method="post" action="#" class="ajax" id="keywords" >
				<textarea name="keywords" style="width:98%; height:200px; margin-bottom:25px;"></textarea>
				<input type="hidden" name="the_main_site_id" value="<?= $id ?>"  />
				<input type="submit" value="Add Keywords" name="submit" />
			</form>
    
			<div id="new_ranking">
				<div class="newRanking"></div>
			</div>
		</div>
	</div>
</div>

<br style="clear:both;" />
<br />
<?php
    $db =   $rankings->connection;
    $keywordupdate_query = $rankings->pdo->query("SELECT keywordupdate FROM sapphire_rankings.sites WHERE id='$id'");
    $keywordupdate       = $keywordupdate_query->fetch();	//mysqli_fetch_row($keywordupdate_query);
    $keywordupdate_date  = strtotime($keywordupdate[0]);
?>

<div id="the_stats" class="well">
	<center>
    	<h1><?= $row['name'] ?> Rankings</h1>
    	<h6>for <?= date("M d, Y", $date) ?></h6>
  	</center>
  	<br>
  	
  	<div class="span3special">
    <?php
    $google_page1_query = $rankings->pdo->query("SELECT COUNT(google) FROM sapphire_rankings.rankings WHERE DATE(datetime)=DATE('$keywordupdate[0]') AND site_id='$id' AND google<=10 AND google != 0;");
    $google_page1       = $google_page1_query->fetch();	//mysqli_fetch_row($google_page1_query);
	?>
    <h4>Google Page 1</h4>
    <span class="value">
    <?php
    echo $google_page1[0];
	?>
	</span>
	</div>
	
	<div class="span3special">
    <?php
    $google_top3_query = $rankings->pdo->query("SELECT COUNT(google) FROM sapphire_rankings.rankings WHERE DATE(datetime)=DATE('$keywordupdate[0]') AND site_id='$id' AND google<=3 AND google != 0;");
    $google_top3       = $google_top3_query->fetch();	//mysqli_fetch_row($google_top3_query);
	?>
    <h4>Google Top 3</h4>
    <span class="value">
    <?php
    echo $google_top3[0];
	?>
	</span>
	</div>
	
  	<div class="span3special">
    <?php
    $bing_page1_query = $rankings->pdo->query("SELECT COUNT(bing) FROM sapphire_rankings.rankings WHERE DATE(datetime)=DATE('$keywordupdate[0]') AND site_id='$id' AND bing<=10 AND bing != 0;");
    $bing_page1       = $bing_page1_query->fetch();	//mysqli_fetch_row($bing_page1_query);
	?>
    <h4>Bing Page 1</h4>
    <span class="value">
    <?php
    echo $bing_page1[0];
	?>
	</span>
	</div>
  	
  	<div class="span3special">
    <?php
    $bing_top3_query = $rankings->pdo->query("SELECT COUNT(bing) FROM sapphire_rankings.rankings WHERE DATE(datetime)=DATE('$keywordupdate[0]') AND site_id='$id' AND bing<=3 AND bing != 0;");
    $bing_top3       = $bing_top3_query->fetch();	//mysqli_fetch_row($bing_top3_query);
	?>
    <h4>Bing Top 3</h4>
    <span class="value">
    <?php
    echo $bing_top3[0];
	?>
	</span>
	</div>
</div>

<br>
<br>
<script type="text/javascript" charset="utf-8">
			/* Formating function for row details */
						
			$(document).ready(function() {
				
				
				
				/*
				 * Insert a 'details' column to the table
				 */
				var nCloneTh = document.createElement( 'th' );
				nCloneTh.setAttribute('style', 'width:80px');
				var nCloneTd = document.createElement( 'td' );
				nCloneTd.innerHTML = '<span class="add_rows">';
				nCloneTd.className = "center";
				nCloneTd.id = "expand_details";
				
				$('#keywordstable thead tr').each( function () {
					this.insertBefore( nCloneTh, this.childNodes[0] );
				} );
				
				$('#keywordstable tbody tr').each( function () {
					this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
				} );
				
				function setDetailOpenCloseEvent() {
						$('.details').hide();
						$("span.remove_rows").attr('class','add_rows');
				}
				
				 
				/*
				 * Initialse DataTables, with no sorting on the 'details' column
				 */
				var oTable = $('#keywordstable').dataTable( {
					"aoColumnDefs": [
						{ "bSortable": false, "aTargets": [ 0,10 ] }
					],
					"aaSorting": [[ 1, "asc" ]],
					"sDom": "<'datatablesrow'<'lengthdiv'l><'filterdiv'f>>t<'datatablesrow'<'infodiv'i><'paginatediv'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records per page",
						"sSearch": "Search: _INPUT_"
					},
					"bAutoWidth": false,
					"fnDrawCallback": function () {setDetailOpenCloseEvent(); initializeqtips(); resizetime();},
					"aoColumns": [
					/* Expand */	{ "sWidth": "3%" },
					/* Keyword */	{ "sType": 'string',
									"sWidth": "34%" },
					/* Google */	{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Gday */		{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Gweek */		{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Gmonth */	{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Bing */		{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Bday */		{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Bweek */		{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Bmonth */	{ "sType": 'title-numeric',
									"sWidth": "7%" },
					/* Check */		{ "sWidth": "7%" },
										]
				});
				$('#expand_details span').live('click', function () {
					var nTr = $(this).parents('tr')[0];
					if ( oTable.fnIsOpen(nTr) )
					{
						/* This row is already open - close it */
						$(this).attr('class', 'add_rows');
											}
					else
					{
						/* Open this row */
						$(this).attr('class', 'remove_rows');
											}
				} );
			
			var resizeTimer;
			
			//Event to handle resizing
			function resizetime() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(removeoradd, 100);
			};
			function removeoradd(){
				if ($(window).width() > 600)  {
					oTable.fnSetColumnVis( 0, true );
					oTable.fnSetColumnVis( 3, true );
					oTable.fnSetColumnVis( 4, true );
					oTable.fnSetColumnVis( 5, true );
					oTable.fnSetColumnVis( 7, true );
					oTable.fnSetColumnVis( 8, true );
					oTable.fnSetColumnVis( 9, true );
					oTable.fnSetColumnVis( 10, true );
					
				} else {
					oTable.fnSetColumnVis( 0, false );
					oTable.fnSetColumnVis( 3, false );
					oTable.fnSetColumnVis( 4, false );
					oTable.fnSetColumnVis( 5, false );
					oTable.fnSetColumnVis( 7, false );
					oTable.fnSetColumnVis( 8, false );
					oTable.fnSetColumnVis( 9, false );
					oTable.fnSetColumnVis( 10, false );
				}
			}
			
			$(window).resize(function(){
			  resizetime();
			}); 
		} );
	</script>
	<script type="text/javascript">
	$('a#addKeywords').live ('click', function (e) {
		e.preventDefault ();
		$("#confirmDiv").confirmModal({
			heading: 'Add Keywords (one per line)',
			body: '<form id="keywordsform"><input type=hidden name="the_main_site_id" value="<?php echo $id; ?>">' +
				'<textarea name="keywords" style="width:98%; height:150px;"></textarea></form>',
			callback: function () { 
				$.post("inc/forms/add-ranking.php", $("#keywordsform").serialize())
				bootstrap_alert.warning('Your keywords have been added.  Please check back soon for your rankings.');
			}
		});
	});
	$(".remove_item").click(function (e) {
		e.preventDefault ();
		var oTable = $('#keywordstable').dataTable();
		var row = $(this).closest("tr").get(0);
		var the_main_site_id = $(this).attr("the_main_site_id");
		var the_main_site = $(this).attr("the_main_site");
		var thisId = $(this).attr("id");
		$("#confirmDiv").confirmModal({
			heading: 'Confirm to remove keyword.',
			body: 'Are you sure you want to remove '+thisId+'?',
			callback: function () {
				$.post("removekeyword.php", { keywords: thisId, the_main_site_id: the_main_site_id } );
				oTable.fnDeleteRow(oTable.fnGetPosition(row));
				bootstrap_alert.warning(thisId+' has been removed from the '+the_main_site+' campaign.');
			}
		});
	});
	$('.refresh_keyword').live ('click', function (e) {
		e.preventDefault ();
		var thisId = $(this).attr("id");
		var the_main_site_id = $(this).attr("the_main_site_id");
		var the_main_site = $(this).attr("the_main_site");
		$("#confirmDiv").confirmModal({
			heading: 'Confirm to refresh keyword.',
			body: 'Are you sure you want to refresh '+thisId+'?',
			callback: function () {
				$.post("refreshkeyword.php", { keywords: thisId, the_main_site_id: the_main_site_id, the_main_site: the_main_site } );
				bootstrap_alert.warning(thisId+' will now be refreshed.  Please check back soon for your updated rankings.');
			}
		});	
	});
	$(function(){
	$('.container').show();
		$('#myModal').modal({
			backdrop: true,
			keyboard: true,
			show: false
		});
		$('.dailyrankings').live('click', function(e) {
			e.preventDefault();
			var keyword=$(this).attr("keyword");
			$('#myModal').find('h3').html('Complete results for '+keyword);
			$('#myModal').modal('show');
			$('#modal-body').load('dailyrankings.php?keyword='+encodeURIComponent(keyword));
		});
	});
</script>
<script type="text/javascript">
function initializeqtips() {
	$('.more_details').each(function () {
		var thisId = $(this).attr("id");
		$(this).qtip({
			position: {
				my: 'top right', 
				at: 'bottom left',
				viewport: $(window),
				effect: false
			},
			style: {
				classes: 'ui-tooltip-tipped ui-tooltip-shadow'
			},
			content: {
				text: 'Loading...',
				ajax: {
					loading: true,
					url: 'thirdParty/simplyRank/json/keywordinfo.php',
					type: 'POST',
					data: { 
						id: thisId
					},
					dataType: 'json',
					success: function(data, status) {
						var content = '<b>Google URL:</b><br>' + data.info.google_url + '<br><br><b>Bing URL:</b><br>' + data.info.bing_url + '<br><br><b>Data from:</b><br>' + data.info.datetime;
						this.set('content.text', content);
					}
				}
			}
		});
	});
}
$(function () {
	
	
	
	initializeqtips();
	$('.widget-header').live ('click', function (e) {
		e.preventDefault ();
		var the_main_site_id = $(this).attr("the_main_site_id");
		$("#chartholder").slideToggle();
	});
	
	//Keywords Chart
	$('.keyword_charts').click( function (e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var keyword = $(this).attr("keyword");
		var the_main_site_id = $(this).attr("the_main_site_id");
		var the_main_site = $(this).attr("the_main_site");
		var placeholder = $("#chart");
		
		var outerplaceholder = $("#outerchart");
		if ($("#chart").attr("currentkeyword") !== undefined) {
			if ($("#chart").attr("currentkeyword") == keyword) {
			$("#outerouterchart").slideToggle(400,function () {
				removechart();
				$("#chart").empty();
				$(".filterdiv").show();
				$(".lengthdiv").show();
			});
			} else {
			$("#outerouterchart").slideToggle(400,function () {
				removechart();
				$("#chart").empty();
			});
			$("#outerouterchart").slideToggle(400,function () {
				$(".filterdiv").hide();
				$(".lengthdiv").hide();
				
				addchart();
			});
			}
		} else {
			$("#outerouterchart").slideToggle(400,function () {
				$(".filterdiv").hide();
				$(".lengthdiv").hide();
				
				addchart();
			});
		}
		
		function removechart() {
			$("#chart").removeAttr("currentkeyword");
			$("#chart_title").empty();
		}
		
		function addchart() {
			$("#chart").attr("currentkeyword", keyword);
			$("#chart_title").html("Keyword: "+keyword);
			
			$.ajax({
				url: 'thirdParty/simplyRank/json/flotjson.php',
				type: 'POST',
				data: { 
					keyword: keyword,
					the_main_site_id: the_main_site_id
				},
				dataType: 'json',
				success: onOutboundReceived
			});
		 
			function onOutboundReceived(series) {
				var finalData = series;
				var options = {
					lines: { show: true, lineWidth: 3 },
					points: { show: true, radius: 4, lineWidth: 3 },
					grid: { hoverable: true, borderColor: "#EFEFEF", labelMargin: 15, borderWidth: 1, backgroundColor: "#f7f9fb", show: true, tickColor: "#EDF2F4"},
					xaxis: { mode: "time", minTickSize: [1, "day"] },
					yaxis: {
						transform: function (v) { return -v; },
						inverseTransform: function (v) { return -v; },
						min:1,
						max:50
					},
					legend: {container: $(".visualize-info")}
				};
				makethegraph(placeholder, finalData, options);
			}
			
			function makethegraph(placeholder, finalData, options){
				$.plot(placeholder, finalData, options);
			}
			
			// Create a tooltip on our chart
			placeholder.qtip({
				prerender: true,
				content: 'Loading...', // Use a loading message primarily
				position: {
					viewport: $(window), // Keep it visible within the window if possible
					target: 'mouse', // Position it in relation to the mouse
					adjust: { x: 10, y: 10 } // ...but adjust it a bit so it doesn't overlap it.
				},
				show: false, // We'll show it programatically, so no show event is needed
				style: {
					tip: false // Remove the default tip.
				}
			});
		 
			// Bind the plot hover
			placeholder.bind('plothover', function(event, coords, item) {
				// Grab the API reference
				var self = $(this),
					api = $(this).qtip(),
					previousPoint, content,
		 
				// Setup a visually pleasing rounding function
				round = function(x) { return Math.round(x * 1000) / 1000; };
		 
				// If we weren't passed the item object, hide the tooltip and remove cached point data
				if(!item) {
					api.cache.point = false;
					return api.hide(event);
				}
		 
				// Update the cached point data
				api.cache.point = item.dataIndex;
	 
				// Setup new content
				// original from qtip demo: content = item.series.label + '(' + round(item.datapoint[0]) + ') = ' + round(item.datapoint[1]);
				content = "<b>" + item.series.label + " Rank: " + round(item.datapoint[1]) + "</b> for " + item.series.date[api.cache.point] + "<br><br>URL: " + item.series.url[api.cache.point];
	 
				// Update the tooltip content
				api.set('content.text', content);
				api.set('style.classes', 'ui-tooltip-tipped ui-tooltip-shadow ui-tooltip-'+item.series.color.substr(1));
	 
				// Make sure we don't get problems with animations
				api.elements.tooltip.stop(1, 1);
	 
				// Show the tooltip, passing the coordinates
				api.show(coords);
			});
		}
		return false;
	});
});



//may not need all of this

jQuery.fn.dataTableExt.oSort['title-numeric-asc']  = function(a,b) {
    var x = a.match(/title="*(-?[0-9\.]+)/)[1];
    var y = b.match(/title="*(-?[0-9\.]+)/)[1];
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};
jQuery.fn.dataTableExt.oSort['title-numeric-desc'] = function(a,b) {
    var x = a.match(/title="*(-?[0-9\.]+)/)[1];
    var y = b.match(/title="*(-?[0-9\.]+)/)[1];
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};

function givemetime(that) {
	if(that==0){
		return '12am-1am';
	} else if(that==1){
		return '1am-2am';
	} else if(that==2){
		return '2am-3am';
	} else if(that==3){
		return '3am-4am';
	} else if(that==4){
		return '4am-5am';
	} else if(that==5){
		return '5am-6am';
	} else if(that==6){
		return '6am-7am';
	} else if(that==7){
		return '7am-8am';
	} else if(that==8){
		return '8am-9am';
	} else if(that==9){
		return '9am-10am';
	} else if(that==10){
		return '10am-11am';
	} else if(that==11){
		return '11am-12pm';
	} else if(that==12){
		return '12pm-1pm';
	} else if(that==13){
		return '1pm-2pm';
	} else if(that==14){
		return '2pm-3pm';
	} else if(that==15){
		return '3pm-4pm';
	} else if(that==16){
		return '4pm-5pm';
	} else if(that==17){
		return '5pm-6pm';
	} else if(that==18){
		return '6pm-7pm';
	} else if(that==19){
		return '7pm-8pm';
	} else if(that==20){
		return '8pm-9pm';
	} else if(that==21){
		return '9pm-10pm';
	} else if(that==22){
		return '10pm-11pm';
	} else if(that==23){
		return '11pm-12am';
	}
}


/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
	"sSortAsc": "header headerSortDown",
	"sSortDesc": "header headerSortUp",
	"sSortable": "header"
} );

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
}

/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
	"bootstrap": {
		"fnInit": function( oSettings, nPaging, fnDraw ) {
			var oLang = oSettings.oLanguage.oPaginate;
			var fnClickHandler = function ( e ) {
				e.preventDefault();
				if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
					fnDraw( oSettings );
				}
			};

			$(nPaging).addClass('pagination').append(
				'<ul>'+
					'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
					'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
				'</ul>'
			);
			var els = $('a', nPaging);
			$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
			$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
		},

		"fnUpdate": function ( oSettings, fnDraw ) {
			var iListLength = 5;
			var oPaging = oSettings.oInstance.fnPagingInfo();
			var an = oSettings.aanFeatures.p;
			var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

			if ( oPaging.iTotalPages < iListLength) {
				iStart = 1;
				iEnd = oPaging.iTotalPages;
			}
			else if ( oPaging.iPage <= iHalf ) {
				iStart = 1;
				iEnd = iListLength;
			} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
				iStart = oPaging.iTotalPages - iListLength + 1;
				iEnd = oPaging.iTotalPages;
			} else {
				iStart = oPaging.iPage - iHalf + 1;
				iEnd = iStart + iListLength - 1;
			}

			for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
				// Remove the middle elements
				$('li:gt(0)', an[i]).filter(':not(:last)').remove();

				// Add the new list items and their event handlers
				for ( j=iStart ; j<=iEnd ; j++ ) {
					sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
					$('<li '+sClass+'><a href="#">'+j+'</a></li>')
						.insertBefore( $('li:last', an[i])[0] )
						.bind('click', function (e) {
							e.preventDefault();
							oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
							fnDraw( oSettings );
						} );
				}

				// Add / remove disabled classes from the static elements
				if ( oPaging.iPage === 0 ) {
					$('li:first', an[i]).addClass('disabled');
				} else {
					$('li:first', an[i]).removeClass('disabled');
				}

				if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
					$('li:last', an[i]).addClass('disabled');
				} else {
					$('li:last', an[i]).removeClass('disabled');
				}
			}
		}
	}
} );
</script>
<div>
<div id="chartholder">
<div class="keywordchart_outer_outer" id="outerouterchart">
  <div class="visualize" id="outerchart">
    <div class="visualize-scroller" id="chart" style="height:300px;"></div>
    <div class="visualize-info"></div>
  </div>
</div>
<div id = "alert_placeholder"></div>
<table id="keywordstable" class="table table-bordered table-striped data-table keywords">
  <thead>
    <tr>
      <th>Keyword</th>
      <th>Google</th>
      <th style="font-weight:normal;">Day</th>
      <th style="font-weight:normal;">Week</th>
      <th style="font-weight:normal;">Month</th>
      <th>Bing</th>
      <th style="font-weight:normal;">Day</th>
      <th style="font-weight:normal;">Week</th>
      <th style="font-weight:normal;">Month</th>
      <th>Options</th>
    </tr>
  </thead>
  <tbody>
              <?php
    $result = $rankings->pdo->query("SELECT 
    q1.keyword AS q1keyword,q1.domain AS q1domain, q1.id AS q1id,
    q1.google AS q1google,q1.bing AS q1bing,
    q2.google AS q2google,q2.bing AS q2bing,
    q3.google AS q3google,q3.bing AS q3bing,
    q4.google AS q4google,q4.bing AS q4bing
    FROM sapphire_rankings.rankings q1
    LEFT JOIN sapphire_rankings.rankings q2 ON q1.keyword=q2.keyword AND q1.site_id=q2.site_id AND q1.datetime - INTERVAL 1 DAY=q2.datetime
    LEFT JOIN sapphire_rankings.rankings q3 ON q1.keyword=q3.keyword AND q1.site_id=q3.site_id AND q1.datetime - INTERVAL 1 WEEK=q3.datetime
    LEFT JOIN sapphire_rankings.rankings q4 ON q1.keyword=q4.keyword AND q1.site_id=q4.site_id AND q1.datetime - INTERVAL 1 MONTH=q4.datetime
    WHERE DATE(q1.datetime) = DATE(NOW()) AND q1.site_id='$id'
    OR DATE(q1.datetime) = DATE(NOW()) AND q1.competitor_id='$id'
    ORDER BY q1.keyword ASC,q1.competitor_id ASC,q1.domain DESC,q1.site_id ASC") or die(mysql_error());
    unset($new_sites);
    $new_sites_query = $rankings->pdo->query("select sites.site AS new_sites
      FROM sapphire_rankings.sites left JOIN sapphire_rankings.rankings
      on sites.site=rankings.domain
      where rankings.domain is null AND sites.competitor_id='$id'");
    while ($new_sites_row = $new_sites_query->fetch()){		//mysqli_fetch_array($new_sites_query)) {
        $new_sites[] = $new_sites_row['new_sites'];
    }
    $saved_keyword = NULL;
    
    while ($row = $result->fetch()){	//mysqli_fetch_array($result)) {
        $used_keywords_array[] = $row['q1keyword'];
        $keyword_id            = $row['q1id'];
        if ($saved_keyword == NULL) {
            echo "<tr><td style='text-align:left;'><a href='#' class='dailyrankings' id='" . $row['q1id'] . "' keyword='" . $row['q1keyword'] . "'>" . $row['q1keyword'] . "</a></td>";
        } elseif ($saved_keyword != $row['q1keyword']) {
            if (isset($new_sites)) {
                foreach ($new_sites as $key => $new_site) {
                    echo "<td>" . $new_site . "</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                }
            }
            echo "</tr><tr><td style='text-align:left;'><a class='dailyrankings' id='" . $row['q1id'] . "' keyword='" . $row['q1keyword'] . "' href='#'>" . $row['q1keyword'] . "</a></td>";
        } else {
            echo "<td style='text-align:left;'>" . $row['q1domain'] . "</td>";
        }
        
        if ($row['q1google'] != "0") {
            echo "<td><span title='" . $row['q1google'] . "'></span>" . $row['q1google'] . "</td>";
        } else {
            echo "<td><span title='51'></span>over 50</td>";
        }
        
        //YESTERDAY DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q2google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q2google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q2google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        //LAST WEEK DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q3google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q3google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q3google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        //LAST MONTH DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q4google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q4google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q4google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        if ($row['q1bing'] != "0") {
            echo "<td><span title='" . $row['q1bing'] . "'></span>" . $row['q1bing'] . "</td>";
        } else {
            echo "<td><span title='51'></span>over 50</td>";
        }
        
        //YESTERDAY DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q2bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q2bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q2bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        
        //LAST WEEK DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q3bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q3bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q3bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        
        //LAST MONTH DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q4bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q4bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q4bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        if ($saved_keyword != $row['q1keyword']) {
            echo '<td>' . 
//            		<a class="more_details" id="' . $keyword_id . '"></a>
//            		<a class="refresh_keyword" id="' . $row['q1keyword'] . '" the_main_site_id="' . $id . '" the_main_site="' . $row['site'] . '"></a>
            		'<div class="keyword_charts" keyword="' . $row['q1keyword'] . '" the_main_site_id="' . $id . '" the_main_site="www.hubspot.com' . $row['site'] . '"></div>
            		<a class="remove_item" id="' . $row['q1keyword'] . '" the_main_site="' . $row['site'] . '" the_main_site_id="' . $id . '"></a>
            	</td>';
        } else {
            echo '<td></td>';
        }
        $saved_keyword = $row['q1keyword'];
    }
    if (isset($new_sites)) {
        foreach ($new_sites as $key => $new_site) {
            echo "<td>" . $new_site . "</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
        }
    }
?>
                </tr>
              
              <?php
    if (isset($used_keywords_array)) {
        $used_keywords_unique = array_unique($used_keywords_array);
        $used_keywords        = implode("','", $used_keywords_unique);
        $used_keywords_ready  = "'$used_keywords'";
        $result = $rankings->pdo->query("SELECT 
      q1.keyword AS q1keyword,q1.domain AS q1domain, q1.id AS q1id,
      q1.google AS q1google,q1.bing AS q1bing,
      q2.google AS q2google,q2.bing AS q2bing,
      q3.google AS q3google,q3.bing AS q3bing,
      q4.google AS q4google,q4.bing AS q4bing
      FROM sapphire_rankings.rankings q1
      LEFT JOIN sapphire_rankings.rankings q2 ON q1.keyword=q2.keyword AND q1.site_id=q2.site_id AND q1.datetime - INTERVAL 1 DAY=q2.datetime
      LEFT JOIN sapphire_rankings.rankings q3 ON q1.keyword=q3.keyword AND q1.site_id=q3.site_id AND q1.datetime - INTERVAL 1 WEEK=q3.datetime
      LEFT JOIN sapphire_rankings.rankings q4 ON q1.keyword=q4.keyword AND q1.site_id=q4.site_id AND q1.datetime - INTERVAL 1 MONTH=q4.datetime
      WHERE DATE(q1.datetime) = DATE('$keywordupdate[0]') AND q1.site_id='$id' AND q1.keyword not in ($used_keywords_ready)
      OR DATE(q1.datetime) = DATE('$keywordupdate[0]') AND q1.competitor_id='$id' AND q1.keyword not in ($used_keywords_ready)
      ORDER BY q1.keyword ASC,q1.competitor_id ASC,q1.domain DESC,q1.site_id ASC") or die(mysql_error());
    } else {
        $result = $rankings->pdo->query("SELECT 
      q1.keyword AS q1keyword,q1.domain AS q1domain, q1.id AS q1id,
      q1.google AS q1google,q1.bing AS q1bing,
      q2.google AS q2google,q2.bing AS q2bing,
      q3.google AS q3google,q3.bing AS q3bing,
      q4.google AS q4google,q4.bing AS q4bing
      FROM sapphire_rankings.rankings q1
      LEFT JOIN sapphire_rankings.rankings q2 ON q1.keyword=q2.keyword AND q1.site_id=q2.site_id AND q1.datetime - INTERVAL 1 DAY=q2.datetime
      LEFT JOIN sapphire_rankings.rankings q3 ON q1.keyword=q3.keyword AND q1.site_id=q3.site_id AND q1.datetime - INTERVAL 1 WEEK=q3.datetime
      LEFT JOIN sapphire_rankings.rankings q4 ON q1.keyword=q4.keyword AND q1.site_id=q4.site_id AND q1.datetime - INTERVAL 1 MONTH=q4.datetime
      WHERE DATE(q1.datetime) = DATE('$keywordupdate[0]') AND q1.site_id='$id'
      OR DATE(q1.datetime) = DATE('$keywordupdate[0]') AND q1.competitor_id='$id'
      ORDER BY q1.keyword ASC,q1.competitor_id ASC,q1.domain DESC,q1.site_id ASC") or die(mysql_error());
    }
    unset($new_sites, $used_keywords, $used_keywords_ready, $used_keywords_unique, $used_keywords_array);
    $new_sites_query = $rankings->pdo->query("select sites.site AS new_sites
        FROM sapphire_rankings.sites left JOIN sapphire_rankings.rankings
        on sites.site=rankings.domain
        where rankings.domain is null AND sites.competitor_id='$id'");
    while ($new_sites_row = $new_sites_query->fetch()){ //mysqli_fetch_array($new_sites_query)) {
        $new_sites[] = $new_sites_row['new_sites'];
    }
    $saved_keyword = NULL;
    
    while ($row = $result->fetch()){ //mysqli_fetch_array($result)) {
        $keyword_id = $row['q1id'];
        if ($saved_keyword == NULL) {
            echo "<tr><td style='text-align:left;'>" . $row['q1keyword'] . "</td>";
        } elseif ($saved_keyword != $row['q1keyword']) {
            if (isset($new_sites)) {
                foreach ($new_sites as $key => $new_site) {
                    echo "<td>" . $new_site . "</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                    echo "<td>-</td>";
                }
            }
            echo "</tr><tr><td style='text-align:left;'>" . $row['q1keyword'] . "</td>";
        } else {
            echo "<td style='text-align:left;'>" . $row['q1domain'] . "</td>";
        }
        
        if ($row['q1google'] != "0") {
            echo "<td><span title='" . $row['q1google'] . "'></span>" . $row['q1google'] . "</td>";
        } else {
            echo "<td><span title='51'></span>over 50</td>";
        }
        
        //YESTERDAY DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q2google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q2google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q2google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        //LAST WEEK DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q3google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q3google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q3google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        //LAST MONTH DIFFERENCE (GOOGLE)
        if ($row['q1google'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1google'];
        }
        if ($row['q4google'] == NULL) {
            $google_difference = '-';
        } else if ($row['q4google'] == "0") {
            $yesterday         = 51;
            $google_difference = $yesterday - $today;
        } else {
            $yesterday         = $row['q4google'];
            $google_difference = $yesterday - $today;
        }
        if ($google_difference < 0) {
            echo '<td><span class="loss" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } elseif ($google_difference > 0) {
            echo '<td><span class="gain" title="' . $google_difference . '"></span>' . $google_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $google_difference . '</td>';
        }
        
        if ($row['q1bing'] != "0") {
            echo "<td><span title='" . $row['q1bing'] . "'></span>" . $row['q1bing'] . "</td>";
        } else {
            echo "<td><span title='51'></span>over 50</td>";
        }
        
        //YESTERDAY DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q2bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q2bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q2bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        
        //LAST WEEK DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q3bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q3bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q3bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        
        //LAST MONTH DIFFERENCE (BING)
        if ($row['q1bing'] == "0") {
            $today = 51;
        } else {
            $today = $row['q1bing'];
        }
        if ($row['q4bing'] == NULL) {
            $bing_difference = '-';
        } else if ($row['q4bing'] == "0") {
            $yesterday       = 51;
            $bing_difference = $yesterday - $today;
        } else {
            $yesterday       = $row['q4bing'];
            $bing_difference = $yesterday - $today;
        }
        if ($bing_difference < 0) {
            echo '<td><span class="loss" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } elseif ($bing_difference > 0) {
            echo '<td><span class="gain" title="' . $bing_difference . '"></span>' . $bing_difference . '</td>';
        } else {
            echo '<td><span title="0"></span>' . $bing_difference . '</td>';
        }
        if ($saved_keyword != $row['q1keyword']) {
            echo '<td>
            		<a class="more_details" id="' . $keyword_id . '"></a>
            		<a class="refresh_keyword" id="' . $row['q1keyword'] . '" the_main_site_id="' . $id . '" the_main_site="' . $row['site'] . '"></a>
            		<div class="keyword_charts" keyword="' . $row['q1keyword'] . '" the_main_site_id="' . $id . '" the_main_site="www.hubspot.com' . $row['site'] . '"></div>
            		<a class="remove_item" id="' . $row['q1keyword'] . '" the_main_site="' . $row['site'] . '" the_main_site_id="' . $id . '"></a>
            	</td>';
        } else {
            echo '<td></td>';
        }
        $saved_keyword = $row['q1keyword'];
    }
    if (isset($new_sites)) {
        foreach ($new_sites as $key => $new_site) {
            echo "<td>" . $new_site . "</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
            echo "<td>-</td>";
        }
    }
?>
                </tr>
              
            </tbody>
      </table>
        </div>
  </div>


<?
} else
    echo "Error displaying page.";
?>
