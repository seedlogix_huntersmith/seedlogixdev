<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$mode = $_GET['mode'] ? $_GET['mode'] : 'all';
$formID = is_numeric($_GET['formID']) ? $_GET['formID'] : 0;
$leadID = is_numeric($_GET['leadID']) ? $_GET['leadID'] : 0;
$reseller_site = $_GET['reseller_site'] ? 1 : 0;
$hubID = $reseller_site ? $_SESSION['main_site_id'] : '';

if($mode=='all' && $formID){
	$leads = $hub->getCustomFormLeads($_SESSION['user_id'], $formID, NULL, NULL, NULL, $hubID);
	$query = "SELECT name FROM lead_forms WHERE id = '".$formID."'";
	$a = $hub->queryFetch($query);
	$formName = $a['name'];
	$backLink = 'lead-form-prospects.php';
	if($reseller_site) $backLink .= '?reseller_site=1';
?>
	<h1><?=$formName?> - Responses</h1>
	<p>Click <i>View Details</i> on an item below to view the full response details.</p><br />
	<a href="<?=$backLink?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Lead Forms" /></a>
	<div id="message_cnt" style="display:none;"></div>
	<br /><br /><br />

<?
	if($leads && $hub->numRows($leads)){
		while($row = $hub->fetchArray($leads)){
		//get form data
		$query = "SELECT name, data FROM lead_forms WHERE id = '".$row['lead_form_id']."'";
		$form = $hub->queryFetch($query);
		//get hub data
		if($row['hub_id']){
			$query = "SELECT name, pages_version FROM hub WHERE id = '".$row['hub_id']."'";
			$hubInfo = $hub->queryFetch($query, NULL, 1);
			$hubName = $hubInfo['name'];
			
			$query = "SELECT hub_page_id FROM leads WHERE id = '".$row['id']."'";
			$pageID = $hub->queryFetch($query, NULL, 1);
			
			if($pageID['hub_page_id']){
				$pTable = 'hub_page';
				if($hubInfo['pages_version']==2) $pTable .= '2';
				$query = "SELECT page_title FROM `".$pTable."` WHERE id = '".$pageID['hub_page_id']."'";
				if($pageInfo = $hub->queryFetch($query)) $pageTitle = $pageInfo['page_title'];
			}
			

		}
		$leadData = explode('[==]', $row['data']);
		$b = explode('|-|', $leadData[0]);
		$dispTitle = $b[1].': '.$b[2];
		$timestamp = date('n/j/Y', strtotime($row['created'])).' at '.date('g:ia', strtotime($row['created']));
?>
	<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
		<li class="name" style="width:30%;"><?=$dispTitle?></li>
		<li class="phone" style="width:20%;"><i><?=$hubName?></i></li>
        <li class="phone" style="width:20%;"><i><? if($pageTitle){ ?><strong>Page:</strong> <?=$pageTitle?><? } ?></i></li>
		<li class="phone" style="width:15%;"><i><?=$timestamp?></i></li>
		<li class="details" style="width:5%;"><a href="inc/forms/view-custom-leads.php?mode=single&leadID=<?=$row['id']?>"><img src="img/v3/view-details.png" border="0" /></a></li>
		<li class="delete" style="width:5%;"><a href="#" class="delete rmParent" rel="table=leads&id=<?=$row['id']?>&undoLink=hub-forms"><img src="img/v3/x-mark.png" border="0" /></a></li>
	</ul>
<?
		} //end while loop
	} //end if($leads && mysqli_num_rows($leads))
	else echo '<p><strong>No responses to this form yet.</strong></p>';
} //end if($mode=='all' && $formID)

else if($mode=='single' && $leadID){
	$lead = $hub->getCustomFormLeads($_SESSION['user_id'], NULL, $leadID);
	if($lead){
		$query = "SELECT name FROM lead_forms WHERE id = '".$lead['lead_form_id']."'";
		$form = $hub->queryFetch($query);
		$leadData = explode('[==]', $lead['data']);
		$backLink = 'inc/forms/view-custom-leads.php?mode=all&formID='.$lead['lead_form_id'];
		if($reseller_site) $backLink .= '&reseller_site=1';
		//get hub info
		$query = "SELECT name, pages_version FROM hub WHERE id = '".$lead['hub_id']."'";
		$hubInfo = $hub->queryFetch($query);
		if($lead['hub_page_id']){
			$pTable = 'hub_page';
			if($hubInfo['pages_version']==2) $pTable .= '2';
			$query = "SELECT page_title FROM `".$pTable."` WHERE id = '".$lead['hub_page_id']."' LIMIT 1";
			if($pageInfo = $hub->queryFetch($query)) $pageTitle = $pageInfo['page_title'];
		}
		
	?>
    <script type="text/javascript">
$(function(){
	
	$(".contactPush").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>

    	<a href="#contactsContents" class="greyButton contactPush" style="float:right;margin-top:15px;" >Push To Contacts</a>
        
		        <div style="display: none;">
				<div id="contactsContents" style="width:640px;height:200px;overflow:auto;">
					<div id="createDir">
                    <h3>Enter First & Last Name</h3><br />
	<form method="post" action="inc/forms/add-leadto-contact.php" class="ajax">
		<input type="text" name="name" value="">
        <input type="hidden" name="leadID" value="<?=$leadID?>"  />
		<input type="submit" value="Push Lead" name="submit" />
	</form>
    
    <div id="sugar_success"><div class="sugar_container"></div></div>
</div>
</div>
</div>
        
		<h1><?=$form['name']?></h1>
		<strong><?=date('l F jS, Y', strtotime($lead['created']))?></strong> at <strong><?=date('g:ia', strtotime($lead['created']))?></strong> from IP address <strong><?=$lead['ip']?></strong><br />
		Submitted from the 
		<? if($pageTitle){ ?><strong><?=$pageTitle?></strong> page on the <? } ?>
		<strong><?=$hubInfo['name']?></strong> hub
		<br /><br />
		<a href="<?=$backLink?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Leads" /></a>
        
		<br /><br /><br />
	<?
		if($leadData){
			echo '<div class="contactDetails">
						<div class="outer-rounded-box-bold">
						<div class="simple-rounded-box">
						<h2>Lead Details</h2>';
			foreach($leadData as $key=>$value){
				$fieldData = explode('|-|', $value);
				echo '
						<p><strong>'.$fieldData[1].'</strong>: 
										'.$fieldData[2].'
						</p>';
			}
			echo '</div></div></div>';
			echo '<div class="contactDetails">
						<div class="outer-rounded-box-bold">
						<div class="simple-rounded-box">
						
						<p><small><strong>HUB Name:</strong> '.$hubInfo['name'].'</small></p>
					<p><small><strong>Page Submitted:</strong> '.$pageTitle.'</small></p>
					<p><small><strong>Search Engine:</strong> '.$lead['search_engine'].'</small></p>
					<p><small><strong>Keyword Searched:</strong> '.$lead['keyword'].'</small></p>
					</div></div></div>';
			
		}
	}
}
else echo 'Validation error.';
echo '<br style="clear:both;" />';
?>