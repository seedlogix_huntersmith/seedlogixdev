<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once('../auth.php');

//initiate the class
require_once('../contacts.class.php');
$contacts = new Contacts();

$name = $contacts->validateText($_POST['name']);
$name_array = explode(' ', $name);
$last = $name_array[count($name_array)-1];
$name_array[count($name_array)-1] = '';
$first = trim(implode(' ', $name_array));
$leadID = $_POST['leadID'];

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$lead = $hub->getCustomFormLeads($_SESSION['user_id'], NULL, $leadID);

if($name){
		$contactID = $contacts->addNewLeadtoContact($_SESSION['user_id'], $_SESSION['campaign_id'], $first, $last, $lead['data'], $lead['lead_email']);
		
		if($contactID){
			$html = 
			'<br style="clear:both;" /><br />
				<div class="sugar_response">'.$first.' '.$last.' has been pushed to Contacts.</div>';
			
			$response['success']['id'] = '1';
			$response['success']['container'] = '#sugar_success .sugar_container';
			$response['success']['message'] = $html;
		}
	}
	echo json_encode($response);
?>