<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
require_once(QUBEADMIN . 'inc/Authnet.class.php');
$reseller = new Reseller();
$anet = new Authnet();

$id = $_GET['id'] ? $_GET['id'] : $specID;
$id = is_numeric($id) ? $id : '';

$query = "SELECT id, admin_user, payments, payments_acct, payments_acct2  
		FROM resellers 
		WHERE admin_user = '".$id."'";
$user = $reseller->queryFetch($query);
$query = "SELECT parent_id, billing_id, billing_id2, class FROM users WHERE id = '".$id."'";
$user2 = $reseller->queryFetch($query);

if(($_SESSION['reseller'] && ($id==$_SESSION['login_uid'])) || $_SESSION['admin']){
	$isReseller = true;
	$continue = true;
	if($id==$_SESSION['login_uid']){
		$isSelf = true;
	}
	if($user['payments']=='authorize' || $user['payments']=='6qube'){
		$processor = 'authorize';
	}
}
else if(($_SESSION['user_id']==$id) || ($_SESSION['reseller'] && $_SESSION['login_uid']==$user2['parent_id'])){
	if(!$_SESSION['reseller']) $isUser = true;
	$continue = true;
	//check if user is a reseller client
	if($user2['parent_id'] && $user2['parent_id']!=7 && !$_SESSION['brandoverride']){
		$query = "SELECT payments, payments_acct, payments_acct2 FROM resellers WHERE admin_user = '".$user2['parent_id']."'";
		$resellerInfo = $reseller->queryFetch($query);
		if($resellerInfo['payments']=='authorize'){
			$apiLogin = $resellerInfo['payments_acct'];
			$apiAuth = $resellerInfo['payments_acct2'];
			$processor = 'authorize';
		}
		else if($resellerInfo['payments']=='6qube'){
			$processor = 'authorize';
		}
		else if($resellerInfo['payments']=='paypal'){
			$processor = 'paypal';
			/////////////////////////
			//PAYPAL GOES HERE
			/////////////////////////
		}
		$sixqube = 0;
	}
	else if($user2['parent_id']==7 || $_SESSION['brandoverride']) {
		$processor = 'authorize';
		$sixqube = 1;
		if(!$_SESSION['admin']) $clientBillingTab = true;
	}
	//check if user has any items which will be charged after they set up a profile
	if($charges = $reseller->chargesDueOnSetup($id, $sixqube)){
		$chargeAmount = $charges['totalAmount'];
	}
}

if($continue){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
    
<?
if(!$clientBillingTab){
	//section navigation
	$thisPage = 'billing';
	include('edit-user-nav.php');
}
?>
<br style="clear:left;" />
<br />
	<h1>User #<?=$id?> Billing Info</h1>
<div style="width:100%">

<?
if($processor=='authorize' || ($_SESSION['reseller'] && $isSelf)){
	if((!$user2['billing_id2'] && !$sixqube) || ($sixqube && $user2['billing_id'] && $user2['billing_id']<2000)){
?>
<h2>Please take a moment to create a payment profile</h2>
This information only needs to be entered once and is securely stored by our third-party payment processor.<br />You may update it here at any time after creating your profile.<br />
<?
if($chargeAmount){
	echo '<br /><strong>Note:</strong><br />Once you set up your profile your card will be charged $'.$chargeAmount.' for your first month and any setup fees if applicable.';
}
?>
<br /><br />
<? if(!$sixqube){ ?>
<a href="#" id="newBillingProfile" class="dflt-link" rel="<?=$id?>" title="create">Click here to create your profile</a>
<? } else if($sixqube && $user2['class']>20) { ?>
<a href="#" id="update6QubeUserProfile" class="dflt-link" rel="<?=$id?>" title="">Click here to create your profile</a>
<? } else { ?>
Sorry, this feature isn't available for your account type or must be set up manually by our support team.
<? } 
} else { 
	$profileInfo = $anet->getPaymentProfile($id, $apiLogin, $apiAuth);
	if(!$profileInfo['success']){
		echo $profileInfo['message'];
	}
	else {
?>
<form class="jquery_form">
<ul>
	<li>
	<label>First Name</label>
	<?=$profileInfo['info']['firstName']?>
	</li>
	<li>
	<label>Last Name</label>
	<?=$profileInfo['info']['lastName']?>
	</li>
	<li>
	<label>Company</label>
	<?=$profileInfo['info']['company']?>
	</li>
	<li>
	<label>Address</label>
	<?=$profileInfo['info']['address']?>
	</li>
	<li>
	<label>City</label>
	<?=$profileInfo['info']['city']?>
	</li>
	<li>
	<label>State</label>
	<?=$profileInfo['info']['state']?>
	</li>
	<li>
	<label>Zip</label>
	<?=$profileInfo['info']['zip']?>
	</li>
	<li>
	<label>Phone</label>
	<?=$profileInfo['info']['phone']?>
	</li><br />	
	<li>
	<label>Card Number <small style="color:#666;">(last 4)</small></label>
	<?='************'.substr($profileInfo['info']['cardNum'], -4)?>
	</li><br /><br />
	<li>
	<? if(!$sixqube){ ?>
		<a href="#" id="newBillingProfile" class="dflt-link" rel="<?=$id?>" title="update">Click here to update your profile</a>
	<? } else if($sixqube) { ?>
		<a href="#" id="update6QubeUserProfile" class="dflt-link" rel="<?=$id?>" title="">Click here to update your profile</a>
	<? } ?>
	</li>
</ul>
</form>
<?
			} //end else [getProfile success]
		} //end else [billing_id2 set]
	} //end if($processor=='authorize')
	else if($processor=='paypal'){
		
		//////////////////////
		// PAYPAL GOES HERE
		//////////////////////
		
	}
?>
</div>
<br style="clear:both;" />
<? } else echo "Not authorized. ";//.$user['id'].' = '.$_SESSION['login_uid']; ?>