<?
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/analytics.class.php');
	
	//create new instance of class
	$connector = new Analytics();
	
	$id = $_GET['id'];
	
	if(!$id){
		echo "<br /><br />Use the drop-down above to select which blog's analytics to view...<br /><br />";
	}
	else{	
		$anal_id = isset($_GET['aid']) ? $_GET['aid'] : $connector->ANAL_getAnalyticsId('blogs', $id);
		$form = isset($_GET['form']) ? $_GET['form'] : "snapshot";
		$period = $_GET['period'] ? $_GET['period'] : 'day';
		$bold = 'style="font-weight:bold;"';
		$lbase = 'inc/forms/blog_analytics_view.php?id='.$id.'&aid='.$anal_id.'&form='.$form.'&period=';
		
		if($_GET['custom_date']){
			//make sure passed values are numeric
			$year = is_numeric($_GET['year']) ? $_GET['year'] : '';
			$month = is_numeric($_GET['month']) ? $_GET['month'] : '';
			$day = is_numeric($_GET['day']) ? $_GET['day'] : '';
			$date = $year.'-'.$month.'-'.$day;
			$lbase .= '&custom_date=true&year='.$year.'&month='.$month.'&day='.$day.'&period=';
		}
		else {
			if($period=='day'){
				if($form=='snapshot') $date = 'today';
				else $date = 'yesterday';
			}
			else if($period=='week') $date = date('Y-m-d', strtotime('-7 days'));
			else if($period=='month') $date = date('Y-m', strtotime('-1 month'));
			else if($period=='year') $date = date('Y', strtotime('-1 year'));
			$year = date('Y');
			$month = date('m');
			$day = date('d');
			$lbase .= '&period=';
		}
		
		function date_picker($thisPage = 'blog_analytics_view'){
			global $id, $anal_id, $form, $period, $year, $month, $day;
			$months = array('','January','February','March','April','May','June','July', 
						'August','September','October','November','December');
			$s = ' selected="selected"';
			echo
			'<div id="createDir"><form class="datePicker" rel="inc/forms/'.$thisPage.'.php?id='.$id.'&aid='.$anal_id.'&form='.$form.'&period='.$period.'&">
				<select name="month" class="uniformselect" >';
				for($i=1;$i<=12;$i++){
					echo '<option value="'.$i.'" '; if($month==$i) echo $s; echo '>'.$months[$i].'</option>';
				}
			echo 
				'</select>
				<select name="day" class="uniformselect">';
				for($i=1;$i<=31;$i++){
					echo '<option value="'.$i.'" '; if($day==$i) echo $s; echo '>'.$i.'</option>';
				}
			echo
				'</select>
				<select name="year" class="uniformselect">
					<option value="'.(date('Y')-1).'" '; if($year==(date('Y')-1)) echo $s; echo '>'.(date('Y')-1).'</option>
					<option value="'.date('Y').'" '; if($year==date('Y')) echo $s; echo '>'.date('Y').'</option>';
			echo 
				'</select>
				<input type="hidden" value="true" name="custom_date" />
				<input type="submit" value="Custom Date" name="submit" />
			</form></div>';
		}
?>
<script language="JavaScript">
$(".uniformselect, input[type=file]").uniform();
$('.datePicker').die();
$('.datePicker').live('submit', function(event){
	event.stopPropagation();
	var params = $(this).attr('rel')+$(this).serialize();
	$.get(params, function(data){
		$("#ajax-load").html(data);
	});
	return false;
});

</script>
<script type="text/javascript" src="http://analytics.6qube.com/js/jquery.ba-postmessage.min.js"></script>
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/blog_analytics_view.php?form=snapshot&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="snapshot") echo 'class="current"'; ?>>Snap Shot</a></li>
		<li><a href="inc/forms/blog_analytics_view.php?form=activity&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="activity") echo 'class="current"'; ?>>Activity</a></li>
		<li><a href="inc/forms/blog_analytics_view.php?form=referers&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="referers") echo 'class="current"'; ?>>Referers</a></li>
		<li><a href="inc/forms/blog_analytics_view.php?form=search&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="search") echo 'class="current"'; ?>>Searches</a></li>
        <li><a href="inc/forms/blog_analytics_view.php?form=campaigns&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="Campaigns") echo 'class="current"'; ?>>Campaigns</a></li>
		<li class="last"><a href="inc/forms/blog_analytics_view.php?form=visitors&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="visitors") echo 'class="current"'; ?>>Visitors</a></li>
	</ul>
</div>
<br style="clear:left;" />

<? if($form=="snapshot"){ ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>
	<h2>Traffic Snap Shot</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

    
<? } else if($form=="activity") { ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                 &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>
     

    
	<h2>Activity</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageTitles&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>
 </div>    
 <div  >      
	<h2>Links Clicked On</h2>
    

 <div id="iframe2"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getOutlinks&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe2' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

<? } else if($form=="referers") { ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                 &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>
	<h2>Referers</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getWebsites&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

<? } else if($form=="search") { ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                 &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>
	<h2>Search Engines</h2>
	 <div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getSearchEngines&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

<? } else if($form=="campaigns") { ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>	
	
   
	<h2>List of Campaigns</h2>


 <div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getCampaigns&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>


<? } else if($form=="visitors") { ?>
<br />
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
                 &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="#" class="dflt-link" id="showCustomPageTags" <? if($period=='custom_date') echo $bold; ?>>Custom</a>
                <div id="availableTags" style="display:none;line-height:25px;">
                <br />
				<? date_picker() ?>
                <br style="clear:both;" /><br/>
                </div>
	<h2>Visits</h2>


	<div id="widgetIframe">
		
 <div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<h2>Returning Visitors</h2>
	<div id="widgetIframe">
		 <div id="iframe2"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitFrequency&actionToWidgetize=getSparklines&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe2' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<h2>Visits by Time</h2>
	<div id="widgetIframe">
		<div id="iframe3"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitTime&actionToWidgetize=getVisitInformationPerLocalTime&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe3' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<div class="mini_graph">
		<h2>Length of Visits</h2>
		<div id="widgetIframe">
			<div id="iframe4"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerVisitDuration&viewDataTable=graphPie&idSite=<?=$anal_id?>&period=year&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe4' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>


		</div>
	</div>
    
    <div class="mini_graph">
		<h2>Visitor Specs</h2>
		<div id="widgetIframe">

			<div id="iframe5"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getConfiguration&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe5' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

		</div>
	</div>
    
<br style="clear:both;" />

		
        <h2>Pages Per Visit</h2>
		<div id="widgetIframe">

			<div id="iframe6"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage&viewDataTable=graphEvolution&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe6' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>


		</div>
 
	
<? } //end else if($form=="visitors")  { ?>
<? } //end else { at top of page ?>
<br style="clear:both;" /><br />