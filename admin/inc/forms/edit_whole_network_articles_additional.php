<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/local.class.php');
$local = new Local();
$parent_id = $_GET['id'];
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

//get site info
$query = "SELECT 
			id, admin_user, name, domain, free_link, phone, twitter, 
			facebook, youtube, linkedin, blog, theme
		FROM resellers_network 
		WHERE parent = ".$parent_id." AND type = 'articles' LIMIT 1";
$row = $local->queryFetch($query);

//prepare domain value for Google note below
$a = explode('.', $row['domain']);
$domain2 = $a[count($a)-2].'.'.$a[count($a)-1];

$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
$themeInfo = $local->queryFetch($query);
	
if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "articles";
include('edit_whole_network_nav.php');
?>
<br style="clear:left;" />
<div id="form_test" >
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Articles Additional Settings</h2>
	
	
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<h2>Additional Settings:</h2>
			
			<li>
				<label>Free Account Link</label>
				<input  type="text" name="free_link" value="<?=$row['free_link']?>" />
			</li>
			<li>
				<label>Phone</label>
				<input  type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Twitter URL</label>
				<input  type="text" name="twitter" value="<?=$row['twitter']?>" />
			</li>
			<li>
				<label>Facebook URL</label>
				<input  type="text" name="facebook" value="<?=$row['facebook']?>" />
			</li>
			<li>
				<label>YouTube URL</label>
				<input type="text" name="youtube" value="<?=$row['youtube']?>" />
			</li>
			<li>
				<label>LinkedIn URL</label>
				<input  type="text" name="linkedin" value="<?=$row['linkedin']?>" />
			</li>
			<li>
				<label>Blog URL</label>
				<input  type="text" name="blog" value="<?=$row['blog']?>" />
			</li>
			
		</ul>
	</form><br />
	
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_articles_nav.php');
?>
	
						
</div>
<!--End APP Right Panel-->

<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>