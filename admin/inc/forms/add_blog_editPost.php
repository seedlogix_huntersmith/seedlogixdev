<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$row = $blog->getSinglePost($_SESSION['current_id']);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
		$post_results = $blog->getBlogPost($row['blog_id']);
		echo '<div id="dash" title="'.$_SESSION['current_id'].'"><div id="list_blogposts" title="item_list" class="dash-container"><div class="container">';
		if($blog->getPostRows()){
			//loop through and out the blogs post (limits 5)
			while($post_row = $blog->fetchArray($post_results)){
				echo '<div class="item2">
						<div class="icons">
							<a href="blog.php?form=editPost&id='.$post_row['id'].'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete" rel="table=blog_post&id='.$post_row['id'].'"><span>Delete</span></a>
						</div>';
							if($_SESSION['theme']) echo '<img src="img/blog-post/'.$_SESSION['thm_buttons-clr'].'.png" class="blogPost" />';
							else echo '<img src="img/blog-post.png" class="icon" />';
						echo '<a href="blog.php?form=editPost&id='.$post_row['id'].'">'.$post_row['post_title'].'</a>
					</div>';
			}
		}
		echo '<br /><a href="blog.php?form=post&id='.$row['blog_id'].'" style="margin-left:110px;" class="dflt-link">New Post</a>';
		echo '</div></div></div>';
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<div id="message-container"></div>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "post";
	$parentID = $row['blog_id'];
	include('add_blog_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<form name="blog_post" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Title</label>
				<input type="text" name="post_title" value="<?=$row['post_title']?>" />
			</li>
			<li>
				<label>Category</label>
				<input type="text" name="category" value="<?=$row['category']?>" />
			</li>
			<li>
				<label>Post</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="post_content" rows="6" id="edtr1"><?=$row['post_content']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Tags</label
				><input type="text" name="tags" value="<?=$row['tags']?>" />
			</li>
			<li>
				<label>Tags URL</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Make sure you include http:// in the link.</div>
				<input type="text" name="tags_url" value="<?=$row['tags_url']?>" />
			</li>
            <li>
				<label>SEO Title *optional</label>
				<input type="text" name="seo_title" value="<?=$row['seo_title']?>" />
			</li>
			<li>
				<label>SEO Description</label>
				<input type="text" name="post_desc" value="<?=$row['post_desc']?>" />
			</li>
			<li>
				<label>Include in network?</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Change this to "Yes" to publish this blog post to our network.</div>
				<select name="network" title="select" class="uniformselect">
					<option value="0" <? if(!$row['network']) echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($row['network']) echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
		</ul>
        
		<input type="hidden" name="blog" value="<?=$row['blog_id']?>" />
		<input type="hidden" name="edit" value="<?=$row['id']?>" />
	</form>
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="blog_post" />
		<input type="hidden" name="fname" value="photo" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['photo']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/blog_post/'.$row['photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Edit Blog Post Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogposteditVideo" class="trainingvideo">Edit Blog Post Video</a>
			</li>
			<div style="display:none;">
				<div id="blogposteditVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blog-postedit-overview.flv" style="display:block;width:640px;height:480px" id="blogposteditplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogposteditplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="external" target="_new">Common HTML Tags</a>
			</li>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new">Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
	</div><!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>