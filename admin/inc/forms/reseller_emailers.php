<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
if($emailers = $reseller->getResellerEmailers($_SESSION['login_uid'])){
	$numEmailers = count($emailers);
}
if($_SESSION['reseller'] || $_SESSION['admin']){
	if($_GET['form']){
		$form = $_GET['form'];
		include('reseller_emailers_'.$form.'.php');
	}
	else{
		if(!$noForm){
			echo '<div id="createDir">
					<form method="post" class="ajax" action="add-reseller-emailer.php">
						<input type="text" name="name" />
						<input type="submit" value="Create New Emailer" name="submit" />
					</form>
				</div>';
			echo '<div id="message_cnt"></div>';
		}
		
		if($numEmailers){ //if user has any emailers
			echo '<div id="dash"><div id="fullpage_reseller-emailers" class="dash-container"><div class="container">';
			foreach($emailers as $key=>$row){ //loop through and display links to each
				echo '<div class="item2">
						<div class="icons">
							<a href="inc/forms/reseller_emailers.php?form=settings&id='.$key.'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete" rel="table=resellers_emailers&id='.$key.'"><span>Delete</span></a>
						</div>';
					echo '<img src="img/field-email.png" class="icon" />';
					echo '<a href="inc/forms/reseller_emailers.php?form=settings&id='.$key.'">'.$row['name'].'</a>
					</div>';
			}
			echo '</div></div></div>';
		}
		
		else{ //if no autoresponders
			echo '<div id="dash"><div id="fullpage_emailers" class="dash-container"><div class="container">';
			echo 'Use the form above to create a new email template.<br />';
			echo '</div></div></div>';
		}
	}
} //end if($_SESSION['reseller'])
?>