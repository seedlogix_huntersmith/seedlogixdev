<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/blogs.class.php');
	$blog = new Blog();
	
require_once '../../6qube/core.php';
require_once QUBEPATH . '../classes/VarContainer.php';
require_once QUBEPATH . '../classes/QubeDataProcessor.php';
require_once QUBEPATH . '../classes/ContactFormProcessor.php';
require_once QUBEPATH . '../models/Response.php';
require_once QUBEPATH . '../classes/Notification.php';
require_once QUBEPATH . '../classes/NotificationMailer.php';
require_once QUBEPATH . '../models/Responder.php';
require_once QUBEPATH . '../models/AutoResponderTheme.php';
require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';
	
	$_SESSION['current_id'] = $_GET['id'];
	
//	$results = $blog->getAutoResponders($current_id, 'blogs', $_SESSION['user_id']);
//	$ar_count = $results ? $blog->numRows($results) : 0;
	
// @todo implement a 'deleted' column in auto_responders table so that we dont have to use a 'trash' subquery.
$statement = Qube::GetDriver()->queryRespondersWhere('table_id = ? AND table_name = ? AND user_id = ? AND NOT EXISTS (SELECT trash.table_id FROM trash WHERE trash.table_name = "auto_responders" AND trash.table_id = T.id)')        
                        ->addFields('T.name')
                        ->OrderBy('sched*(CASE sched_mode WHEN "weeks" THEN 7*24 WHEN "months" THEN 30*24 WHEN "days" THEN 1*24 ELSE 1 END) ASC,
                                    sched_mode = "hours" DESC, sched_mode = "days" DESC, sched_mode = "weeks" DESC, sched_mode = "months" DESC')->Prepare();

//if($_SESSION['user_id'] == 6610) deb($_SESSION);

$results = $statement->execute(array($_SESSION['current_id'], 'blogs', $_SESSION['user_id']));

$responders = $statement->fetchAll();

$ar_count = $results ? count($responders) : 0;

/**
 * Some of this stuff makes my head spin!
 * I'm afraid to touch it and break something but
 * it should be re-coded to make it more clear.
 */
	//check responder limits for type/campaign
	if($_SESSION['user_limits']['responder_limit']==-1)
		$respondersRemaining = "Unlimited";
	else {
		$numResponders = $blog->getAutoResponders(NULL, 'blogs', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
		$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
		if($respondersRemaining<0) $respondersRemaining = 0;
	}
	//email limits
	$emailsRemaining = $blog->emailsRemaining($_SESSION['user_id']);
	if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
	else $emailsRemaining = number_format($emailsRemaining);
	
	if($_GET['form']){
		$form = $_GET['form'];
		include('autoresponse-'.$form.'.php');
	}	
	else{
		if($_GET['mode']=='nav'){
		$thisPage = "blogauto";
		include('auto_nav.php');
		}
		echo '<div id="createDir">
				<form method="post" class="ajax" action="add-autoresponder2.php">
					<input type="hidden" name="table_id" value="'.$_SESSION['current_id'].'" />
					<input type="hidden" name="table_name" value="blogs" />
					<input type="hidden" name="type" value="blog" />
					<input type="text" name="name" ';
					if((is_numeric($respondersRemaining) && $respondersRemaining==0)) 
					echo 'readonly';
		echo			' />
					<input type="submit" value="Create New Email" name="submit" />
				</form>
			</div>';
		
		if($ar_count){ //if user has any autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
//			while($row = $hub->fetchArray($results)){ //loop through and display links to each
                        foreach($responders as $responder)
                        {
                            $row = $responder->toArray();
				echo '<div class="item3">
						<div class="icons">
							<a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete rmParent" rel="table=auto_responders&id='.$row['id'].'&undoLink=auto_responders"><span>Delete</span></a>
						</div>';
					echo '<img src="img/field-email.png" class="icon" />';
					echo '<div class="name"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a></div>
						  <div class="type"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched'].'</a></div>
						  <div class="date"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched_mode'].'</a></div>
					</div>';
			}
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> blog auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
		
		else { //if no autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			echo 'Use the form above to create a new auto responder.<br />';
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> blog auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
	}
?>