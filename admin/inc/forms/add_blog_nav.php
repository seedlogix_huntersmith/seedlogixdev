<?php 
	session_start();
	if(!defined('QUBEADMIN'))
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	$curr = 'class="current"';
	$navID = $thisPage=="post" ? $parentID : $_SESSION['current_id'];
	$postForm = $postsV2 ? 'post2' : 'post';
?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="blog.php?form=settings&id=<?=$navID?>" <? if($thisPage=="settings") echo $curr; ?>>Settings</a></li>
		<li><a href="blog.php?form=theme&id=<?=$navID?>" <? if($thisPage=="theme") echo $curr; ?>>Theme</a></li>
		<li><a href="blog.php?form=seo&id=<?=$navID?>" <? if($thisPage=="seo") echo $curr; ?>>SEO</a></li>
        <li><a href="blog.php?form=edit&id=<?=$navID?>" <? if($thisPage=="edit") echo $curr; ?>>Edit Regions</a></li>
		<li><a href="blog.php?form=social&id=<?=$navID?>" <? if($thisPage=="social") echo $curr; ?>>Social</a></li>
		<li class="last"><a href="blog.php?form=<?=$postForm?>&id=<?=$navID?>" <? if($thisPage=="post") echo $curr; ?>>Posts</a></li>
	</ul>
</div>
