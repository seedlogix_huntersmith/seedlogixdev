<?
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/blogs.class.php');
	
	//create new instance of class
	$blog = new Blog();
	
	$blog_id = $_GET['id'];
	
	
	$row = $blog->emailForm($blog_id, 'blogs');
	
	if(!$row){
		echo '<br /><br /><p>Use the drop down box above to select which Hub\'s email settings you would like to configure.</p>';
	}
	else{ ?>
	<h1>Email Form</h1>
<form name="email_settings" class="jquery_form" id="<?=$row['id']?>">
	<ul>
        <li><label>From:</label>
        <span style="color:#666; font-size:75%">Should be in this format: Company Name &lt;email@company.com&gt;</span>
        <input type="text" name="from_field" value="<?=$row['from_field']?>" /></li>
        <li><label>Subject:</label><input type="text" name="subject" value="<?=$row['subject']?>" /></li>
    	<li><label>Message:</label><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><textarea name="message" rows="15" cols="80" id="edtr1"><?=$row['message']?></textarea><br style="clear:left" /></li>
        <li><label>Return URL:</label>
        <span style="color:#666; font-size:75%">Optional: Specify the URL a prospect is taken to after submitting the contact form</span>
        <input type="text" name="return_url" value="<?=$row['return_url']?>" /></li>
	</ul>
    <span style="font-size:9.5px;">Hint: You can use "#name" (without quotes) in the body or subject and it will automatically be replaced with the prospect's name when the email is sent.</span>
</form>
<?	
	}
?>