<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$uid = $_SESSION['login_uid'];
	$type = $_GET['type'];
	$id = $_GET['id'];
	
	$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
	$row = $local->queryFetch($query);
	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "add";
include('edit_network_site_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h1>Additional - <?=$row['name']?></h1>
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Free Account Link</label>
				<input  type="text" name="free_link" value="<?=$row['free_link']?>" />
			</li>
			<li>
				<label>Phone</label>
				<input  type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Twitter URL</label>
				<input  type="text" name="twitter" value="<?=$row['twitter']?>" />
			</li>
			<li>
				<label>Facebook URL</label>
				<input  type="text" name="facebook" value="<?=$row['facebook']?>" />
			</li>
			<li>
				<label>YouTube URL</label>
				<input type="text" name="youtube" value="<?=$row['youtube']?>" />
			</li>
			<li>
				<label>LinkedIn URL</label>
				<input  type="text" name="linkedin" value="<?=$row['linkedin']?>" />
			</li>
			<li>
				<label>Blog URL</label>
				<input  type="text" name="blog" value="<?=$row['blog']?>" />
			</li>
		</ul>
	</form>
	
	<br />
	
</div>
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>