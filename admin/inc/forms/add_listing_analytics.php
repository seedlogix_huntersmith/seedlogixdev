<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT * FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$result = $directory->query($query, $dbName);
	if($result) $row = $directory->fetchArray($result);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="form-nav">
		<li class="first"><a href="directory.php?form=listings&id=<?=$_SESSION['current_id']?>">Listing Info</a></li>
		<li><a href="directory.php?form=seo&id=<?=$_SESSION['current_id']?>">SEO</a></li>
		<li><a href="directory.php?form=edit&id=<?=$_SESSION['current_id']?>">Edit Regions</a></li>
		<li><a href="directory.php?form=gallery&id=<?=$_SESSION['current_id']?>">Photo Gallery</a></li>
		<li><a href="directory.php?form=social&id=<?=$_SESSION['current_id']?>">Social Networking</a></li>
		<li class="last"><a href="directory.php?form=analytics&id=<?=$_SESSION['current_id']?>" class="current">Analytics</a></li>
	</ul>
</div>
<br style="clear:left;" />
<!-- Text Input -->
<div id="form_test">
    <h2>Analytics</h2>
    	<? $anal = new Analytics() ?>
    	<? if($anal_id = $anal->ANAL_getAnalyticsId('directory', $_GET['id'])): ?>
			<div id='widgetIframe'>
				<iframe width='100%' height='150' src='http://6qube.com/analytics/index.php?module=Widgetize&action=iframe&columns[]=nb_uniq_visitors&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite={$anal_id}&period=day&date=last30&disableLink=1' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe>
			</div>
			
			<div id='widgetIframe'><iframe width='100%' height='150' src='http://6qube.com/analytics/index.php?module=Widgetize&action=iframe&columns[]=bounce_count&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite={$anal_id}&period=day&date=last30&disableLink=1' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe></div>
			
			<div id='widgetIframe'><iframe width='100%' height='150' src='http://6qube.com/analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getWebsites&idSite={$anal_id}&period=day&date=2010-03-21&disableLink=1' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe></div>
			<div id='widgetIframe'><iframe width='100%' height='200' src='http://6qube.com/analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getKeywords&idSite={$anal_id}&period=day&date=2010-03-21&disableLink=1' scrolling='no' frameborder='0' marginheight='0' marginwidth='0'></iframe></div>
		<? endif ?>
</div>
<br style="clear:both;" />
<? } ?>