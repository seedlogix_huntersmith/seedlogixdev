<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
if($_GET['mode']=='noteedit'){
	$noteid = is_numeric($_GET['noteid']) ? $_GET['noteid'] : '';
	$contactID = is_numeric($_GET['contactID']) ? $_GET['contactID'] : '';
	include_once(QUBEADMIN . 'inc/forms/edit-note.php');
}
else if($_GET['mode']=='noteview'){
	$noteid = is_numeric($_GET['noteid']) ? $_GET['noteid'] : '';
	$contactID = is_numeric($_GET['contactID']) ? $_GET['contactID'] : '';
	include_once(QUBEADMIN . 'inc/forms/view-note.php');
}
else {	
	require_once(QUBEADMIN . 'inc/contacts.class.php');
	$contacts = new Contacts();
	if($id){
	$row = $contacts->getContacts($_SESSION['user_id'], $_SESSION['campaign_id'], $id);
	}
	$leadData = explode('[==]', $row['lead_data']);
	//get notes
	$notesView = $contacts->getNotes($_SESSION['user_id'], $_SESSION['campaign_id'], $id, NULL);
	
	//see if user activated SugarCRM
	$user_id = $_SESSION['user_id'];
	$query = "SELECT * FROM sugarcrm WHERE user_id = '".$user_id."' 
			  AND cid = '".$_SESSION['campaign_id']."' LIMIT 1";	
	$allowSugar = $contacts->queryFetch($query);	
	$SugarID = $allowSugar['id'];
	
	//see if user activated Salesforce
	$query = "SELECT * FROM salesforce WHERE user_id = '".$user_id."' 
			  AND cid = '".$_SESSION['campaign_id']."' LIMIT 1";	
	$allowSalesforce = $contacts->queryFetch($query);	
	$SalesforceID = $allowSalesforce['id'];

	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	
	$(".contactPush").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>

<a href="contacts.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Contacts" /></a>
<? if($SugarID){ ?>
<a href="#sugarContents" class="contactPush" ><img style="padding:15px 0 0 15px;float:right;" src="img/v3/sugarcrm.gif" border="0" alt="Push <?=$row['first_name']?> <?=$row['last_name']?> To SugarCRM" /></a>
<div style="display: none;">
  <div id="sugarContents" style="width:640px;height:200px;overflow:auto;">
    <div id="createDir">
      <h3>SugarCRM Username for Transfer</h3>
      <br />
      <form method="post" action="inc/api/sugarcrm/sugar-lead-push.php" class="ajax">
        <input type="text" name="assigned_user_id" value="">
        <input type="hidden" name="contactID" value="<?=$id?>"  />
        <input type="submit" value="Push Lead" name="submit" />
      </form>
      <div id="sugar_success">
        <div class="sugar_container"></div>
      </div>
    </div>
  </div>
</div>
<? } ?>
<? if($SalesforceID){ ?>
<a href="#salesforceContents" class="contactPush" ><img style="padding:15px 0 0 15px;float:right;" src="img/v3/salesforce.png" border="0" alt="Push <?=$row['first_name']?> <?=$row['last_name']?> To Salesforce.com" /></a>
<div style="display: none;">
  <div id="salesforceContents" style="width:640px;height:200px;overflow:auto;">
    <div id="createDir">
      <h3>Salesforce.com Username for Transfer</h3>
      <br />
      <form method="post" action="inc/api/salesforce/salesforce-lead-push.php" class="ajax">
        <input type="text" name="assigned_user_id" value="">
        <input type="hidden" name="contactID" value="<?=$id?>"  />
        <input type="submit" value="Push Lead" name="submit" />
      </form>
      <div id="salesforce_success">
        <div class="salesforce_container"></div>
      </div>
    </div>
  </div>
</div>
<? } ?>
<br style="clear:both;" />
<br />
<? if($row['lead_data']){ 
			   echo '<div class="leadDetails">
									<div class="outer-rounded-box-bold">
									<div class="simple-rounded-box">
									<h2>Form Response</h2>';
					foreach($leadData as $key=>$value){
						$fieldData = explode('|-|', $value);
						echo '
								<p><strong>'.$fieldData[1].'</strong>: 
												'.$fieldData[2].'
								</p>';
					}
					echo '</div></div></div>';
					
	
    } else { ?>
<? } ?>
<div class="contactDetails">
  <div class="outer-rounded-box-bold">
    <div class="simple-rounded-box">
      <div class="edit" style="float:right;"> <a href="contacts.php?mode=edit&id=<?=$id?>" class="edit"><span>Edit</span></a> </div>
      <h2 >
        <?=$row['first_name']?>
        <?=$row['last_name']?>
      </h2>
      <h4 ><b>Title:</b>
        <?=$row['title']?>
      </h4>
      <p><b>Company Name:</b> <a href="https://www.google.com/search?q=<?=$row['company']?>" class="external" target="_new" >
        <?=$row['company']?>
        </a></p>
      <? if($row['reports_to']){ ?>
      <p><b>Reports To:</b>
        <?=$row['reports_to']?>
      </p>
      <? } ?>
      <p><b>Phone:</b>
        <?=$row['phone']?>
      </p>
      <p><b>Email:</b>
        <?=$row['email']?>
      </p>
    </div>
  </div>
</div>
<h2>Contact Details</h2>
<div class="contactDetails">
  <div class="outer-rounded-box-bold">
    <div class="simple-rounded-box">
      <div class="edit" style="float:right;"> <a href="contacts.php?mode=edit&id=<?=$id?>" class="edit"><span>Edit</span></a> </div>
      <p><b>Address:</b>
        <?=$row['address']?>
      </p>
      <? if($row['address2']){ ?>
      <p><b>Address 2:</b>
        <?=$row['address2']?>
      </p>
      <? } ?>
      <p><b>City:</b>
        <?=$row['city']?>
      </p>
      <p><b>State:</b>
        <?=$row['state']?>
      </p>
      <p><b>Zip Code:</b>
        <?=$row['zip']?>
      </p>
      <p><b>Website:</b> <a href="<?=$row['website']?>" class="external" target="_new">
        <?=$row['website']?>
        </a></p>
    </div>
  </div>
</div>
<h2>Add Notes</h2>
<div id="createDir">
  <form method="post" action="inc/forms/add-note.php" class="ajax">
    <input type="text" name="subject" value="">
    <input type="hidden" name="contactID" value="<?=$id?>"  />
    <input type="submit" value="Create Note" name="submit" />
  </form>
</div>
<? if($notesView && is_array($notesView)){
	echo '<div id="message_cnt" style="display:none;"></div>';
	echo '<div id="dash" title="'.$noteid.'"><div id="list_notes" title="notes" class="dash-container"><div class="container">';
	foreach($notesView as $noteid=>$note){
		$updated = date("F jS, Y", strtotime($note['last_updated']));
		echo '<div class="item3">
				<div class="icons">
					<a href="inc/forms/view-contact.php?mode=noteedit&noteid='.$noteid.'&contactID='.$id.'" class="edit"><span>Edit</span></a>
					<a href="#" class="delete rmParent" rel="table=notes&id='.$noteid.'&undoLink=notes"><span>Delete</span></a>
				</div>';
				
				echo '<img src="img/blog-post.png" class="icon" />';
				
				echo '<div class="name"><a href="inc/forms/view-contact.php?mode=noteview&noteid='.$noteid.'&contactID='.$id.'">'.$note['subject'].'</a></div>
					  <div class="type"><a href="inc/forms/view-contact.php?mode=noteview&noteid='.$noteid.'&contactID='.$id.'">'.$note['type'].'</a></div>
					  <div class="date"><a href="inc/forms/view-contact.php?mode=noteview&noteid='.$noteid.'&contactID='.$id.'">'.$updated.'</a></div>
			</div>';
	}
	echo '</div></div></div>';
} else { ?>
<div id="message_cnt" style="display:none;"></div>
<div id="dash">
  <div id="list_notes" title="notes" class="dash-container">
    <div class="container"> Use the form above to create a new note. </div>
  </div>
</div>
<? } ?>
<br style="clear:both;" />
<br />
<? } else echo "Error displaying page."; ?>
<? } ?>
