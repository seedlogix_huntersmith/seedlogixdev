<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/analytics.class.php');
	
	//create new instance of class
	$connector = new Analytics();
	
	$id = $_GET['id'];
	
	if(!$id){
		echo "<br /><br />Use the drop-down above to select which directory listing's analytics to view...<br /><br />";
	}
	
	else {	
		$anal_id = isset($_GET['aid']) ? $_GET['aid'] : $connector->ANAL_getAnalyticsId('directory', $id);
		$form = isset($_GET['form']) ? $_GET['form'] : "snapshot";
		$period = $_GET['period'] ? $_GET['period'] : 'day';
		$bold = 'style="font-weight:bold;"';
		$lbase = 'inc/forms/directory_analytics_view.php?id='.$id.'&aid='.$anal_id.'&form='.$form.'&period=';
		
		if($period=='day'){
			if($form=='snapshot') $date = 'today';
			else $date = 'yesterday';
		}
		else if($period=='week') $date = date('Y-m-d', strtotime('-7 days'));
		else if($period=='month') $date = date('Y-m', strtotime('-1 month'));
		else if($period=='year') $date = date('Y', strtotime('-1 year'));
?>

<?php
	$thisPage = "analytics";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />
<br /><br />
<script type="text/javascript" src="http://analytics.6qube.com/js/jquery.ba-postmessage.min.js"></script>
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/directory_analytics_view.php?form=snapshot&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="snapshot") echo 'class="current"'; ?>>Snap Shot</a></li>
		<li><a href="inc/forms/directory_analytics_view.php?form=activity&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="activity") echo 'class="current"'; ?>>Activity</a></li>
		<li><a href="inc/forms/directory_analytics_view.php?form=referers&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="referers") echo 'class="current"'; ?>>Referers</a></li>
		<li><a href="inc/forms/directory_analytics_view.php?form=search&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="search") echo 'class="current"'; ?>>Searches</a></li>
		<li class="last"><a href="inc/forms/directory_analytics_view.php?form=visitors&id=<?=$id?>&aid=<?=$anal_id?>" <? if($form=="visitors") echo 'class="current"'; ?>>Visitors</a></li>
	</ul>
</div>
<br style="clear:left;" />
<br />
<? if($form=="snapshot"){ ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Traffic Snap Shot</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

<? } else if($form=="activity") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Activity</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageTitles&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>
	
	<h2>Links Clicked On</h2>
	<div id="iframe2"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getOutlinks&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe2' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>
	
<? } else if($form=="referers") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Referers</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getWebsites&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

<? } else if($form=="search") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Search Engines</h2>
	<div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getSearchEngines&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="1000" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>
	
<? } else if($form=="visitors") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Visits</h2>


	<div id="widgetIframe">
		
 <div id="iframe"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<h2>Returning Visitors</h2>
	<div id="widgetIframe">
		 <div id="iframe2"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitFrequency&actionToWidgetize=getSparklines&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe2' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<h2>Visits by Time</h2>
	<div id="widgetIframe">
		<div id="iframe3"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitTime&actionToWidgetize=getVisitInformationPerLocalTime&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe3' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

	</div>
	
	<div class="mini_graph">
		<h2>Length of Visits</h2>
		<div id="widgetIframe">
			<div id="iframe4"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerVisitDuration&viewDataTable=graphPie&idSite=<?=$anal_id?>&period=year&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe4' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>


		</div>
	</div>
    
    <div class="mini_graph">
		<h2>Visitor Specs</h2>
		<div id="widgetIframe">

			<div id="iframe5"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getConfiguration&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe5' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>

		</div>
	</div>
    
<br style="clear:both;" />

		
        <h2>Pages Per Visit</h2>
		<div id="widgetIframe">

			<div id="iframe6"></div>
<script type="text/javascript">
(function ($) {
var if_height,
       src = 'http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage&viewDataTable=graphEvolution&idSite=<?=$anal_id?>&period=<?=$period?>&date=<?=$date?>&disableLink=1#' + encodeURIComponent( document.location.href ),
       iframe = $( '<iframe " src="' + src + '" width="100%" height="250" scrolling="no" frameborder="0"><\/iframe>' ).appendTo( '#iframe6' );
 
       setInterval(function getHeight() {
             $.receiveMessage(function(e){
                    var h = Number( e.data.replace( /.*if_height=(\d+)(?:&|$)/, '$1' ) );
                    if ( !isNaN( h ) && h > 0 && h !== if_height ) {
                    // Height has changed, update the iframe.
                    iframe.height( if_height = h );
                    }
             } );
       }, 500);
})(jQuery);
</script>


		</div>
<? } //end else if($form=="visitors") ?>
<? } //end else { at top of page ?>
<br style="clear:both;" /><br/>