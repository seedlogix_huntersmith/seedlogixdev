<?php 
	$curr = 'class="current"';
?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="inc/forms/edit-reseller-product.php?id=<?=$id?>" <? if($thisPage=="main") echo $curr; ?>>Product Settings</a>
		</li>
		<li>
			<a href="inc/forms/edit-reseller-product-design.php?id=<?=$id?>" <? if($thisPage=="design") echo $curr; ?>>Design Settings</a>
		</li>
        <li>
			<a href="inc/forms/edit-reseller-product-fields.php?id=<?=$id?>" <? if($thisPage=="fields") echo $curr; ?>>Form Fields</a>
		</li>
		 <li class="last">
			<a href="inc/forms/edit-reseller-product-secure.php?id=<?=$id?>" <? if($thisPage=="secure") echo $curr; ?>>Secure Page Design</a>
		</li>
	</ul>
</div>