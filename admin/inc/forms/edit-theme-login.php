<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['reseller']){
		$query = "SELECT `login-bg`, `login-bg-clr`, `login-logo`, `login-brdr-clr`, `login-txt-clr`, `login-signup-lnk`, `login-lnk-clr`, `login-suggest-ff` FROM resellers WHERE admin_user = ".$_SESSION['login_uid'];
		$loginTheme = $reseller->queryFetch($query);
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = 'login';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" />
	<h1>Branding - Login Page:</h1>
	<h2>Site</h2>
	Login page:<br />
	<a href="http://<?=$_SESSION['main_site']?>/admin/" target="_blank" class="external dflt-lnk">http://<?=$_SESSION['main_site']?>/admin/</a> 
	<br /><br />
	
	<h2>Customize</h2>
	<form class="jquery_form">
	<label>Background Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="login-bg-clr" value="<?=$loginTheme['login-bg-clr']?>" />
	<br />
	<label>Box Border Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="login-brdr-clr" value="<?=$loginTheme['login-brdr-clr']?>" />
	<br />
	<label>Text Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="login-txt-clr" value="<?=$loginTheme['login-txt-clr']?>" />
	<br />
	<label>Link Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="login-lnk-clr" value="<?=$loginTheme['login-lnk-clr']?>" />
	<br />
	<label>"Sign Up" Link</label>
	&nbsp;<input type="text" class="edit-theme-text no-upload" name="login-signup-lnk" value="<?=$loginTheme['login-signup-lnk']?>" />
	<br />
	</form><br />
	
	<label style="margin-top:-10px;">Suggest Firefox? <span style="color:#666;font-size:75%;"> (recommended)</span></label>
	<select class="edit-theme-select uniformselect" name="login-suggest-ff">
		<option value="0" <? if(!$loginTheme['login-suggest-ff']) echo 'selected="yes"'; ?>>No</option>
		<option value="1" <? if($loginTheme['login-suggest-ff']) echo 'selected="yes"'; ?>>Yes</option>
	</select><br /><br />
	
	<form id="login_bg" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
        <label>Background Image</label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="loginBG" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="login_bg_prvw" class="img_cnt" src="<? if($loginTheme['login-bg']) echo $_SESSION['themedir'].$loginTheme['login-bg'];  else echo 'img/no-photo.jpg'; ?>" style="max-width:300px;max-height:300px;" />
	<br /><br />
	
	<form id="login_logo" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
        <label>Logo Image</label>
        <input type="file" class="theme_logo" name="theme_img" />
	   <input type="hidden" name="action" value="editThemeImg" />
	   <input type="hidden" name="logo_type" value="loginLogo" />
        <input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
        <input type="submit" class='hideMe' value="Upload!"/>
    </form><br style="clear:both;" /><img id="login_logo_prvw" class="img_cnt" src="<? if($loginTheme['login-logo']) echo $_SESSION['themedir'].$loginTheme['login-logo'];  else echo 'img/no-photo.jpg'; ?>" style="max-width:300px;max-height:300px;" />
	<br /><br />
	
<br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>