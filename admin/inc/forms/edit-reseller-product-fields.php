<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
if($_SESSION['reseller'] || $_SESSION['admin']){
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$uid = is_numeric($_GET['uid']) ? $_GET['uid'] : '';
	if($_SESSION['reseller']) $uid = $_SESSION['login_uid'];
	else $uid = $uid;
	
	$query = "SELECT main_site, payments, main_site_id FROM resellers WHERE admin_user = '".$uid."'";
	$resellerInfo = $reseller->queryFetch($query);
	$query = "SELECT theme FROM hub WHERE id = '".$resellerInfo['main_site_id']."'";
	$resellerThemeInfo = $reseller->queryFetch($query);
	$product = $reseller->getResellerProducts($uid, $id, true);
	
	$sel = 'selected="selected"';
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
});
</script>

<div id="form_test" style="width:100%;">
  <? if($_SESSION['admin']){ ?>
  <a href="inc/forms/edit-user-reseller-products.php?id=<?=$uid?>" class="dflt-link"><img src="img/v3/nav-previous-icon.jpg" alt="Back" /></a><br />
  <br />
  <? } else { ?>
  <a href="reseller-products.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Forms" /></a><br />
  <br />
  <? } ?>
  <?php
	$thisPage = "fields";
	include('edit_reseller_product_nav.php');
?>
  <br style="clear:left;" />
  <br />
  <h1>Sign-Up Fields:
    <?=$product['name']?>
  </h1><br />
  <h2>Contact Info Fields</h2><br />
  <form name="user_class" class="jquery_form" id="<?=$id?>">
    <ul>
      <li>
        <input type="checkbox" name="fname_field" <? if($product['fname_field']) echo 'checked="checked"'; ?> />
        First Name 
        </li>
      <br style="clear:both" />
      
       <li>
        <input type="checkbox" name="lname_field" <? if($product['lname_field']) echo 'checked="checked"'; ?> />
        Last Name 
        </li>
      <br style="clear:both" />
      

       <li>
        <input type="checkbox" name="phone_field" <? if($product['phone_field']) echo 'checked="checked"'; ?> />
        Phone Number 
        </li>
      <br style="clear:both" />
      
      <li>
        <input type="checkbox" name="company_field" <? if($product['company_field']) echo 'checked="checked"'; ?> />
        Company Name </li>
        
      <br style="clear:both" />
      <li>

        <input type="checkbox" name="address_field" <? if($product['address_field']) echo 'checked="checked"'; ?> />
        Address </li>
        
      <br style="clear:both" />
     
        <li>
        <input type="checkbox" name="address2_field" <? if($product['address2_field']) echo 'checked="checked"'; ?> />
        Address 2</li>
        
      <br style="clear:both" />
     
       <li>
        <input type="checkbox" name="city_field" <? if($product['city_field']) echo 'checked="checked"'; ?> />
        City </li>
      <br style="clear:both" />
      
      <li>
        <input type="checkbox" name="state_field" <? if($product['state_field']) echo 'checked="checked"'; ?> />
        State </li>
      <br style="clear:both" />
      
      <li>
        <input type="checkbox" name="zip_field" <? if($product['zip_field']) echo 'checked="checked"'; ?> />
        Zip </li>
      <br style="clear:both" />
      

      <li>
        <input type="checkbox" name="date_field" <? if($product['date_field']) echo 'checked="checked"'; ?> />
        Date Field </li>
      <br style="clear:both" />
      
      <li>
        <label>Calendar Link, Message, etc.</label>
        <div style="font-size:75%;color:#666;">Please note, users can simply click on the date field and calendar pops up.  You can direct them to a Google Calendar or anything by putting something in this field. <br />
          <br />
        </div>
        <textarea name="date_message_field" rows="2" cols="55" ><?=$product['date_message_field']?>
        </textarea>
      </li>
      
      <br style="clear:both" />
        
        <h2>Business Info Fields</h2><br />
        
         <li>
        <input type="checkbox" name="biz_contact_name" <? if($product['biz_contact_name']) echo 'checked="checked"'; ?> />
        Contact Name </li>
      <br style="clear:both" />

      <li>
        <input type="checkbox" name="biz_company_name" <? if($product['biz_company_name']) echo 'checked="checked"'; ?> />
        Company Name </li>
      <br style="clear:both" />

       <li>
        <input type="checkbox" name="biz_phone_field" <? if($product['biz_phone_field']) echo 'checked="checked"'; ?> />
        Company Phone Number </li>
      <br style="clear:both" />

       <li>
        <input type="checkbox" name="website_url" <? if($product['website_url']) echo 'checked="checked"'; ?> />
        Website URL </li>
      <br style="clear:both" />

       <li>
        <input type="checkbox" name="biz_address_field" <? if($product['biz_address_field']) echo 'checked="checked"'; ?> />
       Business Address </li>
      <br style="clear:both" />
     

       
        <li>
        <input type="checkbox" name="biz_address2_field" <? if($product['biz_address2_field']) echo 'checked="checked"'; ?> />
        Address 2</li>
      <br style="clear:both" />
     
       <li>
        <input type="checkbox" name="biz_city_field" <? if($product['biz_city_field']) echo 'checked="checked"'; ?> />
        City </li>
      <br style="clear:both" />

       <li>
        <input type="checkbox" name="biz_state_field" <? if($product['biz_state_field']) echo 'checked="checked"'; ?> />
        State </li>
      <br style="clear:both" />

      <li>
        <input type="checkbox" name="biz_zip_field" <? if($product['biz_zip_field']) echo 'checked="checked"'; ?> />
        Zip </li>
      <br style="clear:both" />

      
      
  
    </ul>
  </form>
</div>
<br style="clear:both" />
<br />
<br />
<? } ?>
