<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
//create new instance of class
$reseller = new Reseller();

$id = $_GET['id'];
$id = is_numeric($id) ? $id : '';

$user = $reseller->getUsers(NULL, $id);

if(($_SESSION['reseller'] && (($user['parent_id']==$_SESSION['user_id']) || ($user['id']==$_SESSION['login_uid']))) 
 || $_SESSION['admin']){
	if($user['super_user'] == 1){
		$isReseller = true;
		if($user['id']==$_SESSION['login_uid']){
			$isSelf = true;
		}
	}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
    
<?
//section navigation
$thisPage = 'details';
include('edit-user-nav.php');
?>
<br style="clear:left;" />
<br />
	<h1>User #<?=$id?> Info: Details</h1>
<form name="user_info" class="jquery_form" id="<?=$user['user_info_id']?>">
	<ul>
        <li><label>First Name:</label><input type="text" name="firstname" value="<?=$user['firstname']?>" /></li><br />
        <li><label>Last Name:</label><input type="text" name="lastname" value="<?=$user['lastname']?>" /></li><br />
	   <li><label>Phone:</label><input type="text" name="phone" value="<?=$user['phone']?>" /></li><br />
	   <li><label>Company Name:</label><input type="text" name="company" value="<?=$user['company']?>" /></li><br />
	   <li><label>Address:</label><input type="text" name="address" value="<?=$user['address']?>" /></li><br />
	   <li><label>Address2:</label><input type="text" name="address2" value="<?=$user['address2']?>" /></li><br />
	   <li><label>City:</label><input type="text" name="city" value="<?=$user['city']?>" /></li><br />
	   <li><label>State:</label><?=$reseller->stateSelect('state', $user['state'])?></li><br />
	   <li><label>Zip:</label><input type="text" name="zip" value="<?=$user['zip']?>" maxlength="5" /></li><br />
	</ul>
</form><br />
<? } else echo "Not authorized."; ?>