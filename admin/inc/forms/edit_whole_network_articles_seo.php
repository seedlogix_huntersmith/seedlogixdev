<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/local.class.php');
$local = new Local();
$parent_id = $_GET['id'];
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

//get site info
$query = "SELECT 
			id, admin_user, name, domain, google_verif, google_stats, 
			theme, seo_title, seo_desc, city_seo_title, city_seo_desc, citycat_seo_title, citycat_seo_desc
		FROM resellers_network 
		WHERE parent = ".$parent_id." AND type = 'articles' LIMIT 1";
$row = $local->queryFetch($query);

//prepare domain value for Google note below
$a = explode('.', $row['domain']);
$domain2 = $a[count($a)-2].'.'.$a[count($a)-1];

$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
$themeInfo = $local->queryFetch($query);
	
if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "articles";
include('edit_whole_network_nav.php');
?>
<br style="clear:left;" />
<div id="form_test" >
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Articles SEO Settings</h2>
	<br />
	<?php
include('edit_whole_network_tags.php');
?>
	
	<form name="resellers_network" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<h2>SEO Settings:</h2>
			
			<li>
				<label>Main SEO Title</label>
				<input  type="text" name="seo_title" value="<?=$row['seo_title']?>" />
			</li>
			<li>
				<label>Main SEO Description</label>
				<input  type="text" name="seo_desc" value="<?=$row['seo_desc']?>" />
			</li>
			<li>
				<label>City Level SEO Title</label>
				<input  type="text" name="city_seo_title" value="<?=$row['city_seo_title']?>" />
			</li>
			<li>
				<label>City Level SEO Description</label>
				<input  type="text" name="city_seo_desc" value="<?=$row['city_seo_desc']?>" />
			</li>
			<li>
				<label>City Category Level SEO Title</label>
				<input  type="text" name="citycat_seo_title" value="<?=$row['citycat_seo_title']?>" />
			</li>
			<li>
				<label>City Category Level SEO Description</label>
				<input  type="text" name="citycat_seo_desc" value="<?=$row['citycat_seo_desc']?>" />
			</li>
			<li>
				<label>Google Site Verification Code</label>
				<span style="color:#666;font-size:75%;">Enter only the string, like <i>Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y</i></span>
				<input type="text" name="google_verif" value="<?=$row['google_verif']?>" /><br /><br />
				<small>When setting this site up in Google Webmaster Tools enter this URL for the sitemap:</small><br />
				<span style="text-decoration:underline;"><?=$row['domain']?>/sitemap_<?=$domain2?>.xml</span>
			</li>
			<li>
				<label>Additonal Tracking Code</label>
			<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Google Analytics Embed Code</i></div>
				<textarea  name="google_stats" rows="6" ><?=htmlentities($row['google_stats'])?></textarea>
				<br style="clear:left" />
			</li>
			
		</ul>
	</form><br />
	
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
		<?php
include('edit_whole_network_articles_nav.php');
?>


	
						
</div>
<!--End APP Right Panel-->

<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>