<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, name, company_name, city, blog, twitter, facebook, myspace, youtube, diigo, technorati, linkedin FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$row = $directory->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript">
$(function(){
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>

<!-- FORM NAVIGATION -->
<?php
	$thisPage = "social";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />

<div id="form_test">
	<h2><?=$row['name']?> - Social Networking</h2>
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Company Blog Link</label>
				<input type="text" name="blog" value="<?=$row['blog']?>" />
			</li>
			<li>
				<label>Twitter Link</label>
				<input type="text" name="twitter" value="<?=$row['twitter']?>" />
			</li>
			<li>
				<label>Facebook Link</label>
				<input type="text" name="facebook" value="<?=$row['facebook']?>" />
			</li>
			<li>
				<label>Myspace Link</label>
				<input type="text" name="myspace" value="<?=$row['myspace']?>" />
			</li>
			<li>
				<label>Youtube Link</label>
				<input type="text" name="youtube" value="<?=$row['youtube']?>" />
			</li>
			<li>
				<label>Diigo Link</label>
				<input type="text" name="diigo" value="<?=$row['diigo']?>" />
			</li>
			<li>
				<label>Technorati Link</label>
				<input type="text" name="technorati" value="<?=$row['technorati']?>" />
			</li>
			<li>
				<label>LinkedIn Link</label>
				<input type="text" name="linkedin" value="<?=$row['linkedin']?>" />
			</li>
		</ul>
	</form>
</div>

<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Social Network Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#dirsocialVideo" class="trainingvideo">Social Network Overview</a>
			</li>
			<div style="display:none;">
				<div id="dirsocialVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-social-overview.flv" style="display:block;width:640px;height:480px" id="dirsocialplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("dirsocialplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.facebook.com/pages/create.php" class="external" target="_new">Create a Facebook Business Page</a>
			</li>
			<li>
				<a href="https://twitter.com/signup" class="external" target="_new">Create a Twitter Account</a>
			</li>
		</ul>
		<? if(!$_SESSION['theme']){ ?>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->

<br style="clear:both;" /><br/>
<? } else echo "Error displaying page."; ?>