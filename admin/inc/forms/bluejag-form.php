<form class="signupform" action="/custom-website-design-signup/" class="jquery_form" method="post" id="sign-up">
		<h5>Web Design Package <em>(* Indicates required information)</em></h5>
		<ul>
			<li>
				<label class="plan" for="plan">Plan:</label>
				<select	class="plan" id="plan" name="userType">
					<option value="8">Starter</option>
					<option value="9">Advanced</option>
					<option value="10">Business</option>
					<option value="11">Commercial</option>
					<option value="12">Enterprise</option>
				</select>
				<em>*</em>
			</li>
		</ul>
        
        <h5>User Account Info</h5>
		<ul>
			<li>
				<label for="email">Email Address:</label>
				<input id="email" name="email" type="text" value="<?=$_POST['email']?>" />
				<em>*</em>
			</li>
            <li>
				<label for="zip">Password:</label>
				<input class="zip" id="zip" name="password" type="password" value="" />
				<em>*</em>
			</li>
            <li>
            <label for="zip">Re-Enter Password:</label>
				<input class="zip" id="zip" name="password2" type="password" value="" />
				<em>*</em>
                </li>
		</ul>
	
		<h5>Profile Info</h5>
		<ul>
			<li>
				<label for="firstName">First Name:</label>
				<input id="firstName" name="first_name" value="<?=$_POST['first_name']?>" type="text" />
				<em>*</em>
			</li>
			
			<li>
				<label for="lastName">Last Name:</label>
				<input id="lastName" name="last_name" value="<?=$_POST['last_name']?>" type="text" />
				<em>*</em>
			</li>
			
			<li>
				<label for="company">Company Name:</label>
				<input class="company" id="company" name="company" value="<?=$_POST['company']?>" type="text" />
				<em>*</em>
			</li>
			<li>
				<label for="zip">Phone:</label>
				<input class="zip" id="zip" name="phone" value="<?=$_POST['phone']?>" type="text" />
				<em>*</em>
			</li>
			
			<li>
				<label for="street">Streeet Address:</label>
				<input class="street" id="street" name="address" value="<?=$_POST['address']?>"  type="text" />
				<em>*</em>
			</li>
			
			<li>
				<label for="city">City:</label>
				<input class="city" id="city" name="city" value="<?=$_POST['city']?>" type="text" />
				<em>*</em>
			</li>
            
            <li>
				<label for="zip">State:</label>
				<input class="zip" id="zip" name="state" value="<?=$_POST['state']?>" type="text" />
				<em>*</em>
			</li>
			
			<li>
				<label for="zip">ZIP:</label>
				<input class="zip" maxlength="10" id="zip" name="zip" value="<?=$_POST['zip']?>" type="text" />
				<em>*</em>
			</li>
			<li class="a-center">
				<button type="submit"><span class="button"><span>Sign Up!</span></span></button>
			</li>
			
		</ul>
	
		
		</form>