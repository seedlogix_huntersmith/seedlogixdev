<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
if(1 || $_SESSION['user_id'] == 6610)   // LIVE!!
{
    require 'blog_autoresponder.amado.php';
    exit;
}	

	require_once(QUBEADMIN . 'inc/blogs.class.php');
	$blog = new Blog();
	
	$_SESSION['current_id'] = $_GET['id'];
	
	$results = $blog->getAutoResponders($current_id, 'blogs', $_SESSION['user_id']);
	$ar_count = $results ? $blog->numRows($results) : 0;
	
	//check responder limits for type/campaign
	if($_SESSION['user_limits']['responder_limit']==-1)
		$respondersRemaining = "Unlimited";
	else {
		$numResponders = $blog->getAutoResponders(NULL, 'blogs', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
		$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
		if($respondersRemaining<0) $respondersRemaining = 0;
	}
	//email limits
	$emailsRemaining = $blog->emailsRemaining($_SESSION['user_id']);
	if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
	else $emailsRemaining = number_format($emailsRemaining);
	
	if($_GET['form']){
		$form = $_GET['form'];
		include('autoresponse-'.$form.'.php');
	}	
	else{
		if($_GET['mode']=='nav'){
		$thisPage = "blogauto";
		include('auto_nav.php');
		}
		echo '<div id="createDir">
				<form method="post" class="ajax" action="add-autoresponder2.php">
					<input type="hidden" name="table_id" value="'.$_SESSION['current_id'].'" />
					<input type="hidden" name="table_name" value="blogs" />
					<input type="hidden" name="type" value="blog" />
					<input type="text" name="name" ';
					if((is_numeric($respondersRemaining) && $respondersRemaining==0)) 
					echo 'readonly';
		echo			' />
					<input type="submit" value="Create New Email" name="submit" />
				</form>
			</div>';
		
		if($ar_count){ //if user has any autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			while($row = $blog->fetchArray($results)){ //loop through and display links to each
				echo '<div class="item3">
						<div class="icons">
							<a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
							<a href="#" class="delete rmParent" rel="table=auto_responders&id='.$row['id'].'&undoLink=auto_responders"><span>Delete</span></a>
						</div>';
					echo '<img src="img/field-email.png" class="icon" />';
					echo '<div class="name"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a></div>
						  <div class="type"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched'].'</a></div>
						  <div class="date"><a href="inc/forms/blog_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched_mode'].'</a></div>
					</div>';
			}
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> blog auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
		
		else { //if no autoresponders
			echo '<div id="dash"><div id="fullpage_autoresponders" class="dash-container"><div class="container">';
			echo 'Use the form above to create a new auto responder.<br />';
			echo 
			'<br /><p>You have <strong>'.$respondersRemaining.'</strong> blog auto-responders remaining for this campaign.</p>';
			echo 
			'<br /><p>You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.</p>';
			echo '</div></div></div>';
		}
	}
?>