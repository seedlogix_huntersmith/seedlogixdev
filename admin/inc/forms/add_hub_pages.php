<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$hub = new Hub();
	
	$hub_id = $_GET['id'];	
	$page_results = $hub->getHubPage($hub_id, NULL, NULL, 1);
	
	if($hub->getPageRows()){
		if($reseller_site) $add = '&reseller_site=1';
		$site = $_SESSION['main_site'] ? $_SESSION['main_site'] : '6qube.com';
		$listPages = '<div id="message_cnt" style="display:none;"></div>';
		$listPages .= '<div id="dash" title="'.$_SESSION['current_id'].'"><div id="list_hubpages" title="item_list" class="dash-container"><div class="container">';
		//loop through and out the blogs post (limits 5)
		while($page_row = $hub->fetchArray($page_results)){
			$listPages .= '<div class="item2">
					<div class="icons">
						<a href="hub.php?form=editPage&id='.$page_row['id'].$add.'&hub_id='.$hub_id.'" class="edit"><span>Edit</span></a>';
			if($page_row['allow_delete'] || ($_SESSION['reseller'] || $_SESSION['admin']))
				$listPages .= '<a href="#" class="delete" rel="table=hub_page&id='.$page_row['id'].'"><span>Delete</span></a>';
			$listPages .= '</div>';
				if($_SESSION['theme'])
					$listPages .= '<img src="img/hub-page/'.$_SESSION['thm_buttons-clr'].'.png" class="blogPost" />';
				else
					$listPages .= '<img src="img/v3/hub-page.png" class="icon" />';
					
				$listPages .= '<a href="hub.php?form=editPage&id='.$page_row['id'].$add.'&hub_id='.$hub_id.'">'.$page_row['page_navname'].': '.$page_row['page_title'].'</a>
				</div>';
			if($page_row['user_id']) $user_id = $page_row['user_id'];	
		}
		$listPages .= '</div></div></div>';
	}
	
	if(!$user_id || $user_id==$_SESSION['user_id']){
		echo $listPages;
		
		if($reseller_site){
			$img_url = '/reseller/'.$_SESSION['login_uid'].'/';
		} else {
			$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub/';
		}
		
		if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
		else $dflt_noImg = 'img/no-photo.jpg';
		
		$query = "SELECT theme, req_theme_type, pages_version FROM hub WHERE id = '".$hub_id."'";
		$row = $hub->queryFetch($query);
		$query = "SELECT user_class2_fields FROM themes WHERE id = '".$row['theme']."'";
		$themeInfo = $hub->queryFetch($query);
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if($row['req_theme_type']==4 && $_SESSION['reseller']){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$addClass = ' muPage multiUser-'.$row['theme'].'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && $hub->isMultiUserTheme($row['theme'])){
			$multi_user2 = true;
			//figure out which fields to lock
			if($themeInfo['user_class2_fields']){
				$fieldsInfo = json_decode($themeInfo['user_class2_fields'], true);
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller']){
						return 'readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$('#migratePages').die();
	$('#migratePages').live('click', function(){
		if(confirm('This will upgrade your pages and site navigation to Version 2 while preserving all your settings and pages.  Do you wish to continue?')){
			var params = $(this).attr('rel');
			$.get('migrate_pages.php?'+params, function(data){
				var json = $.parseJSON(data);
				if(json.success==1){
					if(json.message){
						alert('Your hub was successfully upgraded but there were one or more errors:\n'+json.message);
					}
					else alert('Your hub was successfully upgraded to Version 2!');
					$('#wait').show();
					$.get('hub.php?form=pages2&id=<?=$hub_id?>', function(data2){
						$('#wait').hide();
						$('#ajax-load').html(data2);
					});
				}
				else {
					alert(json.message);
				}
			});
		}
		return false;
	});
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "pages";
	$parentID = $_SESSION['current_id'];
	$hasPages = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<? if($multi_user){ ?>
	<label>Allow theme users to create more pages?</label>
	<select name="lock_allow_pages" title="select" class="uniformselect pagesLock multiUser-<?=$row['theme']?>n" rel="<?=$hub_id?>">
		<option value="0">Yes</option>
		<option value="1" <?=checkLock('allow_pages',1)?>>No</option>
	</select><div style="margin-top:-10px;"></div>
	<? } ?>
	<h2>Create New Page</h2>
	<? if(!checkLock('allow_pages')){ ?>
	To create a new page you must <strong>enter the title first</strong>.<br /><br />
    <?php include('hubs_tags.php'); ?>
	<form name="hub_page" class="jquery_form" id="new-page-form">
		<ul>
			<li>
				<label>Title</label>
				<? if($multi_user){ ?>
				<select name="lock_page_title" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_title" title="new_hub_page" id="new_hub_page" rel="<?=$_SESSION['current_id']?>" value="" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Navigation Name</label>
				<? if($multi_user){ ?>
				<select name="lock_page_navname" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_navname" value="" class="<?=$addClass?>" />
			</li>
				<li>
					<label>Page Edit Region</label>
					<? if($multi_user){ ?>
					<select name="lock_page_region" title="select" class="uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1">Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea name="page_region" rows="10" id="edtr1" class="<?=$addClass?>"></textarea>
					<br style="clear:left" />
				</li>
			<li>
				<label>Keywords</label>
				<? if($multi_user){ ?>
				<select name="lock_page_keywords" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Comma-separated. Example: Austin Flooring, California Sun Rooms, ... (up to six)</div>
				<input type="text" name="page_keywords" value="" class="<?=$addClass?>" />
			</li>
			<li>
				<label>SEO Title *optional</label>
				<? if($multi_user){ ?>
				<select name="lock_page_seo_title" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_seo_title" value="<?=$row['page_seo_title']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>SEO Page Description</label>
				<? if($multi_user){ ?>
				<select name="lock_page_seo_desc" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_seo_desc" value="" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Include Contact Form?</label>
				<? if($multi_user){ ?>
				<select name="lock_inc_contact" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="inc_contact" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="inc_contact" title="select" class="uniformselect<?=checkLock('inc_contact')?>">
				<? } ?>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</li>
			<?php if($row['theme'] == "24" || $row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "98" || $row['theme'] == "99" || $row['theme'] == "102" || $row['theme'] == "104") {?>
			<li>
				<label>Full Width Page?</label>
				<? if($multi_user){ ?>
				<select name="lock_page_full_width" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if($addClass){ ?>
				<select name="page_full_width" title="select" class="uniformselect<?=$addClass?>">
				<? } else { ?>
				<select name="page_full_width" title="select" class="uniformselect<?=checkLock('page_full_width')?>">
				<? } ?>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</li>
			<? } ?>
			<li>
				<label>Include In Navigation Links?</label>
				<? if($multi_user){ ?>
				<select name="lock_nav" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<select name="nav" title="select" class="uniformselect<?=$addClass?>">
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</li>
			<?php if($row['theme'] == "24" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "58" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "98" || $row['theme'] == "102" || $row['theme'] == "104") {?>
			<li>
					<label>Column Edit Region</label>
					<? if($multi_user){ ?>
					<select name="lock_page_edit_2" title="select" class="uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1">Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea name="page_edit_2" rows="10" id="edtr1" class="<?=$addClass?>"></textarea>
					<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	
	<form id="img_page_photo" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo</label>
		<? if($multi_user){ ?>
		<select name="lock_page_photo" title="select" class="uniformselect pagesLock imgLock<?=$addClass?>" id="new-page-img-lock" rel="">
			<option value="0">Unlocked</option>
			<option value="1">Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub_page" />
		<input type="hidden" name="fname" value="page_photo" />
		<input type="hidden" name="img_id" id="img_id" value="" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$row['theme']?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<br style="clear:both;" />
	</form>
	<img id="img_page_photo_prvw" class="img_cnt" src="<?=$dflt_noImg?>" style="max-width:300px;max-height:300px;" />
    
	<form name="hub_page" class="jquery_form" id="new-page-form2">
		<ul>
			<li>
				<label>Photo Description</label>
				<? if($multi_user){ ?>
				<select name="lock_page_photo_desc" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1">Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Optional: Set the alt tag for the image on your page.</div>
				<input type="text" name="page_photo_desc" value="" class="<?=$addClass?>" />
			</li>
		</ul>
	</form>
	<? } else echo 'New page creation has been disabled for this theme.'; ?>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Add Hub Page Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#hubpagesVideo" class="trainingvideo">Add HUB Page Video</a>
			</li>
			<div style="display:none;">
				<div id="hubpagesVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-pages-overview.flv" style="display:block;width:640px;height:480px" id="hubpagesplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubpagesplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="external" target="_new">Common HTML Tags</a>
			</li>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new">Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
				<li><a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
			<? if($row['pages_version']!=2 && !$multi_user && !$multi_user2){ ?>
			<div align="center">
				<a id="migratePages" href="#" rel="hubID=<?=$hub_id?>"><img src="img/upgrade-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a>
			</div>
			<? } ?>
		</ul>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>							
</div>
<!--End APP Right Panel-->
<br /><br style="clear:both;" />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>