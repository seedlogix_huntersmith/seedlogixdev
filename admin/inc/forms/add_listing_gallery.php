<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, name, company_name, city, pic_one_title, pic_one, pic_two_title, pic_two, pic_three_title, pic_three, pic_four_title, pic_four, pic_five_title, pic_five, pic_six_title, pic_six FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$row = $directory->queryFetch($query);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$("input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>	
});
</script>

<!-- FORM NAVIGATION -->
<?php
	$thisPage = "gallery";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />

<div id="form_test">
	<h2><?=$row['name']?> - Photo Gallery</h2>
	
	<!-- photo one -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #1</label>
				<input type="text" name="pic_one_title" value="<?=$row['pic_one_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #1</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_one" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_one']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_one'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    
	<!-- photo two -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #2</label>
				<input type="text" name="pic_two_title" value="<?=$row['pic_two_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_two" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #2</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_two" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_two']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_two'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    
	<!-- photo three -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #3</label>
				<input type="text" name="pic_three_title" value="<?=$row['pic_three_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_three" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #3</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_three" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_three']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_three'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<!-- photo four -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #4</label>
				<input type="text" name="pic_four_title" value="<?=$row['pic_four_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_four" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #4</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_four" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_four']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_four'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<!-- photo five -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #5</label>
				<input type="text" name="pic_five_title" value="<?=$row['pic_five_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_five" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #5</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_five" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_five']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_five'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<!-- photo six -->
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo title #6</label>
				<input type="text" name="pic_six_title" value="<?=$row['pic_six_title']?>" />
			</li>
		</ul>
	</form>
	<form id="img_six" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #6</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="directory" />
		<input type="hidden" name="fname" value="pic_six" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img class="img_cnt" src="<? if($row['pic_six']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/directory/'.$row['pic_six'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
</div>

<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Photo Gallery Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#dirgalleryVideo" class="trainingvideo">Photo Gallery Overview</a>
			</li>
			<div style="display:none;">
				<div id="dirgalleryVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-gallery-overview.flv" style="display:block;width:640px;height:480px" id="dirgalleryplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("dirgalleryplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="helpLinks">Stock Photos</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
		<? if(!$_SESSION['theme']){ ?>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br/>.

<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>