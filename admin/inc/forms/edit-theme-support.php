<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['reseller']){
		$query = "SELECT `support-page`, support_email, support_phone, activation_email, activation_email_img 
				FROM resellers WHERE admin_user = ".$_SESSION['login_uid'];
		$row = $reseller->queryFetch($query);
		
		if($_POST['sendTest']){
			$activationUrl = 'http://'.$_SESSION['main_site'].'/?page=activation';
			$imageUrl = 'http://'.$_SESSION['main_site'].'/admin/themes/'.$_SESSION['login_uid'].'/'.
						$row['activation_email_img'];
			$to = $_POST['user_email'];
			$subject = "Test Activation Email";
			$message = str_replace("#activate", $activationUrl, stripslashes($row['activation_email']));
			$message = str_replace("#image", $imageUrl, $message);
			
			if($reseller->sendMail($to, $subject, $message, NULL, "<no-reply@".$_SESSION['main_site'].">", NULL, true))
				echo "Test email sent successfully to ".$to;
			else echo "Error sending test email.";
			
			$stop = true;
		}
		
		if(!$stop){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = 'support';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" /><br />
	<h1>Branding - Support/Users:</h1>
	
	<h2>Support</h2>
	<form class="jquery_form">
	<label>Support Page Content</label>
	<a href="#" id="1" class="tinymce"><span>Rich Text</span></a><textarea name="support-page" rows="8" cols="50" id="edtr1" class="edit-theme-textarea no-upload"><?=stripslashes($row['support-page']);?></textarea>
	<br />
	<label>Support Email Address</label>
	<input type="text" class="edit-theme-text no-upload" name="support_email" value="<?=$row['support_email']?>" />
	<br />
	<label>Support Phone Number</label>
	<input type="text" class="edit-theme-text no-upload" name="support_phone" value="<?=$row['support_phone']?>" />
	<br /><br />
	
	<h2>Users</h2>
	<label style="display:inline;">User Activation Email</label>
	<span style="color:#666;font-size:75%;"> | Sent to free user signups to activate their account.</span><br />
	<span style="font-size:75%;"><b>Important:</b><br />
	Use "<b>#activate</b>" as a placeholder for the activation link.<br />
	<i>Example:</i> &lt;a href="#activate"&gt;Click here to activate your account&lt;/a&gt;
	<br />
	If you upload an image below to use in the email, just use the "<b>#image</b>" placeholder.<br />
	<i>Example:</i> &lt;img src="#image" alt="My image title" /&gt;
	</span>
	<br /><br />
	<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
	<textarea name="activation_email" rows="8" cols="50" id="edtr2" class="edit-theme-textarea no-upload"><?=stripslashes($row['activation_email']);?></textarea>
	<br />
	</form>
	<form id="activation_email_img" enctype="multipart/form-data" class="upload_form" action="reseller-functions.php" target="uploadIFrame" method="POST">
		<label>User Activation Email Image</label>
		<input type="file" class="theme_logo" name="theme_img" />
		<input type="hidden" name="action" value="editThemeImg" />
		<input type="hidden" name="logo_type" value="actEmailImg" />
		<input type="hidden" name="reseller_uid" value="<?=$_SESSION['login_uid']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form><img id="activation_email_img_prvw" class="img_cnt" src="<? if($row['activation_email_img']) echo $_SESSION['themedir'].$row['activation_email_img'];  else echo 'img/no-photo.jpg'; ?>" style="max-width:300px;max-height:300px;" /><br />
	<div id="createDir">
	<form class="jquery_form" action="inc/forms/edit-theme-support.php" method="POST" target="hiddenIframe">
    	<label>Send test email:</label>
        <span style="color:#666; font-size:75%">Enter your email below and click submit to be sent a test copy of this email.<br /><b>Only click the button once.</b></span><br /><br />
    	<input type="text" value="<?=$_SESSION['login_user']?>" name="user_email" style="width:350px;" />
	<input type="submit" value="Send Test Email" name="sendTest"  />
	</form>
    </div>
	<iframe name="hiddenIframe" id="hiddenIframe" src="testemail.php" style="display:none;width:1px;height:1px;"></iframe>
	
	<br style="clear:both;" /><br />
	<!-- hidden iframe -->
	<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<?
	} //end if(!$stop)
} else echo "Not authorized."; 
?>