 <select name="industry" title="select" class="uniformselect">
					<option value="0">Please choose an industry</option>
                    <option value="Automotive" <? if($row['industry'] == "Automotive") echo 'selected="yes"'; ?> >Automotive</option>
					<option value="Business Services" <? if($row['industry'] == "Business Services") echo 'selected="yes"'; ?> >Business Services</option>
                    <option value="Community and Education" <? if($row['industry'] == "Community and Education") echo 'selected="yes"'; ?> >Community and Education</option>
                    <option value="Computers and Electronics" <? if($row['industry'] == "Computers and Electronics") echo 'selected="yes"'; ?> >Computers and Electronics</option>
                    <option value="Entertainment" <? if($row['industry'] == "Entertainment") echo 'selected="yes"'; ?> >Entertainment</option>
					<option value="Finance" <? if($row['industry'] == "Finance") echo 'selected="yes"'; ?> >Finance</option>
                    <option value="Food and Dining" <? if($row['industry'] == "Food and Dining") echo 'selected="yes"'; ?> >Food and Dining</option>
                    <option value="Health and Personal Care" <? if($row['industry'] == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Home Based Business" <? if($row['industry'] == "Home Based Business") echo 'selected="yes"'; ?> >Home Based Business</option>
                    <option value="Home Improvement" <? if($row['industry'] == "Home Improvement") echo 'selected="yes"'; ?> >Home Improvement</option>
                    <option value="Health and Personal Care" <? if($row['industry'] == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Legal" <? if($row['industry'] == "Legal") echo 'selected="yes"'; ?> >Legal</option>
                    <option value="Real Estate" <? if($row['industry'] == "Real Estate") echo 'selected="yes"'; ?> >Real Estate</option>
                    <option value="Shopping" <? if($row['industry'] == "Shopping") echo 'selected="yes"'; ?> >Shopping</option>
                    <option value="Travel and Recreation" <? if($row['industry'] == "Travel and Recreation") echo 'selected="yes"'; ?> >Travel and Recreation</option>
                    <option value="Web Services" <? if($row['industry'] == "Web Services") echo 'selected="yes"'; ?> >Web Services</option>
				</select>
