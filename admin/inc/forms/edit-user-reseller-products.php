<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
//create new instance of class
$reseller = new Reseller();

$id = is_numeric($_GET['id']) ? $_GET['id'] : '';

if($_SESSION['admin']){
	$isReseller = true;
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script> 
    
<?
//section navigation
$thisPage = 'reseller-products';
include('edit-user-nav.php');
?>
<br style="clear:left;" />

	<h1>User #<?=$id?> Info: Reseller Products</h1>
	<? echo $reseller->displayResellerProducts($id, NULL, NULL, NULL, NULL, NULL, NULL, 1); ?>

<br />
<? } else echo "Not authorized. ";//.$user['id'].' = '.$_SESSION['login_uid']; ?>