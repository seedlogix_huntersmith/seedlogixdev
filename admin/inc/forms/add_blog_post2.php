<?php
session_start();
if(!defined('QUBEADMIN'))
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
         
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

//get/set vars

$is_dev    =   ($_SESSION['user_id']    ==  6610);
         
if(!isset($blogID) && !isset($postID)){
$blogID = is_numeric($_GET['id']) ? $_GET['id'] : 0;
$postID = is_numeric($_GET['postID']) ? $_GET['postID'] : 0;
}

$category = isset($_GET['cat']) ? $blog->sanitizeInput($_GET['cat']) : '';
//misc display vars
$sel = 'selected="selected"';
$mode = 'categories';
if(!$category && !$postID){
	$mode = 'categories';
	//default display, no category or post selected
	//get all categories
	$query = "SELECT user_id, category, category_url FROM blog_post 
			  WHERE blog_id = '".$blogID."' AND user_id = '".$_SESSION['user_id']."'
			  AND trashed = '0000-00-00'  
			  ORDER BY category ASC";
	if($cats = $blog->query($query)){
		$dispCats = array();
		while($row = $blog->fetchArray($cats, 1)){
			if($row['category'] && !in_array($row['category_url'], $dispCats)){
				$dispCats[$row['category_url']] = $row['category'];
				$user_id = $row['user_id']; //get user id
			}
		}
	}
}
else if($category){
	$mode = 'posts';
	//listing all the posts in a certain category
	//get all posts from the category
	$query = "SELECT id, user_id, post_title, category FROM blog_post 
			  WHERE category_url = '".$category."' AND blog_id = '".$blogID."' AND user_id = '".$_SESSION['user_id']."' 
			  AND trashed = '0000-00-00'  
			  ORDER BY created DESC";
	if($posts = $blog->query($query)){
		if($blog->numRows($posts)){
			$dispPosts = array();
			$thisCat = '';
			while($row = $blog->fetchArray($posts, 1)){
				//set the title to display.  trim it if it's too long.
				if(strlen($row['post_title'])>50) $postTitle = substr($row['post_title'], 0, 50).'...';
				else $postTitle = $row['post_title'];
				$dispPosts[$row['id']] = $postTitle;
				$thisCat = $row['category']; //get category name
				$user_id = $row['user_id']; //get user id
				$postTitle = NULL;
			}
		}
		else $loadDefault = true;
	}
	else $loadDefault = true;
	
	if($loadDefault){
		//if the category wasn't found, load default page (same code as above)
		$mode = 'categories';
		//default display, no category or post selected
		//get all categories
		$query = "SELECT user_id, category, category_url FROM blog_post
				  WHERE blog_id = '".$blogID."' AND user_id = '".$_SESSION['user_id']."'
				  AND blog_post.trashed = '0000-00-00'
				  ORDER BY category ASC";
		if($cats = $blog->query($query)){
			$dispCats = array();
			while($row = $blog->fetchArray($cats, 1)){
				if($row['category'] && !in_array($row['category_url'], $dispCats)){
					$dispCats[$row['category_url']] = $row['category'];
					$user_id = $row['user_id']; //get user id
				}
			}
		}
	}
}
else if($postID){
	$mode = 'single_post';
	//editing a single post
	//get post info
	$query = "SELECT *, j.feed_ID, date_format(publish_date,'%m/%d/%Y') as str_publish_date FROM blog_post left join blogfeed_masterposts j on (j.master_post_ID = blog_post.id)
			  WHERE id = '".$postID."' AND user_id = '".$_SESSION['user_id']."'
			  AND trashed = '0000-00-00' ";
	$postInfo = $blog->queryFetch($query, NULL, 1);
	//get other categories for dropdown
	$query = "SELECT category, category_url FROM blog_post 
			  WHERE blog_id = '".$blogID."' AND user_id = '".$_SESSION['user_id']."' 
			  AND trashed = '0000-00-00' 
			  ORDER BY category ASC";
	if($cats = $blog->query($query)){
		$categories = array();
		while($row = $blog->fetchArray($cats, 1)){
			if($row['category'] && !in_array($row['category_url'], $categories)){
				$categories[$row['category_url']] = $row['category'];
			}
		}
	}
	$user_id = $postInfo['user_id'];
}
if(!$user_id || $user_id==$_SESSION['user_id']){
?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.3/themes/smoothness/jquery-ui.css" />
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	$('.datepicker').datepicker();
	<? if($mode=='categories' || $mode=='posts'){ ?>
	$('#cat-select').die();
	$('#cat-select').live('change', function(){
		var val = $(this).val();
		if(val!=''){
			if(val=='_new'){
				$('#cat-select').remove();
				$('#cat-input').html('<input type="text" class="no-upload" name="category" style="margin-bottom:5px;" />');
				$('#v2_cat').attr('value', '1');
			}
			else $('#v2_cat').attr('value', '');
		}
	});
	
	$('#new-post-form').die();
	$('#new-post-form').live('submit', function(event){
		event.stopPropagation();
		var params = $(this).serialize();
		$.post('create-blog-post.php', params, function(data){
			if(data.error===0){
				$('#new-title').attr('value', '');
				//$('#listContainer').append(data.html);
				$('#wait').show();
				$.get('blog.php?form=post2&id=<?=$blogID?>&postID='+data.post_id, function(data2){
					$('#wait').hide();
					$('#ajax-load').html(data2);
				});
			}
			else {
				$('#errors').html(data.message);
			}
		}, 'json');
		return false;
	});
	<? } else if($mode=='single_post'){ ?>
	$('#cat-select-feed').die();
	$('#cat-select-feed').live('change', function(){
		var val = $(this).val();
		if(val!=''){
			if(val=='_new'){
				$('#cat-select-feed').remove();
				$('#cat-input-feed').html('<label>Feed</label><input type="text" name="feed_name" value="" />');
			}
		}
	});
	$('#cat-select-post').die();
	$('#cat-select-post').live('change', function(){
		var val = $(this).val();
		if(val!=''){
			if(val=='_new'){
				$('#cat-select-post').remove();
				$('#cat-input-post').html('<label>Category</label><input type="text" name="category" value="" />');
			}
		}
	});
	<? } ?>
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
	
	$('.fancybox-iframe').fancybox({
			type: 'iframe',
				'width'	: '80%',
					'height' : '80%'
	});
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "post";
	$postsV2 = true;
	$parentID = $_SESSION['current_id'];
        if(!isset($isMaster))
	include('add_blog_nav.php');
?>
<?
//////////////////////////////////////////////////////////////////////////////////////
// CREATE A POST, VIEW CATEGORIES:													//
//////////////////////////////////////////////////////////////////////////////////////
if($mode=='categories'){
?>
<br style="clear:both;" /><br />
<div style="width:100%;"><?php if($is_dev || 1): ?><a href="controller.php?ctrl=WordpressImport&bid=<?php echo $blogID; ?>" class="fancybox-iframe">Import Wordpress Posts</a><!--
<form id="import-form" class="ajax bypass">
<label>Feed URL</label>
<input type="text" name="___"><br>
<input type="submit" name="submit" value="Import" />
<br style="clear:both;" /><br />
</form> -->
        <?php endif; ?>
<h2>Create a new post</h2>
<div id="createDir" style="padding-top:0;margin-top:-10px;">
	<form class="ajax bypass" id="new-post-form">
		<div id="errors"></div>
		<label>Title:</label>
		<input type="text" name="title" value="" id="new-title" /><br />
		<label>Category Name:</label>
		<div id="cat-input">
		<? if($dispCats && is_array($dispCats)){ ?>
		<select title="select" class="uniformselect" name="category" id="cat-select">
			<option value="">Choose a category...</option>
			<optgroup label="Existing Categories">
				<? foreach($dispCats as $key=>$value){ ?>
				<option value="<?=$value?>"><?=$value?></option>
				<? } ?>
			</optgroup>
			<optgroup label="New Category">
				<option value="_new">Create a new category</option>
			</optgroup>
		</select>
		<? } else { ?>
		<input type="text" class="no-upload" name="category" value="" />
		<? } ?>
		</div>
		<input type="hidden" name="blog_id" value="<?=$blogID?>" />
		<input type="hidden" name="v2_cat" id="v2_cat" value="1" />
		<input type="submit" value="Create" name="submit" />
	</form>
</div>
<br style="clear:both;" /><br />
Blog Feeds
<form name="blog" class="jquery_form" id="<?=$blogID; ?>"><?php 
    $q  =   'SELECT *, not(isnull(s.feed_ID)) as checked FROM blog_feedsmu f left join blogfeed_subs s on (s.feed_ID = f.ID AND s.blog_ID = ' . (int)$blogID . ' ) '
            . ' WHERE (user_ID = (select parent_id from users where id = ' . $_SESSION['login_uid'] . ') or user_ID = ' . $_SESSION['login_uid'] . ') AND trashed = 0 AND f.name NOT LIKE "trash"';
#            echo $q;
    $feeds  =   $blog->query($q);
    while($feedinfo = $blog->fetchArray($feeds)): // var_dump($feedinfo); 
		$fname	=	htmlentities($feedinfo['name']);
?><div class="feed-<?=$fname;?>">
    <input type="checkbox" name="blog_feeds[<?php echo $feedinfo['ID']; ?>]" value="1"<?php
        if($feedinfo['checked']=='1') 
                echo ' checked="checked"'; ?>><?php echo htmlentities($feedinfo['name']); ?></div><br/><br/><?php
    endwhile; ?>
</form>
<h2>Categories:</h2>
<? if($dispCats && is_array($dispCats)){ ?>
	<div id="dash"><div class="dash-container"><div class="container" id="listContainer">
	<? foreach($dispCats as $key=>$value){ ?>
		<div class="item2">
			<!--<div class="icons">
				<a href="#" id="delete-cat" rel="<?=$key?>" class="delete"><span>Delete</span></a>
			</div>-->
            <img src="img/nav-section.png" class="icon" />
			<a href="blog.php?form=post2&id=<?=$blogID?>&cat=<?=$key?>"><?=$value?></a>
		</div>
	<? } ?>
	</div></div></div>
<? } else { ?>
You haven't created any posts or categories yet.  Use the form above to get started.
<? } ?>
</div>
<?
} //end if($mode=='categories')
//////////////////////////////////////////////////////////////////////////////////////
// VIEW POSTS IN A CATEGORY:													//
//////////////////////////////////////////////////////////////////////////////////////
if($mode=='posts'){
	$img = '<br style="clear:both;" /><br /><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Categories" />';
	echo '<a href="blog.php?form=post2&id='.$blogID.'">'.$img.'</a><br />';
?>
<div style="width:100%;">
<h2>Create a new post under "<?=$thisCat?>"</h2>
<div id="createDir" style="padding-top:0;margin-top:-10px;">
	<form class="ajax bypass" id="new-post-form">
		<div id="errors"></div>
		<label>Title:</label>
		<input type="text" name="title" value="" id="new-title" style="margin-bottom:5px;" />
		<input type="hidden" name="blog_id" value="<?=$blogID?>" />
		<input type="hidden" name="v2_post" value="1" />
		<input type="hidden" name="category" value="<?=$thisCat?>" />
		<input type="submit" value="Create" name="submit" />
	</form>
</div>
<br style="clear:both;" /><br />
<h2>Posts:</h2>
<? if($dispPosts && is_array($dispPosts)){ ?>
	<div id="dash"><div class="dash-container"><div class="container" id="listContainer">
	<? foreach($dispPosts as $key=>$value){ ?>
		<div class="item2">
			<div class="icons">
				<a href="blog.php?form=post2&id=<?=$blogID?>&postID=<?=$key?>" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=blog_post&id=<?=$key?>" onclick="javascript:$(this).parent().parent().fadeOut();return false;"><span>Delete</span></a>
			</div>
			
			<img src="img/blog-post.png" class="icon" />
			<a href="blog.php?form=post2&id=<?=$blogID?>&postID=<?=$key?>"><?=$value?></a>
		</div>
	<? } ?>
	</div></div></div>
<? } else { ?>
You haven't created any posts yet.  Use the form above to get started.
<? } ?>
</div>
<?
} //end if($mode=='posts')
//////////////////////////////////////////////////////////////////////////////////////
// EDIT A SINGLE POST:																//
//////////////////////////////////////////////////////////////////////////////////////
if($mode=="single_post"){
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	//back button
	$img = '<br style="clear:both;" /><br /><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Categories" />';
        
        if(!isset($isMaster))
            echo '<a href="blog.php?form=post2&id='.$blogID.'&cat='.$postInfo['category_url'].'">'.$img.'</a><br />';
        else{            
            echo '<a href="controller.php?ctrl=BlogFeeds">'.$img.'</a><br />';
        }
?>
<br style="clear:left;" />
<div id="form_test">
	<form name="blog_post" class="jquery_form" id="<?=$postID?>">
            <ul><li id="cat-input-feed">
                    <?php if(isset($isMaster)): ?>
		<label>Feed:</label>
				<select title="select" class="uniformselect" name="feed_ID" id="cat-select-feed">
                                    <option value="">Select a Feed</option>
					<optgroup label="Existing Feeds">
						<? foreach($feeds as $feed){ ?>
						<option value="<?=$feed->getID()?>" <? if($feed->getID()==$postInfo['feed_ID']) echo 'selected="selected"'; ?>><?= $feed->html('name'); ?></option>
						<? } ?>
					</optgroup>
					<optgroup label="New Feed">
						<option value="_new">Create a new feed</option>
					</optgroup>
				</select><br />
                
                </li><?php endif; ?> <li>
                    
		<label>Schedule Date:</label>
		<input type="text" name="publish_date" value="<?php
                        if($postInfo['publish_date']){
                            echo $postInfo['str_publish_date'];
                        }
                    ?>" class="datepicker" /><br />
                
                </li>
			<li>
				<label>Title</label>
				<input type="text" name="post_title" value="<?=$postInfo['post_title']?>" />
			</li>
			<li id="cat-input-post">
				<label>Category</label>
				<select title="select" class="uniformselect" name="category" id="cat-select-post">
					<optgroup label="Existing Categories">
						<? foreach($categories as $key=>$value){ ?>
						<option value="<?=$value?>" <? if($value==$postInfo['category']) echo 'selected="selected"'; ?>><?=$value?></option>
						<? } ?>
					</optgroup>
					<optgroup label="New Category">
						<option value="_new">Create a new category</option>
					</optgroup>
				</select>
			</li>
			<li>
				<label>Post</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="post_content" rows="6" id="edtr1"><?=$postInfo['post_content']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Tags</label
				><input type="text" name="tags" value="<?=$postInfo['tags']?>" />
			</li>
			<li>
				<label>Tags URL</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Make sure you include http:// in the link.</div>
				<input type="text" name="tags_url" value="<?=$postInfo['tags_url']?>" />
			</li>
            <li>
				<label>SEO Title *optional</label>
				<input type="text" name="seo_title" value="<?=$postInfo['seo_title']?>" />
			</li>
			<li>
				<label>SEO Description</label>
				<input type="text" name="post_desc" value="<?=$postInfo['post_desc']?>" />
			</li>
			<li>
				<label>Include in network?</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">Change this to "Yes" to publish this blog post to our network.</div>
				<select name="network" title="select" class="uniformselect">
					<option value="0" <? if(!$postInfo['network']) echo 'selected="yes"'; ?>>No</option>
					<option value="1" <? if($postInfo['network']) echo 'selected="yes"'; ?>>Yes</option>
				</select>
			</li>
		</ul>
        
		<input type="hidden" name="blog" value="<?=$postInfo['blog_id']?>" />
		<input type="hidden" name="edit" value="<?=$postInfo['id']?>" />
	</form>
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="blog_post" />
		<input type="hidden" name="fname" value="photo" />
		<input type="hidden" name="img_id" value="<?=$postInfo['id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($postInfo['photo']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/blog_post/'.$postInfo['photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Edit Blog Post Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogposteditVideo" class="trainingvideo">Edit Blog Post Video</a>
			</li>
			<div style="display:none;">
				<div id="blogposteditVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blog-postedit-overview.flv" style="display:block;width:640px;height:480px" id="blogposteditplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogposteditplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="external" target="_new">Common HTML Tags</a>
			</li>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new">Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
	</div><!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } //end if($mode=='single_post') ?>
<br style="clear:both;" /><br />
<? } else echo "Error displaying page."; ?>
