<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

if(!$id){
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$id = $_GET['id'];
}
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
$row = $local->queryFetch($query);

if($row['setup']!=1){
	include(QUBEADMIN . 'reseller-network-setup.php');
}
else {	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "global";
include('edit_whole_network_nav.php');
?>
<div id="form_test">
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Network Names</h2>
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Local Network Name</label>
				<input class="update-whole-network" type="text" name="network_name" value="<?=$row['network_name']?>" />
			</li>
			<li>
				<label>HUBs Network Name</label>
				<input class="update-whole-network" type="text" name="hubs_network_name" value="<?=$row['hubs_network_name']?>" />
			</li>
            <li>
				<label>Press Network Name</label>
				<input class="update-whole-network" type="text" name="press_network_name" value="<?=$row['press_network_name']?>" />
			</li>
            <li>
				<label>Articles Network Name</label>
				<input class="update-whole-network" type="text" name="articles_network_name" value="<?=$row['articles_network_name']?>" />
			</li>
            <li>
				<label>Blogs Network Name</label>
				<input class="update-whole-network" type="text" name="blogs_network_name" value="<?=$row['blogs_network_name']?>" />
			</li>
			 <li>
				<label>Search Network Name</label>
				<input class="update-whole-network" type="text" name="search_network_name" value="<?=$row['search_network_name']?>" />
			</li>
		</ul>
	</form><br />

	
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_global_nav.php');
?>

	
						
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<?
	} else echo "Unable to display this page."; 
}
?>