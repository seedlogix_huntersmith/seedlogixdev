<?php

#if($_SERVER['SERVER_NAME'] == 'pastephp.com' || TRUE)       // WARNING: LIVE CODE
#{

require_once 'autoresponse-theme.amado.php';
exit;
#}

#WARNING: DEAD CODE
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/local.class.php');

//create new instance of class
$local = new Local();

$id = $_GET['id'];

$ar = $local->getAutoResponders(NULL, NULL, NULL, $id, 'theme');
$row = $local->fetchArray($ar);

//get theme id
$table = $row['table_id'];
$query = "SELECT theme FROM hub WHERE id = '".$table."' LIMIT 1";	
$theme = $local->queryFetch($query);	
$themeID = $theme['theme'];

//get theme type
$query = "SELECT type FROM themes WHERE id = '".$themeID."' LIMIT 1";	
$themeType = $local->queryFetch($query);	

if($row['user_id']==$_SESSION['user_id']){
    
			//Get email templates
			$query = "SELECT * FROM auto_responder_themes";
			$templates = $local->query($query);
                        
                        if($row['user_id'] == 6610)
                        {
                            var_dump($row);
                        }
	if($themeType['type']=="1"){ 
	$back_page = "inc/forms/website_autoresponder.php";
	$mode = "nav";
	}
	else if($themeType['type']=="2"){ 
	$back_page = "inc/forms/landing_autoresponder.php";
	$mode = "nav";
	}
	else if($themeType['type']=="5"){ 
	$back_page = "inc/forms/social_autoresponder.php";
	$mode = "nav";
	}
	else if($row['table_name']=="directory") $back_page = "directory-autoresponders.php";
	else if($row['table_name']=="press_release") $back_page = "press-autoresponders.php";
	else if($row['table_name']=="blogs"){ 
	$back_page = "inc/forms/blog_autoresponder.php";
	$mode = "nav";
	}
	else if($row['table_name']=="articles") $back_page = "articles-autoresponders.php";
	else if($row['table_name']=="lead_forms") $back_page = "inc/forms/forms2_autoresponder.php";
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$(".helpLinks").fancybox({
		'width'				: '75%',
		'height'			: '75%',
		'autoScale'			: false,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'		: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none'
	});

	// Autoresponder Templates Dropdown
	$('.ar-templates').live('change', function(){
            var themeid = parseInt($(this).val()*1);
                
            <?php if($row['theme'] == 0): // IF currently no theme is selected, show a warning about possibly losing custom html code */ ?>
                    if(themeid != 0 && !confirm('This action may cause any custom html to be lost or corrupted. Do you still want to continue?'))
                    {
                        $(this).val(0);
                        $.uniform.update();
                        return;
                    }
            <?php endif;                                                                                                    ?>
            
            $('#wait').show();
            $('#AR1').slideUp();
            params = { type: 'autoresponder', id: themeid, emailid: '<?=$row['id']?>', tableid: '<?=$row['table_id']?>', tablename: '<?=$row['table_name']?>' };
            $.post("get-template.php", params, function(data){
                    if(!data.error){
//					$('#edtrAR1').html(data.html);
                            $('#wait').hide();
                            if(themeid)
                                $('.theme_config').slideDown();
                            else
                                $('.theme_config').slideUp();
//                            $("#subform-nav .current").click();
                    }
            }, 'json');
	});	
});
</script>
<!-- FORM NAVIGATION -->

<div id="side_nav">
  <ul id="subform-nav">
    <li class="first"><a href="inc/forms/autoresponse-settings.php?id=<?=$id?>" >Settings</a></li>
    <li><a href="inc/forms/autoresponse-email.php?id=<?=$id?>">Email</a></li>
    <li><a href="inc/forms/autoresponse-theme.php?id=<?=$id?>" class="current">Theme</a></li>
    <li class="last"><a href="<?=$back_page?>?id=<?=$row['table_id']?>&mode=<?=$mode?>">Back</a></li>
  </ul>
</div>
<br style="clear:left;" /><br />
<h1>Autoresponder Design Settings</h1>

<form id="logo" enctype="multipart/form-data" class="upload_form theme_config" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Logo</label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="auto_responders" />
		<input type="hidden" name="fname" value="logo" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['logo']) echo 'http://'.$_SESSION['main_site'].'/users/'.$row['user_id'].'/hub_page/'.$row['logo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
</form>
        <div>
<label>Template</label>
<? if($templates && $local->numRows($templates)){
    echo '<select class="ar-templates no-upload uniformselect" name="theme" rel="', $row["theme"], '">';
    echo '<option value="0">choose a template...</option>';
    while($temprow = $local->fetchArray($templates)){
            echo '<option value="'.$temprow['id'].'" ', $row['theme'] == $temprow['id'] ? 'selected="true"' : '', '>'.$temprow['theme_name'].'</option>';
    }
    echo '</select>';
} ?>

          <a href="email-preview.php?id=<?=$row['id'];?>" class="greyButton helpLinks theme_config" style="float:right;margin-bottom:15px;" >Preview</a>
      </div>
<form name="auto_responders" class="jquery_form theme_config" id="<?=$row['id']?>">
    <ul>
    <?php if($_SERVER['SERVER_NAME'] == 'pastephp.com'): 
    
    
	//include classes
	require_once('../blogs.class.php');

?>
    <li>
      <label>Blog Stream:</label>
<? $Blogs = new Blog;
		echo $Blogs->userBlogsDropDown($_SESSION['user_id'], 'stream_blog_id', $_SESSION['campaign_id'], '', 
		  $row['stream_blog_id']);
 ?>
    </li>
    <?php endif; ?>
    
    <li>
      <label>Background Color:</label>
      <input type="text" name="bg_color" value="<?=$row['bg_color']?>" />
    </li>
    <li>
      <label>Links Color:</label>
      <input type="text" name="links_color" value="<?=$row['links_color']?>" />
    </li>
     <li>
      <label>Accent Color:</label>
      <input type="text" name="accent_color" value="<?=$row['accent_color']?>" />
    </li>
     <li>
      <label>Titles Color:</label>
      <input type="text" name="titles_color" value="<?=$row['titles_color']?>" />
    </li>
     <br />
    <h2>Core Content Areas</h2>
   
     <li>
     <label>Call To Action:</label>
    <a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
    <textarea name="call_action" rows="5" cols="60" id="edtr1"><?=stripslashes($row['call_action'])?></textarea><br style="clear:left;" />
    </li>
    
     <li>
     <label>Edit Region #1:</label>
    <a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
    <textarea name="edit_region1" rows="20" cols="60" id="edtr2"><?=stripslashes($row['edit_region1'])?></textarea><br style="clear:left;" />
    </li>
    
     <li>
     <label>Edit Region #2:</label>
    <a href="#" id="3" class="tinymce"><span>Rich Text</span></a>
    <textarea name="edit_region2" rows="20" cols="60" id="edtr3"><?=stripslashes($row['edit_region2'])?></textarea><br style="clear:left;" />
    </li>

    
  </ul>
</form>
<br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>
