<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/local.class.php');

//create new instance of class
$local = new Local();

$id = $_GET['id'];

$ar = $local->getAutoResponders(NULL, NULL, NULL, $id, 'settings');
$row = $local->fetchArray($ar);

if($row['user_id']==$_SESSION['user_id']){
	if($row['table_name']=="directory") $back_page = "directory-autoresponders.php";
	else if($row['table_name']=="hub") $back_page = "hub-autoresponders.php";
	else if($row['table_name']=="press_release") $back_page = "press-autoresponders.php";
	else if($row['table_name']=="blogs") $back_page = "blogs-autoresponders.php";
	else if($row['table_name']=="articles") $back_page = "articles-autoresponders.php";
	else if($row['table_name']=="lead_forms") $back_page = "inc/forms/forms_autoresponder.php";
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	$(".helpLinks").fancybox({
		'width'				: '75%',
		'height'			: '75%',
		'autoScale'			: false,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'		: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none'
	});
	
});
</script> 
    
    <!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/autoresponse_settings.php?id=<?=$id?>" class="current">Settings</a></li>
		<li><a href="inc/forms/autoresponse_email.php?id=<?=$id?>">Email</a></li>
        <li class="last"><a href="<?=$back_page?>?id=<?=$row['table_id']?>">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />

	<h1>Autoresponder Settings</h1>
<form name="auto_responders" class="jquery_form" id="<?=$row['id']?>">
	<ul>
        <li><label>Name:</label><input type="text" name="name" value="<?=$row['name']?>" /></li><br />
        <li>
        	<label>Schedule:</label>
            <div style="color:#888;font-size:12px; padding-bottom:5px;">Amount of time from when the prospect first fills out the contact form until this email is sent.</div>
            <input type="text" name="sched" value="<?=$row['sched']?>" style="width:50px;" />
            <select name="sched_mode" title="select" class="uniformselect"><option value="hours" <? if($row['sched_mode']=="hours") echo 'selected="yes"'; ?>>Hours</option><option value="days" <? if($row['sched_mode']=="days") echo 'selected="yes"'; ?>>Days</option><option value="weeks" <? if($row['sched_mode']=="weeks") echo 'selected="yes"'; ?>>Weeks</option><option value="months" <? if($row['sched_mode']=="months") echo 'selected="yes"'; ?>>Months</option></select>
        </li>
    	<li><label>Active?</label>
            <select name="active" title="select" class="uniformselect">
            	<option value="1" <? if($row['active']==1) echo 'selected="yes"'; ?>>Yes</option>
            	<option value="0" <? if($row['active']==0) echo 'selected="yes"'; ?>>No</option>
            </select></li>
	</ul>
</form><br />
<? } else echo "Not authorized."; ?>