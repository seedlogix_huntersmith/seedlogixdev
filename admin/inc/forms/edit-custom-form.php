<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize
require_once(QUBEADMIN . 'inc/auth.php');
//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
if($id){
	$multiUser = $_GET['multi_user'] ? 1 : 0;
	$form = $hub->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], $id, $multiUser);
	$fields = $hub->parseCustomFormFields($form['data']);
	$options = explode('||', $form['options']);
	if($multiUser){
		//allowed classes for this page
		$allowed = explode('::', $form['no_access_classes']);
		//get user class names
		$classes = array();
		$query = "SELECT id, name FROM user_class WHERE admin_user = '".$_SESSION['login_uid']."'";
		$b = $hub->query($query);
		while($c = $hub->fetchArray($b)){
			$classes[$c['id']] = $c['name'];
		}
	}
	if(($form['user_id']!=$_SESSION['user_id']) && ($form['user_id']==$_SESSION['parent_id'])) $noEdit = true;
?>
<? if(!$noEdit){ ?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
	$('#newFormFieldType').die();
	$('#newFormFieldType').live('change', function(){
		var type = $(this).attr('value');
		$('.fieldOptions').each(function(){
			if($(this).hasClass('current')) $(this).removeClass('current');
			if($(this).attr('id')!=type+'_options') $(this).css('display', 'none');
			else {
				$(this).slideDown('fast');
				$(this).addClass('current');
			}
		});
	});
	$('#addSelectOption').die();
	$('#addSelectOption').live('click', function(){
		var currOptNum = parseInt($('#newSelectOptions').attr('rel'));
		var newOptNum = currOptNum+1;
		var currHTML = $('#newSelectOptions').html();
		var html = '<div id="newSelectOption'+newOptNum+'"><div style="display:inline-block;"><label>Value</label><input type="text" name="option_'+newOptNum+'_value" value="" class="no-upload" style="width:250px;" /></div>&nbsp;<div style="display:inline-block;"><label>Display Text</label><input type="text" name="option_'+newOptNum+'_text" value="" class="no-upload" style="width:450px;" /></div>&nbsp;&nbsp;<a href="#" class="delNewSelectOption">[x]</a></div>';
		
		$('#newSelectOptions').html(currHTML+html).attr('rel', newOptNum);
		
		return false;
	});
	$('.delNewSelectOption').die();
	$('.delNewSelectOption').live('click', function(){
		$(this).parent().slideUp().html('');
		
		return false;
	});
	$('.dateDefault').die();
	$('.dateDefault').live('change', function(){
		if($(this).attr('value')=='custom'){
			$('#customDefaultDate').slideDown();
		}
		else $('#customDefaultDate').css('display', 'none');
	});
	$('.nameFieldLabel').die();
	$('.nameFieldLabel').live('change', function(){
		var label = $(this).attr('name');
		$('#name_'+label).html($(this).attr('value'));
	});
	$('#addNewFormField').die();
	$('#addNewFormField').live('click', function(){
		$('.fieldOptions').each(function(){
			if(!$(this).hasClass('current')) $(this).html('');
		});
		
		var params = $('#addNewFormFieldForm').serialize()+'&action=addField';
		postUpdate(params);
		
		return false;
	});
	$('.deleteField').die();
	$('.deleteField').live('click', function(){
		var fieldNum = $(this).attr('rel');
		var params = { 'formID': <?=$id?>, 'userID': <?=$_SESSION['user_id']?>, 'action': 'delField', 'fieldID': fieldNum, 'multi_user': '<?=$multiUser?>' };
		if(confirm('Are you sure you want to delete this form field?')){
			postUpdate(params);
		}
		
		return false;
	});
	$('.moveField').die();
	$('.moveField').live('click', function(){
		var mode = $(this).attr('title');
		var fieldNum = $(this).attr('rel');
		var params = { 'formID': <?=$id?>, 'userID': <?=$_SESSION['user_id']?>, 'action': 'shiftField', 'mode': mode, 'fieldID': fieldNum, 'multi_user': '<?=$multiUser?>' };
		postUpdate(params);
		
		return false;
	});
	$('.formOptions').die();
	$('.formOptions').live('blur', function(){
		var option = $(this).attr('id');
		var val = $(this).attr('value');
		var params = { 'formID': <?=$id?>, 'userID': <?=$_SESSION['user_id']?>, 'option': option, 'value': val, 'action': 'editOption', 'multi_user': '<?=$multiUser?>' };
		postUpdate(params);
	});
	$('.updateCheckbox2').die();
	$('.updateCheckbox2').live('click', function(){
		var formID = $(this).attr('rel');
		var classID = $(this).attr('name');
		var params = { 'formID': formID, 'classID': classID, 'action': 'updateClassAccess', 'multi_user': '<?=$multiUser?>' };
		
		postUpdate(params);
	});
	$('.hideOptions').die();
	$('.hideOptions').live('click', function(){
		var currState = $('#currHideOptionsState').html();
		if(currState=='hide'){
			$('#optionsDiv').slideUp();
			$('#currHideOptionsState').html('show');
		}
		else {
			$('#optionsDiv').slideDown();
			$('#currHideOptionsState').html('hide');
		}
	});
});
function postUpdate(params){
	$.post('update-custom-form.php', params, function(data){
		//alert(data);
		var json = $.parseJSON(data);
		if(json.success == 1){
			$('#wait').show();
			$.get(json.refreshUrl, function(data){
				$("#ajax-load").html(data);
				$('#wait').hide();
			});
		}
		else {
			alert(json.message);
		}
	});
}
</script>
<? } ?>
<h1>Custom Form:</h1>
<h2 style="color:#000;margin-top:-10px;"><?=$form['name']?></h2>
<a href="hub-forms.php<? if($multiUser) echo '?multi_user=1'; ?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Forms" /></a>
<br />
<form class="jquery_form"><ul>
	<li style="margin-top:10px;">
		<label>Embed Code <small style="color:#666;">(Paste this into any Hub or Hub Page edit region)</small></label>
		<input type="text" class="no-upload" value="|@form-<?=$id?>@|" readonly />
	</li>
</ul></form>
<h2>Form Options</h2>
<? if($multiUser){ ?><a href="#" class="hideOptions dflt-link">Click here to <span id="currHideOptionsState" style="font-weight:bold;">hide</span> this section</a><? } ?>
<div id="optionsDiv">
<form class="jquery_form">
	<ul>
	<? if(!$noEdit){ ?>
		<li>
			<label>Show Reset Button</label>
			<select id="showReset" class="uniformselect formOptions no-upload">
				<option value="1" <? if($options[0]) echo 'selected'; ?>>Yes</option>
				<option value="0" <? if(!$options[0]) echo 'selected'; ?>>No</option>
			</select>
		</li>
		<li style="margin-top:-10px;">
			<label>Submit Button Text</label>
			<input type="text" id="submitText" value="<?=$options[1]?>" class="no-upload formOptions" />
		</li>
		<li>
			<label>Redirect URL <small style="color:#666;">(If successful)</small></label>
			<input type="text" id="redirUrl" value="<?=$options[2]?>" class="no-upload formOptions" />
		</li>
		<li>
			<label>AJAX Submit</label>
			<select id="ajaxSubmit" class="uniformselect formOptions no-upload">
				<option value="1" <? if($options[3]) echo 'selected'; ?>>Yes</option>
				<option value="0" <? if(!$options[3]) echo 'selected'; ?>>No</option>
			</select>
		</li>
		<? if($multiUser){ ?>
		<br /><br />
		<strong style="color:#666;">Multi-User Form Options:</strong><br />
		<li style="line-height:23px;">
			<label>Allowed User Types</label>
			<? foreach($classes as $key=>$value){ ?>
			<input type="checkbox" name="<?=$key?>" rel="<?=$id?>" class="updateCheckbox2" <? if(!in_array($key, $allowed)) echo 'checked'; ?> />&nbsp; <?=$value?><br />
			<? } ?>
		</li>
		<? } ?>
	<? } else { ?>
		<li>
			<br />
			<p style="font-weight:bold;">You don't have permission to edit this form, but you can still embed it into your hubs.</p>
		</li>
	<? } ?>
	</ul>
</form>
</div>

<? if($fields){ ?>

	<div class="titleSection" >

	<h2>Form Fields:</h2>

    </div>

	<div id="dash" title="<?=$id?>"><div id="list_hubformfields" title="item_list" class="dash-container"><div class="container">
    <br />
	<?
	foreach($fields as $field=>$params){
		$labelDisp = $params['label'];
		if($params['type']=='text') $typeDisp = 'Text';
		else if($params['type']=='textarea') $typeDisp = 'Text Area';
		else if($params['type']=='select') $typeDisp = 'Drop-down';
		else if($params['type']=='checkbox') $typeDisp = 'Checkbox';
		else if($params['type']=='hidden') $typeDisp = 'Hidden';
		else if($params['type']=='email') $typeDisp = 'Email';
		else if($params['type']=='money') $typeDisp = 'Money';
		else if($params['type']=='date') $typeDisp = 'Date';
		else if($params['type']=='name'){
			$typeDisp = 'Name';
			$labelDisp = $params['label1'].' / '.$params['label2'];
		}
		else if($params['type']=='section') $typeDisp = 'Section Header';
		echo '<div class="item2">';
			if(!$noEdit){ echo 
				'<div class="icons" style="width:140px;">
					<a href="#" class="moveField" title="Up" rel="'.$field.'" style="margin:0 2px;padding:0px;display:inline;float:left;"><img src="img/up-arrow-icon.gif" alt="Move up" border="0" /></a>
					<a href="#" class="moveField" title="Down" rel="'.$field.'" style="margin:0 2px;padding:0px;display:inline;float:left;"><img src="img/down-arrow-icon.gif" alt="Move down" border="0" /></a>
					<a href="hub-forms.php?mode=editField&id='.$id.'&field='.$field;
					if($multiUser) echo '&multi_user=1';
					echo '" class="edit" title="Edit"><span>Edit</span></a>
					<a href="#" class="delete deleteField field" rel="'.$field.'" title="Delete"><span>Delete</span></a>
				</div>';
			}
				
			echo '<img src="img/field-'.$params['type'];
				echo	'.png" ';
				
				echo 'class="icon" />'; 
					
			if(!$noEdit){
				echo '<a href="hub-forms.php?mode=editField&id='.$id.'&field='.$field;
				if($multiUser) echo '&multi_user=1';
				echo '"';
				if($params['type']=='section') echo ' style="color:#000;font-weight:bold;"';
				echo '>';
			}
			echo $typeDisp.': '.$labelDisp;
			if(!$noEdit) echo '</a>';
			echo '</div>';
			
		$labelDisp = $typeDisp = NULL;
	}
	?>
	</div></div></div>
<? } ?>
<? if(!$noEdit){ ?>

<br style="clear:both;" />
	<div class="titleSection" >

	<h2>Add a new field:</h2>

    </div>
    <br style="clear:both;" />

<strong>Field type:</strong>
<select class="uniformselect no-upload" id="newFormFieldType">
	<optgroup label="Basic">
		<option value="text" selected>Text Input</option>
		<option value="textarea">Text Area</option>
		<option value="select">Drop-down</option>
		<option value="checkbox">Checkbox</option>
		<option value="hidden">Hidden</option>
	</optgroup>
	<optgroup label="Presets">
		<option value="email">Email Address</option>
		<option value="money">Money Amount</option>
		<option value="date">Date</option>
		<option value="name">Name</option>
		<option value="states">US States</option>
	</optgroup>
	<optgroup label="Non-Input">
		<option value="section">Section Header</option>
	</optgroup>
</select>
<div id="formFieldOptions">
	<form class="jquery_form" id="addNewFormFieldForm">
		<strong>Field Options:</strong><br />
		<!-- TEXT INPUT -->
		<div id="text_options" class="current fieldOptions"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" /><br /><br />
				<label style="display:inline;float:left;padding:0;padding-right:10px;">Numbers only?</label>&nbsp;<input type="checkbox" name="numsOnly" style="display:inline;" />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" selected>Large</option>
					<option value="medium">Medium</option>
					<option value="small">Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="text" />
		</ul></div>
		<!-- END TEXT INPUT -->
		<!-- TEXT AREA -->
		<div id="textarea_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" selected>Large</option>
					<option value="medium">Medium</option>
					<option value="small">Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="textarea" />
		</ul></div>
		<!-- END TEXT AREA -->
		<!-- DROPDOWN -->
		<div id="select_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<li>
				<label><strong>Options</strong></label>
				<div id="newSelectOptions" rel="1" style="margin-top:-10px;">
					<div id="newSelectOption1">
						<div style="display:inline-block;">
							<label>Value</label>
							<input type="text" name="option_1_value" value="" class="no-upload" style="width:250px;" />
						</div>
						<div style="display:inline-block;">
							<label>Display Text</label>
							<input type="text" name="option_1_text" value="" class="no-upload" style="width:450px;" />
						</div>
					</div>
				</div>
				<a href="#" class="dflt-link" id="addSelectOption">[+] Add Another</a><br /><br />
			</li>
			<input type="hidden" name="fieldType" value="select" />
		</ul></div>
		<!-- END DROPDOWN -->
		<!-- CHECKBOX -->
		<div id="checkbox_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:83px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" /><br /><br />
				<label style="display:inline;float:left;padding:0;padding-right:10px;">Checked By Default?</label>&nbsp;<input type="checkbox" name="checked" style="display:inline;" />
			</li>
			<input type="hidden" name="fieldType" value="checkbox" />
		</ul></div>
		<!-- END CHECKBOX -->
		<!-- HIDDEN -->
		<div id="hidden_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Name</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Value</label><input type="text" name="default" value="" class="no-upload" /></li>
			<input type="hidden" name="fieldType" value="hidden" />
		</ul></div>
		<!-- END HIDDEN -->
		<!-- EMAIL ADDRESS -->
		<div id="email_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li>
			<li><label>Default Text</label><input type="text" name="default" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<li>
				<label>Size</label>
				<select name="size" class="uniformselect no-upload">
					<option value="large" selected>Large</option>
					<option value="medium">Medium</option>
					<option value="small">Small</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="email" />
		</ul></div>
		<!-- END EMAIL ADDRESS -->
		<!-- MONEY AMOUNT -->
		<div id="money_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li>
			<li><label>Default Value</label>$<input type="text" name="default" value="0.00" class="no-upload" style="width:75px;" /></li>
			<br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<input type="hidden" name="fieldType" value="money" />
		</ul></div>
		<!-- END MONEY AMOUNT -->
		<!-- DATE -->
		<div id="date_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li>
			<li>
				<label>Default Value</label>
				<select name="default" class="uniformselect no-upload dateDefault">
					<option value="" selected>Blank</option>
					<option value="today">Current Date</option>
					<option value="custom">Custom</option>
				</select>
			</li>
			<li id="customDefaultDate" style="display:none;margin-top:-10px;">
				<label>Custom Default Value</label>
				<div style="display:inline-block;">
					<input type="text" name="default_M" value="MM" class="no-upload" style="width:50px;" maxlength="2" />
				</div>
				/
				<div style="display:inline-block;">
					<input type="text" name="default_D" value="DD" class="no-upload" style="width:50px;" maxlength="2" />
				</div>
				/
				<div style="display:inline-block;">
					<input type="text" name="default_Y" value="YYYY" class="no-upload" style="width:75px;" maxlength="4" />
				</div>
				<br /><br />
			</li>
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<input type="hidden" name="fieldType" value="date" />
		</ul></div>
		<!-- END DATE -->
		<!-- NAME -->
		<div id="name_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label 1</label><input type="text" name="label1" value="First Name" class="no-upload nameFieldLabel" /></li>
			<li><label>Label 2</label><input type="text" name="label2" value="Last Name" class="no-upload nameFieldLabel" /></li>
			<li>
				<label><strong>Default Value</strong></label>
				<div style="display:inline-block;margin-top:-15px;">
					<label id="name_label1">First Name</label>
					<input type="text" name="default1" value="" class="no-upload" style="width:250px;" />
				</div>
				&nbsp;
				<div style="display:inline-block;margin-top:-15px;">
					<label id="name_label2">Last Name</label>
					<input type="text" name="default2" value="" class="no-upload" style="width:250px;" />
				</div>
			</li>
			<li>
				<label>Required?</label>
				<select name="required" class="uniformselect no-upload">
					<option value="no" selected>Neither</option>
					<option value="field1">Field 1</option>
					<option value="field2">Field 2</option>
					<option value="both">Both</option>
				</select>
			</li>
			<input type="hidden" name="fieldType" value="name" />
		</ul></div>
		<!-- END NAME -->
		<!-- US STATES -->
		<div id="states_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Label</label><input type="text" name="label" value="" class="no-upload" /></li>
			<li><label>Label Note <small style="color:#666;">(like this)</small></label><input type="text" name="label_note" value="" class="no-upload" /></li><br />
			<li>
				<label style="display:inline;float:left;padding:0;padding-right:44px;">Required?</label>&nbsp;<input type="checkbox" name="required" style="display:inline;" />
			</li>
			<input type="hidden" name="fieldType" value="states" />
		</ul></div>
		<!-- END US STATES -->
		<!-- SECTION -->
		<div id="section_options" class="fieldOptions" style="display:none;"><ul>
			<li><label>Text</label><input type="text" name="label" value="" class="no-upload" /></li>
			<input type="hidden" name="fieldType" value="section" />
		</ul></div>
		<!-- END SECTION -->
		
		<br />
		<input type="hidden" name="formID" value="<?=$id?>" />
		<input type="hidden" name="userID" value="<?=$_SESSION['user_id']?>" />
		<input type="hidden" name="multi_user" value="<?=$multiUser?>" />
       <a href="#" id="addNewFormField" class="greyButton" >Add Field</a>
	</form>
</div>
<? } ?>
<? } ?>