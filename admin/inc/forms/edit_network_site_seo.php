<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$uid = $_SESSION['login_uid'];
	$type = $_GET['type'];
	$id = $_GET['id'];
	
	$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
	$row = $local->queryFetch($query);
	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "seo";
include('edit_network_site_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h1>Edit SEO - <?=$row['name']?></h1>
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Main SEO Title</label>
				<input  type="text" name="seo_title" value="<?=$row['seo_title']?>" />
			</li>
			<li>
				<label>Main SEO Description</label>
				<input  type="text" name="seo_desc" value="<?=$row['seo_desc']?>" />
			</li>
			<li>
				<label>City Level SEO Title</label>
				<input  type="text" name="city_seo_title" value="<?=$row['city_seo_title']?>" />
			</li>
			<li>
				<label>City Level SEO Description</label>
				<input  type="text" name="city_seo_desc" value="<?=$row['city_seo_desc']?>" />
			</li>
			<li>
				<label>City Category Level SEO Title</label>
				<input  type="text" name="citycat_seo_title" value="<?=$row['citycat_seo_title']?>" />
			</li>
			<li>
				<label>City Category Level SEO Description</label>
				<input  type="text" name="citycat_seo_desc" value="<?=$row['citycat_seo_desc']?>" />
			</li>
			<li>
				<label>Google Site Verification Code</label>
				<span style="color:#666;font-size:75%;">Enter only the string, like <i>Gf2bIhOIO93BtsCorC4u2aQlFlD7WwOm6xAMMxCHE2Y</i></span>
				<input type="text" name="google_verif" value="<?=$row['google_verif']?>" /><br /><br />
				
			</li>
			<li>
				<label>Additonal Tracking Code</label>
			<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Google Analytics Embed Code</i></div>
				<textarea  name="google_stats" rows="6" ><?=htmlentities($row['google_stats'])?></textarea>
				<br style="clear:left" />
			</li>
		</ul>
	</form>
	
	<br />
	
</div>
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>