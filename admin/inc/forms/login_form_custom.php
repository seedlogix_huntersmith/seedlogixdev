<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	isset($_SESSION['first_visit']) ? $_SESSION['first_visit'] = 0 : $_SESSION['first_visit'] = 1;
	
	//get reseller info
	//$site var is set in parent file admin/login.php
	require_once('./inc/reseller.class.php');
	$reseller = new Reseller();
	$query = "SELECT `id`, `admin_user`, `favicon`, `login-bg`, `login-bg-clr`, `login-logo`, `login-brdr-clr`, `login-txt-clr`,
			`login-signup-lnk`, `login-lnk-clr`, `login-suggest-ff`, `support_email`, `support_phone` 
			FROM resellers WHERE main_site = '".$site."'";
	$resellerLogin = $reseller->queryFetch($query);
	$resellerInfo = $reseller->getResellerInfo($resellerLogin['admin_user']);
	
	//theme vars
	$bg = $resellerLogin['login-bg'] ? $resellerLogin['login-bg'] : '';
	$bgColor = $resellerLogin['login-bg-clr'] ? $resellerLogin['login-bg-clr'] : '000000';
	$logo = $resellerLogin['login-logo'] ? $resellerLogin['login-logo'] : 'img/dflt-login-logo.png';
	$border = $resellerLogin['login-brdr-clr'] ? $resellerLogin['login-brdr-clr'] : '333333';
	$text = $resellerLogin['login-txt-clr'] ? $resellerLogin['login-txt-clr'] : '333333';
	$link = $resellerLogin['login-signup-lnk'] ? $resellerLogin['login-signup-lnk'] : '#';
	$linkColor = $resellerLogin['login-lnk-clr'] ? $resellerLogin['login-lnk-clr'] : '00E';
	$ff = $resellerLogin['login-suggest-ff'] ? 1 : 0;
	
	//detect browser
	require_once('inc/phpsniff/phpSniff.class.php');
	$GET_VARS = isset($_GET) ? $_GET : $HTTP_GET_VARS;
	$POST_VARS = isset($_POST) ? $_GET : $HTTP_POST_VARS;
	if(!isset($GET_VARS['UA'])) $GET_VARS['UA'] = '';
	if(!isset($GET_VARS['cc'])) $GET_VARS['cc'] = '';
	if(!isset($GET_VARS['dl'])) $GET_VARS['dl'] = '';
	if(!isset($GET_VARS['am'])) $GET_VARS['am'] = '';
	$sniffer_settings = array('check_cookies'=>$GET_VARS['cc'],
						 'default_language'=>$GET_VARS['dl'],
						 'allow_masquerading'=>$GET_VARS['am']);
	$client = new phpSniff($GET_VARS['UA'],$sniffer_settings);
	
	$browser = $client->get_property('browser');
	
	if($browser=="ie") $browser = "Internet Explorer";
	else if($browser=="fx" || $browser=="mz") $browser = "Firefox";
	else if($browser=="sf"){
		if(strpos($client->get_property('ua'), "chrome")) $browser = "Chrome";
		else $browser = "Safari";
	}
	else if($browser=="op") $browser = "Opera";
	else $browser = "Unknown";

	//Messages to display
	if(isset($_GET['action'])){
		switch($_GET['action']):
			case 'password': $msg = 'Your password was sent to your email address.'; break;
			case 'password-error': $msg = 'We could\'t send your password to your email address. Please contact support.<br />'.
									urldecode($_GET['msg']); break;
			case 'username-error': $msg = 'We could\'t find that username in our system.'; break;
			case 'admin-account': $msg = 'For security reasons we cannot email password reset instructions to admins.
									<br />Please contact support to have your password manually changed.'; break;
		endswitch;
	}
	if(isset($_GET['error_code'])){
		switch ($_GET['error_code']):
			case 1: $msg = 'Your Account has not been activated.<br />Please check your email for activation link.'; break;
			case 2: $msg = 'Username or password was incorrect.'; break;
			case 3: $msg = "We couldn't find your account in our system."; break;
			case 4: $msg = "Access to your account has been disabled."; break;
			case 'x': 
			$msg = 'For your protection we\'ve disabled access to your account.<br />
				   Please contact us to verify your information and have it re-enabled.<br />';
			if($resellerLogin['support_email']) $msg .= '<br />Email: '.$resellerLogin['support_email'];
			if($resellerLogin['support_phone']) $msg .= '<br />Phone: '.$resellerLogin['support_phone'];
			break;
		endswitch;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$resellerInfo['company']?> .: Login Page</title>
<link rel="shortcut icon" href="themes/<?=$resellerLogin['admin_user']?>/<?=$resellerLogin['favicon']?>" />
<link rel="stylesheet" type="text/css" href="css/login-style.css" />
<style type="text/css">
	body { color: #<?=$text?>; }
	a { color: #<?=$linkColor?>; }
	img { border: none; }
	body#login-page { background: #<?=$bgColor?> url('<?='./themes/'.$resellerLogin['admin_user'].'/'.$bg?>') no-repeat; }
	body#login-page #wrapper #login_box { border: 1px solid #<?=$border?>; }
	.alignleft { float:left; margin-top:15px; }
</style>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$('.forgotLink').live('click', function(){
		$('#form-area').html($('#forgotForm').html());
		return false;
	});
	$('#name').focus();
});
</script>
</head>
<body id="login-page">
	<noscript>
		<div align="center"><h1 style="color:red;">Javascript is not currently enabled on your browser.<br />You must enable it for this website to function properly!</h1></div>
	</noscript>
	<div id="logo"><a href="./" target="_self"><img src="<?='./themes/'.$resellerLogin['admin_user'].'/'.$logo?>" alt="<?=$resellerInfo['company']?>" /></a></div>
	<div id="wrapper">
		<div id="login_box">
			<div id="inner-content">
				<? if($_GET['error_code']!='x'){ ?>
				<div id="error" style="color:black;font-size:14px;font-weight:bold;"><?=$msg?></div>
				<div id="form-area">
				<form action="validate_login.php" method="post" name="login" class="login-form" >
					<h2>Please login <span>(or <a href="<?=$link?>">Sign Up</a>)</span></h2>
					<p class="clearfix">
						<label>username</label>
						<input class="input1" type="text" name="username" id="name" />
					</p>
					<p class="clearfix">
						<label>password</label>
						<input class="input1" type="password" name="password" id="password" />
					</p>
					<!-- send current page for redirect back -->
					<input type="hidden" name="browser" value="<?=$browser?>" />
					<input type="hidden" name="page" value="./" />
					<input type="hidden" name="reseller-id" value="<?=$resellerLogin['admin_user']?>" />
					<!-- submit button -->
					<p class="clearfix">
						<span class="alignleft">
							<a href="#" style="color:#<?=$text?>;" class="forgotLink">Forgot Your Password?</a>
						</span>
						<span class="alignright"><input type="submit" value="Login" id="submit" /></span>
					</p>
				</form>
				<? } else { ?>
				<div id="error" style="color:black;font-size:14px;font-weight:bold;"><?=$msg?></div>
				<? } ?>
				</div>
			</div>
		</div><!-- end login-box div -->
	</div><br />
	<? if($ff){ ?>
	<div style="text-align:center;color:#fff;">This site works best with<br /><a href="http://www.mozilla.com/en-US/firefox/firefox.html" target="_blank"><img src="img/firefox-logo.png" alt="Mozilla Firefox" border="0" /></a></div>
	<? } ?>
	<div style="display:none;" id="forgotForm">
	<form action="retrieve_password.php" method="post" name="login" class="login-form" >
		<h2>Retrieve Password</h2>
		<p class="clearfix">
			<label>username:</label>
			<input class="input1" type="text" name="username" id="name" />
		</p><br />
		<!-- send current page for redirect back -->
		<input type="hidden" name="parent_id" value="<?=$resellerLogin['admin_user']?>" />
		<input type="hidden" name="parent_site" value="<?=$site?>" />
		<input type="hidden" name="page" value="login.php" />
		<!-- submit button -->
		<p class="clearfix">
			<span class="alignleft"><a href="#" style="color:#<?=$text?>;">Forget Your Password?</a></span>
			<span class="alignright"><input type="submit" value="Submit" id="submit" /></span>
		</p>
	</form>
	</div>
</body>
</html>