<?php 
	$curr = 'class="current"';
?>
<div id="side_nav">
	<ul id="subform-nav">
   		<li class="first">
			<a href="forms-autoresponders.php" <? if($thisPage=="formsauto") echo $curr; ?>>Custom Forms</a>
		</li>
		<li class="first">
			<a href="website-autoresponders.php" <? if($thisPage=="websiteauto") echo $curr; ?>>Websites</a>
		</li>
		<li>
			<a href="landing-autoresponders.php" <? if($thisPage=="landingauto") echo $curr; ?>>Landing Pages</a>
		</li>
		<li>
			<a href="social-autoresponders.php" <? if($thisPage=="socialauto") echo $curr; ?>>Social</a>
		</li>
		<li class="last">
			<a href="blog-autoresponders.php" <? if($thisPage=="blogauto") echo $curr; ?>>Blogs</a>
		</li>
	</ul>
</div>
<br style="clear:left;"  /><br />