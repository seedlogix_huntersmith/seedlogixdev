<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<h1>Upgrade Account</h1>
<p>
	We have a wide range of software editions available to suit any business' needs.  Click on either of the links below to learn more and compare, then come back and make your upgrade selection from the dropdown.
</p><br />
<h2>Inbound Marketing Software</h2>
<p><a href="http://6qube.com/pricing/inbound-marketing-software/" target="_blank" class="dflt-link">Click Here</a> to learn more about our Inbound Marketing Software.<br /><br />
	<select name="upgrade_class" title="select" class="uniformselect" id="elements-account">
		<option value="">Please choose an account type...</option>
		<? if($_SESSION['login_uid']==8966 || $_SESSION['login_uid']==7){ ?>
        <option value="845">CMS - $20/mo</option>
        <? } ?>
		<option value="422">Professional - $50/mo</option>
		<option value="423">Advanced &nbsp;&nbsp;&nbsp;&nbsp;- $100/mo</option>
		<option value="424">Corporate &nbsp;&nbsp;&nbsp;&nbsp;- $200/mo</option>
		<option value="425">Enterprise &nbsp;&nbsp;&nbsp;- $500/mo</option>
	</select>
	<a href="#" id="upgrade6QubeUser" class="dflt-link" rel="<?=$_SESSION['user_id']?>" title="elements-account"><img src="img/v3/upgrade.png" alt="Upgrade now!" /></a>
</p>
<br />
<h2>Local Internet Marketing Services</h2>
<p><a href="https://6qube.com/services/local-internet-marketing/" target="_blank" class="dflt-link">Click Here</a> to learn more about our Local Internet Marketing.<br /><br />
	<select name="upgrade_class" title="select" class="uniformselect" id="marketing-package">
		<option value="">Please choose an account type...</option>

		<option value="183">FastTrack &nbsp;&nbsp;&nbsp;&nbsp;- $200/mo</option>
		<option value="184">AdvTrack &nbsp;&nbsp;&nbsp;&nbsp;- $300/mo</option>
		<option value="185">ProTrack &nbsp;&nbsp;&nbsp;&nbsp;- $400/mo</option>
	</select>
	<a href="#" id="upgrade6QubeUser" class="dflt-link" rel="<?=$_SESSION['user_id']?>" title="marketing-package"><img src="img/v3/upgrade.png" alt="Upgrade now!" /></a>
</p>