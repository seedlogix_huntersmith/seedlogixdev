<?php
$isdev  =   $_SESSION['user_id']    ==  6610 || $_SESSION['user_id']    ==  6312;



#session_start();
#require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize Access to page

require_once(QUBEADMIN . 'inc/auth.php');
$hub = new Hub();

$hub_id = is_numeric($_GET['id']) ? (int)$_GET['id'] : 0;
$user_id        =   (int)$_SESSION['user_id'];

$messages       =   array();
$db =   Qube::GetPDO();

$mode   =  $_GET['mode'];   //'refresh';


function updatefeed(PDO $pdo, $feedid)
{
    $pdo->query('UPDATE feeds SET last_import = NOW() WHERE ID = ' . (int)$feedid);
}

?>

<!-- FORM NAVIGATION -->
<?php 

	$thisPage = "feeds";
	$parentID = $_SESSION['current_id'];
	$hasPages = true;
	$pagesV2 = true;
	include('add_hub_nav.php');
        
//get hub info and make sure the hub belongs to the user
$query = "SELECT has_feedobjects, user_id, httphostkey, 
        hub_parent_id, domain, theme, req_theme_type, pages_version, multi_user, multi_user_classes,
        multi_user_fields FROM hub WHERE id = '".$hub_id."' AND user_id= $user_id";
$hubInfo = $hub->queryFetch($query, NULL, 1);

if($hubInfo['has_feedobjects']  ==  0){     ?>

    <div></div>
    <br style="clear:left;"/>
    
<h2>There is no feed configured for this Website. Please enable a feed on the Pages tab first.</h2> <?php
    
}else{
$res    =    $db->query('SELECT ID, url, root, last_import, classname FROM feeds WHERE ID = ' . (int)$hubInfo['has_feedobjects']);
$feedobject =   $res->fetch(PDO::FETCH_OBJ);

$classname  =   $feedobject->classname;
require_once HYBRID_PATH . 'classes/custom/' . $classname . '.php';


$url    =   $hubInfo['httphostkey'];

$parseObject    =   new $classname;

$reporter   =   isset($_SESSION['feedreporter']) ? $_SESSION['feedreporter'] : null;

if(isset($_GET['delobj']) && isset($_GET['d']))
{
    $keys   = array();
    
    $query= "delete f, m, i from feedobjects f, feedobjects_meta m, feedobjects_catindex i where f.KEY IN (%s) and f.FEEDID=%d AND m.OBJECTID=f.ID and i.OBJECTID=f.ID";
    foreach($_GET['d'] as $d):
        $keys[] =   $db->quote($d);
    endforeach;
    
    $query  =   sprintf($query, join(',', $keys), $feedobject->ID);
    
#    echo $query;
    $q    =   $db->query($query);
    
    if($q)
        $messages[] =   'The items were deleted. <br />';
}

if(isset($_GET['saveobj']) && $reporter) // save new objects.
{
    $reportinfo=    $reporter->getItemsSummary($parseObject, $feedobject);
    $savedi =   0;
    foreach($reportinfo->new_items as $item):
        if(!isset($_GET['k']) || in_array($item['KEY'], $_GET['k'])){
            $obj    =   $item['object'];
            $obj->Save($feedobject);
            $savedi++;
        }
    endforeach;
    
    if($savedi)
        updatefeed ($db, $feedobject->ID);
    $messages[] =   "$savedi New Items were saved.";
    $mode   =   'refresh';
}

if($mode    ==  'refresh'):
    
        $feedobjects    =   new FeedObjects($feedobject->url);
#		if($_SESSION['user_id']== 6610) var_dump($feedobjects);
        /** @var SCLeadsFeedObject  */
        $reporter   =   new FeedReporter($feedobjects);
        $reporter->date =   $db->query('SELECT NOW();')->fetchColumn();
        $_SESSION['feedreporter'] =   $reporter;
        
endif;


if($reporter){    
#    echo 'reporter:';
#           var_dump($reporter);
            $feedobjects    =   $reporter->getFeedObjects();
        if($mode    ==  'acceptall'){
            $feedobjects->SaveAll($parseObject, $feedobject);
            updatefeed ($db, $feedobject->ID);

		$db->query('DELETE o, m, c, cs FROM feedobjects o, feedobjects_meta m, 
		feedobjects_catindex c, feedobjects_categories cs WHERE o.TS < "' . $feedobject->last_import . '" AND
	 o.FEEDID = ' . $feedobject->ID . ' AND c.OBJECTID = o.ID AND 
		m.OBJECTID=o.ID AND cs.FEEDID = o.FEEDID');

        }

        $reportinfo =   $reporter->getItemsSummary($parseObject, $feedobject);
}
#var_dump($feedobject);
#var_dump($parseObject);
#var_dump($reportinfo);
?>

<div id="createDir">
    
    <div></div>
    <br style="clear:left;"/>
    
<h2>Feed Configuration</h2>
        <?php
                if($mode    ==  'reviewexisting' && $reportinfo): 
                    
#                    var_dump($reportinfo);
                    ?><div>
    <forms action="bullshit" class="ajax" name="wtf" id="<?php echo $feedobject->ID; ?>">
        <h2>Local Feed Items (Check to delete.)</h2>
        
    <div style="height:600px; overflow: scroll;"><table>        
        <?php      $i = 0; foreach($reportinfo->items as $item):
            $i++;
        $object =   $item['object'];
        $object->current_path = $feedobject->root;
#                var_dump($item);
//                        $item   =   $feedobjects->getItem($i, $parseObject);
        ?>
        <tr><td><label><input type="checkbox" class="deletefeedobject" name="d[]" value="<?php echo $item['KEY']; ?>" 
                              onclick="$('#delete_lnk').attr('href', $('#delete_lnk').attr('href').replace(/&delobj=.*$/, '&delobj=1&' + $('.deletefeedobject:checked').serialize())).text('Delete ' + $('.deletefeedobject:checked').length + ' items');" />
            <?php echo $item['KEY']; ?></label></td>
            <td><a href="http://<?php echo $url . $object->getPageUrl(); ?>" class="external" target="_blank">
                    <strong> <?= $i; ?>. <?php echo htmlentities($item['title']); ?></strong></a></td></tr>
        <tr><td colspan="2"><blockquote><?php 
                echo htmlentities(substr(strip_tags($item['description']), 0, 128) . '...'); ?></blockquote>
            </td></tr>
        <?php   
                endforeach; ?></tbody>
        </table></div>
        <a id="delete_lnk" href="hub.php?form=feeds&id=<?php 
        echo $hub_id; ?>&delobj=1">
            Delete</a>
    </forms></div><br style="clear:left;"/>
    
            <?php
                
                    endif;
                    
                    if($mode    ==  'reviewnew' && $reportinfo): ?><div>
    <forms action="bullshit" class="ajax" name="wtf" id="<?php echo $feedobject->ID; ?>">
        <h2>New Feed Items (Check to import.)</h2>
        
    <div style="height:600px; overflow: scroll;"><table>        
        <?php      $i = 0; foreach($reportinfo->new_items as $item):
            $i++;
        $object =   $item['object'];
        $object->current_path = $feedobject->root;
#                var_dump($item);
//                        $item   =   $feedobjects->getItem($i, $parseObject);
        ?>
        <tr><td><label><input type="checkbox" class="importfeedobj" name="k[]" checked="checked" value="<?php echo $item['KEY']; ?>" 
                              onclick="$('#import_lnk').attr('href', $('#import_lnk').attr('href').replace(/&saveobj=.*$/, '&saveobj=1&' + $('.importfeedobj:checked').serialize())).text('Import ' + $('.importfeedobj:checked').length + ' items');" />
            <?php echo $item['KEY']; ?></label></td>
            <td><a href="http://<?php echo $url . $object->getPageUrl(); ?>" class="external" target="_blank">
                    <strong> <?= $i; ?>. <?php echo htmlentities($item['title']); ?></strong></a></td></tr>
        <tr><td colspan="2"><blockquote><?php 
                echo htmlentities(substr(strip_tags($item['description']), 0, 128) . '...'); ?></blockquote>
            </td></tr>
        <?php   
                endforeach; ?></tbody>
        </table></div>
        <a id="import_lnk" href="hub.php?form=feeds&id=<?php 
        echo $hub_id; ?>&saveobj=1">
            Import All Items</a>
    </forms></div><br style="clear:left;"/>
    <?php
        endif; ?>
                    
    
<table>
            
        
    <tr><th colspan="2">Feed Information</th></tr><?php if(count($messages)): 
            foreach($messages as $msg): ?><tr><td colspan="2"><?php echo $msg; ?></td></tr><?php
            endforeach;
    endif;
?>
    <tr><td width="200">Feed URL</td><td><?php echo $feedobject->url; ?></td></tr>
    <tr><td>Hub Path</td><td><a href="<?php echo $url . '/' . $feedobject->root; ?>" class="external" target="_blank"><?php echo $url; ?></a></td></tr>
    <tr><td>Last Import</td><td><?php echo $feedobject->last_import; ?>
                <a href="hub.php?form=feeds&id=<?= $hub_id; ?>&mode=refresh">Refresh</a></td>
    </tr>
    <?php   if($reporter):
                ?>
    <tr><th colspan="2">Feed Status</th></tr>
    <tr><td>Last Checked</td><td><?php echo $reporter->date; ?></td></tr>
        <tr><td>Total Feed Items: </td><td><?php echo $reportinfo->total_items; ?></td></tr>     
        <tr><td>Existing Items: </td>
            <td><a href="hub.php?form=feeds&id=<?php echo $hub_id; ?>&mode=reviewexisting"><?php 
                    echo $reportinfo->found_items_count; ?></a></td></tr>   
        <tr><td>Changed Items: </td><td><?php echo $reportinfo->changed_items_count; ?> / <?php echo $reportinfo->found_items_count; ?></td></tr>
        <tr><td>New Items: </td><td><?php if($reportinfo->new_items_count): ?><a href="hub.php?form=feeds&id=<?= $hub_id; ?>&mode=reviewnew">
                <?php echo $reportinfo->new_items_count; ?></a>
            <?php else: ?> None
                <?php endif; ?></td></tr>
        
        <?php   if($reportinfo->new_items_count || $reportinfo->changed_items_count || TRUE):   ?>
        <tr><td colspan="2"><a href="hub.php?form=feeds&id=<?= $hub_id; ?>&mode=acceptall">Accept All Changes</a></td></tr>
        <?php   else: ?>
        <tr><td colspan="2" align="center">The feed has not changed. All items are up to date.</td></tr>
        <?php
            endif; ?>
        
            <?php
        
            echo '<pre>';
//                    var_dump($reportinfo);
                echo '</pre>';
            ?>
    
    
    <?php   endif; ?>
    </table>
</div>
<?php
}
?>
<br style="clear:left;">
