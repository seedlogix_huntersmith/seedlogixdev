<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elements Internet Marketing Software Password Reset</title>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<link rel="stylesheet" href="http://hubs.6qube.com/themes/elements/css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<!--jquery library -->
<!--<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>-->
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="http://hubs.6qube.com/themes/elements/css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/scroll.js"></script>
<link href="http://www.6qube.com/css/uniform.default.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="http://www.6qube.com/js/jquery.uniform.min.js"></script>
<script type="text/javascript">
$(function(){
	$("select").uniform();
});
</script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="css/ie6.css" type="text/css" />
<![endif]-->
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
					<a title="Internet Marketing Software | Elements by 6Qube" href="http://elements.6qube.com/" ><img src="http://hubs.6qube.com/themes/elements/images/logo.png" /></a></div>
				</div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="http://elements.6qube.com/" title="Internet Marketing Software | Elements by 6Qube"><span>Home</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-about/" title="About Internet Marketing Software Elements by 6Qube"><span>About Us</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-features/" title="Internet Marketing Software Features | Elements Internet Marketing Tools"><span>App Features</span></a></li>
                                        <li><a href="http://elements.6qube.com/internet-marketing-tools-signup/" class="activelink" title="Sign-up for Internet Marketing Software Elements | Free | Professional | Agency"><span>Sign Up</span></a></li>												
										<li><a href="http://elementsblog.6qube.com/" title="Internet Marketing Software Blog | Internet Marketing Tools"><span>Blog</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-contact/" title="Contact Internet Marketing Software Elements by 6Qube"><span>Contact Us</span></a></li>
                                        <li><a href="http://login.6qube.com/" title="Login to Elements | Internet Marketing Software"><span>Login</span></a></li>
									</ul>
									
 
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					<br clear="all" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<h1>Elements Internet Marketing Software Packages</h1>
					<p></p>
					<br clear="all" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container_3-->
			<div id="container_3">
				<!--Start Container_3 Top-->
				<div id="container_3_top">
					<!--Start Container_3 Bottom-->
					<div id="container_3_bottom">
						<!--Start Breadcrumb-->
						<div id="breadcrumb">
							<ul>
								<li>You Are Here:</li>
								<li><a title="Internet Marketing Software | Elements by 6Qube" href="/" >Home</a></li>
								<li class="boldbreadcrumb">Reset Password</li>							 
							</ul>
						</div>
						<!--End Breadcrumb-->
						<br clear="all" />
						<!--Start Content-->
						<div id="content">
						<?
						echo '<div id="error" style="padding:15px;color:red;font-weight:bold;">'.$errors.'</div>';
						if($continue){
						?>
						<form action="password-reset.php" method="post" class="jquery_form">
							<label>Enter your new password</label>
							<input type="password" name="newPass" size="30" /><br />
							<input type="hidden" name="id" value="<?=$id?>" />
							<input type="hidden" name="email" value="<?=$email?>" />
							<input type="hidden" name="y" value="<?=md5($validation.$pepper)?>" />
							<input type="submit" value=" Submit " style="width:100px;height:34px;" />
						</form>
						<? } else if($success){ ?>
						Your password was successfully changed!<br />
						<a href="http://6qube.com/admin/" target="_self">Click here</a> to log in now.
						<? } else if($error==1){ ?>
						Your validation code seems to be invalid.<br />
						Please try using the Forgot Password form again to receive a new one.
						<? } else if($error==2){ ?>
						Sorry, we couldn't find an account in our system with that username.
						<? } else if($error==3){ ?>
						Sorry, there was an error updating your password.<br />
						Please try again or contact support.
						<? } else if($error==4){ ?>
						Authorization error.<br />
						Please try going back to your email and re-clicking the link we sent you.
						<? } else { ?>
						Something weird happened.  Please try again or contact support.
						<? } ?>
						</div>
						<!--End Content-->
						<!--Start Right Panel-->
						<div id="right_panel">
						<?	
						// ** Footer ** //
						require_once(QUBEROOT . 'includes/rightPanelE.inc.php');
						?>
							
						</div>
				    <!--End Right Panel-->
						<br clear="all" />
					</div>
					<!--End Container_3 Bottom-->
				</div>
				<!--End Container_3 Top-->
			</div>
			<!--End Container_3-->
		</div>
		<!--End Page Container-->
		<?	
		// ** Footer ** //
		require_once(QUBEROOT . 'includes/v2Efooter.inc.php');
		?>
</body>
</html>