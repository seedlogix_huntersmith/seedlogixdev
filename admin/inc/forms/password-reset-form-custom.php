<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	
	//get reseller info
	//$site var is set in parent file admin/login.php
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	$query = "SELECT `id`, `admin_user`, `main_site`, `favicon`, `login-bg`, `login-bg-clr`, `login-logo`, `login-brdr-clr`,
			`login-txt-clr`, `login-signup-lnk`, `login-lnk-clr`, `login-suggest-ff`, `support_email`, `support_phone` 
			FROM resellers WHERE admin_user = '".$_SESSION['temp_parent_id']."'";
	$resellerLogin = $reseller->queryFetch($query);
	$resellerInfo = $reseller->getResellerInfo($resellerLogin['admin_user']);
	
	//theme vars
	$bg = $resellerLogin['login-bg'] ? $resellerLogin['login-bg'] : '';
	$bgColor = $resellerLogin['login-bg-clr'] ? $resellerLogin['login-bg-clr'] : '000000';
	$logo = $resellerLogin['login-logo'] ? $resellerLogin['login-logo'] : 'img/dflt-login-logo.png';
	$border = $resellerLogin['login-brdr-clr'] ? $resellerLogin['login-brdr-clr'] : '333333';
	$text = $resellerLogin['login-txt-clr'] ? $resellerLogin['login-txt-clr'] : '333333';
	$link = $resellerLogin['login-signup-lnk'] ? $resellerLogin['login-signup-lnk'] : '#';
	$linkColor = $resellerLogin['login-lnk-clr'] ? $resellerLogin['login-lnk-clr'] : '00E';
	$ff = $resellerLogin['login-suggest-ff'] ? 1 : 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$resellerInfo['company']?> .: Login Page</title>
<link rel="shortcut icon" href="themes/<?=$resellerLogin['admin_user']?>/<?=$resellerLogin['favicon']?>" />
<link rel="stylesheet" type="text/css" href="css/login-style.css" />
<style type="text/css">
	body { color: #<?=$text?>; }
	a { color: #<?=$linkColor?>; }
	img { border: none; }
	body#login-page { background: #<?=$bgColor?> url('<?='./themes/'.$resellerLogin['admin_user'].'/'.$bg?>') no-repeat; }
	body#login-page #wrapper #login_box { border: 1px solid #<?=$border?>; }
	.alignleft { float:left; margin-top:15px; }
</style>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$('.forgotLink').live('click', function(){
		$('#form-area').html($('#forgotForm').html());
		return false;
	});
	$('#name').focus();
});
</script>
</head>
<body id="login-page">
	<noscript>
		<div align="center"><h1 style="color:red;">Javascript is not currently enabled on your browser.<br />You must enable it for this website to function properly!</h1></div>
	</noscript>
	<div id="logo"><a href="./" target="_self"><img src="<?='./themes/'.$resellerLogin['admin_user'].'/'.$logo?>" alt="<?=$resellerInfo['company']?>" /></a></div>
	<div id="wrapper">
		<div id="login_box">
			<div id="inner-content">
				<div id="error" style="color:black;font-size:12px;font-weight:bold;"><?=$errors?></div>
				<div id="form-area">
					<? if($continue){ ?>
					<form action="password-reset.php" method="post" class="login-form">
						<h2>Enter your new password:</h2>
						<input class="input1" type="password" name="newPass" value="" size="40" style="margin-top:-15px;" />
						<input type="hidden" name="email" value="<?=$email?>" />
						<input type="hidden" name="id" value="<?=$id?>" />
						<input type="hidden" name="r" value="<?=$_SESSION['temp_parent_id']?>" />
						<input type="hidden" name="y" value="<?=md5($validation.$pepper)?>" /><br /><br />
						<input type="submit" value="Submit" id="submit" />
					</form>
					<? } else if($success) { ?>
					Your password was successfully changed!<br />
					<a href="http://<?=$resellerLogin['main_site']?>/admin/" target="_self">Click here</a> to log in now.
					<? } else if($error==1){ ?>
					Your validation code seems to be invalid.<br />
					Please try using the Forgot Password form again to receive a new one.
					<? } else if($error==2){ ?>
					Sorry, we couldn't find an account in our system with that username.
					<? } else if($error==3){ ?>
					Sorry, there was an error updating your password.<br />
					Please try again or contact support.
					<? } else if($error==4){ ?>
					Authorization error.<br />
					Please try going back to your email and re-clicking the link we sent you.
					<? } else { ?>
					Something weird happened.  Please try again or contact support.
					<? } ?>
				</div>
			</div>
		</div><!-- end login-box div -->
	</div><br />
	<? if($ff){ ?>
	<div style="text-align:center;color:#fff;">This site works best with<br /><a href="http://www.mozilla.com/en-US/firefox/firefox.html" target="_blank"><img src="img/firefox-logo.png" alt="Mozilla Firefox" border="0" /></a></div>
	<? } ?>
</body>
</html>