<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, name, company_name, city, company_spot, video_spot FROM directory WHERE id = '".$_SESSION['current_id']."' ";
	$row = $directory->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript">
$(function(){
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>

<!-- FORM NAVIGATION -->
<?php
	$thisPage = "edit";
	include('add_listing_nav.php');
?>
<br style="clear:left;" />

<div id="form_test">
	<h2><?=$row['name']?> - Edit Regions</h2>
	<form class="jquery_form" name="directory" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Your Company Spot</label>
				<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="company_spot" rows="15" id="edtr1"><?=$row['company_spot']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Video Spot</label>
				<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="video_spot" rows="15" id="edtr2"><?=$row['video_spot']?></textarea>
				<br style="clear:left" />
			</li>
		</ul>
	</form>
</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Edit Regions Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#direditVideo" class="trainingvideo">Edit Regions Video</a>
			</li>
			<div style="display:none;">
				<div id="direditVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/directory-edit-overview.flv" style="display:block;width:640px;height:480px" id="direditplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("direditplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<li>
				<a href="#addVideo" class="trainingvideo">How to Add Videos?</a>
			</li>
			<div style="display:none;">
				<div id="addVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/addvideo-overview.flv" style="display:block;width:640px;height:480px" id="addvideoplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("addvideoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.6qube.com/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="helpLinks">Common HTML Tags</a>
			</li>
			<li>
				<a class="helpLinks" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new">Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="helpLinks">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="helpLinks" >Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="helpLinks" >Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new" >Resize or Crop Images</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
			<a class="greyButton external" style="margin-left:60px;" target="_blank" href="http://local.<?=$_SESSION['main_site']?>/directory.php?id=<?=$_SESSION['current_id']?>">Preview Listing</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?	
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>									
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<? } else echo "Error displaying page."; ?>