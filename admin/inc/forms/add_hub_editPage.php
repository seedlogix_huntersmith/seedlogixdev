<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$page_id = $_SESSION['current_id'];	
	$row = $hub->getSinglePage($page_id);
	
	if($row['user_id']==$_SESSION['user_id']){
		$page_results = $hub->getHubPage($row['hub_id'], NULL, NULL, 1);
		$site = $_SESSION['main_site'] ? $_SESSION['main_site'] : '6qube.com';
		if($reseller_site) $add = '&reseller_site=1';
		echo '<div id="dash" title="'.$row['hub_id'].'"><div id="list_hubpages" title="item_list" class="dash-container"><div class="container">';
		if($hub->getPageRows()){
			//loop through and out the hub pages (limits 5)
			while($page_row = $hub->fetchArray($page_results)){
				echo '<div class="item2">
						<div class="icons">
							<a href="hub.php?form=editPage&id='.$page_row['id'].$add.'&hub_id='.$row['hub_id'].'" class="edit"><span>Edit</span></a>';
				if($page_row['allow_delete'] || ($_SESSION['reseller'] || $_SESSION['admin'])) 
						echo	'<a href="#" class="delete" rel="table=hub_page&id='.$page_row['id'].'"><span>Delete</span></a>';
				echo		'</div>';
				if($_SESSION['theme']){
					echo '<img src="img/hub-page';
					if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];  
					echo '.png" class="blogPost" />';
				}
				else {
					echo '<img src="img/v3/hub-page.png" class="icon" />';
				}
				echo '<a href="hub.php?form=editPage&id='.$page_row['id'].$add.'&hub_id='.$row['hub_id'].'">'.$page_row['page_navname'].': '.$page_row['page_title'].'</a>
					</div>';
			}
		}
		echo '<br /><a href="hub.php?form=pages&id='.$row['hub_id'].$add.'&hub_id='.$row['hub_id'].'" style="margin-left:110px;" class="dflt-link">New Page</a>';
		echo '</div></div></div>';
		
		if($reseller_site){
			$img_url = '/reseller/'.$_SESSION['login_uid'].'/';
		} else {
			$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub_page/';
		}
		
		if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
		else $dflt_noImg = 'img/no-photo.jpg';
		
		$query = "SELECT theme, req_theme_type FROM hub WHERE id = '".$row['hub_id']."'";
		$row2 = $hub->queryFetch($query);
		$query = "SELECT user_class2_pages FROM themes WHERE id = '".$row2['theme']."'";
		$themeInfo = $hub->queryFetch($query);
				
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if($row2['req_theme_type']==4 && $_SESSION['reseller']){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$addClass = ' muPage multiUser-'.$row2['theme'].'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && $hub->isMultiUserTheme($row2['theme'])){
			$multi_user2 = true;
			//figure out which fields to lock
			if($themeInfo['user_class2_pages']){
				$fieldsInfo = json_decode($themeInfo['user_class2_pages'], true);
				foreach($fieldsInfo as $key=>$val){
					foreach($fieldsInfo[$key] as $key2=>$val2){
						if($fieldsInfo[$key][$key2]['lock']==1)
							$lockedFields[] = $key2;
					}
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller']){
						return 'readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<div id="message-container"></div>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "pages";
	$parentID = $row['hub_id'];
	$hasPages = true;
	include('add_hub_nav.php');
?>
<div id="form_test">
	<h2>Edit Page</h2>
	<?php 
		$hub_row = $hub->queryFetch("SELECT domain FROM hub WHERE id = '".$row['hub_id']."'");
		if($hub_row['domain']){
			if(substr($hub_row['domain'], -1) != "/") $hub_row['domain'] .= "/"; //check/add trailing slash
			$pageURL = $hub_row['domain'].$hub->convertKeywordUrl($row['page_title']);
			echo '<span style="font-size:14px;color:#666;">Page URL:<br />';
			echo '<a href="'.$pageURL.'" target="_blank" class="view dflt-link"><i>'.$pageURL.'</i></a></span>';
		}
	?>
    <br /><br />
    <?php include('hubs_tags.php'); ?>
	<form name="hub_page" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<? if($multi_user){ ?>
			<li>
				<label>Allow Page Deletion?</label>
				<select name="allow_delete" title="select" class="uniformselect<?=$addClass?>">
					<option value="1" <? if($row['allow_delete']==1) echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['allow_delete']==0) echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<? } ?>
			<li>
				<label>Title</label>
				<? if($multi_user){ ?>
				<select name="lock_page_title" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('page_title')?> name="page_title" value="<?=$row['page_title']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Navigation Name</label>
				<? if($multi_user){ ?>
				<select name="lock_page_navname" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_navname',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('page_navname')?> name="page_navname" value="<?=$row['page_navname']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Page Edit Region</label>
				<? if($multi_user){ ?>
				<select name="lock_page_region" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_region',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('page_region')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="page_region" <?=checkLock('page_region')?> rows="10" id="edtr1" class="<?=$addClass?>"><?=$row['page_region']?></textarea>
				<br style="clear:left" />
			</li>
			<li>
				<label>Keywords</label>
				<? if($multi_user){ ?>
				<select name="lock_page_keywords" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_keywords',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Comma-separated. Example: <i>Austin Flooring, California Sun Rooms, ...</i> (up to six)</div>
				<input type="text" name="page_keywords" <?=checkLock('page_keywords')?> value="<?=$row['page_keywords']?>" class="<?=$addClass?>" />
			</li>
			 <li>
				<label>SEO Title *optional</label>
				<? if($multi_user){ ?>
				<select name="lock_page_seo_title" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_seo_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_seo_title" <?=checkLock('page_seo_title')?> value="<?=$row['page_seo_title']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>SEO Page Description</label>
				<? if($multi_user){ ?>
				<select name="lock_page_seo_desc" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_seo_desc',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" name="page_seo_desc" <?=checkLock('page_seo_desc')?> value="<?=$row['page_seo_desc']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Include Contact Form?</label>
				<? if($multi_user){ ?>
				<select name="lock_inc_contact" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('inc_contact',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<select name="inc_contact" title="select" class="uniformselect<?=$addClass.' '.checkLock('inc_contact')?>">
					<option value="1" <? if($row['inc_contact']==1) echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['inc_contact']==0) echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<?php if($row2['theme'] == "24" || $row2['theme'] == "57" || $row2['theme'] == "78" || $row2['theme'] == "79" || $row2['theme'] == "80" || $row2['theme'] == "81" || $row2['theme'] == "82" || $row2['theme'] == "85" || $row2['theme'] == "86" || $row2['theme'] == "98" || $row2['theme'] == "99" || $row2['theme'] == "102" || $row2['theme'] == "104") {?>
			<li>
				<label>Full Width Page?</label>
				<? if($multi_user){ ?>
				<select name="lock_page_full_width" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_full_width',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<select name="page_full_width" title="select" class="uniformselect<?=$addClass.' '.checkLock('page_full_width')?>">
					<option value="1" <? if($row['page_full_width']==1) echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['page_full_width']==0) echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<? } ?>
			<li>
				<label>Include In Navigation Links?</label>
				<? if($multi_user){ ?>
				<select name="lock_nav" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('nav',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<select name="nav" title="select" class="uniformselect<?=$addClass.' '.checkLock('nav')?>">
					<option value="1" <? if($row['nav']==1) echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['nav']==0) echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			
			<?php if($row2['theme'] == "24" || $row2['theme'] == "54" || $row2['theme'] == "57" || $row2['theme'] == "58" || $row2['theme'] == "78" || $row2['theme'] == "79" || $row2['theme'] == "80" || $row2['theme'] == "81" || $row2['theme'] == "82" || $row2['theme'] == "85" || $row2['theme'] == "86" || $row2['theme'] == "98" || $row2['theme'] == "102" || $row2['theme'] == "104") {?>
			<li>
				<label>Column Edit Region</label>
				<? if($multi_user){ ?>
				<select name="lock_page_edit_2" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_edit_2',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('page_edit_2')){ ?><a href="#" id="2" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="page_edit_2" <?=checkLock('page_edit_2')?> rows="10" id="edtr2" class="<?=$addClass?>"><?=$row['page_edit_2']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo</label>
		<? if($multi_user){ ?>
		<select name="lock_page_photo" title="select" class="uniformselect pagesLock imgLock<?=$addClass?>" rel="<?=$page_id?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('page_photo',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('page_photo')){ ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub_page" />
		<input type="hidden" name="fname" value="page_photo" />
		<input type="hidden" name="img_id" value="<?=$page_id?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$row2['theme']?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
		<br style="clear:both;" />
	</form>
	<img id="img_one_prvw" class="img_cnt" src="<? if($row['page_photo']) echo $img_url.$row['page_photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<form name="hub_page" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Description</label>
				<? if($multi_user){ ?>
				<select name="lock_page_photo_desc" title="select" class="uniformselect pagesLock<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('page_photo_desc',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Optional: Set the alt tag for the image on your page.</div>
				<input type="text" <?=checkLock('page_photo_desc')?> name="page_photo_desc" value="<?=$row['page_photo_desc']?>" class="<?=$addClass?>" />
			</li>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Edit Hub Page Videos</h2>
	<div class="app_right_links">
		<ul>
			<li><a href="#hubpageseditVideo" class="trainingvideo">Edit HUB Page Video</a></li>
			<div style="display:none;">
				<div id="hubpageseditVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-pagesedit-overview.flv" style="display:block;width:640px;height:480px" id="hubpageseditplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubpageseditplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	<? } ?>
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.w3schools.com/html/html_primary.asp" class="external" target="_new">Common HTML Tags</a>
			</li>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://animoto.com/?ref=a_nbxcrspf" class="external" target="_new">Make Instant Videos</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
	</div> <!--End APP Right Links-->
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>							
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>