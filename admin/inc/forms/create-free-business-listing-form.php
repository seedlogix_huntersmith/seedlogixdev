<h1>Create Free Business Listing</h1>
<p>All fields are required to register a new user</p>
<!--<p>Please note that simply creating an account <strong>DOES NOT</strong> create your <strong>Free Business Listing</strong>.  You must login and configure your listing before it is listed live on our directory.  </p>-->
<form action="/create-free-business-listing/" class="jquery_form" method="post" id="sign-up">
	<label>Email Address (What you login with)</label><input type="text" name="email" size="30" value="<?=$_POST['email']?>" /><br />
	<div style="display:inline-block;margin-top:5px;">
		<label style="display:inline;">Password</label><br /><br />
		<input type="password" name="password" value="" style="width:214px;display:inline;" />
	</div>&nbsp;
	<div style="display:inline-block;">
		<label style="display:inline;">Re-Enter Password</label><br /><br />
		<input type="password" name="password2" value="" style="width:213px;display:inline;" />
	</div>
	<label>First Name</label><input type="text" name="first_name" value="<?=$_POST['first_name']?>" /><br />
	<label>Last Name</label><input type="text" name="last_name" value="<?=$_POST['last_name']?>" /><br />
	<label>Phone Number</label><input type="text" name="phone" value="<?=$_POST['phone']?>" /><br />
	<label>Company Name</label><input type="text" name="company" value="<?=$_POST['company']?>" /><br />
	<label>Address</label><input type="text" name="address" value="<?=$_POST['address']?>" /><br />
	<label>Address 2 - <small>(optional)</small> </label><input type="text" name="address2" value="<?=$_POST['address2']?>" /><br />
	<label>City</label><input type="text" name="city" value="<?=$_POST['city']?>"  /><br />
	<label>State</label>
	<select name="state">
		<option value="">Choose one...</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>
	</select><br />
	<label>Zip Code</label><input type="text" name="zip" size="5" maxlength="5" value="<?=$_POST['zip']?>" /><br />
	<input name="userType" value="178" type="hidden" /><br />
	<h2>Basic listing information</h2>
	<p>
		This is just to get your listing started.<br />
		Once you activate your account and log in you can add much more to it.
		<label>Business Category</label>
		<?=$local->displayIndustries()?>
		<label>Targeted Category</label>
		<div style="color:#888;font-size:12px;margin-top:-15px;">
			Example: <i>Austin Landscaping</i> or <i>Houston Golf Instructor</i>.
		</div>
		<input type="text" name="keyword_one" value="<?=stripslashes($_POST['keyword_one'])?>" maxlength="200" />
		<label>Business Listing Description</label>
		<textarea name="listing_desc" rows="5"><?=stripslashes($_POST['listing_desc'])?></textarea>
		<label>Website URL</label>
		<input type="text" name="website_url" value="<?=$_POST['website_url']?>" maxlength="255" />
	</p>
	<br style="clear:both" />
	<input type="image" src="http://hubs.6qube.com/themes/elements/images/start-button.png" style="width:108px;height:33px;padding:0;background:none;border:none;" />
</form>
<br style="clear:both" /><br />
<br style="clear:both" /><br />