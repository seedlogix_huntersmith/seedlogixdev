<?php
	$curr = 'class="current"';
?>
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/edit-user.php?id=<?=$id?>" <? if($thisPage=='basics') echo $curr; ?>>Basics</a></li>
		<li><a href="inc/forms/edit-user-info.php?id=<?=$id?>" <? if($thisPage=='details') echo $curr; ?>>Details</a></li>
		<? if($isReseller && !$_SESSION['support_user']){ ?>
		<li><a href="inc/forms/edit-user-reseller.php?id=<?=$id?>" <? if($thisPage=='reseller') echo $curr; ?>>System Info</a></li>
		<? if($_SESSION['admin']){ ?>
		<li><a href="inc/forms/edit-user-reseller-products.php?id=<?=$id?>" <? if($thisPage=='reseller-products') echo $curr; ?>>Products</a></li>
		<? } ?>
		<? } ?>		
		<? if(!$_SESSION['support_user']) { ?>
		<li class="last"><a href="inc/forms/edit-user-billing.php?id=<?=$id?>" <? if($thisPage=='billing') echo $curr; ?>>Billing Info</a></li>
	    <? } ?>
	</ul>
</div>