<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	
	//create new instance of class
	$reseller = new Reseller();
	
	$id = $_GET['id'] ? $_GET['id'] : $_POST['id'];
	
	if($emailer = $reseller->getResellerEmailers($_SESSION['login_uid'], $id))
		$row = $emailer[$id];
	
	if($_POST['sendTest']){
		$to = $_POST['user_email'];
		$subject = str_replace("#name", "John Doe", $row['subject']);
		$subject = str_replace('#company', 'Company, Inc.', $subject);
		$message = stripslashes($row['body']);
		$message = str_replace("#name", "John Doe", $message);
		$message = str_replace("#company", "Company, Inc.", $message);
		if($row['emailer_type']=='marketing'){
			$message .= "<br /><br />".$row['contact'];
			$message .= "<br />".$row['anti_spam'];
		}
		
		$send = $reseller->sendMail($to, $subject, $message, NULL, $row['from_field'], true, true);
		if($send['success']) echo '<script type="text/javascript">alert("Test email sent successfully to '.$to.'.");</script>';
		else echo '<script type="text/javascript">alert("Error sending test email: \n'.$send['errors'].'");</script>';
	}
	else if($_POST['sendNow'] || $_GET['getList']==1){
		//if reseller or admin is sending an instant email
		//OR they clicked the "see a list of users" link
		$isInclude = true;
		if($row['admin_user']==7 || $row['admin_user']==40) $admin = true;
		//determine the send settings to construct user queries
		//sendto_class
		if($row['sendto_class']==-1) $class = ""; //email all users
		else if($row['sendto_class']==-2 || $row['sendto_class']==-3 || $row['sendto_class']==-4){
			if($admin){
				//6qube admin
				if($row['sendto_class']==-2){
					//all non-free
					$class = " AND class != 1 AND class != 178 ";
				}
				else if($row['sendto_class']==-3 || $row['sendto_class']==-4){
					//all free users
					$class = " AND (class = 1 OR class = 178) ";
					if($row['sendto_class']==-3){
						//all free users who've never logged in
						$class .= " AND last_login = '0000-00-00 00:00:00' ";
					}
				}
			}
			else {
				//get reseller's free class id
				$query = "SELECT id FROM user_class WHERE maps_to = 27 AND admin_user = '".$_SESSION['login_uid']."' LIMIT 1";
				if($a = $reseller->queryFetch($query)){
					if($row['sendto_class']==-2){
						//email all non-free users
						$class = " AND class != '".$a['id']."' ";
					}
					else if($row['sendto_class']==-3 || $row['sendto_class']==-4){
						//email all free users [who've never logged in]
						//all free users (-4)
						$class = " AND class = '".$a['id']."' ";
						//who've never logged in (-3)
						if($row['sendto_class']==-3) $class .= " AND last_login = '0000-00-00 00:00:00' ";
					}
				}
				else $class = " AND class = 999999 ";
			}
		}
		else $class = " AND class = '".$row['sendto_class']."' ";
		//sendto_type
		if($row['sendto_type']=='all') $active = '';
		else if($row['sendto_type']=='active') $active = " AND access = '1' AND canceled = '0' ";
		else if($row['sendto_type']=='suspended') $active = " AND access = '0' AND canceled = '0' ";
		else if($row['sendto_type']=='active_suspended') $active = " AND canceled = '0' ";
		else if($row['sendto_type']=='canceled') $active = " AND canceled = '1' ";
		else $active = "";
		//optout for marketing emails
		if($row['emailer_type']=='marketing') $optout = " AND marketing_optout != 1 ";
		else $optout = "";
		//query ids
		$query = "SELECT id, username FROM users ";
		if(!$admin) $query .= " WHERE parent_id = '".$_SESSION['login_uid']."' ";
		else $query .= " WHERE (parent_id = 0 OR parent_id = 7) ";
		$query .= $class.$active.$optout;
		if($b = $reseller->query($query)){
			if($reseller->numRows($b)){
				//set up user ids to pass to email_users script
				$users = '';
				$usersEmails = array();
				while($c = $reseller->fetchArray($b, NULL, 1)){
					if(!in_array($c['username'], $usersEmails)){
						//if email address isn't already in the list
						$users .= $c['id'].',';
						$usersEmails[$c['id']] = $c['username'];
					}
					else $usersEmails[$c['id']] = $c['username']; //add the userid/email to the array anyway for use below
				}
				$userID = trim($users, ",");
				if($_POST['sendNow']){
					//set up other email vars to pass
					$emailerID = $id;
					$userID = $users;
					$emailSubject = $row['subject'];
					$emailBody = $row['body'];
					if($row['emailer_type']=='marketing'){
						$emailBody .= '<br /><br />'.$row['contact'].'<br />'.$row['anti_spam'];
					}
					//FROM info
					$customFrom = $row['from_field'];
					//include script
					include_once(QUBEADMIN . 'email_users.php');
				}
				else {
					//output a list of users who will be emailed
					$usersArray = explode(',', $userID);
					$i = 0;
					if($usersArray){
						foreach($usersArray as $key=>$value){
							if($i<=50){
								//get email address
								//$query = "SELECT username FROM users WHERE id = '".$value."'";
								//$d = $reseller->queryFetch($query);
								//output like "105 - username@email.com"
								echo $value.' - '.$usersEmails[$value].'<br />';
								$i++;
							}
							else $limitReached = true;
						}
						if($row['sched_mode2']=='signup' && $row['retro']==1){
							echo '<strong>Plus all future signups</strong><br />';
						}
						if($limitReached){
							echo '<strong>'.(count($usersArray)-50).' more not shown...</strong>';
						}
					}
					else echo 
					'<script type="text/javascript">alert("No users were found that matched your sending criteria.");</script>';
				}
			}
			//if no users were found for criteria:
			else echo '<script type="text/javascript">alert("No users were found that matched your sending criteria.");</script>';
		}
		//if no users were found for criteria:
		else echo '<script type="text/javascript">alert("No users were found that matched your sending criteria.");</script>';
	}
	else {
		if($row['admin_user']==$_SESSION['login_uid']){
			if($row['from_field']){
				$fromField = str_replace('<', '&lt;', $row['from_field']);
				$fromField = str_replace('>', '&gt;', $fromField);
				$fromField = str_replace('" ', '', $fromField);
				$fromField = str_replace('"', '', $fromField);
			}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
    
<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/reseller_emailers_settings.php?id=<?=$id?>">Settings</a></li>
		<li><a href="inc/forms/reseller_emailers_email.php?id=<?=$id?>" class="current">Email</a></li>
		<li class="last"><a href="reseller-emailers.php">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />
<br />
	<h1>Emailer Message</h1>
	<form name="resellers_emailers" class="jquery_form" id="<?=$row['id']?>">
		<ul>
			<? if($row['sched_mode2']=='now' || ($row['sched_mode2']=='signup' && $row['retro']==1)){ ?>
			<li>
				<label>To:</label>
				<a href="#" id="sendtoEmailList" class="dflt-link" rel="<?=$id?>">[+] Click here to show a list of users who will be emailed</a>
				<div id="sendToEmailListDiv" style="display:none;margin-top:5px;" name="0"></div>
			</li>
			<? } ?>
			<li>
				<label>From:</label>
				<span style="color:#666; font-size:75%"><strong>Must</strong> be in this format: <strong>Company Name &lt;email@company.com&gt;</strong></span><br />
				<input type="text" name="from_field" value="<?=$fromField?>" />
			</li>
			<li><label>Subject:</label><input type="text" name="subject" value="<?=$row['subject']?>" /></li>
			<li>
				<label>Message:</label>
				<a href="#" id="AR1" class="tinymce"><span>Rich Text</span></a>
				<? if($_SESSION['user_class']==5){ ?>
					<? if($templates && $local->numRows($templates)){
						echo '<select class="ar-templates no-upload">';
						echo '<option value="">Or choose a template...</option>';
						while($temprow = $local->fetchArray($templates)){
							echo '<option value="'.$temprow['id'].'">'.$temprow['theme_name'].'</option>';
						}
						echo '</select>';
					} ?>
				<? } ?>
				<textarea name="body" rows="15" cols="80" id="edtrAR1"><?=$row['body']?></textarea><br style="clear:left" />
			</li>
			<span style="font-size:9.5px;"><strong>Hint:</strong> You can use <strong>"#name"</strong> (without quotes) in the body or subject and it will automatically be<br />replaced with the prospect's name when the email is sent.<br /><strong>#company</strong> is replaced with the user's company.</span><br />
			<? if($row['emailer_type']=='marketing') { ?>
			<br />
			<span style="color:#000; font-size:75%; font-weight:bold;">The following fields are required for the email to be sent, due to the CAN-SPAM act.</span><br />Feel free to customize them.<br />
			<li>
				<label>Business Name/Address:</label>
				<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="contact" rows="5" cols="80" id="edtr2"><?=stripslashes($row['contact'])?></textarea>
				<br style="clear:left;" />
			</li>
			<li>
				<label>Anti-Spam Message:</label>
				<textarea name="anti_spam" rows="3" cols="80" id="edtr3"><?=$row['anti_spam']?></textarea>
				<br style="clear:left;" />
			</li>
			<? } ?>
		</ul>
	</form><br />
    <div id="createDir">
	<form class="jquery_form" action="inc/forms/reseller_emailers_email.php" method="POST" target="hiddenIframe">
		<label>Send Test Email:</label>
		<span style="color:#666; font-size:75%">Enter your email below and click submit to be sent a test copy of this email.<br /></span><br />
		<input type="text" value="<?=$_SESSION['user']?>" name="user_email" style="width:350px;" />
		<input type="hidden" value="<?=$id?>" name="id" />
		<input type="submit" value="Send Test Email" name="sendTest"  />
	</form>
    </div>
	<? if($row['sched']==0 && $row['active']==1 && $row['sched_mode2']=='now'){ ?>
	<br /><br /><br />
	<form class="jquery_form" action="inc/forms/reseller_emailers_email.php" method="POST" target="hiddenIframe">
		<label>Send Email Now:</label>
		<span style="color:#666; font-size:75%"><b>We strongly recommend sending yourself a test copy before<br />broadcasting this message out to your users.</b></span><br /><br />
		<input type="hidden" value="<?=$id?>" name="id" />
		<input type="hidden" value="1" name="sendNow" />
		<input type="submit" value="Launch Broadcast" name="sendNow"  onclick="javascript:$(this).slideUp();" />
	</form>
    <br /><br /><br />
	<? } ?>
	<br /><br style="clear:both;" />
	<iframe name="hiddenIframe" id="hiddenIframe" src="testemail.php" style="display:none;width:1px;height:1px;"></iframe>
	<? } else echo "Not authorized."; ?>
<? } ?>