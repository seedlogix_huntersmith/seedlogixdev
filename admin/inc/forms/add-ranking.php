<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once('../auth.php');

//initiate the class
require_once(QUBEADMIN . 'inc/rankings.class.php');
$rankings = new Rankings();

//User's nickname for website/domain
$name = $rankings->validateText($_POST['name']);

//	validate user domain
//	valid examples are: 
//	www.6qube.com
//	6qube.com	
//	no http:// or with a forward slash
// Created a new custom validation function instead of using function:validateDomain()

$site = $rankings->validateDomainforRankings($_POST['site']);
//$site = false;

if(isset($_POST['keywords']) && isset($_POST['the_main_site_id']))
{
	$keywords	=	explode("\n", $_POST['keywords']);
	$siteid		=	(int)$_POST['the_main_site_id'];
	$rankings->addKeywords($keywords, $siteid, $_SESSION['user_id']);
}else
//if both the name and domain validate
if($name && $site)
{	
	//returns mysql insert ID or false
	
	/*********************************************************************************************************
		UNCOMMENT THE NEXT LINE TO START ADDING COMAPAIGNS to SIMPLYRANK.com AND DELETE "$rankingID = true;"
	/********************************************************************************************************/
	
	$rankingID = $rankings->addNewRankings($_SESSION['user_id'], $_SESSION['campaign_id'], $name, $site);
#	$rankingID = true;
	
	//if new ranking was added then send back html and success codes 
	if($rankingID)
	{
		$html = '<div class="contactContain">
					<ul class="prospects" title="'.$rankingID.'">
						<li class="name" style="width:25%;"><a href="rankings.php?mode=view&id='.$rankingID.'">777'.$name.'</a></li>
						<li class="email" style="width:22%;"><i>'.$site.'</i></li>
						<li class="email" style="width:20%;"><i></i></li>
					</ul>
				</div>';
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#new_ranking .newRanking';
		$response['success']['message'] = $html;
	}
	//if new ranking campaing was unable to be created - send user an error message
	else
	{
		$response['error']['id'] = '1';
		$response['error']['type'] = 'displayMessage';
		$response['error']['message'] = '<div id="formErrorMessageContainer"><span class="error">There was an error creating your campaing. Please <a href="http://6qube.com/contact/">Contact support.</a>.</span></div>';
		$response['error']['container'] = '#formErrorMessageContainer';	
	}
}
//if there was a problem domain name the user entered show an error message
else if(!$site)
{
	$response['error']['id'] = '1';
	$response['error']['type'] = 'displayMessage';
	$response['error']['message'] = '<div id="formErrorMessageContainer"><span class="error">There was an error with your domain name - '.$site.'. Proper examples are "6qube.com" amd "wwww.6qube.com</span></div>';
	$response['error']['container'] = '#formErrorMessageContainer';
}
	echo json_encode($response);
?>