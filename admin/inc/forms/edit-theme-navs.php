<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	//require_once(QUBEADMIN . 'inc/reseller.class.php');
	//create new instance of class
	//$reseller = new Reseller();
	
	//$id = $_GET['id'];
	
	if($_SESSION['reseller']){
		//Left nav
		//nav-bg
		$a = explode('-', $_SESSION['thm_nav-bg']);
		$leftNavColor1 = $a[0];
		$leftNavColor2 = $a[1];
		//nav-bg2
		$a = explode('_', $_SESSION['thm_nav-bg2']);
		$leftNavColor3 = $a[0];
		$leftNavColor3_hex = $a[1];
		
		//Top nav
		//nav2-bg
		$topNavColor1 = $_SESSION['thm_nav2-bg'];
		$topNavColor2 = $_SESSION['thm_nav2-bg2'];
		
		//Inner navs
		$contentNavColor1 = $_SESSION['thm_nav3-bg'];
		$contentNavColor2 = $_SESSION['thm_nav3-bg2'];
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
<?
	$frm = 'navs';
	include('edit-theme-nav.php');
?>
<br style="clear:left;" />
	<h1>Branding - Navigation:</h1>
	<h2>Top Navigation Bar</h2>
	<label>Top Navigation Bar Color</label>
	<select class="edit-theme-select uniformselect" name="nav2-bg">
		<option value="black" <? if($topNavColor1=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="brown" <? if($topNavColor1=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="maroon" <? if($topNavColor1=="maroon") echo 'selected="yes"'; ?>>Maroon</option>
		<option value="navy" <? if($topNavColor1=="navy") echo 'selected="yes"'; ?>>Navy</option>
		<option value="olive" <? if($topNavColor1=="olive") echo 'selected="yes"'; ?>>Olive</option>
		<option value="plum" <? if($topNavColor1=="plum") echo 'selected="yes"'; ?>>Plum</option>
		<option value="rust" <? if($topNavColor1=="rust") echo 'selected="yes"'; ?>>Rust</option>
		<option value="white" <? if($topNavColor1=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<label>Top Navigation Active/Highlight Color</label>
	<select class="edit-theme-select uniformselect" name="nav2-bg2">
		<option value="black" <? if($topNavColor2=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($topNavColor2=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($topNavColor2=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($topNavColor2=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($topNavColor2=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($topNavColor2=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($topNavColor2=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($topNavColor2=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form">
	<label>Top Navigation Links Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav2-links" value="<?=$_SESSION['thm_nav2-links']?>" />
	<br />
	<label>Top Navigation Links Color - Active/Hover</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav2-links2" value="<?=$_SESSION['thm_nav2-links2']?>" />
	<br />
	</form><br />
	<br /><br />
	<h2>Left Navigation Bar</h2>
	<label>Left Navigation Tab Color <span style="color:#666; font-size:75%">(page will automatically refresh to apply this change)</span></label>
	<select class="edit-theme-select uniformselect" name="nav-bg">
		<option value="<? echo $a = 'black-'.$leftNavColor2; ?>" <? if($leftNavColor1=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="<? echo $a = 'blue-'.$leftNavColor2; ?>" <? if($leftNavColor1=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="<? echo $a = 'brown-'.$leftNavColor2; ?>" <? if($leftNavColor1=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="<? echo $a = 'green-'.$leftNavColor2; ?>" <? if($leftNavColor1=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="<? echo $a = 'purple-'.$leftNavColor2; ?>" <? if($leftNavColor1=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="<? echo $a = 'red-'.$leftNavColor2; ?>" <? if($leftNavColor1=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="<? echo $a = 'tan-'.$leftNavColor2; ?>" <? if($leftNavColor1=="tan") echo 'selected="yes"'; ?>>Tan</option>
		<option value="<? echo $a = 'white-'.$leftNavColor2; ?>" <? if($leftNavColor1=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<label>Left Navigation Active Tab Color <span style="color:#666; font-size:75%">(page will automatically refresh to apply this change)</span></label>
	<select class="edit-theme-select uniformselect" name="nav-bg">
		<option value="<? echo $a = $leftNavColor1.'-black'; ?>" <? if($leftNavColor2=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="<? echo $a = $leftNavColor1.'-blue'; ?>" <? if($leftNavColor2=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="<? echo $a = $leftNavColor1.'-green'; ?>" <? if($leftNavColor2=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="<? echo $a = $leftNavColor1.'-orange'; ?>" <? if($leftNavColor2=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="<? echo $a = $leftNavColor1.'-purple'; ?>" <? if($leftNavColor2=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="<? echo $a = $leftNavColor1.'-red'; ?>" <? if($leftNavColor2=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="<? echo $a = $leftNavColor1.'-white'; ?>" <? if($leftNavColor2=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<label>Left Navigation Slide-down Color</label>
	<select class="edit-theme-select uniformselect" name="nav-bg2">
		<option value="black_434343" <? if($_SESSION['thm_nav-bg2']=="black_434343") echo 'selected="yes"'; ?>>Black</option>
		<option value="brown_5c4d43" <? if($_SESSION['thm_nav-bg2']=="brown_5c4d43") echo 'selected="yes"'; ?>>Brown</option>
		<option value="grey_919191" <? if($_SESSION['thm_nav-bg2']=="grey_919191") echo 'selected="yes"'; ?>>Grey</option>
		<option value="light-green_b9ffb9" <? if($_SESSION['thm_nav-bg2']=="light-green_b9ffb9") echo 'selected="yes"'; ?>>Light Green</option>
		<option value="maroon_561717" <? if($_SESSION['thm_nav-bg2']=="maroon_561717") echo 'selected="yes"'; ?>>Maroon</option>
		<option value="navy_1e3256" <? if($_SESSION['thm_nav-bg2']=="navy_1e3256") echo 'selected="yes"'; ?>>Navy</option>
		<option value="olive_3f5c3d" <? if($_SESSION['thm_nav-bg2']=="olive_3f5c3d") echo 'selected="yes"'; ?>>Olive</option>
		<option value="salmon_ffc6c6" <? if($_SESSION['thm_nav-bg2']=="salmon_ffc6c6") echo 'selected="yes"'; ?>>Salmon</option>
		<option value="sky_bed2f5" <? if($_SESSION['thm_nav-bg2']=="sky_bed2f5") echo 'selected="yes"'; ?>>Sky</option>
		<option value="tan_dccdb9" <? if($_SESSION['thm_nav-bg2']=="tan_dccdb9") echo 'selected="yes"'; ?>>Tan</option>
		<option value="white_f4f4f4" <? if($_SESSION['thm_nav-bg2']=="white_f4f4f4") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form">
	<label>Left Navigation Inactive Tab Link Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav-links3" value="<?=$_SESSION['thm_nav-links3']?>" />
	<br />
	<label>Left Navigation Inactive Tab Link Color - Shadow <span style="color:#666; font-size:75%">(leave blank for none)</span></label>
	#<input type="text" class="edit-theme-text no-upload" name="nav-links4" value="<?=$_SESSION['thm_nav-links4']?>" />
	<br />
	<label>Left Navigation Active Tab Link Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav-links" value="<?=$_SESSION['thm_nav-links']?>" />
	<br />
	<label>Left Navigation Slide-down Links Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav-links2" value="<?=$_SESSION['thm_nav-links2']?>" />
	<br />
	</form><br />
	<br /><br />
	<h2>Content Navigation Bar</h2>
	<label>Content Navigation Bar Color</label>
	<select class="edit-theme-select uniformselect" name="nav3-bg">
		<option value="black" <? if($contentNavColor1=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="brown" <? if($contentNavColor1=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="maroon" <? if($contentNavColor1=="maroon") echo 'selected="yes"'; ?>>Maroon</option>
		<option value="navy" <? if($contentNavColor1=="navy") echo 'selected="yes"'; ?>>Navy</option>
		<option value="olive" <? if($contentNavColor1=="olive") echo 'selected="yes"'; ?>>Olive</option>
		<option value="plum" <? if($contentNavColor1=="plum") echo 'selected="yes"'; ?>>Plum</option>
		<option value="rust" <? if($contentNavColor1=="rust") echo 'selected="yes"'; ?>>Rust</option>
		<option value="white" <? if($contentNavColor1=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<label>Content Navigation Active/Highlight Color</label>
	<select class="edit-theme-select uniformselect" name="nav3-bg2">
		<option value="black" <? if($contentNavColor2=="black") echo 'selected="yes"'; ?>>Black</option>
		<option value="blue" <? if($contentNavColor2=="blue") echo 'selected="yes"'; ?>>Blue</option>
		<option value="brown" <? if($contentNavColor2=="brown") echo 'selected="yes"'; ?>>Brown</option>
		<option value="green" <? if($contentNavColor2=="green") echo 'selected="yes"'; ?>>Green</option>
		<option value="orange" <? if($contentNavColor2=="orange") echo 'selected="yes"'; ?>>Orange</option>
		<option value="purple" <? if($contentNavColor2=="purple") echo 'selected="yes"'; ?>>Purple</option>
		<option value="red" <? if($contentNavColor2=="red") echo 'selected="yes"'; ?>>Red</option>
		<option value="white" <? if($contentNavColor2=="white") echo 'selected="yes"'; ?>>White</option>
	</select>
	<form class="jquery_form">
	<label>Content Navigation Links Color</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav3-links" value="<?=$_SESSION['thm_nav3-links']?>" />
	<br />
	<label>Content Navigation Links Color - Active/Hover</label>
	#<input type="text" class="edit-theme-text no-upload" name="nav3-links2" value="<?=$_SESSION['thm_nav3-links2']?>" />
	<br />
	</form><br />
	<br /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Not authorized."; ?>