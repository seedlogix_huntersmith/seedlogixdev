<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, user_id_created, hub_parent_id, httphostkey, name, company_name, city, twitter, facebook, facebook_plugin, facebook_id, myspace, youtube, diigo, technorati, linkedin, google_local, yahoo, blog, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields FROM hub WHERE id = '".$_SESSION['current_id']."'";
	$row = $hub->queryFetch($query);
	$query = "SELECT user_class2_fields, pages FROM themes WHERE id = '".$row['theme']."'";
	$themeInfo = $hub->queryFetch($query);
	
	if($row['user_id']==$_SESSION['user_id']){
		//get field info from hub row for locks
		if($row['hub_parent_id'] || $row['multi_user']){
			if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
			else {
				//if hub is a V2 MU Hub copy get parent hub info
				$query = "SELECT multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentHubInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentHubInfo['multi_user_fields'];
			}
		}
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if(($row['req_theme_type']==4 && $_SESSION['reseller']) || 
			($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
			$addClass = ' multiUser-'.$muHubID.'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		   ($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
			$multi_user2 = true;
			//figure out which fields to lock
			if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
			if($fieldsInfo = json_decode($fieldsInfoData, true)){
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
						return 'readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
		
		if(isset($_GET['resetfbid']))
            $blog->query('UPDATE hubs SET facebook_id = "" WHERE user_id = ' . $_SESSION['user_id'] . ' AND id = ' . $_SESSION['current_id']);
        
        

?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "social";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - Social Networking</h2>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
        	<li>
				<label>blog</label>
				<? if($multi_user){ ?>
				<select name="lock_blog" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('blog',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('blog')?> name="blog" value="<?=$row['blog']?>" class="<?=$addClass?>" />
			</li>
   
            <li>
				<label>google plus</label>
				<? if($multi_user){ ?>
				<select name="lock_google_local" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('google_local',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('google_local')?> name="google_local" value="<?=$row['google_local']?>" class="<?=$addClass?>" />
			</li>
	
			<li>
				<label>twitter <? if($reseller_site) echo 'username'; ?> <? if($row['theme'] == "57" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "84" || $row['theme'] == "85" || $row['theme'] == "86"  || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "98" || $row['theme'] == "102") echo 'username'; ?></label>
				<? if($multi_user){ ?>
				<select name="lock_twitter" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('twitter',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('twitter')?> name="twitter" value="<?=$row['twitter']?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>facebook</label>
				<? if($multi_user){ ?>
				<select name="lock_facebook" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('facebook',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('facebook')?> name="facebook" value="<?=$row['facebook']?>" class="<?=$addClass?>" />
			</li>
			<?php if($row['theme'] == "57" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "88" || $row['theme'] == "98") {?>
			<li>
				<label>Facebook Plugins | <span style="color:#666; font-size:75%;">Recommended Size: 280 w X 320 h</span></label>
				<? if($multi_user){ ?>
				<select name="lock_facebook_plugin" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('facebook_plugin',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				
				<textarea name="facebook_plugin" <?=checkLock('facebook_plugin')?> rows="3" id="edtr1" class="<?=$addClass?>"><?=$row['facebook_plugin']?></textarea>
				<br style="clear:left" />
			</li>
			<?php } ?>
            <li>
				<label>linkedin</label>
				<? if($multi_user){ ?>
				<select name="lock_linkedin" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('linkedin',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('linkedin')?> name="linkedin" value="<?=$row['linkedin']?>" class="<?=$addClass?>" />
			</li>
            
            <?php if($row['theme'] != "57" && $row['theme'] != "78" && $row['theme'] != "79" && $row['theme'] != "80" && $row['theme'] != "81" && $row['theme'] != "82" && $row['theme'] != "83" && $row['theme'] != "84" && $row['theme'] != "85" && $row['theme'] != "86" && $row['theme'] != "87" && $row['theme'] != "88" && $row['theme'] != "102") {?>
			<li>
				<label>myspace</label>
				<? if($multi_user){ ?>
				<select name="lock_myspace" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('myspace',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('myspace')?> name="myspace" value="<?=$row['myspace']?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
            <?php if($row['theme'] != "57" && $row['theme'] != "78" && $row['theme'] != "79" && $row['theme'] != "83" && $row['theme'] != "85" && $row['theme'] != "86" && $row['theme'] != "87" && $row['theme'] != "88" && $row['theme'] != "102") {?>
			<li>
				<label>youtube</label>
				<? if($multi_user){ ?>
				<select name="lock_youtube" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('youtube',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('youtube')?> name="youtube" value="<?=$row['youtube']?>" class="<?=$addClass?>" />
			</li>
			
			
            <?php } ?>
            
             <li><label>Facebook Publishing</label>
                            <?php if($row['facebook_id'] == ''): ?>
                            <a href="http://hybrid.6qube.com/api/facebook-website-app.php?sitehttphost=<?php
                                    echo urlencode($row['httphostkey']); ?>" id="facebook-approve" class="external" target="_blank">Authorize</a>
                            <?php else: 
							$fbpost_info    =   explode("\n", $row['facebook_id']);
							?>
                                    ID: <a target="_blank" class="external" href="https://facebook.com/<?php
                                    echo $fbpost_info[0]; ?>"><?php echo htmlentities($fbpost_info[1]); ?></a> 
									<?php
                            endif; ?>                            
                        </li>

	
		</ul>
	</form>
</div>
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Social Network Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#hubsocialVideo" class="trainingvideo">Social Network Overview</a>
			</li>
			<div style="display:none;">
				<div id="hubsocialVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-social-overview.flv" style="display:block;width:640px;height:480px" id="hubsocialplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubsocialplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="http://www.facebook.com/pages/create.php?campaign_id=372931622610&placement=pghm&extra_1=0" class="external" target="_new">Create a Facebook Business Page</a>
			</li>
			<li>
				<a href="https://twitter.com/signup" class="external" target="_new">Create a Twitter Account</a>
			</li>
		</ul>
		  <? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->
<br /><br style="clear:both;" />
<? } else echo "Error displaying page."; ?>