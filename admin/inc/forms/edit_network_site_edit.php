<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$uid = $_SESSION['login_uid'];
	$type = $_GET['type'];
	$id = $_GET['id'];
	
	$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
	$row = $local->queryFetch($query);
	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<script type="text/javascript">
<!--
// this tells jquery to run the function below once the DOM is read
$(document).ready(function() {

// choose text for the show/hide link
var showText="Show";
var hideText="Hide";

// append show/hide links to the element directly preceding the element with a class of "toggle"
$(".toggle").prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

// hide all of the elements with a class of 'toggle'
$('.toggle').hide();

// capture clicks on the toggle links
$('a.toggleLink').click(function() {

// change the link depending on whether the element is shown or hidden
if ($(this).html()==showText) {
$(this).html(hideText);
}
else {
$(this).html(showText);
}

// toggle the display
$(this).parent().next('.toggle').toggle('slow');

// return false so any link destination is not followed
return false;

});
});

//-->
</script>
<?php
$thisPage = "edit";
include('edit_network_site_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h1>Edit Regions - <?=$row['name']?></h1>
	
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
        
        	
        	<label><?=$themeInfo['region1_title']?></label>
			<li class="toggle">
					
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_1"  rows="20" cols="60" id="edtr1" ><?=htmlentities($row['edit_region_1'])?></textarea>
				</li>
                <label><?=$themeInfo['region2_title']?></label>
				<li class="toggle">
					
					
					<a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_2"  rows="20" cols="60" id="edtr2" ><?=htmlentities($row['edit_region_2'])?></textarea>
				</li>
                <label><?=$themeInfo['region3_title']?></label>
				<li class="toggle">
					
					
					<a href="#" id="3" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_3"  rows="20" cols="60" id="edtr3" ><?=htmlentities($row['edit_region_3'])?></textarea>
				</li>
                <label><?=$themeInfo['region4_title']?></label>
				<li class="toggle">
					
					
					<a href="#" id="4" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_4"  rows="20" cols="60" id="edtr4" ><?=htmlentities($row['edit_region_4'])?></textarea>
				</li>
                <label><?=$themeInfo['region5_title']?></label>
				<li class="toggle">
					
					
					<a href="#" id="5" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_5"  rows="20" cols="60" id="edtr5" ><?=htmlentities($row['edit_region_5'])?></textarea>
				</li>
                <label><?=$themeInfo['region6_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="6" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_6"  rows="20" cols="60" id="edtr6" ><?=htmlentities($row['edit_region_6'])?></textarea>
				</li>
            
            <label><?=$themeInfo['region7_title']?></label>
			<li class="toggle">
					
					
					<a href="#" id="7" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_7"  rows="20" cols="60" id="edtr7" ><?=htmlentities($row['edit_region_7'])?></textarea>
				</li>
            
       
            <label><?=$themeInfo['region8_title']?></label>
			<li class="toggle">
					
					
					<a href="#" id="8" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_8"  rows="20" cols="60" id="edtr8" ><?=htmlentities($row['edit_region_8'])?></textarea>
				</li>
                
                 <label><?=$themeInfo['region9_title']?></label>
			<li class="toggle">
					
					
					<a href="#" id="9" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_9"  rows="20" cols="60" id="edtr9" ><?=htmlentities($row['edit_region_9'])?></textarea>
				</li>
                <label><?=$themeInfo['region10_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="10" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_10"  rows="20" cols="60" id="edtr10" ><?=htmlentities($row['edit_region_10'])?></textarea>
				</li>
                
                 <label><?=$themeInfo['region11_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="11" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_11"  rows="20" cols="60" id="edtr11" ><?=htmlentities($row['edit_region_11'])?></textarea>
				</li>
                
                <label><?=$themeInfo['region12_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="12" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_12"  rows="20" cols="60" id="edtr12" ><?=htmlentities($row['edit_region_12'])?></textarea>
				</li>
                
                 <label><?=$themeInfo['region13_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="13" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_13"  rows="20" cols="60" id="edtr13" ><?=htmlentities($row['edit_region_13'])?></textarea>
				</li>
                
                 <label><?=$themeInfo['region14_title']?></label>
                <li class="toggle">
					
					
					<a href="#" id="14" class="tinymce"><span>Rich Text</span></a>
					<textarea  name="edit_region_14"  rows="20" cols="60" id="edtr14" ><?=htmlentities($row['edit_region_14'])?></textarea>
				</li>
                
               
                
                
            
         
		</ul>
	</form>
	
	<br />
	
</div>
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Unable to display this page."; ?>