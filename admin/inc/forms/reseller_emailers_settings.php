<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	
	//create new instance of class
	$reseller = new Reseller();
	
	$id = $_GET['id'];
	
	if($emailer = $reseller->getResellerEmailers($_SESSION['login_uid'], $id))
		$row = $emailer[$id];
		
	$addOption = '<option value="-1"';
	if($row['sendto_class']==-1) $addOption .= ' selected="yes"';
	$addOption .= '>All Product Groups</option>';
	$addOption .= '<option value="-2"';
	if($row['sendto_class']==-2) $addOption .= ' selected="yes"';
	$addOption .= '>All Non-Free</option>';
	$addOption .= '<option value="-3"';
	if($row['sendto_class']==-3) $addOption .= ' selected="yes"';
	$addOption .= '>Free Users Who Have Never Logged In</option>';
	if($_SESSION['admin'] && $row['admin']){
		$addOption .= '<option value="-4"';
		if($row['sendto_class']==-4) $addOption .= ' selected="yes"';
		$addOption .= '>All Free 6Qube Users</option>';
	}
	
	$createdDisp = date('M jS', strtotime($row['created']));
	$createdDisp .= ' at ';
	$createdDisp .= date('g:ia', strtotime($row['created']));
	$createdDisp .= ' CST';
	
	if($row['admin_user']==$_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
    
    <!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/reseller_emailers_settings.php?id=<?=$id?>" class="current">Settings</a></li>
		<li><a href="inc/forms/reseller_emailers_email.php?id=<?=$id?>">Email</a></li>
		<li class="last"><a href="reseller-emailers.php">Back</a></li>
	</ul>
</div>
<br style="clear:left;" />
<h1>Emailer Settings</h1>
<div style="color:#333;"><strong>Tip:</strong> To send this email right now set the schedule to "0 Hours From Now" and Active to "Yes".<br />On the Email tab page you will have an option to send the email immediately.  Please note Retro Emails go out every 30 minutes.</div>
<form name="resellers_emailers" class="jquery_form" id="<?=$row['id']?>">
	<ul>
		<li><label>Name:</label><input type="text" name="name" value="<?=$row['name']?>" /></li><br />
		<li>
			<label>Schedule:</label>
			<input type="text" name="sched" value="<?=$row['sched']?>" style="width:50px;" />
			<select name="sched_mode" title="select" class="uniformselect">
				<option value="hours" <? if($row['sched_mode']=="hours") echo 'selected="yes"'; ?>>Hours</option>
				<option value="days" <? if($row['sched_mode']=="days") echo 'selected="yes"'; ?>>Days</option>
				<option value="weeks" <? if($row['sched_mode']=="weeks") echo 'selected="yes"'; ?>>Weeks</option>
				<option value="months" <? if($row['sched_mode']=="months") echo 'selected="yes"'; ?>>Months</option>
			</select>
			<div style="margin-top:-15px;"></div>
			<select name="sched_mode2" title="select" class="uniformselect">
				<option value="signup" <? if($row['sched_mode2']=='signup') echo 'selected="yes"'; ?>>From when user signs up</option>
				<option value="now" <? if($row['sched_mode2']=='now') echo 'selected="yes"'; ?>>From now</option>
			</select>
			<div style="font-size:75%;color:#666;margin-top:-10px;">"From Now" is determined by when this emailer was created (<?=$createdDisp?>)</div><br />
		</li>
		<li>
			<label>Emailer Active?</label>
			<select name="active" title="select" class="uniformselect">
				<option value="1" <? if($row['active']==1) echo 'selected="yes"'; ?>>Yes</option>
				<option value="0" <? if($row['active']==0) echo 'selected="yes"'; ?>>No</option>
			</select>
		</li>
		<li>
			<label>Send To User Type:</label>
			<select name="sendto_type" title="select" class="uniformselect">
				<option value="all" <? if($row['sendto_type']=='all') echo 'selected="yes"'; ?>>All</option>
				<option value="active" <? if($row['sendto_type']=='active') echo 'selected="yes"'; ?>>Active</option>
				<option value="suspended" <? if($row['sendto_type']=='suspended') echo 'selected="yes"'; ?>>Suspended</option>
				<option value="active_suspended" <? if($row['sendto_type']=='active_suspended') echo 'selected="yes"'; ?>>Active + Suspended</option>
				<option value="canceled" <? if($row['sendto_type']=='canceled') echo 'selected="yes"'; ?>>Canceled</option>
			</select>
		</li>
		<li>
			<label>Send To Product Group:</label>
			<?
			if(!$row['admin']) $rid = $_SESSION['login_uid'];
			else $rid = 0;
			echo $reseller->displayResellerProducts($rid, 'dropdown', $row['sendto_class'], NULL, NULL, 'sendto_class', $addOption);
			?>
		</li>
		<li>
			<label>Communication Type:</label>
			<select name="emailer_type" title="select" class="uniformselect">
				<option value="marketing" <? if($row['emailer_type']=='marketing') echo 'selected="yes"'; ?>>Marketing/Promotional</option>
				<option value="info" <? if($row['emailer_type']=='info') echo 'selected="yes"'; ?>>Info/Important Communication</option>
			</select>
		</li>
		<li>
			<label>Retroactive?</label>
			<span style="font-size:75%;color:#666;"><strong>This only affects "From when user signs up"-type emailers.</strong><br />If set to "Yes" this email will be sent to all existing users who meet the sending criteria as well as future signups.</span>
			<select name="retro" title="select" class="uniformselect">
				<option value="1" <? if($row['retro']==1) echo 'selected="yes"'; ?>>Yes</option>
				<option value="0" <? if($row['retro']==0) echo 'selected="yes"'; ?>>No</option>
			</select>
		</li>
	</ul>
</form><br />
<? } else echo "Not authorized."; ?>