<?php
	session_start();
#         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, user_id_created, hub_parent_id, name, company_name, city, 
			theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields  
			FROM hub WHERE id = '".$_SESSION['current_id']."' ";

#			pic_one_title, pic_one_desc, pic_one, pic_two_title, pic_two_desc, pic_two, pic_three_title, pic_three_desc, pic_three, pic_four_title, pic_four_desc, pic_four, pic_five_title, pic_five, pic_five_desc, pic_six_title, pic_six, pic_six_desc, pic_one_link, pic_two_link, pic_three_link, pic_four_link, pic_five_link, pic_six_link, pic_seven_title, pic_seven, pic_seven_desc, pic_seven_link, pic_eight_title, pic_eight, pic_eight_desc, pic_eight_link, pic_nine_title, pic_nine, pic_nine_desc, pic_nine_link, pic_ten_title, pic_ten, pic_ten_desc, pic_ten_link, 

	$row = $hub->queryFetch($query);
	$da	=	new HubDataAccess();
	$da->getPics($row['id'], $row);
	if($row['user_id']==$_SESSION['user_id']){
		//get field info from hub row for locks
		if($row['hub_parent_id'] || $row['multi_user']){
			if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
			else {
				//if hub is a V2 MU Hub copy get parent hub info
				$query = "SELECT multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
						  AND user_id = '".$_SESSION['parent_id']."'";
				$parentHubInfo = $hub->queryFetch($query, NULL, 1);
				$fieldsInfoData = $parentHubInfo['multi_user_fields'];
			}
		}
		//get current theme info
		$query = "SELECT user_class2_fields, pages, photo_size, full_photo_options FROM themes WHERE id = '".$row['theme']."'";
		$themeInfo = $hub->queryFetch($query);
		
		$fullphotoOptions = $themeInfo['full_photo_options']==1;
		
		$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub/';
			
		if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
		else $dflt_noImg = 'img/no-photo.jpg';
		
		$lockedFields = array('');
		//if user is a reseller editing a multi-user hub
		if(($row['req_theme_type']==4 && $_SESSION['reseller']) || 
			($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
			//multi-user hub
			$_SESSION['multi_user_hub'] = $multi_user = 1;
			$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
			$addClass = ' multiUser-'.$muHubID.'n';
		}
		else $_SESSION['multi_user_hub'] = $multi_user = NULL;
		//if user is a reseller or client editing a multi-user hub
		if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		   ($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
			$multi_user2 = true;
			//figure out which fields to lock
			if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
			if($fieldsInfo = json_decode($fieldsInfoData, true)){
				foreach($fieldsInfo as $key=>$val){
					if($fieldsInfo[$key]['lock']==1)
						$lockedFields[] = $key;
				}
			}
		}
		
		function checkLock($field, $admin = ''){
			global $lockedFields, $multi_user2;
			//bypass for non-multi-user hubs
			if($multi_user2){
				//otherwise check if field is in lockedFields array
				if(in_array($field, $lockedFields)){
					if($admin){
						return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
					}
					else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
						return 'readonly'; //else if so and user is a client, lock field
					}
				}
			}
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<script type="text/javascript">
<!--
// this tells jquery to run the function below once the DOM is read
$(document).ready(function() {

// choose text for the show/hide link
var showText="Show";
var hideText="Hide";

// append show/hide links to the element directly preceding the element with a class of "toggle"
$(".toggle").prev().append(' (<a href="#" class="toggleLink">'+showText+'</a>)');

// hide all of the elements with a class of 'toggle'
$('.toggle').hide();

// capture clicks on the toggle links
$('a.toggleLink').click(function() {

// change the link depending on whether the element is shown or hidden
if ($(this).html()==showText) {
$(this).html(hideText);
}
else {
$(this).html(showText);
}

// toggle the display
$(this).parent().next('.toggle').toggle('slow');

// return false so any link destination is not followed
return false;

});
});

//-->
</script>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "photos";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include(QUBEADMIN . 'inc/forms/add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - Gallery</h2>
	<!-- photo one -->
    <label>Slider Photo #1</label>
    <div class="toggle">
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
    
		<ul>
			<li>
				<label>Photo Title #1</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_one_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_one_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_one_title')?> name="pic_one_title" value="<?=$row['pic_one_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "60" || $row['theme'] == "62" || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #1</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_one_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_one_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_one_link')?> name="pic_one_link" value="<?=$row['pic_one_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "60" || $row['theme'] == "62" || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #1</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_one_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_one_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_one_desc')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_one_desc" <?=checkLock('pic_one_desc')?> rows="6" id="edtr1" class="<?=$addClass?>" ><?=$row['pic_one_desc']?></textarea>
				<br style="clear:left" />
			</li>
			
			<? } ?>
		</ul>
	</form>
	<form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #1 | <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_one" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_one',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_one')){ ?>
		 <? if($row['pic_one']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_one').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_one" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_one']) echo $img_url.$row['pic_one'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
		</div>
        <label>Slider Photo #2</label>
    <div class="toggle">
	<!-- photo two -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #2</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_two_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_two_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_two_title')?> name="pic_two_title" value="<?=$row['pic_two_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62"  || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #2</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_two_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_two_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_two_link')?> name="pic_two_link" value="<?=$row['pic_two_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62"  || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #2</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_two_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_two_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_two_desc')){ ?><a href="#" id="2" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_two_desc" <?=checkLock('pic_two_desc')?> rows="6" id="edtr2" class="<?=$addClass?>" ><?=$row['pic_two_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>      
	</form>
	<form id="img_two" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #2 | <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_two" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_two',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_two')){ ?>
		<? if($row['pic_two']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_two').trigger('blur');">Clear Image</a>
		 <? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_two" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_two']) echo $img_url.$row['pic_two'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    </div>
    <label>Slider Photo #3</label>
    <div class="toggle">
	<!-- photo three -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #3</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_three_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_three_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_three_title')?> name="pic_three_title" value="<?=$row['pic_three_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62"  || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #3</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_three_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_three_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_three_link')?> name="pic_three_link" value="<?=$row['pic_three_link']?>" class="<?=$addClass?>" />
			</li>
            	<? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "43" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "76" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #3</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_three_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_three_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_three_desc')){ ?><a href="#" id="3" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_three_desc" <?=checkLock('pic_three_desc')?> rows="6" id="edtr3" class="<?=$addClass?>" ><?=$row['pic_three_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_three" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #3 |  <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_three" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_three',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_three')){ ?>
		<? if($row['pic_three']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_three').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_three" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<?  if($row['pic_three']) echo $img_url.$row['pic_three'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    </div>
    <label>Slider Photo #4</label>
    <div class="toggle">
	<!-- photo four -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #4</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_four_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_four_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_four_title')?> name="pic_four_title" value="<?=$row['pic_four_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #4</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_four_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_four_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_four_link')?> name="pic_four_link" value="<?=$row['pic_four_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #4</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_four_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_four_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_four_desc')){ ?><a href="#" id="4" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_four_desc" <?=checkLock('pic_four_desc')?> rows="6" id="edtr4" class="<?=$addClass?>" ><?=$row['pic_four_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_four" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #4 |  <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_four" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_four',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_four')){ ?>
		<? if($row['pic_four']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_four').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_four" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		 <? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_four']) echo $img_url.$row['pic_four'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    </div>
    <label>Slider Photo #5</label>
    <div class="toggle">
	<!-- photo five -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #5</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_five_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_five_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_five_title')?> name="pic_five_title" value="<?=$row['pic_five_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83" || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #5</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_five_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_five_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_five_link')?> name="pic_five_link" value="<?=$row['pic_five_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "54" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #5</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_five_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_five_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_five_desc')){ ?><a href="#" id="5" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_five_desc" <?=checkLock('pic_five_desc')?> rows="6" id="edtr5" class="<?=$addClass?>" ><?=$row['pic_five_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_five" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #5 |   <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_five" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_five',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_five')){ ?>
		<? if($row['pic_five']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_five').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_five" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<?  if($row['pic_five']) echo $img_url.$row['pic_five'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   </div>
    <label>Slider Photo #6</label>
    <div class="toggle">
	<!-- photo six -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #6</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_six_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_six_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_six_title')?> name="pic_six_title" value="<?=$row['pic_six_title']?>" class="<?=$addClass?>" />
			</li>
			<? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #6</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_six_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_six_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_six_link')?> name="pic_six_link" value="<?=$row['pic_six_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #6</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_six_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_six_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_six_desc')){ ?><a href="#" id="6" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_six_desc" <?=checkLock('pic_six_desc')?> rows="6" id="edtr6" class="<?=$addClass?>" ><?=$row['pic_six_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_six" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #6 |   <? if($themeInfo['photo_size']){ ?>| <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['photo_size']?></span><? } ?></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_six" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_six',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_six')){ ?>
		<? if($row['pic_six']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_six').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_six" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_six']) echo $img_url.$row['pic_six'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    </div>
    <label>Slider Photo #7</label>
    <div class="toggle">
	<!-- photo seven -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #7</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_seven_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_seven_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_seven_title')?> name="pic_seven_title" value="<?=$row['pic_seven_title']?>" class="<?=$addClass?>" />
			</li>
            <? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #7</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_seven_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_seven_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_seven_link')?> name="pic_seven_link" value="<?=$row['pic_seven_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #7</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_seven_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_seven_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_seven_desc')){ ?><a href="#" id="7" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_seven_desc" <?=checkLock('pic_seven_desc')?> rows="6" id="edtr7" class="<?=$addClass?>" ><?=$row['pic_seven_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_seven" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #7 | <a href="http://www.picnik.com/" class="external dflt-link" target="_new">Crop Gallery Image</a></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_seven" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_seven',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_seven')){ ?>
		<? if($row['pic_seven']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_seven').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_seven" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		 <? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_seven']) echo $img_url.$row['pic_seven'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   </div>
    <label>Slider Photo #8</label>
    <div class="toggle">
	<!-- photo eight -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #8</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_eight_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_eight_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_eight_title')?> name="pic_eight_title" value="<?=$row['pic_eight_title']?>" class="<?=$addClass?>" />
			</li>
            <? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "83" || $row['theme'] == "82"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #8</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_eight_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_eight_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_eight_link')?> name="pic_eight_link" value="<?=$row['pic_eight_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "83" || $row['theme'] == "82"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #8</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_eight_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_eight_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_eight_desc')){ ?><a href="#" id="8" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_eight_desc" <?=checkLock('pic_eight_desc')?> rows="6" id="edtr8" class="<?=$addClass?>" ><?=$row['pic_eight_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_eight" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #8 | <a href="http://www.picnik.com/" class="external dflt-link" target="_new">Crop Gallery Image</a></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_eight" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_eight',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_eight')){ ?>
		<? if($row['pic_eight']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_eight').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_eight" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_eight']) echo $img_url.$row['pic_eight'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   </div>
    <label>Slider Photo #9</label>
    <div class="toggle">
	<!-- photo nine -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #9</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_nine_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_nine_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_nine_title')?> name="pic_nine_title" value="<?=$row['pic_nine_title']?>" class="<?=$addClass?>" />
			</li>
            <? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #9</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_nine_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_nine_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_nine_link')?> name="pic_nine_link" value="<?=$row['pic_nine_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #9</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_nine_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_nine_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_nine_desc')){ ?><a href="#" id="9" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_nine_desc" <?=checkLock('pic_nine_desc')?> rows="6" id="edtr9" class="<?=$addClass?>" ><?=$row['pic_nine_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
            
		</ul>
	</form>
	<form id="img_nine" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #9 | <a href="http://www.picnik.com/" class="external dflt-link" target="_new">Crop Gallery Image</a></label>
		<? if($multi_user){ ?>
		<select name="lock_pic_nine" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_nine',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_nine')){ ?>
		<? if($row['pic_nine']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_nine').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_nine" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_nine']) echo $img_url.$row['pic_nine'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
   </div>
    <label>Slider Photo #10</label>
    <div class="toggle">
	<!-- photo ten -->
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Photo Title #10</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_ten_title" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_ten_title',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_ten_title')?> name="pic_ten_title" value="<?=$row['pic_ten_title']?>" class="<?=$addClass?>" />
			</li>
            <? if($row['theme'] == "5" || $row['theme'] == "8" || $row['theme'] == "22" || $row['theme'] == "24" || $row['theme'] == "32" || $row['theme'] == "33" || $row['theme'] == "36" || $row['theme'] == "37" || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "95" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "104" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Link #10</label>
				<? if($multi_user){ ?>
				<select name="lock_pic_ten_link" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('pic_ten_link',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('pic_ten_link')?> name="pic_ten_link" value="<?=$row['pic_ten_link']?>" class="<?=$addClass?>" />
			</li>
            <? } ?>
            <? if($row['theme'] == "22" || $row['theme'] == "32" || $row['theme'] == "33"  || $row['theme'] == "57" || $row['theme'] == "62" || $row['theme'] == "78" || $row['theme'] == "79" || $row['theme'] == "80" || $row['theme'] == "81" || $row['theme'] == "82" || $row['theme'] == "83"  || $row['theme'] == "85" || $row['theme'] == "86" || $row['theme'] == "87" || $row['theme'] == "88" || $row['theme'] == "89" || $row['theme'] == "98" || $row['theme'] == "101" || $row['theme'] == "102" || $row['theme'] == "105" || $row['theme'] == "106" || $row['theme'] == "107" || $row['theme'] == "110" || $row['theme'] == "111" || $row['theme'] == "116" || $row['theme'] == "117" || $row['theme'] == "119" || $row['theme'] == "120" || $row['theme'] == "122" || $row['theme'] == "126" || $fullphotoOptions) {?>
			<li>
				<label>Photo Description #10</label>
				<? if($multi_user){ ?>
					<select name="lock_pic_ten_desc" title="select" class="ajax-select uniformselect<?=$addClass?>">
						<option value="0">Unlocked</option>
						<option value="1" <?=checkLock('pic_ten_desc',1)?>>Locked</option>
					</select><div style="margin-top:-10px;"></div>
					<? } ?>
				<? if(!checkLock('pic_ten_desc')){ ?><a href="#" id="10" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="pic_ten_desc" <?=checkLock('pic_ten_desc')?> rows="6" id="edtr10" class="<?=$addClass?>" ><?=$row['pic_ten_desc']?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
		</ul>
	</form>
	<form id="img_ten" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Photo #10 | </label>
		<? if($multi_user){ ?>
		<select name="lock_pic_ten" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('pic_ten',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('pic_ten')){ ?>
		<? if($row['pic_ten']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=gallery&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_pic_ten').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="pic_ten" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img class="img_cnt" src="<? if($row['pic_ten']) echo $img_url.$row['pic_ten'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	</div>
	<form class="jquery_form" name="hub" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li style="display:none;">
				<input type="text" name="pic_one" value="" id="clear_pic_one" />
				<input type="text" name="pic_two" value="" id="clear_pic_two" />
				<input type="text" name="pic_three" value="" id="clear_pic_three" />
				<input type="text" name="pic_four" value="" id="clear_pic_four" />
				<input type="text" name="pic_five" value="" id="clear_pic_five" />
				<input type="text" name="pic_six" value="" id="clear_pic_six" />
				<input type="text" name="pic_seven" value="" id="clear_pic_seven" />
				<input type="text" name="pic_eight" value="" id="clear_pic_eight" />
				<input type="text" name="pic_nine" value="" id="clear_pic_nine" />
				<input type="text" name="pic_ten" value="" id="clear_pic_ten" />
			</li>
		</ul>
	</form>
</div>
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>

	<div class="app_right_links">

		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div> <!--End APP Right Links-->
	<? } ?>
	
	<div class="app_right_links">

		<? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div> <!--End APP Right Links-->
	
	<?	
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>								
</div>
<!--End APP Right Panel-->
<br /><br style="clear:both;" />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>
