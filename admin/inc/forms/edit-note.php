<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/contacts.class.php');
	$notes = new Contacts();
	
	if($noteid){
	//get notes
	$row = $notes->getNotes($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID, $noteid);
	}
	
	if($row['user_id']==$_SESSION['user_id']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<div id="form_test">

<a href="inc/forms/view-contact.php?mode=noteview&noteid=<?=$noteid?>&contactID=<?=$contactID?>"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Contacts" /></a>

	<h2><?=$row['subject']?></h2>
    
	<form class="jquery_form" name="notes" id="<?=$noteid?>">
		<ul>
        	<li>
                <label>Subject</label>
            	<input type="text" name="subject" value="<?=$row['subject']?>" />
            </li>
            <li>
				<label>Note Type</label>
				<select name="type" class="ajax-select uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="Email" <? if($row['type'] == "Email") echo 'selected="yes"'; ?>>Email</option>
                <option value="Voicemail" <? if($row['type'] == "Voicemail") echo 'selected="yes"'; ?>>Voicemail</option>
                <option value="Live Call" <? if($row['type'] == "Live Call") echo 'selected="yes"'; ?>>Live Call</option>
				</select>
			</li>
			<li>
				<label>Note</label>
                <a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="note" rows="20" cols="60" id="edtr1" class="<?=$addClass?>"><?=$row['note']?></textarea>
			</li>
			
            
			
		</ul>
	</form> 
	
	
	<br style="clear:both;" />

</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
		
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<? } else echo "Error displaying page."; ?>