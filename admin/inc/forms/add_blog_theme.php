<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	$query = "SELECT id, user_id, user_id_created, blog_title, connect_hub_id, bg_color, bg_img, links_color, theme, affiliate_info, adv_css, theme_color, remove_contact, posts_v2 FROM blogs WHERE id = '".$_SESSION['current_id']."'";
	$row = $blog->queryFetch($query);
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if($row['user_id']==$_SESSION['user_id']){
		if($row['connect_hub_id']){
			$query = "SELECT name FROM hub WHERE id = ".$row['connect_hub_id'];
			$hub_info = $blog->queryFetch($query);
			$hubName = $hub_info['name'];
		}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
	
});
</script> 
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "theme";
	$postsV2 = $row['posts_v2'] ? 1 : 0;
	include('add_blog_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['blog_title']?> - Theme</h2>
	<form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
		<?php if($row['theme'] == "36"){ ?>
			<li>
				<label>Theme Color</label>
				<select name="theme_color" class="ajax-select uniformselect" title="select">
				<option value="0">Please choose an option</option>
				<option value="grey" <? if($row['theme_color'] == "grey") echo 'selected="yes"'; ?>>Grey</option>
                    <option value="green" <? if($row['theme_color'] == "green") echo 'selected="yes"'; ?>>Green</option>
                    <option value="blue" <? if($row['theme_color'] == "blue") echo 'selected="yes"'; ?>>Blue</option>
                    <option value="bronze" <? if($row['theme_color'] == "bronze") echo 'selected="yes"'; ?>>Bronze</option>
                    <option value="darkred" <? if($row['theme_color'] == "darkred") echo 'selected="yes"'; ?>>Dark Red</option>
                    <option value="lightblue" <? if($row['theme_color'] == "lightblue") echo 'selected="yes"'; ?>>Light Blue</option>
                    <option value="orange" <? if($row['theme_color'] == "orange") echo 'selected="yes"'; ?>>Orange</option>
                    <option value="pink" <? if($row['theme_color'] == "pink") echo 'selected="yes"'; ?>>Pink</option>
				</select>
			</li>
		<?php } ?>
			<li>
				<label>Background Color</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" name="bg_color" value="<?=$row['bg_color']?>" />
			</li>
			<li>
				<label>Links Color</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" name="links_color" value="<?=$row['links_color']?>" />
			</li>
			<?php if($row['theme'] == "36"){ ?>
			<li>
				<label>Footer Text Color</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" name="footer_text_color" value="<?=$row['footer_text_color']?>" />
			</li>
			<li>
				<label>Footer Links Color</label>
				<div style="color:#888;font-size:12px; padding-bottom:5px;">Example: <i>red</i> or <i>#ff0000</i></div>
				<input type="text" name="footer_links_color" value="<?=$row['footer_links_color']?>" />
			</li>
		<?php } ?>
		
			<li>
				<label>Layout Theme</label>
				<? $blog->displayThemes($_SESSION['user_id'], $row['theme'], "blogs", $_SESSION['user_class']); ?>
			</li>
			<?php if($hubName){ ?>
			<li>
				<div style="color:#888;font-size:14px; margin-bottom:15px; margin-top:-10px;">This blog is connected to your <i><?=$hubName?></i> hub.</div>
			</li>
			<? } ?>
			<?php if($row['theme'] == "27") {?>
			<li>
				<label>Affiliate Information</label>
				<input type="text" name="affiliate_info" value="<?=$row['affiliate_info']?>" />
			</li>
			<?php } ?>
		</ul>
	</form>
	
	<form id="bg_img" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Background Image | <a href="http://www.picnik.com/" class="external dflt-link" target="_new" >Crop BG Image</a> | <span style="color:#666; font-size:75%">Recommended Width: 1900</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="blogs" />
		<input type="hidden" name="fname" value="bg_img" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="hidden" name="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['bg_img']) echo 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/blogs/'.$row['bg_img'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
    
	<form name="blogs" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
        	<li>
				<label>Remove Contact Form</label>
				
			
				<select name="remove_contact" title="select" class="uniformselect">
			
                	<option value="0">Please choose an option</option>
					<option value="1" <? if($row['remove_contact'] == "1") echo 'selected="yes"'; ?>>Yes</option>
					<option value="0" <? if($row['remove_contact'] == "0") echo 'selected="yes"'; ?>>No</option>
				</select>
			</li>
			<li>
				<label>Advanced CSS<? if(!$_SESSION['theme']){ ?> | <span style="color:#666; font-size:75%"><a href="#advcssVideo" class="trainingvideo" >Watch Video</a></span><? } ?></label>
				<textarea name="adv_css" rows="6" ><?=$row['adv_css']?></textarea>
				<br style="clear:left" />
			</li>
			<? if(!$_SESSION['theme']){ ?>
			<div style="display: none;">
				<div id="advcssVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/advcss-overview.flv" style="display:block;width:640px;height:480px" id="advcssplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("advcssplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			<? } ?>
		</ul>
	</form>
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>Blog Theme Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#blogthemeVideo" class="trainingvideo">Blog Theme Video</a>
			</li>
			<div style="display:none;">
				<div id="blogthemeVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/blogs-theme-overview.flv" style="display:block;width:640px;height:480px" id="blogthemeplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("blogthemeplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		<div align="center">
			<a class="external" target="_new" href="http://6qube.com/blog-preview.php?uid=<?=$_SESSION['user_id']?>&bid=<?=$row['id']?>"><img src="http://6qube.com/admin/img/preview-button.png" border="0" /></a>
		</div>
	</div><!--End APP Right Links-->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a class="external" target="_new" href="http://6qube.com/admin/inc/forms/gradient_form.php">Gradient Background Creator</a>
			</li>
			<li>
				<a href="http://us.fotolia.com/partner/234432" class="external" target="_new">Stock Background Photos</a>
			</li>
			<li>
				<a href="http://www.cssdrive.com/imagepalette/" class="external" target="_new">Color Palette Generator</a>
			</li>
			<li>
				<a href="http://www.w3schools.com/html/html_colors.asp" class="external" target="_new">Common Web Colors</a>
			</li>
			<li>
				<a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a>
			</li>
		</ul>
		<? if($_SESSION['theme']){ ?>
		<div align="center">
			<a class="helpLinks" target="_blank" href="http://<?=$_SESSION['main_site']?>/blog-preview.php?uid=<?=$row['user_id_created']?>&bid=<?=$row['id']?>"><img src="img/preview-button/<?=$_SESSION['thm_buttons-clr']?>.png" border="0" /></a>
		</div>
		<? } ?>
	</div><!--End APP Right Links-->
	
	<?
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>