<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once('../auth.php');

//initiate the class
require_once('../contacts.class.php');
$contacts = new Contacts();

$name = $contacts->validateText($_POST['name']);
$name_array = explode(' ', $name);
$last = $name_array[count($name_array)-1];
$name_array[count($name_array)-1] = '';
$first = trim(implode(' ', $name_array));

if($name){
		$contactID = $contacts->addNewContact($_SESSION['user_id'], $_SESSION['campaign_id'], $first, $last);
		
		if($contactID){
			$html = 
			'<div class="contactContain"><ul class="prospects" title="'.$contactID.'">
			<li class="name" style="width:25%;"><a href="contacts.php?mode=view&id='.$contactID.'">'.$first.' '.$last.'</a></li>
			<li class="email" style="width:22%;"></li>
			<li class="email" style="width:20%;"></li>
			<li class="phone" style="width:10%;"></li>
			<li class="delete" style="width:5%;float:right;"><a href="#" class="delete rmParent" rel="table=contacts&id='.$contactID.'&undoLink=contacts"><img src="img/v3/x-mark.png" border="0" /></a></li>
			<li class="details" style="width:5%;float:right;"><a href="contacts.php?mode=view&id='.$contactID.'"><img src="img/v3/view-details.png" border="0" /></a></li>
			</ul></div>';
			
			$response['success']['id'] = '1';
			$response['success']['container'] = '#new_contact .newContact';
			$response['success']['message'] = $html;
		}
	}
	echo json_encode($response);
?>