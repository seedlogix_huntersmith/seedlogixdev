<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once('../auth.php');

//initiate the class
require_once('../reseller.class.php');
$reseller = new Reseller();

$name = $reseller->validateText($_POST['name']);
$parent_id = $reseller->validateNumber($_POST['parent_id']);
$name_array = explode(' ', $name);
$last = $name_array[count($name_array)-1];
$name_array[count($name_array)-1] = '';
$first = trim(implode(' ', $name_array));

//check to see if user is a reseller with no billing profile
if($_SESSION['reseller']){
	$query = "SELECT billing_id2 FROM users WHERE id = '".$_SESSION['login_uid']."'";
	$a = $reseller->queryFetch($query);
	if($_SESSION['login_uid']=='2996' || $_SESSION['login_uid']=='5722' || $_SESSION['login_uid']=='7065' || $_SESSION['thm_no_billing']) $noForm = false;
	else if(!$a['billing_id2']) $noForm = true;
}

if($noForm){
	$html =
	'<h1>Add new user</h1>
	<strong>Unable to create new user</strong>: No billing profile found.<br /><br />
	In order for us to be able to charge you our wholesale fees for new accounts you must first create a billing profile in our system.  To do this click Settings under the Manage tab on the left, then go to the Billing Info tab.
	<br style="clear:both;" />';
}
else {
	$html = 
	'<script type="text/javascript">
	$(function(){
		$(".uniformselect").uniform();
	});
	</script>
	<div id="form_test" style="margin-bottom:10px;">
	<h1>Add new user</h1>
	<form action="reseller-functions.php" class="jquery_form new_account" method="post">
		<label>Email Address</label><input type="text" name="email" size="30" value="" class="no-upload" /><br />
		<div style="display:inline-block;margin-top:5px;">
		<label style="display:inline;">Password</label><br /><br />
		<input type="password" name="password" value="" style="width:214px;display:inline;" class="no-upload" />
		</div>&nbsp;
		<div style="display:inline-block;">
		<label style="display:inline;">Re-Enter Password</label><br /><br />
		<input type="password" name="password2" value="" style="width:213px;display:inline;" class="no-upload" />
		</div>
		<label>First Name</label><input type="text" name="first_name" value="'.$first.'" class="no-upload" /><br />
		<label>Last Name</label><input type="text" name="last_name" value="'.$last.'" class="no-upload" /><br />
		<label>Phone Number</label><input type="text" name="phone" value="" class="no-upload" /><br />
		<label>Company Name</label><input type="text" name="company" value="" class="no-upload" /><br />
		<label>Address</label><input type="text" name="address" value="" class="no-upload" /><br />
		<label>Address 2 - <small>(optional)</small> </label><input type="text" name="address2" value="" class="no-upload" /><br />
		<label>City</label><input type="text" name="city" value="" class="no-upload" /><br />
		<label>State</label><input type="text" name="state" size="2" maxlength="2" value="" class="no-upload" /><br />
		<label>Zip Code</label><input type="text" name="zip" size="5" maxlength="5" value="" class="no-upload" /><br /><br />
		<label>Active?</label>
		<select name="active" class="uniformselect no-upload" title="select">
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>';
	if($_SESSION['reseller'])	
		$html .= '<label>Product</label>
		'.$reseller->displayResellerProducts($_SESSION['login_uid'], 'dropdownWithOptions').'
		<br style="clear:both" />';
	else if($_SESSION['admin'])
		$html .= '<label>Class <span style="font-size:75%;color:#666;">(Admins only.  Defaults to 178 if blank)</span></label>
				<input type="text" name="class" maxlength="4" class="no-upload" value="" />
				<br style="clear:both" /><br />';
	if($_SESSION['admin'] && $parent_id)
		$html .= '<input type="hidden" name="parent_id" value="'.$parent_id.'" class="no-upload" />';
	else if($_SESSION['admin'])
		$html .= '<input type="hidden" name="parent_id" value="7" class="no-upload" />';
	else if($_SESSION['reseller'])
		$html .= '<input type="hidden" name="parent_id" value="'.$_SESSION['login_uid'].'" class="no-upload" />';
	$html .= '<input type="hidden" name="action" value="addNewUser" class="no-upload" />
			<input type="image" src="http://6qube.com/admin/img/signup-button';
	if($_SESSION['theme']) $html .= '/'.$_SESSION['thm_buttons-clr'];
	$html .= '.png" style="width:108px;height:33px;padding:0;background:none;border:none;" />
	</form>
	</div>
	<br style="clear:both" />';
}

$response['success']['id'] = '1';
$response['success']['container'] = '#ajax-load';
$response['success']['action'] = "replace";
$response['success']['message'] = $html;

echo json_encode($response);
?>