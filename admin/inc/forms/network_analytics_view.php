<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/analytics.class.php');
	
	//create new instance of class
	$db = new Analytics();
	
	$site_id = $_GET['id'];
	
	if(!$site_id){
		echo "<br /><br />Use the drop-down above to select which network site's analytics to view...<br /><br />";
	}
	
	else {
		$form = isset($_GET['form']) ? $_GET['form'] : "snapshot";
		$anal_id = isset($_GET['aid']) ? $_GET['aid'] : $db->ANAL_getAnalyticsId('resellers_network', $site_id);
		$period = $_GET['period'] ? $_GET['period'] : 'day';
		$bold = 'style="font-weight:bold;"';
		$lbase = 'inc/forms/press_analytics_view.php?id='.$site_id.'&aid='.$anal_id.'&form='.$form.'&period=';
		if($anal_id){
?>
<script language="JavaScript">
function calcHeight(){
	//find the actual height of the internal page and resize iframe
	$('.rsz_iframe').each(function(){
		var h = document.getElementById($(this).attr('id')).contentWindow.document.body.scrollHeight;
		$(this).attr('height', h);
	});
}
</script>

<!-- FORM NAVIGATION -->
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first"><a href="inc/forms/network_analytics_view.php?form=snapshot&id=<?=$site_id?>&aid=<?=$anal_id?>" <? if($form=="snapshot") echo 'class="current"'; ?>>Snap Shot</a></li>
		<li><a href="inc/forms/network_analytics_view.php?form=activity&id=<?=$site_id?>&aid=<?=$anal_id?>" <? if($form=="activity") echo 'class="current"'; ?>>Activity</a></li>
		<li><a href="inc/forms/network_analytics_view.php?form=referers&id=<?=$site_id?>&aid=<?=$anal_id?>" <? if($form=="referers") echo 'class="current"'; ?>>Referers</a></li>
		<li><a href="inc/forms/network_analytics_view.php?form=search&id=<?=$site_id?>&aid=<?=$anal_id?>" <? if($form=="search") echo 'class="current"'; ?>>Searches</a></li>
		<li class="last"><a href="inc/forms/network_analytics_view.php?form=visitors&id=<?=$site_id?>&aid=<?=$anal_id?>" <? if($form=="visitors") echo 'class="current"'; ?>>Visitors</a></li>
	</ul>
</div>
<br style="clear:left;" />

<? if($form=="snapshot"){ ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Traffic Snap Shot</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe1" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1">An iframe capable browser is required to view this web site.</iframe>
	</div>

<? } else if($form=="activity") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Activity</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe1" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageTitles&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1">An iframe capable browser is required to view this web site.</iframe>
	</div>
        
	<h2>Links Clicked On</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe2" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getOutlinks&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:600px;">An iframe capable browser is required to view this web site.</iframe>
	</div>
	
<? } else if($form=="referers") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Referers</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe1" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getWebsites&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:600px;">An iframe capable browser is required to view this web site.</iframe>
	</div>

<? } else if($form=="search") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Search Engines</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe1" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getSearchEngines&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:600px;">An iframe capable browser is required to view this web site.</iframe>
	</div>

<? } else if($form=="visitors") { ?>
	View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>day" class="dflt-link" <? if($period=='day') echo $bold; ?>>Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>week" class="dflt-link" <? if($period=='week') echo $bold; ?>>Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>month" class="dflt-link" <? if($period=='month') echo $bold; ?>>Month</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>year" class="dflt-link" <? if($period=='year') echo $bold; ?>>Year</a>
	<h2>Visits</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe1" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1">An iframe capable browser is required to view this web site.</iframe>
	</div>
	
	<h2>Returning Visitors</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe2" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitFrequency&actionToWidgetize=getSparklines&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:210px;" >An iframe capable browser is required to view this web site.</iframe>
	</div>
	
	<h2>Visits by Time</h2>
	<div id="widgetIframe">
		<iframe width="100%" id="iframe3" class="rsz_iframe"  onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitTime&actionToWidgetize=getVisitInformationPerLocalTime&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:300px;">An iframe capable browser is required to view this web site.</iframe>
	</div>
	
	<div class="mini_graph">
		<h2>Pages Per Visit</h2>
		<div id="widgetIframe">
			<iframe width="100%" id="iframe4" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:300px;">An iframe capable browser is required to view this web site.</iframe>
		</div>
	</div>
	
	<div class="mini_graph">
		<h2>Length of Visits</h2>
		<div id="widgetIframe">
			<iframe width="100%" id="iframe5" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:300px;">An iframe capable browser is required to view this web site.</iframe>
		</div>
	</div>
	
	<div class="mini_graph">
		<h2>Visitor Specs</h2>
		<div id="widgetIframe">
			<iframe width="100%" id="iframe6" class="rsz_iframe" onLoad="calcHeight();" src="../analytics/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getConfiguration&idSite=<?=$anal_id?>&period=<?=$period?>&date=today&disableLink=1" scrolling="NO" frameborder="0" height="1" style="min-height:300px;">An iframe capable browser is required to view this web site.</iframe>
		</div>
	</div>
<? } //end else if($form=="visitors") ?>
<? } //end if($anal_id) ?>
<? } //end else { at top of page ?>
<br style="clear:both;" /><br/>