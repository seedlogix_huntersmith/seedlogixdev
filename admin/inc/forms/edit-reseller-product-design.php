<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
if($_SESSION['reseller'] || $_SESSION['admin']){
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$uid = is_numeric($_GET['uid']) ? $_GET['uid'] : '';
	if($_SESSION['reseller']) $uid = $_SESSION['login_uid'];
	else $uid = $uid;
	
	$query = "SELECT main_site, payments, main_site_id FROM resellers WHERE admin_user = '".$uid."'";
	$resellerInfo = $reseller->queryFetch($query);
	$query = "SELECT theme FROM hub WHERE id = '".$resellerInfo['main_site_id']."'";
	$resellerThemeInfo = $reseller->queryFetch($query);
	$product = $reseller->getResellerProducts($uid, $id, true);
	
	$sel = 'selected="selected"';
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();

});
</script>
<div id="form_test" style="width:100%;">
<? if($_SESSION['admin']){ ?>
	<a href="inc/forms/edit-user-reseller-products.php?id=<?=$uid?>" class="dflt-link"><img src="img/v3/nav-previous-icon.jpg" alt="Back" /></a><br /><br />
    <? } else { ?>
    <a href="reseller-products.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Forms" /></a><br /><br />
	<? } ?>
<?php
	$thisPage = "design";
	include('edit_reseller_product_nav.php');
?>
<br style="clear:left;" /><br />
	<h1>Design Product: <?=$product['name']?></h1>
	
	<form name="user_class" class="jquery_form" id="<?=$id?>">
		<ul>

           <li>
			<label>Full Width Page?</label>
			<select name="product_full_width" title="select" class="uniformselect">
				<option value="1" <? if($product['product_full_width']) echo $sel; ?>>Yes</option>
				<option value="0" <? if(!$product['product_full_width']) echo $sel; ?>>No</option>
			</select>
		</li>
        
         <li>

			<input type="checkbox" name="remove_default_price" <? if($product['remove_default_price']) echo 'checked="checked"'; ?> /> Remove Default Price Title
		   </li>
           <br style="clear:both" />
            <li>
				<label>Between Contact Info & Business Info (must have business fields selected)</label>
                <a href="#" id="3" class="tinymce"><span>Rich Text</span></a>
				<textarea name="edit_region_3" rows="8" cols="55" id="edtr3"><?=$product['edit_region_3']?></textarea>
			</li>
        <li>
				<label>Below Form Edit Region</label>
                <a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="edit_region_1" rows="8" cols="55" id="edtr1"><?=$product['edit_region_1']?></textarea>
			</li>
              <li>
				<label>Column Edit Region</label>
                <a href="#" id="2" class="tinymce"><span>Rich Text</span></a>
				<textarea name="edit_region_2" rows="8" cols="55" id="edtr2"><?=$product['edit_region_2']?></textarea>
			</li>
             <li>
				<label>User Success Thank You Page (Only if no payment amount is set)</label>
                <a href="#" id="4" class="tinymce"><span>Rich Text</span></a>
				<textarea name="edit_region_4" rows="8" cols="55" id="edtr4"><?=$product['edit_region_4']?></textarea>
			</li>
        </ul>
        


	</form>
</div>
<br style="clear:both" /><br /><br />
<? } ?>