<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$mode = $_GET['mode'] ? $_GET['mode'] : 'all';
$formID = is_numeric($_GET['formID']) ? $_GET['formID'] : 0;
$leadID = is_numeric($_GET['leadID']) ? $_GET['leadID'] : 0;
$reseller_site = $_GET['reseller_site'] ? 1 : 0;
$hubID = $reseller_site ? $_SESSION['main_site_id'] : '';

if($mode=='all' && $formID){
	$leads = $hub->getCustomFormLeads($_SESSION['user_id'], $formID, NULL, NULL, NULL, $hubID);
	$query = "SELECT name FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$_SESSION['user_id']."'";
	$a = $hub->queryFetch($query);
	$formName = $a['name'];
	$backLink = 'hub-prospects.php';
	if($reseller_site) $backLink .= '?reseller_site=1';
?>
	<h1><?=$formName?> - Responses</h1>
	<p>Click <i>View Details</i> on an item below to view the full response details.</p><br />
	<a href="<?=$backLink?>"><img src="img/back-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" /></a>
	<div id="message_cnt" style="display:none;"></div>
	<br /><br /><br />
<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
	<li class="email" style="width:295px;"><strong>Prospect</strong></li>
	<li class="email" style="width:190px;"><strong>Hub Name</strong></li>
	<li class="phone" style="width:170px;"><strong>Date &amp; Time</strong></li>
</ul>
<?
	if($leads && $hub->numRows($leads)){
		while($row = $hub->fetchArray($leads)){
		//get form data
		$query = "SELECT name, data FROM lead_forms WHERE id = '".$row['lead_form_id']."'";
		$form = $hub->queryFetch($query);
		//get hub data
		if($row['hub_id']){
			$query = "SELECT name FROM hub WHERE id = '".$row['hub_id']."'";
			$hubInfo = $hub->queryFetch($query, NULL, 1);
			$hubName = $hubInfo['name'];
		}
		$leadData = explode('[==]', $row['data']);
		$b = explode('|-|', $leadData[0]);
		$dispTitle = $b[1].': '.$b[2];
		$timestamp = date('n/j/Y', strtotime($row['created'])).' at '.date('g:ia', strtotime($row['created']));
?>
	<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
		<li class="name" style="width:270px;"><?=$dispTitle?></li>
		<li class="phone" style="width:190px;"><i><?=$hubName?></i></li>
		<li class="phone" style="width:170px;"><i><?=$timestamp?></i></li>
		<li class="details" style="width:100px;"><a href="inc/forms/view-leads.php?mode=single&leadID=<?=$row['id']?>">View Details</a></li>
		<li class="delete"><a href="#" class="delete rmParent" rel="table=leads&id=<?=$row['id']?>&undoLink=hub-forms"><span>Delete</span></a></li>
	</ul>
<?
		} //end while loop
	} //end if($leads && mysqli_num_rows($leads))
	else echo '<p><strong>No responses to this form yet.</strong></p>';
} //end if($mode=='all' && $formID)

else if($mode=='single' && $leadID){
	$lead = $hub->getCustomFormLeads($_SESSION['user_id'], NULL, $leadID);
	if($lead){
		$query = "SELECT name FROM lead_forms WHERE id = '".$lead['lead_form_id']."'";
		$form = $hub->queryFetch($query);
		$leadData = explode('[==]', $lead['data']);
		$backLink = 'inc/forms/view-leads.php?mode=all&formID='.$lead['lead_form_id'];
		if($reseller_site) $backLink .= '&reseller_site=1';
		//get hub info
		$query = "SELECT name, pages_version FROM hub WHERE id = '".$lead['hub_id']."'";
		$hubInfo = $hub->queryFetch($query);
		if($lead['hub_page_id']){
			$pTable = 'hub_page';
			if($hubInfo['pages_version']==2) $pTable .= '2';
			$query = "SELECT page_title FROM `".$pTable."` WHERE id = '".$lead['hub_page_id']."' LIMIT 1";
			if($pageInfo = $hub->queryFetch($query)) $pageTitle = $pageInfo['page_title'];
		}
		
	?>
		<h1><?=$form['name']?></h1>
		<strong><?=date('l F jS, Y', strtotime($lead['created']))?></strong> at <strong><?=date('g:ia', strtotime($lead['created']))?></strong> from IP address <strong><?=$lead['ip']?></strong><br />
		Submitted from the 
		<? if($pageTitle){ ?><strong><?=$pageTitle?></strong> page on the <? } ?>
		<strong><?=$hubInfo['name']?></strong> hub
		<br /><br />
		<a href="<?=$backLink?>"><img src="img/back-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" /></a>
		<br /><br /><br />
	<?
		if($leadData){
			foreach($leadData as $key=>$value){
				$fieldData = explode('|-|', $value);
				echo '<strong>'.$fieldData[1].'</strong>:<br />
					'.$fieldData[2].'<br /><br />';
			}
		}
	}
}
else echo 'Validation error.';
echo '<br style="clear:both;" />';
?>