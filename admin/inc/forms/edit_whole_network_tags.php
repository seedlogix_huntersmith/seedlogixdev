<small>
	<strong>Hint:</strong><br />
	&bull; You can pull the city group listings for further optimization in each edit region. <strong>((city))</strong>.  <a href="#" class="dflt-link" id="showCustomPageTags">Click here to see available tags</a>.<br />
	<div id="availableTags" style="display:none;line-height:25px;">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((city))</strong> - City (<em>Austin Business Listings</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((state))</strong> - State (<em>TX Business Listings</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((category))</strong> - Category (<em>Austin Landscaping</em>)<br />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>((category)) in ((city)), ((state))</strong> - Example (<em>Landscaping in Austin, TX</em>)<br />
	</div>
	</small>
	<br />
