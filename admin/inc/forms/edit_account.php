<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//Authorize access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once('./inc/local.class.php');
	$local = new Local();
	
	$query = "SELECT * FROM user_info WHERE user_id = '".$_SESSION['user_id']."' ";
	$row = $local->queryFetch($query);
	
	$_SESSION['current_id'] = $row['id'];
	
	$query2 = "SELECT * FROM users WHERE id = '".$_SESSION['user_id']."'";
	$row2 = $local->queryFetch($query2);
	
	$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/user_info/';
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	if(!$user_id || $user_id==$_SESSION['user_id']){	
?>
<script type="text/javascript">
$(function(){
	$(function(){
		$(".uniformselect, input[type=file]").uniform();
	});
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<!-- FORM NAVIGATION -->
<div id="form_test">
	<h2>Edit Account Info</h2>
	<form name="users" class="jquery_form" id="<?=$_SESSION['user_id']?>">
		<ul>
			<li>
				<label>Login Email</label>
				<input type="text" name="username" value="<?=$row2['username']?>" />
			</li>
			<? if(!$_SESSION['support_user'] || ($_SESSION['support_user'] && $_SESSION['user_id']!=$_SESSION['login_uid'])) { ?>
			<li>
				<label>Password</label>
				<input type="password" name="password" value="<?=$row2['password']?>" />
			</li>
			<? if($_SESSION['admin']){ ?>
			<li>
				<label>Security Phrase</label>
				<input type="text" name="sec_phrase" value="<?=$row2['sec_phrase']?>" />
			</li>
			<? } ?>
			<? } ?>
		</ul>
	</form>
	<br />
	<form name="user_info" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>First Name</label>
				<input type="text" name="firstname" value="<?=$row['firstname']?>" />
			</li>
			<li>
				<label>Last Name</label>
				<input type="text" name="lastname" value="<?=$row['lastname']?>" />
			</li>
			<li>
				<label>Secondary E-mail</label>
				<input type="text" name="email" value="<?=$row['email']?>" />
			</li>
			<li>
				<label>Phone</label>
				<input type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
			<li>
				<label>Company Name</label>
				<input type="text" name="company" value="<?=$row['company']?>" />
			</li>
			<li>
				<label>Address</label>
				<input type="text" name="address" value="<?=$row['address']?>" />
			</li>
			<li>
				<label>City</label>
				<input type="text" name="city" value="<?=$row['city']?>" />
			</li>
			<li>
				<label>State</label>
				<?=$local->stateSelect('state', $row['state'])?>
			</li>
			<li>
				<label>Zip</label>
				<input type="text" name="zip" value="<?=$row['zip']?>" />
			</li>
		</ul>
	</form>
    
 
     <!-- profile photo -->
    
    <form id="img_one" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Backoffice Profile Picture</label>
        <? if($row['profile_photo']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="account.php?id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_profile_photo').trigger('blur');">Clear Image</a>
		 <? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="user_info" />
		<input type="hidden" name="fname" value="profile_photo" />
		<input type="hidden" name="img_id" id="img_id" value="<?=$_SESSION['current_id']?>" />
		<input type="submit" class='hideMe' value="Upload!" />
	</form>
	<br style="clear:both;" />
	<img class="img_cnt" src="<? if($row['profile_photo']) echo $img_url.$row['profile_photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />

    
	<? if(!$_SESSION['reseller'] && !$_SESSION['admin']){ ?>
	<br />
	<form name="users" class="jquery_form" id="<?=$_SESSION['user_id']?>">
		<ul>
			<li>
				<label>Opt-Out From Marketing Emails?</label>
				<select name="marketing_optout" title="select" class="uniformselect">
					<option value="0"<? if(!$row2['marketing_optout']) echo ' selected="selected"'; ?>>No</option>
					<option value="1"<? if($row2['marketing_optout']) echo ' selected="selected"'; ?>>Yes</option>
				</select>
			</li>
		</ul>
	</form>
	<? } ?>
    <form name="user_info" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li style="display:none;">
				<input type="text" name="profile_photo" value="" id="clear_profile_photo" />
			</li>
		</ul>
	</form>
	<br /><br />
</div>

<? if(!$_SESSION['theme']){ ?>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<h2>Account Settings Videos</h2>
	<div class="app_right_links">
		<ul>
			<li>
				<a href="#accountsetVideo" class="trainingvideo">Account Settings Video</a>
			</li>
			<div style="display:none;">
				<div id="accountsetVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/accountset-overview.flv" style="display:block;width:640px;height:480px" id="accountsetplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("accountsetplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
	</div><!--End APP Right Links-->
	
	<?	
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	?>							
</div>
<!--End APP Right Panel-->
<? } ?>
<br style="clear:both;" /><br />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>