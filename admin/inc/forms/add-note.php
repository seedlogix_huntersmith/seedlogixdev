<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once('../auth.php');

//initiate the class
require_once('../contacts.class.php');
$notes = new Contacts();

$subject = $notes->validateText($_POST['subject']);
$contactID = $_POST['contactID'];

if($subject){
		$subjectID = $notes->addNewNote($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID, $subject);
		
		if($subjectID){
			$html = 
			'<div class="item3">
				<div class="icons">
					<a href="inc/forms/view-contact.php?mode=noteedit&noteid='.$subjectID.'" class="edit"><span>Edit</span></a>
					<a href="#" class="delete rmParent" rel="table=notes&id='.$subjectID.'&undoLink=hub-forms"><span>Delete</span></a>
				</div>';
				
				$html .= '<img src="img/blog-post.png" class="icon" />';
				
				$html .= '<a href="inc/forms/view-contact.php?mode=noteedit&noteid='.$subjectID.'&contactID='.$contactID.'">'.$subject.'</a>
			</div>';
			
			$response['success']['id'] = '1';
			$response['success']['container'] = '#list_notes .container';
			$response['success']['message'] = $html;
		}
	}
	echo json_encode($response);
?>