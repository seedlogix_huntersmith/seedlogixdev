<?php
session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

if(!$id){
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$id = $_GET['id'];
}
if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
else $dflt_noImg = 'img/no-photo.jpg';

$query = "SELECT * FROM resellers_network WHERE id = ".$id." AND admin_user = ".$_SESSION['login_uid'];
$row = $local->queryFetch($query);

if($row['setup']!=1){
	include(QUBEADMIN . 'reseller-network-setup.php');
}
else {	
	$query = "SELECT * FROM themes_network WHERE id = ".$row['theme'];
	$themeInfo = $local->queryFetch($query);
	
	if($row['admin_user'] == $_SESSION['login_uid']){
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
$thisPage = "global";
include('edit_whole_network_nav.php');
?>
<div id="form_test">
	<h1>Edit Whole Network - <?=$row['name']?></h1>
	<h2>Global Edit Regions Settings</h2>
	Editing these settings will copy them to all sites in the network.<br />
	For instance, if you add an advertisement this would be displayed on each site as well. <br /><br />
	<?php
include('edit_whole_network_tags.php');
?>
	<form name="resellers_network" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
					<label><?=$themeInfo['region1_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea class="update-whole-network" name="edit_region_1"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_1']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region2_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea class="update-whole-network" name="edit_region_2"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_2']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region3_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea class="update-whole-network" name="edit_region_3"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_3']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region4_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea class="update-whole-network" name="edit_region_4"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_4']?></textarea>
				</li>
				<li>
					<label><?=$themeInfo['region5_title']?></label>
					
					<a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
					<textarea class="update-whole-network" name="edit_region_5"  rows="20" cols="60" id="edtr1" ><?=$row['edit_region_5']?></textarea>
				</li>
			
		</ul>
	</form><br />
	

	
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">

	<?php
include('edit_whole_network_global_nav.php');
?>

	
						
</div>
<!--End APP Right Panel-->
<br style="clear:both;" />
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<?
	} else echo "Unable to display this page."; 
}
?>