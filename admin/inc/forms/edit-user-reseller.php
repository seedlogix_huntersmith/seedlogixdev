<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');

//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
//create new instance of class
$reseller = new Reseller();

$id = $_GET['id'];
$id = is_numeric($id) ? $id : '';

//$user = $reseller->getUsers(NULL, $id);
$query = "SELECT id, admin_user, `custom-ui`, `custom-backoffice`, payments, payments_acct, payments_acct2, fbapp_id, fbapp_secret,
            twapp_id, twapp_secret
		FROM resellers
		WHERE admin_user = '".$id."'";
$user = $reseller->queryFetch($query);

if(($_SESSION['reseller'] && ($user['admin_user']==$_SESSION['login_uid'])) || $_SESSION['admin']){
	$isReseller = true;
	if($user['admin_user']==$_SESSION['login_uid']){
		$isSelf = true;
	}
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script> 
    
<?
//section navigation
$thisPage = 'reseller';
include('edit-user-nav.php');
?>
<br style="clear:left;" />
<br />
	<h1>User #<?=$id?> Info: System Info</h1>
<form name="resellers" class="jquery_form" id="<?=$user['id']?>">
    <p></p>
	<ul>
<? if(!$_SESSION['thm_adminmanage_processor']){ ?>
		<li>
		<label>Payment Processor: <small style="color:#666;">(reload form after changing)</small></label>
		<select name="payments" title="select" class="uniformselect">
			<option value="6qube" <? if($user['payments']=='6qube') echo 'selected="yes"'; ?>>6Qube</option>
			<option value="paypal" <? if($user['payments']=='paypal') echo 'selected="yes"'; ?>>PayPal</option>
			<option value="authorize" <? if($user['payments']=='authorize') echo 'selected="yes"'; ?>>Authorize.net</option>
			<option value="other" <? if($user['payments']=='other') echo 'selected="yes"'; ?>>Other</option>
		</select>
		</li>
	<? if($user['payments']=='paypal'){ ?>
		<li>
		<label>PayPal Email Account:</label>
		<input type="text" name="payments_acct" value="<?=$user['payments_acct']?>" />
		</li><br />
		<b>Note:</b> This method is still under development and may not work properly. <small>Last updated 4/20/2011</small><br /><br />
	<? } else if($user['payments']=='authorize'){ ?>
		<li>
		<label>API Login: <small style="color:#666;">(8-10 digits, like 1aBc5giKaB)</small></label>
		<input type="text" name="payments_acct" value="<?=$user['payments_acct']?>" />
		</li><br />
		<li>
		<label>Transaction Key: <small style="color:#666;">(16 digits, like 12AF4mnR55gmb4tN)</small></label>
		<input type="text" name="payments_acct2" value="<?=$user['payments_acct2']?>" />
		</li><br />
		<b>Important:</b> You must enable the Customer Information Manager (CIM) add-on<br />
		in your Authorize.net account settings for this method to work.
		<br /><br />
	<? } ?>	
<? } ?>	
    <? if(!$_SESSION['admin_design']){ ?>	
		<li>
		<label>Custom jQuery UI?</label>
		<select name="custom-ui" title="select" class="uniformselect">
			<option value="0" <? if(!$user['custom-ui']) echo 'selected="yes"'; ?>>No</option>
			<option value="1" <? if($user['custom-ui']=='1') echo 'selected="yes"'; ?>>Yes</option>
		</select>
		</li>
        <? } ?>	
		<li>
		<label>Enable Custom Back Office Section</label>
		<select name="custom-backoffice" title="select" class="uniformselect">
			<option value="0" <? if(!$user['custom-backoffice']) echo 'selected="yes"'; ?>>No</option>
			<option value="1" <? if($user['custom-backoffice']=='1') echo 'selected="yes"'; ?>>Yes</option>
		</select>
		</li>
		<li>
		<label>Branding / Facebook App ID</label>
                <input type="text" name="fbapp_id" value="<?php echo htmlentities($user['fbapp_id'], ENT_QUOTES); ?>"><p></p>
		</li>
		<li>
		<label>Branding / Facebook App Secret</label>
                <input type="text" name="fbapp_secret" value="<?php echo htmlentities($user['fbapp_secret'], ENT_QUOTES); ?>"><p></p>
		</li>
		<li>
		<label>Branding / Twitter Customer ID</label>
                <input type="text" name="twapp_id" value="<?php echo htmlentities($user['twapp_id'], ENT_QUOTES); ?>"><p></p>
		</li>
		<li>
		<label>Branding / Tiwtter Customer Secret</label>
                <input type="text" name="twapp_secret" value="<?php echo htmlentities($user['twapp_secret'], ENT_QUOTES); ?>"><p></p>
		</li>
	<? if($_SESSION['admin']){ ?>
		<h2>6Qube Admin Options</h2>
		<li>
		<label>Reseller Type</label>
		<select name="reseller_type" title="select" class="uniformselect">
			<option value="standard">Standard</option>
			<option value="multihub">Multi-Hub</option>
		</select>
		</li>
	<? } ?>
		<br />
	</ul>
</form><br />
<? } else echo "Not authorized. ";//.$user['id'].' = '.$_SESSION['login_uid']; ?>