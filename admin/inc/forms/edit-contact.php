<?php
	session_start();         require_once( dirname(__FILE__) . '/../../6qube/core.php');

	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	require_once(QUBEADMIN . 'inc/contacts.class.php');
	$contacts = new Contacts();
	if($id){
	$row = $contacts->getContacts($_SESSION['user_id'], $_SESSION['campaign_id'], $id);
	}
	$leadData = explode('[==]', $row['lead_data']);
	
	if($row['user_id']==$_SESSION['user_id']){
?>
 <? if($row['lead_data']){ 
			   echo '<div class="leadDetails" style="padding-top:50px;">
									<div class="outer-rounded-box-bold">
									<div class="simple-rounded-box">
									<h2>Form Response</h2>';
					foreach($leadData as $key=>$value){
						$fieldData = explode('|-|', $value);
						echo '
								<p><strong>'.$fieldData[1].'</strong>: 
												'.$fieldData[2].'
								</p>';
					}
					echo '</div></div></div>';
					
	
    } else { ?>
    
    <? } ?>	

<div id="form_test" style="width:48%;">
<a href="contacts.php?mode=view&id=<?=$id?>" ><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Contacts" /></a>

	<h2><?=$row['first_name']?> <?=$row['last_name']?></h2>
    
	<form class="jquery_form" name="contacts" id="<?=$id?>">
		<ul>
			<li>
				<label>First Name</label>
				<input type="text" name="first_name" value="<?=$row['first_name']?>" />
			</li>
			<li>
				<label>Last Name</label>
				<input type="text" name="last_name" value="<?=$row['last_name']?>" />
			</li>
             <li>
				<label>Company</label>
				<input type="text" name="company" value="<?=$row['company']?>" />
			</li>
             <li>
				<label>Title</label>
				<input type="text" name="title" value="<?=$row['title']?>" />
			</li>
            <li>
				<label>Email Address</label>
				<input type="text" name="email" value="<?=$row['email']?>" />
			</li>
            <li>
				<label>Phone</label>
				<input type="text" name="phone" value="<?=$row['phone']?>" />
			</li>
             <li>
				<label>Address</label>
				<input type="text" name="address" value="<?=$row['address']?>" />
			</li>
             <li>
				<label>Address 2</label>
				<input type="text" name="address2" value="<?=$row['address2']?>" />
			</li>
             <li>
				<label>City</label>
				<input type="text" name="city" value="<?=$row['city']?>" />
			</li>
             <li>
				<label>State</label>
				<input type="text" name="state" value="<?=$row['state']?>" />
			</li>
             <li>
				<label>Zip Code</label>
				<input type="text" name="zip" value="<?=$row['zip']?>" />
			</li>
             <li>
				<label>Website</label>
				<input type="text" name="website" value="<?=$row['website']?>" />
			</li>
             <li>
				<label>Reports To</label>
				<input type="text" name="reports_to" value="<?=$row['reports_to']?>" />
			</li>
            
			
		</ul>
	</form> 
	
	
	<br style="clear:both;" />

</div>

<!--Start APP Right Panel-->
<div id="app_right_panel">
		
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br />
<? } else echo "Error displaying page."; ?>