<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
//$id = $_SESSION['current_id'] = $_GET['id'];
$query = "SELECT id, user_id, user_id_created, hub_parent_id, name, company_name, phone, street, city, state, zip, domain, category, logo, profile_photo, favicon, slogan, google_webmaster, button_1, coupon_offer, backlink, description, affiliate_info, display_page, form_type, google_tracking, theme, req_theme_type, pages_version, multi_user, multi_user_classes, multi_user_fields 
		  FROM hub WHERE id = '".$_SESSION['current_id']."' ";
$row = $hub->queryFetch($query);
if($row['user_id']==$_SESSION['user_id']){
	//get current theme info
	$query = "SELECT user_class2_fields, pages, region1_title, region2_title, region3_title, region4_title,
				  region5_title, photo_size, logo_size 
			FROM themes WHERE id = '".$row['theme']."'";
	$themeInfo = $hub->queryFetch($query);
	
	$img_url = 'http://'.$_SESSION['main_site'].'/users/'.$_SESSION['user_id'].'/hub/';
	
	//get field info from hub row for locks
	if($row['hub_parent_id'] || $row['multi_user']){
		if($row['multi_user']) $fieldsInfoData = $row['multi_user_fields'];
		else {
			//if hub is a V2 MU Hub copy get parent hub info
			$query = "SELECT multi_user_fields FROM hub WHERE id = '".$row['hub_parent_id']."' 
					  AND user_id = '".$_SESSION['parent_id']."'";
			$parentHubInfo = $hub->queryFetch($query, NULL, 1);
			$fieldsInfoData = $parentHubInfo['multi_user_fields'];
		}
	}
	
	if($_SESSION['thm_dflt-no-img']) $dflt_noImg = $_SESSION['themedir'].$_SESSION['thm_dflt-no-img'];
	else $dflt_noImg = 'img/no-photo.jpg';
	
	$lockedFields = array('');
	//if user is a reseller editing a multi-user hub
	if(($row['req_theme_type']==4 && $_SESSION['reseller']) ||
		($row['multi_user'] && ($_SESSION['reseller'] || $_SESSION['admin']))){
		//multi-user hub
		$_SESSION['multi_user_hub'] = $multi_user = 1;
		$muHubID = $row['multi_user'] ? $_SESSION['current_id'] : $row['theme'];
		$addClass = ' multiUser-'.$muHubID.'n';
		//get user class names
		$allowed = explode('::', $row['multi_user_classes']);
		$classes = array();
		$query = "SELECT id, name, admin_user FROM user_class ";
		if($_SESSION['reseller']) $query .= "WHERE admin_user = '".$_SESSION['login_uid']."'";
		else if($_SESSION['admin']) $query .= "WHERE admin_user = 0 OR admin_user = 7 OR maps_to = 885";
		$b = $hub->query($query);
		while($c = $hub->fetchArray($b)){
			if($c['admin_user'] && $_SESSION['admin']) $reselerdisplay = '- '.$c['admin_user'];
			$classes[$c['id']] = $c['name'].' '.$reselerdisplay;
		}
	}
	else $_SESSION['multi_user_hub'] = $multi_user = NULL;
	//if user is a reseller or client editing a multi-user hub
	if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && 
		($hub->isMultiUserTheme($row['theme']) || $row['multi_user']) || $row['hub_parent_id']){
		$multi_user2 = true;
		//figure out which fields to lock
		if(!$fieldsInfoData) $fieldsInfoData = $themeInfo['user_class2_fields'];
		if($fieldsInfo = json_decode($fieldsInfoData, true)){
			foreach($fieldsInfo as $key=>$val){
				if($fieldsInfo[$key]['lock']==1)
					$lockedFields[] = $key;
			}
		}
	}
	
	function checkLock($field, $admin = ''){
		global $lockedFields, $multi_user2;
		//bypass for non-multi-user hubs
		if($multi_user2){
			//otherwise check if field is in lockedFields array
			if(in_array($field, $lockedFields)){
				if($admin){
					return 'selected="selected"'; //if so and user is reseller, set Locked/Unlocked dropdown
				}
				else if(!$_SESSION['reseller'] && !$_SESSION['admin']){
					return ' readonly'; //else if so and user is a client, lock field
				}
			}
		}
	}
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file], input:checkbox").uniform();
	
	<? if($multi_user){ ?>
	$('.muAllowedClassesCheckbox').die();
	$('.muAllowedClassesCheckbox').live('click', function(){
		var checked = $(this).attr('value');
		var id = $(this).parent().parent().parent().parent().parent().attr('id');
		var thisClass = $(this).attr('name');
		
		params = { 'action': 'updateAllowedClass', 'checked': checked, 'id': id, 'product': thisClass, 'muV2': 1 };
		$.post('reseller-functions.php', params, function(data){
			json = $.parseJSON(data);
			if(json.action == 'errors'){
				alert(json.errors);
			}
		});
	});
	$(".createCampaigns").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
	
	<? if(!$_SESSION['theme']){ ?>
	$(".helpLinks").fancybox({
		'width'			: '75%',
		'height'			: '75%',
		'autoScale'		: false,
		'transitionIn'		: 'none',
		'transitionOut'	: 'none',
		'type'			: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
	<? } ?>
});
</script>
<? if($_SESSION['user_id']== "7"){ ?>
<a href="#campaignContents" class="greyButton createCampaigns" style="float:right;margin-top:-40px;" >Auto Campaigns</a>
<div style="display: none;">
				<div id="campaignContents" style="width:640px;height:400px;overflow:auto;">
					<div id="createDir">
                    <h3>Select Pre-Defined Campaign Structure</h3><br />
	<form method="post" action="add-campaigns-bulk.php" class="ajax">
		<!--<input type="checkbox" name="uscities"  /> Major US Cities (Campaigns by State Name/Websites by City Name) -->
        <label>Campaign ID</label>
        <input type="text" name="campaignid" value="" />
        <br style="clear:both;" />
        <label>Root URL (domain.com)</label>
       <input type="text" name="rooturl" value="" />
        <br style="clear:both" /><br />
		<input type="submit" value="Create Campaigns" name="submit" />
	</form>
    
    <div id="campaigns_success"><div class="campaigns_container"></div></div>
</div>
</div>
</div>
<? } ?>
<!-- FORM NAVIGATION -->
<?php
	$thisPage = "settings";
	//$currTheme = $row['theme'];
	if($themeInfo['pages']) $hasPages = true;
	if($row['pages_version']==2) $pagesV2 = true;
	include('add_hub_nav.php');
?>
<br style="clear:left;" />
<div id="form_test">
	<h2><?=$row['name']?> - Settings</h2>
	<?
	if($multi_user){ 
		echo '<strong>Multi-User Instructions:</strong><br />';
		echo 'Enter a value in any field and it will be reproduced for your user when they choose this theme.<br /><br />';
		echo 'To prevent a user from changing a preset value enter the value first and then change the field to Locked.';
	}
	?>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<? if($multi_user){ ?>
			<li style="line-height:23px;">
				<label style="padding-bottom:0;">Allowed User Types</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">User types who are allowed to access this multi-user hub.</div>
				<? foreach($classes as $key=>$value){ ?>
				<input type="checkbox" name="<?=$key?>" class="muAllowedClassesCheckbox no-upload" <? if(in_array($key, $allowed)) echo 'checked'; ?> />&nbsp; <?=$value?><br />
				<? } ?>
			</li>
			<? } ?>
			<li>
				<label>Hub Name</label>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">For internal purposes</div>
				<input type="text" name="name" value="<?=htmlentities($row['name'])?>" />
			<li>
				<label>
				<?php if($row['theme']=="54" || $_SESSION['user_class']== "387"){?>
                	<?php if($row['theme']=="54"){?>
                    Agent's Name
                    <?php } ?>
                    <?php if($_SESSION['user_class']== "387"){?>
                    First & Last Name
                    <?php } ?>
				<? } else { ?>
				Company Name
				<?php } ?>
				</label>
				<? if($multi_user){ ?>
				<select name="lock_company_name" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('company_name',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('company_name')?> name="company_name" value="<?=htmlentities($row['company_name'])?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Phone Number</label>
				<? if($multi_user){ ?>
				<select name="lock_phone" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('phone',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('phone')?> name="phone" value="<?=htmlentities($row['phone'])?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>Address</label>
				<? if($multi_user){ ?>
				<select name="lock_street" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('street',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('street')?> name="street" title="address" value="<?=htmlentities($row['street'])?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>City</label>
				<? if($multi_user){ ?>
				<select name="lock_city" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('city',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('city')?> name="city" title="address" value="<?=htmlentities($row['city'])?>" class="<?=$addClass?>" />
			</li>
			<li>
				<label>State</label>
				<? if($multi_user){ ?>
				<select name="lock_state" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('state',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<?
				if($addClass) echo $hub->stateSelect('state', $row['state'], $addClass);
				else echo $hub->stateSelect('state', $row['state'], checkLock('state'));
				?>
			</li> 
			<li>
				<label>Zip Code</label>
				<? if($multi_user){ ?>
				<select name="lock_zip" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('zip',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<input type="text" <?=checkLock('zip')?> name="zip" title="address" value="<?=htmlentities($row['zip'])?>" class="<?=$addClass?>" />
			</li>
			<?php if($row['theme'] == "80" || $row['theme'] == "82"){ ?>
			<li>
				<label>License Number</label>
				<? if($multi_user){ ?>
				<select name="lock_affiliate_info" title="select" class="ajax-select uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('affiliate_info',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				
				<input type="text" <?=checkLock('affiliate_info')?> name="affiliate_info" value="<?=$row['affiliate_info']?>" class="<?=$addClass?>" />
			</li>
			<?php } ?>
			<li>
				<label>Business Category</label>
				<? if($multi_user){ ?>
				<select name="lock_category" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('category',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<?
				if($addClass) echo $hub->displayCategories($row['category'], $addClass);
				else echo $hub->displayCategories($row['category'], checkLock('category'));
				?>
			</li>
		</ul>
	</form>
	
	<!-- Logo -->
	<form id="logo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>
		<?php if($row['theme'] == "54"){?>Agent's Photo<? } else { ?>Company Logo<? } ?> 
		<? if($themeInfo['logo_size']){ ?> | <span style="color:#666; font-size:75%;">Recommended Size: <?=$themeInfo['logo_size']?></span><? } ?>
		</label>
		<? if($multi_user){ ?>
		<select name="lock_logo" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('logo',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('logo')){ ?>
		<? if($row['logo']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=settings&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_logo').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="logo" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		<? if($reseller_site){ ?>
		<input type="hidden" name="reseller_site" value="true" />
		<? } ?>
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img id="logo_upload_prvw" class="img_cnt" src="<? if($row['logo']) echo $img_url.$row['logo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />
	
	<? if($reseller_site){ ?>
	<!-- Favicon -->
	<form id="favicon_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>Favicon | <span style="color:#666;font-size:75%;">max 64w x 64h image in .png format</span></label>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="favicon" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<input type="hidden" name="reseller_site" value="true" />
		<input type="submit" class='hideMe' value="Upload!"/>
	</form>
	<img id="favicon_upload_prvw" class="img_cnt" src="<? if($row['favicon']) echo $img_url.$row['favicon'];  else echo $dflt_noImg; ?>" style="width:64px;height:64px;" />
	<? } ?>
    
   
    <!-- Logo -->
	<form id="profile_photo_upload" enctype="multipart/form-data" class="upload_form" action="upload_file.php" target="uploadIFrame" method="POST">
		<label>
		<?php if($row['theme'] == "82"){?>Loan Officer Photo<? } else { ?>Secondary Logo<? } ?>
		
		</label>
		<? if($multi_user){ ?>
		<select name="lock_profile_photo" title="select" class="uniformselect imgLock<?=$addClass?>" rel="<?=$_SESSION['current_id']?>">
			<option value="0">Unlocked</option>
			<option value="1" <?=checkLock('profile_photo',1)?>>Locked</option>
		</select><div style="margin-top:-10px;"></div>
		<? } ?>
		<? if(!checkLock('profile_photo')){ ?>
		<? if($row['profile_photo']){ ?>
		&nbsp;&nbsp;&nbsp;<a href="hub.php?form=settings&id=<?=$_SESSION['current_id']?>" class="dflt-link" onclick="javascript:$('#clear_profile_photo').trigger('blur');">Clear Image</a>
		<? } ?>
		<input type="file" class="test_file" name="test_file" />
		<input type="hidden" name="table" value="hub" />
		<input type="hidden" name="fname" value="profile_photo" />
		<input type="hidden" name="img_id" value="<?=$current_id?>" />
		<? if($multi_user){ ?>
		<input type="hidden" name="multi_user" value="true" />
		<input type="hidden" name="multi_user_id" value="<?=$muHubID?>" />
		<? } ?>
		
		<input type="submit" class='hideMe' value="Upload!"/>
		<? } ?>
	</form>
	<img id="profile_photo_upload_prvw" class="img_cnt" src="<? if($row['profile_photo']) echo $img_url.$row['profile_photo'];  else echo $dflt_noImg; ?>" style="max-width:300px;max-height:300px;" />

	
	
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Slogan / Tagline</label>
				<? if($multi_user){ ?>
				<select name="lock_slogan" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('slogan',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<textarea name="slogan" <?=checkLock('slogan')?> rows="6" class="<?=$addClass?>"><?=htmlentities($row['slogan'])?></textarea>
				<br style="clear:left" />
			</li>
		<? if(!$reseller_site && !$multi_user){ ?>
			<li>
				<label>Domain Name: <? if(!$_SESSION['theme']){ ?><span style="color:#666; font-size:75%">(<a class="external" target="_new" href="https://6qube.com/billing/domainchecker.php" >Buy or Transfer Domain Name</a>)</span><? } ?>
                 
                </label>
				<div id="domainerror" style="color:#888;font-size:12px; padding-bottom:5px;">Example: http://www.domain.com <?php if($_SESSION['user_class']== "387"){?>or <strong>http://bobgaston.myenw.com</strong><?php } ?></div>
				<input type="text" class="website-url" id="domain" name="domain" value="<?=$row['domain']?>" title="hub" />
			</li>
		<? } ?>
			
			<li>
				<label>Coupon Offer</label>
				<? if($multi_user){ ?>
				<select name="lock_coupon_offer" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('coupon_offer',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<? if(!checkLock('coupon_offer')){ ?><a href="#" id="1" class="tinymce"><span>Rich Text</span></a><? } ?>
				<textarea name="coupon_offer" <?=checkLock('coupon_offer')?> rows="6" id="edtr1" class="<?=$addClass?>"><?=htmlentities($row['coupon_offer'])?></textarea>
				<br style="clear:left" />
			</li>
            <?php if($_SESSION['user_class']== "387"){?>
            <? } else { ?>
			<li>
				<label>
				<?php if($row['theme'] == "54"){?>Agent's Decription<? } else { ?>Hub Description<?php } ?>
				</label>
				<?php if($row['theme'] == "54"){?><div style="color:#888;font-size:12px;padding-bottom:5px;">(Write Small Paragraph About Yourself)</div><?php } ?>
				<? if($multi_user){ ?>
				<select name="lock_description" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('description',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
				<div style="color:#888;font-size:12px;padding-bottom:5px;">This is publicly displayed with your hub on our <a href="http://hubs.<?=$_SESSION['main_site']?>" target="_blank" class="dflt-link">network</a></div>
				<input type="text" <?=checkLock('description')?> name="description" value="<?=htmlentities($row['description'])?>" class="<?=$addClass?>" />
			</li>
            <?php } ?>
			<li>
				<label>Google Webmaster Tools</label>
				<textarea name="google_webmaster" rows="2" class="<?=$addClass?>"><?=htmlentities($row['google_webmaster'])?></textarea>
			</li>
			<li>
				<label>Additonal Tracking Code</label>
			<div style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>Google Analytics Embed Code</i></div>
				<textarea  name="google_tracking" rows="2" class="<?=$addClass?>" ><?=htmlentities($row['google_tracking'])?></textarea>
				
			</li>
			<?php if($_SESSION['reseller'] || $_SESSION['login_uid']==7){?>
			 <li>
				<label><?php if($_SESSION['login_uid']==7){?>6Qube Backlink<? } else { ?>Global Backlink<? } ?></label>
				<? if($multi_user){ ?>
				<select name="lock_backlink" title="select" class="uniformselect<?=$addClass?>">
					<option value="0">Unlocked</option>
					<option value="1" <?=checkLock('backlink',1)?>>Locked</option>
				</select><div style="margin-top:-10px;"></div>
				<? } ?>
                <div style="color:#888;font-size:12px;padding-bottom:5px;">This is where you can add a global backlink that the user cannot change.  Link to the main site with an anchor text.</div>
				<textarea name="backlink" <?=checkLock('backlink')?> rows="4" class="<?=$addClass?>"><?=htmlentities($row['backlink'])?></textarea>
				<br style="clear:left" />
			</li>
			<? } ?>
           
			<!-- HIDDEN IMAGE CLEAR FIELDS -->
			<li style="display:none;">
			<input type="text" name="logo" value="" id="clear_logo" />
            <input type="text" name="profile_photo" value="" id="clear_profile_photo" />
			</li>
		</ul>
	</form>
	<br />
	
	
	
	<?php if($_SESSION['user_class']== "14" || $_SESSION['user_class']== "132"){ ?>   
	<h2>Addtional HUB Theme Settings</h2>
	<form name="hub" class="jquery_form" id="<?=$_SESSION['current_id']?>">
		<ul>
			<li>
				<label>Affiliate Information</label>
				<input type="text" name="affiliate_info" value="<?=$row['affiliate_info']?>" />
			</li>
			
			<?php if($row['theme'] == "51") {?>
			<li>
				<label>Nopalea ID Number</label>
				<input type="text" name="button_1" title="button_1" value="<?=$row['button_1']?>" />
			</li>
			<?php } ?>
			
		</ul>
	</form>
	<?php } ?>
	
</div>
<!--Start APP Right Panel-->
<div id="app_right_panel">
	<? if(!$_SESSION['theme']){ ?>
	<h2>HUB Settings Videos</h2>
	<div class="app_right_links">
		<ul>
			<li><a href="#hubsettingsVideo" class="trainingvideo" >HUB Settings Video</a></li>
			<div style="display: none;">
				<div id="hubsettingsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hubs-settings-overview.flv" style="display:block;width:640px;height:480px" id="hubsettingsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubsettingsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			
			<li><a href="#buydomainsVideo" class="trainingvideo" >How to Buy Domain Name?</a></li>
			<div style="display: none;">
				<div id="buydomainsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/buydomains-overview.flv" style="display:block;width:640px;height:480px" id="buydomainsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("buydomainsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			
			<li><a href="#domainsVideo" class="trainingvideo" >How to Add a Domain Name?</a></li>
			<div style="display: none;">
				<div id="domainsVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/domains-overview.flv" style="display:block;width:640px;height:480px" id="domainsplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("domainsplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
			
			<li><a href="#hubreqVideo" class="trainingvideo" >What is Required for the Network?</a></li>
			<div style="display: none;">
				<div id="hubreqVideo" style="width:640px;height:480px;overflow:auto;">
					<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
					<a href="http://6qube.com/videos/training/hub-req-overview.flv" style="display:block;width:640px;height:480px" id="hubreqplayer"> </a>
					<!-- this will install flowplayer inside previous A- tag. -->
					<script>
						flowplayer("hubreqplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
					</script>
				</div>
			</div>
		</ul>
		
		<div align="center">
			<a href="http://hubpreview.6qube.com/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		
		</div>
        <br style="clear:both;" />
	</div><!--End APP Right Links - hub settings -->
	<? } ?>
	
	<h2>Helpful Links</h2>
	<div class="app_right_links">
		<ul>
			<li><a class="external" target="_new" href="https://www.google.com/accounts/ServiceLogin?service=sitemaps&passive=true&nui=1&continue=https%3A%2F%2Fwww.google.com%2Fwebmasters%2Ftools%2F&followup=https%3A%2F%2Fwww.google.com%2Fwebmasters%2Ftools%2F&hl=en">Google Webmaster Tools</a></li>
			<li><a href="http://www.picnik.com/" class="external" target="_new">Resize or Crop Images</a></li>
			<? if($row['domain']){ ?>
			<li><a href="hub-sitemapper.php?id=<?=$_SESSION['current_id']?>">Build Sitemap</a></li>
			<? } ?>
			
		</ul>
        <? if($_SESSION['theme']){ ?>
		<div align="center">
        <a href="http://hubpreview.<?=$_SESSION['main_site']?>/i/<?=$row['user_id_created']?>/<?=$_SESSION['current_id']?>/" class="greyButton external" style="margin-left:60px;" target="_blank">Preview Website</a>
		</div>
		<? } ?>
	</div> <!--End APP Right Links - helpful links-->
	<?	
	if(!$_SESSION['theme']){
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
	}
	?>							
</div>
<!--End APP Right Panel-->
<br /><br style="clear:both;" />
<!-- hidden iframe -->
<iframe style="display:none;" id="uploadIFrame" name='uploadIFrame' scrolling="no" frameborder="0" hidefocus="true" src="upload.php"></iframe>
<? } else echo "Error displaying page."; ?>