<?php
session_start();
         require_once( dirname(__FILE__) . '/../../6qube/core.php');
//Authorize access to page
require_once(QUBEADMIN . 'inc/auth.php');
if($_SESSION['reseller'] || $_SESSION['admin']){
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$uid = is_numeric($_GET['uid']) ? $_GET['uid'] : '';
	if($_SESSION['reseller']) $uid = $_SESSION['login_uid'];
	else $uid = $uid;
	
	$query = "SELECT main_site, payments, main_site_id FROM resellers WHERE admin_user = '".$uid."'";
	$resellerInfo = $reseller->queryFetch($query);
	$query = "SELECT theme FROM hub WHERE id = '".$resellerInfo['main_site_id']."'";
	$resellerThemeInfo = $reseller->queryFetch($query);
	$product = $reseller->getResellerProducts($uid, $id, true);
	
	$sel = 'selected="selected"';
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
	
	$('.updateCheckbox2').die();
	$('.updateCheckbox2').live('click', function(){
		var classID = $(this).parent().parent().parent().attr('rel');
		var themeID = $(this).attr('rel');
		var disabled = 0;
		if(!$(this).attr('checked')) disabled =  1;
		var params = { 'themeID': themeID, 'classID': classID, 'disabled': disabled, 'action': 'updateClassThemeAccess' };
		$.post('reseller-functions.php', params, function(data){
			//alert(data);
			var json = $.parseJSON(data);
			if(json.success!=1){
				alert(json.message);
			}
		});
	});
});
</script>
<div id="form_test" style="width:100%;">
<? if($_SESSION['admin']){ ?>
	<a href="inc/forms/edit-user-reseller-products.php?id=<?=$uid?>" class="dflt-link"><img src="img/v3/nav-previous-icon.jpg" alt="Back" /></a><br /><br />
    <? } else { ?>
    <a href="reseller-products.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Forms" /></a><br /><br />
	<? } ?>
<?php
	$thisPage = "main";
	include('edit_reseller_product_nav.php');
?>
<br style="clear:left;" /><br />
	<h1>Edit Product: <?=$product['name']?></h1>
	
	<form name="user_class" class="jquery_form" id="<?=$id?>">
		<ul>
			<h2>Product Info</h2>
           
			<li>
				<label>Signup Link</label>
				<? if(!$resellerThemeInfo['theme'] == "40"){ ?><a href="http://<?=$resellerInfo['main_site']?>/<?=$reseller->convertKeywordUrl($product['name']).$product['id']?>/" target="_blank">http://<?=$resellerInfo['main_site']?><? } ?>/<?=$reseller->convertKeywordUrl($product['name']).$product['id']?>/<? if(!$resellerThemeInfo['theme'] == "40"){ ?></a><? } ?>
			</li><br />
		
			<li>
				<label>Product Name</label>
				<input type="text" name="name" size="30" value="<?=$product['name']?>" />
			</li>
            <? if($product['maps_to']=='27'){ ?>
            <br />
             <li>
				<label>Category Only Network (Leave Blank if All Categories)</label>
               <select name="industry" title="select" class="uniformselect">
					<option value="0">Please choose an industry</option>
                    <option value="Automotive" <? if($product['industry'] == "Automotive") echo 'selected="yes"'; ?> >Automotive</option>
					<option value="Business Services" <? if($product['industry'] == "Business Services") echo 'selected="yes"'; ?> >Business Services</option>
                    <option value="Community and Education" <? if($product['industry'] == "Community and Education") echo 'selected="yes"'; ?> >Community and Education</option>
                    <option value="Computers and Electronics" <? if($product['industry'] == "Computers and Electronics") echo 'selected="yes"'; ?> >Computers and Electronics</option>
                    <option value="Entertainment" <? if($product['industry'] == "Entertainment") echo 'selected="yes"'; ?> >Entertainment</option>
					<option value="Finance" <? if($product['industry'] == "Finance") echo 'selected="yes"'; ?> >Finance</option>
                    <option value="Food and Dining" <? if($product['industry'] == "Food and Dining") echo 'selected="yes"'; ?> >Food and Dining</option>
                    <option value="Health and Personal Care" <? if($product['industry'] == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Healthcare" <? if($product['industry'] == "Healthcare") echo 'selected="yes"'; ?> >Healthcare</option>
                    <option value="Home Based Business" <? if($product['industry'] == "Home Based Business") echo 'selected="yes"'; ?> >Home Based Business</option>
                    <option value="Home Improvement" <? if($product['industry'] == "Home Improvement") echo 'selected="yes"'; ?> >Home Improvement</option>
                    <option value="Health and Personal Care" <? if($product['industry'] == "Health and Personal Care") echo 'selected="yes"'; ?> >Health and Personal Care</option>
                    <option value="Legal" <? if($product['industry'] == "Legal") echo 'selected="yes"'; ?> >Legal</option>
                    <option value="Photography" <? if($product['industry'] == "Photography") echo 'selected="yes"'; ?> >Photography</option>
                    <option value="Real Estate" <? if($product['industry'] == "Real Estate") echo 'selected="yes"'; ?> >Real Estate</option>
                    <option value="Shopping" <? if($product['industry'] == "Shopping") echo 'selected="yes"'; ?> >Shopping</option>
                    <option value="Travel and Recreation" <? if($product['industry'] == "Travel and Recreation") echo 'selected="yes"'; ?> >Travel and Recreation</option>
                    <option value="Web Services" <? if($product['industry'] == "Web Services") echo 'selected="yes"'; ?> >Web Services</option>
				</select>
			</li>
           	<li>
				<label>Single Sub-Category Only Network Product (Leave Blank if All Sub-Categories)</label>
				<?php echo $reseller->displayCategories($product['category']);?>
			</li>
            <li>
				<label>Root Target Keyword (Only if Category Selected)</label>
                <div style="font-size:75%;color:#666;">Local listings will contain city in front of this keyword.  Example: <i>Landscaper will publish Austin Landscaper to listing url. http://domainname.com/<strong>austin-landscaper</strong>/company-name/</i> <br /><br /></div>
				<input type="text" name="cat_key" value="<?=$product['cat_key']?>" />
			</li>
            <? } ?>
			<? if($_SESSION['admin']){ ?>
			<li>
				<label>System Name</label>
				<input type="text" name="type" value="<?=$product['type']?>" />
			</li>
			<? } ?>
			<li>
				<label>Description</label>
                <a href="#" id="1" class="tinymce"><span>Rich Text</span></a>
				<textarea name="description" rows="8" cols="55" id="edtr1"><?=$product['description']?></textarea>
			</li>
	
        
			<li>
				<label>One-time Setup Price</label>
				$<input type="text" name="setup_price" class="product" value="<?=$product['setup_price']?>" style="width:100px;" />
				<? if($product['wholesale']['setup_price']!=0){ ?>
				 (includes $<?=$product['wholesale']['setup_price']?> wholesale fee)
				<? } ?>
			</li>
			<li>
				<label>Monthy Price</label>
				$<input type="text" name="monthly_price" class="product" value="<?=$product['monthly_price']?>" style="width:100px;" />
				<? if($product['wholesale']['monthly_price']!=0){ ?>
				 (includes <?=$product['wholesale']['monthly_price']?> wholesale fee)
				<? } ?>
			</li>
			<? if($product['wholesale']['add_site']>0){ ?>
			<li>
				<label>Add Custom Theme</label>
				$<input type="text" name="add_site" class="product" value="<?=$product['add_site']?>" style="width:100px;" />
				 (includes $<?=$product['wholesale']['add_site']?> wholesale fee)
			</li>
			<? } ?>
			<? if($resellerInfo['payments']=='other'){ ?>
			<br />
			<li>
				<label>Form Submit Button Link (To 3rd Party Payment Gateway)</label>
				<div style="font-size:75%;color:#666;">URL users will be sent to make payment after creating account</div>
				<input type="text" name="pay_link" value="<?=$product['pay_link']?>" /><br /><br />
                
				<p><b>Variables:</b><br />
				You can use the following placeholders in the URL above and they will be automatically<br />replaced with the correct values on the page.  Not all processors require the same information<br />in the same manner, so you probably won't have to use all of these. <a href="#" class="dflt-link" id="showCustomPageTags">Click Here</a>.</p>
                <div id="availableTags" style="display:none;line-height:25px;">
				<i>Payment info:</i><br />
				#dueNow - Amount due right now to start services (setup fee + website + 1st month)<br />
				#monthly - Amount customer will be charged every month<br />
				<i>Customer info:</i><br />
				#firstname - Customer's first name<br />
				#lastname - Customer's last name<br />
				#email - Customer's email address<br />
				#phone - Customer's phone number<br />
				#address- Customer's address<br />
				#city - Customer's city<br />
				#state - Customer's state<br />
				#zip - Customer's zip<br />
				#company - Customer's company name<br />
				<b>Example:</b><br />
				https://paymentprocessor.com/process?businessID=12345&amp;amount1=#dueNow&amp;amount2=#monthly&amp;custname=#firstname%20#lastname&amp;custemail=#email
				<b>Would turn into:</b><br />
				https://paymentprocessor.com/process?businessID=12345&amp;amount1=600&amp;amount2=100&amp;custname=John%20Doe&amp;custemail=johndoe@email.com
                </div>
			</li>
			<? } ?>

		</ul><br style="clear:both;" />
		
		<? if($_SESSION['admin']){ ?>
		<ul>
        	 <li>
				<label>Maps To</label>
				<input type="text" name="maps_to" size="30" value="<?=$product['maps_to']?>" style="width:50px;" />
			</li>
			<h2>Package Limits</h2>
			<div style="display:inline-block;text-align:center;width:100%;">
			<li style="display:inline-block;padding:10px;">
				<label>Campaigns</label>
				<input type="text" name="campaign_limit" value="<?=$product['campaign_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Directory Listings</label>
				<input type="text" name="directory_limit" value="<?=$product['directory_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Hubs</label>
				<input type="text" name="hub_limit" value="<?=$product['hub_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Duplicated Hubs</label>
				<input type="text" name="hub_dupe_limit" value="<?=$product['hub_dupe_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Landing Pages</label>
				<input type="text" name="landing_limit" value="<?=$product['landing_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Social Media Sites</label>
				<input type="text" name="social_limit" value="<?=$product['social_limit']?>" style="width:50px;" />
			</li>
			</div>
			<div style="display:inline-block;text-align:center;width:100%;">
			<li style="display:inline-block;padding:10px;">
				<label>Press Releases</label>
				<input type="text" name="press_limit" value="<?=$product['press_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Articles</label>
				<input type="text" name="article_limit" value="<?=$product['article_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Blogs</label>
				<input type="text" name="blog_limit" value="<?=$product['blog_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Auto Responders</label>
				<input type="text" name="responder_limit" value="<?=$product['responder_limit']?>" style="width:50px;" />
			</li>
			<li style="display:inline-block;padding:10px;">
				<label>Email Sends</label>
				<input type="text" name="email_limit" value="<?=$product['email_limit']?>" style="width:50px;" />
			</li>
			</div>
		</ul>
		<? } ?>
        <? if($product['maps_to']!='27' && $product['maps_to']!='463'){ ?>
		<ul>
			<li rel="<?=$id?>">
				<label>Theme Access</label>
				<?
					//get all themes available to this class
					$query = "SELECT id, name FROM themes WHERE (user_class_access = 0 AND user_id_access = 0) OR user_class_access = '".$id."'";
					$themes = $reseller->query($query);
					//get user class's current allowed themes
					$query = "SELECT disabled_themes FROM user_class WHERE id = '".$id."'";
					$a = $reseller->queryFetch($query, NULL, 1);
					$disabledThemes = explode('::', $a['disabled_themes']);
					while($theme = $reseller->fetchArray($themes, NULL, 1)){
				?>
				<input type="checkbox" rel="<?=$theme['id']?>" class="updateCheckbox2" <? if(!in_array($theme['id'], $disabledThemes)) echo 'checked'; ?> /> <?=$theme['name']?><br /><br />
				<? } ?>
			</li>
		</ul>
        <? } ?>
	</form>
</div>
<br style="clear:both" /><br /><br />
<? } ?>