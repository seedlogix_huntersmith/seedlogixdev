<?php
////////////////////////////////////////////////////////////////////////////////////////
// Class: DbConnector
// Purpose: Connect to a database, MySQL version
///////////////////////////////////////////////////////////////////////////////////////

class DbConnectorRankings {

	var $theQuery;
	var $link;

	//*** Function: DbConnector, Purpose: Connect to the database ***
	function DbConnector(){
		
		// Get the main settings from the array we just loaded
		$host = '50.97.78.208';
		$db = 'simplyra_rankings';
		$user = 'simplyra_admin';
		$pass = '=HeJX.zeqy(3';
		
		// Connect to the database
		if($this->link = @mysqli_connect($host, $user, $pass)){
			@mysqli_select_db($db);
			@register_shutdown_function(array(&$this, 'close'));
		}
	}

	//*** Function: query, Purpose: Execute a database query ***
	function query($query, $custom_db = ''){
		if($custom_db) {
			mysqli_select_db($custom_db);
			return mysqli_query($query, $this->link);
			mysqli_select_db($db);
		}
		else
			return mysqli_query($query, $this->link);
	}

	//*** Function: fetchArray, Purpose: Get array of query results ***
	function fetchArray($result, $assocOnly = '', $assocOnly2 = ''){
		if($assocOnly || $assocOnly2) return mysqli_fetch_array($result, mysqli_ASSOC);
		else return mysqli_fetch_array($result);
	}
	
	//*** Function: queryFetch, Purpose: Execute a database query and return row array ***
	function queryFetch($query, $custom_db = '', $assocOnly = ''){
		if($custom_db){
			mysqli_select_db($custom_db);
			$result = mysqli_query($query, $this->link);
			if($result){
				if($assocOnly) return mysqli_fetch_array($result, mysqli_ASSOC);
				else return mysqli_fetch_array($result);
			}
			else
				return false;
			mysqli_select_db($db);
		}
		else {
			$result = mysqli_query($query, $this->link);
			if($result){
				if($assocOnly) return mysqli_fetch_array($result, mysqli_ASSOC);
				else return mysqli_fetch_array($result);
			}
			else
				return false;
		}
    }
    
	//*** Function: mysqli_num_rows
	function numRows($result){
		return mysqli_num_rows($result);
	}

	//*** Function: close, Purpose: Close the connection ***
	function close(){
		mysqli_close($this->link);
	}
	
	function sanitizeInput($string){
		if(get_magic_quotes_gpc()){  // prevents duplicate backslashes
			$string = stripslashes($string);
		}
		$string = mysqli_real_escape_string($string);
		return $string;
	}
	
	function safe_string_escape($str){
		$len = strlen($str);
		$escapeCount = 0;
		$targetString = '';
		for($offset=0;$offset<$len;$offset++){
			switch($c = $str{$offset}){
				case "'":
					// Escapes this quote only if its not preceded by an unescaped backslash
					if($escapeCount % 2 == 0) $targetString .= "\\";
					$escapeCount = 0;
					$targetString .= $c;
				break;
				case '"':
				// Escapes this quote only if its not preceded by an unescaped backslash
					if($escapeCount % 2 == 0) $targetString .= "\\";
					$escapeCount = 0;
					$targetString .= $c;
				break;
				case '\\':
					$escapeCount++;
					$targetString .= $c;
				break;
				default:
					$escapeCount = 0;
					$targetString .= $c;
			}
		}
		return $targetString;
	}
	
	function getLastId(){
		return mysqli_insert_id();
	}
	
	function passSalt(){
		return 'AA<,sd9dfgd-,d,.D>FG>FG:Ld=d=d0fgo3W#Msjsnxxfg';
	}
	
	function encryptPassword($input){
		$enc1 = md5($input);
		$enc2 = md5($enc1.$this->passSalt());
		return $enc2;
	}
	
	function checkPass($inputPass, $checkPass = '', $userID = ''){
		$salt = $this->passSalt();
		$enc1 = md5($inputPass);
		$enc2 = md5($enc1.$salt);
		if($userID && is_numeric($userID)){
			$query = "SELECT password FROM users WHERE id = '".$userID."'";
			if($result = $this->queryFetch($query)){
				if($enc2 == $result['password']) return true;
				else return false;
			}
			else return false;
		}
		else if($inputPass && $checkPass){
			if($enc2 == $checkPass) return true;
			else return false;
		}
		else return false;
	}
}
?>