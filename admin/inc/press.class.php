<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Press extends Local {

     var $press_id = 0;
	var $press_rows = 0;
	 
	/**********************************************************************/
	// function: Get Presss
	// returns all Press, or Press for specified user
	//
	// Accepted Input: User ID or press id.  Query Limit and campaign id
	/**********************************************************************/
	function getPresss($user_id = '', $limit = '', $press_id = '', $cid = '', $count = '', $limited = ''){
		if($count) $query = "SELECT id FROM press_release";
		else if($limited) $query = "SELECT id, cid, user_id, thumb_id, category, headline, summary, created FROM press_release";
	 	else $query = "SELECT * FROM press_release";
		if($user_id) $query .= " WHERE user_id = '".$user_id."'";
		else if($press_id) $query .= " WHERE id = '".$press_id."'";
		if($cid) $query .= " AND cid = ".$cid;
		$query .= " AND trashed = '0000-00-00'";
		$query .= " ORDER BY timestamp DESC";
		if($limit) $query .= " LIMIT ".$limit;
		$result = $this->query($query);
		$this->press_rows = $this->numRows($result);
		return $result;
	}
	
	function userPressDropDown($user_id, $name, $cid = ''){
		$result = $this->getPresss($user_id, NULL, NULL, $cid);
		
		if($this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$pressArray[$row['id']] = $row['name'];
			}
			return $this->buildSelect($name, $pressArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
		}
		else{
			return 'No Press created yet';
		}

	}
	
	/**********************************************************************/
	// function: Add New Press
	// adds a new Press to the DB, assumes you validate input before passing
	//
	// Accepted Input: User ID and Press Name
	/**********************************************************************/
	function addNewPress($user_id, $name, $cid, $press_limit = '', $bypass = '', $parent_id = ''){
		if(!$press_limit && !$bypass) $press_limit = $this->getLimits($user_id, 'press');
		if(!$bypass){
			$presss = $this->getPresss($user_id, NULL, NULL, $cid, true);
			$presscount = $presss ? $this->numRows($presss) : 0;
		}
		if(!$parent_id) $parent_id = '0';
		
		if($press_limit==-1 || ($press_limit - $presscount)>0 || $bypass){
			$query = "INSERT INTO 
						press_release (
							cid,
							user_id, 
							user_parent_id, 
							name, 
							created
							) 
						VALUES (
							'".$cid."',
							'".$user_id."',
							'".$parent_id."',
							'".$name."', 
							NOW()
							)
						";
			$result = $this->query($query);
			if($result){
				$id = $this->getLastId();
				$this->ANAL_addSite('press_release', $id, 'Press'.$id);
				return $this->pressDashUpdate($id, $name);
			}
			else return false;
	 	}
		else return false;
	}
	
	/**********************************************************************/
	// function: Display Presss (NEW)
	// outputs a set of Press results in new format
	//
	// Accepted Input: Query Limit, Query Offset, Layout Type and User ID
	/**********************************************************************/	 
	function displayPress($limit = '', $offset = '', $main = 0, $user = '', $city = '', $state = '', 
						$category = '', $cid = '', $domain = '', $parent = ''){
		if(!$domain) $domain = 'press.6qube.com';
		if($parent){
			//get parent site (for imgs)
			$query = "SELECT main_site FROM resellers WHERE admin_user = ".$parent;
			$a = $this->queryFetch($query);
			$parent_site = $a['main_site'];
		}
		/*
	 	$query = "SELECT count(*) FROM press_release ";
		$query .= "WHERE trashed = '0000-00-00' 
				 AND headline != '' 
				 AND summary != '' 
				 AND body != '' 
				 AND city != '' 
				 AND state != '' 
				 AND category != '0'
				 AND category != '' 
				 AND user_id != 105";
		if($user) $query .= " AND user_id = {$user}";
		if($city) $query .= " AND city = '{$city}'";
		if($state) $query .= " AND state = '{$state}'";
		if($category) $query .= " AND category LIKE '%{$category}'";
		if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = press_release.user_id) = ".$parent;
		if($cid) $query .= " AND cid = {$cid}";
		$result = $this->query($query); //query the DB
	 	if($result) $this->press_rows = $this->fetchColumn($result); //set $dir_rows
		*/
                
		
			//Query DB for all data matching specific criteria
			$query = "SELECT * FROM press_release ";
			$query .= "WHERE trashed = '0000-00-00'  
					 AND headline != '' 
					 AND summary != '' 
					 AND body != '' 
					 AND city != '' 
					 AND state != '' 
					 AND category != '0'
					 AND category != '' 
					 AND user_id != 105";
			if($user) $query .= " AND user_id = {$user}";
			if($city) $query .= " AND city = '{$city}'";
			if($state) $query .= " AND state = '{$state}'";
			if($category) $query .= " AND category LIKE '%{$category}'";
			if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = press_release.user_id) = ".$parent;
			if($cid) $query .= " AND cid = {$cid}";
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT {$limit}";
			if($offset) $query .= " OFFSET {$offset}";
			$result = $this->query($query); //query the DB
			
                        $this->press_rows   =   $this->numRows($result);
		if($this->press_rows){
			
			while($row = $this->fetchArray($result)){ //if so loop through and display links to each Press
				if($row['network']){
					$html .= Local::displayPress($row, $main, $domain, $parent_site);
				}
			}
			if($main==1) { echo $html; }
			else { echo "<div class=\"blog_right_links\"><ul id=\"popular_posts\">".$html."</ul></div>"; }
			if($main) Local::displayNav3($domain, $this->press_rows, $limit, $offset, $city, $state, $category, $parent_site);
			else echo '<div style="text-align:center;"><a href="http://'.$domain.'" class="neutral-grey" >More Press</a></div>';
		}
		else { //if no items
			if($user){
				echo "This user hasn't created any press releases yet.";
			} else {
				echo 'None.';
			}
		}
	}
	
	/**********************************************************************/
	// function: Update Press
	// updates a press release
	//
	// Accepted Input: POST Array
	/**********************************************************************/	 	
	function updatePress(){
                
		$settings = SystemComponent::getSettings();
		$filepath = $settings['press_img_dir'];
		// Collect form variables and Validate
		$action = $_POST['action'];
		$user_id = $_SESSION['user'];
		$id = $_POST['id'];
		$thumb_id = Validator::validateFile($_FILES['thumbnail']['name'], 'Thumbnail'); //$_FILES['thumbnail']['name'];
		$headline = Validator::validateText($_POST['headline'], 'Headline');
		$summary = Validator::validateText($_POST['summary'], 'Summary');
		$body = $_POST['body'];
		$category = Validator::validateText($_POST['category'], 'Industry');
		$country = $_POST['country'];
		$keywords = $_POST['keywords'];
		$website = $_POST['website'];
		$author = Validator::validateText($_POST['author'], 'Author');
		$email = Validator::validateText($_POST['email'], 'Email');
		$contact = $_POST['contact'];
		$date = date("Y-m-d");
		// Check if any errors were found
		if( Validator::foundErrors() ) {
			$message = '<p class="error"><strong>Errors were found</strong><br />'. Validator::listErrors('<br />') .'</p>';
		}
		else{
			$img_directory = QUBEROOT . 'users/' . UserModel::GetUserSubDirectory($_SESSION['user_id']) .'/press_release/';
			// Upload Files
			if($thumb_id)
				move_uploaded_file($_FILES['thumbnail']['tmp_name'], $img_directory.$thumb_id);
			// Insert or Update DataBase
			$query = "UPDATE press_release ";
			$query .= "SET ";
			$query .= "category='$category', ";
			if($thumb_id) { $query.= "thumb_id='$thumb_id', "; }
			$query.= "headline='$headline', ";
			$query.= "summary='$summary', ";
			$query.= "body='$body', ";
			$query.= "category='$category', ";
			$query.= "country='$country', ";
			$query.= "keywords='$keywords', ";
			$query.= "website='$website', ";
			$query.= "author='$author', ";
			$query.= "email='$email', ";
			$query.= "contact='$contact', ";
			$query.= "timestamp='$date' ";
			$query .= "WHERE id=$id";
			$result = DbConnector::query($query);
			if($result){	
				$message = '<p class="success"><strong>Success!</strong><br />Your Press Release has been successfully edited.';
				$message.= '<br />You can view and edit your press release at the following URL: ';
				$message.= '<a href="'.$settings['press_url'].Local::convertPressUrl($headline, $id).'" class="external">'.$title.'-'.$id.'.htm</a></p>';
				
			}
		}
		return $message;
	}
	
	/**********************************************************************/
	// function: Display Cities
	// displays cities for Press pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesPress($limit = '', $domain = '', $reseller = '', $setCat = '', $state = ''){
		if(!$domain) $domain = 'press.6qube.com';
		
		$query = "SELECT id, COUNT(type), city, state FROM press_release";
		$query .= " WHERE trashed = '0000-00-00' ";
		$query .= " AND headline != '' 
				  AND summary != '' 
				  AND body != '' 
				  AND category != '0'
				  AND category != '' 
				  AND city != ''
				  AND state != ''
				  AND user_id != 105
				  AND network = 1";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		if($setCat) $query .= " AND category LIKE '%".$setCat."'";
		if($state) $query .= " AND state = '".$state."'";
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		 
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$html .= $this->getCityLink($row['city'], $row['state'], $domain, true);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit) $html .= ' <a href="http://'.$domain.'/cities/">more cities...</a> ';
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No cities found.";
		 
	}
	
	/**********************************************************************/
	// function: Display Categories
	// displays categories for Press pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesPress($city, $state, $limit = '', $domain = '', $reseller = ''){	
		if(!$domain) $domain = 'press.6qube.com';
			
		$query = "SELECT id, COUNT(type), state, category FROM press_release";
		$query .= " WHERE trashed = '0000-00-00' ";
		$query .= " AND headline != '' 
				  AND summary != '' 
				  AND body != '' 
				  AND category != '0'
				  AND category != '' 
				  AND city = '".$city."'
				  AND state = '".$state."'
				  AND user_id != 105
				  AND network = 1";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		if($result = $this->query($query)) $numResults = $this->numRows($result);
		if($numResults){
			while($row = $this->fetchArray($result)){
				$a = explode("->", $row['category']);
				$category = $a[1];
				$html .= $this->getCatLink($city, $state, $category, $domain);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit && $numResults>=$limit) 
				$html .= ' <a href="http:/'.$domain.'/'.$this->convertKeywordUrl($city).'categories/">more categories...</a> ';
			else
				$html = substr($html, 0, count($html)-3); //remove trailing |
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No categories found.";
	}
	
	/**********************************************************************/
	// function: Return Latest mysqli_NUM_ROWS
	// returns the last mysqli_num_rows result for latest Press Query 
	//
	// Accepted Input: none
	/**********************************************************************/
	function getPressRows(){ return $this->press_rows; }

}
?>