<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Dir extends Local{
	
	var $dir_id = 0;
	var $dir_rows = 0;
	
	/**********************************************************************/
	// function: Get Directories
	// returns all directories, or directories for specified user, or specified directory ID
	//
	// Accepted Input: User ID, Query Limit, OR Directory ID
	/**********************************************************************/
	function getDirectories($user_id = '', $limit = '', $dir_id = '', $cid = '', $count = '', $cat='', $city='', $state=''){
		if($count) $query = "SELECT `id` FROM `directory`";
		else $query = "SELECT * FROM `directory`";
		if($user_id) $query .= " WHERE `user_id` = '$user_id'";
		else if($dir_id) $query .= " WHERE `id` = '$dir_id'";
		else if($city && $state) $query .= " WHERE `city` = '$city' AND `state` = '$state'";
		if($cid) $query .= " AND `cid` = '$cid'";
		if($cat) $query .= " AND category LIKE '%".$cat."%'";
		$query .= " AND `trashed` = '0000-00-00' ";
		$query .= " ORDER BY id DESC";
		if($limit) $query .= " LIMIT $limit";
		if($result = DbConnector::query($query)) $this->dir_rows = $this->numRows($result); //set $dir_rows
		return $result;
	}
	
	function userDirsDropDown($user_id, $name, $cid = ''){
		$result = $this->getDirectories($user_id, NULL, NULL, $cid);
		
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$directoryArray[$row['id']] = $row['name'];
			}
			return $this->buildSelect($name, $directoryArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
		}
		else
			return 'No directories created yet.';
	}
	
	 /**********************************************************************/
	 // function: Add New Directory
	 // adds a new directory to the DB, assumes you validate input before passing
	 //
	 // Accepted Input: User ID and Directory Name
	 /**********************************************************************/
	 function addNewDirectory($user_id, $name, $cid, $dir_limit = '', $bypass = '', $parent_id = '', $freeListing = '', $class_id = ''){
		if(!$dir_limit && !$bypass) $dir_limit = $this->getLimits($user_id, 'directory');
		if(!$bypass){
			$dirs = $this->getDirectories($user_id, NULL, NULL, $cid, true);
			$dircount = $dirs ? $this->numRows($dirs) : 0;
		}
		if(!$parent_id) $parent_id = '0';
		
		if($dir_limit==-1 || ($dir_limit - $dircount)>0 || $bypass){
			$query = "
				INSERT INTO
					`directory`(
						cid,
						user_id,
						user_parent_id,
						class_id,
						name,
						aid,
						created
					)
				VALUES(
					'".$cid."',
					'".$user_id."',
					'".$parent_id."',
					'".$class_id."',
					'".$name."',
					'1',
					NOW()
				);
			";
			if($result = $this->query($query)){
				$id = $this->getLastId();
				$this->ANAL_addSite('directory', $id, 'Listing'.$id, NULL, 1);
				if(!$bypass) return $this->directoryDashUpdate($id, $name);
				else return $id;
			} else {
				return false;
			}
	 	}
		else return false;
	 }
	 
	 /**********************************************************************/
	 // function: Display Directories (NEW)
	 // outputs a set of directory results in the new format
	 //
	 // Accepted Input: Query Limit and Query Offset
	 /**********************************************************************/	 
	 function displayDirectories($limit = '', $offset = '', $main = 0, $user = '', $city = '', $state = '', 
	 						$category = '', $cid = '', $domain = '', $parent = '', $parent_company = '', $raw = ''){
		if(!$domain) $domain = 'local.6qube.com';
		if($parent){
			//get parent site (for imgs)
			$query = "SELECT main_site FROM resellers WHERE admin_user = ".$parent;
			$a = $this->queryFetch($query);
			$parent_site = $a['main_site'];
		}
		## WRONG::: USE COUNT(*)
		//Query DB for total number of listings that fit criteria.  Only select id to save time
	 	$query = "
	 		SELECT count(*) FROM `directory` 
			WHERE `company_name` != ''
			AND `phone` != ''
			AND `street` != ''
			AND `city` != ''
			AND `state` != ''
			AND `category` != ''
			AND `category` != '0' 
			AND `website_url` != ''
			AND `display_info` != ''
			AND `keyword_one` != ''
			AND `trashed` = '0000-00-00'
	 	";
		if($user) $query .= " AND user_id = $user";
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($category) $query .= " AND category LIKE '%".$category."'";
		if($cid) $query .= " AND cid = '".$cid."'";
		if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = directory.user_id) = ".$parent;
#wrong		if($result = $this->query($query)) $this->dir_rows = $this->numRows($result);
/* right */
                    $result =   $this->query($query);
                    $this->dir_rows =   $this->fetchColumn($result);    // @important @todo this is still wrong, no need to count,
                        // we should execute the query and process an empty result set instead of trying to count first.
		
		if($this->dir_rows){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed
			$query = "
				SELECT 
					id, 
					user_id, 
					company_name, 
					phone, 
					street, 
					city, 
					state, 
					zip, 
					category, 
					photo, 
					website_url, 
					display_info, 
					company_spot, 
					keyword_one, 
					keyword_one_link, 
					keyword_two, 
					keyword_two_link, 
					keyword_three, 
					keyword_three_link, 
					keyword_four, 
					keyword_four_link, 
					keyword_five, 
					keyword_five_link, 
					keyword_six, 
					keyword_six_link, 
					twitter ,
					facebook, 
					myspace, 
					youtube, 
					diigo, 
					technorati, 
					linkedin, 
					blog, 
					pic_one, 
					last_edit,
					created 
				FROM 
					`directory` 
				WHERE `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `state` != ''
				AND `category` != ''
				AND `category` != '0' 
				AND `website_url` != ''
				AND `display_info` != ''
				AND `keyword_one` != ''
				AND `trashed` = '0000-00-00'
			";
			if($user) $query .= " AND user_id = $user";
			if($city) $query .= " AND city = '".$city."'";
			if($state) $query .= " AND state = '".$state."'";
			if($category) $query .= " AND category LIKE '%".$category."%'";
			if($cid) $query .= " AND cid = '".$cid."'";
			if($parent) $query .= " AND (SELECT parent_id FROM users WHERE id = directory.user_id) = ".$parent;
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			$result = DbConnector::query($query);


				while($row = DbConnector::fetchArray($result)){ //if so loop through and display links to each
					$html .= Local::displayDirectory($row, $main, $domain, $parent_site, $parent_company);
				}
				if($main==1){
					echo $html;
					Local::displayNav3($domain, $this->dir_rows, $limit, $offset, $city, $state, $category, $parent_site);
				}
				else{
					echo '<div class="blog_right_links"><ul id="popular_posts">'.$html.'</ul></div>';
					//Local::displayNav($this->dir_rows, $limit, $offset, 'local_display');
					echo '<div style="text-align:center;"><a href="http://'.$domain.'">More from local...</a></div>';
				}

		}
		else { //if no directories
			if($user){
				echo "This user hasn't created any local listings yet.";
			} else {
				echo 'None.';
			}
		}
	}
	 
	/**********************************************************************/
	// function: Display Directory Photos
	// returns up to six photos
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function displayDirectoryPhotos($row, $domain = ''){
		if(!$domain) $domain = '6qube.com';
		$p = 0;
                $usersubdir =   UserModel::GetUserSubDirectory($row['user_id']);
		if($row['pic_one']){
			$html .= '<a title="'.$row['pic_one_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_one'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_one'].'\')"></div></a>';
			$p++;
		}
		if($row['pic_two']){
			$html .= '<a title="'.$row['pic_two_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_two'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_two'].'\')"></div></a>';
			$p++;
		}
		if($row['pic_three']){
			$html .= '<a title="'.$row['pic_three_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_three'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_three'].'\')"></div></a>';
			$p++;
		}
		if($row['pic_four']){
			$html .= '<a title="'.$row['pic_four_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_four'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_four'].'\')"></div></a>';
			$p++;
		}
		if($row['pic_five']){
			$html .= '<a title="'.$row['pic_five_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_five'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_five'].'\')"></div></a>';
			$p++;
		}
		if($row['pic_six']){
			$html .= '<a title="'.$row['pic_six_title'].'" href="http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_six'].'" rel="portfolio" ><div class="diricon bloglistimg" style="background-image:url(\'http://'.$domain.'/users/'.$usersubdir.'/directory/'.$row['pic_six'].'\')"></div></a>';
			$p++;
		}
		 
		if($p > 0){
			echo $html;
		} else {
			echo "This directory listing doesn't have any photos yet.";
		}		 
	}
	 
	/**********************************************************************/
	// function: Calculate and return percentage of directory listing completed
	// returns a number, 0 - 100 depending on level of directory listing completion
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function getListingCompletion($row){
		$percentComplete = 0;
		
		if($row['company_name'] && $row['phone'] && $row['street'] && $row['city'] && $row['state'] && $row['category'] && $row['website_url'] && $row['display_info'] && $row['keyword_one']){
			$percentComplete = 25;
			
			if($row['company_spot']){
				$percentComplete = 50;	
				
				if($row['pic_one']){
					$percentComplete = 75;
					
					if($row['keyword_one'] && $row['keyword_one_link'] && $row['keyword_two'] && $row['keyword_two_link'] && $row['keyword_three'] && $row['keyword_three_link'] && $row['keyword_four'] && $row['keyword_four_link'] && $row['keyword_five'] && $row['keyword_five_link'] && $row['keyword_six'] && $row['keyword_six_link']){
						$percentComplete = 95;
						
						if($row['twitter'] || $row['facebook'] || $row['myspace'] || $row['youtube'] || $row['diigo'] || $row['technorati'] || $row['linkedin'] || $row['blog']){
							$percentComplete = 100;
						}
						
					}
					
				}
				
			}
				
		}
		
		return $percentComplete;
	}
	
	/**********************************************************************/
	// function: Gets a Users Hubs
	// returns all hubs, or hubs for specified user
	//
	// Accepted Input: User ID and Query Limit
	/**********************************************************************/
	function displayUserHubs($user_id, $limit = ''){
		$query = "SELECT * FROM hub";
		if($user_id) $query .= " WHERE user_id = '$user_id' ";
		$query .= " ORDER BY id DESC";
		if($limit) $query .= " LIMIT $limit";
		$result = DbConnector::query($query);
		if($this->numRows($result)){
			while($row = DbConnector::fetchArray($result)){ //if so loop through and display links to each Hub
				$html .= Local::displayHub($row);
			}
		}
		else{
			$html = '<p>Has not created a Hub yet.</p>';
		}
		
		echo $html;
	}
	 
	/**********************************************************************/
	// function: Gets a Users Press Releases
	// returns all hubs, or hubs for specified user
	//
	// Accepted Input: User ID and Query Limit
	/**********************************************************************/
	function displayUserPress($user_id, $limit = ''){
		$query = "SELECT * FROM press_release";
		if($user_id) $query .= " WHERE user_id = '$user_id' ";
		$query .= " ORDER BY id DESC";
		if($limit) $query .= " LIMIT $limit";
		$result = DbConnector::query($query);
		if($this->numRows($result)){
			while($row = DbConnector::fetchArray($result)){ //if so loop through and display links to each Hub
				$html .= Local::displayPress($row);
			}
		}
		else{
			$html = '<p>Has not created a Press Release.</p>';
		}
		echo $html;
	}	
	
	/**********************************************************************/
	// function: Gets a Users Articles 
	// displays articles for specified user
	//
	// Accepted Input: User ID and Query Limit
	/**********************************************************************/
	function displayUserArticles($user_id, $limit = ''){
	 	$query = "SELECT * FROM articles";
		if($user_id) $query .= " WHERE user_id = '$user_id' ";
		$query .= " ORDER BY id DESC";
		if($limit) $query .= " LIMIT $limit";
		$result = DbConnector::query($query);
		if($this->numRows($result)){
			while($row = DbConnector::fetchArray($result)){ //if so loop through and display links to each Hub
				$html .= Local::displayArticle($row);
			}
		}
		else{
			$html = '<p>Has not created an Article.</p>';
		}
		echo $html;
	 }
	 
	/**********************************************************************/
	// function: Display Cities
	// displays cities for Local pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesLocal($limit = '', $domain = '', $reseller = '', $setCat = '', $state = ''){
		if(!$domain) $domain = 'local.6qube.com';
		
		
		if($reseller){ 	
			$dataCheck = "SELECT master_network_data FROM resellers WHERE admin_user = '".$reseller."'";
			$masterdata = $this->queryFetch($dataCheck);		
			if($masterdata['master_network_data']==1) $reseller = false;
		}
		
		$query = "SELECT id, COUNT(type), city, state FROM directory";
		$query .= " WHERE `trashed` = '0000-00-00'";
		$query .= " AND `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `state` != ''
				AND `category` != '0'
				AND `category` != ''
				AND `website_url` != ''
				AND `photo` != ''
				AND `display_info` != ''
				AND `keyword_one` != ''";	
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		if($setCat) $query .= " AND category LIKE '%".$setCat."%'";
		if($state) $query .= " AND state = '".$state."'";
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		 
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$html .= $this->getCityLink($row['city'], $row['state'], $domain, true);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit) $html .= ' <a href="http://'.$domain.'/cities/">more cities...</a> ';
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No cities found.";
		 
	}
	 
	/**********************************************************************/
	// function: Display Categories
	// displays categories for Local pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesLocal($city, $state, $limit = '', $domain = '', $reseller = '', $setCat = ''){
		if(!$domain) $domain = 'local.6qube.com';
		
		if($reseller){ 	
			$dataCheck = "SELECT master_network_data FROM resellers WHERE admin_user = '".$reseller."'";
			$masterdata = $this->queryFetch($dataCheck);		
			if($masterdata['master_network_data']==1) $reseller = false;
		}
		
		$query = "SELECT id, COUNT(type), state, category FROM directory";
		$query .= " WHERE `trashed` = '0000-00-00'";
		$query .= " AND `company_name` != ''
				AND `phone` != ''
				AND `street` != ''
				AND `city` != ''
				AND `state` != ''
				AND `category` != '0'
				AND `category` != ''
				AND `website_url` != ''
				AND `display_info` != ''
				AND `photo` != ''
				AND `keyword_one` != ''";
		$query .= " AND city = '".$city."'";
		$query .= " AND state = '".$state."'";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		if($setCat) $query .= " AND category LIKE '%".$setCat."%'";
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		if($result = $this->query($query)) $numResults = $this->numRows($result);
		if($numResults){
			while($row = $this->fetchArray($result)){
				$a = explode("->", $row['category']);
				$category = $a[1];
				$html .= $this->getCatLink($city, $state, $category, $domain);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit && $numResults>=$limit) 
				$html .= ' <a href="http://'.$domain.'/'.$this->convertKeywordUrl($city).'categories/">more categories...</a> ';
			else
				$html = substr($html, 0, count($html)-3); //remove trailing |
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No categories found.";
	}
	
	/**********************************************************************/
	// function: Return Latest mysqli_NUM_ROWS
	// returns the last mysqli_num_rows result for latest Directory Query 
	//
	// Accepted Input: none
	/**********************************************************************/
	function getDirectoryRows(){ return $this->dir_rows; }
	
}
?>