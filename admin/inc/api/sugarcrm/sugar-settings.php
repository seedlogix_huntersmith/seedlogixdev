<?php	
	if($row['user_id']==$_SESSION['user_id']){
?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="crm-integration.php?mode=sugar&id=<?=$id?>" <? if($thisPage=="sugarcrm") echo $curr; ?>>SugarCRM</a>
		</li>
		<li class="last">
			<a href="crm-integration.php?mode=salesforce&id=<?=$id?>" <? if($thisPage=="salesforce") echo $curr; ?>>Salesforce.com</a>
		</li>
	</ul>
</div>
<br style="clear:left;" /><br />
<div id="form_test">
	<h2>Edit SugarCRM Account Settings</h2>

	<form name="sugarcrm" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>SugarCRM Username</label>
				<input type="text" name="sugar_username" value="<?=$row['sugar_username']?>" />
			</li>
			<li>
				<label>SugarCRM Password</label>
				<input type="password" name="sugar_password" value="<?=$row['sugar_password']?>" />
			</li>
			<li>
				<label>SugarCRM Install Path</label>
				<input type="text" name="sugar_link" value="<?=$row['sugar_link']?>" />
			</li>
            <li>
				<label>SugarCRM NuSOAP Files Absolute Path</label>
				<input type="text" name="sugar_absolute" value="<?=$row['sugar_absolute']?>" />
			</li>
            <li>
				<label>Default Assigned To</label>
				<input type="text" name="sugar_assigned_to" value="<?=$row['sugar_assigned_to']?>" />
			</li>
			
			
		</ul>
	</form>

	<br /><br />
</div>
<br style="clear:both;" /><br />

<? } else echo "Error displaying page."; ?>