<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/contacts.class.php');
$contacts = new Contacts();

$assigned_user_id = $_POST['assigned_user_id'];
$contactID = $_POST['contactID'];


$row = $contacts->getContacts($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID);
//get notes
$notesView = $contacts->getNotes($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID, NULL);

//get sugar settings
$sugar = $contacts->getSugarSettings($_SESSION['user_id'], $_SESSION['campaign_id']);


	// Let SugarCRM know that this is a valid file
	define('sugarEntry', TRUE);
	
	//Use the NuSOAP files included with SugarCRM to authenticate against the SOAP APO
	//Setup NuSOAP session
	require_once(''.$sugar['sugar_absolute'].'');
	
	$soapclient = new nusoapclient( $sugar['sugar_link'] . 'soap.php?wsdl',true);
	
	 $err = $soapclient->getError();
	if ($err) {
		echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	}
	 

	
	
	 $user_auth = array(
					 'user_auth' => array(
						   'user_name' => $sugar['sugar_username'],
						   'password' => md5($sugar['sugar_password']),
						   'version' => '0.1'
						   ),
					 'application_name' => 'soapleadcapture');
	
	
	//login
	 $result_array = $soapclient->call('login',$user_auth);
	  $err = $soapclient->getError();
	if ($err) {
		echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	}
	
	
	 $session_id =  $result_array['id'];
	
	 $user_guid = $soapclient->call('get_user_id',$session_id);
	   $err = $soapclient->getError();
	if ($err) {
		echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	}
		
	 // create lead
	 $set_entry_params = array(
						   'session' => $session_id,
						   'module_name' => 'Leads',
						   'name_value_list'=>array(
							   array('name'=>'first_name','value'=>$row['first_name']),
							   array('name'=>'last_name','value'=>$row['last_name']),
							   array('name'=>'status', 'value'=>'New'),
							   array('name'=>'title', 'value'=>$row['title']),
							   array('name'=>'phone_work', 'value'=>$row['phone']),
							   array('name'=>'email1', 'value'=>$row['email']),
							   array('name'=>'primary_address_street', 'value'=>$row['address']),
							   array('name'=>'primary_address_city', 'value'=>$row['city']),
							   array('name'=>'primary_address_state', 'value'=>$row['state']),
							   array('name'=>'primary_address_postalcode', 'value'=>$row['zip']),
							   array('name'=>'account_name','value'=>$row['company']),
							   array('name'=>'lead_source','value'=>$row['contact_source']),
							   array('name'=>'description','value'=>$row['lead_data']),
							   array('name'=>'team_id', 'value'=> '1'),
							   array('name'=>'assigned_user_id', 'value'=>$user_guid)));
							   
	
		$result = $soapclient->call('set_entry',$set_entry_params);
		
		// send response back so user is not waiting
		
		if($result){

				$html = 
				'<br style="clear:both;" /><br />
				<div class="sugar_response">Contact Successfully Pushed to SugarCRM</div>';
				
				$response['success']['id'] = '1';
				$response['success']['container'] = '#sugar_success .sugar_container';
				$response['success']['message'] = $html;
	
		}
		echo json_encode($response);
		
		// loop and attach notes
		
		if($notesView && is_array($notesView)){
			foreach($notesView as $noteid=>$note){
				
				$lead_id = $result['id'];
				
				$note=array(
					'session' => $session_id,
					'module_name' => 'Notes',
					'name_value_list'=>array(
						  array('name'=>'name','value'=>$note['subject']),
						array('name'=>'description','value'=>strip_tags($note['note'])),
						array('name'=>'date_entered','value'=>$note['last_updated']),
						array('name'=>'parent_type','value'=>'Leads'),
						array('name'=>'parent_id','value'=>$lead_id)
				));
				
				$create_note = $soapclient->call('set_entry', $note); 
	
			}
		}
		
		
		
	  
	  $err = $soapclient->getError();
	if ($err) {
		echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	}
		
	
?>
