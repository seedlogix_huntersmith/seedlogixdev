<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

$thisPage = "sugarcrm";
$curr = 'class="current"';

require_once(QUBEADMIN . 'inc/contacts.class.php');
$contacts = new Contacts();

$user_id = $_SESSION['user_id'];
$query = "SELECT * FROM sugarcrm WHERE user_id = '".$user_id."' 
		  AND cid = '".$_SESSION['campaign_id']."' LIMIT 1";

$row = $contacts->queryFetch($query);

$id = $row['id'];

if($id){
		  include(QUBEADMIN . 'inc/api/sugarcrm/sugar-settings.php');
	} 
	else{

?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="crm-integration.php?mode=sugar&id=<?=$id?>" <? if($thisPage=="sugarcrm") echo $curr; ?>>SugarCRM</a>
		</li>
		<li class="last">
			<a href="crm-integration.php?mode=salesforce&id=<?=$id?>" <? if($thisPage=="salesforce") echo $curr; ?>>Salesforce.com</a>
		</li>
	</ul>
</div>
<br style="clear:left;" /><br />
<h2>Activate SugarCRM</h2>
<script type="text/javascript">
		$('#createDir').delegate('#addSugarCRM', 'submit', function(event){
		event.stopPropagation();		
		
		$('#wait').show();
		$.ajax({
			type:'POST',
			url:'inc/api/sugarcrm/sugar_save.php',
			data: $('#addSugarCRM').serialize(),
			success: function(d){
				if(d == "ok"){
				  alert('SugarCRM Account added successfully!');
				}
				else{
				  alert('There was a problem with your submission, please contact support.');
				}
				$.get('inc/api/sugarcrm/sugar-activate.php', function(r){
				  $('#ajax-load').html(r);
				  $('#wait').hide();
				});
			}
		});
		return false;		
	});
</script>
<div id="createDir">
                    <h3>SugarCRM Username</h3><br />
	<form id="addSugarCRM" method="post" action="" class="ajax">
		<input type="text" name="sugar_username" value="">
        <input type="hidden" name="user_id" value="<?=$user_id?>" />
        <input type="hidden" name="cid" value="<?=$_SESSION['campaign_id']?>" />
		<input type="submit" value="Activate Account" name="submit" />
	</form>
</div>
<?php
}
?>
