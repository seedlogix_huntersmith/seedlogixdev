<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

$thisPage = "salesforce";
$curr = 'class="current"';

require_once(QUBEADMIN . 'inc/contacts.class.php');
$contacts = new Contacts();

$user_id = $_SESSION['user_id'];
$query = "SELECT * FROM salesforce WHERE user_id = '".$user_id."' 
		  AND cid = '".$_SESSION['campaign_id']."' LIMIT 1";

$row = $contacts->queryFetch($query);

$id = $row['id'];

if($id){
		  include(QUBEADMIN . 'inc/api/salesforce/salesforce-settings.php');
	} 
	else{

?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="crm-integration.php?mode=sugar&id=<?=$id?>" <? if($thisPage=="sugarcrm") echo $curr; ?>>SugarCRM</a>
		</li>
		<li class="last">
			<a href="crm-integration.php?mode=salesforce&id=<?=$id?>" <? if($thisPage=="salesforce") echo $curr; ?>>Salesforce.com</a>
		</li>
	</ul>
</div>
<br style="clear:left;" /><br />
<h2>Activate Salesforce.com</h2>
<script type="text/javascript">
		$('#createDir').delegate('#addSalesforceCRM', 'submit', function(event){
		event.stopPropagation();		
		
		$('#wait').show();
		$.ajax({
			type:'POST',
			url:'inc/api/salesforce/salesforce_save.php',
			data: $('#addSalesforceCRM').serialize(),
			success: function(d){
				if(d == "ok"){
				  alert('Salesforce.com Account added successfully!');
				}
				else{
				  alert('There was a problem with your submission, please contact support.');
				}
				$.get('inc/api/salesforce/salesforce-activate.php', function(r){
				  $('#ajax-load').html(r);
				  $('#wait').hide();
				});
			}
		});
		return false;		
	});
</script>
<div id="createDir">
                    <h3>Salesforce.com Username</h3><br />
	<form id="addSalesforceCRM" method="post" action="" class="ajax">
		<input type="text" name="salesforce_username" value="">
        <input type="hidden" name="user_id" value="<?=$user_id?>" />
        <input type="hidden" name="cid" value="<?=$_SESSION['campaign_id']?>" />
		<input type="submit" value="Activate Account" name="submit" />
	</form>
</div>
<?php
}
?>
