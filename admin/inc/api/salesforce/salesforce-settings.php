<?php	
	if($row['user_id']==$_SESSION['user_id']){
?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="crm-integration.php?mode=sugar&id=<?=$id?>" <? if($thisPage=="sugarcrm") echo $curr; ?>>SugarCRM</a>
		</li>
		<li class="last">
			<a href="crm-integration.php?mode=salesforce&id=<?=$id?>" <? if($thisPage=="salesforce") echo $curr; ?>>Salesforce.com</a>
		</li>
	</ul>
</div>
<br style="clear:left;" /><br />
<div id="form_test">
	<h2>Edit Salesforce.com Account Settings</h2>

	<form name="salesforce" class="jquery_form" id="<?=$id?>">
		<ul>
			<li>
				<label>Salesforce.com Username</label>
				<input type="text" name="salesforce_username" value="<?=$row['salesforce_username']?>" />
			</li>
			<li>
				<label>Salesforce.com Password</label>
				<input type="password" name="salesforce_password" value="<?=$row['salesforce_password']?>" />
			</li>
			<li>
				<label>Salesforce.com Token</label>
				<input type="text" name="salesforce_token" value="<?=$row['salesforce_token']?>" />
			</li>
			
			
		</ul>
	</form>

	<br /><br />
</div>
<br style="clear:both;" /><br />

<? } else echo "Error displaying page."; ?>