<?php
	session_start();
         require_once( dirname(__FILE__) . '/../../../6qube/core.php');

//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/contacts.class.php');
$contacts = new Contacts();

$assigned_user_id = $_POST['assigned_user_id'];
$contactID = $_POST['contactID'];

$row = $contacts->getContacts($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID);

$leadData = explode('[==]', $row['lead_data']);

//get notes
$notesView = $contacts->getNotes($_SESSION['user_id'], $_SESSION['campaign_id'], $contactID, NULL);

//get sugar settings
$salesforce = $contacts->getSalesforceSettings($_SESSION['user_id'], $_SESSION['campaign_id']);

	define('SALESFORCE_USER', ''.$salesforce['salesforce_username'].'');
	define('SALESFORCE_PASS', ''.$salesforce['salesforce_password'].'');
	define('SALESFORCE_TOKEN', ''.$salesforce['salesforce_token'].'');
	define('SALESFORCE_WSDL', QUBEADMIN . 'inc/api/salesforce/soapclient/partner.wsdl.xml');
	
	require_once(QUBEADMIN . 'inc/api/salesforce/soapclient/SforcePartnerClient.php');
	
	/**
	 * explicitly turn off WSDL caching
	 * there is a bug in PHP with this setting in php.ini
	 * http://bugs.php.net/bug.php?id=41665
	 * so it's safest to set it in the PHP source
	 *
	 * This param should take an int, not a string like most examples use
	 * see http://us3.php.net/manual/en/soap.configuration.php
	*/
	ini_set('soap.wsdl_cache_enabled', 0);
	
	// instantiate a new Salesforce Partner object
	$crmHandle = new SforcePartnerClient();
	
	// instantiate a SOAP connection to Salesforce
	try {
	  $crmHandle->createConnection(SALESFORCE_WSDL);
	} catch (Exception $e) {
	  // handle exception - did you set the WSDL path above?
	  // we may also be in a Salesforce outage right now
	}
	
	// log in to Salesforce
	try {
	  $crmHandle->login(SALESFORCE_USER, SALESFORCE_PASS . SALESFORCE_TOKEN);
	} catch (Exception $e) {
	  // handle exception - did you modify the credentials above to your own?
	}
	
	// create our lead
	$lead = new sObject();
	$lead->type = 'Lead';
	$lead->fields = array('FirstName' => ''.$row['first_name'].'',
						  'LastName' => ''.$row['last_name'].'',
						  'Email' => ''.$row['email'].'',
						  'Phone' => ''.$row['phone'].'',
						  'Title' => ''.urlencode($row['title']).'',
						  'Street' => ''.$row['address'].'',
						  'City' => ''.$row['city'].'',
						  'State' => ''.$row['state'].'',
						  'PostalCode' => ''.$row['zip'].'',
						  'Website' => ''.$row['website'].'',	
						  'Status' => 'Open - Not Contacted',
						  'Company' => ''.$row['company'].'');
	
	// create the lead
	// $lead must be wrapped in an array, as create() can create
	// many objects with a single API call
	$result = $crmHandle->create(array($lead), 'Lead');
	
	if($result){

				$html = 
				'<br style="clear:both;" /><br />
				<div class="sugar_response">'.$lead_id.' Contact Successfully Pushed to Salesforce.com</div>';
				
				$response['success']['id'] = '1';
				$response['success']['container'] = '#salesforce_success .salesforce_container';
				$response['success']['message'] = $html;
	
	}
	echo json_encode($response);
	
	// loop and attach notes
		
		if($notesView && is_array($notesView)){
			foreach($notesView as $noteid=>$note){
				
				$lead_id = $result[0]->id;
				
				// create our lead
				$call = new sObject();
				$call->type = 'Task';
				$call->fields = array('ActivityDate' => ''.$note['last_updated'].'',
									  'Description' => ''.strip_tags($note['note']).'',
									  'Status' => 'Completed',
									  'Priority' => 'High',
									  'Subject' => ''.$note['subject'].'',
									  'WhoId' => ''.$lead_id.'');
				
				// create the lead
				// $lead must be wrapped in an array, as create() can create
				// many objects with a single API call
				$call_result = $crmHandle->create(array($call), 'Task');
	
			}
		}
		
		if($row['lead_data']){ 
			
			$lead_results = '';
						foreach($leadData as $key=>$value){
						 $fieldData = explode('|-|', $value);
						 $lead_results .= ''.$fieldData[1].' : '.$fieldData[2].' 
						 
						 ';
						}	
						
			$lead_id = $result[0]->id;		
			// create our lead
			$leadmessage = new sObject();
			$leadmessage->type = 'Task';
			$leadmessage->fields = array('ActivityDate' => ''.$row['created'].'',
								  'Description' => ''.strip_tags($lead_results).'',
								  'Status' => 'Completed',
								  'Priority' => 'High',
								  'Subject' => 'Lead Form Response',
								  'WhoId' => ''.$lead_id.'');
			
			// create the lead
			// $lead must be wrapped in an array, as create() can create
			// many objects with a single API call
			$response_result = $crmHandle->create(array($leadmessage), 'Task');
		}
	

		
		

?>
