<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/AuthnetARB.class.php';
require_once QUBEADMIN . 'inc/AuthnetCIM.class.php';
require_once QUBEADMIN . 'inc/AuthnetAIM.class.php';
require_once QUBEADMIN . 'inc/db_connector.php';

class Authnet extends DbConnector {
	
///////////////////////////////////////////////////////
// PROFILE FUNCTIONS
///////////////////////////////////////////////////////
	//function to set up a new CIM profile under specified API account
	function setupCIMProfile($uid, $reseller = '', $apiLogin = '', $apiAuth = '', $sixqube = '', $bypassCheck = ''){
		if($uid && is_numeric($uid)){
			//get user info
			$query = "SELECT parent_id, billing_id, username FROM users WHERE id = '".$uid."'";
			$user = $this->queryFetch($query);
			if(!$bypassCheck){
				//check to see if they're already set up
				if(!$user['billing_id']) $continue = true;
				else $continue = false;
			}
			else $continue = true;
			if($continue){
				if(!$apiLogin && !$apiAuth){
					$api = $this->getAuthnetAPI();
					$apiLogin = $api['apiLogin'];
					$apiAuth = $api['transKey'];
				}
				$cim = new AuthnetCIM($apiLogin, $apiAuth);
				
				//reseller or customer
				if($reseller){
					$description = '6Qube Reseller Billing';
					$custID = 'reseller'.$uid;
				}
				else if($sixqube){
					$description = '6Qube User #'.$uid;
					$custID = '6q_user'.$uid;
				}
				else {
					//if reseller client, get reseller name
					$query = "SELECT company FROM resellers WHERE admin_user = '".$user['parent_id']."'";
					$a = $this->queryFetch($query);
					$description = $a['company'].' User #'.$uid;
					$custID = 'user'.$uid;
				}
				
				//set params and execute CIM function
				$cim->setParameter('email', $user['username']);
				$cim->setParameter('description', $description);
				$cim->setParameter('merchantCustomerId', $custID);
				$cim->createCustomerProfile();
				if($cim->isSuccessful()){
					$profile_id = $cim->getProfileID();
					//update users table with new ID
					$query = "UPDATE users SET billing_id = '".$profile_id."' WHERE id = '".$uid."'";
					if($this->query($query)){
						$return['success'] = true;
						//weird glitch fix
						$return['profile_id'] = $profile_id ? $profile_id : $cim->getProfileID();
					}
					else {
						$return['success'] = false;
						$return['message'] = 'Error updating users table with id: ';//.mysqli_error();
					}
				}
				else {
					$return['success'] = false;
					$return['message'] = 'Error creating profile: '.$cim->getResponseSummary();
				}
				$cim->__destruct();
			} //end if($continue)
			else {
				//if user already has a CIM profile, return the ID
				$return['success'] = true;
				$return['profile_id'] = $user['billing_id'];
			}
		} //end if($uid && is_numeric($uid))
		else {
			$return['success'] = false;
			$return['message'] = 'Invalid UID';
		}
		
		return $return;
	} //end setupCIMProfile()
	
	//function to create a new payment profile under a CIM account
	function setupPaymentProfile($uid, $cimID, $params, $apiLogin = '', $apiAuth = '', $noCVV = 0){
		if(!$apiLogin && !$apiAuth){
			$api = $this->getAuthnetAPI();
			$apiLogin = $api['apiLogin'];
			$apiAuth = $api['transKey'];
		}
		$cim = new AuthnetCIM($apiLogin, $apiAuth);
		
		//set params and submit request
		$cim->setParameter('customerProfileId', $cimID);
		$cim->setParameter('billToFirstName', $params['firstname']);
		$cim->setParameter('billToLastName', $params['lastname']);
		$cim->setParameter('billToCompany', $params['company']);
		$cim->setParameter('billToAddress', $params['address']);
		$cim->setParameter('billToCity', $params['city']);
		$cim->setParameter('billToState', $params['state']);
		$cim->setParameter('billToZip', $params['zip']);
		$cim->setParameter('billToCountry', 'US');
		$cim->setParameter('billToPhoneNumber', $params['phone']);
		$cim->setParameter('cardNumber', $params['cardnumber']);
		$cim->setParameter('expirationDate', $params['expyear'].'-'.$params['expmonth']);
		$cim->createCustomerPaymentProfile();
		
		if($cim->isSuccessful()){
			$return['success'] = true;
			$return['payment_profile_id'] = $payment_profile_id = $cim->getPaymentProfileId();
			
			//update users table with new payment profile id and cvv for future CIM transactions
			if($noCVV) $cvv = '';
			else $cvv = "$params[cvv]";
			$query = "UPDATE users 
					SET billing_id2 = '".$return['payment_profile_id']."', billing_id3 = '".$cvv."' 
					WHERE id = '".$uid."'";
			$this->query($query);
		}
		else {
			$return['success'] = false;
			$return['message'] = $cim->getResponseSummary();
		}
		
		$cim->__destruct();
		return $return;
	} //end setupPaymentProfile()
	
	//update a user's payment profile and keep same ID
	function updatePaymentProfile($uid, $cimID, $cimID2, $params, $apiLogin = '', $apiAuth = ''){
		if(!$apiLogin && !$apiAuth){
			$api = $this->getAuthnetAPI();
			$apiLogin = $api['apiLogin'];
			$apiAuth = $api['transKey'];
		}
		$cim = new AuthnetCIM($apiLogin, $apiAuth);
		
		//set params and submit request
		$cim->setParameter('customerProfileId', $cimID);
		$cim->setParameter('customerPaymentProfileId', $cimID2);
		$cim->setParameter('firstName', $params['firstname']);
		$cim->setParameter('lastName', $params['lastname']);
		$cim->setParameter('company', $params['company']);
		$cim->setParameter('address', $params['address']);
		$cim->setParameter('city', $params['city']);
		$cim->setParameter('state', $params['state']);
		$cim->setParameter('zip', $params['zip']);
		$cim->setParameter('country', 'US');
		$cim->setParameter('phoneNumber', $params['phone']);
		$cim->setParameter('cardNumber', $params['cardnumber']);
		$cim->setParameter('expirationDate', $params['expyear'].'-'.$params['expmonth']);
		$cim->updateCustomerPaymentProfile();
		
		if($cim->isSuccessful()){
			$return['success'] = true;
			
			//update users table with new cvv
			$query = "UPDATE users 
					SET billing_id3 = '".$params['cvv']."' 
					WHERE id = '".$uid."'";
			$this->query($query);
		}
		else {
			$return['success'] = false;
			$return['message'] = $cim->getResponseSummary();
		}
		
		$cim->__destruct();
		return $return;
	}
	
	//function to retrieve details of a CIM payment profile
	function getPaymentProfile($uid, $apiLogin = '', $apiAuth = ''){
		$query = "SELECT billing_id, billing_id2 FROM users WHERE id = '".$uid."'";
		$a = $this->queryFetch($query);
		//make sure they have a payment profile set up
		if($a['billing_id2']){
			if(!$apiLogin && !$apiAuth){
				//if no API info provided use 6qube's
				$api = $this->getAuthnetAPI();
				$apiLogin = $api['apiLogin'];
				$apiAuth = $api['transKey'];
			}
			
			$cim = new AuthnetCIM($apiLogin, $apiAuth);
			$cim->setParameter('customerProfileId', $a['billing_id']);
			$cim->setParameter('customerPaymentProfileId', $a['billing_id2']);
			$cim->getCustomerPaymentProfile();
			
			if($cim->isSuccessful()){
				$return['success'] = true;
				$return['info'] = $cim->getProfileInfo();
			}
			else {
				$return['success'] = false;
				$return['message'] = 'Error retrieving profile info: '.$cim->getResponseSummary();
			}
		}
		else {
			$return['success'] = false;
			$return['message'] = 'No profile set up yet.';
		}
		
		if($cim) $cim->__destruct();		
		return $return;
	} //end getPaymentProfile();

	
///////////////////////////////////////////////////////
// PAYMENT/TRANSACTION FUNCTIONS
///////////////////////////////////////////////////////
	//Function to charge a card one time for specified amount
	//ALL $params fields below must be present
	function chargeAIM($params, $apiLogin = '', $apiAuth = ''){
		if(!$apiLogin && !$apiAuth){
			//if no API info provided use 6qube's
			$api = $this->getAuthnetAPI();
			$apiLogin = $api['apiLogin'];
			$apiAuth = $api['transKey'];
		}
		try {
			$payment = new AuthnetAIM($apiLogin, $apiAuth);
			$payment->setTransaction($params['cardnumber'], $params['expmonth'].'-'.$params['expyear'], 
								$params['amount'], '$params[cvv]', $params['invoiceID']);
			$payment->setParameter("x_cust_id", $params['custID']);
			$payment->setParameter("x_customer_ip", $params['ip']);
			$payment->setParameter("x_email", $params['email']);
			$payment->setParameter("x_email_customer", FALSE);
			$payment->setParameter("x_first_name", $params['firstname']);
			$payment->setParameter("x_last_name", $params['lastname']);
			$payment->setParameter("x_address", $params['address']);
			$payment->setParameter("x_city", $params['city']);
			$payment->setParameter("x_state", $params['state']);
			$payment->setParameter("x_zip", $params['zip']);
			$payment->setParameter("x_phone", $params['phone']);
			$payment->setParameter("x_company", $params['company']);
			$payment->setParameter("x_description", $params['description']);
			$payment->process();
			
			if($payment->isApproved()){
				// Get the approval code
				//$approval_code = $payment->getAuthCode();
				// Get the results of AVS
				//$avs_result = $payment->getAVSResponse();
				// Get the Authorize.Net transaction ID
				$transaction_id = $payment->getTransactionID();
				$response['success'] = true;
				$response['transaction_id'] = $transaction_id;
				//$response['approval_code'] = $approval_code;
			}
			else if($payment->isDeclined()){
				// Get reason for the decline from the bank. This always says,
				// "This credit card has been declined". Not very useful.
				//$reason = $payment->getResponseText();
				// Politely tell the customer their card was declined
				$response['success'] = false;
				$response['declined'] = true;
				$response['amount'] = $params['amount'];
			}
			else {
				$error_number = $payment->getResponseSubcode();
				$error_message = $payment->getResponseText();
				
				$response['success'] = false;
				$response['error'] = true;
				$response['errorcode'] = $error_number;
				$response['errormsg'] = $payment->getResponseMessage();
			} 
		} //end try
		//catch communication errors
		catch(AuthnetAIMException $e){
			require_once QUBEADMIN . 'inc/reseller.class.php';
			$reseller = new Reseller();
			//try / catch are only to catch SOAP communication errors and don't pertain to the transaction
			//email error to 6qube admin
			$subject = 'Error communicating an authorize.net AIM transaction with servers';
			$message = 
			'<h2>Details:</h2>
			<b>Reseller ID:</b> '.$params['resellerID'].'<br />
			<b>Customer/transaction ID:</b> '.$invoiceID.'<br />
			<b>Date/time</b>: '.date('m/d/Y').' at '.date('g:i:s').'<br /><br />
			<b>Error details:</b><br />
			'.$e->__toString();
			
			$reseller->sendMail('reeselester@gmail.com', $subject, $message, NULL, 'noreply@6qube.com', NULL, TRUE);
			
			//let user know something went wrong
			$response['success'] = false;
			$response['error'] = true;
			$response['errorcode'] = 9991;
			$response['errormsg'] = 'A communications error occured.  Please try again or contact support.  We apologize for the inconvenience.';
		} //end catch(AuthnetAIMException $e)
		
		if($payment) $payment->__destruct();
		return $response;
	} //end chargeAIM()
	
	//Function to charge a card on file with a CIM payment profile
	function chargeCIM($uid, $rid='', $pid='', $amount, $type='', $log=1, $addNote='', $apiLogin='', $apiAuth='', $uid2=''){
		require_once QUBEADMIN . 'inc/reseller.class.php';
		$reseller = new Reseller();
		$query = "SELECT billing_id, billing_id2, billing_id3 FROM users WHERE id = '".$uid."'";
		if($result = $this->queryFetch($query)){
			if(!$apiLogin && !$apiAuth){
				//if no API info provided use 6qube's
				$api = $this->getAuthnetAPI();
				$apiLogin = $api['apiLogin'];
				$apiAuth = $api['transKey'];
				$anetType = '6qube';
			}
			else $anetType = 'authorize';
			
			try {
			if($amount>0){
				//types of charges:
				//wholesale charge to reseller ($uid will be rid, $uid2 will be the user's id, $type will be 'wholesale')
				//monthly charge to reseller ($uid will be rid, type will be '6qubeMonthly')
				//monthly charge to user ($uid will be uid, $rid rid, $type 'monthlySubscription')
				//initial month charge to user ($uid uid, $rid rid, $type 'initialSubscription')
				//initial charge to user ($uid uid, $rid rid, $type 'initialSetupFees')
				//initial monthly AND setup fees consolidated (charged on profile creation)($uid uid, $rid rid, $type 'initial')
				$productName = $reseller->getProductName($pid);
				if($type=="wholesale"){
					$invoiceNum = substr('wholesale'.$uid2.date('Ymd'), 0, 20);
					$invoiceDesc = substr('Wholesale charge for user '.$uid2.' with product '.$productName, 0, 255);
				}
				else if($type=="6qubeMonthly"){
					$invoiceNum = substr('6qubeMonthly'.$uid.date('Ymd'), 0, 20);
					$invoiceDesc = 'Monthly fee for 6Qube Reseller account for month of '.date('F');
				}
				else if($type=="monthlySubscription"){
					$invoiceNum = substr('monthlySub'.$uid.date('Ymd'), 0, 20);
					$invoiceDesc = substr('Monthly subscription fee for '.$productName.' account.', 0, 255);
				}
				else if($type=="initialSubscription"){
					$invoiceNum = substr('initialSub'.$uid.date('Ymd'), 0, 20);
					$invoiceDesc = substr('Initial monthly subscription for '.$productName.' account.', 0, 255);
				}
				else if($type=="initialSetupFees"){
					$invoiceNum = substr('initialFees'.$uid.date('Ymd'), 0, 20);
					$invoiceDesc = substr('Initial setup fees for '.$productName.' account.', 0, 255);
				}
				else if($type=="initial"){
					$invoiceNum = substr('initial'.$uid.date('Ymd'), 0, 20);
					$invoiceDesc = substr('Initial charges for '.$productName.' account.', 0, 255);
				}
				//set up params and attempt charge
				$payment = new AuthnetCIM($apiLogin, $apiAuth);
				$payment->setParameter('amount', $amount);
				$payment->setParameter('customerProfileId', $result['billing_id']);
				$payment->setParameter('customerPaymentProfileId', $result['billing_id2']);
				if($result['billing_id3']) $payment->setParameter('cardCode', $result['billing_id3']);
				$payment->setParameter('orderInvoiceNumber', $invoiceNum);
				$payment->setParameter('description', $invoiceDesc);
				$payment->createCustomerProfileTransaction();
				
				//if charge was successful
				if($payment->isSuccessful()){
					$transaction_id = $payment->getTransactionID();
					//log it
					if($log)
						$logID = $reseller->logTransaction($uid, $rid, $pid, $type, $transaction_id, $amount, 
													 $result['billing_id2'], $anetType, 1, $addNote, $uid2);
					//log credit if reseller is using our processor
					if($anetType=='6qube' && $rid && 
					  ($type=='initialSetupFees' || $type=='initialSubscription' || $type=='monthlySubscription'))
						$reseller->logCredit($rid, $uid, $pid, $type, $amount, $logID);
					
					$return['success'] = true;
					$return['message'] = $transaction_id;
					$return['profile_id'] = $result['billing_id2'];
					$return['log_id'] = $logID;
				}
				//if charge failed or error
				else {
					$error = $payment->getResponse();
					if($log)
						$logID = $reseller->logTransaction($uid, $rid, $pid, $type, NULL, $amount, $result['billing_id2'], 
													$anetType, 0, $error.'<br />'.$addNote, $uid2);
					$return['success'] = false;
					$return['message'] = $error;
					$return['profile_id'] = $result['billing_id2'];
					$return['log_id'] = $logID;
				}
				$payment->__destruct();
			} //end if($result['amount']>0)
			else {
				$return['success'] = true;
				$return['message'] = 'NA';
				$return['profile_id'] = $result['billing_id2'];
				$return['log_id'] = 999999;
			}
			} //end try
			catch(AuthnetCIMException $e){
				//try / catch are only to catch SOAP communication errors and don't pertain to the transaction
				$subject = 'Error communicating an authorize.net CIM transaction with servers';
				$message = 
				'<h2>Details:</h2>
				<b>User ID:</b> '.$uid.'<br />';
				if($rid) $message .= '<b>Reseller ID:</b> '.$rid.'<br />';
				$message .=
				'<b>Type:</b> '.$type.'<br />
				<b>Note:</b> '.$addNote.'<br />
				<b>Date/time</b>: '.date('m/d/Y').' at '.date('g:is').'<br /><br />
				<b>Error details:</b><br />
				'.$e->__toString();
				
				$reseller->sendMail('reeselester@gmail.com', $subject, $message, NULL, 'noreply@6qube.com', NULL, TRUE);
				
				//let user know something went wrong
				$return['success'] = false;
				$return['profile_id'] = $result['billing_id2'];
				$return['message'] = '9992: A communications error occured.  Please try again or contact support.  We apologize for the inconvenience.';
			}
		}
		else {
			$return['success'] = false;
			$return['message'] = 'Couldn\'t retrieve billing profile';
		}
		
		return $return;
	} //end chargeCIM()
	
} //end class
?>
