<?php
if(!defined('QUBEADMIN')){
    throw new Exception('Access Denied.');
}

require_once QUBEADMIN . 'inc/analytics.class.php';
if(class_exists('Local')) return;
class Local extends Analytics {
	
	/* Function getItemsHTML returns html list items.
	 * @params:
	 * $type (a string) - 'hub', 'form', 'blog', 'autoresponder'
	 * $cid (a number) - the campaign id number
	 */
	function getItemsHTML($type, $cid){
		$i = 1;
		$cid = is_numeric($cid) ? $cid : '';
		$type = $this->sanitizeInput($type);
		if($type=='form') $table = 'lead_forms';
		else if($type=='blog'){
			$table = 'blogs';
			$nameField = 'blog_title';
		}
		//defaults
		if(!$nameField) $nameField = 'name';
		if(!$table) $table = $type;
		
		if($cid && $type && $table){
			$query = "SELECT id, ".$nameField." FROM ".$table." WHERE cid = '".$cid."' AND ".$table.".trashed = '0000-00-00'";
			$items = $this->query($query);
			if($items && $this->numRows($items)){
				while($row = $this->fetchArray($items, NULL, 1)){
					echo '<li>';
					echo '<input type="checkbox" name="item['.$i.']" value="'.$row['id'].'" checked>&nbsp;&nbsp;'.$row[$nameField].'<br />';
					echo '</li>';
					$i++;
				}
				if($type == 'blog'){
					echo '<li style="color:red;font-size:0.8em;padding-top:5px;">*Transferring a blog will permanently disconnect it from it\'s associated hub if applicable.</li>';
				}
			}
			else {
				echo '<li>None available.</li>';
			}
		}
	}
	
	/*Transfers an item to another campaign
	 * @params:
	 * $type (a string) - 'hub', 'form', 'blog', 'autoresponder'
	 * $item_id (string or num) - id (from the DB table) of the item you are transferring
	 * $cid - the campaign id that you would like to transfer the item TO
	 */
	function transferToAnotherCamp($type, $item_id, $cid){
		$item_id = is_numeric($item_id) ? $item_id : '';
		$cid = is_numeric($cid) ? $cid : '';
		$type = $this->sanitizeInput($type);
		if($type == 'form')
			$table = 'lead_forms';
		else if ($type == 'blog')
			$table = 'blogs';
		else
			$table = $type;
		
		if($table){
			$query = "UPDATE ".$table." SET cid = '".$cid."' WHERE id = '".$item_id."'";
			if($type == 'hub'){
				$blogQuery = "UPDATE blogs SET cid = '".$cid."' WHERE connect_hub_id = '".$item_id."'";
				$autoQuery = "UPDATE auto_responders SET cid = '".$cid."' WHERE table_id = '".$item_id."' AND table_name = 'hub'";
				if($this->query($query) && $this->query($blogQuery) && $this->query($autoQuery))
					return true;
			}
			else if ($type == 'blog'){
				//If it is a blog that's connected to a hub, we need to disconnect it by zeroing out the connect id's
				//Get the connected hub id from the blog
				$checkConnectQuery = "SELECT connect_hub_id FROM blogs WHERE id = '".$item_id."'";
				$connectedBlog = $this->query($checkConnectQuery);
				$row = $this->fetchArray($connectedBlog);
				//If the blog has a connected hub id
				if($row['connect_hub_id'] > 0){
					$connected_hub_id = $row['connect_hub_id'];
					//Change the campaign id and set the connected hub id to zero
					$blogQuery = "UPDATE blogs SET cid = '".$cid."', connect_hub_id = 0 WHERE id = '".$item_id."'";
					//And then change the hub's associated blog id to zero
					$hubQuery = "UPDATE hub SET connect_blog_id = 0, blog = '' WHERE id = '".$connected_hub_id."'";
					if($this->query($blogQuery) && $this->query($hubQuery)){
						return true;
					}
					else{
						return false;
					}
				}
				//Otherwise attempt the standard query to change the campaign id
				else {
					if($this->query($query)){
						return true;
					}
					else return false;
				}						
			}
			else if($this->query($query))
				return true;
			else
				return false;
		}
		else return false;
	}
	
	/**********************************************************************/
	// Display a dropdown of a user/campaigns's items with tracking
	/**********************************************************************/
	function displaySnapshotSelector($cid, $uid, $selected){
		$cid = is_numeric($cid) ? $cid : '';
		$uid = is_numeric($uid) ? $uid : '';
		if($cid && $uid){
			//default query
			$query = "SELECT tracking_id, name FROM [[table]] WHERE cid = '".$cid."' AND user_id = '".$uid."' AND tracking_id != 0
					  AND [[table]].trashed = '0000-00-00'";
			$types = array();
			//get each type
			$types['Websites'] = $this->query(str_replace('[[table]]', 'hub', $query));
			//set up select
			$output = '<select id="changeGraph" class="uniformselect">';
			//loop through items
			//hubs
			foreach($types as $key=>$value){
				if($value && $this->numRows($value)){
					$output .= '<optgroup label="'.$key.'">';
					while($row = $this->fetchArray($types[$key], NULL, 1)){
						$output .= '<option value="'.$row['tracking_id'].'"';
						if($row['tracking_id']==$selected) $output .= ' selected';
						if($key!='Blogs') $output .= '>'.$row['name'].'</option>';
						else $output .= '>'.$row['blog_title'].'</option>';
					}
					$output .= '</optgroup>';
				}
			}
			$output .= '</select>';
		}
		echo $output;
	}
	
	/**********************************************************************/
	// Parse a hub's req_theme_type field and return value(s) in an array
	/**********************************************************************/
	function uniqueFilename($filename){
		$a = explode('.', $filename); //break apart
		$ext = $a[count($a)-1]; //get extension
		$a[count($a)-1] = ''; //remove extension
		$output = trim(substr(implode('.', $a), 0, -1)); //put back together without trailing '.'
		$output .= '_'.substr(strtotime('now'), -3).'.'.$ext; //add #
		
		return $output;
	}
	
	/**********************************************************************/
	// Parse a hub's req_theme_type field and return value(s) in an array
	/**********************************************************************/
	function parseHubReqThemeType($type = '', $hubID = ''){
		if(!$type && $hubID){
			$query = "SELECT req_theme_type FROM hub WHERE id = '".$hubID."'";
			$result = $this->queryFetch($query);
			$type = $result['req_theme_type'];
		}
		
		if($type && $type!='0'){
			if(strpos($type, ',')!==false){
				$array = explode(',', $type);
				return $array;
			}
			else {
				$array = array($type);
				return $array;
			}
		}
		else return false;
	}
	
	/**********************************************************************/
	// Register user as online now
	/**********************************************************************/
	function onlineNow($uid, $action = ''){
		if(is_numeric($uid)){
			if($action=='update'){
				$query = "UPDATE online_now SET time = '".time()."' WHERE uid = '".$uid."'";
			} 
			else if($action=='delete'){
				$query = "DELETE FROM online_now WHERE uid = '".$uid."'";
			} else {
				$query = "INSERT INTO online_now (uid, time) VALUES ('".$uid."', '".time()."')";
			}
			$this->query($query);
		}
	}
	
	/**********************************************************************/
	// Check IP against blacklist table
	/**********************************************************************/
	function checkBlacklist($ip = ''){
		if($ip){
			$query = "SELECT count(id) FROM blacklist WHERE ip = '".$ip."'";
			$result = $this->queryFetch($query);
			
			if($result['count(id)']) return true;
			else return false;
		}
		else return false;
	}
	
	/**********************************************************************/
	// Return user's number of email sends remaining for the class limit
	/**********************************************************************/
	function emailsRemaining($uid){
		if($uid && is_numeric($uid)){
			//get user's class limit
			$query = "SELECT user_class.email_limit AS email_limit FROM user_class 
					WHERE user_class.id = (SELECT users.class FROM users WHERE users.id = '".$uid."')";
			$a = $this->queryFetch($query);
			if($a && $a['email_limit']!=-1){
				//get user's current number of emails sent
				$query = "SELECT emails_sent FROM users WHERE id = '".$uid."'";
				$b = $this->queryFetch($query);
				$remaining = $a['email_limit'] - $b['emails_sent'];
				if($remaining <= 0) return 0;
				else return $remaining;
			}
			else return 999999;
		}
	}	
	
	/**********************************************************************/
	// Simple function to check if a user has access
	/**********************************************************************/
	function checkUserAccess($uid){
		$query = "SELECT parent_id, access FROM users WHERE id = '".$uid."'";
		$a = $this->queryFetch($query);
		if($a['access']) $return = true;
		if($a['parent_id']){
			//check parent user has client_access enabled
			$query = "SELECT client_access FROM users WHERE id = '".$a['parent_id']."'";
			$b = $this->queryFetch($query);
			if(!$b['client_access']) $return = false;
		}
		
		return $return;
	}
	
	/*************************************************************************
	// Check if user's account is a free one
	/*************************************************************************/
	function isFreeAccount($pid){
		$query = "SELECT maps_to FROM user_class WHERE id = '".$pid."'";
		$a = $this->queryFetch($query);
		
		if($a['maps_to']==27) return true;
		else if($pid==27 || $pid==1 || $pid==178 || $pid==318) return true;
		//else if($a['monthly_price']==0) return true;
		else return false;
	}
	
	/**********************************************************************/
	// Get listing rows for reseller networks
	/**********************************************************************/
	function getListingsForNetwork($limit='',$offset='',$city='',$state='',$category='',$pid='',$uid='',$cid='',$keyword='',$setCat='',$classid='',$remove='',$basiclisting='',$featured=''){
		//Query DB for total number of listings that fit criteria.  Only select id to save time
		if($pid){ 	
			$dataCheck = "SELECT master_network_data FROM resellers WHERE admin_user = '".$pid."'";
			$masterdata = $this->queryFetch($dataCheck);		
			if($masterdata['master_network_data']==1) $pid = false;
		}
	 	$query1 = "SELECT count(id) FROM `directory` ";
		if($basiclisting){
		$query =  "WHERE `company_name` != ''
				 AND `phone` != ''
				 AND `street` != ''
				 AND `city` != ''
				 AND `state` != ''
				 AND `category` != ''
				 AND `category` != '0' 
				 AND `keyword_one` != ''
				 AND `trashed` = '0000-00-00'";
		} else {
		$query =  "WHERE `company_name` != ''
				 AND `phone` != ''
				 AND `street` != ''
				 AND `city` != ''
				 AND `state` != ''
				 AND `category` != ''
				 AND `category` != '0' 
				 AND `website_url` != ''
				 AND `display_info` != ''
				 AND `keyword_one` != ''
				 AND `trashed` = '0000-00-00'";	
		}
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($category) $query .= " AND category LIKE '%".$category."%'";
		if($setCat) $query .= " AND category LIKE '%".$setCat."%'";
		if($pid) $query .= " AND user_parent_id = ".$pid;
		if($uid) $query .= " AND user_id = ".$uid;
		if($cid) $query .= " AND cid = ".$cid;
		if($remove) $query .= " AND class_id != ".$remove;
		else if($classid) $query .= " AND class_id = ".$classid;
		if($keyword) $query .= " AND (category LIKE '%" . $keyword . "%' OR company_name LIKE '%".$keyword."%' OR keyword_one LIKE '%".$keyword."%' OR display_info LIKE '%".$keyword."%') ";
		if(!$limit && !$offset && !$city && !$state && !$category && !$uid && !$cid && !$keyword && !$setCat) $count = true;
		
		$results1 = $this->queryFetch($query1.$query);
		if($results1['count(id)']){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed

			$query2 = "SELECT 
						id, 
						user_id,
						class_id, 
						company_name, 
						phone, 
						street, 
						city, 
						state, 
						zip, 
						category, 
						photo, 
						display_info, 
						keyword_one, 
						keyword_one_link, 
						keyword_two, 
						keyword_two_link, 
						keyword_three, 
						keyword_three_link, 
						basic_listing, 
						last_edit, 
						created
					 FROM 
						`directory` ";

			if($featured && $basiclisting) $query .= " ORDER BY (class_id='".$featured."') DESC, (basic_listing='') DESC, created DESC";
			else if($featured && !$basiclisting) $query .= " ORDER BY (class_id='".$featured."') DESC, created DESC";
			else if(!$featured && $basiclisting) $query .= " ORDER BY (basic_listing='' AND created) DESC, created DESC";
			else $query .= " ORDER BY created DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			
			if($count){
				return $results1['count(id)'];
			}
			else {
				$results2 = $this->query($query2.$query);
				if($results2 && $this->numRows($results2)){
					$return['numResults'] = $results1['count(id)'];
					$return['results'] = $results2;
					return $return;
				}
				else return false;
			}
		}
		else return false;
	}
	
	/**********************************************************************/
	// Get hub rows for reseller networks
	/**********************************************************************/
	function getHubsForNetwork($limit='',$offset='',$city='',$state='',$category='',$pid='',$uid='',$cid='',$keyword=''){
		//Query DB for total number of hubs that fit criteria.  Only select id to save time
	 	$query1 = "SELECT count(id) FROM `hub` ";
		$query =  "WHERE `domain` != ''
				 AND `category` != ''
				 AND `category` != '0'
				 AND `company_name` != ''
				 AND `phone` != ''
				 AND `street` != ''
				 AND `city` != ''
				 AND `state` != ''
				 AND `slogan` != ''
				 AND `description` != ''
				 AND `cid` != 99999
				 AND `trashed` = '0000-00-00'";
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($category) $query .= " AND category LIKE '%".$category."'";
		if($pid) $query .= " AND user_parent_id = ".$pid;
		if($uid) $query .= " AND user_id = ".$uid;
		if($cid) $query .= " AND cid = ".$cid;
		if($keyword) $query .= " AND (category LIKE '%".$keyword."%' OR company_name LIKE '%".$keyword."%') ";
		if(!$limit && !$offset && !$city && !$state && !$category && !$uid && !$cid && !$keyword) $count = true;
		
		$results1 = $this->queryFetch($query1.$query);
		if($results1['count(id)']){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed
			$query2 = "SELECT 
						id, 
						user_id, 
						city, 
						state, 
						street, 
						phone, 
						zip, 
						slogan, 
						domain, 
						company_name, 
						logo, 
						description, 
						keyword1, 
						keyword2, 
						keyword3, 
						keyword4, 
						keyword5, 
						keyword6, 
						has_tags, 
						created
					 FROM 
					 	`hub` ";
			$query .= " ORDER BY created DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			
			if($count){
				return $results1['count(id)'];
			}
			else {
				$results2 = $this->query($query2.$query);
				if($results2 && $this->numRows($results2)){
					$return['numResults'] = $results1['count(id)'];
					$return['results'] = $results2;
					return $return;
				}
				else return false;
			}
		}
		else return false;
	}
	
	/**********************************************************************/
	// Get press rows for reseller networks
	/**********************************************************************/
	function getPressForNetwork($limit='',$offset='',$city='',$state='',$category='',$pid='',$uid='',$cid='',$keyword=''){
		//Query DB for total number of press releases that fit criteria.  Only select id to save time
	 	$query1 = "SELECT count(id) FROM `press_release` ";
		$query =  "WHERE headline != '' 
				 AND summary != '' 
				 AND body != '' 
				 AND city != '' 
				 AND state != '' 
				 AND category != '0'
				 AND category != '' 
				 AND network = 1
				 AND trashed = '0000-00-00'";
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($category) $query .= " AND category LIKE '%".$category."'";
		if($pid) $query .= " AND user_parent_id = ".$pid;
		if($uid) $query .= " AND user_id = ".$uid;
		if($cid) $query .= " AND cid = ".$cid;
		if($keyword) 
			$query .= " AND (category LIKE '%".$keyword."%' OR headline LIKE '%".$keyword."%' OR summary LIKE '%".$keyword."%')";
		if(!$limit && !$offset && !$city && !$state && !$category && !$uid && !$cid && !$keyword) $count = true;
		
		$results1 = $this->queryFetch($query1.$query);
		if($results1['count(id)']){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed
			$query2 = "SELECT 
						id, 
						user_id, 
						thumb_id, 
						category, 
						headline, 
						summary, 
						keywords, 
						author, 
						timestamp, 
						created
					 FROM 
						`press_release` ";
			$query .= " ORDER BY created DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			
			if($count){
				return $results1['count(id)'];
			}
			else {
				$results2 = $this->query($query2.$query);
				if($results2 && $this->numRows($results2)){
					$return['numResults'] = $results1['count(id)'];
					$return['results'] = $results2;
					return $return;
				}
				else return false;
			}
		}
		else return false;
	}
	
	/**********************************************************************/
	// Get article rows for reseller networks
	/**********************************************************************/
	function getArticlesForNetwork($limit='',$offset='',$city='',$state='',$category='',$pid='',$uid='',$cid='',$keyword=''){
		//Query DB for total number of articles that fit criteria.  Only select id to save time
	 	$query1 = "SELECT count(id) FROM `articles` ";
		$query =  "WHERE headline != '' 
				 AND summary != '' 
				 AND body != '' 
				 AND city != '' 
				 AND state != '' 
				 AND category != '0'
				 AND category != '' 
				 AND network = 1
				 AND trashed = '0000-00-00' ";
		if($city) $query .= " AND city = '".$city."'";
		if($state) $query .= " AND state = '".$state."'";
		if($category) $query .= " AND category LIKE '%".$category."'";
		if($pid) $query .= " AND user_parent_id = ".$pid;
		if($uid) $query .= " AND user_id = ".$uid;
		if($cid) $query .= " AND cid = ".$cid;
		if($keyword) 
			$query .= " AND (category LIKE '%".$keyword."%' OR headline LIKE '%".$keyword."%' OR summary LIKE '%".$keyword."%')";
		if(!$limit && !$offset && !$city && !$state && !$category && !$uid && !$cid && !$keyword) $count = true;
		
		$results1 = $this->queryFetch($query1.$query);
		if($results1['count(id)']){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed
			$query2 = "SELECT 
						id, 
						user_id, 
						thumb_id, 
						category, 
						headline, 
						summary, 
						keywords, 
						author, 
						timestamp, 
						created
					 FROM 
						`articles` ";
			$query .= " ORDER BY created DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			
			if($count){
				return $results1['count(id)'];
			}
			else {
				$results2 = $this->query($query2.$query);
				if($results2 && $this->numRows($results2)){
					$return['numResults'] = $results1['count(id)'];
					$return['results'] = $results2;
					return $return;
				}
				else return false;
			}
		}
		else return false;
	}
	
	/**********************************************************************/
	// Get blog post rows for reseller networks
	/**********************************************************************/
	function getPostsForNetwork($limit='',$offset='',$city='',$state='',$category='',$pid='',$uid='',$cid='',$keyword=''){
		//Query DB for total number of blog posts that fit criteria.  Only select id to save time
		//this is set up a little differently for blogs because of the table join
	 	$query1 =  "SELECT count(id) FROM `blog_post` 
				  WHERE category != '' ";
		$query =  " AND post_title != '' 
				  AND post_content != '' 
				  AND tags != '' 
				  AND blog_id != 0
				  AND network = 1
				  AND blog_post.trashed = '0000-00-00'";
		if($city) $query .= " AND (SELECT blogs.city FROM blogs WHERE blogs.id = blog_post.blog_id) = '".$city."'";
		if($state) $query .= " AND (SELECT blogs.state FROM blogs WHERE blogs.id = blog_post.blog_id) = '".$state."'";
		if($category) $query .= " AND (SELECT blogs.category FROM blogs WHERE blogs.id = blog_post.blog_id) LIKE '%".$category."'";
		if($pid) $query .= " AND blog_post.user_parent_id = ".$pid;
		if($uid) $query .= " AND blog_post.user_id = ".$uid;
		if($cid) $query .= " AND (SELECT blogs.cid FROM blogs WHERE blogs.id = blog_post.blog_id) = ".$cid;
		if($keyword) $query .= " AND (blog_post.category LIKE '%".$keyword."%' OR blog_post.post_title LIKE '%".$keyword."%' 
							OR blog_post.tags LIKE '%".$keyword."%')";
		if(!$limit && !$offset && !$city && !$state && !$category && !$uid && !$cid && !$keyword) $count = true;
		
		$results1 = $this->queryFetch($query1.$query);
		if($results1['count(id)']){
			//Query DB for data matching specific criteria
			//only pull fields that are displayed
			$query2 = "SELECT 
						blog_post.id AS post_id, 
						blog_post.blog_id, 
						blog_post.tracking_id, 
						blog_post.user_id, 
						blog_post.category, 
						blog_post.post_title, 
						blog_post.post_content, 
						blog_post.photo, 
						blog_post.tags, 
						blog_post.tags_url, 
						blog_post.created, 
						blogs.domain, 
						blogs.blog_title
					 FROM 
						`blog_post`, `blogs` 
					 WHERE
					 	blog_post.blog_id = blogs.id ";
			$query .= " ORDER BY created DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			
			if($count){
				return $results1['count(id)'];
			}
			else {
				$results2 = $this->query($query2.$query);
				if($results2 && $this->numRows($results2)){
					$return['numResults'] = $results1['count(id)'];
					$return['results'] = $results2;
					return $return;
				}
				else return false;
			}
		}
		else return false;
	}
	/**********************************************************************/
	// Get blog post rows for reseller networks
	/**********************************************************************/
	function getSearchResultsForNetwork($keyword, $location, $pid = '', $setCat=''){
		$location = $this->fixLoc($location);
		$numResults = 0;
		$resultsArray = array();
		
		//get results from each type
		//local
		if($localResults = $this->getListingsForNetwork(50,NULL,$location[0],$location[1],NULL,$pid,NULL,NULL,$keyword,$setCat)){
			$numResults += $localResults['numResults'];
			$resultsArray['local'] = array();
			while($row = $this->fetchArray($localResults['results'])){
				$resultsArray['local'][] = $row;
			}
		}
		//hub
		if($hubResults = $this->getHubsForNetwork(50,NULL,$location[0],$location[1],NULL,$pid,NULL,NULL,$keyword)){
			$numResults += $hubResults['numResults'];
			$resultsArray['hubs'] = array();
			while($row = $this->fetchArray($hubResults['results'])){
				$resultsArray['hubs'][] = $row;
			}
		}
		//press
		if($pressResults = $this->getPressForNetwork(50,NULL,$location[0],$location[1],NULL,$pid,NULL,NULL,$keyword)){
			$numResults += $pressResults['numResults'];
			$resultsArray['press'] = array();
			while($row = $this->fetchArray($pressResults['results'])){
				$resultsArray['press'][] = $row;
			}
		}
		//articles
		if($articleResults = $this->getArticlesForNetwork(50,NULL,$location[0],$location[1],NULL,$pid,NULL,NULL,$keyword)){
			$numResults += $articleResults['numResults'];
			$resultsArray['articles'] = array();
			while($row = $this->fetchArray($articleResults['results'])){
				$resultsArray['articles'][] = $row;
			}
		}
		//blog posts
		if($blogResults = $this->getPostsForNetwork(50,NULL,$location[0],$location[1],NULL,$pid,NULL,NULL,$keyword)){
			$numResults += $blogResults['numResults'];
			$resultsArray['blogposts'] = array();
			while($row = $this->fetchArray($blogResults['results'])){
				$resultsArray['blogposts'][] = $row;
			}
		}
		
		//set return values
		if($numResults){
			$return['numResults'] = $numResults;
			$return['results'] = $resultsArray;
		}
		else {
			$return['numResults'] = 0;
		}
	
		return $return;
	}
	
	/******************************************************
	// Get Elements blog posts
	/******************************************************/
	function getElementsBlogPost($numberOfPost){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->getElementsBlogPost($numberOfPost);
	}
	
	/******************************************************
	// Get 6Qube blog posts
	/******************************************************/
	function getQubeBlogPost($numberOfPost, $blogid){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->getQubeBlogPost($numberOfPost, $blogid);
	}
	
	/******************************************************
	// Get user's campaigns
	// optional "count" parameter to only select id
	/******************************************************/
	function getCampaigns($user_id, $count='', $limit='', $offset='', $countOnly = '', $search=''){
		//validate inputs
		$user_id = is_numeric($user_id) ? $user_id : 0;
		$limit = is_numeric($limit) ? $limit : 0;
		$offset = is_numeric($offset) ? $offset : 0;
		if($countOnly) $sql = "SELECT count(id) ";
		else if($count) $sql = "SELECT id ";
		else $sql = "SELECT * ";
		$sql .= "FROM campaigns 
			    WHERE user_id = '".$user_id." '";
		if($search){
			$sql .= "AND name LIKE '%".$search."%' ";
		}
		$sql .= "ORDER BY date_created DESC ";
		if($limit) $sql .= " LIMIT ".$limit;
		if($offset) $sql .= " OFFSET ".$offset;
		if($countOnly){
			$result = $this->queryFetch($sql);
			return $result['count(id)'];
		}
		else {
			$results = $this->query($sql);
			if($results && $this->numRows($results)) return $results;
			else return false;
		}
	}
	
	/******************************************************
	// Get autoresponders
	/******************************************************/
	function getAutoResponders($table_id = '', $table = '', $user_id = '', $ar_id = '', $page = '', $count = '', $cid = ''){
		if($count) $query = "SELECT count(id) ";
		else if($page=="settings") $query = "SELECT id, user_id, table_id, table_name, name, sched, sched_mode, active";
		else if($page=="email") $query = "SELECT id, user_id, table_id, table_name, sched, from_field, subject, body, return_url, contact, anti_spam, optout_msg, theme";
		else if($page=="theme") $query = "SELECT id, user_id, table_id, table_name, logo, bg_color, links_color, accent_color, titles_color, edit_region1, call_action, edit_region2, theme";
		else $query  = "SELECT *";
		$query .= " FROM auto_responders WHERE id IS NOT NULL ";
		if(!$count)  $query .= " AND auto_responders.trashed = '0000-00-00'";
		if($table_id) $query .= " AND table_id = ".$table_id;
		if($table) $query .= " AND table_name = '".$table."'";
		if($user_id) $query .= " AND user_id = ".$user_id;
		if($ar_id) $query .= " AND id = ".$ar_id;
		if($cid) $query .= " AND cid = ".$cid;
		$query .= " ORDER BY id ASC ";

#echo $query;
#		deb($query);
		if($count){
			$result = $this->queryFetch($query);
			return $result['count(id)'];
		}
		else {
			$result = $this->query($query);
			if($result) return $result;
			else return false;
		}
	}
	
	/******************************************************
	// Function to update campaign section of dash
	/******************************************************/
	function campaignDashUpdate($row, $display = ''){
		$settings = $this->getSettings();
		$analSiteID = $this->getHubOrDirID($row['id']);
		if(!$display) $display = "block";
		if(!$analSiteID || $analSiteID==0) $analSiteID = 26;
		$html = '<div class="item" style="display:'.$display.';">
			<div class="icons">
				<a href="dashboard.php?CID='.$row['id'].'" class="view2" rev="dashboard"><span>View</span></a>
				<!-- <a href="directory.php?form=listings&id='.$id.'" class="edit" rev="directory"><span>Edit</span></a> -->
				<a href="#" class="delete deleteCamp" rel="'.$row['id'].'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('campaign', $row['id'], $analSiteID, $row['user_id']).'" class="graph trigger" />';
		$html .= '<a href="dashboard.php?CID='.$row['id'].'" rev="dashboard">'.$row['name'].'</a>
		</div>';
		
		return $html;
	}
	
	/******************************************************
	// Function to update campaign section of dash
	/******************************************************/
	function campaignDashUpdate2($row, $display = ''){
		$settings = $this->getSettings();
		$analSiteID = $this->getHubOrDirID($row['id']);
		if(!$display) $display = "block";
		if(!$analSiteID || $analSiteID==0) $analSiteID = 26;
		if($v3){
		$campaignStats = $this->getCampaignStats($row['id'], $row['user_id']);
		$conversions = round($campaignStats['numProspects'] * 100 / $campaignStats['totalVisits']) . "%";
		$created = date("F jS, Y", strtotime($row['date_created']));
		$html = '<tr class="gradeA">
                	<td><a href="dashboardv3.php?CID='.$row['id'].'" rev="dashboard">'.$row['name'].'</a></td>
		<td class="center">'.$campaignStats['totalVisits'].'</td>
                	<td class="center">'.$campaignStats['numProspects'].'</td>
                	<td class="center">'.$conversions.'%</td>
                	<td class="center">'.$campaignStats['numTouchPoints'].'</td>
                	<td class="center">'.$created.'</td>
                	<td class="center">
                		<a class="tipN" href="#" original-title="Edit Campaign">
                			<span class="icos-create2"></span>
                		</a>
                		<a class="tipN" href="#" original-title="Delete Campaign">
                			<span class="icos-cross"></span>
                		</a>
                	</td>
                </tr>';
		} else {
		$html = '<div class="item" style="display:'.$display.';">
			<div class="icons">
				<a href="dashboardv3.php?CID='.$row['id'].'" class="view2" rev="dashboard"><span>View</span></a>
				<!-- <a href="directory.php?form=listings&id='.$id.'" class="edit" rev="directory"><span>Edit</span></a> -->
				<a href="#" class="delete deleteCamp" rel="'.$row['id'].'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('campaign', $row['id'], $analSiteID, $row['user_id']).'" class="graph trigger" />';
		$html .= '<a href="dashboardv3.php?CID='.$row['id'].'" rev="dashboard">'.$row['name'].'</a>';
		$html .= '</div>';
		}
		if($row['user_id']==496){
			$html .= '<br /><div style="clear:both;width:100%;border:1px solid black;">';
			$campaignStats = $this->getCampaignStats($row['id'], $row['user_id']);
			$html .= 
			'<strong>Total visits across all sites:</strong> '.$campaignStats['totalVisits'].'<br />
			<strong>Most visited site:</strong> '.$campaignStats['bestSite'].'<br />
			<strong>Number of prospects:</strong> '.$campaignStats['numProspects'];
			if($campaignStats['numCalls']) $html .= '<br /><strong>Number of phone calls:</strong> '.$campaignStats['numCalls'];
			$html .= '</div><br style="clear:both;" />';
		}
		
		return $html;
	}
	
	/************************************************************************
	// Function to gather and return performance data for an entire campaign
	/************************************************************************/
	function getCampaignStats($campaignID, $uid){
		$campaignID = is_numeric($campaignID) ? $campaignID : 0;
		$return = array();
		//get number of leads
		$query = "SELECT count(id) FROM leads WHERE (SELECT hub.cid FROM hub WHERE hub.id = leads.hub_id) = '".$campaignID."'";
		$leads1 = $this->queryFetch($query);
		$query = "SELECT count(id) FROM contact_form WHERE cid = '".$campaignID."'";
		$leads2 = $this->queryFetch($query);
		$return['numProspects'] = $leads1['count(id)']+$leads2['count(id)'];
		//get number of touchpoints
		$query = "SELECT count(id) FROM hub WHERE (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '1' AND cid = '".$campaignID."' AND trashed = '0000-00-00'";
		$websites = $this->queryFetch($query);
		$query = "SELECT count(id) FROM hub WHERE (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '2' AND cid = '".$campaignID."' AND trashed = '0000-00-00'";
		$landing = $this->queryFetch($query);
		$query = "SELECT count(id) FROM hub WHERE (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '3' AND cid = '".$campaignID."' AND trashed = '0000-00-00'";
		$mobile = $this->queryFetch($query);
		$query = "SELECT count(id) FROM hub WHERE (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '5' AND cid = '".$campaignID."' AND trashed = '0000-00-00'";
		$social = $this->queryFetch($query);
		$query = "SELECT count(id) FROM hub WHERE (SELECT themes.type FROM themes WHERE themes.id = hub.theme) = '4' AND cid = '".$campaignID."' AND trashed = '0000-00-00'";
		$legacyMU = $this->queryFetch($query);
		$return['numTouchPoints'] = $websites['count(id)']+$landing['count(id)']+$mobile['count(id)']+$social['count(id)']+$legacyMU['count(id)'];
		//get analytics info
		$visitsData = $this->ANAL_getCampViewCount($campaignID);
		$visitsBlogsData = $this->ANAL_getCampViewCountBlogs($campaignID);
		$return['totalVisits'] = ($visitsData['total']+$visitsBlogsData['total']);
		$return['bestSite'] = $visitsData['best']['name'].' ('.$visitsData['best']['count'].')';
		//check if the user has phone numbers set up
		$userphones = $this->queryFetch("SELECT mailbox_id FROM phone_numbers WHERE user_id = '".$uid."' AND mailbox_id != ''");
		if($userphones['mailbox_id']){
			require_once QUBEADMIN . 'inc/phone.class.php';
			$phone = new Phone();
			$calls = $phone->getCallDetails($uid, $userphones['mailbox_id'], 'YearToDate');
			$return['numCalls'] = $calls['ResultSet']['TotalResults'];
		}
		
		return $return;
	}
	
	/******************************************************
	// Function to update autoresponder section of dash
	/******************************************************/
	function autoResponderDashUpdate($row, $display = '', $type){
		if($type=='lead_forms') $type = 'hub';
		$html = '<div class="item" style="display:'.$display.';">
			<div class="icons">
				<a href="inc/forms/'.$type.'_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a class="delete rmParent" rel="table=auto_responders&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="img/autoresponder';
		if($_SESSION['thm_buttons-clr']) $html .= '/'.$_SESSION['thm_buttons-clr'];
		$html .= '.png" class="graph trigger" />';
		$html .= '<a href="inc/forms/'.$type.'_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
		
		return $html;
	}
	
	/******************************************************
	// Function to update autoresponder section of dash2
	/******************************************************/
	function autoResponderDashUpdate2($row, $display = '', $type){
		if($type=='lead_forms') $type = 'hub';
		$html = '<div class="item3" style="display:'.$display.';">
			<div class="icons">
				<a href="inc/forms/'.$type.'_autoresponder.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a class="delete rmParent" rel="table=auto_responders&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="img/field-email.png" class="icon" />';
		$html .= '<div class="name"><a href="inc/forms/website_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a></div>
						  <div class="type"><a href="inc/forms/website_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched'].'</a></div>
						  <div class="date"><a href="inc/forms/website_autoresponder.php?form=settings&id='.$row['id'].'">'.$row['sched_mode'].'</a></div>
		</div>';
		
		return $html;
	}
	
	/******************************************************
	// Analytics: Get site ID (tracking #) of first created hub 
	// or directory listing for campaign
	/******************************************************/
	function getHubOrDirID($campaign){
		//Check to see if campaign has any HUBs
		$query = "
		 		SELECT tracking_id 
				FROM hub 
				WHERE cid = {$campaign} 
				AND trashed = '0000-00-00'   
				LIMIT 1
		 		";
		$result = $this->query($query);
		//If so, return the tracking_id of the first HUB
		if($result){
			if($this->numRows($result)){
				$row = $this->fetchArray($result);
				return $row['tracking_id'];
			}
		}
		else {
			//Otherwise check for the first Directory listing and return its tracking_id
			$query = "
		 		SELECT tracking_id 
				FROM directory 
				WHERE cid = {$campaign} 
				AND trashed = '0000-00-00'  
				LIMIT 1
		 		";
			$result = $this->query($query);
			if($result){
				if($this->numRows($result)){
					$row = $this->fetchArray($result);
					return $row['tracking_id'];
				}
			}
			//If campaign doesn't have a directory listing either, return 1
			else { return 26; }
		}
	}
	
	/******************************************************
	// Add new campaign
	/******************************************************/
	function addNewCampaign($user_id, $name, $camp_limit = '', $bypass = ''){
		if(!$bypass) $camps = $this->getCampaigns($user_id, true);
		if(!$bypass) $campcount = $camps ? $this->numRows($camps) : 0;
		if($camp_limit==-1 || ($camp_limit - $campcount)>0 || $bypass){
			$sql = "INSERT INTO campaigns (user_id, name) VALUES ('".$user_id."','".$name."')";
			$result = $this->query($sql);
			
			if(!$bypass){
				$result2 = $this->getCampaigns($user_id);
				if($result2 && $this->numRows($result2)){
					while($row = $this->fetchArray($result2)){
						$html .= $this->campaignDashUpdate($row);
					}
					return $html;
				}
				else return false;
			}
			else return $this->getLastId();
		}
		else return false;
	}
	
	/******************************************************
	// Add new autoresponder
	/******************************************************/
	function addNewAutoResponder($user_id, $table_id, $name, $table, $parent_id = '', $cid = ''){
		//get info for contact field
		$contact = $this->getContactInfo($table, $table_id);
		//get company/email for from_field
		$a = $this->queryFetch("SELECT username FROM users WHERE id = '".$user_id."'");
		$b = $this->queryFetch("SELECT company FROM user_info WHERE user_id = '".$user_id."'");
		$email = $a['username'];
		$company = $b['company'];
		if($user_id && $name && $table_id){
			$query = "INSERT INTO auto_responders 
					(user_id, user_parent_id, cid, name, table_name, table_id, sched_mode, 
					 from_field, contact, anti_spam, optout_msg) ";
			$query .= " VALUES 
					('".$user_id."', '".$parent_id."', '".$cid."', '".$name."', '".$table."', ".$table_id.", 'hours', 
					'".$this->stripNonAlphaNum($company)." <".$email.">', '".$contact."', 
					'You\'re receiving this email because you submitted your information to the contact form on one of our web pages.', 'Click here to stop receiving emails from us.') ";
			$result = $this->query($query);
#			echo 'user_id', $user_id;
			$result2 = $this->getAutoResponders($table_id, $table, $user_id);
			if($result2 && $this->numRows($result2)){
				while($row = $this->fetchArray($result2)){
					$html .= $this->autoResponderDashUpdate($row, NULL, $table);
				}
				return $html;
			}
		}
		else return false;
	}
	
	/******************************************************
	// Add new autoresponder2
	/******************************************************/
	function addNewAutoResponder2($user_id, $table_id, $name, $table, $parent_id = '', $cid = '', $type){
		//get info for contact field
		$contact = $this->getContactInfo($table, $table_id);
		//get company/email for from_field
		$a = $this->queryFetch("SELECT username FROM users WHERE id = '".$user_id."'");
		$b = $this->queryFetch("SELECT company FROM user_info WHERE user_id = '".$user_id."'");
		$email = $a['username'];
		$company = $b['company'];
		if($user_id && $name && $table_id){
			$query = "INSERT INTO auto_responders 
					(user_id, user_parent_id, cid, name, table_name, table_id, sched_mode, 
					 from_field, contact, anti_spam, optout_msg) ";
			$query .= " VALUES 
					('".$user_id."', '".$parent_id."', '".$cid."', '".$name."', '".$table."', ".$table_id.", 'hours', 
					'".$this->stripNonAlphaNum($company)." <".$email.">', '".$contact."', 
					'You\'re receiving this email because you submitted your information to the contact form on one of our web pages.', 'Click here to stop receiving emails from us.') ";
			$result = $this->query($query);
			
			$result2 = $this->getAutoResponders($table_id, $table, $user_id);
			if($result2 && $this->numRows($result2)){
				while($row = $this->fetchArray($result2)){
					$html .= $this->autoResponderDashUpdate2($row, NULL, $type);
				}
				return $html;
			}
		}
		else return false;
	}
	
	/******************************************************
	// Get contact info from an item
	/******************************************************/
	function getContactInfo($table, $table_id){
		if($table=="articles" || $table=="press_release") $contact_field = "author, contact";
		else if($table=="blogs") $contact_field = "company_name, phone, address, city, state";
		else $contact_field = "company_name, phone, street, city, state";
		
		$query = "SELECT ".$contact_field." FROM ".$table." WHERE id = ".$table_id;
		$contact_info = $this->queryFetch($query);
		
		if($contact_info){
			if($table=="articles" || $table=="press_release"){
				$contact = '<p>'.$contact_info['author'].'<br />'.$contact_info['contact'].'</p>';
			}
			else {
				$contact = '<p>'.addslashes($contact_info['company_name']).'<br />';
				if($table=="blogs") $contact .= $contact_info['address'].' ';
				else $contact .= $contact_info['street'].' ';
				if($contact_info['city'] && $contact_info['state'])
					$contact .= $contact_info['city'].', '.$contact_info['state'].'<br />';
				else $contact .= "<br />";
				$contact .= $contact_info['phone'].'</p>';
			}
		}
		else $contact = "";
		return $contact;
	}
	
	/******************************************************
	// Function to get info from the "user" table by an item's ID
	/******************************************************/
	function getUserInfoByItemID($fields, $table, $table_id){
		$query = "SELECT ".$fields." FROM users WHERE users.id IN";
		$query .= " (SELECT user_id FROM ".$table." WHERE ".$table.".id = ".$table_id.")";
		$result = $this->queryFetch($query);
		return $result;
	}
	
	/******************************************************
	// Get users
	/******************************************************/
	function getUsers($parent_id='',$user_id='',$resellers='',$limit='',$offset='',
					  $count='',$active='',$suspended='',$canceled='', $search=''){
		if($count){
			$query = "SELECT count(id) FROM users WHERE id IS NOT NULL ";
			if($resellers) $query .= " AND super_user = 1 ";
			if($parent_id) $query .= " AND parent_id = '".$parent_id."' ";
			if($active) $query .= " AND access = 1 AND canceled = 0 ";
			if($suspended) $query .= " AND access = 0 AND canceled = 0 ";
			if($canceled) $query .= " AND canceled = 1";
			if($search){
				$query .= 
				" AND (
						((SELECT CONCAT(firstname,' ',lastname) FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						(users.username LIKE '%".$search."%') OR
						((SELECT name FROM user_class WHERE user_class.id = users.class) LIKE '%".$search."%') OR
						((SELECT phone FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT company FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT zip FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT social FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT address FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT city FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT state FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_contact_name FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_company_name FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_phone_field FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT website_url FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_address FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_city FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_state FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_zip FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT referral_id FROM user_info WHERE user_id = users.id) LIKE '%".$search."%')
					) ";
			}
			$result = $this->queryFetch($query);
			return $result['count(id)'];
		}
		else {
			$query = "
				SELECT 
					user_info.id AS user_info_id, 
					user_info.user_id,
					user_info.firstname,
					user_info.lastname,
					user_info.email,
					user_info.phone,
					user_info.company,
					user_info.address,
					user_info.address2,
					user_info.city,
					user_info.state,
					user_info.zip, 
					user_info.biz_contact_name, 
					user_info.biz_company_name, 
					user_info.biz_phone_field,
					user_info.website_url,
					user_info.biz_address, 
					user_info.biz_city, 
					user_info.biz_state,
					user_info.biz_zip,
					user_info.referral_id,
	
					users.id,
					users.username, ";
			if($user_id)
			$query .= "users.password, ";
			$query .= "users.parent_id,
					users.access,
					users.canceled,
					users.client_access,
					users.class,
					users.super_user,
					users.manage_user_id, 
					users.emails_sent, 
					users.marketing_optout, 
					users.created,
					users.upgraded, 
					users.last_login
				FROM 
					user_info, users";
			if($user_id){
				$query .= " WHERE user_info.user_id = '{$user_id}' 
						  AND users.id = '{$user_id}'";
			}
			else{
				$query .= " WHERE user_info.user_id = users.id";
			}
			if($parent_id) $query .= " AND users.parent_id = {$parent_id} ";
			if($resellers) $query .= " AND users.super_user = 1 ";
			if($active) $query .= " AND access = 1 AND canceled = 0 ";
			if($suspended) $query .= " AND access = 0 AND canceled = 0 ";
			if($canceled) $query .= " AND canceled = 1 ";
			if($search){
				$query .= 
				" AND (
						((SELECT CONCAT(firstname,' ',lastname) FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR 
						(users.username LIKE '%".$search."%') OR
						((SELECT name FROM user_class WHERE user_class.id = users.class) LIKE '%".$search."%') OR
						((SELECT phone FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT company FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT zip FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT social FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT address FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT city FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT state FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_contact_name FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_company_name FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_phone_field FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT website_url FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_address FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_city FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_state FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT biz_zip FROM user_info WHERE user_id = users.id) LIKE '%".$search."%') OR
						((SELECT referral_id FROM user_info WHERE user_id = users.id) LIKE '%".$search."%')
					) ";
			}
			$query .= " ORDER BY users.id DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			
			if($user_id) return $this->queryFetch($query);
			else return $this->query($query);
		}
		
	}
	
	/******************************************************
	// Display users
	/******************************************************/
	function displayUsers($page='', $parent_id='', $limit='', $offset='', $activeOnly='', $suspended='', $canceled='', $search=''){
		//get results and total count
		$results = $this->getUsers($parent_id, NULL, NULL, $limit, $offset, NULL, $activeOnly, $suspended, $canceled, $search);
		$numUsers = $this->getUsers($parent_id, NULL, NULL, NULL, NULL, true, $activeOnly, $suspended, $canceled, $search);
		if($parent_id){
			//get reseller's products and store in an array
			require_once QUBEADMIN . 'inc/reseller.class.php';
			if($prods = Reseller::getResellerProducts($parent_id)){
				$products = array();
				while($prodRow = $this->fetchArray($prods)){
					$products[$prodRow['id']] = $prodRow['name'];
				}
			}
		}
		
		if($numUsers){
			if(($limit && ($numUsers > $limit)) && !$search){
				//pagination
				$numPages = ceil($numUsers/$limit);
				if($offset>0) $currPage = ($offset/$limit)+1;
				else $currPage = 1;
				echo '<p style="font-weight:bold;">';
				if($currPage > 1){
					echo '<a href="'.$page.'?offset=0';
					if($parent_id) echo '&parent_id='.$parent_id;
					echo '"><<| start</a>&nbsp;&nbsp;&nbsp;';
					echo '<a href="'.$page.'?offset='.($offset-$limit);
					if($parent_id) echo '&parent_id='.$parent_id;
					echo '">< prev</a> ';
				}
				if($currPage>1 && $currPage<$numPages){
					echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				if($currPage < $numPages){
					echo ' <a href="'.$page.'?offset='.($offset + $limit);
					if($parent_id) echo '&parent_id='.$parent_id;
					echo '">next ></a>&nbsp;&nbsp;&nbsp;';
					echo ' <a href="'.$page.'?offset='.(($numPages*$limit)-$limit);
					if($parent_id) echo '&parent_id='.$parent_id;
					echo '">end |>></a>';
				}
				echo '</p><br />';
			}
			while($row = $this->fetchArray($results)){
				if($row['referral_id'] && is_numeric($row['referral_id'])){
					//get referrer info
					$query = "SELECT firstname, lastname FROM user_info WHERE user_id = '".$row['referral_id']."'";
					$referrer = $this->queryFetch($query);
				}
				else $referrer = NULL;
				echo '
					<ul class="prospects">
					<li class="name3"><small>#'.$row['user_id'].'</small> '.$row['firstname'].' '.$row['lastname'].'</li>
					<li class="email2"><a href="'.$row['username'].'" class="email-prospect" id="'.$row['user_id'].'" name="'.$row['company'].'" rel="user">'.$row['username'].'</a></li>
					<li class="user-links">
						<a href="#" class="user-details" rel="'.$row['user_id'].'">Details</a> |
						<a href="inc/forms/edit-user.php?id='.$row['user_id'].'">Edit</a> | 
						<a href="#" class="manage-user" rel="'.$row['user_id'].'">Manage</a> | 
						<a href="#" class="del-user" rel="'.$row['user_id'].'">Delete</a>
					</li>
					<li class="comment" id="user-details-'.$row['user_id'].'">
						<p><strong>Contact Information</strong><br /><br /></p>';
				if($row['company']) echo '<p>Company: '.$row['company'].'</p>';
				if($row['phone']) echo '<p>Phone: '.$row['phone'].'</p>';
				if($row['address']) echo '<p>Address:<br />'.$row['address'];
				if($row['address2']) echo '<br />'.$row['address2'];
				echo 	'</p>';			
				if($row['city'] || $row['state'] || $row['zip']) echo '<p>'.$row['city'].', '.$row['state'].' '.$row['zip'].'</p>';

				if($row['biz_contact_name'] || $row['biz_company_name'] || $row['biz_phone_field'] || $row['website_url'] || $row['biz_address'] || $row['biz_city'] || $row['biz_state'] || $row['biz_zip']) echo '<br /><p><strong>Business Information</strong><br /><br /></p>
						<p>Contact Name: '.$row['biz_contact_name'].'</p>
						<p>Company Name: '.$row['biz_company_name'].'</p>
						<p>Company Phone Number: '.$row['biz_phone_field'].'</p>
						<p>Website URL: '.$row['website_url'].'</p>
						<p>Business Address: '.$row['biz_address'].'<br />
						'.$row['biz_city'].', '.$row['biz_state'].' '.$row['biz_zip'].'</p>';
				echo	'<br /><p><strong>Additional Information</strong><br /><br /></p>
						<p>Created: '.date('F j, Y', strtotime($row['created'])).
						' at '.date('g:ia', strtotime($row['created'])).'</p>
						<p>Last Login: ';
				if($row['last_login'] != '0000-00-00 00:00:00'){
					echo date('F j, Y', strtotime($row['last_login'])).' at '.date('g:ia', strtotime($row['last_login']));
				} else {
					echo 'Never';
				}
				echo 	'</p>';
				if(!$parent_id){
					echo '<p>Class: '.$row['class'].'</p>';
					echo '<p>Parent ID: '.$row['parent_id'].'</p>';
				} else if($row['class']!=18) {
					echo '<p>Product: '.$products[$row['class']].'</p>';
				}
				if($referrer) echo	'<p>Referred By: '.$referrer['firstname'].' '.$referrer['lastname'].' <small>(User #'.$row['referral_id'].')</small></p>';
				echo 	'<p><a href="#" class="user-details" rel="'.$row['user_id'].'"><b>Close</b></a></p>
					</li>
					</ul>';
			} //end while($row = $this->fetchArray($results))
		} //end if($numUsers)
		else {
			echo 'No users to display.';
			if($suspended)
				echo '<br />To suspend a user go to Active Users, click Edit on the user, and set Access to "No".';
			if($canceled)
				echo '<br />To cancel a user go to Active or Suspended Users, click Edit on the user, and set Canceled to "Yes".';
		}
	}
	
	/******************************************************
	// Get Prospects.  Returns formatted HTML results if specific 
	// type is given, or result array if type is "all"
	/******************************************************/
	function getProspects($user_id='', $type='', $limit='', $offset='', $count='', 
							$type_id='', $exclude='', $limited='', $parent_id='', $cid=''){
		if($count){
			$query = "SELECT count(id)  
					FROM contact_form 
					WHERE trashed = '0000-00-00' and spamstatus = 0 ";
			if($user_id) $query .= " AND user_id = '".$user_id."'";
			if($parent_id) $query .= " AND parent_id = '".$parent_id."'";
			if($type) $query .= " AND type = '".$type."'";
			if($type_id) $query .= " AND type_id = ".$type_id."'";
			else if($exclude) $query .= " AND type_id != '".$exclude."'";
			if($cid) $query .= " AND cid = '".$cid."' ";
			if($limit) $query .= " LIMIT ".$limit;
			$result = $this->queryFetch($query);
			return $result['count(id)'];
		}
		else {
			if($limited) $query = "SELECT id, type, name, email, comments, page, keyword, search_engine, responded ";
			else $query = "SELECT * ";
			$query .= "FROM contact_form 
						WHERE trashed = '0000-00-00' and spamstatus=0 ";
			if($user_id) $query .= " AND user_id = '".$user_id."'";
			if($parent_id) $query .= " AND parent_id = '".$parent_id."'";
			if($type) $query .= " AND type = '".$type."'";
			if($type_id) $query .= " AND type_id = '".$type_id."'";
			else if($exclude) $query .= " AND type_id != '".$exclude."'";
			if($cid) $query .= " AND cid = '".$cid."' ";
			$query .= " ORDER BY created DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			$result = $this->query($query);
			
			return $result;
		}
	}
	
	/******************************************************
	// Display prospects
	/******************************************************/	
	function displayProspects($user_id = '', $type = '', $format = '', $limit = '', $offset = '', 
							  $page = '', $type_id = '', $exclude = '', $countLimit = '', $parent_id='', $cid=''){
		$limited = ($format=="last5-dash") ? 1 : 0;
		$numProspects = $this->getProspects($user_id, $type, $countLimit, NULL, true, $type_id, $exclude, NULL, $parent_id, $cid);
		
		if($numProspects){
			$prospects = $this->getProspects($user_id, $type, $limit, $offset, NULL, $type_id, $exclude, $limited, $parent_id, $cid);
			if(!$format || $format=='admin' || $format=='dashboard'){
				if($limit && ($numProspects > $limit)){
					//pagination
					$numPages = ceil($numProspects/$limit);
					if($offset>0) $currPage = ($offset/$limit)+1;
					else $currPage = 1;
					echo '<p style="font-weight:bold;">';
					if($currPage > 1){
						echo '<a title="Start of Prospects" href="'.$page.'?offset=0';
						echo '"><img src="img/v3/nav-start-icon.jpg" border="0" /></a>&nbsp;';
						echo '<a title="Previous Set of Prospects" href="'.$page.'?offset='.($offset-$limit);
						echo '"><img src="img/v3/nav-previous-icon.jpg" border="0" /></a> ';
					}
					if($currPage>1 && $currPage<$numPages){
						echo '';
					}
					if($currPage < $numPages){
						echo ' <a title="Next Set of Prospects" href="'.$page.'?offset='.($offset + $limit);
						echo '"><img src="img/v3/nav-next-icon.jpg" border="0" /></a>';
						echo ' <a title="End of Prospects" href="'.$page.'?offset='.(($numPages*$limit)-$limit);
						echo '"><img src="img/v3/nav-end-icon.jpg" border="0" /></a>';
					}
					echo '</p><br />';
				}
				while($row = $this->fetchArray($prospects)){
					$dt1 = date("F j, Y", strtotime($row['timestamp']));
					$dt2 = date("g:ia", strtotime($row['timestamp']));
					if($row['type']=='hub' || $row['type']=='blogs')
						$itemInfo = '<b>Website:</b> <a href="'.$row['website'].'" target="_blank" class="dflt-link">'.$row['website'].'</a>';
					else{
						if($row['website']!='' && $row['website']!='/') $link = $row['website']; else $link = '#';
						$itemInfo = '<b>Listing:</b> <a href="'.$link.'" target="_blank" class="dflt-link">'.$row['page'].'</a>';
					}
					
					if(!$format){
						$html .= '<ul class="prospects"';
								if($row['responded']==0) $html .= ' style="font-weight:bold;"';
						$html .=	' title="prospect" id="prospect'.$row['id'].'">
								<li class="name">'.$row['name'].'</li>
								<li class="email"><a href="'.$row['email'].'" class="email-prospect" id="'.$row['id'].'" name="'.$row['name'].'" rel="prospect">'.$row['email'].'</a></li>
								<li class="phone">'.$row['phone'].'</li>
								<li class="date">'.$dt1.'</li>
								<div class="iconWrap">
								<li class="details"><a href="#" title="View Details" class="prospect-details" rel="'.$row['id'].'"><img src="img/v3/view-details.png" border="0" /></a></li>
								<li class="delete"><a href="#" title="Delete" class="delete rmParent" rel="table=contact_form&id='.$row['id'].'"><img src="img/v3/x-mark.png" border="0" /></a></li>
								</div>
								<li class="comment" id="prospect-details-'.$row['id'].'">
									<p style="font-weight:normal;">'.$itemInfo.'</p><br />
									<p style="font-style:italic;"><small><b>'.$dt1.'</b> at <b>'.$dt2.'</b> from <b>'.$row['ip'].'</b></small></p>';
									if($row['search_engine']) $html .= '<p id="'.$row['id'].'-search_engine"><strong>Search Engine: </strong>'.$row['search_engine'].'</p>';
									if($row['keyword']) $html .= '<p id="'.$row['id'].'-keyword"><strong>Keyword: </strong>'.$row['keyword'].'</p>';
						$html .=	'<p id="'.$row['id'].'-responded"><strong>Responded:</strong> '.$row['responded'].'<br />
									<p id="'.$row['id'].'-optout"><strong>Opted out:</strong> '.$row['optout'].'<br /><br />
									<p id="'.$row['id'].'-comment"><strong>Comments: </strong>'.$row['comments'].'</p>
								</li>
							</ul>';
						$ids .= $row['id'].",";
					}
					else if($format=='admin'){
						$html .= '<ul class="prospects" title="prospect" id="prospect'.$row['id'].'">
								<li class="name">'.$row['name'].'</li>
								<li class="email"><a href="'.$row['email'].'" class="email-prospect" id="'.$row['id'].'" name="'.$row['name'].'" rel="prospect">'.$row['email'].'</a></li>
								<li class="phone">'.$row['phone'].'</li>
								<li class="date">'.$dt1.'</li>
								<div class="iconWrap">
								<li class="details"><a href="#" title="View Details" class="prospect-details" rel="'.$row['id'].'"><img src="img/v3/view-details.png" border="0" /></a></li>
								<li class="delete"><a href="#" title="Delete" class="delete" rel="table=contact_form&id='.$row['id'].'"><img src="img/v3/x-mark.png" border="0" /></a></li>
								</div>
								<li class="comment" id="prospect-details-'.$row['id'].'">
									<p style="font-style:italic;"><small><b>'.$dt1.'</b> at <b>'.$dt2.'</b> from <b>'.$row['ip'].'</b></small></p>
									<p id="'.$row['id'].'-comment">'.$row['comments'].'</p><br />
									<p>
									<b>Prospect id:</b> '.$row['id'].'<br />
									<b>User id:</b> '.$row['user_id'].'<br />
									<b>Type/id:</b> '.$row['type'].' #'.$row['type_id'].' - '.$row['page'].'<br />
									<b>Website:</b> '.$row['website'].'<br />
									<b>Responded:</b> '.$row['responded'].'<br />
									<b>Opted out:</b> '.$row['optout'].'<br />
								</li>
							</ul>';
					}
					else if($format=='dashboard'){
						$html .= '<ul class="prospects"';
								if($row['responded']==0) $html .= ' style="font-weight:bold;"';
						$html .=	' title="prospect" id="prospect'.$row['id'].'">
								<li class="name">'.$row['name'].'</li>
								<li class="email"><a href="'.$row['email'].'" class="email-prospect" id="'.$row['id'].'" name="'.$row['name'].'" rel="prospect">'.$row['email'].'</a></li>
								<li class="phone">'.$row['phone'].'</li>
								<li class="date">'.$dt1.'</li>
								<div class="iconWrap">
								<li class="details"><a href="#" title="View Details" class="prospect-details" rel="'.$row['id'].'"><img src="img/v3/view-details.png" border="0" /></a></li>
								<li class="delete"><a href="#" title="Delete" class="delete" rel="table=contact_form&id='.$row['id'].'"><img src="img/v3/x-mark.png" border="0" /></a></li>
								</div>
								<li class="comment" id="prospect-details-'.$row['id'].'">
									<p style="font-weight:normal;">'.$itemInfo.'</p><br />
									<p style="font-style:italic;"><small><b>'.$dt1.'</b> at <b>'.$dt2.'</b> from <b>'.$row['ip'].'</b></small></p>';
									if($row['search_engine']) $html .= '<p id="'.$row['id'].'-search_engine"><strong>Search Engine: </strong>'.$row['search_engine'].'</p>';
									if($row['keyword']) $html .= '<p id="'.$row['id'].'-keyword"><strong>Keyword: </strong>'.$row['keyword'].'</p>';
						$html .=	'<p id="'.$row['id'].'-responded"><strong>Responded:</strong> '.$row['responded'].'<br />
									<p id="'.$row['id'].'-optout"><strong>Opted out:</strong> '.$row['optout'].'<br /><br />
									<p id="'.$row['id'].'-comment"><strong>Comments: </strong>'.$row['comments'].'</p>
								</li>
							</ul>';
						$ids .= $row['id'].",";
					}
				}
				if(!$format){
					$emailsRemaining = $this->emailsRemaining($_SESSION['user_id']);
					if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
					else $emailsRemaining = number_format($emailsRemaining);
					$ids = rtrim($ids, ","); //remove trailing comma
					$html .= '<a href="#" class="email-prospect dflt-link" id="all" name="'.$ids.
						    '">Click here to email all prospects</a><br /><br />';
					$html .= 'You have <strong>'.$emailsRemaining.'</strong> outgoing emails remaining for this month.';
				}
			} //end if(!$format || $format=='admin')
			else if($format=="last5-dash"){
				while($row = $this->fetchArray($prospects)){
					if($row['type']=='direc'){ $type2 = "Directory"; $table = "directory"; }
					if($row['type']=='hub'){ $type2 = "HUB"; $table = "hub"; }
					if($row['type']=='press'){ $type2 = "Press Release"; $table = "press_release"; }
					if($row['type']=='blogs'){ $type2 = "Blog"; $table = "blogs"; }
					if($row['type']=='artic'){ $type2 = "Article"; $table = "articles"; }
					
					$html .= '<ul class="prospects"';
							if($row['responded']==0) $html .= ' style="font-weight:bold;"';
					$html .=	'>
								<li class="name2" style="width:120px;"><a href="'.$row['email'].'" class="email-prospect" id="'.$row['id'].'" name="'.$row['name'].'" rel="prospect">'.$row['name'].'</a></li>
								<li class="type">'.$type2.'</li>
								<li class="page">'.$row['page'].'</li>
								<li class="comment">
								<p id="'.$row['id'].'-comment">'.$row['comments'].'</p>
								</li>
							</ul>';
				}
			}
			echo $html;
		} //end if($numProspecs) 
		else {
			echo "There are currently no prospects to display.<br /><br />";	
		}
	}
	
	/******************************************************
	// Display pagination nav
	/******************************************************/
	function displayNav($num_rows, $limit, $offset, $section){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		$sixqube->displayNav($num_rows, $limit, $offset, $section);
	}
	
	/******************************************************
	// Display pagination nav for search/browse
	/******************************************************/
	function displayNav2($num_rows, $limit, $offset, $section, $keyword, $table, $location, $parent_site){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		$sixqube->displayNav2($num_rows, $limit, $offset, $section, $keyword, $table, $location, $parent_sit);
	}
	
	/******************************************************
	// Display pagination nav for local.6qube.com
	/******************************************************/
	function displayNav3($network, $num_rows, $limit, $offset, $city, $state, $category, $parent_site){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		$sixqube->displayNav3($network, $num_rows, $limit, $offset, $city, $state, $category, $parent_site);
	}
	
	/******************************************************
	// Display trash
	/******************************************************/
	function displayTrash($table, $user_id, $admin = ''){
		$userField = 'user_id';
		switch($table):
				case 'articles':
					$title = 'name';
					$desc = 'headline';
				break;
				case 'blogs':
					$title = 'blog_title';
					$desc = 'slogan';
				break;
				case 'blog_post':
					$title = 'post_title';
					//$desc = 'slogan';
				break;
				case 'directory':
					$title = 'name';
					$desc = 'company_name';
				break;
				case 'hub';
					$title = 'name';
					$desc = 'company_name';
					if($admin){
						$add = " AND multi_user = 1 ";
					}
				break;
				case 'hub_page';
					$title = 'page_title';
					$desc = 'hub_name';
					if($admin) $add = " AND (SELECT hub.multi_user FROM hub WHERE hub.id = hub_page.hub_id) = 1 ";
					else $add = " AND (SELECT hub.multi_user FROM hub WHERE hub.id = hub_page.hub_id) != 1 ";
				break;
				case 'hub_page2';
					$title = 'page_title';
					if($admin) $add = " AND (SELECT hub.multi_user FROM hub WHERE hub.id = hub_page2.hub_id) = 1 ";
					else $add = " AND (SELECT hub.multi_user FROM hub WHERE hub.id = hub_page2.hub_id) != 1 ";
				break;
				case 'press_release';
					$title = 'name';
					$desc = 'headline';
				break;
				case 'contact_form';
					$title = 'name';
					$desc = 'page';
					if($admin){
						$add = " AND type_id = '".$_SESSION['main_site_id']."' ";
					}
				break;
				case 'auto_responders';
					$title = 'name';
					//$desc = 'subject';
				break;
				case 'resellers_network';
					$title = 'name';
					$desc = 'type';
					$userField = 'admin_user';
				break;
				case 'resellers_emailers';
					$title = 'name';
					$desc = 'subject';
					$userField = 'admin_user';
				break;
				case 'resellers_backoffice';
					$title = 'name';
					$desc = 'type';
					$userField = 'admin_user';
				break;
				case 'lead_forms';
					$title = 'name';
					//$desc = 'type';
					$userField = 'user_id';
				break;
				case 'lead_forms_emailers';
					$title = 'name';
					$userField = 'user_id';
				break;
			endswitch;
		
		$query = "SELECT id, `".$title."`";
		if($desc) $query .= ", `".$desc."`";
		$query .= " FROM `".$table."` 
				WHERE `".$table."`.trashed != '0000-00-00' ";		
		$query .= " AND ".$userField." = ".$user_id;
		if($add) $query .= $add;
		$result = $this->query($query);
		
		if($result && $this->numRows($result)){			
			while($row = $this->fetchArray($result)){
				echo '
					<div class="trash-item" id="'.$table.$row['id'].'">
						<p class="title">'.$row[$title].'</p>
						<p class="desc">'.$row[$desc].'</p>
						<p class="permdelete">
							<a href="#" class="permdelete dflt-link" id="'.$row['id'].'" title="'.$table.'">[Delete Forever]</a>
						</p>&nbsp;&nbsp;&nbsp;
						<p class="restore"><a href="#" class="restore dflt-link" id="'.$row['id'].'" title="'.$table.'">[Restore]</a></p>
					</div>
				';
			}
		}
		else {
			echo '<div class="trash-item"><p class="title">None</p></div>';
		}
	}
	/******************************************************
	// Convert url for press
	/******************************************************/
	function convertPressUrl($headline, $id){
		$headline = str_replace(",","",$headline);
		$headline = str_replace("'","",$headline);
		$headline = str_replace("?","",$headline);
		$headline = str_replace(".","",$headline);
		$headline = str_replace("!","",$headline);
		$headline = str_replace("&","and",$headline);
		$headline = str_replace(":","",$headline);
		$headline = mb_strtolower("$headline");
		$title = explode(" ", stripslashes($headline));
		$title = implode("-", $title);
		return $title.'-'.$id.'.htm';
	}
	
	/******************************************************
	// Convert keyword to url-friendly
	/******************************************************/
	static function convertKeywordUrlStatic($input, $noTrail = '', $imploder = '-'){
		$output = str_replace(",","",$input);
		$output = str_replace("'","",$output);
		$output = str_replace("?","",$output);
		$output = str_replace(".","",$output);
		$output = str_replace("!","",$output);
		$output = str_replace("&","and",$output);
		$output = str_replace("+","plus",$output);
		$output = str_replace("/","",$output);
		$output = str_replace(":","",$output);
		$output = str_replace(":","",$output);
		$output = str_replace("\$","",$output);
		$output = mb_strtolower("$output");
		$output = explode(" ", stripslashes($output));
		$output = implode($imploder, $output);
		if($noTrail) return $output;
		else return $output.'/';
	}
	
	function convertKeywordUrl($input, $noTrail = '', $imploder = '-'){
		return self::convertKeywordUrlStatic($input, $noTrail, $imploder);
	}
	
	/******************************************************
	// Convert part of url to reader friendly
	/******************************************************/
	function decodeUrl($url){
		$title = explode("-", stripslashes($url));
		$title = implode(" ", $title);
		return $title;
	}
	
	/******************************************************
	// Shorten text
	/******************************************************/
	function shortenSummary($text, $len){
		$text = strip_tags($text);
		if(strlen($text) >= $len){
			$text = substr($text, 0, $len);
			return $text.'...';
		}
		else {
			return $text;
		}
	}
	/******************************************************
	// Shorten title
	/******************************************************/
	function shortenTitle($text, $len){
		$text = strip_tags($text);
		if(strlen($text) >= $len){
			$text = substr($text, 0, $len);
			return $text.'...';
		}
		else return $text;
	}
	
	/******************************************************
	// Function to create folders for user in 6qube.com/users/
	/******************************************************/
	function createUserFolders($user, $reseller = '', $justcheck=false){
            
            
//		$user_dir = QUBEROOT . 'users/' .$user;
		
		$old = umask(0); //temporarily escalate chmod priveleges
		//user directory
//		if(!is_dir($user_dir)) mkdir($user_dir, 0777) ? $errors += 0 : $errors += 1;
                
                $user_dir   =   UserModel::CreateUserDirectory(QUBEROOT . 'users', $user);  // create a user directory
                
                // make backward compatible symlink
                // 
                // 
		if(!is_dir(QUBEROOT . 'users/' . $user))
                symlink(UserModel::GetUserSubDirectory($user), QUBEROOT . 'users/' . $user);    // jFFFFF/users/$user -> users/uxx/xxxx
                //end

		//directory
		if(!is_dir($user_dir.'/directory')) mkdir($user_dir.'/directory', 0777) ? $errors += 0 : $errors += 1;
		//hub
		if(!is_dir($user_dir.'/hub')) mkdir($user_dir.'/hub', 0777) ? $errors += 0 : $errors += 1;
		//hub page
		if(!is_dir($user_dir.'/hub_page')) mkdir($user_dir.'/hub_page', 0777) ? $errors += 0 : $errors += 1;
		//press
		if(!is_dir($user_dir.'/press_release')) mkdir($user_dir.'/press_release', 0777) ? $errors += 0 : $errors += 1;
		//articles
		if(!is_dir($user_dir.'/articles')) mkdir($user_dir.'/articles', 0777) ? $errors += 0 : $errors += 1;
		//blog
		if(!is_dir($user_dir.'/blogs')) mkdir($user_dir.'/blogs', 0777) ? $errors += 0 : $errors += 1;
		//blog_post
		if(!is_dir($user_dir.'/blog_post')) mkdir($user_dir.'/blog_post', 0777) ? $errors += 0 : $errors += 1;
		//user_info
		if(!is_dir($user_dir.'/user_info')) mkdir($user_dir.'/user_info', 0777) ? $errors += 0 : $errors += 1;
		//graphImgs
		if(!is_dir($user_dir.'/graph_imgs')) mkdir($user_dir.'/graph_imgs', 0777) ? $errors += 0 : $errors += 1;
		if($reseller){
			//reseller
			if(!is_dir($user_dir.'/reseller')) mkdir($user_dir.'/reseller', 0777) ? $errors += 0 : $errors += 1;
			if(!is_dir(QUBEROOT . 'themes/'.$user.'/')){
				mkdir(QUBEROOT . 'themes/'.$user.'/', 0777) ? $errors += 0 : $errors += 1;
				mkdir(QUBEROOT . 'themes/'.$user.'/jqueryui/',0777) ? $errors += 0 : $errors += 1;
			}
			if(!is_dir(QUBEROOT . 'reseller/'.$user.'/')) 
				mkdir(QUBEROOT . 'reseller/'.$user.'/', 0777) ? $errors += 0 : $errors += 1;
			if(!is_dir('/home/thepartn/public_html/reseller/'.$user.'/')) 
				mkdir('/home/thepartn/public_html/reseller/'.$user.'/', 0777) ? $errors += 0 : $errors += 1;
		}
		umask($old); //set chmod back
		if($justcheck) return;

		//also use this function to email admin about new user
		//get user info first
		$query = "SELECT parent_id, username, class, signup_ip FROM users WHERE id = '".$user."'";
		$userInfo = $this->queryFetch($query, NULL, 1);
		$query = "SELECT * FROM user_info WHERE user_id = '".$user."'";
		$userInfo2 = $this->queryFetch($query, NULL, 1);
		if(!$userInfo2['company']) $company = $userInfo2['biz_company_name'];
		else $company = $userInfo2['company'];
		if(!$userInfo2['address']) $address = $userInfo2['biz_address'];
		else $address = $userInfo2['address'];
		if(!$userInfo2['city']) $city = $userInfo2['biz_city'];
		else $city = $userInfo2['city'];
		if(!$userInfo2['state']) $state = $userInfo2['biz_state'];
		else $state = $userInfo2['state'];
		$query = "SELECT name FROM user_class WHERE id = '".$userInfo['class']."'";
		$userClass = $this->queryFetch($query, NULL, 1);
		if(!$userInfo['parent_id'] || $userInfo['parent_id']==7){
			$to = 'admin@6qube.com';
		} else {
			$query = "SELECT username FROM users WHERE id = '".$userInfo['parent_id']."'";
			$parentInfo = $this->queryFetch($query, NULL, 1);
			$to = $parentInfo['username'];
			//if new user is a reseller client also look to see if they were referred by another user
			if($userInfo2['referral_id']){
				$query = "SELECT username FROM users WHERE id = '".$userInfo2['referral_id']."'";
				$referInfo = $this->queryFetch($query, NULL, 1);
				if($referInfo['username'] && $referInfo['username']!=$to) $to2 = $referInfo['username'];
				//get parent reseller support email
				$query = "SELECT support_email FROM resellers WHERE admin_user = '".$userInfo['parent_id']."'";
				$parentInfo2 = $this->queryFetch($query, NULL, 1);
				$from2 = $parentInfo2['support_email'] ? $parentInfo2['support_email'] : $parentInfo['username'];
			}
		}
		$from = 'noreply@6qube.com';
		$subject = 'New User Created - '.$userInfo['username'];
		$message = 
		'<h1>A New User Account Has Been Created</h1>
		<p><strong>Account Details:</strong><br />
		<strong>Email Address:</strong> '.$userInfo['username'].'<br />
		<strong>Name:</strong> '.$userInfo2['firstname'].' '.$userInfo2['lastname'].'<br />
		<strong>Company:</strong> '.$company.'<br />
		<strong>Address:</strong><br />
		'.$address.'<br />';
		if($userInfo2['address2']) $message .= $userInfo2['address2'].'<br />';
		$message .= 
		$city.', '.$state.' '.$userInfo2['zip'].'<br />
		<strong>Phone:</strong> '.$userInfo2['phone'].'<br />
		<strong>Product:</strong> '.$userClass['name'].'<br /><br />';
		if($userInfo['referral_id']) $message .= '<strong>Referred by:</strong> '.$to2.' (#'.$userInfo2['referral_id'].')<br /><br />';
		if($userInfo['signup_ip']) $message .= '<small>Signup IP: '.$userInfo['signup_ip'].'</small>';
		$this->sendMail($to, $subject, $message, NULL, $from, NULL, 1);
		if($to2){
			$subject2 = 'You Referred A New User! - '.$userInfo['username'];
			$this->sendMail($to2, $subject2, $message, NULL, $from2, NULL, 1);
		}
		
		if($errors){
			return false;
		} else {
			return true;
		}
	}
	
	/******************************************************
	// Function to delete a folder and its contents
	/******************************************************/
	function deleteFolder($directory, $empty = false){
		$continue = false;
		if(substr($dir, -1)=='/') $dir = substr($dir, 0, -1);
		$allowedDelBase = 
		array(QUBEROOT . 'users/',
			QUBEROOT . 'hubs/domains/',
			QUBEROOT . 'blogs/domains/');
			
		foreach($allowedDelBase as $key=>$value){
			if($dir != $value){
				$strlen = strlen($value);
				if(substr($dir, 0, $strlen)==$value) $continue = true;
			}
			else $continue = false;
		}
		
		if($continue){
			if(is_dir($dir)){
				$objects = scandir($dir);
				foreach($objects as $object){
					if($object != "." && $object != ".."){
						if(filetype($dir."/".$object) == "dir") $this->delDir($dir."/".$object); 
						else unlink($dir."/".$object);
					}
				}
				reset($objects);
				if(!$empty) rmdir($dir);
			}
		}
	}
	
	/******************************************************
	// Get limits for user class
	/******************************************************/
	function getLimits($uid, $type = '', $class = ''){
		if($type)
			$query = "SELECT ".$type."_limit ";
		else 
			$query = "SELECT 
					campaign_limit, directory_limit, hub_limit, hub_dupe_limit, landing_limit, 
					social_limit, press_limit, article_limit, blog_limit, responder_limit, email_limit ";
		
		if(!$class)
			$query .= " FROM user_class WHERE id = (SELECT class FROM users WHERE id = '".$uid."')";
		else
			$query .= " FROM user_class WHERE id = '".$class."'";
		$result = $this->queryFetch($query);
		
		if($result){
			if($type){
				$type .= "_limit";
				return $result[$type];
			}
			else {
				$result['user_class'] = $class;
				return $result;
			}
		}
		else {
			return false;
		}
	}
	
	/**********************************************************************/
	// function: Billing API
	// calls billing API with passed array and returns results
	//
	// Accepted Input: postfields array (without username and password)
	/**********************************************************************/
	static function billAPIStatic($postfields, $raw = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
                return SixQube::billAPIStatic($postfields, $raw);
	}
        
        function billAPI($postfields, $raw  =   ''){
            return self::billAPIStatic($postfields, $raw);
        }
	 
	/**********************************************************************/
	// function: Get View Count
	// returns the number of views, via Piwik, that a Press Release has received
	//
	// Accepted Input: valid tracking_id number
	/**********************************************************************/
	function getViewCount($tracking_id, $period = ''){
		return rand(10, 100);	// temp fix for press.6qube
		if($tracking_id != 0){
			if($period){
				$url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&idSite=".$tracking_id."&period=".$period."&date=today&format=PHP&token_auth=b00ce9652e919198e1c4daf7bf5aed5a";
				$fetched = file_get_contents($url);
				return $result = unserialize($fetched);
			}
			else {
				$url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&idSite=".$tracking_id."&period=year&date=today&format=PHP&token_auth=b00ce9652e919198e1c4daf7bf5aed5a";
				$fetched = file_get_contents($url);
				$year = unserialize($fetched);
				//due to a glitch in Piwik, period=year&date=today doesn't include the results from today,
				//so get that separately and add them together:
				$url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&idSite=".$tracking_id."&period=day&date=today&format=PHP&token_auth=b00ce9652e919198e1c4daf7bf5aed5a";
				$fetched = file_get_contents($url);
				$today = unserialize($fetched);
				
				return $year + $today;
			}
		} else {
			return rand(10,100);	
		}
	}	
	
	/******************************************************
	// Updates and sends back html to update directory 
	// section of dashboard
	/******************************************************/
	function directoryDashUpdate($id, $name, $tracking = '', $display = ''){
		$settings = $this->getSettings();
		//get user_id for graph img
		$query = "SELECT user_id FROM directory WHERE id = {$id}";
		$result = $this->queryFetch($query);
		$user_id = $result['user_id'];
		if($_SESSION['theme']) $previewUrl = 'http://local.'.$_SESSION['main_site'].'/';
		else $previewUrl = 'http://local.6qube.com/';
		
		if(!$display) $display = 'none';
		if(!$tracking || $tracking==0) $tracking = 26;
		$html = '<div class="item" style="display:'.$display.';">
				<div class="icons">
				<a href="'.$previewUrl.'directory.php?id='.$id.'" target="_new" class="view" rev="directory"><span>View</span></a>
				<a href="directory.php?form=listings&id='.$id.'" class="edit" rev="directory"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=directory&id='.$id.'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('directory', $id, $tracking, $user_id).'" class="graph trigger" />';
		$html .= '<a href="directory.php?form=listings&id='.$id.'" rev="directory">'.$name.'</a></div>';
		return $html;
	}
	
	/******************************************************
	// Updates and sends back html to update hub 
	// section of dashboard
	/******************************************************/
	function hubDashUpdate($id, $name, $user_id, $tracking = '', $display = ''){
		$settings = $this->getSettings();
		//get user_id for graph img
		$query = "SELECT user_id FROM hub WHERE id = {$id}";
		$result = $this->queryFetch($query);
		$user_id = $result['user_id'];
		
		if(!$display) $display = 'none';
		if(!$tracking || $tracking==0) $tracking = 26;
		$html = '<div class="item" style="display:'.$display.';">
				<div class="icons">
				<a href="http://'.$_SESSION['main_site'].'/hub-preview.php?id='.$id.'&uid='.$user_id.'" target="_new" class="view"" title="Preview"><span>View</span></a>
				<a href="hub.php?form=settings&id='.$id.'" class="edit" title="Edit"><span>Edit</span></a>
				<a href="#" class="delete rmParent" rel="table=hub&id='.$id.'" title="Delete"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('hub', $id, $tracking, $user_id).'" class="graph trigger" />';
		$html .= '<a href="hub.php?form=settings&id='.$id.'" >'.$name.'</a></div>';
		return $html;
	}
	
	/******************************************************
	// Updates and sends back html to update press 
	// section of dashboard
	/******************************************************/
	function pressDashUpdate($id, $name, $tracking = '', $display = ''){
		$settings = $this->getSettings();
		//get user_id for graph img
		$query = "SELECT user_id FROM press_release WHERE id = {$id}";
		$result = $this->queryFetch($query);
		$user_id = $result['user_id'];
		if($_SESSION['theme']) $previewUrl = 'http://press.'.$_SESSION['main_site'].'/';
		else $previewUrl = 'http://press.6qube.com/';
		
		if(!$display) $display = 'none';
		if(!$tracking || $tracking==0) $tracking = 26;
		$html = '<div class="item" style="display:'.$display.';">
				<div class="icons">
				<a href="'.$previewUrl.'press.php?id='.$id.'" target="_new" class="view" rev="press"><span>View</span></a>
				<a href="press.php?form=settings&id='.$id.'" class="edit" rev="press"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=press_release&id='.$id.'"><span>Delete</span></a>
			</div>
			';
		$html .= '<img src="'.$this->ANAL_getGraphImg('press', $id, $tracking, $user_id).'" class="graph trigger" />';
		$html .= '<a href="press.php?form=settings&id='.$id.'" rev="press">'.$name.'</a></div>';
		return $html;
	}
	
	/******************************************************
	// Updates and sends back html to update articles 
	// section of dashboard
	/******************************************************/
	function articleDashUpdate($id, $name, $tracking = '', $display = ''){
		$settings = $this->getSettings();
		//get user_id for graph img
		$query = "SELECT user_id FROM articles WHERE id = {$id}";
		$result = $this->queryFetch($query);
		$user_id = $result['user_id'];
		if($_SESSION['theme']) $previewUrl = 'http://articles.'.$_SESSION['main_site'].'/';
		else $previewUrl = 'http://articles.6qube.com/';
		
		if(!$display) $display = 'none';
		if(!$tracking || $tracking==0) $tracking = 26;
		$html = '<div class="item" style="display:'.$display.';">
			<div class="icons">
				<a href="'.$previewUrl.'articles.php?id='.$id.'" target="_new" class="view" rev="article"><span>View</span></a>
				<a href="articles.php?form=settings&id='.$id.'" class="edit" rev="article"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=articles&id='.$id.'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('article', $id, $tracking, $user_id).'" class="graph trigger" />';
		$html .= '<a href="articles.php?form=settings&id='.$id.'" rev="article">'.$name.'</a></div>';
		return $html;
	}
	
	/******************************************************
	// Updates and sends back html to update blogs 
	// section of dashboard
	/******************************************************/
	function blogDashUpdate($id, $name, $user_id, $tracking = '', $display = ''){
		$settings = $this->getSettings();
		//get user_id for graph img
		$query = "SELECT user_id FROM blogs WHERE id = {$id}";
		$result = $this->queryFetch($query);
		$user_id = $result['user_id'];
		
		if(!$display) $display = 'none';
		if(!$tracking || $tracking==0) $tracking = 26;
		$html = '<div class="item" style="display:'.$display.';">
				<div class="icons">
				<a href="http://'.$_SESSION['main_site'].'/blog-preview.php?uid='.$user_id.'&bid='.$id.'" target="_new" class="view" rev="blog"><span>View</span></a>
				<a href="blog.php?form=settings&id='.$id.'" class="edit" rev="blog"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=blogs&id='.$id.'"><span>Delete</span></a>
			</div>';
		$html .= '<img src="'.$this->ANAL_getGraphImg('blog', $id, $tracking, $user_id).'" class="graph trigger" />';
		$html .= '<a href="blog.php?form=settings&id='.$id.'" rev="blog">'.$name.'</a></div>';
		return $html;
	}
	
	/******************************************************
	// Sends user activation email
	/******************************************************/ 
	function sendRegisterMail($id, $nomessage = '', $listingID = '', $resellerID = ''){
		//activation URL
		$salt = 'hs3rDct1v4T1oon!';
		
		$query = "SELECT * FROM users WHERE id = '".$id."'";
		$row = $this->queryFetch($query);
		if($row){
			$activationHash = md5($row['username'].$salt.$row['password']);
			$to = $row['username'];
			
			//if user is signing up from a reseller's site get correct info
			if($resellerID){
				$query = "SELECT main_site, company, logo, activation_email, activation_email_img 
						FROM resellers WHERE admin_user = ".$resellerID;
				$reseller = $this->queryFetch($query);
				//get address info
				$query = "SELECT phone, address, address2, city, state, zip FROM user_info WHERE user_id = ".$resellerID;
				$reseller2 = $this->queryFetch($query);
								
				$from = $this->stripNonAlphaNum($reseller['company'])." <no-reply@".$reseller['main_site'].">";
				$subject = $reseller['company']." User Activation";
				if($reseller['activation_email']){
					$activationUrl = 'http://'.$reseller['main_site'].
								  '/?page=activation&email='.$to.'&id='.$activationHash.'&l='.$listingID;
					$imageUrl = 'http://'.$reseller['main_site'].'/admin/themes/'.$resellerID.'/'.
							  $reseller['activation_email_img'];
					$message = str_replace('#activate', $activationUrl, stripslashes($reseller['activation_email']));
					$message = str_replace('#image', $imageUrl, $message);
				}
				else {
					$domain = $activationUrl = 'http://'.$reseller['main_site'];
					$loginUrl = 'http://'.$reseller['main_site'].'/admin/';
					
					$contact = $reseller['company'].'<br />'.$reseller2['address'];
					if($reseller2['address2']) $contact .= '<br />'.$reseller2['address2'];
					$contact .= '<br />'.$reseller2['city'].', '.$reseller2['state'].' '.$reseller2['zip'];
					if($reseller2['phone']) $contact .= '<br />'.$reseller2['phone'];
					
					$image = 'http://'.$reseller['main_site'].'/admin/themes/'.$resellerID.'/'.$reseller['logo'];
				}		
			}
			else {
				$from = "6Qube <no-reply@6qube.com>"; 
				$subject = "6Qube User Activation";
				$domain = 'http://6qube.com';
				$activationUrl = 'http://6qube.com';
				$loginUrl = 'http://6qube.com/admin/';
				$contact = 
				'Elements by 6Qube, a BlueJag Enterprises, LLC. Company
				<br />
				13740 Research Blvd., Suite D4 <br />
				Austin, TX 78750 <br />
				Toll Free Number: 877-570-5005';
				$image = 'http://6qube.com/img/activation-header.jpg';
			}
			
			if(!$message){
				//HTML message
				$message = '<html><body style="text-align:center;"><br />';
				$message .= '<a href="'.$activationUrl.'/?page=activation&email='.$to.'&id='.$activationHash.'&l='.$listingID.'" target="_blank">';
				$message .= '<img src="'.$image.'" alt="Click here to activate your account" />';
				$message .= '<br /><span style="font-weight: bold; font-size:14px; line-height:150%; color:#333333;">Click here to activate your new account</span></a>';
				$message .= '<br /><br />';
				$message .= '<table align="center" cellpadding="0" cellspacing="0" border="0" width="405">';
				$message .= '<tr>
							<td width="34" align="center"></td>
							<td width="411" style="font-family:verdana,tahoma,arial,sans-serif; font-size:13px; color:#666666;">
								<p>
								<span style="font-weight: bold; font-size:14px; line-height:150%; color:#333333;">Keep this information in a safe place:</span><br />
								<strong style="color:#369;">Your username:</strong> '.$row['username'].'<br />
								<strong style="color:#369;">Forgot your password?</strong><br />You can retrieve it from the <a href="'.$loginUrl.'" target="_blank" style="color:#c60;">login page</a>.	</p></td>
							</tr>
							<tr>
							<td colspan="2"><img src="'.$domain.'/img/line-seperator.gif" border="0"></td>
							</tr>
							<tr>
							<td colspan="2">
							<span style="font-family: Verdana, Helvetica, Arial; font-size:10px; line-height:150%; color:#666666;" style="text-align:center;">
							'.$contact.'
							<br />
							</span>
							</td>
							</tr>';
				$message .= '</table>';
				$message .= '</body></html>';
			}
		
			// To send the HTML mail we need to set the Content-type header. 
			//$headers = "From: $from\r\n";
			//$headers .= "Reply-To: $from\r\n";
			//$headers .= "MIME-Version: 1.0\r\n";
			//$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			 
			// now lets send the email 
			require_once 'Mail.php';
			$host = "relay.jangosmtp.net";
			$port = '25';
			$smtp = Mail::factory('smtp',
						array ('host' => $host,
							  'port' => $port, 
							  'auth' => false));
			
			$headers = array ('From' => $from,
						   'Reply-To' => $from,
						   'To' => $to,
						   'Subject' => $subject,
						   'Content-Type' => 'text/html; charset=ISO-8859-1',
						   'MIME-Version' => '1.0');
						   
			$mail = $smtp->send($to, $headers, $message);
			if(!PEAR::isError($mail)){
				if(!$nomessage){
					echo '<p><strong>Thank you for registering.</strong><p>You will need to check your email within 24 hours to activate your account otherwise it will be deleted!</p><br />';
				} else {
					return true;
				}
			} else {
				if(!$nomessage){
					echo "An error occured while sending your confirmation email.";
					echo $mail->getMessage();
				} else {
					return false;
				}
			}
		}
		else {
			if(!$nomessage){
				echo "An error occured while sending your confirmation email.";	
			} else {
				return false;
			}
		}
	}
	
	/******************************************************
	// Function to email admin
	/******************************************************/
	function emailAdmin($subject, $message){
		$to = "admin@6qube.com";
		$headers = "From: admin@6qube.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		if(mail($to, $subject, $message, $headers)) return true;
		else return false;
	}
	
	/**********************************************************************/
	// function: send SMTP or PHP email
	//********************************************************************/
	function sendMail($to, $subject, $message, $headers = '', $from = '', $returnErrors = '', $smtp = ''){
		if(!$smtp){
			if(mail($to, $subject, $message, $headers)) return true;
			else return false;
		}
		else {
			require_once 'Mail.php';
			$host = "relay.jangosmtp.net";
			$port = '25';
			$smtp = Mail::factory('smtp',
						array ('host' => $host,
							  'port' => $port, 
							  'auth' => false));
			if(!$headers){
				$headers = array('From' => $from,
							  'To' => $to,
							  'Subject' => $subject,
							  'Content-Type' => 'text/html; charset=ISO-8859-1',
							  'MIME-Version' => '1.0');
			}
			if(!$headers['Content-Type']) $headers['Content-Type'] = 'text/html; charset=ISO-8859-1';
			if(!$headers['MIME-Version']) $headers['MIME-Version'] = '1.0';
			
			$mail = $smtp->send($to, $headers, $message);
			
			if(!PEAR::isError($mail)){
				if($returnErrors){
					$return['success'] = true;
					return $return;
				}
				else
					return true;
			}
			else {
				if($returnErrors){
					$return['success'] = false;
					$return['errors'] = $mail->getMessage();
					return $return;
				}
				else
					return false;
			}
		}
	}
	
	/**********************************************************************/
	// function: Display Hub (NEW)
	// outputs a set of hub results in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayHub($row, $layout, $parent_site = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->displayHub($row, $layout, $parent_site);
	}
	
	/**********************************************************************/
	// function: Display Directory (NEW)
	// returns a single Directory listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function displayDirectory($row, $layout, $domain = '', $parent_site = '', $parent_company = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->displayDirectory($row, $layout, $domain, $parent_site, $parent_company);
	}
	
	/**********************************************************************/
	// function: Display Press (NEW)
	// returns a single Press Release listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayPress($row, $layout = 0, $domain = '', $parent_site = '', $parent_company = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->displayPress($row, $layout, $domain, $parent_site, $parent_company);
	}
	
	/**********************************************************************/
	// function: Display Article (NEW)
	// returns a single Hub listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayArticle($row, $layout = 0, $domain = '', $parent_site = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->displayArticle($row, $layout, $domain, $parent_site);
	}
	
	/**********************************************************************/
	// Browse functions
	// 
	/**********************************************************************/
	/******************************************************
	// Build dropdown of sections for Browse
	/******************************************************/
	function browseGetSections($selected){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->browseGetSections($selected);
	}
	
	/******************************************************
	// Build dropdown of categories for Browse
	/******************************************************/
	function browseGetCategories($table, $selected){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->browseGetCategories($table, $selected);
	}
	
	/******************************************************
	// Build dropdown of states for Browse
	/******************************************************/
	function browseGetStates($table, $category, $selected){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->browseGetStates($table, $category, $selected);
	}
	
	/******************************************************
	// Build dropdown of cities for Browse
	/******************************************************/
	function browseGetCities($table, $category, $state, $selected){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->browseGetCities($table, $category, $state, $selected);
	}
	
	/**********************************************************************/
	// function: Display Browse Results (NEW)
	// Outputs a Set of listings in new format
	//
	// Accepted Input: table (section), category, city, state
	/**********************************************************************/	 
	function displayBrowse($table, $category, $state, $city, $offset, $limit){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		$sixqube->displayBrowse($table, $category, $state, $city, $offset, $limit);
	}
	
	/**********************************************************************/
	// function: Display Search Results (NEW)
	// Outputs a Set of Search Results in new format
	//
	// Accepted Input: Category/keyword, location, limit, offset
	/**********************************************************************/	 
	function displaySearch($keyword, $location, $limit, $offset, $domain = '', $parent = '', $parent_company = ''){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		$sixqube->displaySearch($keyword, $location, $limit, $offset, $domain, $parent, $parent_company);
	}
	
	/**********************************************************************/
	// function: Search Function
	// Returns an array of search results
	//
	// Accepted Input: Category/keyword, location, loose/strict search bool
	/**********************************************************************/	
	function searchFunction($keyword, $location, $page, $loose){
		require_once QUBEADMIN . 'inc/sixqube.class.php';
		$sixqube = new SixQube();
		
		return $sixqube->searchFunction($keyword, $location, $page, $loose);
	}
	
	/**********************************************************************/
	// function: Fix Location
	// Checks inputted string, determines content/format, and returns 
	// an array with 0 key as city and 1 key as state
	//
	// Accepted Input: String
	/**********************************************************************/
	function fixLoc($location){
		//examples:
		//input:               output:
		//------------------------------
		//Austin     -->       array('Austin', '')
		//Glen Falls  -->      array('Glen Falls', '')
		//Austin, TX    -->    array('Austin', 'TX')
		//Austin, Texas  -->   array('Austin', 'TX')
		//Glen Falls, TX  -->  array('Glen Falls', 'TX')
		//TX              -->  array('', 'TX')
		//Texas           -->  array('', 'TX')
		//Austin Texas     --> array('Austin', 'TX')
		//Buffalo New York --> array('Buffalo', 'NY')
		
		if($state = $this->fixLocState($location)){  //if the string is only a state (like Texas or New York)
			$newLoc = array('', $state); //return it in an array with a blank 0 key
		} else {
			//if string is just one word and not a state, return it (like Austin):
			if(!strpbrk($location, ' ')){ //no spaces
				$newLoc = array($location, '');
			}
			//if string has at least one space but no commas (like Glen Falls or Austin Texas):
			else if(!strpbrk($location, ',')){ //no commas
				$locationArray = explode(' ', $location);
				$numWords = count($locationArray);
				if($numWords==2){  //if the string is two words
					if($state = $this->fixLocState($locationArray[1])){ 
						//if the second word is a state (like Austin Texas)
						$newLoc = array($locationArray[0], $state); //return array with city and state
					} else {
						$newLoc = array($location, '');
					}
				}
				else if($numWords==3){ //if the string is three words
					if($state = $this->fixLocState($locationArray[2])){ 
						//if the third word is a state (like Glen Falls Texas)
						$newLoc = array($locationArray[0].' '.$locationArray[1], $state); //return array
					} else if($state = $this->fixLocState($locationArray[1].' '.$locationArray[2])) {
						//if the second two words are a state (like Buffalo New York)
						$newLoc = array($locationArray[0], $state);
					} else {
						$newLoc = array($location, '');
					}
				}
				else {
					$newLoc = array($location, ''); //string isn't a state and doesnt contain one, so just return it
				}
			}
			//otherwise string must have at least one space and one comma (like Austin, Texas or Buffalo, New York)
			else {
				$locationArray = explode(',', $location); //split at comma
				$locationArray[1] = trim($locationArray[1]); //remove any leading or trailing spaces
				//check/convert everything after the comma to state abbreviation
				$state = $this->fixLocState($locationArray[1]); 
			
				$newLoc = array($locationArray[0], $state);
			}
		}
		
		return $newLoc;
	}
	//helper function for fixLoc().  checks if string is a state
	//and returns its abbreviation if so, FALSE otherwise
	function fixLocState($check){
		$statesName = array('alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'district of columbia', 'florida', 'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts', 'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new hampshire', 'new jersey', 'new mexico', 'new york', 'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode island', 'south carolina', 'south dakota', 'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west virginia', 'wisconsin', 'wyoming');
		$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
		$checkLower = strtolower($check); //convert string to lowercase
		
		if(!strpbrk($checkLower, ' ')){ //if the string doesn't have any spaces
			$check2 = $checkLower;
			$numWords = 1;
		} else { //otherwise create an array of the words
			$check2 = explode(" ", $checkLower); 
			$numWords = count($check2);
		}
		
		if($numWords==1){ //if string is one word
			if(in_array($check2, $statesName)){ //if it's a state name
				$state = array_search($check2, $statesName); //find array key of state in $stateName
				$result = strtoupper($statesAbv[$state]); //return the capitalized state abbreviation from $stateAbv
			} else if(in_array($check2, $statesAbv)){ //it it's a state abbreviation
				$result = strtoupper($check2);
			} else {
				$result = false;
			}
		}
		else if($numWords==2){
			if($check2[0]=="new" || $check2[0]=="north" || $check2[0]=="rhode" || $check2[0]=="south" || $check2[0]=="west"){
				//if the first word of the string could be at the beginning of a state name
				if(in_array($checkLower, $statesName)){ //note the use of $checkLower and not $check2
					$state = array_search($check, $statesName); //find array key of state in $stateName
					$result = strtoupper($statesAbv[$state]); //return the capitalized state abbreviation from $stateAbv
				}
			} else {
				$result = false;
			}
		}
		else if($numWords==3){
			if($check2[0]=="district"){
				$result = "DC";
			} else {
				$result = false;
			}
		}
		else { $result = false; } //if none of the above conditions were met it could not be a state
		
		if($result!=false){
			return $result;
		} else {
			return false;
		}
	}
	
	/**********************************************************************/
	// function: Major Cities
	// takes in the name of a city (like "Austin") and checks if it's 
	// a "major city". returns the city's state abbreviation if so, false if not
	//
	// Accepted Input: validated city name
	/**********************************************************************/
	function majorCity($city){
		$city = strtolower($city);
		$cities = array(
					'new york'=>'NY', 'new york city'=>'NY', 'los angeles'=>'CA', 'chicago'=>'IL', 
					'houston'=>'TX', 'phoenix'=>'AZ', 'philadelphia'=>'PA', 'san antonio'=>'TX', 
					'san diego'=>'CA', 'dallas'=>'TX', 'san jose'=>'CA', 'detroit'=>'MI', 'san francisco'=>'CA',
					'jacksonville'=>'FL', 'indianapolis'=>'IN', 'austin'=>'TX', 'columbus'=>'OH',
					'fort worth'=>'TX', 'charlotte'=>'NC', 'memphis'=>'TN', 'boston'=>'MA',
					'baltimore'=>'MD', 'el paso'=>'TX', 'chicago'=>'IL', 'seattle'=>'WA',
					'denver'=>'CO', 'nashville'=>'TN', 'milwaukee'=>'WI', 'washington'=>'DC',
					'las vegas'=>'NV', 'louisville'=>'KY', 'portland'=>'OR', 'oklahoma city'=>'OK',
					'tucson'=>'AZ', 'atlanta'=>'GA', 'albuquerque'=>'NM', 'kansas city'=>'KS',
					'fresno'=>'CA', 'mesa'=>'AZ', 'sacramento'=>'CA', 'long beach'=>'CA',
					'omaha'=>'NE', 'virginia beach'=>'VA', 'miami'=>'FL', 'cleveland'=>'OH',
					'oakland'=>'CA', 'raleigh'=>'NC', 'colorado springs'=>'CO', 'tulsa'=>'OK',
					'minneapolis'=>'MN', 'arlington'=>'VA', 'honolulu'=>'HI', 'wichita'=>'KS',
					'st. louis'=>'MO', 'new orleans'=>'LA', 'tampa'=>'FL', 'cincinnati'=>'OH',
					'pittsburgh'=>'PA', 'corpus christi'=>'TX', 'anchorage'=>'AK', 'st. paul'=>'MN',
					'newark'=>'NJ', 'buffalo'=>'NY', 'scottsdale'=>'AZ', 'orlando'=>'FL',
					'baton rouge'=>'LA', 'reno'=>'NV', 'des moines'=>'IA', 'fort lauderdale'=>'FL',
					'salt lake city'=>'UT', 'tempe'=>'AZ', 'tallahassee'=>'FL', 'boulder'=>'CO',
					);
		if(array_key_exists($city, $cities)) return $cities[$city];
		else return false;
	}
	
	/**********************************************************************/
	// Display a dropdown of US states
	/**********************************************************************/
	function stateSelect($name, $selected = '', $class = ''){
		$statesArray = $this->states();
		echo $this->buildSelect($name, $statesArray, $selected, $class);
	}
	
	/**********************************************************************/
	// Return a state's name from its abbreviation
	/**********************************************************************/
	function getStateFromAbv($abv){
		$abv = strtoupper($abv);
		$statesArray = $this->states();
		return $statesArray[$abv];
	}
	
	/**********************************************************************/
	// Return an array of US states
	/**********************************************************************/
	function states(){
		$statesArray = array (
			'US' => '--United States--',
				'AL' => 'Alabama',
	    'AK' => 'Alaska', 
	    'AZ' => 'Arizona', 
	    'AR' => 'Arkansas', 
	    'CA' => 'California', 
	    'CO' => 'Colorado', 
	    'CT' => 'Connecticut', 
	    'DE' => 'Delaware', 
	    'DC' => 'District Of Columbia', 
	    'FL' => 'Florida', 
	    'GA' => 'Georgia', 
	    'HI' => 'Hawaii', 
	    'ID' => 'Idaho', 
	    'IL' => 'Illinois', 
	    'IN' => 'Indiana', 
	    'IA' => 'Iowa', 
	    'KS' => 'Kansas', 
	    'KY' => 'Kentucky', 
	    'LA' => 'Louisiana', 
	    'ME' => 'Maine', 
	    'MD' => 'Maryland', 
	    'MA' => 'Massachusetts', 
	    'MI' => 'Michigan', 
	    'MN' => 'Minnesota', 
	    'MS' => 'Mississippi', 
	    'MO' => 'Missouri', 
	    'MT' => 'Montana',
	    'NE' => 'Nebraska',
	    'NV' => 'Nevada',
	    'NH' => 'New Hampshire',
	    'NJ' => 'New Jersey',
	    'NM' => 'New Mexico',
	    'NY' => 'New York',
	    'NC' => 'North Carolina',
	    'ND' => 'North Dakota',
	    'OH' => 'Ohio', 
	    'OK' => 'Oklahoma', 
	    'OR' => 'Oregon', 
	    'PA' => 'Pennsylvania', 
	    'RI' => 'Rhode Island', 
	    'SC' => 'South Carolina', 
	    'SD' => 'South Dakota',
	    'TN' => 'Tennessee', 
	    'TX' => 'Texas', 
	    'UT' => 'Utah', 
	    'VT' => 'Vermont', 
	    'VA' => 'Virginia', 
	    'WA' => 'Washington', 
	    'WV' => 'West Virginia', 
	    'WI' => 'Wisconsin', 
	    'WY' => 'Wyoming',

		 'CN' => '--Canadian Provinces--',
		 'AB' => 'Alberta',
		 'BC' => 'British Columbia',
 		 'MB' => 'Manitoba',
		 'NB' => 'New Brunswick',
		 'NF' => 'Newfoundland',
		 'NT' => 'Northwest Territories',
		 'NS' => 'Nova Scotia',
		 'NU' => 'Nunavut',
		 'ON' => 'Ontario',
		 'PE' => 'Prince Edward Island',
		 'QC' => 'Quebec',
		 'SK' => 'Saskatchewan',
		 'YT' => 'Yukon Territory',

		'AU' => '--Australia Territory--',
		'ACT' => 'Australian Capital Territory',
		'NSW' => 'New South Wales',
		'NTT' => 'Northern Territory',
		'QLD' => 'Queensland',
		'SA' => 'South Australia',
		'TAS' => 'Tasmania',
		'VIC' => 'Victoria',
		'WAT' => 'Western Australia'
						);
		return $statesArray;
	}
	
	/**********************************************************************/
	// function: Get City Link
	// returns html for a link to a city's page in a network
	//
	// Accepted Input: city, state, network, major city bold, include state for major
	/**********************************************************************/
	function getCityLink($city, $state, $network, $bold = '', $incState = '', $title = '', $catall = false){
		///////////////////////////////////////////////////////////////////////
		//temporary code for while transitioning networks to reseller version:/
		if(strpos($network, '.')===false) $network .= '.6qube.com';		    //
		///////////////////////////////////////////////////////////////////////
		$city = ucwords($city); //capitalize first letter(s) if not already
		if($major = $this->majorCity($city)){
			if($major == strtoupper($state)){
				if($catall){
					$link = 'http://'.$network.'/'.$this->convertKeywordUrl($city).'/';	
				} else {
					$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city).'"';
					if($title) $link .= ' title="'.$title.'"';
					$link .= '>';
					if($bold) $link .= '<b>';
					$link .= $city;
					if($incState) $link .= ', '.strtoupper($state);
					if($bold) $link .= '</b>';
					$link .= '</a>';
				}
			}
			else {
				if($catall){
					$link = 'http://'.$network.'/'.$this->convertKeywordUrl($city, true).
					  '-'.strtolower($state).'/';	
				} else {
					$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city, true).
						  '-'.strtolower($state).'/"';
					if($title) $link .= ' title="'.$title.'"';
					$link .= '>';
					$link .= $city.', '.strtoupper($state);
					$link .= '</a>';
				}
			}
		}
		else {
			if($catall){
				$link = 'http://'.$network.'/'.$this->convertKeywordUrl($city, true).
				  '-'.strtolower($state).'/';	
			} else {
				$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city,true).'-'.
						strtolower($state).'/"';
				if($title) $link .= ' title="'.$title.'"';
				$link .= '>';
				$link .= $city.', '.strtoupper($state);
				$link .= '</a>';
			}
		}
			
		return $link;
	}
	
	/**********************************************************************/
	// function: Get Category Link
	// returns html for a link to a category's page in a network
	//
	// Accepted Input: city, state, category, network, title tag
	/**********************************************************************/
	function getCatLink($city, $state, $category, $network, $title = ''){
		///////////////////////////////////////////////////////////////////////
		//temporary code for while transitioning networks to reseller version:/
		if(strpos($network, '.')===false) $network .= '.6qube.com';			 //
		///////////////////////////////////////////////////////////////////////
		if($major = $this->majorCity($city)){
			if($major == strtoupper($state)){
				$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city).
					    $this->convertKeywordUrl($category).'"';
				if($title) $link .= ' title="'.$title.'"';
				$link .= '>';
				
			}
			else {
				$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city, true).
					  '-'.strtolower($state).'/'. $this->convertKeywordUrl($category).'"';
				if($title) $link .= ' title="'.$title.'"';
				$link .= '>';
			}
		}
		else {
			$link = '<a href="http://'.$network.'/'.$this->convertKeywordUrl($city,true).'-'.
				    strtolower($state).'/'.$this->convertKeywordUrl($category).'"';
			if($title) $link .= ' title="'.$title.'"';
			$link .= '>';
		}
		$link .= ucwords($category);
		$link .= '</a>';
			
		return $link;
	}
	
	/**********************************************************************/
	// function: Display Themes (for hubs and blogs)
	// returns a set of themes for specified criteria
	/**********************************************************************/
	function displayThemes($user_id, $selected='', $section='', $userClass='', $selectClass='', $req_type='', $userParent='', $mu='', $globalMUparent=''){
		if(!$userClass){
			//get user class
			$query = "SELECT class FROM users WHERE id = '".$user_id."'";
			$userInfo = $this->queryFetch($query);
			$userClass = $userInfo['class'] ? $userInfo['class'] : 0;
		}
		//check if user has a free account
		$classExceptions = array(318);
		if(in_array($userClass, $classExceptions)) $isFreeAccount = false;
		else $isFreeAccount = $this->isFreeAccount($userClass);
		if($req_type && $req_type!=-1) $reqTypesArray = $this->parseHubReqThemeType($req_type);
		else $reqTypesArray = NULL;
		
		if($section=="network_site"){
			$table = "themes_network";
			$optgroups = NULL;
			$fields = 'id, name';
		}
		else {
			$table = "themes";
			$optgroups = 1;
			$fields = 'id, type, name';
			$order = 'type ASC, name ASC';
		}
		
		$query = "SELECT ".$fields." FROM `".$table."` WHERE id IS NOT NULL";
		if($section=="blogs") $query .= " AND blog = 1";
		if($section=="reseller") $query .= " AND reseller = 1";
		else if($section!="network_site") $query .= " AND reseller = 0";
		if($section!="network_site"){
			if(!$isFreeAccount){
				$query .= " AND (
							  (
							   (user_class_access = '".$userClass."' OR user_class_access = 0) 
							    AND 
							   (user_id_access = '".$user_id."' OR user_id_access = 0)
							  )
							 ) ";
			}
			else if($section!="reseller"){
				$query .= " AND user_class_access = -1";
				$selected = 7;
			}
		}
		if($reqTypesArray){
			if(count($reqTypesArray)>1){
				$query .= " AND (";
				foreach($reqTypesArray as $key=>$value){
					if($value!=4)
						$query .= " type = '".$value."' OR ";
					else
						$reqMU = true;
				}
				$query = substr($query, 0, -4); //trim last OR
				$query .= ") ";
			}
			else {
				$query .= " AND type = '".$reqTypesArray[0]."' ";
			}
		}
		if(@$reqMU) $query .= " AND (control_hub = 0 OR id = '".$selected."')";
		if(@$order) $query .= " ORDER BY ".$order;
		
		if($result = $this->query($query)){
			//check the user class for themes with access disabled
			$query = "SELECT disabled_themes FROM user_class WHERE id = '".$userClass."'";
			$d = $this->queryFetch($query, NULL, 1);
			if($d['disabled_themes']){
				$disabledThemes= explode('::', $d['disabled_themes']);
			}
			else $disabledThemes = array();
			//build array of themes grouped into types
			$optgroupLabels = 
			array(1=>'Full Websites', 2=>'Landing Pages', 3=>'Mobile Websites', 5=>'Social Media');
			if($mu) $optgroupLabels[4] = 'Multi-User Hub Themes';
			while($row = $this->fetchArray($result)){
				if($optgroups){
					$themeArray[$row['type']]['label'] = $optgroupLabels[$row['type']];
					if(!in_array($row['id'], $disabledThemes)) $themeArray[$row['type']][$row['id']] = $row['name'];
				}
				else if(!in_array($row['id'], $disabledThemes)) $themeArray[$row['id']] = $row['name'];
			}
			if(!$mu){
				//get parents' MU hubs to include as "themes"
				$query = "SELECT id, name FROM hub WHERE user_id = '".$userParent."' AND multi_user = 1 and trashed = 0";
				$parentMUHubs = $this->query($query);
			
				//get MU Access
				$query2 = "SELECT admin_mu FROM resellers WHERE admin_user = '".$userParent."'";
				$muInfo = $this->queryFetch($query2);
				$muAccess = $muInfo['admin_mu'];
				
				//get Global' MU hubs to include as "themes" - aka 6Qube Global Base Themes
				$query3 = "SELECT id, name FROM hub WHERE user_id = '".$globalMUparent."' AND multi_user = 1 and trashed = 0";
				$globalMUHubs = $this->query($query3);
				
				if(@$this->numRows($parentMUHubs)){
					$themeArray[4]['label'] = 'Base Websites';
					while($row2 = $this->fetchArray($parentMUHubs, 1)){
						//check if the user can access this hub
						$query = "SELECT multi_user_classes FROM hub WHERE id = '".$row2['id']."'";
						$a = $this->queryFetch($query, NULL, 1);
						if($a['multi_user_classes']){
							$b = explode('::', $a['multi_user_classes']);
							if(in_array($userClass, $b)) $disp = true;
						}
						if($userClass==17 || $muAccess==1) $disp = true;
						if($disp) $themeArray[4]['hub'.$row2['id']] = $row2['name'];
						$a = $b = $disp = NULL;
					}
				}
				
				if(@$this->numRows($globalMUHubs)){
					$themeArray[5]['label'] = 'Global Base Websites';
					while($row3 = $this->fetchArray($globalMUHubs, 1)){
						//check if the user can access this hub
						$query = "SELECT multi_user_classes FROM hub WHERE id = '".$row3['id']."'";
						$a = $this->queryFetch($query, NULL, 1);
						if($a['multi_user_classes']){
							$b = explode('::', $a['multi_user_classes']);
							$allowedClass = in_array($userClass, $b);
							if(in_array($userClass, $b)) $disp = true;
						}
						if($userClass==17 && $allowedClass) $disp = true;
						if($disp) $themeArray[5]['gmu'.$row3['id']] = $row3['name'];
						$a = $b = $disp = NULL;
					}
				}
			}
			if($optgroups){
				//check for empty optgroups
				foreach($themeArray as $key=>$value){
					//a blank optgroup will have $value['label'] but nothing else
					$test = $value;
					unset($test['label']);
					if(empty($test)) unset($themeArray[$key]);
				}
			}
		}
		echo $this->buildSelect('theme', $themeArray, $selected, $selectClass, $optgroups, $selected);
	}
	
	/******************************************************
	// Function to build select drop-down
	/******************************************************/
	function buildSelect($name, $industry, $selected = '', $class = '', $optgroups = '', $rel = ''){
		$select = '<select name="'.$name.'" title="select" class="uniformselect';
		if($class) $select .= ' '.$class.'"';
		else $select .= '"';
		if($rel) $select .= ' rel="'.$rel.'"';
		$select .= '><option value="0">Please choose an option</option>';
		
		if($industry){
			if($optgroups){
				foreach( $industry as $key => $value){
					$select .= '<optgroup label="'.$value['label'].'">';
					foreach($value as $key2=>$value2){
						if($key2!='label'){
							if($key2=="0") $selectVal = "";
							else $selectVal = $key2;
							$select .= '<option value="'.$selectVal.'"';
							if($selected == $key2)
								$select .= 'selected="selected"';
							$select .= '>'.$value2.'</option>';
						}
					}
					$select .= '</optgroup>';
				}
			}
			else {
				foreach( $industry as $key => $value){
					if($key=="0") $selectVal = "";
					else $selectVal = $key;
					$select .= '<option value="'.$selectVal.'"';
					if($selected == $key)
						$select .= 'selected="selected"';
					$select .= '>'.$value.'</option>';
				}
			}
		}
		
		$select .= '</select>';
		
		return $select;
	}
	
	/******************************************************
	// Display dropdown of categories
	/******************************************************/
	function displayCategories($selected = '', $class = '', $catdefine = ''){
		$industry = $this->returnCategories($catdefine);
		echo $this->buildSelect('category', $industry, $selected, $class);
	}
	
	/******************************************************
	// Display dropdown of industries
	/******************************************************/
	function displayIndustries($selected = '', $catdefine = ''){
		$industry = $this->returnCategories($catdefine);
		echo $this->buildSelect('industry', $industry, $selected);
	}
	
	/******************************************************
	// Return array of industries/business categories
	/******************************************************/
	function returnCategories($catdefine){	
		require_once QUBEADMIN . '../hybrid/classes/Defaults.php';
    return Defaults::Industries();
    
		require_once QUBEADMIN . 'inc/industries.php';
		return Industries::returnCategories($catdefine);
	}
	
	/******************************************************
	// Function for hubs to help build nav links
	/******************************************************/
	function convertKeywordFolder($input, $strpSlsh=1){
		$output = str_replace(",","",$input);
		$output = str_replace("'","",$output);
		$output = str_replace("?","",$output);
		$output = str_replace("!","",$output);
		$output = str_replace("&","and",$output);
		$output = str_replace("-","--",$output);
		if($strpSlsh) $output = str_replace("/","",$output);
		$output = str_replace(":","",$output);
		$output = mb_strtolower("$output");
		$title = explode(" ", stripslashes($output));
		//$title = array_filter($title);
		$title = implode("-", $title);
		return $title;
	}
	
	/******************************************************
	// Function for blogs to build urls
	/******************************************************/
	function convertCategoryBlog($category){
		$category = str_replace(",","",$category);
		$category = str_replace("'","",$category);
		$category = str_replace("?","",$category);
		$category = str_replace("!","",$category);
		$category = str_replace(".","",$category);
		$category = str_replace("&","and",$category);
		$category = str_replace("+","plus",$category);
		$category = str_replace(":","",$category);
		$category = mb_strtolower($category);
		$title = explode(" ", stripslashes($category));
		$title = implode("-", $title);
		$title = trim($title, "-");
		return $title.'/';
	}
	
	/************************************************************
	// Function to backup a table row (serialized) to a text file
	/***********************************************************/
	function serialBackup($row, $folder, $filename, $noDate = 0){
		$year = date("Y");
		$date = date("m-d-Y");
		//5/30/2012 by Reese
		//updated this function to encode serialized string to base64
		//before saving to preserve formatting.
		//filenames now include base64 to denote this
		$filename = str_replace('.txt', '.base64.txt', $filename);
		
		if($noDate){
			$backupDir = "/home/sapphire/6qube_backups/".$folder."/";
			//check/create year folder
			if(!is_dir($backupDir)){
				$old = umask(0); //temporarily escalate CHMOD permissions
				mkdir($backupDir, 0777);
				umask($old);
			}
		}
		else {
			$backupDir = "/home/sapphire/6qube_backups/".$folder."/".$year."/";
			//check/create year folder
			if(!is_dir($backupDir)){
				$old = umask(0); //temporarily escalate CHMOD permissions
				mkdir($backupDir, 0777);
				umask($old);
			}
			//add date and check/create month-day-year folder
			$backupDir = "/home/sapphire/6qube_backups/".$folder."/".$year."/".$date."/";
			if(!is_dir($backupDir)){
				$old = umask(0); //temporarily escalate CHMOD permissions
				mkdir($backupDir, 0777);
				umask($old);
			}
		}
		
		//ex. /home/sapphire/public_html/6qube.com/$folder/2010/11-18-2010/$filename
		if(!is_file($backupDir.$filename)){ 
			$data = base64_encode(serialize($row));
		
			$fc = fopen($backupDir.$filename, "w");
			if(fwrite($fc, $data)) return true;
			else return false;
			fclose($fc);
		}
	}
	
	/************************************************************
	// Function to strip all non alpha-numeric chars from string
	// "Tester's Companies Inc. $$$" >> "Testers Companie Inc"
	/***********************************************************/
	function stripNonAlphaNum($input){
		$stripped = preg_replace("/[^a-zA-Z0-9\s]/", "", $input); //remove all non-alphanumerics
		
		return $stripped;
	}
	
	/************************************************************
	// Function to format a phone number to (123) 456-7890
	/***********************************************************/
	function formatPhone($input){
		$input = $this->stripNonAlphaNum($input);
		$input = str_replace(' ', '', $input);
		if(strlen($input)>10) $input = substr($input, -10);
		//Number should now look like 1234567890 no matter original
		$a = substr($input, 0, 3);
		$b = substr($input, 3, 3);
		$c = substr($input, 6, 4);
		$output = '('.$a.') '.$b.'-'.$c;
		
		return $output;
	}
	
	//Calculates a date lying a given number of months in the future of a given date.
	//The results resemble the logic used in MySQL where '2009-01-31 +1 month' is '2009-02-28'
	//rather than '2009-03-03' (like in PHP's strtotime).
	function get_x_months_to_the_future($base_time = NULL, $months = 1){
		if(is_null($base_time)) $base_time = time();
	    
		$x_months_to_the_future = strtotime("+".$months." months", $base_time);
	    
		$month_before = (int) date("m", $base_time)+12 * (int) date("Y", $base_time);
		$month_after = (int) date("m", $x_months_to_the_future)+12 * (int) date("Y", $x_months_to_the_future);
	    
		if($month_after > $months + $month_before)
			$x_months_to_the_future = strtotime(date("Ym01His", $x_months_to_the_future)." -1 day");
	    
		return date('Y-m-d H:i:s', $x_months_to_the_future);
	} //get_x_months_to_the_future()<br />
	
	//Insert a class into an HMTL element
	function insertClassIntoElement($element, $class){
		$element = trim($element);
		//check to see if the element already has a class
		if(strpos($element, 'class="')){
			$a = explode('class="', $element);
			$output = $a[0].'class="'.$class.' '.$a[1]; //inert new class with existing one(s)
		}
		else {
			//otherwise just appent the class at hte end of the element
			$a = substr($element, 0, -1);
			$output = $a.' class="'.$class.'">';
		}
		
		return $output;
	}
}
?>
