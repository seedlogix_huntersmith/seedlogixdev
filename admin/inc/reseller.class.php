<?php

         require_once( dirname(__FILE__) . '/../6qube/core.php');

require_once dirname(__FILE__) . '/../6qube/core.php';
require_once QUBEADMIN . 'inc/local.class.php';
require_once QUBEADMIN . 'inc/cpanel.xmlapi.php';
require_once QUBEADMIN . 'inc/Authnet.class.php';
class Reseller extends Local {
	
	/******************************************************
	// Display resellers
	/******************************************************/	
	function displayResellers($page = '', $limit = '', $offset = ''){
		$results = $this->getUsers(NULL, NULL, true, $limit, $offset);
		$numUsers = $this->getUsers(NULL, NULL, true, NULL, NULL, true);
		
		if($limit && ($numUsers > $limit)){
			$numPages = ceil($numUsers/$limit);
			if($offset>0) $currPage = ($offset/$limit)+1;
			else $currPage = 1;
			echo '<p style="font-weight:bold;">';
			if($currPage > 1){
				echo '<a href="'.$page.'?offset=0"><<| start</a>&nbsp;&nbsp;&nbsp;';
				echo '<a href="'.$page.'?offset='.($offset-$limit).'">< prev</a> ';
			}
			if($currPage>1 && $currPage<$numPages){
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			if($currPage < $numPages){
				echo ' <a href="'.$page.'?offset='.($offset + $limit).'">next ></a>&nbsp;&nbsp;&nbsp;';
				echo ' <a href="'.$page.'?offset='.(($numPages*$limit)-$limit).'">end |>></a>';
			}
			echo '</p><br />';
		}
		while($row = $this->fetchArray($results)){
			$dt1 = date("F j, Y", strtotime($row['last_login']));
			$dt2 = date("g:ia", strtotime($row['last_login']));
			echo '<ul class="prospects" title="prospect">
					<li class="name3" style="width:230px;">'.$row['company'].'</li>
					<li class="email2" style="width:300px;">'.$row['username'].'</li>
					<li class="user-links"><a href="#" class="user-details" rel="'.$row['user_id'].'">Details</a> | <a href="inc/forms/edit-user.php?id='.$row['user_id'].'">Edit</a> | <a href="reseller-billing.php?view=2&id='.$row['user_id'].'">Billing</a> | <a href="admin-users.php?parent_id='.$row['user_id'].'">Clients</a> | <a href="#" class="del-user" rel="'.$row['user_id'].'">Delete</a></li>
					<li class="comment" id="user-details-'.$row['user_id'].'">
						<p>Phone: '.$row['phone'].'</p>
						<p>Address:<br />'.$row['address'];
			if($row['address2']) echo '<br />'.$row['address2'];
			echo 		'</p>
						<p>'.$row['city'].', '.$row['state'].' '.$row['zip'].'</p>
						<p>Created: '.date('F j, Y', strtotime($row['created'])).
						' at '.date('g:ia', strtotime($row['created'])).'</p>
						<p>Last Login: ';
			if($row['last_login'] != '0000-00-00 00:00:00') 
			echo date('F j, Y', strtotime($row['last_login'])).' at '.date('g:ia', strtotime($row['last_login']));
			else	echo		'Never';
			echo 		'</p>
						<p><a href="#" class="user-details" rel="'.$row['user_id'].'"><b>Close</b></a></p>
					</li>
				</ul>';
		}
	}
	
	/******************************************************
	// Display dropdown of users (For switching between)
	/******************************************************/
	function displayChangeUsersDrop($admin, $uid, $curr_id, $username, $curr_username, $limit = ''){
		if(!$admin) $parent_id = $uid;
		
		if($users = $this->getUsers($parent_id, NULL, NULL, $limit)){
			echo '<select name="admin-user-select" class="userdropdown" id="userdropdown">
				 <option value="'.$uid.'">'.$uid.' - '.$username.'</option>
				 <option value="">---</option>';
			$numUsers = 0;
			$userIds = array();
			while($row = $this->fetchArray($users)){
				echo '<option value="'.$row['id'].'" ';
				if($row['id']==$curr_id) echo 'selected="selected"';
				echo '>'.$row['id'].' - '.$row['username'].'</option>';
				$userIds[] = $row['id'];
				$numUsers++;
			}
			if(!in_array($curr_id, $userIds) && $curr_id!=$uid){
				echo '<option value="">---</option>';
				echo '<option value="'.$curr_id.'" selected="selected">';
				echo $curr_id.' - '.$curr_username;
				echo '</option>';
			}
			echo '</select>';
		}
		else {
			echo '<select name="admin-user-select" class="userdropdown" id="userdropdown">
				 <option value="'.$uid.'">'.$uid.' - '.$username.'</option>
				 <option value="">---</option>
				 </select>';
		}
	}
	
	/******************************************************
	// Display dropdown of users (For switching between)
	/******************************************************/
	function displayUsersHTML($admin, $uid, $curr_id, $username, $curr_username, $limit = ''){
		
		if(!$admin) $parent_id = $uid;
		
		$prefix = '<li>';
		$suffix = '</li>';
		
		$getTotalUsers = $this->getUsers($parent_id, NULL, NULL);
		//$totalUsers = $this->numRows($getTotalUsers);
		
		
		if($users = $this->getUsers($parent_id, NULL, NULL, $limit))
		{
		/*
			if($totalUsers > $limit)
			{
				echo $prefix.'<a href="admin-users.php" class="ajax dropDownHighlight">Show All Users</a>'.$suffix;
			}
		*/	
			while($row = $this->fetchArray($users))
			{
				echo $prefix.'<a class="manage-user';
				if($row['id']==$curr_id) { echo ' userSelected'; }
				echo '" rel="'.$row['id'].'" href="#">'.$row['username'].'</a>'.$suffix;
					
				$userIds[] = $row['id'];
				$numUsers++;
			}
		}
		else
		{
			echo $prefix.'No Users Available'.$suffix;
		}
	}
	
	
	/*******************************************************
	// Get Reseller's Users
	/******************************************************/
	function getResellerUsers($parent_id, $user_id='',$resellers='',$limit=''){
	
		$getTotalUsers = $this->getUsers($parent_id, NULL, NULL);
		return $this->numRows($getTotalUsers);
		
	
	}
	
	/******************************************************
	// Return dropdown of reseller's network sites
	/******************************************************/
	function networkSitesDropdown($reseller_id, $name){
		$result = $this->getNetworkSites($reseller_id);
		
		if($this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$sitesArray[$row['id']] = $row['name'];
			}
			return $this->buildSelect($name, $sitesArray, NULL, 'ajax-select uniformselect'); //(name, values, option to select, class)
		}
		else
			return 'No Hubs created yet';		
	}
	
	/******************************************************
	// Get reseller's network sites
	// (does not return whole sites)
	/******************************************************/
	function getNetworkSites($reseller_id){
		if(is_numeric($reseller_id)){
			$query = "SELECT id, name FROM resellers_network WHERE admin_user = '".$reseller_id."' AND type != 'whole'";
			if($result = $this->query($query)){
				return $result;
			}
			else return false;
		}
		else return false;
	}
	
	/******************************************************
	// Get a user's parent_id (for reseller clients)
	/******************************************************/
	function getParentID($user){
		$query = "SELECT parent_id FROM users WHERE id = '".$user."' LIMIT 1";
		if($result = $this->queryFetch($query)){
			$parent_id = $result['parent_id'];
			return $parent_id;
		}
		else return false;
	}
	
	/******************************************************
	// Get user_info row for reseller
	/******************************************************/
	function getResellerInfo($user){
		$query = "SELECT * FROM user_info WHERE user_id = '".$user."'";
		if($result = $this->queryFetch($query)){
			return $result;
		}
		else return false;
	}
	
	/******************************************************
	// Update 6qube.com root .htaccess
	// accepts domain in 'domain.com' format
	/******************************************************/
	function updateHtaccess($domain){		
		//////////////////////////////
		//6qube.com root .htaccess  //
		//////////////////////////////
		$file = QUBEROOT . '.htaccess';
		
		//get file contents first
		$origData = file_get_contents($file);
		//data to update it with
		$newData = '
RewriteCond %{HTTP_HOST} ^'.$domain.'$ [OR] 
RewriteCond %{HTTP_HOST} ^www.'.$domain.'$ 
RewriteRule ^(.*)/$ index.php?page=$1
';
		//update
		$fc = fopen($file, "w");
		if(fwrite($fc, $origData.$newData)) $success = true;
		fclose($fc);
		
		//////////////////////////////
		//6qube.com/admin .htaccess //
		//////////////////////////////
		$file2 = QUBEADMIN . '.htaccess';
				
		$a = explode('.', $domain);
		$a[0] .= '\\';
		$domainslashed = implode('.', $a);
			
		//get file contents first
		$origData2 = file_get_contents($file2);
		//data to update it with
		$newData2 = '
RewriteCond %{HTTP_HOST} ^login.'.$domain.'$
RewriteRule ^/?$ "http\:\/\/'.$domainslashed.'\/admin" [R=301,L]';
		//update
		$fc2 = fopen($file2, "w");
		if(fwrite($fc2, $origData2.$newData2)) $success = true;
		else $success = false;
		fclose($fc2);
		
		//return
		if($success) return true;
		else return false;
	}
	
	/******************************************************
	// Get a reseller's products from user_class table
	/******************************************************/
	function getResellerProducts($uid = '', $id = '', $inc_wholesale = '', $maps_to = '', $exclude_free = ''){
		$query = "SELECT * FROM user_class WHERE ";
		if(!$uid) $query .= " (admin_user = 0 OR admin_user = 7) ";
		else $query .= " admin_user = '".$uid."' ";
		if($id) $query .= " AND id = '".$id."'";
		if($maps_to) $query .= " AND maps_to = '".$maps_to."'";
		if($exclude_free) $query .= " AND maps_to != 27";
		if(!$uid){
			$query .= " AND (id < 27 OR id >= 178 OR id <= 185)";
			$query .= " AND id != 18";
		}
		$query .= " ORDER BY id ASC";
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			if($id || $maps_to){
				$return = $this->fetchArray($result);
				//if product is based on a wholesale one get those specs
				if($inc_wholesale){
					$query = "SELECT setup_price, monthly_price, add_site FROM user_class WHERE id = '".$return['maps_to']."'";
					$return['wholesale'] = $this->queryFetch($query);
				}
				return $return;
			} else {
				return $result;
			}
		}
		else return false;
	}
	
	/****************************************************************
	// Convert a product's ID (user_class.id) into a link to sign up
	/****************************************************************/
	function getResellerProductLink($id = '', $domain = ''){
		if(!$domain){
			//if input didn't include the domain, get it
			$query = "SELECT main_site 
					FROM resellers 
					WHERE admin_user = (SELECT user_class.admin_user FROM user_class WHERE id = '".$id."')";
			$a = $this->queryFetch($query);
			$domain = 'http://'.$a['main_site'];
		}
		
		//get product's name
		$query = "SELECT name FROM user_class WHERE id = '".$id."'";
		$result = $this->queryFetch($query);
		
		//return link (like http://reseller.com/my-product-name/123/)
		return $domain.'/'.$this->convertKeywordUrl($result['name'],1).'/'.$id.'/';
	}
	
	/******************************************************
	// Initial setup of reseller products (copy ours)
	/******************************************************/
	function setupResellerProducts($uid, $company){
		//prepare company for names
		$companyConverted = preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($company)); //remove all non-alphanumerics
		$companyConverted = str_replace(".", "", $companyConverted);//remove .'s
		$companyConverted = str_replace(" ", "", $companyConverted); //My Company -> mycompany
		
		//set classes to copy and their suffixes
		$copyTypes = array(27=>'free', 28=>'pro_diy', 29=>'pro_full', 30=>'adv_diy', 
						31=>'full_small', 32=>'full_med', 33=>'full_large');
		//loop through and copy/update each
		foreach($copyTypes as $key=>$value){
			$query = "INSERT INTO user_class 
					(name, description, setup_price, monthly_price, add_site, campaign_limit, directory_limit, 
					 hub_limit, hub_dupe_limit, landing_limit, social_limit, press_limit, article_limit, blog_limit, 
					 responder_limit, email_limit)
					(SELECT name, description, setup_price, monthly_price, add_site, 
							campaign_limit, directory_limit, hub_limit, hub_dupe_limit, landing_limit, 
							social_limit, press_limit, article_limit, blog_limit, responder_limit, email_limit 
							FROM user_class WHERE id = '".$key."' ORDER BY id ASC)";
			if($result = $this->query($query)){
				$newID = $this->getLastId();
				$query2 = "UPDATE 
							user_class 
						SET 
							admin_user = '".$uid."', maps_to = '".$key."', type = '".$companyConverted.".".$value."'
						WHERE
							id = '".$newID."'";
				if(!$result2 = $this->query($query2)) echo mysqli_error();
			}
			//else echo mysqli_error();
		}
	}
	
	/******************************************************
	// Display a reseller's products from user_class table
	/******************************************************/
	function displayResellerProducts($uid='',$format='',$id='',$excludeFree='',$selectID='',$selectName='',$addOpt='',$admin=''){
		$products = $this->getResellerProducts($uid, NULL, NULL, NULL, $excludeFree);
		if(!$selectName) $selectName = 'class';
		if($products){
			if($format=='dropdown' || $format=='dropdownWithOptions'){
				$html = '<select class="uniformselect';
				if($id=='') $html .= ' no-upload';
				$html .= '"';
				if($selectID) $html .= 'id="'.$selectID.'"';
				$html .= ' name="'.$selectName.'" title="select">';
				$html .= '<option value="">Please Choose</option>';
				if($addOpt) $html .= $addOpt;
				while($row = $this->fetchArray($products)){
					$html .= '<option value="'.$row['id'].'"';
					if($row['id']==$id) $html .= ' selected="yes"';
					$html .= '>'.$row['name'].'</option>';
					
					if($row['add_site']>0 && $format=='dropdownWithOptions'){
						$html .= '<option value="'.$row['id'].'-add_site">'.$row['name'].' + Custom Theme</option>';
					}
				}
				$html .= '</select>';				
				return $html;
			}
			else {
				$nameLength = $admin ? 300 : 350;
				$nameLength2 = $admin ? 37 : 50;
				$summLength = $admin ? 350 : 400;
				$summLength2 = $admin ? 43 : 50;
				while($row = $this->fetchArray($products)){
					$editLink = 'inc/forms/edit-reseller-product.php?id='.$row['id'];
					if($admin) $editLink .= '&uid='.$uid;
					echo '
						<ul class="prospects">
						<li class="name4" style="width:'.$nameLength.'px;">'.$this->shortenSummary($row['name'], $nameLength2).'</li>
						<li class="user-links" style="float:right;">
							<a href="'.$editLink.'">Edit</a> | 
							<a href="#" class="dupe-product" rel="'.$row['id'].'" title="'.$row['admin_user'].'">Duplicate</a> | 
							<a href="#" class="del-product" rel="'.$row['id'].'" title="'.$row['admin_user'].'">Delete</a>';
					echo '
						</li>
						</ul>';
				}
			}
		}
		else {
			if($format=='dropdown' || $format=='dropdownWithOptions'){
				return '<select class="uniformselect no-upload" title="select"><option value="">No products created yet</option></select>';
			} else {
				echo "There are currently no products to display.  Use the form above to create one.";
			}
		}
	}
	
	/******************************************************
	// Set up a single reseller network
	// (also used by createWholeNetwork)
	/******************************************************/
	function setupNetworkSite($resellerID, $name, $type, $parent = '', $domain = '', $company = ''){
		if(!$parent) $parent = '0';
		//insert into resellers_network
		$query = "INSERT INTO resellers_network (admin_user, parent, type, name, domain, company) 
				VALUES (".$resellerID.", ".$parent.", '".$type."', '".$name."', '".$domain."', '".$company."')";
		if($this->query($query)){
			//enter into analytics and get tracking
			$id = $this->getLastId();
			if($type!='whole'){
				if(!$domain) $domain = 'http://www.new-reseller-network.com';
				$tracking_id = $this->ANAL_addSite('resellers_network', $id, $domain);
			}
			if(!$tracking_id) $tracking_id = 26;
			//return id's
			$return['tracking_id'] = $tracking_id;
			$return['id'] = $id;
			return $return;
		}
		else return false;
	}
	
	/******************************************************
	// Set up a whole reseller network
	// called from upload_form.php when a reseller enters 
	// a domain after creating a Whole Network item
	/******************************************************/
	function setupWholeNetworkSites($resellerID, $networkID, $domain, $company){
		$errors = '';
		$ip = "67.18.16.29";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire","ignitedCLOUD2013");	//3376Elements");
		$xmlapi->set_output('json');
		//make sure domain is already in the system
		$query = "SELECT id FROM hub WHERE (domain = 'http://".$domain."' OR domain LIKE '%.".$domain."')";
		if($result = $this->query($query) && $this->numRows($result))
			$continue = true;
		else $errors = 'Domain is not in the system yet';
		
		if($continue){
		////////////////////////////////////////////////////////////////////
		//START SITES SETUP
		////////////////////////////////////////////////////////////////////
		////////////////////////////////
		//LOCAL
		$dir = 'public_html/6qube.com/local';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'local', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Local', 'local', $networkID, 'http://local.'.$domain, $company))
				$errors .= ' Error setting up site in system for Local ';
		}
		else $errors .= ' Error setting up subdomain for Local ';
		////////////////////////////////
		//HUBS
		$dir = 'public_html/6qube.com/hubs';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'hubs', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Hubs', 'hubs', $networkID, 'http://hubs.'.$domain, $company))
				$errors .= ' Error setting up site in system for Hubs ';
		}
		else $errors .= ' Error setting up subdomain for Hubs ';
		////////////////////////////////
		//BLOGS
		$dir = 'public_html/6qube.com/blogs';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'blogs', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Blogs', 'blogs', $networkID, 'http://blogs.'.$domain, $company))
				$errors .= ' Error setting up site in system for Blogs ';
		}
		else $errors .= ' Error setting up subdomain for Blogs ';
		////////////////////////////////
		//PRESS
		$dir = 'public_html/6qube.com/press';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'press', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Press', 'press', $networkID, 'http://press.'.$domain, $company))
				$errors .= ' Error setting up site in system for Press ';
		}
		else $errors .= ' Error setting up subdomain for Press ';
		////////////////////////////////
		//ARTICLES
		$dir = 'public_html/6qube.com/articles';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'articles', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Articles', 'articles', $networkID, 'http://articles.'.$domain, $company))
				$errors .= ' Error setting up site in system for Articles ';
		}
		else $errors .= ' Error setting up subdomain for Articles ';
		////////////////////////////////
		//SEARCH
		$dir = 'public_html/6qube.com/search';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'search', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Search', 'search', $networkID, 'http://search.'.$domain, $company))
				$errors .= ' Error setting up site in system for Search ';
		}
		else $errors .= ' Error setting up subdomain for Search ';
		////////////////////////////////
		//BROWSE
		$dir = 'public_html/6qube.com/browse';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'browse', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			if(!$this->setupNetworkSite($resellerID, $company.' Browse', 'browse', $networkID, 'http://browse.'.$domain, $company))
				$errors .= ' Error setting up site in system for Browse ';
		}
		else $errors .= ' Error setting up subdomain for Browse ';
		////////////////////////////////
		//HUB PREVIEW
		$dir = 'public_html/6qube.com/hubs/preview';
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>'browse', rootdomain=>$domain)), true);
		if($subdomain["cpanelresult"]["data"][0]["result"]!=1){
			$errors .= ' Error setting up subdomain for Hub Preview ';
		}
		////////////////////////////////////////////////////////////////////
		//END SITES SETUP
		////////////////////////////////////////////////////////////////////
		} //end if($continue)
		
		if($errors){
			$return['success'] = 0;
			$return['errors'] = $errors;
		}
		else {
			$this->updateWholeNetwork($networkID, 'setup', 1);
			$return['success'] = 1;
		}
		
		return $return;
	}
	
	/******************************************************
	// Set up a whole reseller network
	// called from upload_form.php when a reseller enters 
	// a domain after creating a Whole Network item
	/******************************************************/
	function updateWholeNetwork($networkID, $field, $value){
		$blocked_fields = array('domain', 'parent', 'admin_user', 'type');
		if(!in_array($field, $blocked_fields)){
			//parent row
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE id = ".$networkID;
			if(!$this->query($query)) $errors .= ' parent ';
			//local
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'local'";
			if(!$this->query($query)) $errors .= ' local ';
			//hubs
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'hubs'";
			if(!$this->query($query)) $errors .= ' hubs ';
			//blogs
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'blogs'";
			if(!$this->query($query)) $errors .= ' blogs ';
			//press
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'press'";
			if(!$this->query($query)) $errors .= ' press ';
			//articles
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'articles'";
			if(!$this->query($query)) $errors .= ' articles ';
			//search
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'search'";
			if(!$this->query($query)) $errors .= ' search ';
			//browse
			$query = "UPDATE resellers_network SET `".$field."` = '".$value."' WHERE parent = ".$networkID." AND type = 'browse'";
			if(!$this->query($query)) $errors .= ' browse ';
			
			if(!$errors) $return['success'] = 1;
			else $return['errors'] = $errors;
		}
		
		return $return;
	}
	
	/**************************************************************************
	// Update signups table, notify reseller and 6qube admin
	/**************************************************************************/
	function finalizeSignup($type, $id1 = '', $id2 = '', $rid, $uid){
		///////////////////////////////////////////////
		// UPDATE QUERIES
		///////////////////////////////////////////////
		//update signups table
		$query = "UPDATE signups SET success = 1, transaction_method = '".$type."' WHERE user_id = ".$uid;
		$update = $this->query($query);
		//update users table, fill billing_id fields with subscription info and set access to 1
		$query = "UPDATE users SET billing_id = '".$id1."', billing_id2 = '".$id2."', access = 1 WHERE id = ".$uid;
		$update2 = $this->query($query);
		
		///////////////////////////////////////////////
		// SELECT INFO QUERIES
		///////////////////////////////////////////////
		//get some user info
		$query = "SELECT firstname, lastname, company, phone FROM user_info WHERE user_id = ".$uid;
		$userInfo = $this->queryFetch($query);
		$query = "SELECT username FROM users WHERE id = ".$uid;
		$a = $this->queryFetch($query);
		$userInfo['email'] = $a['username'];
		//get info from signups table
		$query = "SELECT product_id, setup_cost, monthly_cost, add_site FROM signups WHERE user_id = ".$uid;
		$signupInfo = $this->queryFetch($query);
		//get product info
		$query = "SELECT id, maps_to, name, setup_price, add_site FROM user_class WHERE id = '".$signupInfo['product_id']."'";
		$productInfo = $this->queryFetch($query);
		//get reseller info for email to admin
		$query = "SELECT main_site, company, support_email, receipt_email, payments FROM resellers WHERE admin_user = ".$rid;
		$resellerInfo = $this->queryFetch($query);
		//get reseller's email
		$query = "SELECT username FROM users WHERE id = ".$rid;
		$b = $this->queryFetch($query);
		$resellerInfo['email'] = $b['username'];
		
		///////////////////////////////////////////////
		// CHARGE RESELLER WHOLESALE AMOUNT
		///////////////////////////////////////////////
		if($resellerInfo['payments']!='6qube'){
			$anet = new Authnet();
			$query = "SELECT monthly_price FROM user_class WHERE id = ".$productInfo['maps_to'];
			$wholesaleInfo = $this->queryFetch($query);
			$wholesaleBill = $anet->chargeCIM($rid, NULL, $productInfo['id'], $wholesaleInfo['monthly_price'], 'wholesale', 1,
									    "Monthly wholesale charge for user account type - first month", NULL, NULL, $uid);
			//if wholesale billing failed reschedule it (note is added in email below)
			if(!$wholesaleBill['success']){
				$reattemptDate = date('Y-m-d H:i:s', strtotime('+1 day'));
				$this->scheduleTransaction($wholesaleBill['log_id'], NULL, NULL, NULL, NULL, NULL, NULL, NULL, $reattemptDate);
			}
		}
		
		///////////////////////////////////////////////
		// SCHEDULE NEXT MONTH'S BILLING
		///////////////////////////////////////////////
		$nextBillDate = $this->get_x_months_to_the_future(NULL, 1);
		$this->scheduleTransaction(NULL, $uid, $rid, $productInfo['id'], 'monthlySubscription', $signupInfo['monthly_cost'], $resellerInfo['payments'], 'Scheduled monthly billing for '.date('F', strtotime($nextBillDate)), $nextBillDate, 1);
		
		///////////////////////////////////////////////
		// SEND EMAIL(S)
		///////////////////////////////////////////////
		//email reseller and Bcc 6qube Admin
		$to = $resellerInfo['email'];
		$subject = 'You have a new customer!';
		$message = 
			'<h1>'.$resellerInfo['company'].' New User Details:</h1>
			<h2>Who</h2>
			'.$userInfo['firstname'].' '.$userInfo['lastname'].'<br />'.
			$userInfo['company'].'<br />'.
			$this->formatPhone($userInfo['phone']).'<br />
			<h2>What</h2>'.
			$productInfo['name'];
		if($signupInfo['add_site']) $message .= '<br /> + Custom Theme';
		$message .= 
			'<h2>How much</h2>
			Setup cost: $'.$signupInfo['setup_cost'].'<br />
			Monthly cost: $'.$signupInfo['monthly_cost'].' <small>(first month paid)</small>
			<h2>When</h2>'.
			date('m/d/Y').' at '.date('g:ia').' CST';
		//if there was an error billing the reseller for wholesale include in email
		if(!$wholesaleBill['success'])
			$message .=
				'<br /><b>Billing Error</b><br />
				The customer was successfully charged but we encountered an error charging you for the wholesale amount due.<br />
				Error details:<br />';
				$wholesaleBill['message'].'<br /><br />
				Our system will automatically reattempt to bill you tomorrow.  Please contact us if you need assistance.';
		$headers = array('From' => 'admin@6qube.com',
					  'To' => $to,
					  'Cc' => 'admin@6qube.com', 
					  'Subject' => $subject,
					  'Content-Type' => 'text/html; charset=ISO-8859-1',
					  'MIME-Version' => '1.0');
		$this->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
		
		//email customer
		if($signupInfo['setup_cost']>0) $paidToday = $signupInfo['setup_cost'] + $signupInfo['monthly_cost'];
		else $paidToday = $signupInfo['monthly_cost'];
		$to = $userInfo['email'];
		$subject = "Information regarding your new account with ".$resellerInfo['company']." and receipt for your payment";
		if($resellerInfo['receipt_email']){
			$message = stripslashes($resellerInfo['receipt_email']);
			$message = str_replace('#login', 'http://'.$resellerInfo['main_site'].'/admin/', $message);
			$message = str_replace('#amount', '$'.$signupInfo['setup_cost'], $message);
			$message = str_replace('#monthly', '$'.$signupInfo['monthly_cost'], $message);
			$message = str_replace('#paid', '$'.$paidToday, $message);
		}
		else {
			$message = 
			'<h1>Congratulations</h1>
			<p>You just took the next step to making your business more visible online and increasing your amount of internet-based sales and clients.<br />
			This email is the receipt for your initial payment and also the confirmation that your new account is active.</p>
			<h2>Payment information</h2>';
			if($paidToday > $signupInfo['monthly_cost']){
				$message .= 
				'<p>You paid $'.$paidToday.' today, which included:<br />
				&bull; $'.$signupInfo['monthly_cost'].' for the first month of your service<br />
				&bull; $'.$productInfo['setup_price'].' as a one-time setup fee';
				if($signupInfo['add_site']){
					$message .=
					'<br />
					&bull; $'.$productInfo['add_site'].' for us to build you a custom website and blog';
				}
				$message .= '<br />';
			}
			else {
				$message .=
				'<p>You paid $'.$paidToday.' today for the first month of your service.<br />';
			}
			$message .= 'Please note that this is a monthly service.  Our secure third-party payment processor has your payment information on file and will automatically bill you on this day every month for $'.$signupInfo['monthly_cost'].'.</p>
			<p>We appreciate your business and look forward to helping you exceed your online marketing goals!  If you ever need assistance or have any questions feel free to email us at <a href="mailto:'.$resellerInfo['support_email'].'">'.$resellerInfo['support_email'].'</a>.</p>
			Sincerely,<br />
			-'.$resellerInfo['company'];
		}
		$headers = array('From' => $resellerInfo['support_email'],
					  'To' => $to,
					  'Subject' => $subject,
					  'Content-Type' => 'text/html; charset=ISO-8859-1',
					  'MIME-Version' => '1.0');
		$this->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
		
		
		///////////////////////////////////////////////
		//RETURN
		///////////////////////////////////////////////
		if($update && $update2) return true;
	}
	
	/**************************************************************************
	// Return an array of resellers' emailers
	/**************************************************************************/
	function getResellerEmailers($rid, $id = '', $history = ''){
		if($rid && is_numeric($rid)){
			if($history){
				$query = "SELECT * FROM resellers_emailers_history WHERE admin_user = '".$rid."' ORDER BY sent DESC";
			}
			else {
				$query = "SELECT * FROM resellers_emailers WHERE admin_user = '".$rid."'
						AND resellers_emailers.trashed = '0000-00-00' ";
				if($id) $query .= " AND resellers_emailers.id = '".$id."'";
			}
			$results = $this->query($query);
			if($results){
				while($row = $this->fetchArray($results)){
					$return[$row['id']] = $row;
				}
				return $return;
			}
			else return false;
		}
		else return false;
	}
	
	/**************************************************************************
	// Add a new emailer from reseller/admin to users
	/**************************************************************************/
	function addNewEmailer($rid, $name, $admin = ''){
		if($rid && is_numeric($rid)){
			//get user info for default values
			if(!$admin){
				$query = "SELECT main_site, support_email FROM resellers WHERE admin_user = '".$rid."'";
				$resellerInfo = $this->queryFetch($query);
				$query = "SELECT phone, company, address, address2, city, state, zip FROM user_info WHERE user_id = '".$rid."'";
				$resellerInfo2 = $this->queryFetch($query);
				$admin = 0;
			}
			else {
				$resellerInfo['main_site'] = '6qube.com';
				$resellerInfo['support_email'] = 'support@6qube.com';
				$resellerInfo2['phone'] = '877-570-5005';
				$resellerInfo2['company'] = '6Qube';
				$resellerInfo2['address'] = '13740 Research Blvd., Suite D4';
				$resellerInfo2['city'] = 'Austin';
				$resellerInfo2['state'] = 'TX';
				$resellerInfo2['zip'] = '78750';
				$admin = 1;
			}
			//set default values
			$fromField = '"'.$resellerInfo2['company'].'" <'.$resellerInfo['support_email'].'>';
			$contactField = '<p>'.$resellerInfo2['company'].'<br />'.$resellerInfo2['address'].'<br />';
			if($resellerInfo2['address2']) $contactField .= $resellerInfo2['address2'].'<br />';
			$contactField .= $resellerInfo2['city'].', '.$resellerInfo2['state'].' '.$resellerInfo2['zip'].'<br />'.$resellerInfo2['phone'].'</p>';	
			$antiSpam = 'You\'re receiving this email because you\'re a user in our system.  To update your email preferences or unsubscribe from future emails like this, <a href="http://'.$resellerInfo['main_site'].'/admin/" target="_blank">log in to your account here</a> and go to the Settings menu.';		 
			$query = "INSERT INTO resellers_emailers
					(admin_user, admin, name, from_field, sendto_type, emailer_type, contact, anti_spam)
					VALUES
					('".$rid."', '".$admin."', '".addslashes($name)."', '".addslashes($fromField)."', 'active', 
					 'marketing', '".addslashes($contactField)."', '".addslashes($antiSpam)."')";
					 
			if($this->query($query)){
				$newID = $this->getLastId();
				$html = '<div class="item">
					<div class="icons">
						<a href="inc/forms/reseller_emailers.php?form=settings&id='.$newID.'" class="edit"><span>Edit</span></a>
						<a href="#" class="delete" rel="table=resellers_emailers&id='.$newID.'"><span>Delete</span></a>
					</div>';
				$html .= '<img src="http://'.$_SESSION['main_site'].'/admin/img/email-template';
				if($_SESSION['thm_buttons-clr']) $html .= '/'.$_SESSION['thm_buttons-clr'];
				$html .= '.png" class="graph trigger" />';
				$html .= '<a href="inc/forms/reseller_emailers.php?form=settings&id='.$newID.'">'.$name.'</a>
				</div>';
				
				return $html;
			}
			else return false;
		}
		else return false;
	}
	
	/**************************************************************************
	// Add a new back office page or nav section
	/**************************************************************************/
	function addNewBackofficeItem($type, $name, $parent = 0, $uid){
		if(($type == 'nav' || $type=='page') && $name && is_numeric($parent) && is_numeric($uid)){
			$query = "INSERT INTO resellers_backoffice
					(admin_user, type, parent, name)
					VALUES
					('".$uid."', '".$type."', '".$parent."', '".$name."')";
					
			if($this->query($query)){
				$newID = $this->getLastId();
				
				if($type=='nav'){
					$form = 'reseller-backoffice-pages.php?mode=pages&n='.$newID;
					$image = 'nav-section';
				} else {
					$form = 'inc/forms/edit-backoffice-page.php?id='.$newID;
					$image = 'page';
				}
				
				$html = '<div class="item2">
					<div class="icons">
						<a href="'.$form.'" class="edit"><span>Edit</span></a>
						<a href="#" class="delete" rel="table=resellers_backoffice&id='.$newID.'"><span>Delete</span></a>
					</div>';
				$html .= '<img src="img/'.$image.'.png" class="blogPost" />';
				$html .= '<a href="'.$form.'">'.$name.'</a>
				</div>';
				
				return $html;
			}
			else return false;
		}
	}
	
	/**************************************************************************
	// Return a reseller's first custom page (nav order #1, page order #1)
	/**************************************************************************/
	function getFirstCustomPage($resellerID){
		//get first nav
		$query = "SELECT id FROM resellers_backoffice WHERE admin_user = '".$resellerID."'
				AND resellers_backoffice.trashed = '0000-00-00'  
				AND type = 'nav' ORDER BY `list_order` ASC, id ASC LIMIT 1";
		$a = $this->queryFetch($query);
		//get first page under that nav
		$query = "SELECT id FROM resellers_backoffice WHERE admin_user = '".$resellerID."' 
				AND resellers_backoffice.trashed = '0000-00-00'  
				AND type = 'page' AND parent = '".$a['id']."' ORDER BY `list_order` ASC, id ASC LIMIT 1";
		$b = $this->queryFetch($query);
		$firstPageID = $b['id'];
		
		return $firstPageID;
	}
	
	/**************************************************************************
	// Return a formatted link to another custom back office page
	// accepts a string that is the page's title
	/**************************************************************************/
	function getCustomPageLink($title, $text = '', $uid){
		//find page (note mysql queries are case insensitive)
		$query = "SELECT id FROM resellers_backoffice WHERE admin_user = '".$uid."'
				AND type = 'page' AND name = '".$title."' LIMIT 1";
		$a = $this->queryFetch($query);
		if($a['id']) $link = 'inc/home.php?p='.$a['id'];
		else $link = '#';
		$linkText = $text ? $text : $title;
		
		return '<a href="'.$link.'" class="dflt-link">'.$linkText.'</a>';
	}
	
///////////////////////////////////////////////////////////////////////////
// BILLING FUNCTIONS
/**************************************************************************/
	
	/**************************************************************************
	// Log transaction details from a processor into resellers_billing table
	/**************************************************************************/
	function logTransaction($uid, $rid='', $pid='', $type='', $id='', $amount='', $identifier='', 
					    $processor='', $success='', $note='', $uid2=''){
		if($note) $note = $this->validateText($note);
		if(!$pid) $pid = 0;
		$query = "INSERT INTO resellers_billing (user_id, user_id2, user_parent_id, product_id, transaction_type,
										transaction_id, amount, method_identifier, processor, success, note)
				VALUES ('".$uid."', '".$uid2."', '".$rid."', '".$pid."', '".$type."', '".$id."', 
						'".$amount."', '".$identifier."', '".$processor."', '".$success."', '".addslashes($note)."')";
		if($this->query($query)) return $this->getLastId();
		else return false;
	}
	
	/**************************************************************************
	// Schedule transaction in resellers_billing table to be run by cron script
	// OR update an existing transaction to be reattempted by specifying ID
	/**************************************************************************/
	function scheduleTransaction($id='', $uid='', $rid='', $pid='', $type='', $amount='', $processor='', 
							$note='', $date='', $reattempts=0, $uid2='', $chargeOnSetup=0, $sixqube = 0){
		if(!$id){
			$sched = $chargeOnSetup ? 0 : 1;
			if($sixqube){
				$query = "INSERT INTO sixqube_billing (user_id, product_id, transaction_type, amount, note, sched, sched_date, sched_reattempt, charge_on_setup) 
						VALUES ('".$uid."', '".$pid."', '".$type."', '".$amount."', '".$note."', '".$sched."', '".$date."', '".$reattempts."', '".$chargeOnSetup."')";
			}
			else {
				$query = "INSERT INTO resellers_billing (user_id, user_id2, user_parent_id, product_id, transaction_type, amount, processor, note, sched, sched_date, sched_reattempt, charge_on_setup) 
						VALUES ('".$uid."', '".$uid2."', '".$rid."', '".$pid."', '".$type."', '".$amount."', '".$processor."', '".$note."', '".$sched."', '".$date."', '".$reattempts."', '".$chargeOnSetup."')";
			}
			if($this->query($query)) return $this->getLastId();
			else return false;
		}
		else if($id){
			if($sixqube) $table = 'sixqube_billing';
			else $table = 'resellers_billing';
			$query = "UPDATE `".$table."` SET sched = 1, sched_date = '".$date."' WHERE id = ".$id;
			if($this->query($query)) return true;
			else return false;
		}
		else return false;
	}
	
	/**************************************************************************
	// Get billing items from resellers_billing table
	/**************************************************************************/
	function getBillingItems($uid='', $rid='', $pid='', $success='', $limit='', $offset='', $sixqube='', $count=''){
		$table = $sixqube ? 'sixqube_billing' : 'resellers_billing';
		if($count) $query = "SELECT count(id) ";
		else $query = "SELECT * ";
		$query .= " FROM `".$table."` WHERE ((sched = 0 AND charge_on_setup = 0) OR sched_attempted = 1)";
		if($uid && is_numeric($uid)) $query .= " AND user_id = ".$uid;
		if(($rid && is_numeric($rid)) && !$sixqube) $query .= " AND user_parent_id = ".$rid;
		if($pid && is_numeric($pid)) $query .= " AND product_id = ".$pid;
		if($success) $query .= " AND success = 1";
		if(!$count){
			$query .= " ORDER BY timestamp DESC";
			if($limit) $query .= " LIMIT ".$limit;
			if($offset) $query .= " OFFSET ".$offset;
			
			$result = $this->query($query);
			if($result && $this->numRows($result))
				return $result;
			else 
				return false;
		}
		else {
			$result = $this->queryFetch($query);
			if($result['count(id)']) return $result['count(id)'];
			else return 0;
		}
	}
	
	/**************************************************************************
	// Display transactions charged by US to RESELLER(s)
	/**************************************************************************/
	function displayBillingHistory($limit = '', $offset = '', $uid = '', $rid = '', $all = '', $view = '', $sixqube = ''){
		if($uid){
			if($numItems = $this->getBillingItems($uid, NULL, NULL, NULL, NULL, NULL, $sixqube, 1)){
				$billing = $this->getBillingItems($uid, NULL, NULL, NULL, $limit, $offset, $sixqube);
			}
		}
		else if($rid){
			if($numItems = $this->getBillingItems(NULL, $rid, NULL, NULL, NULL, NULL, $sixqube, 1)){
				$billing = $this->getBillingItems(NULL, $rid, NULL, NULL, $limit, $offset, $sixqube);
			}
		}
		else if($all){
			if($numItems = $this->getBillingItems(NULL, NULL, NULL, NULL, NULL, NULL, $sixqube, 1)){
				$billing = $this->getBillingItems(NULL, NULL, NULL, NULL, $limit, $offset, $sixqube);
			}
		}
		
		if($billing && $numItems){
			if($limit && ($numItems > $limit)){
				//pagination
				$numPages = ceil($numItems/$limit);
				if($offset>0) $currPage = ($offset/$limit)+1;
				else $currPage = 1;
				if($sixqube) $baseUrl = '6qube-billing';
				else $baseUrl = 'reseller-billing';
				$baseUrl .= '.php?view='.$view;
				if($uid) $baseUrl .= '&id='.$uid;
				else if($rid) $baseUrl .= '&id='.$rid;
				echo '<p style="font-weight:bold;">';
				if($currPage > 1){
					echo '<a href="'.$baseUrl.'&offset=0"><<| start</a>&nbsp;&nbsp;&nbsp;';
					echo '<a href="'.$baseUrl.'&offset='.($offset-$limit).'">< prev</a> ';
				}
				if($currPage>1 && $currPage<$numPages){
					echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				if($currPage < $numPages){
					echo ' <a href="'.$baseUrl.'&offset='.($offset + $limit).'">next ></a>&nbsp;&nbsp;&nbsp;';
					echo ' <a href="'.$baseUrl.'&offset='.(($numPages*$limit)-$limit).'">end |>></a>';
				}
				echo '</p><br />';
			}
			echo 
				'<ul class="prospects">';
			if($all || $rid) echo '<li style="width:80px;"><b>UID</b></li>';
			echo 	'<li class="name4" style="width:120px;"><b>Date</b></li>
					<li style="width:150px;"><b>Type</b></li>
					<li style="width:150px;"><b>Transaction ID</b></li>
					<li style="width:100px;"><b>Amount</b></li>
					<li style="width:80px;"><b>Success</b></li>
					<li style="width:100px;">&nbsp;</li>
				</ul>';
			while($row = $this->fetchArray($billing)){
				//get billed user's company name
				if($row['user_id2']) $billedUserID = $row['user_id2'];
				else $billedUserID = $row['user_id'];
				$query = "SELECT company FROM user_info WHERE user_id = '".$billedUserID."'";
				$a = $this->queryFetch($query);
				$clientCompany = $a['company'];
				
				$successful = $row['success'] ? 'Yes' : 'No';
				$detailsLink = '<a href="#" class="user-details" rel="'.$row['id'].'">Details</a>';
				echo 
				'<ul class="prospects">';
				if($all || $rid) echo '<li style="width:80px;">'.$row['user_id'].'</li>';
				echo '<li class="name4" style="width:120px;">'.date('m/d/Y', strtotime($row['timestamp'])).'</li>
					<li style="width:150px;">'.$row['transaction_type'].'</li>
					<li style="width:150px;">'.$row['transaction_id'].'</li>
					<li style="width:100px;">$'.$row['amount'].'</li>
					<li style="width:80px;">'.$successful.'</li>
					<li style="width:100px;">'.$detailsLink.'</li>';
					echo '<li class="comment" id="user-details-'.$row['id'].'">';
					if($row['product_id']){
						$query = "SELECT name FROM user_class WHERE id = '".$row['product_id']."'";
						$productInfo = $this->queryFetch($query);
						echo '<p><b>For product:</b> '.$productInfo['name'].'</p>';
						echo '<p><b>For user:</b> '.$billedUserID.' - '.$clientCompany.'</p>';
					}
					if($row['note']) echo '<p><b>Notes:</b><br />'.stripslashes($row['note']);
					echo '<p><a href="#" class="user-details" rel="'.$row['id'].'"><b>Close</b></a></p>';
					echo '</li>';
				echo '</ul>';
				
				$detailsLink = $productInfo = NULL;
			}
		}
		else 
			echo 'No billing items to display yet.';
	}
	
	/**************************************************************************
	// Figure out and log a reseller credit (for resellers using our processor)
	/**************************************************************************/
	function logCredit($rid, $uid, $pid, $type, $transaction_amount, $billing_logID = ''){
		$credAmount = $transaction_amount*0.85; //minus our 15% processing fee
		//get product wholesale info
		$product = $this->getResellerProducts($rid, $pid, 1);
		if($type=='initialSetupFees'){
			if($billing_logID){
				//if the billing log id was included it's to check for add_site
				$query = "SELECT notes FROM resellers_billing WHERE id = '".$billing_logID."'";
				$a = $this->queryFetch($query);
				if($a['notes']=='add_site') $add_site = true;
			}
			if($product['wholesale']['setup_price']>0)
				$credAmount -= $product['wholesale']['setup_price']; //minus setup fee wholesale
			if($product['wholesale']['add_site']>0 && $add_site)
				$credAmount -= $product['wholesale']['add_site']; //minus custom site wholesale
		}
		else if($type=='initialSubscription' || $type=='monthlySubscription'){
			if($product['wholesale']['monthly_price'])
				$credAmount -= $product['wholesale']['monthly_price'];
		}
		
		if($credAmount>0){
			$credAmount = round($credAmount);
			$query = "INSERT INTO resellers_credits 
					(reseller_id, user_id, product_id, type, credit_amount)
					VALUES
					('".$rid."', '".$uid."', '".$pid."', '".$type."', '".$credAmount."')";
			if($this->query($query)) return $this->getLastId();
			else return false;
		}
		else return false;
	}
	
	/**************************************************************************
	// Get reseller credits
	/**************************************************************************/
	function getCredits($rid, $period = ''){
		$query = "SELECT * FROM resellers_credits WHERE reseller_id = '".$rid."'";
		if($period){
			//add to query to only pull rows from a certain time period
		}
		$query .= " ORDER BY timestamp DESC";
		$result = $this->query($query);
		if($result && $this->numRows($result))
			return $result;
		else 
			return false;
	}
	
	/**************************************************************************
	// Display reseller credits
	/**************************************************************************/
	function displayCredits($rid, $period = ''){
		$credits = $this->getCredits($rid, $period);
		
		if($credits && $numItems = $this->numRows($credits)){
			$html = 
				'<ul class="prospects">
					<li class="name4" style="width:120px;"><b>Date</b></li>
					<li style="width:160px;"><b>Type</b></li>
					<li style="width:110px;"><b>For User</b></li>
					<li style="width:140px;"><b>Credit Amount</b></li>
					<li style="width:50px;"><b>Paid</b></li>
					<li style="width:90px;">&nbsp;</li>
				</ul>';
			while($row = $this->fetchArray($credits)){
				$paid = $row['paid'] ? 'Yes' : 'No';
				$html .=  
				'<ul class="prospects">
					<li class="name4" style="width:120px;">'.date('m/d/Y', strtotime($row['timestamp'])).'</li>
					<li style="width:160px;">'.$row['type'].'</li>
					<li style="width:110px;">'.$row['user_id'].'</li>
					<li style="width:140px;">$'.$row['credit_amount'].'</li>
					<li style="width:50px;">'.$paid.'</li>';
				if($row['paid']) $html .= '<li style="width:90px">'.date('n/j/Y', strtotime($row['paid_date'])).'</li>';
				$html .= '</ul>';
				
				if($row['paid']) $totalPaid += $row['credit_amount'];
				else $totalUnpaid += $row['credit_amount'];
			}
			
			if($totalPaid>0) echo '<b>Total amount of paid credits:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$'.$totalPaid.'<br />';
			if($totalUnpaid>0) echo '<b>Total amount of unpaid credits:</b> &nbsp;$'.$totalUnpaid.'<br /><br />';
			echo $html;
		}
		else 
			echo 'No credits to display yet.';
	}
	
	/**************************************************************************
	// Return monthly wholesale cost for a reseller product
	/**************************************************************************/
	function getMonthlyWholesale($pid){
		$query = "SELECT maps_to FROM user_class WHERE id = '".$pid."'";
		$a = $this->queryFetch($query);
		$query = "SELECT monthly_price FROM user_class WHERE id = '".$a['maps_to']."'";
		$b = $this->queryFetch($query);
		
		if($b['monthly_price']) return $b['monthly_price'];
		else return 0.00;
	}
	
	/**************************************************************************
	// Return info about charges due on billing profile setup - false if none
	/**************************************************************************/
	function chargesDueOnSetup($uid, $sixQube = ''){
		if($sixQube) $table = 'sixqube_billing';
		else $table = 'resellers_billing';
		$query = "SELECT id, product_id, transaction_type, amount FROM `".$table."` 
				WHERE user_id = '".$uid."' AND success = 0 AND charge_on_setup = 1";
		$result = $this->query($query);
		
		if($result){
			if($this->numRows($result)){
				$totalChargeAmount = 0.00;
				while($row = $this->fetchArray($result)){
					if($row['transaction_type']=='initialSetupFees'){
						$return['setupCharge']['id'] = $row['id'];
						$return['setupCharge']['amount'] = $row['amount'];
					}
					if($row['transaction_type']=='initialSubscription'){
						$return['monthlyCharge']['id'] = $row['id'];
						$return['monthlyCharge']['amount'] = $row['amount'];
					}
					
					$return['productID'] = $row['product_id'];
					$totalChargeAmount += $row['amount'];
				}
				$return['totalAmount'] = $totalChargeAmount;
				
				return $return;
			}
			else return false;
		}
		else return false;
	}
	
	/**************************************************************************
	// Return just the name of a product
	/**************************************************************************/
	function getProductName($pid){
		if($pid && is_numeric($pid)){
			$query = "SELECT name FROM user_class WHERE id = '".$pid."'";
			$result = $this->queryFetch($query);
			return $result['name'];
		}
	}
}
?>
