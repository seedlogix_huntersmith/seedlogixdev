<?php

#echo dirname(__FILE__);

#require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';

#var_dump(Qube::get_settings());

class CustomPagesDataClass {
	
	
	function CustomPagesDataClass(){
		
		$this->pdo	=	Qube::Start()->GetDB('master');
		
		return; 
		
	}
	
	//get events list
	function getCustomPages($hubID, $navName){

			//build query $select->Where(' domain != "" '); +++  $select->Where(' trashed = "0000-00-00" ');
			$select	=	new DBSelectQuery($this->pdo);
			$select->From('sapphire_6qube.hub_page2');
			$select->Fields('id');
			$select->Where('hub_id = %d', $hubID);
			$select->Where(' type = "nav" ');
			$select->Where(' page_title_url = "'.$navName.'" ');
			$select->Where(' trashed = "0000-00-00" ');

			if($stmt	=	$select->Exec()){

				$row	=	$stmt->fetch();

				$select	=	new DBSelectQuery($this->pdo);
				$select->From('sapphire_6qube.hub_page2');
				$select->Fields('page_title, page_parent_id');
				$select->Where('nav_parent_id = %d', $row['id']);
				$select->Where(' type = "page" ');
				$select->Where(' trashed = "0000-00-00" ');
				$select->Where(' inc_contact = "1" ');
				$select->Where(' inc_nav = "1" ');
				$select->orderBy(' nav_order asc');

				$stmt	=	$select->Exec();

				//$pagelist	=	$stmt2->fetch();

				//return $pagelist;

				$pagelist	=	array();
				while($row	=	$stmt->fetch()){
					$pagelist[$row['page_parent_id']]	=	$row;
				}

				return $pagelist;



			}


	}

	
}

