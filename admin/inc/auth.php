<?php
session_start();
         require_once( dirname(__FILE__) . '/../6qube/core.php');

if(isset($_SESSION['auth'])) {
	if($_SESSION['do_second_validation']){
		header('Location: validate_login_phrase.php');
	}
	else {
		if($_SESSION['auth']==1) {
			if($_SESSION['login_fingerprint'] == md5($_SERVER['HTTP_USER_AGENT'].'sdf97dfndndsha!!')){
				$authError = 0;
				if((time()-$_SESSION['checkin_time'])>=1800){
					//every 30min refresh the user's row in the online_now table
					require_once('local.class.php');
					$local = new Local();
					$local->onlineNow($_SESSION['login_uid'], 'update');
					$_SESSION['checkin_time'] = time();
				}
			}
			else {
				//possible session hijack attempt -- redirect to login screen
				session_destroy();
				header('Location: login.php');
                                exit;
			}
		}
		else {
			//unknown error
			session_destroy();
			header('Location: deny.php');
                        exit;
		}
	}
}
else if($page == 'index'){
	$page = false;
	session_destroy();
	header('Location: login.php');
        exit;
}
else {
	session_destroy();
	header('Location: deny.php');
        exit;
}

