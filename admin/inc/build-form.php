<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
//this script is pulled as an include on theme pages and builds a custom form onto the page
//it MUST be called with the form's ID and user ID.  it can alternately be called with get-type args
if(!$formID) $formID = $_GET['formID'];
if(!$userID) $userID = $_GET['userID'];
if(!$hubID) $hubID = $_GET['hubID'];
if(!$hubPageID) $hubPageID = $_GET['hubPageID'];
if(!$searchEngine) $searchEngine = $_GET['searchEngine'];
if(!$seKeyword) $seKeyword = $_GET['seKeyword'];
//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();
$query = "SELECT user_parent_id, data, options FROM lead_forms WHERE (id = '".$formID."' AND user_id = '".$userID."')";
$form = $hub->queryFetch($query, NULL, 1);
if(!$form){
	//get user info
	$query = "SELECT parent_id, class FROM users WHERE id = '".$userID."'";
	$user = $hub->queryFetch($query, NULL, 1);
	//if the form couldn't be found check if it's a reseller MU form
	$query = "SELECT user_parent_id, data, options FROM lead_forms 
			WHERE user_id = '".$user['parent_id']."' AND id = '".$formID."'
			AND no_access_classes NOT LIKE '".$user['class']."::%'
			AND no_access_classes NOT LIKE '%::".$user['class']."::%'
			AND no_access_classes NOT LIKE '%::".$user['class']."'";
	$form = $hub->queryFetch($query, NULL, 1);
}
if($form['data']){
$fields = $hub->parseCustomFormFields($form['data']);
$options = explode('||', $form['options']);
if($options[3]==1) $ajaxSubmit = true;
$hiddenInputs = '';
//get user's parent's main site for form submit url
//if($form['user_parent_id']){
//	if($form['user_parent_id']==7) $submitUrl = 'http://6qube.com/process-custom-form.php';
//	else {
//		$query = "SELECT main_site FROM resellers WHERE admin_user = '".$form['user_parent_id']."'";
//		$a = $hub->queryFetch($query, NULL, 1);
//		if($a['main_site']) $submitUrl = 'http://'.$a['main_site'].'/process-custom-form.php';
//		else $submitUrl = 'http://6qube.com/process-custom-form.php';
//	}
//}
//else $submitUrl = 'http://6qube.com/process-custom-form.php';
//
//UPDATED 8/24/11: instead use current hub url to prevent cross-domain issues
$query = "SELECT domain, pages_version FROM hub WHERE id = '".$hubID."' AND user_id = '".$userID."'";
$a = $hub->queryFetch($query, NULL, 1);
$submitUrl = $a['domain'].'/process-custom-form.php';
$pv = $a['pages_version'];
if($_GET['style']){
?>
<style type="text/css">
form fieldset {
	border: 0;
}
form ul {
	list-style: none;
}
form ul li {
	padding: 5px;
}
form ul li label {
	display: block;
	font-size: 15px;
	font-weight: bold;
	color: #000;
}
form ul li label span {
	font-size: 13px;
	font-weight: normal;
	color: #666;
}
form ul li input.small {
	width: 100px;
}
form ul li input.medium {
	width: 250px;
}
form ul li input.large {
	width: 400px;
}
form ul li textarea.small {
	width: 200px;
	height: 75px;
}
form ul li textarea.medium {
	width: 350px;
	height: 120px;
}
form ul li textarea.large {
	width: 500px;
	height: 200px;
}
form ul li .checkboxDiv {
	margin:0;
	padding:0;
	display: inline-block;
}
form ul h2 {
	margin-top: 20px;
	margin-bottom: 5px;
}
.highlightError {
	border: 1px solid #ff0000;
}
.checkboxDivHighlightError {
	padding: 1px;
	background-color: #ff0000;
}
</style>
<?	
}
if($_GET['js']){
?>
<script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.form<?=$formID?>').unbind();
	$('.form<?=$formID?>').bind('submit', function(<? if($ajaxSubmit){ ?>event<? } ?>){
		<? if($ajaxSubmit){ ?>
		event.stopPropagation();
		$(this).attr('target', '');
		<? } ?>
		$('.highlightError').each(function(){
			$(this).removeClass('highlightError');
		});
		$('.checkboxDiv').each(function(){
			if($(this).hasClass('checkboxDivHighlightError')){
				$(this).removeClass('checkboxDivHighlightError');
			}
		});
		var errors = 0;
		var numOnlyErrors = 0;
		var numOnlyErrorText = '';
		$('.required').each(function(){
			if($(this).val()==''){
				$(this).addClass('highlightError');
				errors += 1;
			}
			else if($(this).hasClass('checkbox')){
				if(!$(this).attr('checked')){
					$(this).parent().addClass('checkboxDivHighlightError');
					errors += 1;
				}
			}
		});
		$('.numsOnly').each(function(){
			var val = $(this).attr('value');
			if(val && isNaN(val)){
				var fieldName = $(this).parent().find('label').html();
				numOnlyErrors += 1;
				numOnlyErrorText = numOnlyErrorText+fieldName+' may only contain numbers\n';
			}
		});
		if(errors>0 || numOnlyErrors>0){
			if(errors>0) alert('Please complete the highlighted required fields');
			if(numOnlyErrorText) alert(numOnlyErrorText);
			return false;
		}
		<? if($ajaxSubmit){ ?>
		if(errors==0){
			var submitURL = $(this).attr('action');
			var params = $(this).serialize();
			$.post(submitURL, params, function(data){
				if(data.success==1){
					if(!data.redir) alert('Your response was successfully sent!');
					else window.location = data.redir;
				}
				else {
					alert('Sorry, there was an error submitting your form.  Please try again.');
				}
			}, 'json');
			return false;
		}
		<? } ?>
	});
	$('#reset').bind('click', function(){
		if(confirm('Are you sure you want to clear the entire form?')){
			$('.form<?=$formID?> input').each(function(){
				if($(this).attr('id')!='submit' && $(this).attr('id')!='reset'){
					$(this).attr('value', '');
					if($(this).hasClass('checkbox')){
						$(this).attr('checked', false);
					}
				}
			});
			$('.form<?=$formID?> textarea').each(function(){
				$(this).val('');
			});
		}
		return false;
	});
});
</script>
<? } ?>
<form action="<?=$submitUrl?>" method="post" class="custom-form form<?=$formID?><? if($ajaxSubmit){ echo ' ajaxSubmit"'; } else { ?>" target="hiddenIframe"<? } ?> id="<?=$formID?>"><fieldset>
	<ul>
	<?
	if($fields){
	foreach($fields as $fieldNum=>$field){
		$fieldType = $field['type'];
		if($fieldType!='hidden') echo '<li>';
		if($fieldType!='name' && $fieldType!='checkbox' && $fieldType!='section' && $fieldType!='hidden'){ ?>
			<label><?=$field['label']?> <? if($field['label_note']){ ?><span><?=$field['label_note']?></span><? } ?></label>
	<?		if($fieldType=='text'){ ?>
				<input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>" value="<?=$field['default']?>" class="<? if($field['required']){ ?>required<? } ?> <? if($field['numsOnly']){ ?>numsOnly<? } ?> <?=$field['size']?>" id="<?=$hub->fieldNameFromLabel($field['label'])?>" />
	<?		}
			else if($fieldType=='textarea'){ ?>
				<textarea name="<?=$hub->fieldNameFromLabel($field['label'])?>" class="<? if($field['required']){ ?>required<? } ?> <?=$field['size']?>"><?=$field['default']?></textarea>
	<?		}
			else if($fieldType=='select'){ ?>
				<select name="<?=$hub->fieldNameFromLabel($field['label'])?>" <? if($field['required']){ ?>class="required"<? } ?>>
				<?
				if($field['options']){
					foreach($field['options'] as $key=>$value){ ?>
					<option value="<?=$value['key']?>"><?=$value['value']?></option>
				<?	}
				}
				?>
				</select>
	<?		}
			else if($fieldType=='email'){ ?>
				<input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>" value="<?=$field['default']?>" class="email <? if($field['required']){ ?>required<? } ?> <?=$field['size']?>" />
	<?		}
			else if($fieldType=='money'){ ?>
				<span style="float:left;" class="moneySymbol">$</span><input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>" value="<?=$field['default']?>" class="numsOnly <? if($field['required']){ ?>required<? } ?>" style="width:75px;" />
	<?		}
			else if($fieldType=='date'){
				if(!$field['default']) $dateM = $dateD = $dateY = '';
				else if($field['default']=='today'){ $dateM = date('m'); $dateD = date('d'); $dateY = date('Y'); }
				else if($field['default']=='custom'){ $dateM = $field['defaultM']; $dateD = $field['defaultD']; $dateY = $field['defaultY']; }
	?>
				<div style="display:inline-block;">
					<input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>_M" value="<?=$dateM?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:30px;" maxlength="2" />
				</div>
				<span class="dateSeparator">/</span>
				<div style="display:inline-block;">
					<input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>_D" value="<?=$dateD?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:30px;" maxlength="2" />
				</div>
				<span class="dateSeparator">/</span>
				<div style="display:inline-block;">
					<input type="text" name="<?=$hub->fieldNameFromLabel($field['label'])?>_Y" value="<?=$dateY?>" <? if($field['required']){ ?>class="required"<? } ?> style="width:45px;" maxlength="4" />
				</div>
	<?		}
		} //end if($fieldType!='name'){
		else if($fieldType=='name'){
	?>
			<div style="display:inline-block;" class="name-field">
				<label><?=$field['label1']?></label>
				<input type="text" name="<?=$hub->fieldNameFromLabel($field['label1'])?>" value="<?=$field['default1']?>" <? if($field['required1']){ ?>class="required"<? } ?> />
			</div>
			<div style="display:inline-block;" class="name-field">
				<label><?=$field['label2']?></label>
				<input type="text" name="<?=$hub->fieldNameFromLabel($field['label2'])?>" value="<?=$field['default2']?>" <? if($field['required2']){ ?>class="required"<? } ?> />
			</div>
	<?	}
		else if($fieldType=='checkbox'){ ?>
			<div class="checkboxDiv"><input type="checkbox" name="<?=$hub->fieldNameFromLabel($field['label'])?>" class="checkbox <? if($field['required']){ ?>required<? } ?>" <? if($field['checked']){ ?>checked<? } ?> /></div>&nbsp;&nbsp;&nbsp;<label style="display:inline-block;"><?=$field['label']?> <? if($field['label_note']){ ?><span><?=$field['label_note']?></span><? } ?></label>
	<?
		}
		else if($fieldType=='section'){ ?>
			<h2><?=$field['label']?></h2>
	<?
		}
		else if($fieldType=='hidden'){
			$hiddenInputs .= '<input type="hidden" name="'.$hub->fieldNameFromLabel($field['label']).'" value="'.$field['default'].'" />
';
		}
		if($fieldType!='hidden') echo '</li>';
		$fieldType = $dateM = $dateD = $dateY = NULL; //reset vars
	} //end foreach(fields)
	?>
		<li class="submit-li">
			<?=$hiddenInputs?>
			<input type="hidden" name="formID" value="<?=$formID?>" />
			<input type="hidden" name="userID" value="<?=$userID?>" />
			<input type="hidden" name="hubID" value="<?=$hubID?>" />
			<? if($hubPageID){ ?>
			<input type="hidden" name="hubPageID" value="<?=$hubPageID?>" />
			<input type="hidden" name="pagesVersion" value="<?=$pv?>" />
			<? } ?>
            <input name="keyword" value="<?=$seKeyword?>" type="hidden" />
            <input name="search_engine" value="<?=$searchEngine?>" type="hidden" />
			<input type="text" name="hpot" value="" style="width:1px;display:none;" tabindex="-1" />
			<input type="submit" value="<?=$options[1]?>" id="submit" />
			<? if($ajaxSubmit){ ?><input type="hidden" name="ajaxSubmit" value="1" /><? } ?>
			<? if($options[0]){ ?>
			&nbsp;&nbsp;&nbsp;
			<input type="submit" value="Reset" id="reset" />
			<? } ?>
		</li>
	<?
	} //end if($fields)
	?>
	</ul>
</fieldset></form>
<? if(!$ajaxSubmit){ ?>
<iframe src="<?=$submitUrl?>" id="hiddenIframe" name="hiddenIframe" scrolling="no" hidefocus="true" width="1" height="1" frameborder="0" style="display:none;"></iframe>
<? } ?>
<? } else echo 'Form not found ('.$formID.', '.$userID.').'; ?>