<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once 'validator.php';
class Analytics extends Validator { 
   function ANAL_addSite($type, $system_id, $name, $link = ''){
      $settings = $this->getSettings();
      //if($type=='directory'){
      // $name = 'Listing'.$system_id;
      // $listingUrl = 'http://'.$domain.'/'.$this->convertKeywordUrl($row['keyword_one']).
      // $this->convertKeywordUrl($row['company_name']).$row['id'].'/';
      //}
      if(!$link) $link = $name;
      
      $url = "http://analytics.6qube.com/index.php?module=API&method=SitesManager.addSite&siteName=".urlencode($name)."&urls[]={$link}&token_auth={$settings['analytics_token_auth']}";
      $xml_string = file_get_contents($url);
      
      $xml = new SimpleXMLElement($xml_string);
      $analytics_id = "{$xml}";
      
      $this->ANAL_setAnalyticsId($type, $system_id, $analytics_id);
      $this->ANAL_setAnonAccess($analytics_id);
      
      return $analytics_id;
   }
   
   function ANAL_deleteSite($system_id = '', $analytics_id = ''){
      $settings = $this->getSettings();
      if(!$analytics_id) $analytics_id = $this->ANAL_getAnalyticsId($system_id);
      file_get_contents("http://analytics.6qube.com/index.php?module=API&method=SitesManager.deleteSite&idSite={$analytics_id}&token_auth={$settings['analytics_token_auth']}");
   }
   
   function ANAL_updateSite($type, $system_id, $url, $analytics_id = '', $noReturn = ''){
      $settings = $this->getSettings();
      if(!$analytics_id) $analytics_id = $this->ANAL_getAnalyticsId($type, $system_id);
      $url = "http://analytics.6qube.com/index.php?module=API&method=SitesManager.updateSite&idSite={$analytics_id}&siteName={$url}&urls[]={$url}&token_auth={$settings['analytics_token_auth']}";
      file_get_contents($url);
      if(!$noReturn) return $this->ANAL_setAnalyticsId($type, $system_id, $analytics_id);
   }
   
   function ANAL_getAnalyticsId($type, $type_id) {
      $query = "SELECT tracking_id FROM `".$type."` WHERE id = ".$type_id;
      $row = $this->queryFetch($query);
      
      if($row['tracking_id']){
         return $row['tracking_id'];
      } else {
         return false;
      }
   }
   
   function ANAL_setAnalyticsId($type, $system_id, $analytics_id){
      if($system_id && $analytics_id){       
         $sql = "
            UPDATE
               `{$type}`
            SET
               `tracking_id` = {$analytics_id}
            WHERE
               `id` = {$system_id};
         ";
      }
      return $this->query($sql);
   }
   
   function ANAL_setAnonAccess($analytics_id){
      $settings = $this->getSettings();
      $url = "http://analytics.6qube.com/index.php?module=API&method=UsersManager.setUserAccess&userLogin=anonymous&access=view&idSites={$analytics_id}&token_auth={$settings['analytics_token_auth']}";
      
      $xml_string = file_get_contents($url);
      $xml = new SimpleXMLElement($xml_string);
      
      if($xml){ return true; }
      else { return false; }
   }
   
   //function for caching user item graph images
   //refreshes every 24 hours
   function ANAL_getGraphImg($type, $id, $tracking, $user_id, $period = 'day', $date='last30', $bypassTimeout=''){
      $domain = $_SESSION['main_site'] ? $_SESSION['main_site'] : '6qube.com';
      $dir = QUBEROOT . 'users/' .$user_id.'/graph_imgs/';
      $filename = $type."_".$id."_".$tracking.".png";
      $timeout = 86400; //24hrs in seconds = 86400
      if((!is_file($dir.$filename)||filemtime($dir.$filename)<($_SERVER['REQUEST_TIME']-$timeout)) || $bypassTimeout){
         //create image file if it doesn't exist and return url
         $url = "http://analytics.6qube.com/index.php?module=VisitsSummary&action=getEvolutionGraph&idSite=".$tracking."&period=".$period."&date=".$date."&viewDataTable=sparkline&columns[]=nb_visits&token_auth=b00ce9652e919198e1c4daf7bf5aed5a";
         $remoteImg = file_get_contents($url);
         if(!$remoteImg) return false;
         else if(is_dir($dir)){
            //copy image from Piwik to user's dir
            $fc = fopen($dir.$filename, "w");
            fwrite($fc, $remoteImg);
            fclose($fc);
            
            return "http://".$domain."/users/".$user_id."/graph_imgs/".$filename;
         }
         else {
            return "http://".$domain."/admin/img/blank_graph.png";
         }
      }
      else {
         return "http://".$domain."/users/".$user_id."/graph_imgs/".$filename;
      }
   }
   
   //return a view count # for a specified site ID - default past 365 days
   function ANAL_getViewCount($siteID, $periodDays = '2012-01-17,2013-11-25'){
      $settings = $this->getSettings();
      $url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&period=range&date=".$periodDays."&idSite=".$siteID."&token_auth=".$settings['analytics_token_auth'];
      
      $xml_string = file_get_contents($url);
      $xml = new SimpleXMLElement($xml_string);
      
      if($xml){ return $xml; }
      else { return false; }
   }
   
   //return a view count # for a specified site ID - default past 365 days
   function ANAL_getViewCountEmailReports($siteID, $periodDays){
      $settings = $this->getSettings();
      $url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&period=range&date=".$periodDays."&idSite=".$siteID."&token_auth=".$settings['analytics_token_auth'];
      
      $xml_string = file_get_contents($url);
      $xml = new SimpleXMLElement($xml_string);
      
      if($xml){ return $xml; }
      else { return false; }
   }
   
   //get view count data for all sites under specified campaign
   function ANAL_getCampViewCount($campaignID){
      $results = array();
      $campTotal = 0;
      $bestSite = array();
      //get tracking IDs for all hubs under campaign
      $hubs = $this->query("SELECT name, tracking_id FROM hub WHERE cid = '".$campaignID."'");
      if($this->numRows($hubs)){
         while($hub = $this->fetchArray($hubs, 1)){
            $thisCount = $this->ANAL_getViewCount($hub['tracking_id']);
            $results['name'] = $thisCount;
            $campTotal += $thisCount;
            if(!$bestSite || $thisCount>$bestSite['count']){
               $bestSite['name'] = $hub['name'];
               $bestSite['count'] = $thisCount;
            }
         }
      }
      $return = array();
      $return['total'] = $campTotal;
      $return['best'] = array('name'=>$bestSite['name'], 'count'=>$bestSite['count']);
      
      return $return;
   }

   function ANAL_getVisitsForCampaign($resellerID, $period = 'week', $dateRange = 'last7', $format = 'xml'){
      
      //get all websites for the campaign
      $sitesForCampaign = $this->getAllSiteIDsByResellerID($resellerID);
      
      
      if($sitesForCampaign)
      {
         $siteID = $sitesForCampaign[0];
         
         switch ($period)
         {
            case "day":
               $dateRange = date("Y-m-d");
               break;
            case "lastWeek":
            case "week":
               //'week' returns data for the week that contains the specified 'date'
               $period = 'day';
               $date='last7';
               break;
            case "lastMonth":
            case "month":
               //'month' returns data for the month that contains the specified 'date'
               $period = 'month';
               break;
            case "lastYear":
            case "year":
               //'year' returns data for the year that contains the specified 'date'
               $period = 'year';
               break;
            default:
               //'range' returns data for the specified 'date' range.
               //For example to request a report for the range Jan 1st to Feb 15th you would write &period=range&date=2011-01-01,2011-02-15
               $period = 'range';
               
               if(gettype($dateRange) == 'integer')
               {
                  //gets date time from now() to x days
               }
               break;
         }
         
         $TOKEN   =  'b00ce9652e919198e1c4daf7bf5aed5a';    // ALWAYS define TOKEns and security shit in a variable or constant.
         $list_siteids  =  implode(',', $sitesForCampaign);
         $url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&period=".$period."&date=".$dateRange."&idSite=".$list_siteids."&token_auth=$TOKEN&format=".$format;         
         $results_string   =  file_get_contents($url);
         $xml  =  new SimpleXMLElement($results_string);
         
#        deb($url, $list_siteids, $results, $format);
#        $xml  =  new SimpleXMLElement($results_string);
         
#        deb($results_string, $xml);
         
         
         $weeklyVisits = array( 
            array(strtotime("2012-07-01 UTC") * 1000,1),
            array(strtotime("2012-07-02 UTC") * 1000,7),
            array(strtotime("2012-07-03 UTC") * 1000,2),
            array(strtotime("2012-07-04 UTC") * 1000,3),
            array(strtotime("2012-07-05 UTC") * 1000,5),
            array(strtotime("2012-07-06 UTC") * 1000,1),
            array(strtotime("2012-07-07 UTC") * 1000,9)
            );
         
         $weeklyLeads = array( 
            array(strtotime("2012-07-01 UTC") * 1000,1),
            array(strtotime("2012-07-02 UTC") * 1000,4),
            array(strtotime("2012-07-03 UTC") * 1000,1),
            array(strtotime("2012-07-04 UTC") * 1000,1),
            array(strtotime("2012-07-05 UTC") * 1000,3),
            array(strtotime("2012-07-06 UTC") * 1000,0),
            array(strtotime("2012-07-07 UTC") * 1000,5)
            );
         
         $results =  array($weeklyVisits, $weeklyLeads);
         
         return $results;
         
         /*
         foreach($xml->result as $result)
         {
           $idSite   =  (string)$result['idSite'];
           echo $idSite, "\n";
           
           foreach($result->result as $numVisits)
           {
             $date   =  (string)$numVisits['date'];
             $results[$idSite][$date] = (string)$numVisits;
           }
         }
         
         return $results;
         
          
         foreach($sitesForCampaign AS $siteID)
         {
            
            $url = "http://analytics.6qube.com/index.php?module=API&method=VisitsSummary.getVisits&period=".$period."&date=".$dateRange."&idSite=".$siteID."&token_auth=b00ce9652e919198e1c4daf7bf5aed5a&format=".$format;
            
            $xml_string = file_get_contents($url);
            $xml = new SimpleXMLElement($xml_string);
            
            $TotalHits += $xml;
         }
         
         return $TotalHits;
         */
      }
      else
      {
         //try something else;
      }
   }

   //get view count data for all sites under specified reseller
   function ANAL_getUsersSitesViewCount($resellerID){
      //get tracking IDs for all hubs under campaign
      //get number of leads
      //AND hub_parent_id = '7024'
      //AND hub_parent_id = '8220'

      $query = "SELECT id, user_id, cid, name, tracking_id, domain, company_name, phone, street, city, state, zip, created FROM hub WHERE user_parent_id = '".$resellerID."'
               AND trashed = '0000-00-00' 
               AND company_name != 'Supercircuits'
               AND domain != ''
               AND cid != '99999'
               ORDER BY name ASC";
      if($hubs = $this->query($query)){
         while($row = $this->fetchArray($hubs)){

            $query = "SELECT count(id) FROM leads WHERE hub_id = '".$row['id']."'";
         $leads1 = $this->queryFetch($query);
         $query = "SELECT count(id) FROM contact_form WHERE type_id = '".$row['id']."'";
         $leads2 = $this->queryFetch($query);

         $numProspects = $leads1['count(id)']+$leads2['count(id)'];
           
		   echo ''.$row['id'].', '.$row['user_id'].', '.$row['tracking_id'].', '.$row['company_name'].', '.$row['domain'].', '.$row['phone'].', '.$row['street'].', '.$row['city'].', '.$row['state'].', '.$row['zip']; 
           //echo 'Name: '.$row['name'].', view count: '.$this->ANAL_getViewCount($row['tracking_id']).', lead count: '.$numProspects.'<br />';
          // echo ''.$row['id'].'|'.$row['user_id'].'|'.$row['tracking_id'].'|'.$row['company_name'].'|'.$row['domain'].'|'.$row['phone'].'|'.$row['street'].'|'.$row['city'].'|'.$row['state'].'|'.$row['zip'].'|'.$this->ANAL_getViewCount($row['tracking_id']).'|'.$numProspects.'|'.$row['created'].'<br />';
        }
      }
   }

   function getAllSiteIDsByResellerID($resellerID){
   
      $query = "SELECT id, user_id, cid, name, tracking_id FROM hub WHERE user_parent_id = '".$resellerID."'
               AND trashed = '0000-00-00' 
               ORDER BY name ASC";
      
      if($hubs = $this->query($query))
      {
         $siteViewsArray = array();
         
         while($row = $this->fetchArray($hubs)){
         
         //array_push($siteViewsArray, $this->ANAL_getViewCount($row['tracking_id']));
         array_push($siteViewsArray, $row['tracking_id']);
}
        
        return $siteViewsArray;
        
      }
      else
         return false;
   }
   
   //get view count data for all sites under specified reseller
   //this returns an array instead of echoing the data
   function ANAL_getSitesViewCount($resellerID){
      //get tracking IDs for all hubs under campaign
      //get number of leads

      $query = "SELECT id, user_id, cid, name, tracking_id, phone FROM hub WHERE user_parent_id = '".$resellerID."'
               AND trashed = '0000-00-00' 
               ORDER BY name ASC";
      if($hubs = $this->query($query)){
         
         $siteViewsArray = array();
         
          while($row = $this->fetchArray($hubs))
          {
               $query = "SELECT count(id) FROM leads WHERE hub_id = '".$row['id']."'";
            $leads1 = $this->queryFetch($query);
            $query = "SELECT count(id) FROM contact_form WHERE type_id = '".$row['id']."'";
            $leads2 = $this->queryFetch($query);

            $numProspects = $leads1['count(id)']+$leads2['count(id)'];
         
            $siteViewsArray[] = array('viewCount' => $this->ANAL_getViewCount($row['tracking_id']), 'leadCount' => $numProspects);
          }
        
        return $siteViewsArray;
        
      }
      else
         return false;
   }
}
?>
