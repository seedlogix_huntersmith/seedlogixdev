<?php

#echo dirname(__FILE__);

#require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';

#var_dump(Qube::get_settings());

class HubLocationsMapClass {
	
	
	function HubLocationsMapClass(){
		
		$this->pdo	=	Qube::GetPDO();
		
		return; 
		
	}

	
	//get events list
	function getLocations($hubparentid = '', $full_home){

			//build query $select->Where(' domain != "" '); +++  $select->Where(' trashed = "0000-00-00" ');
			$select	=	new DBSelectQuery($this->pdo);
			$select->From('sapphire_6qube.hub');
			$select->Fields('hub_parent_id, domain, city, state, street, zip, phone, full_home');
			$select->Where('hub_parent_id = %d', $hubparentid);
			$select->Where(' domain != "" ');
			if($full_home) $select->orderBy(' state asc, full_home asc');
			else $select->orderBy(' state asc, city asc');

#try{

				$stmt	=	$select->Exec();
			


				
			// else return an array
			
			$locationlist	=	array();
			while($row	=	$stmt->fetch()){
				$locationlist[$row['domain']]	=	$row;
			}
		
			return $locationlist;

	}

	//get events list
	function getAgents($hubparentid = '', $city, $full_home, $remove_font, $team){

		//build query
		$select	=	new DBSelectQuery($this->pdo);
		$select->From('sapphire_6qube.hub h, sapphire_6qube.user_info u');
		$select->Fields('h.user_id, h.hub_parent_id, h.profile_photo, h.domain, h.keyword1, h.keyword2, h.keyword3, h.keyword4,
		h.keyword5, h.keyword6, h.button_1,
		 h.button1_link, h.button_2, h.button2_link, h.button_3, h.button3_link, h.legacy_gallery_link, h.city, h.state, h.street, h.zip,
		  h.phone, h.full_home, h.remove_font, h.httphostkey');
		$select->Where('h.hub_parent_id = %d', $hubparentid);
		$select->Where(' h.user_id = u.user_id ');
		$select->Where(' h.domain != "" ');
		$select->Where(' h.trashed = "0000-00-00" ');
		if($city) $select->Where(' h.city = "'.$city.'" ');
		if($full_home) $select->Where(' h.full_home = "'.$full_home.'" ');
		if($remove_font) $select->Where(' h.remove_font = "'.$remove_font.'" ');
		if($team) $select->orderBy(' u.api_id asc, h.state asc, h.full_home asc, h.city asc, h.keyword1 asc');
		else $select->orderBy(' u.tcity asc, h.state asc, h.full_home asc, h.city asc, h.keyword1 asc');

#try{

		$stmt	=	$select->Exec();




		// else return an array

		$agentlist	=	array();
		while($row	=	$stmt->fetch()){
			$agentlist[$row['domain']]	=	$row;
		}

		return $agentlist;

	}

	//get events list
	function getLocationsByTheme($themeid = '', $category, $count, $start, $limit, $city, $keyword, $quality){
		//build query $select->Where(' domain != "" '); +++  $select->Where(' trashed = "0000-00-00" ');
		$select	=	new DBSelectQuery($this->pdo);
		$select->From('sapphire_6qube.hub');
		$select->Fields('id, company_name, domain, city, state, street, zip, phone, bg_img, description, logo, overview,
		offerings, yelp, facebook, twitter, google_local, linkedin, instagram, pinterest, site_title, google_places,
		mobile_domain, facebook_id, blog, links_color, bg_color, meta_description, meta_keywords, category');
		$select->Where('theme = %d', $themeid);
		$select->Where(' domain != "" ');
		$select->Where(' company_name != "" ');
		$select->Where(' city != "" ');
		$select->Where(' state != "" ');
		$select->Where(' zip != "" ');
		$select->Where(' category != "" ');
		$select->Where(' description != "" ');
		if($quality) $select->Where(' bg_img != "" ');
		if($city) $select->Where(' city = "'.$city.'" ');
		if($category) $select->Where(' category LIKE "%'.$category.'%" ');
		if($keyword) {
			$select->Where(' company_name LIKE "%' . $keyword . '%" ');
		}
		$select->orderBy(' created desc ');
		$select->limit($start,$limit);



#try{

		$stmt	=	$select->Exec();



		if($count)
		// else return an array

		$locationlist	=	array();
		while($row	=	$stmt->fetch()){
			$locationlist[$row['domain']]	=	$row;
		}

		return $locationlist;

	}

	//get events list
	function getAgentsbyRole($roleid = '', $biz_company_name){


		$select	=	new DBSelectQuery($this->pdo);
		$select->From('sapphire_6qube.6q_userroles r, sapphire_6qube.user_info u');
		$select->Fields('r.role_ID, r.user_ID, u.firstname, u.lastname, u.account_number, u.biz_contact_name, u.bio, u.profile_photo, u.biz_company_name');
		$select->Where('r.role_ID = %d', $roleid);
		$select->Where(' r.user_ID = u.user_id ');
		//$select->Where(' u.biz_company_name = "'.$biz_company_name.'" ');
		$select->Where(' u.biz_company_name LIKE "%'.$biz_company_name.'%" ');
		$select->orderBy(' u.tcity asc, u.firstname asc, u.lastname asc');

		$stmt	=	$select->Exec();


		$agentlist	=	array();
		while($row	=	$stmt->fetch()){
			$agentlist[$row['user_ID']]	=	$row;
		}

		return $agentlist;


	}

	function getLocationsByParentID($parentid = '', $city, $state, $limit){
		//build query $select->Where(' domain != "" '); +++  $select->Where(' trashed = "0000-00-00" ');
		if($city && $state) {
			$select = new DBSelectQuery($this->pdo);
			$select->From('sapphire_6qube.hub');
			$select->Fields('id, company_name, domain, city, state, phone, logo, facebook');
			$select->Where('user_parent_id = %d', $parentid);
			$select->Where(' domain != "" ');
			$select->Where(' httphostkey NOT LIKE "%powerleads.biz%" ');
			$select->Where(' httphostkey NOT LIKE "%alibidealer.com%" ');
			$select->Where('user_id != %d', $parentid);
			if ($city) $select->Where(' city = "' . $city . '" ');
			if ($state) $select->Where(' state = "' . $state . '" ');
			$select->orderBy(' created desc ');
			$select->limit($limit);


#try{

			$stmt = $select->Exec();


			if ($count)
				// else return an array

				$locationlist = array();
			while ($row = $stmt->fetch()) {
				$locationlist[$row['domain']] = $row;
			}

			return $locationlist;
		} else {
			return false;
		}

	}

	function getLocalListings($city = '', $state = ''){

		//build query $select->Where(' domain != "" '); +++  $select->Where(' trashed = "0000-00-00" ');
		$select	=	new DBSelectQuery($this->pdo);
		$select->From('sapphire_6qube.directory');
		$select->Fields('id, company_name, city, state, street, zip, phone, category');
		$select->Where(' city = "' . $city . '" ');
		$select->Where(' state = "' . $state . '" ');
		$select->Where(' trashed = "0000-00-00" ');
		$select->orderBy(' created DESC');

        $stmt = $select->Exec();

		$locationlist	=	array();
		while($row	=	$stmt->fetch()){
			$locationlist[$row['id']]	=	$row;
		}

		return $locationlist;

	}
	
	
	

	
	
	
}

