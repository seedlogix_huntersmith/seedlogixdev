<?php
////////////////////////////////////////////////////////////////////////////////////////
// Class: DbConnector
// Purpose: Connect to a database, MySQL version
///////////////////////////////////////////////////////////////////////////////////////
require_once 'systemcomponent.php';

class DbConnector extends SystemComponent {

        var $dolog  =   false;
        var $querylog   =   array();
        var $querycount =   0;
        var $totalquerytime =   0;
        
        protected $db_user = NULL;
        protected $db_host	=	NULL;
        protected $db_pass	=	NULL;
        protected $db_dbname	=	NULL;
	protected $theQuery;
	protected $links       =   array();
        
        protected $last_link    =   NULL;
        protected static $master_db =   NULL;
        protected static $slave_db  =   NULL;
	protected static $_dolog	=	false;
        protected static $count_instances   =   0;
        
        function DbConnector()
        {
            // delay connections
            if(self::$count_instances == 0)
{
                register_shutdown_function(array(&$this, 'close'));
		self::$_dolog	=	false;	// not sure why i had this enabled for the particular domain $_SERVER['HTTP_HOST'] == 'press.6qube.com';
}
            $this->dolog	=	self::$_dolog;//false;//$_SERVER['HTTP_HOST'] == 'press.6qube.com' ? true : false;
            self::$count_instances++;            
        }
        
        function connect_slave()
        {
            static $has_slave = 1;
		if(!$has_slave) return NULL;

            // Load settings from parent class
            $settings = SystemComponent::getSettings();

            // IF SLAVE DB IS NOT CONFIGURED, return NULL
            if(!isset($settings['dbslaves']) || Qube::getDBConnectionInfo() == false)
            {
                $has_slave = 0;
		deb('connecting master.');
                return $this->connect_master();
            }
            // Get the main settings from the array we just loaded
            $host = $settings['dbslaves'][0]['dbhost'];
            $db = $settings['dbslaves'][0]['dbname'];
            $user = $settings['dbslaves'][0]['dbusername'];
            $pass = $settings['dbslaves'][0]['dbpassword'];

            $link   =   NULL;
            if($link = @mysqli_connect($host, $user, $pass)){
                    @mysqli_select_db($link, $db);
#		deb('slave connection successful!' . $host);
            }else
{
		$link	=	$this->connect_master();
#                die('SOMETHING WENT WRONG. PLEASE REFRESH (3)');
}
            self::$slave_db = $link;
            return $link;
        }
        
	//*** Function: DbConnector, Purpose: Connect to the database ***
	function connect_master(){
            if(self::$master_db) return self::$master_db;
#            deb('connectingn to master');
                if(isset($_GET['debug']))
                {
                    $this->dolog    =true;
                }
                
#                if(in_array(@$_SERVER['SERVER_NAME'], array('pastephp.com', 'hubpreview.6qube.com')))
                if(FALSE)    #strpos($_SERVER['REQUEST_URI'], 'hubs') !==  FALSE)
                {
                    $this->dolog    =   1;
                }
                
		// Load settings from parent class
		$settings = SystemComponent::getSettings();
		
		// Get the main settings from the array we just loaded
		$host = $settings['dbhost'];
		$db = $settings['dbname'];
		$user = $settings['dbusername'];
		$pass = $settings['dbpassword'];
                
                $link   =   NULL;
		
                    // Connect to the database
		if($link = @mysqli_connect($host, $user, $pass))
                {
                        self::$master_db    =   $link;
			@mysqli_select_db($link, $db);
		}else
                    Qube::Fatal('Db Connection Failed');
#echo 'master connection success';                
            return $link;
	}

            function fetchColumn($result)
            {
                if(is_string($result)) return $this->fetchColumn ($this->query($result));
                
                return array_pop(mysqli_fetch_row($result));
            }
            
	//*** Function: query, Purpose: Execute a database query ***
	function query($query, $custom_db = ''){

	$admin_request	=	(strpos($_SERVER['REQUEST_URI'], '/admin/') !== FALSE);
	
	if(!$admin_request){
#		trigger_error($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		$link = NULL;
	}else{
            $link   =   $this->connect_master();	//self::$master_db;
	}

                // DETERMINE THE TYPE OF SERVER TO RUN THE QUERY ON
                if(defined('HYBRID_PATH') && preg_match('/^\s*SELECT/i', $query))
                {
	                    $link   =   is_null(self::$slave_db) ? $this->connect_slave() : self::$slave_db;
                }
	# else	 deb($query);
                

                if(is_null($link))
                {
#                    echo 'running other query on master <h1>', $query, '</h1>';
                    
                    $link   =   $this->connect_master();
                }
                
		if($custom_db) {
			mysqli_select_db($link, $custom_db);
			$r	=	$this->query($query, '');//mysqli_query($query, $this->link);
			mysqli_select_db($link, $db);
			return $r;
		}
		else
                {
                    if($this->dolog)
                    {
                        $time   =   microtime(true);
                        $r  =   mysqli_query($link, $query);//, $link);
                        $time   =   microtime(true) - $time;
                        $this->querycount++;
                        $this->totalquerytime   += $time;                                
                        $this->querylog[]   =   array($query, $time);

                        $this->last_link    =   $link;                        
                        return $r;
                    }
                        $this->last_link    =   $link;
			return mysqli_query($link, $query);
                }
	}

	function getLastLink(){
		return $this->last_link;
	}

	//*** Function: fetchArray, Purpose: Get array of query results ***
	function fetchArray($result, $assocOnly = '', $assocOnly2 = ''){
		if(is_array($result)){
			trigger_error(print_r(debug_backtrace(), 1), E_USER_ERROR);
		}
		if(is_bool($result) && $_GET['amado']){ echo '<pre>';
			debug_print_backtrace(); }
		if($assocOnly || $assocOnly2) return mysqli_fetch_array($result, MYSQLI_ASSOC);
		else return mysqli_fetch_array($result);
	}
	
	//*** Function: queryFetch, Purpose: Execute a database query and return row array ***
	function queryFetch($query, $custom_db = '', $assocOnly = ''){
		if($custom_db){
#			mysqli_select_db($custom_db);
			$result = $this->query($query, $custom_db); #mysqli_query($query, $this->link);
			if($result){
				if($assocOnly) return mysqli_fetch_array($result, MYSQLI_ASSOC);
				else return mysqli_fetch_array($result);
			}
			else
				return false;
			mysqli_select_db($db);
		}
		else {
			$result = $this->query($query); #mysqli_query($query, $this->link);
			if($result){
				if($assocOnly) return mysqli_fetch_array($result, MYSQLI_ASSOC);
				else return mysqli_fetch_array($result);
			}
			else
				return false;
		}
    }
    
	//*** Function: mysqli_num_rows
	function numRows($result){
		return @mysqli_num_rows($result);
	}

	//*** Function: close, Purpose: Close the connection ***
	function close(){
                
                if($this->dolog)
                {
                    $link   =   is_null(self::$master_db) ? $this->connect_master() : self::$master_db;
                    
                    if($link)
                    {
                        $key    =   mysqli_real_escape_string($link, 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
                        $q      =   'INSERT DELAYED INTO 6qube_benchmarks (`type`, `url`, `value`) VALUES("totalquerytime", "' . $key . '", "' . $this->totalquerytime . '"), ("totalquerycount", "' . $key . '", "' . $this->querycount . '"); ';
    #                    echo $q;
                        if(!mysqli_query($link, $q))
                                echo '<!-- ' . mysqli_error() . ' -->';

                        if($this->totalquerytime > 0)
                        {
                            foreach($this->querylog as $querydata):
                                $qs = mysqli_real_escape_string($link, $querydata[0]);
                                $q  =   'INSERT DELAYED INTO 6qube_benchmarks (`type`, `url`, `key`, `value`) VALUES("querylog", "' . $key . '", "' . $qs . '", "' . $querydata[1] . '")';
                                mysqli_query($link, $q);                            
                            endforeach;
                        }
                    }
                }
		
		if(!is_null(self::$slave_db) && self::$slave_db == self::$master_db)
{
		mysqli_close(self::$slave_db);
}else{                
                if(!is_null(self::$slave_db))
                    mysqli_close(self::$slave_db);
                
                if(!is_null(self::$master_db))
                    mysqli_close(self::$master_db);
}
	}
	
	function sanitizeInput($string){
#                var_dump(debug_backtrace());
		if(empty($string)) return '';
		if(!is_string($string)) Qube::Fatal('not sring', $string);
		if(get_magic_quotes_gpc()){  // prevents duplicate backslashes
			$string = stripslashes($string);
		}
		$string = mysqli_real_escape_string($this->connect_master(), $string);
		return $string;
	}
	
	function safe_string_escape($str){
		$len = strlen($str);
		$escapeCount = 0;
		$targetString = '';
		for($offset=0;$offset<$len;$offset++){
			switch($c = $str{$offset}){
				case "'":
					// Escapes this quote only if its not preceded by an unescaped backslash
					if($escapeCount % 2 == 0) $targetString .= "\\";
					$escapeCount = 0;
					$targetString .= $c;
				break;
				case '"':
				// Escapes this quote only if its not preceded by an unescaped backslash
					if($escapeCount % 2 == 0) $targetString .= "\\";
					$escapeCount = 0;
					$targetString .= $c;
				break;
				case '\\':
					$escapeCount++;
					$targetString .= $c;
				break;
				default:
					$escapeCount = 0;
					$targetString .= $c;
			}
		}
		return $targetString;
	}
	
	function getLastId(){
		return mysqli_insert_id($this->last_link);
	}
	
	function passSalt(){
		return 'AA<,sd9dfgd-,d,.D>FG>FG:Ld=d=d0fgo3W#Msjsnxxfg';
	}
	
	function encryptPassword($input){
		$enc1 = md5($input);
		$enc2 = md5($enc1.$this->passSalt());
		return $enc2;
	}
	
	function checkPass($inputPass, $checkPass = '', $userID = ''){
		$salt = $this->passSalt();
		$enc1 = md5($inputPass);
		$enc2 = md5($enc1.$salt);
		if($userID && is_numeric($userID)){
			$query = "SELECT password FROM users WHERE id = '".$userID."'";
			if($result = $this->queryFetch($query)){
				if($enc2 == $result['password']) return true;
				else return false;
			}
			else return false;
		}
		else if($inputPass && $checkPass){
			if($enc2 == $checkPass) return true;
			else return false;
		}
		else return false;
	}
}
?>
