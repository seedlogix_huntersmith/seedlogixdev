<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Phone extends Local {
	var $counter = 0;
	protected $cookiefile;
	
	/*
	function Phone()
	{
	  parent::Local();
	  $this->cookiefile	=	'cookie-.txt';
	}
	*/
	
	//get customer id
	function getCustomerID($uid, $cid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($cid){
			//build query
			$query = "SELECT id, customer_id FROM phone_numbers WHERE user_id = '".$uid."' 
		    AND cid = '".$cid."'";
			$return = $this->queryFetch($query, NULL, 1);
			
			//return final data!
			return $return;
		}
		else {
			
			//build query
			$query = "SELECT id, customer_id FROM phone_numbers WHERE user_id = '".$uid."'";
			$return = $this->queryFetch($query, NULL, 1);
			
			//return final data!
			return $return;
			
		
		}
	}
	 
	//get mailboxes 
	function getMailboxes($uid, $custID){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
		  $cookiefile	=	QUBEPATH . 'cache/cookie-' . $custID . '.txt';
		  
			//$req_url = 'https://my.patlive.com/api/v1.0/Customer/'.$custID.'/Mailbox/?format=json';
			$req_url = '';
			$hCurl = curl_init();
			$creds_pass = 'testing';
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
			
			if($_SERVER['SERVER_NAME'] == 'pastephp.com')
			{
			echo '<textarea>';
			  echo $cookiefile;
			  curl_setopt($hCurl, CURLOPT_COOKIEJAR, $cookiefile);
			  curl_setopt($hCurl, CURLOPT_COOKIEFILE, $cookiefile);
			  
			  curl_setopt($hCurl, CURLOPT_STDERR, fopen('php://output', 'w+'));
			  curl_setopt($hCurl, CURLOPT_VERBOSE, 1);
			$data = curl_exec($hCurl);
			curl_close($hCurl);
			echo $data;
			echo '</textarea>';
			
			}else{
			$data = curl_exec($hCurl);
			}
			
//			curl_close($hCurl);
			$json = json_decode($data, TRUE);
		
			$mailboxes = $json['Data'];
			return $mailboxes;
		}
		else return false;
	}
	
	//get mailbox numbers
	function getMBXNumbers($uid, $mid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$mid = is_numeric($mid) ? $mid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//$req_url = 'https://my.patlive.com/api/v1.0/Mailbox/'.$mid.'/PhoneNumbers/?format=json';
			$req_url = '';
			$hCurl = curl_init();
			$creds_pass = md5('testing');
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
			
			$data = curl_exec($hCurl);
			curl_close($hCurl);
			$json = json_decode($data, TRUE);
		
			$mailboxNumbers = $json['Data'];
			return $mailboxNumbers;
		}
		else return false;
	}
	
	//get call details
	function getCallDetails($uid, $mid = '', $rqtype = '', $currentpage = '', $limit = '', $SortBy = 'DateTime', $SortDir = 'DESC'){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//$req_url = 'https://my.patlive.com/api/v1.0/Mailbox/'.$mid.'/CallDetails/?Format=json&RequestType='.$rqtype.'&PageSize='.$limit.'&Page='.$currentpage.'&SortBy='.$SortBy.'&SortDir='.$SortDir;
			$req_url = '';
			$hCurl = curl_init();
			$creds_pass = 'testing';
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
			
			$data = curl_exec($hCurl);
			curl_close($hCurl);
			
			$json = json_decode($data, TRUE);
		
			$callReports = $json['Data'];
			
			//curl_close($hCurl);
            
			return $json;
		}
		else return false;
	}

	
	//get mailbox numbers
	function getCallCount($uid, $mid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//$req_url = 'https://my.patlive.com/api/v1.0/Mailbox/ysq5OYb3417nZEOCHTic7w2/callcount/01-01-2013/12-9-2013/?format=json';
			$req_url = '';
			$hCurl = curl_init();
			$creds_pass = md5('testing');
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
			
			$data = curl_exec($hCurl);
			curl_close($hCurl);
			$json = json_decode($data, TRUE);
		
			$mailboxNumbers = $json['Data'];
			return $mailboxNumbers;
		}
		else return false;
	}
	
	//get call details
	function getCallRecording($uid, $mid = '', $phoneNum){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//$req_url = 'https://my.patlive.com/api/v1.0/Mailbox/'.$mid.'/CallDetails/?Format=json&RequestType='.$rqtype.'&PageSize='.$limit.'&Page='.$currentpage.'';
			//$req_url = 'https://my.patlive.com/api/v1.0/Mailbox/'.$mid.'/SearchMessages/?format=json&MessageFrom='.$phoneNum.'';
			$req_url = '';
			$hCurl = curl_init();
			$creds_pass = 'testing';
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
			
			$data = curl_exec($hCurl);
			curl_close($hCurl);
			
			$json = json_decode($data, TRUE);
		
			$recordings = $json['Data'];
			
			return $recordings;
		}
		else return false;
	}

	function getCallDetailsNew($did, $rqtype = '', $currentpage = '', $limit = ''){

		//authorize
		if($did){
			$continue = true;
		}
		if($continue){

			require_once HYBRID_PATH . 'api/plivo/plivo.php';
			require_once HYBRID_PATH . 'api/plivo/config.php';

			if(!$conf_pilvo_authid) $conf_pilvo_authid = 'MAYJA4ZWUYNMU1YJG2ZD';
			if(!$conf_plivo_authtoken) $conf_plivo_authtoken = 'NDM0YjUxM2FiZTA4Nzc4YzkwM2M1ZGFmMDE5Nzgw';

			$p = new RestAPI($conf_pilvo_authid, $conf_plivo_authtoken);

			//$response = $p->get_cdrs();
			//print_r ($response);

			if(!$limit) $limit = 10;
			if(!$currentpage) $currentpage = 0;

			if($rqtype) {
				$v = explode(",", $rqtype);
				$starttime = $v[0];
				$endtime = $v[1];
				// Filtering the records
				$params = array(
					'end_time__gt' => $starttime . ' 00:00', # Filter out calls according to the time of completion. gte stands for greater than or equal.
					'end_time__lt' => $endtime . ' 23:59', # Filter out calls according to the time of completion. lte stands for less than or equal.
					'call_direction' => 'inbound', # Filter the results by call direction. The valid inputs are inbound and outbound
					//'from_number' => '1111111111', # Filter the results by the number from where the call originated
					'to_number' => $did, # Filter the results by the number to which the call was made
					'limit' => $limit, # The number of results per page
					'offset' => $currentpage # The number of value items by which the results should be offset
				);
			} else {

				// Filtering the records
				$params = array(
					//'end_time__gt' => $starttime . ' 00:00', # Filter out calls according to the time of completion. gte stands for greater than or equal.
					//'end_time__lt' => $endtime . ' 00:00', # Filter out calls according to the time of completion. lte stands for less than or equal.
					'call_direction' => 'inbound', # Filter the results by call direction. The valid inputs are inbound and outbound
					//'from_number' => '1111111111', # Filter the results by the number from where the call originated
					'to_number' => $did, # Filter the results by the number to which the call was made
					'limit' => $limit, # The number of results per page
					'offset' => $currentpage # The number of value items by which the results should be offset
				);
			}

			$response = $p->get_cdrs($params);

			//print_r ($response);

			return $response;

		}
		else return false;


	}
	//get call details
	function getCallRecordingNew($uid, $CallUUID){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';

		//authorize
		if($uid){
			$continue = true;
		}

		if($continue){

			require_once HYBRID_PATH . 'api/plivo/plivo.php';
			require_once HYBRID_PATH . 'api/plivo/config.php';

			$p = new RestAPI($conf_pilvo_authid, $conf_plivo_authtoken);

			//$response = $p->get_cdrs();
			//print_r ($response);

			// Filtering the records
			$params = array(
				//'call_direction' => 'inbound', # Filter the results by call direction. The valid inputs are inbound
				# and outbound
				//'from_number' => '1111111111', # Filter the results by the number from where the call originated
				'call_uuid' => $CallUUID, # Filter the results by the number to which the call was made
				//'limit' => '20', # The number of results per page
				//'offset' => '0' # The number of value items by which the results should be offset
			);
			$recordings = $p->get_recordings($params);


			return $recordings;
		}
		else return false;
	}
	//get customer id
	function getDID($uid, $cid = ''){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		$cid = is_numeric($cid) ? $cid : 0;

		//authorize
		if($uid){
			$continue = true;
		}

		if($cid){
			//build query
			$query = "SELECT phone_num, date_created FROM phone_numbers WHERE user_id = '".$uid."'
		    AND cid = '".$cid."'";
			$return = $this->queryFetch($query);

			//return final data!
			return $return;
		}
		else {

			//build query
			$query = "SELECT phone_num, date_created FROM phone_numbers WHERE user_id = '".$uid."'";
			$return = $this->queryFetch($query, NULL, 1);

			//return final data!
			return $return;


		}
	}

	function blacklistCalls() {

		$blacklist = array('12169206413', '16016188876', '16014150673', '18887895999', '17182873762', '16044090872',
			'12318934583', '15149695508', '15125547626', '15123097095', '17036060890', '19545055388');

		return $blacklist;
	}
	
}
?>