<?php
	session_start();
         require_once( dirname(__FILE__) . '/../6qube/core.php');
	
        require_once( dirname(__FILE__) . '/../6qube/core.php');
	require_once(QUBEADMIN . 'inc/auth.php');
	require_once(QUBEADMIN . 'inc/local.class.php');
	$connector = new Local();
	
	//get page settings
	$settings = $connector->getSettings();
	
	//set campaign ID
	if(isset($_GET['CID'])){
		$cID = is_numeric($_GET['CID']) ? $_GET['CID'] : '';
		$_SESSION['campaign_id'] = $cID;
	}
	else{
		$cID = $_SESSION['campaign_id'];
	}
	$connector->setCampaignID($cID);
	//get campaign name
	$query = "SELECT name, default_snapshot_id FROM campaigns WHERE id = '".$cID."'";
	$result = $connector->queryFetch($query, NULL, 1);
	$cName = $result['name'];
	$graphID = $result['default_snapshot_id'] ? $result['default_snapshot_id'] : $connector->getHubOrDirID($cID);
	if(!$graphID) $graphID = 26;
	
	//set page variables 
	$display_count = $settings['display_count'];
	$dir_count = $hub_count = $press_count = $blog_count = $art_count = 0;
	
	$query2 = "SELECT id, name FROM hub WHERE tracking_id = '".$result['default_snapshot_id']."'";
	if($result2 = $connector->queryFetch($query2)){
	   $snapshotItemType = 'hub';
	   $snapshotItemID = $result2['id'];
	   $snapshotItemName = $result2['name'];
	   $snapshopForm = 'settings';
	}
	else {
	  //check directory table
	  $query2 = "SELECT id, name FROM directory WHERE tracking_id = '".$result['default_snapshot_id']."'";
	  if($result2 = $connector->queryFetch($query2)){
		$snapshotItemType = 'directory';
		$snapshotItemID = $result2['id'];
		$snapshotItemName = $result2['name'];
		$snapshopForm = 'listings';
	  }
	}
	
?>
<script type="text/javascript">
$(".uniformselect").uniform();
$('#changeGraph').die();
$('#changeGraph').live('change', function(){
	var newID = $(this).attr('value');
	$.get('inc/analytics_graph.php?id='+newID, function(data){
		$('#trafficGraph').html(data);
	});
});
$('#setDefaultGraph').die();
$('#setDefaultGraph').live('click', function(){
	var newDefault = $('#changeGraph').attr('value');
	var cid = '<?=$cID?>';
	$.get('inc/analytics_graph.php?id='+newDefault+'&cid='+cid+'&setDefault=1', function(data){});
	return false;
});
$('#renameCamp').die();
$('#renameCamp').live('click', function(){
	var campName = $(this).attr('title');
	$('#renameCamp').hide();
	$('#campaign-name').html('<form name="campaigns" class="jquery_form" id="<?=$cID?>"><ul><li><input type="text" name="name" style="width:430px;" value="'+campName+'" /></li></ul></form><br /><br />');
	return false;
});
<? if(!$_SESSION['theme']){ ?>
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition': 'inside',
		'transitionIn': 'none',
		'transitionOut': 'none'
	});
});
<? } else if($_SESSION['thm_camp-pnl']){ ?>
$(function(){ $('.custom-block a').addClass('dflt-link'); });
<? } ?>
</script>
<a href="#" id="renameCamp" class="dflt-link greyButton" title="<?=$cName?>" rel="<?=$cID?>" style="float:right;margin-top:15px;">Rename Campaign</a>
<div id="campaign-name"><h1 style="float:left;padding-right:10px;font-size:2em;"><?=$cName?> - Dashboard </h1>
 <? if(!$_SESSION['theme']){ ?>
	<div class="watch-video"><ul><li><a href="#campVideo" class="trainingvideo" >Watch Video</a></li></ul></div>
	<? } ?>
    
	<? if(!$_SESSION['theme']){ ?>
	<div style="display:none;">
        <div id="campVideo" style="width:1000px;height:590px;overflow:auto;">
            <!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
            <a href="http://6qube.mobi/videos/training/dashboard.flv"  style="display:block;width:1000px;height:590px" id="demoplayer"> </a>
            <!-- this will install flowplayer inside previous A- tag. -->
            <script>
                flowplayer("demoplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
            </script>
        </div>
    </div>
	<? } ?>
</div>
 <div class="clear"></div>  
View snapshot for:
		<?=$connector->displaySnapshotSelector($cID, $_SESSION['user_id'], $graphID)?>
		<p style="margin-top:-15px;"><a href="#" id="setDefaultGraph" class="dflt-link">Set as default</a></p>
		<div id="message_cnt"></div>
<? 
	//require_once('inc/analytics.class.php');
	//$anal = new Analytics();
	//$anal_id = 1;//$anal->ANAL_getAnalyticsId('directory', 1) 
?>	
		<div id="trafficGraph" style="padding:0;clear:both;">
			<div id="widgetIframe" style="margin:0;"><iframe width="100%" height="240" src="http://analytics.6qube.com/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&filter_limit=10&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=<?=$graphID?>&period=week&date=today&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
		</div>
        <br />
        <? if($result['default_snapshot_id'] && !$_SESSION['reports_user']){ ?>
        <div id="dash"><div class="dash-container"><div class="container">
	<div class="item">
		<div class="icons">
			<a href="<?=$snapshotItemType?>.php?form=<?=$snapshopForm?>&id=<?=$snapshotItemID?>" class="edit"><span>Edit</span></a>
		</div>
        <img src="<?=$connector->ANAL_getGraphImg($snapshotItemType,$snapshotItemID,$result['default_snapshot_id'],$_SESSION['user_id'])?>" class="graph trigger" />
		<a href="<?=$snapshotItemType?>.php?form=<?=$snapshopForm?>&id=<?=$snapshotItemID?>"><?=$snapshotItemName?></a>
	</div>
	</div></div></div>
	<br />
    <? } ?>
    <br />
        <? include("dashboard_prospects.php"); ?>
    
		
	</div><br />
	<!-- End Directory -->
	
	
	<? if($_SESSION['reseller']){ ?>
	<br /><br />
	<!-- <a href="reseller-transfer-items.php?mode=campaign&cid=<?=$cID?>" class="dflt-link">Transfer this entire campaign to another user</a> -->
	<? } ?>
<br style="clear:both" />
<br />
