<?php
session_start();
         require_once( dirname(__FILE__) . '/../6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
$page = $_GET['p'] ? $_GET['p'] : $reseller->getFirstCustomPage($_SESSION['parent_id']);
$page = is_numeric($page) ? $page : '';
if($page){
	$query = "SELECT `type`, `name`, `data` FROM `resellers_backoffice` WHERE `id` = '".$page."'";
	$a = $reseller->queryFetch($query);
	
	if($a['type']=='page'){
		$pageData = $a['data'];
		$dispName = $a['name'];
	}
	else if($a['type']=='nav'){
		//if the id is for a nav, get the content for the first page under that nav
		$query = "SELECT `name`, `data` FROM `resellers_backoffice` WHERE `parent` = '".$page."' 
				AND `admin_user` = '".$_SESSION['parent_id']."' 
				AND `trashed` = '0000-00-00' 
				ORDER BY `list_order` ASC, id ASC";
		$b = $reseller->queryFetch($query);
		$pageData = $b['data'];
		$dispName = $b['name'];
	}
	
	//replace link tags
	$links = array();
	preg_match_all("/(\[\[(.*?)\]\])/", $pageData, $links, PREG_SET_ORDER);
	if($links){
		foreach($links as $value){
			$linkArray = explode('||', $value[2]);
			$pageLinkTitle = $linkArray[0];
			$linkText = $linkArray[1] ? $linkArray[1] : '';
			$link = $reseller->getCustomPageLink($pageLinkTitle, $linkText, $_SESSION['parent_id']);
			$pageData = str_replace($value[0], $link, $pageData);
			$pageLinkTitle = $link = NULL;
		}
	}
	//replace user info tags
	$tags = array();
	preg_match_all("/(\(\((.*?)\)\))/", $pageData, $tags, PREG_SET_ORDER);
	if($tags){
		$query = "SELECT `firstname`, `lastname`, `phone`, `company`, `address`, `address2`, `city`, `state`, `zip`, `profile_photo` 
				FROM `user_info` WHERE `user_id` = '".$_SESSION['user_id']."' LIMIT 1";
		$userInfo = $reseller->queryFetch($query);
		foreach($tags as $value){
			$tag = strtolower($value[2]);
			if($tag=='name' || $tag=='firstname' || $tag=='lastname' || $tag=='phone' || $tag=='company' || $tag == 'fulladdress' ||
			   $tag=='address' || $tag=='address2' || $tag=='city' || $tag=='state' || $tag=='zip' || $tag=='id' || $tag=='email' || $tag=='photo' || $tag=='photodisplayleft' || $tag=='photodisplayright'){
				if($tag=='name') $replace = $userInfo['firstname'].' '.$userInfo['lastname'];
				else if($tag=='fulladdress'){
					$replace = $userInfo['address'].'<br />';
					if($userInfo['address2']) $replace .= $userInfo['address2'].'<br />';
					$replace .= $userInfo['city'].', '.$userInfo['state'].'<br />'.$userInfo['zip'];
				}
				else if($tag=='id') $replace = $_SESSION['user_id'];
				else if($tag=='email') $replace = $_SESSION['user'];
				else if($tag=='phone') $replace = $reseller->formatPhone($userInfo['phone']);
				else if($tag=='photo') $replace = 'http://'.$_SESSION['main_site'].'/users/'.UserModel::GetUserSubDirectory($_SESSION['user_id']).'/user_info/'.$userInfo['profile_photo'];
				else if($tag=='photodisplayleft') $replace = '<img src="http://'.$_SESSION['main_site'].'/users/'.UserModel::GetUserSubDirectory($_SESSION['user_id']).'/user_info/'.$userInfo['profile_photo'].'" width="125px" style="padding:15px;float:left;" />';
				else if($tag=='photodisplayright') $replace = '<img src="http://'.$_SESSION['main_site'].'/users/'.UserModel::GetUserSubDirectory($_SESSION['user_id']).'/user_info/'.$userInfo['profile_photo'].'" width="125px" style="padding:15px; float:right; " />';
				else $replace = $userInfo[$tag];
				$pageData = str_replace($value[0], $replace, $pageData);
				$tag = $replace = NULL;
			}
		}
	}
}
?>
<script type="text/javascript">
$(function(){ $('#custom_content a').addClass('dflt-link'); });
</script>
<h1><?=$dispName?></h1>
<div id="custom_content" style="width:100%;overflow:hidden;">
<?=$pageData?>
</div>
<br style="clear:both;" />