<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

require_once QUBEADMIN . 'inc/local.class.php';
class UserPermissions extends Local {

	function getManagedUsers($parent_id = '', $uid = ''){
	  $return = array();
	  if($parent_id && $uid){
	    $query = "SELECT users.id AS id, user_info.firstname AS firstname, user_info.lastname AS lastname FROM users, user_info WHERE users.manage_user_id != '' AND (users.manage_user_id LIKE '".$uid."' OR users.manage_user_id LIKE '".$uid."::%' OR users.manage_user_id LIKE '%::".$uid."::%' OR users.manage_user_id LIKE '%::".$uid."') AND users.parent_id = '".$parent_id."' AND user_info.user_id = users.id";
	     if($users = $this->query($query)){
	       while($user = $this->fetchArray($users)){
	         $return[] = array('id'=>$user['id'], 'firstname'=>$user['firstname'], 'lastname'=>$user['lastname']);
	      }
	    }
	    else echo mysqli_error();
	  }
	  if($return) return $return;
	  else return false;
	}

	/******************************************************
	// Display dropdown of users (For switching between)
	/******************************************************/
	function displayManageUsersDrop($parent_id, $uid, $curr_id, $username, $curr_username, $limit = ''){
		
		if($users = $this->getManagedUsers($parent_id, $uid)){
			echo '<select name="admin-user-select" class="userdropdown" id="usermanagerdropdown">
					<option value="'.$uid.'">'.$uid.' - '.$username.'</option>
					 <option value="">---</option>';
			$numUsers = 0;
			$userIds = array();
			foreach($users as $key=>$user){
			  echo '<option value="'.$user['id'].'"';
			  if($user['id']==$curr_id) echo ' selected="selected"';
			  echo '>'.$user['id'].' - '.$user['firstname'].' '.$user['lastname'].'</option>';
			  $userIds[] = $user['id'];
			  $numUsers++;
			}
			if(!in_array($curr_id, $userIds) && $curr_id!=$uid){
				echo '<option value="">---</option>';
				echo '<option value="'.$curr_id.'" selected="selected">';
				echo $curr_id.' - '.$curr_username;
				echo '</option>';
			}
			echo '</select>';
		}
		else {
			echo '<select name="admin-user-select" class="userdropdown" id="usermanagerdropdown">
				 <option value="'.$uid.'">'.$uid.' - '.$username.'</option>
				 <option value="">---</option>
				 </select>';
		}
	}
	
	function outputManagedUsers($parent_id, $uid, $curr_id, $username, $curr_username){
		
		$prefix = '<li>';
		$suffix = '</li>';
		
		if($users = $this->getManagedUsers($parent_id, $uid))
		{
			foreach($users as $key=>$user)
			{
				echo $prefix.'<a class="manage-user';
				if($user['id']==$curr_id) { echo ' userSelected'; }
				echo '" rel="'.$user['id'].'" href="#">'.$user['firstname'].' '.$user['lastname'].'</a>'.$suffix;
				
				//echo $prefix.$user['firstname'].' '.$user['lastname'].$suffix;
			}
		}
		else
		{
			echo $prefix.'No Users to Manage'.$suffix;
		}
	
	}

	
}
?>