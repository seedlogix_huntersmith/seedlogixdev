<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class SixQube extends Local {
	
	/******************************************************
	// Get Elements blog posts
	/******************************************************/
	function getElementsBlogPost($numberOfPost){
		$sql = "SELECT * FROM `blog_post` 
			   WHERE `blog_id` = 42 
			   ORDER BY `blog_post`.`created` DESC 
			   LIMIT ".$numberOfPost;
		$results = $this->query($sql);
		
		if($this->numRows($results)){
			$post = $results;
		}
		else{
			$post = false;
		}
		
		return $post;
	}
	
	/******************************************************
	// Get 6Qube blog posts
	/******************************************************/
	function getQubeBlogPost($numberOfPost, $blogid){
		$sql = "SELECT * FROM `blog_post` 
			   WHERE `blog_id` = ".$blogid." 
			   ORDER BY `blog_post`.`created` DESC 
			   LIMIT ".$numberOfPost;
		$results = $this->query($sql);
		
		if($this->numRows($results)){
			$post = $results;
		}
		else{
			$post = false;
		}
		
		return $post;
	}
	
	
	/******************************************************
	// Display pagination nav
	/******************************************************/
	function displayNav($num_rows, $limit, $offset, $section){
		$maxPage = ceil($num_rows/$limit);
		if($offset > 0) { $currPage = ($offset/$limit)+1; } else { $currPage = 1; }
		
		if(($currPage < 3) || ($maxPage <= 5)){ // if there are less than 5 pages, or the current page # is less than 3
		
			if($maxPage<=5){ $total = $maxPage; } else { $total = 5; } // calculate how many pages to display
			
			for($page = 1; $page <= $total; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= "<li><a href=\"#\" class=\"pagination_links_active\">".$page."</a></li>";
			   }
			   else
			   {
				  $nav .= "<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".$page_offset.", '".$section."')\" class=\"pagination_links\">".$page."</a></li>";
			   }
			}
		}
		
		else if(($currPage>=3) && ($currPage<=($maxPage-3))){ // example display: 1 2 [3 4 [5] 6 7] 8
			$page_offset = ($currPage*$limit)-$limit;
			$nav =  "<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".($page_offset-($limit*2)).", '".$section."')\" class=\"pagination_links\">".($currPage-2)."</a></li>
					<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".($page_offset-$limit).", '".$section."')\" class=\"pagination_links\">".($currPage-1)."</a></li>
					<li><a href=\"#\" class=\"pagination_links_active\">".$currPage."</a></li>
					<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".($page_offset+$limit).", '".$section."')\" class=\"pagination_links\">".($currPage+1)."</a></li>
					<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".($page_offset+($limit*2)).", '".$section."')\" class=\"pagination_links\">".($currPage+2)."</a></li>";
		}
		
		else { // example display: 1 2 3 [4 5 6 [7] 8]
			for($page = ($maxPage-4); $page <= $maxPage; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= "<li><a href=\"#\" class=\"pagination_links_active\">$page</a></li>";
			   }
			   else
			   {
				  $nav .= "<li><a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".$page_offset.", '".$section."')\" class=\"pagination_links\">$page</a></li>";
			   } 
			}
		}
		
		// calculate prev/next page offsets and set button links
		$prevPageOffset = (($currPage-1)*$limit)-$limit;
		$nextPageOffset = (($currPage+1)*$limit)-$limit;
		$prevLink = "<a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".$prevPageOffset.", '".$section."')\">";
		$nextLink = "<a href=\"javascript:return false;\" onclick=\"miniNav(".$limit.", ".$nextPageOffset.", '".$section."')\">";
		if($currPage==1){
			$prevLink = "<a href=\"javascript:return false;\">";
		}
		if($currPage==$maxPage){
			$nextLink = "<a href=\"javascript:return false;\">";
		}
		
		if($maxPage!=1){ // don't display nav if there's only one page
			$html = "<div id=\"pagination\"><ul>
					<li>".$prevLink."<img src=\"http://blogs.6qube.com/images/blog-previous-icon.jpg\" alt=\"\" /></a></li>
					".$nav."
					<li>".$nextLink."<img src=\"http://blogs.6qube.com/images/blog-next-icon.jpg\" alt=\"\" /></a></li>
					</ul></div>
					<br />";
		}
		
		echo $html;
	}
	
	/******************************************************
	// Display pagination nav for search/browse
	/******************************************************/
	function displayNav2($num_rows, $limit, $offset, $section, $keyword, $table, $location, $parent_site = ''){
		if(!$parent_site) $parent_site = '6qube.com';
		
		//$currUrl = currPageURL();
		$maxPage = ceil($num_rows/$limit);
		if($section=='search'){
			$link = "?business=".$keyword."&location=".$location;
		} else { //browse
			$link = "?section=".$table;
			if($keyword){ $link .= "&category=".$keyword; }
			if($location[1]){ 
				$link .= "&state=".$location[1];
				if($location[0]!='all' && $location[0]){ $link .= "&city=".$location[0]; }
				else{ $link .= "&city=all"; }
			}
		}
		if($offset > 0) { $currPage = ($offset/$limit)+1; } else { $currPage = 1; }
		
		if(($currPage < 3) || ($maxPage <= 5)){ // if there are less than 5 pages, or the current page # is less than 3
		
			if($maxPage<=5){ $total = $maxPage; } else { $total = 5; } // calculate how many pages to display
			
			for($page = 1; $page <= $total; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= '<li><a href="#" class="pagination_links_active">'.$page.'</a></li>';
			   }
			   else
			   {
				  
				  $nav .= '<li><a href="'.$link.'&offset='.$page_offset.'" class="pagination_links">'.$page.'</a></li>';
			   } 
			}
		}			
		
		else if(($currPage>=3) && ($currPage<=($maxPage-3))){ // example display: 1 2 [3 4 [5] 6 7] 8
			$page_offset = ($currPage*$limit)-$limit;
			$nav = '<li><a href="'.$link.'&offset='.($page_offset-($limit*2)).'" class="pagination_links">'.($currPage-2).'</a></li>';
			$nav .= '<li><a href="'.$link.'&offset='.($page_offset-$limit).'" class="pagination_links">'.($currPage-1).'</a></li>';
			$nav .= '<li><a href="#" class="pagination_links_active">'.$currPage.'</a></li>';
			$nav .= '<li><a href="'.$link.'&offset='.($page_offset+$limit).'" class="pagination_links">'.($currPage+1).'</a></li>';
			$nav .= '<li><a href="'.$link.'&offset='.($page_offset+($limit*2)).'" class="pagination_links">'.($currPage+2).'</a></li>';
		}
		
		else { // example display: 1 2 3 [4 5 6 [7] 8]
			for($page = ($maxPage-4); $page <= $maxPage; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= '<li><a href="#" class="pagination_links_active">'.$page.'</a></li>';
			   }
			   else
			   {
				  $nav .= '<li><a href="'.$link.'&offset='.$page_offset.'" class="pagination_links">'.$page.'</a></li>';
			   } 
			}
		}
		
		// calculate prev/next page offsets and set button links
		$prevPageOffset = (($currPage-1)*$limit)-$limit;
		$nextPageOffset = (($currPage+1)*$limit)-$limit;
		$prevLink = $link."&offset=".$prevPageOffset;
		$nextLink = $link."&offset=".$nextPageOffset;
		if($currPage==1){
			$prevLink = "#";
		}
		if($currPage==$maxPage){
			$nextLink = "#";
		}
		
		if($maxPage!=1){ // don't display nav if there's only one page
			$html = '<div id="pagination"><ul>
					<li><a href="'.$prevLink.'"><img src="http://'.$parent_site.'/images/nav-previous-icon.jpg" alt="" /></a></li>
					'.$nav.'
					<li><a href="'.$nextLink.'"><img src="http://'.$parent_site.'/images/nav-next-icon.jpg" alt="" /></a></li>
					</ul></div>
					<br />';
		}
		
		echo $html;
	}
	
	/******************************************************
	// Display pagination nav for local.6qube.com
	/******************************************************/
	function displayNav3($network, $num_rows, $limit, $offset, $city = '', $state = '', $category = '', $parent_site = ''){
		///////////////////////////////////////////////////////////////////////
		//temporary code for while transitioning networks to reseller version:/
		if(strpos($network, '.')===false) $network .= '.6qube.com';		    //
		///////////////////////////////////////////////////////////////////////
		if(!$parent_site) $parent_site = '6qube.com';
		
		$maxPage = ceil($num_rows/$limit);
		if($offset > 0) { $currPage = ($offset/$limit)+1; } else { $currPage = 1; }
		
		$baseLink = "http://".$network."/";
		if($city){
			if($major = $this->majorCity($city)){
				if($major == $state)
					$baseLink .= $this->convertKeywordUrl($city, true)."/";
				else
					$baseLink .= $this->convertKeywordUrl($city, true)."-".strtolower($state)."/";
			}
			else
				$baseLink .= $this->convertKeywordUrl($city, true)."-".strtolower($state)."/";
		}
		if($category) $baseLink .= $this->convertKeywordUrl($category)."/";
		
		if(($currPage < 10) || ($maxPage <= 10)){ // if there are less than 5 pages, or the current page # is less than 3
		
			if($maxPage<=10){ $total = $maxPage; } else { $total = 10; } // calculate how many pages to display
			
			for($page = 1; $page <= $total; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= '<li><a href="#" class="pagination_links_active">'.$page.'</a></li>';
			   }
			   else
			   {
				  $nav .= '<li><a href="'.$baseLink.$page.'/" class="pagination_links">'.$page.'</a></li>';
			   }
			}
		}
		
		else if(($currPage>=6) && ($currPage<=($maxPage-6))){ // example display: 1 2 [3 4 [5] 6 7] 8
			$page_offset = ($currPage*$limit)-$limit;
			$nav =  '<li><a href="'.$baseLink.($currPage-5).'/" class="pagination_links">'.($currPage-5).'</a></li>';
			$nav .=  '<li><a href="'.$baseLink.($currPage-4).'/" class="pagination_links">'.($currPage-4).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage-3).'/" class="pagination_links">'.($currPage-3).'</a></li>';
			$nav .=  '<li><a href="'.$baseLink.($currPage-2).'/" class="pagination_links">'.($currPage-2).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage-1).'/" class="pagination_links">'.($currPage-1).'</a></li>';
			$nav .= '<li><a href="#" class="pagination_links_active">'.$currPage.'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage+1).'/" class="pagination_links">'.($currPage+1).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage+2).'/" class="pagination_links">'.($currPage+2).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage+3).'/" class="pagination_links">'.($currPage+3).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage+4).'/" class="pagination_links">'.($currPage+4).'</a></li>';
			$nav .= '<li><a href="'.$baseLink.($currPage+5).'/" class="pagination_links">'.($currPage+5).'</a></li>';
		}
		
		else { // example display: 1 2 3 [4 5 6 [7] 8]
			for($page = ($maxPage-4); $page <= $maxPage; $page++)
			{
				$page_offset = ($page - 1) * $limit;
			   if ($page_offset == $offset) //current page
			   {
				  $currPage = $page;
				  $nav .= '<li><a href="#" class="pagination_links_active">'.$page.'</a></li>';
			   }
			   else
			   {
				  $nav .= '<li><a href="'.$baseLink.$page.'/" class="pagination_links">'.$page.'</a></li>';
			   } 
			}
		}
		
		// set prev/next button links
		$prevLink = '<a href="'.$baseLink.($currPage-1).'/">';
		$nextLink = '<a href="'.$baseLink.($currPage+1).'/">';
		if($currPage==1){
			$prevLink = '<a href="#">';
		}
		if($currPage==$maxPage){
			$nextLink = '<a href="#">';
		}
		
		if($maxPage!=1){ // don't display nav if there's only one page
			$html = '<div id="pagination"><ul>
				    <li>'.$prevLink.'<img src="http://'.$parent_site.'/images/nav-previous-icon.jpg" alt="" /></a></li>
				    '.$nav.'
				    <li>'.$nextLink.'<img src="http://'.$parent_site.'/images/nav-next-icon.jpg" alt="" /></a></li>
				    </ul></div>
				    <br />';
		}		
		
		echo $html;
	}
	
	/**********************************************************************/
	// function: Billing API
	// calls WHMCS billing API with passed array and returns results
	//
	// Accepted Input: postfields array (without username and password)
	/**********************************************************************/
	function billAPI($postfields, $raw = ''){
            return self::billAPIStatic($postfields, $raw);
	}
        
        static function billAPIStatic($postfields, $raw =   '')
        {
		$url = "http://www.6qube.com/billing/includes/api.php";
		$username = "apiadmin";
		$password = "3376API";
		
		$postfields["username"] = $username;
		$postfields["password"] = md5($password);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		$data = curl_exec($ch);
		curl_close($ch);
		
		if($raw) return $data;
		else {
			$data = explode(";",$data);
			foreach ($data AS $temp) {
				$temp = explode("=",$temp);
				$results[$temp[0]] = $temp[1];
			}
			
			return $results;
		}            
        }
	
	/**********************************************************************/
	// function: Display Hub (NEW)
	// outputs a set of hub results in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayHub($row, $layout, $parent_site = ''){
		if(!$parent_site) $parent_site = '6qube.com';
		$hubUrl = $row['domain'];
		//add http:// to url if not present
		$url_array = explode(".", $hubUrl);
		if(strpos($url_array[0], "http")===false){
			$url_array[0] = "http://".$url_array[0];
			$hubUrl = implode(".", $url_array);
		}
		if($layout==1 || $layout==2){
			require_once QUBEADMIN . 'inc/hub.class.php';
			
			if($row['logo'] == "") {
				$icon = 'http://'.$parent_site.'/img/no_image.jpg';//6qube_cube.png';
			}
			else {
				$icon = 'http://'.$parent_site.'/users/'.$row['user_id'].'/hub/'.$row['logo'];
			}
			
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$html = '<!--Start Blog Div-->';
			if($layout==2){ $html .= '<span class="hubs_blog_date"></span>'; }
			if($layout==1){ $html .= '<span class="blog_date">'.$createdMo.'<br />'.$createdDay.'</span>'; }
			$html .=	'<div class="blog_div">
							<div class="blog_heading">
								<h3><a title="Visit Website" href="'.$hubUrl.'" rel="nofollow" >'.$row['company_name'].'</a></h3>';
			$html .=		'</div><!--End Blog Heading-->';
			if($layout==2){ $html .= '<p class="blog_comments">'.Hub::getHubCompletion($row).'%</p>'; }
			if($layout==1){ $html .= '<p class="blog_comments">'.Hub::getHubCompletion($row).'%</p>'; }
			$html .=		'<br clear="all" />
							<div class="blog_title_border">
								<a href="'.$hubUrl.'"><img src="'.$icon.' " class="bloglistimg" alt="" width="175px" /></a>
								<p>
									'.$this->shortenSummary($row['description'], 250).'  <a href="'.$hubUrl.'" title="Learn More" >Learn More</a>
								</p>
								<p class="stamp"><a href="'.$hubUrl.'">Submitted by '.$row['company_name'].'</a> on '.$created.'</p>
								<div class="view-listing">
									<a title="Visit Website" href="'.$hubUrl.'" ><img src="http://'.$parent_site.'/images/view-hub.png" alt="" /></a>
								</div>
							</div><!--End Blog Title Border-->
						</div>
					<!--End Blog Div-->';
			
		}
		else if($layout==3){
			if(!$row['logo']){
				$html = '<div class="hubsnap">
				<a href="'.$hubUrl.'" rel="nofollow" title="'.$row['company_name'].' | '.$row['keyword1'].'" >'.$row['company_name'].' | '.$row['city'].', '.$row['state'].'</a>			
				</div>';				
			} else {
				$html = '
				<div class="hubsnap"><a href="'.$hubUrl.'" rel="nofollow" title="'.$row['company_name'].' | '.$row['keyword1'].'" ><img width="235px" src="http://'.$parent_site.'/users/'.$row['user_id'].'/hub/'.$row['logo'].'" /></a>
				<a href="'.$hubUrl.'" rel="nofollow" title="'.$row['company_name'].' | '.$row['keyword1'].'" >'.$row['company_name'].' | '.$row['city'].', '.$row['state'].'</a>			
				</div>';
			}
		}
		else {
			$html = '<li><a href="'.$hubUrl.'" rel="nofollow" title="View Website"><div class="diricon bloglistimg" style="background-image:url(\'http://'.$parent_site.'/users/'.$row['user_id'].'/hub/'.$row['logo'].'\')"></div></a></li>';
		}
		return $html;
	}
	
	/**********************************************************************/
	// function: Display Directory (NEW)
	// returns a single Directory listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/
	function displayDirectory($row, $layout, $domain = '', $parent_site = '', $parent_company = ''){
		if(!$domain) $domain = 'local.6qube.com';
		if(!$parent_site) $parent_site = '6qube.com';
		if(!$parent_company) $parent_company = '6Qube';
		$listingUrl = 'http://'.$domain.'/'.$this->convertKeywordUrl($row['keyword_one']).
					$this->convertKeywordUrl($row['company_name']).$row['id'].'/';
		
		if($layout==1 || $layout==2){
			require_once QUBEADMIN . 'inc/directory.class.php';	
			
			if($row['photo'] == "") {
				$icon = 'http://'.$parent_site.'/img/no_image.jpg';
			}
			else {
				$icon = 'http://'.$parent_site.'/users/'.$row['user_id'].'/directory/'.$row['photo'];
			}
				
			$lastUpdated = date("F jS, Y", strtotime($row['last_edit']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$html = '<!--Start Blog Div-->';
			if($layout==2){ $html .= '<span class="local_blog_date"></span>'; }
			if($layout==1){ $html .= '<span class="blog_date">'.$createdMo.'<br />'.$createdDay.'</span>'; }
			$html .=	'<div class="blog_div">
							<div class="blog_heading">
								<h3><a href="'.$listingUrl.'" title="'.$row['company_name'].'">'.$row['company_name'].'</a></h3>';
			$html .= '
							</div><!--End Blog Heading-->';
			if($layout==2){ $html .= '<p class="blog_comments">'.Dir::getListingCompletion($row).'%</p>'; }
			if($layout==1){ $html .= '<p class="blog_comments">'.Dir::getListingCompletion($row).'%</p>'; }
			$html .=		'<br clear="all" />
							<div class="blog_title_border">
								<a href="'.$listingUrl.'" title="Local Yellow Pages Directory by '.$parent_company.' Presents '.$row['company_name'].'" ><img src="'.$icon.' " class="bloglistimg" alt="" width="175px" /></a>
								<p>
									<div class="displayPhone">'.$this->formatPhone($row['phone']).'</div>
									<p class="date"><i>Last Updated: <br/>'.$lastUpdated.'</i></p>
									'.$this->shortenSummary($row['display_info'], 250).'  <a href="'.$listingUrl.'" title="Local Yellow Pages Directory by '.$parent_company.' Presents '.$row['company_name'].'" >Visit '.$row['company_name'].' Local Profile</a></p>
									<br class="clear" />
									<div class="add-container">';
									if($row['street']&&$row['city']&&$row['state']){
									$html .= '
									<p class="address">'.$row['street'].'</p>
									<p class="city">'.$row['city'].', '.$row['state'].'</p>
									';
									}
			$html .=				'</div>
									<div class="view-listing"><a href="'.$listingUrl.'" title="Local Yellow Pages Directory by '.$parent_company.' Presents '.$row['company_name'].'" ><img src="http://'.$parent_site.'/local/images/view-listing.png" alt="" /></a>
								</div>
							</div><!--End Blog Title Border-->
						</div>
					<!--End Blog Div-->';
					
		} else {
			$html = '<li><a href="'.$listingUrl.'" title="">'.$row['company_name'].'</a></li>';
		}
				
		return $html;
	}
	
	/**********************************************************************/
	// function: Display Press (NEW)
	// returns a single Press Release listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayPress($row, $layout = 0, $domain = '', $parent_site = '', $parent_company = ''){
		if(!$domain) $domain = 'press.6qube.com';
		if(!$parent_site) $parent_site = '6qube.com';
		if(!$parent_company) $parent_company = '6Qube';
		
		$a = explode("->", $row['category']);
		$category = $a[1];
		//$pressUrl = 'http://press.6qube.com/'.Local::convertPressUrl($row['headline'], $row['id']);
		$pressUrl = 'http://'.$domain.'/'.$this->convertKeywordUrl($category).
				   $this->convertKeywordUrl($row['headline']).$row['id'].'/';
		
		if($layout==1 || $layout==2){
			if($row['thumb_id'] == "") {
				$icon = 'http://'.$parent_site.'/img/no_image.jpg';//6qube_cube.png';
			}
			else {
				$icon = 'http://6qube.com/users/'.$row['user_id'].'/press_release/'.$row['thumb_id'];
			}
				
			$lastUpdated = date("F jS, Y", strtotime($row['timestamp']));
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$keywords = explode(",", $row['keywords']);
			
			$html = '<!--Start Blog Div-->';
			if($layout==2){ $html .= '<span class="press_blog_date"></span>'; }
			if($layout==1){ $html .= '<span class="blog_date">'.$createdMo.'<br />'.$createdDay.'</span>'; }
			$html .=	'<div class="blog_div">
							<div class="blog_heading">
								<h3><a href="'.$pressUrl.'" title="'.$row['headline'].'">'.$row['headline'].'</a></h3>';
			$html .=		'</div><!--End Blog Heading-->
							<br clear="all" />
							<div class="blog_title_border">
								<a title="'.$row['headline'].'" href="'.$pressUrl.'"><img src="'.$icon.' " class="bloglistimg" alt="" width="175px" /></a>
								<p>
									'.$this->shortenSummary($row['summary'], 250).'  <a href="'.$pressUrl.'" title="'.$row['headline'].'">Visit Author\'s Press Release</a>
								</p>
								<p class="stamp">Submitted by <a href="'.$pressUrl.'" title="'.$row['headline'].'">'.$row['author'].'</a> on '.$created.'</p>
								<div class="view-listing">
									<a href="'.$pressUrl.'" title="Local Press Directory by 6Qube Local | Local Press Releases"><img src="http://press.6qube.com/images/view-press.png" alt="Local Press Directory by 6Qube Local | Local Press Releases" /></a>
								</div>
							</div><!--End Blog Title Border-->
						</div>
					<!--End Blog Div-->';
					
		} else {
			$html = '<li><a href="'.$pressUrl.'" title="">'.$this->shortenTitle($row['headline'], 75).'</a></li>';
		}
		
		return $html;
	}
	
	/**********************************************************************/
	// function: Display Article (NEW)
	// returns a single Hub listing display in new format
	//
	// Accepted Input: A Single mysqli_FETCH_ARRAY Row
	/**********************************************************************/	 
	function displayArticle($row, $layout = 0, $domain = '', $parent_site = ''){
		if(!$domain) $domain = 'articles.6qube.com';
		if(!$parent_site) $parent_site = '6qube.com';
		
		if($layout==1 || $layout==2){
			if($row['thumb_id'] == "") {
				$icon = 'http://'.$parent_site.'/img/no_image.jpg';//6qube_cube.png';
			}
			else {
				$icon = 'http://'.$parent_site.'/users/'.$row['user_id'].'/articles/'.$row['thumb_id'];
			}
			
			$a = explode('->', $row['category']);
			$category = $a[1];
			$articleUrl = 'http://'.$domain.'/'.$this->convertKeywordUrl($category).
						$this->convertKeywordUrl($row['headline']).$row['id'].'/';
			$lastUpdated = date("F jS, Y", strtotime($row['timestamp']));
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$keywords = explode(",", $row['keywords']);
			
			$html = '<!--Start Blog Div-->';
			if($layout==2){ $html .= '<span class="articles_blog_date"></span>'; }
			if($layout==1){ $html .= '<span class="blog_date">'.$createdMo.'<br />'.$createdDay.'</span>'; }
			$html .=	'<div class="blog_div">
							<div class="blog_heading">
								<h3><a title="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" href="'.$articleUrl.'" title="">'.$row['headline'].'</a></h3>';
			$html .=		'</div><!--End Blog Heading-->
							<br clear="all" />
							<div class="blog_title_border">
								<a title="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" href="'.$articleUrl.'"><img src="'.$icon.' " class="bloglistimg" alt="" width="175px" /></a>
								<p>
									'.$this->shortenSummary($row['summary'], 250).'  <a title="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" href="'.$articleUrl.'" title="">Read Article</a>
								</p>
								<p class="stamp">Submitted by <a title="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" href="'.$articleUrl.'">'.$row['author'].'</a> on '.$created.'</p>
								<div class="view-listing">
									<a href="'.$articleUrl.'" title="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" ><img src="http://'.$parent_site.'/images/view-article.png" alt="'.$row['author'].' | '.$keywords[0].' | '.$keywords[1].' | '.$keywords[2].'" /></a>
								</div>
							</div><!--End Blog Title Border-->
						</div>
					<!--End Blog Div-->';
			
		}
		else {
			$a = explode('->', $row['category']);
			$category = $a[1];
			$articleUrl = 'http://'.$domain.'/'.$this->convertKeywordUrl($category).
						$this->convertKeywordUrl($row['headline']).$row['id'].'/';
			$html = '<li><a href="'.$articleUrl.'">'.$this->shortenTitle($row['headline'], 75).'</a></li>';
		}
		
		return $html;
	}
	
	/**********************************************************************/
	// Browse functions
	// 
	/**********************************************************************/
	/******************************************************
	// Build dropdown of sections for Browse
	/******************************************************/
	function browseGetSections($selected){
		$sections['all'] = 'All';
		$sections['directory'] = 'Directories';
		$sections['hub'] = 'HUBs';
		$sections['press_release'] = 'Press Releases';
		$sections['articles'] = 'Articles';
		//$sections['blog_post'] = 'Blogs';
		
		$select = '<select name="section" id="section" class="autosubmit">';
		
		foreach( $sections as $key => $value)
		{
			$select .= '<option value="'.$key.'"';
			if($selected == $key) $select .= 'selected="selected"';
			$select .= '>'.$value.'</option>';
		}
		
		$select .= '</select>';
		
		return $select;
	}
	/******************************************************
	// Build dropdown of categories for Browse
	/******************************************************/
	function browseGetCategories($table, $selected){
		if($table == "all" || !$table) //if user selected "all" use query to load categories from all tables
		{
			$query = "(SELECT distinct category FROM directory WHERE category IS NOT NULL)
					UNION
					(SELECT distinct category FROM hub WHERE category IS NOT NULL)
					UNION
					(SELECT distinct category FROM press_release WHERE category IS NOT NULL)
					UNION
					(SELECT distinct category FROM articles WHERE category IS NOT NULL)
					UNION
					(SELECT distinct category FROM blogs WHERE category IS NOT NULL)
					ORDER BY category ASC";
		}
		else { //if you selected a section then just load categorise from that table (aka section)
			$query = "SELECT category FROM `".$table."` WHERE category IS NOT NULL ORDER BY category ASC";
		}
		
		$result = $this->query($query); //query the db
		$cat = $this->returnCategories($catdefine);//load categories array
		
		$select = '<select name="category" id="category" class="autosubmit"><option value="all">All</option>';
		while($row = $this->fetchArray($result)) //loop through all categories pulled from db
		{
			$present = $row['category'];
			if($past != $present){
				$select .= '<option value="'.$row['category'].'"';
				if($selected == $row['category']) $select .= ' selected="selected"';
				$select .= '>'.$cat[$row['category']].'</option>';
				$past = $row['category'];
			}
		}
		$select .= '</select>';
		return $select;
	}
	/******************************************************
	// Build dropdown of states for Browse
	/******************************************************/
	function browseGetStates($table, $category, $selected){
		$query = "SELECT state FROM `".$table."` WHERE state != '' ";
		if($category!="all")$query .= "AND category = '$category' ";
		$query .="ORDER BY state ASC";
		$result = $this->query($query); //query the db
		
		$select = '<br /><br /><h3>Find Local Businesses</h3><hr /><br /> by choosing your <strong>State</strong>:<select name="state" id="state" class="autosubmit"><option value="all">All</option>';
		while($row = $this->fetchArray($result)) //loop through all categories pulled from db
		{
			$present = $row['state'];
			if($past == '' || $past != $present){
				$select .= '<option value="'.$row['state'].'"';
				if($selected == $row['state']) $select .= 'selected="selected"';
				$select .= '>'.$row['state'].'</option>';
				$past = $row['state'];
			}
		}
		$select .= '</select>';
		return $select;
	}
	/******************************************************
	// Build dropdown of cities for Browse
	/******************************************************/
	function browseGetCities($table, $category, $state, $selected){
		$query = "SELECT city FROM `".$table."` WHERE city != '' ";
		if($state!="all" && $state != "") $query .= "AND state = '$state' ";
		if($category!="all")$query .= "AND category = '$category' ";
		$query .="ORDER BY city ASC";
		$result = $this->query($query); //query the db
		
		$select = '<br /><br />choose your <strong>City</strong>:<select name="city" id="city" class="autosubmit"><option value="all">All</option>';
		while($row = $this->fetchArray($result)) //loop through all categories pulled from db
		{
			$present = $row['city'];
			if($past == '' || $past != $present){
				$select .= '<option value="'.$row['city'].'"';
				if($selected == $row['city']) $select .= 'selected="selected"';
				$select .= '>'.$row['city'].'</option>';
				$past = $row['city'];
			}
		}
		$select .= '</select>';
		return $select;
	}
	/**********************************************************************/
	// function: Display Browse Results (NEW)
	// Outputs a Set of listings in new format
	//
	// Accepted Input: table (section), category, city, state
	/**********************************************************************/	 
	function displayBrowse($table, $category, $state, $city, $offset, $limit){
		//load some classes to use their functions
		require_once QUBEADMIN . 'inc/blogs.class.php';
		
		if($table == "all" || !$table){ //if user has entered something in both search boxes
			$fullLoc = $city.', '.$state;
			$location[0] = $city; $location[1] = $state;
			$results = $this->searchFunction($category, $fullLoc, 'browse', false); //get array of results
			$resultsArray = $results['data'];
			$resultsInfo = $results['info'];
			$numResults = $resultsInfo['numResults'];
		}
		else {
			$query = "SELECT * FROM `".$table."` WHERE category IS NOT NULL ";
			if($table == 'directory'){
				$query .= 
				" AND (company_name != '' AND phone != '' AND street != '' AND state != '' AND display_info != '') ";
			} else if($table == 'hub'){
				$query .= 
				" AND (company_name != '' AND phone != '' AND street != '' AND state != '' AND overview != '')";
				$query .= " AND user_id != 105 ";
			} else { //press release or article
				$query .= " AND (headline != '' AND summary != '' AND body != '') ";
				$query .= " AND user_id != 105";
			}
			if($category!='all' && $category){ $query .= " AND category = '".$category."' "; }
			if($state!='all' && $state){ $query .= " AND state = '".$state."' "; }
			if($city!='all' && $city){ $query .= " AND city = '".$city."' "; }
			$query .= " AND `".$table."`.`trashed` = '0000-00-00' ";
			$query .= " ORDER BY created DESC";
			
			if($result = $this->query($query)){
				$numResults = $this->numRows($result);
				while($row = $this->fetchArray($result)){
					$resultsArray[] = $row;
				}
			}
		}
		
		if(!$numResults){ 
			echo '<div class="blog_div"><div class="blog_heading"><h3>Sorry, no results were found.</h3></div></div>';
		}
		else {	
			for($i = 0; $i < $numResults; $i++){
				//set up pagination
				$startRange = $offset; //$limit*$offset;
				$endRange = $offset+($limit-1); //$startRange+($limit-1);
				if($i >= $startRange && $i <= $endRange){ //only display results if they should be on the current page
					if($resultsArray[$i]['type'] == 1){
						echo $this->displayDirectory($resultsArray[$i], 2);
					}
					else if($resultsArray[$i]['type'] == 2){
						echo $this->displayHub($resultsArray[$i], 2);
					}
					else if($resultsArray[$i]['type'] == 3){
						echo $this->displayPress($resultsArray[$i], 2);
					}
					else if($resultsArray[$i]['type'] == 4){
						echo $this->displayArticle($resultsArray[$i], 2);
					}
					else if($resultsArray[$i]['type'] == 5){
		$Blog = new Blog();
						$blogInfo = $this->fetchArray(Blog::getBlogs(NULL, NULL, $resultsArray[$i]['blog_id']));
						echo $Blog->createPostHTML($resultsArray[$i], 2, $blogInfo);
					}
					else { echo "error"; }
				}
			}//end for loop
			echo "<br style='clear:both;' />";
			$this->displayNav2($numResults, $limit, $offset, 'browse', $category, $table, $location);
			$totalPages = ceil($numResults/$limit);
			$offset==0 ? $currentPage = 1 : $currentPage = ceil($offset/$limit)+1;
			echo '<br /><div class="blog_div" style="float:right;">Displaying page <strong>'.$currentPage.'</strong> of <strong>'.$totalPages.'</strong></div>';
		}
	}
	
	/**********************************************************************/
	// function: Display Search Results (NEW)
	// Outputs a Set of Search Results in new format
	//
	// Accepted Input: Category/keyword, location, limit, offset
	/**********************************************************************/	 
	function displaySearch($keyword, $location, $limit, $offset, $domain = '', $parent = '', $parent_company = ''){
		if(!$domain) $domain = 'local.6qube.com';
		if($parent){
			//get parent site (for imgs)
			$query = "SELECT main_site FROM resellers WHERE admin_user = ".$parent;
			$a = $this->queryFetch($query);
			$parent_site = $a['main_site'];
		}
		
		//load some classes to use their functions
		require_once QUBEADMIN . 'inc/blogs.class.php';
		
		if($keyword && $location){ //if user has entered something in both search boxes			
			$results = $this->searchFunction($keyword, $location, 'search', false); //get array of results
			$resultsArray = $results['data'];
			$resultsInfo = $results['info'];
			$numResults = $resultsInfo['numResults'];
			if($numResults<4 || $resultsInfo['numTypes']<3){
				$results = $this->searchFunction($keyword, $location, 'search', true); //get array of results with looser matching
				$resultsArray = $results['data'];
				$resultsInfo = $results['info'];
				$numResults = $resultsInfo['numResults'];
			}
			if($numResults==0){ 
				echo 
				'<div class="blog_div"><div class="blog_heading">
				<h3>Sorry, no results were found for your search.</h3>
				</div></div>';
			}
			else {	
				for($i = 0; $i < $numResults; $i++){
					//set up pagination
					$startRange = $offset; //$limit*$offset;
					$endRange = $offset+($limit-1); //$startRange+($limit-1);
					if($i >= $startRange && $i <= $endRange){ //only display results if they should be on the current page
						if($resultsArray[$i]['type'] == 1){
							echo $this->displayDirectory($resultsArray[$i], 2, $domain, $parent_site, $parent_company);
						}
						else if($resultsArray[$i]['type'] == 2){
							echo $this->displayHub($resultsArray[$i], 2, $parent_site);
						}
						else if($resultsArray[$i]['type'] == 3){
							echo $this->displayPress($resultsArray[$i], 2, $domain, $parent_site, $parent_company);
						}
						else if($resultsArray[$i]['type'] == 4){
							echo $this->displayArticle($resultsArray[$i], 2, $domain, $parent_site);
						}
						else if($resultsArray[$i]['type'] == 5){
$Blog = new Blog();
							$blogInfo = $this->fetchArray(Blog::getBlogs(NULL, NULL, $resultsArray[$i]['blog_id']));
							echo $Blog->createPostHTML($resultsArray[$i], 2, $blogInfo, $parent_site);
						}
						else { echo "error"; }
					}
				}//end for loop
				echo "<br style='clear:both;' />";
				$this->displayNav2($numResults, $limit, $offset, 'search', $keyword, NULL, $location);
			}
			
		}
		else {
			echo '<div class="blog_div"><div class="blog_heading"><h3>Please fill in both search boxes.</h3></div></div>';	
		}
	}
	
	/**********************************************************************/
	// function: Search Function
	// Returns an array of search results
	//
	// Accepted Input: Category/keyword, location, loose/strict search bool
	/**********************************************************************/	
	function searchFunction($keyword, $location, $page, $loose){
		/////////////////////
		//initial setup
		//******************
		$searchResults = array();
		$searchInfo = array();
		$searchInfo['numTypes'] = 0;
		$searchInfo['numResults'] = 0;
		$i = 0; //starts/tracks numbering of searchResults array
		$fixedLoc = $this->fixLoc($location); //send location to fixLoc() function to check/correct it
		
		/////////////////////
		//get local results
		//******************
		//directory listings that match basic criteria
		$query = "SELECT * 
				FROM directory 
				WHERE category != '' 
				AND company_name != '' 
				AND phone != '' 
				AND street != '' 
				AND city != '' 
				AND state != '' 
				AND category != '' 
				AND website_url != '' 
				AND display_info != '' 
				AND keyword_one != '' 
				AND trashed = '0000-00-00' ";
		if($page=='search'){
			//and match search keyword 
			$query .= " AND (category LIKE '%".$keyword."%' OR company_name LIKE '%".$keyword."%') ";
			//and match search location
			if($fixedLoc[0] && $fixedLoc[1]){
				$query .= " AND (city = '".$fixedLoc[0]."' OR state = '".$fixedLoc[1]."') ";
			} else if($fixedLoc[1]) {
				$query .= " AND state = '".$fixedLoc[1]."' ";
			} else {
				$query .= " AND city = '".$fixedLoc[0]."' ";
			}
		} else {
			if($keyword!='all' && $keyword) $query .= " AND category = '".$keyword."' ";
		}
		
		//get resulting rows and add them to the $searchResults array
		$result = $this->query($query);
		if($result){
			while($row = $this->fetchArray($result)){
				$searchInfo['directory'] = 1;
				$searchInfo['numResults']++;
				$searchResults[$i] = $row;
				$i++;
			}
		}
		
		/////////////////////
		//get hub results
		//******************
		//HUBs that match basic criteria
		$query = "SELECT * 
				FROM hub 
				WHERE name != '' 
				AND domain != '' 
				AND category != '' 
				AND company_name != '' 
				AND meta_description != '' 
				AND keyword1 != ''
				AND trashed = '0000-00-00' ";
		if($page=='search'){
			//and match search keyword 
			$query .= " AND (category LIKE '%".$keyword."%' OR company_name LIKE '%".$keyword."%') ";
			//and match search location if search not set to loose
			if($fixedLoc[0] && $fixedLoc[1]){
				$query .= " AND (city = '".$fixedLoc[0]."' OR state = '".$fixedLoc[1]."') ";
			} else if($fixedLoc[1]) {
				$query .= " AND state = '".$fixedLoc[1]."' ";
			} else {
				$query .= " AND city = '".$fixedLoc[0]."' ";
			}
		} else {
			if($keyword!='all' && $keyword) $query .= " AND category = '".$keyword."' ";
		}
		
		//get resulting rows and add them to the $searchResults array
		$result = $this->query($query);
		if($result){
			while($row = $this->fetchArray($result)){
				$searchInfo['hub'] = 1;
				$searchInfo['numResults']++;
				$searchResults[$i] = $row;
				$i++;
			}
		}
		
		/////////////////////
		//get press results
		//******************
		//press releases that match basic criteria
		$query = "SELECT * 
				FROM press_release 
				WHERE headline != '' 
				AND summary != '' 
				AND body != '' 
				AND category != ''
				AND network = 1
				AND trashed = '0000-00-00'";
		if($page=='search'){
			//and match search keyword 
			$query .= " AND (category LIKE '%".$keyword."%' OR headline LIKE '%".$keyword."%' OR summary LIKE '%".$keyword."%') ";
			if($loose==false){
				//and match search location if search not set to loose
				if($fixedLoc[0] && $fixedLoc[1]){
					$query .= " AND (contact LIKE '%".$fixedLoc[0]."%' OR contact LIKE '%".$fixedLoc[1]."%') ";
				} else if($fixedLoc[1]) {
					$query .= " AND contact LIKE '%".$fixedLoc[1]."%' ";
				} else {
					$query .= " AND contact LIKE '%".$fixedLoc[0]."%' ";
				}
			}
		} else {
			if($keyword!='all' && $keyword) $query .= " AND category = '".$keyword."' ";
		}
		
		//get resulting rows and add them to the $searchResults array
		$result = $this->query($query);
		if($result){
			while($row = $this->fetchArray($result)){
				$searchInfo['press'] = 1;
				$searchInfo['numResults']++;
				$searchResults[$i] = $row;
				$i++;
			}
		}
		
		/////////////////////
		//get article results
		//******************
		//articles that match basic criteria
		$query = "SELECT * 
				FROM articles 
				WHERE headline != '' 
				AND summary != '' 
				AND body != '' 
				AND network = 1
				AND trashed = '0000-00-00' ";
		if($page=='search'){
			//and match search keyword 
			$query .= " AND (category LIKE '%".$keyword."%' OR headline LIKE '%".$keyword."%' OR summary LIKE '%".$keyword."%') ";
			if($loose==false){
				//and match search location if search not set to loose
				if($fixedLoc[0] && $fixedLoc[1]){
					$query .= " AND (contact LIKE '%".$fixedLoc[0]."%' OR contact LIKE '%".$fixedLoc[1]."%') ";
				} else if($fixedLoc[1]) {
					$query .= " AND contact LIKE '%".$fixedLoc[1]."%' ";
				} else {
					$query .= " AND contact LIKE '%".$fixedLoc[0]."%' ";
				}
			}
		} else {
			if($keyword!='all' && $keyword) $query .= " AND category = '".$keyword."' ";
		}
		
		//get resulting rows and add them to the $searchResults array
		$result = $this->query($query);
		if($result){
			while($row = $this->fetchArray($result)){
				$searchInfo['article'] = 1;
				$searchInfo['numResults']++;
				$searchResults[$i] = $row;
				$i++;
			}
		}
		
		/////////////////////
		//get blog post results
		//******************
		//blog posts that match basic criteria
		$query = "SELECT * 
				FROM blog_post 
				WHERE category != '' 
				AND post_title != '' 
				AND post_content != '' 
				AND tags != '' 
				AND network = 1
				AND trashed = '0000-00-00' ";
		if($page=='search'){
			//and match search keyword 
			$query .= " AND (category LIKE '%".$keyword."%' OR post_title LIKE '%".$keyword."%' OR tags LIKE '%".$keyword."%') ";
			if($loose==false){
				if($fixedLoc[0] && $fixedLoc[1]){
					$query .= " AND (post_title LIKE '%".$fixedLoc[0]."%' OR post_title LIKE '%".$fixedLoc[1]."%') ";
				} else if($fixedLoc[1]) {
					$query .= " AND post_title LIKE '%".$fixedLoc[1]."%' ";
				} else {
					$query .= " AND post_title LIKE '%".$fixedLoc[0]."%' ";
				}
			}
		} else {
			if($keyword!='all' && $keyword) $query .= " AND category = '".$keyword."' ";
		}
		
		//get resulting rows and add them to the $searchResults array
		$result = $this->query($query);
		if($result){
			while($row = $this->fetchArray($result)){
				$searchInfo['blog'] = 1;
				$searchInfo['numResults']++;
				$searchResults[$i] = $row;
				$i++;
			}
		}
		
		if(isset($searchInfo['directory'])) $searchInfo['numTypes']++;
		if(isset($searchInfo['hub'])) $searchInfo['numTypes']++;
		if(isset($searchInfo['press'])) $searchInfo['numTypes']++;
		if(isset($searchInfo['article'])) $searchInfo['numTypes']++;
		if(isset($searchInfo['blog'])) $searchInfo['numTypes']++;
		
		$results = array('data' => $searchResults, 'info' => $searchInfo);
		return $results;
	}
}
?>
