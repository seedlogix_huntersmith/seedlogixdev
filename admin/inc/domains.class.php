<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
require_once QUBEADMIN . 'inc/cpanel.xmlapi.php';
class Domains extends Local {
	
        const SAPPHIREPASS  =   '=a_~85RnTu#n';//3376igniteD';
        
	//Check if the specified domain is the specified user's
	function validateDomainOwnership($uid, $domain){
		$query = "SELECT count(id) FROM hub WHERE domain LIKE '%".$domain."' AND user_id = '".$uid."'";
		$check1 = $this->queryFetch($query);
		if($check1['count(id)']){
			return true;
		}
		else {
			//if no hub domain, check if the domain is for a blog with their UID
			$query = "SELECT count(id) FROM blogs WHERE domain LIKE '%".$domain."' AND user_id = '".$uid."'";
			$check2 = $this->queryFetch($query);
			if($check2['count(id)']) return true;
			else return false;
		}
	}
	
	//Function to break up URL into useful parts.  Returns an associative array of parts.
	function breakUpURL($url){
		$url_array = explode(".", $url);
		//Remove http://
		if(substr($url_array[0], 0, 7)=='http://') $url_array[0] = substr($url_array[0], 7);
		else if(substr($url_array[0], 0, 8)=='https://') $url_array[0] = substr($url_array[0], 8);
		$numParts = count($url_array);
		if($numParts==2){
			$return['sub'] = "www";
			$return['name'] = $url_array[0];
			$return['tld'] = $url_array[1];
		} else if($numParts==3){
			if($url_array[1]=='co' || $url_array[1]=='com'){
				$return['sub'] = 'www';
				$return['name'] = $url_array[0];
				$return['tld'] = $url_array[1].'.'.$url_array[2];
			}
			else {
				$return['sub'] = $url_array[0];
				$return['name'] = $url_array[1];
				$return['tld'] = $url_array[2];
			}
		} else if($numParts==4){
			if($url_array[2]=='co' || $url_array[2]=='com'){
				$return['sub'] = $url_array[0];
				$return['name'] = $url_array[1];
				$return['tld'] = $url_array[2].'.'.$url_array[3];
			}
			else $error = 1;	
		} else $error = 1;
		
		$return['full'] = $url;
		
		if(!$error){
			return $return;
		}
		else return false;
	}
	//Function to check if another user has set up this domain
	function checkOtherUsers($domain, $uid, $rid = '', $sub = '', $resellerClient = ''){
		if($rid && $sub!='www'){
			//if user is a reseller adding a subdomain to one of their
			//user's hubs, only check hubs/blogs that aren't under their
			//reseller ID
			$field = 'user_parent_id';
			$id = $rid;
		}
		else {
			$field = 'user_id';
			$id = $uid;
		}
		//check hub table
		$query = "SELECT count(id) FROM hub WHERE ".$field." != ".$id." AND domain LIKE '%.".$domain."%'";
		$query .= " OR (".$field." != ".$id." AND domain LIKE 'http://".$domain."%')";
	deb($query);
		$result = $this->queryFetch($query);
		if(!$result['count(id)']){
			//if not in hub table check blogs too
			$query = "SELECT count(id) FROM blogs WHERE ".$field." != ".$id." AND domain LIKE '%.".$domain."%'";
			$query .= " OR (".$field." != ".$id." AND domain LIKE 'http://".$domain."%')";
			$result2 = $this->queryFetch($query);
			if(!$result2['count(id)']) return true;
			else return false;
		}
		else if($resellerClient){
			//if user is a reseller client check to see if they're trying to add a subdomain under
			//a domain owned by the parent
			$query = "SELECT main_site, support_email FROM resellers WHERE admin_user = (SELECT users.parent_id FROM users WHERE users.id = '".$uid."') LIMIT 1";
			if($result = $this->queryFetch($query, NULL, 1)){
				if((strpos($domain, $result['main_site'])!==false) && $sub!='www'){
					//condition is true.  email the reseller to notify of the new subdomain
					//get user info first
					$query = "SELECT username FROM users WHERE id = '".$uid."' LIMIT 1";
					$userInfo = $this->queryFetch($query);
					$to = $result['support_email'];
					$from = 'admin@6qube.com';
					$subject = 'Domain Use Alert - '.$sub.'.'.$domain;
					$message = 
					'<h1>Domain Use Alert</h1>
					<p><strong>One of your users has created a subdomain of your main domain, '.$result['main_site'].'</strong><br />
					This shouldn\'t be anything to worry about, but you should check the information below to make sure that an abuse hasn\'t occured (such as a user creating a domain like admin.'.$result['main_site'].')</p>
					<p>
					<strong>User ID/Email:</strong> '.$userInfo['username'].' (#'.$uid.')<br />
					<strong>Subdomain Created:</strong> <a href="http://'.$sub.'.'.$domain.'">'.$sub.'.'.$domain.'</a>
					</p>';
					$this->sendMail($to, $subject, $message, NULL, $from, NULL, 1);
					return true;
				}
				else return true;
			}
			else return false;
		}
		else return false;
	}
	
	//Function to check if a domain exists at all in the system
	function checkIfDomainInUse($domain, $returnServer = ''){
		if($domain = $this->sanitizeInput($domain)){
			//check hubs
			if($returnServer) $query = "SELECT lincoln_domain ";
			else $query = "SELECT count(id) ";
			$query .= " FROM hub WHERE domain LIKE '%".$domain."' ";
			if($returnServer) $query .= " ORDER BY lincoln_domain DESC LIMIT 1 ";
			$result1 = $this->queryFetch($query);
			if($returnServer){
				if($result1){
					$return = $result1['lincoln_domain'] ? 'Lincoln' : 'Kennedy';
					return $return;
				}
				else $keepSearching = 1;
			}
			if((!$result1['count(id)'] && !$returnServer) || $keepSearching){
				//check the blogs table too
				if($returnServer) $query = "SELECT lincoln_domain ";
				else $query = "SELECT count(id) ";
				$query .= " FROM blogs WHERE domain LIKE '%".$domain."'";
				if($returnServer) $query .= " ORDER BY lincoln_domain ASC LIMIT 1 ";
				$result2 = $this->queryFetch($query);
				if($returnServer){
					if($result2){
						$return = $result2['lincoln_domain'] ? 'Lincoln' : 'Kennedy';
						return $return;
					}
					else return false;
				}
				else if($result2['count(id)']) return true;
				else return false;
			}
			else if(!$returnServer) return true;
		}
		else return false;
	}
	
	//Function to use the WHMCS API to get WHOIS info
	//Must be in "domain.com" format
	static function getWhoisStatic($domain){
		$postfields["action"] = "domainwhois";
		$postfields["domain"] = $domain;
		$result = self::billAPIStatic($postfields);
		$result['whois'] = urldecode($result['whois']); //added urldecode 7-28 - Reese
		
		return $result;
	}
        
        // @amado 2012. deprecated, use Static methods for methods that do not require the use of the $this variable. this is common practice
        function getWhois($domain)
        {
            return self::getWhoisStatic($domain);
        }
	
	function getNameServers2($whois, $tld){
		//the function uses smart RegEx search calls to search through provided WHOIS text
		//and determine the domain's nameservers.  WHOIS text results are different for some TLDs
		//exception TLDs:
		if($tld == "co.uk"){
//commented out for now because of issues
//			//co.uk looks like:
//			//Name servers:
//			//ns1.6qube.com
//			//ns2.6qube.com
//			$nsSearch = array();
//			preg_match_all('/Name servers:\<br \/\>(.*)\<br \/\>/i', $whois, $nsSearch);
//			if(is_array($nsSearch) && !empty($nsSearch)){
//				$results = $nsSearch;
//				$nsDump = $nsSearch[1];
//				if($nameservers = explode('<br />', trim($nsDump))){
//					$i = 1;
//					$results = array();
//					foreach($nameservers as $nameserver){
//						$results['ns'.$i] = $nameserver;
//						$i++;
//					}
//				}
//				else $results = false;
//			}
//			else $results = false;
			$results = false;
		}
		else {
			//regular WHOIS name server text looks like:
			//Name Server: ns1.6qube.com
			//Name Server: ns2.6qube.com
			//smart regex search through whois text for name servers
			$nsSearch = array();
			preg_match_all('/Name Server:(.*)\<br \/\>/i', $whois, $nsSearch);
			if(is_array($nsSearch) && !empty($nsSearch)){
				$i = 1;
				$results = array();
				foreach($nsSearch[1] as $nameserver){
					$results['ns'.$i] = trim($nameserver);
					$i++;
				}
			}
			else $results = false;
		}
		
		return $results;
	}
	
        // @deprecated call static method directly
        function getNameservers($whois, $tld)
        {
            return self::getNameserversStatic($whois, $tld);
        }
        
	//Function to extract first two nameservers from WHOIS info
	static function getNameserversStatic($whois, $tld){
		//Find where nameserver info starts/ends in WHOIS result, extract it
		//find start
		if($tld == "co.uk") $ns_pos_beg = strpos($whois, "Name servers:");
		else $ns_pos_beg = strpos($whois, "Name Server:");
		//find end
		if($tld == "org") $ns_pos_end = strpos($whois, "DNSSEC:");
		else if($tld == "info") $ns_pos_end = strpos($whois, "Name Server: <br />");
		else $ns_pos_end = strpos($whois, "Status: ");
		$nameservers = substr($whois, $ns_pos_beg, ($ns_pos_end-$ns_pos_beg))."<br />";
		
		//Separate nameserver section into useful parts.
		//Example input: Name Server: NS1.6QUBE.NET<br />Name Server: NS2.6QUBE.NET
		$nameservers_array = explode(":", $nameservers);
		$ns1_array = explode("<br />", $nameservers_array[1]);
		$ns1 = trim($ns1_array[0], " \n\r\t\0\x0B");
		if($nameservers_array[2]){ 
			$ns2_array = explode("<br />", $nameservers_array[2]);
			$ns2 = trim($ns2_array[0], " \n\r\t\0\x0B");
		}
		if($nameservers_array[3]){ 
			$ns3_array = explode("<br />", $nameservers_array[3]);
			$ns3 = trim($ns3_array[0], " \n\r\t\0\x0B");
		}
		if($nameservers_array[4]){ 
			$ns4_array = explode("<br />", $nameservers_array[4]);
			$ns4 = trim($ns4_array[0], " \n\r\t\0\x0B");
		}
		
		if($ns1){
			$results['ns1'] = $ns1;
			$results['ns2'] = $ns2;
			$results['ns3'] = $ns1;
			$results['ns4'] = $ns2;
			return $results;
		}
		else return false;
	}
	
	//Function to use cPanel API to create an addon domain
	function createAddon($dir, $domain, $password, $domainName, $subdomain, $tld, $type){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
	
		$addon = json_decode($xmlapi->api2_query("sapphire", "AddonDomain", "addaddondomain",
                        array(dir=>$dir, newdomain=>$domain, pass=>$password, subdomain=>$domainName)), true);
		
		if($addon["cpanelresult"]["data"][0]["result"]==1){ //if domain addon success
			//an error can occur where result=1 but the domain is not added
			//in that case, "reason" will contain server terms like "kennedy"
			if(strpos($addon["cpanelresult"]["data"][0]["reason"], "Removed")===false){
				if($subdomain!="www"){
					$results = $this->createSubdomain($dir, $subdomain, $domain, $type);
					if($results['message']=="Subdomain successfully created!"){
						$results['message'] = "Domain and subdomain were set up successfully!";
						$addon['failure'] = false;
					}
				}
				else {
					$results['error'] = 0;
					$results['message'] = "Domain was set up successfully!";
					$results['reason'] = $addon["cpanelresult"]["data"][0]["reason"];
					$results['dir'] = $dir;
					$addon['failure'] = false;
				}
				return $results;
			}
			else {
				$results["error"] = 1;
				$results["message"] = "There was an error creating your addon domain.  Please contact us at 877-570-5005 to have your website set up manually. Error 1";
				//$results["message"] .= "<br />".$addon["cpanelresult"]["data"][0]["reason"];
				$addon['failure'] = false;
				return $results;
			}
		}
		else {
			$addon['failure'] = true;
			return $addon; 
		}
	}
	
	//Function to remove an addon domain
	function deleteAddon($domain, $uid){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$domain = $this->breakUpURL($domain);
		if($domain['name']!="6qube"){
			$deladdon = json_decode($xmlapi->api2_query(
							    "sapphire", "AddonDomain", "deladdondomain", 
							    array(domain=>$domain['name'].'.'.$domain['tld'], subdomain=>$domain['name'].'_bluejagweb.com')
					  ), true);
			
			if($deladdon["cpanelresult"]["data"][0]["result"]==1
			   &&(strpos($deladdon["cpanelresult"]["data"][0]["reason"], "Removed")!==false)){
				$results['success'] = true;
				$results['message'] = "Domain successfully removed.";
			}
			else {
				$results['success'] = false;
				$results['message'] = "Error removing domain: ".$deladdon["cpanelresult"]["data"][0]["reason"];
			}
			
			return $results;
		}
	}
	
	//Function to use cPanel API to create a subdomain for an addon domain
	function createSubdomain($dir, $sub, $root, $type = ''){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$subdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "addsubdomain", array(dir=>$dir, domain=>$sub, rootdomain=>$root)), true);
		
		if($subdomain["cpanelresult"]["data"][0]["result"]==1){
			$results["error"] = 0;
			$results["message"] = "Subdomain successfully created!";
			$results["message2"] = "http://www.".$root;
			$results["type"] = $type;
			$results["failure"] = false;
		}
		else {
			$results["error"] = 1;
			$results["message"] = "There was an error creating the subdomain.";
			$results["message2"] = $subdomain["cpanelresult"]["data"][0]["reason"];
			$results["failure"] = true;
		}
		
		return $results;
	}
	
	//Function to delete a subdomain
	function deleteSubdomain($domain){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$domain = $this->breakUpURL($domain);
		$delsubdomain = json_decode($xmlapi->api2_query("sapphire", "SubDomain", "delsubdomain", 
							   array(domain=>$domain['sub'].'_'.$domain['name'].'.'.$domain['tld'])),
							   true);
		
		if($delsubdomain["cpanelresult"]["data"][0]["result"]==1
		  &&(strpos($delsubdomain["cpanelresult"]["data"][0]["reason"], "Removed ".$domain['sub'])!==false)){
			$results['success'] = true;
			$results['message'] = 'Successfully deleted subdomain!';
		}
		else {
			$results['success'] = false;
			$results['message'] = "Error removing subdomain: ".$delsubdomain["cpanelresult"]["data"][0]["reason"];
		}
		
		return $results;
	}
	
	//Function to display all addon domains
	function listAddons(){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$addons = json_decode($xmlapi->api2_query("sapphire", "AddonDomain", "listaddondomains", array(regex=>''), true), true);
		foreach($addons['cpanelresult']['data'] as $key=>$value){
			echo "#".$key.":<br />";
			echo "<b>domain: </b>".$value['domain']."<br />";
			echo "<b>domainkey: </b>".$value['domainkey']."<br />";
			echo "<br />";
		}
	}
	
	//Function to create root addons for reseller site (hubs.reseller.com, etc)
	function setupResellerSubs($domain){
		$errors = 0;
		//login.reseller.com
		$dir = "public_html/6qube.com/admin";
		$subdomain = "login";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//hubs.reseller.com
		$dir = "public_html/6qube.com/hubs";
		$subdomain = "hubs";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//blogs.reseller.com
		$dir = "public_html/6qube.com/blogs";
		$subdomain = "blogs";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//articles.reseller.com
		$dir = "public_html/6qube.com/articles";
		$subdomain = "articles";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//press.reseller.com
		$dir = "public_html/6qube.com/press";
		$subdomain = "press";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//local.reseller.com
		$dir = "public_html/6qube.com/local";
		$subdomain = "local";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//browse.reseller.com
		$dir = "public_html/6qube.com/browse";
		$subdomain = "browse";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		//search.reseller.com
		$dir = "public_html/6qube.com/search";
		$subdomain = "search";
		$results = $this->createSubdomain($dir, $subdomain, $domain);
		if($results["failure"]==true && strpos($results["message2"], "Domain already exists")===false) $errors++;
		
		if(!$errors) return true;
		else return false;
	}
	
	//testing function
	function checkmx($domain ){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		//first check if there are existing MX records
		$check = json_decode($xmlapi->api2_query("sapphire", "Email", "listmxs", array(domain=>$domain)), true);
		if($check['cpanelresult']['data'][0]['status'] && $check['cpanelresult']['data'][0]['entries']){
			//var_dump($check);
			foreach($check['cpanelresult']['data'][0]['entries'] as $key=>$value){
				echo 'mx: '.$value['mx'].'<br />';
				echo 'domain: '.$value['domain'].'<br />';
				echo 'priority: '.$value['priority'].'<br /><br />';
			}
		}
	}
	
	//Function to set up Google Apps support
	function addGoogleAppsMX($domain){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		//add each Google Mail server
		$mx1 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'aspmx.l.google.com', preference=>'10')), true);
		$mx2 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'alt1.aspmx.l.google.com', preference=>'20')), true);
		$mx3 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'alt2.aspmx.l.google.com', preference=>'20')), true);
		$mx4 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'aspmx2.googlemail.com', preference=>'30')), true);
		$mx5 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'aspmx3.googlemail.com', preference=>'30')), true);
		$mx5 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'aspmx4.googlemail.com', preference=>'30')), true);
		$mx6 = json_decode($xmlapi->api2_query("sapphire", "Email", "addmx", 
						array(domain=>$domain, exchange=>'aspmx5.googlemail.com', preference=>'30')), true);
		if($mx1['cpanelresult']['data'][0]['status'] && 
			$mx2['cpanelresult']['data'][0]['status'] && 
			$mx3['cpanelresult']['data'][0]['status'] && 
			$mx4['cpanelresult']['data'][0]['status'] && 
			$mx5['cpanelresult']['data'][0]['status'] && 
			$mx6['cpanelresult']['data'][0]['status']){
				//if successful adding all, delete original self-pointing MX entry
				$del = json_decode($xmlapi->api2_query("sapphire", "Email", "delmx", 
								array(domain=>$domain, exchange=>$domain, priority=>0)), true);
				//add cname record to point mail.domain.com to Google Apps Mail login
				$cname = json_decode($xmlapi->api2_query("sapphire", "ZoneEdit", "add_zone_record", 
							array(domain=>$domain, name=>'email', type=>'CNAME', cname=>'ghs.google.com', ttl=>14400)), true);
				return true;
		}
		else return false;
	}
	
	//function to return the email addresses for a given domain
	//assumes $domain input string is in the format 'domain.com'
	function getEmailAddresses($domain){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		$regex = '@'.$domain;
		
		$emails = json_decode($xmlapi->api2_query("sapphire", "Email", "listpops", array(regex=>$regex)), true);
		if(count($emails['cpanelresult']['data'])>0){
			$return['address'] = array();
			foreach($emails['cpanelresult']['data'] as $email) { 
				$return['address'][] = $email['email']; 
			}
		}
		else $return = "No email addresses found for the domain ".$domain.".";
		
		return $return;
	}
	
	//function to add a new email address for a given domain
	//assumes $domain input string is in the format 'domain.com'
	function addNewEmailAccount($account, $domain){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$tempPwd = substr(md5(time().'se0f8vdMMMCCN1s,xm'.$domain), 0, 8);
		
		$params = array(domain=>$domain, email=>$account, password=>$tempPwd, quota=>25);
		$add = json_decode($xmlapi->api2_query("sapphire", "Email", "addpop", $params), true);
		
		if($add['cpanelresult']['data'][0]['result']){
			$return['success'] = 1;
			$return['message'] = $tempPwd;
		}
		else {
			$return['success'] = 0;
			$return['message'] = "Error creating email account:\n".$add['cpanelresult']['data'][0]['reason'];
		}
		
		return $return;
	}
	
	//change an email account's password to a temporary one and return
	//assumes $domain input string is in the format 'domain.com'
	function resetEmailPassword($account, $domain){
		$ip = "50.22.60.10";
		$xmlapi = new xmlapi($ip);
		$xmlapi->password_auth("sapphire",Domains::SAPPHIREPASS);
		$xmlapi->set_output('json');
		
		$tempPwd = substr(md5(time().'se0f8vdMMMCCN1s,xm'.$domain), 0, 8);
		
		$params = array(domain=>$domain, email=>$account, password=>$tempPwd);
		$add = json_decode($xmlapi->api2_query("sapphire", "Email", "passwdpop", $params), true);
		
		if($add['cpanelresult']['data'][0]['result']){
			$return['success'] = 1;
			$return['message'] = $tempPwd;
		}
		else {
			$return['success'] = 0;
			$return['message'] = "Error changing password:\n".$add['cpanelresult']['data'][0]['reason'];
		}
		
		return $return;
	}
	
	//check if a domain is on the lincoln server
	function isLincolnDomain($domain){
		$server = $this->checkIfDomainInUse($domain, 1);
		if($server=='Lincoln') return true;
		else return false;
	}
	
	//Update a site's htaccess file to change www/non-www behavior
	//Added 6/3/2012 by Reese
	function changeWwwOptions($hub_id, $option){
		$result['success'] = 0;
		$result['message'] = '';
		//get domain name and figure out if it's a lincoln domain or not
		$hubInfo = $this->queryFetch("SELECT user_id_created, domain, lincoln_domain FROM hub WHERE id = '".$hub_id."'", NULL, 1);
		$domainInfo = $this->breakUpURL($hubInfo['domain']);
		$domain = $domainInfo['name'].'.'.$domainInfo['tld'];
		$uid = $hubInfo['user_id_created'];
		//set the code to use
		if($option=='www_only'){
			$code = 
'Options +FollowSymLinks
RewriteEngine on
Rewritecond %{HTTP_HOST} ^'.$domain.' [NC]
RewriteRule ^(.*)$ http://www.'.$domain.'/$1 [R=301,L] 
RewriteRule ^(.*)/$ index.php?page=$1';
		} else if($option=='no_www'){
			$code = 
'Options +FollowSymLinks
RewriteEngine on
Rewritecond %{HTTP_HOST} ^www.'.$domain.' [NC]
RewriteRule ^(.*)$ http://'.$domain.'/$1 [R=301,L] 
RewriteRule ^(.*)/$ index.php?page=$1';
		} else {
			$code = 
'Options +FollowSymLinks
RewriteEngine on
RewriteRule ^(.*)/$ index.php?page=$1';			
		}
		//if it's on kennedy write the code
		if(!$hubInfo['lincoln_domain']){
			$dir = QUBEROOT . 'hubs/domains/'.$uid.'/'.$hub_id.'/';
			//first delete the existing .htaccess
			if(@is_file($dir.'.htaccess')) @unlink($dir.'.htaccess');
			//now create the file
			if($f = @fopen($dir.'.htaccess', 'w')){
				if(@fwrite($f, $code)!==false){
					//Success!
					$result['success'] = 1;
				}
				else $result['message'] = 'Error writing to the file.';
				@fclose($f);
			}
			else $result['message'] = 'Error opening/creating the file.';
		} //end if(!$hubInfo['lincoln_domain'])
		else {
			//domain is on the lincoln server -- securely send the update code over there in base64
			//scramble it way up to get the correct hash
			$code = base64_encode(str_replace('\\', '', $code));
			$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
			$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
			$secHash = md5(substr($code, 20).$salt);
			$secHash = md5($secHash.$hub_id.$pepper.$uid);
			//makes the hash only good if it was submitted today:
			$secHash = md5($secHash.date('d'));
			$data = @file_get_contents('http://50.97.78.210/secure/www_options.php?hid='.$hub_id.'&uid='.$uid.'&hash='.$secHash.'&d='.$code);
			//interpret json response:
			$result = json_decode($data, true);
		}
		
		return $result;
	}
}
?>
