<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Affiliates extends Local {
	var $counter = 0;
	
	//get affiliates
	function getAffiliates($uid){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//build query
				$query = "SELECT * FROM user_info WHERE referral_id = '".$uid."'";	
				$query .= " ORDER BY id ASC";
						
				if($result = $this->query($query)){
	
					while($row = $this->fetchArray($result, 1)){
						$affiliatesView[$row['id']] = $row;
					}
					$return = $affiliatesView;
				}
				else $return = false;
			
			
			
			//return final data!
			return $return;
		}
		else return false;
	}
	
	//get affiliates count
	function getAffiliatesCount($uid){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($continue){
			//build query
				$query = "SELECT count(id) FROM user_info WHERE referral_id = '".$uid."'";	
				$total = $this->queryFetch($query);		
				$return = $total['count(id)'];

			//return final data!
			return $return;
		}
		else return false;
	}
	
	//get Class Info
	function getClassInfo($uid){
		//sanitize
		$uid = is_numeric($uid) ? $uid : '';
		
		//authorize
		if($uid){
			$continue = true;
		}
		
		if($uid){
			//build query
			$query = "SELECT class FROM users WHERE id = '".$uid."'";
			$row = $this->queryFetch($query, NULL, 1);
			$query2 = "SELECT name FROM user_class WHERE id = '".$row['class']."'";
			
			$return = $this->queryFetch($query2, NULL, 1);
			
			//return final data!
			return $return;
		}
		else return false;
	}
	
}
?>