<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
require_once QUBEADMIN . 'inc/local.class.php';
class Blog extends Local {
	var $blog_id;
	var $blog_rows;
	var $post_rows;
	var $wrapper;	// new wrapper
	
	//returns all blogs, accepts user_ID and limit
	function getBlogs($user_id = '', $limit = '', $blog_id = '', $cid = '', $count = '', $limited = '', $returnArray = ''){
		//validate inputs
            static $f=0;
		$user_id = is_numeric($user_id) ? $user_id : 0;
		$limit = is_numeric($limit) ? $limit : 0;
		$blog_id = is_numeric($blog_id) ? $blog_id : 0;
		$cid = is_numeric($cid) ? $cid : 0;
		
#                if($f++ > 3) throw new Exception();
		if($limited) $fields = 'id, cid, user_id, domain, blog_title, posts_v2';
		else $fields = '*';
		
		if($count) $query = "SELECT id FROM blogs";
		else $query = "SELECT ".$fields." FROM blogs";
		$query .= " WHERE trashed = '0000-00-00' ";
		if($user_id) $query .= " AND user_id = '".$user_id."'";
		if($blog_id) $query .= " AND id = '".$blog_id."'";
		if($cid) $query .= " AND cid = '".$cid."'";
		if($limit) $query .= " LIMIT ".$limit;
		if($blog_id && $returnArray){
			return $this->queryFetch($query, NULL, 1);
		}
		else {
			$result = $this->query($query);
			if($result){
				$this->blog_rows = $this->numRows($result);
				return $result;
			} else {
				return false;
			}
		}
	}
	
	function userBlogsDropDown($user_id, $name, $cid = '', $noClass = '', $selected = ''){
		$result = $this->getBlogs($user_id, NULL, NULL, $cid);
		
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$blogArray[$row['id']] = $row['blog_title'];
			}
			if(!$noClass) $class = 'ajax-select uniformselect';
			return $this->buildSelect($name, $blogArray, $selected, $class); //(name, values, option to select, class)
		}
		else return 'No blogs created yet.';
	}
	
	//returns all post for specified blog ID
	function getBlogPost($blog_id = '', $limit = '', $user_id = '', $cid = '', $category = '', $limited = ''){
		//build query
		if(!$limited) $query = "SELECT if(publish_date=0, created, publish_date) as created, * FROM blog_post";
		else $query = "SELECT id, category, category_url, post_title, post_title_url, post_content, created FROM blog_post";
		$query .= " WHERE blog_post.trashed = '0000-00-00' ";
		$query .= " AND (SELECT blogs.trashed FROM blogs WHERE blogs.id = blog_post.blog_id) = '0000-00-00' ";
		if($blog_id) $query .= " AND blog_id = ".$blog_id;
		if($user_id) $query .= " AND blog_post.blog_id IN (SELECT id FROM blogs WHERE user_id = ".$user_id.")";
		if($cid) $query .= " AND blog_post.blog_id IN (SELECT id FROM blogs WHERE cid = ".$cid.")";
		if($category) $query .= " AND category = '".$category."'";
		$query .= " ORDER BY created DESC";
		if($limit) $query .= " LIMIT ".$limit;
		//query for results
		$result = $this->query($query);
		//set number of rows
		if($result) $this->post_rows = $this->numRows($result);
		//return query results
		return $result;
	}
	
	//returns a single blog post
	function getSinglePost($post_id){
		if(!is_numeric($post_id)) $post_id = 0;
		$query = "SELECT blog_post.*,if(publish_date=0, created, publish_date) as created FROM blog_post WHERE id = '".$post_id."'";
		$query .= " AND trashed = '0000-00-00'";
		$result = $this->query($query);
		if($result && $this->post_rows = $this->numRows($result)){
			$row = $this->fetchArray($result);
			return $row;
		}
		else return false;
	}
	
	//adds new blog to DB (assumes you validate input before passing)
	function addNewBlog($user_id, $blog_title, $cid, $blog_limit = '', $bypass = '', $parent_id = '', $singleid){
		if(!$blog_limit && !$bypass) $blog_limit = $this->getLimits($user_id, 'blog');
		if(!$bypass){
			$blogs = $this->getBlogs($user_id, NULL, NULL, $cid, true);
			$blogcount = $blogs ? $this->numRows($blogs) : 0;
		}
		if(!$parent_id) $parent_id = '0';
		require_once dirname(__FILE__) . '/../6qube/debugger.php';
                
		if($blog_limit==-1 || ($blog_limit - $blogcount)>0 || $bypass){
			$query = "INSERT INTO blogs 
						(cid, user_id, user_id_created, user_parent_id, blog_title, posts_v2, created) 
					VALUES 
						('".$cid."', '".$user_id."', '".$user_id."', (select if(parent_id=0,$user_id,parent_id)
                                                        from users where id = $user_id), '" .
//                                                    '".$parent_id."', " 
                                    $blog_title."', 1, NOW())";
                        
			$result = $this->query($query);
			
			if($result){
				$id = $this->getLastId();
				$settings = $this->getSettings();
				$errors = '';
				
				$user_dir = $settings['site_root']."blogs/domains/$user_id";
				$user_blog = $settings['site_root']."blogs/domains/$user_id/$id";
				
				$old = umask(0); //temporarily escalate chmod priveleges
				if(is_dir($user_dir)){ //check to see if user directory exist
					if(is_dir($user_blog)){ //checks to see if users blog directory exist
						if(!@file_exists($user_blog.'/index.php')){ //checks to see if an index file exist
							if(!@copy($settings['site_root']."blogs/domains/index-copy.v2.php", $user_blog.'/index.php')){
								$errors .= 'Error copying index-copy.v2.php to $user_blog/index.php<br />';
							}
							if(!@copy($settings['site_root']."blogs/domains/htaccess.v2.txt", $user_blog.'/.htaccess')){
								$errors .= 'Error copying htaccess.v2.txt to $user_blog/.htaccess<br />';
							}
						}
					}
					else { //if directory for blog does not exist, create it
						if(@mkdir($user_blog, 0777)){
							if(!@copy($settings['site_root']."blogs/domains/index-copy.v2.php", $user_blog.'/index.php')){
								$errors .= 'Error copying index-copy.php to $user_blog/index.php<br />';
							}
							if(!@copy($settings['site_root']."blogs/domains/htaccess.v2.txt", $user_blog.'/.htaccess')){
								$errors .= 'Error copying htaccess.v2.txt to $user_blog/.htaccess<br />';
							}
						}
						else {
							$thisError = error_get_last();
							$emailErrors .= $error['message'];
							$errors .= 'Error creating folder $user_blog<br />';
						}
					}	
				}
				else { //If user directory does not exist create it and the blog folder
					if(@mkdir($user_dir, 0777)){
						if(@mkdir($user_blog, 0777)){
							if(!@copy($settings['site_root']."blogs/domains/index-copy.v2.php", $user_blog.'/index.php')){
								$errors .= 'Error copying index-copy.php to $user_blog/index.php<br />';
							}
							if(!@copy($settings['site_root']."blogs/domains/htaccess.v2.txt", $user_blog.'/.htaccess')){
								$errors .= 'Error copying htaccess.txt to $user_blog/.htaccess<br />';
							}
						}
						else $errors .= 'Error creating folder $user_blog<br />';
					}
					else {
						$thisError = error_get_last();
						$emailErrors .= $error['message'];
						$errors .= 'Error creating folder $user_dir<br />';
					}
				}
				
				$this->ANAL_addSite('blogs', $id, 'Blog'.$id);
				
				if($errors){
					//email 6qube admin with any errors
					$subject = 'Error(s) creating blog folder/files for ID #'.$id.' / UID #'.$user_id;
					$message = 'There were one or more errors during the blog creation process for Blog ID #'.$id.' by user #'.$user_id.'.  Read the error message below and manually correct the issues if necessary.<br /><br />'.$errors.'<br /><br />'.$emailErrors;
					$this->sendMail('admin@6qube.com', $subject, $message, NULL, NULL, NULL, 1);
					$this->sendMail('reeselester@gmail.com', $subject, $message, NULL, NULL, NULL, 1);
				}
				
				umask($old); //set chmod back
				if($singleid) return $id;
				else return $this->blogDashUpdate($id, $blog_title, $user_id);
				//return $query;
			}
			else return false;
		}
		else return false;
	}
	
	/***********************************************************
	// FUNCTION: Add New Post
	//  Adds a new Blog Post into the 'blog_post' table and
	//  installs a tracking id with Piwik
	//  
	/***********************************************************/
	function addNewPost($blog_id, $post, $user_id = '', $parent_id = ''){
		if(!$user_id){
			//$blog = $this->getBlogs(NULL, NULL, $blog_id);
			$query = "SELECT user_id, tracking_id FROM blogs WHERE id = ".$blog_id;
			$blog = $this->queryFetch($query);
			$user_id = $blog['user_id'];	
			$blog_tracking = $blog['tracking_id'];
		}
		else {
			$query = "SELECT tracking_id FROM blogs WHERE id = ".$blog_id;
			$blog = $this->queryFetch($query);
			$blog_tracking = $blog['tracking_id'];
		}
		if(!$parent_id) $parent_id = '0';
		
		$query = "INSERT INTO blog_post
					(blog_id, user_id, user_parent_id, tracking_id, category, category_url, post_title, post_title_url, post_content, tags, created) 
				VALUES
					(".$blog_id.", ".$user_id.", (select user_parent_id FROM blogs WHERE id = $blog_id ), '" .
//                                                .$parent_id.", '".
                                                  $blog_tracking."', 
					 '".$post['category']."', '".$this->convertKeywordUrl($post['category'], 1)."', 
					 '".$post['post_title']."', '".$this->convertKeywordUrl($post['post_title'], 1)."', 
					 '".$post['post_content']."', '".$post['tags']."', NOW())";
		$result = $this->query($query);
		
		if($result) {
			$id = $this->getLastId();
			//NOTE: commented out below: blog posts should use parent blog's tracking
			//$this->ANAL_addSite('blog_post', $id, 'http://www.example-blog.com/post');
			return $id;
			//return '<p>New blog post created!</p>';
		} else {
			return false;
		}
	}
	
	function editPost($post){
		$query = "UPDATE blog_post 
				SET  category = '".$post['category']."', 
					post_title = '".$post['post_title']."', 
					post_content = '".$post['post_content']."', 
					tags = '".$post['tags']."' 
				WHERE id = '".$post['edit']."'";
		
		if($result = $this->query($query))
			return '<p>Blog Post Updated!</p>';
		else
			return false;
	}
	
	/**********************************************************************/
	// function: Display Blog Post (NEW)
	// outputs a set of Blog Post results in new format
	//
	// Accepted Input: Query Limit and Query Offset
	/**********************************************************************/	 
	function displayPost($limit = '', $offset = '', $main = 0, $blog_id = '', $blog_disp = '', 
					 $city = '', $state = '', $category = '', $user_id = '', $cid = '', $domain = '', $parent = ''){
		if(!$domain) $domain = 'blogs.6qube.com';
		if($parent){
			if(is_numeric($parent)){
				//get parent site (for imgs)
				$query = "SELECT main_site FROM resellers WHERE admin_user = ".$parent;
				$a = $this->queryFetch($query);
				$parent_site = $a['main_site'];
			}
			else $parent_site = $parent;
		}
		//Query DB for total number of blog posts that fit criteria.  Only select id to save time
		$query = "SELECT count(p.id) FROM blog_post p ";
                if($blog_id){
                    $query .= "left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
                    left join blogfeed_subs s on (s.feed_ID = m.feed_ID)";
                }
		$query .= " WHERE ";
                if($blog_id){
                    $query .= "p.blog_ID = $blog_id and (isnull(s.blog_ID) or s.blog_ID = $blog_id) 
                            AND ";
                }else{
			// do not display slave posts
			$query .= "p.master_ID = 0 AND ";
		}
                $query .= "p.category != '' 
				  AND p.post_title != '' 
				  AND p.post_content != '' ";
		if(!$blog_disp) $query .= " AND p.tags != '' ";
		$query .= " AND p.blog_id != 0
				  AND p.trashed = '0000-00-00' AND ( p.publish_date = 0 OR p.publish_date <= DATE(NOW()) ) ";
		if(!$blog_disp) $query .= " AND p.network = 1";
		if($city) $query .= " AND (SELECT blogs.city FROM blogs WHERE id = p.blog_id) = '".$city."'";
		if($state) $query .= " AND (SELECT blogs.state FROM blogs WHERE id = p.blog_id) = '".$state."'";
		if($category) $query .= " AND (SELECT blogs.category FROM blogs WHERE id = p.blog_id) LIKE '%".$category."'";
#		if($blog_id) $query .= " AND blog_id = ".$blog_id;
		if($user_id) $query .= " AND p.user_id = ".$user_id;
		if($cid) $query .= " AND p.blog_id IN (SELECT id FROM blogs WHERE cid = ".$cid.")";
		if($parent && is_numeric($parent)) $query .= " AND (SELECT parent_id FROM users WHERE id = p.user_id) = ".$parent;
		deb($query);
		$result = $this->queryFetch($query, NULL, 1);
		if($result['count(p.id)']) $numResults = $this->post_rows = $result['count(p.id)'];
                    deb($numResults, $result);
		if($numResults){
			//Query DB for all data matching specific criteria
			$query = "SELECT 
						id, 
						p.blog_id, 
						tracking_id, 
						user_id, 
						category, 
						category_url, 
						post_title, 
						post_title_url, 
						post_content, 
						photo, 
						tags, 
						tags_url, 
						network, created,
						if(publish_date=0, created, publish_date) as publish_date_str
					FROM 
						blog_post p ";
                if($blog_id){
                    $query .= "left join blogfeed_masterposts m on (m.master_post_ID = p.master_ID)
                    left join blogfeed_subs s on (s.feed_ID = m.feed_ID)";
                }
		$query .= " WHERE ";
                if($blog_id){
                    $query .= "p.blog_ID = $blog_id and (isnull(s.blog_ID) or s.blog_ID = $blog_id) 
                            AND ";
                }else{
			$query .= " p.master_ID = 0 AND ";
		}
			$query .= " category != '' 
					  AND post_title != '' 
					  AND post_content != '' 
					  AND tags != '' 
					  AND p.blog_id != 0
					  AND p.trashed = '0000-00-00' AND ( p.publish_date = 0 OR p.publish_date <= DATE(NOW()) )";
			if(!$blog_disp) $query .= " AND network = 1";
			if($city) $query .= " AND (SELECT blogs.city FROM blogs WHERE id = p.blog_id) = '".$city."'";
			if($state) $query .= " AND (SELECT blogs.state FROM blogs WHERE id = p.blog_id) = '".$state."'";
			if($category) $query .= " AND (SELECT blogs.category FROM blogs WHERE id = p.blog_id) LIKE '%".$category."'";
#			if($blog_id && is_numeric($blog_id)) $query .= " AND p.blog_id = ".$blog_id;
			if($user_id && is_numeric($user_id)) $query .= " AND user_id = ".$user_id;
			if($cid) $query .= " AND p.blog_id IN (SELECT id FROM blogs WHERE cid = ".$cid.")";
			if($parent && is_numeric($parent)) 
				$query .= " AND (SELECT parent_id FROM users WHERE id = p.user_id) = ".$parent;
			$query .= " ORDER BY publish_date_str DESC";
			if($limit && is_numeric($limit)) $query .= " LIMIT ".$limit;
			if($offset && is_numeric($offset)) $query .= " OFFSET ".$offset;
			$result = $this->query($query); //query the DB
				deb($query);
			if($blog_id) $blogInfo = $this->fetchArray($this->getBlogs(NULL, NULL, $blog_id, NULL, NULL, true), 1);
			$lastID = '';
			while($row = $this->fetchArray($result, 1)){
				if(!$blogInfo || ($row['blog_id']!=$lastID)){
					$blogInfo = $this->fetchArray($this->getBlogs(NULL, NULL, $row['blog_id'], NULL, NULL, true), 1);
				}
				if($row['network'] || $blog_disp){
					$post_html	= $this->createPostHTML($row, $main, $blogInfo, $parent_site);

					$html .= $post_html;
				}
			}
			if($main==1)
				echo $html;
			else 
				echo "<div class=\"blog_right_links\"><ul id=\"popular_posts\">\n".$html."</ul></div>\n";
			
			if($main) 
				Local::displayNav3($domain, $this->post_rows, $limit, $offset, $city, $state, $category, $parent_site);
			else 
				echo '<div style="text-align:center;"><a href="http://'.$domain.'" class="neutral-grey" >More Posts</a></div>';
		}
		else {
			if($blog_id || $user_id)
				echo "This user hasn't made any blog posts yet.";
			else
				echo 'None.';
		}
	}
	
	function convertCategory($category){
		$category = str_replace(",","",$category);
		$category = str_replace("'","",$category);
		$category = str_replace("?","",$category);
		$category = str_replace(".","",$category);
		$category = str_replace("&","and",$category);
		$category = str_replace("+","plus",$category);
		$category = mb_strtolower("$category");
		$title = explode(" ", stripslashes($category));
		$title = implode("-", $title);
		$title = trim($title, "-");
		return $title.'/';
	}
	
	function getBlogPostSummary($blogInfo, $blogPostRow, $length)
	{
		if($this->wrapper)
			return $this->wrapper->getBlogPostSummary($blogInfo, $blogPostRow, $length);

deb($blogInfo, $blogPostRow);
		return $this->shortenSummary($blogPostRow['post_content'], 250);
	}
	
	/**********************************************************************/
	// Create Blog Post HTML - this class has this function in here
	// rather than in local.class
	/**********************************************************************/
	function createPostHTML($row, $layout, $blogInfo, $parent_site = ''){
		if(!$parent_site) $parent_site = '6qube.com';
		
		if($blogInfo['domain']){
	
				$postUrl = $blogInfo['domain'].'/'.$row['category_url'].'/'.$row['post_title_url'].'/';	

		} else {
			$postUrl = "#";
		}
		$keywords = explode(",", $row['tags']);
		
		if($layout==1 || $layout==2 || $layout==5){ //1 = main blogs.6qube.com layout
								//2 = search/browse layout
								//3 = blog's main site layout
				
			if($row['photo']=="") {
				$icon = 'http://'.$parent_site.'/img/no_image.jpg';//6qube_cube.png';
			}
			else {
				if($row['created'] > '2016-01-01 00:00:00') $icon = '/users/'.$row['photo'];
				else $icon = '/users/'.UserModel::GetUserSubDirectory($blogInfo['user_id']).'/blog_post/'.$row['photo'];
			}
			$created = date("F jS, Y", strtotime($row['publish_date_str']));
			$createdMo = date("M", strtotime($row['publish_date_str']));
			$createdDay = date("j", strtotime($row['publish_date_str']));
			
			$html = '<!--Start Blog Div-->';
			if($layout==2){ $html .= '<span class="blog_date"></span>'; }
			if($layout==1 || $layout==5){ $html .= '<span class="blog_date">'.$createdMo.'<br />'.$createdDay.'</span>'; }
			$html .=	'<div class="blog_div">
						<div class="blog_heading">
							<h3><a href="'.$postUrl.'" title="Read blog post '.$row['post_title'].' about '.$keywords[0].' at '.$blogInfo['blog_title'].'">'.$row['post_title'].'</a></h3>';
			$html .=		'</div><!--End Blog Heading-->';
			if($layout==5){ $html .= ''; }
			else $html .=	'<br clear="all" />';
			$html .=	'<div class="blog_title_border">
							<a href="'.$postUrl.'"><img src="'.$icon.' " class="bloglistimg" alt="" width="175px" /></a>
							<p>'. $this->getBlogPostSummary($blogInfo, $row, 250)
								// $this->shortenSummary($row['post_content'], 250)
								 .'  <a href="'.$postUrl.'" title="Read blog post about '.$row['post_title'].' on '.$blogInfo['blog_title'].'">Read More</a></p>
							<p class="stamp">Posted on <a href="'.$blogInfo['domain'].'">'.$blogInfo['blog_title'].'</a> on '.$created.'</p>
							<div class="view-listing">
								<a href="'.$postUrl.'" title=""><span class="listingImg"></span></a>
							</div>';
			if($layout==5){ $html .= '<div class="social">
						<div style="float:left; height:26px; margin-top:-5px;">
                             <g:plusone href="'.$postUrl.'" size="medium" ></g:plusone>
                        </div>
						
						<div style="float:left; height:26px;">
                              <fb:like layout="button_count" style="vertical-align: top;" href="'.$postUrl.'" "show_faces="false" send="false" width="50"></fb:like>
                        </div>
						
						<div style="float:left; height:26px;">
                              <a href="http://twitter.com/share" class="twitter-share-button" data-url="'.$postUrl.'" data-text="'.$row['post_title'].'" data-count="horizontal" data-via="">Tweet</a>
                              <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                        </div>
						
						<div style="float:left; height:26px; margin-top:-10px;">
                             <script type="text/javascript" src="http://platform.linkedin.com/in.js"></script>
                              <script type="in/share" data-url="'.$postUrl.'" data-counter="right"></script>
                        </div>
						
						</div>'; }
			$html .=		'</div><!--End Blog Title Border-->
					</div>
					<!--End Blog Div-->
					<div class="clear"></div><br />';
		} // end if($layout==1 || $layout==2)
		else {	
			$html = '<li><a href="'.$postUrl.'">'.$this->shortenTitle($row['post_title'], 75).'</a></li>';
		}
		
		return $html;
	}
			 
	//displays $limit amount of blog posts that match $category and $blog_id
	function displaySelectedCategories($blog_id, $category, $limit = '', $offset = 0){
		$category = $this->sanitizeInput($category);
		$blogInfo = $this->fetchArray($this->getBlogs(NULL, 1, $blog_id, NULL, NULL, true));
		$query = "SELECT *, if(publish_date=0, created, publish_date) as publish_date_str FROM blog_post WHERE blog_id = {$blog_id} AND (category = '{$category}'";
		if(strpos($category, "and")){
			$query .= " OR category = '".str_replace("and", "&", $category)."'";
		}
		$query .= ") AND blog_post.trashed = '0000-00-00' AND ( publish_date = 0 OR publish_date <= DATE(NOW()) )";
		$query .= " ORDER BY created DESC";
		if($limit) $query .= " LIMIT {$limit}";
		if($offset) $query .= " OFFSET {$offset}";
		if($result = $this->query($query)){
			while($row = $this->fetchArray($result)){
				$html .= $this->createPostHTML($row, 1, $blogInfo);	
			}
			echo $html; 
		}
		else return false;
	}
	
	//displays $limit amount of blog posts that match $keyword and $blog_id
	function displaySelectedKeyword($blog_id, $keyword, $limit = '', $offset = 0){
		//$category = $this->decodeUrl($category);
		$blogInfo = $this->fetchArray($this->getBlogs(NULL, 1, $blog_id, NULL, NULL, true));
		$query = "SELECT * FROM blog_post WHERE tags LIKE '%{$keyword}%' AND blog_id = {$blog_id}";
		$query .= " AND trashed = '0000-00-00' ";
		$query .= " ORDER BY created DESC";
		if($limit) $query .= " LIMIT {$limit}";
		if($offset) $query .= " OFFSET {$offset}";
		$result = $this->query($query);
		while($row = $this->fetchArray($result)){
			$html .= $this->createPostHTML($row, 1, $blogInfo);	
		}
		echo $html; 
	}
		 
	function displayPostCategories($blog_id, $domain, $limit = '', $layout = '', $postsV2 = ''){
		if(!is_numeric($blog_id)) $blog_id = 0;
		$query = "SELECT * FROM blog_post WHERE blog_id = {$blog_id}";
		$query .= " AND trashed = '0000-00-00' and publish_date <= date(NOW()) ";
		$query .= " ORDER BY category ASC, created DESC";
		$result = $this->query($query);
		if($result){
			$notfirst = 0;
			$cats = array();
			while($row = $this->fetchArray($result)){
				$present = strtolower($row['category']);
				$cats[$row['category']]++;
				if($postsV2){
					$catUrl = $domain.'/'.$row['category_url'].'/';
					$postUrl = $domain.'/'.$row['category_url'].'/'.$row['post_title_url'].'/';
				} else {
					$catUrl = $domain."/".$blog_id."-".$this->convertKeywordUrl($row['category']);
					$postUrl = $domain."/".self::convertCategory($row['category']).
							 $this->convertPressUrl($row['post_title'], $row['id']);
				}
				if($layout==''){
					if($present!=$past) echo '<h3 class="blog_cat"><a href="'.$catUrl.'">'.$row['category'].'</a></h3>';
					echo '<p class="blog_p"><a href="'.$postUrl.'">'.$row['post_title'].'</a></p>';
				}
				else if($layout=='5'){
					if($cats[$row['category']]<=$limit){
						if($present!=$past) echo '<h3 class="blog_cat"><a href="'.$catUrl.'">'.$row['category'].'</a></h3>';
						echo '<p class="blog_p"><a href="'.$postUrl.'">'.$row['post_title'].'</a></p>';
					}
				}
				else {
					if($cats[$row['category']]<=$limit){
						if($present!=$past){
							if($notfirst) echo '</li>';
							echo '<li><a href="'.$catUrl.'">'.$row['category'].'</a>';
						}
						echo '<ul><li><a href="'.$postUrl.'">'.$row['post_title'].'</a></p></li></ul>';
					}
				}
				$past = $present;
				$notfirst = 1;
			}
		}
	}
		
	/**********************************************************************/
	// function: Display Cities
	// displays cities for Local pages, organized by most active
	//
	// Accepted Input: Limit (optional)
	/**********************************************************************/
	function displayCitiesBlogs($limit = '', $domain = '', $reseller = ''){
		if(!$domain) $domain = 'blogs.6qube.com';
		
		$query = "SELECT id, COUNT(type), city, state FROM blogs";
		$query .= " WHERE blogs.trashed = '0000-00-00' ";
		$query .= " AND category != '' 
				  AND domain != '' 
				  AND city != '' 
				  AND state != '' 
				  AND id != 0
				  AND user_id != 105
				  AND '1' IN (SELECT network FROM blog_post WHERE blog_id = blogs.id)";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		$query .= " GROUP BY city, state";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY city ASC";
		
		$result = $this->query($query);
		if($result && $this->numRows($result)){
			while($row = $this->fetchArray($result)){
				$html .= $this->getCityLink($row['city'], $row['state'], $domain, true);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit) $html .= ' <a href="http://'.$domain.'/cities/">more cities...</a> ';
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No cities found.";
		 
	}
	 
	/**********************************************************************/
	// function: Display Categories
	// displays categories for Local pages, organized by most active
	//
	// Accepted Input: city, state, Limit (optional)
	/**********************************************************************/
	function displayCategoriesBlogs($city, $state, $limit = '', $domain = '', $reseller = ''){	
		if(!$domain) $domain = 'blogs.6qube.com';
			
		$query = "SELECT id, COUNT(type), state, category FROM blogs";
		$query .= " WHERE blogs.trashed = '0000-00-00' ";
		$query .= " AND category != '' 
				  AND domain != '' 
				  AND city != '' 
				  AND state != '' 
				  AND id != 0
				  AND user_id != 105
				  AND '1' IN (SELECT network FROM blog_post WHERE blog_id = blogs.id) 
				  AND city = '".$city."'
				  AND state = '".$state."'";
		if($reseller) $query .= " AND `user_parent_id` = ".$reseller;
		$query .= " GROUP BY category";
		if($limit){
			$query .= " ORDER BY COUNT(type) DESC";
			$query .= " LIMIT ".$limit;
		}
		else $query .= " ORDER BY category ASC";
		
		if($result = $this->query($query)) $numResults = $this->numRows($result);
		if($numResults){
			while($row = $this->fetchArray($result)){
				$a = explode("->", $row['category']);
				$category = $a[1];
				$html .= $this->getCatLink($city, $state, $category, $domain);
				if($limit) $html .= ' | ';
				else $html .= '<br />';
			}
			if($limit && $numResults>=$limit) 
				$html .= ' <a href="http://'.$domain.'/'.$this->convertKeywordUrl($city).'categories/">more categories...</a> ';
			else
				$html = substr($html, 0, count($html)-3); //remove trailing |
			
			echo $html.'<br /><br />';
		}
		else 
			echo "No categories found.";
	}
	
	//check if a theme is V2 Friendly
	function isThemeV2($themeID){
		if(!is_numeric($themeID)) $themeID = 0; //validate input
		$result = $this->queryFetch("SELECT v2_blog_support FROM themes WHERE id = '".$themeID."'", NULL, 1);
		if($result['v2_blog_support']==1) return true;
		else return false;
	}
		 
	function getBlogRows(){ return $this->blog_rows; }
	function getPostRows(){ return $this->post_rows; }
}
?>
