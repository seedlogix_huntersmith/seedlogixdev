<?php
	//DB connections
	require_once('/home/sapphire/public_html/corfro/6qube/inc/db_connector.php');
	$connector = new DbConnector();
	
	$query = "SELECT * FROM wp_posts WHERE post_type = 'post' ORDER BY post_date DESC LIMIT 3";
	$result = $connector->query($query, 'sapphire_6qubeblog');
	
	while($row = $connector->fetchArray($result)) {
		echo '<h3 class="blog-title"><a href="'.$row['guid'].'">'.$row['post_title'].'</a></h3><p class="blog-post">'.ShortenText(strip_tags($row['post_content'])).' <a href="'.$row['guid'].'">[read more]</a></p>';
	}
	
	function ShortenText($text) { 

        // Change to the number of characters you want to display 
        $chars = 50; 

        $text = $text." "; 
        $text = substr($text,0,$chars); 
        $text = substr($text,0,strrpos($text,' ')); 
        $text = $text."..."; 

        return $text; 

    } 
?>