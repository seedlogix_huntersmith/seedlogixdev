<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/reports.class.php');
	
	//create new instance of class
	$reports = new Reports();
	$numProspects = $reports->getProspects(NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, $_SESSION['parent_id']);
	$offset = $_GET['offset'] ? $_GET['offset'] : 0;

	//content
	echo '<h1>Contact Form Leads</h1>';
	echo '<p>'.$numProspects.' Leads</p><br /><br />';
	echo $reports->displayContactFormLeads(NULL, NULL, NULL, 2, $offset, 'lead-reports.php', NULL, NULL, NULL, $_SESSION['parent_id']);
	echo '<br /><p>&nbsp;</p>';
	
?>