<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/press.class.php');
	$press = new Press();

	if($_SESSION['theme']) $previewUrl = 'http://press.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://press.6qube.com/';
	
	$press_count = 0;
	//$press_limit = $press->getLimits($_SESSION['user_id'], "press");
	$press_limit = $_SESSION['user_limits']['press_limit'];

	$result = $press->getPresss($_SESSION['user_id']); //primes Press results, and is needed for getPressRows()
		if($press_count = $press->getPressRows()) { //checks to see if the user has any press releases
			if($press_limit==-1) $remaining = "Unlimited";
			else $remaining = $press_limit - $press_count;
			echo '<div id="dash"><div id="fullpage_press" class="dash-container"><div class="container">';
			while($row = $press->fetchArray($result)){ //if so loop through and display links to each Press
				$tracking = $row['tracking_id'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'press.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="press.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=press_release&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$press->ANAL_getGraphImg('press', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="press.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Press Releases remaining.';
			if(!$_SESSION['theme']){
				if(($press_limit-$press_count)<=0 && $press_limit!=-1) echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
		}
		else{ //if no press releases
			if($press_limit==-1) $press_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_press" class="dash-container"><div class="container">';

			if($press_limit==0 && $press_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Press Releases remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Press Release.  You have <strong>'.$press_limit.'</strong> remaining.';

			echo '</div></div></div>';
		}
?>
