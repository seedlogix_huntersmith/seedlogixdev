<?
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/hub.class.php');
	
	//create new instance of class
	$hub = new Hub();
	
	//content
	echo '<h1>Analytics</h1>';
	echo $hub->userHubsDropDown($_SESSION['user_id'], 'hub_analytics_view', $_SESSION['campaign_id'], 3);
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop-down above to select which hub's analytics to view.</p>
</div>
