<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

$period = isset($_GET['period']) ? $_GET['period'] : 0;
?>
<h1>Reseller Credits</h1><br />

<? $reseller->displayCredits($_SESSION['login_uid'], $period); ?>

<br style="clear:both;" />