<?php
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//require db connection
require_once('inc/domains.class.php');
$d = new Domains();

//validate domain string
$domain = $d->validateDomain($_POST['domain'], 1, 1, 0);
$account = $_POST['account'];
$accountArray = explode('@', $account);
$account = $accountArray[0];
if(!is_array($domain)){ //if there are no validation errors, $domain is a string

	//first check if user is authorized (if the domain is for a hub that matches their UID)
	$query = "SELECT count(id) FROM hub WHERE domain LIKE '%".$domain."' AND user_id = '".$_SESSION['user_id']."'";
	$check1 = $d->queryFetch($query);
	if($check1['count(id)']){
		$continue = true;
	}
	else {
		//if no hub domain, check if the domain is for a blog with their UID
		$query = "SELECT count(id) FROM blogs WHERE domain LIKE '%".$domain."' AND user_id = '".$_SESSION['user_id']."'";
		$check2 = $d->queryFetch($query);
		if($check2['count(id)']) $continue = true;
	}
	
	if($continue){
		//if domain is valid, attempt to reset password
		if($_POST['lincoln']){
			//do it over on the lincoln server
			$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
			$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
			$secHash = md5($domain.$salt);
			$secHash = md5($secHash.'resetPass'.$pepper);
			//makes the hash only good if it was submitted today:
			$secHash = md5($secHash.date('d'));
			$data = file_get_contents('http://50.97.78.210/secure/setup_email.php?domain='.$domain.'&mode=resetPass&acct='.$account.'&hash='.$secHash);
			$reset = json_decode($data, true);
		} else {
			$reset = $d->resetEmailPassword($account, $domain);
		}
		if($reset['success']){
			$return['success'] = 1;
			$return['message'] = $reset['message'];
		}
		else {
			$return['success'] = 0;
			$return['message'] = 'Error resetting the password.';
		}
	}
	else {
		$return['success'] = 0;
		$return['message'] = 'Error validating domain ownership.';
	}
	
} //end if(!is_array($domain))
else {
	$return['success'] = 0;
	$return['message'] = 'Error validating the domain: '.$domain['message'];
}

echo json_encode($return);
?>