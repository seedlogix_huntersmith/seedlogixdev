<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/hub.class.php');

//create new instance of class
$connector = new Hub();
$offset = $_GET['offset'] ? $_GET['offset'] : 0;
if($_SESSION['reseller']) $reseller_site = $_SESSION['main_site_id'];
?>
<?
$forms = $connector->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id']);
if($forms && is_array($forms)){
?>
<script type="text/javascript">
$('#formResponsesSelect').die();
$('#formResponsesSelect').live('change', function(){
	var formID = $(this).attr('value');
	if(formID){
		$('#wait').show();
		$.get('inc/forms/view-leads.php?mode=all&formID='+formID<? if($_GET['reseller_site']) echo "+'&reseller_site=1'"; ?>, function(data){
			$("#ajax-load").html(data);
			$('#wait').hide();
		});
	}
});
$('.hideFormsResponses').die();
$('.hideFormsResponses').live('click', function(){
	var currState = $('#currHideFormsState').html();
	if(currState=='hide'){
		$('#formResponsesDiv').slideUp();
		$('#currHideFormsState').html('show');
	}
	else {
		$('#formResponsesDiv').slideDown();
		$('#currHideFormsState').html('hide');
	}
});
$(function(){
	$(".uniformselect").uniform();
});
</script>
<h1 style="margin-bottom:2px;">Custom Form Responses</h1>
<p><a href="#" class="hideFormsResponses dflt-link">Click here to <span id="currHideFormsState" style="font-weight:bold;">hide</span> this section</a></p>
<div id="formResponsesDiv">
<p style="margin-top:6px;">Choose a form below to view all of the responses.</p>
<select id="formResponsesSelect" class="uniformselect no-upload">
	<option value="">Choose a form...</option>
	<?
	foreach($forms as $key=>$value){
		echo '<option value="'.$key.'">'.$value['name'].'</option>';
	}
	?>
</select>
Latest 5 responses from all forms:<br />
<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
	<li class="email" style="width:240px;"><strong>Prospect</strong></li>
	<li class="email" style="width:150px;"><strong>Form Name</strong></li>
	<li class="email" style="width:160px;"><strong>Hub Name</strong></li>
	<li class="phone" style="width:90px;"><strong>Date</strong></li>
</ul>
<?
	$hubID = $_GET['reseller_site'] ? $reseller_site : '';
	$leads = $connector->getCustomFormLeads($_SESSION['user_id'], NULL, NULL, 5, $_SESSION['campaign_id'], $hubID);
	if($leads && $connector->numRows($leads)){
		while($row = $connector->fetchArray($leads, NULL, 1)){
			//get form data
			$query = "SELECT name, data FROM lead_forms WHERE id = '".$row['lead_form_id']."'";
			$form = $connector->queryFetch($query, NULL, 1);
			//get hub data
			if($row['hub_id']){
				$query = "SELECT name FROM hub WHERE id = '".$row['hub_id']."'";
				$hubInfo = $connector->queryFetch($query, NULL, 1);
				$hubName = $hubInfo['name'];
			}
			$leadData = explode('[==]', $row['data']);
			$b = explode('|-|', $leadData[0]);
			$dispTitle = $b[1].': '.$b[2];
			$timestamp = date('n/j/Y', strtotime($row['created']));//.' at '.date('g:ia', strtotime($row['created']));
?>
	<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
		<li class="name" style="width:220px;"><?=$dispTitle?></li>
		<li class="email" style="width:150px;"><i><?=$form['name']?></i></li>
		<li class="email" style="width:160px;"><i><?=$hubName?></i></li>
		<li class="phone" style="width:90px;"><i><?=$timestamp?></i></li>
		<li class="details" style="width:100px;"><a href="inc/forms/view-leads.php?mode=single&leadID=<?=$row['id']?>">View Details</a></li>
		<li class="delete"><a href="#" class="delete rmParent" rel="table=leads&id=<?=$row['id']?>&undoLink=hub-forms"><span>Delete</span></a></li>
	</ul>
<?
		}
	} else echo '<b>No form responses to display.</b>';
?>
</div>
<br style="clear:both;" />
<? } //end if($forms && is_array($forms)) ?>
<h1>Prospects</h1>
<p>Click a person's email address to contact them using our Prospect Emailer.</p><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'hub', NULL, 40, $offset, 'hub-prospects.php', $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'hub', NULL, 40, $offset, 'hub-prospects.php', NULL, $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
}
?>
<br />
<br style="clear:both;" />