<?php
	error_reporting(E_ALL);
	require_once(dirname(__FILE__) . '6qube/core.php');
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//DB connections
	require_once('inc/db_connector.php');
	$connector = new DbConnector();
	
	if($_SESSION['admin']){
		if($_POST){
			$data = unserialize(base64_decode($_POST['data']));
			var_dump($data);
		}
		else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Restore a permanently deleted item</title>
</head>

<body>
<strong>Instructions:</strong><br />
1. Log in to the FTP server and navigate to /home/sapphire/6qube_backups/delete-bckp/<br />
2. Locate the item you want to restore, which will be named like hub_page2-83594_uid-6451.txt<br />
3. Open the file and paste its contents below, then click Submit
<br /><br />
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<textarea name="data" style="width:700px;height:500px;"></textarea><br />
	<input type="submit" name="submit" value=" SUBMIT " style="height:50px; width:100px;" />
</form>
</body>
</html>

<?
		}
	}
?>