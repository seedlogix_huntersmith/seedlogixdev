<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/articles.class.php');
	$article = new Articles();
	
	$newArticle = $article->validateText($_POST['name'], 'Press Name'); //validate directory name
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	if($article->foundErrors()){ //check if there were validation errors
		//$error = $article->listErrors('<br />'); //list the errors
	}
	else{ //if no errors
		$message = $article->addNewArticles($_SESSION['user_id'], $newArticle, $_SESSION['campaign_id'], 
									$_SESSION['user_limits']['article_limit'], NULL, $parent_id);
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_article .container';
		$response['success']['message'] = $message;
	}

	echo json_encode($response);
	
?>