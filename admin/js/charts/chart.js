function createDashBoard (datemin, datemax, yMin, yMax, visits, leads) {
   
//	var yMax = Math.max.apply(null, visits);	
	
    var plot = $.plot($(".chart"),
           [ { data: visits, label: "Total Visits"}, { data: leads, label: "Total Leads"} ], {
               series: {
                   lines: { show: true },
                   points: { show: true }
               },
               grid: { hoverable: true, clickable: false },
               yaxis: { min: yMin, max: yMax },
			   xaxis: { 
			   	mode: "time",
			   	timeformat: "%y/%m/%d",
                                minTickSize: [1, "day"],
			   	min: datemin,//(new Date("2012/07/01")).getTime(),
                max: datemax}//(new Date("2012/07/08")).getTime() }
             });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip" class="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
			'z-index': '9999',
			'color': '#fff',
			'font-size': '11px',
            opacity: 0.8
        }).appendTo("body").fadeIn(200);
    }
/*
    var previousPoint = null;
    $(".chart").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if ($(".chart").length > 0) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    
                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);
                    
                    showTooltip(item.pageX, item.pageY,
                                item.series.label + " of " + x + " = " + y);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;            
            }
        }
    });

    $(".chart").bind("plotclick", function (event, pos, item) {
        if (item) {
            $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
            plot.highlight(item.series, item.datapoint);
        }
    });
    */
};
