// JavaScript Document
$(document).ready(function() {
	//############################################
	// TEXT INPUT AUTO SUBMIT
	//############################################
	$(".jquery_form input, .jquery_form select, .jquery_form textarea").blur(function() {
		$elm = $(this);
		$elm.removeClass("error");
		$elm.removeClass("approved");
		
		params = {
			name: $(this).attr('name'),
			value: $(this).attr('value'),
			form: $(this).parent().parent().parent().attr('name'),
			id: $('.jquery_form').attr('id'),
			"class": $elm.attr('class')
		};
		
		$.getJSON("upload_form.php", params, function(data){
			if(data.error === 0) {
				$elm.find(' + p').html('');
				$elm.addClass("approved");
			}
			else {
				$elm.find(' + p').html(data.message);
				$elm.addClass("error");
			}
		});
	});
	
	//############################################
	// FILE INPUT AUTO SUBMIT
	//############################################
	$('.test_file').change(function(){
		$elm = $(this).parent().attr('id');
		$theForm = $(this).parent();
		$theForm.submit();
		$('#uploadIFrame').load(function(){
			eval("data = "+$(this).contents().text());

			if(data.result == 'true'){ 
				$("#"+$elm+" + img")
				.attr('src', data.file)
				.addClass('pic');
			}
			else{
				alert(data.msg);	
			}
		});
	});
	
	//############################################
	// DELETE ITEM
	//############################################
	$('.delete').live('click', function(){
		$params = $(this).attr('rel'); //get delete parameters
		$container_div = $(this).parent().parent().parent().parent().attr('id'); //get parent div
		$test = "#"+$container_div;
		$.get("delete_item.php?"+$params+"&section="+$container_div, function(data){	//load undo box
			$("#message_cnt").html(data);
			$.get($container_div+".php", function(data){	//load new dash section
				$($test).html(data);
				showCodaBubble();
			});
		}); 
	});
	//############################################
	// UNDO DELETE
	//############################################
	$('.undo').live('click', function() {
  		$params = $(this).attr('id');
  		$container_div = $(this).attr('title');
  		$.get("delete_undo.php?id="+$params, function(){
  			$.get($container_div+".php", function(data){
				$("#"+$container_div).html(data);
				showCodaBubble();
		  		$("#undo-test").fadeOut("slow");
			});
		});
	});
	
	function showCodaBubble(){
		$('.coda_bubble').codaBubble({ //re-activate coda bubbles for newly loaded div
	      distances : [-20,-20,-20],
	      leftShifts : [-20,-20,-20],
	      bubbleTimes : [400,400,400],
	      hideDelays : [0,0,0],
	      bubbleWidths : [200,200,200],
	      bubbleImagesPath : "coda_bubble/images/skins/classic",
	      msieFix : true
	   });
	}
	
//close $(document).ready(function()
});