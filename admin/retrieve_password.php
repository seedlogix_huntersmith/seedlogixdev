<?php
	//iniate the page class
	require_once('inc/local.class.php');
	$connector = new Local();
	
	$username = $connector->validateText($_POST['username']);
	$parent_id = is_numeric($_POST['parent_id']) ? $_POST['parent_id'] : '';
	$parent_site = $_POST['parent_site'];
	
	$query = "SELECT id, password, super_user FROM users WHERE username = '".$username."'";
	if($parent_id) $query .= " AND (parent_id = '".$parent_id."' OR id = '".$parent_id."')";
	$row = $connector->queryFetch($query);
	$userID = $row['id'];
	
	if($row['super_user']==0){
		if($row && !$connector->foundErrors()){
			require_once 'Mail.php';
			$host = "relay.jangosmtp.net";
			$port = '25';
			$smtp = Mail::factory('smtp',
						array ('host' => $host,
							  'port' => $port, 
							  'auth' => false));
						  
			$to = $username; 
			$from = "<no-reply@".$parent_site.">";
			$subject = "Your ".$parent_site." password";
			$salt = '?v8jfFFFGMcxckOMFG!';
			$validation = md5($userID.$salt.$username);
			$link = 'http://'.$parent_site.'/admin/password-reset.php?email='.$username.'&x='.$validation;
			if($parent_id) $link .= "&r=".$parent_id;
			
			//html message
			//$message = '<html><body>';
			$message .= '<h1>Reset Password</h1><br />';
			$message .= '<a href="'.$link.'" target="_blank">Click here to reset your password</a>.';
			//$message .= '</body></html>';
		
			
			$headers = array ('From' => $from,
						   'Reply-To' => $from,
						   'To' => $to,
						   'Subject' => $subject,
						   'Content-Type' => 'text/html; charset=ISO-8859-1',
						   'MIME-Version' => '1.0');
						   
			$mail = $smtp->send($to, $headers, $message);
			if(!PEAR::isError($mail))
				header("Location: login.php?action=password");
			else
				header("Location: login.php?action=password-error&msg=".urlencode($mail->getMessage()));
		} //end if($row && !$connector->foundErrors())
		else
			header("Location: login.php?action=username-error");
	} //end if($row['super_user']==0)
	else 
		header("Location: login.php?action=admin-account");
?>