<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/articles.class.php');
	
	//create new instance of class
	$connector = new Articles();
	
	//content
	echo '<h1>Analytics</h1>';
	echo $connector->userArticlesDropDown($_SESSION['user_id'], 'articles_analytics_view');
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop-down above to select which article's analytics to view.</p>
</div>
