<?
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/blogs.class.php');

//create new instance of class
$connector = new Blog();
$_SESSION['autoresponder_section'] = "blogs";

//check limits
if($_SESSION['user_limits']['responder_limit']==-1)
	$respondersRemaining = "Unlimited";
else {
	//get number of directory autoresponders for this campaign
	$numResponders = $connector->getAutoResponders(NULL, 'blogs', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
	$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
	if($respondersRemaining<0) $respondersRemaining = 0;
}
$emailsRemaining = $connector->emailsRemaining($_SESSION['user_id']);
if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
else $emailsRemaining = number_format($emailsRemaining);

$thisPage = "blogauto";
include('./inc/forms/auto_nav.php');

//content
echo '<h1>Blog Auto Responders</h1>';
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input[type=file]").uniform();
});
</script>
<?php
	if($_GET['id']){
		include('./inc/forms/blog_autoresponder.php');
	}
	else {
		echo $connector->userBlogsDropDown($_SESSION['user_id'], 'blog_autoresponder', $_SESSION['campaign_id']);
?>
<br style="clear:both;" />
<div id="email-settings" style="width:100%;">
	<div id="ajax-select">
		<p>Use the drop-down above to select which blog you'd like to configure auto responders for.</p><br />
		<p>With this optional setting you can create scheduled auto-response emails to be sent to prospects<br />after they fill out the contact form on your blog.</p><br /><br />
		<p>You have <strong><?=$respondersRemaining?></strong> auto-responders remaining for this campaign.</p><br />
		<p>You have <strong><?=$emailsRemaining?></strong> outgoing emails remaining for this month.</p>
	</div>
</div>
<? } ?>
<br style="clear:both;" /><br />