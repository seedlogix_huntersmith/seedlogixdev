<?php


if(!$isCron) require_once('inc/auth.php');

if($isInclude){
	
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$connector = new Hub();
	
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
	$port = '25';
	/** @var Mail_smtp $smtp */
	$smtp = Mail::factory('smtp',
				array ('host' => $host,
					  'port' => $port, 
					  'auth' => false));
	require_once QUBEADMIN . 'models/LeadData.php';
	
	if($emailBody=="") {
		$data['error'] = 1;
		$data['message'] = 'Message field cannot be blank.';
	}
	else {
		//check to see if user is emailing all users
		if(strpbrk($leadID, ",")){ //if $userID has a comma
			//trim it first
			$leadID = trim($leadID, ',');
			$leadID2 = explode(",", $leadID); //turn it into an array
			$numIDs = count($leadID2);
		}
deb($leadID2);		
		//set up for() loop 
		if($leadID2){ //if user is emailing all users
			$iterations = $numIDs; //set number of times to loop through email script
		} else if($userID){
			$iterations = 1; //set number of times to loop through email script
			$leadID2[0] = $leadID; //add the single ID to an array at key 0 to work with loop
		}
deb($leadID2);
		$leadsSentNow = '';
		$errorNum = 0;
		$emailsSent = 0;
		$duplicates = 0;
		$emails = array();
		$errors = array();
		//get sending company's info
		if(!$customFrom){
			if(!$isCron){
				$query = "SELECT company FROM user_info WHERE user_id = '".$_SESSION['user_id']."'";
				$fromEmail = $_SESSION['user'];
			}
			else {
				$query = "SELECT company FROM user_info WHERE user_info.user_id = (SELECT lead_forms_emailers.user_id FROM lead_forms_emailers WHERE lead_forms_emailers.id = '".$emailerID."')";
				$query2 = "SELECT username FROM users WHERE id = (SELECT user_id FROM lead_forms_emailers WHERE lead_forms_emailers.id = '".$emailerID."')";
				$userInfo2 = $connector->queryFetch($query, NULL, 1);
				$fromEmail = $userInfo2['username'];
			}
			$userInfo = $connector->queryFetch($query, NULL, 1);
			$fromCompany = $userInfo['company'];
		}
		
		for($i=0; $i<$iterations; $i++){ //loop through email script $iterations number of times
			
			//get name and email from lead data
			$leadInfo = $connector->leadInfoFromData(NULL, $leadID2[$i]);
			
			//make sure contact hasn't been emailed already during this run
			if(!in_array(strtolower($leadInfo['email']), $emails)){
				
				//set up email to send
				if($leadInfo['name']) $sendTo = '"'.$leadInfo['name'].'" <'.$leadInfo['email'].'>';
				else $sendTo = $leadInfo['email'];
				if(!$customFrom) $sendFrom = '"'.$fromCompany.'" <'.$fromEmail.'>';
				else $sendFrom = $customFrom;
				$subject = str_replace("#name", $leadInfo['name'], $emailSubject);
				$subject = stripslashes($subject);
				$body = str_replace("#name", $leadInfo['name'], $emailBody);
				$leadData = LeadData::getData(Qube::GetDriver(), $leadID2[$i]);
				$body = preg_replace_callback('/\[\[([^\]]+)\]\]/', function($matches) use ($leadData){
					if(isset($leadData[$matches[1]]))
						return $leadData[$matches[1]]['value'];
					return $matches[0];
				}, $body);
				$body = stripslashes($body);
				$leadDET = LeadModel::Fetch('id = %d', $leadID2[$i]);
				$main_site = ResellerModel::Fetch('admin_user = %d', $leadDET->parent_id);
				$body .= '<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0" cellspacing="0" align="center" width="660">
						  <tbody>
							<tr>
							  <td valign="top" align="left"><p><br />
								  TO UNSUBSCRIBE: This email was sent to you because you filled out our contact form.
								   To remove yourself from our email program, please <a href="http://'.$main_site->main_site.'/?page=client-optout&email='.$leadInfo['email'].'&pid='.$leadID2[$i].'" target="_blank" style="color:#666666; text-decoration:underline;" >click here to opt-out.</a></p></td>
							</tr>
						  </tbody>
						</table>';
				$headers = array('From' => $sendFrom,
							  'To' => $sendTo,
							  'Subject' => $subject,
							  'Content-Type' => 'text/html; charset=ISO-8859-1',
							  'MIME-Version' => '1.0');
				if(!$customFrom) $headers['Reply-To'] = $fromEmail;
				else $headers['Reply-To'] = $customFrom;
				//send
print_r($leadInfo);
				if($leadInfo['email']) $mailed = $smtp->send($sendTo, $headers, $body);
				
				//error check
				if($mailed && !PEAR::isError($mailed)){
					$emailsSent++;
					$emails[$emailsSent] = strtolower($leadInfo['email']); //add email to array of emails
					
					//update string of users_sent
					$leadsSent .= '::'.$leadID2[$i];
					$leadsSentNow .= '::'.$leadID2[$i];
					
					//update the user's number of emails sent
					$query = "UPDATE users SET emails_sent = emails_sent+1 WHERE id = '".$_SESSION['user_id']."' LIMIT 1";
					$connector->query($query);
				} else {
					$data['error'] = 1;
					$errors[$errorNum] = "Lead ID: ".$leadID2[$i]."\nEmail: ".$leadInfo['email'];
					if($mailed) $errors[$errorNum] .= "\nReason:\n".addslashes($mailed->getMessage());
					$errorNum++;
				}
				
			} //end if(!in_array($row['email'], $emails))
			else { $duplicates++; }
			
			//clear vars for next iteration
			$query = $leadInfo = $sendTo = $subject = $body = $headers = $mailed = NULL;
		} //end for() loop
		if($isCron){
			//update users_sent field with new string
			$query = "UPDATE lead_forms_emailers SET leads_sent = '".$leadsSent."' WHERE id = '".$emailerID."'";
			$connector->query($query);
		}
		//add it to the history table
		//get whole row
		if($emailsSentNow && $emailsSentNow>1){
			$query = "SELECT * FROM lead_forms_emailers WHERE id = '".$emailerID."'";
			$email = $connector->queryFetch($query);
			$query = "INSERT INTO lead_forms_emailers_history 
					(user_id, name, from_field, subject, body, sent_to, contact, anti_spam)
					VALUES
					('".$email['user_id']."', '".addslashes($email['name'])."', '".addslashes($email['from_field'])."', 
					'".addslashes($email['subject'])."', '".addslashes($email['body'])."', '".trim($leadsSentNow, '::')."', 
					'".addslashes($email['contact'])."', '".addslashes($email['anti_spam'])."')";
			$connector->query($query);
		}
	
		if($data['error']==1){
			$data['message'] = "One or more emails were not sent.  Detailed info:\n";
			for($j=0; $j<count($errors); $j++){
				$data['message'] .= $errors[$j]."\n\n";
			}
			//email errors to user if cron
			//if($isCron){
				//$to = $customFrom ? $customFrom : $fromEmail;
				//$subject = "Scheduled lead emailer results: Error report";
				//$sendMessage = nl2br($data['message']);
				//$body =
				//"<b>The scheduled lead communication with subject '".$emailSubject."' was just run and had
				//errors:</b><br />
				//<br />".stripslashes($sendMessage);
				//$headers = array('From' => '6Qube <support@6qube.com>',
							  //'To' => array($to, 'admin@6qube.com'),
							  //'Subject' => $subject,
							  //'Content-Type' => 'text/html; charset=ISO-8859-1',
							  //'MIME-Version' => '1.0');
				//$smtp->send($to, $headers, $body);
			//}
		} else {
			$data['error'] = 0;
			$data['message'] = " " . $emailsSent." emails(s) sent successfully!";
			if($duplicates>0){
				if($duplicates==1){
					$data['message'] .= "\n".$duplicates." duplicate email was not sent";
				} else {
					$data['message'] .= "\n".$duplicates." duplicate emails were not sent";
				}
			}
		}
		
	} //end else [email body is not blank]

} //end if($_SESSION['admin'] || $_SESSION['reseller'])
else {
	$data['error'] = 1;
	$data['message'] = 'Not authorized.';
}

if(!$isInclude){
	echo json_encode($data);
}
else if(!$isCron){
	if($data['error']==1){
		echo 
		'<script type="text/javascript">alert("Error sending email(s):\n'.$data['message'].'");</script>';
	}
	else if($data['error']==0){
		echo
		'<script type="text/javascript">alert("'.$data['message'].'");</script>';
	}
}
else if($isCron){
	if($data['error']==1){
		echo 
		'Error sending email(s):<br />'.nl2br($data['message']).'<br />';
	}
	else if($data['error']==0){
		echo
		$data['message'].'<br />';
	}
}
?>
