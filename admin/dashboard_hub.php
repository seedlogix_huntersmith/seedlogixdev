<?php
//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//require db connection
	require_once('inc/hub.class.php');
	$connector = new Hub();
	
	//get page settings
	$settings = $connector->getSettings();
	
	//set page variables 
	$display_count = $settings['display_count'];
	$hub_count = 0;
	//$hub_limit = $connector->getLimits($_SESSION['user_id'], "hub");
	$hub_limit = $_SESSION['user_limits']['hub_limit'];
	if($hub_limit==-1 || $hub_limit=="") $hub_limit = "Unlimited";
	//Hub
	$result = $connector->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']);
	if($hub_count = $connector->getHubRows()){
		//$result = $connector->getHubs($_SESSION['user_id'], $display_count, NULL, $cID);
		while($row = $connector->fetchArray($result)){
			if($row['id'] != $past){
				$hub_title[] = $connector->hubDashUpdate($row['id'], $row['name'], $_SESSION['user_id'], $row['tracking_id'], 'block');
				$past = $row['id'];
			}
		}			
	}
?>

	<div class="headline">
		<h2>Hubs:</h2>
		<!--<p>You currently have <span class="bold"><?=$hub_count?></span> HUBs out of <span class="bold"><?=$hub_limit?></span> available.</p>-->
		<a href="hub.php"><p>New Hub</p></a>
	</div>
	<?
		if($hub_count){
			echo '<div class="container">';
			foreach($hub_title as $key => $value){
				echo $value;
			}
			echo '</div>';
		}
		else {
			echo '<div class="container">None.</div>';
		}
	?>