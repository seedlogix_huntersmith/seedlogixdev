<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$forms = $hub->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], NULL, NULL, NULL, 1);
?>
<? if($forms && is_array($forms)){ ?>
<script type="text/javascript">
$('#formsSelect').die();
$('#formsSelect').live('change', function(){
	var formID = $(this).attr('value');
	if(formID){
		$('#wait').show();
		$.get('inc/forms/form_emailers.php?mode=all&formID='+formID, function(data){
			$("#emailersListDiv").html(data);
			$('#wait').hide();
		});
	}
});
$(function(){
	$(".uniformselect").uniform();
});
</script>
<h1>Email Marketing</h1>
<p style="margin-top:6px;">Choose a form below to configure scheduled email broadcasts.</p>
<select id="formsSelect" class="uniformselect no-upload">
	<option value="">Choose a form...</option>
	<?
	foreach($forms as $key=>$value){
		echo '<option value="'.$key.'">'.$value['name'].'</option>';
	}
	?>
</select>
<div id="emailersListDiv">
	
</div>
<?
} //end if($forms && is_array($forms))
else {
?>
<br />
<p>You must first create a custom form with an Email Address field before you can configure an emailer.</p>
<? } ?>
<br style="clear:both;" />