<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/auth.php');
	require_once('./inc/hub.class.php');
	$hub = new Hub();
	
	$hub_id = $_GET['id'];	
	$page_results = $hub->getHubPage($hub_id, NULL, NULL, NULL, NULL, 1);

	echo '<div class="container">';
	if($hub->getPageRows()){
		$site = $_SESSION['main_site'] ? $_SESSION['main_site'] : '6qube.com';
		//loop through and out the blogs post (limits 5)
		while($page_row = $hub->fetchArray($page_results)){
			echo '<div class="item">
					<div class="icons">
						<a href="hub.php?form=editPage&id='.$page_row['id'].'" class="edit"><span>Edit</span></a>';
			if($page_row['allow_delete'] || ($_SESSION['reseller'] || $_SESSION['admin']))
			echo 		'<a href="#" class="delete" rel="table=hub_page&id='.$page_row['id'].'"><span>Delete</span></a>';
			echo		'</div>
					
					<img src="img/hub-page';
					if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
					echo	'.png" class="blogPost" />
					
					<a href="hub.php?form=editPage&id='.$page_row['id'].'">'.$page_row['page_navname'].': '.$page_row['page_title'].'</a>
				</div>';			
		}
	}
	echo '</div>';

?>