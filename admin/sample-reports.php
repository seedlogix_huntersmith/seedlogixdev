<?php session_start();


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Base ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip', 'Visits Last 7 Days', 'Visits Last 30 Days', 'Visits Last 90 Days', 'Leads'));


//include phone class
#require_once('/home/sapphire/production-code/2012-08-24/admin/inc/analytics.class.php');
require_once '/home/sapphire/production-code/dev-trunk-svn/hybrid/bootstrap.php';
require_once('/home/sapphire/production-code/dev-trunk-svn/admin/inc/analytics.class.php');
$stats = new Analytics();

//AND hub_parent_id = '7024'
//AND hub_parent_id = '8220'

$query = "SELECT id, user_id, cid, name, tracking_id, hub_parent_id, domain, company_name, phone, street, city, state, zip, created FROM hub WHERE user_parent_id = '6312'
              AND trashed = '0000-00-00'
			  AND hub_parent_id != '18953'
			  AND hub_parent_id != '23395'
			  AND hub_parent_id != '47868'
			  AND hub_parent_id != '8015'
			  AND hub_parent_id != '18951'
			  AND hub_parent_id != '18953'
			  AND hub_parent_id != '18952'
			  AND hub_parent_id != '22671'
              AND domain != '' AND tracking_id != 0
              AND cid != '99999' 
              ORDER BY name ASC";	// temp 50235
			   
$hubs = $stats->query($query);

//instantiate blog class for blog section in right panel
//get analytics info

// loop over the rows, outputting them
//fputcsv($output, array('Website ID', 'User ID', 'Tracking ID', 'Name', 'Domain', 'Phone', 'Address', 'City', 'State', 'Zip'));

while ($row = $stats->fetchArray($hubs)){

$query2 = "SELECT count(id) FROM leads WHERE hub_id = '".$row['id']."' AND spam < 50 AND created < '2015-04-12 00:00:00'";
$leads1 = $stats->queryFetch($query2);
$query3 = "SELECT count(id) FROM contact_form WHERE type_id = '".$row['id']."' AND spamstatus = '0' AND created < '2015-04-12 00:00:00'";
$leads2 = $stats->queryFetch($query3);

$numProspects = $leads1['count(id)']+$leads2['count(id)'];


if(TRUE)
{
$r0	=		$stats->ANAL_getViewCountEmailReports($row['tracking_id'], '2015-04-06,2015-04-12');
//$r1	=		'';
$r1	=		$stats->ANAL_getViewCountEmailReports($row['tracking_id'], '2015-03-12,2015-04-12');
//$r2	=		'';
$r2	=		$stats->ANAL_getViewCountEmailReports($row['tracking_id'], '2015-01-12,2015-04-12');
}else{
	$r0	=	$r1	=	$r2	=	'test';
}
 	
fputcsv($output, array($row['id'], $row['user_id'], $row['tracking_id'], $row['hub_parent_id'], $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'], 
	$r0,
	$r1,
	$r2,
	$numProspects));
flush();
//fputcsv($output, array($row['id'], $row['user_id'], $row['cid'], $row['company_name'], $row['domain'], $row['phone'], $row['street'], $row['city'], $row['state'], $row['zip'], NULL, NULL, NULL, $numProspects));

}
