<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//check user out
	require_once('inc/local.class.php');
	$local = new Local();
	$local->onlineNow($_SESSION['login_uid'], 'delete');
	
	session_destroy();
	
	if($_GET['redir']){
		header("Location: ".$_GET['redir']);
	} else {
		header("Location: login.php");
	}
?>