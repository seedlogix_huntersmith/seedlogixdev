<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once(QUBEADMIN . 'inc/auth.php');
$multiUser = $_GET['multi_user'] ? 1 : 0;
if($_GET['mode']=='edit'){
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	include_once(QUBEADMIN . 'inc/forms/edit-custom-form.php');
}
else if($_GET['mode']=='editField'){
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$fieldID = is_numeric($_GET['field']) ? $_GET['field'] : '';
	include_once(QUBEADMIN . 'inc/forms/edit-custom-form-field.php');
}
else {
	//include classes
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$hub = new Hub();
	//get forms
	$forms = $hub->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], NULL, $multiUser);
?>
<h1>Custom Forms</h1>
<p>
	Here you can create and edit custom forms to place on your hubs.
	<? if($multiUser){ ?><br />Forms created here will be made available to all of your users' hubs as well.<? } ?>
</p>
<div id="createDir">
	<form method="post" action="add-custom-form.php" class="ajax">
		<input type="text" name="form_name" value="">
		<? if($multiUser){ ?><input type="hidden" name="multi_user" value="1" /><? } ?>
		<input type="submit" value="Create New Form" name="submit" />
	</form>
</div>
<br />
<? if($forms && is_array($forms)){
	echo '<div id="message_cnt" style="display:none;"></div>';
	echo '<div id="dash" title="'.$id.'"><div id="list_hubforms" title="hub-forms" class="dash-container"><div class="container">';
	foreach($forms as $id=>$form){
		echo '<div class="item2">
				<div class="icons">
					<a href="hub-forms.php?mode=edit&id='.$id;
					if($multiUser) echo '&multi_user=1';
					echo '" class="edit"><span>Edit</span></a>
					<a href="#" class="delete rmParent" rel="table=lead_forms&id='.$id.'&undoLink=hub-forms"><span>Delete</span></a>
				</div>';
				
				echo '<img src="img/hub-form2.png" class="icon" />';
				
				echo '<a href="hub-forms.php?mode=edit&id='.$id;
					if($multiUser) echo '&multi_user=1';
					echo '">'.$form['name'].'</a>
			</div>';
	}
	echo '</div></div></div>';
} else { ?>
<div id="message_cnt" style="display:none;"></div>
<div id="dash"><div id="list_hubforms" title="hub-forms" class="dash-container"><div class="container">
Use the form above to create a new one custom form.
</div></div></div>
<? } ?>
<? } ?>
<br style="clear:both;" />