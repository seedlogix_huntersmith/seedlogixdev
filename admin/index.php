<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	$page = 'index';
	
	//authorize
	require_once('./inc/auth.php');
	
	//header
	require_once('./admin/inc/header.php');
	
	//content
	if($_GET['rtrn']){
		include('./inc/forms/'.$_GET['rtrn'].'.php');
	} else if(($_SESSION['reseller_client'] || $_SESSION['reseller']) && $_SESSION['thm_custom-backoffice']) {
		include('./home.php');
	} else {
		include('./campaigns.php');
	}
	
	//footer
	require_once('./admin/inc/footer.php');
?>