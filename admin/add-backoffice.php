<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize Access to page
require_once('inc/auth.php');

if($_SESSION['reseller']){
	
	require_once('inc/reseller.class.php');
	$reseller = new Reseller();
	
	$type = $_POST['type'];
	$name = $reseller->validateText($_POST['name'], $type.' Name');
	$parent = isset($_POST['parent']) ? $_POST['parent'] : 0;
	
	if($reseller->foundErrors()){ //check if there were validation errors
		//$error = $reseller->listErrors('<br />'); //list the errors
	}
	else{ //if no errors
		if($message = $reseller->addNewBackofficeItem($type, $name, $parent, $_SESSION['login_uid'])){
			$response['success']['id'] = '1';
			$response['success']['container'] = '#fullpage_backoffice .container';
			$response['success']['message'] = $message;
		}
		else $response['error'] = 1;
	}
	
	echo json_encode($response);
}
?>