<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/blogs.class.php');
	
	//create new instance of class
	$connector = new Blog();
	
	//content
	echo '<h1>Analytics</h1>';
	echo $connector->userBlogsDropDown($_SESSION['user_id'], 'blog_analytics_view', $_SESSION['campaign_id']);
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop-down above to select which blog's analytics to view.</p>
</div>
