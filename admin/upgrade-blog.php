<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/blogs.class.php');
$blog = new Blog();
$blogID = is_numeric($_POST['blog_id']) ? $_POST['blog_id'] : 0;

$return['success'] = 0;

//get blog
$blogInfo = $blog->getBlogs(NULL, NULL, $blogID, NULL, NULL, NULL, 1);

//validate permission
if(($blogInfo['user_id']==$_SESSION['user_id']) || ($blogInfo['user_parent_id']==$_SESSION['login_uid']) || $_SESSION['admin']){
	$old = umask(0); //temporarily escalate chmod priveleges
	//first give the blog folder the update htaccess
	$blogDirBase = QUBEROOT . 'blogs/domains/';
	$blogDir = $blogDirBase.$blogInfo['user_id_created'].'/'.$blogID;
	//try to set the folder's chmod to 777 in case it isn't already
	@chmod($blogDir, 0777);
	//delete the existing one
	if(@is_file($blogDir.'/.htaccess')) @unlink($blogDir.'/.htaccess');
	if(@copy($blogDirBase.'htaccess.v2.txt', $blogDir.'/.htaccess')){
		//.htaccess update complete, set posts_v2 = 1
		$query = "UPDATE blogs SET posts_v2 = 1 WHERE id = '".$blogID."'";
		if($blog->query($query)){
			$return['success'] = 1;
			//if the domain is on Lincoln
			if($blogInfo['lincoln_domain']){
				//send a message over to Lincoln to update the htaccess and index.php files
				//generate a secure hash
				$salt = 'fghdtrhe4ye4n5ybdrthdrtydrgbyrtdbe4tw!!';
				$pepper = 'SGE$TB#rq389w2qrqv2ke 1e21q@!#@kmmm';
				$secHash = md5($blogInfo['domain'].$salt);
				$secHash = md5($secHash.$blogInfo['user_id'].$pepper.$blogID);
				//makes the hash only good if it was submitted today:
				$secHash = md5($secHash.date('d'));
				$data = file_get_contents('http://50.97.78.210/secure/upgrade_blog.php?uid='.$blogInfo['user_id'].'&id='.$blogID.'&domain='.$blogInfo['domain'].'&hash='.$secHash);
				//interpret json response:
				$results = json_decode($data, true);
			}
		}
		else {
			$return['message'] = 'Error updating the database.<br />';
			if($_SESSION['admin']) $return['message'] .= '\\nMySQL error: '.mysqli_error();
		}
	}
	else $return['message'] = 'Error updating your blog\'s .htaccess file.  Please contact support to have this done manually.<br />';
	umask($old); //set chmod back
}
else $return['message'] = 'Permission denied.<br />';

echo json_encode($return);
?>