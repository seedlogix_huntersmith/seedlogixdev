<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize Access to page
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

if(!$_POST){
	////////////////////////////////////////////////
	//Display sitemap settings/editing page       //
	////////////////////////////////////////////////
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	$hubInfo = $hub->queryFetch("SELECT user_id, name, domain, keyword1, theme, pages_version FROM hub WHERE id = '".$id."'", NULL, 1);
	$pv = $hubInfo['pages_version'];
	if($pv==2){
		$hubPages = $hub->query("SELECT nav_root_id, nav_parent_id, page_title_url FROM hub_page2 WHERE hub_id = '".$id."' AND type = 'page'");
	} else {
		$hubPages = $hub->query("SELECT page_navname, page_title FROM hub_page WHERE hub_id = '".$id."'");
	}
	$themeInfo = $hub->queryFetch("SELECT pages, theme_pages FROM themes WHERE id = '".$hubInfo['theme']."'", NULL, 1);
	//Start sitemap code
	$base = 
'<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	
	<url>
		 <loc>'.$hubInfo['domain'].'</loc>
	</url>
';
	$end = 
'

</urlset>';
	//Go through pages
	$pages = '';
	//Theme pages
	if($themeInfo['theme_pages']!='index'){
		$themePages = explode('|', $themeInfo['theme_pages']);
		if(is_array($themePages)){
			foreach($themePages as $key=>$value){
				if($value!='index'){
				$pages .= 
'	
	<url>
		<loc>'.$hubInfo['domain'].'/'.$hub->convertKeywordFolder($hubInfo['keyword1']).'-'.$value.'/</loc>
	</url>
';
				}
			}
		}
	} //end if($themeInfo['theme_pages']!='index')
	//User-created pages
	if(@$hub->numRows($hubPages)){
		while($page = $hub->fetchArray($hubPages, NULL, 1)){
			if($pv==2){
				//v2 pages
				if(!$page['nav_parent_id']) $navFolder = '';
				else if($page['nav_root_id'] && ($page['nav_parent_id']!=$page['nav_root_id'])){
					//if page is a sub-nested page get both parent title and root title
					$a = $hub->queryFetch("SELECT page_title_url FROM hub_page2 WHERE id = '".$page['nav_root_id']."'");
					$b = $hub->queryFetch("SELECT page_title_url FROM hub_page2 WHERE id = '".$page['nav_parent_id']."'");
					$navFolder = $a['page_title_url'].'/'.$b['page_title_url'].'/';
				}
				else {
					//if page is a nested page
					$a = $hub->queryFetch("SELECT page_title_url FROM hub_page2 WHERE id = '".$page['nav_parent_id']."'");
					$navFolder = $a['page_title_url'].'/';
				}
				$pages .= 
'
	<url>
		<loc>'.$hubInfo['domain'].'/'.$navFolder.$page['page_title_url'].'/</loc>
	</url>
';
				$navFolder = $a = $b = NULL; //reset vars
			} //end if($pv==2)
			else {
				//v1 pages
				$navFolder = $hub->convertKeywordFolder($page['page_navname']).'/';
				$pageUrl = $hub->convertKeywordFolder($page['page_title']);
				$pages .= 
'
	<url>
		<loc>'.$hubInfo['domain'].'/'.$navFolder.$pageUrl.'/</loc>
	</url>
';
				$navFolder = $pageUrl = NULL; //reset vars
			}
		}
	}

	$sitemapCode = $base.$pages.$end;
	
?>
<script type="text/javascript">
$(function(){
	$('#submit-sitemapper').die();
	$('#submit-sitemapper').live('click', function(){
		var code = $('#sitemap_code').val();
		var hid = '<?=$id?>';
		var params = { 'sitemap_code': code, 'hub_id': hid, 'action': 'build' };
		$.post('hub-sitemapper.php', params, function(data){
			if(data.success){
				$("#sitemapper-notify").css('color', '#0C0').html('Sitemap successfully updated!');
			}
			else {
				$("#sitemapper-notify").css('color', '#C00').html('An error occured:<br />'+data.message);
			}
		}, 'json');
		return false;
	});
	
	$('#revert-sitemap').die();
	$('#revert-sitemap').live('click', function(){
		var hid = '<?=$id?>';
		var params = { 'hub_id': hid, 'action': 'revert' };
		$.post('hub-sitemapper.php', params, function(data){
			if(data.success==1){
				$("#sitemapper-notify").css('color', '#0C0').html('Sitemap successfully reverted!');
			}
			else {
				$("#sitemapper-notify").css('color', '#C00').html('An error occured:<br />'+data.message);
			}
		}, 'json');
		return false;
	});
});
</script>
<h1>Website Sitemapper</h1>
<a href="hub.php?id=<?=$id?>&form=settings"><img src="img/back-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a>

<h2><?=$hubInfo['name']?></h2>
<p>
Below is the current system-generated XML sitemap for this hub.<br />
Check it for accuracy, make any changes, then click the Submit button at the bottom.<br /><br />
<small>
<strong>Tip:</strong> You can add properties to different links.  For example:<br />
&lt;url&gt;<br />
&nbsp;&nbsp;&nbsp;&lt;loc&gt;http://www.mysite.com/my-page/&lt;/loc&gt;<br />
&nbsp;&nbsp;&nbsp;&lt;lastmod&gt;2012-01-31&lt;/lastmod&gt;<br />
&nbsp;&nbsp;&nbsp;&lt;changefreq&gt;daily&lt;/changefreq&gt;<br />
&nbsp;&nbsp;&nbsp;&lt;priority&gt;0.6&lt;/priority&gt;<br />
&lt;/url&gt;
</small>
</p>
<br />
<div id="sitemapper-notify" style="font-weight:bold;"></div>
<br />
<form class="jquery_form" id="sitemapper_code"><ul>
	<li>
		<label>Sitemap Code <small>(Automatically Generated Just Now)</small></label>
		<textarea id="sitemap_code" rows="40" style="width:97%" class="no-upload"><?=$sitemapCode?></textarea>
	</li>
</ul></form><br />
<a href="#" id="submit-sitemapper"><img src="img/submit-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']; ?>.png" border="0" /></a><br /><br />

<a href="<?=trim($hubInfo['domain'], '/')?>/sitemap.xml" target="_blank" class="dflt-link">View the current sitemap</a><br />
<a href="#" id="revert-sitemap" class="dflt-link">Revert the sitemap to the last saved version</a><br />

<br style="clear:both;" />
<?
} //end if(!$_POST)
else {
	////////////////////////////////////////////////
	// Sitemapping script if form has been posted //
	////////////////////////////////////////////////
	$id = is_numeric($_POST['hub_id']) ? $_POST['hub_id'] : 0;
	$uid = $_SESSION['user_id'];
	$result['success'] = 0;
	//make sure hub belongs to the user
	$h = $hub->queryFetch("SELECT user_id, user_id_created, lincoln_domain FROM hub WHERE id = '".$id."'");
	if($h['user_id']==$uid){
		$uid = $h['user_id_created'] ? $h['user_id_created'] : $h['user_id']; //use user_id in case user_id_created blank
		if(!$h['lincoln_domain']){
			$base = QUBEROOT . 'hubs/domains/'.$uid.'/'.$id.'/';
			if($_POST['action']=='build'){
				$code = $_POST['sitemap_code'];
				//delete the last revert file if it exists
				if(@is_file($base.'sitemap_revert.xml')) @unlink($base.'sitemap_revert.xml');
				//create revert file (if sitemap already exists)
				if(@is_file($base.'sitemap.xml')){
					@copy($base.'sitemap.xml', $base.'sitemap_revert.xml');
					@unlink($base.'sitemap.xml');
				}
				//now create the file
				if($f = @fopen($base.'sitemap.xml', 'w')){
					if(@fwrite($f, stripslashes($code))!==false){
						//Success!
						$result['success'] = 1;
					}
					else $result['message'] = 'Error writing to the file.';
					@fclose($f);
				}
				else $result['message'] = 'Error opening/creating the file.';
			} //end if($_POST['action']=='build')
			else if($_POST['action']=='revert'){
				//delete the current sitemap and restore the sitemap_revert.xml (if one exists)
				if(@is_file($base.'sitemap_revert.xml')){
					@unlink($base.'sitemap.xml');
					if(@copy($base.'sitemap_revert.xml', $base.'sitemap.xml')){
						@unlink($base.'sitemap_revert.xml');
						$result['success'] = 1;
					}
					else $result['message'] = 'Error restoring the previous file.  Please contact support for manual correction.';
				}
				else $result['message'] = 'No previous file to revert to.';
			}
			else $result['message'] = 'No action specified.';
		} //end if(!$h['lincoln_domain'])
		else {
			//pass vars over to the Lincoln server to create it there
			$code = urlencode($_POST['sitemap_code']);
			//create security hash
			$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
			$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
			$secHash = md5($salt.$uid.$pepper.$id);
			//makes the hash only good if it was submitted today:
			$secHash = md5($secHash.date('d'));
			$data = file_get_contents('http://50.97.78.210/secure/create_hub_sitemap.php?id='.$id.'&uid='.$uid.'&data='.$code.'&hash='.$secHash.'&action='.$_POST['action']);
			//interpret json response:
			if(!$result = json_decode($data, true)) $result['message'] = 'Unknown error.';
		}
	} //end if($h['user_id']==$uid)
	else $result['message'] = 'Validation error...';
	
	echo json_encode($result);
}
?>