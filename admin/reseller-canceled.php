<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	
	$offset = $_GET['offset'] ? $_GET['offset'] : 0;
	if($offset) $offset = is_numeric($offset) ? $offset : 0;
	
	if($_SESSION['reseller']){
?>
<h1>Canceled Accounts</h1>
User accounts either you or the customer have canceled.
<br /><br />

<? $local->displayUsers('reseller-canceled.php', $_SESSION['login_uid'], 20, $offset, NULL, NULL, true); ?>

<br style="clear:both;" />
<? } ?>