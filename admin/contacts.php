<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once(QUBEADMIN . 'inc/auth.php');
if($_GET['mode']=='edit'){
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	include_once(QUBEADMIN . 'inc/forms/edit-contact.php');
}
else if($_GET['mode']=='view'){
	$id = is_numeric($_GET['id']) ? $_GET['id'] : '';
	include_once(QUBEADMIN . 'inc/forms/view-contact.php');
}
else {
	//include classes
	require_once(QUBEADMIN . 'inc/contacts.class.php');
	$contacts = new Contacts();
	//get contacts
	$views = $contacts->getContacts($_SESSION['user_id'], $_SESSION['campaign_id'], NULL);

?>
<div id="message_cnt" style="display:none;"></div>
<h1>Contacts</h1>

<div id="createDir">
	<form method="post" action="inc/forms/add-contact.php" class="ajax">
		<input type="text" name="name" value="">
		<input type="submit" value="Create Contact" name="submit" />
	</form>
</div>
<br style="clear:both; margin-bottom:50px;" />
<div id="message_cnt" style="display:none;"></div>
<div id="new_contact" title="New Contact" ><div class="newContact"></div></div>
<? if($views && is_array($views)){
	echo '<div class="contactContain">';
	foreach($views as $id=>$view){
		echo '
		<ul class="prospects" title="'.$id.'">
		<li class="name" style="width:25%;"><a href="contacts.php?mode=view&id='.$id.'">'.$view['first_name'].' '.$view['last_name'].'</a></li>
		<li class="email" style="width:22%;"><i>'.$view['company'].'</i></li>
		<li class="email" style="width:20%;"><i>'.$view['email'].'</i></li>
		<li class="phone" style="width:10%;"><i>'.$view['phone'].'</i></li>
		<li class="delete" style="width:5%;float:right;"><a href="#" class="delete rmParent" rel="table=contacts&id='.$id.'&undoLink=contacts"><img src="img/v3/x-mark.png" border="0" /></a></li>
		<li class="details" style="width:5%;float:right;"><a href="contacts.php?mode=view&id='.$id.'"><img src="img/v3/view-details.png" border="0" /></a></li>
		</ul>';
	}
	echo '</div>';
} else { ?>
Use the form above to create a new contact.

<? } ?>
<? } ?>
<br style="clear:both;" />