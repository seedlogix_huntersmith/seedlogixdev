<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/hub.class.php');

//create new instance of class
$connector = new Hub();
$offset = $_GET['offset'] ? $_GET['offset'] : 0;

$query = "SELECT hub_parent_id FROM hub WHERE user_id = '".$_SESSION['user_id']."'";
$muhubs = $connector->queryFetch($query);
if($muhubs['hub_parent_id']){
	
	$campaignid = NULL; 

}
else {

	$campaignid = $_SESSION['campaign_id']; 
}

if($_SESSION['reseller']) $reseller_site = $_SESSION['main_site_id'];
?>
<? if(!$_SESSION['network_user']){ ?>
<h2>Latest Lead Form Prospects</h2><br />
<?
	$hubID = $_GET['reseller_site'] ? $reseller_site : '';
	$leads = $connector->getCustomFormLeads($_SESSION['user_id'], NULL, NULL, 5, $campaignid, $hubID);
	if($leads && $connector->numRows($leads)){
		while($row = $connector->fetchArray($leads, NULL, 1)){
			//get form data
			$query = "SELECT name, data FROM lead_forms WHERE id = '".$row['lead_form_id']."'";
			$form = $connector->queryFetch($query, NULL, 1);
			//get hub data
			if($row['hub_id']){
				$query = "SELECT name FROM hub WHERE id = '".$row['hub_id']."'";
				$hubInfo = $connector->queryFetch($query, NULL, 1);
				$hubName = $hubInfo['name'];
			}
			$leadData = explode('[==]', $row['data']);
			$b = explode('|-|', $leadData[0]);
			$dispTitle = $b[1].': '.$b[2];
			$timestamp = date('n/j/Y', strtotime($row['created']));//.' at '.date('g:ia', strtotime($row['created']));
?>
	<ul class="prospects" title="prospect" id="prospect<?=$row['id']?>">
		<li class="name" style="width:28%;"><?=$dispTitle?></li>
		<li class="email" style="width:22%;"><i><?=$form['name']?></i></li>
		<li class="email" style="width:20%;"><i><?=$hubName?></i></li>
		<li class="phone" style="width:10%;"><i><?=$timestamp?></i></li>
		<li class="details" style="width:5%;"><a href="inc/forms/view-custom-leads.php?mode=single&leadID=<?=$row['id']?>"><img src="img/v3/view-details.png" border="0" /></a></li>
		<li class="delete" style="width:5%;"><a href="#" class="delete rmParent" rel="table=leads&id=<?=$row['id']?>&undoLink=hub-forms"><img src="img/v3/x-mark.png" border="0" /></a></li>
	</ul>
<?
		}
	} else echo '<b>No lead form responses to display.</b>';
?>
<br />
<? } ?>
<h2>Latest Contact Form Prospects</h2><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'hub', 'dashboard', 5, $offset, 'hub-prospects.php', $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'hub', 'dashboard', 5, $offset, 'hub-prospects.php', NULL, $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
}
?>
<br />
<br style="clear:both;" />