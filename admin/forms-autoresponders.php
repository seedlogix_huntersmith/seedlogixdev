<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once('./inc/auth.php');
//include classes
require_once('./inc/hub.class.php');
//create new instance of class

$connector = new Hub();
$_SESSION['autoresponder_section'] = "hub";
//check limits
if($_SESSION['user_limits']['responder_limit']==-1)
	$respondersRemaining = "Unlimited";
else {
	//get number of directory autoresponders for this campaign
	$numResponders = $connector->getAutoResponders(NULL, 'hub', $_SESSION['user_id'], NULL, 
										  NULL, 1, $_SESSION['campaign_id']);
	$respondersRemaining = $_SESSION['user_limits']['responder_limit'] - $numResponders;
	if($respondersRemaining<0) $respondersRemaining = 0;
}
$emailsRemaining = $connector->emailsRemaining($_SESSION['user_id']);
if($emailsRemaining==999999) $emailsRemaining = 'Unlimited';
else $emailsRemaining = number_format($emailsRemaining);
$thisPage = "formsauto";
include('./inc/forms/auto_nav.php');
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
$('#formRespondersSelect').die();
$('#formRespondersSelect').live('change', function(){
	var formID = $(this).attr('value');
	if(formID){
		$('#wait').show();
		$.get('inc/forms/forms2_autoresponder.php?id='+formID<? if($_GET['reseller_site']) echo "+'&reseller_site=1'"; ?>, function(data){
			$("#ajax-load").html(data);
			$('#wait').hide();
		});
	}
});
</script>
<h1>Custom Form Auto-Responders</h1>
<select id="formRespondersSelect" class="uniformselect no-upload">
	<option value="">Choose a form...</option>
	<?
	$forms = $connector->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], NULL, NULL, NULL, 1);
	if($forms && is_array($forms)){
		foreach($forms as $key=>$value){
			echo '<option value="'.$key.'">'.$value['name'].'</option>';
		}
	}
	?>
</select>
<br style="clear:both;" />
<div id="email-settings" style="width:100%;">
	<div id="ajax-select">
		<p>
			Use the drop-down above to select which custom form you'd like to configure auto responders for.<br />
			The form must contain an <b>Email Address field</b>.
		</p><br />
	</div>
</div>
<br style="clear:both;" /><br />