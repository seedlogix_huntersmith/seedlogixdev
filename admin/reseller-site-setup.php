<?php
	//EXECUTION FLOW:
	//1. User enters the domain here and clicks submit
	//2. jQuery posts the domain to check_domain.php script
	//3. If domain successfully added, check_domain.php calls setupResellerSubs() function in domains.class
	//	which sets up all subdomains (login, local, search, etc)
	//4. If check_domain.php returns success, jQuery calls finishResellerSiteSetup() function
	//5. finishResellerSiteSetup(): Domain is posted to reseller-functions.php with action resellerSiteSetup
	//6. reseller-functions.php: Domain is added to reseller table's main_site field
	//7. reseller-functions.php: Domain is added to reseller's main site hub with id = main_site_id
	//8. reseller-functions.php: Network sites are setup
	//9. reseller-functions.php: 6qube.com/.htaccess and 6qube.com/admin/.htaccess are appended
	
	require_once('inc/auth.php');
	
	require_once('inc/reseller.class.php');
	$reseller = new Reseller();
	
	if($_SESSION['reseller']){
?>
<h1>Main Reseller Site Setup</h1>
<br />
Enter the domain you would like to use as your main website below.<br /><br />
<small><b>Note:</b> You must already have this domain registered and its nameservers<br />
must be pointed only to both <i>ns1.theparnerportal.com</i> and <i>ns2.theparnerportal.com</i>.</small><br /><br /><br />

<form class="jquery_form">
	<ul>
		<li>
			<label>Domain Name:</label>
			<div id="domainerror" style="color:#888;font-size:12px;padding-bottom:5px;">Example: <i>http://www.domain.com</i></div>
			<input type="text" id="new-reseller-site" name="reseller-domain" class="no-upload" value="" /><br />
			<input type="image" id="new-reseller-site-sbmt" src="img/create-button/<?=$_SESSION['thm_buttons-clr']?>.png" style="width:116px;height:35px;background:none;border:none;margin-left:345px;" />			
		</li>
	</ul>
</form><br /><br />

<div id="success-message" style="display:none;">
<strong>Website setup successful!</strong><br /><br />
<a href="hub.php?form=settings&id=<?=$_SESSION['main_site_id']?>&reseller_site=1" class="dflt-link">Click here to start editing your website</a>.<br />
You may also customize your site's login page in the Theme section of the Manage tab on the left.
</div>
<br style="clear:both;" />
<? } ?>