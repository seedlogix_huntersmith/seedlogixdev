<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
if($_SESSION['reseller']){
	$mode = isset($_GET['mode']) ? $_GET['mode'] : 'navs';
	$resellerID = $_SESSION['login_uid'];
	
	if($mode=='navs'){
		//get reseller's nav sections
		$query = "SELECT `id`, `name` FROM `resellers_backoffice` WHERE `admin_user` = '".$resellerID."' 
				AND `type` = 'nav' 
				AND `trashed` = '0000-00-00'  
				ORDER BY `list_order` ASC, id ASC";
		if($navs = $reseller->query($query)) $numNavs = $reseller->numRows($navs);
?>
<h1>Custom Back Office Nav Sections</h1>
<div id="createDir">
<form method="post" class="ajax" action="add-backoffice.php">
	<input type="text" name="name" maxlength="15" />
	<input type="hidden" name="type" value="nav" />
	<input type="submit" value="Create Nav Section" name="submit" />
</form>
</div>
<div id="message_cnt"></div>
<div id="dash"><div id="fullpage_backoffice" class="dash-container"><div class="container">
<?
	if($numNavs){ 
		while($row = $reseller->fetchArray($navs)){
			echo '<div class="item2">
			<div class="icons">
				<a href="reseller-backoffice-pages.php?mode=pages&n='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=resellers_backoffice&id='.$row['id'].'" onclick="javascript:$(this).parent().parent().fadeOut();"><span>Delete</span></a>
			</div>
			<img src="img/nav-section.png" class="blogPost" />';
			
			echo '<a href="reseller-backoffice-pages.php?mode=pages&n='.$row['id'].'">'.$row['name'].'</a></div>';
		}
	}
	else {
		echo 'Use the form above to create a new custom back office nav section.';
	}
?>
</div></div></div>
<?
	} //end if($mode=='navs')
	else if($mode=='pages'){
		$navSection = $_GET['n'];
		$query = "SELECT id, name FROM resellers_backoffice WHERE admin_user = '".$resellerID."' 
				AND type = 'page' AND parent = '".$navSection."' 
				AND resellers_backoffice.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'resellers_backoffice')  
				ORDER BY `list_order` ASC, id ASC";
		if($pages = $reseller->query($query)) $numPages = $reseller->numRows($pages);
		//get nav section name
		$query = "SELECT name, `list_order`, allow_class FROM resellers_backoffice WHERE id = '".$navSection."' LIMIT 1";
		$a = $reseller->queryFetch($query);
		$navName = $a['name'];
		$navListOrder = $a['list_order'];
		//allowed classes for this page
		$allowed = explode('::', $a['allow_class']);
		//get user class names
		$classes = array();
		$query = "SELECT id, name FROM user_class WHERE admin_user = '".$_SESSION['login_uid']."'";
		$b = $reseller->query($query);
		while($c = $reseller->fetchArray($b)){
			$classes[$c['id']] = $c['name'];
		}
?>
<script type="text/javascript">
$(function(){
	$("input:checkbox").uniform();
});
</script>
<div id="message_cnt"></div>
<a href="reseller-backoffice-pages.php"><img src="img/v3/nav-previous-icon.jpg" border="0" alt="Back to Pages" /></a><br /><br />
<h1>Custom Back Office Pages - <small><?=$navName?></small></h1>
<div id="createDir">
<form method="post" class="ajax" action="add-backoffice.php">
	<input type="text" name="name" maxlength="18" />
	<input type="hidden" name="type" value="page" />
	<input type="hidden" name="parent" value="<?=$navSection?>" />
	<input type="submit" value="Create New Page" name="submit" />
</form>
</div>
<div id="form_test">
<form name="resellers_backoffice" class="jquery_form" id="<?=$navSection?>">
	<ul>
		<li>
			<label>Nav Section Name</label>
			<input type="text" name="name" value="<?=$navName?>" maxlength="15" />
		</li>
		<li>
			<label>List Order <span style="color:#666; font-size:75%;">0 - 999</span></label>
			<input type="text" name="list_order" value="<?=$navListOrder?>" maxlength="3" />
		</li>
		<li style="line-height:23px;">
			<label>Allowed User Types</label>
			<? foreach($classes as $key=>$value){ ?>
			<input type="checkbox" name="<?=$key?>" rel="<?=$navSection?>" class="updateCheckbox" <? if(in_array($key, $allowed)) echo 'checked'; ?> />&nbsp; <?=$value?><br />
			<? } ?>
		</li>
	</ul>
</form>
</div>
<div id="dash"><div id="fullpage_backoffice" class="dash-container"><div class="container">
<?
	if($numPages){ 
		while($row = $reseller->fetchArray($pages)){
			echo '<div class="item2">
			<div class="icons">
				<a href="inc/forms/edit-backoffice-page.php?id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=resellers_backoffice&id='.$row['id'].'" onclick="javascript:$(this).parent().parent().fadeOut();"><span>Delete</span></a>
			</div>
			<img src="img/page.png" class="blogPost" />';
			
			echo '<a href="inc/forms/edit-backoffice-page.php?id='.$row['id'].'">'.$row['name'].'</a></div>';
		}
	}
	else {
		echo 'Use the form above to create a new custom back office page.';
	}
?>
</div></div></div>
<?
	} //end if($mode=='pages')
} //end if($_SESSION['reseller'])
else echo 'Not authorized.';
?>