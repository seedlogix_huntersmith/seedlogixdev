<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

$offset = $_GET['offset'] ? $_GET['offset'] : 0;
if($offset) $offset = is_numeric($offset) ? $offset : 0;

//Page viewing types:
//1 - User viewing their own history
//2 - Admin viewing a user's history
//3 - Admin viewing entire billing log

if(!$specView)
	$view = $_GET['view'] ? $_GET['view'] : '1';
else 
	$view = $specView;
if($view==1 || $view==2){
	$uid = $_GET['id'] ? $_GET['id'] : $_SESSION['user_id'];
	//make sure user has permission
	if($uid==$_SESSION['user_id']) $continue = true;
	else if($_SESSION['admin']) $continue = true;
}
if($view==3){
	if($_SESSION['admin']){
		$all = 1;
		$continue = true;
	}
}

if($continue){
	//get company name
	if($uid){
		$query = "SELECT company FROM user_info WHERE user_id = '".$uid."'";
		$userInfo = $reseller->queryFetch($query);
	}
?>
<div style="width:100%;">
<? if(!$all){ ?>
	<h1>Billing History for <?=$userInfo['company']?></h1>
	<? if($uid){ ?>
	History of items we've attempted to bill you for.
	<? } else { ?>
	History of items we've charged users for.
	<? } ?>
<? } else { ?>
	<h1>6Qube System Billing Log</h1>
<? } ?>
<br /><br />

<? $reseller->displayBillingHistory(50, $offset, $uid, $rid, $all, $view, 1); ?>
</div>
<br style="clear:both;" />
<? } ?>