<?php
//start session
session_start();
	require_once ( dirname(__FILE__) . '/../hybrid/bootstrap.php');
require_once(QUBEADMIN . 'inc/db_connector.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$connector = new DbConnector();
$local = new Reseller();
//first check to make sure they're not blacklisted
$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
if($ip && $local->checkBlacklist($ip)) $blacklisted = true;
//limit login attempts to prevent bruteforcing
$attempt = isset($_SESSION['login_attempt']) ? $_SESSION['login_attempt'] : 0;
if(isset($_GET['amado'])) var_dump(Qube::IS_DEV(), Qube::get_configname());
if($attempt < 10 && !$blacklisted){	
	$username = $local->validateEmail($_POST['username']);
	$password = $local->validateText($_POST['password']);
	$resellerID = $_POST['reseller-id'];
	$query = "SELECT * FROM users WHERE username = '".$username."' AND password = '".$connector->encryptPassword($password)."'";
	$row = $connector->queryFetch($query);
	
	/* USER INFO TABLE */
	$userInfoQuery = "SELECT firstname, lastname FROM user_info WHERE user_id = '".$row['id']."'";
	$userInfoRow = $connector->queryFetch($userInfoQuery);
	
	$query = "SELECT campaign_limit, admin_access, admin_regular, 
			  admin_design, reports_user, maps_to FROM user_class WHERE id = '".$row['class']."'";
	$rowClass = $connector->queryFetch($query);
	$query2 = "SELECT user_manager FROM user_info WHERE user_id = '".$row['id']."'";
	$rowInfo = $connector->queryFetch($query2);
	
	if($row){
		if($row['super_user']>0) $super_user = true;
		if(!$local->checkUserAccess($row['id'])) {
                    
			//if they're never logged in
			if($row['last_login']=="0000-00-00 00:00:00")
				header("Location: login.php?error_code=1");
			//otherwise their access has been disabled
			else 
				header("Location: login.php?error_code=4");
                        
                        exit;
		}
		else if($resellerID && ($row['parent_id']!=$resellerID && $row['id']!=$resellerID && !$super_user)){
			//if user is trying to log into a reseller system but isn't under the reseller's account
			header("Location: login.php?error_code=3");
		}
		else {
			//Set last_login
			$query = "UPDATE users SET last_login = NOW() WHERE id = ".$row['id'];
			$connector->query($query);
			$_SESSION['auth'] = 1;
			$_SESSION['login_user'] = $_SESSION['user'] = $_POST['username'];
			$_SESSION['login_uid'] = $_SESSION['user_id'] = $row['id'];
			$_SESSION['user_fullName'] = $userInfoRow['firstname'].' '.$userInfoRow['lastname'];
			$_SESSION['FIRST_login_uid'] = $_SESSION['user_id'] = $row['id'];
			$_SESSION['FIRST_user_fullName'] = $userInfoRow['firstname'].' '.$userInfoRow['lastname'];
			$_SESSION['user_class'] = $row['class'];
			$_SESSION['brandoverride'] = $row['parent_id']==7065;
			$_SESSION['admin_access'] = $rowClass['admin_access']==1;
			$_SESSION['admin_regular'] = $rowClass['admin_regular']==1;
			$_SESSION['admin_design'] = $rowClass['admin_design']==1;
			$_SESSION['reports_user'] = $rowClass['reports_user']==1;
			$_SESSION['user_manager'] = $rowInfo['user_manager']==1;
			$_SESSION['network_user'] = $rowClass['maps_to']==27;
			$_SESSION['user_limits'] = $local->getLimits($user_id, NULL, $row['class']);
			$_SESSION['user_browser'] = $_POST['browser'];
			$_SESSION['login_fingerprint'] = md5($_SERVER['HTTP_USER_AGENT'].'sdf97dfndndsha!!');
			$_SESSION['checkin_time'] = time();
			if($row['super_user']==2 || $row['super_user']==3){
				if($row['super_user']==3) $_SESSION['support_user'] = true;
				//if user is an admin or support user
				if($resellerID){
					//if user is a 6qube admin logging into a reseller site, fake being that user
					$query = "SELECT username, class FROM users WHERE id = ".$resellerID;
					$a = $connector->queryFetch($query);
					$_SESSION['login_user'] = $_SESSION['user'] = $a['username'];
					$_SESSION['reseller'] = true;
					$_SESSION['login_uid'] = $_SESSION['user_id'] = $_SESSION['parent_id'] = $resellerID;
					$_SESSION['user_class'] = $a['class'];
					$_SESSION['parent_id'] = $resellerID;
				}
				else {
					$_SESSION['admin'] = true;
					$_SESSION['parent_id']	=	$row['parent_id'] == 0 ? 
$row['id'] : $row['parent_id'];
					if($row['super_user']==2) $_SESSION['do_second_validation'] = true;
				}
			}
			if($row['super_user']==1 || $row['super_user']==4 || $row['super_user']==5 || $row['super_user']==6  || $rowClass['admin_access']==1){
				//if user is a reseller or reseller support user
				$_SESSION['reseller'] = true;
				$_SESSION['globalMUpid'] = $row['parent_id'];
				if($row['super_user']==4){
					//only reseller support user	
					$_SESSION['support_user'] = true;
					$_SESSION['parent_id'] = $resellerID;
				}
				else if($row['super_user']==5){
					//only still defining
					$_SESSION['super_user'] = true;
					$_SESSION['parent_id'] = $resellerID;
				}
				else if($row['super_user']==6){
					//only still defining	
					$_SESSION['max_user'] = true;
					$_SESSION['parent_id'] = $resellerID;
				}
				else $_SESSION['parent_id'] = $row['id'];
			}
			if($row['reseller_client']==1){
				//reseller client
				$_SESSION['reseller_client'] = true;
				if($rowClass['campaign_limit']==0){
					//no campaigns	
					$_SESSION['backoffice_user'] = true;
					$_SESSION['parent_id'] = $local->getParentID($row['id']);
				}
				else $_SESSION['parent_id'] = $local->getParentID($row['id']);
			}
			if($row['sixqube_client']==1){
				//reseller client
				$_SESSION['sixqube_client'] = true;
				$_SESSION['parent_id'] = 7;
			}
                        
                    // make directories if they do not exist.
                    $local->createUserFolders($row['id'], '', true);
                    
			//check user in
			$local->onlineNow($_SESSION['login_uid']);
			
			//theme settings
			if($_SESSION['reseller'] || $_SESSION['reseller_client']){
				$query = "SELECT * FROM `resellers`";
				if($_SESSION['reseller']){
					$query .= "WHERE `admin_user` = ".$_SESSION['user_id'];
					$_SESSION['themedir'] = './themes/'.$_SESSION['login_uid'].'/';
				}
				else {
					$query .= "WHERE `admin_user` = ".$_SESSION['parent_id'];
					$_SESSION['themedir'] = './themes/'.$_SESSION['parent_id'].'/';
				}
				if($theme = $connector->queryFetch($query)){
					$noSession = array('id', 'admin_user', 'main_site', 'main_site_id', 'support-page', 'support_email',
									'support_phone', 'login-bg', 'login-bg-clr', 'login-logo', 'login-brdr-clr', 
									'login-txt-clr', 'login-signup-lnk', 'login-lnk-clr', 'login-suggest-ff', 
									'activation_email', 'payments_acct', 'payments_acct2', 'cim_pro_id', 
									'cim_pro_payment_id', 'receipt_email', 'private_domain', 'private_theme', 'payments');
					foreach($theme as $key=>$value){
						if(!in_array($key, $noSession)){
							   $_SESSION['thm_'.$key] = $value;
						}
						if($key=="main_site" || $key=="main_site_id" || $key=="company" || $key=="payments" || $key=="private_domain" || $key=="private_theme" || $key=="payments"){
							if($key=="company") $_SESSION['main_company'] = $value;
							else $_SESSION[$key] = $value;
							if($_SESSION['private_domain']==1) $_SESSION['main_site'] = '6qube.com';
						}
					} //end foreach($theme as $key=>$value)
					if($_SESSION['private_theme']==1) $_SESSION['theme'] = false;
					else $_SESSION['theme'] = true;
				} //end if($theme = $connector->queryFetch($query))
			} //end if($_SESSION['reseller'] || $_SESSION['reseller_client'])
			else {
				$_SESSION['main_site'] = '6qube.com';
				$_SESSION['main_company'] = '6Qube';
			}
			if(!@$_SESSION['main_site']) 
				$_SESSION['main_site']	=	$_SERVER['HTTP_HOST'];
			$_SESSION['login_attempt'] = 0;
			//change session id to prevent hijacking
			session_regenerate_id(true);
			header('Location: ./');
		} //end else [login success]
	} //end if($row)
	else {
		$_SESSION['login_attempt']++;
		header("Location: login.php?error_code=2");
	}
} //end if($attempt < 10)
else if($blacklisted){
	header("Location: login.php?error_code=x");
}
else {
	//if person has tried to login more than 10 times blacklist their IP
	$local->blacklistIP($ip, $_POST['username'], 'login_attempts');
	//and disable access to account
	$query = "UPDATE users SET access = 0 WHERE username = '".$local->validateText($_POST['username'])."'";
	$local->query($query);
	//reset login attempt counter
	$_SESSION['login_attempt'] = 0;
	header("Location: login.php?error_code=x");
}
?>
