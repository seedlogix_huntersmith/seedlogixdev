
<form name="create_campaign" class="ajax" method="post" action="<?php echo $this->action_url('campcollab_create'); ?>">

    <h2>Name</h2>
    <input type="text" name="campaigncollab[name]" onmousemove="focus()" <?= $readonly ?> />
    <br clear="all" />

    <h2>Email Address</h2>
    <input type="text" name="campaign_name" onmousemove="focus()" <?= $readonly ?> />
    <br clear="all" />

    <h2>Scope</h2>
    <select class="uniformselect" name="fff" title="Scope">
        <optgroup label="Campaign Level">
            <option value="">All websites.</option>
        </optgroup>
        <optgroup label="Website Level">
            <?php foreach($websites as $website): ?>
            <option value="14204" selected=""><?php
                echo $website->name; ?>
            </option>
            <?php endforeach; ?>
        </optgroup>
    </select>
    
    <fieldset>
        <?php foreach($websites as $website): ?>
        <label><input type="checkbox" name="fff"><?php
            echo $website->name;
        ?></label>
        <?php endforeach; ?>
    </fieldset>
    
    
    <LABEL><input type="checkbox" /> Active</label>

    <br clear="all" />

    <input type="hidden" name="camp_limit" value="<?= $camp_limit ?>" />
    <input type="submit" name="submit" value="Save" />
</form>
<script type="text/javascript">
    $(".uniformselect").uniform();
</script>