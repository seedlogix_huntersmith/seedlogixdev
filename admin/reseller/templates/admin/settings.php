


<div id="side_nav">
	<ul id="subform-nav">
   		<li class="first">
			<a href="<?php echo $this->action_url('campcollab_vcampaign'); ?>" <? if($thisPage=="formsauto") echo $curr; ?>>Campaign Level</a>
		</li>
		<li class="first">
			<a href="<?php
                                echo $this->action_url('campcollab_vwebsite');
                            ?>" <? if($thisPage=="websiteauto") echo $curr; ?>>Website Level</a>
		</li>
		<li class="first">
			<a href="<?php
                                echo $this->action_url('campcollab_createnew');
                            ?>" <? if($thisPage=="websiteauto") echo $curr; ?>>Create a new .. </a>
		</li>
	</ul>
</div>
<br style="clear:left;"  /><br />

<div id="camps">
    
    <div class="icon"><img src="img/v3/campaign-icon.png" width="64" /></div>
	<h1>
            Create a .. 
	</h1>
	<div class="clear"></div>
        
        
	<form name="create_campaign" class="ajax" method="post" action="<?php echo $this->action_url('campcollab_create'); ?>">
            
            <h2>Name</h2>
            <input type="text" name="campaign_name" onmousemove="focus()" <?=$readonly?> />
            <br clear="all" />
            
            <h2>Email Address</h2>
            <input type="text" name="campaign_name" onmousemove="focus()" <?=$readonly?> />
            <br clear="all" />
            
            <h2>Scope</h2>
            <select id="changeGraph" class="uniformselect" style="opacity: 0; ">
                <optgroup label="Campaign Level">
                    <option value="">All websites.</option>
                </optgroup>
                <optgroup label="Website Level">
                    <option value="14204" selected="">HUB / WEBSITE TEST 1</option>
                    <option value="14483">beewere</option>
                </optgroup>
            </select> 
            <LABEL><input type="checkbox" /> Active</label>
            
            <br clear="all" />
            
            <input type="hidden" name="camp_limit" value="<?=$camp_limit?>" />
            <input type="submit" name="submit" value="Save" />
	</form>
        
</div>

<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
$('#formRespondersSelect').die();
$('#formRespondersSelect').live('change', function(){
	var formID = $(this).attr('value');
	if(formID){
		$('#wait').show();
		$.get('inc/forms/forms2_autoresponder.php?id='+formID<? if($_GET['reseller_site']) echo "+'&reseller_site=1'"; ?>, function(data){
			$("#ajax-load").html(data);
			$('#wait').hide();
		});
	}
});
</script>
<h1>Custom Form Auto-Responders</h1>
<select id="formRespondersSelect" class="uniformselect no-upload">
	<option value="">Choose a form...</option>
	<?
	$forms = $connector->getCustomForms($_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], NULL, NULL, NULL, 1);
	if($forms && is_array($forms)){
		foreach($forms as $key=>$value){
			echo '<option value="'.$key.'">'.$value['name'].'</option>';
		}
	}
	?>
</select>
<br style="clear:both;" />
<div id="email-settings" style="width:100%;">
	<div id="ajax-select">
		<p>
			Use the drop-down above to select which custom form you'd like to configure auto responders for.<br />
			The form must contain an <b>Email Address field</b>.
		</p><br />
	</div>
</div>
<br style="clear:both;" /><br />