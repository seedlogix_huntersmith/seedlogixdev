<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/local.class.php');

$c_local = new Local();
$campaigns = $c_local->getCampaigns($_SESSION['user_id']);
$current_cid = $_SESSION['campaign_id'];
?>

<h1>Transfer Items</h1>
<? if($_SESSION['reseller']){ ?>
<h2>Transfer Items to Another User</h2>
<!--<a href="reseller-transfer-items.php?mode=campaign&cid=<?=$current_cid?>" class="dflt-link">Transfer this entire campaign to another user</a>
<br /><br />-->
<a href="reseller-transfer-items.php?mode=campaign2&cid=<?=$current_cid?>" class="dflt-link">Choose specific items to transfer to another user</a>
<br /><br />
<? } ?>
<h2>Transfer Items to Another Campaign</h2>

<script type="text/javascript">
	$(".uniformselect").uniform();
	
	$('#itemTypeSelector').die();
	$('#itemTypeSelector').live('change', function(){
		var formType = $(this).attr('value');
		if(formType != 'Choose Item...'){
			$('#wait').show();
			$.get('transfer-item-type.php?type='+formType+'&cid='+<?=$current_cid?>, function(data){
				$('#typeHolder').html(data);
				$('input:checkbox').uniform();
				$('#wait').hide();			
			});
		}
		else
			$('#typeHolder').empty();		
	});
	
	$('#transferItemForm').bind('submit', function(event){
		event.stopPropagation();

		if($('#itemTypeSelector').attr('selectedIndex') == 0){
			alert('You must select a type to transfer.')
			return false;
		}
		if($('#campaignSelector').attr('selectedIndex') == 0){
			alert('You must select a campaign.');
			return false;			
		}		
		else{
			$.ajax({
				type: 'POST',
				url: 'transfer-item-complete.php',
				data: $('#transferItemForm').serialize(),
				success: function(data){
					if (data == 'success'){
						alert('Item was transferred successfully');
						$.get('dashboard.php', function(data){
							$('#ajax-load').html(data);							
						});
					}
					else if(data == 'none selected'){
						alert('You must check an item to transfer.');
						return false;
					}
					else{
						alert('There was a problem transfering your item, please try again or contact our support team.');
						return false;						
					}
					$('#transferItemForm').unbind('submit');
				}
			})
		}
		return false;		
	});
</script>
<form id="transferItemForm" name="transferItemForm" action="#">
<h3>What Type of Item Would You Like to Transfer?</h3>
<select id="itemTypeSelector" name="itemTypeSelector" class="uniformselect">
	<option>Choose Item...</option>
	<option value="hub">Hub</option>
	<option value="form">Form</option>
	<option value="blog">Blog</option>
</select>

<ul id="typeHolder"></ul>


<h3 class="campaignTrans">Transfer to Campaign:</h3>
<select id="campaignSelector" name="campaignSelector" class="uniformselect">
	<option>Choose Campaign...</option>
	<? 
	  if($campaigns && $c_local->numRows($campaigns)) {
	  	while($rows = $c_local->fetchArray($campaigns)){
	  		if($rows['id'] != $_SESSION['campaign_id'])
	  			echo '<option value="'.$rows['id'].'">'.$rows['name'].'</option>';	  				
	  	}		
	  }
    ?>
</select>
<input type="hidden" name="old_cid" value="<?=$current_cid?>" />
<br />
<br />
<input type="image" src="img/submit-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']?>.png" id="transferSubmit" style="width:108px;height:33px;padding:0;background:none;border:none;" />
</form>