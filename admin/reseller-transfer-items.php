<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$connector = new Reseller();

$mode = $_GET['mode'];
$mode2 = $_GET['mode2'];
$cid = is_numeric($_GET['cid']) ? $_GET['cid'] : '';

if($_SESSION['reseller']){
	//get items
	if($mode=='campaign'){
		//if just trying to copy an entire campaign
		
	}
	else if($mode=='campaign2'){
		//if campaign get all sections
		//hubs
		$query = "SELECT id, name FROM hub WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$hubs = $connector->query($query);
		//blogs
		$query = "SELECT id, blog_title FROM blogs WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$blogs = $connector->query($query);
		//directory
		$query = "SELECT id, name FROM directory WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$local = $connector->query($query);
		//articles
		$query = "SELECT id, name FROM articles WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$articles = $connector->query($query);
		//press
		$query = "SELECT id, name FROM press_release WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$press = $connector->query($query);
		//custom forms
		$query = "SELECT id, name FROM lead_forms WHERE cid = '".$cid."' AND user_parent_id = '".$_SESSION['login_uid']."'
				AND trashed = '0000-00-00'";
		$forms = $connector->query($query);
	}
	$i = 1;
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect, input:checkbox").uniform();
});
</script>
<style type="text/css">
	.transfer_items li { line-height: 20px; }
</style>
<h1>Transfer Items to Another User</h1>
<a href="transfer-items.php" class="dflt-link"><img src="img/back-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']?>.png" alt="Back" /></a>
<? if($mode=='campaign2'){ ?>
	<h2 style="color:#000;">Campaign #<?=$cid?></h2>
	<p>
		&bull; Select which items you'd like to transfer and then choose a user to transfer to at the bottom.<br />
		&bull; Everything associated with each item (prospects, analytics, autoresponders, etc.) will be transferred too.<br />
		&bull; Trashed items <strong>do not</strong> appear in the list below.
	</p>
	<br />
	
	<form class="jquery_form transfer_items" action="reseller-functions.php" method="post">
		<h2>Hubs</h2>
		<ul>
		<?
		if($hubs && $connector->numRows($hubs)){
			while($row = $connector->fetchArray($hubs)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="hub-'.$row['id'].'" checked>&nbsp;&nbsp;'.$row['name'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br />
		<h2>Blogs</h2>
		<?
		if($blogs && $connector->numRows($blogs)){
			while($row2 = $connector->fetchArray($blogs)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="blogs-'.$row2['id'].'" checked>&nbsp;&nbsp;'.$row2['blog_title'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br />
		<h2>Directory Listings</h2>
		<?
		if($local && $connector->numRows($local)){
			while($row3 = $connector->fetchArray($local)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="directory-'.$row3['id'].'" checked>&nbsp;&nbsp;'.$row3['name'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br />
		<h2>Articles</h2>
		<?
		if($articles && $connector->numRows($articles)){
			while($row4 = $connector->fetchArray($articles)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="articles-'.$row4['id'].'" checked>&nbsp;&nbsp;'.$row4['name'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br />
		<h2>Press Releases</h2>
		<?
		if($press && $connector->numRows($press)){
			while($row5 = $connector->fetchArray($press)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="press_release-'.$row5['id'].'" checked>&nbsp;&nbsp;'.$row5['name'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br />
		<h2>Custom Forms</h2>
		<?
		if($forms && $connector->numRows($forms)){
			while($row6 = $connector->fetchArray($forms)){
				echo '<li>';
				echo '<input type="checkbox" name="item'.$i.'" value="lead_forms-'.$row6['id'].'" checked>&nbsp;&nbsp;'.$row6['name'].'<br />';
				echo '</li>';
				$i++;
			}
		}
		else echo 'None available.';
		?>
		
		<br /><br />
		<h2>Transfer to user</h2>
		<div id="transferUserChoiceDiv">
			<li>
				<?=$connector->displayUsersDrop($_SESSION['login_uid'], NULL, 'no-upload', 'uid1', 'uid1', 50);?>
			</li>
			<li style="margin-top:-8px;"><strong>OR</strong></li>
			<li>Enter User ID #: <input type="text" id="uid2" name="uid2" value="" class="no-upload" style="width:50px;" /></li>
		</div>
		<br />
		<input type="hidden" name="action" value="transferItems" />
		<input type="hidden" name="cid" value="<?=$cid?>" />
		<input type="hidden" name="uid" value="" id="toUid" />
		<div id="continueTransferDiv">
			<a href="#" class="dflt-link continueTransferButton"><img src="img/continue-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']?>.png" alt="Continue" /></a>
		</div>
		<input type="image" src="img/submit-button<? if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr']?>.png" id="transferSubmit" style="width:108px;height:33px;padding:0;background:none;border:none;display:none;" />
		</ul>
	</form>
<? } ?>
<br style="clear:both;" />
<? } ?>