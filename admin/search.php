<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
if(!$action = $_POST['action']) $action = $_GET['action'];	
//authorize Access to page
require_once('inc/auth.php');
if($_SESSION['user_id']) $continue = true;

if($continue){	
	//***************************************************************************
	//if user searches campaigns page
	//***************************************************************************//
	if($action=='searchCampaigns'){
		//iniate the local class
		require_once('inc/local.class.php');
		$searching = new Local();
		
		$limit = is_numeric($_GET['limit']) ? $_GET['limit'] : 5;
		$user_id = is_numeric($_GET['user_id']) ? $_GET['user_id'] : '';
		$search = $searching->sanitizeInput($_GET['search']);
		
		echo '<div id="currOffset" title="'.$offset.'" style="display:none;"></div>';
		$results = $searching->getCampaigns($user_id, NULL, $limit, NULL, NULL, $search);
		
		if($results){
			while($row = $searching->fetchArray($results)){
				$html .= $searching->campaignDashUpdate2($row);
			}
		}
		
		if($html){
			echo '<div id="dash"><div id="dashboard_campaign" class="camp-container"><div class="container">';
			echo $html;
			echo '</div></div></div>';
		}
	}
	
	//***************************************************************************
	//if user searches campaigns page
	//***************************************************************************//
	if($action=='searchSites'){
		//iniate the local class
		require_once('inc/hub.class.php');
		$searching = new Hub();
		
		$limit = is_numeric($_GET['limit']) ? $_GET['limit'] : 5;
		$user_id = is_numeric($_GET['user_id']) ? $_GET['user_id'] : '';
		$search = $searching->sanitizeInput($_GET['search']);
		
		echo '<div id="currOffset" title="'.$offset.'" style="display:none;"></div>';
		$results = $searching->getHubs($user_id, $limit, NULL, $_SESSION['campaign_id'], NULL, NULL, 1, NULL, NULL, $search);
		
		if($results){
			while($row = $searching->fetchArray($results)){
				$html .= $searching->hubDashUpdate($row['id'], $row['name'], $user_id, $row['tracking_id'], 'block');
			}
		}
		
		if($html){
			//echo '<div id="dash"><div id="fullpage_website" class="dash-container"><div class="container" id="">';
			echo $html;
			//echo '</div></div></div>';
		}
	}
}
?>