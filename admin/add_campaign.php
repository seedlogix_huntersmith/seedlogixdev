<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/local.class.php');
	$directory = new Local();
	
	
	$newDir = $directory->validateText($_POST['campaign_name'], 'Campaign'); //validate directory name
	if($directory->foundErrors()){ //check if there were validation errors
		//$error = $directory->listErrors('<br />'); //list the errors
	}
	else{ //if no errors
		$message = $directory->addNewCampaign($_SESSION['user_id'], $newDir, $_POST['camp_limit']); //add new campaign
		
		$response['success']['id'] = '1';
		$response['success']['action'] = 'replace';
		$response['success']['container'] = '#dashboard_campaign .container';
		$response['success']['message'] = $message;
	}

	echo json_encode($response);
	
?>