<?php

require 'ContactData.php';

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of Lead
 *
 * @author amado
 */

class Lead extends ContactData
{
    function _getCondition($alias, $purpose = self::FOR_SELECT) {
        $where = new DBWhereExpression(parent::_getCondition($alias, $purpose));
        
        $where->Where('`' . $alias . '`.optout != 1');
        
        return $where;
    }
}
