<?php


/**
 * LeadData Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * Column	Type	Null	Default	Comments
id	int(11)	No 	 	 
user_id	int(11)	No 	0 	 
lead_form_id	int(11)	No 	0 	 
hub_id	int(11)	No 	0 	 
hub_page_id	int(11)	No 	0 	 
lead_email	varchar(255)	No 	 	 
data	longblob	No                  #ignored in this class
search_engine	varchar(255)	No 	 	 
keyword	varchar(255)	No 	 	 
responded	tinyint(1)	No 	0 	 
responses_received	text	No 	 	 
optout	tinyint(1)	No 	0 	 
ip	varchar(255)	No 	 	 
trashed	tinyint(1)	No 	 	 
timestamp	timestamp	No 	CURRENT_TIMESTAMP 	 
created	timestamp	No 	0000-00-00 00:00:00 
 * 
 */
class LeadData {    
    
    static function getPdoQuery(QubeDriver $D, $lead_id)
    {
        $qstr = 'SELECT coalesce(`6q_formfields_tbl`.label, leads_datax.label) label, coalesce(leads_datax.valuestr, leads_datax.valueint) value FROM leads_datax LEFT JOIN `6q_formfields_tbl` ON leads_datax.field_ID = `6q_formfields_tbl`.ID WHERE leads_datax.lead_ID = ' . (int)$lead_id;

	$Q = $D->getDB()->query($qstr);        
        return $Q;
    }
    
    /**
     * returns an array of [$label] => (['value'] => $value)
     * 
     * @param QubeDriver $D
     * @param int $lead_id
     * @return array
     */
    static function getData(QubeDriver $D, $lead_id)
    {
        $Q = self::getPdoQuery($D, $lead_id);
        
        $results    = $Q->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);//, 'Website');
        $array      = array_map('reset', $results); // some array magic @see pdostatement::fetchAll() doc

#        deb($array, $lead_id, $Q, $D);
#	exit;
        return $array;
    }
    
    static function Load(QubeDriver $D, $lead_id)
    {
	        
    }
}
