<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Row from the 6q_roles table
 *
 * @author amado
 */
class QubeRoleModel extends HybridModel {
    /**
     *
     * @var int
     */
    public $ID;
    /**
     *
     * @var int
     */
    public $reseller_user;
    /**
     *
     * @var string
     */
    public $role;   
    
    function __getColumns() {
        return array('ID', 'reseller_user', 'role');
    }
    
    function Validate(array &$row, ValidationStack $errors){
        $vc =   VarContainer::getContainer($row);
        if($vc->isEmpty('role'))
            $errors->addError ('role', 'Role name is empty.');
		
        if($vc->isEmpty('ID') && $vc->isEmpty('reseller_user'))
			$errors->addError ('role', 'must define a reseller');
		
        return !$errors->hasErrors();
    }
    
    static function GetRoles(UserModel $Admin)
    {
        $q  =   Qube::Start()->queryObjects('QubeRoleModel', 'T', 'reseller_user = 0 OR reseller_user = %d',
                    $Admin->getID());
        
        return $q->Exec()->fetchAll();
    }
}

