<?php

/**
 * LeadFormModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class LeadFormModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */

    public $id, $user_id, $user_parent_id, $cid, $name, $options, $multi_user, $trashed, $data = 'converted', $submit_button_text, $redirect_url, $show_reset_button, $ajax_submit, $sort, $version, $created = 'NOW()';

    protected $_PR_KEY  =   'id';
    protected $_fields  =   null;

    protected $_defaults = array('data' => '"converted"', 'created' => 'NOW()');
		protected $_convertLegacy	=	false;

    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id', 'user_id', 'user_parent_id', 'data', 'options', 'cid', 'name', 'multi_user',
                    'created', 'trashed', 'data', 'submit_button_text', 'redirect_url', 'show_reset_button', 'ajax_submit', 'version', 'created');
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
    
    function getFields_legacy(){
        require_once QUBEADMIN . 'inc/hub.class.php';
        
        $fields =   Hub::parseCustomFormFieldsStatic($this->data);
        
        
        return ($fields ? $fields : array());
    }
    
    function removeField_legacy($index){
        $rows   =   explode('[==]', $this->data);
        unset($rows[$index]);
        $rows   =   trim(implode('[==]', $rows), '[==]');
        
        $this->data =    $rows;        
    }
    
    /**
     * 
     * @param array $data
     * @param ValidationStack $V
     * @return LeadFormFieldModel
     */
    function addField(array &$data, ValidationStack $V){
        
        if($this->data != 'converted')
            $this->getFields();
        
        $Field  =   new LeadFormFieldModel;
        $data['form_ID']    =   $this->getID();
        $data['user_ID']    =   $this->user_id;
        
        if($data['type']    ==  'state'){
            $states = array_values(Defaults::getStates());
            $data['options']    =   $states;
        }

        return $Field->SaveData($data, NULL, $V, NULL, FALSE, $result);
    }

    function isLegacyForm()
    {
        return $this->data != 'converted';
    }

    /**
     * @return LeadFormFieldModel[]|null
     * @throws Exception
     */
    function getFields(){
        if(is_null($this->_fields)){
            if($this->isLegacyForm()){ // convert legacy to new fields
                $VS =   new ValidationStack;
                $fields =   $this->getFields_legacy();
								if(FALSE === $this->_convertLegacy)
								{
									// return leadformfieldmodel objects
									$returnFields	=	array();
									foreach($fields as $i => $fieldInfo)
									{
										$newField	=	new LeadFormFieldModel();
										$newField->Validate($fieldInfo, new ValidationStack());
										$newField->loadArray($fieldInfo);
										$newField->setID($newField->getInputName());
										$returnFields[]	=	$newField;
									}
									return $returnFields;
								}
								
                if(!$fields){
                    // don't insert any fields, but save converted status anyway
                }else{
									// convert old fields to new fields
                    Qube::GetPDO()->beginTransaction();
                    foreach($fields as $i => $oldfield){
                        $RESULT =   array('error' => false);
                        $oldfield['form_ID']    =   $this->getID();
                        $oldfield['user_ID']    =   $this->user_id;
                        $newField   =   new LeadFormFieldModel();
                        $newField->SaveData($oldfield, NULL, $VS,
                                    NULL, NULL, $RESULT);
                    }
                    Qube::GetPDO()->commit();
                }
                    Qube::Start()->query('UPDATE %s SET data = "converted" WHERE id = %d', $this, $this->getID());
                $this->data =   'converted';
                return $this->getFields();
            }
            // $this->data == converted, and _fields is null, fetch fields
            $this->_fields  =   LeadFormFieldModel::Query('form_ID = %d', $this->getID())
                ->orderBy('sort ASC')->Exec()->fetchAll();//$this->queryLeadFormFields()->Exec()->fetchAll();
        }
        
        return $this->_fields;
    }

    function ConvertLeadForm(){
        $fields = $this->getFields();
        $V = new ValidationStack();
        $new_fields = array();
        /** @var LeadFormFieldModel $field */
        foreach($fields as $field){
            $field->set('ID', 0);
            $field->set('form_ID', $this->getID());
            $field->set('user_ID', $this->user_id);
            $newfield = $field->Save();
            if($newfield){
                $new_fields[] = $field;
            }else{
                $V->addError('error','errors x.x');
            }
        }
        $this->set('version', 2);
        $this->set('data', 'converted');
        $this->Save();
        // If Save is update, always returns false. @todo check that function.
//        if(!$this->Save()){
//            $V->addError('error','errors x.x');
//        };
        return $V;
    }
    
    function updateData(Qube $q){
        static $p = null;
        if(!$p)  $p = $q->GetPDO()->prepare('UPDATE ' . $q->getTable('LeadFormModel') . ' SET data = ? WHERE id = ?');
        
        return $p->execute(array($this->data, $this->id));
    }
    
    function Validate(array &$data, ValidationStack $VS){
        if(isset($data['name']) && empty($data['name'])){
            
            $VS->addError('name', 'You must define a name');
            
        }
        return !$VS->hasErrors();
    }
    
    function getBindings() {
        return parent::getBindings() + array('LeadFormFieldModel'
                => 'form_ID = %d');
    }

    function upgrade(){
        if($this->version == 0){
            $options = explode('||', $this->options);
            $this->show_reset_button = $this->show_reset_button ? $this->show_reset_button : $options[0];
            $this->submit_button_text = $this->submit_button_text ? $this->submit_button_text : $options[1];
            $this->redirect_url = $this->redirect_url ? $this->redirect_url : $options[2];
            $this->ajax_submit = $this->ajax_submit ? $this->ajax_submit : $options[3];
            $this->version = 1;
        }
    }

    /**
     * detect $redirect_url saved in $options;
     *
     * @return mixed
     */
    function getRedirectURL()
    {
        if(!$this->redirect_url)
        {
            $this->upgrade();
        }
        return $this->redirect_url;
    }
}

