<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Nav Model
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class PageModel extends HybridModel {
    
    /**
     * 
     * Table Fields
     */
    /*public $ID,
$oldtbl_ID = 0,
$user_ID,
$hub_ID, $nav_ID, $sortk = 0,
$page_parent_ID = 0,
$feedobject_id = 0,
$page_title,
$page_title_url = '',
$page_region = '',
$page_keywords = '',
$page_seo_title = '',
$page_seo_desc = '',
$page_photo = '',
$page_photo_desc = '',
$page_edit_2 = '',
$page_edit_3 = '',
$inc_contact = '',
$inc_nav = '',
$page_full_width,
$page_adv_css,
$allow_delete,
$has_custom_form,
$has_tags,
$multi_user,
$multi_user_fields,
$default_page,
$outbound_url,
$outbound_target,
$trashed,
$last_edit,
$created;*/
    
    public $id = 0,
$user_id,
$type,
$hub_id, $user_parent_id, $nav_order = 0,
$nav_parent_id = 0,
$nav_root_id = 0,
$page_parent_id = 0,
$feedobject_id = 0,
$page_title,
$page_title_url = '',
$page_region = '',
$page_keywords = '',
$page_seo_title = '',
$page_seo_desc = '',
$page_photo = '',
$page_photo_desc = '',
$page_edit_2 = '',
$page_edit_3 = '',
$inc_contact = '',
$inc_nav = '',
$page_full_width,
$page_adv_css,
$allow_delete,
$has_custom_form,
$has_tags,
$multi_user,
$multi_user_fields,
$default_page,
$outbound_url,
$outbound_target,
$trashed,
$last_edit,
$created;
    
    //protected $_PR_KEY = 'ID';
    protected $_PR_KEY = 'id';
    protected $_defaults    =   array('created' =>  'NOW()');
    /** @var  HubFieldLocks */
    protected $_locks;
    
    function __getColumns() {
        /*static $cols    =   array('ID',
'oldtbl_ID',
'user_ID',
'hub_ID',
            'nav_ID', 'sortk',
'page_parent_ID',
'feedobject_id',
'page_title',
'page_title_url',
'page_region',
'page_keywords',
'page_seo_title',
'page_seo_desc',
'page_photo',
'page_photo_desc',
'page_edit_2',
'page_edit_3',
'inc_contact',
'inc_nav',
'page_full_width',
'page_adv_css',
'allow_delete',
'has_custom_form',
'has_tags',
'multi_user',
'multi_user_fields',
'default_page',
'outbound_url',
'outbound_target',
'trashed',
'last_edit',
'created');*/
        static $cols    =   array('id',
'user_id',
'hub_id',
'user_parent_id', 'nav_order',
'type',
'nav_parent_id',
'nav_root_id',
'page_parent_id',
'feedobject_id',
'page_title',
'page_title_url',
'page_region',
'page_keywords',
'page_seo_title',
'page_seo_desc',
'page_photo',
'page_photo_desc',
'page_edit_2',
'page_edit_3',
'inc_contact',
'inc_nav',
'page_full_width',
'page_adv_css',
'allow_delete',
'has_custom_form',
'has_tags',
'multi_user',
'multi_user_fields',
'default_page',
'outbound_url',
'outbound_target',
'trashed',
'last_edit',
'created');
        return $cols;
    }
    
    /**
     * 
     * @return WebsiteModel
     */
    function getSite(){
        return $this->queryObjects('WebsiteModel')->Exec()->fetch();
    }
    
    static function ImportLegacyPage2(Qube $qube, $old_ID){
        $pdo    =   $qube->GetPDO();
        $pdo->query('REPLACE INTO 6q_hubpages_tbl
                SELECT NULL, id, user_id, hub_id, nav_parent_id, nav_order*10,
                        page_parent_id, feedobject_id, 
                        page_title, page_title_url, page_region, page_keywords, 
                        page_seo_title, page_seo_desc, page_photo, page_photo_desc, 
                        page_edit_2, page_edit_3, inc_contact, inc_nav, page_full_width, 
                        page_adv_css, allow_delete, has_custom_form, has_tags, multi_user, 
                        multi_user_fields, default_page, outbound_url, outbound_target, trashed, 
                        last_edit, created
                FROM hub_page2 WHERE id = ' . $old_ID);
        return $pdo->lastInsertId();
    }
    
    function getBindings() {
        $bindings   =   array(
            //'WebsiteModel'  =>  '/* %d */ id = ' . $this->hub_ID,
            //'NavModel'   =>  "hub_ID = %d",
            'WebsiteModel'  =>  '/* %d */ id = ' . $this->hub_id,
            'NavModel'   =>  "hub_id = %d",
        );
        
        return $bindings;
    }
    
    static function ValidatePage(array &$vars, ValidationStack $vs){
        $vc = VarContainer::getContainer($vars);
        
        if(!$vc->checkValue('page_title', $title) || trim($title) == '')
        {
            $vs->addError('page_title', 'You must provide a valid page title.');
        }
        
        /*
        if(!$vc->checkValue('cid', $cid))
                $vs->addError('cid', 'Valid CID is required');
        
        if(!$vc->checkValue('hub_ID', $hubID))
                $vs->addError('hub_ID', 'Valid hub_ID is required');
        */
        
        return !$vs->hasErrors();
    }

    function Validate(array &$data, ValidationStack $VS)
    {
        $vc = VarContainer::getContainer($data);
        $is_insert = !$vc->checkValue($this->_PR_KEY, $id);
        if($is_insert)
        {
            if(($manager = $this->getEventManager()) && $manager instanceof ModelManager){
                /** @var WebsiteDataAccess $WDA */
                $WDA = $manager->getDA();

				if(!$WDA->isMultiSiteHub($data['hub_id'])){
					if($data['nav_parent_id']){
						if($WDA->getPageParentLocks($data['nav_parent_id'])->isLocked('allow_pages')){
							$VS->addError('Locked', 'New page creation has been disabled for this hub');
						}
					}elseif(HubDataAccess::FromDataAccess($WDA)->getWebsiteLocks($data['hub_id'])->isLocked('allow_pages')){
						$VS->addError('Locked', 'New page creation has been disabled for this hub');
					}
				}
            }
        }else{
            $this->setID($id);
		}
        if($vc->checkValue('outbound_url', $outbound_url) && !empty($outbound_url))
        {
            $VS->ValidateURL('outbound_url', $outbound_url);
        }
        if(isset($data['page_title']) && empty($data['page_title'])){
            $VS->addError ('Create Page', 'Specify a Title for Create Page, first.');
            return false;
        }
        return !$VS->hasErrors();
    }

	function getFieldLocker(){
		if(!$this->_locks)
		{
			$this->_locks	=	new HubFieldLocks ($this->multi_user_fields);
		}

		return $this->_locks;
	}

	function doLockFields(){
		$fieldsJSON	=	$this->Refresh('multi_user_fields')->multi_user_fields;	// get multi_user_fields
		$this->getFieldLocker()->appendLocks(new HubFieldLocks($fieldsJSON));	// add locks from db

		$this->set('multi_user_fields', $this->getFieldLocker());
	}

	function set($c, $n)
	{
		/// catch lock values
		if(preg_match('/^lock_(.*?)$/', $c, $m))
		{
			$this->getFieldLocker()->setLock($m[1], $n);
		}else
			parent::set($c, $n);
	}

    function onBeforeUpdatedEvent(\Zend\EventManager\Event $ev)
    {
        $params = $ev->getParams();

        /** @var WebsiteDataAccess $DA */
        $DA =   $params['DataAccess'];
        $changed_vars   =   &$params['object_vars'];

        // #0 - Initialize some stuff
        $this->doLockFields();

        // #1 - filter data based on locks
        $currentPage    =   $this->Refresh('id, page_parent_id, multi_user');
        $currentPage->applyMultiSitePageLocks($changed_vars, $DA);

        // #2 - Propagate Fields to slave pages.
        unset($changed_vars[$this->_PR_KEY]);
//        $changed_cols = array_intersect($this->_getColumns(), array_keys($changed_vars));
        $DA->PerformPageValuePropagation($this, $changed_vars);
    }

    function onCreatedEvent(\Zend\EventManager\Event $ev)
    {
        /** @var WebsiteDataAccess $DA */
        $DA =   $ev->getParam('DataAccess');
        $DA->CreateNewPageInSlaveHubs($this->getID());
    }

    function onTrashed(\Zend\EventManager\Event $ev)
    {
        // update slave navs/pages
        Qube::Start()->query('UPDATE hub_page2 SET trashed = NOW() WHERE page_parent_id = ' . $this->getID());
    }

	function isMultiSite(){
		return $this->multi_user == 1;
	}

    /**
     * Requires internal fields: multi_user, page_parent_id
     * @param array $data
     * @params WebsiteDataAccess $DA
     */
    public function applyMultiSitePageLocks(array &$data, WebsiteDataAccess $DA)
    {
        $refresh = $this;   // ->Refresh('multi_user, page_parent_id');

        if (!$refresh->isMultiSite() && $refresh->page_parent_id) {
            $locks = $DA->getPageParentLocks($refresh->page_parent_id);
            foreach ($data as $field => $value) {
                if ($locks->isLocked($field)) {
                    unset($data[$field]);
                }
            }
        }
    }
}
