<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/16/14
 * Time: 2:15 PM
 */

class RankingKeywordModel extends HybridModel{
	public $ID;
	public $user_id;
	public $campaign_id;
	public $site_id;
	public $keyword;
	public $created;
	public $google_task_id;
	public $bing_task_id;
	public $blast_google;
	public $last_bing;
	public $trashed;

	function __getColumns() {
		static $cols = array('ID', 'user_id', 'campaign_id', 'site_id', 'keyword', 'created', 'google_task_id', 'bing_task_id', 'last_google', 'last_bing', 'trashed');
		return $cols;
	}
}