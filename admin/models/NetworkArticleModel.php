<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NetworkArticleModel
 *
 * @author amado
 */
class NetworkArticleModel extends NetworkItemLegacyModel {
    
    protected $_PR_KEY = 'id';
    
    protected $_defaults	=	array('created' => 'NOW()');
    public $id  =   NULL;
    
    protected $thumb_id;  // hack
    
    function __getColumns() {
        $cols	=	array("id", "cid", "type", "user_id", "user_parent_id", "tracking_id", 
                "thumb_id", "category", "headline", "summary", "body", "industry", 
                "country", "keywords", "website", "author", "email", "contact", "city", "state", "email_id", 
            "network", "timestamp", "created", "name", "trashed");
				return $cols;
    }
    //put your code here
    
    function Validate(array &$data, \ValidationStack $VS) {
			
			$VC	= VarContainer::getContainer($data);
			if(!$this->getID())
			{
				if(!$VC->check('is_numeric', 'cid', 'user_id', 'user_parent_id'))
								$VS->addError ('id', 'Not all required ids were provided for this model.');
			}

      $this->AcceptImages($VC, $data, 'articles', array('thumb_id'));     
          
			
			return !$VS->hasErrors();
    }
    
    
    function getURL($colkey, $obj_vars, DashboardBaseController $Ctrl)
    {
      switch($colkey){
        case 'thumb_id':
          return $Ctrl->getUser()->getStorageUrl('articles', $obj_vars[$colkey]);
      }
    }
    
   
    function getthumb_id()
    {
        $storage  = UserModel::getStorageURL_ID($this->user_id) . '/articles/'; 
        return $storage . $this->thumb_id;
    }
}
