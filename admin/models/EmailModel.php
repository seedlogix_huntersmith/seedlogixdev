<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 1/29/2015
 * Time: 5:45 AM
 */

abstract class EmailModel extends HybridModel {

    public $body, $subject, $from_field;

    /**
     * @return ResponseEmail
     */
    abstract protected function createNotification($params = NULL);

    /**
     * @param array $params
     *
     * @return ResponseEmail
     */
    function getNotification($params = NULL)
    {
        $response = $this->createNotification($params);
        if(is_array($response)){
            $Email = $response[0];
        }else{
            $Email = $response;
        }
//        $Email = $response instanceof LeadResponse ? $response : $response[0];
////        $Email = $response[0];
        $Email->setSubject($this->subject);
        $Email->setMessage($this->body);
        $Email->setSender(new SendTo(NULL, $this->from_field));

        return $Email;
    }
}