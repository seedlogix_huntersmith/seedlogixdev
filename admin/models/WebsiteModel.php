<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class WebsiteModel extends HubModel implements InputValidator {

    const HUB_TYPE_WEBSITE = 1;

    public $req_theme_type = 1;

    /**
     *
     * @var HubFieldLocks
     */
    protected $_locks = NULL;

    /** IMPLEMENTED VIA mysql view
      function _getCondition($alias, $purpose = self::FOR_SELECT) {
      $parent    =   parent::_getCondition($alias, $purpose);
      return DBWhereExpression::Create($parent)->Where(' req_theme_type = %d', self::HUB_TYPE_WEBSITE);    //
      }
     * 
     */
    function preloadLocksField() {
        $fieldsJSON = $this->Refresh('multi_user_fields')->multi_user_fields; // get multi_user_fields
        $this->getFieldLocker()->appendLocks(new HubFieldLocks($fieldsJSON)); // add locks from db

        $this->set('multi_user_fields', $this->getFieldLocker());
    }

    function getFieldLocker() {
        if (!$this->_locks) {
            $this->_locks = new HubFieldLocks($this->multi_user_fields);
        }

        return $this->_locks;
    }

    function set($c, $n) {
        /// catch lock values
        if (preg_match('/^lock_(.*?)$/', $c, $m)) {
            $this->getFieldLocker()->setLock($m[1], $n);
        } else
            parent::set($c, $n);
    }

    static function GetDomainConfigFromHostInput($domain, $VS = NULL) {
        $hostname = parse_url($domain, PHP_URL_HOST);
        if ($hostname === FALSE)
            $hostname = $domain;

        $DC = DomainConfig::FromHostname($hostname, $VS);
        return $DC;
    }

    function getTouchpointType() {
        return $this->touchpoint_type;
    }

    /**
     * @todo possibly refactor, I remember there are about 10 different functions that do touchpoint type string validation.. theyre all over the place
     * @param $touchpoint_type
     * @param ValidationStack $vs
     * @return bool true no error, false error.
     */
    static function ValidateTouchpointType($touchpoint_type, ValidationStack $vs) {
        // validate touchpoint_Type
        switch ($touchpoint_type) {
            case 'BLOG':
            case 'WEBSITE':
            case 'LANDING':
            case 'PROFILE':
            case 'MOBILE':
            case 'SOCIAL':
                break;
            default: // other
                $vs->addError('touchpoint_type', 'Not a valid touchpoint_type.');
                return !$vs->hasErrors();
        }
        return false;
    }

    function Validate(array &$vars, ValidationStack $vs) {
        $vc = VarContainer::getContainer($vars);
        $is_insert = !$vc->checkValue($this->_PR_KEY, $id);
        $checkName = $vc->exists('name') || $is_insert;
        if ($checkName && (!$vc->checkValue('name', $sitename) || trim($sitename) == '')) {
            $vs->addError('name', 'You must provide a valid name for this site.');
        }

        if ($is_insert) { // if new
            if (!$vc->checkValue('cid', $cid))
                $vs->addError('cid', 'Valid CID is required');

            if (!$vc->checkValue('touchpoint_type', $touchpoint_type)) {
                $vs->addError('touchpoint_type', 'Specify a Touchpoint Type.');
                return !$vs->hasErrors();
            }
            if (FALSE == self::ValidateTouchpointType($touchpoint_type, $vs))
                return $vs;
        }else {
            $this->setID($id); // used by $this->Refresh();
            if ($vc->checkValue('domain', $domain) && !empty($domain) && !Qube::IS_DEV()) {
                $this->ValidateDomainString($vars, $vs, $domain);
            } elseif (Qube::IS_DEV() && !empty($domain)) {
                $vars['httphostkey'] = str_replace('http://www.', '', $vc->get('domain'));
            } elseif ($vc->checkValue('domain', $domain) && $domain == '') {
                $vars['httphostkey'] = '';
            }
        }


        foreach ($vc->getData() as $field => $value) {
            switch ($field) {
                //Validate URLs
                case 'backlink':
                case 'blog':
                    //case 'facebook':
                    //case 'linkedin':
                    if (!empty($value))
                        $vs->ValidateURL($field, $value);
                    break;
                //Validate numeric fields
                case 'zip':
                case 'phone':
                    $phoneStripped = str_replace(array('(', ')', '+', '-', '.', ' '), '', $value);
                    if (!empty($phoneStripped) && !is_numeric($phoneStripped))
                        $vs->addError($field, "Please provide a valid $field");
                    break;
            }
        }

        return parent::Validate($vars, $vs);
    }

    function isMultiSite() {
        return $this->multi_user == 1;
    }

    static function isRequired($varname) {
        switch ($varname) {
            // fields that can't be empty values
            case 'name':
                return true;
        }
        return false;
    }

    function getBindings() {
        static $bindings = array(
            'NavModel' => "hub_id = %d", // uses hub_page2
            'PageModel' => 'hub_id =   %d',
            'PageTreeNodeModel' => 'hub_id = %d AND nav_parent_id = 0'
        );

        return $bindings;
    }

    /**
     * @return NavModel[]
     */
    function getRootNavs() {
        //return $this->queryObjects('NavModel', 'N', 'N.parent_ID = 0')->Exec()->fetchAll();
        return $this->queryObjects('NavModel', 'N', "N.nav_parent_id = 0 AND type='nav' AND trashed = 0")->Exec()->fetchAll();
    }

    function getNodes(DBWhereExpression $Where = NULL) {
        $q = $this->queryObjects('PageTreeNodeModel')->orderBy('nav_order asc');
        if ($Where)
            $q->Where($Where);

        return $q->Exec()->fetchAll();
    }

    function getPageDataInfo() {
        return array('user_id' => $this->user_id,
            'hub_id' => $this->id, 'type' => 'page');
    }

    function newPage() {
        $pageModel = new PageModel;
        $pageModel->loadArray($this->getPageDataInfo());

        $PM = new PageManager($pageModel);

        return $PM->AddToSite($this);
    }

    function newNav() {
        $Nav = new NavModel;

        /* $Nav->loadArray(array('user_ID' => $this->user_id,
          'hub_ID'    => $this->id)); */
        $Nav->loadArray(array('user_id' => $this->user_id,
            'hub_id' => $this->id, 'type' => 'nav'));
        return $Nav;
    }

    function _getDeleteClause() {
        ;
    }

    static function TitlesToPath($titlesarray) {

        array_walk($titlesarray, function (&$val, $index) {
            $val = mb_strtolower(preg_replace('/\s+/', '-', str_replace('-', '--', preg_replace('#[^a-z0-9\s]+#i', '', $val))
            ));
        });

        return join('/', $titlesarray);
    }

    function onLangChange() {
        // translate!!!!!!
        // loop through navs and shit!
        $RootNodes = $this->getNodes();
#	deb($RootNodes);
        foreach ($RootNodes as $Node) {
            $Node->RebuildPagePaths($this->language);
        }
        // endif
    }

    function doTransitionPages(Qube $qube) {

        $pdo = $qube->GetDB();
        $q = new DBSelectQuery($pdo);
        $q->From('hub_page2 p')->Where('type = "nav" AND nav_parent_id = ? AND hub_id = %d AND not exists (select oldtbl_ID from 6q_navs_tbl WHERE oldtbl_ID = p.id)', $this->getID());
        $q->addFields('exists (select id FROM hub_page2 WHERE type="nav" AND nav_parent_id = p.id) as has_children');
        $pStmt = $q->Prepare();

        $origin = (object) array('page_title' => '__ORIGIN__', 'has_children' => 1, 'id' => 0,
                    'nav_parent_id' => 0);

//$pdo->query('CREATE TEMPORARY TABLE newpages_oldpages ( hb2id int(11) NOT NULL, hp_tblid int(11) NOT NULL, primary key (hb2id) ) ');
        PageTreeNodeModel::ImportLegacyNav($origin, 0, 0, $pStmt, $qube);


        $stmt = $pdo->query('insert ignore into 6q_hubpages_tbl
SELECT NULL, id, user_id, hub_id, nav_parent_id, nav_order*10,
	page_parent_id, feedobject_id, 
	page_title, page_title_url, page_region, page_keywords, 
	page_seo_title, page_seo_desc, page_photo, page_photo_desc, 
	page_edit_2, page_edit_3, inc_contact, inc_nav, page_full_width, 
	page_adv_css, allow_delete, has_custom_form, has_tags, multi_user, 
	multi_user_fields, default_page, outbound_url, outbound_target, trashed, 
	last_edit, created
    FROM hub_page2 t WHERE t.type = "page" AND not exists
    (select oldtbl_ID from 6q_hubpages_tbl where oldtbl_ID = t.id) and t.hub_id = ' . $this->getID());

        if ($stmt->rowCount()) {
            $qube->query('UPDATE 6q_hubpages_tbl SET nav_ID = (SELECT ID from 6q_navs_tbl WHERE oldtbl_ID = nav_ID) WHERE hub_ID = ' . $this->getID());
        }
        $this->onLangChange();

        return;
    }

    function onBeforeUpdatedEvent(\Zend\EventManager\Event $ev) {
        // #0 - setup lock fields that will be modified by ->set($lock, $lockstatus)
        $this->preloadLocksField();

        $params = $ev->getParams();
        /** @var HubDataAccess $hubDataAccess */
        $hubDataAccess = $params['DataAccess'];   // provided by ModelManager.
        $changed_vars = &$params['object_vars'];

        // #1 - Verify/Sanitize Locks/** @var HubDataAccess $hubDataAccess */
        $currentSite = $this->Refresh('multi_user, hub_parent_id, id');
        $currentSite->applyMultiSiteLocks($changed_vars, $hubDataAccess);

        // #2 - Replicate Field Updates to Slave Sites. (if site being updated is a multisite master)
        $isMultiSite = $currentSite->isMultiSite();
        if (!$isMultiSite)
            return;   //
        unset($changed_vars[$this->_PR_KEY]);
        $changed_cols = array_intersect($this->_getColumns(), array_keys($changed_vars));
        $Original = $this->Refresh();

        foreach ($changed_cols as $col) {
            $hubDataAccess->UpdateChildHubsColumnValue($Original, $col, $changed_vars[$col], 1, $changed_vars);
        }
    }

    /**
     * @param array $vars
     * @param ValidationStack $vs
     * @param $domain
     * @throws QubeException
     */
    public function ValidateDomainString(array &$vars, ValidationStack $vs, $domain) {
        $dc = static::GetDomainConfigFromHostInput($domain, $vs);
        $validate_options = array('ALLOW_UNREGISTERED' => TRUE);
        if ($dc instanceof DomainConfig) {

            $domain_owner_info = $this->Refresh('user_ID, user_parent_ID', PDO::FETCH_OBJ);

            // check permissions
            $permission_check = $this->getQube()->queryObjects('WebsiteModel', 'T', 'T.httphostkey LIKE :host AND (user_ID != :user_ID)')
                            ->Fields('T.user_parent_ID, T.httphostkey')->Prepare();
            $permission_check->execute(array('host' => $dc->getDomainName(),
                'user_ID' => $domain_owner_info->user_ID));

            $domain_info = $permission_check->fetch(PDO::FETCH_OBJ);
            //$domain_info = FALSE;
# deb($domain_owner_info, $id, $this);
            // if the domain was not found in the system, or it belongs to the parent account.
            if ($domain_info === FALSE || $domain_info->user_parent_ID === $domain_owner_info->user_parent_ID) {
                //if ($dc->Validate($validate_options, $vs)) {
#                            $domain =   $dc->getDomainName();
                    $vars['httphostkey'] = str_replace('www.', '', $dc->getHostname());//$domain);
                //}
            } else if(strpos($domain_info->httphostkey, 'localmashup.com') !== false) {
                // no permission to domain.
                $vars['httphostkey'] = str_replace('www.', '', $dc->getHostname());//$domain);
            } else {
                // no permission to domain.
                $vs->addError('domain', 'This domain is in use by another user.');
            }
        }
    }

    /**
     * Requires internal vars: hub_parent_id, multi_user, ID
     * Unsets variables in &$vars based on parent locks.
     *
     * @param array $vars
     * @param $manager
     * @param $field
     * @param $value
     */
    public function applyMultiSiteLocks(array &$vars, HubDataAccess $DA) {
        // check locks only for non-masters (is not a MULTISITE (master) and has a parent)
        if (!$this->isMultiSite() && $this->hub_parent_id) {
            $locks = $DA->getWebsiteLocks($this->getID());
            foreach ($vars as $field => $value) {
                if ($locks->isLocked($field)) {
                    unset($vars[$field]);
                }
            }
        }
    }

}
