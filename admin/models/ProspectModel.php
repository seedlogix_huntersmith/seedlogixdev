<?php
/**
 * Represents 6q_prospects_campaigns tbl.
 * ^ - tbl is a union of leads and contacts (contact_form)
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

class ProspectModel extends HybridModel {    
    
    public $ID, $touchpoint_ID, $cid, $form_id, $grade, $name, $table, $source, $contact_ID,
            $email, $phone, $company, $TP_TYPE, $TP_NAME, $TP_PAGE, 
            $keyword, $search_engine, $created, $lead_ID, $contactform_ID, $user_ID, $user_parent_ID, $ip, $import, $visitor_ID, $hub_ID;
    protected $_PR_KEY = 'ID';
    
    static function getTransports() {
        return array('hub' => 'WebsiteModel');
    }
    
    function getData() {
//        if($this->table == 'contact_form') return false;

        //return Qube::GetPDO()->query('SELECT label, valuestr FROM leads_datax WHERE lead_id = ' . $this->lead_ID)
                //->fetchAll(PDO::FETCH_KEY_PAIR);
		return Qube::GetPDO()->query('SELECT label, value FROM leads_data WHERE lead_id = ' . $this->lead_ID)
                ->fetchAll(PDO::FETCH_KEY_PAIR);
    }

	function getTypeID() {
		if($this->lead_ID != 0) return $this->lead_ID;
		return $this->contactform_ID;
	}
    /**
     *
     * @var PDOStatement
     */
//    protected static $insert_statement;   
    
    
    function __getColumns() {
        static $cols    =   array('ID', 'touchpoint_ID', 'cid', 'form_ID', //'table', 'transport',
                'keywords', 'searchengine', 'ip', 'contactform_ID', 'contact_ID', 'lead_ID', 'ip', 'import', 'source', 
//                'transport_ID',
						'grade', 'name', 'email', 'phone', 'company', 'created', 'user_ID', 'user_parent_ID', 'visitor_ID', 'hub_ID');
        return $cols;
    }
    
    function getGrade() {
		if($this->contactform_ID || $this->lead_ID){
			if($this->grade >= 9):
				return 'A';
			elseif($this->grade >= 8):
				return 'B';
			elseif($this->grade >= 7):
				return 'C';
			else:
				return 'D';
			endif;
		} else {
			if($this->name && $this->email && $this->phone && $this->company):
				return 'A';
			elseif($this->name && $this->email && $this->phone):
				return 'B';
			elseif(($this->name && $this->email) || ($this->name && $this->phone)):
				return 'C';
			else:
				return 'D';
			endif;			
		}
			
    }
    
    function getColor() {
		
		if($this->contactform_ID || $this->lead_ID){
			 if($this->grade >= 9):
				return 'dataNumGreen';
			elseif($this->grade >= 8):
				return 'dataNumCyan';
			elseif($this->grade >= 7):
				return 'dataNumIndigo';
			else:
				return 'dataNumRed';
        	endif;
		} else {
			if($this->name && $this->email && $this->phone && $this->company):
				return 'dataNumGreen';
			elseif($this->name && $this->email && $this->phone):
				return 'dataNumCyan';
			elseif(($this->name && $this->email) || ($this->name && $this->phone)):
				return 'dataNumIndigo';
			else:
				return 'dataNumRed';
			endif;			
		}
    }
		
    function convertToContact()
    {
        // assoc reference map of contactmodel keys to prospect keys
        $data = array('id' => 'ID', 
                    'cid' => 'CID',
                    'user_id' => '',
                    'first_name' => '');
        $CM	= new ContactModel();
        $CM->load(array(''));
    }

	protected function _Trash(Qube $q, $andwhere = 'user_id = dashboard_userid()', $trash = 0)
	{
		/*if(!$this->getTypeID())
			return false;*/

		if(parent::_Trash($q, $andwhere, $trash)){
			
		
				if($this->lead_ID){
					return $q->squery('UPDATE leads SET trashed = %s WHERE ID = %d AND user_ID = %d', $trash ? 'NOW()' : 0, $this->lead_ID, $this->user_ID);
				}elseif($this->contactform_ID){
					return $q->squery('UPDATE contacts SET trashed = %s WHERE ID = %d AND user_ID = %d', $trash ? 'NOW()' : 0, $this->lead_ID, $this->user_ID);
				}
	
			
		}
		return false;
	}

	public function isContact(){
		return $this->contactform_ID ? true : false;
	}

	public function isLead(){
		return $this->lead_ID ? true : false;
	}

	/**
	 * @return array
	 */
	public function getProspectData(){
		if($this->isLead()){
			$leadID = intval($this->lead_ID);
			$q = "SELECT coalesce(`6q_formfields_tbl`.label, leads_datax.label) AS label, ".
				"IF(leads_datax.valueint, leads_datax.valueint, leads_datax.valuestr) AS value ".
				"FROM leads_datax LEFT JOIN `6q_formfields_tbl` ".
				"ON leads_datax.field_ID = `6q_formfields_tbl`.ID  ".
				"WHERE lead_ID = $leadID";
            $q .= " UNION SELECT label, value FROM leads_data d WHERE d.lead_ID = $leadID";
			return $this->getQube()->query($q)->fetchAll(PDO::FETCH_KEY_PAIR);
		}elseif($this->isContact()){
			$contactID = intval($this->contactform_ID);
			$q = "SELECT name `Name`, email `E-mail`, phone Phone, website Website, comments Comments, page FROM contact_form WHERE id = $contactID";
			return $this->getQube()->query($q)->fetch(PDO::FETCH_ASSOC);
		}else{
			return array();
		}


	}

    public function getFirstTouch(){
        $firstProspect = "SELECT P.*, lead_forms.name FROM (
                        SELECT created, form_ID, cid FROM prospects WHERE visitor_ID = :piwikVisitorID AND user_ID = {$this->user_ID} AND visitor_ID != '' ORDER BY prospects.created ASC LIMIT 1
                        ) P " .
                            "LEFT JOIN lead_forms " .
                            "ON P.form_ID = lead_forms.id";
        $stmt = $this->getQube()->GetDB()->prepare($firstProspect);
        $stmt->execute(array('piwikVisitorID' => $this->visitor_ID));
        return $stmt->fetchObject('ProspectModel');
    }

    public function getLastTouch(){
        if($this->visitor_ID != null){
            $firstProspect = "SELECT P.*, ifnull(hub_page2.page_title, hub.name) name, if(hub_page2.page_title IS NULL, 0, 1) is_page,  ifnull(hub_page2.id, hub.id) page_id, hub.touchpoint_type FROM (SELECT created, page_ID, touchpoint_ID FROM prospects WHERE visitor_ID = :piwikVisitorID AND user_ID = {$this->user_ID} ORDER BY prospects.created DESC LIMIT 1) P " .
                "LEFT JOIN hub_page2 " .
                "ON P.page_ID = hub_page2.id " .
                "LEFT JOIN hub " .
                "ON P.touchpoint_ID = hub.touchpoint_ID";

            $stmt = $this->getQube()->GetDB()->prepare($firstProspect);
            $stmt->execute(array('piwikVisitorID' => $this->visitor_ID));
            return $stmt->fetchObject('ProspectModel');
        }

        return null;
    }

    public function getTouchCreatedLabel(){
        /*$diff = DateTime::createFromFormat('Y-m-d H:i:s', $this->created)->setTime(0,0,0)->diff(date_create()->setTime(0,0,0));

        if($diff->d < 1)
        {
            return "Today";
        }elseif($diff->d < 2)
        {
            return "Yesterday";
//        }elseif($diff->d < 7){
        }elseif($diff->m == 0){
//            $weeks = $diff->d / 3;
            return "$diff->d Days Ago";
//            return "$weeks Week Ago";
        }elseif($diff->y == 0){
            return "$diff->m Months Ago";
        }else{
            return "$diff->y Years Ago";
        }*/
			$time = strtotime($this->created);
		    $time = time() - $time; // to get the time since that moment
			$time = ($time<1)? 1 : $time;
			$tokens = array (
				31536000 => 'year',
				2592000 => 'month',
				604800 => 'week',
				86400 => 'day',
				3600 => 'hour',
				60 => 'minute',
				1 => 'second'
			);

			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
			}
		
    }
}


