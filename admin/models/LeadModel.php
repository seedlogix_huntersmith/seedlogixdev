<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class LeadModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id, $user_id, $parent_id, $name, $phone, $lead_form_id, $hub_id, $hub_page_id, $search_engine, $source,
							$lead_name, $lead_email, $cid;
    
		protected $_data	=	NULL;
		
		protected $_PR_KEY	= 'id';
    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id', 'cid', 'user_id', 'parent_id', 'lead_name', 'lead_email', 'lead_form_id', 'source',
            'hub_id', 'hub_page_id', 'search_engine', 'lead_name', 'lead_email');
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
    
    function meta($var, $default    =   'N/A')
    {
#        var_dump($var, $this->$var);
        if(trim($this->$var)  !=  '') return $this->$var;
        return $default;
    }
		
		function getData(LeadsDAO $DAO)
		{
			if(!$this->_data)
			{
				$this->_data	=	$DAO->getLeadFieldsData($this->getID());
			}
			return $this->_data;
		}

	/**
	 * Return the email with format
	 * [name &lt;email&gt;] used in email headers
	 * @return string
	 */
	function getEmailWithFormat(){
		if($this->name)
			return "$this->name <$this->lead_email>";

		return $this->lead_email;
	}
}

