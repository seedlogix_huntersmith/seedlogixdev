<?php
/**
 * AutoresponderTheme Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class AutoResponderTheme extends DBObject {
    
    protected $id, $theme_name, $html;
    
    protected $_variables = array();
    
    function _getSelectColumns() {
        return array('id', 'theme_name', 'html');
    }
    
    function _getDeleteClause() {
        ;
    }
    
    function setVariables($vars)
    {
        $this->_variables = $vars;
    }
    
    function getVariables()
    {
        return $this->_variables;
    }
    
    function parse($matches)
    {
      if($matches[1] == 'BLOGSTREAM')
      {
      
      
      }
      
    }
    
    function ReplaceVars(Responder $r, $othervars = array())
    {
        // @todo make this automagic (bind variable names to real database column names)
        $row = array_merge($othervars, $this->_variables, $r->toArray());
        
        $tablename = $row['table_name'];
        $tableid = $row['table_id'];

        // apprently this piece of code replaces the 'logo' variable
        // somewhat arbitrary, wuold rather use a left join, but lets roll with this for now.
        // this will also get really intensive in the cron when processing batch notifications.
        
        /** disabled because it makes no sense! lead_forms does not have such columns.. so what is the use of this?
         * Finally made sense of it. The statement is designed to fail.  ok. got iit. not good, but lets roll with it. */
        
        
	$query = "SELECT logo, phone FROM {$tablename} WHERE id = {$tableid}";
        try{
            $row2 = Qube::GetPDO()->query($query)->fetch(PDO::FETCH_ASSOC);
            $row2['logo'] = 'http://'.$row['main_site'].'/users/'.UserModel::GetUserSubDirectory($row['user_id']).'/'.$row['table_name'].'/'.$row2['logo'];
        }catch(PDOException $badquery)
        {
            // @see get-template line 55, replaced https with http
            
            $row2['logo'] = 'http://'.$row['main_site'].'/users/'.UserModel::GetUserSubDirectory($row['user_id']).'/hub_page/'.$row['logo'];            
        }
	
        // end
        $html = $this->html;
	$imgLink = 'http://'.$row['main_site'].'/';
        
//        deb($imgLink)
        
#        $html = preg_replace_callback('/\[\[([A-Z0-9]+)\]\]/', array($this, 'parse'), $html);
        $tags = array('[[BODY]]', '[[BGCOLOR]]','[[LINKSCOLOR]]','[[ACCENTCOLOR]]','[[TITLESCOLOR]]','[[CALLTOACTION]]','[[LOGO]]','[[TABLEID]]','[[TABLENAME]]','[[USER]]','[[PHONE]]','[[EDIT1]]','[[EDIT2]]','[[IMGLINK]]'); 
        $fields = array($row['body'], $row['bg_color'],$row['links_color'],$row['accent_color'],$row['titles_color'],$row['call_action'],$row2['logo'],$row['table_id'],$row['table_name'],$row['user_id'],
                $row2['phone'],$row['edit_region1'],$row['edit_region2'],$imgLink);
        
        // okay, so the htmlentities is used
        // for returning the code via ajax,.. its not necessary for anything else.
        // so I have disabled it bellow
#        $newhtml = htmlentities(str_replace($tags, $fields, $html));
        $htmlup = str_replace($tags, $fields, $html);
//        deb($fields, $row, 'html:', $html);
        
        return $htmlup;
    }
}
