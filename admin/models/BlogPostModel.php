<?php

/**
 * BlogPostModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class BlogPostModel extends HybridModel {

    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    public $id, $post_title, $blog_id, $post_content, $publish_date, $trashed, $category_url,
            $post_title_url, $user_parent_id, $post_edit_1, $post_desc, $photo, $video_embed, $author, $author_url, $profile_connect,
            $user_id, $seo_title, $master_ID;
    protected $_PR_KEY = 'id';
    protected $_defaults = array('created' => 'now()');

    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c = array('created', 'blog_id', 'id', 'post_title', 'post_content', 'post_edit_1', 'trashed', 'publish_date', 'category', 'network', 'profile_connect',
            'tags', 'tags_url', 'publish_date',
            'post_title_url', 'category_url', 'user_parent_id', 'author', 'author_url', 'post_desc', 'photo', 'video_embed', 'user_id', 'seo_title', 'master_ID');
        return $c;
    }

    function _getDeleteClause() {
        ;
    }

    function _getColumns($alas = NULL, $purpose = self::FOR_INSERT, &$columns = NULL) {
        $cols = parent::_getColumns($alas, $purpose, $columns);
        if ($purpose == self::FOR_UPDATE)
            unset($cols['created']);

        return $cols;
    }

    function setf($var, $val) {
        if ($var == 'publish_date') {
            $val = date('Y-m-d', strtotime($val));
        }
        return parent::set($var, $val);
    }

    function doAfterUpdate(array $updatedata, BlogDataAccess $da, ModelManager $MM) {
        // figure out if I'm a master....
        if ($this->master_ID != 0 and $this->master_ID != $this->getID())
            return; // nothing to do

            
// from here on, I am a master post being modified.
        if (isset($updatedata['feed_name'])) {
            $feed_ID = NULL;
            $result = $da->getFeeds(array('user_ID' => $this->user_id, 'name' => $updatedata['feed_name']), 'name LIKE :name');
            if (!$result) {
                // create a new feed
                $feed_ID = $MM->doSaveBlogFeed(array('name' => $updatedata['feed_name']), 0, $da)->getID();
            } else {
                $feed_ID = array_pop($result)->getID();
            }

            $updatedata['feed_ID'] = $feed_ID;
        }
        if (isset($updatedata['feed_ID'])) {

            // currently only support 1 post-feed relationship
            $da->clearFromFeeds($this->getID());

            // we have the id.. add post to feed
            $da->addPostToFeed($this, $updatedata['feed_ID']);
        }
        if ($da->isMasterPost($this->getID())) {
            $da->updateChildPosts($updatedata, $this);
        }
        return $this;
    }

    function Validate(array &$data, ValidationStack $VS) {
        if (isset($data['publish_date'])) {
            $data['publish_date'] = date('Y-m-d', strtotime($data['publish_date']));
#		if(preg_match('/^([0-9])+\/([0-9]+)\/([0-9]{4}$/', $data['publish_date'], $matches))			$data['publish_date']	=	$matches[
        }
        if (!isset($data['blog_id']) && !isset($data['id'])) {
            $VS->addError('blog', 'Specify a blog, first.');
            return false;
        }

//            if(isset($data['tags_url']) && !empty($data['tags_url'])){
//                return $VS->ValidateURL('tags_url', $data['tags_url']);
//            }

        if (isset($data['post_title']) && empty($data['post_title'])) {
            $VS->addError('Create Post', 'Specify a Title for Create Post, first.');
            return false;
        }

        return true;
    }

    function getPostURL($withDomain = true) {
//        return 'http://6qube.com/';
        return ($withDomain ? "http://{$this->httphostkey}" : "") . "/$this->category_url/$this->post_title_url/";
    }

    static function getPhotoStorage($user_ID, $create = TRUE) {
        $path = UserModel::getStorage($user_ID) . '/blog_post';
        if ($create)
            FileUtil::CreatePath($path);
        return $path;
    }

    /**
     * 
     * returns an absolute url 
     * @param type $user_id
     * @param type $photo
     * @return string
     */
    static function getPhotoURLPath($row) {
        return '/' . UserModel::getStorageURL_ID($row['user_id']) . '/blog_post/' . $row['photo'];
    }

    public function isMaster() {
        if ($this->master_ID == null) {
            throw new QubeException("blog masterID not initialized");
        }

        return $this->id == $this->master_ID;
    }

    function Trash(Qube $q, $andwhere = '') { //user_id = dashboard_userid()')
        $q->GetDB()->beginTransaction();
        $success = parent::Trash($q, $andwhere);
        if ($this->isMaster()) {
            $statement = $q->GetPDO()->prepare('UPDATE blog_post SET trashed = NOW() WHERE master_ID = :master_ID AND user_id = :user_id');
            $statement->execute(array('master_ID' => $this->master_ID, 'user_id' => $this->user_id));
        }
        $q->GetDB()->commit();

        return $success;
    }

}
