<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PageRowModel
 *
 * @author amado
 */
class PageRowModel extends ArrayObject {

	protected $translatefunc = NULL;
	protected $_translations = array();

	// fields to be parsed for |@form-#whatever#@|
	static function getFormTagFields()
	{
		return array('page_region', 'page_edit_2');
	}

	static function getParsableFields(){
		return array('page_region', 'page_edit_2', 'page_title', 'page_seo_desc', 'page_seo_title', 'page_keywords', 'page_region', 'page_edit_2');
	}

	function getValue($index)
	{
		return parent::offsetGet($index);
	}
	
	function offsetGet($index) {

		if (!$this->offsetExists($index))
			return '';
		
		$RAWVALUE = $this->getValue($index);

		if ($this->translatefunc) {
			if (isset($this->_translations[$index]))
				return $this->_translations[$index];
			$translation = call_user_func($this->translatefunc, $RAWVALUE, $index, $this);
			return $translation;
		}
		return $RAWVALUE; //($this->translatefunc ? call_user_func($this->translatefunc, $ret, $index) : $ret);
	}

	function setTranslator($func) {
		$this->translatefunc = $func;
		return $this;
	}
	
	function getTranslator(){
		return $this->translatefunc;
	}

	function loadTranslations($tbl, $ID, $lang, Qube $q) {
		$st = $q->query('SELECT `key`, translation FROM 6q_contenttranslations_tbl WHERE language = "' . $lang . '" AND tbl = "' . $tbl . '" AND ID = ' . $ID);
		$this->_translations = $st->fetchAll(PDO::FETCH_KEY_PAIR);
		return $this;
	}

	function saveTranslation($key, $translation, $tbl, $ID, $user_ID, $language, Qube $q) {
		$p = $q->GetDB()->prepare('REPLACE INTO 6q_contenttranslations_tbl SET language = ?, tbl = ?, ID = ?, `key` = ?, user_ID = ?, translation = ?');
		return $p->execute(array($language, $tbl, $ID, $key, $user_ID, $translation));
	}

	function replaceTags(Hub $legacy, HubModel $hub, HubRowModel $HR)
	{
		$hub_row = (array) $hub;
		$bda	=	new BlogDataAccess();
		$searchFields = array('page_region', 'page_keywords', 'page_seo_desc', 'page_photo_desc', 'page_edit_2', 'page_seo_title', 'page_edit_2', 'page_photo_desc');
		$replaceValues = array('city' => $hub_row['city'], 'state' => $hub_row['state'],
				'citycommastate' => $hub_row['city'] . ', ' . $hub_row['state'],
				'company' => $hub_row['company_name'], 'address' => $hub_row['street'],
				'phone' => $hub_row['phone'], 'zip' => $hub_row['zip'],
				'slogan' => $hub_row['slogan'], 'domain' => $hub_row['domain'],
				'coupon' => $hub_row['coupon_offer'], 'google_webmaster' => $hub_row['google_webmaster'],
				'google_tracking' => $hub_row['google_tracking'], 'keyword1' => $hub_row['keyword1'],
				'keyword2' => $hub_row['keyword2'], 'keyword3' => $hub_row['keyword3'],
				'keyword4' => $hub_row['keyword4'], 'keyword5' => $hub_row['keyword5'],
				'keyword6' => $hub_row['keyword6'], 'edit1' => $hub_row['overview'],
				'edit2' => $hub_row['offerings'], 'edit3' => $hub_row['photos'],
				'edit4' => $hub_row['videos'], 'edit5' => $hub_row['events'],
				'edit6' => $hub_row['edit_region_6'], 'edit7' => $hub_row['edit_region_7'],
				'edit8' => $hub_row['edit_region_8'], 'edit9' => $hub_row['edit_region_9'],
				'edit10' => $hub_row['edit_region_10'],
				'userid' => $hub_row['user_id'], 'logo' => $hub_row['logo'], 'logopath' => 'http://6qube.com/users/' . UserModel::GetUserSubDirectory($hub_row['user_id']) . '/hub/' . $hub_row['logo'] . '',
				'profilepath' => 'http://6qube.com/users/' . UserModel::GetUserSubDirectory($hub_row['user_id']) . '/hub/' . $hub_row['profile_photo'] . '',
				'citylowercase' => trim(strtolower(str_replace(' ', '', $hub_row['city']))),
				'incitycommastate' => 'in ' . $hub_row['city'] . ', ' . $hub_row['state'],
                                'fullstate' => HubRowModel::getFullState($hub_row['state']),
				'navname-children' => '', 'blogfeed' => array($HR, 'getVar'));
		$legacy->replaceTags($this, $searchFields, $replaceValues);
	}

	public function getImageUrl($colname, $baseUrl = ""){
		$value = $this->getValue($colname);
		if(strpos($value, "/") === FALSE){
			return $baseUrl . "/" . UserModel::getStorageURL_ID($this['user_id']) . "/hub_page/" . $value;
		}

		return UserModel::USER_DATA_DIR . $value;
	}
}
