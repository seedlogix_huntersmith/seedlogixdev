<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Campaign Model
 *
 * @author Jon Aguilar
 */
class CampaignModel extends HybridModel {
    
    /**
     * 
     * Table Fields
     */
    public $id, $user_id, $name, $date_created, $default_snapshot_id;
    protected $_PR_KEY = 'id';
    
    function Validate(array &$vars, ValidationStack $vs){
        $vc = VarContainer::getContainer($vars);
        
        if(!$this->getID())   // if new
        {
            if(!$vc->checkValue('name', $campname) || !preg_match('/\w{1}/', $campname))
            {
                $vs->addError('name', 'Campaign Name must be at least 1 character in length.');
            }
        }
        
//        
//        if(!$vc->checkValue('user_id', $cid))
//                $vs->addError('user_id', 'User ID?');
        
        return !$vs->hasErrors();
    }
    
    function __getColumns() {
        static $cols    =   array('id', 'user_id', 'name', 'date_created', 'default_snapshot_id', 'user_parent_ID', 'trashed');
        return $cols;
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
            
    function hasReseller()
    {
        // return ture if user HAS reseller parent
        // or it is a reseller themselves.
        
        // this logic is a little sketchy
        return ($this->parent_id || $this->class == 17);
            // @todo why class == ' 17' ? 
            
    }
    
    function getBindings() {
        static $bindings   =   array(
            
            'LeadFormModel'   =>  "T.cid = %d",
            'LeadModel'   =>  "T.cid = %d",
            'ContactDataModel'   => 'T.cid = %d',
            'ResponderModel' => 'T.cid = %d',
            'ProspectModel' =>  'T.cid = %d',
            'ContactModel' =>  'T.cid = %d',
						'NetworkArticleModel'	=>	'T.cid	=	%d',
						'PressArticleModel'	=>	'T.cid	=	%d',
            'DirectoryArticleModel'	=>	'T.cid	=	%d'
            
        );
        
        return $bindings;
    }
    
    /**
     * returns total touch points for campaign when joined with dashboard_touchpoints and select count(*) blogs
     * 
     * @return type
     */
    function getTotalTouchpoints(){
        return (int)$this->TOTAL_TOUCHPOINTS + (int)$this->BLOG;
    }
        
    /**
     *
     * @return Reseller
     */
    function getReseller()
    {
        // @todo once I get a better view of the whole system
        // this needs to be converted to a left-join.
        
        /**
         * get reseller data 
         */
        
        require_once 'Reseller.php';
        
        $reseller = NULL;
        if($this->hasReseller())
        {
            $reseller = Qube::GetDriver()->getResellerWhere('admin_user = %d OR admin_user = %d', $this->id, $this->parent_id);
        }
//        deb($this, ' does this user have a reseller? ', $this->hasReseller(), $this->parent_id, $this->class);
        return $reseller ? $reseller : self::get6QubeReseller();
    }

    /**
     *
     * 
     * @return Reseller 
     */
    private static function get6QubeReseller()
    {
        
        $R = new Reseller;
        $R->_set('main_site', '6qube.com');
        $R->_set('company', '6qube');
        
        return $R;
    }
    
    /** pseudo function for left-joined user_class stuff **/
    function hasEmailLimit()
    {
        // isset returns false even if email_limit is declared as NULL
        // if(($this->email_limit)) throw new Exception('the variable is undefined');
        
        if(is_null($this->email_limit) || $this->email_limit == -1)
                    return false;
        
        return true;
    }
}
