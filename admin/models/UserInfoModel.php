<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * User Model
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class UserInfoModel extends DBObject {
    
    /**
     * 
     * Table Fields
     */
    public $id, $user_id, $parent_id, $firstname, $lastname, $email, $phone, $company, 
            $address, $address2, $city, $state, $zip, $social, $profile_photo, $bio, $referral_id,
            $recruiter_id, $biz_contact_name, $biz_company_name, $biz_phone_field, $website_url, 
            $biz_address, $biz_city, $biz_state, $biz_zip, $api_id, $api_id_2, $license_info, $license_info_2,
        $hours, $user_manager, $account_manager, $account_number, $date_field,
        $facebook, $twitter, $google, $linkedin, $instagram, $youtube, $blog, $profile_override, $setup_details,
        $custom1, $custom2, $custom3, $custom4, $custom5, $custom6, $custom7, $custom8, $custom9, $custom10, $custom11, $bank_name, $routing_number, $baccount_number,
        $pinterest, $color_code;
    
    protected $_PR_KEY  =   'id';
    
    
    function __getColumns() {
        return array('id', 'user_id', 'parent_id', 'firstname', 'lastname', 'email', 'phone', 'company', 'address',
            'address2', 'city', 'bio', 'state', 'zip', 'social', 'profile_photo', 'referral_id', 'recruiter_id',
            'tcity', 'license_info', 'license_info_2', 'hours', 'biz_contact_name', 'biz_company_name',
            'biz_phone_field', 'website_url', 'biz_address', 'biz_city', 'biz_state', 'biz_zip', 'api_id',
            'api_id_2', 'user_manager', 'account_manager', 'account_number', 'date_field', 'facebook', 'twitter',
            'google', 'linkedin', 'instagram', 'youtube', 'blog', 'profile_override', 'setup_details', 'custom1',
            'custom2', 'custom3', 'custom4', 'custom5', 'custom6', 'custom7', 'custom8', 'custom9', 'custom10', 'custom11', 'bank_name', 'routing_number', 'baccount_number',
            'pinterest', 'color_code');
    }
    
    static function Validate($vars, ValidationStack &$vs){
        $vc = VarContainer::getContainer($vars);
        
        
        if(!$vc->checkValue('username', $username) || filter_var($username, FILTER_VALIDATE_EMAIL) === FALSE)
        {
            $vs->addError('username', 'You must provide a valid email address.');
        }
        
        return !$vs->hasErrors();
    }
    
    static function ValidateField($field, $value){
        static $cols;
        
        if(!$cols){
            $model  =   new \UserInfoModel;
            $cols   =  $model->__getColumns ();
        }
        
        if(!in_array($field, $cols)) return false;
        
        return true;
    }
}
/*
class QubeActionEvent
{
    function __construct($query)
    {
        
    }
}
 * 
 */