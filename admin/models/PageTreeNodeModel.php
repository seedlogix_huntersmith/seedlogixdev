<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PageTreeNode
 *
 * @author amado
 */
class PageTreeNodeModel extends HybridModel {
    //put your code here
    
    public $type =   'page';
    //public $ID, $hub_ID, $parent_nav_ID, $name, $user_ID, $oldtbl_ID, $created;
    public $id, $hub_id, $nav_parent_id, $page_title, $user_id, $created, $nav_order;
    
    function __getColumns() {
        //return array('ID', 'type', 'hub_ID', 'parent_nav_ID', 'oldtbl_ID', 'user_ID', 'name', 'created');
        return array('id', 'type', 'hub_id', 'nav_parent_id', 'user_id', 'page_title', 'created', 'nav_order');
    }
    
    function getChildren(){
        if($this->isPage()) return array();
        
        /*return Qube::Start()->queryObjects('PageTreeNodeModel', 'T',
                    'hub_ID = %d and parent_nav_ID = %d', $this->hub_ID, $this->ID)
                ->orderBy('sortk ASC')->Exec()->fetchAll();*/
        return Qube::Start()->queryObjects('PageTreeNodeModel', 'T',
                    'hub_id = %d AND nav_parent_id = %d AND trashed = 0', $this->hub_id, $this->id)
                ->orderBy('nav_order ASC')->Exec()->fetchAll();
    }
    
function isNav(){
		return $this->type == 'nav';
	}

    function isPage(){
        return $this->type == 'page';
    }
    
    function RebuildPagePaths($lang =   '', $TitlesStack    =   array()){
        
        $CurItemTitle   =   $this->name;
        // I am a page.
        if($lang){
            $NewCurItemTitle    = TranslationApi::GetTranslation($this->name, $lang);
            if($NewCurItemTitle)
                $CurItemTitle   =   $NewCurItemTitle;
//            $CurItemTitle   =   $CurItemTitle . '-' . $lang;
        }
        
        if(!$CurItemTitle) throw new Exception('Translation Failed.');
        
        $TitlesStack[]  =   $CurItemTitle;

        $URLToObject  =   WebsiteModel::TitlesToPath($TitlesStack);


	// save title translation! :)
		if($lang){
			$PRM	=	new PageRowModel();
			$PRM->saveTranslation('page_title', $CurItemTitle, $this->type, $this->oldtbl_ID, 
				$this->user_ID, $lang, Qube::Start());
		}        

        if($this->isPage()){
            // translate default page?
            PathIndexModel::CreateIndex($this->hub_ID, $URLToObject, $this->getID(), Qube::Start());
		return;
        }

	if($this->isNav()){
		Qube::Start()->Save(new NavModel, array('ID' => $this->getID(),
			'name_url' => $URLToObject), 'ID = %d', $this->getID());
	}
        
        foreach($this->getChildren() as $Node){
            $Node->RebuildPagePaths($lang, $TitlesStack);
        }
    }
    
    static function ImportLegacyNav($nav_node, $level, $parent_ID, $pStmt, $qube, $stack = array()){
        
#            echo str_repeat("-", $level) ."$nav_node->id, (parent=$nav_node->nav_parent_id) $nav_node->page_title has_children = $nav_node->has_children \n";
    // create node
    $node_ID    =   0;
    
    if($level > 0){        
        
        $stack[]    =   $nav_node->page_title;
        
        $NavNode    =   new NavModel;
//        $NavNode->loadArray();
        $page_ID    =   0;
        if($nav_node->default_page){
            $page_ID    =   PageModel::ImportLegacyPage2($qube, $nav_node->default_page);
        }
        $qube->Save($NavNode, 
                array('name' => $nav_node->page_title, 
		'name_url' => WebsiteModel::TitlesToPath($stack), 
                'user_ID' => $nav_node->user_id, 
		'hub_ID' => $nav_node->hub_id, 'parent_ID' => $parent_ID,
                    'page_ID'   =>  $page_ID, 'sortk' => $nav_node->nav_order*10,
                    'oldtbl_ID' => $nav_node->id
                ));
        $node_ID    =   $NavNode->ID;
//        if($page_ID){   // update new page with new parent node id        }
    }
    if(!$nav_node->has_children) return;
    
    $pStmt->execute(array($nav_node->id));
#    $cStmt  =   clone($pStmt);
    $allnodes   =   $pStmt->fetchAll(PDO::FETCH_OBJ);
    foreach($allnodes as $node){
        if($level == 0) $stack = array();   // empty the stack and begin creating url structures from an empty array()
#    while($node = $pStmt->fetchObject())
        self::ImportLegacyNav($node, $level+1, $node_ID, $pStmt, $qube, $stack);
    }
    }
}

