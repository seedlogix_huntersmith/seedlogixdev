<?php

#class ContactDataExists extends Exception{}

/**
 * ContactForm Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * columns:
 * 
 * id	int(5)	NO	PRI	NULL	auto_increment
user_id	int(6)	NO	MUL	0	
parent_id	int(11)	NO		0	
type	varchar(5)	NO	MUL		
type_id	int(9)	NO		0	
cid	int(11)	NO		0	
name	varchar(150)	NO			
email	varchar(150)	NO			
phone	varchar(16)	NO			
website	varchar(255)	NO			
comments	text	NO		NULL	
page	varchar(150)	NO			
keyword	varchar(255)	NO			
search_engine	varchar(255)	NO			
responded	tinyint(1)	NO		0	
responses_received	text	NO		NULL	
optout	tinyint(1)	NO		0	
ip	varchar(255)	NO			
timestamp	timestamp	NO		CURRENT_TIMESTAMP	on update CURRENT_TIMESTAMP
created	timestamp	NO	MUL	0000-00-00 00:00:00	
 * 
 */
class ContactDataModel extends HybridModel {    
    
    public $id, $user_id, $parent_id, $type, $type_id;
    public $name, $email, $phone, $website, $comments, $page, $keyword;
    public $search_engine, $ip, $created, $cid, $spamstatus = NULL;
    
    protected $_PR_KEY  =   'id';
    
    function getTableNameFromType(){
        static $tableref = array('direc' => 'directory',
                            'artic' => 'articles',
                            'press' => 'press_release',
                            'hub' => 'hub',
                            '' => '',
                                'blogs' => 'blogs',
                            'contact_form' => 'contact_form',
                            'directory' => 'directory');
        return $tableref[$this->type];
    }
    
    function Exists()
    {
        $S = Qube::GetDriver()->QueryObject('ContactDataModel')
                ->Fields('count(*)')->Where('email= ? AND user_id = ? AND website= ? AND comments = ?')->Prepare();
        $S->execute(array($this->email, $this->user_id, $this->website, $this->comments));
        
        return $S->fetchColumn() > 0;
    }
    
    /**
     *
     * @param SendTo $recipient
     * @return \ContactFormNotification 
     */
    static function NotifyStatic($obj)
    {
        require_once QUBEPATH . '../classes/ContactFormNotification.php';
        $Notification   =   new ContactFormNotification($obj);
        return $Notification;
    }

    function Notify()
    {
        return self::NotifyStatic($this);
    }
    
    /**
     *
     * @var PDOStatement
     */
//    protected static $insert_statement;   
    
    
    function __getColumns() {
        static $cols    =   array('id', 'user_id', 'type', 'type_id', 'cid', 'parent_id',
            'name', 'email', 'phone', 'website', 'comments', 'page',
            'keyword', 'search_engine', 'ip', 'created', 'spamstatus');
        
        return $cols;
    }
    
    function _getInsertColumnBinding($column_name) {
        if($column_name == 'created')
        {
            return 'NOW()';
        }
        return parent::_getInsertColumnBinding($column_name);
    }
    

    function _getColumn($col_name, $get_type = 0)
    {
        if($get_type == self::FOR_INSERT && $col_name == 'created')
            throw new Exception();
        
        return parent::_getColumn($col_name, $get_type);
    }    
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
    
    function getSendTo()
    {
        return new SendTo($this->name, $this->email);
    }
    /**
     *
     * @param User $user 
     * @return ContactFormNtification
     *
    function Notify(User $user)
    {
        require_once QUBEPATH . '../classes/ContactFormNotification.php';
        return new ContactFormNotification($this, $user);
    }
     * 
     */
    
    
    function CheckForSpam()
		{
			$spamPoints	=	$this->getSpamPoints();
			$this->_set('spamstatus', $spamPoints);
		}
    
    
    function getSpamPoints()
    {
			if(!is_null($this->spamstatus))
			{
				return $this->spamstatus;
			}
        require_once QUBEROOT . 'admin/library/Akismet.class.php';
				
        // hardcoded check
          if(preg_match('/^[^\s]{6}\s+http:\/\/[^\s]+$/', $this->comments))
                        return 3;   // 3 = hard code match
					if(preg_match('/^twitter/', $this->email))
							return 4; 	// 4 another hardcoded match
				
$SC	=	new SpamChecker();	
				if($SC->checkSpammerIP($this->ip, $this->getQube()))
				{
					return 7;
				}
				
				if($SC->checkMessage($this->comments) !== 0)
				{
					return 6;	// detected match
				}
            // end
				
        $WordPressAPIKey = '3ed423682928';	//aoeu1aoue';
        $MyBlogURL = 'http://www.projectivemotion.com/';

        $akismet = new Akismet($MyBlogURL ,$WordPressAPIKey);
        $akismet->setUserIP($this->ip);
        $akismet->setCommentAuthor($this->name);
        $akismet->setCommentAuthorEmail($this->email);
        #$akismet->setCommentAuthorURL($url);
        $akismet->setCommentContent($this->comments);
        #$akismet->setPermalink('http://www.example.com/blog/alex/someurl/');

            try{
                $isspam	=	$akismet->isCommentSpam();
                    if($isspam)
                            return 1;	// 1 = spam (akismet)
                    
										// 0 = not spam
                    return 0;
            }catch(Exception $badkey){
                return 5;  // 5 = bad key
            }
#var_dump($akismet->isKeyValid(), $isspam);
    }
		
		function canNotifyUsers()
		{
			return $this->getSpamPoints() == 0;
		}
}

