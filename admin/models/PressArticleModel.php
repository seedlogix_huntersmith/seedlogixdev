<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NetworkArticleModel
 *
 * @author amado
 */
class PressArticleModel extends NetworkItemLegacyModel {

  protected $_PR_KEY = 'id';
  protected $_defaults = array('created' => 'NOW()');
  public $id = NULL;

  protected $thumb_id;  // protect to enable getthumb_id() function
  
  function __getColumns() {
    $cols = array('author', 'body', 'category', 'cid', 'city', 'contact', 'country', 'created',
        'email', 'email_id', 'headline', 'id', 'industry', 'keywords', 'network', 'state', 'summary',
        'thumb_id', 'timestamp', 'tracking_id', 'type', 'user_id', 'user_parent_id', 'website', 'name', 'trashed');
    return $cols;
  }

  //put your code here

  function Validate(array &$data, \ValidationStack $VS) {

    $VC = VarContainer::getContainer($data);
    if (!$this->getID()) {
      if (!$VC->check('intval', 'cid', 'user_id', 'user_parent_id'))
        $VS->addError('id', 'Not all required ids were provided for this model.');
    }

    $this->AcceptImages($VC, $data, 'press_release', array('thumb_id'));

    return !$VS->hasErrors();
  }

  function getURL($colkey, $obj_vars, DashboardBaseController $Ctrl) {
    switch ($colkey) {
      case 'thumb_id':
        return $Ctrl->getUser()->getStorageUrl('press_release', $obj_vars[$colkey]);
    }
  }

  function getthumb_id() {

      $storage = UserModel::getStorageURL_ID($this->user_id) . '/press_release/';
          return $storage . $this->thumb_id;
     
  }

}
