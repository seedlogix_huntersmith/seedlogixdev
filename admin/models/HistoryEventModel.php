<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HistoryEvent
 *
 * @author amado
 */

class HistoryEventModel extends HybridModel
{
    /**
     *
     * @var PDOStatement
     */
    protected static $prep  =   NULL;
    
    protected $valid    =   true;
    
    
    function __getColumns() {
        static $cols    =   array('user_ID', 'ID', 'event', 'value', 'context', 'ts', 'object');
        
        return $cols;
    }
    
    static function encodeData($d)
    {
        if(is_array($d))
        {
            $str    =   array();
            foreach($d as $k => $v)
            {
                $str[]  =   "`$k` = " . (is_array($v) ? '(' . self::encodeData($v) . ')' : $v );
            }
            
            return join("; ", $str);
        }
        
        return $d;
    }
    
    
    function construct($user_ID, $object, $event, $data)
    {   
        if(!self::$prep)
            self::$prep =   Qube::GetPDO()->prepare('INSERT INTO 6qube_history (user_ID, object, event, data) VALUES (?, ?, ?, ?)');
        
        $data   =   self::encodeData($data);
        $this->sqlparams    =   array($user_ID, $object, $event, $data);
    }
    
    function getID(){
        return $this->ID;
    }
    
    function destruct() {
        
        if($this->valid)
            self::$prep->execute($this->sqlparams);
    }
}
