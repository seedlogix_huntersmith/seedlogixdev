<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class HubModel extends HybridModel implements TouchpointPage {

    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    public $id, $cid = '', $user_id, $user_parent_id, $name, $phone = '', $language = '', $touchpoint_ID = 0,
            $hub_parent_id = '',
            $touchpoint_type = '',
            $domain = '', $company_name = '', $state = '', $city = '',
            $category = '',
            $street = '',
            $logo = '', $bg_img = '', $bg_repeat = '',
            $profile_photo = '',
            $zip = '',
            $slogan = '',
            $meta_keywords = '',
            $meta_description = '',
            $site_title = '',
            $blog = '', // blog url
            $description = '',
            $overview = '', $offerings = '',
            $photos = '', $videos = '', $events = '',
            $keyword1 = '',
            $keyword2 = '',
            $keyword3 = '',
            $keyword4 = '',
            $keyword5 = '',
            $keyword6 = '',
            $button_1 = '',
			$facebook_id = '',
            $button1_link = '',
            $button_2 = '',
            $button2_link = '',
            $button_3 = '',
            $button3_link = '',
            $legacy_gallery_link = '',
            $full_home = '',
            $form_box_color = '',
            $remove_font = '',
            $gallery_page = '',
            $slider_option = '',
            $display_page = '',
            $remove_contact = '',
            $google_webmaster = '',
            $backlink = '',
            $google_tracking = '',
            $lat = '',
            $lng = '',
            $www_options = '',
            $theme = 0,
            $bg_color = '',
            $links_color = '',
            $adv_css = '',
            $add_autoresponder = '',
            $connect_blog_id = 0,
            $pages_version = '',
            $has_tags = '',
            $has_feedobjects = '',
            $has_custom_form = '',
            $httphostkey = '',
            // edit regions.. :S

            $edit_region_6 = '',
            $edit_region_7 = '',
            $edit_region_8 = '',
            $edit_region_9 = '',
            $edit_region_10 = '',
            $edit_region_11 = '',
            $edit_region_12 = '',
            $edit_region_13 = '',
            $edit_region_14 = '',
            $edit_region_15 = '',
            $multi_user = 0,
            $multi_user_fields = '',
            $created = 'NOW()'
    ;
    protected $_PR_KEY = 'id';
    protected $_translations = array();

    public static function hasTags($data) {
        foreach (array_keys($data) as $field) {
            if (preg_match('/\(\((.*)\)\)/i', $data[$field]))
                return true;
        }
        return false;
    }

    function getTranslation($key) {
        return isset($this->_translations[$this->language][$key]) ? $this->_translations[$this->language][$key] : FALSE;
    }

    function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
        parent::setEventManager($eventManager);
        $eventManager->attach('Created', array($this, 'RefreshCampaignTouchPointCounts'));
        $eventManager->attach('Trashed', array($this, 'RefreshCampaignTouchPointCounts'));
    }

    function RefreshCampaignTouchPointCounts() {
        $cda = new \Qube\Hybrid\DataAccess\CampaignDataAccess();
        $cda->RefreshCampaignTouchPointCounts($this->cid);
    }

    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c = array('id', 'cid', 'user_id', 'user_parent_id', 'name', 'phone', 'language', 'hub_parent_id',
            'domain', 'company_name', 'state', 'city', 'category', 'street', 'logo', 'profile_photo', 'bg_img', 'facebook_id',
            'zip', 'slogan', 'description', 'req_theme_type', 'theme',
            'site_title', 'bg_repeat',
            'meta_keywords',
            'meta_description',
            'keyword1',
            'keyword2',
            'keyword3',
            'keyword4',
            'keyword5',
            'keyword6',
            'button_1',
            'button1_link',
            'button_2',
            'button2_link',
            'button_3',
            'button3_link',
            'legacy_gallery_link',
            'full_home',
            'form_box_color',
            'remove_font',
            'gallery_page',
            'slider_option',
            'display_page',
            'remove_contact',
            'google_webmaster',
            'google_tracking',
            'lat',
            'lng',
            'backlink',
            'bg_color',
            'links_color', 'connect_blog_id',
            'adv_css',
            'facebook', 'twitter', 'blog', 'linkedin', 'google_local', 'youtube', 'yelp', 'pinterest', 'instagram', 'google_places', 'mobile_domain',
            // output (display) vars
            'pages_version',
            'has_tags', 'has_feedobjects',
            'has_custom_form',
            // custom form embed fields
            'overview', 'offerings', 'photos', 'videos', 'events', 'edit_region_6', 'edit_region_7',
            'edit_region_8', 'edit_region_9', 'edit_region_10', 'edit_region_11', 'edit_region_12', 'edit_region_13', 'edit_region_14', 'edit_region_15', 'add_autoresponder',
            'www_options',
            'httphostkey', 'touchpoint_type',
            // multi user stuff
            'multi_user', 'multi_user_fields', 'touchpoint_ID'
            // www options
            , 'created'
        );
        return $c;
    }

    function _getDeleteClause() {
        ;
    }

    /**
     * 
     * @return SlideModel[]
     */
    function getSlidePhotos() {
        require_once HYBRID_PATH . 'models/SlideModel.php';

        return Qube::Start()->queryObjects('SlideModel')->Where('hub_id = %d AND deleted = 0', $this->id)->Exec()->fetchAll();
    }

    function Validate(array &$data, ValidationStack $VS) {
        if (self::hasEmbedForm($data)) {
            $data['has_custom_form'] = 1;
        }
//		else{
//			$data['has_custom_form'] = 0;
//		}
        return parent::Validate($data, $VS);
    }

    /**
     * @param array $fields
     * @return array
     */
    public static function hasEmbedForm(array &$fields) {
        foreach (array_keys($fields) as $field) {
            if (preg_match('/\|@form-[0-9]+@\|/', $fields[$field])) {
                return true;
            }
        }
        return false;
    }

}
