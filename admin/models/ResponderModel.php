<?php

require_once 'Response.php';
require_once QUBEPATH . '../classes/Notification.php';
require_once QUBEPATH . '../classes/ResponseNotification.php';
require_once QUBEPATH . '../classes/LeadResponseNotification.php';

/**
 * Responder Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * Table Structure: 
 * id 	int(11)	NO 	PRI 	NULL	auto_increment
user_id 	int(11)	NO 		0	
user_parent_id 	int(11)	NO 		0	
cid 	int(11)	NO 		0	
type 	int(11)	NO 		1	
table_name 	varchar(255)	NO 			
table_id 	int(11)	NO 		0	
name 	text	NO 		NULL	
active 	tinyint(1)	NO 		0	
sched 	int(11)	NO 		0	
sched_mode 	varchar(50)	NO 			
from_field 	varchar(255)	NO 			
subject 	text	NO 		NULL	
body 	text	NO 		NULL	
return_url 	varchar(255)	NO 			
contact 	text	NO 		NULL	
anti_spam 	text	NO 		NULL	
optout_msg 	text	NO 		NULL	
theme 	tinyint(11)	NO 		0	
logo 	varchar(255)	NO 			
bg_color 	varchar(75)	NO 			
links_color 	varchar(75)	NO 			
accent_color 	varchar(75)	NO 			
titles_color 	varchar(75)	NO 			
edit_region1 	blob	NO 		NULL	
call_action 	blob	NO 		NULL	
edit_region2 	blob	NO 		NULL	
 */
class ResponderModel extends EmailModel {
    
    // Sched [ 0 = immediately, else: time interval in $sched_mode units]
    public $id, $sched,
            $sched_mode,    // hours, days, weeks, months
            $stream_blog_id,
            $table_name,    // a bit unclear how this column is used
                            // I believe it is a reference to a table
                            // that triggers the autoresponder somehow.
            $table_id,      // @todo @important
            $theme,
            $theme_id,
            $cid, $master_ID, $active;
		
    protected $_PR_KEY	=	'id';
		private static $propagage_cols	=	array('body', 'subject', 'name', 'trashed');
		
		static function getPropagateColumns()
		{
			return self::$propagage_cols;
		}
		
		function canSendEmail()
		{
			static $hub	= NULL;
			if(!$hub){
				require_once QUBEADMIN . 'inc/hub.class.php';
				$hub	=	new Hub();
			}
			//if instant, send now
			//before sending check user's remaining email sends
			return ($hub->emailsRemaining($this->user_id) > 0);
		}
		
    function __getColumns() {
        static $c   =   array('id', 'user_id', 'sched', 'active', 'sched_mode',
						'body', 'subject', 'from_field', 'master_ID',
            'theme', 'table_id', 'table_name', 'stream_blog_id', 'theme_id',
            
            // antispam stuff:
            'anti_spam', 'contact', 'optout_msg', 'return_url',
            
            
            // the following particular email theme configurations would fit better on a separate table
            
            'bg_color', 'links_color', 'accent_color', 'titles_color', 'call_action', 
            'table_id', 'table_name', 'edit_region2', 'edit_region1', 'logo', 'name', 'cid', 'trashed'
                );
        return $c;
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
    
    /**
     *
     * Schedule a Response or send an Instant Response.
     * 
     * @param ContactForm $form 
     * @return int number of responses successffully sent.
     */
    function TriggerResponse_deprecated(ResponseTriggerEvent $Trigger, ScheduledResponse $response)
    {
        $notified = 0;
        if($this->isInstantResponder())
        {
            // the question is do we want to LOG instant responses? 
            // @todo increment user email sent count!
            
            $mail = $this->createNotification($Trigger);
            $sent = $Trigger->mailer->Send($mail);
            if(!$sent)
            {   // try again in 1 hour if it failed
                $response->setScheduledTime(1, 'hours');
            }else
                $notified = 1;
        }
        
        return $notified;
    }
    
		function doAfterUpdate(array $data, ResponderDataAccess $da, ModelManager $mm)
		{
			if(!$this->isMaster()) return;
			// continue (master_ID = 0 or master_ID = this->ID)
			$da->updateSlaveResponders($data, $this);
			
		}
		
    /**
     *
     * @param ResponseTriggerEvent $Trigger
     * @return ResponseNotification 
     */
    function createNotification_deprecated(ResponseTriggerEvent $Trigger)
    {
//        static $s
        $Trigger->setResponder($this);
        
        // @todo improve this
        
        /*
        if($this->isInstantResponder())
        {
            /*
            require_once QUBEPATH . '../classes/InstantResponseNotification.php';
            $N = new InstantResponseNotification($Trigger);//->form, $Trigger->user, $this);
        }
        else {
            */
            
            require_once QUBEPATH . '../classes/ThemedResponseNotification.php';
            
            if($Trigger->evt_type == 'leads')
            {
                require_once QUBEPATH . '../classes/LeadResponseNotification.php';
                return new LeadResponseNotification($Trigger);
            }
            
            $N = new ThemedResponseNotification($Trigger);
//        }
            
        return $N;
    }
    
    function hasTheme()
    {
        return $this->theme ? 1 : 0;
    }
    
    function getTheme()
    {        
        return Qube::GetDriver()->getAutoResponderThemeWhere('id = %d', $this->theme);
    }
    
    function isInstantResponder()
    {
        return $this->sched == 0 ? 1 : 0;
    }

    protected function createNotification($params = NULL)
    {
        if(!$params) throw new QubeException('Params is NULL.');
        $table = NULL;
        $lead = NULL;
        extract($params);

        return $this->CreateResponse($table, $lead);
    }
    
    function CreateResponse($datatable, $object_vars)//)
    {
        require_once QUBEPATH . '../classes/ThemeConfig.php';
        
        $response = NULL;            
        switch($datatable)
        {
            case 'contact_form':
                return $this->createFormResponse($object_vars);
               break; 
           
            case 'leads':
                return self::createLeadResponse($object_vars);
                break;
        }
        
        throw new QubeException($datatable);
    }
    
    /**
     *
     * needs array of contact_name, contact_email, contact_phone, and contact_comments
     * @param object $vars 
     * @return ContactResponse, SendTo
     */
    function createFormResponse($ScheduledResponse)
    {
        require_once QUBEPATH . '../classes/ContactResponseNotification.php';
        
        $sendTo     = new SendTo($ScheduledResponse->lead_name, $ScheduledResponse->lead_email);
        
        $Response   =   new ContactResponse(array(
                    'name'      => $ScheduledResponse->lead_name,
                    'email'     => $ScheduledResponse->lead_email,
                    'phone'     => $ScheduledResponse->lead_phone,
                    'comments'  => $ScheduledResponse->lead_comments,
                    'recipient_email' => $sendTo->getEmail())
                );
#deb($Response, QUBEPATH);
        // @todo create a view to determine responders with missing data!

        return array($Response, $sendTo);
    }
    
		/**
		 * Scheduled Response params required lead_email, lead_name, and lead_id
		 * 
		 * @param LeadModel $ScheduledResponse
		 * @return \LeadResponse
		 */
    static function createLeadResponse(LeadModel $ScheduledResponse)
    {
        $sendTo     =   new SendTo($ScheduledResponse->lead_name, $ScheduledResponse->lead_email);        
        
        $Response   =   new LeadResponse();//$ScheduledResponse->responder_subject);

        /*$dataAccess = new BlogDataAccess();
        $posts = $dataAccess->getLatestPostsByLead($ScheduledResponse->getID(), 5);*/
        $posts = null;



				// arbitrary placeholders used in responder body.
				$Response->setPlaceHolders(array('name' => $ScheduledResponse->lead_name,
													'recipient_email' => $sendTo->getEmail(),
													'email' => $sendTo->getEmail(),
													'blogfeed' => BlogModel::getHTMLPosts($posts)));
				
				
        $Response->loadVars(Qube::GetDriver(), $ScheduledResponse->getID());
        self::loadUserInfo($ScheduledResponse->getID(), NULL, $Response);
				
        return $Response;	//array($Response, $sendTo);
    }
    
		static function loadUserInfo($lead_id	=	NULL, $user_id = NULL, ResponseEmail $email = NULL)
		{
			if(!is_null($lead_id))
				$stmt=	Qube::Start()->squery('SELECT * FROM user_info WHERE user_id = (SELECT user_id FROM leads where id = %d)', $lead_id);
			else
			if(!is_null($user_id))
				$stmt=	Qube::Start()->squery('SELECT * FROM user_info WHERE user_id = %d', $user_id);
			else
				throw new Exception();	

					$userdata	=	array('my' => array());

				if($stmt->rowCount() == 1){

					$valuearray	=	$stmt->fetch(PDO::FETCH_ASSOC);
					foreach($valuearray as $col => $value)
					{
						$userdata['my'][$col]		=	$value;
					}
                    if($email)
					    $email->setPlaceHolders($userdata);
				}
				return $userdata;
		}

    /**
     * @param $create_params
     * @param $ResponseConfig
     * @param QubeSmarty $Smarty
     * @param array $placeholders
     * @param array $smartyvars
     * @return ResponseEmail|ThemedEmail
     */
    function ConfigureResponse($create_params, $ResponseConfig, QubeSmarty $Smarty, $placeholders = array(), $smartyvars = array())
    {
        $N = $this->getNotification($create_params);
        $N->setPlaceHolders($placeholders);

        if ($this->theme) {
            $vars = ThemeConfig::Load($this->id, 'auto_responder');
#            $vars   =   $this->toArray();
            $vars['site_root'] = 'http://' . $ResponseConfig->main_site . '/';
            $vars['themepath'] = 'http://' . $ResponseConfig->main_site . '/emails/themes';
            $vars['responder_phone'] = $ResponseConfig->responder_phone;
            $smartyvars['logo'] = UserModel::USER_DATA_DIR . $ResponseConfig->site_logo;

#            deb($vars);

            $N = new ThemedEmail($Smarty, $N);
            $N->setThemeVars(array_merge($vars, $smartyvars));
            $N->setThemeName($this->theme);

            if (Qube::IS_DEV()) {
                static $fff = 0;
                $fff++;
#                file_put_contents('email-' . $fff . '.html', $N->getContent());
            }
        }

        return $N;
    }
		
		function isMaster()
		{
				return	$this->master_ID	==	$this->getID();
		}
		
		function isSlave()
		{
				return !$this->isMaster() && $this->master_ID !=	0;
		}
		
//	function doTrash(ResponderDataAccess $da, ModelManager $mm) {
//		$unmodified	=	clone $this;
//		$ret	=	parent::Trash($this->getQube());
//		if(!$this->isMaster())	return	$ret;
//		
//		$trashed	=	$this->Refresh('trashed', PDO::FETCH_COLUMN);
//		$data	=	array('trashed' => $trashed);
//		
//		$unmodified->doAfterUpdate($data, $da, $mm);
//	}
		
    function Validate(array &$data, ValidationStack $VS){
        $Vc =   VarContainer::getContainer($data);
        $emptycid   =   $Vc->isEmpty('cid');
        if($Vc->checkValue('name', $sitename) && trim($sitename) == '')
        {
            $VS->addError('name', 'You must provide a valid name for this responder.');
        }
        if($Vc->isEmpty($this->_PR_KEY))   // doing an insert
        {
            if($emptycid)
            {
                $VS->addError('cid', 'Missing field.');
            }
            if($Vc->isEmpty('table_id'))
            {
                $VS->addError('table_id', 'You must select a trigger context for this responder.');
            }
            switch($data['table_name']) {
                case 'blogs':
                case 'hub':
                case 'lead_forms':
                    break;
                default:
                    $VS->addError('table_name', 'Invalid table name.');
            }
        }else{
            // basic security shit
            unset($data['cid']);
            unset($data['user_id']);
            unset($data['table_id']);
            unset($data['table_name']);
        }
        return !$VS->hasErrors();
    }

    function getResponderType()
    {
        switch($this->table_name)
        {
            case 'lead_forms':
                return 'Lead Form';
            case 'hub':
                return 'Touchpoint';
            case 'blogs':
                return 'Blogs';
        }
        return 'Unknown';
    }
}


class ActiveResponderModel extends ResponderModel
{
    function getAliasedSelectClause($alias) {      
        
        $where = new DBWhereExpression(parent::getAliasedSelectClause($alias));
        
        $where->Where('`' . $alias . '`.active = 1');
        
        return $where;
    }
}
