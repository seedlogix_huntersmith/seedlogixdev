<?php

/**
 * SlideModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class SlideModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $ID  =   0, $user_id, $hub_id, $title, $internalname, $background  =   '', $url    =   '', $urlenabled = 0, $description    =   '', $deleted;
    
    protected $_PR_KEY   =  'ID';
    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('ID', 'user_id', 'hub_id', 'title', 'internalname', 'background', 'url', 'urlenabled', 'description', 'deleted');
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
    
    function hasBackground()
    {
        return !empty($this->background);
    }
    
    function meta($var, $default    =   'N/A')
    {
#        var_dump($var, $this->$var);
        if(trim($this->$var)  !=  '') return $this->$var;
        return $default;
    }

	function getOwnerrUser(){

	}

    function Validate(array &$data, ValidationStack $VS)
    {
        $vc = VarContainer::getContainer($data);

        // @todo validate title

        if($vc->checkValue('url', $url) && !empty($url)){
            $VS->ValidateURL('URL', $url);
        }

        return !$VS->hasErrors();
    }

    static function getPicsInternalNames(){
        return array('one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten');
    }

    function onBeforeUpdatedEvent(\Zend\EventManager\Event $ev)
    {
        /** @var HubDataAccess $DA */
        $params =   $ev->getParams();
        $DA = $params['DataAccess'];   // provided by ModelManager.
        $changed_vars = &$params['object_vars'];

        $curpicInfo =   $this->Refresh('internalname, hub_id');
        if(FALSE === WebsiteDataAccess::FromDataAccess($DA)->isMultiSiteHub($curpicInfo->hub_id))
            return;

        // propagate slide for multisite
        // @todo priority LOW optimize this piece
        unset($changed_vars[$this->_PR_KEY]);
        unset($changed_vars['hub_id']);
        $changed_cols = array_intersect($this->_getColumns(), array_keys($changed_vars));
//        $Original = $this->Refresh();
        foreach($changed_cols as $col)
        {
            $fieldname  =   HubDataAccess::convertSlideColToHubCol($curpicInfo->internalname, $col);
            $DA->PropagateSiteSlideUpdate($fieldname, $changed_vars[$col], 1, $curpicInfo->hub_id);
//            $hubDataAccess->UpdateChildHubsColumnValue($Original, $col, $changed_vars[$col]);
        }
    }

    function onCreatedEvent(\Zend\EventManager\Event $ev)
	{
		//@todo
		/** @var HubDataAccess $DA */
		$DA =   $ev->getParam('DataAccess');
        $vars = $ev->getParam('object_vars');

        if(FALSE === WebsiteDataAccess::FromDataAccess($DA)->isMultiSiteHub($vars['hub_id']))
            return;

        // propagate slide for multisite
        $DA->PropagateNewSiteSlide($this->getID());
	}

	function onTrashed(\Zend\EventManager\Event $ev)
	{
		// update slave navs/pages
		/** @var QubeLogPDOStatement $stmt */
		$stmt = Qube::Start()->GetPDO()->prepare('UPDATE slides SET deleted = NOW() WHERE hub_id IN (SELECT id FROM hub WHERE hub_parent_id = :hub_parent_id) AND internalname = :internalname');
		$stmt->execute(array('hub_parent_id' => $this->hub_id, 'internalname' => $this->internalname));
		return $stmt->rowCount();
	}

	protected function _Trash(Qube $q, $andwhere = 'user_id = dashboard_userid()', $trash = 0)
	{
		return $q->query('UPDATE %s SET deleted = %s WHERE %s', $this, $trash ? 'NOW()' : 0,
			DBWhereExpression::Create($this->getPKWhere())->appendWhere(' AND ', $andwhere))->rowCount();
	}


}