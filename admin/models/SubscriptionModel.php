<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Row from the 6q_roles table
 *
 * @author amado
 */
class SubscriptionModel extends HybridModel {
	const UNLIMITED	=	-1;
	
    /**
     *
     * @var int
     */
    public $ID	=	0;
	
    /**
     *
     * @var int
     */
    public $name;
	
    /**
     *
     * @var string
     */
    public $price	=	0.0;
	
	protected $limits_json	=	FALSE;
    
	public $limits	=	NULL;
	
	function __construct(\Qube $q = NULL, $data = array()) {
		parent::__construct($q, $data);
		if($this->limits_json)
		{
			$this->limits	= json_decode($this->limits_json);
		}
	}
	
	function get($c, $purpose = self::FOR_OUTPUT) {
		if($c == 'limits_json')
		{
			$json = json_encode($this->limits);
			if($json)
				return $json;
			else
				return new stdClass();
		}
		if(preg_match('/^limit_(.*)$/', $c, $m))
		{
			$type	=	$m[1];
			return $this->limits->$type;
		}
		return parent::get($c, $purpose);
	}
	
	function set($c, $n) {
		if(preg_match('/^limit_(.*)$/', $c, $m))
		{
			$type	=	$m[1];
			$this->setLimit($type, $n);
		}
		return parent::set($c, $n);
	}
	
	function setLimit($type, $value)
	{
		if(NULL === $this->limits)
		{
			if($this->getID())
			{
				$this->limits	=	json_decode($this->Refresh('limits_json', PDO::FETCH_COLUMN));
			}
		}

		if(!$this->limits)
			$this->limits = new stdClass();

		$this->limits->$type	=	(int)$value;
		$this->_setfields['limits_json']	=	1;

		return $this;
	}
	
	function getLimits()
	{
		return $this->limits;
	}
	
    function __getColumns() {
        return array('ID', 'name', 'price', 'limits_json');
    }
    
    function Validate(array &$row, ValidationStack $errors){
		return true;
    }
}

