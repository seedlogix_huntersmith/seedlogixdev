<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class HubThemeModel extends DBObject {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id, $type, $name, $css_file_path, $user_class2_fields, $pages, $training_link, $thumbnail, $custom_css_support, $theme_color_options, $slider_options,
            $path, $region1_title,
$region2_title,
$region3_title,
$region4_title,
$region5_title,
$region6_title,
$region7_title,
$region8_title,
$region9_title,
$region10_title,
$region11_title,
$region12_title,
$region13_title,
$region14_title,
$region15_title,
$hybrid;
    
    protected $_PR_KEY  =   'id';


        /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id', 'type', 'name', 'css_file_path', 'user_class2_fields', 'pages', 'training_link',
                            'thumbnail', 'custom_css_support', 'theme_color_options', 'slider_options', 'path',
            'blog_region1_title', 'blog_region2_title', 'blog_region3_title',
            // soon to be deprecated
            'region1_title',
'region2_title',
'region3_title',
'region4_title',
'region5_title',
'region6_title',
'region7_title',
'region8_title',
'region9_title',
'region10_title',
'region11_title',
'region12_title',
'region13_title',
'region14_title',
'region15_title',
'hybrid'
                    );
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }

    function getEditFields(WebsiteModel $website){
        $fields =   array();
        
        // where to get the value from.. 
        $crazyshit  =   array('', 'overview', 'offerings', 'photos', 'videos', 'events');
        
        for($i  =   1;  $i <= 15; $i++){
            $key    =   "region{$i}_title";
            $title  =   'Edit Region' . $i;
            if(!empty($this->$key)){
                $site_column   = array_key_exists($i, $crazyshit) ? $crazyshit[$i] : 'edit_region_' . $i;
                $fields[$site_column]   =   array('title' => $this->$key, 'value' => $website->get($site_column));
            }
        }
        
        return $fields;
    }

    function getBlogEditFields(BlogModel $blog){
        $fields = array();

        for($i = 1; $i <= 3; $i++){
            $key = "blog_region{$i}_title";
            if(!empty($this->$key)){
                $blogColumn = "blog_edit_{$i}";
                $fields[$blogColumn] = array("title" => $this->$key, 'value' => $blog->get($blogColumn));
            }
        }

        return $fields;
    }

	function getThumbnailURL(){
		if($this->thumbnail)
			return 'hubs/themes/thumbnails/' . $this->thumbnail;
		else{
			return 'themes/default/images/no-thumb.png';
		}
	}
}

