<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PendingTwitterBlogPostModel
 *
 * @author amado
 */
class PendingLinkedInBlogPostModel extends ScheduledBlogPostPublicationModel
{    
    function _getCondition($T, $purpose = self::FOR_SELECT) {
        return parent::_getCondition($T, $purpose)->Where('b.linkedin_accessid !=""');
    }
    
    static function Select(Qube $q)
    {
        return ScheduledBlogPostPublicationModel::SelectPost($q, get_class(), 'LINKEDIN_BLOGPOST')
                ->addFields('b.linkedin_accessid');
    }
}
