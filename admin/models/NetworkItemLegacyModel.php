<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NetworkArticleLegacy
 *
 * @author amado
 */
class NetworkItemLegacyModel extends NetworkListingModel {
  
	function __getColumns() {
		$cols	=	parent::__getColumns();
		$cols[]		=	'name';
		$cols[]		=	'created';
		
		return $cols;	//+ array('name');
	}
	
	function CreateNetworkListing(){
			$N = new NetworkListingModel($this->getQube(), array('tbl' => $this->getNetworkListingType(), 'tbl_ID' => $this->getID(), 'cid' => $this->get('cid')));
			return $N;
	}
}
