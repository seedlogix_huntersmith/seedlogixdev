<?php
/**
 * Created by Jon Aguilar
 */

/**
 * Class EmailTemplatesModel
 *
 * Note. This could be improved. There appear to be a number of models used for configurating emails. Might be a good idea
 * to create a base model for those objects.
 */
class TextsModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id,
        $user_id,
	$user_parent_id, 
		$cid,
		$account_id,
		$contact_id,
	$lead_id,
	$tracking_id,
		$chain_id,
        $to_sent,
		$from_text,
        $message,
        $trashed,
        $created;
    
    protected $_PR_KEY  =   'id';


        /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id',
            'user_id',
								  'user_parent_id',
            'cid',
			'account_id',
			'contact_id',
								  'lead_id',
			'tracking_id',
			'chain_id',
            'to_sent',
			 'from_text',
            'message',
            'trashed',
            'created');
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
	
	function getFormattedDate(){
		$FormattedDate = new DateTime($this->created);
		$FormattedDate = $FormattedDate->format('Y-m-d');
		return $FormattedDate;
	}
	
	function getTimeLineText($delete_link = '', $contact_id){
		$contact = ContactModel::Fetch('id = %d', $contact_id);
		
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $this->created);
		$callstamp = $date->format('g:i A');
		
		if($this->contact_id) $from = '<span style="color: #748a96;">to '.$contact->first_name.' '.$contact->last_name.' - '.$callstamp.'</span>';
		else $from = '<span style="color: #748a96;">from '.$contact->first_name.' '.$contact->last_name.' - '.$callstamp.'</span>';
		
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "<span class=\"texttimeline-title\"><i class=\"icon-comment_stroke\"></i> SMS {$from}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "<p>{$this->message}</p>"
		);
	}
	function getTimeLineTextLead($delete_link = '', $lead_id){
		$Prospect = ProspectModel::Fetch('id = %d', $lead_id);
		
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $this->created);
		$callstamp = $date->format('g:i A');
		
		if($this->lead_id) $from = '<span style="color: #748a96;">to '.$Prospect->name.' - '.$callstamp.'</span>';
		else $from = '<span style="color: #748a96;">from '.$Prospect->name.' - '.$callstamp.'</span>';
		
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "<span class=\"texttimeline-title\"><i class=\"icon-comment_stroke\"></i> SMS {$from}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "<p>{$this->message}</p>"
		);
	}

	

}

