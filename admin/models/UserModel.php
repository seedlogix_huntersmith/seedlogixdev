<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * User Model
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class UserModel extends HybridModel {

    /**
     *
     * Table Fields
     */
    public  $id, 
            $parent_id, 
            $username, 
            $emails_sent, 
            $class, 
            $canceled, 
            $email_limit,
            $profile_photo, 
            $super_user = 0, 
            $reseller_client = 0, 
            // Added by Hunter (to allow sub users to override content submitted by and admin aka Override Admin Content
            $override_admin_content = 0, 
            $last_login = '0000-00-00 00:00:00', 
            $created;
    protected $_PR_KEY = 'id';

    const ADMIN_CANVIEWALL = false;  // disabled. do not allow admin to view all user data
    const PASSSALT = 'AA<,sd9dfgd-,d,.D>FG>FG:Ld=d=d0fgo3W#Msjsnxxfg';

    protected $_defaults = array('password' => 'MD5(CONCAT(MD5(:password), :salt))');

    /**
     * Alias, see UserDataAccess::getUserWithRoles
     */

    /** @var bool  */
    public $has_referred_users = false;

    const USER_DATA_DIR = 'users';
    const DEFAULT_PROFILE_PHOTO = 'themes/default/images/userLogin2.png';

    function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
        parent::setEventManager($eventManager);

        $eventManager->attach('SaveFailed', array($this, 'onUserSaveFailed'));
        $eventManager->attach('LimitReached', array($this, 'onLimitReached'));
    }

    function onLimitReached($e) {
        /* @var $target UserWithRolesModel */
        $target = $e->getTarget();

        $func = $e->getParam('func');

        $msgs = array('canCreateUser' => 'You have reached the limit of users.',
            'canCreateResponder' => 'You have reached the limit for autoresponders.',
            'canCreateWEBSITE' => "You need to upgrade your account create more websites.",
            'canCreateBLOG' => "You need to upgrade your account create more blogs.",
            'canCreateKeyword' => "You need to upgrade your account create more keywords.",
            'UNKNOWN' => 'Unknown Error.');

        if (!array_key_exists($func, $msgs))
            $func = 'UNKNOWN';

        $msg = $msgs[$func];

        return $e->getParam('Validation')->deny($msg);
    }

    function onUserSaveFailed(\Zend\EventManager\Event $ev) {
        $UM = $ev->getTarget();
        $object_vars = $ev->getParam('object_vars');

        $error = $ev->getParam('error');

        switch ($error->getCode()) {
            case "23000": // duplicate username found
                $ev->getParam('Validation')->addError('username', 'Username Exists.');
                break;
            default:
                $ev->getParam('Validation')->addError('Unknown', 'Unknown Error Ocurred');
        }

        // attempt to get a PDO Error.

        return false;
    }

    function setRandomPassword() {
        $this->password = substr(md5(uniqid()), 0, 10);
    }

    function __construct(\Qube $q = NULL) {
        parent::__construct($q);
        unset($this->password);
#        echo $this->username;
    }

    function getValue($c, $p, &$array) {

        if ($c == 'password') {
            $array['salt'] = UserModel::PASSSALT;
            $array['password'] = $this->get($c, $p);
        }

        parent::getValue($c, $p, $array);
    }

    function __getColumns() {
        static $cols = array(
            'id', 
            'parent_id', 
            'username', 
            'emails_sent', 
            'class',
            'super_user', 
            'reseller_client', 
            'access', 
            'canceled',
            //'email',
            'override_admin_content', //Added by Hunter 10.26.2017
            'created', 
            'upgraded', 
            'last_login', 
            'password', 
            'created');
        return $cols;
    }

    /**
     * Returns the path RELATIVE to the /users/ directory that corresponds to the $user_id's files.
     * 
     * @param type $user_id
     * @return string
     */
    static function GetUserSubDirectory($user_id) {
        return FileUtil::getIDPath('u', $user_id);
    }

    /**
     * Return the ABSOLUTE PATH given a $usersdir and a $user_id. (No Trailing slash)
     * 
     * @return string     * 
     */
    static function CreateUserDirectory($usersdir, $user_id) {
        $destination = realpath($usersdir) . '/' . self::GetUserSubDirectory($user_id);

        FileUtil::CreatePath($destination);
        return $destination;
    }

    /**
     * returns real absolute path and creates directory if necessary
     * 
     * @param string $param1 path
     * @return type
     */
    function getStoragePath() {
        $args = func_get_args();
        return self::getStorage($this->getID()) . '/' . implode('/', $args);
    }

    /**
     * 
     * @param type $arg1
     * @return boolean|string
     */
    function hasFile($arg1) {
        $fargs = func_get_args();
        $filename = call_user_func_array(array($this, 'getStoragePath'), $fargs);
        return (file_exists($filename) ? $filename : false);
    }

    /**
     * 
     * get relative url given a set of parameters to append to it
     * 
     * @return string
     */
    function getStorageUrl() {
        $Fargs = func_get_args();
        return self::getStorageURL_ID($this->getID(), self::USER_DATA_DIR . '/') . '/' . implode('/', $Fargs);
//		return 'users';
    }

    static function getStorageURL_ID($user_ID, $base = 'users/') {
        return $base . self::GetUserSubDirectory($user_ID);  // . '/' . implode('/', $Fargs);
    }

    static function getAbsoluteUsersStorage() {
        return realpath(Qube::get_settings('USERS_STORAGE'));
    }

    static function getStorage($user_ID, $create = TRUE, $subpath = FALSE) {
//			$path	=	'/home/sapphire/data/6qube-' // realpath(QUBEROOT) . '?'
        $path = self::getAbsoluteUsersStorage() . '/' . self::getStorageURL_ID($user_ID, '') . ($subpath ? '/' . $subpath : '');
        if (Qube::IS_DEV() != TRUE) {
            if ($create && FALSE === FileUtil::CreatePath($path)) {
                throw new QubeException('Unable to create path: ' . $path);
            }
        }
        return $path;
    }

    function getID() {
        return $this->id;
    }

    function _getDeleteClause() {
        return array('id' => $this->id);
    }

    function isReseller() {
        return $this->parent_id == $this->getID() || $this->parent_id == 0;
    }

    function hasReseller() {
        // return ture if user HAS reseller parent
        // or it is a reseller themselves.
        // this logic is a little sketchy
        return ($this->parent_id || $this->class == 17);
        // @todo why class == ' 17' ? 
    }

    /**
     * If the parent does not have a parent (parent_id = 0, or parent_id = id)
     * 	=> then return the parent_id.
     * 
     * if the parent does have a parent, return my id.
     * 
     * @return int
     */
    function getParentID() {
        if ($this->parent_id == 0)
            return $this->getID();
        return $this->parent_id; //
//        if($this->parent_id){
//            $UDA = new UserDataAccess();
//            $parent = $UDA->getUser($this->parent_id);
//            if(!$parent->isReseller())
//                return $this->parent_id;
//        }
        return $this->getID();
    }

    function hasParent() {
        return ($this->getParentID() !== $this->getID());
    }

    /**
     *
     * @return ResellerModel
     */
    function getReseller() {
        // @todo once I get a better view of the whole system
        // this needs to be converted to a left-join.

        /**
         * get reseller data 
         */
        require_once 'ResellerModel.php';

        $reseller = NULL;
        if ($this->hasReseller()) {
            $reseller = Qube::GetDriver()->getResellerModelWhere('admin_user = %d OR admin_user = %d', $this->id, $this->parent_id);
        }
//        deb($this, ' does this user have a reseller? ', $this->hasReseller(), $this->parent_id, $this->class);
        return $reseller ? $reseller : self::get6QubeReseller();
    }

    /**
     *
     * 
     * @return Reseller 
     */
    private static function get6QubeReseller() {

        $R = new ResellerModel();
        $R->_set('main_site', '6qube.com');
        $R->_set('company', '6qube');

        return $R;
    }

    /** pseudo function for left-joined user_class stuff * */
    function hasEmailLimit() {
        // isset returns false even if email_limit is declared as NULL
        // if(($this->email_limit)) throw new Exception('the variable is undefined');

        if (is_null($this->email_limit) || $this->email_limit == -1)
            return false;

        return true;
    }

    /**
     * 
     * @param array $vars requires parent_user to be declared in vars
     * @param ValidationStack $vs
     * @return type
     */
    function Validate(array &$vars, ValidationStack $vs) {
        $vc = VarContainer::getContainer($vars);
        $parentUser = $vc->getValue('parent_user');
        foreach ($vc->getData() as $key => $value) {
#            if(!$vc->checkValue('username', $username)) continue;   // if 
            if ($key == 'password' && !isset($vars['ID']))
                continue; // do not validate password for new users
            self::ValidateField($key, $value, $vs);
        }

        if ($vc->exists('username')) {
            
        }

        $isUpdate = $this->getID();
        $updateUserID = $this->getID();

        if ($isUpdate && !empty($updateUserID)) {
            $isSelfUpdate = $updateUserID == $vc->getValue('user_edit_id');
            if (!($isSelfUpdate || $this->parent_id == $vc->getValue('user_edit_id') || $parentUser->can('edit_user', $this->getID()))) {
                $vs->addError('permission', 'Permission Denied');
            }
        }
        return !$vs->hasErrors();
    }

    static function ValidateField($field, $value, ValidationStack &$vs) {
        switch ($field) {
            case 'username':
                //if(FALSE === $vs->Validate('username', $value, new \Zend\Validator\EmailAddress))
                //{
                //return FALSE;
                //}
//                if (filter_var($value, FILTER_VALIDATE_EMAIL) === FALSE) {
//                    $vs->addError('username', 'You must provide a valid email address.');
//                    return false;
//                }
                break;
            case 'password':
                if (empty($value) || strlen(trim($value)) < 5) {
                    $vs->addError('password', 'Password field must be atleast 5 characters long.');
                    return false;
                }
                break;
        }

        return true;
    }

    function getBindings() {
        return array('CampaignModel' => 'user_id = %d',
            'NetworkItemLegacyModel' => 'user_id = %d', 'WebsiteModel' => 'user_id = %d', 'BlogModel' => 'user_id');
    }

    function getProfileImageUrl() {
        //return $this->getStorageUrl( $this->profile_photo);
        //do not add 'users' prefix  string to this URL
        $profileImage = $this->profile_photo ? SystemDataAccess::getUserUrlPath($this->profile_photo) : self::DEFAULT_PROFILE_PHOTO;
        return file_exists(SystemDataAccess::getUserPath($profileImage)) ? $profileImage : self::DEFAULT_PROFILE_PHOTO;
    }

    function getSubscriptionLimits() {
        
    }

    /**
     * 
     * @return \SendTo
     */
    function asRecipient() {
        $sendTo = new SendTo($this->username, $this->username);

        return $sendTo;
    }

    function isAdmin() {
        return $this->getParentID() == $this->getID();
    }

    function getCreatedDate() {
        return new DateTime($this->created);
    }

    public function jsonSerialize() {
        $json = parent::jsonSerialize();
        unset($json->password);
        return $json;
    }

}
