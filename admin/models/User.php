<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * User Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class User extends DBObject {
    
    /**
     * 
     * Table Fields
     */
    public $id, $parent_id, $username, $emails_sent, $class;
    
    function _getSelectColumns() {
        return array('id', 'parent_id', 'username', 'emails_sent', 'class');
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
    
    function hasReseller()
    {
        // return ture if user HAS reseller parent
        // or it is a reseller themselves.
        
        // this logic is a little sketchy
        return ($this->parent_id || $this->class == 17);
            // @todo why class == ' 17' ? 
            
    }
    
    /**
     *
     * @return Reseller
     */
    function getReseller()
    {
        // @todo once I get a better view of the whole system
        // this needs to be converted to a left-join.
        
        /**
         * get reseller data 
         */
        
        require_once 'Reseller.php';
        
        $reseller = NULL;
        if($this->hasReseller())
        {
            $reseller = Qube::GetDriver()->getResellerWhere('admin_user = %d OR admin_user = %d', $this->id, $this->parent_id);
        }
//        deb($this, ' does this user have a reseller? ', $this->hasReseller(), $this->parent_id, $this->class);
        return $reseller ? $reseller : self::get6QubeReseller();
    }

    /**
     *
     * 
     * @return Reseller 
     */
    private static function get6QubeReseller()
    {
        
        $R = new Reseller;
        $R->_set('main_site', '6qube.com');
        $R->_set('company', '6qube');
        
        return $R;
    }
    
    /** pseudo function for left-joined user_class stuff **/
    function hasEmailLimit()
    {
        // isset returns false even if email_limit is declared as NULL
        // if(($this->email_limit)) throw new Exception('the variable is undefined');
        
        if(is_null($this->email_limit) || $this->email_limit == -1)
                    return false;
        
        return true;
    }
}

#User::Prepare('User');