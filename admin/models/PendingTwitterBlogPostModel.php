<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PendingTwitterBlogPostModel
 *
 * @author amado
 */
class PendingTwitterBlogPostModel extends ScheduledBlogPostPublicationModel
{    
    function _getCondition($T, $purpose = self::FOR_SELECT) {
        return parent::_getCondition($T, $purpose)->Where('b.twitter_accessid !=""');
    }
    
    static function Select(Qube $q)
    {
        return ScheduledBlogPostPublicationModel::SelectPost($q, get_class(), 'TWITTER_BLOGPOST')
                ->addFields('b.twitter_accessid');
    }
}
