<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/16/14
 * Time: 2:15 PM
 */

class ReportModel extends HybridModel{
	public $ID;
	public $campaign_ID;
	public $user_ID;
	public $created;
	public $name;
	public $type = 'DAILY';
	public $touchpoint_type = 'WEBSITE';
	public $start_date;
	public $end_date;

	function __getColumns() {
		static $cols = array('ID', 'campaign_ID', 'user_ID', 'created', 'name',
			'type', 'touchpoint_type', 'start_date', 'end_date');
		return $cols;
	}

	/**
	 * @return bool|DateInterval
	 */
	public function getNumberDays(){
		$start = new DateTime($this->start_date);
		$end = new DateTime($this->end_date);
		return $start->diff($end, true);
	}

	public function getType(){
		switch($this->type){
			case 'DAILY':
			case 'WEEKLY':
			case 'MONTHLY':
			case 'YEARLY':
				return $this->type;
			case 'CUSTOM':
				if($this->getNumberDays()->days <= 10){
					return 'DAILY';
				}
				elseif($this->getNumberDays()->days <= 70){
					return 'WEEKLY';
				}
				elseif($this->getNumberDays()->days <= 300){
					return 'MONTHLY';
				}
				else{
					return 'YEARLY';
				}
			default:
				throw new InvalidArgumentException("{$this->type} Not supported");
		}
	}

	public function isCustom()
	{
		return $this->type == "CUSTOM";
	}

	public function getChartOptions(){
		$options = array();
		SWITCH ($this->getType()){
			case 'DAILY':
				$options['day_swap'] = 10;
				$options['day_size'] = 1;
				$options['tick_type'] = 'day';
				$options['timeformat'] = '"%m/%d"';
				break;
			case 'WEEKLY':
				$options['day_swap'] = 70;
				$options['day_size'] = 7;
				$options['tick_type'] = 'day';
				$options['timeformat'] = '"%m/%d"';
				break;
			case 'MONTHLY':
				$options['day_swap'] = 10;
				$options['day_size'] = 1;
				$options['tick_type'] = 'month';
				$options['timeformat'] = '"%m/%Y"';
				break;
			case 'YEARLY':
				$options['day_swap'] = 3650;
				$options['day_size'] = 365;
				$options['tick_type'] = 'day';
				$options['timeformat'] = '"%Y"';
				break;
		}

		return $options;
	}

}