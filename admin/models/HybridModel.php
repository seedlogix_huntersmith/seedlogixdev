<?php

/**
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HybridModel
 *
 * @author amado
 */
abstract class HybridModel extends DBObject implements queriesObjects, \Zend\EventManager\EventManagerAwareInterface {

    protected $loadedColumns = null;
    protected $_defaults = array('created' => 'now()');

    /**
     *
     * @var \Zend\EventManager\EventManager()
     */
    protected $eventManager;

    /**
     * Overload this method, call parent::setEventManager, and add events.
     *
     * @param \Zend\EventManager\EventManagerInterface $eventManager
     * @return $this
     */
    function setEventManager(\Zend\EventManager\EventManagerInterface $eventManager) {
        $eventManager->addIdentifiers(get_called_class());
        $this->eventManager = $eventManager;
        // adding onUpdate event
        $eventManager->attach('Updated', array($this, 'onUpdatedEvent'));
        $eventManager->attach('Created', array($this, 'onCreatedEvent'));
        $eventManager->attach('BeforeUpdate', array($this, 'onBeforeUpdatedEvent'));
        $eventManager->attach('BeforeCreate', array($this, 'onBeforeCreatedEvent'));
        $eventManager->attach('Trashed', array($this, 'onTrashed'));

        return $this;
    }

    /**
     * 
     * @return \Zend\EventManager\EventManager
     */
    function getEventManager() {
        if (null === $this->eventManager) {
            $this->setEventManager(new \Zend\EventManager\EventManager());
        }
        return $this->eventManager;
    }

    function isTrashed() {
        return isset($this->trashed) && $this->trashed != '0000-00-00';
    }

    /**
     *
     * @var Qube
     */
    protected $qube = NULL;

    function __construct($q = NULL, $data = array()) {
        if ($q instanceof Qube)
            $this->setQube($q);
        if ($q instanceof \Zend\EventManager\EventManager)
            $this->setEventManager($q);

        if ($data)
            $this->loadArray($data);
    }

    function __get($name) {
        if (method_exists($this, 'get' . $name)) {
            $func = 'get' . $name;
            return $this->$func();
        }

        return $this->get($name, self::FOR_OUTPUT);
    }

    function getMeta($tag, $type = 'valint') {
        return $this->getQube()
                        ->query('SELECT ' . $type . ' FROM 6q_meta WHERE PK = ' . $this->getID() . ' AND `table` = "' . Qube::getTable($this) . '" AND col="' . $tag . '"')
                        ->fetchColumn();
    }

    function saveMeta($tag, $value, $type = 'valint') {
        return $this->getQube()
                        ->query("REPLACE INTO 6q_meta SET $type = '$value', PK = " . $this->getID() . ", `table` = '" .
                                $this->getQube()->getTable($this) . "', col='$tag'");
    }

    function html($col) {
        echo htmlentities($this->get($col), ENT_QUOTES);
    }

    function getID() {
        return (int) $this->get($this->_PR_KEY);
    }

    function setID($id) {
        $this->set($this->_PR_KEY, $id);
    }

    function getInputValidator() {
        if ($this instanceof InputValidator)
            return $this;
    }

    function __call($name, $arguments) {
        if (preg_match('/^query(.*?)s$/', $name, $matches)) {
            ///throw new Exception();					
            $model = $matches[1] . 'Model';

            array_unshift($arguments, 'T');
            array_unshift($arguments, $model);
            return call_user_func_array(array($this, 'queryObjects'), $arguments);
        }

        if (preg_match('/^get(.*?)$/', $name, $matches2)) {
            ///throw new Exception();					
            $model = $matches2[1] . 'Model';

            array_unshift($arguments, 'T');
            array_unshift($arguments, $model);
            $Q = call_user_func_array(array($this, 'queryObjects'), $arguments);

            return $Q->Exec()->fetch();
        }

        throw new Exception();
    }

    function getBindings() {
        return array();
    }

    /**
     * Used to query sub (child) objects based on _PR_KEY
     * 
     * @param type $model
     * @return DBSelectQuery
     */
    function queryObjects($model, $alias = 'T', $andwhere = NULL) {

        $bindings = $this->getBindings();

        if (isset($bindings[$model])) {
            $where = $bindings[$model];

            $Q = $this->getQube()->queryObjects($model, $alias);

            if ($this->_PR_KEY)
                $Q->Where($where, $this->{$this->_PR_KEY});
            else
                $Q->Where($where);

            if ($andwhere) {
                $args = func_get_args();
                array_shift($args);
                array_shift($args); // shift alias
#                array_unshift($args, ' AND ');

                return call_user_func_array(array($Q, 'Where'), $args);
            }
            return $Q;
        }
    }

    function _getColumns($alas = NULL, $purpose = self::FOR_INSERT, &$columns = NULL) {
//        if($this->loadedColumns && $purpose == self::FOR_INSERT)    $columns    =   array_keys($this->loadedColumns);
        if ($this->loadedColumns) {
            $columns = array_unique(array_merge((array) $columns, array_keys($this->loadedColumns + ($purpose == self::FOR_INSERT ? $this->_defaults : array())
            )));
        }
        return parent::_getColumns($alas, $purpose, $columns);
    }

    function loadArray($array) {
#	throw new Exception('array oeprand');
        parent::loadArray($array);
        $this->loadedColumns = ($this->loadedColumns ? $this->loadedColumns : array()) +
                $array;
//			$this->_defaults;
        return $this;
    }

    function setQube(Qube $q) {
        $this->qube = $q;
    }

    /**
     * 
     * @return Qube
     */
    function getQube() {
        if ($this->qube)
            return $this->qube;
        return Qube::Start();
    }

    /**
     * 
     * @return boolean
     */
    function Save() {
        if ($this->getID()) { // do an update.
            return $this->getQube()->Save($this, NULL, $this->getPKWhere()) instanceof HybridModel;
        }

        return $this->getQube()->Save($this) instanceof HybridModel;
    }

    /**
     * @param null $fields
     * @param null $fetch_mode
     * @param QubeDataAccess $qda
     * @return $this
     */
    function Refresh($fields = NULL, $fetch_mode = NULL, QubeDataAccess $qda = NULL) {
        if (!$qda)
            $qda = new QubeDataAccess($this->getQube());

        return $qda->refreshObject($this, $fields, $fetch_mode);
    }

    function getPKWhere() {
        if ($this->_PR_KEY)
            return array($this->_PR_KEY => $this->getID());
        return NULL;
    }

    function Update($dataarray) {
#        $class  =   get_called_class();        $obj    =   new $class;
        if ($this->getQube()->Save($this, $dataarray, $this->getPKWhere()))
            return $this;
        return NULL;
    }

    /**
     * Generate URLS for uploaded assets after saving/updating.
     * 
     * @param type $colkey
     * @param type $fileinfo
     * @param DashboardBaseController $Ctrl
     * @return type
     */
    function getURL($colkey, $fileinfo, DashboardBaseController $Ctrl = null) {
        return 'url for ' . (is_array($fileinfo) ? $fileinfo['name'] : $fileinfo);
    }

    function ProcessUploads($key, &$object_vars) {
        $upload = new Zend_File_Transfer();

        /**
         * the information returned from getFileInfo consists
         * of a a assoc-array with key
         *  such that a field name "website[logo]" is converted to website_logo_ 
         */
        $files = $upload->getFileInfo();

        $uploadsInfo = $this;

        $result = array();

        if (empty($files))
            return false;
        //|| !$upload->hasFiles()) return false;

        $uploads = array();
        foreach ($files as $fkey => $file) {
            if (empty($file['tmp_name']))
                continue;  // nothing uploaded..
            preg_match('/^' . $key . '_(.*?)_$/', $fkey, $matches);
            $ckey = $matches[1];

            // valid..
            $object_vars[$ckey] = array($upload, $fkey, $ckey, $file);  //$upload->getFileInfo($fkey);  // name
            $uploads[$ckey] = $file;  //$this->getURL($ckey, $object_vars[$ckey]);
//                    $object_vars[$key]    =   $file_info['name'];   // filename$uploadsInfo->getFiles('logo', 'name');
//                    $result['uploads'][$key]    =   $this->User->getStorageUrl('hub', $object_vars[$key]);
//                
        }
        return $uploads;
    }

    function ApplySaveData($object_vars) {
        foreach ($object_vars as $ckey => $val) {
            $this->set($ckey, $val);
        }
    }

    /**
     * Applies validation before saving an object. The $misc array is passed by reference, and sanitazion may be done by the Model's Validate method.
     * 
     * @param array $misc
     * @param type $key
     * @param ValidationStack $Validation
     * @param type $uploadInfo
     * @param type $update_where
     * @param type $result
     * @return boolean
     */
    function SaveData(array &$misc, $key, ValidationStack $Validation, $ctrl, $update_where, &$result) {
        $object_vars = &$misc;
        if ($key)
            $object_vars = &$misc[$key];

//        $uploadInfo =   new UploadsInfo();
        $ID = @$object_vars[$this->_PR_KEY];
        $uploads = false;
        if ($key && $ctrl && ($uploads = $this->ProcessUploads($key, $object_vars))) {
            $object_vars['?ctrl'] = $ctrl;
        }

        if (!$this->Validate($object_vars, $Validation))// Validate!!
            return false;


        if ($uploads) {
            foreach ($uploads as $ckey => $file) {
                $uploads[$ckey] = $this->getURL($ckey, $object_vars, $ctrl);
            }
        }
        $result['uploads'] = $uploads;
//            $isupload   =   $uploadInfo->accept($key, $Validation);

        $update = false;
        if ($ID) {
            $update = $update_where ? $update_where : $this->getUpdateWhere();
            $this->set($this->_PR_KEY, (int) $ID);
        }
//        echo 'saving';

        $event_args = $this->getEventManager()->prepareArgs(array('object_vars' => &$object_vars));

        if (FALSE === $update) {
            $proceed_flag = $this->getEventManager()->trigger('BeforeCreate', $this, $event_args);
        } else {
            $proceed_flag = $this->getEventManager()->trigger('BeforeUpdate', $this, $event_args);
        }

        if (FALSE == $proceed_flag || $proceed_flag instanceof ValidationStack) {
            return $proceed_flag;
        }
        $this->ApplySaveData($object_vars);

        $error = NULL;
        try {
            $qresult = $this->getQube()->Save($this, NULL,
//                $object_vars,
                    $update);
        } Catch (PDOException $e) {
#deb($e, $object_vars, $update);
            // @todo report errors to me!
            $error = $e;
            $qresult = false;
        }

        if ($qresult instanceof HybridModel && !$update) {
            $this->getEventManager()->trigger('Created', $this, $event_args);
        } else if ($qresult && $update) {
            $this->getEventManager()->trigger('Updated', $this, $event_args);
            return $qresult;
        } else {
            return $this->getEventManager()->trigger('SaveFailed', $this, array('object_vars' => $object_vars, 'error' => $error, 'Validation' => $Validation))->last();
        }
////        if($qresult)
////        {
////            $this->afterUpdate();
//        }

        return $qresult;
    }

    function getUpdateWhere() {
        return NULL;
    }

    protected function AcceptImages(VarContainer $VC, &$data, $dir, $colnames) {

        if ($VC->checkValue('?ctrl', $ctrl) && $ctrl instanceof DashboardBaseController) {
            $storage_path = $ctrl->getUser()->getStorageUrl($dir);
            FileUtil::CreatePath($storage_path);

            foreach ($colnames as $col) {
                // @todo validate image type
                if ($VC->checkValue($col, $upinfo) && $VC->check('is_array', $col)) {
                    list($upload, $fkey, $ckey, $finfo) = $upinfo;
                    if (!$upload->isValid($fkey)) {
                        //                    var_dump($matches, $fkey);
                        $VS->addErrors($ckey, $upload->getMessages());
                    } else {
                        $upload->setDestination($storage_path);

                        $newname = FileUtil::GetFileUniqueName($finfo['name'], $storage_path);
                        if ($newname != $finfo['name']) {
                            $upload->addFilter('Rename', array('target' => $newname), $fkey);
                        }
                        if (!$upload->receive($fkey)) {
                            $VS->addErrors($ckey, $upload->getMessages());
                        } else {
                            // all done!
                            $data[$col] = $newname;  //$this->getURL('photo', $finfo);  //
                        }
                    }
                }
            }
        }
    }

    function Validate(array &$data, ValidationStack $VS) {
        return !$VS->hasErrors();
    }

    /**
     * 
     * @param type $andwhere
     * @return static
     */
    static function Fetch($andwhere = NULL) {
        $query = call_user_func_array('parent::Query', func_get_args());
        return $query->Exec()->fetch();
    }

    function Delete() {
        if (!$this->getID())
            throw new QubeException();
        return $this->getQube()->query('DELETE FROM %s WHERE ' . DBWhereExpression::Create($this->getPKWhere()), $this);
    }

    protected function _Trash(Qube $q, $andwhere = 'user_id = dashboard_userid()', $trash = 0) {
        return $q->query('UPDATE %s SET trashed = %s WHERE %s', $this, $trash ? 'NOW()' : 0, DBWhereExpression::Create($this->getPKWhere())->appendWhere(' AND ', $andwhere))->rowCount();
    }

    protected function _Delete(Qube $q, $andwhere = 'user_id = dashboard_userid()', $LIMIT = 1) {
        return $q->query('DELETE FROM %s WHERE %s LIMIT %d', $this, DBWhereExpression::Create($this->getPKWhere())->appendWhere(' AND ', $andwhere), $LIMIT);
    }

    function Trash(Qube $q, $andwhere = '') { //user_id = dashboard_userid()')
        $affected_rows = $this->_Trash($q, $andwhere, 1);

        if ($affected_rows) {
            $this->getEventManager()->trigger('Trashed', $this);
        }

        return $affected_rows;
    }

    function UnTrash(Qube $q, $andwhere = 'user_id = dashboard_userid()') {
        return $this->_Trash($q, $andwhere, 0);
    }

    function toArray() {
        $columns = $this->_getSelectColumns();
        $load = array();
        foreach ($columns as $key)
            $load[$key] = $this->get($key);

        return $load;
    }

    function onUpdatedEvent(\Zend\EventManager\Event $ev) {
        
    }

    function onBeforeUpdatedEvent(\Zend\EventManager\Event $ev) {
        
    }

    function onBeforeCreatedEvent(\Zend\EventManager\Event $ev) {
        
    }

    function onCreatedEvent(\Zend\EventManager\Event $ev) {
        
    }

    function onTrashed(\Zend\EventManager\Event $ev) {
        
    }

    /**
     * @return string
     */
    function jsonSerialize() {
        $json = array();
        foreach ($this->__getColumns() as $field) {
            $json[$field] = $this->$field;
        }
        return $json;
    }

}
