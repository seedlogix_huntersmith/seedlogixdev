<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ContactForm Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * columns:
 * 
 * id	int(5)	NO	PRI	NULL	auto_increment
user_id	int(6)	NO	MUL	0	
parent_id	int(11)	NO		0	
type	varchar(5)	NO	MUL		
type_id	int(9)	NO		0	
cid	int(11)	NO		0	
name	varchar(150)	NO			
email	varchar(150)	NO			
phone	varchar(16)	NO			
website	varchar(255)	NO			
comments	text	NO		NULL	
page	varchar(150)	NO			
keyword	varchar(255)	NO			
search_engine	varchar(255)	NO			
responded	tinyint(1)	NO		0	
responses_received	text	NO		NULL	
optout	tinyint(1)	NO		0	
ip	varchar(255)	NO			
timestamp	timestamp	NO		CURRENT_TIMESTAMP	on update CURRENT_TIMESTAMP
created	timestamp	NO	MUL	0000-00-00 00:00:00	
 * 
 */
class ContactForm extends DbObject {    
    
    function _getSelectColumns() {
        return array('id', 'user_id');
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
}

class Lead extends ContactForm
{
    function _getSelectClause() {
        $where = parent::_getSelectClause();
        
        $where->Where('optout != 1');
        
        return $where;
    }
}
