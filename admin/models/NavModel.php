<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Nav Model (uses hub_page2)
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class NavModel extends PageModel {

    function __getColumns() {
        /*static $cols    =   array('ID', 'user_ID', 'sortk', 'hub_ID', 
'parent_ID', 'num_children', 'name', 'name_url', 'page_ID', 'created', 'oldtbl_ID');*/
        static $cols    =   array('id', 'user_id', 'nav_order', 'hub_id', 
'nav_parent_id', 'page_title', 'page_title_url', 'created', 'type', 'page_parent_id');
        return $cols;
    }

    function getNavs(){
        return $this->queryObjects('NavModel', 'N', "type = 'nav'")->Exec()->fetchAll();
    }
    
    function getNodes(){
        //return $this->queryObjects('PageTreeNodeModel')->orderBy('sortk asc')->Exec()->fetchAll();
        return $this->queryObjects('PageTreeNodeModel')->orderBy('nav_order asc')->Where('trashed = 0')->Exec()->fetchAll();
    }
    
    function getBindings() {
        $bindings   =   array(
            //'WebsiteModel'  =>  '/* %d */ id = ' . $this->hub_ID,
            //'NavModel'   =>  "hub_ID = {$this->hub_ID} AND parent_ID = %d",
            //'PageTreeNodeModel' =>  "hub_ID = {$this->hub_ID} AND parent_nav_ID = %d"
            'WebsiteModel'  =>  '/* %d */ id = ' . $this->hub_id,
            'NavModel'   =>  "hub_id = {$this->hub_id} AND nav_parent_id = %d",
            'PageTreeNodeModel' =>  "hub_id = {$this->hub_id} AND nav_parent_id = %d"
        );
        
        return $bindings;
    }


}
