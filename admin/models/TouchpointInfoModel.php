<?php
/**
 * Created by PhpStorm.
 * User: sofus
 * Date: 1/5/15
 * Time: 4:00 PM
 */

class TouchpointInfoModel extends HybridModel implements InputValidator {

    public $touchpoint_type, $touchpoint_name, $hub_ID, $blog_ID;

    function __getColumns()
    {
        return array('hostname', 'hub_ID', 'blog_ID',
                'touchpoint_name', 'touchpoint_type',
            'user_parent_ID', 'user_ID', 'campaign_ID');
    }

    function Validate(array &$vars, ValidationStack $vs)
    {

    }
}