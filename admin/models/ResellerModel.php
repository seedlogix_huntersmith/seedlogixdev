<?php

/**
 * Reseller Class
 *
 * @author Jon Aguilar
 */
class ResellerModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    // the admin_user = the ID of the user that owns this records
    public $id, $admin_user, $main_site, $main_site_id, $company, $logo, $fbapp_secret, $proposal_url, 
            $support_email, $fbapp_id, $twapp_id, $twapp_secret, $address, $city, $state, $zip, $phone, $bg_clr, $css_link, $prop_logo, $created;
    
    protected $_PR_KEY  =   'id';
    
    /**
     *
     * @var UserModel 
     */
    protected $_admin_user  =   null;
/**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c   =   array('id', 'admin_user', 'main_site', 'main_site_id', 'company', 'support_email', 'proposal_url',
            'logo', 'fbapp_id', 'fbapp_secret', 'twapp_id', 'twapp_secret', 'created',
            'bg-img','login-logo', 'login_url', 'address', 'city', 'state', 'zip', 'phone', 'bg_clr', 'css_link', 'prop_logo'
            );
        return $c;
    }
    
    /**
     * 
     * @return UserModel
     */
    function getAdmin(UserDataAccess $DA){
        if(!$this->_admin_user){
            $this->_admin_user  =   $DA->getUser($this->admin_user);
        }
        
        return $this->_admin_user;
    }
		
    function getMainStorage() {
        return UserModel::getStorage($this->admin_user) . '/brand';
    }
    
    function getMainStorageURL($path    =   ''){
//        return UserModel::getStorageURL_ID($this->admin_user). '/brand/' . $path;
        return UserModel::getStorageURL_ID($this->admin_user, '') . $path;
    }
    
    function _getDeleteClause() {
        ;
    }    
    
    function hasFacebookApp(){
        return !empty($this->fbapp_id) && !empty($this->fbapp_secret);
    }
    
    function getFacebook(){
        static $fb  =   NULL;
        if(!$fb) $fb    =   new Facebook(array('appId' => $this->fbapp_id, 'secret' => $this->fbapp_secret));
        return $fb;
    }
    
    function hasTwitterApp(){
        return !empty($this->twapp_id) && !empty($this->twapp_secret);
    }
    
    function getTwitter($useraccess_info){        
        return SocialMonitor::getTwitterObject($useraccess_info, $this->twapp_id, $this->twapp_secret);
    }
    
    function sendNotificationEmail(Notification $message)
    {
        $message->send();
    }
    
    function getFacebookAuthorizerForURL($redirect_uri){ 
        
//        $redirect_uri   =   'http://' . $this->main_site . '/api/facebook-blog-app.php?bloghttphost=' . $host;
        
	$url	='http://www.facebook.com/dialog/oauth/?client_id=' . $this->fbapp_id . 
                    '&redirect_uri=' . urlencode($redirect_uri) . 
			'&state=YOUR_STATE_VALUE&scope=' . urlencode('publish_actions,manage_pages,publish_stream');
        
        
        return $url;
    }
	
	function Validate(array &$data, \ValidationStack $VS)
	{
        $isUpdate = $this->getID() !== 0;
		if(!$isUpdate)
		{
			$vc	= VarContainer::getContainer($data);
			if($vc->isEmpty('company'))
			{
				$VS->addError('company', 'Company must be defined.');
			}
		}
		return !$VS->hasErrors();
	}
}

