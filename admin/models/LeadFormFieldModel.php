<?php

/**
 * LeadFormModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class LeadFormFieldModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $ID, $form_ID, $user_ID, $type, $label, $size, $options, $sort, $data = 'converted', $required, $label_note, $numeric, $default, $label1, $label2, $default1, $default2, $required1, $required2;

    protected $_PR_KEY  =   'ID';

    protected $_options =   NULL;
    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('ID', 'user_ID', 'sort',
                'form_ID', 'label', 'size', 'type',
                'label_note', 'required', 'numeric', 'default',
                'options', 'sort'
                    );
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
		
    function getLabel($part = 3)
    {
        if($part == 3)
        	return $this->label;

		if($part == 0)
			return $this->label1;

		if($part == 1){
			return $this->label2;
		}

		return $this->label;
    }

    function isRequired($part = 3)
    {
		if($part == 3)
			return $this->required;

		if($part == 0)
			return $this->required1;

		if($part == 1){
			return $this->required2;
		}

		return $this->required;
    }

    function getDefaultValue($part = 3)
    {
		if($part == 3)
			return $this->default;

		if($part == 0)
			return $this->default1;

		if($part == 1){
			return $this->default2;
		}

		return $this->default;
    }
    function getInputName()
    {
        if($this->ID) return $this->ID;
        return preg_replace('/[^\w]+/', '', $this->label);
    }
    
    function getOptions(){
        if(is_null($this->_options)){
            if($this->options){
                $this->_options = @unserialize ($this->options);
                if(!$this->_options)
                    $this->_options =   array();
            } else {
                $this->_options =   array();
            }
        }
        return $this->_options;
    }

    function hasLabelNote()
    {
        return !empty($this->label_note);
    }

    function getUserFriendlyValue($val)
    {
        if(empty($val))
            return '';
        return $val;
    }
        
    function Validate(array &$data, ValidationStack $VS){
        if(!isset($data['size'])){
            $data['size']   =   0;
        }
        if(@$data['numsOnly']){
            $data['numeric']    =   1;
        }
        if(@$data['options']){
            ksort($data['options'], SORT_REGULAR);
            $tmp    =   array();
            foreach($data['options'] as $row){
                if(is_array($row))
                    $tmp[$row['key']]   =   $row['value'];
                elseif(!empty($row)){
                    $tmp[]  =   $row;

                }
            }
            $data['options']        = serialize($tmp);
        }
        if(@$data['checked']){
            $data['default']    =   1;
        }
        
        if(@$data['type']    ==  'date'){
            if(@$data['default'] == 'custom')
                $data['default']       =    $data['defaultY'] . '-' . $data['defaultM'] . '-' . $data['defaultD'];
        }
        
        if(@$data['type']    ==  'name')
        {
            if(isset($data['label1'], $data['label2']))
                $data['label']  =   @$data['label1'].", ".$data['label2'];
//            var_dump($data);exit;
            
            if(isset($data['default1'], $data['default2']))
                $data['default']    =   @$data['default1']."\n".@$data['default2'];
            
            if(isset($data['required1'], $data['required2']))
                $data['required']   =   @$data['required1'] + @$data['required2']*2;    // hardcore binary hack here. 
        }
        
        if($data['type']    ==  'states'){
            $data['type']   =   'state';
            var_dump($data);
            throw new Exception();
        };
        foreach($data as $key => $val){
            if(empty($val)) $data[$key] =   '';
        }

        if(isset($data['label']) && trim($data['label']) == '')
        {
            $VS->addError('label', 'Label must not be empty.');
        }
        return !$VS->hasErrors();
        
        if(!isset($data['name'])){
            $VS->addError('name', 'You must define a name');
            
        }
        return !$VS->hasErrors();
    }

    public function getLabelNote()
    {
        return $this->label_note;
    }

    function loadArray($array)
    {
        parent::loadArray($array);
        if(isset($array['label1'])){
            $this->label1 = $array['label1'];
        }

        if(isset($array['label2'])){
            $this->label2 = $array['label2'];
        }

		if(isset($array['default1'])){
            $this->default1 = $array['default1'];
        }

		if(isset($array['default2'])){
            $this->default2 = $array['default2'];
        }

		if(isset($array['required1'])){
            $this->required1 = $array['required1'];
        }

		if(isset($array['required2'])){
            $this->required2 = $array['required2'];
        }

		return $this;
    }


//    
//    function _set($c, $n) {
//        
//        if($c   ==  'options'){
//            
//        }
//        
//        parent::_set($c, $n);
//    }
}

