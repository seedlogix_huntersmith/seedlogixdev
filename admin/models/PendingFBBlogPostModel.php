<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of PendingFBBlogPostModel
 *
 * @author amado
 */
class PendingFBBlogPostModel extends ScheduledBlogPostPublicationModel
{   
    protected $httphostkey;
    protected $post_to_fbid;
    
    protected $tokens   =   array();
    
    function _getCondition($T, $purpose = self::FOR_SELECT) {
        return parent::_getCondition($T, $purpose)->Where('b.facebook_id !=""');
    }
    
    static function Select(Qube $q)
    {
        return ScheduledBlogPostPublicationModel::SelectPost($q, get_class(), 'FB_BLOG_POST')->
                    addFields('b.facebook_id as post_to_fbid');
    }
    
    function loadFBObject(&$map, Qube $q, $default_config)
    {
        if(!isset($map[$this->user_parent_id]))
        {
            $stmt   =   $q->query('SELECT fbapp_id as appId, 
                                          fbapp_secret as secret,
                                          0 as cookie FROM %s WHERE admin_user = %d OR admin_user = %d
                                            ',
                        'ResellerModel', $this->user_parent_id, $this->user_id);
            if($stmt    ==  false || $stmt->rowCount() == 0)
            {
                $map[$this->user_parent_id] =   false;
            }else{
                $assoc  = $stmt->fetch(PDO::FETCH_ASSOC);
                if($assoc['appId'] == '' || $assoc['secret']    ==  '')
                {
                    $assoc  =   $default_config;
                }
                
                $map[$this->user_parent_id] =   new Facebook($assoc);
                

/*
                // get an access token to make a post with
                
                $args = array(
                'grant_type' => 'client_credentials',
                'client_id' => $assoc['appId'],
                'client_secret' => $assoc['secret']
                );

                $ch = curl_init();
                $url = 'https://graph.facebook.com/oauth/access_token';
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                $data = curl_exec($ch);
                list(,$token) = explode('=', $data);
                $this->tokens[$assoc['appId']]   =   $token;
*/
            }
        }
        return $map[$this->user_parent_id];
    }
    
    function getPhotoURL()
    {
//        return 'http://6qube.com/admin/themes/6610/logo_295.png';
        
        if($this->photo)
            return 'http://' . $this->httphostkey . '/users/' . UserModel::GetUserSubDirectory($this->user_id) . '/blog_post/' . $this->photo;
        
        // else
        return 'http://' . $this->httphostkey . '/img/no_image.jpg';
    }
    
    function getFBAttachment($access_token)
    {
        $post_link  =   $this->getPostURL();     
        
        return array('message' => $this->seo_title,
                    'access_token' => $access_token,
                            'name' => $this->post_title,
#                            'caption' => 'this is a caption',
                            'link' => $post_link,
                            'description' => $this->post_desc,
                            //'picture' => $this->getPhotoURL(),
                            'actions' => array('name' => 'View Link Now', 'link' => $post_link)
                            );
    }
    
    function PostToFacebook(Facebook $fb)
    {
        list($page_id, $pagename, $token)  =   explode("\n", $this->post_to_fbid);
        
        $attachment =   $this->getFBAttachment($token);
        
        $result = $fb->api('/' . $page_id . '/feed/', 'post', $attachment);
        
        return $result;
    }
    
}
