<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of NetworkArticleModel
 *
 * @author amado
 */
class DirectoryArticleModel extends NetworkItemLegacyModel {
    
    protected $_PR_KEY = 'id';
    
    protected $_defaults	=	array('created' => 'NOW()');
    public $id  =   NULL;
    protected $photo; // hack
    
    function __getColumns() {
        $cols	=	array('aid','basic_listing','blog','category','cid','city','class_id',
'company_name','company_spot','contact_spot','created','description','diigo',
'display_info','email_id','facebook','id','keywords','keyword_five','keyword_five_link','keyword_four',
'keyword_four_link',
            'keyword_one','keyword_one_link','keyword_six','keyword_six_link','keyword_three',
'keyword_three_link','keyword_two','keyword_two_link','last_edit','linkedin',
'myspace','other_spots','phone','photo','pic_five','pic_five_title','pic_four',
'pic_four_title','pic_one','pic_one_title','pic_six','pic_six_title','pic_three','pic_three_title',
'pic_two','pic_two_title','state','street','technorati','thank_you','title',
'tracking_id','twitter','type','user_id','user_parent_id','video_spot',
'website_url','youtube','zip','name','trashed');
				return $cols;
    }
    //put your code here
    
    function Validate(array &$data, \ValidationStack $VS) {
			
			$VC	= VarContainer::getContainer($data);
			if(!$this->getID())
			{
				if(!$VC->check('intval', 'cid', 'user_id', 'user_parent_id'))
								$VS->addError ('id', 'Not all required ids were provided for this model.');
			}
      
      $this->AcceptImages($VC, $data, 'directory', array('photo'));     
          
			return !$VS->hasErrors();
    }
   
    function getphoto()
    {
        $storage  = UserModel::getStorageURL_ID($this->user_id) . '/directory/'; 
        return $storage . $this->photo;
    }
    
    function getURL($colkey, $obj_vars, DashboardBaseController $Ctrl)
    {
      switch($colkey){
        case 'photo':
          return $Ctrl->getUser()->getStorageUrl('directory', $obj_vars[$colkey]);
      }
    }
		
}
