<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of HubRowModel
 *
 * @author amado
 */
class HubRowModel extends PageRowModel
{
	/**
	 *
	 * @var HubDataAccess
	 */
	private $da	=	NULL;
	
	static function getBlogURL($row)
	{
#deb($row['connect_blog_id'], $row->connect_blog_id);
		if(!$row['connect_blog_id'])
		{
			return '#Blog_Not_Connected';
		}
		$BlogDA	=	new BlogDataAccess();
		$blogModel = $BlogDA->getObject('BlogModel', 'httphostkey', 
						'id = :blog_id AND trashed = 0', array('blog_id' => $row['connect_blog_id']));
		if($blogModel)
		{
			return $blogModel->getBlogURL();
		}
		return "#Blog_Not_Found";
	}
    
    static function getFullState($state)
    {
        if($state == '')
            return '';
        $stateArray = Defaults::getStates();
        return $stateArray[$state];
    }
	
	function getVar($contentname, $varname, PageRowModel $pagerow)
	{
		switch($varname)
		{
			case "blogfeed":
					return $this->getBlogFeed(new BlogDataAccess());
				break;
		}
		
		return "!~error~!";
//		echo 'calling using these vasr:', print_r(func_get_args(), true);		return "sup";
	}
	
	function getBlogFeed(BlogDataAccess $bda, $limit	=	5)
	{
		if($this['connect_blog_id'] == 0)	return '';
		$posts	=	$bda->getLatestPosts($this['connect_blog_id'], $limit);
		return BlogModel::getHTMLPosts($posts);
	}
	
	static private function isParsableField($index){
		static $parsableHubFields	=	NULL;
		if(!$parsableHubFields)
			$parsableHubFields	=	Hub::getParsableFields ();
		
		return in_array($index, $parsableHubFields);
	}

	function findFormsInFields(array $fields)
	{
		$form_ids	=	array();
		foreach($fields as $key=>$field){
			if(!is_string($this[$field])) continue;
			$a = array();
			if(preg_match_all("/\|@form-(.*?)@\|/", $this[$field], $a)) {
				$form_ids[$field] = $a[1];
			}
		}
		return $form_ids;
	}
	
	static function ParseHubRow(HubRowModel $HR)
	{
		require_once QUBEADMIN . 'inc/hub.class.php';
		$tr	=	$HR->getTranslator();
		$curField	=	'null';
		
		// disable translation to get the list of original values
		$HR->setTranslator(NULL);
		
		$variableMap	=	Hub::getTagReplacementValues($HR);
		$variableMap['blogfeed']	=	array(array($HR, 'getVar'),
					array(&$curField, 'blogfeed', $HR));
		$tagparser	=	new PlaceHolderParser($variableMap);
		
		// enable translation
		$HR->setTranslator($tr);
		
		$dependency_bullshit	=	new Hub();
		foreach(Hub::getParsableFields() as $parsableKey)
		{
			$curField	=	$parsableKey;
			$HR[$parsableKey]	=	$tagparser->evSTR($HR[$parsableKey]);
			Hub::ReplaceNavigationTags($HR, $parsableKey, $dependency_bullshit);
		}
	}
	//put your code here
	
	function setDataAccess(HubDataAccess $da)
	{
		$this->da	=	$da;
	}
	
	function getDataAccess()
	{
		if($this->da) return $this->da;
		return new HubDataAccess();
	}

	/**
	 * This is called when a value is not found by offsetget() (or it returns an empty string.)
	 * reaching the end of this function means that the value is set and it is an empty string
	 * or that no data was found.
	 * 
	 * @param type $key
	 * @return string
	 * @throws Exception
	 */
	function _fetchDataIfExists($key)
	{
#		if(!$this->da)			throw new Exception();
		
		// load pic_ values
		if(preg_match('/^pic_/', $key) && !isset($this->_picscount))
		{
			$this->_picscount	=	$this->getDataAccess()->getPics($this['id'], $this);
#	throw new Exception();
#			if($this->_picscount)
#				return $this[$key];
#			else
#				return '';
#			deb($this);
#			if(!$count)				$this->_nopics	=	true;
		}

		return parent::offsetGet($key);
	}
	
	function offsetGet($index) {
		$value	=	parent::offsetGet($index);	// get the translated value
		if($value) return $value;
		// if $value does not exist.. attempt to fetch it from other tables.
		return $this->_fetchDataIfExists($index);
	}

    /**
     * Returns the absolute url path to the slide given a slide key (pic_one, pic_two, .. etc)
     * example: /users/uX/uXXXX/hub/$pic_one.jpg
     * baseUrl should NOT have a trailing slash.
     * @param $picname
     * @return string
     */
    public function getSlideUrlPathByName($picname, $baseUrl = "")
    {
        $pic_imagename  =   $this[$picname];    // get value of $this['pic_one'];
//	echo $this['user_id'];
        // if value is a basename,..
        if(strpos($pic_imagename, "/") === FALSE){
            return $baseUrl . "/" . UserModel::getStorageURL_ID($this['user_id']) . "/hub/" . $pic_imagename;
        }

        // if pic image contains paths.. concatenate users path.
        return UserModel::USER_DATA_DIR . $pic_imagename;
    }

    /**
     * Note: $baseUrl SHOULD have a trailing slash!!
     *
     * @param $colname
     * @param string $baseUrl
     * @return string
     */
	public function getImageUrl($colname, $baseUrl = ""){
		$value = $this->getValue($colname);
        // if value is a basename,..
		if(strpos($value, "/") === FALSE){
			return $baseUrl . UserModel::getStorageURL_ID($this['user_id']) . "/hub/" . $value;
		}

		return UserModel::USER_DATA_DIR . $value;
	}
}
