<?php

class NoteModel extends HybridModel{

	protected $_PR_KEY = 'id';

	function __getColumns()
	{
		$columns = array ('id','contact_id','user_id','user_parent_id','cid','account_id','lead_id','subject','type','note','trashed','last_updated','created');
		return $columns;
	}

	function getFormattedDate(){
		$FormattedDate = new DateTime($this->created);
		$FormattedDate = $FormattedDate->format('Y-m-d');
		return $FormattedDate;
	}
	
	//timeline source http://www.melonhtml5.com/demo/timeline/javascript/demo.js

	/*function getTimeLineNote($delete_link = ''){
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "<span class=\"notetimeline-title\"><i class=\"icon-pencil-2\"></i>  {$this->subject}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"bDefault f_rt jajax-action\" title=\"Delete the Note\"><span class=\"delete\"></span></a>"
		);
	}*/
	
	function getTimeLineNote($delete_link = ''){
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "<span class=\"notetimeline-title\"><i class=\"icon-pencil-2\"></i> Note Added: {$this->subject}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"bDefault f_rt jajax-action\"><span class=\"delete\"></span></a>"
			//'content' => "<p>{$this->note}</p>"
		);
	}

}