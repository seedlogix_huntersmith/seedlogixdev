<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BlogRowModel
 *
 * @author amado
 */
class BlogPostRowModel extends PageRowModel {
	
	protected static $parsable	=	array('post_content', 'post_desc', 'seo_title', 'tags', 'tags_url', 'author', 'author_url');
	protected static $blogtags	=	array('city', 'phone', 'state');
	protected static $embbedableForms = array('post_content');
	
	/**
	 *
	 * @var PlaceHolderParser
	 */
	protected $_parser	=	NULL;
	
	function __construct($array, BlogModel $blog_model) {

		$hub_row = HubModel::Fetch('id = %d', $blog_model->connect_hub_id);

		parent::__construct($array);
		$this->setTranslator(array($this, 'parseTags'));
		$this->_parser	=	new PlaceHolderParser($blog_model->getValues(NULL, DBObject::FOR_SELECT), '((blog.');
		//$this->_parser	=	new PlaceHolderParser($hub_row->getValues(NULL, DBObject::FOR_SELECT), '((');

	}
	
	function parseTags($rawvalue, $index, BlogPostRowModel $obj)
	{
		if(!in_array($index, self::$parsable))	return $rawvalue;
		$rawvalue = $this->parseEmbbedForms($index, $rawvalue);
		return $this->_parser->evSTR($rawvalue);	// evaluate the parsable value
	}

	function parseTags2($rawvalue, $index, BlogPostRowModel $obj)
	{
		if(!in_array($index, self::$parsable))	return $rawvalue;
		//$rawvalue = $this->parseEmbbedForms($index, $rawvalue);
		return $this->_parser2->evSTR($rawvalue);	// evaluate the parsable value
	}

	function parseEmbbedForms($field, $value){
		if(!in_array($field, self::$embbedableForms)) return $value;

		if(preg_match('/\|@form-(.*?)@\|/', $value, $matches)){
			$form_id = $matches[1];
			$blogModel = new BlogModel(null, $this->_parser->getVars());
			$Form   =   new FormOutput();

			$params =   array('formID' => $form_id,
				'userID'	=> $blogModel->user_id,
				'hubID' => 0,
				'hubPageID' => 0,
				'searchEngine' => '',
				'seKeyword' => '',
				'js'    => false,
				'style' => false,
				'outputWrapper' => 'router'
			);
			return str_replace($matches[0], '', $value). $Form->GetForm($params, $blogModel);
		}

		return $value;
	}
}
