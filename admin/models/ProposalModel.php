<?php

/**
 * HubModel Class
 *
 * @author Jon Aguilar
 */
class ProposalModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id, $user_id, $user_parent_id, $account_id, $cid, $contact_id, $status, $accepted_date, $name, $expire_date, $trashed, $last_updated, $sent, $created;
    
    protected $_PR_KEY  =   'id';


        /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id', 'user_id',
						'user_parent_id', 'account_id', 'cid', 'contact_id', 'name', 
            'status', 'accepted_date', 'expire_date', 'trashed', 'last_updated', 'sent', 'created'
                    );
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
	
	function getFormattedDate(){
		$FormattedDate = new DateTime($this->created);
		$FormattedDate = $FormattedDate->format('Y-m-d');
		return $FormattedDate;
	}
	
	//timeline source http://www.melonhtml5.com/demo/timeline/javascript/demo.js

	function getTimeLineProposal($delete_link = ''){
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "<span class=\"proposaltimeline-title\"><i class=\"icon-paper-2\"></i> Proposal Added: {$this->name}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "<p>A proposal was created for {$this->name}.</p>"
		);
	}
	
}

