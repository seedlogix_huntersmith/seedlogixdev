<?php
/**
 * NetworkModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class NetworkModel extends HybridModel {
	/**
	 *
	 * @var ResellerModel 
	 */

	protected $_Reseller = NULL;

    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat

     */

    

	public $id,
	$httphostkey,
	$admin_user,
	$tracking_id,
	$parent,
	$type,
	$name,
	$domain,
	$company,
	$industry,
	$category,
	$class_only,
	$featured_class,
	$city_focus,
	$state_focus,
	$link_type,
	$remove_search,
	$remove_contact,
	$phone,
	$google_verif,
	$seo_title,
	$seo_desc,
	$city_seo_title,
	$city_seo_desc,
	$citycat_seo_title,
	$citycat_seo_desc,
	$network_name,
	$hubs_network_name,
	$press_network_name,
	$articles_network_name,
	$blogs_network_name,
	$search_network_name,
	$twitter,
	$facebook,
	$youtube,
	$linkedin,
        $google_local,
	$blog,
	$logo,
	$favicon,
	// $header-bg-img,
	// $footer-bg-img,
	// $nav-link-hover-clr,
	$bg_color,
	$free_link,
	$no_image,
	$adv_css,
	$google_stats,
	$edit_region_1,
	$edit_region_2,
	$edit_region_3,
	$edit_region_4,
	$edit_region_5,
	$edit_region_6,
	$edit_region_7,
	$edit_region_8,
	$edit_region_9,
	$edit_region_10,
	$theme,
	$setup,
	$trashed,
	$created,
	$fb_post_authid;

    
	protected $_PR_KEY  =   'id';


	/**
	*
	* @return type 
	* 
	*/
	function __getColumns() {

		static $c = array('id', 'fb_post_authid',
		'httphostkey',
		'admin_user',
		'tracking_id',
		'parent',
		'type',
		'name',
		'domain',
		'company',
		'industry',
		'category',
		'class_only',
		'featured_class',
		'city_focus',
		'state_focus',
		'link_type',
		'remove_search',
		'remove_contact',
		'phone',
		'google_verif',
		'seo_title',
		'seo_desc',
		'city_seo_title',
		'city_seo_desc',
		'citycat_seo_title',
		'citycat_seo_desc',
		'network_name',
		'hubs_network_name',
		'press_network_name',
		'articles_network_name',
		'blogs_network_name',
		'search_network_name',
		'twitter',
		'facebook',
		'youtube',
		'linkedin',
		'blog',
		'logo',
		'favicon',
		'header-bg-img',
		'footer-bg-img',
		'nav-link-hover-clr',
		'bg_color',
		'free_link',
		'no_image',
		'adv_css',
		'google_stats',
		'edit_region_1',
		'edit_region_2',
		'edit_region_3',
		'edit_region_4',
		'edit_region_5',
		'edit_region_6',
		'edit_region_7',
		'edit_region_8',
		'edit_region_9',
		'edit_region_10',
		'theme',
		'setup',
		'trashed',
		'created');

		return $c;

	}

    
	function _getDeleteClause() {
	
		;
	
	}


	function Validate(array &$vars, ValidationStack $vs){
	
		$vc = VarContainer::getContainer($vars);
		$HostValidator  =   new Zend_Validate_Hostname();
		
		if(!$vc->checkValue('httphostkey', $hostkey) || !$HostValidator->isValid($hostkey))
		{
			$vs->addError('httphostkey', 'You must provide a valid hostname.');
		}
		return !$vs->hasErrors();
	
	}


	function getBindings() {
	
		return array('ResellerModel' => '/* %d */ admin_user = ' . $this->admin_user);
		//parent::getBindings();
	
	}
    

	/**
	* 
	* @return ResellerModel
	*/
	function getReseller(){
	
		if(!$this->_Reseller)
			$this->_Reseller = $this->queryResellers()->Exec()->fetch();

		return $this->_Reseller;

	}
        
        static function getDataTableColumn($index){
            $columns = array(
                'name',
                'httphostkey',
                'type',
                'created'
            );
            return $columns[$index];
        }

}