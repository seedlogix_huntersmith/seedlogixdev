<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of QubeTokenModel
 *
 * @author amado
 */
class QubeTokenModel {
    //put your code here
    
    function debug(){
        var_dump($this);
    }
    
    function Invalidate(Qube $q){
        $q->query('UPDATE 6qube_tokens SET used = NOW() WHERE ID = "' . $this->ID . '"');
    }
    
    function getResellerID(){
        return $this->reseller;
    }
    
    function getUserID(){
        return $this->object_id;
    }
    
    function doLoginUser(SecureController $C){
      AuthController::doLoginSessionUser(UserModel::Fetch('id = %d', $this->getUserID()), $C);
      
        $C->setLoggedInUserID($this->getUserID());
        $C->loadResellerData($this->getResellerID());
        $C->setUserId($this->getUserID());  // @todo not sure aobut this..
        $this->Invalidate(Qube::Start());
    }
}
