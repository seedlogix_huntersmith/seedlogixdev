<?php

/**
 * CampaignCollab Class
 *
 * @author  Amado Martinez <amado@projectivemotion.com>
 * @note    campaign_collab_sitref is table of collab->site values
 * 
 */
class CampaignCollab extends HybridModel {
// DBObject {
    
    const SCOPE_CAMPAIGN    = 'campaign';
    const SCOPE_CUSTOM      = 'custom';
    protected $_PR_KEY  =   'id';
    
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id, $campaign_id, $name, $email, $scope, $active;
    
    /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c   =   array('id', 'campaign_id', 'scope', 'name', 'email', 'active');
        return $c;
    }
    
    /*
     * function _getCondition($alias, $purpose = self::FOR_SELECT) {
     *
        parent::_getCondition($alias, $purpose);
        }() {
        return array('id' => $this->id);
    }
    */
    function setSiteIds($site_ids)
    {
        $stmt = Qube::GetPDO()
                    ->Prepare('INSERT INTO campaign_collab_siteref (collab_id, site_id) VALUES (?, ?)');
        
        
        Qube::GetPDO()->query('DELETE FROM campaign_collab_siteref WHERE collab_id = ' . $this->id);
        
        Qube::GetPDO()->beginTransaction();
        foreach($site_ids as $id)
        {
            $stmt->execute(array($this->id, $id));
        }
        
        Qube::GetPDO()->commit();
        
    }

	function Trash(Qube $q, $andwhere = '') //user_id = dashboard_userid()')
	{
		$db = $q->GetPDO();
		$db->beginTransaction();
		$collab_id = intval($this->id);
		$db->exec("DELETE FROM campaign_collab_siteref WHERE collab_id = $collab_id");
		$return = parent::Trash($q, $andwhere);
		$db->commit();
		return $return;
	}


}


#CampaignCollab::Prepare('CampaignCollab');
