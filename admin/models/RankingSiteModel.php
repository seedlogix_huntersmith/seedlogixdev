<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/16/14
 * Time: 2:15 PM
 */

class RankingSiteModel extends HybridModel{
	public $ID;
	public $user_ID;
	public $ts;
	/** @var  DateTime */
	public $created;
	public $touchpoint_ID	=	0;
	public $campaign_ID;
	public $hostname;

	function __getColumns()
	{
		return array(
			'ID', 'user_ID', 'ts', 'created', 'campaign_ID', 'hostname', 'touchpoint_ID'
		);
	}
	
	function Validate(array &$data, \ValidationStack $VS) {
		$vc= VarContainer::getContainer($data);
		$is_insert  =   !$vc->checkValue('ID', $id);
		$checkName  =   $vc->exists('hostname') || $is_insert;
		if($checkName && (!$vc->checkValue('hostname', $sitename) || trim($sitename) == '') && !$vc->exists('touchpoint_ID'))
		{
			$VS->addError('hostname', 'You must provide a valid hostname for this site.');
		}
		return parent::Validate($data, $VS);
	}
}