<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class NetworkListingModel extends HybridModel implements InputValidator {
    
    protected $_defaults	=	array();
		protected $_PR_KEY	=	'ID';
		public $ID, $tbl, $cid;
    protected $tbl_ID;  // hack..
		
		
		static $types	=	array('directory' => 'DirectoryArticleModel',
							'press'	=> 'PressArticleModel',
							'article'	=>	'NetworkArticleModel');
			
		function __getColumns() {
			return array('ID', 'tbl', 'tbl_ID', 'cid');
		}
    
    function gettbl_ID(){
        if(isset($this->tbl_ID))
          return $this->tbl_ID;
      return $this->getID();
    }
    
    function Validate(array &$vars, ValidationStack $vs){
        $vc = VarContainer::getContainer($vars);
        
        if(!$this->getID())
        {
            if(!$vc->checkValue('tbl_ID', $tblid))
            {
                $vs->addError('tbl_ID', 'You must provide a valid tbl_ID');
            }
            if(!$vc->checkValue('cid', $cid))
                    $vs->addError('cid', 'Valid CID is required');
        }else{
					
        }
        
        return !$vs->hasErrors();
    }
    
    function _getDeleteClause() {
        ;
    }
		
		/**
		 * 
		 * @staticvar array $c
		 * @param type $type
		 * @return NetworkItemLegacyModel
		 * @throws QubeException
		 */
		static function GetModel($type)
		{
			$c	=	static::$types;
			if(!isset($c[$type]))	throw new QubeException('Failed to find article of type . ' . $type);
			
			return new $c[$type];
		}
		
		static function getType($model){
			return array_search($model, static::$types);
		}
		
		
	
	function getNetworkListingType(){
		if(!$this->tbl){
			// if this is not from the view (table join specifying the table.) 
			// then set tbl value from own class
			$this->tbl	=	self::getType(get_class($this));
		}
		return $this->tbl;
	}
}

