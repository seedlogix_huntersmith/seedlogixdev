<?php

require_once 'Response.php';
require_once QUBEPATH . '../classes/Notification.php';
require_once QUBEPATH . '../classes/ResponseNotification.php';

/**
 * Responder Class
 *
 * @deprecated
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * Table Structure: 
 * id 	int(11)	NO 	PRI 	NULL	auto_increment
user_id 	int(11)	NO 		0	
user_parent_id 	int(11)	NO 		0	
cid 	int(11)	NO 		0	
type 	int(11)	NO 		1	
table_name 	varchar(255)	NO 			
table_id 	int(11)	NO 		0	
name 	text	NO 		NULL	
active 	tinyint(1)	NO 		0	
sched 	int(11)	NO 		0	
sched_mode 	varchar(50)	NO 			
from_field 	varchar(255)	NO 			
subject 	text	NO 		NULL	
body 	text	NO 		NULL	
return_url 	varchar(255)	NO 			
contact 	text	NO 		NULL	
anti_spam 	text	NO 		NULL	
optout_msg 	text	NO 		NULL	
theme 	tinyint(11)	NO 		0	
logo 	varchar(255)	NO 			
bg_color 	varchar(75)	NO 			
links_color 	varchar(75)	NO 			
accent_color 	varchar(75)	NO 			
titles_color 	varchar(75)	NO 			
edit_region1 	blob	NO 		NULL	
call_action 	blob	NO 		NULL	
edit_region2 	blob	NO 		NULL	
 */
class Responder extends DBObject {    
    
    // Sched [ 0 = immediately, else: time interval in $sched_mode units]
    public $id, $sched,
            $sched_mode,    // hours, days, weeks, months
            $from_field,
            $subject,
            $body,
            $stream_blog_id,
            $table_name,    // a bit unclear how this column is used
                            // I believe it is a reference to a table
                            // that triggers the autoresponder somehow.
            $table_id,      // @todo @important
            
            $theme
            
            ;
        
    function __getColumns() {
        static $c   =   array('id', 'user_id', 'sched', 'sched_mode', 'body', 'subject', 'from_field', 
                'theme', 'table_id', 'table_name', 'stream_blog_id',
            
            // antispam stuff:
            'anti_spam', 'contact', 'optout_msg',
            
            
            // the following particular email theme configurations would fit better on a separate table
            
            'bg_color', 'links_color', 'accent_color', 'titles_color', 'call_action', 
            'table_id', 'table_name', 'edit_region2', 'edit_region1', 'logo'
                );
        return $c;
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
    
    /**
     *
     * Schedule a Response or send an Instant Response.
     * 
     * @param ContactForm $form 
     * @return int number of responses successffully sent.
     */
    function TriggerResponse(ResponseTriggerEvent $Trigger, ScheduledResponse $response)
    {
        $notified = 0;
        if($this->isInstantResponder())
        {
            // the question is do we want to LOG instant responses? 
            // @todo increment user email sent count!
            
            $mail = $this->createNotification($Trigger);
            $sent = $Trigger->mailer->Send($mail);
            if(!$sent)
            {   // try again in 1 hour if it failed
                $response->setScheduledTime(1, 'hours');
            }else
                $notified = 1;
        }
        
        return $notified;
    }
    
    /**
     *
     * @param ResponseTriggerEvent $Trigger
     * @return ResponseNotification 
     */
    function createNotification(ResponseTriggerEvent $Trigger)
    {
//        static $s
        $Trigger->setResponder($this);
        
        // @todo improve this
        
        /*
        if($this->isInstantResponder())
        {
            /*
            require_once QUBEPATH . '../classes/InstantResponseNotification.php';
            $N = new InstantResponseNotification($Trigger);//->form, $Trigger->user, $this);
        }
        else {
            */
            
            require_once QUBEPATH . '../classes/ThemedResponseNotification.php';
            
            if($Trigger->evt_type == 'leads')
            {
                require_once QUBEPATH . '../classes/LeadResponseNotification.php';
                return new LeadResponseNotification($Trigger);
            }
            
            $N = new ThemedResponseNotification($Trigger);
//        }
            
        return $N;
    }
    
    function hasTheme()
    {
        return $this->theme ? 1 : 0;
    }
    
    function getTheme()
    {        
        return Qube::GetDriver()->getAutoResponderThemeWhere('id = %d', $this->theme);
    }
    
    function isInstantResponder()
    {
        return $this->sched == 0 ? 1 : 0;
    }
    
    function CreateResponse($datatable, $response_vars)
    {
        require_once QUBEPATH . '../classes/ThemeConfig.php';
        
        $response = NULL;            
        switch($datatable)
        {
            case 'contact_form':
                return $this->createFormResponse($response_vars);
               break; 
           
            case 'leads':
                return $this->createLeadResponse($response_vars);
                break;
        }
        
        throw new QubeException($datatable);
        return $response;
    }
    
    /**
     *
     * needs array of contact_name, contact_email, contact_phone, and contact_comments
     * @param object $vars 
     * @return ContactResponse, SendTo
     */
    function createFormResponse($ScheduledResponse)
    {
        require_once QUBEPATH . '../classes/ContactResponseNotification.php';
        
        $sendTo     = new SendTo($ScheduledResponse->contact_name, $ScheduledResponse->contact_email);
        
        $Response   =   new ContactResponse(array(
                    'name'      => $ScheduledResponse->contact_name,
                    'email'     => $ScheduledResponse->contact_email,
                    'phone'     => $ScheduledResponse->contact_phone,
                    'comments'  => $ScheduledResponse->contact_comments,
                    'recipient_email' => $sendTo->getEmail())
                );

        // @todo create a view to determine responders with missing data!

        return array($Response, $sendTo);
    }
    
    function createLeadResponse($ScheduledResponse)
    {
        $sendTo     =   new SendTo($ScheduledResponse->lead_name, $ScheduledResponse->lead_email);        
        
        $Response   =   new LeadResponse(
                array('name' => $ScheduledResponse->lead_name,
                        'recipient_email' => $sendTo->getEmail(),
                        'email' => $sendTo->getEmail())
                );//$ScheduledResponse->responder_subject);

        $Response->loadVars(Qube::GetDriver(), $ScheduledResponse->lead_id);
        
        return array($Response, $sendTo);
    }
    
    function ConfigureResponse(ResponseEmail &$N,
                $ResponseConfig,
                QubeSmarty $Smarty,
                $placeholders   =   array())
    {
        $N->setMessage($this->body);
        $N->setSubject($this->subject);
        $N->setSender(new SendTo(NULL, $this->from_field));        
        
        $N->setPlaceHolders($placeholders);

        if($this->theme)
        {        
            $vars  =   ThemeConfig::Load($this->id, 'auto_responder');
#            $vars   =   $this->toArray();
            $vars['site_root'] =   'http://' . $ResponseConfig->main_site . '/';
            $vars['themepath'] =   'http://' . $ResponseConfig->main_site . '/emails/themes';
            $vars['responder_phone']     =   $ResponseConfig->responder_phone;
#            $vars['logo']       = $ResponseConfig->site_logo;

#            deb($vars);
            
            $N   =   new ThemedEmail($Smarty, $N);
            $N->setThemeVars($vars);
            $N->setThemeName($this->theme);

            if(Qube::IS_DEV())
            {
                static $fff=0;
                $fff++;
#                file_put_contents('email-' . $fff . '.html', $N->getContent());
            }
        }
        
        return $N;        
    }
}


class ActiveResponder extends Responder
{
    function getAliasedSelectClause($alias) {      
        
        $where = new DBWhereExpression(parent::getAliasedSelectClause($alias));
        
        $where->Where('`' . $alias . '`.active = 1');
        
        return $where;
    }
}
