<?php

/**
 * ResellerNetwork Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ResellerNetworkModel extends DBObject {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    // the admin_user = the ID of the user that owns this records
    public $id, $type, $httphostkey, $domain, $company, $admin_user;
    
    protected $_PR_KEY  =   'id';
/**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c   =   array('id', 'admin_user', 'httphostkey', 'company', 'domain', 'type');
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
    
    /*
     * assuming the object contains joined data from the reseller table (reseller_main_site, reseller_main_site_id)
     * @return HubModel
     */
    function getResellerHub()
    {
        return Qube::Start()->queryObjects('HubModel')->Where('id = %d', $this->reseller_main_site_id)->Exec()->fetch();
    }
}

