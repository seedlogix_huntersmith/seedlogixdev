<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Nav Model
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class PathIndexModel extends HybridModel {
    
    /**
     * 
     * Table Fields
     */
    public $hub_ID, $page_ID, $path, $is_current, $ts;
    protected $_PR_KEY = NULL;
    protected $_defaults    =   array('ts' =>  'NOW()');
    
    function __getColumns() {
        static $cols    =   array('hub_ID', 'page_ID', 'path', 'ts', 'is_current');
        return $cols;
    }
    
    /**
     * 
     * @return WebsiteModel
     */
    function getSite(){
        return $this->queryObjects('WebsiteModel')->Exec()->fetch();
    }
    
    function getBindings() {
        $bindings   =   array(
            'WebsiteModel'  =>  'id = ' . $this->hub_ID,
            'PageModel'   =>  "hub_ID = {$this->hub_ID} AND ID = {$this->page_ID}"
        );
        
        return $bindings;
    }
    
    static function CreateIndex($hub_ID, $path, $page_ID, Qube $qube){
        $index  =   new self;
        
        $PDO    =   $qube->GetPDO();
        $PDO->beginTransaction();
        
        $qube->query('UPDATE %s SET is_current = 0 WHERE hub_ID = %d AND page_ID = %d', $index, $hub_ID, $page_ID);
        $res    =   $qube->query('UPDATE %s SET is_current = 1, ts = NOW() WHERE hub_ID = %d AND page_ID = %d AND path = "%s"', $index, $hub_ID, $page_ID, $path);
        
        if($res->rowCount() === 0){
            $index->loadArray(array('hub_ID' => $hub_ID,
                    'path' => $path,
                'page_ID' => $page_ID, 'is_current' => 1));

            $index->Save();
        }
        $PDO->commit();
        
    }
}
