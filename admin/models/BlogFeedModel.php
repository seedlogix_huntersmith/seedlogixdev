<?php

#class ContactDataExists extends Exception{}

/**
 * BlogFeedModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * 
 */
class BlogFeedModel extends HybridModel {    
	
    protected $_PR_KEY  =   'ID';
    
		protected $ID	=	0, $name = '', $created = '', $ts = '', $trashed = '', $user_ID = 0;
		protected $_defaults	=	array('created' => 'NOW()');
		
		function __getColumns() {
        static $cols    =   array('ID', 'name', 'created', 'ts', 'trashed', 'user_ID');        
        return $cols;
    }		
		
		function _getColumns($alas = NULL, $purpose = self::FOR_INSERT, &$columns = NULL) {
			$cols	=	parent::_getColumns($alas, $purpose, $columns);
			if($purpose	==	self::FOR_UPDATE)
				unset($cols['created']);
			
			return $cols;
		}
}

