<?php

/**
 * HubModel Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class PhoneRecordsModel extends HybridModel {
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    
    public $id, $cid, $user_id, $user_parent_id, $tracking_id, $direction, $call_uuid, $caller_id, $destination, $length, $recording, $date;
    
    protected $_PR_KEY  =   'id';


        /**
     *
     * @return type 
     * 
     */
    function __getColumns() {
        static $c       =   array('id', 'cid',
						'user_id', 'user_parent_id', 'tracking_id', 'direction', 'call_uuid', 
            'caller_id', 'destination', 'length', 'recording', 'date'
                    );
        return $c;
    }
    
    function _getDeleteClause() {
        ;
    }
	
	function ConvertSectoAmount($n) { 
		//$n = 10000;
		$hour = $n / 3600; 

		$n %= 3600; 
		$minutes = $n / 60 ; 

		$n %= 60; 
		$seconds = $n; 

		//echo ("$day days $hour hours $minutes minutes $seconds seconds"); 
		return array(
			'hours'=> $hour, # Submit the result of the record to this URL
			'minutes' => $minutes, # HTTP method to submit the action URL
			'seconds'=> $seconds
		);

	} 
	
	function getFormattedDate(){
		$FormattedDate = new DateTime($this->date);
		$FormattedDate = $FormattedDate->format('Y-m-d');
		return $FormattedDate;
	}
	
	function getTimeLinePhone($delete_link = '', $contact_id){
		$contact = ContactModel::Fetch('id = %d', $contact_id);
		$trans = TranscriptionsModel::Fetch('call_uuid = %d', $this->call_uuid);
		
		$ctime = $this->ConvertSectoAmount($this->length);
		if($ctime['hours']>=1) $calltime .= round($ctime['hours']).' hrs ';
		if($ctime['minutes']>=1) $calltime .= round($ctime['minutes']).' mins ';
		if($ctime['seconds']) $calltime .= round($ctime['seconds']).' secs ';
		
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $this->date);
		$callstamp = $date->format('g:i A');
		
		if($this->direction == 'inbound'){
			if($trans->call_uuid == $this->call_uuid){
				$from = 'Voicemail <span style="color: #748a96;">from '.$contact->first_name.' '.$contact->last_name.' - '.$calltime.' - '.$callstamp.'</span>';
				$message = '<p>'.$trans->transcription.'</p>';
			}
			else $from = 'Phone Call <span style="color: #748a96;">from '.$contact->first_name.' '.$contact->last_name.' - '.$calltime.' - '.$callstamp.'</span>';
		}
		else $from = 'Phone Call <span style="color: #748a96;">to '.$contact->first_name.' '.$contact->last_name.' - '.$calltime.' - '.$callstamp.'</span>';
		
		if($this->recording) $download = '<a title="Download Recording" class="bDefault f_rt phoneDownload" href="'.$this->recording.'"><span class="export"></span></a>';
		
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "{$download} <span class=\"phonetimeline-title\"><i class=\"icon-phone\"></i> {$from}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "{$message}"
		);
	}
	function getTimeLinePhoneLead($delete_link = '', $lead_id){
		$Prospect = ProspectModel::Fetch('id = %d', $lead_id);
		$trans = TranscriptionsModel::Fetch('call_uuid = %d', $this->call_uuid);
		
		$ctime = $this->ConvertSectoAmount($this->length);
		if($ctime['hours']>=1) $calltime .= round($ctime['hours']).' hrs ';
		if($ctime['minutes']>=1) $calltime .= round($ctime['minutes']).' mins ';
		if($ctime['seconds']) $calltime .= round($ctime['seconds']).' secs ';
		
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $this->date);
		$callstamp = $date->format('g:i A');
		
		if($this->direction == 'inbound'){
			if($trans->call_uuid == $this->call_uuid){
				$from = 'Voicemail <span style="color: #748a96;">from '.$Prospect->name.' - '.$calltime.' - '.$callstamp.'</span>';
				$message = '<p>'.$trans->transcription.'</p>';
			}
			else $from = 'Phone Call <span style="color: #748a96;">from '.$Prospect->name.' - '.$calltime.' - '.$callstamp.'</span>';
		}
		else $from = 'Phone Call <span style="color: #748a96;">to '.$Prospect->name.' - '.$calltime.' - '.$callstamp.'</span>';
		
		if($this->recording) $download = '<a title="Download Recording" class="bDefault f_rt phoneDownload" href="'.$this->recording.'"><span class="export"></span></a>';
		
		return (object)array(
			'type' => 'blog_post',
			'date' => $this->getFormattedDate(),
			'dateFormat' => 'DD MMM YYYY',
			'title' => "{$download} <span class=\"phonetimeline-title\"><i class=\"icon-phone\"></i> {$from}</span>",
			'width' => '92%',
			//'image' => ['https://blog.seedlogix.com/users/u59/59772/blog_post/marketing_fluff.jpeg'],
			//'content' => "<p>{$this->note}</p><a href=\"$delete_link\" class=\"f_rt jajax-action\">Delete the Note</a>"
			'content' => "{$message}"
		);
	}
	
	function phone_display($num){
		$num = preg_replace('/[^0-9]/', '', $num);

		$len = strlen($num);
		if ($len==11 && substr($num,0,1)=='1'){
			return substr($num,1,10);
		}
		return $num;
	}
	
}

