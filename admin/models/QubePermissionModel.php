<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Row from the 6q_roles table
 *
 * @author amado
 */
class QubePermissionModel extends HybridModel {
    /**
     *
     * @var int
     */
    public $ID;
    /**
     *
     * @var string
     */
    public $flag;  
    
    function __getColumns() {
        return array('ID', 'flag');
    }
    
    function Validate(array &$row, ValidationStack $stack){
        return true;
    }
}

