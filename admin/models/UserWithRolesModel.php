<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
#namespace Qube\User;

use \Qube\Hybrid\Authorizer;

/**
 * User Model
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class UserWithRolesModel extends UserModel {

    /**
     *
     * @var PermissionsSet
     */
    protected $permissions = NULL;

    /**
     *
     * @var \AccountLimitsVerify
     */
    protected $_AccountLimits = NULL;

    function setPermissionsSet(PermissionsSet $P) {
        $this->permissions = $P;
        return $this;
    }

    function verifyLimit($permission, array $options = array()) {
        if ($this->isSystem())
            return true;
        if (!$this->_AccountLimits) {
            $uda = new UserDataAccess();
            $ada = new \Qube\Hybrid\DataAccess\AccountDataAccess();
            $ada->setActionUser($this);

            // initialize accounnt limits from the reseller subscription.
            $limits = $uda->getAccountLimits($this->getParentID());

            // return the class responsible for comparing the number of existing objects vs limits
            $alv = new \AccountLimitsVerify($limits, $ada);

            $this->_AccountLimits = $alv;
        }

        return $this->_AccountLimits->setOptions($options)->can($permission, $this);
    }

    function hasPermissions() {
        return $this->permissions->hasPermissions();
    }

    function getPermissions() {
//        return array(' GETPERMISSION');
        return $this->permissions;
    }

    function can($permission, $id = 0, \Zend\EventManager\EventManagerAwareInterface $SED = NULL) {

        return $this->permissions->can($permission, $id, $SED ? $SED : new \Qube\Hybrid\Events\SystemEventDispatch());
    }

    function setPermission($p, $id = 0) {
        $this->permissions->setPermission($p, $id);
    }

    function renderPermissions() {

        // add default permissions for resellers..
        if ($this->can('is_system'))
            $this->permissions->addAuthorizer(new Authorizer\SystemLevelAuthorizer($this->permissions, $this));
        else
        if ($this->can('is_reseller'))
            $this->permissions->addAuthorizer(new Authorizer\AdminLevelAuthorizer($this->permissions, $this));
        else
            $this->permissions->addAuthorizer(new Authorizer\UserLevelAuthorizer($this->permissions, $this));
//        else            $this->can = 
    }

    function getBindings() {
        return parent::getBindings() + array('NetworkModel' => 'admin_user = %d');
    }

    function isSystem() {
        return $this->can('is_system');
    }

    function hasModuleAccess($module) {
        switch ($module) {
            case 'Campaigns':
                return $this->can('create_campaigns') || $this->can('view_campaigns');
                break;
        }
    }

    function hasRole() {
        $DA = new UserDataAccess();
        $roles = $DA->getRole($this->getID());
        if ($roles) {
            return true;
        } else {
            return true; //@todo stuff
        }
    }

}
