<?php

require_once 'ResellerModel.php';

/**
 * Reseller Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class BrandingModel extends ResellerModel {
    
    protected $cdnhost          = null;
    const FALLBACK_SITE         = 'login.seedlogix.com';
    const _6QUBE_LOGO_ALPHA     = 'themes/default/images/seed-logix_alpha.png';
    const _6QUBE_LOGO_WHITE     = 'themes/default/images/seed-logix_white.png';
    
    /**
     * id,admin_user,parent_id,reseller_type,main_site,main_site_id,company,bg-img,bg-img-rpt,bg-clr,logo,favicon,css_link,main_link_color,main_link_hover,left_nav_active,left_nav_slide,nav-bg,nav-bg2,nav-links,nav-links2,nav-links3,nav-links4,nav2-bg,nav2-bg2,nav2-links,nav2-links2,nav3-bg,nav3-bg2,nav3-links,nav3-links2,adminbar-clr,userbar-clr,userbar-text,userbar-links,welcome-msg,dflt-link-clr,buttons-clr,titlebar-clr,newitem-clr,person-clr,undobar-clr,dflt-no-img,form-input-clr,form-input-brdr-clr,form-lbl-clr,form-img-brdr-clr,app-right-link,app-right-link2,list-bg-clr,list-link-clr,wait-anim-clr,wait-box-clr,wait-brdr-clr,camp-pnl,camp-pnl-title,camp-pnl-html,camp-pnl-brdr-clr,camp-blog-disp,camp-blog-id,support-page,support_email,support_phone,custom-ui,custom-backoffice,custom-backoffice-dflt,login-bg,login-bg-clr,login-logo,login-brdr-clr,login-txt-clr,login-signup-lnk,login-lnk-clr,login-suggest-ff,activation_email,activation_email_img,payments,payments_acct,payments_acct2,no_monthly,receipt_email,privat
     */
    function getCDNUrl(){
        if(!$this->cdnhost){
            $mainsite   =   explode('.', $this->main_site);
            while(count($mainsite) > 2)
                array_shift ($mainsite);
            $this->cdnhost  =   'cdn.' . implode('.', $mainsite);
        }
        
        return "http://$this->cdnhost/" . join('/', func_get_args());
    }
    
    function getID(){
        return $this->admin_user;
    }
    
    function getThemeDir( $f = '' )
    {
        return $this->getAdmin(new UserDataAccess())->getStorageUrl('brand', $f);
//        return './branding/' . $this->admin_user . '/' . $f;
    }
    
    /**
     * sets logoURL to user's image, 
     * or fallbacks to app logo path defined in BrandingModel
     * 
     * checks to see if logo will load and then resets to app logo as fallback or network error
     */    
    function getLogoUrl($not_login = true)
    {
        $slogix     = $not_login ? self::_6QUBE_LOGO_ALPHA : self::_6QUBE_LOGO_WHITE;
        $logo       = $this->logo ? SystemDataAccess::getUserUrlPath($this->logo) : $slogix;
        $logofname  = file_exists(Qube::get_settings('USERS_STORAGE').$this->logo) ? $logo : $slogix;

        return $logofname;
    }

    /**
     * produce a url based on reseller main_site and other variables
     * accepts an arbitrary # of arguments
     * 
     * first path must not start with a slash
     */
    function getResellerUrl($path1)
    {
        $domain    =   empty($this->main_site) ? self::FALLBACK_SITE : $this->main_site;
        
        $args   = func_get_args();
        
        return "http://$domain/" . implode('/', $args);
    }
}

