<?
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/hub.class.php');
	
	//create new instance of class
	$hub = new Hub();
	
	//content
	echo '<h1>Website Analytics</h1>';
	echo $hub->userHubsDropDown($_SESSION['user_id'], 'hub_analytics_view', $_SESSION['campaign_id'], 1);
	
	$result = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], NULL, 4);
	if($result && !$_SESSION['reports_user']){
	echo '<br /><br /><h1>MU Website Analytics</h1>';
	echo $hub->userHubsDropDown($_SESSION['user_id'], 'hub_analytics_view', $_SESSION['campaign_id'], 4);
	}
	
?>
<script type="text/javascript">
$(function(){
	$(".uniformselect").uniform();
});
</script>
<div id="ajax-select">
	<br /><br />
	<p>Use the drop-down above to select which website's analytics to view.</p>
</div>
