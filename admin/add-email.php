<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/domains.class.php');
	$d = new Domains();
	
	$newAcct = $d->validateText($_POST['account'], 'Account Name');
	$domain = $d->validateText($_POST['domain'], 'Domain');
	
	if($d->foundErrors()){ //check if there were validation errors
		//$error = $press->listErrors('<br />'); //list the errors
		$response['error']['id'] = 1;
		$response['error']['message'] = 'Error validating account name:\n';
		$response['error']['message'] .= $d->listErrors('\n');
	}
	else { //if no errors
		if(substr($domain, 0, 1)=='@') $domain = substr($domain, 1);
		//first validate it's the user's domain
		if($d->validateDomainOwnership($_SESSION['user_id'], $domain)){
			if($_POST['lincoln']){
				$server = 'Lincoln';
				//set it up over on the lincoln server
				$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
				$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
				$secHash = md5($domain.$salt);
				$secHash = md5($secHash.'addEmail'.$pepper);
				//makes the hash only good if it was submitted today:
				$secHash = md5($secHash.date('d'));
				$data = file_get_contents('http://50.97.78.210/secure/setup_email.php?domain='.$domain.'&mode=addEmail&acct='.$newAcct.'&hash='.$secHash);
				$add = json_decode($data, true);
				$server = 'Lincoln';
			} else {
				$server = 'Kennedy';
				$add = $d->addNewEmailAccount($newAcct, $domain);
			}
			
			if($add['success']){
				$response['success']['id'] = '1';
				$response['success']['action'] = 'load';
				$response['success']['page'] = 'setup_email.php?domain='.$domain.'&newAddr='.$newAcct.'@'.$domain.'&t='.$add['message'].'&lincoln='.$_POST['lincoln'];
			}
			else {
				$response['error']['id'] = 1;
				$response['error']['message'] = $add['message'];
			}
		}
		else {
			$response['error']['id'] = 1;
			$response['error']['message'] = 'Error validating domain ownership.';
		}
	}
	$response['server'] = $server;

	$string = json_encode($response);
	$string = str_replace('\\\\n', '\n', $string);
	echo $string;
?>