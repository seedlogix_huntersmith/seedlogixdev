<?php
	session_start();
	
	define('USE_GLOBAL_VHOSTS', TRUE);
//	echo dirname(__FILE__);
#        require_once( dirname(__FILE__) . '/6qube/core.php');
 	require_once( dirname(__FILE__) . '/../hybrid/bootstrap.php');         
#         require_once QUBEROOT . 'DomainConfig.php';
         
	require_once('inc/domains.class.php');
	$domains = new Domains();
	$continue = 1;
	
	//Prepare domain
	$domain = trim($_POST['domain'], '/');
	if(strpos($domain, "http://")===false) $continue = 0;
	if($continue==1){
		$domain = substr($domain, 7); //remove http://
		$domain_array = explode(".", $domain); //array of domain components
		$domain2 = $domain_array[count($domain_array)-2].".".$domain_array[count($domain_array)-1]; //example.com
		$domain2 = $domains->validateDomain($domain2);
		if($domain_array[0]=="www"||count($domain_array)==2) $subdomain = "www"; //subdomain
		else $subdomain = $domain_array[0]; //subdomain
		$dname = explode(".", $domain2);
		$domainName = $dname[0]; //just the domain without tld or sub
		$type = $_POST['type']; //hub or blog
		if(substr($type, 0, 12) == "network_site"){
			$a = explode('_', $type);
			$type = "network_site";
			$type2 = $a[2];
		}
		$uid = $_POST['uid']; //user id
		$sid = $_POST['sid']; //section id
		$lincolnDomain = 0;
                $parentIsSetup=0;   // if the parent domain has been already created (addon)
		if($type!='network_site' && $type!='reseller'){
                        // if not in db
			if(!$domains->checkIfDomainInUse($domain2)){
				$lincolnDomain = 1;
				$parentIsSetup = 0;
#echo 'domaininuse=true';
			}
			else {
                            // checkk db to get server name
				$isLincoln = $domains->isLincolnDomain($domain2);
				if($isLincoln) $lincolnDomain = 1;
				#if($subdomain!='www') 
                                    $parentIsSetup = 1;
			}
		}
		if($_SESSION['reseller']) $rid = $_SESSION['login_uid'];
		
		if($_SESSION['reseller'] || $_SESSION['reseller_client']) $ns_domain = "THEPARTNERPORTAL.COM";
		else $ns_domain = "6QUBE.COM";
		$dispNS1 = $lincolnDomain ? 'NS3' : 'NS1';
		$dispNS2 = $lincolnDomain ? 'NS4' : 'NS2';
		
		if(count($domain_array)<4){ //if there aren't more than 4 parts to the domain (sub1.sub2.site.com)
			if($domain2 && !is_array($domain2)){ //validateDomain() function returns an array on error
				//make sure no one else is using the domain:
#echo 'f';
				if($domains->checkOtherUsers($domain2, $uid, $rid, $subdomain, $_SESSION['reseller_client'])){ 
					//Get WHOIS info
					$result = $domains->getWhois($domain2);
					
					if($result){
						//If domain is unavailable
						if($result['status']=="unavailable"){
                                                    
            $results    =   array('error' => true,
                    "message"   =>  "The site was successfully created!",
#                                                                                            "message2"  =>  "http:\/\/www.simplyinboundmarketing.com",
                    "type"  =>  "hub",
                    "failure"=> true,
                    "server"    =>  "Lincoln"
                );
							//check WHOIS info to see if nameservers are pointed to 6qube's
							$whois = urldecode($result['whois']);
                                                        
                                                        
							if($whois){
                                                                    
                        $DC =   new DomainConfig($domain2);
                        $nameservers =   $DC->getNameServers();

                                                                $results['ns']  =   $nameservers;

	if(true){
            $DC     =   new DomainConfig($domain2);
//            $subdomains =   $DC->getSubdomains($server_api);

            $results['error']   =   0;
            $results['failure'] =   false;
            
						if(USE_GLOBAL_VHOSTS)
						{
							$DS	=	new DomainSetup();
							$serverID	=	$DS->getServer($DC);
							$VS	=	new ValidationStack();
							$validateparams	=	array();
//							if($DC->Validate($validateparams, $VS))	
							if($DC->ValidateDomain($VS))
							{
								$results['server']	=	$serverID;
							}else{
                    $results['error']   =   11;
                    $results['failure']   =   true;
                    list($results['message']) =   $VS->getErrors('hostname');
							}
						}else
            if(!$subdomains->exists("*.$domain2"))
            {
                try{
                    $DC->setupWildcardHOST($server_api);
                }catch(HybridWebsiteSetupFailureException $e)
                {
                    $results['error']   =   10;
                    $results['failure']   =   true;
                    $results['message'] =   $e->getMessage();
                }
            }
        }//end if($ns1 == "NS1.6QUBE.NET")
							}//end if($whois)
							else {
								$results["error"] = 1;
								$results["message"] = "Could not find registration information for this domain.  Please contact us at 877-570-5005 to have your website set up manually. Error 6";
							}
						}//end if($result['status']=="unavailable")
						else {
							$results["error"] = 0;
							$results["message"] = "Domain is available!  Click the link above or contact us at 877-570-5005 to purchase it and complete setup.";
							if($uid==496) $results["message"] .= ' ... '.print_r($result, true);
						}
					}
				}//end if($domains->checkOtherUsers($domain2, $type, $uid, $sid))
				else {
					$results["error"] = 1;
					$results["message"] = "Another user is already using this domain.  If you feel this is in error please contact us at 877-570-5005.";	
				}
					
			}//end if($domain2 && !is_array($domain2))
			
			else if(is_array($domain2)){
				$results["error"] = 1;
				$results["message"] = "Error validating domain:\n";
				$results["message"] .= $domain2['message'];	
			}
			
			else {
				$results["error"] = 1;
				$results["message"] = "No domain specified.";
			}
		}//end if(count($domain_array)>3)
		else {
			$results["error"] = 1;
			$results["message"] = "Too many subdomains.  Only one is allowed.";	
		}
	}//end if($continue==1)
	else {
		$results["error"] = 1;
		$results["message"] = "Domain must include http://";	
	}
	
	if($results['notify_admin']){
		$subject = 'Domain Setup Error Notification: '.$type.' #'.$sid.' - UID #'.$uid;
		$message = 'There were one or more errors creating the files and folders for this domain that should be looked into.<br />This is in reference to '.$type.' #'.$sid.' from user #'.$uid.'<br><br><br><b>Details:</b><br>'.$results['error_details'];
		$domains->sendMail(array('admin@6qube.com', 'reeselester@gmail.com', 'error@sofus.mx'), $subject, $message, NULL, 'noreply@6qube.com', NULL, 1);
	}
	
//	$results['server'] = $server;
	
	echo json_encode($results);
?>
