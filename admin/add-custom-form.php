<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/hub.class.php');
	$hub = new Hub();
	
	$name = isset($_POST['form_name']) ? $_POST['form_name'] : '';
	$multiUser = $_POST['multi_user'] ? 1 : 0;
	
	if($name){
		$formID = $hub->addNewCustomForm($name, $_SESSION['user_id'], $_SESSION['parent_id'], $_SESSION['campaign_id'], $multiUser);
		
		if($formID){
			$html = 
			'<div class="item2">
				<div class="icons">
					<a href="hub-forms.php?mode=edit&id='.$formID.'" class="edit"><span>Edit</span></a>
					<a href="#" class="delete rmParent" rel="table=lead_forms&id='.$formID.'&undoLink=hub-forms"><span>Delete</span></a>
				</div>';
				
				if($_SESSION['theme'])
					$html .= '<img src="img/hub-form/'.$_SESSION['thm_buttons-clr'].'.png" class="blogPost" />';
				else
					$html .= '<img src="img/hub-form2.png" class="icon" />';
				
				$html .= '<a href="hub-forms.php?mode=edit&id='.$formID.'">'.$name.'</a>
			</div>';
			
			$response['success']['id'] = '1';
			$response['success']['container'] = '#list_hubforms .container';
			$response['success']['message'] = $html;
		}
	}
	echo json_encode($response);
?>