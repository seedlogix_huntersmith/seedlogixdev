<?
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/articles.class.php');
	
	//create new instance of class
	$connector = new Articles();
	
	//content
	echo '<h1>Email Settings</h1>';
	
	echo $connector->userArticlesDropDown($_SESSION['user_id'], 'article_email_form'); //(user_id, selected, select name)
	
?>
<script type="text/javascript">
$(function(){
	$("select, input[type=file]").uniform();
	
	$(".helpLinks").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
	});
	
	$(".trainingvideo").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
	
});
</script>
<br style="clear:both;" />
<div id="email-settings">
<div id="ajax-select">
	<p>Use the drop down box above to select which Article's email settings you would like to configure.</p><br />
    <p>With this optional setting you can create an auto-response email to be sent to prospects when they fill out your Article's contact form.</p>
</div>
</div>
<!--Start APP Right Panel-->
							<div id="app_right_panel">
                                
                              <h2>Email Settings Videos</h2>
                                
								<div class="app_right_links">
								<ul>
								  <li><a href="#emailsetVideo" class="trainingvideo" >Email Settings Video</a></li>
              <div style="display: none;">
		<div id="emailsetVideo" style="width:640px;height:480px;overflow:auto;">
			<!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
		<a  
			 href="http://6qube.com/videos/training/email-setting-overview.flv"  
			 style="display:block;width:640px;height:480px"  
			 id="emailsetplayer"> 
		</a> 
	
		<!-- this will install flowplayer inside previous A- tag. -->
		<script>
			flowplayer("emailsetplayer", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");
		</script>
		</div>
	</div>
								</ul>
								</div> <!--End APP Right Links-->
	                            		<?	
		// ** Global Admin Right Bar ** //
		require_once(QUBEROOT . 'includes/global-admin-rightbar.inc.php');
?>							
</div>
<!--End APP Right Panel-->
<br style="clear:both;" /><br/>