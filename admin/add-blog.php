<?
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/blogs.class.php');
	$blog = new Blog();
	
	$newBlog = $blog->validateText($_POST['name'], 'Blog Name'); //validate Blog name
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	if($blog->foundErrors()){ //check if there were validation errors
		//$error = $blog->listErrors('<br />'); //list the errors
	}
	else{ //if no errors
		$message = $blog->addNewBlog($_SESSION['user_id'], $newBlog, $_SESSION['campaign_id'], 
								$_SESSION['user_limits']['blog_limit'], NULL, $parent_id);
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_blog .container';
		$response['success']['message'] = $message;
	}

	echo json_encode($response);
	
?>