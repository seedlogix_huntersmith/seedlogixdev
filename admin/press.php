<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/press.class.php');
	
	//create new instance of class
	$press = new Press();
	
	if($_SESSION['theme']) $previewUrl = 'http://press.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://press.6qube.com/';
	
	$press_count = 0;
	//$press_limit = $press->getLimits($_SESSION['user_id'], "press");
	$press_limit = $_SESSION['user_limits']['press_limit'];
	if($press_limit==-1 || $press_limit==NULL) $remaining = "Unlimited";
	
	//content
	echo '<h1>Press Releases</h1>';
	/***************************************************
	IF FORM INFO IS SUBMITTED
	***************************************************/
	if(isset($_POST['headline'])){
		$update = $press->updatePress($_POST);	
		if($update){
			echo $update;
		}
		else{
			include('./inc/forms/add-press-release.php');
		}
	}
	/***************************************************
	IF ID OR FORM in URL
	***************************************************/
	else if($_GET){
		if($_GET['form']) $form = $_GET['form'];
		else $form = 'settings';
		$_SESSION['current_id'] = $_GET['id'];
		include('./inc/forms/add-press-release.php');
	}
	// WHEN THEY FIRST COME TO THE PAGE
	else{
		$result = $press->getPresss($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']); //primes Press results, and is needed for getPressRows()
		$press_count = $press->getPressRows();
		echo '<div id="createDir">';
		if(!$_SESSION['theme']){ 
			echo '<div class="watch-video"><ul><li><a href="#campVideo" class="watch_video trainingvideo" >Watch Video</a></li></ul></div>';
		}
		echo '<form method="post" action="add-press.php" class="ajax"><input type="text" name="name" ';
		if(!$remaining && ($press_limit==0 || (($press_limit-$press_count)<=0))) echo 'readonly';
		echo '/><input type="submit" value="Create New Press" name="submit" /></form></div>';
		if(!$_SESSION['theme']){
			echo '<div style="display:none;"><div id="campVideo" style="width:640px;height:480px;overflow:auto;"><a href="http://6qube.com/videos/training/press-overview.flv" style="display:block;width:640px;height:480px"id="player"> </a><script>flowplayer("player", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");</script></div></div>';
		}
		echo '<div id="message_cnt"></div>';
		
		if($_POST){
			$newPress = $press->validateText($_POST['name'], 'press_release', 'Press Release'); //validate press name
			if($press->foundErrors()){ //check if there were validation errors
				echo '<p class="error"><strong>Errors were found</strong><br />'. $press->listErrors('<br />') .'</p>'; //list the errors
			}
			else{ //if no errors
				$press->addNewPress($_SESSION['user_id'], $newPress, $_SESSION['campaign_id'], $press_limit); //add new hub
			}
		}
		if($press_count) { //checks to see if the user has any press releases
			if(!$remaining) $remaining = $press_limit - $press_count;
			$remaining = $remaining<0 ? 0 : $remaining;
			echo '<div id="dash"><div id="fullpage_press" class="dash-container"><div class="container">';
			while($row = $press->fetchArray($result)){ //if so loop through and display links to each Press
				$tracking = $row['tracking_id'];
				$cid = $row['cid'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'press.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="press.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=press_release&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$press->ANAL_getGraphImg('press', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="press.php?form=settings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Press Releases remaining.';
			if(!$_SESSION['theme']){ 
				if(($press_limit-$press_count)<=0 && $press_limit!=-1 && $press_limit!="Unlimited") echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
			if($_SESSION['reseller']){
				echo '<br /><br />
					<a href="reseller-transfer-items.php?mode=section&mode2=press_release&cid='.$cid.'" class="dflt-link">Transfer one or more press releases to another user</a>';
			}
		}
		else{ //if no press releases
			if($press_limit==-1 || $press_limit==NULL) $press_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_press" class="dash-container"><div class="container">';
			if($press_limit==0 && $press_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Press Releases remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Press Release.  You have <strong>'.$press_limit.'</strong> remaining.';
			echo '</div></div></div>';
		}
	}
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});	
});
</script>
<? } ?>