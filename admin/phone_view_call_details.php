<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/phone.class.php');
	$phone = new Phone();
	$mid = $_GET['mid'];
	$rqtype = $_GET['rqtype'];
	if($_GET['Page']) $currentpage = $_GET['Page'];
	else $currentpage = 1;;
	$prevpage = $currentpage-1;
	$nextpage = $currentpage+1;
	$lbase = 'phone_view_call_details.php?mid='.$mid.'&rqtype=';
	$pagibase = 'phone_view_call_details.php?mid='.$mid.'&rqtype='.$rqtype;
	$limit = 10;
	
	//get call reports
	$callReport = $phone->getCallDetails($_SESSION['user_id'], $mid, $rqtype, $currentpage, $limit);

	$callReports = $callReport['Data'];
	$totalResults = $callReport['ResultSet']['TotalResults'];

?>
<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="phone_temp.php">Call Services</a>
		</li>
        <li>
			<a class="current" href="phone_view_call_details.php?mid=<?=$mid?>&rqtype=MonthToDate">Call Details</a>
		</li>
        <li>
			<a href="phone_view_call_report.php?mid=<?=$mid?>&Page=1">Call Recordings</a>
		</li>
		
	</ul>
</div>
<br style="clear:left;" />
<br />
<h1>Call Reports</h1><br />
<p style="font-size:12px;">View period:&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>Today" class="dflt-link" <? if($rqtype=='Today') echo $bold; ?>>Today</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>PreviousDay" class="dflt-link" <? if($rqtype=='PreviousDay') echo $bold; ?>>Previous Day</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>PreviousWeek" class="dflt-link" <? if($rqtype=='PreviousWeek') echo $bold; ?>>Previous Week</a>
				&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a href="<?=$lbase?>Previous7Days" class="dflt-link" <? if($rqtype=='Previous7Days') echo $bold; ?>>Previous 7 Days</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>PreviousMonth" class="dflt-link" <? if($rqtype=='PreviousMonth') echo $bold; ?>>Previous Month</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>PreviousQuarter" class="dflt-link" <? if($rqtype=='PreviousQuarter') echo $bold; ?>>Previous Quarter</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>MonthToDate" class="dflt-link" <? if($rqtype=='MonthToDate') echo $bold; ?>>Month to Date</a>
                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="<?=$lbase?>YearToDate" class="dflt-link" <? if($rqtype=='YearToDate') echo $bold; ?>>Year to Date</a>
                </p>

<br style="clear:both; margin-bottom:20px;" />
<div id="message_cnt" style="display:none;"></div>
<h3><?=$totalResults?> Calls</h3><br />
<p style="font-weight:bold;">
<? if($totalResults!=$limit || $currentpage > 1){?>
	<? if($currentpage > 2){?><a title="Start of Calls" href="<?=$pagibase?>&Page=1"><img src="img/v3/nav-start-icon.jpg" border="0" /></a>&nbsp;<? } ?>
    <? if($currentpage > 1){?><a title="Previous Set of Calls" href="<?=$pagibase?>&Page=<?=$prevpage?>"><img src="img/v3/nav-previous-icon.jpg" border="0" /></a><? } ?>
	<? if($totalResults!=$limit && $totalResults>=$limit){?><a title="Next Set of Calls" href="<?=$pagibase?>&Page=<?=$nextpage?>"><img src="img/v3/nav-next-icon.jpg" border="0" /></a><? } ?>
	</p><br />
<? } ?>
							               
<? if($callReports && is_array($callReports)){
	echo '<div class="mailboxContain">';
	echo '<ul class="prospects">
	<li style="width:10%;"><strong>Date</strong></li>
	<li style="width:15%;"><strong>Caller ID</strong></li>
	<li style="width:15%;"><strong>City</strong></li>	
	<li style="width:5%;text-align:center;"><strong>State</strong></li>	
	<li style="width:15%;text-align:center;"><strong>Destination</strong></li>
	<li style="width:5%;text-align:center;"><strong>Length</strong></li>	
</ul>';
	foreach($callReports as $json=>$reports){	
		$date = date('m/j/Y', preg_replace('/[^\d]/','', $reports['DateTime'])/1000);   
		echo '
		<ul class="prospects">
	<li style="width:10%;" >'.$date.'</li>
	<li style="width:15%;">'.$reports['CallerId'].'</li>
	<li style="width:15%;">'.$reports['City'].'</li>	
	<li style="width:5%;text-align:center;">'.$reports['State'].'</li>	
	<li style="width:15%;text-align:center;">'.$reports['PhoneNumber'].'</li>
	<li style="width:5%;text-align:center;">'.$reports['CallDuration'].'</li>
		</ul>
		';
	}
	echo '</div><br />
	
	
	
	';

	
} else { ?>

<? if($totalResults=='0') { ?>
<p>No Results</p>
<? } else { ?>
<p>Service line connection lost.  Please try again in 10 seconds. <a href="phone_view_call_details.php?mid=<?=$mid?>&rqtype=<?=$rqtype?>">Click here to refresh</a>.</p>
<? } ?>

<? } ?>

<br style="clear:both;" />