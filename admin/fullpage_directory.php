<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//iniate the page class
	require_once('inc/directory.class.php');
	$directory = new Dir();
	
	if($_SESSION['theme']) $previewUrl = 'http://local.'.$_SESSION['main_site'].'/';
	else $previewUrl = 'http://local.6qube.com/';
	
	$dir_count = 0;
	//$dir_limit = $directory->getLimits($_SESSION['user_id'], "directory");
	$dir_limit = $_SESSION['user_limits']['directory_limit'];
	
	//Directory
	//delete_item.php?table=directory&id='.$row['id'].'
		$result = $directory->getDirectories($_SESSION['user_id']); //primes directory results, and is needed for getDirectoryRows()
		if($dir_count = $directory->getDirectoryRows()){ //checks to see if the user has any directories
			if($dir_limit==-1) $remaining = "Unlimited";
			else $remaining = $dir_limit - $dir_count;
			echo '<div id="dash"><div id="fullpage_directory" class="dash-container"><div class="container">';
			while($row = $directory->fetchArray($result)){ //if so loop through and display links to each directory
				$tracking = $row['tracking_id'];
				if(!$tracking || $tracking==0) $tracking = 26;
				echo '<div class="item">
			<div class="icons">
				<a href="'.$previewUrl.'directory.php?id='.$row['id'].'" target="_new" class="view"><span>View</span></a>
				<a href="directory.php?form=listings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=directory&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$directory->ANAL_getGraphImg('directory', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="directory.php?form=listings&id='.$row['id'].'">'.$row['name'].'</a>
		</div>';
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Directory Listings remaining.';
			if(!$_SESSION['theme']){
				if(($dir_limit-$dir_count)<=0 && $dir_limit!=-1) echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
		}
		else{ //if no directories
			if($dir_limit==-1) $dir_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_directory" class="dash-container"><div class="container">';

			if($dir_limit==0 && $dir_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Directory Listings remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to Create a new Business Listing.  You have <strong>'.$dir_limit.'</strong> Remaining.';

			echo '</div></div></div>';
		}
?>
