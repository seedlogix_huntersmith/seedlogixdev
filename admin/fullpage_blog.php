<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//include classes
	require_once('./inc/blogs.class.php');
	
	//create new instance of class
	$blog = new Blog();
	
	$blog_count = 0;
	//$blog_limit = $blog->getLimits($_SESSION['user_id'], "blog");
	$blog_limit = $_SESSION['user_limits']['blog_limit'];
	
	$result = $blog->getBlogs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']); //primes Hub results, and is needed for getHubRows()
		if($blog_count = $blog->getBlogRows()) { //checks to see if the user has any directories
			if($blog_limit==-1) $remaining = "Unlimited";
			else $remaining = $blog_limit - $blog_count;
			echo '<div id="dash"><div id="fullpage_blog" class="dash-container"><div class="container">';
			while($row = $blog->fetchArray($result)){ //if so loop through and display links to each Hub
				$tracking = $row['tracking_id'];
				if(!$tracking || $tracking==0) $tracking = 26;
				if($_SESSION['reseller'] || $_SESSION['reseller_client']) $rid = "&rid=".$_SESSION['parent_id'];
				echo '<div class="item">
			<div class="icons">
				<a href="http://'.$_SESSION['main_site'].'/blog-preview.php?uid='.$_SESSION['user_id'].'&bid='.$row['id'].$rid.'" target="_new" class="view"><span>View</span></a>
				<a href="blog.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=blogs&id='.$row['id'].'"><span>Delete</span></a>
			</div>';
			echo '<img src="'.$blog->ANAL_getGraphImg('blog', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
			//echo '<img src="http://6qube.com/analytics/?module=VisitsSummary&action=getEvolutionGraph&idSite='.$tracking.'&period=day&date=last30&viewDataTable=sparkline&columns[]=nb_visits" class="graph trigger" />';
			echo '<a href="blog.php?form=settings&id='.$row['id'].'">'.$row['blog_title'].'</a>
		</div>';
				//get results for specified blog ID with a limit of 3
				$post_results = $blog->getBlogPost($row['id'],3);
				//if specified blog has any post associated with it
				if($blog->getPostRows()){
					//loop through and out the blogs post (limits 3)
					while($post_row = $blog->fetchArray($post_results)){
						//$tracking = $post_row['tracking_id'];
						//if(!$tracking || $tracking==0) $tracking = 1;
						echo '<div class="item">
								<div class="icons">
									<a href="http://'.$_SESSION['main_site'].'/blog-preview.php?uid='.$_SESSION['user_id'].'&bid='.$post_row['id'].$rid.'" target="_new" class="view"><span>View</span></a>
									<a href="blog.php?form=editPost&id='.$post_row['id'].'" class="edit"><span>Edit</span></a>
									<a href="#" class="delete" rel="table=blog_post&id='.$post_row['id'].'"><span>Delete</span></a>
								</div>
								
								<img src="http://'.$_SESSION['main_site'].'/admin/img/blog-post';
								if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
								echo '.png" class="blogPost" />
			
								<a href="blog.php?form=editPost&id='.$post_row['id'].'">'.$post_row['post_title'].'</a>
							</div>';
					
					}
					
				}
				
			}
			echo '<br />You have <strong>'.($remaining).'</strong> Blogs remaining.';
			if(!$_SESSION['theme']){
				if(($blog_limit-$blog_count)<=0 && $blog_limit!=-1) echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
		}
		else{ //if no blogs
			if($blog_limit==-1) $blog_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_blog" class="dash-container"><div class="container">';

			if($blog_limit==0 && $blog_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Blogs remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Blog.  You have <strong>'.$blog_limit.'</strong> remaining.';

			echo '</div></div></div>';
		}
?>
