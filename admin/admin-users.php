<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	
	//offset
	if($_GET['offset'] && is_numeric($_GET['offset'])) $offset = $_GET['offset'];
	else $offset = 0;
	//parent_id
	if($_GET['parent_id'] && is_numeric($_GET['parent_id'])) $parent_id = $_GET['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	//suspended
	if($_GET['suspended']==1) $suspended = 1;
	//canceled
	if($_GET['canceled']==1) $canceled = 1;
	
	if($parent_id){
		//get company name
		$query = "SELECT company FROM user_info WHERE user_id = ".$parent_id;
		$result = $local->queryFetch($query);
		$company = $result['company'];
	}
	
	//security measure to check user can access page or is an admin
	if($_SESSION['admin'] || ($_SESSION['reseller'] && ($parent_id==$_SESSION['login_uid']))){
		if(!$suspended && !$canceled){
?>
<script type="text/javascript">
$('#searchBox').focus();
$('#searchBox').die();
$('#searchBox').live('keyup', function(){
	var searchVal = $(this).attr('value');
	$.get('reseller-functions.php?action=searchUsers&limit=25&parent_id=<?=$parent_id?>&search='+searchVal, function(data){
		$('#resultsDiv').html(data);
	});
});
</script>
<?
		}
		//content		
		if(!$suspended && !$canceled){
			$activeOnly = 1;
			echo '<h1>';
			if($parent_id) echo $company.'\'s ';
			echo 'Users</h1>';
			if(!$_SESSION['support_user']){
				echo '<div id="createDir">
					<form method="post" class="ajax" action="inc/forms/add-user.php">
						<input type="text" name="name" value="User Name" />';
				if($parent_id) echo '<input type="hidden" name="parent_id" value="'.$parent_id.'" />';
				echo '<input type="submit" value="Create New User" name="submit" />
					</form>
				</div><br style="clear:both;" /><br />';
			}
			if(!$suspended && !$canceled){
				echo 'Start typing to search: <input type="text" id="searchBox" value="" /><br /><br />';
			}
		}
		else if($suspended){
			echo 
			"<h1>Suspended Accounts</h1>
			User accounts you have suspended access to or haven't been activated yet.
			<br /><br />";
		}
		else if($canceled){
			echo 
			"<h1>Canceled Accounts</h1>
			User accounts either you or the customer have canceled.
			<br /><br />";
		}
		
		echo
		'<div id="resultsDiv">';
		if($_SESSION['reseller']){
			//if it's a reseller viewing
			$local->displayUsers('admin-users.php', $parent_id, 25, $offset, $activeOnly, $suspended, $canceled);
		} else {
			$local->displayUsers('admin-users.php', $parent_id, 25, $offset);
		}
		echo '</div>';
	}
	else {
		echo "Not authorized.";
	}
		
	echo '<br /><p>&nbsp;</p>';
?>