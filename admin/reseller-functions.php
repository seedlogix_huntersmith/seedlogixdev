<?php
//start session
session_start();
#         require_once( '6qube/core.php');
	require_once '/home/sapphire/production-code/dev-trunk-svn/hybrid/bootstrap.php';	
//get action to perform from either POST or GET
if(!$action = $_POST['action']) $action = $_GET['action'];
if($action!='addNewBillingProfile' && $action!='updateBillingProfile'){
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	if($_SESSION['reseller'] || $_SESSION['admin']) $continue = true;
}
else {
	$salt = '4a0fBMMDDR(Cc,,c.bh07890';
	if($_POST['sec'] == md5($_POST['uid'].$salt)) $continue = true;
}
//initiate the class
require_once(QUBEADMIN . 'inc/reseller.class.php');
require_once(QUBEADMIN . 'inc/Authnet.class.php');
$reseller = new Reseller();
if($continue){	
	//***************************************************************************
	//if user is trying to add a new reseller (admins) or a new user (resellers)
	//***************************************************************************//
	if($action == "addNewReseller" || $action == "addNewUser"){
		$user_id = $reseller->validateUserId($_POST['email'], 'Email Address');
		$password = $reseller->validatePassword($_POST['password'], 'Password', 3, 12);
		$password2 = $_POST['password2'];
		$firstName = $reseller->validateText($_POST['first_name'], 'First Name');
		$lastName = $reseller->validateText($_POST['last_name'], 'Last Name');
		$phone = $reseller->validateNumber($_POST['phone'], 'Phone');
		$company = $reseller->validateText($_POST['company'], 'Company Name', true);
		$address = $reseller->validateText($_POST['address'], 'Address', true);
		$address2 = $reseller->validateText($_POST['address2'], 'Address 2', true);
		$city = $reseller->validateText($_POST['city'], 'City', true);
		$state = $reseller->validateText($_POST['state'], 'State', true);
		$zip = $reseller->validateNumber($_POST['zip'], 'Zip Code');
		$product = $_POST['class'];
		$query = "SELECT maps_to FROM user_class WHERE id = '".$product."'";
		$result = $reseller->queryFetch($query);
		if($result['maps_to']==668) $user_manager = 1;
		else $user_manager = 0;
		$continue = 1;
		if($action == "addNewReseller"){
			$parent_id = 0;
			$superUser = 1;
			$class = 17;
			$reseller_client = 0;
			$payments = $_POST['payments'];
		}
		else {
			$parent_id = is_numeric($_POST['parent_id']) ? $_POST['parent_id'] : 0;
			if($parent_id && $parent_id!=7){ $reseller_client = 1; $sixqube_client = 0; }
			else if($parent_id==7){ $sixqube_client = 1; $reseller_client = 0; }
			$superUser = 0;
			if(strpos($product, '-')!==false){
				$a = explode('-', $product);
				$class = $a[0];
				if($a[1]=='add_site') $addSite = true;
			}
			else $class = $product;
			if($_SESSION['admin'] && !$class) $class = 178;
		}
		$active = $_POST['active'] ? 1 : 0;
		$class = is_numeric($class) ? $class : '';
		
		if($reseller->foundErrors() || !$class){ //check to see if there were any errors
			$response['action'] = 'errors';
			$response['errors'] = $reseller->listErrors('<br />'); //if so then store the errors
		}
		else if($password != $password2){
			$response['action'] = 'errors';
			$response['errors'] = 'Passwords did not match.';
		}
		else if($continue){
			$password = $reseller->encryptPassword($password);
			$query = "INSERT INTO users 
					(parent_id, username, password, access, class, super_user, reseller_client, sixqube_client, created) 
					VALUES 
					('{$parent_id}', '{$user_id}', '{$password}', '{$active}', '{$class}', '{$superUser}', 
					'{$reseller_client}', '{$sixqube_client}', now())";
			$result = $reseller->query($query);
			if($result){
				$insertID = $reseller->getLastId();
				$query = "INSERT INTO user_info (user_id, firstname, lastname, phone, company, address, address2, city, state, zip, user_manager) VALUES ('{$insertID}', '{$firstName}', '{$lastName}', '{$phone}', '{$company}', '{$address}', '{$address2}', '{$city}', '{$state}', '{$zip}', '{$user_manager}')";
				if($result = $reseller->query($query)){ //if the query was successful (New user added)
					$anet = new Authnet();
					
					//Create campaign for user
					$cid = $reseller->addNewCampaign($insertID, $company, NULL, true);
					
					if($action == "addNewReseller"){
						//add entry into resellers table
						$query = "INSERT INTO resellers (admin_user, company, support_email, payments, created) 
								VALUES ({$insertID}, '{$company}', '{$user_id}', '{$payments}', now())";
						$reseller->query($query);
						//Create blank hub (to be reseller's main site)
						require_once(QUBEADMIN . 'inc/hub.class.php');
						$hub = new Hub();
						$newHubID = $hub->addNewHub($insertID, $company, 99999, -1, true, true);
						//Update resellers table with hub id
						$query = "UPDATE resellers SET main_site_id = '".$newHubID."' WHERE admin_user = '".$insertID."'";
						$reseller->query($query);
						//Create user/reseller folders
						$reseller->createUserFolders($insertID, true);
						//Create their default products
						$reseller->setupResellerProducts($insertID, $company);
						//Create their CIM profile for wholesale/monthly billing
						$anet->setupCIMProfile($insertID, true);
						//NOTE: reseller main site and network aren't set up here
						//they're set up when user executes the reseller-site-setup script
					}
					else {
						//Create user folders
						$reseller->createUserFolders($insertID);
						//Set them up a CIM profile
						//if new user is a reseller client
						if($parent_id){
							//check to see if user's parent has their own merchant acct
							$query = "SELECT payments, payments_acct, payments_acct2 
									FROM resellers WHERE admin_user = '".$parent_id."'";
							$resellerInfo = $reseller->queryFetch($query);
							
							if($resellerInfo['payments']=='authorize'){
								//if reseller has their own anet account
								//set the new user up with a CIM under it
								$apiLogin = $resellerInfo['payments_acct'];
								$apiAuth = $resellerInfo['payments_acct2'];
								$anet->setupCIMProfile($insertID, NULL, $apiLogin, $apiAuth);
							}
							else if($resellerInfo['payments']=='6qube'){
								//if the reseller is using us as their payment processor
								//set the new user up with a CIM profile under our acct
								$anet->setupCIMProfile($insertID);
							}
							else if($resellerInfo['payments']=='paypal'){
								////////////////////////
								// PAYPAL GOES HERE
								////////////////////////
							}
							
							//if it's a reseller client and not a free product
							//set up user's billing and try to charge reseller wholesale
							if(!$reseller->isFreeAccount($class)){
								//get product/wholesale info
								$productInfo = $reseller->getResellerProducts($parent_id, $class, true);
								
								//reseller wholesale - charged now (if they're not using 6qube's processor)
								if($resellerInfo['payments']!='6qube'){
									$logNote = 'Wholesale charge for initial setup';
									$wholesaleAmount = $productInfo['wholesale']['monthly_price'];
									if($productInfo['wholesale']['setup_price']>0)
										$wholesaleAmount += $productInfo['wholesale']['setup_price'];
									if(($productInfo['wholesale']['add_site']>0) && $addSite){
										$wholesaleAmount += $productInfo['wholesale']['add_site'];
										$logNote .= ' and custom theme';
									}
									//now that we know what to charge for wholesale attempt it
									$wholesaleBill = $anet->chargeCIM($parent_id, NULL, $class, $wholesaleAmount, 
																'wholesale', 1, $logNote, NULL, NULL, $insertID);
									if($wholesaleBill['success']===false){
										//if wholesale charge failed delete new user and alert reseller
										$query = "DELETE FROM users WHERE id = '".$insertID."' LIMIT 1";
										$reseller->query($query);
										$query = "DELETE FROM user_info WHERE user_id = '".$insertID."' LIMIT 1";
										$reseller->query($query);
										$query = "DELETE FROM campaigns WHERE user_id = '".$insertID."' LIMIT 1";
										$reseller->query($query);
										$reseller->deleteFolder(QUBEROOT . 'users/' .$insertID);
										
										$response['action'] = 'errors';
										$response['errors'] = '<b>Could not create account:</b><br />There was an error when we tried to bill you for our wholesale amount:<br />'.$wholesaleBill['message'];
									} //end if(!$wholesaleBill['success'])
								} //end if($resellerInfo['payments']!='6qube')
								
								//user's charge - only schedule if reseller is using a supported processor
								if($resellerInfo['payments']!='other' && $resellerInfo['payments']!='paypal' && !$response['errors']){
									//user's charges
									$userMonthlyAmount = $productInfo['monthly_price'];
									if($productInfo['setup_price']>0)
										$userSetupAmount = $productInfo['setup_price'];
									if($productInfo['add_site']>0 && $addSite)
										$userSetupAmount += $productInfo['add_site'];
									//schedule charges to happen once user sets up a billing profile
									if($userSetupAmount>0){
										//setup fees
										if($addSite) $setupNote = 'add_site';
										$reseller->scheduleTransaction(NULL, $insertID, $parent_id, $class, 
																'initialSetupFees', $userSetupAmount, NULL, 
																$setupNote, NULL, 0, NULL, 1);
									}
									//monthly charge
									$reseller->scheduleTransaction(NULL, $insertID, $parent_id, $class, 
															'initialSubscription', $userMonthlyAmount, NULL, 
															NULL, NULL, 0, NULL, 1);
								}
								//if reseller is using Other or Paypal as their processor set up wholesale billing
								//to occur every month
								else if(($resellerInfo['payments']=='other' || $resellerInfo['payments']=='paypal') && !$response['errors']){
									$monthlyWholesale = $productInfo['wholesale']['monthly_price'];
									$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
									$reseller->scheduleTransaction(NULL, $parent_id, NULL, $class, 
															'wholesale', $monthlyWholesale, NULL, 
														"Monthly wholesale charge for user account type - OtherRecur",
															$nextBillDate, 2, $insertID);
								}
							} //end if(!$reseller->isFreeAccount($class))
							
						} //end if($parent_id)
						//otherwise set them up with 6qube's anet acct
						else {
							$anet->setupCIMProfile($insertID);
						}
					} //end else [new user is a reseller client or admin created]
					
				}//end if($result = $reseller->query($query))
			}//end if
			if(!$response['action']){
				$response['action'] = 'redir';
				if($action == "addNewReseller") $response['url'] = 'admin-resellers.php';
				else $response['url'] = 'admin-users.php';
				$response['lognote'] = $logNote;
				$response['anet'] = $wholesaleBill['message'];
			}
		}// end first else
		
		echo json_encode($response);
	} //end if($action == "addNewReseller" || $action == "addNewUser")
	
	//***************************************************************************
	//if user is trying to delete another user
	//***************************************************************************//
	else if($action == "delUser"){
		$user_id = $_POST['user_id'];
		$del_user = $_POST['del_user'];
		$del_all = $_POST['del_all'];
		//first check to see if user is an admin
		$query = "SELECT super_user FROM users WHERE id = '".$user_id."'";
		$result = $reseller->queryFetch($query);
		if($result['super_user']==2) $continue = true;
		else {
			//if user is a reseller and not an admin, check to make sure their id
			//is the same as the parent_id of del_user
			$query = "SELECT parent_id FROM users WHERE id = '".$del_user."'";
			$result = $reseller->queryFetch($query);
			if($result['parent_id']==$user_id){
				$continue = true;
				$isReseller = true;
			}
			else $continue = false;
		}
		
		if($continue){
			//first backup
			$query = "SELECT * FROM users WHERE id = '".$del_user."'";
			if($row = $reseller->queryFetch($query)) $reseller->serialBackup($row, 'deleted-users-bckp', 
																$del_user.'-users.txt');
			//delete from users table
			$query = "DELETE FROM users WHERE id = '".$del_user."' LIMIT 1";
			if($result = $reseller->query($query)){
				//first backup
				$query = "SELECT * FROM user_info WHERE user_id = '".$del_user."'";
				if($row = $reseller->queryFetch($query)) 
					$reseller->serialBackup($row, 'deleted-users-bckp', $del_user.'-user_info.txt');
				//delete from user_info table
				$query = "DELETE FROM user_info WHERE user_id = '".$del_user."' LIMIT 1";
				if($result = $reseller->query($query)){
					//user successfully deleted
					$response['action'] = 'redir';
					$response['url'] = 'admin-users.php';
					//delete future billing cycles
					if($row['parent_id']==7) $table = 'sixqube_billing';
					else $table = 'resellers_billing';
					$query = "DELETE FROM `".$table."` WHERE (user_id = '".$del_user."' OR user_id2 = '".$del_user."') 
													AND sched = 1 AND sched_attempted = 0";
					$reseller->query($query);
					//delete them from CIM
					
					//if deleting all of user's items
					if($del_all=='true'){
						//back up each first and then delete
						//articles
						$query = "SELECT * FROM articles WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-articles-'.$row['id'].'.txt');
								$query = "DELETE FROM articles WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//autoresponders
						$query = "SELECT * FROM auto_responders WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', $del_user.'-auto_responders-'.
											  		$row['id'].'.txt');
								$query = "DELETE FROM auto_responders WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//blogs
						$query = "SELECT * FROM blogs WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-blogs-'.$row['id'].'.txt');
								$query = "DELETE FROM blogs WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//blog posts
						$query = "SELECT * FROM blog_post WHERE user_id = ".$del_user;
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-blog_post-'.$row['id'].'.txt');
								$query = "DELETE FROM blog_post WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//campaigns
						$query = "SELECT * FROM campaigns WHERE user_id = ".$del_user;
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-campaigns-'.$row['id'].'.txt');
								$query = "DELETE FROM campaigns WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//prospects
						$query = "SELECT * FROM contact_form WHERE user_id = ".$del_user;
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', $del_user.'-contact_form-'.
													$row['id'].'.txt');
								$query = "DELETE FROM contact_form WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//directory
						$query = "SELECT * FROM directory WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-directory-'.$row['id'].'.txt');
								$query = "DELETE FROM directory WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//hubs
						$query = "SELECT * FROM hub WHERE user_id = ".$del_user;
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', $del_user.'-hub-'.$row['id'].'.txt');
								$query = "DELETE FROM hub WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//hub pages
						$query = "SELECT * FROM hub_page WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', 
													$del_user.'-hub_page-'.$row['id'].'.txt');
								$query = "DELETE FROM hub_page WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//press releases
						$query = "SELECT * FROM press_release WHERE user_id = '".$del_user."'";
						if($result = $reseller->query($query)){
							if($reseller->numRows($result))
							while($row = $reseller->fetchArray($result)){
								$reseller->serialBackup($row, 'deleted-users-bckp', $del_user.'-press_release-'.
													$row['id'].'.txt');
								$query = "DELETE FROM press_release WHERE user_id = '".$del_user."'";
								$result2 = $reseller->query($query);
							}
						}
						//trashed items
						//$query = "SELECT * FROM trash WHERE user_id = ".$del_user;
						//if($result = $reseller->query($query)){
						//	if($reseller->numRows($result))
						//	while($row = $reseller->fetchArray($result)){
						//		$reseller->serialBackup($row, 'deleted-users-bckp', 
						//							$del_user.'-trash-'.$row['id'].'.txt');
						//		$query = "DELETE FROM trash WHERE user_id = '".$del_user."'";
						//		$result2 = $reseller->query($query);
						//	}
						//}
						//folders
						$reseller->deleteFolder(QUBEROOT . 'users/' .$del_user);
					} //end if($del_all)
				} //end if($result = $reseller->query($query))
				else {
					//error deleting from user_info
					$response['action'] = 'errors';
					$response['errors'] = 'This user was only partially deleted.  Please note the user\'s ID 
									  (#'.$del_user.') and contact support about this error.';
				}
			} //end if($result = $reseller->query($query)){
			else {
				//error deleting from users
				$response['action'] = 'errors';
				$response['errors'] = 'There was an error deleting this user.  Please try again or contact support.';
			}
		} //end if($continue)
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'You are not authorized to delete this user.';
		}
		
		echo json_encode($response);
	} //end else if($action == "delUser")
	
	//***************************************************************************
	//if user is trying to upgrade a user from a free account to a paid one
	//***************************************************************************//
	else if($action == "upgradeUser"){
		$user_id = $_POST['user_id'];
		$user_parent = $reseller->getParentID($user_id);
		$upgrade_class = $_POST['upgrade_class'];
		if(strpos($upgrade_class, '-')!==false){
			$a = explode('-', $upgrade_class);
			$upgrade_class = $a[0];
			if($a[1]=='add_site') $add_site = true;
		}
		
		//make sure user has permission
		if($_SESSION['reseller'] && ($user_parent==$_SESSION['login_uid'])){
			//get reseller info
			$query = "SELECT payments, payments_acct, payments_acct2 FROM resellers WHERE admin_user = '".$user_parent."'";
			$resellerInfo = $reseller->queryFetch($query);
			//get product info
			$productInfo = $reseller->getResellerProducts($user_parent, $upgrade_class, true);
			$anet = new Authnet();
			
			//attempt to charge reseller wholesale for new account type (if they're not using 6qube's account)
			if($resellerInfo['payments']!='6qube'){
				$logNote = 'Wholesale charge for initial setup fee';
				$wholesaleAmount = $monthlyWholesale = $productInfo['wholesale']['monthly_price'];
				if($productInfo['wholesale']['setup_price']>0)
					$wholesaleAmount += $productInfo['wholesale']['setup_price'];
				if(($productInfo['wholesale']['add_site']>0) && $add_site){
					$wholesaleAmount += $productInfo['wholesale']['add_site'];
					$logNote .= ' and custom theme';
				}
				//now that we know what to charge for wholesale attempt it
				$wholesaleBill = $anet->chargeCIM($user_parent, NULL, $upgrade_class, $wholesaleAmount, 
											'wholesale', 1, $logNote, NULL, NULL, $user_id);
				if(!$wholesaleBill['success']){
					$response['action'] = 'errors';
					$response['errors'] = '<b style="color:red;">Could not upgrade account:</b><br />There was an error when we tried to bill you for our wholesale amount:<br />'.$wholesaleBill['message'];
					
					//if the reseller is using Other or Paypal as their processor schedule this charge to recur
					if($resellerInfo['payments']=='other' || $resellerInfo['payments']=='paypal'){
						$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
						$reseller->scheduleTransaction(NULL, $user_parent, NULL, $upgrade_class, 'wholesale', $monthlyWholesale,
												NULL, "Monthly wholesale charge for user account type - OtherRecur",
												$nextBillDate, 2, $user_id);
					}
				}
			}
			
			//if wholesale billing was successful continue
			if(!$response['errors']){
				//upgrade user
				$query = "UPDATE users SET class = '".$upgrade_class."', upgraded = now() WHERE id = '".$user_id."'";
				$reseller->query($query);
				
				//schedule charges for when they set up their billing profile (if using a supported processor)
				if($resellerInfo['payments']!='other'){
					//user's charges
					$userMonthlyAmount = $productInfo['monthly_price'];
					if($productInfo['setup_price']>0)
						$userSetupAmount = $productInfo['setup_price'];
					if($productInfo['add_site']>0 && $add_site)
						$userSetupAmount += $productInfo['add_site'];
					//schedule charges
					if($userSetupAmount>0){
						//setup fees
						$reseller->scheduleTransaction(NULL, $user_id, $user_parent, $upgrade_class, 'initialSetupFees',
												 $userSetupAmount, NULL, NULL, NULL, 0, NULL, 1);
					}
					//monthly charge
					$reseller->scheduleTransaction(NULL, $user_id, $user_parent, $upgrade_class, 'initialSubscription',
											 $userMonthlyAmount, NULL, NULL, NULL, 0, NULL, 1);
				} //end if($resellerInfo['payments']!='other')
				
				$response['action'] = 'redir';
				$response['url'] = 'inc/forms/edit-user.php?id='.$user_id;
			} //end if(!$response['errors'])
		} //end if($_SESSION['reseller'] && ($user_parent==$_SESSION['login_uid']))
		echo json_encode($response);
	} //end else if($action == "upgradeUser")
	
	//***************************************************************************
	//if user is trying to change users (become another)
	//***************************************************************************//
	else if($action == "changeUser"){
		$login_uid = $_SESSION['login_uid'];
		$new_id = $_POST['new_id'];
		
		//make sure user is either admin or parent_id of new user
		if($_SESSION['admin']) $continue = true;
		else if($new_id == $_SESSION['login_uid']) $continue = true;
		else if($_SESSION['reseller']){
			$query = "SELECT parent_id FROM users WHERE id = ".$new_id;
			$result = $reseller->queryFetch($query);
			if($result['parent_id']==$_SESSION['login_uid']) $continue = true;
			else $continue = false;
		}
		
		if($continue){
			$query = "SELECT id, username, class, reseller_client, sixqube_client FROM users WHERE id = '".$new_id."'";
			$row = $reseller->queryFetch($query);
			if($row){
			$reseller->createUserFolders($row['id'], '', true);
				$_SESSION['user_id'] = $row['id'];
				$_SESSION['user'] = $row['username'];
				$_SESSION['user_class'] = $row['class'];
				$_SESSION['user_limits'] = $reseller->getLimits($row['id'], NULL, $row['class']);
				if($row['reseller_client']==1){
					$_SESSION['reseller_client'] = true;
					$_SESSION['parent_id'] = $reseller->getParentID($row['id']);
				}
				else $_SESSION['reseller_client'] = false;
				if($row['sixqube_client']==1){
					$_SESSION['sixqube_client'] = true;
					$_SESSION['parent_id'] = 7;
				}
				else $_SESSION['sixqube_client'] = false;
				
				$response['action'] = 'redir';
				$response['url'] = './';
			}
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'There was an error loading this user\'s information.  Refresh and try again or contact support for help.';
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'You are not authorized to manage this user.';
		}
		
		echo json_encode($response);
	} //end else if($action == "changeUser")
	
	//****************************************************************************************
	//if user is trying to allow/disallow classes from viewing custom backoffice navs/pages
	//**************************************************************************************//
	else if($action == "updateAllowedClass"){
		$update = $_POST['checked'];
		$id = is_numeric($_POST['id']) ? $_POST['id'] : '';
		$class = is_numeric($_POST['product']) ? $_POST['product'] : '';
		$muV2 = $_POST['muV2'] ? 1 : 0;
		if($muV2){
			$updateField = "multi_user_classes";
			$updateTable = "hub";
			$userField = "user_id";
			
		} else {
			$updateField = "allow_class";
			$updateTable = "resellers_backoffice";
			$userField = "admin_user";
		}
		
		$allow = $update ? 1 : 0;
		
		if($class && $id){
			//first get the colon-separated array from the table
			$query = "SELECT `".$updateField."` FROM `".$updateTable."` WHERE id = '".$id."' 
					AND `".$userField."` = '".$_SESSION['login_uid']."'";
			$a = $reseller->queryFetch($query);
			$removed = $false;
			//now go through and add or remove the class
			if($a[$updateField]){
				$allowed = explode('::', $a[$updateField]);
				foreach($allowed as $key=>$value){
					if($value == $class){
						$allowed[$key] = NULL;
						$removed = true;
					}
				}
				if(!$removed) $allowed[] = $class;
			}
			else $allowed = array($class);
			
			//build string
			if($allowed){
				$string = '';
				foreach($allowed as $key=>$value){
					if($value) $string .= $value.'::';
				}
				//trim trailing '::'
				$string = substr($string, 0, -2);
			}
			
			//update table row
			$query = "UPDATE `".$updateTable."` SET `".$updateField."` = '".$string."' WHERE id = '".$id."' 
					AND `".$userField."` = '".$_SESSION['login_uid']."'";
			if($reseller->query($query)){
				$response['action'] = 'success';
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'Error updating.';
		}
		
		echo json_encode($response);
	} //end else if($action == "updateAllowedClass")
	
	//***************************************************************************
	//if user is trying to add a new product
	//***************************************************************************//
	else if($action == "addNewProduct"){
		$user = $_SESSION['login_uid'];
		$name = $reseller->validateText($_POST['name'], 'Name');
		$codename = $reseller->validateText($_POST['type'], 'Code Name');
		$description = $reseller->validateText($_POST['description'], 'Description', true);
		$maps_to = $_POST['maps_to'];
			
			if($maps_to == "670"){ //set the limits
			$camps = '3';
			$dirs = '-1';
			$hubs = '5';
			$hubsDup = '-1';
			$landing = '10';
			$social = '5';
			$press = '-1';
			$arts = '-1';
			$blogs = '5';
			}
			if($maps_to == "463"){ //set the limits
			$camps = '0';
			$dirs = '0';
			$hubs = '0';
			$hubsDup = '0';
			$landing = '0';
			$social = '0';
			$press = '0';
			$arts = '0';
			$blogs = '0';
			}
			if($maps_to == "668"){ //set the limits
			$camps = '0';
			$dirs = '0';
			$hubs = '0';
			$hubsDup = '0';
			$landing = '0';
			$social = '0';
			$press = '0';
			$arts = '0';
			$blogs = '0';
			}
			if($maps_to == "27"){ //set the limits
			$camps = '1';
			$dirs = '3';
			$hubs = '1';
			$hubsDup = '0';
			$landing = '0';
			$social = '0';
			$press = '3';
			$arts = '3';
			$blogs = '1';
			}
			if($maps_to == "28"){ //set the limits
			$camps = '1';
			$dirs = '-1';
			$hubs = '3';
			$hubsDup = '-1';
			$landing = '10';
			$social = '3';
			$press = '-1';
			$arts = '-1';
			$blogs = '3';
			}
			if($maps_to == "30"){ //set the limits
			$camps = '5';
			$dirs = '-1';
			$hubs = '15';
			$hubsDup = '-1';
			$landing = '25';
			$social = '15';
			$press = '-1';
			$arts = '-1';
			$blogs = '15';
			}
			if($maps_to == "188"){ //set the limits
			$camps = '10';
			$dirs = '-1';
			$hubs = '50';
			$hubsDup = '-1';
			$landing = '100';
			$social = '50';
			$press = '-1';
			$arts = '-1';
			$blogs = '50';
			}
			if($maps_to == "189"){ //set the limits
			$camps = '25';
			$dirs = '-1';
			$hubs = '125';
			$hubsDup = '-1';
			$landing = '250';
			$social = '125';
			$press = '-1';
			$arts = '-1';
			$blogs = '125';
			}
		
		if($reseller->foundErrors()){ //check to see if there were any errors
			$response['action'] = 'errors';
			$response['errors'] = $reseller->listErrors('<br />'); //if so then store the errors
		}
		else {
			$query = "INSERT INTO user_class (admin_user, maps_to, type, name, description, campaign_limit, directory_limit, hub_limit, hub_dupe_limit, landing_limit, social_limit, press_limit, article_limit, blog_limit) 
					VALUES (".$user.", ".$maps_to.", '".$codename."', '".$name."', '".$description."', ".$camps.", ".$dirs.", ".$hubs.", ".$hubsDup.", ".$landing.", ".$social.",".$press.", ".$arts.", ".$blogs.")";
			if($result = $reseller->query($query)){
				$response['action'] = 'redir';
				$response['url'] = 'reseller-products.php';
			}
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'Error adding product:\n';//.mysqli_error();
			}
		}
		echo json_encode($response);
	} //end else if($action == "addNewProduct")
	
	//***************************************************************************
	//if user is trying to delete a product
	//***************************************************************************//
	else if($action == "delProduct"){
		$product = is_numeric($_POST['product']) ? $_POST['product'] : '';
		$admin_user = is_numeric($_POST['admin_user']) ? $_POST['admin_user'] : '';
		
		if(($admin_user == $_SESSION['login_uid']) || $_SESSION['admin']){ //make sure user is authorized
			$query = "DELETE FROM user_class WHERE id = '".$product."' AND admin_user = '".$admin_user."' LIMIT 1";
			
			if($reseller->query($query)){
				$response['action'] = 'redir';
				if($_SESSION['admin']) $response['url'] = 'inc/forms/edit-user-reseller-products.php?id='.$admin_user;
				else $response['url'] = 'reseller-products.php';
			}
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'Error deleting product:\n';//.mysqli_error();
			}
		}
		echo json_encode($response);
	} //end else if($action == "delProduct")
	
	//***************************************************************************
	//if user is trying to duplicate a product
	//***************************************************************************//
	else if($action == "dupeProduct"){
		$product = is_numeric($_POST['product']) ? $_POST['product'] : '';
		$admin_user = is_numeric($_POST['admin_user']) ? $_POST['admin_user'] : '';
		
		if(($admin_user == $_SESSION['login_uid']) || $_SESSION['admin']){ //make sure user is authorized
			$query = "INSERT INTO user_class (admin_user, maps_to, type, name, description, setup_price, monthly_price, pay_link, add_site, campaign_limit, directory_limit, hub_limit, hub_dupe_limit, landing_limit, social_limit, press_limit, article_limit, blog_limit, responder_limit, email_limit) 
					(SELECT admin_user, maps_to, type, name, description, setup_price, monthly_price, pay_link, add_site, campaign_limit, directory_limit, hub_limit, hub_dupe_limit, landing_limit, social_limit, press_limit, article_limit, blog_limit, responder_limit, email_limit FROM user_class WHERE id = '".$product."')";
					
			if($reseller->query($query)){
				$newID = $reseller->getLastId();
				$query2 = "UPDATE user_class SET name = concat(name,' (copy)') WHERE id = '".$newID."'";
				$reseller->query($query2);
				
				$response['action'] = 'redir';
				if($_SESSION['admin']) $response['url'] = 'inc/forms/edit-user-reseller-products.php?id='.$admin_user;
				else $response['url'] = 'reseller-products.php';
			}
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'Error deleting product:<br />';//.mysqli_error();
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'Not authorized';
		}
		echo json_encode($response);
	} //end else if($action == "dupeProduct")
	
	//***************************************************************************
	//if user is trying to edit theme
	//***************************************************************************//
	else if($action == "editTheme"){
		$user_id = $_POST['user_id'];
		$property = $_POST['property'];
		$value = $_POST['value'];
		
		if($user_id == $_SESSION['login_uid']){
			$query = "UPDATE resellers SET `".$property."` = '".$value."' WHERE admin_user = ".$user_id;
			if($reseller->query($query)){
				//change session vars
				$_SESSION['thm_'.$property] = $value;
				
				$a = explode("_", $_SESSION['thm_app-background']);
				$b = explode('-', $a[0]);
				$c = explode('-', $a[1]);
				$appBackgroundStyle1 = $b[0];
				$appBackgroundColor1 = $b[1];
				$appBackgroundStyle2 = $c[0];
				$appBackgroundColor2 = $c[1];
				$appBackgroundColor3 = $a[2];
				
				$a = explode('-', $_SESSION['thm_list-bg-clr']);
				$listItemBgClr1 = $a[0];
				$listItemBgClr2 = $a[1];
				
				////////////////////////////////////////////////////
				//HEADER/BODY								//
				////////////////////////////////////////////////////				
				//bg-img-rpt (body background image repeat)
				if($property=="bg-img-rpt"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme2';
					//user-bar text
					$response['div1'] = 'html,body';
					$response['property1'] = 'background-repeat';
					$response['value1'] = $value;
				}
				
				//bg-clr (body background color)
				else if($property=="bg-clr"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//userbar-text (user/admin bar text)
				else if($property=="userbar-text"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//userbar-links (user/admin bar links)
				else if($property=="userbar-links"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//adminbar-clr (admin bar color)
				else if($property=="adminbar-clr"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//main_link_color (main links color)
				else if($property=="main_link_color"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//main_link_hover (main links hover)
				else if($property=="main_link_hover"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//left_nav_active (left nav active color)
				else if($property=="left_nav_active"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs2';
				}
				
				//left_nav_slide (left nav slide color)
				else if($property=="left_nav_slide"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs2';
				}
				
				//userbar-clr (user bar color)
				else if($property=="userbar-clr"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme';
					//user-panel images
					$response['div1'] = '#header #user-bar .top';
					$response['property1'] = 'background';
					$response['value1'] = "url('img/user-bar-left/".$value.".png') no-repeat";
					$response['div2'] = '#header #user-bar .middle';
					$response['property2'] = 'background';
					$response['value2'] = "url('img/user-bar-middle/".$value.".png') repeat-x";
					$response['div3'] = '#header #user-bar .bottom';
					$response['property3'] = 'background';
					$response['value3'] = "url('img/user-bar-right/".$value.".png') no-repeat";
				}
				
				////////////////////////////////////////////////////
				//LEFT NAV								//
				////////////////////////////////////////////////////
				//nav-bg (left nav)
				else if($property=="nav-bg"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs';
					
					//$response['div1'] = '#navigation ul.top li a';
					//$response['property1'] = 'background';
					//$response['value1'] = "url('img/nav-bg/".$value.".png') no-repeat";
				}
				
				//nav-bg (left nav slide-down)
				else if($property=="nav-bg2"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs';
					//bg color
					$a = explode('_', $value);
					$leftNavBG = $a[1];
					$response['div1'] = '#navigation ul.child li a';
					$response['property1'] = 'background';
					$response['value1'] = '#'.$leftNavBG;
					//clear bg on last
					$response['div2'] = '#navigation ul.child li a.last';
					$response['property2'] = 'background';
					$response['value2'] = 'none';
					//bottom image
					$response['div3'] = '#navigation ul.child li a.last';
					$response['property3'] = 'background';
					$response['value3'] = "url('img/nav-child-bottom/".$value.".png') no-repeat bottom left";
				}
				
				//nav-links3 (left nav tab links color)
				else if($property=="nav-links3"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs2';
					//tab
					//no inactive tab on Theme page so skip css
					//$response['div1'] = '#navigation a';
					//$response['property1'] = 'color';
					//$response['value1'] = "#".$value;
				}
				
				//nav-links (left nav slide-down links color)
				else if($property=="nav-links"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs2';
				}
				
				//nav-links2 (left nav active tab link color)
				else if($property=="nav-links2"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs2';
				}
				
				//nav-links4 (left nav tab links color)
				else if($property=="nav-links4"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs2';
					//no inactive tab on Theme page so skip css
				}
				
				////////////////////////////////////////////////////
				//TOP NAV									//
				////////////////////////////////////////////////////
				//nav2-bg (top nav)
				else if($property=="nav2-bg"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs';
					//middle
					$response['div1'] = '.header-nav li';
					$response['property1'] = 'background';
					$response['value1'] = "url('img/nav-form-bg/".$value.".png') repeat-x";
					//left
					$response['div2'] = '.header-nav li.first';
					$response['property2'] = 'background';
					$response['value2'] = "url('img/nav-form-left/".$value.".png') no-repeat top left";
					//right
					$response['div3'] = '.header-nav li.last';
					$response['property3'] = 'background';
					$response['value3'] = "url('img/nav-form-right/".$value.".png') no-repeat top right";
				}
				
				//nav2-bg2 (top nav active)
				else if($property=="nav2-bg2"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs';
					//active tab
					$response['div1'] = '.header-nav a:hover,.header-nav a.current';
					$response['property1'] = 'background';
					$response['value1'] = "url('img/nav-form-hover-bg/".$value.".png')";
				}
				
				//nav2-links (top nav links color)
				else if($property=="nav2-links"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs';
					//inactive tab link color
					$response['div1'] = '.header-nav a';
					$response['property1'] = 'color';
					$response['value1'] = "#".$value;
				}
				
				//nav2-links (top nav active/hover links color)
				else if($property=="nav2-links2"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-navs';
					//active tab link color
					$response['div1'] = '.header-nav a:hover,.header-nav a.current';
					$response['property1'] = 'color';
					$response['value1'] = "#".$value;
				}
				
				////////////////////////////////////////////////////
				//CONTENT NAV								//
				////////////////////////////////////////////////////
				//nav3-bg (content nav)
				else if($property=="nav3-bg"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs';
				}
				
				//nav3-bg2 (content nav active)
				else if($property=="nav3-bg2"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs';
					//active tab
				}
				
				//nav3-links (left nav links color)
				else if($property=="nav3-links"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs';
					//inactive tab link color
				}
				
				//nav3-links (left nav active/hover links color)
				else if($property=="nav3-links2"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-navs';
					//active tab link color
				}
				////////////////////////////////////////////////////
				//CONTENT									//
				////////////////////////////////////////////////////
				//welcome-msg (welcome message)
				else if($property=="welcome-msg"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//dflt-link-clr (default link color)
				else if($property=="dflt-link-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//camp-pnl (custom campaign panel on/off)
				else if($property=="camp-pnl"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//camp-pnl-title (custom campaign panel title)
				else if($property=="camp-pnl-title"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//camp-pnl-html (custom campaign panel html)
				else if($property=="camp-pnl-html"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//camp-pnl-brdr-clr (campaign panels border color)
				else if($property=="camp-pnl-brdr-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//camp-blog-disp (blog section on camps page)
				else if($property=="camp-blog-disp"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//camp-blog-id (id for blog section on camps page)
				else if($property=="camp-blog-id"){
					$response['action'] = 'none';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//buttons-clr (buttons color)
				else if($property=="buttons-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//titlebar-clr (panel title bar color)
				else if($property=="titlebar-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//newitem-clr (new item button color)
				else if($property=="newitem-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//person-clr (person icon color)
				else if($property=="person-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//undobar-clr (undo bar color)
				else if($property=="undobar-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//css_link (add css link)
				else if($property=="css_link"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme2';
				}
				
				//form-input-clr (form text input color)
				else if($property=="form-input-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//form-input-brdr-clr (form text input border color)
				else if($property=="form-input-brdr-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//form-lbl-clr (form label color)
				else if($property=="form-lbl-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//form-img-brdr-clr (image preview border color)
				else if($property=="form-img-brdr-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//app-right-link (right panel bullet/link color)
				else if($property=="app-right-link"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//app-right-link2 (right panel bullet/link color - hover)
				else if($property=="app-right-link2"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content2';
				}
				
				//list-bg-clr (list item background color)
				else if($property=="list-bg-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//list-link-clr (list item link color)
				else if($property=="list-link-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//wait-anim-clr (wait box animation color)
				else if($property=="wait-anim-clr"){
					$response['action'] = 'refresh';
					$response['returnPage'] = 'edit-theme-content';
				}
				
				//wait-box-clr (wait box background color)
				else if($property=="wait-box-clr"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-content';
					//wait box bg color
					$response['div1'] = '#wait';
					$response['property1'] = 'background-color';
					$response['value1'] = "#".$value;
				}
				
				//wait-brdr-clr (wait box border color)
				else if($property=="wait-brdr-clr"){
					$response['action'] = 'css';
					$response['returnPage'] = 'edit-theme-content';
					//wait box border color
					$response['div1'] = '#wait';
					$response['property1'] = 'border';
					$response['value1'] = "1px solid #".$value;
				}
				
				//support theme settings (do nothing)
				else if($property=="support-page" ||
					   $property=="support_email" ||
					   $property=="support_phone" ||
					   $property=="activation_email"){
					$response['action'] = 'none';
				}
				
				//login theme settings (do nothing)
				else if($property=="login-bg-clr" || 
					   $property=="login-brdr-clr" || 
					   $property=="login-txt-clr" || 
					   $property=="login-lnk-clr" || 
					   $property=="login-signup-lnk" || 
					   $property=="login-suggest-ff"){
					$response['action'] = 'none';
				}
				
				///////////////////////
				//Jon added stuff:   //
				///////////////////////
				else if($property=="jon-added-this"){
					$response['action'] = 'none';
				}
				
				////////////////
				//exception   //
				////////////////
				else {
					$response['action'] = 'errors';
					$response['errors'] = 'Setting not defined.';
				}
			}
			//error with query
			else {
				$response['action'] = 'errors';
				$response['errors'] = 'There was an error saving your settings.';
				$response['errors'] .= "\n";//.mysqli_error();
			}
		}
		//input ID and login_uid do not match
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'You are not authorized to edit this user\'s theme.';
		}
		
		echo json_encode($response);
	} //end else if($action == "editTheme")
	
	//***************************************************************************
	//if user is trying to change theme logo
	//***************************************************************************//
	else if($action == "editThemeImg"){
		$user_id = $_POST['reseller_uid'];
		$logo_type = $_POST['logo_type'];
		$img_directory = QUBEADMIN . 'themes/'.$user_id.'/';
		
		$allowed_types = array('image/gif', 'image/png');
		if($logo_type!="themeLogo" && $logo_type!="themeFavicon"){
			$allowed_types[] = 'image/jpeg';
			$allowed_types[] = 'image/pjpeg';
		}
		
		if(in_array($_FILES["theme_img"]["type"], $allowed_types)){
			if($_FILES["theme_img"]['size'] < 1048576){ //if image is less than 1mb
			
				if(is_file($img_directory.$_FILES["theme_img"]["name"])){ //if img already exists append with a number
					$a = explode('.', $_FILES["theme_img"]["name"]); //break apart
					$ext = $a[count($a)-1]; //get extension
					$a[count($a)-1] = ''; //remove extension
					$name = trim(substr(implode('.', $a), 0, -1)); //put back together
					$copy_name = $name.'_'.substr(strtotime('now'), -3).'.'.$ext; //add #
				} else {
					$copy_name = $_FILES["theme_img"]["name"];
				}
				
				if(move_uploaded_file($_FILES["theme_img"]["tmp_name"], $img_directory.$copy_name)){
					$filename = $copy_name;
					
					//if file copied successfully update DB & return info
					require_once(QUBEADMIN . 'inc/db_connector.php');
					$connector = new DbConnector();
					
					if($logo_type=='themeBgImg'){
						$_SESSION['thm_bg-img'] = $filename;
						$field = 'bg-img';
					}
					if($logo_type=='themeLogo'){
						$_SESSION['thm_logo'] = $filename;
						$field = 'logo';
					}
					else if($logo_type=='themeFavicon'){
						$_SESSION['thm_favicon'] = $filename;
						$field = 'favicon';
					}
					else if($logo_type=='themeDfltNoImg'){
						$_SESSION['thm_dflt-no-img'] = $filename;
						$field = 'dflt-no-img';
					}
					else if($logo_type=='loginLogo'){
						$field = 'login-logo';
					}
					else if($logo_type=='loginBG'){
						$field = 'login-bg';
					}
					else if($logo_type=='actEmailImg'){
						$field = 'activation_email_img';
					}
					$query = "UPDATE resellers SET `".$field."` = '".$filename."' WHERE admin_user = '".$user_id."'";
					$result = $connector->query($query);
					
					echo '{"result": "true", "file": "./themes/'.$user_id.'/'.$filename.'", "filename": "'.$filename.'"}';
					exit;
				} //end if(move_uploaded_file(...
				else {
					echo '{"result": "false", "msg": "Error uploading file"}';
					exit;
				}
				
			} //end if($_FILES["theme_logo"]['size'] < 1048576)
			else echo '{"result": "false", "msg": "Image is too large.  Must be under 1MB."}';
		} //end if(($_FILES["theme_logo"]["type"] == "image/gif")...
		else {
			echo '{"result": "false", "msg": "Bad filetype: '.$_FILES["theme_img"]["type"].'."}';
			exit;
		}
	} //end else if($action == "editThemeImg")
	
	//***************************************************************************
	//if user is setting up a new reseller site (and has validated domain)
	//***************************************************************************//
	else if($action == "resellerSiteSetup"){
		$uid = $_POST['uid'];
		//break apart url
		$fullUrl = $domain = $_POST['domain'];
		$domain = substr($domain, 7); //remove http://
		$domain_array = explode(".", $domain); //array of domain components
		$domain2 = $domain_array[count($domain_array)-2].".".$domain_array[count($domain_array)-1]; //example.com
		//$domain2 = $domains->validateDomain($domain2);
		$tld = $domain_array[count($domain_array)-1]; //tld
		if($domain_array[0]=="www"||count($domain_array)==2) $subdomain = "www"; //subdomain
		else $subdomain = $domain_array[0]; //subdomain
		$dname = explode(".", $domain2);
		$domainName = $dname[0]; //just the domain without tld or sub
		
		if($uid == $_SESSION['login_uid']){
			$errors = 0;
			//update main_site field in reseller table
			$query = "UPDATE resellers SET main_site = '".$domain2."' WHERE admin_user = '".$uid."'";
			if(!$reseller->query($query)){
				$errors++;
				$error_msg .= "\n";//.addslashes(mysqli_error());
			}
			//update hub table
			$query = "UPDATE hub SET 
					user_parent_id = ".$_SESSION['login_uid'].", 
					domain = 'http://".$domain2."', 
					theme = '38' 
					WHERE id = '".$_SESSION['main_site_id']."'";
			if(!$reseller->query($query)){
				$errors++;
				$error_msg .= "\n";//.addslashes(mysqli_error());
			}
			//add default networks to resellers_network table
				//get company name first
				$query = "SELECT company FROM user_info WHERE user_id = ".$uid;
				$result = $reseller->queryFetch($query);
				$company = $result['company'];
				//entry for whole network
				$query = "INSERT INTO resellers_network
						(admin_user, type, name, domain, company)
						VALUES
						(".$uid.", 'whole', '".$company." Default Network', '".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $networkID = $reseller->getLastId();
				//local
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'local', '".$company." Local', 'http://local.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $localTracking = $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://local.'.$domain2);
				//hubs
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'hubs', '".$company." Hubs', 'http://hubs.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://hubs.'.$domain2);
				//blogs
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'blogs', '".$company." Blogs', 'http://blogs.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://blogs.'.$domain2);
				//articles
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'articles', '".$company." Articles', 'http://articles.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://articles.'.$domain2);
				//press
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'press', '".$company." Press', 'http://press.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://press.'.$domain2);
				//search
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'search', '".$company." Search', 'http://search.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://search.'.$domain2);
				//browse
				$query = "INSERT INTO resellers_network
						(admin_user, parent, type, name, domain, company)
						VALUES
						(".$uid.", ".$networkID.", 'browse', '".$company." Browse', 'http://browse.".$domain2."', '".$company."')";
				if(!$reseller->query($query)){
					$errors++;
					$error_msg .= "\n";//.addslashes(mysqli_error());
				}
				else $reseller->ANAL_addSite('resellers_network', $reseller->getLastId(), 'http://browse.'.$domain2);
			//update whole network with tracking_id of Local network site
			$query = "UPDATE resellers_network SET tracking_id = '".$localTracking."' WHERE id = '".$networkID."'";
			$reseller->query($query);
			//update analytics url
			require_once(QUBEADMINN . 'inc/analytics.class.php');
			$anal = new Analytics();
			if(!$anal->ANAL_updateSite('hub', $_SESSION['main_site_id'], $fullUrl)){
				$errors++;
				$error_msg .= "\n";//.addslashes(mysqli_error());
			}
			//set session vars
			$_SESSION['main_site'] = $domain2;
			//update .htaccess
			if(!$reseller->updateHtaccess($domain2)){
				$errors++;
				$error_msg .= "\nError updating .htaccess";
			}
			//add default custom back office pages to resellers_backoffice table
			$navName = "Home";
			$pageName = "Welcome";
			$pageData = "Welcome to the organic marketing suite.";
			$query = "INSERT INTO resellers_backoffice
					(admin_user, type, name)
					VALUES
					('".$uid."', 'nav', '".$navName."')";
			$reseller->query($query);
			$newNavID = $reseller->getLastId();
			$query = "INSERT INTO resellers_backoffice
					(admin_user, type, parent, name, data)
					VALUES
					('".$uid."', 'page', '".$newNavID."', '".$pageName."', '".$pageData."')";
			$reseller->query($query);
			//return
			if($errors){
				$response['action'] = 'errors';
				$response['errors'] = $error_msg;
			}
			else {
				$reseller->updateWholeNetwork($networkID, 'setup', 1);
				$response['action'] = 'success';
			}
		}
		else {
			$response['action'] = 'errors';
			$response['errors'] = 'Not authorized';
		}
		
		echo json_encode($response);
	} //end else if($action == "resellerSiteSetup")
	
	//***************************************************************************
	//if a user is trying to add a new billing profile to an ANet CIM
	//***************************************************************************//
	else if($action == "addNewBillingProfile" || $action == "updateBillingProfile"){
		$uid = ($_POST['uid'] && is_numeric($_POST['uid'])) ? $_POST['uid'] : '';
		$rid = ($_POST['rid'] && is_numeric($_POST['rid'])) ? $_POST['rid'] : '';
		if($rid){
			//if a reseller has been specified (the user adding a profile is a reseller client)
			$query = "SELECT payments, payments_acct, payments_acct2 FROM resellers WHERE admin_user = '".$rid."'";
			$resellerInfo = $reseller->queryFetch($query);
			if($resellerInfo['payments']=='authorize'){
				//if they're using their own merchant account set vars
				$apiLogin = $resellerInfo['payments_acct'];
				$apiAuth = $resellerInfo['payments_acct2'];
				$anetType = 'authorize';
			}
			//otherwise 6qube is used by default
			else $anetType = '6qube';
		}
		$anet = new Authnet();
		
		//get user's CIM profile id
		$query = "SELECT billing_id, billing_id2 FROM users WHERE id = '".$uid."'";
		$a = $reseller->queryFetch($query);
		//if they don't have one create it
		if(!$a['billing_id']){
			$newCIM = $anet->setupCIMProfile($uid, NULL, $apiLogin, $apiAuth);
			$a['billing_id'] = $newCIM['profile_id'];
		}
		
		if($uid && $a['billing_id']){
			//set params to send to function
			$params = array('firstname' => $_POST['firstname'],
						 'lastname' => $_POST['lastname'],
						 'company' => $_POST['company'],
						 'address' => $_POST['address'],
						 'city' => $_POST['city'],
						 'state' => $_POST['state'],
						 'zip' => $_POST['zip'], 
						 'phone' => $_POST['phone'],
						 'cardnumber' => $_POST['cardnumber'],
						 'expmonth' => $_POST['expMonth'],
						 'expyear' => $_POST['expYear'],
						 'cvv' => "$_POST[cvv]");
			if($action=='addNewBillingProfile')
				$profile = $anet->setupPaymentProfile($uid, $a['billing_id'], $params, $apiLogin, $apiAuth);
			else  if($action=='updateBillingProfile')
				$profile = $anet->updatePaymentProfile($uid, $a['billing_id'], $a['billing_id2'], $params, $apiLogin, $apiAuth);
			
			if($profile['success'] && $action=='addNewBillingProfile'){
				echo '<h1>Profile Successfully Created</h1>';
				//check to see if user has any items that should be charged right now
				if($charges = $reseller->chargesDueOnSetup($uid)){
					$chargeAmount = $charges['totalAmount'];
					$monthly = $charges['monthlyCharge']['amount'];
					$pid = $charges['productID'];
					echo 'Attempting to bill you for $'.$chargeAmount.'...<br />';
					
					//attempt to charge new payment profile
					//no need to include rid, pid, etc because charge isn't logged
					$chargeUser = $anet->chargeCIM($uid, NULL, $pid, $chargeAmount, 'initial', 0, NULL, $apiLogin, $apiAuth);
					if($chargeUser['success']){
						//charge was successful, schedule next month's
						$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
						$nextSched = $reseller->scheduleTransaction(NULL, $uid, $rid, $pid, 'monthlySubscription', 
															$monthly, NULL, 'Scheduled monthly billing for '.
															date('F', strtotime($nextBillDate)), $nextBillDate, 1);
						//log reseller credits:
						//usually this is done by the chargeCIM() function but this is a special case
						if($anetType=='6qube'){
							//log first month
							$reseller->logCredit($rid, $uid, $pid, 'initialSubscription', $monthly);
							//if there was a setup fee
							if($charges['setupCharge']['amount']>0)
								$reseller->logCredit($rid, $uid, $pid, 'initialSetupFees', 
												 $charges['setupCharge']['amount'], $charges['setupCharge']['id']);
						}
												
						//also update the table rows
						$query = "UPDATE resellers_billing 
								SET transaction_id = '".$chargeUser['message']."', method_identifier = '".$profile['payment_profile_id']."', processor = '".$anetType."', success = 1, charge_on_setup = 0, timestamp = now()   
								WHERE user_id = '".$uid."' AND success = 0 AND charge_on_setup = 1";
						$reseller->query($query);
						//notify user
						echo '<b>Billing was successful!</b><br />Thank you for your payment, your account is now active.<br />Monthly billing has been scheduled to occur on the same day each month following.<br />';
					} //end if($chargeUser['success'])
					else {
						//billing failed - reschedule for 2 days
						$nextAttempt = date('Y-m-d H:i:s', strtotime('+2 days'));
						$query = "UPDATE resellers_billing 
								SET method_identifier = '".$chargeUser['profile_id']."', processor = '".$anetType."',
									success = 0, sched = 1, sched_date = '".$nextAttempt."',  sched_reattempt = 1, 
									charge_on_setup = 0, timestamp = now() 
								WHERE user_id = '".$uid."' AND success = 0 AND charge_on_setup = 1";
						$reseller->query($query);
						//notify user
						echo '<b>Sorry, there was a billing error:</b><br />'.$chargeUser['message'].'<br /><br />A reattempt has been scheduled to occur in 2 days.  Please correct the problem before then or contact support if you need assistance.<br />';
					}
				} //end if($charges = $reseller->chargesDueOnSetup($uid))
				echo '<h2>You may now close this window</h2>';
			} //end if($profile['success'] && $action=='addNewBillingProfile')
			else if($profile['success'] && $action=='updateBillingProfile'){
				//if user was updating their profile and it was successful
				echo '<h1>Profile Successfully Updated</h1><br />
					 <h2>You may now close this window</h2>';
			}
			else {
				echo '<h1>An Error Occured</h1>';
				echo $profile['message'];
				echo '<br /><br /><b>Please try again or contact support if the problem persists</b>';
			}
		} //end if($uid && $a['billing_id'])
		else {
			echo '<h1>An Error Occured</h1>';
			echo 'Please try again or contact support.<br />';
			echo 'No information was stored and your card was not charged.';
		}
	} //end else if($action == "addNewBillingProfile" || $action == "updateBillingProfile")
	
	//***************************************************************************
	//if a reseller is trying to get a user's campaigns for transfer
	//***************************************************************************//
	else if($action == "getCampaignsForTransfer"){
		$uid = is_numeric($_GET['uid']) ? $_GET['uid'] : '';
		if($uid){
			//prepare output
			$output = '<strong>Choose one of this user\'s campaigns to transfer the items to:</strong><br />';
			$output .= '<select name="newCid" class="no-upload uniformselect" title="select">';
			//get user's campaigns
			$query = "SELECT id, name FROM campaigns WHERE user_id = '".$uid."' AND (SELECT parent_id FROM users WHERE id = '".$uid."') = '".$_SESSION['login_uid']."'";
			if($result = $reseller->query($query)){
				while($row = $reseller->fetchArray($result, NULL, 1)){
					$output .= '<option value="'.$row['id'].'">#'.$row['id'].' - '.$row['name'].'</option>';
				}
			}
			else $output .= '<option value="">No campaigns found for this user.</option>';
			$output .= '</select>';
			
			echo $output;
		}
	}
	
	//***************************************************************************
	//if a reseller is trying to transfer items to another user
	//***************************************************************************//
	else if($action == "transferItems" && $_POST){
		$cid = is_numeric($_POST['cid']) ? $_POST['cid'] : '';
		$newCid = is_numeric($_POST['newCid']) ? $_POST['newCid'] : '';
		$rid = $_SESSION['login_uid'];
		$uid = is_numeric($_POST['uid']) ? $_POST['uid'] : '';
		//if(!$uid) $uid = is_numeric($_POST['uid2']) ? $_POST['uid2'] : '';
		$i = 1;
		$baseDir = QUBEROOT . 'users';
		$hubsBaseDir = QUBEROOT . 'hubs/domains/';
		$errors = array();
		$status = array();
		//validate first
		if($cid && $newCid && $rid && $uid){
		////////////////////////////////////////////////////////////////////
		//LOOP THROUGH EACH ITEM									 //
		////////////////////////////////////////////////////////////////////
		foreach($_POST as $key=>$value){ //loop through each 'itemx' and transfer it
			if(substr($key, 0, 4)=='item'){
				$na = explode('-', $value);
				$type = $na[0];
				$typeID = $na[1];
				if($type=='hub') $dispType = 'HUB';
				else if($type=='blogs') $dispType = 'Blog';
				else if($type=='directory') $dispType = 'Directory Listing';
				else if($type=='articles') $dispType = 'Article';
				else if($type=='press_release') $dispType = 'Press Release';
				else if($type=='lead_forms') $dispType = 'Custom Form';
				else $invalidType = true;
				//first make sure the item belongs to the reseller or one of their users
				$query = "SELECT user_id FROM `".$type."` WHERE (user_parent_id = '".$rid."') AND id = '".$typeID."'";
				$a = $reseller->queryFetch($query);
				if($a['user_id']){
					$oldUID = $a['user_id'];
					//make sure the transfer-to user is the reseller's
					$query = "SELECT count(id) FROM users WHERE parent_id = '".$rid."' AND id = '".$uid."'";
					$b = $reseller->queryFetch($query);
					if($b['count(id)']){
						if($invalidType){
							$status[$i] = 3;
							$errors[$i] = 'Invalid transfer type: '.$type.' #'.$typeID;
						}
						else if($typeID==$_SESSION['main_site_id']){
							$status[$i] = 3;
							$errors[$i] = 'Cannot transfer Main Site to another user';
						}
						else $continue = true;
					}
					else {
						$continue = false;
						$status[$i] = 3;
						$errors[$i] = $dispType.' # '.$typeID.': Invalid UID - Does not belong to user';
					}
				}
				else {
					$continue = false;
					$status[$i] = 3;
					$errors[$i] = $dispType.' # '.$typeID.': Invalid item ID - Does not belong to user';
				}
				
				if($continue){
					//set type vars
					if($type=='hub'){
						//check to see if a domain has been set
						$query = "SELECT domain, pages_version FROM hub WHERE id = '".$typeID."'";
						$hubInfo = $reseller->queryFetch($query);
						if($hubInfo['domain']) $extraUpdate = ", user_id_created = '".$oldUID."'";
						else {
							//hub doesn't have a domain yet.  set user_id_created to new user and create folder
							$extraUpdate = ", user_id_created = '".$uid."'";
							SystemDataAccess::getHubStorage($reseller_ID, $hub_ID, true);
							/*
							if(!@is_dir($hubsBaseDir.$uid.'/'.$typeID)){
								@mkdir($hubsBaseDir.$uid.'/'.$typeID, 0777);
								@copy($hubsBaseDir.'index-copy.php', $hubsBaseDir.$uid.'/'.$typeID.'/index.php');
								@copy($hubsBaseDir.'htaccess.txt', $hubsBaseDir.$uid.'/'.$typeID.'/.htaccess');
								@copy($hubsBaseDir.'process-custom-form.copy.php', $hubsBaseDir.$uid.'/'.$typeID.'/process-custom-form.php');
							}
							 * 
							 */
						}
						
						$imgFields = 'logo, favicon, bg_img';	/*, pic_one, pic_two, pic_three, pic_four, 
									pic_five, pic_six, pic_seven, pic_eight, pic_nine, pic_ten';	
						 * 
						 // */
						$subTable = ($hubInfo['pages_version']==2) ? 'hub_page2' : 'hub_page';
						$subTableImgField = "page_photo";
						$subTableParentIDField = 'hub_id';
					}
					else if($type=='blogs'){
						$extraUpdate = ", user_id_created = '".$oldUID."'";
						$imgFields = 'logo, bg_img';
						$subTable = 'blog_post';
						$subTableImgField = "photo";
						$subTableParentIDField = 'blog_id';
					}
					else if($type=='directory'){
						$imgFields = 'photo, pic_one, pic_two, pic_three, pic_four, pic_five, pic_six';
					}
					else if($type=='articles' || $type=='press_release'){
						$imgFields = 'thumb_id';
					}
					////////////////////////////////////////////////////////////////////
					//UPDATE ITEM MAIN TABLE									 //
					////////////////////////////////////////////////////////////////////
					$query = "UPDATE `".$type."` SET user_id = '".$uid."', cid = '".$newCid."' 
							".$extraUpdate." WHERE id = '".$typeID."' LIMIT 1";
					if($reseller->query($query)){
						/////////////////////////////////////////////////////////////
						//HUB PAGES / BLOG POSTS							    //
						/////////////////////////////////////////////////////////////
						if($type=='hub' || $type=='blogs'){
							$query = "UPDATE `".$subTable."` SET user_id = '".$uid."' 
									WHERE `".$subTableParentIDField."` = '".$typeID."'";
							if($reseller->query($query)){
								//copy sub item image(s) now
								$query = "SELECT `".$subTableImgField."` FROM `".$subTable."` 
										WHERE `".$subTableParentIDField."` = '".$typeID."'";
								if($subImages = $reseller->queryFetch($query, NULL, 1)){
									$subFolder = $subTable;
									if($subTable=='hub_page2') $subFolder = 'hub_page';
									$baseDirOld = $baseDir.'/'.$oldUID.'/'.$subFolder.'/';
									$baseDirNew = $baseDir.'/'.$uid.'/'.$subFolder.'/';
									$imgName = $subImages[$subTableImgField];
									//validate filename
									if(is_file($baseDirOld.$imgName)){
										if(is_file($baseDirNew.$imgName)){
											$copyImgName = $reseller->uniqueFilename($imgName);
										}
										else $copyImgName = $imgName;
										
										if(copy($baseDirOld.$imgName, $baseDirNew.$copyImgName)){
											//update item with image filename (in case it was changed)
											$query = "UPDATE `".$subTable."` 
													SET `".$subTableImgField."` = '".$copyImgName."' 
													WHERE `".$subTableParentIDField."` = '".$typeID."' LIMIT 1";
											$reseller->query($query);
										} 
										else $errors[$i] .= 
										'Error copying one or more sub-images for '.$dispType.' #'.$typeID.'<br />';
										$copyImgName = NULL;
									}
									$subFolder = $subImages = $imgName = $baseDirNew = $baseDirOld = NULL;
								} //end if($subImages = $reseller->queryFetch($query, NULL, 1))
							} //end if($reseller->query($query)) [optional second update query - hub pages/blog posts]
							else $errors[$i] .= 'Error transfering '.$dispType.' #'.$typeID.' - One or more pages<br />';
						} //end if($type=='hub' || $type=='blogs')
						////////////////////////////////////////////////////////////////////
						//COPY MAIN IMAGES FOR HUBS, BLOGS, DIRECTORY, ARTICLES, PRESS	 //
						////////////////////////////////////////////////////////////////////
						if($type!='lead_forms'){
							if($type == 'hub')
							{
								$da	=	new HubDataAccess();
								$pics	=	array();
								$da->getPics($typeID, $pics);
							}
							$query = "SELECT ".$imgFields." FROM `".$type."` WHERE id = '".$typeID."'";
							if($mainImages = $reseller->queryFetch($query, NULL, 1)){
								$baseDirOld = $baseDir.'/'.$oldUID.'/'.$type.'/';
								$baseDirNew = $baseDir.'/'.$uid.'/'.$type.'/';
								$imgCopyErrs = 0;
								foreach($mainImages as $imgType=>$imgName){
									//validate image name
									if(is_file($baseDirOld.$imgName)){
										if(is_file($baseDirNew.$imgName)){
											$copyImgName = $reseller->uniqueFilename($imgName);
										}
										else $copyImgName = $imgName;
										
										if(copy($baseDirOld.$imgName, $baseDirNew.$copyImgName)){
											//update item with image filename (in case it was changed)
											$query = "UPDATE `".$type."` SET `".$imgType."` = '".$copyImgName."' 
													WHERE id = '".$typeID."' LIMIT 1";
											$reseller->query($query);
										}
										else $imgCopyErrs++;
										$copyImgName = NULL;
									}
								} //end foreach loop
								if($imgCopyErrs){
									$errors[$i] .= 'Error copying one or more images for '.$dispType.' #'.$typeID.'<br />';
								}
								$mainImages = $imgCopyErrs = $imgType = $imgName = $copyImgName = $baseDirNew = $baseDirOld = NULL;
							} //end if($mainImages = $reseller->queryFetch($query, NULL, 1))
						} //end if($type!='lead_forms')
						////////////////////////////////////////////////////////////////////
						//TRANSFER AUTORESPONDERS, PROSPECTS						 //
						////////////////////////////////////////////////////////////////////
						$query = "UPDATE auto_responders SET user_id = '".$uid."', cid = '".$newCid."' 
								WHERE table_name = '".$type."' AND table_id = '".$typeID."'";
						if(!$reseller->query($query)){
							$errors[$i] .= 'Error transferring '.$dispType.' #'.$typeID.' - Autoresponders<br />';
						}
						$query = "UPDATE contact_form SET user_id = '".$uid."' 
								WHERE type = '".$type."' AND type_id = '".$typeID."'";
						if(!$reseller->query($query)){
							$errors[$i] .= 'Error transferring '.$dispType.' #'.$typeID.' - Prospects<br />';
						}
						//add results to status array
						if(!$errors[$i]) $status[$i] = 1; //all good
						else $status[$i] = 2; //continue with errors
					} //end if($reseller->query($query)) [first update query]
					else {
						$status[$i] = 3;
						$errors[$i] .= 'Error transfering '.$dispType.' #'.$typeID.'<br />';
					}
				} //end if($continue)
				else {
					$status[$i] = 3;
					$errors[$i] = 'Error validating submission.<br />';
				}
			} //end if(substr($key, 0, 4)=='item')
			$i++;
			$na = $type = $typeID = $dispType = $hubInfo = $invalidType = $a = $oldUID = $b = $continue = 
			$extraUpdate = $imgFields = $subTable = $subFolder = $subTableImgField = $subTableParentIDField = NULL;
		} //end foreach($_POST as $key=>$value)
		} //end first validation
		else echo 'cid: '.$cid.', newCid: '.$newCid.', rid: '.$rid.', uid: '.$uid;
		////////////////////////////////////////////////////////////////////
		//LOOP IS OVER, FIGURE OUT RESULTS TO DISPLAY				 //
		////////////////////////////////////////////////////////////////////
		if(!$errors){
			$return['action'] = 'popSuccessRedir';
			$return['url'] = 'inc/campaigns.php';
		}
		else {
			if($status){
				$numFails = 0;
				foreach($status as $itemNum=>$statusCode){
					if($statusCode==2){
						$returnMessage .= '<strong>Succeeded but with errors</strong>:<br />'.$errors[$itemNum].'<br /><br />';
					}
					else if($statusCode==3){
						$returnMessage .= '<strong>Failed</strong>:<br />'.$errors[$itemNum];
						$numFails++;
					}
				}
				if($numFails==($i-1)){
					$return['action'] = 'errors';
					$return['errors'] = '<br /><br /><strong>All item transfers failed.</strong>'.$returnMessage;
				}
				else {
					$return['action'] = 'errors';
					$return['errors'] = $returnMessage;
				}
			}
			else {
				$return['action'] = 'errors';
				$return['errors'] = '<strong>Unknown error.  Check if items were transferred.</strong>';
			}
		}
		//output results
		echo json_encode($return);
	} //end else if($action == "transferItems" && $_POST)
	//***************************************************************************
	//if an admin or reseller is searching for users
	//***************************************************************************//
	else if($action=='searchUsers'){
		$limit = is_numeric($_GET['limit']) ? $_GET['limit'] : 0;
		$parent_id = is_numeric($_GET['parent_id']) ? $_GET['parent_id'] : '';
		$search = $reseller->sanitizeInput($_GET['search']);
		
		echo '<div id="currOffset" title="'.$offset.'" style="display:none;"></div>';
		$reseller->displayUsers('admin-users.php', $parent_id, $limit, NULL, NULL, NULL, NULL, $search);
	}
	//***************************************************************************
	//if an admin or reseller editing a user class's theme access
	//***************************************************************************//
	else if($action=='updateClassThemeAccess'){
		$classID = is_numeric($_POST['classID']) ? $_POST['classID'] : 0;
		$themeID = is_numeric($_POST['themeID']) ? $_POST['themeID'] : 0;
		$disabled = $_POST['disabled'] ? 1 : 0;
		$response['success'] = 0;
		
		if($classID && $themeID){
			//first get the colon-separated array from the table
			$query = "SELECT disabled_themes FROM user_class WHERE id = '".$classID."'";
			$a = $reseller->queryFetch($query);
			//now go through and add or remove the theme
			if($a['disabled_themes']){
				$disabledThemes = explode('::', $a['disabled_themes']);
				$removed = false;
				foreach($disabledThemes as $key=>$value){
					if($value == $themeID){
						$disabledThemes[$key] = NULL;
						$removed = true;
					}
				}
				if(!$removed) $disabledThemes[] = $themeID;
			}
			else $disabledThemes = array($themeID);
			
			//build string
			if($disabledThemes){
				$string = '';
				foreach($disabledThemes as $key=>$value){
					if($value) $string .= $value.'::';
				}
				//trim trailing '::'
				$string = substr($string, 0, -2);
			}
			
			//update table row
			$query = "UPDATE user_class SET disabled_themes = '".$string."' WHERE id = '".$classID."'";
			if($reseller->query($query)){
				$response['success'] = 1;
			}
			else $response['message'] = 'Error updating DB';
		}
		else $response['message'] = 'Validation error.  ClassID: '.$classID.', themeID: '.$themeID;
		
		echo json_encode($response);
	}
}
?>
