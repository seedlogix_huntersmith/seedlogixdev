<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//authorize
	require_once('./inc/auth.php');

	require_once('./inc/press.class.php');
	$press = new Press();
			
	$id = $_GET['id'];
	$action = "edit";
			
	$query = "SELECT * FROM press_release WHERE id = '$id' AND user_id = '$_SESSION[user]'";
	$result = $press->query($query);
	
	
	//header
	require_once('./admin/inc/header.php');
	//content		
	if(!$press->numRows($result))
	{
		echo '<p class="error">You do not own the press release you are trying to edit.</p>';
	}
	else
	{
		$row = $press->fetchArray($result);
		include('./inc/forms/add-press-release.php');
	}
	
	//footer
	require_once('./admin/inc/footer.php');		
?>