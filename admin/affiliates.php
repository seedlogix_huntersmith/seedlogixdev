<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/affiliates.class.php');
	$affiliates = new Affiliates();
	
	//offset
	if($_GET['offset'] && is_numeric($_GET['offset'])) $offset = $_GET['offset'];
	else $offset = 0;
	
	$affiliatesView = $affiliates->getAffiliates($_SESSION['user_id']);
?>

<h1>Affiliates</h1>
<p></p><br />
<? if($affiliatesView && is_array($affiliatesView)){
	echo '<div id="message_cnt" style="display:none;"></div>';
	echo '<div id="dash" title="'.$noteid.'"><div id="list_notes" title="notes" class="dash-container"><div class="container">';
	
	foreach($affiliatesView as $id=>$affiliate){
		
		echo '<ul class="prospects">
					<li class="name3"><small>#'.$affiliate['user_id'].'</small> '.$affiliate['firstname'].' '.$affiliate['lastname'].' </li>
					<li class="email2"><a href="'.$email.'" class="email-prospect" id="'.$affiliate['user_id'].'" name="'.$affiliate['company'].'" rel="user">'.$email.'</a></li>
					<div class="iconWrap">
								<li class="details"><a href="#" title="View Details" class="user-details" rel="'.$affiliate['user_id'].'"><img src="img/v3/view-details.png" border="0" /></a></li>
					</div>
					<li class="comment" id="user-details-'.$affiliate['user_id'].'">
						<p>Company: '.$affiliate['company'].'</p>
						<p>Phone: '.$affiliate['phone'].'</p>
						<p>Address:<br />'.$affiliate['address'];
				if($row['address2']) echo '<br />'.$affiliate['address2'];
				echo 	'</p>
						<p>'.$affiliate['city'].', '.$affiliate['state'].' '.$affiliate['zip'].'</p>
						<p><a href="#" class="user-details" rel="'.$affiliate['user_id'].'"><b>Close</b></a></p>
					</li>
					</ul>';
	}
} else { ?>
<div id="message_cnt" style="display:none;"></div>
<div id="dash">
  <div id="list_notes" title="notes" class="dash-container">
    <div class="container"> No Affiliates yet. </div>
  </div>
</div>
<? } ?>
<br style="clear:both;" />
<br />

