<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/directory.class.php');
	$directory = new Dir();
	
	$newResponder = $directory->validateText($_POST['name'], 'Autoresponder Name');
	$tableId = $_POST['table_id'];
	$tableName = $_POST['table_name'];
	$type = $_POST['type'];
	
	if($_SESSION['reseller_client']) $parent_id = $_SESSION['parent_id'];
	else if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	
	if($directory->foundErrors()){ //check if there were validation errors
		//$error = $directory->listErrors('<br />'); //list the errors
	}
	else { //if no errors
		//make sure user hasn't the autoresponder limit for this item type/campaign
		$numResponders = $directory->getAutoResponders(NULL, $tableName, $_SESSION['user_id'], NULL, 
											  NULL, 1, $_SESSION['campaign_id']);
		if($_SESSION['user_limits']['responder_limit']!=-1){
			if($numResponders < $_SESSION['user_limits']['responder_limit']) $continue  = true;
		}
		else $continue = true;
		
		if($continue){
			$message = $directory->addNewAutoResponder2($_SESSION['user_id'], $tableId, $newResponder, 
											   $tableName, $parent_id, $_SESSION['campaign_id'], $type);
			
			$response['success']['id'] = '1';
			$response['success']['container'] = '#fullpage_autoresponders .container';
			$response['success']['message'] = $message;
			$response['success']['action'] = "replace";
		}
	}

	echo json_encode($response);
	
?>