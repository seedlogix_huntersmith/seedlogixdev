<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize Access to page
require_once('inc/auth.php');
//iniate the page class
require_once('inc/hub.class.php');
$hub = new Hub();
$newHub = $hub->validateText($_POST['name'], 'Hub Name'); //validate directory name
$multi_user = 0;
if($_POST['req_theme_type']==4){
	$req_type = $hub->validateNumber($_POST['req_theme_type']);
	$cid = 99999;
}
else if($_POST['multi_user']==1){
	$multi_user = 1;
	$cid = 99999;
}
else $cid = $_SESSION['campaign_id'];
$parent_id = $_SESSION['parent_id'] ? $_SESSION['parent_id'] : 0;

if($hub->foundErrors()){ //check if there were validation errors
	//$error = $hub->listErrors('<br />'); //list the errors
}
else { //if no errors
	if(!$_SESSION['reseller'] && !$_SESSION['admin']){ //skip the following if a super user
		//make sure user hasn't hit any campaign limits for hubs
		$numHubs = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 1); //main
		$numHubs += $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 4); //multi-user
		$numLandings = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 2); //landing
		$numSocial = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 5); //social
		//check if unlimited hub limit
		if($_SESSION['user_limits']['hub_limit']!=-1){
			//continue if number of hubs in campaign is less than the limit
			if($numHubs < $_SESSION['user_limits']['hub_limit']) $continue = true;
			else {
				//otherwise continue but require the new hub to be a landing page or social.
				if(
				   (($_SESSION['user_limits']['landing_limit']==-1) || ($numLandings < $_SESSION['user_limits']['landing_limit'])) 
				   &&
				   (($_SESSION['user_limits']['social_limit']==-1) || ($numSocial < $_SESSION['user_limits']['social_limit']))
				  ){
					//user hasn't hit any social or landing limits - allow both for hub
					$continue = true;
					$req_type = '2,5';
				}
				else if($numSocial < $_SESSION['user_limits']['social_limit']){
					//user hasn't hit social limit
					$continue = true;
					$req_type = 5;
				}
				else if($numSocial < $_SESSION['user_limits']['landing_limit']){
					//user hasn't hit landing limit
					$continue = true;
					$req_type = 2;
				}
				else $continue = false;
			}
		}
		else $continue = true; //unlimited (-1)
	}
	else $continue = true;
	
	if($continue){
		if($multi_user){
			$message = $hub->addNewHub($_SESSION['user_id'], $newHub, $cid, NULL, NULL, NULL, $parent_id, 1);
		}
		else {
			$message = $hub->addNewHub($_SESSION['user_id'], $newHub, $cid, 5, NULL, NULL, $parent_id);
		}
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_social .container';
		$response['success']['message'] = $message;
		//$response['success']['action'] = 'load';
		//$response['success']['page'] = 'social_media.php';
		if($_POST['req_theme_type']==4 || $multi_user) $response['success']['page'] .= '?multi_user=1';
	}
}
echo json_encode($response);
?>