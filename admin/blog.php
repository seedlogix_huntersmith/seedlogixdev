<?php
	//start session
	session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	require_once('./inc/blogs.class.php');
	$blog = new Blog();
	
	$blog_count = 0;
	//$blog_limit = $blog->getLimits($_SESSION['user_id'], "blog");
	$blog_limit = $_SESSION['user_limits']['blog_limit'];
	if($blog_limit==-1 || $blog_limit==NULL) $remaining = "Unlimited";
	
	$current_id = is_numeric($_GET['id']) ? $_GET['id'] : 0;
	//content
	echo '<div id="message_cnt"></div><br /><h1>My Blogs</h1>';
	/*******************************************
		If they have clicked a blog or post
	/*******************************************/
	if($_GET){
		if($_GET['form']) $form = $_GET['form'];
		else $form = 'settings';
		$_SESSION['current_id'] = $_GET['id'];
		//if($form=="post"){ $blank = array(); $blog->addNewPost($_SESSION['current_id'], $blank); }
		include('./inc/forms/add_blog_'.$form.'.php');
	}
	else {
		/*******************************************
			Start output of blogs and post
		/*******************************************/
		$result = $blog->getBlogs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], NULL, 1);
		$blog_count = $blog->getBlogRows();
		if(!$remaining) $remaining = $blog_limit - $blog_count;
		$remaining = $remaining<0 ? 0 : $remaining;
		
		echo '<div id="createDir">';
		if(!$_SESSION['theme']){
			echo '<div class="watch-video"><ul><li><a href="#campVideo" class="watch_video trainingvideo" >Watch Video</a></li></ul></div>';
		}
		echo '<form method="post" class="ajax" action="add-blog.php"><input type="text" name="name" ';
		if(!$remaining && ($blog_limit==0 || (($blog_limit-$blog_count)<=0))) echo 'readonly';
		echo '/><input type="submit" value="Create New Blog" name="submit" /></form></div><div class="clear"></div>';
		if(!$_SESSION['theme']){
			echo '<div style="display: none;"><div id="campVideo" style="width:640px;height:480px;overflow:auto;"><a href="http://6qube.com/videos/training/blogs-overview.flv" style="display:block;width:640px;height:480px"id="player"> </a><script>flowplayer("player", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");</script></div></div>';
		}
		
		if($blog_count){ //checks to see if the user has any blogs
			//start dashboard
			echo '<div id="dash"><div id="fullpage_blog" class="dash-container"><div class="container">';
			while($row = $blog->fetchArray($result)){ //if so loop through and display links to each blog
				$tracking = $row['tracking_id'];
				$cid = $row['cid'];
				if(!$tracking || $tracking==0) $tracking = 26;
				if($_SESSION['reseller'] || $_SESSION['reseller_client']) $rid = "&rid=".$_SESSION['parent_id'];
				echo 
				'<div class="item">
					<div class="icons">
						<a href="http://'.$_SESSION['main_site'].'/blog-preview.php?uid='.$_SESSION['user_id'].'&bid='.$row['id'].$rid.'" target="_new" class="view"><span>View</span></a>
						<a href="blog.php?form=settings&id='.$row['id'].'" class="edit"><span>Edit</span></a>
						<a href="#" class="delete" rel="table=blogs&id='.$row['id'].'"><span>Delete</span></a>
					</div>';
				echo '<img src="'.$blog->ANAL_getGraphImg('blog', $row['id'], $tracking, $_SESSION['user_id']).'" class="graph trigger" />';
				echo '<a href="blog.php?form=settings&id='.$row['id'].'">'.$row['blog_title'].'</a>
				</div>';
				//get results for specified blog ID with a limit of 3
				$post_results = $blog->getBlogPost($row['id'], 3);
				//if specified blog has any post associated with it
				if($blog->getPostRows()){
					$postLinkBase = $row['posts_v2'] ? 'blog.php?form=post2&id='.$row['id'].'&postID=' : 'blog.php?form=editPost&id=';
					//loop through and out the blogs post (limits 3)
					while($post_row = $blog->fetchArray($post_results)){
						echo '<div class="item2">
								<div class="icons">
									<a href="http://'.$_SESSION['main_site'].'/blog-preview.php?uid='.$_SESSION['user_id'].'&bid='.$post_row['id'].$rid.'" target="_new" class="view"><span>View</span></a>
									<a href="'.$postLinkBase.$post_row['id'].'" class="edit"><span>Edit</span></a>
									<a href="#" class="delete" rel="table=blog_post&id='.$post_row['id'].'"><span>Delete</span></a>
								</div>';
								
								echo '<img src="img/blog-post.png" class="icon" />';
			
								echo '<a href="'.$postLinkBase.$post_row['id'].'">'.$post_row['post_title'].'</a>
							</div>';
					
					} //end while($post_row = $blog->fetchArray($post_results)) [post loop]
					$postLinkBase = NULL;
				} //end if($blog->getPostRows())
			} //end while($row = $blog->fetchArray($result)) [blog loop]
			//end dashboard
			echo '<br />You have <strong>'.($remaining).'</strong> Blogs remaining.';
			if(!$_SESSION['theme']){
				if(($blog_limit-$blog_count)<=0 && $blog_limit!=-1 && $blog_limit!="Unlimited") echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
			}
			echo '</div></div></div>';
			if($_SESSION['reseller']){
				echo '<br /><br />
					<a href="reseller-transfer-items.php?mode=section&mode2=blogs&cid='.$cid.'" class="dflt-link">Transfer one or more blogs to another user</a>';
			}
		}	
		else {
			if($blog_limit==-1 || $blog_limit==NULL) $blog_limit = "Unlimited";
			echo '<div id="dash"><div id="fullpage_blog" class="dash-container"><div class="container">';
			if($blog_limit==0 && $blog_limit!="Unlimited"){
				echo 'You have <strong>0</strong> Blogs remaining.';
				if(!$_SESSION['theme']){
					echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else echo 'Use the form above to create a new Blog.  You have <strong>'.$blog_limit.'</strong> remaining.';
			echo '</div></div></div>';
		}
	}
?>
<? if(!$_SESSION['theme']){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});	
});
</script>
<? } ?>