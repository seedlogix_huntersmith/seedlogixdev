<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//DB connections
	require_once('inc/db_connector.php');
	$db = new DbConnector();
	
	isset($_POST['id']) ? $id = $_POST['id'] : $id = "";
	isset($_POST['table']) ? $table = $_POST['table'] : $table = "";
	
	if($id && $table){
//		OLD trash table setup (commented out 5/30/2012 by Reese):
		//$query = "DELETE FROM trash WHERE table_id = '".$id."' AND table_name = '".$table."'";
		$query = "UPDATE `".$table."` SET `trashed` = '0000-00-00' WHERE `id` = '".$id."' LIMIT 1";
		$result = $db->query($query);
		if($result){
			$response['error'] = 0;
		} else {
			$response['error'] = 1;
			$response['message'] = "Could not delete from trash DB table";
		}
		
		//if user is a reseller restoring a MU hub page
		if(($table=='hub_page' || $table=='hub_page2') && $_SESSION['reseller']){
			$query = "SELECT hub_id FROM `".$table."` WHERE id = '".$id."'";
			$pageInfo = $db->queryFetch($query);
			$query = "SELECT req_theme_type, multi_user FROM hub WHERE id = '".$pageInfo['hub_id']."'";
			$a = $db->queryFetch($query);
			if($a['req_theme_type']==4 || $a['multi_user']){
				$query = "UPDATE `".$table."` SET `trashed` = '0000-00-00' WHERE `page_parent_id` = '".$id."'";
				$db->query($query);
//				OLD trash table setup (commented out 5/30/2012 by Reese):
//				//get the id of all pages that were trashed with the original
//				$query = "SELECT id FROM hub_page WHERE page_parent_id = '".$id."' 
//						AND user_parent_id = '".$_SESSION['login_uid']."'";
//				$b = $db->query($query);
//				if($b && mysqli_num_rows($b)){
//					while($c = $db->fetchArray($b)){
//						//loop through and remove the trash items
//						$query = "DELETE FROM trash WHERE table_id = '".$c['id']."' AND table_name = 'hub_page'";
//						$db->query($query);
//					}
//				}
			}
		}
	} else {
		$response['error'] = 1;
		$response['message'] = "Insufficient information";
	}
	
	echo json_encode($response);	
?>