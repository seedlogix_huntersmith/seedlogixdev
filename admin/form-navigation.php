<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//authorize
	//require_once('./inc/auth.php');
	//header
	require_once('./admin/inc/header.php');
	//content
?>
	<style>
	#top_nav { height:56px; overflow:hidden; }
	#side_nav { float:left; width:150px; border-right:1px solid #bababa; padding-bottom:100px; }
	#side_nav ul { list-style:none; }
	#side_nav ul li { }
	#side_nav ul li a { display:block; padding:8px; background:url('img/side-menu-bg.png') repeat-x; color:#333; text-decoration:none;  }
	#side_nav ul li a:hover { background-position:0 -32px; }
	#form_test { float:left; margin-left:20px; }
	#bottom_nav { clear:left; height:150px; margin-left:170px; }
	</style>
	<div id="side_nav">
		<ul>
			<li><a href="#">Settings</a></li>
			<li><a href="#">SEO</a></li>
			<li><a href="#">Social Networking</a></li>
			<li><a href="#">Photo Gallery</a></li>
		</ul>
	</div>
	<div id="form_test">
		<form class="jquery_form" name="directory">
			<ul>
				<li><label>Website Title</label><input type="text" name="title" value="<?=$row['title']?>" /></li>
				<li><label>Website Keywords</label><input type="text" name="keywords" value="<?=$row['keywords']?>" /></li>
				<li><label>Website Description</label><input type="text" name="description" value="<?=$row['description']?>" /></li>
				<li><label>Keyword #1</label><input type="text" name="keyword_one" value="<?=$row['keyword_one']?>" /></li>
				<li><label>Keyword #1 URL</label><input type="text" name="keyword_one_link" value="<?=$row['keyword_one_link']?>" /></li>
				<li><label>Keyword #2</label><input type="text" name="keyword_two" value="<?=$row['keyword_two']?>" /></li>
				<li><label>Keyword #2 URL</label><input type="text" name="keyword_two_link" value="<?=$row['keyword_two_link']?>" /></li>
				<li><label>Keyword #3</label><input type="text" name="keyword_three" value="<?=$row['keyword_three']?>" /></li>
				<li><label>Keyword #3 URL</label><input type="text" name="keyword_three_link" value="<?=$row['keyword_three_link']?>" /></li>
				<li><label>Keyword #4</label><input type="text" name="keyword_four" value="<?=$row['keyword_four']?>" /></li>
				<li><label>Keyword #4 URL</label><input type="text" name="keyword_four_link" value="<?=$row['keyword_four_link']?>" /></li>
				<li><label>Keyword #5</label><input type="text" name="keyword_five" value="<?=$row['keyword_five']?>" /></li>
				<li><label>Keyword #5 URL</label><input type="text" name="keyword_five_link" value="<?=$row['keyword_five_link']?>" /></li>
				<li><label>Keyword #6</label><input type="text" name="keyword_six" value="<?=$row['keyword_six']?>" /></li>
				<li><label>Keyword #6 URL</label><input type="text" name="keyword_six_link" value="<?=$row['keyword_six_link']?>" /></li>
			</ul>
		</form>
	</div>
	<div id="bottom_nav">
		<img src="img/bottom-menu.png" />
	</div>	
<?
	
	//footer
	require_once('./admin/inc/footer.php');
?>