<?php

/**
 * Response Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ScheduledResponse extends DBObject
{
    public $q_id = '', $contactdata_id = NULL, $responder_id, $delivered_date = '', $schedule_date = array(0, 0, 0, 0);
    
    protected $_PR_KEY  =   'q_id';
    private $_delivery_offset = ''; // expression
        
    protected function getTimeOffsetArrayDescr()
    {
        static $bindings = array('hours' => 0,       // the index in the schedule_date value array
                            'days' => 1,
                            'weeks' => 2,
                            'months' => 3);
        
        return $bindings;
    }
    
    function setScheduledTime($quantity, $unit_type)
    {
        $this->schedule_date = array(0, 0, 0, 0);
        if($quantity <= 0) return;
        
        $sched_mode = $this->getTimeOffsetArrayDescr();
        
        $value_index = array_key_exists($unit_type, $sched_mode) ? $sched_mode[$unit_type]  : 0;
        
        $this->schedule_date[$value_index] = $quantity;
//        $this->schedule_date = array($type == 'HOUR' ? $quantity : 0, $type == 'DAY' ? $quantity : 0);
    }
    
    function _getInsertValues() {
        $arr = parent::_getInsertValues();
        foreach($this->getTimeOffsetArrayDescr() as $unit => $valueindex)
        {
            $arr[$unit] = $this->schedule_date[$valueindex]*1;
        }
        $arr['isInstantResponse'] = $this->isInstantResponse();
        
        unset($arr['schedule_date'], $arr['delivered_date']);  // use isInstantResponse to calculate delivered date (could be NOW)
//        deb($arr);
        return $arr;
    }
    
    function isInstantResponse()
    {
        return array_sum($this->schedule_date) == 0 ? 1 : 0;
    }
    
    
    function _getColumnPlaceholder_none($c, $purpose = self::FOR_UPDATE) {
        if($c   ==  'schedule_date')
            return 'NOW() + INTERVAL :hours HOUR + INTERVAL :days DAY + INTERVAL :weeks WEEK + INTERVAL :months MONTH';
        
        if($c   ==  'delivered_date')
            return 'IF(:isInstantResponse, NOW(), 0)';
        
        return parent::_getColumnPlaceholder($c, $purpose);
    }
    
    function __getColumns() {
        static $c   =   array('contactdata_id', 'responder_id', 'delivered_date', 'schedule_date');
        
        return $c;
    }
    
    function __construct() {
        $this->_defaults['schedule_date']   =   'NOW() + INTERVAL :hours HOUR + INTERVAL :days DAY + INTERVAL :weeks WEEK + INTERVAL :months MONTH';
        $this->_defaults['delivered_date']   =   'IF(:isInstantResponse, NOW(), 0)';        
    }
    
    function getValue($c, $p, &$array) {
        if($p   ==  self::FOR_INSERT)
        {
            if($c   ==  'delivered_date')
            {
                $array[':isInstantResponse'] =   $this->isInstantResponse();
                return;
            }
            if($c   ==  'schedule_date')
            {
                // set placeholder values in the array for hours, days, weeks, months based on schedule_date array values.
                foreach($this->getTimeOffsetArrayDescr() as $unit => $valueindex)
                {
                    $array[':' . $unit] = $this->schedule_date[$valueindex]*1;
                    return;
                }                
            }
        }
        
        // process other columns
        parent::getValue($c, $p, $array);
    }
}

#ScheduledResponse::Prepare('ScheduledResponse');