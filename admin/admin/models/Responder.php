<?php

require_once 'Response.php';
require_once QUBEPATH . '../classes/ResponseNotification.php';


/**
 * Responder Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * Table Structure: 
 * id 	int(11)	NO 	PRI 	NULL	auto_increment
user_id 	int(11)	NO 		0	
user_parent_id 	int(11)	NO 		0	
cid 	int(11)	NO 		0	
type 	int(11)	NO 		1	
table_name 	varchar(255)	NO 			
table_id 	int(11)	NO 		0	
name 	text	NO 		NULL	
active 	tinyint(1)	NO 		0	
sched 	int(11)	NO 		0	
sched_mode 	varchar(50)	NO 			
from_field 	varchar(255)	NO 			
subject 	text	NO 		NULL	
body 	text	NO 		NULL	
return_url 	varchar(255)	NO 			
contact 	text	NO 		NULL	
anti_spam 	text	NO 		NULL	
optout_msg 	text	NO 		NULL	
theme 	tinyint(11)	NO 		0	
logo 	varchar(255)	NO 			
bg_color 	varchar(75)	NO 			
links_color 	varchar(75)	NO 			
accent_color 	varchar(75)	NO 			
titles_color 	varchar(75)	NO 			
edit_region1 	blob	NO 		NULL	
call_action 	blob	NO 		NULL	
edit_region2 	blob	NO 		NULL	
 */
class Responder extends DbObject {    
    
    // Sched [ 0 = immediately, else: time interval in $sched_mode units]
    protected $id, $sched,
            $sched_mode,    // hours, days, weeks, months
            $from_field,
            $subject,
            $body
            ;
        
    function _getSelectColumns() {
        return array('id', 'user_id', 'sched', 'sched_mode', 'body', 'subject', 'from_field', );
    }
    
    function _getDeleteClause() {
        return array('id' => $this->id);
    }
    
    
    /**
     *
     * Schedule a Response or send a Response to be sent.
     * 
     * @param ContactForm $form 
     * @return int number of responses successffully sent.
     */
    function TriggerResponse(ResponseTriggerEvent $Trigger)
    {
        $response = $this->createScheduledResponse($Trigger->form);
        $notified = 0;
        if($this->isInstantResponder())
        {
            // the question is do we want to LOG instant responses? 
            // @todo increment user email sent count!
            
            $mail = $this->createNotification($Trigger);
            $sent = $Trigger->mailer->Send($mail);
            if(!$sent)
            {   // try again in 1 hour if it failed
                $response->setScheduledTime(1, 'hours');
            }else
                $notified = 1;
        }
        
        $response->Save();
        return $notified;
    }
    
    function createScheduledResponse(ContactDataModel $form)
    {
        $R = new ScheduledResponse();
        $R->_set('contactdata_id', $form->id);
        $R->_set('responder_id', $this->id);
        $R->setScheduledTime($this->sched, $this->sched_mode);
        return $R;
    }
    
    function createNotification(ResponseTriggerEvent $Trigger)
    {
        $N = new ResponseNotification($Trigger->form, $Trigger->user, $this);
        return $N;
    }
    
    function isInstantResponder()
    {
        return $this->sched == 0;
    }
}


class ActiveResponder extends Responder
{
    function getAliasedSelectClause($alias) {      
        
        $where = new DBWhereExpression(parent::getAliasedSelectClause());
        
        $where->Where('`' . $alias . '`.active = 1');
        
        return $where;
    }
}