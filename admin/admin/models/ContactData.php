<?php

class ContactDataExists extends Exception{}

/**
 * ContactForm Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * columns:
 * 
 * id	int(5)	NO	PRI	NULL	auto_increment
user_id	int(6)	NO	MUL	0	
parent_id	int(11)	NO		0	
type	varchar(5)	NO	MUL		
type_id	int(9)	NO		0	
cid	int(11)	NO		0	
name	varchar(150)	NO			
email	varchar(150)	NO			
phone	varchar(16)	NO			
website	varchar(255)	NO			
comments	text	NO		NULL	
page	varchar(150)	NO			
keyword	varchar(255)	NO			
search_engine	varchar(255)	NO			
responded	tinyint(1)	NO		0	
responses_received	text	NO		NULL	
optout	tinyint(1)	NO		0	
ip	varchar(255)	NO			
timestamp	timestamp	NO		CURRENT_TIMESTAMP	on update CURRENT_TIMESTAMP
created	timestamp	NO	MUL	0000-00-00 00:00:00	
 * 
 */
class ContactData extends DBObject {    
    
    
    public $id, $user_id, $parent_id, $type, $type_id;            
    public $name, $email, $phone, $website, $comments, $page, $keyword;
    public $search_engine, $ip, $created, $spamstatus;
    
    protected $_PR_KEY  =   'id';
    
    function Exists()
    {
        $S = Qube::GetDriver()->QueryObject('ContactData')
                ->Fields('count(*)')->Where('email= ? AND user_id = ? AND website= ? AND comments = ?')->Prepare();
        $S->execute(array($this->email, $this->user_id, $this->website, $this->comments));
        
        return $S->fetchColumn() > 0;
    }
    
    /**
     *
     * @var PDOStatement
     */
//    protected static $insert_statement;   
    
    
    function __getColumns() {
        static $cols    =   array('id', 'user_id', 'type', 'type_id',
            'name', 'email', 'phone', 'website', 'comments', 'page',
            'keyword', 'search_engine', 'ip', 'created', 'spamstatus');
        return $cols;
    }
    
    function _getInsertColumnBinding($column_name) {
        if($column_name == 'created')
        {
            return 'NOW()';
        }
        return parent::_getInsertColumnBinding($column_name);
    }
    

    function _getColumn($col_name, $get_type = 0)
    {
        if($get_type == self::FOR_INSERT && $col_name == 'created')
            throw new Exception();
        
        return parent::_getColumn($col_name, $get_type);
    }    
    /*
    function _getCondition($alias, $purpose = self::FOR_SELECT) {
        return array('id' => $this->id);
    }
    
    /**
     *
     * @param User $user 
     * @return ContactFormNtification
     */
    function Notify(User $user)
    {
        require_once QUBEPATH . '../classes/ContactFormNotification.php';
        return new ContactFormNotification($this, $user);
    }
}


class Lead extends ContactData
{
    function getAliasedSelectClause($alias) {
        $where = new DBWhereExpression(parent::getAliasedSelectClause());
        
        $where->Where('`' . $alias . '`.optout != 1');
        
        return $where;
    }
}

ContactData::Prepare('ContactData');