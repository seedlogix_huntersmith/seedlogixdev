<?php
	session_start();
	if($_SESSION['reseller'] || $_SESSION['reseller_client']) $isReseller = true;
	if($isReseller){
		$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
		$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
		$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
		if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow: #'.$_SESSION['thm_nav-links4'].' 1px 1px;';
		$style3 .= '"';
		
		if($_SESSION['brandoverride']){
			$link1 = '6qube-client-billing.php';
			$link2 = '6qube-billing.php';
		} else {
			$link1 = 'reseller-client-billing.php';
			$link2 = 'reseller-billing.php';
			$link3 = 'billing-subscription.php';	
		}
		$billingSetup = true;
	}
	else {
		$link1 = '6qube-client-billing.php';
		$link2 = '6qube-billing.php';
		if($_SESSION['user_class']!=1 && $_SESSION['user_class']!=178) $billingSetup = true;
	}
?>
<ul class="top">
	<li><a href="<?=$link1?>" class="hover" id="billing" <? if($isReseller) echo $style1; ?>><p>Billing</p></a></li>
	<li class="active">
		<ul class="child">
		<? if($billingSetup){ ?>
			<li><a href="<?=$link1?>" class="child-link" <? if($isReseller) echo $style2; ?>><p>Payment Profile</p></a></li>
			<li><a href="<?=$link2?>" class="child-link subnav-last last" <? if($isReseller) echo $style2; ?>><p>Billing History</p></a></li>
            <? if($_SESSION['reseller']){ ?>
            <li><a href="<?=$link3?>" class="child-link subnav-last last" <? if($isReseller) echo $style2; ?>><p>Billing Subscription</p></a></li>
            <? } ?>
		<? } else { ?>
			<li><a href="<?=$link1?>" class="child-link subnav-last last"><p>Upgrade Account</p></a></li>
		<? } ?>
		</ul>
	</li>
</ul>