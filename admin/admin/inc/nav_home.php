<?php
#	require_once(dirname(__FILE__) . '/../../6qube/core.php');
	session_start();
	
	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Reseller();
	
	$isReseller = true;
	$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
	$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
	$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
	if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow: #'.$_SESSION['thm_nav-links4'].' 1px 1px;';
	$style3 .= '"';
	
	//get custom nav sections
	$query = "SELECT id, name, allow_class FROM resellers_backoffice WHERE type = 'nav' 
			AND admin_user = '".$_SESSION['parent_id']."' 
			AND resellers_backoffice.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'resellers_backoffice') 
			ORDER BY `list_order` ASC, id ASC";
	if($result = $reseller->query($query)){
		$navs = array();
		while($row = $reseller->fetchArray($result)){
			if($row['allow_class']) $allowed = explode('::', $row['allow_class']);
			else $allowed[] = 0;
			$navs[$row['id']]['name'] = $row['name'];
			$navs[$row['id']]['allow_class'] = $allowed;
			$navs[$row['id']]['pages'] = array();
			$query2 = "SELECT id, name, allow_class FROM resellers_backoffice WHERE type = 'page' 
					 AND parent = '".$row['id']."' AND admin_user = '".$_SESSION['parent_id']."' 
					 AND resellers_backoffice.id NOT IN (SELECT table_id FROM trash WHERE table_name = 'resellers_backoffice') 
					 AND nav = 1 
					 ORDER BY `list_order` ASC, id ASC";
			if($result2 = $reseller->query($query2)){
				while($row2 = $reseller->fetchArray($result2)){
					if($row2['allow_class']) $allowed2 = explode('::', $row2['allow_class']);
					else $allowed2[] = 0;
					$navs[$row['id']]['pages'][$row2['id']]['name'] = $row2['name'];
					$navs[$row['id']]['pages'][$row2['id']]['allow_class'] = $allowed2;
				}
			}
		}
	}
	
	function safeName($input){
		$output = preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($input));
		$output = str_replace('"', '', $output);
		$output = str_replace("'", "", $output);
		$output = str_replace('.', '', $output);
		return $output;
	}
?>
<ul class="top">
	<? if($navs){
	$numNavs = count($navs);
	$i = 0;
	foreach($navs as $key=>$value){
		if(in_array($_SESSION['user_class'], $value['allow_class']) || $_SESSION['reseller']){
			if($i==0){
				$class1 = "active";
				$style4 = 'class="hover"';
				$navStyle = $style1;
			}
			else {
				$class1 = "hide";
				$style4 = "";
				$navStyle = $style3;
			}
			echo '<li><a href="inc/home.php?p='.$key.'" '.$style4.' id="'.safeName($value['name']).'" '.$navStyle.'><p>'.$value['name'].'</p></a></li>
				<li class="'.$class1.'">
					<ul class="child">';
					$pages = $value['pages'];
					$numPages = count($pages);
					if($numPages>=1){
						$addPage = array();
						foreach($value['pages'] as $key2=>$value2){
							if(in_array($_SESSION['user_class'], $value2['allow_class']) || $_SESSION['reseller']){
								$addPage[$key2] = $value2;
							}
						}
						$i2 = 0;
						$actualNumPages = count($addPage);
						foreach($addPage as $key2=>$value2){
							if($i2<($actualNumPages-1)) $class2 = "child-link";
							else $class2 = "last child-link";
							echo '<li><a href="inc/home.php?p='.$key2.'" class="'.$class2.'" '.$style2.'><p>'.$value2['name'].'</p></a></li>';
							$i2++;
						}
					}
			echo '</ul></li>';
			$pages = $numPages = $i2 = $class2 = $class1 = $style4 = $navStyle = NULL;
			$i++;
		} //end if(in_array($_SESSION['user_class'], $value['allow_class']))
	} //end foreach($navs as $key=>$value)
	}?>
</ul>
