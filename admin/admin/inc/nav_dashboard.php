<?php


if(1 || $_SERVER['SERVER_NAME'] == 'pastephp.com')  // 1 : LIVE
{
    require 'nav_dashboard.amado.php';
    exit;
}

// this code is deprecated. use nav_dashboard.amado.php

	session_start();
	if($_SESSION['reseller'] || $_SESSION['reseller_client']) $isReseller = true;
	if($isReseller){
		$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
		$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
		$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
		if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow: #'.$_SESSION['thm_nav-links4'].' 1px 1px;';
		$style3 .= '"';
	}
?>

<ul class="top"> 
	<!--dashboard-->
	<li class="dashboard" ><a href="dashboardv3.php" class="hover" id="dashboard" <? if($isReseller) echo $style1; ?>><img src="img/v3/dashboard-icon.png" /><p>Dashboard</p></a></li>
	<li class="active">
		<ul class="child">
			<li><a href="trash-can.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Trash Can</p></a></li>
            <li><a href="transfer-items.php" class="child-link"><p>Transfer Items</p></a></li>
			<li><a href="domains-manager.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Domains</p></a></li>
            <li><a href="crm-integration.php" class="last child-link"  <? if($isReseller) echo $style2; ?>><p>CRM Integration</p></a></li>			
			
		</ul>
	</li>
	
	<!--directory-->
	<li><a href="websites.php" id="website" <? if($isReseller) echo $style3; ?>><img src="img/v3/website-icon.png" /><p>Websites</p></a></li>
	<li class="hide">
		<ul class="child">
            <? if($_SESSION['login_uid']==3625){ ?>
            <li><a href="http://store.<?=$_SESSION['main_site']?>/admin/" target="_blank" class="child-link" <?=$style2?>><p>eCommerce Admin</p></a></li>
            <? } ?>
			<li><a href="website-analytics.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	
	<!--hub-->
	<li><a href="landing_pages.php" id="landing" <? if($isReseller) echo $style3; ?>><img src="img/v3/landing-icon.png" /><p>Landing Pages</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="landing-analytics.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	
	<!--press-->
	<li><a href="blog.php" id="blog" <? if($isReseller) echo $style3; ?>><img src="img/v3/blogs-icon.png" /><p>Blogs</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="blog-analytics.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	
	<!--blog-->
	<li><a href="social_media.php" id="social" <? if($isReseller) echo $style3; ?>><img src="img/v3/social-icon.png" /><p>Social Media</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="social-analytics.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
    
    <!--blog-->
	<li><a href="mobile.php" id="social" <? if($isReseller) echo $style3; ?>><img src="img/v3/mobile-icon.png" /><p>Mobile</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="mobile-analytics.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	
	<!--articles-->
	<li><a href="hub-forms.php" id="forms" <? if($isReseller) echo $style3; ?>><img src="img/v3/forms-icon.png" /><p>Custom Forms</p></a></li>
    
	<li class="hide">
		<ul class="child">
         <? if($_SESSION['login_uid']==7){ ?>
			<li><a href="hub-forms.php?multi_user=1" class="last child-link" <?=$style2small?>><p>Multi-User Forms</p></a></li>
               <? } ?>
		</ul>
	</li>
  
    <!--articles-->
	<li><a href="forms-autoresponders.php" id="email" <? if($isReseller) echo $style3; ?>><img src="img/v3/email-icon.png" /><p>Email Marketing</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="forms-autoresponders.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Auto-Responders</p></a></li>
			<li><a href="hub-form-emailers.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Email Broadcast</p></a></li>
		</ul>
	</li>
    
    <!--articles-->
	<li><a href="prospects.php" id="article" <? if($isReseller) echo $style3; ?>><img src="img/v3/prospects-icon.png" /><p>Prospects</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="lead-form-prospects.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Lead Form Prospects</p></a></li>
			<li><a href="contact-form-prospects.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Contact Form Prospects</p></a></li>
			<li><a href="blog-prospects.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Blog Prospects</p></a></li>
            <li><a href="network-prospects.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Network Prospects</p></a></li>
		</ul>
	</li>
 
    <li><a href="contacts.php" id="contacts" <? if($isReseller) echo $style3; ?>><img src="img/v3/contact-icon.png" /><p>Contacts</p></a></li>
	<li class="hide">
		<ul class="child">
		</ul>
	</li>
    
    <!--articles-->
	<li><a href="directory.php" id="network" <? if($isReseller) echo $style3; ?>><img src="img/v3/network-icon.png" /><p>Network</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="directory.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Directory</p></a></li>
			<li><a href="press.php" class="child-link" <? if($isReseller) echo $style2; ?>><p>Press Releases</p></a></li>
			<li><a href="articles.php" class="last child-link" <? if($isReseller) echo $style2; ?>><p>Articles</p></a></li>
		</ul>
	</li>

    <li><a href="phone_temp.php" id="phone"  <? if($isReseller) echo $style3; ?>><img src="img/v3/phones-icon.png" /><p>Phone</p></a></li>

	   
</ul>