<?php
	session_start();
	if($_SESSION['reseller'] || $_SESSION['reseller_client']) $isReseller = true;
	$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
	$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
	$style2small = 'style="color:#'.$_SESSION['thm_nav-links2'].';font-size:10.5px;white-space:nowrap;"';
	$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
	if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow:#'.$_SESSION['thm_nav-links4'].' 1px 1px;';
	else $style3 .= 'text-shadow:none;';
	$style3 .= '"';
	$mu_access = $_SESSION['thm_admin_mu']==1;
	//To enable the MU link for a reseller add their UID to the array below
	$hasMultiUser = array('496', '123', '753', '769', '775', '807', '2088', '1929', '2110', '2310', '2556', '2600', '2672', '2755', '2981', '2996', '3027', '3122', '3210', '3215', '3445', '3550', '3558', '3625', '4060', '4149', '4797', '5589', $_SESSION['super_user'], $mu_access);
?>
<ul class="top">
	<li><a href="reseller.php" class="hover" id="manage" <?=$style1?>><p>Manage</p></a></li>
	<li class="active">
		<ul class="child">
        	<? if(!$_SESSION['super_user']){ ?>
			<li><a href="inc/forms/edit-user.php?id=<?=$_SESSION['user_id']?>" class="child-link" <?=$style2?>><p>Settings</p></a></li>
            <? } ?>
            <? if(!$_SESSION['private_theme']==1){ ?>
			<li><a href="inc/forms/edit-theme2.php" class="child-link" <?=$style2?>><p>System Theme</p></a></li>
			<? } ?>
			<? if($_SESSION['thm_custom-backoffice']){ ?>
			<li><a href="reseller-backoffice-pages.php" class="child-link" <?=$style2?>><p>Custom Pages</p></a></li>
			<? } ?>
            <? if(!$_SESSION['super_user'] && !$_SESSION['max_user'] && !$_SESSION['thm_adminmanage_products']){ ?>
			<li><a href="reseller-products.php" class="child-link" <?=$style2?>><p>Products</p></a></li>
            <? }?>
			<? if($_SESSION['payments']=='6qube'){ ?>
			<li><a href="reseller-credits.php" class="child-link" <?=$style2?>><p>Credits</p></a></li>
			<? }?>
            <? if(!$_SESSION['super_user']){ ?>
			<li><a href="reseller-billing.php" class="child-link" <?=$style2?>><p>My Billing Log</p></a></li>      
            <? if(!$_SESSION['thm_adminmanage_userbilling']){ ?>
			<li><a href="reseller-billing.php?view=3" class="child-link" <?=$style2?>><p>User Billing Log</p></a></li>
            <? }?>
            <? }?>
			<!--<li><a href="trash-can.php?admin=1" class="last subnav-last child-link" <?=$style2?>><p>Admin Trash</p></a></li>-->
		</ul>
	</li>
	<li><a href="admin-users.php?parent_id=<?=$_SESSION['login_uid']?>" id="manage" <?=$style3?>><p>Users</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="admin-users.php?parent_id=<?=$_SESSION['login_uid']?>" class="child-link" <?=$style2?>><p>Active Users</p></a></li>
			<li><a href="admin-users.php?parent_id=<?=$_SESSION['login_uid']?>&suspended=1" class="child-link" <?=$style2?>><p>Suspended</p></a></li>
			<li><a href="admin-users.php?parent_id=<?=$_SESSION['login_uid']?>&canceled=1" class="child-link" <?=$style2?>><p>Canceled</p></a></li>
          
			<li><a href="reseller-emailers.php" class="last subnav-last child-link" <?=$style2?>><p>Communications</p></a></li>
		</ul>
	</li>
	<? if($_SESSION['login_uid']=='753' || $_SESSION['login_uid']=='6610'){ ?>
    <li><a href="reporting.php" id="reporting" <?=$style3?>><p>Reporting</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="lead-reports.php" class="child-link" <?=$style2?>><p>Lead Reports</p></a></li>
			<li><a href="analytic-reports.php" class="child-link" <?=$style2?>><p>Analytic Reports</p></a></li>
			<li><a href="ranking-reports.php" class="child-link" <?=$style2?>><p>Ranking Reports</p></a></li>
			<li><a href="call-reports.php" class="last subnav-last child-link" <?=$style2?>><p>Call Reports</p></a></li>
		</ul>
	</li>
    <? } ?>
    <? if(in_array($_SESSION['login_uid'], $hasMultiUser)){ ?>
    <li><a href="hub.php?multi_user=1" id="manage" <?=$style3?>><p>Multi-Site CMS</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="hub.php?multi_user=1" class="child-link" <?=$style2?>><p>MU Websites</p></a></li>

			<?php if($_SESSION['login_uid'] == 6610): ?>
			<li><a href="controller.php?ctrl=Responders" class="child-link" <?=$style2?>><p>MU Responders</p></a></li>
			<?php endif; ?>

			<li><a href="controller.php?ctrl=BlogFeeds" class="child-link" <?=$style2?>><p>MU Blogs</p></a></li>
            
            <?php if($_SESSION['login_uid'] == 2996): ?>
			<li><a href="social_mu.php" class="child-link" <?=$style2?>><p>MU Social Engine</p></a></li>
			<?php endif; ?>

			<li><a href="hub-forms.php?multi_user=1" class="last subnav-last child-link" <?=$style2?>><p>MU Forms</p></a></li>
            
            
		</ul>
	</li>
    <? } ?>
    <? if(!$_SESSION['super_user'] && !$_SESSION['max_user'] && !$_SESSION['thm_admin_site']){ ?>
	<li><a href="hub.php?id=<?=$_SESSION['main_site_id']?>&reseller_site=1" id="site" <?=$style3?>><p>Site</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="hub.php?form=settings&id=<?=$_SESSION['main_site_id']?>&reseller_site=1" class="child-link" <?=$style2?>><p>Edit</p></a></li>
			<li><a href="hub-prospects.php?reseller_site=1" class="child-link" <?=$style2?>><p>Prospects</p></a></li>
			<li><a href="hub-autoresponders.php?reseller_site=1" class="child-link" <?=$style2?>><p>Auto Responders</p></a></li>
			<li><a href="hub-forms.php" class="child-link" <?=$style2?>><p>Forms</p></a></li>
			<li><a href="reseller-site-analytics.php?id=<?=$_SESSION['main_site_id']?>" class="last subnav-last child-link" <?=$style2?>><p>Analytics</p></a></li>
		</ul>
	</li>
    <? }?>
    <? if(!$_SESSION['max_user'] && !$_SESSION['thm_admin_networks']){ ?>
    <li><a href="networks.php" id="networks" <?=$style3?>><p>Networks</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="networks.php?type=local" class="child-link" <?=$style2?>><p>Local</p></a></li>
			<li><a href="networks.php?type=hubs" class="child-link" <?=$style2?>><p>Websites</p></a></li>
			<li><a href="networks.php?type=blogs" class="child-link" <?=$style2?>><p>Blogs</p></a></li>
			<li><a href="networks.php?type=press" class="child-link" <?=$style2?>><p>Press</p></a></li>
			<li><a href="networks.php?type=articles" class="child-link" <?=$style2?>><p>Articles</p></a></li>
		</ul>
	</li>
    <? }?>
</ul>
