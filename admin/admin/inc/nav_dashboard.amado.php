<?php

	session_start();
	if($_SESSION['reseller'] || $_SESSION['reseller_client']) $isReseller = true;
        
        $additional_style = '';
	if($isReseller){
		$style1 = 'style="color:#'.$_SESSION['thm_nav-links'].';"';
		$style2 = 'style="color:#'.$_SESSION['thm_nav-links2'].';"';
		$style3 = 'style="color:#'.$_SESSION['thm_nav-links3'].';';
		if($_SESSION['thm_nav-links4']) $style3 .= 'text-shadow: #'.$_SESSION['thm_nav-links4'].' 1px 1px;';
		$style3 .= '"';
                
                $additional_style = $style2;
	}
?>
<? if($_SESSION['network_user']){ ?>
<ul class="top"> 
	<!--dashboard-->
	<li class="dashboard" ><a href="dashboardv3.php" class="hover" id="dashboard" <? if($isReseller) echo $style1; ?>><img src="img/v3/dashboard-icon.png" /><p>Dashboard</p></a></li>
	<li class="active">
		<ul class="child">
       
			
		</ul>
	</li>	
     <!--network-->
	<li><a href="directory.php" id="network" <? if($isReseller) echo $style3; ?>><img src="img/v3/network-icon.png" /><p>Network</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="directory.php" class="child-link" <?php echo $additional_style; ?>><p>Directory</p></a></li>
			<li><a href="press.php" class="child-link" <?php echo $additional_style; ?>><p>Press Releases</p></a></li>
			<li><a href="articles.php" class="last child-link" <?php echo $additional_style; ?>><p>Articles</p></a></li>
		</ul>
	</li>
    <!--prospects-->
	<li><a href="network-prospects.php" id="prospects" <? if($isReseller) echo $style3; ?>><img src="img/v3/prospects-icon.png" /><p>Prospects</p></a></li>
	<li class="hide">
		<ul class="child">
		</ul>
	</li>

    
    <li><a href="contacts.php" id="contacts" <? if($isReseller) echo $style3; ?>><img src="img/v3/contact-icon.png" /><p>Contacts</p></a></li>
	<li class="hide">
		<ul class="child">
		</ul>
	</li>	   
</ul>
<? } else { ?>
<ul class="top"> 
	<!--dashboard-->
	<li class="dashboard" ><a href="dashboardv3.php" class="hover" id="dashboard" <? if($isReseller) echo $style1; ?>><img src="img/v3/dashboard-icon.png" /><p>Dashboard</p></a></li>
	<li class="active">
		<ul class="child">
        <? if(!$_SESSION['reports_user']){ ?>
			<li><a href="trash-can.php" class="child-link" <?php echo $additional_style; ?>><p>Trash Can</p></a></li>
            <li><a href="transfer-items.php" class="child-link"><p>Transfer Items</p></a></li>
			<!--<li><a href="domains-manager.php" class="child-link" <?php echo $additional_style; ?>><p>Domains</p></a></li>-->
            <li><a href="crm-integration.php" class="child-link"  <?php echo $additional_style; ?>><p>CRM Integration</p></a></li>			
        <? } ?>    
	<?php if($_SESSION['login_uid'] == 6610 || TRUE): ?>
            <li><a href="controller.php?ctrl=Spam&action=spam" class="child-link"  <?php echo $additional_style; ?>><p>Spam</p></a></li>
	<?php endif; ?>
            <li><a href="controller.php?action=settings" class="last child-link"  <?php echo $additional_style; ?>><p>Settings</p></a></li>	
			
		</ul>
	</li>	
    <? if($_SESSION['reports_user']){ ?>
    <li><a href="website-analytics.php" id="website" <? if($isReseller) echo $style3; ?>><img src="img/v3/website-icon.png" /><p>Analytics</p></a></li>
	<li class="hide">
		<ul class="child">
		</ul>
	</li>
    <? } else { ?>
	<!--websites-->
	<li><a href="websites.php" id="website" <? if($isReseller) echo $style3; ?>><img src="img/v3/website-icon.png" /><p>Websites</p></a></li>
	<li class="hide">
		<ul class="child">
            <? if($_SESSION['login_uid']==3625){ ?>
            <li><a href="http://store.<?=$_SESSION['main_site']?>/admin/" target="_blank" class="child-link" <?=$style2?>><p>eCommerce Admin</p></a></li>
            <? } ?>
			<li><a href="website-analytics.php" class="last child-link" <?php echo $additional_style; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	<? } ?>
    <? if(!$_SESSION['reports_user']){ ?>
	<!--landing pages-->
	<li><a href="landing_pages.php" id="landing" <? if($isReseller) echo $style3; ?>><img src="img/v3/landing-icon.png" /><p>Landing Pages</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="landing-analytics.php" class="last child-link" <?php echo $additional_style; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	<? } ?>
    <? if(!$_SESSION['reports_user']){ ?>
	<!--blogs-->
	<li><a href="blog.php" id="blog" <? if($isReseller) echo $style3; ?>><img src="img/v3/blogs-icon.png" /><p>Blogs</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="blog-analytics.php" class="last child-link" <?php echo $additional_style; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	<? } ?>
    <? if(!$_SESSION['reports_user']){ ?>
	<!--social-->
	<li><a href="social_media.php" id="social" <? if($isReseller) echo $style3; ?>><img src="img/v3/social-icon.png" /><p>Social Media</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="social-analytics.php" class="last child-link" <?php echo $additional_style; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
    <? } ?>
    <? if(!$_SESSION['reports_user']){ ?>
    <!--mobile-->
	<li><a href="mobile.php" id="social" <? if($isReseller) echo $style3; ?>><img src="img/v3/mobile-icon.png" /><p>Mobile</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="mobile-analytics.php" class="last child-link" <?php echo $additional_style; ?>><p>Analytics</p></a></li>
		</ul>
	</li>
	<? } ?>

	<? if($_SESSION['login_uid']==7 || $_SESSION['login_uid']==7990
			|| $_SESSION['login_uid'] == 6610){     // may-08-13 LIVE! ?>
	<!--rankings-->
	<li><a href="rankings.php" id="rankings" <? if($isReseller) echo $style3; ?>><img src="img/v3/rankings-icon.png" /><p>Rankings</p></a></li>
    
	<li class="hide">
		<ul class="child">
		</ul>
	</li>
	<? } ?>

    <? if(!$_SESSION['reports_user']){ ?>
	<!--custom forms-->
	<li><a href="hub-forms.php" id="forms" <? if($isReseller) echo $style3; ?>><img src="img/v3/forms-icon.png" /><p>Custom Forms</p></a></li>
    
	<li class="hide">
		<ul class="child">
		</ul>
	</li>
  	<? } ?>
    <? if(!$_SESSION['reports_user']){ ?>
    <!--email marketing-->
	<li><a href="forms-autoresponders.php" id="email" <? if($isReseller) echo $style3; ?>><img src="img/v3/email-icon.png" /><p>Email Marketing</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="forms-autoresponders.php" class="child-link" <?php echo $additional_style; ?>><p>Auto-Responders</p></a></li>
			<li><a href="hub-form-emailers.php" class="last child-link" <?php echo $additional_style; ?>><p>Email Broadcast</p></a></li>
		</ul>
	</li>
    <? } ?>

    <!--prospects-->
	<li><a href="prospects.php" id="prospects" <? if($isReseller) echo $style3; ?>><img src="img/v3/prospects-icon.png" /><p>Prospects</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="lead-form-prospects.php" class="child-link" <?php echo $additional_style; ?>><p>Lead Form Prospects</p></a></li>
			<li><a href="contact-form-prospects.php" class="child-link" <?php echo $additional_style; ?>><p>Contact Form Prospects</p></a></li>
			<? if(!$_SESSION['reports_user']){ ?>
			<li><a href="blog-prospects.php" class="child-link" <?php echo $additional_style; ?>><p>Blog Prospects</p></a></li>
            <li><a href="network-prospects.php" class="last child-link" <?php echo $additional_style; ?>><p>Network Prospects</p></a></li>
            <? } ?>
		</ul>
	</li>

    
    <li><a href="contacts.php" id="contacts" <? if($isReseller) echo $style3; ?>><img src="img/v3/contact-icon.png" /><p>Contacts</p></a></li>
	<li class="hide">
		<ul class="child">
		</ul>
	</li>
    <? if(!$_SESSION['reports_user']){ ?>
    <!--network-->
	<li><a href="directory.php" id="network" <? if($isReseller) echo $style3; ?>><img src="img/v3/network-icon.png" /><p>Network</p></a></li>
	<li class="hide">
		<ul class="child">
			<li><a href="directory.php" class="child-link" <?php echo $additional_style; ?>><p>Directory</p></a></li>
			<li><a href="press.php" class="child-link" <?php echo $additional_style; ?>><p>Press Releases</p></a></li>
			<li><a href="articles.php" class="last child-link" <?php echo $additional_style; ?>><p>Articles</p></a></li>
		</ul>
	</li>
	<? } ?>
    <li><a href="phone_temp.php" id="phone"  <? if($isReseller) echo $style3; ?>><img src="img/v3/phones-icon.png" /><p>Phone</p></a></li>

	   
</ul>
<? } ?>
