<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//Authorize
require_once(QUBEADMIN . 'inc/auth.php');
//DB connections
require_once(QUBEADMIN . 'inc/local.class.php');
$db = new Local();
$table = $db->sanitizeInput($_GET['table']);
$id = is_numeric($_GET['id']) ? $_GET['id'] : 0;
$perm = $_GET['perm'] ? 1 : 0;
$section = $db->sanitizeInput($_GET['section']);
//check user has permission first
if($table=='resellers_network' || $table=='resellers_emailers' || $table=='resellers_backoffice')
	$uid_field = 'admin_user';
else
	$uid_field = 'user_id';
$query = "SELECT `".$uid_field."` FROM `".$table."` WHERE id = '".$id."'";
$result = $db->queryFetch($query);
$uid = $result[$uid_field];
//force permanent delete for some
if($table=='leads') $perm = true;
if($result[$uid_field]==$_SESSION['user_id']){
	
	if(!$perm){
//		OLD trash table setup: (commented out 5/30/2012 by Reese)
//		$query = "INSERT INTO trash 
//				(table_name, table_id, user_id, date) 
//				VALUES 
//				('".$table."', '".$id."', '".$_SESSION['user_id']."', NOW())";

		$query = "UPDATE `".$table."` SET `trashed` = '".date('Y-m-d')."' WHERE `id` = '".$id."' LIMIT 1";
		if($result = $db->query($query)){
			
			if($table=="articles") $item = "Article";
			if($table=="press_release") $item = "Press Release";
			if($table=="hub") $item = "Hub";
			if($table=="hub_page" || $table=="hub_page2") $item = "Hub Page";
			if($table=="directory") $item = "Directory Listing";
			if($table=="blogs") $item = "Blog";
			if($table=="blog_post") $item = "Blog Post";
			if($table=="auto_responder") $item = "Autoresponder";
			if($table=="resellers_network") $item = "Network Site";
			if($table=="resellers_emailers") $item = "Email Template";
			if($table=="resellers_backoffice") $item = "Back Office Item";
			if($table=="lead_forms") $item = "Custom Form";
			if($table=="leads") $item = "Form Response";
			if($table=="lead_forms_emailers") $item = "Marketing Emailer";
			
			echo '<div style="background:url(\'img/undo-bar';
			if($_SESSION['theme']) echo '/'.$_SESSION['thm_undobar-clr'];
			echo '.png\') no-repeat;color:#fff;font-weight:bold;font-size:12px;width:437px;height:27px;" id="undo-test"><p style="padding:4px;">'.$item.' has been moved to the trash. ';
			if($table!='hub_page'){
				echo '<span style="color:#ffffff;cursor:pointer;text-decoration:underline;margin-left:90px;" class="undo" title="'.$section.'" id="'.$table.'-'.$id.'" rel="'.$_GET['undoLink'].'">Undo</span>';
			}
			echo '</p></div>';
			
			//if user is deleting a hub page check to see if it's a reseller deleting a MU hub page
			if(($table=='hub_page' || $table=='hub_page2') && $_SESSION['reseller']){
				$query = "SELECT hub_id FROM `".$table."` WHERE id = '".$id."'";
				$pageInfo = $db->queryFetch($query, NULL, 1);
				$query = "SELECT req_theme_type, theme, multi_user FROM hub WHERE id = '".$pageInfo['hub_id']."'";
				$a = $db->queryFetch($query, NULL, 1);
				if(($a['req_theme_type']==4) || $a['multi_user']){
					//if so, trash the page for all users using the theme
					$query = "UPDATE `".$table."` SET `trashed` = '".date('Y-m-d')."' WHERE `page_parent_id` = '".$id."' 
							  AND `user_parent_id` = '".$_SESSION['login_uid']."'";
					$b = $db->query($query);
//					OLD trash table setup (commented out 5/30/2012 by Reese):
//					$query = "SELECT id, user_id FROM `".$table."` WHERE page_parent_id = '".$id."' 
//							AND user_parent_id = '".$_SESSION['login_uid']."'";
//					$b = $db->query($query);
//					if($b && $db->numRows($b)){
//						while($c = $db->fetchArray($b, NULL, 1)){
//							//for each user's hub with the MU theme
//							$query = "INSERT INTO trash
//									(table_name, table_id, user_id, date)
//									VALUES
//									('".$table."', '".$c['id']."', '".$c['user_id']."', NOW())";
//							$db->query($query);
//						}
//					}
				}
			} //end if($table=='hub_page' && $_SESSION['reseller'])
			else if($table=='lead_forms'){
				//check to see if it's a MU custom form
				$query = "SELECT multi_user FROM lead_forms WHERE id = '".$id."'";
				$a = $db->queryFetch($query, NULL, 1);
				if($a['multi_user']==1){
					//if so add all the child forms to the trash too
					$query = "UPDATE `lead_forms` SET `trashed` = '".date('Y-m-d')."' WHERE `form_parent_id` = '".$id."'";
					$b = $db->query($query);
//					OLD trash table setup (commented out 5/30/2012 by Reese):
//					$query = "SELECT id, user_id FROM lead_forms WHERE form_parent_id = '".$id."'";
//					$b = $db->query($query);
//					if($b){
//						if($db->numRows($b)){
//							while($row = $db->fetchArray($b, NULL, 1)){
//								$query = "INSERT INTO trash 
//										(table_name, table_id, user_id, date) 
//										VALUES 
//										('lead_forms', '".$row['id']."', '".$row['user_id']."', NOW())";
//								$db->query($query);
//							}
//						}
//					}
				}
			}
		}
		else {
			echo '<div style="background:url(\'img/undo-bar';
			if($_SESSION['theme']) echo '/'.$_SESSION['thm_undobar-clr'];
			echo '.png\') no-repeat;color:#fff;font-weight:bold;font-size:12px;width:437px;height:27px;" id="undo-test"><p style="padding:4px;">There was an error deleting your item.</p></div>';
			if($_SESSION['admin']) echo '<strong>Admin error:</strong> '.mysqli_error();
		}
	} //end if(!$perm)
	
	else { //permanent delete
		require_once('inc/domains.class.php');
		$connector = new Domains();
		
		//if user isn't trying to delete a whole network
		if($_GET['type']!='whole' && $_GET['table']!='campaigns'){
			//first get analytics_id to remove from analytics
			if($table=="hub" || $table=="directory" || $table=="press_release" || $table=="articles" || 
			   $table=="blogs" || $table=="resellers_network"){
				$query = "SELECT tracking_id FROM `".$table."` WHERE id = '".$id."'";
				$result = $db->queryFetch($query, NULL, 1);
				//delete from analytics
				if($result['tracking_id']!=0) $connector->ANAL_deleteSite(NULL, $result['tracking_id']);
			}
			
			//if user is deleting a hub page check to see if it's a reseller deleting a MU hub page
			if($table=='hub_page2' && $_SESSION['reseller']){
				$query = "SELECT hub_id FROM hub_page2 WHERE id = '".$id."'";
				$pageInfo = $db->queryFetch($query, NULL, 1);
				$query = "SELECT multi_user FROM hub WHERE id = '".$pageInfo['hub_id']."'";
				$a = $db->queryFetch($query, NULL, 1);
				if($a['multi_user']){
					//if so, trash the page for all users using the theme
					//first get ids of copy pages
					$query = "SELECT * FROM hub_page2 WHERE page_parent_id = '".$id."' 
							  AND user_parent_id = '".$_SESSION['login_uid']."'";
					$a = $db->query($query);
					if($a && $db->numRows($a)){
						//loop through them
						while($b = $db->fetchArray($a, 1)){
							//back up first
							$db->serialBackup($b, 'delete-bckp', 'hub_page2-'.$b['id'].'_uid-'.$b['user_id'].'.txt');
							//delete item
							$query = "DELETE FROM hub_page2 WHERE id = '".$b['id']."' LIMIT 1";
							$db->query($query);
							//delete trash entry
							//$query = "DELETE FROM trash WHERE table_name = 'hub_page2' AND table_id = '".$b['id']."' LIMIT 1";
							//$db->query($query);
						}
					}
				}
			}
			//if user is perm deleting a MU control hub remove its ID as control_hub for the theme
			if($table=='hub' && $_SESSION['reseller']){
				//get hub info to see if it's an MU hub
				$query = "SELECT req_theme_type, theme FROM hub WHERE id = '".$id."' AND user_id = '".$_SESSION['login_uid']."'";
				$c = $db->queryFetch($query);
				if($c['req_theme_type']==4){
					$query = "UPDATE themes SET control_hub = 0 WHERE control_hub = '".$id."' AND id = '".$c['theme']."' 
							AND user_id_access = '".$_SESSION['login_uid']."' LIMIT 1";
					$db->query($query);
				}
			}
			
			//if user is deleting from hub_page2 table check to see if it's a page or nav
			if($table=='hub_page2'){
				$query = "SELECT type FROM hub_page2 WHERE id = '".$id."'";
				$pageInfo = $db->queryFetch($query, NULL, 1);
			}
			
			//permanently delete item - backup first
			$query = "SELECT * FROM `".$table."` WHERE id = '".$id."' LIMIT 1";
			$backupRow = $db->queryFetch($query, NULL, 1);
			$db->serialBackup($backupRow, 'delete-bckp', $table.'-'.$id.'_uid-'.$uid.'.txt');
			$query = "DELETE FROM `".$table."` WHERE id = '".$id."' LIMIT 1";
			if($db->query($query)){
				$results['error'] = 0;
				$results['message'] = "Item deleted successfully.";
				//delete from trash table too
				//$query2 = "DELETE FROM trash WHERE table_id = '".$id."' AND table_name = '".$table."' LIMIT 1";
				//$db->query($query2);
				//if user is perm deleting a hub also delete its pages
				if($table=='hub'){
					//back them up first
					$query = "SELECT * FROM hub_page WHERE hub_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					if($pages = $db->query($query)){
						while($page = $db->fetchArray($pages, NULL, 1)){
							$db->serialBackup($page, 'delete-bckp', 'hub_page-'.$page['id'].'_uid-'.$page['user_id'].'.txt');
						}
					}
					$query = "SELECT * FROM hub_page2 WHERE hub_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					if($pages2 = $db->query($query)){
						while($page2 = $db->fetchArray($pages2, NULL, 1)){
							$db->serialBackup($page2, 'delete-bckp', 'hub_page2-'.$page2['id'].'_uid-'.$page2['user_id'].'.txt');
						}
					}
					//now delete
					$query1 = "DELETE FROM hub_page WHERE hub_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					$query2 = "DELETE FROM hub_page2 WHERE hub_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					$db->query($query1);
					$db->query($query2);
				}
				//if user is perm deleting a V2 Nav Section delete its pages
				if($table=='hub_page2' && $pageInfo['type']=='nav'){
					//back up first
					$query = "SELECT * FROM hub_page2 WHERE nav_parent_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					if($pages = $db->query($query)){
						while($page = $db->fetchArray($pages, NULL, 1)){
							$db->serialBackup($page, 'delete-bckp', 'hub_page2-'.$page['id'].'_uid-'.$page['user_id'].'.txt');
						}
					}
					//now delete
					$query = "DELETE FROM hub_page2 WHERE nav_parent_id = '".$id."' AND user_id = '".$_SESSION['user_id']."'";
					$db->query($query);
				}
			}
			else {
				$results['error'] = 1;
				$results['message'] = "Error deleting item:\n";//.mysqli_error();
			}
		} //end if($_GET['type']!='whole')
		
		//if user is trying to delete a whole network
		else if($table=='resellers_network' && $_GET['type']=='whole'){
			//delete parent entry from resellers_network table
			$query = "DELETE FROM 'resellers_network' WHERE id = ".$id." LIMIT 1";
			if($db->query($query)){
				//first get tracking_id and domain for each network site to remove them from the system
				$query = "SELECT tracking_id, domain FROM resellers_network WHERE parent = ".$id;
				$networks = $db->query($query);
				while($row = $db->fetchArray($networks)){
					//remove from analytics
					if($row['tracking_id']!=0) $connector->ANAL_deleteSite(NULL, $row['tracking_id']);
					//remove subdomain
					if($row['domain']) $connector->deleteSubdomain($row['domain']);
				}
				//delete all network sites
				$query = "DELETE FROM 'resellers_network' WHERE parent = '".$id."'";
				if($result = $db->query($query)){
					$results['error'] = 0;
					$results['message'] = "Item deleted successfully.";
				}
				else {
					$results['error'] = 1;
					$results['message'] = "Error deleting network sites:\n";//.mysqli_error();
				}
			} //end if($db->query($query))
			else {
				$results['error'] = 1;
				$results['message'] = "Error deleting whole network:\n";//.mysqli_error();
			}
		} //end else if($table=='resellers_network' && $_GET['type']=='whole')
		
		//if user is deleting an entire campaign
		else if($_GET['table']=='campaigns'){
			$errors = array();
			$folder = 'deleted-campaigns';
			$fname = 'camp-'.$id.'_';
			$allTables = array('Articles'=>'articles', 'Auto Responders'=>'auto_responders', 'Blogs'=>'blogs', 'Prospects'=>'contact_form', 'Directories'=>'directory', 'Hubs'=>'hub', 'Custom Forms'=>'lead_forms', 'Press Releases'=>'press_release');
			//BACK UP EVERYTHING FIRST
			//campaign row
			$query = "SELECT * FROM campaigns WHERE id = '".$id."' LIMIT 1";
			$row = $db->queryFetch($query, NULL, 1);
			$db->serialBackup($row, $folder, substr($fname, 0, -1).'.txt');
			//blog posts
			$query = "SELECT * FROM blog_post WHERE (SELECT blogs.cid FROM blogs WHERE blogs.id = blog_post.blog_id) = '".$id."' AND blog_post.user_id = '".$uid."'";
			if($blogPosts = $db->query($query) && $db->numRows($blogPosts)){
				while($row = $db->fetchArray($blogPosts, 1)){
					$db->serialBackup($row, $folder, $fname.'blog_post-'.$row['id'].'_blog-'.$row['blog_id'].'.txt');
				}
			}
			//hub pages
			$query = "SELECT * FROM hub_page WHERE (SELECT hub.cid FROM hub WHERE hub.id = hub_page.hub_id) = '".$id."' AND hub_page.user_id = '".$uid."'";
			if($hubPages = $db->query($query) && $db->numRows($hubPages)){
				while($row = $db->fetchArray($hubPages, 1)){
					$db->serialBackup($row, $folder, $fname.'hub_page-'.$row['id'].'_hub-'.$row['hub_id'].'.txt');
				}
			}
			//hub pages 2
			$query4 = "SELECT * FROM hub_page2 WHERE (SELECT hub.cid FROM hub WHERE hub.id = hub_page2.hub_id) = '".$id."' AND hub_page.user_id = '".$uid."'";
			if($hubPages2 = $db->query($query) && $db->numRows($hubPages2)){
				while($row = $db->fetchArray($hubPages2, 1)){
					$db->serialBackup($row, $folder, $fname.'hub_page2-'.$row['id'].'_hub-'.$row['hub_id'].'.txt');
				}
			}
			//all other tables
			foreach($allTables as $key=>$value){
				$query = "SELECT * FROM `".$value."` WHERE cid = '".$id."' LIMIT 1";
				if($results = $db->query($query) && $db->numRows($results)){
					while($row = $db->fetchArray($results, 1)){
						$db->serialBackup($row, $folder, $fname.$value.'-'.$row['id'].'.txt');
					}
				}
				unset($results, $row); //reset vars for next iteration
			}
			
			//NOW DELETE
			//build queries
			//delete campaign row
			$query1 = "DELETE FROM campaigns WHERE id = '".$id."' LIMIT 1";
			if(!$db->query($query1)) $errors['campaigns'] = 1;
			//delete hub pages, blog posts and leads now while parents still exist
			//blog_post
			$query2 = "DELETE FROM blog_post WHERE (SELECT blogs.cid FROM blogs WHERE blogs.id = blog_post.blog_id) = '".$id."' AND blog_post.user_id = '".$uid."'";
			if(!$db->query($query2)) $errors['blog_posts'] = 1;
			//hub_page
			$query3 = "DELETE FROM hub_page WHERE (SELECT hub.cid FROM hub WHERE hub.id = hub_page.hub_id) = '".$id."' AND hub_page.user_id = '".$uid."'";
			if(!$db->query($query4)) $errors['hub_page'] = 1;
			//hub_page2
			$query4 = "DELETE FROM hub_page2 WHERE (SELECT hub.cid FROM hub WHERE hub.id = hub_page2.hub_id) = '".$id."' AND hub_page.user_id = '".$uid."'";
			if(!$db->query($query4)) $errors['hub_page2'] = 1;
			//leads
			$query5 = "DELETE FROM leads WHERE (SELECT lead_forms.cid FROM lead_forms WHERE lead_forms.id = leads.lead_form_id) = '".$id."' AND leads.user_id = '".$uid."'";
			if(!$db->query($query5)) $errors['leads'] = 1;
			//all other tables
			foreach($allTables as $key=>$value){
				//delete tracking from some
				if($value=="hub" || $value=="directory" || $value=="press_release" || $value=="articles" || $value=="blogs" ){
					$trackingQuery = "SELECT tracking_id";
					if($value=="hub" || $value=="blogs") $trackingQuery .= ", domain, lincoln_domain ";
					$trackingQuery .= " FROM `".$table."` WHERE user_id = '".$uid."' AND cid = '".$id."'";
					if(($result = $db->query($trackingQuery)) && $db->numRows($result)){
						while($row = $db->fetchArray($result)){
							//delete from analytics
							if($row['tracking_id']) $connector->ANAL_deleteSite(NULL, $row['tracking_id']);
							//remove domain if hub or blog
							if($value=="hub" || $value=="blogs"){
								if($row['domain']){
									//check if it's an addon or subdomain
									$domain = $connector->breakUpURL($row['domain']);
									//strip the http://
									if(substr($row['domain'], 0, 7)=='http://') $thisDomain = substr($row['domain'], 7);
									else $thisDomain = $row['domain'];
									if($domain['sub']!='www'){
										//is a subdomain -- parent domain may have already been deleted, this would
										//throw an "error" but not a big deal
										if($row['lincoln_domain']){
											//delete the domain over on the lincoln server
											$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
											$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
											$secHash = md5($thisDomain.$salt);
											$secHash = md5($secHash.'sub'.$pepper);
											//makes the hash only good if it was submitted today:
											$secHash = md5($secHash.date('d'));
											$deleteDomain = file_get_contents('http://6qube.mobi/secure/remove_domain.php?domain='.$thisDomain.'&mode=sub&hash='.$secHash);
										} else {
											$deleteDomain = $connector->deleteSubdomain($row['domain']);
										}
									}
									else {
										//addon domain
										if($row['lincoln_domain']){
											//delete the domain over on the lincoln server
											$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
											$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
											$secHash = md5($thisDomain.$salt);
											$secHash = md5($secHash.'addon'.$pepper);
											//makes the hash only good if it was submitted today:
											$secHash = md5($secHash.date('d'));
											$deleteDomain = file_get_contents('http://6qube.mobi/secure/remove_domain.php?domain='.$thisDomain.'&mode=addon&hash='.$secHash);
										} else {
											$deleteDomain = $connector->deleteAddon($row['domain']);
										}
									}
									if(!$deleteDomain['success']) $errors['domains'] = 1;
								}
							}
							$domain = $thisDomain = $deleteDomain = NULL; //clear vars for next iteration
						}
					}
				}
				$query = "DELETE FROM `".$value."` WHERE cid = '".$id."' LIMIT 1";
				if(!$db->query($query)) $errors[$value] = 1;
				$row = NULL;
			}
			if($errors){
				//return all errors for testing/debugging
				$results['message'] = "There was an error removing one or more of the following item types:\n";
				foreach($errors as $key=>$value){
					$results['message'] .= "\n".$key;
				}
				//user probably only has to really know about domain removal errors
				if($errors['domains']){
					$results['message'] .= "There may have been an error removing one or more of your domains from our system.  You may have to contact support if you have problems using the domain again.";
				}
			}
			$results['success'] = 1;
		} //end else if($_GET['table']=='campaigns')
		
		echo json_encode($results);
	} //end else {
	
} //end if($result['user_id']==$_SESSION['user_id'])
else echo 'Not authorized. ';//.$result['user_id'].'!='.$_SESSION['user_id'];
?>