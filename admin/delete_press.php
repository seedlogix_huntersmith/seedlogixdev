<?php
	
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//authorize
	require_once('./inc/auth.php');
	
		require_once('./inc/db_connector.php');
		$connector = new DbConnector();
		
		$id = $_GET['id'];
		
		if($_GET['action'] == "confirm") {
			$query = "DELETE FROM press_release WHERE id = '$id'";
			$result = $connector->query($query);
			if($result)
				header('Location: press.php');
		}
		else {
			//header
			require_once('./admin/inc/header.php');
			
			echo '<p>Are you sure you want to delete Press Release ID: '.$id.'</p><p><a href="delete.php?id='.$id.'&action=confirm">YES</a> | <a href="press.php">Ooops, no!</a></p>';
			
			//footer
			require_once('./admin/inc/footer.php');
		}
?>