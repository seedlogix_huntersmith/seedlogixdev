<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
require_once QUBEADMIN . 'inc/hub.class.php';
$hub = new Hub();
$newHubName = $_POST['name'];
$copyHubID = $_POST['id'];
$userID = $_SESSION['user_id'];
$copyHub = $hub->getHubs(NULL, NULL, $copyHubID);
if($hub->getHubRows()){
	$copyHub = $hub->fetchArray($copyHub);
	$newHub = $hub->addNewHub($userID, $newHubName, $copyHub['cid'], NULL, true);
	//If successful, $newHub = new hub's ID.  Otherwise empty
	if($newHub && $copyHub['user_id'] == $_SESSION['user_id']){
		
		//determine what type of hub is being duped (full theme or landing page)
		$query = "SELECT type FROM themes WHERE id = '".$copyHub['theme']."'";
		$a = $hub->queryFetch($query);
		$hubType = $a['type'];
		if($hubType==1){
			//if it's a full theme check to make sure the user has dupes left for this campaign
			$numDupes = $hub->getCampaignHubDupes($copyHubID, $copyHub['cid']);
			if($_SESSION['user_limits']['hub_dupe_limit']!=-1){
				if($numDupes < $_SESSION['user_limits']['hub_dupe_limit']) $continue = true;
				else {
					$continue = false;
					$reason = 'You\'ve reached the maximum amount of times this hub can be duplicated.';
				}
			}
			else $continue = true;
		}
		else if($hubType==2){
			//if it's a landing page check to make sure they haven't hit their campaign limit
			$numLandings = $hub->getCampaignLandings($copyHub['cid']);
			if($_SESSION['user_limits']['landing_limit']!=-1){
				if($numLandings < $_SESSION['user_limits']['landing_limit']) $continue = true;
				else {
					$continue = false;
					$reason = 'You\'ve reached the maximum number of landing sites for this campaign.';
				}
			}
			else $continue = true;
		}
		else $continue = true;
		
		if($continue){
			$id_duped = $copyHub['id_duped'] ? $copyHub['id_duped'] : $copyHubID;
			if($copyHub['req_theme_type']) $id_duped = 0;
			//Copy over hub settings
			$query = "UPDATE hub
					SET
					id_duped = '".$id_duped."',
					user_parent_id = '".$copyHub['user_parent_id']."', 
					category = '".$copyHub['company_name']."', 
					company_name = '".addslashes($copyHub['company_name'])."', phone = '".$copyHub['phone']."', 
					street = '".addslashes($copyHub['street'])."', city = '".addslashes($copyHub['city'])."', 
					state = '".$copyHub['state']."', zip = '".$copyHub['zip']."', 
					logo = '".$copyHub['logo']."', slogan = '".addslashes($copyHub['slogan'])."', 
					coupon_offer = '".addslashes($copyHub['coupon_offer'])."', 
					description = '".addslashes($copyHub['description'])."', 
					overview = '".addslashes($copyHub['overview'])."', 
					offerings = '".addslashes($copyHub['offerings'])."', 
					photos = '".addslashes($copyHub['photos'])."', 
					videos = '".addslashes($copyHub['videos'])."', 
					events = '".addslashes($copyHub['events'])."', 
					edit_region_6 = '".addslashes($copyHub['edit_region_6'])."', 
				    edit_region_7 = '".addslashes($copyHub['edit_region_7'])."', 
					edit_region_8 = '".addslashes($copyHub['edit_region_8'])."', 
					edit_region_9 = '".addslashes($copyHub['edit_region_9'])."', 
					edit_region_10 = '".addslashes($copyHub['edit_region_10'])."', 
					bg_color = '".$copyHub['bg_color']."', links_color = '".$copyHub['links_color']."',	
					footer_text_color = '".$copyHub['footer_text_color']."', header_text_color = '".$copyHub['header_text_color']."', 
					footer_links_color = '".$copyHub['footer_links_color']."', header_links_color = '".$copyHub['header_links_color']."', 
					extra_links_color = '".$copyHub['extra_links_color']."', callaction_btn = '".$copyHub['callaction_btn']."', 
					bg_repeat = '".$copyHub['bg_repeat']."', facebook_app_id = '".$copyHub['facebook_app_id']."', 
					facebook_app_secret = '".$copyHub['facebook_app_secret']."', facebook_plugin = '".addslashes($copyHub['facebook_plugin'])."', 
					remove_contact = '".$copyHub['remove_contact']."', backlink = '".addslashes($copyHub['backlink'])."', 
					slider_option = '".$copyHub['slider_option']."', facebook_app_id = '".$copyHub['facebook_app_id']."',  
					bg_img = '".$copyHub['bg_img']."', site_title = '".$copyHub['site_title']."', 
					meta_keywords = '".$copyHub['meta_keywords']."', keyword1 = '".$copyHub['keyword1']."', 
					keyword2 = '".$copyHub['keyword2']."', keyword3 = '".$copyHub['keyword3']."', 
					keyword4 = '".$copyHub['keyword4']."', keyword5 = '".$copyHub['keyword5']."', 
					keyword6 = '".$copyHub['keyword6']."', seo_url = '".$copyHub['seo_url']."', 
					seo_url_title = '".addslashes($copyHub['seo_url_title'])."', 
					meta_description = '".addslashes($copyHub['meta_description'])."', twitter = '".$copyHub['twitter']."', 
					facebook = '".$copyHub['facebook']."', myspace = '".$copyHub['myspace']."', 
					youtube = '".$copyHub['youtube']."', diigo = '".$copyHub['diigo']."', 
					technorati = '".$copyHub['technorati']."', linkedin = '".$copyHub['linkedin']."', 
					google_local = '".$copyHub['google_local']."', yahoo = '".$copyHub['yahoo']."', 
					blog = '".$copyHub['blog']."', 
					pic_one_title = '".addslashes($copyHub['pic_one_title'])."', pic_one = '".$copyHub['pic_one']."',
					pic_one_desc = '".addslashes($copyHub['pic_one_desc'])."', pic_one_link = '".$copyHub['pic_one_link']."',  
					pic_two_title = '".addslashes($copyHub['pic_two_title'])."', pic_two = '".$copyHub['pic_two']."', 
					pic_two_desc = '".addslashes($copyHub['pic_two_desc'])."', pic_two_link = '".$copyHub['pic_two_link']."', 
					pic_three_title = '".addslashes($copyHub['pic_three_title'])."', pic_three = '".$copyHub['pic_three']."', 
					pic_three_desc = '".addslashes($copyHub['pic_three_desc'])."', pic_three_link = '".$copyHub['pic_three_link']."', 
					pic_four_title = '".addslashes($copyHub['pic_four_title'])."', pic_four = '".$copyHub['pic_four']."', 
					pic_four_desc = '".addslashes($copyHub['pic_four_desc'])."', pic_four_link = '".$copyHub['pic_four_link']."', 
					pic_five_title = '".addslashes($copyHub['pic_five_title'])."', pic_five = '".$copyHub['pic_five']."', 
					pic_five_desc = '".addslashes($copyHub['pic_five_desc'])."', pic_five_link = '".$copyHub['pic_five_link']."', 
					pic_six_title = '".addslashes($copyHub['pic_six_title'])."', pic_six = '".$copyHub['pic_six']."', 
					pic_six_desc = '".addslashes($copyHub['pic_six_desc'])."', pic_six_link = '".$copyHub['pic_six_link']."', 
					pic_seven_title = '".addslashes($copyHub['pic_seven_title'])."', pic_seven = '".$copyHub['pic_seven']."', 
					pic_seven_desc = '".addslashes($copyHub['pic_seven_desc'])."', pic_seven_link = '".$copyHub['pic_seven_link']."', 
					pic_eight_title = '".addslashes($copyHub['pic_eight_title'])."', pic_eight = '".$copyHub['pic_eight']."', 
					pic_eight_desc = '".addslashes($copyHub['pic_eight_desc'])."', pic_eight_link = '".$copyHub['pic_eight_link']."', 
					pic_nine_title = '".addslashes($copyHub['pic_nine_title'])."', pic_nine = '".$copyHub['pic_nine']."', 
					pic_nine_desc = '".addslashes($copyHub['pic_nine_desc'])."', pic_nine_link = '".$copyHub['pic_nine_link']."', 
					pic_ten_title = '".addslashes($copyHub['pic_ten_title'])."', pic_ten = '".$copyHub['pic_ten']."', 
					pic_ten_desc = '".addslashes($copyHub['pic_ten_desc'])."', pic_ten_link = '".$copyHub['pic_ten_link']."', 
					theme = '".$copyHub['theme']."', adv_css = '".$copyHub['adv_css']."', 
					theme_color = '".$copyHub['theme_color']."', video_script = '".addslashes($copyHub['video_script'])."', 
					add_autoresponder = '".addslashes($copyHub['add_autoresponder'])."', 
					call_action_submit = '".addslashes($copyHub['call_action_submit'])."', 
					signup_name = '".addslashes($copyHub['signup_name'])."', signup_link = '".$copyHub['signup_link']."', 
					button_1 = '".addslashes($copyHub['button_1'])."', button1_link = '".$copyHub['button1_link']."', 
					button_2 = '".addslashes($copyHub['button_2'])."', button2_link = '".$copyHub['button2_link']."', 
					button_3 = '".addslashes($copyHub['button_3'])."', button3_link = '".$copyHub['button3_link']."', 
					form_box_color = '".$copyHub['form_box_color']."', gallery_page = '".$copyHub['gallery_page']."', 
					display_page = '".$copyHub['display_page']."', coupon_offer2 = '".addslashes($copyHub['coupon_offer2'])."', 
					req_theme_type = '".$copyHub['req_theme_type']."', 
					affiliate_info = '".$copyHub['affiliate_info']."', form_type = '".$copyHub['form_type']."', 
					has_custom_form = '".$copyHub['has_custom_form']."', has_tags = '".$copyHub['has_tags']."', 
					trashed = '".$copyHub['trashed']."'   
					WHERE id = ".$newHub;
			if($hub->query($query)){
				//Check for pages and duplicate those too if there are any
				if($copyHub['pages_version']==2){
					$pagesTable = 'hub_page2';
					$v2 = 1;
				}
				else {
					$pagesTable = 'hub_page';
					$v2 = 0;
				}
				$query = "SELECT * FROM `".$pagesTable."` WHERE hub_id = '".$copyHubID."'";
				$query .= " AND ".$pagesTable.".trashed = '0000-00-00' ";
				$query .= " ORDER BY created ASC, id ASC";
				$result = $hub->query($query);
				if($result && $hub->numRows($result)){
					$errors = 0;
					while($row = $hub->fetchArray($result)){
						$pageVals = array('user_parent_id'=>$row['user_parent_id'], 
									   'page_title'=>addslashes($row['page_title']), 
									   'page_region'=>addslashes($row['page_region']), 
									   'page_edit_2'=>addslashes($row['page_edit_2']), 
									   'page_keywords'=>$row['page_keywords'], 
									   'page_seo_title'=>addslashes($row['page_seo_title']), 
									   'page_seo_desc'=>addslashes($row['page_seo_desc']), 
									   'page_photo'=>$row['page_photo'], 
									   'page_photo_desc'=>addslashes($row['page_photo_desc']), 
									   'inc_contact'=>$row['inc_contact'], 
									   'page_full_width'=>$row['page_full_width'], 
									   'has_custom_form'=>$row['has_custom_form'], 
									   'has_tags'=>$row['has_tags'], 
									   'trashed'=>$row['trashed']);
						if($pagesTable=='hub_page2'){
							$pageVals['type'] = $row['type'];
							$pageVals['inc_nav'] = $row['inc_nav'];
							$pageVals['nav_order'] = $row['nav_order'];
							//Need to find a way to preserve the nav_parent_ids, nav_root_ids
						}
						else {
							$pageVals['page_navname'] = addslashes($row['page_navname']);
							$pageVals['nav'] = $row['nav'];
						}
						if(!$hub->addNewPage($newHub, $pageVals, $userID, $_SESSION['parent_id'], $v2)) $errors++;
					}
					if(!$errors){
						$results['error'] = 0;
						$results['message'] = "Hub and pages duplicated successfully.";
					}
					else {
						$results['error'] = 1;
						$results['message'] = "Hub copied successfully but one or more pages were not copied.";
						$results['message2'] = "Num errors: ".$errors."\n";//.mysqli_error();
					}
				} //end if($result && $hub->numRows($result))
				else {
					$results['error'] = 0;
					$results['message'] = "Hub duplicated successfully.  No pages were found/copied.";
				}
			} //end if($hub->query($query))
			else {
				$results['error'] = 1;
				$results['message'] = "Error copying settings to new hub.";
				//$results['message2'] = mysqli_error();
			}
		} //end if($continue)
		else {
			$results['error'] = 1;
			$results['message'] = $reason;
			//$results['message2'] = mysqli_error();
		}
	} //end if($newHub && $copyHub['user_id'] == $_SESSION['user_id'])
	else {
		$results['error'] = 1;
		$results['message'] = "Error creating new hub.";
		//$results['message2'] = mysqli_error();
	}
} //end if($hub->getHubRows())
else {
	$results['error'] = 1;
	$results['message'] = "Couldn't get original hub info.";
}
echo json_encode($results);
?>