<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

if(!$isCron) require_once(QUBEADMIN . 'inc/auth.php');

if($_SESSION['admin'] || $_SESSION['reseller'] || $isInclude){
	
    if(!function_exists('sendit')){
    function sendit($to, $headers, $body)
    {
        global $smtp;
        
#        return FALSE;
        
        return $smtp->send($to, $headers, $body);
    }
    }
	require_once('inc/local.class.php');
	$connector = new Local();
	
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
	$port = '25';
	$smtp = Mail::factory('smtp',
				array ('host' => $host,
					  'port' => $port, 
					  'auth' => false));
        
	if(!$isInclude){
		$userID = $_POST['id'];
		$emailSubject = $_POST['emailsubject'];
		$emailBody = $_POST['emailbody'];
	}
	
	if($emailBody=="") {
		$data['error'] = 1;
		$data['message'] = 'Message field cannot be blank.';
	}
	else {
		//check to see if user is emailing all users
		if(strpos($userID, ",")!==false){ //if $userID has a comma
			//trim it first
			$userID = trim($userID, ',');
			$userID2 = explode(",", $userID); //turn it into an array
			$numIDs = count($userID2);
		}
                
		//set up for() loop 
		if($userID2){ //if user is emailing all users
			$iterations = $numIDs; //set number of times to loop through email script
		} else if($userID){
			$iterations = 1; //set number of times to loop through email script
			$userID2[0] = $userID; //add the single ID to an array at key 0 to work with loop
		}
		$usersSentNow = '';
		$errorNum = 0;
		$emailsSent = 0;
		$duplicates = 0;
		$emails = array();
		$errors = array();
		//get sending company's info
		if(!$customFrom){
			if($_SESSION['admin']){
				$fromCompany = '6Qube';
				$fromEmail = 'admin@6qube.com';
			}
			else if($_SESSION['reseller']){
				$query = "SELECT company, support_email FROM resellers WHERE admin_user = '".$_SESSION['login_uid']."'";
				$resellerInfo = $connector->queryFetch($query);
				$fromCompany = $resellerInfo['company'];
				$fromEmail = $resellerInfo['support_email'];
			}
		}
		
		for($i=0; $i<$iterations; $i++){ //loop through email script $iterations number of times
			
			//get user's email and info
			$query = "SELECT username FROM users WHERE id = '".$userID2[$i]."'";
			$userInfo = $connector->queryFetch($query, NULL, 1);
			$query = "SELECT company, firstname, lastname FROM user_info WHERE user_id = '".$userID2[$i]."'";
			$userInfo2 = $connector->queryFetch($query, NULL, 1);
			
			//make sure contact hasn't been emailed already during this run
			if(!in_array(strtolower($userInfo['username']), $emails)){
				
				//set up email to send
				$sendTo = $userInfo['username'];
				if(!$customFrom) $sendFrom = '"'.$fromCompany.'" <'.$fromEmail.'>';
				else $sendFrom = $customFrom;
				$subject = str_replace("#name", $userInfo2['firstname'].' '.$userInfo2['lastname'], $emailSubject);
				$subject = str_replace("#company", $userInfo2['company'], $subject);
				$subject = stripslashes($subject);
				$body = str_replace("#name", $userInfo2['firstname'].' '.$userInfo2['lastname'], $emailBody);
				$body = str_replace("#company", $userInfo2['company'], $body);
				$body = stripslashes($body);
				$headers = array('From' => $sendFrom,
							  'To' => $sendTo,
							  'Subject' => $subject,
							  'Content-Type' => 'text/html; charset=ISO-8859-1',
							  'MIME-Version' => '1.0');
				if(!$customFrom) $headers['Reply-To'] = $fromEmail;
				else $headers['Reply-To'] = $customFrom;
				//send
				if($sendTo) $mailed = sendit($sendTo, $headers, $body);
				
				//error check
				if($mailed && !PEAR::isError($mailed)){
					$emails[$emailsSent] = strtolower($userInfo['username']); //add email to array of emails
					$emailsSent++;
					
					//update string of users_sent
					$usersSent .= '::'.$userID2[$i];
					$usersSentNow .= '::'.$userID2[$i];
				} else {
					$data['error'] = 1;
					$errors[$errorNum] = "User ID: ".$userID2[$i]."\nEmail: ".$sendTo;
					if($mailed) $errors[$errorNum] .= "\nReason:\n".addslashes($mailed->getMessage());
					$errorNum++;
				}
				
			} //end if(!in_array($row['email'], $emails))
			else { $duplicates++; }
			
			//clear vars for next iteration
			$query = $userInfo = $userInfo2 = $sendTo = $subject = $body = $headers = $mailed = NULL;
		} //end for() loop
		if($isCron){
			//update users_sent field with new string
			$query = "UPDATE resellers_emailers SET users_sent = '".$usersSent."' WHERE id = '".$emailerID."'";
			$connector->query($query);
		}
		//add it to the history table
		//get whole row
		if($emailsSentNow){
			$query = "SELECT * FROM resellers_emailers WHERE id = '".$emailerID."'";
			$email = $connector->queryFetch($query, NULL, 1);
			$query = "INSERT INTO resellers_emailers_history 
					(admin_user, name, from_field, subject, body, sent_to, contact, anti_spam)
					VALUES
					('".$email['admin_user']."', '".addslashes($email['name'])."', '".addslashes($email['from_field'])."', 
					'".addslashes($email['subject'])."', '".addslashes($email['body'])."', '".trim($usersSentNow, '::')."', 
					'".addslashes($email['contact'])."', '".addslashes($email['anti_spam'])."')";
			$connector->query($query);
		}
	
		if($data['error']==1){
			$data['message'] = "One or more emails were not sent.  Detailed info:\n";
			for($j=0; $j<count($errors); $j++){
				$data['message'] .= $errors[$j]."\n\n";
			}
			//email errors to user if cron
			if($isCron){
				$to = $customFrom ? $customFrom : $fromEmail;
				$subject = "Scheduled user emailer results: Error report";
				$sendMessage = nl2br($data['message']);
				$body = 
				"<b>The scheduled user communication with subject '".$emailSubject."' was just run and had errors:</b><br />
				<br />".stripslashes($sendMessage);
				$headers = array('From' => '6Qube <support@6qube.com>',
							  'To' => array($to, 'admin@6qube.com'), 
							  'Subject' => $subject,
							  'Content-Type' => 'text/html; charset=ISO-8859-1',
							  'MIME-Version' => '1.0');
				sendit($to, $headers, $body);
			}
		} else {
			$data['error'] = 0;
			$data['message'] = $emailsSent." emails(s) sent successfully!";
			if($duplicates>0){
				if($duplicates==1){
					$data['message'] .= "\n".$duplicates." duplicate email was not sent";
				} else {
					$data['message'] .= "\n".$duplicates." duplicate emails were not sent";
				}
			}
		}
		
	} //end else [email body is not blank]

} //end if($_SESSION['admin'] || $_SESSION['reseller'])
else {
	$data['error'] = 1;
	$data['message'] = 'Not authorized.';
}

if(!$isInclude){
	echo json_encode($data);
}
else if(!$isCron){
	if($data['error']==1){
		echo 
		'<script type="text/javascript">alert("Error sending email(s):\n'.$data['message'].'");</script>';
	}
	else if($data['error']==0){
		echo
		'<script type="text/javascript">alert("'.$data['message'].'");</script>';
	}
}
else if($isCron){
	if($data['error']==1){
		echo 
		'Error sending email(s):<br />'.nl2br($data['message']).'<br />';;
	}
	else if($data['error']==0){
		echo
		$data['message'].'<br />';
	}
}
?>