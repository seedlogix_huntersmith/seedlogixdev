<?
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	require_once('inc/auth.php');
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	$uid = $_SESSION['login_uid'];
	$type = $_GET['type'];
	$id = $_GET['id'];
	
	if($id && is_numeric($id)){ //if user is editing a specific network
		if($type == 'whole')
			include('inc/forms/edit_whole_network.php');
		else
			include('inc/forms/edit_network_site.php');
	}
	else { //if user is viewing networks
		if($type){
			//if user is viewing a certain type of network
			$query = "SELECT id, tracking_id, name, type, domain FROM resellers_network 
					WHERE type = '".$type."' AND admin_user = ".$_SESSION['login_uid']."
					AND trashed = '0000-00-00' 
					AND parent = 0";
			if($type != 'whole')
				$query .= " AND type != 'whole'";
		} 
		else {
			//if user is viewing all their networks/sites
			$type = "all";
			$query = "SELECT id, tracking_id, parent, name, type, domain FROM resellers_network 
					WHERE admin_user = ".$_SESSION['login_uid']."
					AND trashed = '0000-00-00' ";
		}
		if($results = $local->query($query)){
			//get results and count them
			$num_results = $local->numRows($results) ? $local->numRows($results) : 0;
		}
		
		//header text
		if($type=='whole')
			echo '<h1>Whole Networks</h1>';
		else 
			echo '<h1>'.ucwords($type).' Network Sites</h1>';
		
		//create new form
		if($type != "all"){
			echo '<div id="createDir">
					<form method="post" action="add-network.php" class="ajax">
						<input type="text" name="name" />
						<input type="hidden" name="type" value="'.$type.'" />
						<input type="submit" value="Create New Network" name="submit" />
					</form>
				</div>';
			echo '<div id="message_cnt"></div>';
		}
		
		if($num_results){
			//if results, loop through them and fill $html array with html
			echo '<div id="dash"><div id="fullpage_network_'.$type.'" class="dash-container"><div class="container">';
			while($row = $local->fetchArray($results)){ //if so loop through and display links to each network
				$tracking = $row['tracking_id'];
				if(!$tracking || $tracking==0) $tracking = 26;
				if($row['type']=='whole') $a = 1; //$html[1] contains whole networks list html
				else if(!$row['parent']) $a = 2; //$html[2] contains single networks list html
				else $a = ''; //don't list if it's part of a whole network
				$html[$a] .=  '<div class="item" title="network" rel="'.$row['type'].'" id="network-'.$row['id'].'">
						<div class="icons">
							<a href="'.$row['domain'].'" target="_new" class="view" title="View"><span>View</span></a>
							<a href="networks.php?type='.$row['type'].'&id='.$row['id'].'" class="edit" title="Edit"><span>Edit</span></a>
							<a href="#" class="delete" rel="table=resellers_network&id='.$row['id'].'&type='.$row['type'].'" title="Delete"><span>Delete</span></a>
						</div>';
				$html[$a] .= '<img src="'.$local->ANAL_getGraphImg('hub', $row['id'], $tracking,$uid).'" class="graph trigger" />';
				$html[$a] .= '<a href="networks.php?type='.$row['type'].'&id='.$row['id'].'">'.$row['name'].'</a>
						</div>';
			}
		
			//if user is viewing all their networks
			if($type=='all'){
				//echo '<h2>Whole networks:</h2>';
				echo $html[1].'<br />';
				if($html[2]){
					//echo '<h2>Single networks:</h2>';
					echo $html[2].'<br />';
				}
			}
			//if user is viewing their whole networks
			else if($type=='whole')
				echo $html[1];
			//if user is viewing their single networks
			else 
				echo $html[2];
		}
		else {
			//if no networks were found
			echo '<div id="dash"><div id="fullpage_network_'.$type.'" class="dash-container"><div class="container">';
			if($type != "all"){
				if($type=='whole')
					echo 'Use the form above to create a whole network of connected sites.';
				else 
					echo 'Use the form above to create a new '.$type.' network site.';
			}
			else {
				echo 'You currently have no network sites set up yet.  Click a type on the left to start creating sites.<br />
				You can create an entire connected network in the Whole Networks section or individual sites in the other sections.';
			}
			echo '</div></div></div>';
		}
	}
?>