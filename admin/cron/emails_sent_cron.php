<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

//This script is run on the 1st of each month
//It combs through the users table and for each user with >0 emails sent in the last month
//it grabs the number of emails sent, puts it in the emails_sent table under today's date,
//and sets the emails_sent field in the users table back to 0

require_once(QUBEADMIN . 'inc/db_connector.php');
$db = new DbConnector();

$query = "SELECT id, parent_id, emails_sent FROM users WHERE emails_sent > 0";
$result = $db->query($query);
if($result){
	while($row = $db->fetchArray($result)){
		$emailsSent = $row['emails_sent'];
		//log number of emails sent into emails_sent table
		$query = "INSERT INTO emails_sent 
				(user_id, user_parent_id, month, emails_sent) 
				VALUES
				('".$row['id']."', '".$row['parent_id']."', '".date('m')."/".date('Y')."', '".$row['emails_sent']."')";
		$db->query($query);
		//set emails_sent field in users table back to 0 for the next month
		$query = "UPDATE users SET emails_sent = 0 WHERE id = '".$row['id']."'";
		$db->query($query);
	}
}
?>