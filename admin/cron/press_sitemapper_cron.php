<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');


require_once(QUBEADMIN . 'inc/directory.class.php');
$dir = new Dir();

//First run sitemapping for 6qube:
runSitemapping('6qube.com');
//Now get reseller main sites and run sitemapping for them
$query = "SELECT id, admin_user, domain FROM resellers_network WHERE type = 'whole' AND domain != '' AND setup = 1";
if($result = $dir->query($query))
if($dir->numRows($result)){
	while($row = $dir->fetchArray($result)){
		runSitemapping($row['domain'], $row['admin_user']);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Start Sitemapping													  //
/////////////////////////////////////////////////////////////////////////////////////////
function runSitemapping($domain, $resellerID = ''){
	$dir = new Dir();
	$limit = 10; //$mainLimit from local.6qube.com/index.php
	$criteria = " WHERE trashed = '0000-00-00' 
			   AND headline != '' 
			   AND summary != '' 
			   AND body != '' 
			   AND city != ''
			   AND state != '' 
			   AND category != '0'
			   AND category != '' 
			   AND user_id != 105";
	
	$base = '<?xml version="1.0" encoding="UTF-8"?>
		   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
		   <url>
			 <loc>http://press.'.$domain.'/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>daily</changefreq>
			 <priority>1.0</priority>
		   </url>
		   <url>
			 <loc>http://press.'.$domain.'/cities/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>weekly</changefreq>
			 <priority>0.6</priority>
		   </url>
		   ';
	$end = "</urlset>";
	
	//Get cities
	$query = "SELECT id, COUNT(type), city, state FROM press_release";
	$query .= $criteria;
	if($resellerID) $query .= " AND user_parent_id = ".$resellerID;
	$query .= " GROUP BY city, state";
	$cities = $dir->query($query);
	while($row = $dir->fetchArray($cities)){
		$city = $row['city'];
		$state = $row['state'];
		if($major = $dir->majorCity($city)){
			if($state == $major)
				$url = 'http://press.'.$domain.'/'.$dir->convertKeywordUrl($city);
			else
				$url = 'http://press.'.$domain.'/'.$dir->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		}
		else
			$url = 'http://press.'.$domain.'/'.$dir->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		$add .= '
			   <url>
				 <loc>'.$url.'</loc>
				 <lastmod>'.date('Y-m-d').'</lastmod>
				 <changefreq>daily</changefreq>
				 <priority>0.8</priority>
			   </url>
			   ';
		//add pages for city
		$numResults = $row['COUNT(type)'];
		$numPages = ceil($numResults/$limit);
		for($i=$numPages; $i>1; $i--){
			$add .= '
				   <url>
					 <loc>'.$url.$i.'/</loc>
					 <lastmod>'.date('Y-m-d').'</lastmod>
					 <changefreq>weekly</changefreq>
				   </url>
				   ';
		}
		$numResults = $numPages = 0;
			   
		//Get categories for each city
		$query2 = "SELECT id, COUNT(type), category FROM press_release";
		$query2 .= $criteria;
		if($resellerID) $query2 .= " AND user_parent_id = ".$resellerID;
		$query2 .= " AND city = '".$city."'";
		$query2 .= " AND state = '".$state."'";
		$query2 .= " GROUP BY category";
		$cats = $dir->query($query2);
		while($row2 = $dir->fetchArray($cats)){
			$a = explode("->", $row2['category']);
			$category = $dir->convertKeywordUrl($a[1]);
			$add .= '
				   <url>
					 <loc>'.$url.$category.'</loc>
					 <lastmod>'.date('Y-m-d').'</lastmod>
					 <changefreq>weekly</changefreq>
					 <priority>0.7</priority>
				   </url>
				   ';
			//add pages for city+category
			$numResults = $row2['COUNT(type)'];
			$numPages = ceil($numResults/$limit);
			for($i=$numPages; $i>1; $i--){
				$add .= '
					   <url>
						 <loc>'.$url.$category.$i.'/</loc>
						 <lastmod>'.date('Y-m-d').'</lastmod>
						 <changefreq>weekly</changefreq>
					   </url>
					   ';
			}
			$numResults = $numPages = 0;
		}
	}
	
	//Now all of the listings
	$query3 = "SELECT id, category, headline, timestamp FROM press_release ";
	$query3 .= $criteria;
	if($resellerID) $query3 .= " AND user_parent_id = ".$resellerID;
	$listings = $dir->query($query3);
	while($row3 = $dir->fetchArray($listings)){
		$a = explode("->", $row3['category']);
		$category = $a[1];
		$listingUrl = 'http://press.'.$domain.'/'.$dir->convertKeywordUrl($category).
					$dir->convertKeywordUrl($row3['headline']).$row3['id'].'/';
		$add .= '
			   <url>
				 <loc>'.$listingUrl.'</loc>
				 <lastmod>'.date('Y-m-d', strtotime($row3['timestamp'])).'</lastmod>
				 <changefreq>monthly</changefreq>
			   </url>
			   ';
	}
	//add pages for root site
	$numResults = $dir->numRows($listings);
	$numPages = ceil($numResults/$limit);
	for($i=$numPages; $i>1; $i--){
		$add .= '
			   <url>
				 <loc>http://press.'.$domain.'/'.$i.'/</loc>
				 <lastmod>'.date('Y-m-d').'</lastmod>
				 <changefreq>daily</changefreq>
			   </url>
			   ';
	}
	
	$data = $base.$add.$end;
	
	if($domain=='6qube.com')
		$sitemapFile = QUBEROOT . 'press/sitemap.xml';
	else
		$sitemapFile = QUBEROOT . 'press/sitemap_'.$domain.'.xml';
	
	$f = fopen($sitemapFile, 'w');
	fwrite($f, $data);
	fclose($f);
} //end function runSitemapping()
?>