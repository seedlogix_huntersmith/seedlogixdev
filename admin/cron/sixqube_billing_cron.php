<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');


//This script is run once a day at noon CST
//It goes through the resellers_billing table and processes charges to resellers.

require_once(QUBEADMIN . 'inc/reseller.class.php');
require_once(QUBEADMIN . 'inc/Authnet.class.php');
$reseller = new Reseller();

//get items that are scheduled to be processed and haven't been attempted
$query = "SELECT * FROM sixqube_billing 
		WHERE sixqube_billing.sched = 1 
		AND (sixqube_billing.sched_attempted = 0 OR sixqube_billing.sched_reattempt > 0) 
		AND sixqube_billing.success = 0 
		AND (SELECT users.access FROM users WHERE users.id = sixqube_billing.user_id) = 1
		AND (SELECT users.canceled FROM users WHERE users.id = sixqube_billing.user_id) = 0 
		AND sixqube_billing.amount != 0.00 ";
$billItems = $reseller->query($query);

if($billItems && $reseller->numRows($billItems)){
	//loop through items to process
	while($item = $reseller->fetchArray($billItems)){
	//instantiate Authnet class here so it's fresh for each item
	$anet = new Authnet();
	//check to see if today is the day for billing
	$today = date('Y-m-d');
	$schedDate = substr($item['sched_date'], 0, 10);
	if((strtotime($today) >= strtotime($schedDate)) || ($item['sched_attempted'] && $item['sched_reattempt'])){
		$echo .= '<b>Attempting billing for id#'.$item['id'].'</b><br />';
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////
		//if item is a reseller's monthly bill
		///////////////////////////////////////////
		if($item['transaction_type']=='monthlySubscription'){
			$echo .= 'Item is user #'.$item['user_id'].'\'s monthly bill for $'.$item['amount'].'<br />';
			$process = $anet->chargeCIM($item['user_id'], NULL, $item['product_id'], $item['amount'], 'monthlySubscription', 0);
			if($process['success']){
				$echo .= '<b>Item was successfully charged!</b><br />';
				//if item was successfully processed update table row
				$query = "UPDATE 
							sixqube_billing 
						SET
							transaction_id = '".$process['message']."', 
							method_identifier = '".$process['profile_id']."', 
							success = 1, 
							sched_attempted = 1,
							timestamp = NOW()
						WHERE 
							id = '".$item['id']."'";
				$reseller->query($query);
				//schedule next month's billing
				$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
				$nextSched = $reseller->scheduleTransaction(NULL, $item['user_id'], NULL, $item['product_id'], 
													'monthlySubscription', $item['amount'], NULL, 
													'Scheduled monthly billing for '.date('F', strtotime($nextBillDate)),
													$nextBillDate, 2, NULL, NULL, 1);
				if($nextSched){
					$echo .= 'Next month\'s billing was successfully scheduled.<br /><br />';
				}
				else {
					$echo .= 'Error scheduling next month\'s billing:<br />'.mysqli_error().'<br /><br />';
				}
			}
			else {
				$echo .= '<b>Billing failed! Details:</b><br />'.$process['message'].'<br />';
				//if item failed billing update table row
				$query = "UPDATE 
							sixqube_billing 
						SET
							method_identifier = '".$process['profile_id']."', 
							success = 0, 
							note = '".addslashes($process['message'])."', 
							sched_attempted = 1, ";
				if($item['sched_reattempt'])
				//if this isn't the first time trying to bill them
				$query .= 	" sched_reattempt = ".($item['sched_reattempt']-1);
				else
				//otherwise set it to try 2 more times
				$query .= 	" sched_reattempt = 2";
				$query .=		", timestamp = NOW() ";
				$query .=	" WHERE 
							id = '".$item['id']."'";
				if($reseller->query($query)){
					$echo .= 'Reattempt scheduled<br />';
				}
				else $echo .= 'Error scheduling reattempt:<br />'.mysqli_error().'<br />';
				
				//get their info for email
				$query = "SELECT username FROM users WHERE id = '".$item['user_id']."'";
				$user = $reseller->queryFetch($query);
				$profile = $anet->getPaymentProfile($item['user_id']);
				
				//if this was their last re-billing attempt
				if($item['sched_reattempt']==1){
					$echo .= 'This was the final billing attempt. Disabling user\'s access...<br /><br />';
					//disable access to reseller's account
					$query = "UPDATE users SET access = '-1' WHERE id = '".$item['user_id']."'";
					$reseller->query($query);
					//disable their users' access
					//(commented out here --  will set up option in 6qube Admin to do this manually)
					//$query = "UPDATE users SET access = 0 WHERE parent_id = ".$item['user_id'];
					//$reseller->query($query);
					
					//set up email
					$subject = "6Qube Monthly Billing Failed - Final Attempt";
					$message = 
					'<h1>Your monthly 6Qube billing failed</h1>
					We regret to inform you that billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed for the third time in a row.  <b>Access to your account has been disabled</b>.<br /><br />
					<h2>Failure Details:</h2>
					'.$process['message'].'<br /><br />
					Please contact us at 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a> to correct the situation.';
				} //end if($item['sched_reattempt']==1)
				//otherwise send them a failed biling email
				else {
					$echo .= 'First failed billing attempt for this month. Rescheduled for 2 more attempts.<br /><br />';
					//set up email
					if(!$item['sched_reattempt']) $attemptNum = "First Attempt";
					else if($item['sched_reattempt']==2) $attemptNum = "Second Attempt";
					$subject = "6Qube Monthly Billing Failed - ".$attemptNum;
					$message = 
					'<h1>Your monthly 6Qube billing failed</h1>
					We regret to inform you that the billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed. You can still access your account for now, however we will automatically attempt to bill you again tomorrow and if processing fails three times your account will be disabled.<br /><br />
					<h2>Failure Details:</h2>
					'.$process['message'].'<br /><br />
					<b>To avoid another failed billing attempt please make sure your card has sufficient funds and update your billing profile if your payment information has changed.</b><br />
					To do this, log in and go to the Billing tab.<br /><br />
					If you need assistance please call us as 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a>';
				} //end else (wasn't the final attempt)
				//send email (admin cc'd)
				$to = $user['username'];
				$to2 = $to.', admin@6qube.com';
				$headers = array('From' => 'admin@6qube.com',
							  'To' => $to,
							  'Cc' => 'admin@6qube.com', 
							  'Subject' => $subject);
				$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
			} //end else (processing failed)
			//CLEAR VARIABLES
			$process = $nextBillDate = $nextSched = $user = $profile = $subject = 
			$message = $attemptNum = $to = $to2 = $headers = NULL;
		} //end if($item['transaction_type']=='6qubeMonthly')
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	else if(date('Y-m-d', strtotime('+3 days')) == $schedDate){
		//send courtesy email reminding user they will be charged in 3 days
		if($item['transaction_type']=='monthlySubscription'){
			//get reseller info
			$query = "SELECT company, support_email, support_phone FROM resellers 
					WHERE admin_user = '".$item['user_parent_id']."'";
			$resellerInfo = $reseller->queryFetch($query);
			//get customer's email
			$query = "SELECT username FROM users WHERE id = ".$item['user_id'];
			$user = $reseller->queryFetch($query);
			//email vars
			$to = $user['username'];
			$from = '6Qube <admin@6qube.com>';
			$subject = 'Reminder - Monthly Billing Will Be Charged In 3 Days';
			$message = 
			'<h1>Your monthly subscription will renew in 3 days</h1>
			You\'re receiving this email as a reminder that your monthly subscription amount of $'.$item['amount'].' is set to automatically renew in 3 days.  Please make sure you have sufficient funds and your billing information is up to date in our system.<br /><br />
			If you need any assistance just reply to this email or give us a call at 877-570-5005.';
			
			$headers = array('From' => $from,
						  'To' => $to,
						  'Subject' => $subject);
			$reseller->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
			//CLEAR VARIABLES
			$resellerInfo = $user = $to = $from = $subject = $message = NULL;
		}
	}
	} //end while($item = $reseller->fetchArray($billItems))
} //end if($billItems && $reseller->numRows($billItems))
//else $echo .= 'No items to process.';

if($echo){
	echo $echo;
	//email Reese output for testing
	$to = 'admin@6qube.com';
	$from = 'noreply@6qube.com';
	$subject = 'sixqube_billing_cron run results: '.date('Y-m-d H:i');
	$message = 
	'The 6Qube Billing cron script was run at '.date('H:i:s').'CST on '.date('Y-m-d').'.  Here\'s the output:<br /><br />'.$echo;
	$headers = array('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
	$reseller->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
}
?>