<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

//This script is run every 30min and combs through the resellers_emailers table to find
//scheduled emails from resellers to their users.

require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

//SMTP settings
require_once 'Mail.php';
$host = "relay.jangosmtp.net";
$port = '25';
$smtp = Mail::factory('smtp',
			array ('host' => $host,
				  'port' => $port, 
				  'auth' => false));

//initial query to find appropriate emails
$query = "SELECT * FROM resellers_emailers WHERE (sched > 0 OR sched_mode2 = 'signup') AND active = 1";
$query .= " AND from_field != '' AND subject != '' AND body != ''";
$emailers = $reseller->query($query);
$echo = '';
if($emailers && $reseller->numRows($emailers)){
	//loop through each emailer
	while($email = $reseller->fetchArray($emailers)){
                $admin  =   false;  // ALWAYS - ALWAYS - INITIALIZE VARIABLES. OTHERWISE YOU GET CRAZY SHIT HAPPENIN AND SPEND HOURS FINDING BUGS AND SHIT
                $sentToArray    =   array();    // initialize more variables
#		$echo .= 'Emailer #'.$email['id'].'<br />';
#		$echo .= 'From reseller #'.$email['admin_user'].'<br />';
		if($email['admin_user']==7) $admin = true;

                #FOUND THE BUG THAT HAPPENS WHEN $ADMIN IS NOT (RESET) MAKES ALL CONSEQUENT USERS ADMIN = 1 AFTER PROCESSING AN ADMIN EMAIL.
#                echo "User {$email['admin_user']} is admin = $admin\n";
#continue;
                # end bug confirmation
                
		//determine the send settings to construct user queries
		//sendto_class
		if($email['sendto_class']==-1) $class = ""; //email all users
		else if($email['sendto_class']==-2 || $email['sendto_class']==-3 || $email['sendto_class']==-4){
			if($email['admin_user']==7 || $email['admin_user']==40){
				//6qube admin
				if($email['sendto_class']==-2){
					//all non-free
					$class = " AND class != 1 AND class != 178 ";
				}
				else if($email['sendto_class']==-3 || $email['sendto_class']==-4){
					//all free users
					$class = " AND (class = 1 OR class = 178) ";
					if($email['sendto_class']==-3){
						//all free users who've never logged in
						$class .= " AND last_login = '0000-00-00 00:00:00' ";
					}
				}
			}
			else {
				//get reseller's free class id
				$query = "SELECT id FROM user_class WHERE maps_to = 27 AND admin_user = '".$email['admin_user']."' LIMIT 1";
				$a = $reseller->queryFetch($query);
				if($email['sendto_class']==-2){
					//email all non-free users
					if($a['id']) $class = " AND class != '".$a['id']."' ";
					else $class = " AND class = 999999 ";
				}
				else {
					//email all free users who've never logged in
					if($a['id']) $class = " AND class = '".$a['id']."' AND last_login = '0000-00-00 00:00:00' ";
					else $class = " AND class = 999999 ";
				}
			}
		}
		else $class = " AND class = '".$email['sendto_class']."' ";
		//sendto_type
		if($email['sendto_type']=='all') $active = '';
		else if($email['sendto_type']=='active') $active = " AND access = '1' AND canceled = '0' ";
		else if($email['sendto_type']=='suspended') $active = " AND access = '0' AND canceled = '0' ";
		else if($email['sendto_type']=='active_suspended') $active = " AND canceled = '0' ";
		else if($email['sendto_type']=='canceled') $active = " AND canceled = '1' ";
		else $active = "";
		//optout for marketing emails
		if($email['emailer_type']=='marketing') $optout = " AND marketing_optout != 1 ";
		else $optout = "";
		//start query
		$query = "SELECT id FROM users ";
		if(!$admin) $query .= " WHERE parent_id = '".$email['admin_user']."' ";
		else $query .= " WHERE (parent_id = 0 OR parent_id = 7) ";
		$query .= $class.$active.$optout;
		//scheduling
		$continue = true;
		if($email['sched_mode']=="hours") $timeDiff = 3600; //60x60
		else if($email['sched_mode']=="days") $timeDiff = 86400; //3600x24
		else if($email['sched_mode']=="weeks") $timeDiff = 604800; //86400x7
		else if($email['sched_mode']=="months") $timeDiff = 2419200; //604800x4
		else if($email['sched_mode']=="") $timeDiff = 3600; //Default to hours
		$timeDiff = $email['sched'] * $timeDiff;
		if($email['sched_mode2']=='now'){
			//if the schedule is "1 Days from Now" type
			$createdTime = strtotime($email['created']);
			$nowTime = time();
			if(($nowTime - $createdTime) >= $timeDiff) $continue = true;
			else $continue = false;
		}
		else {
			//if the schedule is "from when user signs up" type
			$nowDate = date("Y-m-d H:i:s");
			$minSendDate = date("Y-m-d H:i:s", (time() - $timeDiff));
			if($email['retro']==1)
				$query .= " AND created BETWEEN '2010-01-01 00:00:00' AND '".$minSendDate."'";
			else 
				$query .= " AND created BETWEEN '".$minSendDate."' AND '".$nowDate."'";
		}
#		$echo .= $query.'<br />';

#		echo $echo; $echo = '';		#continue;

		if($continue){
			//query ids
			if($b = $reseller->query($query)){
				if($reseller->numRows($b)){
					//set up user ids to pass to email_users script
					$users = '';
					if(TRUE){   #$email['sched_mode2']=='signup' && $email['retro']==1){
						//remove leading/trailing '::'
						$email['users_sent'] = trim($email['users_sent'], '::');
						$sentToArray = explode('::', $email['users_sent']);
						$isRetro = true;
					}
					while($c = $reseller->fetchArray($b)){
						//if schedule is "from signup" and retroactive, check if this user has already been sent this email
#replaced						if($isRetro && in_array($c['id'], $sentToArray)) echo ''; //do nothing -- don't add user to list
                                                if(in_array($c['id'], $sentToArray)) echo ''; //do nothing -- don't add user to list
						else $users .= $c['id'].',';
					}
					$userID = trim($users, ','); //remove leading/leading comma
#echo 'userid:', $userID, ';'; continue;
					//set up other email vars to pass
					$emailSubject = $email['subject'];
					$emailBody = $email['body'];
					if($row['emailer_type']=='marketing'){
						$emailBody .= '<br /><br />'.$email['contact'].'<br />'.$email['anti_spam'];
					}
					//final email vars
					# 2013-02-05 pls.. INITIALIZE VARIABLES.. ANOTEHR MOTHERUFKCIN BUG
					$userID2	=	false;
					$iterations	=	0;
					#end bug
					$isInclude = true;
					$isCron = true;
					$customFrom = stripslashes($email['from_field']);
					$emailSubject = stripslashes($emailSubject);
					$emailBody = stripslashes($emailBody);
					$usersSent = $email['users_sent'];
					$emailerID = $email['id'];
					//include script
					include(QUBEADMIN . 'email_users.php');
				}
				//if no users were found for criteria:
				else $echo .= "No users were found that matched your sending criteria";
			}
			//if no users were found for criteria:
			else $echo .= "No users were found that matched your sending criteria";
		} //end if($continue)
		else {
			$echo .= 'Send time not yet reached.';
		}
		
		$echo .= '<br /><br />';
	} //end while($email = $reseller->fetchArray($emailers))
} //end if($emailers && $reseller->numRows($emailers))
if($echo) echo $echo;
?>
