<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');


//This script culls the online_now table to track users currently online
//Delete rows from the table that are over 30min old
require_once(QUBEADMIN . 'inc/db_connector.php');
$db = new DbConnector();
$query = "DELETE FROM online_now WHERE (".time()." - time) > 1800";
$db->query($query);
?>