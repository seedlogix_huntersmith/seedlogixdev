<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');
//define('QUBE_NOLAUNCH', 1);
//require_once dirname(__FILE__) . '/../../hybrid/bootstrap.php';
//Qube::AutoLoader();

//This script is run every 30min and combs through the resellers_emailers table to find
//scheduled emails from resellers to their users.

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

//SMTP settings
require_once 'Mail.php';
$host = "relay.jangosmtp.net";
$port = '25';
$smtp = Mail::factory('smtp',
			array ('host' => $host,
				  'port' => $port, 
				  'auth' => false));

//Criteria to filter appropriate emails
$filter = array(
	"from_field != ''",
	"subject != ''",
	"body != ''",
	"sched_date != ''"
);
//initial query to find appropriate emails
$query = "SELECT * FROM lead_forms_emailers WHERE sched_mode = 'date' AND active = 1 and trashed = 0 AND str_to_date(concat(sched_date, ' ', sched_hour), '%m/%d/%Y %H') < NOW()";
$query .= " AND " . join(' AND ', $filter);
$emailers = $hub->query($query);
$echo = '';
if($emailers && $hub->numRows($emailers)){
	//loop through each emailer
	while($email = $hub->fetchArray($emailers)){
		$echo .= 'Form Emailer #'.$email['id'].'<br />';
		$echo .= 'From user #'.$email['user_id'].'<br />';

		//optout for marketing emails
		//if($email['emailer_type']=='marketing') $optout = " AND marketing_optout != 1 ";
		//else $optout = "";
		//start query
		//$query = "SELECT lead_ID as id FROM prospects
				  //WHERE user_ID = '{$email['user_id']}'
				 //AND lead_form_id = '{$email['lead_form_id']}'
				// AND trashed = 0
				 //AND optout != 1 ";

		$query = "SELECT l.id FROM leads l, prospects p
				  WHERE l.user_id = '{$email['user_id']}'
				  AND l.lead_form_id = '{$email['lead_form_id']}'
				  AND p.lead_ID = l.id
				  AND p.user_ID = '{$email['user_id']}'
				  AND p.trashed = 0
				  AND l.optout != 1 ";

		//scheduling
		$dateA = explode('/', $email['sched_date']);
		$dateString = $dateA[2].'-'.$dateA[0].'-'.$dateA[1].' '.$email['sched_hour'].':00:00';
		$sendDate = strtotime($dateString);
		$now = strtotime('now');

		if($sendDate<=$now) $continue = true;
				
		//$echo .= $query.'<br />';
		if($continue){
			//query ids
			if($b = $hub->query($query)){
				if($b && $hub->numRows($b)){
					//set up user ids to pass to email_users script
					$leads = '';
					//set up tracking of sending history
					$sentToArray = explode('::', trim($email['leads_sent'], '::'));
					if(!$sentToArray) $sentToArray = array();

					while($c = $hub->fetchArray($b)){
						//check if this lead has already been sent this email
						if(!in_array($c['id'], $sentToArray)) $leads .= $c['id'].',';
					}
					$leadID = trim($leads, ','); //remove leading/leading comma
deb($leadID);
					//set up other email vars to pass
					$emailSubject = $email['subject'];
					$emailBody = $email['body'];
					$emailBody .= '<br /><br />'.$email['contact'].'<br />'.$email['anti_spam'];
					//final email vars
					$isInclude = true;
					$isCron = true;
					$customFrom = stripslashes($email['from_field']);
					$emailSubject = stripslashes($emailSubject);
					$emailBody = stripslashes($emailBody);
					$leadsSent = $email['leads_sent'];
					$emailerID = $email['id'];
					$echo .= "\nLeads: $leadID";
					//include script
					include(QUBEADMIN . 'email_leads.php');
				}
				//if no users were found for criteria:
				else $echo .= "No leads were found that matched your sending criteria";
			}
			//if no users were found for criteria:
			else $echo .= "No leads were found that matched your sending criteria";
		} //end if($continue)
		else {
			$echo .= 'Send time not yet reached: '.$dateString;
		}
		
		$echo .= '<br /><br />';
	} //end while($email = $hub->fetchArray($emailers))
} //end if($emailers && $hub->numRows($emailers))
echo $echo;
//if($echo){
//	//email Reese output for testing
//	$to = 'reeselester@gmail.com';
//	$from = 'noreply@6qube.com';
//	$subject = 'form_emailers_cron run results: '.date('Y-m-d H:i');
//	$message = 
//	'The Resellers Billing cron script was run at '.date('H:i:s').'CST on '.date('Y-m-d').'.  Here\'s the output:<br /><br />'.$echo;
//	$headers = array('From' => $from,
//				  'To' => $to,
//				  'Subject' => $subject);
//	$hub->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
//	//$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
//}
?>
