<?php

require_once '../6qube/core.php';
require_once QUBEPATH . '../classes/VarContainer.php';
require_once QUBEPATH . '../classes/QubeDataProcessor.php';
require_once QUBEPATH . '../classes/ContactFormProcessor.php';
require_once QUBEPATH . '../models/ContactData.php';
require_once QUBEPATH . '../models/Response.php';
require_once QUBEPATH . '../classes/Notification.php';
require_once QUBEPATH . '../classes/NotificationMailer.php';
require_once QUBEPATH . '../models/Responder.php';
require_once QUBEPATH . '../models/AutoResponderTheme.php';
require_once QUBEPATH . '../models/ContactData.php';
require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';

// legacy:
require_once(QUBEADMIN . 'inc/hub.class.php');

// @todo maybe construct a class for this little script? Probably not necessary. its only 4 steps.
/**
 * 1. Query pending QueuedResponse Objects
 * 2. Parse Templates and Variables (Parsed by AutoResponderTheme object - implemented by ThemedResponseNotification)
 * 3. Send Notification
 * 4. Update Queued Response Object with Sent Date (if successful)
 * 5. Do a jig.
 * 
 */

// id => Theme (cache array)
$themes = array();

$Mailer = $qube->getMailer();

$D = Qube::GetDriver();

/** 1. */
// @note restrict this cron to a single user by adding ' AND r.user_id = %user-id%' to the WHERE part of the query

$Q = $D->queryScheduledResponsesWhere('')
        ->Join(array('ActiveResponder', 'r'),   // get active responders
            'r.id = T.responder_id AND r.trashed = "0000-00-00"')
        
        /* old trash table join 
        ->leftJoin(array('trash', 'respondertrash'),                 // check that the responder hasnt been deleted
                'respondertrash.table_id = r.id AND respondertrash.table_name="auto_responders"')
        
         * 
         */
        
        ->leftJoin(array('AutoResponderTheme', 'theme'),
                'theme.id = r.theme')
        
        // according to the old code, if the table_name column of the responder
        // is NOT 'lead_forms' then get the prospect info from contact_form table
        // @important this must be a LEFT JOIN and NOT inner join because given the case that
        // table_name = 'lead_forms' would cause the entire query to fail if an innerjoin is used
        
        ->leftJoin(array('ContactData', 'd'),
                'd.id = T.contactdata_id AND T.contactdata_table = "contact_form" AND d.trashed = "0000-00-00"')
        /* replaced: 
        ->leftJoin('leads',
                'T.contactdata_table = "leads" AND leads.optout != 1 AND leads.id = T.contactdata_id AND NOT EXISTS (SELECT table_id FROM trash WHERE table_name = "leads" AND table_id=leads.id)')
        with: */
        
        ->leftJoin('leads',
                'T.contactdata_table = "leads" AND leads.optout != 1 AND leads.id = T.contactdata_id AND leads.trashed = "0000-00-00"')
        
        // inner join
        ->Join(array('User', 'u'),
                'u.id = r.user_id')

        // inner join
        /*  this was leaving out user autoresponders where parent_id = 0!
        ->Join(array('resellers', 'reseller'),
                '(reseller.admin_user = u.id AND u.parent_id = 0) OR reseller.admin_user = u.parent_id')
        */
        ->leftJoin(array('resellers', 'reseller'),
#                'reseller.admin_user = u.id AND r.user_parent_id != 0 AND r.user_parent_id != 7')
                '(u.parent_id = 0 AND reseller.admin_user = u.id) OR reseller.admin_user = u.parent_id')
        
        // possibly get the domain name from reseller for opt-out.
        
        ->addFields('theme.*, leads.data, leads.lead_form_id, d.*, u.*, r.*, IFNULL(reseller.main_site, "6qube.com") as main_site')
        
        // some last minute legacy stuff
        ->Where('r.from_field IS NOT NULL AND r.body IS NOT NULL AND r.contact IS NOT NULL AND r.anti_spam IS NOT NULL AND r.optout_msg IS NOT NULL
            AND (NOT ISNULL(leads.id) or NOT ISNULL(d.id)) ');
        ;
        
if(isset($_GET['ar_restrict']))     /*
 *  Given a ar_restrict (responder_id) value.. force the autoresponder execution and bypass the time restrictions.
 */
        $Q->Where('T.responder_id = %d', $_GET['ar_restrict']);
else
{
////    echo 'no restriction (running all responders)';
    // follow normal execution for scheduled responses.
    $Q->Where('delivered_date = "0000-00-00 00:00:00" AND schedule_date < NOW() + INTERVAL 10 MINUTE');
}
if($_GET['amado'] == 'AMADO')
    echo $Q;

$update_delivered_date = Qube::GetPDO()->prepare('UPDATE ' . QubeDriver::getTableName('ScheduledResponse') . ' SET delivered_date = NOW() 
        WHERE contactdata_id = ? AND responder_id = ? AND delivered_date = 0 LIMIT 1');

$PendingNotifications = $Q->Exec()->fetchAll();

//  load the template processor
$Smarty     =   new QubeSmarty();

foreach($PendingNotifications as $ScheduledResponse)
{
//    deb($ScheduledResponse);
    
    // @ contactdata object (requires user_id)

    $Responder = $ScheduledResponse->convertToModel('Responder');

    $event = new ResponseTriggerEvent(
            $ScheduledResponse->convertToModel('User'),
            $Mailer, 
            new SendTo('name', 'email'),
            $ScheduledResponse->contactdata_table
    );
    
    if(!$ScheduledResponse->lead_form_id) /* if contactData exists */
    {
        // @todo create a view to determine responders with missing data!
        $form_data  = $ScheduledResponse->convertToModel('ContactData');
        $sendTo     = new SendTo($form_data->name, $form_data->email);        
        $event->setRecipient($sendTo);
        
    }else{
        
        if($ScheduledResponse->lead_form_id)
        {
            /** Process Lead Responder **/
            
            /**
             * returns [fields] => (assoc-array), [name] => .., [email] => ..
             * 
             */
            $leadInfo   = Hub::leadInfoFromData($ScheduledResponse->data, NULL, 1);
            $sendTo     = new SendTo($leadInfo['name'], $leadInfo['email']);
            $event->setRecipient($sendTo);
            $event->setData($leadInfo);
            
        }else{
    // @log            throw new Exception('something is completely fuxed. ')   not a lead-type responder or contact-type responder.. then what is it? :S
        }
    }

    /** 2.      **/
    $Notification = $Responder->createNotification($event);
    
    
//    deb($ScheduledResponse);
    $vars = array('main_site' => $ScheduledResponse->main_site,
        
        
        // this stuff is so arbritrary its freaking insane!
        // i'm still not 100% sure what the pid variable comes from used to generate the opt-out link
        
        // this current value passed is the id of the contactdata record (could be on leads table or contact_forms table)
        // this is the most logical value that I makes sense to me
        // after all, the contact record contains the email and other columns such as optout
        // however, I found the code that actually processes the optout form page
        // inside a themes/ directory, which makes me think that the same code is
        // distributed across the different themes rendering any modifications to that code
        // quite unpredictable due to its redundancy across the themes! yuck
        
                    'prospect_id' => $ScheduledResponse->contactdata_id);
    
    if($Responder->hasTheme())
    {
        $Email      = new ThemedEmailNotification;
        if(!array_key_exists($Responder->theme, $themes))
            $themes[$Responder->theme] = $Responder->getTheme();

        $Theme = &$themes[$Responder->theme];
        $Theme->setVariables($vars);
        $Notification->setTheme($Theme);
    }

//    deb($Notification, $event);
    
    /** 3 */
    if($Mailer->Send_Old($Notification))
    {
        // @todo @idea.. this could also be compiled into one OR condition possibly using the DBWhereExpression
        //      so that we dont execute the same statement 'hundreds' of times
        // 
        $update_delivered_date->execute(array($ScheduledResponse->contactdata_id, $ScheduledResponse->responder_id));
    }else{
        mail('diag@sofus.mx', 'Cron Email Failed',
                    print_r($Notification, true), print_r($ScheduledResponse, true));
        
    }
    
    // @todo maybe log the number of failed send attempts in the future? 
}

echo 'done';