<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');


//This script is run once per day and combs through the users table
//looking for free trial users (of class 421) to disable ones that have expired
require_once(QUBEADMIN . 'inc/local.class.php');
$local = new Local();
$query = "SELECT id, username, created FROM users WHERE class = 421 AND access = 1";
if($users = $local->query($query)){
	while($user = $local->fetchArray($users)){
		//check how long ago the account was created
		$timeSinceCreation = time()-strtotime($user['created']);
		if($timeSinceCreation>=2592000){
			//equal to or greater than 30 days, disable the account and email user
			$query = "UPDATE users SET access = 0 WHERE id = '".$user['id']."' LIMIT 1";
			$local->query($query);
			$sendEmail = true;
			$subject = 'Your Free 30-Day Trial Of 6Qube Has Expired!';
			$message = 
			'<h1 style="font-family:Arial,helvetica,sans-serif;">Your Free 30-Day Trial Of 6Qube Has Expired!</h1>
			<p style="font-family:Arial,helvetica,sans-serif;"><strong>Don\'t panic</strong>, none of your information or items have been deleted (yet).  You can still <a href="https://login.6qube.com">log in to your account</a> with limited access which gives you the ability to upgrade to one of our paid plans and keep all your stuff.</p><br />
			<p style="font-family:Arial,helvetica,sans-serif;"><strong>Didn\'t like it?</strong><br />If your free trial wasn\'t satisfactory or 6Qube wasn\'t for you, just disregard this email and your account and information will be purged after 30 days.</p>';
		}
		else if((30-(ceil($timeSinceCreation/86400)))==3){
			//notify the user their account expires in 3 days
			$sendEmail = true;
			$subject = 'Your Free 30-Day Trial Of 6Qube Expires In 3 Days!';
			$message = 
			'<h1 style="font-family:Arial,helvetica,sans-serif;">Your Free 30-Day Trial Of 6Qube Expires In 3 Days!</h1>
			<p style="font-family:Arial,helvetica,sans-serif;"><strong>We just wanted to let you know.</strong><br />Hopefully you\'re enjoying everything 6Qube has to offer and you\'ll <a href="https://login.6qube.com">log in to your account</a> to upgrade it today!</p>';
		}
		
		if($sendEmail){
			$to = $user['username'];
			$from = 'no-reply@6qube.com';
			$local->sendMail($to, $subject, $message, NULL, $from, NULL, 1);
		}
		
		$timeSinceCreation = $query = $sendEmail = $subject = $message = NULL; //clear vars
	}
}
?>