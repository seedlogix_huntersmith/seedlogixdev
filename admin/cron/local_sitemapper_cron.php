<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

require_once(QUBEADMIN . 'inc/directory.class.php');
$dir = new Dir();

//First run sitemapping for 6qube:
runSitemapping('6qube.com');
//Now get reseller main sites and run sitemapping for them
$query = "SELECT id, admin_user, domain FROM resellers_network WHERE type = 'whole' AND domain != '' AND setup = 1";
if($result = $dir->query($query))
if($dir->numRows($result)){
	while($row = $dir->fetchArray($result)){
		runSitemapping($row['domain'], $row['admin_user']);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Start Sitemapping													  //
/////////////////////////////////////////////////////////////////////////////////////////
function runSitemapping($domain, $resellerID = ''){
	$dir = new Dir();
	$limit = 10;
	$criteria = " WHERE trashed = '0000-00-00' 
		  AND `company_name` != ''
		  AND `phone` != ''
		  AND `street` != ''
		  AND `city` != ''
		  AND `city` != ' '
		  AND `city` != '0'
		  AND `state` != ''
		  AND `state` != ' '
		  AND `state` != '0'
		  AND `category` != '0'
		  AND `category` != ''
		  AND `website_url` != ''
		  AND `display_info` != ''
		  AND `keyword_one` != '' ";
	
	$base = '<?xml version="1.0" encoding="UTF-8"?>
		   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
		   <url>
			 <loc>http://local.'.$domain.'/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>daily</changefreq>
			 <priority>1.0</priority>
		   </url>
		   <url>
			 <loc>http://local.'.$domain.'/cities/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>weekly</changefreq>
			 <priority>0.6</priority>
		   </url>
		   ';
	$end = "</urlset>";
	
	//Get cities
	$query = "SELECT id, COUNT(type), city, state FROM directory";
	$query .= $criteria;
	if($resellerID) $query .= " AND user_parent_id = ".$resellerID;
	$query .= " GROUP BY city, state";
	$cities = $dir->query($query);
	while($row = $dir->fetchArray($cities)){
		$city = $row['city'];
		$state = $row['state'];
		if($major = $dir->majorCity($city)){
			if($state == $major)
				$url = 'http://local.'.$domain.'/'.$dir->convertKeywordUrl($city);
			else
				$url = 'http://local.'.$domain.'/'.$dir->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		}
		else
			$url = 'http://local.'.$domain.'/'.$dir->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		$add .= '
			   <url>
				 <loc>'.$url.'</loc>
				 <lastmod>'.date('Y-m-d').'</lastmod>
				 <changefreq>daily</changefreq>
				 <priority>0.8</priority>
			   </url>
			   ';
		//add pages for city
		$numResults = $row['COUNT(type)'];
		if($numResults) $numPages = ceil($numResults/$limit);
		for($i=$numPages; $i>1; $i--){
			$add .= '
				   <url>
					 <loc>'.$url.$i.'/</loc>
					 <lastmod>'.date('Y-m-d').'</lastmod>
					 <changefreq>weekly</changefreq>
				   </url>
				   ';
		}
		$numResults = $numPages = 0;
			   
		//Get categories for each city
		$query2 = "SELECT id, COUNT(type), category FROM directory";
		$query2 .= $criteria;
		if($resellerID) $query2 .= " AND user_parent_id = ".$resellerID;
		$query2 .= " AND city = '".$city."'";
		$query2 .= " AND state = '".$state."'";
		$query2 .= " GROUP BY category";
		$cats = $dir->query($query2);
		while($row2 = $dir->fetchArray($cats)){
			$a = explode("->", $row2['category']);
			$category = $dir->convertKeywordUrl($a[1]);
			$add .= '
				   <url>
					 <loc>'.$url.$category.'</loc>
					 <lastmod>'.date('Y-m-d').'</lastmod>
					 <changefreq>weekly</changefreq>
					 <priority>0.7</priority>
				   </url>
				   ';
			//add pages for city+category
			$numResults = $row2['COUNT(type)'];
			$numPages = ceil($numResults/$limit);
			for($i=$numPages; $i>1; $i--){
				$add .= '
					   <url>
						 <loc>'.$url.$category.$i.'/</loc>
						 <lastmod>'.date('Y-m-d').'</lastmod>
						 <changefreq>weekly</changefreq>
					   </url>
					   ';
			}
			$numResults = $numPages = 0;
		}
	}
	
	//Now all of the listings
	$query3 = "SELECT id, company_name, keyword_one, last_edit FROM directory ";
	$query3 .= $criteria;
	if($resellerID) $query3 .= " AND user_parent_id = ".$resellerID;
	$listings = $dir->query($query3);
	while($row3 = $dir->fetchArray($listings)){
		$listingUrl = 'http://local.'.$domain.'/'.$dir->convertKeywordUrl($row3['keyword_one']).
					$dir->convertKeywordUrl($row3['company_name']).$row3['id'].'/';
		$add .= '
			   <url>
				 <loc>'.$listingUrl.'</loc>
				 <lastmod>'.date('Y-m-d', strtotime($row3['last_edit'])).'</lastmod>
				 <changefreq>monthly</changefreq>
			   </url>
			   ';
	}
	//add pages for root site
	$numResults = $dir->numRows($listings);
	$numPages = ceil($numResults/$limit);
	for($i=$numPages; $i>1; $i--){
		$add .= '
			   <url>
				 <loc>http://local.'.$domain.'/'.$i.'/</loc>
				 <lastmod>'.date('Y-m-d').'</lastmod>
				 <changefreq>daily</changefreq>
			   </url>
			   ';
	}
	
	$data = $base.$add.$end;
	
	if($domain=='6qube.com')
		$sitemapFile = QUBEROOT . 'local/sitemap.xml';
	else
		$sitemapFile = QUBEROOT . 'local/sitemap_'.$domain.'.xml';
	
	$f = fopen($sitemapFile, 'w');
	fwrite($f, $data);
	fclose($f);
} //end function runSitemapping()
?>