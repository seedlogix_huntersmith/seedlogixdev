<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

//This script is run once a day at noon CST
//It goes through the resellers_billing table and processes charges to resellers.

require_once(QUBEADMIN . 'inc/reseller.class.php');
require_once(QUBEADMIN . 'inc/Authnet.class.php');
$reseller = new Reseller();

//get items that are scheduled to be processed and haven't been attempted
$query = "SELECT * FROM resellers_billing 
		WHERE resellers_billing.sched = 1 
		AND (resellers_billing.sched_attempted = 0 OR resellers_billing.sched_reattempt > 0) 
		AND resellers_billing.success = 0 
		AND (SELECT users.access FROM users WHERE users.id = resellers_billing.user_id) = 1
		AND (SELECT users.canceled FROM users WHERE users.id = resellers_billing.user_id) = 0 
		AND resellers_billing.amount != 0.00 ";
$billItems = $reseller->query($query);

if($billItems && $reseller->numRows($billItems)){
	//loop through items to process
	while($item = $reseller->fetchArray($billItems)){
	//instantiate Authnet class here so it's fresh for each item
	$anet = new Authnet();
	//check to see if today is the day for billing
	$today = date('Y-m-d');
	$schedDate = substr($item['sched_date'], 0, 10);
	if((strtotime($today) >= strtotime($schedDate)) || ($item['sched_attempted'] && $item['sched_reattempt'])){
		$echo .= '<br /><b>Attempting billing for id#'.$item['id'].'</b><br />';
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////
		//if item is a reseller's monthly bill
		///////////////////////////////////////////
		if($item['transaction_type']=='6qubeMonthly'){
			$echo .= 'Item is reseller #'.$item['user_id'].'\'s monthly bill for $'.$item['amount'].'<br />';
			$process = $anet->chargeCIM($item['user_id'], NULL, NULL, $item['amount'], '6qubeMonthly', 0);
			if($process['success']){
				$echo .= '<b>Item was successfully charged!</b><br />';
				//if item was successfully processed update table row
				$query = "UPDATE 
							resellers_billing 
						SET
							transaction_id = '".$process['message']."', 
							method_identifier = '".$process['profile_id']."', 
							processor = '6qube', 
							success = 1, 
							sched_attempted = 1,
							timestamp = NOW()
						WHERE 
							id = ".$item['id'];
				$reseller->query($query);
				//schedule next month's billing
				$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
				$nextSched = $reseller->scheduleTransaction(NULL, $item['user_id'], NULL, NULL, '6qubeMonthly', $item['amount'], 
												   '6qube', 'Scheduled monthly billing for reseller account', 
												   $nextBillDate, 2);
				if($nextSched){
					$echo .= 'Next month\'s billing was successfully scheduled.<br /><br />';
				}
				else {
					$echo .= 'Error scheduling next month\'s billing:<br />'.mysqli_error().'<br /><br />';
				}
			}
			else {
				$echo .= '<b>Billing failed! Details:</b><br />'.$process['message'].'<br />';
				//if item failed billing update table row
				$query = "UPDATE 
							resellers_billing 
						SET
							method_identifier = '".$process['profile_id']."', 
							processor = '6qube', 
							success = 0, 
							note = '".addslashes($process['message'])."', 
							sched_attempted = 1, ";
				if($item['sched_reattempt'])
				//if this isn't the first time trying to bill them
				$query .= 	" sched_reattempt = ".($item['sched_reattempt']-1);
				else
				//otherwise set it to try 2 more times
				$query .= 	" sched_reattempt = 2";
				$query .=		", timestamp = NOW() ";
				$query .=	" WHERE 
							id = '".$item['id']."'";
				if($reseller->query($query)){
					$echo .= 'Reattempt scheduled<br />';
				}
				else $echo .= 'Error scheduling reattempt:<br />'.mysqli_error().'<br />';
				
				//get their info for email
				$query = "SELECT username FROM users WHERE id = ".$item['user_id'];
				$user = $reseller->queryFetch($query);
				$profile = $anet->getPaymentProfile($item['user_id']);
				
				//if this was their last re-billing attempt
				if($item['sched_reattempt']==1){
					$echo .= 'This was the final billing attempt. Disabling user\'s access...<br /><br />';
					//disable access to reseller's account
					$query = "UPDATE users SET access = '-1' WHERE id = ".$item['user_id'];
					$reseller->query($query);
					//disable their users' access
					//(commented out here --  will set up option in 6qube Admin to do this manually)
					//$query = "UPDATE users SET access = 0 WHERE parent_id = ".$item['user_id'];
					//$reseller->query($query);
					
					//set up email
					$subject = "6Qube Monthly Billing Failed - Final Attempt";
					$message = 
					'<h1>Your monthly 6Qube billing failed</h1>
					We regret to inform you that billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed for the third time in a row.  <b>Access to your users\' accounts will be disabled shortly</b>.<br /><br />
					<h2>Failure Details:</h2>
					'.$process['message'].'<br /><br />
					Please contact us at 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a> to correct the situation.';
				} //end if($item['sched_reattempt']==1)
				//otherwise send them a failed biling email
				else {
					$echo .= 'First failed billing attempt for this month. Rescheduled for 2 more attempts.<br /><br />';
					//set up email
					if(!$item['sched_reattempt']) $attemptNum = "First Attempt";
					else if($item['sched_reattempt']==2) $attemptNum = "Second Attempt";
					$subject = "6Qube Monthly Billing Failed - ".$attemptNum;
					$message = 
					'<h1>Your monthly 6Qube billing failed</h1>
					We regret to inform you that the billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed. You and your users can still access your accounts for now, however we will automatically attempt to bill you again tomorrow and if processing fails three times your account and your users\' accounts will be disabled.<br /><br />
					<h2>Failure Details:</h2>
					'.$process['message'].'<br /><br />
					<b>To avoid another failed billing attempt please make sure your card has sufficient funds and update your billing profile if your payment information has changed.</b><br />
					To do this, log in and go to the Reseller tab, then Settings on the left, then go to the Billing Info section.<br /><br />
					If you need assistance please call us as 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a>';
				} //end else (wasn't the final attempt)
				//send email (admin cc'd)
				$to = $user['username'];
				$to2 = $to.', admin@6qube.com';
				$headers = array('From' => 'admin@6qube.com',
							  'To' => $to,
							  'Cc' => 'admin@6qube.com', 
							  'Subject' => $subject);
				$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
			} //end else (processing failed)
			//CLEAR VARIABLES
			$process = $nextBillDate = $nextSched = $user = $profile = $subject = 
			$message = $attemptNum = $to = $to2 = $headers = NULL;
		} //end if($item['transaction_type']=='6qubeMonthly')
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		///////////////////////////////////////////
		//if item is a user's monthly billing
		///////////////////////////////////////////
		else if($item['transaction_type']=='monthlySubscription'){
			$echo .= 'Item is a user\'s monthly billing:<br />
				User ID: '.$item['user_id'].'<br />
				Reseller ID: '.$item['user_parent_id'].'<br />
				Amount: $'.$item['amount'].'<br />
				Product ID #'.$item['product_id'].'<br />';
			//determine whether to use reseller's merchant or 6qube's
			$query = "SELECT company, support_email, support_phone, payments, payments_acct, payments_acct2 FROM resellers 
					WHERE admin_user = '".$item['user_parent_id']."'";
			$resellerInfo = $reseller->queryFetch($query);
			if($resellerInfo['payments']=='authorize' || '6qube'){
				if($resellerInfo['payments']=='authorize'){
					$echo .= 'User\'s parent is using their own Authorize.net merchant account<br />';
					//if reseller is using their own Anet merchant account get creds
					$apiLogin = $resellerInfo['payments_acct'];
					$apiAuth = $resellerInfo['payments_acct2'];
					$processor = 'authorize';
				}
				//otherwise 6Qube is used by default
				else {
					$echo .= 'User\'s parent is using 6Qube\'s Authorize.net merchant account<br />';
					$processor = '6qube';
				}
				//attempt billing
				$process = $anet->chargeCIM($item['user_id'], $item['user_parent_id'], $item['product_id'], $item['amount'], 'monthlySubscription', 0, NULL, $apiLogin, $apiAuth);
				if($process['success']){
					$echo .= '<b>Billing was successful!</b><br />';
					//if successful update table row
					$query = "UPDATE resellers_billing 
							SET 
								transaction_id = '".$process['message']."', 
								method_identifier = '".$process['profile_id']."', 
								processor = '".$processor."', 
								success = 1, 
								sched_attempted = 1, 
								timestamp = NOW() 
							WHERE id = '".$item['id']."'";
					if($reseller->query($query)){
						$echo .= 'Table row successfully updated<br />';
					}
					else $echo .= 'Error updating table row:<br />'.mysqli_error().'<br />';
					//schedule next month's billing
					$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
					$nextSched = $reseller->scheduleTransaction(NULL, $item['user_id'], $item['user_parent_id'], 
											$item['product_id'], 'monthlySubscription', $item['amount'], $processor, 
											'Scheduled monthly billing for '.date('F', strtotime($nextBillDate)),
											$nextBillDate, 1);
					if($nextSched) $echo .= 'Next month\'s billing successfully scheduled<br />';
					else $echo .= 'Error scheduling next month\'s billing:<br />'.mysqli_error().'<br />';
					//charge reseller Wholesale if they're using their own merchant account
					if($processor=='authorize'){
						//get wholesale amount
						$wholesaleAmount = $reseller->getMonthlyWholesale($item['product_id']);
						$wholesaleBill = $anet->chargeCIM($item['user_parent_id'], NULL, $item['product_id'], $wholesaleAmount, 'wholesale', 1, 'Monthly wholesale charge for user\'s account type', NULL, NULL, $item['user_id']);
						$echo .= 'Attempting to bill user\'s parent for wholesale amount of $'.$wholesaleAmount.'...<br />';
						if(!$wholesaleBill['success']){
							$echo .= '<b>Error billing reseller for wholesale amount! Details:</b><br />
								'.$wholesaleBill['message'].'<br /><br />';
							//if charging reseller wholesale amount failed schedule reattempt and email them
							$reattemptDate = date('Y-m-d H:i:s', strtotime('+1 day'));
							$reseller->scheduleTransaction($wholesaleBill['log_id'], NULL, NULL, NULL, NULL, 
													 NULL, NULL, NULL, $reattemptDate);
							//set up email
							$profile = $anet->getPaymentProfile($item['user_parent_id']);
							$to = $resellerInfo['support_email'];
							$subject = "Monthly Wholesale Billing For A Product Failed";
							$message =
							'<h1>Monthly wholesale billing failed</h1>
							You\'re receiving this email because we successfully charged one of your customers their monthly subscription amount on your behalf, but when we attempted to bill you for our wholesale amount there was an error.<br />
							<h2>Details</h2>
							<b>User ID:</b> '.$item['user_id'].'<br />
							<b>Amount:</b> $'.$wholesaleAmount.'<br />
							<b>Error:</b><br />'.$wholesaleBill['message'].'<br />
							<b>Last 4 of your card:</b> '.substr($profile['info']['cardNum'], -4).'<br /><br />
							Our system will automatically reattempt billing tomorrow.  To avoid your user\'s account being disabled (and possibly yours) please make sure you have sufficient funds in your account, or if your payment information has changed update your payment profile.<br />
							If you need assistance please call us as 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a>.';
							$to2 = $to.', admin@6qube.com';
							$headers = array('From' => 'admin@6qube.com',
										  'To' => $to,
										  'Cc' => 'admin@6qube.com', 
										  'Subject' => $subject);
							$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
						} //end if(!$wholesaleBill['success'])
						else $echo .= '<b>Successfully billed reseller for wholesale amount!</b><br /><br />';
					} //end if($processor=='authorize')
					else if($processor=='6qube'){
						//if reseller is using 6qube's processor log the credit
						$reseller->logCredit($item['user_parent_id'], $item['user_id'], $item['product_id'], 
										 'monthlySubscription', $item['amount']);
						$echo .= '<br /><br />';
					}
				} //end if($process['success'])
				else {
					$echo .= '<b>Billing failed! Details:</b><br />
						'.$process['message'].'<br />';
					//billing failed, schedule reattempt and email customer/reseller
					//get customer's email
					$query = "SELECT username FROM users WHERE id = ".$item['user_id'];
					$user = $reseller->queryFetch($query);
					$profile = $anet->getPaymentProfile($item['user_id']);
					//email vars
					$to = $user['username'];
					$from = '"'.$resellerInfo['company'].'" <'.$resellerInfo['support_email'].'>';
					
					//if this was their only reattempt and it failed
					if($item['sched_reattempt']==1){
						$echo .= 'This was the user\'s second failed billing attempt. Disabling account access...<br /><br />';
						$query = "UPDATE 
									resellers_billing 
								SET
									method_identifier = '".$process['profile_id']."', 
									success = 0, 
									note = '".addslashes($process['message'])."', 
									sched_attempted = 1, 
									sched_reattempt = 0, 
									timestamp = NOW() 
								WHERE 
									id = ".$item['id'];
						$reseller->query($query);
						
						//set up email
						$subject = "Monthly Billing Failed Again - Account Access Suspended";
						$message =
						'<h1>Monthly billing failed</h1>
						We regret to inform you that billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed for the second time.  Access to your account has been suspended until you resolve the issue.<br /><br />
						<h2>Details</h2>
						'.stripslashes($process['message']).'
						<br /><br />
						Please contact us at ';
						if($resellerInfo['support_phone']) $message .= $resellerInfo['support_phone'].' or ';
						$message .=
						'<a href="mailto:'.$resellerInfo['support_email'].'">'.$resellerInfo['support_email'].'</a>.';
					} //end if($item['sched_reattempt']==1)
					else {
						$echo .= 'First failed billing attempt. Scheduling for one more attempt in 2 days...<br /><br />';
						$nextAttempt = date('Y-m-d H:i:s', strtotime('+2 days'));
						$query = "UPDATE 
									resellers_billing 
								SET
									method_identifier = '".$process['profile_id']."', 
									success = 0, 
									note = '".addslashes($process['message'])."', 
									sched_date = '".$nextAttempt."',
									sched_attempted = 1, 
									sched_reattempt = 1, 
									timestamp = NOW() 
								WHERE 
									id = ".$item['id'];
						$reseller->query($query);
						
						//set up email
						$subject = "Monthly Billing Failed";
						$message =
						'<h1>Monthly billing failed</h1>
						We regret to inform you that billing for your scheduled monthly payment of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> has failed.<br /><br />
						<h2>Details</h2>
						'.stripslashes($process['message']).'
						<br /><br />
						Our system will automatically reattempt billing in 2 days.  Please make sure you have sufficient funds in your account and update your billing profile in our system if your payment information has changed.<br />
						Contact us at ';
						if($resellerInfo['support_phone']) $message .= $resellerInfo['support_phone'].' or ';
						$message .=
						'<a href="mailto:'.$resellerInfo['support_email'].'">'.$resellerInfo['support_email'].'</a> if you need assistance.';
					} //end else [billing failed and it was the first attempt]
					$to2 = $to.', '.$from;
					$headers = array('From' => $from,
								  'To' => $to,
								  'Cc' => $from, 
								  'Subject' => $subject);
					$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
				} //end else [billing failed]
			} //end if($resellerInfo['payments']=='authorize' || '6qube')
			else if($resellerInfo['payments']=='paypal'){
				$echo .= 'User\'s parent is using Paypal for processing<br /><br />';
				///////////////////////
				//paypal goes here
				///////////////////////
			}
			//CLEAR VARIABLES
			$resellerInfo = $process = $apiLogin = $apiAuth = $processor = $nextBillDate = $nextSched = 
			$nextAttempt = $user = $profile = $to = $from = $subject = $message = $headers = NULL;
		} //end else if($item['transaction_type']=='monthlySubscription')
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////
		//if item is a failed wholesale billing
		//that was rescheduled OR monthly wholesale
		//billing for a reseller using Other processor
		///////////////////////////////////////////
		else if($item['transaction_type']=='wholesale'){
			$echo .= 'Item is a wholesale billing that was scheduled:<br />
				For User ID: '.$item['user_id2'].'<br />
				Reseller ID: '.$item['user_id'].'<br />
				Amount: $'.$item['amount'].'<br />
				Product ID #'.$item['product_id'].'<br />';
			//attempt billing
			$process = $anet->chargeCIM($item['user_id'], NULL, $item['product_id'], $item['amount'], 'wholesale', 0, NULL, NULL, NULL, $item['user_id2']);
			
			if($process['success']){
				$echo .= '<b>Billing was successful!</b><br /><br />';
				//if reattempt was successful update table row
				$query = "UPDATE resellers_billing 
						SET 
							transaction_id = '".$process['message']."', 
							method_identifier = '".$process['profile_id']."', 
							processor = '".$processor."', 
							success = 1, 
							sched_attempted = 1, 
							timestamp = NOW() 
						WHERE id = '".$item['id']."'";
				$reseller->query($query);
				//schedule the charge to repeat next month.
				$query = "SELECT payments FROM resellers WHERE admin_user = '".$item['user_id']."'";
				$resellerInfo = $reseller->queryFetch($query);
				if(strpos($item['note'], 'OtherRecur')!==false){
					$echo .= 'OtherRecur detected, scheduling next month... ';
					$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
					$nextSched = $reseller->scheduleTransaction(NULL, $item['user_id'], NULL, $item['product_id'], 
																'wholesale', $item['amount'], NULL, 
																 "Monthly wholesale charge for user account type - OtherRecur", 
																 $nextBillDate, 2, $item['user_id2']);
					if($nextSched) $echo .= '<strong>Success!</strong><br /><br />';
					else $echo .= '<strong>Error:</strong><br />'.mysqli_error().'<br /><br />';
				}
			} //end if($process['success'])
			else {
				$echo .= '<b>Billing failed! Details:</b><br />
					'.$process['message'].'<br />';
				//update table row
				$query = "UPDATE 
							resellers_billing 
						SET
							method_identifier = '".$process['profile_id']."', 
							success = 0, 
							note = '".addslashes($process['message'])."', 
							sched_attempted = 1, 
							sched_reattempt = '";
							if($item['sched_reattempt'])
							$query .= ($item['sched_reattempt']-1);
							else 
							$query .= 0;
				$query .=	"', 
							timestamp = NOW() 
						WHERE 
							id = ".$item['id'];
				$reseller->query($query);
				
				$query = "SELECT username FROM users WHERE id = ".$item['user_id'];
				$resellerInfo2 = $reseller->queryFetch($query);
				$profile = $anet->getPaymentProfile($item['user_id']);
				
				//if there were 0 reattempts left
				if($item['sched_reattempt']<=1){
					//disable reseller client's account
					if($item['user_id2']){
						$query = "UPDATE users SET access = 0 WHERE id = ".$item['user_id2'];
						$reseller->query($query);
					}
					
					//email reseller/6qube admin
					$message =
					'<h1>Wholesale billing reattempt failed</h1>
					You\'re receiving this email because when we reattempted to bill you for our wholesale cost of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> there was an error.  Since there are no re-attempts remaining your user\'s account has been temporarily disabled.  Please call us as 877-570-5005 or <a href="mailto:admin@6qube.com">email us</a> to resolve the situation as soon as possible.<br />';
				} //end if(!$item['sched_reattempt'])
				else if($item['sched_reattempt']==3) {
					//if this was the first attempt
					$message =
					'<h1>Wholesale billing Attempt failed</h1>
					You\'re receiving this email because when we attempted to bill you for our wholesale cost of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> there was an error.  Our system will automatically reattempt this billing one more time tomorrow.  Please make sure your account has sufficient funds and/or any errors have been corrected.<br />';
				}
				else {
					//one more attempt remaining
					$message =
					'<h1>Wholesale billing reattempt failed</h1>
					You\'re receiving this email because when we reattempted to bill you for our wholesale cost of <b>$'.$item['amount'].'</b> to your card ending in <b>'.substr($profile['info']['cardNum'], -4).'</b> there was an error.  There is one reattempt remaining, scheduled for tomorrow.  Please make sure your account has sufficient funds and/or any errors have been corrected.<br />';
				}
				$to = $resellerInfo2['username'];
				$to2 = $to.', admin@6qube.com';
				$subject = "Wholesale Billing Attempt Failed";
				$message .= '<h2>Details</h2>'.$process['message'];
				$headers = array('From' => 'admin@6qube.com',
							  'To' => $to,
							  'Cc' => 'admin@6qube.com', 
							  'Subject' => $subject);
				$reseller->sendMail($to2, $subject, $message, $headers, NULL, NULL, TRUE);
			} //end else [billing reattempt failed]
		} //end else if($item['transaction_type']=='wholesale')
		
		//clear vars for next iteration
		$process = $processor = $user = $resellerInfo = $profile = $message = $subject = 
		$to = $headers = $wholesaleAmount = $wholesaleBill = $nextSched = NULL;
		//CLEAR VARIABLES
		$process = $resellerInfo = $nextBillDate = $resellerInfo2 = $profile = $message = $to = $to2 = $subject = $headers = NULL;
	} //end if($item['transaction_type']=='wholesale')
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	else if(date('Y-m-d', strtotime('+3 days')) == $schedDate){
		//send courtesy email reminding user they will be charged in 3 days
		if($item['transaction_type']=='monthlySubscription'){
			//get reseller info
			/////$query = "SELECT company, support_email, support_phone FROM resellers 
					/////WHERE admin_user = '".$item['user_parent_id']."'";
			/////$resellerInfo = $reseller->queryFetch($query);
			//get customer's email
			//////$query = "SELECT username FROM users WHERE id = ".$item['user_id'];
			/////$user = $reseller->queryFetch($query);
			//email vars
			/////$to = $user['username'];
			/////$from = '"'.$resellerInfo['company'].'" <'.$resellerInfo['support_email'].'>';
			/////$subject = 'Reminder - Monthly Billing Will Be Charged In 3 Days';
			/////$message = 
			/////'<h1>Your monthly subscription will renew in 3 days</h1>
			/////You\'re receiving this email as a reminder that your monthly subscription amount of $'.$item['amount'].' is set to automatically renew in 3 days.  Please make sure you have sufficient funds and your billing information is up to date in our system.<br /><br />
			/////If you need any assistance just reply to this email';
			/////if($resellerInfo['support_phone']) $message .=  ' or give us a call at '.$resellerInfo['support_phone'];
			/////$message .= '.';
			
			/////$headers = array('From' => $from,
						 ///// 'To' => $to,
						 ///// 'Subject' => $subject);
			/////$reseller->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
			//CLEAR VARIABLES
			////$resellerInfo = $user = $to = $from = $subject = $message = NULL;
		} //end if($item['transaction_type']=='monthlySubscription')
	} //end else if(date('Y-m-d', strtotime('+3 days')) == $schedDate)
	} //end while($item = $reseller->fetchArray($billItems))
} //end if($billItems && $reseller->numRows($billItems))
else 
	$echo .= 'No items to process.';

if($echo){
	echo $echo;
	//email Reese output for testing
	$to = 'admin@6qube.com';
	$from = 'noreply@6qube.com';
	$subject = 'reseller_billing_cron run results: '.date('Y-m-d H:i');
	$message = 
	'The Resellers Billing cron script was run at '.date('H:i:s').'CST on '.date('Y-m-d').'.  Here\'s the output:<br /><br />'.$echo;
	$headers = array('From' => $from,
				  'To' => $to,
				  'Subject' => $subject);
	$reseller->sendMail($to, $subject, $message, $headers, NULL, NULL, TRUE);
}
?>