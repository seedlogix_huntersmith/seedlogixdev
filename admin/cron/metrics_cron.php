<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');

	require_once(QUBEADMIN . 'inc/reseller.class.php');
	$reseller = new Local();
	
////////////////////////////////////////////////////////////
// USERS SECTION
////////////////////////////////////////////////////////////
	//# Users
	$query = "SELECT count(id) FROM users";
	$a = $reseller->queryFetch($query);
	$totalNumUsers = $a['count(id)'];
	//# Users access=1
	$query = "SELECT count(id) FROM users WHERE access = 1";
	$b = $reseller->queryFetch($query);
	$totalNumUsers2 = $b['count(id)'];
	//# Reseller clients
	$query = "SELECT count(id) FROM users WHERE reseller_client = 1";
	$c = $reseller->queryFetch($query);
	$resellerClients = $c['count(id)'];
	//# Users logged in last 30days
	$onemonthago = date('Y-m-d', strtotime('-30 days'));
	$today = date('Y-m-d');
	$query = "SELECT count(id) FROM users WHERE date(last_login) BETWEEN '".$onemonthago."' AND '".$today."'";
	$d = $reseller->queryFetch($query);
	$loginsLast30 = $d['count(id)'];
	//Classes
	$query = "SELECT id, name, type FROM user_class";
	$e = $reseller->query($query);
	//Signups last 7 days
	$onedayago = date('Y-m-d', strtotime('-1 day'));
	$query1 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$onedayago."' AND '".$today."'";
	$f1 = $reseller->queryFetch($query1);
	$twodaysago = date('Y-m-d', strtotime('-2 days'));
	$query2 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$twodaysago."' AND '".$onedayago."'";
	$f2 = $reseller->queryFetch($query2);
	$threedaysago = date('Y-m-d', strtotime('-3 days'));
	$query3 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$threedaysago."' AND '".$twodaysago."'";
	$f3 = $reseller->queryFetch($query3);
	$fourdaysago = date('Y-m-d', strtotime('-4 days'));
	$query4 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$fourdaysago."' AND '".$threedaysago."'";
	$f4 = $reseller->queryFetch($query4);
	$fivedaysago = date('Y-m-d', strtotime('-5 days'));
	$query5 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$fivedaysago."' AND '".$fourdaysago."'";
	$f5 = $reseller->queryFetch($query5);
	$sixdaysago = date('Y-m-d', strtotime('-6 days'));
	$query6 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$sixdaysago."' AND '".$fivedaysago."'";
	$f6 = $reseller->queryFetch($query6);
	$sevendaysago = date('Y-m-d', strtotime('-7 days'));
	$query7 = "SELECT count(id) FROM users WHERE date(created) BETWEEN '".$sevendaysago."' AND '".$sixdaysago."'";
	$f7 = $reseller->queryFetch($query7);
	
	$output = 
	'<table width="100%" cellspacing="20" border="0">
		<tr><td colspan="3"><h2>Users</h2></td></tr>
		<tr>
			<td><table border="1" bordercolor="#000000">
				<td><b>Total # Users</b></td>
				<td>'.$totalNumUsers.'</td>
			</table></td>
			<td><table border="1" bordercolor="#000000">
				<td><b>Total # Users, access=1</b></td>
				<td>'.$totalNumUsers2.'</td>
			</table></td>
			<td><table border="1" bordercolor="#000000">
				<td><b>Reseller Clients</b></td>
				<td>'.$resellerClients.'</td>
			</table></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><table border="1" bordercolor="#000000">
				<td><b># Users With Login In Last 30 Days</b></td>
				<td>'.$loginsLast30.'</td>
				<td>~%'.ceil(($loginsLast30/$totalNumUsers)*100).'</td>
			</table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><table border="1" bordercolor="#000000">
				<tr><td colspan="3"><b>Classes</b></td></tr>';
				while($row = $reseller->fetchArray($e)){
					$query = "SELECT count(id) FROM users WHERE class = ".$row['id'];
					$row2 = $reseller->queryFetch($query);
					if($row2['count(id)']>0){
						$output .= '<tr>
									<td>'.$row['id'].' ('.$row['type'].')</td>
									<td>'.$row2['count(id)'].'</td>
									<td>~%'.ceil(($row2['count(id)']/$totalNumUsers)*100).'</td>
								</tr>';
					}
				}
		$output .= '</table></td>
			<td valign="top"><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Signups per day, last 7 days</b></td></tr>
				<tr>
					<td>Today</td>
					<td>'.$f1['count(id)'].'</td>
				</tr>
				<tr>
					<td>Yesterday</td>
					<td>'.$f2['count(id)'].'</td>
				</tr>
				<tr>
					<td>'.date('n/j', strtotime('-2 days')).'</td>
					<td>'.$f3['count(id)'].'</td>
				</tr>
				<tr>
					<td>'.date('n/j', strtotime('-3 days')).'</td>
					<td>'.$f4['count(id)'].'</td>
				</tr>
				<tr>
					<td>'.date('n/j', strtotime('-4 days')).'</td>
					<td>'.$f5['count(id)'].'</td>
				</tr>
				<tr>
					<td>'.date('n/j', strtotime('-5 days')).'</td>
					<td>'.$f6['count(id)'].'</td>
				</tr>
				<tr>
					<td>'.date('n/j', strtotime('-6 days')).'</td>
					<td>'.$f7['count(id)'].'</td>
				</tr>
			</table></td>
		</tr>
	</table>';
	
	$query = "UPDATE metrics SET users = '".mysqli_real_escape_string($output)."' WHERE id = 1";
	$result = $reseller->query($query);
	$output = '';
	//echo mysqli_error().'<br />';
	$a = $b = $c = $d = $e = $f1 = $f2 = $f3 = $f4 = $f5 = $f6 = $f7 = NULL;
	
	
////////////////////////////////////////////////////////////
// INFO FOR RESELLERS SECTION
////////////////////////////////////////////////////////////
	//Resellers
	$query = "SELECT admin_user, main_site, main_site_id, company FROM resellers ORDER BY id ASC";
	$a = $reseller->query($query);
	//More info gathered below for each reseller
	//Rankings
	$query = "SELECT count(id), parent_id FROM users WHERE parent_id != 0 GROUP BY parent_id";
	$z = $reseller->query($query);
	
	$output = 
	'<table width="100%" cellspacing="10" border="0">
		<tr><td><h2>Resellers</h2></td></tr>
		<tr>
			<table>
				<td><table border="1" bordercolor="#000000">
					<tr><td colspan="2"><b>Reseller | # Users</b></td></tr>';
					while($row4 = $reseller->fetchArray($z)){
						$output .= 
						'<tr>
							<td><a href="#'.$row4['parent_id'].'" class="dflt-link external">#'.$row4['parent_id'].'</a></td>
							<td>'.$row4['count(id)'].'</td>
						</tr>';
					}
	$output .= 	'</table></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</table>
		</tr>';
		while($row = $reseller->fetchArray($a)){
			//# users
			$query = "SELECT count(id) FROM users WHERE parent_id = ".$row['admin_user'];
			$b = $reseller->queryFetch($query);
			$totalNumUsers = $b['count(id)'];
			//# users access=1
			$query = "SELECT count(id) FROM users WHERE parent_id = ".$row['admin_user']." AND access = 1";
			$c = $reseller->queryFetch($query);
			$totalNumUsers2 = $c['count(id)'];
			//# Users logged in last 30days
			$onemonthago = date('Y-m-d', strtotime('-30 days'));
			$today = date('Y-m-d');
			$query = "SELECT count(id) FROM users WHERE parent_id = ".$row['admin_user']."
					AND date(last_login) BETWEEN '".$onemonthago."' AND '".$today."'";
			$d = $reseller->queryFetch($query);
			$loginsLast30 = $d['count(id)'];
			//Classes
			$query = "SELECT id, name, type FROM user_class WHERE admin_user = ".$row['admin_user'];
			$e = $reseller->query($query);
			//Signups last 7 days
			$base = "SELECT count(id) FROM users WHERE parent_id = ".$row['admin_user'];
			$onedayago = date('Y-m-d', strtotime('-1 day'));
			$query1 = $base." AND date(created) BETWEEN '".$onedayago."' AND '".$today."'";
			$f1 = $reseller->queryFetch($query1);
			$twodaysago = date('Y-m-d', strtotime('-2 days'));
			$query2 = $base." AND date(created) BETWEEN '".$twodaysago."' AND '".$onedayago."'";
			$f2 = $reseller->queryFetch($query2);
			$threedaysago = date('Y-m-d', strtotime('-3 days'));
			$query3 = $base." AND date(created) BETWEEN '".$threedaysago."' AND '".$twodaysago."'";
			$f3 = $reseller->queryFetch($query3);
			$fourdaysago = date('Y-m-d', strtotime('-4 days'));
			$query4 = $base." AND date(created) BETWEEN '".$fourdaysago."' AND '".$threedaysago."'";
			$f4 = $reseller->queryFetch($query4);
			$fivedaysago = date('Y-m-d', strtotime('-5 days'));
			$query5 = $base." AND date(created) BETWEEN '".$fivedaysago."' AND '".$fourdaysago."'";
			$f5 = $reseller->queryFetch($query5);
			$sixdaysago = date('Y-m-d', strtotime('-6 days'));
			$query6 = $base." AND date(created) BETWEEN '".$sixdaysago."' AND '".$fivedaysago."'";
			$f6 = $reseller->queryFetch($query6);
			$sevendaysago = date('Y-m-d', strtotime('-7 days'));
			$query7 = $base." AND date(created) BETWEEN '".$sevendaysago."' AND '".$sixdaysago."'";
			$f7 = $reseller->queryFetch($query7);
	$output .= '<tr><td style="padding:0;">
			<table border="1" bordercolor="#000000" style="margin-bottom:20px;" id="'.$row['admin_user'].'">
				<tr><td colspan="3" align="center" style="padding:8px;">
					<b>#'.$row['admin_user'].' - '.$row['company'].' - '.$row['main_site'].' (hub id#'.$row['main_site_id'].')</b>
				</td></tr>
				<tr>
					<td style="padding:8px;"><table border="1" bordercolor="#000000">
						<td style="padding:8px;"><b>Total # Users</b></td>
						<td style="padding:8px;">'.$totalNumUsers.'</td>
					</table></td>
					<td style="padding:8px;"><table border="1" bordercolor="#000000">
						<td style="padding:8px;"><b>Total # Users, access=1</b></td>
						<td style="padding:8px;">'.$totalNumUsers2.'</td>
					</table></td>
					<td style="padding:8px;"><table border="1" bordercolor="#000000">
						<td style="padding:8px;"><b># Users Logged In Last 30 Days</b></td>
						<td style="padding:8px;">'.$loginsLast30.'</td>
					</table></td>
				</tr>
				<tr>
					<td colspan="2" style="padding:8px;" align="center"><table border="1" bordercolor="#000000">
						<tr><td colspan="2" style="padding:8px;"><b>Products</b></td></tr>';
						if($e && $reseller->numRows($e)){
						while($row2 = $reseller->fetchArray($e)){
							$query = "SELECT count(id) FROM users WHERE parent_id = ".$row['admin_user']." AND class = ".$row2['id'];
							$row3 = $reseller->queryFetch($query);
							$output .= 
							'<tr>
								<td style="padding:8px;">'.$row2['id'].' ('.$row2['type'].')</td>
								<td style="padding:8px;">'.$row3['count(id)'].'</td>
							</tr>';
						}}
	$output .= 		'</table></td>
					<td valign="top" style="padding:8px;" align="center"><table border="1" bordercolor="#000000">
						<tr><td colspan="2" style="padding:8px;"><b>Signups per day, last 7 days</b></td></tr>
						<tr>
							<td style="padding:8px;">Today</td>
							<td style="padding:8px;">'.$f1['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">Yesterday</td>
							<td style="padding:8px;">'.$f2['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">'.date('n/j', strtotime('-2 days')).'</td>
							<td style="padding:8px;">'.$f3['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">'.date('n/j', strtotime('-3 days')).'</td>
							<td style="padding:8px;">'.$f4['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">'.date('n/j', strtotime('-4 days')).'</td>
							<td style="padding:8px;">'.$f5['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">'.date('n/j', strtotime('-5 days')).'</td>
							<td style="padding:8px;">'.$f6['count(id)'].'</td>
						</tr>
						<tr>
							<td style="padding:8px;">'.date('n/j', strtotime('-6 days')).'</td>
							<td style="padding:8px;">'.$f7['count(id)'].'</td>
						</tr>
					</table></td>
				</tr>
			</table>
		</td></tr>';
		}
	$output .= 
	'</table>';
	
	$query = "UPDATE metrics SET resellers = '".mysqli_real_escape_string($output)."' WHERE id = 1";
	$result = $reseller->query($query);
	$output = '';
	//echo mysqli_error().'<br />';
	$a = $b = $c = $d = $e = $f1 = $f2 = $f3 = $f4 = $f5 = $f6 = $f7 = $z = NULL;
	
	
////////////////////////////////////////////////////////////
// INFO FOR NETWORKS SECTION
////////////////////////////////////////////////////////////
	//Local
	$query = "SELECT admin_user, domain FROM resellers_network WHERE type = 'local' ORDER BY id ASC";
	$a = $reseller->query($query);
	//Hubs
	$query = "SELECT admin_user, domain FROM resellers_network WHERE type = 'hubs' ORDER BY id ASC";
	$b = $reseller->query($query);
	//Blogs
	$query = "SELECT admin_user, domain FROM resellers_network WHERE type = 'blogs' ORDER BY id ASC";
	$c = $reseller->query($query);
	//Articles
	$query = "SELECT admin_user, domain FROM resellers_network WHERE type = 'articles' ORDER BY id ASC";
	$d = $reseller->query($query);
	//Press
	$query = "SELECT admin_user, domain FROM resellers_network WHERE type = 'press' ORDER BY id ASC";
	$e = $reseller->query($query);
	
	$output = 
	'<table width="100%" cellspacing="20" border="0" cols="2">
		<tr><td colspan="2"><h2>Networks</h2></td></tr>
		<tr>
			<td><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Local</b></td></tr>
				<tr><td>Network</td><td># Items</td></tr>
				<tr>
					<td><b>local.6qube.com <small>(all networks)</small></b></td>
					<td>'.$reseller->getListingsForNetwork().'</td>
				</tr>';
				while($row = $reseller->fetchArray($a)){
					$domain = substr($row['domain'], 7);
				if($domain){
					$numItems = $reseller->getListingsForNetwork(NULL, NULL, NULL, NULL, NULL, $row['admin_user']);
	$output .=		'<tr>
						<td><a href="'.$row['domain'].'" target="_blank">'.$domain.'</a></td>
						<td>'.$numItems.'</td>
					</tr>';
				}
				}
	$output .= '</table></td>
			<td><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Hubs</b></td></tr>
				<tr><td>Network</td><td># Items</td></tr>
				<tr>
					<td><b>hubs.6qube.com <small>(all networks)</small></b></td>
					<td>'.$reseller->getHubsForNetwork().'</td>
				</tr>';
				while($row = $reseller->fetchArray($b)){
					$domain = substr($row['domain'], 7);
				if($domain){
					$numItems = $reseller->getHubsForNetwork(NULL, NULL, NULL, NULL, NULL, $row['admin_user']);
	$output .=		'<tr>
						<td><a href="'.$row['domain'].'" target="_blank">'.$domain.'</a></td>
						<td>'.$numItems.'</td>
					</tr>';
				}
				}
	$output .= '</table></td>
		</tr>
		<tr>
			<td><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Blogs</b></td></tr>
				<tr><td>Network</td><td># Items</td></tr>
				<tr>
					<td><b>blogs.6qube.com <small>(all networks)</small></b></td>
					<td>'.$reseller->getPostsForNetwork().'</td>
				</tr>';
				while($row = $reseller->fetchArray($c)){
					$domain = substr($row['domain'], 7);
				if($domain){
					$numItems = $reseller->getPostsForNetwork(NULL, NULL, NULL, NULL, NULL, $row['admin_user']);
	$output .=		'<tr>
						<td><a href="'.$row['domain'].'" target="_blank">'.$domain.'</a></td>
						<td>'.$numItems.'</td>
					</tr>';
				}
				}
	$output .= '</table></td>
			<td><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Press</b></td></tr>
				<tr><td>Network</td><td># Items</td></tr>
				<tr>
					<td><b>press.6qube.com <small>(all networks)</small></b></td>
					<td>'.$reseller->getPressForNetwork().'</td>
				</tr>';
				while($row = $reseller->fetchArray($e)){
					$domain = substr($row['domain'], 7);
				if($domain){
					$numItems = $reseller->getPressForNetwork(NULL, NULL, NULL, NULL, NULL, $row['admin_user']);
	$output .=		'<tr>
						<td><a href="'.$row['domain'].'" target="_blank">'.$domain.'</a></td>
						<td>'.$numItems.'</td>
					</tr>';
				}
				}
	$output .= '</table></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><table border="1" bordercolor="#000000">
				<tr><td colspan="2"><b>Articles</b></td></tr>
				<tr><td>Network</td><td># Items</td></tr>
				<tr>
					<td><b>articles.6qube.com <small>(all networks)</small></b></td>
					<td>'.$reseller->getArticlesForNetwork().'</td>
				</tr>';
				while($row = $reseller->fetchArray($d)){
					$domain = substr($row['domain'], 7);
				if($domain){
					$numItems = $reseller->getArticlesForNetwork(NULL, NULL, NULL, NULL, NULL, $row['admin_user']);
	$output .=		'<tr>
						<td><a href="'.$row['domain'].'" target="_blank">'.$domain.'</a></td>
						<td>'.$numItems.'</td>
					</tr>';
				}
				}
	$output .= '</table></td>
		</tr>
	</table>';
	
	$query = "UPDATE metrics SET networks = '".mysqli_real_escape_string($output)."' WHERE id = 1";
	$result = $reseller->query($query);
	$output = '';
	//echo mysqli_error().'<br />';
	$a = $b = $c = $d = $e = NULL;
	
////////////////////////////////////////////////////////////
// INFO FOR AUTORESPONDERS SECTION
////////////////////////////////////////////////////////////
	//Timed autoresponders
	$query = "SELECT id, user_id, table_name, name, sched, sched_mode, subject 
			FROM auto_responders WHERE active = 1 AND sched > 0
			ORDER BY user_id DESC, id DESC";
	$a = $reseller->query($query);
	//Instant autoresponders
	$query = "SELECT id, user_id, table_name, name, subject FROM auto_responders 
			WHERE active = 1 AND sched = 0 AND sched_mode = 'hours'
			ORDER BY user_id DESC, id DESC";
	$z = $reseller->query($query);
	
	$numTimedResponders = $reseller->numRows($a);
	$numInstantResponders += $reseller->numRows($z);
	$mostSent = 0; $mostSentID = '';
	$mostSent2 = 0; $mostSentID2 = '';
	$mostSent3 = 0; $mostSentID3 = '';
	$mostSent4 = 0; $mostSentID4 = '';
	$mostSent5 = 0; $mostSentID5 = '';
	
	//TIMED
	$output = '<table width="100%" cols="7" border="1" bordercolor="#000000">
				<tr><td colspan="7"><strong>Timed Autoresponders</strong></td></tr>
				<tr>
					<td><strong>ID</strong></td>
					<td><b>User</b></td>
					<td><b>Type</b></td>
					<td><b>Name</b></td>
					<td><b>Send Sched</b></td>
					<td><b>Subject</b></td>
					<td><b>Times Sent</b></td>
				</tr>';
				while($row = $reseller->fetchArray($a)){
					//responder type
					if($row['table_name']=='hub') $type = "Hub";
					else if($row['table_name']=='blogs') $type = "Blog";
					else if($row['table_name']=='directory') $type = "Listing";
					else if($row['table_name']=='articles') $type = "Article";
					else if($row['table_name']=='press_release') $type = "Press Release";
					//send sched
					$sendSched = $row['sched'].' '.$row['sched_mode'];
					//times sent
					$query = "SELECT count(id) FROM contact_form WHERE responses_received 
							LIKE '%::".$row['id']."::%' OR responses_received LIKE '::".$row['id']."'";
					$c = $reseller->queryFetch($query);
					if($c) $timesSent = $c['count(id)']; else $timesSent = 0;
					if($timesSent>$mostSent){ $mostSent = $timesSent; $mostSentID = $row['id']; }
					else if($timesSent>$mostSent2){ $mostSent2 = $timesSent; $mostSentID2 = $row['id']; }
					else if($timesSent>$mostSent3){ $mostSent3 = $timesSent; $mostSentID3 = $row['id']; }
					else if($timesSent>$mostSent4){ $mostSent4 = $timesSent; $mostSentID4 = $row['id']; }
					else if($timesSent>$mostSent5){ $mostSent5 = $timesSent; $mostSentID5 = $row['id']; }
					$output .= 
					'<tr>
						<td>'.$row['id'].'</td>
						<td>#'.$row['user_id'].'</td>
						<td>'.$type.'</td>
						<td>'.$reseller->shortenSummary($row['name'], 20).'</td>
						<td>'.$sendSched.'</td>
						<td>'.$reseller->shortenSummary($row['subject'], 35).'</td>
						<td>'.$timesSent.'</td>
					</tr>';
				}
	$output .= '</table>';
	
	//INSTANT
	$output .= '<br /><br /><br />
			<table width="100%" cols="4" border="1" bordercolor="#000000">
				<tr><td colspan="4"><strong>Instant Autoresponders</strong></td></tr>
				<tr>
					<td><b>User</b></td>
					<td><b>Type</b></td>
					<td><b>Name</b></td>
					<td><b>Subject</b></td>
				</tr>';
				while($row = $reseller->fetchArray($z)){
					//responder type
					if($row['table_name']=='hub') $type = "Hub";
					else if($row['table_name']=='blogs') $type = "Blog";
					else if($row['table_name']=='directory') $type = "Listing";
					else if($row['table_name']=='articles') $type = "Article";
					else if($row['table_name']=='press_release') $type = "Press Release";
					$output .= 
					'<tr>
						<td>#'.$row['user_id'].'</td>
						<td>'.$type.'</td>
						<td>'.$reseller->shortenSummary($row['name'], 40).'</td>
						<td>'.$reseller->shortenSummary($row['subject'], 70).'</td>
					</tr>';
				}
	$output .= '</table>';
	
	$output = '<h2>Autoresponders</h2>
			<strong>Number of timed responders:</strong> '.$numTimedResponders.'<br />
			<strong>Number of instant responders:</strong> '.$numInstantResponders.'<br /><br />
			<table border="1" bordercolor="#000000" cellpadding="10" cols="2">
				<tr><td colspan="2"><strong>5 Most sent responders <small>(timed)</small></strong></td></tr>
				<tr>
					<td>ID</td>
					<td><strong># times sent</strong></td>
				</tr>
				<tr>
					<td>#'.$mostSentID.'</td>
					<td>'.$mostSent.'</td>
				</tr>
				<tr>
					<td>#'.$mostSentID2.'</td>
					<td>'.$mostSent2.'</td>
				</tr>
				<tr>
					<td>#'.$mostSentID3.'</td>
					<td>'.$mostSent3.'</td>
				</tr>
				<tr>
					<td>#'.$mostSentID4.'</td>
					<td>'.$mostSent4.'</td>
				</tr>
				<tr>
					<td>#'.$mostSentID5.'</td>
					<td>'.$mostSent5.'</td>
				</tr>
			</table><br /><br />'.
			$output;
	
	$query = "UPDATE metrics SET autoresponders = '".mysqli_real_escape_string($output)."' WHERE id = 1";
	$result = $reseller->query($query);
	$output = '';
	//echo mysqli_error().'<br />';
	$a = $b = $c = NULL;


////////////////////////////////////////////////////////////
// INFO FOR SITES SECTION
////////////////////////////////////////////////////////////
	//Hubs
	$query = "SELECT id, user_id, name, domain FROM hub WHERE domain != '' ORDER BY user_id DESC, domain DESC";
	$a = $reseller->query($query);
	//Blogs
	$query = "SELECT id, user_id, domain, blog_title FROM blogs WHERE domain != '' ORDER BY user_id DESC, domain DESC";
	$b = $reseller->query($query);
	$numDomains = $reseller->numRows($a);
	$numDomains += $reseller->numRows($b);
	
	//HUBS
	$output = '<h2>Domains</h2>
			<strong>Total number of domains:</strong> '.$numDomains.'<br /><br />
			<table width="100%" cols="4" border="1" bordercolor="#000000">
				<tr><td colspan="4"><strong>Hub Domains</strong></td></tr>
				<tr>
					<td><b>User</b></td>
					<td><b>Hub</b></td>
					<td><b>Hub Name</b></td>
					<td><b>Domain</b></td>
				</tr>';
				while($row = $reseller->fetchArray($a)){
					//display domain
					if(substr($row['domain'], 0, 11)=='http://www.') $dispDomain = substr($row['domain'], 11);
					else if(substr($row['domain'], 0, 7)=='http://') $dispDomain = substr($row['domain'], 7);
					$output .= 
					'<tr>
						<td>#'.$row['user_id'].'</td>
						<td>#'.$row['id'].'</td>
						<td>'.$row['name'].'</td>
						<td><a href="'.$row['domain'].'" target="_blank">'.$dispDomain.'</a></td>
					</tr>';
				}
	$output .= '</table><br /><br />';
	
	//BLOGS
	$output .= '<table width="100%" cols="4" border="1" bordercolor="#000000">
				<tr><td colspan="4"><strong>Blog Domains</strong></td></tr>
				<tr>
					<td><b>User</b></td>
					<td><b>Blog</b></td>
					<td><b>Blog Name</b></td>
					<td><b>Domain</b></td>
				</tr>';
				while($row = $reseller->fetchArray($b)){
					//display domain
					if(substr($row['domain'], 0, 11)=='http://www.') $dispDomain = substr($row['domain'], 11);
					else if(substr($row['domain'], 0, 7)=='http://') $dispDomain = substr($row['domain'], 7);
					$output .= 
					'<tr>
						<td>#'.$row['user_id'].'</td>
						<td>#'.$row['id'].'</td>
						<td>'.$row['blog_title'].'</td>
						<td><a href="'.$row['domain'].'" target="_blank">'.$dispDomain.'</a></td>
					</tr>';
				}
	$output .= '</table>';
	
	$query = "UPDATE metrics SET sites = '".mysqli_real_escape_string($output)."' WHERE id = 1";
	$result = $reseller->query($query);
	//echo $output;
	$output = '';
	//echo mysqli_error().'<br />';
?>