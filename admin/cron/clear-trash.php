<?php
    require_once(dirname(__FILE__) . '/../6qube/core.php');

	require_once(QUBEADMIN . 'inc/local.class.php');
	$connector = new Local();
	
	//Tables to search and clear:
	$tables = array('articles', 
					'auto_responders', 
					'blogs', 
					'blog_post', 
					'campaign_collab', 
					'contacts', 
					'contact_form', 
					'directory', 
					'hub', 
					'hub_page', 
					'hub_page2', 
					'leads', 
					'lead_forms', 
					'lead_forms_emailers', 
					'notes', 
					'phone_numbers', 
					'press_release', 
					'resellers_backoffice', 
					'resellers_emailers', 
					'resellers_network');
					
	//loop through tables
	foreach($tables as $table){
		if($table=='resellers_emailers') $idField = 'admin_user';
		if($table=='resellers_network') $idField = 'admin_user';
		if($table=='resellers_backoffice') $idField = 'admin_user';
		if($table=='campaign_collab') $idField = 'campaign_id';
		else $idField = 'user_id';
		//
		echo '<strong>Table '.$table.'</strong><br />';
		if($trash = $connector->query("SELECT * FROM `".$table."` WHERE `trashed` != '0000-00-00' LIMIT 3")){
			if($numItems = @$connector->numRows($trash)){
				echo '<i>'.$numItems.' trashed items.</i><br /><br />';
				while($row = $connector->fetchArray($trash)){
					echo '#'.$row['id'].' - trashed on '.$row['trashed'].'<br />';
					if((strtotime("now") - strtotime($row['trashed'])) > 2592000){
						//If older than 30 days
						//backup first
						if($connector->serialBackup($row, 'delete-bckp', $table."-".$row['id']."_uid-".$row[$idField].".txt")){
							echo 'Item successfully backed up ('.$table."-".$row['id']."_uid-".$row[$idField].'.txt).<br />';
						}
						//now delete
						if($connector->query("DELETE FROM `".$table."` WHERE `id` = '".$row['id']."' LIMIT 1")){
							echo 'Item deleted.<br />';
						} else {
							echo 'Error deleting item - '.mysqli_error().'<br />';
						}
					}
					else echo 'Not ready for deletion yet.<br />';
					echo '<br />';
				}
			}
			else echo 'No trashed items in this table.<br />';
		}
		else echo 'No trashed items in this table.<br />';
		echo '<br /><br />';
		//clear vars
		unset($idField, $trash, $numItems, $row);
	}
					
					
//	//Get all trash items
//	$query = "SELECT * FROM trash";
//	$result = $connector->query($query);
//	if($result && mysqli_num_rows($result)){
//		//Loop through and check each item for time threshold.  Delete if old.
//		while($row = $connector->fetchArray($result, NULL, 1)){
//			if((strtotime("now") - strtotime($row['date'])) > 1296000){ //If older than 30 days
//				//First backup
//				$query2 = 
//				"SELECT * FROM ".$row['table_name']." WHERE id = ".$row['table_id']." AND user_id = ".$row['user_id']." LIMIT 1";
//				$bckp_item = $connector->queryFetch($query2, NULL, 1);
//				if($bckp_item) 
//					if($connector->serialBackup($bckp_item, 'delete-bckp', $row['table_name']."-".$row['table_id']."_uid-".$row['user_id'].".txt"))
//						echo $row['table_name']." #".$row['table_id']." (trash id #".$row['id'].")
//							 successfully backed up.<br />";
//				//Then delete
//				if($row['table_name']=='resellers_emailers') $idField = 'admin_user';
//				if($row['table_name']=='resellers_network') $idField = 'admin_user';
//				if($row['table_name']=='resellers_backoffice') $idField = 'admin_user';
//				else $idField = 'user_id';
//				$query3 = 
//				"DELETE FROM ".$row['table_name']." WHERE id = ".$row['table_id']." AND ".$idField." = ".$row['user_id']." LIMIT 1";
//				if($connector->query($query3)){
//					echo "Successfully deleted ".$row['table_name']." #".$row['table_id']."<br />";
//					$query4 = "DELETE FROM trash WHERE id = ".$row['id']." LIMIT 1";
//					if($connector->query($query4)) echo "Successfully deleted trash item #".$row['id']."<br /><br />";
//					else echo "Actual item was deleted but error deleting from trash item #".$row['id']."<br /><br />";
//				}
//				else echo "Error deleting ".$row['table_name']." #".$row['table_id'].": ".mysqli_error()."<br /><br />";
//			}
//			else {
//				echo "Trash item #".$row['id']." not old enough to be deleted.<br /><br />";
//				//echo strtotime("now")-strtotime($row['date'])." !> 2592000.<br />";
//			}
//		}
//	}
//	else {
//		echo "No items in trash.";
//	}
?>