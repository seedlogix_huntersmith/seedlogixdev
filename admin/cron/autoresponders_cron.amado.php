<?php

define('QUBE_NOLAUNCH', 1);
require_once dirname(__FILE__) . '/../../hybrid/bootstrap.php';//6qube/core.php';
require_once QUBEPATH . '../classes/VarContainer.php';
require_once QUBEPATH . '../classes/QubeDataProcessor.php';
require_once QUBEPATH . '../classes/ContactFormProcessor.php';
require_once HYBRID_PATH . '/models/HybridModel.php';
require_once QUBEPATH . '../models/Response.php';
require_once QUBEPATH . '../classes/Notification.php';
require_once QUBEPATH . '../classes/NotificationMailer.php';
require_once QUBEPATH . '../models/Responder.php';
require_once QUBEPATH . '../models/AutoResponderTheme.php';
require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';
require_once QUBEPATH . '../classes/QubeSmarty.php';


        require_once QUBEPATH . '../classes/ContactResponseNotification.php';
        require_once QUBEPATH . '../classes/LeadResponseNotification.php';
        
        
// legacy:
require_once QUBEPATH . '../inc/hub.class.php';

// @todo maybe construct a class for this little script? Probably not necessary. its only 4 steps.
/**
 * 1. Query pending QueuedResponse Objects
 * 2. Parse Templates and Variables (Parsed by AutoResponderTheme object - implemented by ThemedResponseNotification)
 * 3. Send Notification
 * 4. Update Queued Response Object with Sent Date (if successful)
 * 5. Do a jig.
 * 
 */

if(isset($argc) && $argc > 1)
{
    parse_str($argv[1], $_GET);
    
    deb($_GET, '_GET');
}

Qube::ForceMaster();

$C	=	new Cron(Cron::DOLOG | Cron::DOPRINT);
$qube	=	Qube::Start();
// id => Theme (cache array)
$themes = array();

$Mailer = $qube->getMailer();

$D = Qube::GetDriver();

/** 1. */
// @note restrict this cron to a single user by adding ' AND r.user_id = %user-id%' to the WHERE part of the query

# phone, and logo values are stored in separate tables.
# hub: logo, hub
# lead_forms: (none)
# 
#Qube::IS_DEV() || die("DEBUG ONLY.");
$Q = $D->queryScheduledResponsesWhere('')
        ->Join(array('ActiveResponder', 'r'),   // get active responders
            'r.id = T.responder_id AND r.trashed = "0000-00-00"')
        
        /* old trash table join 
        ->leftJoin(array('trash', 'respondertrash'),                 // check that the responder hasnt been deleted
                'respondertrash.table_id = r.id AND respondertrash.table_name="auto_responders"')
        
         * 
         */
        
#        ->leftJoin(array('AutoResponderTheme', 'theme'),
#                'theme.id = r.theme')
        
        // according to the old code, if the table_name column of the responder
        // is NOT 'lead_forms' then get the prospect info from contact_form table
        // @important this must be a LEFT JOIN and NOT inner join because given the case that
        // table_name = 'lead_forms' would cause the entire query to fail if an innerjoin is used
        
        ->leftJoin(array('ContactDataModel', 'd'),
                'd.id = T.contactdata_id AND T.contactdata_table = "contact_form" AND d.trashed = "0000-00-00" AND d.spamstatus = 0')
        /* replaced: 
        ->leftJoin('leads',
                'T.contactdata_table = "leads" AND leads.optout != 1 AND leads.id = T.contactdata_id AND NOT EXISTS (SELECT table_id FROM trash WHERE table_name = "leads" AND table_id=leads.id)')
        with: */
        
        ->leftJoin('leads',
                'T.contactdata_table = "leads" AND leads.optout != 1 AND leads.id = T.contactdata_id AND leads.trashed = "0000-00-00"')
        
        // inner join
        ->Join(array('UserModel', 'u'),
                'u.id = r.user_id')

        // inner join
        /*  this was leaving out user autoresponders where parent_id = 0!
        ->Join(array('resellers', 'reseller'),
                '(reseller.support_email != "" AND ((reseller.admin_user = u.id AND u.parent_id = 0) OR reseller.admin_user = u.parent_id))')
        */
        ->leftJoin(array('resellers', 'reseller'),
#                'reseller.admin_user = u.id AND r.user_parent_id != 0 AND r.user_parent_id != 7')
                '(reseller.support_email != "" AND ((u.parent_id = 0 AND reseller.admin_user = u.id) OR reseller.admin_user = u.parent_id))')
        
        ->leftJoin('hub h',
                'r.table_name = "hub" AND h.id = r.table_id')
        
        // possibly get the domain name from reseller for opt-out.
        
        ->addFields('ifnull(h.phone, "") as responder_phone, T.q_id,
                if(r.logo="",
                        ifnull(h.logo,"")
                    ,
                    concat("/users/%s/hub_page/",r.logo)) as site_logo , 
                    d.name  as contact_name,
                    d.email as contact_email,
                    d.phone as contact_phone,
                    d.comments  as contact_comments,
                    r.contact   as responder_contact,
                    r.anti_spam as responder_anti_spam,
                    r.optout_msg as responder_optout_msg,
                    
                    leads.id as lead_id,
                coalesce(leads.lead_name, d.name) as lead_name,
		coalesce(leads.lead_email, d.email) as lead_email,
		coalesce(d.phone, "Unknown") as lead_phone,
		d.comments as lead_comments,
		leads.lead_form_id, d.*, u.*, r.*, IFNULL(reseller.main_site, "6qube.com") as main_site')
        
        // some last minute legacy stuff
        ->Where('r.from_field IS NOT NULL AND r.body IS NOT NULL
            AND (NOT ISNULL(leads.id) or NOT ISNULL(d.id)) ');
        ;
        
if(isset($_GET['ar_restrict']))     /*
 *  Given a ar_restrict (responder_id) value.. force the autoresponder execution and bypass the time restrictions.
 */
        $Q->Where('T.responder_id = %d', $_GET['ar_restrict']);
else
{
////    echo 'no restriction (running all responders)';
    // follow normal execution for scheduled responses.
    $Q->Where('delivered_date = "0000-00-00 00:00:00" AND schedule_date < NOW() + INTERVAL 10 MINUTE');

}

if($_GET['amado'] == 'AMADO')
{
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
}

echo 'Query:', "\n";
echo $Q, "\n";

$update_delivered_date = Qube::GetPDO()->prepare('UPDATE ' . QubeDriver::getTableName('ScheduledResponse') . ' SET delivered_date = NOW() 
        WHERE contactdata_id = ? AND responder_id = ? AND delivered_date = 0 LIMIT 1');

/** @var ResponderModel[] $PendingNotifications */
$PendingNotifications = $Q->Exec()->fetchAll(PDO::FETCH_CLASS, 'ResponderModel');

//  load the template processor
$Smarty     =   new QubeSmarty();

$C->logDEBUG("Starting.. Dispatching %d Responders.", count($PendingNotifications));
foreach($PendingNotifications as $ScheduledResponse)
{
    // Responder holds the theme config settings.
    /**
     * @var Responder 
     */
//    $Responder = $ScheduledResponse->convertToModel('Responder');
    
    $Responder= $ScheduledResponse;
    
    // at this point Response is an EmailNotification
    
    $vars = array('main_site' => $ScheduledResponse->main_site,
                
        // this stuff is so arbritrary its freaking insane!
        // i'm still not 100% sure what the pid variable comes from used to generate the opt-out link
        
        // this current value passed is the id of the contactdata record (could be on leads table or contact_forms table)
        // this is the most logical value that I makes sense to me
        // after all, the contact record contains the email and other columns such as optout
        // however, I found the code that actually processes the optout form page
        // inside a themes/ directory, which makes me think that the same code is
        // distributed across the different themes rendering any modifications to that code
        // quite unpredictable due to its redundancy across the themes! yuck
        
                    'prospect_id' => $ScheduledResponse->contactdata_id);

    $vars['site_root'] =   'http://' . $ScheduledResponse->main_site . '/';
    $vars['responder_anti_spam']    =   $ScheduledResponse->responder_anti_spam;
    $vars['responder_contact']      =   $ScheduledResponse->responder_contact;
    $vars['responder_optout_msg']   =   $ScheduledResponse->responder_optout_msg;
    
    
    # 1. Create Response, and sendTo objects
    
    $smartyvars     =   array();
	$create = array('table' => $ScheduledResponse->contactdata_table);
    try{
        switch($ScheduledResponse->contactdata_table)
        {
            case 'leads':
		$create['lead']	= LeadModel::Fetch(array('id' => $ScheduledResponse->contactdata_id));
//                $Response = $Responder->CreateResponse($ScheduledResponse->contactdata_table, $xlead);
                $smartyvars =   ResponderModel::loadUserInfo($ScheduledResponse->contactdata_id, NULL, NULL);//$Response);
                
                // Validate Email
                if (filter_var($ScheduledResponse->lead_email, FILTER_VALIDATE_EMAIL)) {
                
                    // Clean quotes from name
                    $replace_array = array('\'', '"');
                    $clean_lead_name = str_replace($replace_array, '', $replace_array);

                    //$sendTo =  new SendTo($ScheduledResponse->lead_name, $ScheduledResponse->lead_email); <-- OLD
                    $sendTo = new SendTo($clean_lead_name, $ScheduledResponse->lead_email); 
                    
                } else {
                    print "\r\nInvalid Email: {$ScheduledResponse->lead_email}";
                    
                }
        
            break;
        default:
#		continue;
            list($Response, $sendTo) = $Responder->CreateResponse($ScheduledResponse->contactdata_table, $ScheduledResponse);
        }
        
        
    }  catch (QubeException $exception)
    {
        continue;
    }
var_dump($ScheduledResponse->lead_email, $ScheduledResponse->contactdata_table, $sendTo);       

#	if($sendTo == null || $ScheduledResponse->contactdata_table == 'contact_form') continue;
    // @var EmailResponse
    # 2. configure common variables for the responder.. body and subject and more
    $Response = $Responder->ConfigureResponse($create, //$Response,
                $ScheduledResponse,
                $Smarty,
                $vars, $smartyvars);
    /** 3 */
    if($Mailer->Send($Response, $sendTo))
    {
        // @idea.. this could also be compiled into one OR condition possibly using the DBWhereExpression
        //      so that we dont execute the same statement 'hundreds' of times
        // 
        
        if(FALSE === $update_delivered_date->execute(array($ScheduledResponse->contactdata_id, $ScheduledResponse->responder_id)))
	{
		throw new Exception();
	}
	$C->logINFO('Success: q_id %d, Sent email to %s, responder_id %d, contactdata_id %d, contactdata_table %s', $ScheduledResponse->q_id, $sendTo,
		$ScheduledResponse->responder_id,	$ScheduledResponse->contactdata_id, $ScheduledResponse->contactdata_table);

    }else{
		$C->error();
	$C->logERROR('Failure: Email to %s, responder_id %d, contactdata_id %d, contactdata_table %s', $sendTo,
		$ScheduledResponse->responder_id,	$ScheduledResponse->contactdata_id, $ScheduledResponse->contactdata_table);

        Qube::Fatal($Response, $Mailer, $ScheduledResponse);
    }
    
#    deb($Mailer, $sendTo);
    // @todo maybe log the number of failed send attempts
}

echo "\ndone";
