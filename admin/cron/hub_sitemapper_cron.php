<?php
require_once(dirname(__FILE__) . '/../6qube/core.php');


require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

//First run sitemapping for 6qube:
runSitemapping('6qube.com');
//Now get reseller main sites and run sitemapping for them
$query = "SELECT id, admin_user, domain FROM resellers_network WHERE type = 'whole' AND domain != '' AND setup = 1";
if($result = $hub->query($query))
if($hub->numRows($result)){
	while($row = $hub->fetchArray($result)){
		runSitemapping($row['domain'], $row['admin_user']);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
// Start Sitemapping													  // 
/////////////////////////////////////////////////////////////////////////////////////////
function runSitemapping($domain, $resellerID = ''){
	$hub = new Hub();
	$limit = 10;
	$criteria .= " WHERE trashed = '0000-00-00' 
			  AND domain != ''  
			  AND `company_name` != ''
			  AND `category` != ''
			  AND `category` != '0'
			  AND `company_name` != ''
			  AND `phone` != ''
			  AND `street` != ''
			  AND `city` != ''
			  AND `state` != ''
			  AND `slogan` != ''
			  AND `description` != ''
			  AND user_id != 105";
			  
	$base = '<?xml version="1.0" encoding="UTF-8"?>
		   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
		   <url>
			 <loc>http://hubs.'.$domain.'/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>daily</changefreq>
			 <priority>1.0</priority>
		   </url>
		   <url>
			 <loc>http://hubs.'.$domain.'/cities/</loc>
			 <lastmod>'.date('Y-m-d').'</lastmod>
			 <changefreq>weekly</changefreq>
			 <priority>0.6</priority>
		   </url>
		   ';
	$end = "</urlset>";
	
	//Get cities
	$query = "SELECT id, COUNT(type), city, state FROM hub";
	$query .= $criteria;
	if($resellerID) $query .= " AND user_parent_id = ".$resellerID;
	$query .= " GROUP BY city, state";
	$cities = $hub->query($query);
	while($row = $hub->fetchArray($cities)){
		$city = $row['city'];
		$state = $row['state'];
		if($major = $hub->majorCity($city)){
			if($state == $major)
				$url = 'http://hubs.'.$domain.'/'.$hub->convertKeywordUrl($city);
			else
				$url = 'http://hubs.'.$domain.'/'.$hub->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		}
		else
			$url = 'http://hubs.'.$domain.'/'.$hub->convertKeywordUrl($city, true).'-'.strtolower($state).'/';
		$add .= '
			   <url>
				 <loc>'.$url.'</loc>
				 <lastmod>'.date('Y-m-d').'</lastmod>
				 <changefreq>daily</changefreq>
				 <priority>0.8</priority>
			   </url>
			   ';
		//add pages for city
		$numResults = $row['COUNT(type)'];
		$numPages = ceil($numResults/$limit);
		for($i=$numPages; $i>1; $i--){
			$add .= '
				   <url>
					 <loc>'.$url.$i.'/</loc>
					 <lastmod>'.date('Y-m-d').'</lastmod>
					 <changefreq>weekly</changefreq>
				   </url>
				   ';
		}
		$numResults = $numPages = 0;
		
		//Get categories for each city
		$query2 = "SELECT id, COUNT(type), category FROM hub";
		$query2 .= $criteria;
		if($resellerID) $query2 .= " AND user_parent_id = ".$resellerID;
		$query2 .= " AND city = '".$city."'";
		$query2 .= " AND state = '".$state."'";
		$query2 .= " GROUP BY category";
		$cats = $hub->query($query2);
		while($row2 = $hub->fetchArray($cats)){
			$a = explode("->", $row2['category']);
			if($a[1]){
				$category = $hub->convertKeywordUrl($a[1]);
				$add .= '
					   <url>
						 <loc>'.$url.$category.'</loc>
						 <lastmod>'.date('Y-m-d').'</lastmod>
						 <changefreq>weekly</changefreq>
						 <priority>0.7</priority>
					   </url>
					   ';
				//add pages for city+category
				$numResults = $row2['COUNT(type)'];
				$numPages = ceil($numResults/$limit);
				for($i=$numPages; $i>1; $i--){
					$add .= '
						   <url>
							 <loc>'.$url.$category.$i.'/</loc>
							 <lastmod>'.date('Y-m-d').'</lastmod>
							 <changefreq>weekly</changefreq>
						   </url>
						   ';
				}
			}
		}
	}
	
	$data = $base.$add.$end;
	
	if($domain=='6qube.com')
		$sitemapFile = QUBEROOT . 'hubs/sitemap.xml';
	else
		$sitemapFile = QUBEROOT . 'hubs/sitemap_'.$domain.'.xml';
	
	$f = fopen($sitemapFile, 'w');
	fwrite($f, $data);
	fclose($f);
} //end function runSitemapping()
?>