<?php
#require_once(dirname(__FILE__) . '/../6qube/core.php');
    // amado code is LIVE
    require_once 'autoresponders_cron.amado.php';
    exit;
    
    
	//This script is set up with the server to be executed every one hour
	//It combs through the auto_responders table and the contact_form table
	//and sends out the appropriate autoresponder emails.
	
	//MAJOR UPDATE 2/9/2011
	//The query for each autoresponder that selects prospects sign up to it
	//has been changed to only pull prospects to have either not been emailed
	//(and are due right now) or have already been sent.  Future prospects
	//aren't pulled anymore to save query time and system memory.
	//The script has also been updated to check the user's number of emails sent
	//this month against their class's monthly limit.
		
	require_once(QUBEADMIN . 'inc/hub.class.php');
	//require_once 'Mail.php';
	$local = new Hub();
	
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
	$port = '25';
	$smtp = Mail::factory('smtp',
				array ('host' => $host,
					  'port' => $port, 
					  'auth' => false));
					  
	$specID = $_GET['id'];
	//All output echos have been commented out to save memory
	//uncomment them for testing.  

	//Get all autoresponders that aren't instant and are active
	$query = "SELECT * FROM auto_responders WHERE sched>0 AND active = 1";
	$query .= " AND trashed = '0000-00-00' ";
	$query .= " AND from_field IS NOT NULL";
	$query .= " AND body IS NOT NULL";
	$query .= " AND contact IS NOT NULL";
	$query .= " AND anti_spam IS NOT NULL";
	$query .= " AND optout_msg IS NOT NULL";
	if($specID && is_numeric($specID)) $query .= " AND id = '".$specID."'";
	$result = $local->query($query);
	if($result){
//		echo "<b>Found ".$local->numRows($result)." autoresponders</b><br /><br />";
		//For each AUTORESPONDER
		while($row = $local->fetchArray($result)){
			$userID = $row['user_id'];
			$userParentID = $row['user_parent_id'];
			$userEmailsRemaining = $local->emailsRemaining($userID);
			//check if the user has any emails remaining before continuing
			if($userEmailsRemaining > 0){
			//If a reseller or reseller client get info
			if($userParentID!=0 && $userParentID!=7){
				$resellerquery = "SELECT main_site FROM resellers WHERE admin_user = '".$userParentID."'";
				$resellerInfo = $local->queryFetch($resellerquery);
				$parentDomain = $resellerInfo['main_site'];
			}
			if(!$parentDomain) $parentDomain = '6qube.com';
			
//			echo "<b>Autoresponder ".$row['id'].":</b><br />";
//			echo 'User has '.$userEmailsRemaining.' sends remaining.<br />';
			if($row['active']){
				if($row['table_name']=="directory") $table = "direc";
				else if($row['table_name']=="articles") $table = "artic";
				else if($row['table_name']=="press_release") $table = "press";
				else $table = $row['table_name'];
				$now = date("Y-m-d H:i:s");
				//Set time vars for this autoresponder
				if($row['sched_mode']=="hours") $timeDiff = 3600; //60x60
				else if($row['sched_mode']=="days") $timeDiff = 86400; //3600x24
				else if($row['sched_mode']=="weeks") $timeDiff = 604800; //86400x7
				else if($row['sched_mode']=="months") $timeDiff = 2419200; //604800x4
				else if($row['sched_mode']=="") $timeDiff = 3600; //Default to hours
				$timeDiff = $row['sched'] * $timeDiff;
				$minSendDate = date("Y-m-d H:i:s", (time() - $timeDiff));
				//Find all prospects that have the correct type/type_id
				if($table=='lead_forms'){
					$updateTable = 'leads';
					$query2 = "SELECT id, user_id, lead_form_id, hub_id, data, responses_received, created 
							 FROM leads 
							 WHERE optout != 1 
							 AND lead_form_id = '".$row['table_id']."' 
							 AND leads.trashed = '0000-00-00' ";
				}
				else {
					$updateTable = 'contact_form';
					$query2 = "SELECT id, user_id, type, type_id, name, email, responses_received, created 
							 FROM contact_form 
							 WHERE optout != 1 
							 AND type_id = '".$row['table_id']."' 
							 AND type = '".$table."' 
							 AND contact_form.trashed = '0000-00-00' ";
							 
				}
				$query2 .= "AND created BETWEEN '2010-08-26 00:00:00' AND '".$minSendDate."'";
				$result2 = $local->query($query2);
				
				if($result2){
//					echo "&nbsp;&nbsp;Found ".mysqli_num_rows($result2)." prospects<br />";
					//For each PROSPECT signed up to this autoresponder
					while($row2 = $local->fetchArray($result2, NULL, 1)){
						//Find out which auto responses they've been sent
						$responses_array = explode("::", $row2['responses_received']);
						if(!$responses_array || !in_array($row['id'], $responses_array)){
							//If they haven't been sent this one, add them to
							//an array of emails to send
							$sendTo[$row2['id']] = array();
							$sendTo[$row2['id']]['user_id'] = $row2['user_id'];
							if($table=='lead_forms'){
								$leadInfo = $local->leadInfoFromData($row2['data'], NULL, 1);
								$sendTo[$row2['id']]['name'] = $leadInfo['name'];
								$sendTo[$row2['id']]['email'] = $leadInfo['email'];
								$sendTo[$row2['id']]['fields'] = $leadInfo['fields'];
							} else {
								$sendTo[$row2['id']]['name'] = $row2['name'];
								$sendTo[$row2['id']]['email'] = $row2['email'];
							}
							$sendTo[$row2['id']]['created'] = $row2['created'];
							$sendTo[$row2['id']]['update'] = $row2['responses_received']."::".$row['id']; //intentional
						}
						else {
//							echo "&nbsp;&nbsp;&nbsp;&nbsp;<b>Prospect ".$row2['id'].":</b><br />";
//							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Message already sent<br />";
						}
					}
					if($sendTo){
						//Now send the emails (if the time is right)
						foreach($sendTo as $key=>$value){
//							echo "&nbsp;&nbsp;&nbsp;&nbsp;<b>Prospect ".$key.":</b><br />";
							//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created at: ".date("F jS, g:ia", $created)."<br />";
//							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Scheduled for: ".date("F jS, g:ia", $sendAt)."<br />";
							//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current time: ".date("F jS, g:ia")."<br />";
							//If the scheduled time has passed, send the email:
							if($value['name']) $to = '"'.$value['name'].'" <'.$value['email'].'>';
							else  $to = $value['email'];
							$subject = str_replace("#name", $value['name'], $row['subject']);
							$body = stripslashes($row['body']);
							$body = str_replace("#name", $value['name'], $body);
							if($table=='lead_forms' && is_array($value['fields'])){
								foreach($value['fields'] as $key=>$value){
									$searchTag = $local->convertKeywordUrl($value['key'], 1, '_');
									$body = str_replace('[['.$searchTag.']]', $value, $body);
									$subject = str_replace('[['.$searchTag.']]', $value, $subject);
									unset($searchTag);
								}
							}
							$body .= "<br /><br />".$row['contact'];
							$body .= "<br />".$row['anti_spam'];
							$body .= '<br /><a href="http://'.$parentDomain.'/?page=client-optout&email='.$value['email'].'&pid='.$key.'" target="_blank">'.$row['optout_msg'].'</a>';
							$headers = "From: ".$row['from_field']."\r\n";
							$headers .= "Reply-To: ".$row['from_field']."\r\n";
							$headers .= "MIME-Version: 1.0\r\n";
							$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							
							$headers = array('From' => $row['from_field'],
										  'Reply-To' => $row['from_field'],
										  'To' => $to,
										  'Subject' => $subject,
										  'Content-Type' => 'text/html; charset=ISO-8859-1',
										  'MIME-Version' => '1.0');
							//final check to make sure things are valid
							$continue = true;
							//"From" address
							if(strpos($row['from_field'], '@')===false || strpos($row['from_field'], '.')===false) $continue = false;
							//"To" address
							if(strpos($to, '@')===false || strpos($to, '.')===false) $continue = false;
							//Subject
							if(!$subject) $continue = false;
							//check remaining sends one more time before sending
							if(($userEmailsRemaining > 0) && $continue)
								$mail = $smtp->send($to, $headers, $body);
							
							//if(mail($to, $subject, $body, $headers)){
							if(!PEAR::isError($mail)){
//								echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Message sent successfully</i><br />";
								//add this responder to the list of prospect's received ones
								$query = "UPDATE `".$updateTable."`";
								$query .= " SET responses_received = '".$value['update']."' WHERE id = '".$key."'";
								$local->query($query);
								//+1 the user's number of sent emails
								$query = "UPDATE users SET emails_sent = emails_sent+1 WHERE id = '".$userID."'";
								$local->query($query);
								//-1 remaining emails (for use in this script)
								$userEmailsRemaining--;
							}
							else {
//								echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$mail->getMessage()."<br />";
							}
							$to = $subject = $body = $headers = $mail = NULL; //clear vars for next iteration
						} //end foreach($sendTo as $key=>$value)
						$sendTo = NULL; //Clear $sendTo array for next autoresponder!
					} //end if($sendTo)
//					echo "<br />";
	
				} //end if($result2)
			} //end if($row['active'])
//			else echo "&nbsp;&nbsp;Autoresponder is not set to Active.<br /><br />";
			} //end if($userEmailsRemaining > 0)
		} //end while($row = $local->fetchArray($result))
	} //end if($result)
?>
