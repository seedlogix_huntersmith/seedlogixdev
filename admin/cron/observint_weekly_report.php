<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('IS_DEBUG', 0);

define('QUBE_NOLAUNCH', 1);
require_once '/home/sapphire/production-code/sofus-unstable/hybrid/bootstrap.php';
Qube::AutoLoader();
#var_dump(Date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']));

#require_once(dirname(__FILE__) . '/../6qube/core.php');
#require_once('/home/sapphire/production-code/dev-trunk-svn/admin/6qube/core.php');

if(!isset($argc)){
	die('Web Access Denied.');
}

//This script is run every 30min and combs through the resellers_emailers table to find
//scheduled emails from resellers to their users.

require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

//include phone class
require_once(QUBEADMIN . 'inc/phone.class.php');
$phone = new Phone();

//include analytics class
require_once(QUBEADMIN . 'inc/analytics.class.php');
$anal = new Analytics();

//SMTP settings
require_once 'Mail.php';
$host = "relay.jangosmtp.net";
$port = '25';
$smtp = Mail::factory('smtp',
			array ('host' => $host,
				  'port' => $port, 
				  'auth' => false));
// */
function doSendEmail($smtp, $to, $headers, $msg, $manageremail, $totalcalls){
	if(!IS_DEBUG){
		//if($totalcalls=='0'){
		  //$to	=	array('support@6qube.com');
		//} else {
		  $to	=	array($to, 'robby.thomas@observint.com', $manageremail, 'admin@6qube.com', 'robby.thomas@scx.com');
          //$to	=	array('george.farley@observint.com', 'sue.chen@observint.com', 'robby.thomas@scx.com', 'billy.grumbles@scx.com', 'admin@6qube.com');
		//}
	      foreach($to as $i => $email){
		if($i	==	0) continue;
//		  $headers['
	      }
		$smtp->send(join(', ', $to), $headers, $msg);
		return;
	}	
	// print the email
	echo "Sending Email to: ", $to, "\n", 
		"Headers: ", join(', ', $headers), "\n",
		"Message:\n\n$msg";
}				  

function strstr_after($haystack, $needle, $case_insensitive = false) {
    $strpos = ($case_insensitive) ? 'stripos' : 'strpos';
    $pos = $strpos($haystack, $needle);
    if (is_int($pos)) {
        return substr($haystack, $pos + strlen($needle));
    }
    // Most likely false or null
    return $pos;
}

function apicall($req_url){
			$hCurl = curl_init();
			$creds_pass = 'testing';
			$creds = 'sixqube:'.$creds_pass;
			curl_setopt($hCurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($hCurl, CURLOPT_URL, $req_url);
			curl_setopt($hCurl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($hCurl, CURLOPT_USERPWD, $creds);
			curl_setopt($hCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($hCurl, CURLOPT_FOLLOWLOCATION, 1);
	
			$data = curl_exec($hCurl);
#var_dump($data, $req_url, $custID, $custids);
			curl_close($hCurl);
			$json = json_decode($data, TRUE);
		return $json;	
}

function getcalldetails($mailbox){
	
	$currentDate = date("Y-m-d");
	$lastMonth = date("Y-m-d", $_SERVER['REQUEST_TIME'] - 2592000);
	
	//$url	=	'https://my.patlive.com/api/v1.0/Mailbox/' . $mailbox . '/CallDetails/?format=json&StartDate=' . $lastMonth . '&EndDate=' . $currentDate . '&PageSize=60';
	$url	=	'https://my.patlive.com/api/v1.0/Mailbox/' . $mailbox . '/callcount/' . $lastMonth . '/' . $currentDate . '?format=json';
	$json	=	apicall($url);

#	echo 'call:';	var_dump($json);
	return $json;
}

function callinfo_message($call){
	
#	var_dump($call);exit;
	$str	= <<<EOF
<tr bgcolor="#f0f0e1">
<td align="left" valign="top" bgcolor="#f0f0e1">{$call->CallerId}</td>
<td width="3%" align="left" valign="top" bgcolor="#FFF"></td>
<td align="center" valign="top" bgcolor="#f0f0e1">{$call->CallDuration}</td>
</tr>		
EOF;


	return $str;

	}
	
function apiGetDayMailbox($custID){
	
	if(IS_DEBUG){
	    $req_url = 'https://my.patlive.com/api/v1.0/Customer/'.
			  $custID.'/Mailbox/?format=json&RequestType=YearToDate';
	}else
	    $req_url = 'https://my.patlive.com/api/v1.0/Customer/'.
			  $custID.'/Mailbox/?format=json&RequestType=Today';
		      
		
		$json	=	apicall($req_url);
		
		return $json;
}

function TotalContacts($USER_ID){
global $hub;
	  		$lastMonth = date("Y-m-d", $_SERVER['REQUEST_TIME'] - 2592000);
			$leadsquery = "SELECT count(id) FROM leads WHERE user_id = '$USER_ID' AND spam < 50 AND created >=  '".$lastMonth."'";
			$leads1 = $hub->queryFetch($leadsquery);
			$formsquery = "SELECT count(id) FROM contact_form WHERE user_id = '$USER_ID' AND spamstatus = '0' AND created >=  '".$lastMonth."'";
			$leads2 = $hub->queryFetch($formsquery);
	
			$numProspects = $leads1['count(id)']+$leads2['count(id)'];
			
			return $numProspects;
}

function TotalWebsiteVisits($USER_ID, &$visitsView, &$totalvisits){
global $hub;
global $anal;

	  		  $query = "SELECT httphostkey, tracking_id FROM hub WHERE user_id = '$USER_ID'
               AND trashed = '0000-00-00'
               AND httphostkey != ''
			   AND tracking_id != 0
               ORDER BY httphostkey ASC";

//	echo "getting sites for $USER_ID\n$query\n\n";			   
			  $hubs = $hub->query($query);
			  
			  
//			  echo "Hubs:";			  var_dump($hubs);
			  
				 while($row = $hub->fetchArray($hubs)){
//					 echo "Getting Views for $row[tracking_id]\n";
					$currentDate = date("Y-m-d");
					$lastMonth = date("Y-m-d", $_SERVER['REQUEST_TIME'] - 2592000);
		
//					$current_views	=	$anal->ANAL_getViewCountEmailReports($row['tracking_id'], ''.$lastMonth.','.$currentDate.'');
					 $piwikDataAccess = new PiwikReportsDataAccess();
					$current_views = $piwikDataAccess->getTotalVisitsByTrackingId($row['tracking_id'], "$lastMonth,$currentDate");
//					echo 'current_views: ' . $current_views, "\n";
					$totalvisits += $current_views;
					$visitsView[] = ' <tr bgcolor="#f0f0e1">
									  <td align="left" valign="top">'.$row['httphostkey'].'</td>
									  <td width="3%" bgcolor="#FFF"></td>
									  <td align="center" valign="middle">'.$current_views .'</td>
									</tr>';
				}

			
}



$PARENT_ID	=	6312;
//initial query to find appropriate emails
$query = "SELECT u.username, i.user_id,
			i.firstname, i.lastname, i.company, i.profile_photo, i.account_manager,
			mgr.firstname as mgr_firstname, mgr.lastname as mgr_lastname, mgr.email as mgr_email, mgr.phone as mgr_phone
			FROM users u, user_info i, user_info mgr
				WHERE u.id = i.user_id AND mgr.user_id = i.account_manager AND i.account_manager != 0";

$customer_ids = $hub->query($query);
$echo = '';
//get customer ids
if(!$customer_ids || $hub->numRows($customer_ids) == 0){
		echo $query;
		
		die('Fatal error ocurred. Quitting..');
}
	//loop through each customer id
	
	$counter	=	0;
	while($custids = $hub->fetchArray($customer_ids)){
	
	    $custID = $custids['customer_id'];
	
		if(IS_DEBUG)	  echo 'Processing: custID: ', $custID, "\n";

	  $usersys = $custids['username'];
	  $userinfo = $custids;	//$hub->queryFetch($query3);
	  $managerinfo = $custids;	//$hub->queryFetch($query4);		
	  $USER_ID = $custids['user_id'];
	  
	  $currentDate = IS_DEBUG ? '2014-09-10' : date('Y-m-d');	//date("2013-09-10");
	    
		//$numProspects =	TotalContacts($USER_ID);
		
		$visitsView	=	array();
		$totalVisits	=	0;
		
		TotalWebsiteVisits($USER_ID, $visitsView, $totalVisits);
	    $counter++;
			
	    
	    $message	 =	'';
		//$totalcalls	=	0;
	    //$tries	=	0;
	    //while($tries < 5){
	    
	      //$json	=	apiGetDayMailbox($custID);
		//if(!$json){
			//echo 'bad json';
			//var_dump($json);
			//$tries++;
			//continue;
		//}

		if(IS_DEBUG) print_r($json);

		//$mailboxes = $json['Data'];
      
		//if($json['ErrorCode']){
		//echo "Error: {$json['ErrorMessage']}\n";
	
			//$tries++;
	
		//if($json['ErrorCode']	==	4001)
			//sleep(240*$tries);
			//continue;
		//}else
			//break;
	   // }
	    
	    //if(!is_array($mailboxes)){
		//if(IS_DEBUG){
			//echo 'Mailboxes: ';		    var_dump($mailboxes, $json);
		//}
		    //continue;
	    //}
	  	//if(IS_DEBUG)  echo $counter, ' ';
	    //$message	=	false;	// init loop variable
	    
	    //foreach($mailboxes as $mb){
		   // $mbid	=	$mb['MailboxId'];
		   // $calldetails	=	getcalldetails($mbid);
		    
			//if(IS_DEBUG) print_r($calldetails);
		   // if($calldetails['count'])
		   // {
			   // $totalcalls	+=	$calldetails['count'];
				    
    
			   // echo $totalcalls.' '.$userinfo['company'].'<br />';
// exit;
		  //  }
		    //else echo 'no calls. ', "\n";
	   // }
	    
#	    if(!$message) continue;	// do we really want to quit if there are no messages from the phone stuff?
	    
			
			$today = date("F j, Y", strtotime($currentDate)); 
			
			$htmlemail = '<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title></title>
<style type="text/css">
body {
	font-family:Arial;
	font-size:16px;
}
h1, h2, h3, h4, h5 {
	margin:0px 0px 0px 0px;
	padding:5px 0px 5px 0px;
}
h1 {
	font-family:35px;
	padding:0px 0px 10px 0px;
}
h2 {
	font-family:28px;
	padding:0px;
}
h3 {
	font-family:22px;
}
</style>
</head>

<body>
<div style="font-size: 1px; color: #ffffff; display: none;">Below is your weekly PowerLeads report for '.$userinfo['company'].'. If you have any questions, please contact your account manager. </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" rowspan="3"></td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right" valign="bottom" style="height:39px; color:#909395; background:url(images_weekly/header_top_left.jpg) no-repeat #2b3337;"><strong>'.$today.'</strong></td>
          <td rowspan="2" align="right" valign="bottom" bgcolor="#2b3337"><a href="#"><img src="https://6qube.com/clients/ab/powerleads/images_weekly/header_right.jpg" alt="Daily Report Left Alt Tag" width="507" height="226" border="0" /></a></td>
        </tr>
        <tr align="left" valign="top">
          <td align="left" valign="top" bgcolor="#2b3337"><a href="#"><img src="https://6qube.com/clients/ab/powerleads/images_weekly/header_left.jpg" alt="Daily Report Left Alt Tag" width="155" height="187" border="0" /></a></td>
        </tr>
      </table></td>
    <td width="50%" rowspan="3"></td>
  </tr>
  <tr>
    <td style="background:#f6921c; height:5px;"></td>
  </tr>
  <tr>
    <td width="660" bgcolor="#FFFFFF"><br />
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td colspan="2" align="left" valign="top"><h1>'.$userinfo['firstname'].' '.$userinfo['lastname'].',</h1>
          Below is the weekly PowerLeads report for '.$userinfo['company'].'. If you have any questions, please contact your account manager:</td>
          <td width="38%" rowspan="2" align="left" valign="top"><a href="#"><img src="https://6qube.com/users/'.$userinfo['user_id'].'/user_info/'.$userinfo['profile_photo'].'" alt="Logo Alt Tag" width="230" border="0" /></a></td>
        </tr>
        <tr>
					  <td width="3%" align="left" valign="top"></td>
					  <td width="59%" align="left" valign="top"><strong>'.$managerinfo['mgr_firstname'].' '.$managerinfo['mgr_lastname'].'<br />
						'.$managerinfo['mgr_email'].'<br />
						'.$managerinfo['mgr_phone'].'</strong></td>
					</tr>
        <tr>
          <td colspan="3" align="left" valign="top"> Thank you for your continued business.<br />
            Sincerely,<br />
            <a href="#"><img src="https://6qube.com/clients/ab/powerleads/images_weekly/signature.gif" alt="Signature Alt Tag" width="108" height="60" border="0" /></a><br />
            Robby Thomas<br />
            <em>Director of Sales</em></td>
        </tr>
        </table>
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
        
          <td align="center"><img src="https://6qube.com/clients/ab/powerleads/images_daily/line.jpg" width="650" height="1" border="0" /></td>
        </tr>
        </table>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        
          <td colspan="3" align="left" valign="top"><h2 style="text-align:center;">WEEKLY REPORT</h2></td>
        </tr>
       </table>
      
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td colspan="3" style="background:#f6921c; color:#FFF;"><h2>Your PowerLeads Website Activity:</h2></td>
        </tr>
        <tr>
          <td width="70%" align="left" valign="top"><strong>Website</strong></td>
          <td width="3%" bgcolor="#FFF"></td>
          <td width="27%" align="center" valign="middle"><strong>Visits (last 30 days)</strong></td>
        </tr>
		'.join("\n", $visitsView) .'
       
        <tr >
          <td align="left" valign="top"><strong>Total:</strong></td>
          <td width="3%" bgcolor="#FFF"></td>
          <td align="center" valign="middle" ><strong>'.$totalVisits.'</strong></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td align="left" valign="top" style="height:1px;"></td>
        </tr>
      </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
        
          <td align="center"><img src="https://6qube.com/clients/ab/powerleads/images_daily/line.jpg" width="650" height="1" border="0" /></td>
        </tr>
        </table><br />
      
<table width="100%" border="0" cellspacing="0">
        <tr>
          <td align="left" valign="top" bgcolor="#333333"><a href="#"><img src="https://6qube.com/clients/ab/powerleads/images_weekly/footer.jpg" width="660" height="100" border="0" /></a></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>';
			
			//if($totalcalls=='0' || $totalVisits=='0'){
			  //$to	=	'support@6qube.com';
			//} else {
			  $to	=	$usersys;
              //$to	=	'support@6qube.com';
			//}

			$subject	=	'Power Leads Program: Weekly Report';
			
							$headers = array('From' => 'Power Leads Program <no-reply@powerleads.biz>',
										  'To' => $to, 
										  'Subject' => $subject,
										  'Content-Type' => 'text/html; charset=ISO-8859-1',
										  'MIME-Version' => '1.0');

					// send message
			
			
					//doSendEmail($smtp, $to, $headers, $htmlemail, $managerinfo['mgr_email'], $totalcalls);
					doSendEmail($smtp, $to, $headers, $htmlemail, $managerinfo['mgr_email']);
	#					$smtp->send($to	, $headers, $htmlemail);
		
		flush();
	} //end while($custids = $hub->fetchArray($customer_ids))
	
