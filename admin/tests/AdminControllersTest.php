<?php
/**
* @backupGlobals disabled
* @backupStaticAttributes disabled
*/

class AdminControllers extends PHPUnit_Framework_TestCase
{
  function testLoad()
  {
    require_once QUBEPATH . '../classes/controllers/NotificationsAdmin.php';
    require_once QUBEPATH . '../classes/controllers/EmailThemeAdmin.php';
    
    return true;
  }
  
  function testCampaignCollabCtrlr()
  {      
        $Clr = new NotificationsAdminController;
        $_SESSION['campaign_id'] = 7221;

        $_GET['id'] = 7;
        $_GET['action'] =   'edit';
        $Clr->Execute('edit')->display();
        
        
        $_GET['action'] =   'save';
        $_POST['campaigncollab'] = array('name' => 'john script',
                'id' => 9,      // edit
                'email' => 'copkiller@insanity',
                'scope' => '',
                'sites' => array(1, 2, 3, 5, ),
                'campaign_id' => $_SESSION['campaign_id']);
        $Clr->Execute('save')->display();

        
        $_GET['action'] =   'settings';
        $Clr->Execute('settings')->display();
  }  
  
  function testThemeAdminCtrlr()
  {
      $Ctrlr    =   new EmailThemeAdminController;
#      deb($Ctrlr->getThemes());
      $_GET['id']   =   928;
      
      $Ctrlr->Execute('display')->display();
  }
  
}

