<?php
/**
* @backupGlobals disabled
* @backupStaticAttributes disabled
*/

class ThemedEmailTest extends PHPUnit_Framework_TestCase
{
  function testLoad()
  {
    require_once QUBEPATH . '../classes/Notification.php';
    require_once QUBEPATH . '../classes/EmailNotification.php';
    require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';    
    require_once QUBEPATH . '../classes/ResponseNotification.php';    
    require_once QUBEPATH . '../classes/LeadFormNotification.php';    
    require_once QUBEPATH . '../classes/LeadResponseNotification.php';    
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';    
    
    require_once QUBEPATH . '../models/ContactData.php';
    require_once QUBEPATH . '../classes/ThemedResponseNotification.php';    
    require_once QUBEPATH . '../classes/QubeSmarty.php';
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/EmailResponsesQueueProcessor.php';
    
    return true;
  }
  
  function testLeadEmailResponse()
  {
    $U = new User;
    $M = Qube::GetMailer(true);
    $Recipient = new SendTo('amado MARTINEZ', 'lead.phpunit.test@sofus.mx');
    $T  = new QubeSmarty;
    
    $R = new LeadResponse(array('name' => 'amado martinez'));
    $R->loadVars(Qube::GetDriver(), 6999);
    $R->setMessage('hello #name your name is [[name]] and your email is [[email]]');
    $R->setSubject('hello #name your name is [[name]] and your email is [[email]]');
    
    $Email = new ThemedEmail($T, $R);
    $Email->setThemeName('Corporate');
    $Email->setThemeVars(array('themepath' => '[[IMGLINK]]emails/themes/',
        'links_color' => '#fff',
        'titles_color' => '#fff',
        'accent_color' => '#fff',
        'logo' => 'http://fff',
        'bg_color' => '#fff',
        'site_root' => 'http://site_root/',
        'responder_phone' => '',
        'call_action' => '',
        'edit_region1' => '',
        'edit_region2' => '',
        
        'email' => array('subject' => 'subject',
                'body' => 'body'),
        ));
    
    $M->Send($Email, $Recipient);
    
#    deb($M->getMailer());
  }
  
  /**
   * Test object instantiation and execution of 'collaborator' emails.
   * 
   */
  function testEmailResponseQueue()
  {
      return;
      $Mailer   = Qube::GetMailer(FALSE);
      $EmailResponses   = new EmailResponsesQueueProcessor(Qube::Start());
      $EmailResponses->Execute(new VarContainer(array('Mailer' => $Mailer), false));
      
#      deb($Mailer->getMailer());
  }
  
  /**
   * Execute a real contact form processor 
   */
  function testQueryContactFormProcessorData()
  {
      #return @TODO USE MOCK OBJECT
#      return;
      
      // USE USER 311 (reseller should be BIZROIDS)
      $f = ContactFormProcessor::GetUserResellerInfoQuery(311);
      $r    = $f->Exec()->fetch();
      
      // simulate

      require_once QUBEPATH . '../models/CampaignCollab.php';
      
      $_POST['domain'] = 'domain';
      
      // submit form to pastephp
    $FormProcessor = Qube::Start()->getContactFormProcessor();
    $FormProcessor->Execute(new VarContainer(array(
        'type' => 'hub',
        'type_id' => '7555',
        'comments' => 'comments!!!!!!test',
        'user_id' => '6610',
        'name' => 'jack mosley',
        'email' => 'jackmosley@sofus.mx',
        'phone' => '3332222555',
        'domain' => 'pastephp.com',
        'website' => 'pastephp.com',
        'page' => 'phpunit',
        'keyword' => 'phpunit',
        'search_engine' => 'phpunit',
        'ip' => '')));
            
      // end
      $this->assertEquals($r->main_site, 'bizroids.com');      
  }
  
  /**
   * Test for runtime and sql errors 
   */
  function testLeadDataQuery()
  {
      require_once QUBEPATH . '../models/LeadData.php';
      $Q    =   LeadData::getPdoQuery(Qube::GetDriver(), 6955);
            
      $data =   LeadData::getData(Qube::GetDriver(), 6955);
  }
  
  /**
   * Simulate a form submission. 
   */
  function testResponderProcessorLeadTheme()
  {
      $D    =   Qube::GetDriver();
      $Mailer   =   Qube::GetMailer();
      $Responder    =   $D->getActiveResponderWhere('T.user_id = 6610 AND T.theme <> ""');
      $lead_config  =   (object)array('lead_name' => 'john smith',
            'lead_email' => 'john.smith@sofus.mx',
            'lead_id' => 999999);
      
      $Smarty   =   new QubeSmarty();
      $response_config  =   (object)array('theme' => 'Corporate',
            'main_site' => 'google.com',
            'responder_phone' => '000-000-0000',
            'site_logo' => 'http://logo.jpg');
      
      list($Response, $sendTo)  =   $Responder->CreateResponse('leads', $lead_config);
      
      deb($Responder);
      
      $theme_vars    =   array();
      $F    =   $Responder->ConfigureResponse($Response,
                $response_config,
                $Smarty,
                $theme_vars);      
#      deb($Response, $F);
#      echo ':::....';
      $sent =   $Mailer->Send($Response, $sendTo);
      if(!$sent)
            deb($Mailer->getError ());
      $this->assertTrue($sent);      
  }
  
  
  
  /**
   * test another form submission 
   */
  function testResponderProcessorForm()
  {
      $D    =   Qube::GetDriver();
      $Mailer   =   Qube::GetMailer();
      
      $Responder    =   $D->getActiveResponderWhere('T.id = 932');
      $form_config  =   (object)array('contact_name' => 'john smith',
            'contact_email' => 'john.smith@sofus.mx',
            'contact_phone' => '000000',
            'contact_comments' => 'phpunit',
            'lead_id' => 999999);
      
      $Smarty   =   new QubeSmarty();
      $response_config  =   (object)array('theme' => 'Corporate',
            'main_site' => '6qube.com',
            'responder_phone' => '000-000-0000',
            'site_logo' => 'http://logo.jpg');
      
      list($Response, $sendTo)  =   $Responder->CreateResponse('contact_form', $form_config);
      
      $theme_vars    =   array();
      $Responder->ConfigureResponse($Response,
                $response_config,
                $Smarty,
                $theme_vars);      
      
      $sent =   $Mailer->Send($Response, $sendTo);
      if(!$sent)
          deb($Mailer->getError());
      $this->assertTrue($sent);      
      
  }
  
  function testThemeConfig()
  {
#      $arr  = ThemeConfig::Load(1, 'auto_responder');
           
  }
}
