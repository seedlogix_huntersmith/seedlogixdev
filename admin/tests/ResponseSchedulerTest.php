<?php
/**
* @backupGlobals disabled
* @backupStaticAttributes disabled
*/

class ResponseSchedulerTest extends PHPUnit_Framework_TestCase
{
  function testLoad()
  {
    require_once(QUBEADMIN . 'inc/db_connector.php');
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';
    require_once QUBEPATH . '../models/ContactData.php';
    require_once QUBEPATH . '../models/CampaignCollab.php';
    require_once QUBEADMIN . 'classes/controllers/FormHandlerController.php';
    
    return true;
  }
  
  function testSubmit()
  {
      $C    =   new FormHandlerController;
      
      $C->Execute('HandleContactForm');
  }
}

