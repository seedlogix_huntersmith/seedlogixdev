<?php
/**
* @backupGlobals disabled
* @backupStaticAttributes disabled
*/

class HubSearchTest extends PHPUnit_Framework_TestCase
{
  function testLoad()
  {
    require_once QUBEPATH . '../classes/controllers/NotificationsAdmin.php';
    require_once QUBEPATH . '../classes/controllers/EmailThemeAdmin.php';
    require_once QUBEPATH . '../classes/HubSearch.php';
    
    return true;
  }
  
  function testSearchHub()
  {
      $hub_id   =   8153;
      $search_str   =   'government';
      
      $D    =   Qube::GetDriver();
      $PDO  =   $D->getDb();
      
      // test
      $S    =   new HubSearch($PDO, $hub_id);      
      $results  =   $S->getResults($search_str, 2, 2);
      
      $links    =   $S->createPagination('/search-str/%s', 2, 2);
      deb($S);      
      echo $links;
  }
  
  
}

