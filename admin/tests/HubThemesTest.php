<?php
/**
* @backupGlobals disabled
* @backupStaticAttributes disabled
*/

class ThemedEmailTest extends PHPUnit_Framework_TestCase
{
  function testLoad()
  {
    require_once QUBEPATH . '../classes/Notification.php';
    require_once QUBEPATH . '../classes/EmailNotification.php';
    require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';    
    require_once QUBEPATH . '../classes/ResponseNotification.php';    
    require_once QUBEPATH . '../classes/LeadFormNotification.php';    
    require_once QUBEPATH . '../classes/LeadResponseNotification.php';    
    require_once QUBEPATH . '../classes/ContactFormProcessor.php';    
    
    require_once QUBEPATH . '../models/ContactData.php';
    require_once QUBEPATH . '../classes/ThemedResponseNotification.php';    
    require_once QUBEPATH . '../classes/QubeSmarty.php';
    require_once QUBEPATH . '../classes/VarContainer.php';
    require_once QUBEPATH . '../classes/QubeDataProcessor.php';
    require_once QUBEPATH . '../classes/EmailResponsesQueueProcessor.php';
    
    return true;
  }
  
  function testAllThemes()
  {
      $D    =   Qube::GetPDO();
      $q    =   'SELECT h.id as hub_id, h.user_id as hub_user_id, t . * FROM  `themes` t INNER JOIN hub h ON ( h.theme = t.id ) GROUP BY t.id';
      
      $st   =   $D->query($q);
           
      $hub_wtheme = $st->fetchAll();
      
      foreach($hub_wtheme as $h)
      {
          $uid  =   $h['hub_user_id'];
          $id   =   $h['hub_id'];
            $url    =   'http://6qube.com/hubs/domains/'.$uid.'/'.$id.'/index.php';
            echo $url, "\n";
      }
      
  }
  
}
