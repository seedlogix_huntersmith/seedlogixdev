<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();
$isAdmin = 0;

if($_SESSION['reseller'] || $_SESSION['admin']){
	$newEmailer = $reseller->validateText($_POST['name'], 'Email Template Name');
	
	if($reseller->foundErrors()){ //check if there were validation errors
		//$error = $directory->listErrors('<br />'); //list the errors
	}
	else { //if no errors
		if($_SESSION['admin']) $isAdmin = 1;
		$message = $reseller->addNewEmailer($_SESSION['login_uid'], $newEmailer, $isAdmin);
			
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_reseller-emailers .container';
		$response['success']['message'] = $message;
		//$response['success']['action'] = "replace";
	}

	echo json_encode($response);
}
?>