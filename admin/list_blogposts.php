<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/blogs.class.php');
	$blog = new Blog();
	
	$blog_id = $_GET['id'];
	$post_results = $blog->getBlogPost($blog_id);

	echo '<div class="container">';
	if($blog->getPostRows()){
		//loop through and out the blogs post (limits 5)
		while($post_row = $blog->fetchArray($post_results)){
			echo '<div class="item">
					<div class="icons">
						<a href="blog.php?form=editPost&id='.$post_row['id'].'" class="edit"><span>Edit</span></a>
						<a href="#" class="delete" rel="table=blog_post&id='.$post_row['id'].'"><span>Delete</span></a>
					</div>
					
					<img src="img/blog-post';
					if($_SESSION['theme']) echo '/'.$_SESSION['thm_buttons-clr'];
					echo '.png" class="blogPost" />
					
					<a href="blog.php?form=editPost&id='.$post_row['id'].'">'.$post_row['post_title'].'</a>
				</div>';		
		}
	}
	echo '</div>';

?>