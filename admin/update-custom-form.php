<?php
//////////////////////////////////////////////////////
// Script to update a custom form with a new field
// Each form input is stored in a text array
// Parsing scheme:
// inputtype|-|label|-|label note|-|[required: 1 or 0]|-|[input-specific args, separated by |=|, |+|, or |_| depending on depth]
// Examples:
// text|-|First Name|-||-|1|-|Kyle|=|250
// (a text input with label "First Name", no label note, required, default text "Kyle" and size 250
//
// select|-|Household Income|-|(for previous tax year)|-|1|-|low|+|0 to 30,000|=|low-med|+|30,000 to 50,000|=|med|+|50,000 to 80,000|=|med-high|+|80,000 to 150,000|=|high|+|Greater than 150,000
//////////////////////////////////////////////////////
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

if($_POST){
	$continue = true;
	$formID = is_numeric($_POST['formID']) ? $_POST['formID'] : '';
	$userID = $_SESSION['user_id'];//is_numeric($_POST['userID']) ? $_POST['userID'] : '';
	$multiUser = $_POST['multi_user'] ? 1 : 0;
	$action = $_POST['action'];
	$clearExisting = false;
	if($formID && $userID){
		if($action=='addField' || $action=='editFormField'){
			if($action=='editFormField') $fieldID = is_numeric($_POST['fieldID']) ? $_POST['fieldID'] : '';
			$field = array();
			$field['type'] = $_POST['fieldType'];
			//shared inputs for most fields
			if($field['type']!='name'){
				$field['label'] = $_POST['label'];
				if(!$field['label']){
					$continue = false;
					$return['message'] = 'Label cannot be blank.';
				}
				$field['label_note'] = $_POST['label_note'];
				$field['required'] = $_POST['required'] ? 1 : 0;
			}
			//specific inputs for each field type
			if($field['type']=='text'){
				$field['default'] = $_POST['default'];
				$field['numsOnly'] = $_POST['numsOnly'] ? 1 : 0;
				$field['size'] = $_POST['size'];
				$parsed = 
				'text|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['default'].'|=|'.$field['numsOnly'].'|=|'.$field['size'];
			}
			else if($field['type']=='textarea'){
				$field['default'] = $_POST['default'];
				$field['size'] = $_POST['size'];
				//$a = explode('x', $size);
				//$field['width'] = $a[0] ? $a[0] : 300;
				//$field['height'] = $a[1] ? $a[1] : 150;
				$parsed = 
				'textarea|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['default'].'|=|'.$field['size'];//.'|+|'.$field['height'];
			}
			else if($field['type']=='select'){
				$parsed = 
				'select|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|';
				$options = array();
				foreach($_POST as $key=>$value){
					if(substr($key, 0, 7)=='option_'){
						$a = explode('_', $key);
						//e.g. $options[3]['value'] = 'VT';, $options[3]['text'] = 'Vermont';
						$options[$a[1]][$a[2]] = $value;
					}
					$a = NULL;
				}
				if($options){
					foreach($options as $key2=>$value2){
						$parsed .= $options[$key2]['value'].'|+|'.$options[$key2]['text'].'|=|';
					}
				}
				else {
					$continue = false;
					$return['message'] = 'You must add at least one option to the drop-down "'.$field['label'].'"';
				}
				if(substr($parsed, -3)=='|=|') $parsed = substr($parsed, 0, -3);
			}
			else if($field['type']=='checkbox'){
				$field['checked'] = $_POST['checked'] ? 1 : 0;
				$parsed = 
				'checkbox|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['checked'];
			}
			else if($field['type']=='hidden'){
				$field['default'] = $_POST['default'];
				$parsed = 
				'hidden|-|'.$field['label'].'|-||-|0|-|'
				.$field['default'];
			}
			else if($field['type']=='email'){
				$field['default'] = $_POST['default'];
				$field['size'] = $_POST['size'];
				$parsed = 
				'email|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['default'].'|=|'.$field['size'];
			}
			else if($field['type']=='money'){
				$field['default'] = $_POST['default'] ? preg_replace("/[^0-9\.\,\-\+\s]/", "", $_POST['default']) : '';
				$parsed = 
				'money|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['default'];
			}
			else if($field['type']=='date'){
				$field['default'] = $_POST['default'];
				$parsed = 
				'date|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|'
				.$field['default'];
				if($field['default']=='custom'){
					$field['default_M'] = $_POST['default_M'];
					$field['default_D'] = $_POST['default_D'];
					$field['default_Y'] = $_POST['default_Y'];
					$parsed .= '|=|'.$field['default_M'].'|+|'.$field['default_D'].'|+|'.$field['default_Y'];
				}
			}
			else if($field['type']=='name'){
				$required = $_POST['required'];
				if($required=='no') $f1Req = $f2Req = 0;
				else if($required=='field1'){ $f1Req = 1; $f2Req = 0; }
				else if($required=='field2'){ $f1Req = 0; $f2Req = 1; }
				else if($required=='both') $f1Req = $f2Req = 1;
				$field['field1']['label'] = $_POST['label1'];
				$field['field1']['default'] = $_POST['default1'];
				$field['field1']['required'] = $f1Req;
				$field['field2']['label'] = $_POST['label2'];
				$field['field2']['default'] = $_POST['default2'];
				$field['field2']['required'] = $f2Req;
				if(strpos(strtolower($field['field1']['label']), 'password')!==false){
					$continue = false;
					$return['message'] = 'Label name is not allowed.';
				}
				else {
					$parsed = 
					'name|-|'.$field['field1']['label'].'|=|'.$field['field2']['label'].'|-|'
					.$field['field1']['default'].'|=|'.$field['field2']['default'].'|-|'
					.$field['field1']['required'].'|=|'.$field['field2']['required'];
				}
			} //end if($action=='addField' || $action=='editFormField')
			else if($field['type']=='states'){
				$parsed = 
				'select|-|'.$field['label'].'|-|'.$field['label_note'].'|-|'.$field['required'].'|-|';
				$fieldType = 'select';
				$states = $hub->states();
				$options = array();
				$i = 0;
				foreach($states as $key=>$value){
					$options[$i]['value'] = $key;
					$options[$i]['text'] = $value;
					$parsed .= $key.'|+|'.$value.'|=|';
				}
				if(substr($parsed, -3)=='|=|') $parsed = substr($parsed, 0, -3); //strip off trailing '|=|'
			}
			else if($field['type']=='section'){
				$parsed = 
				'section|-|'.$field['label'].'|-||-||-|';
			}
			else $continue = false;
			
			if($action=='editFormField'){
				if($fieldID>=0){
					//get existing fields
					$query = "SELECT data FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
					if($f = $hub->queryFetch($query)){
						$fields = explode('[==]', $f['data']);
						//overwrite the existing field's string
						$fields[$fieldID] = $parsed;
						$parsed = trim(implode('[==]', $fields), '[==]');
						$clearExisting = true;
					}
					else {
						$continue = false;
						$return['message'] = 'Invalid form ID';
					}
				}
				else {
					$continue = false;
					$return['message'] = 'Invalid field';
				}
			}
		} //end if($action=='addField' || editField)
		else if($action=='delField'){
			$fieldID = is_numeric($_POST['fieldID']) ? $_POST['fieldID'] : '';
			if($fieldID >= 0){
				//get existing fields
				$query = "SELECT data FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
				if($f = $hub->queryFetch($query)){
					//$fields = $hub->parseCustomFormFields($f['data']);
					$fields = explode('[==]', $f['data']);
					unset($fields[$fieldID]);
					$parsed = trim(implode('[==]', $fields), '[==]');
					$clearExisting = true;
				}
				else {
					$continue = false;
					$return['message'] = 'Invalid form ID';
				}
			}
			else {
				$continue = false;
				$return['message'] = 'Invalid field:';
			}
		} //end else if($action=='delField')
		else if($action=='shiftField'){
			$mode = $_POST['mode'];
			$fieldID = is_numeric($_POST['fieldID']) ? $_POST['fieldID'] : '';
			if(($mode=='Up' || $mode=='Down') && $fieldID){
				//get existing fields
				$query = "SELECT data FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
				if($f = $hub->queryFetch($query)){
					$fields = explode('[==]', $f['data']);
					//get fields to be swapped and swap them
					$temp = $fields[$fieldID];
					if($mode=='Up'){
						$temp2 = $fields[$fieldID-1];
						$fields[$fieldID] = $temp2;
						$fields[$fieldID-1] = $temp;
					}
					else {
						$temp2 = $fields[$fieldID+1];
						$fields[$fieldID] = $temp2;
						$fields[$fieldID+1] = $temp;
					}
					$parsed = trim(implode('[==]', $fields), '[==]');
					$clearExisting = true;
				}
				else {
					$continue = false;
					$return['message'] = 'Invalid form ID';
				}
			}
			else $continue = false;
		} //end else if($action=='shiftField')
		else if($action=='editOption'){
			$continue = false;
			$query = "SELECT options FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
			$a = $hub->queryFetch($query);
			if($a['options']){
				//options key:
				//options[0]: show reset button (0 or 1)
				//options[1]: submit button text
				//options[2]: redirect url
				//options[3]: ajax submit
				$options = explode('||', $a['options']);
				$option = $_POST['option'];
				if($option=='showReset'){
					$value = $_POST['value'] ? 1 : 0;
					$options[0] = $value;
				}
				else if($option=='submitText'){
					$value = $_POST['value'];
					$options[1] = $value;
				}
				else if($option=='redirUrl'){
					$value = $_POST['value'];
					$options[2] = $value;
				}
				else if($option=='ajaxSubmit'){
					$value = $_POST['value'];
					$options[3] = $value;
				}
				$options2 = implode('||', $options);
				$query = "UPDATE lead_forms SET options = '".$options2."' WHERE id = '".$formID."' AND user_id = '".$userID."'";
				if($hub->query($query)){
					$noError = true;
				}
				else $return['message'] = 'Error updating options.';
			}
		}
		else if($action=='updateClassAccess'){
			$continue = false;
			$classID = is_numeric($_POST['classID']) ? $_POST['classID'] : '';
			if($classID){
				//get current list of allowed classes
				$query = "SELECT id, no_access_classes FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
				$form = $hub->queryFetch($query, NULL, 1);
				if($form){
					$blockedClasses = explode('::', $form['no_access_classes']);
					if($blockedClasses && is_array($blockedClasses)){
						foreach($blockedClasses as $key=>$value){
							if($value==$classID){
								unset($blockedClasses[$key]);
								$found = true;
							}
						}
						if(!$found){
							$blockedClasses[] = $classID;
						}
						$parsed = trim(implode('::', $blockedClasses), '::');
					}
					else {
						$parsed = $classID.'::';
					}
					
					$query = "UPDATE lead_forms SET no_access_classes = '".$parsed."' WHERE id = '".$formID."' LIMIT 1";
					if($hub->query($query)){
						$noError = true;
					}
					else $return['message'] = 'Error updating allowed user types.';
				}
			}
		}
		else $continue = false;
	} //end if($formID && $userID)
	else $continue = false;
	
	if($continue){
		if($clearExisting) $update = $parsed;
		else {
			//get existing data
			$query = "SELECT data FROM lead_forms WHERE id = '".$formID."' AND user_id = '".$userID."'";
			$d = $hub->queryFetch($query);
			if($d['data']){
				$update = $d['data'].'[==]'.$parsed;
			}
			else $update = $parsed;
		}
		//sanitize data
		$update = $hub->sanitizeInput($update);
		//update
		$query = "UPDATE lead_forms SET data = '".$update."' WHERE id = '".$formID."' AND user_id = '".$userID."'";
		if($hub->query($query)){
			$return['success'] = 1;
			$return['refreshUrl'] = 'hub-forms.php?mode=edit&id='.$formID;
		}
		else {
			$return['success'] = 0;
			$return['message'] = 'Update error';
			//$return['message'] .= ': '.mysqli_error();
		}
	}
	else if($noError){
		$return['success'] = 1;
		$return['refreshUrl'] = 'hub-forms.php?mode=edit&id='.$formID;
	}
	else {
		$return['success'] = 0;
		if(!$return['message']) $return['message'] = 'Validation error';
	}
	if($multiUser) $return['refreshUrl'] .= '&multi_user=1';
	
	//output result
	echo json_encode($return);
}
?>