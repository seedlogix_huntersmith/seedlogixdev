<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize
	require_once('./inc/auth.php');
	
	//require db connection
	require_once('inc/domains.class.php');
	$connector = new Domains();
	
	$table = $connector->sanitizeInput($_GET['type']);
	if(substr($_GET['delete'], 0, 7)=='http://') $domain = substr($_GET['delete'], 7);
	else $domain = $_GET['delete'];
	$type = ($_GET['type']=="hub") ? "Hub" : "Blog";
	$id = is_numeric($_GET['id']) ? $_GET['id'] : 0;
	
	//If user has clicked "remove"
	//validate ownership
	$check = $connector->queryFetch("SELECT user_id, user_parent_id, lincoln_domain FROM `".$table."` WHERE id = '".$id."'");
	if($_GET['delete'] && ($check['user_id']==$_SESSION['user_id'] || $check['user_parent_id']==$_SESSION['login_uid'])){
		//Delete addon or subdomain
		if($_GET['dtype']=="addon"){
			if($check['lincoln_domain']){
				//delete the domain over on the lincoln server
				$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
				$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
				$secHash = md5($domain.$salt);
				$secHash = md5($secHash.'addon'.$pepper);
				//makes the hash only good if it was submitted today:
				$secHash = md5($secHash.date('d'));
				$delete = file_get_contents('http://50.97.78.210/secure/remove_domain.php?domain='.$domain.'&mode=addon&hash='.$secHash);
			} else {
				$delete = $connector->deleteAddon($_GET['delete'], $_SESSION['user_id']);
			}
		}
		if($_GET['dtype']=="sub"){
			if($check['lincoln_domain']){
				//delete the domain over on the lincoln server
				$salt = 'dfgdga438se4rSW#RA@ERAQ@E,mdfdzfawerawr';
				$pepper = 'ABABA2er09fdsgfawrm 312@!!@!@!@ dfgmbfg...';
				$secHash = md5($domain.$salt);
				$secHash = md5($secHash.'sub'.$pepper);
				//makes the hash only good if it was submitted today:
				$secHash = md5($secHash.date('d'));
				$delete = file_get_contents('http://50.97.78.210/secure/remove_domain.php?domain='.$domain.'&mode=sub&hash='.$secHash);
			} else {
				$delete = $connector->deleteSubdomain($_GET['delete']);
			}
		}
		
		//If successful
		if($delete['success']==true || strpos($delete['message'], "has been removed")){
			
			//Remove domain from item row
			$query = "UPDATE `".$table."` SET domain = ''";
			if($type=="Hub") 
				$query .= ", blog = '', connect_blog_id = ''";
			if($type=="Blog" && $_GET['dtype']=="addon") 
				$query .= ", website_url = '', connect_hub_id = '', connect_hub_name = ''";
			$query .= " WHERE id = '".$id."'";
			
			if($connector->query($query)){
				echo '<span style="color:green;">Domain '.$domain.' sucessfully removed!</span>';
				
				//If deleting an addon domain, also remove subdomains from other items
				//This only removes the DB entry. Actual subdomains are removed by cpanel when addondomain is removed
				if($_GET['dtype']=="addon"){
					//remove www.
					if(strpos($domain, "www.")!==false) $addon = substr($domain, 4);
					else $addon = $domain;
					$query = "UPDATE blogs";
					$query .= " SET domain = '', website_url = '', connect_hub_id = '', connect_hub_name = ''";
					$query .= " WHERE domain LIKE '%.".$addon."%'";
					$connector->query($query);
					$query = "UPDATE hub";
					$query .= " SET domain = ''";
					$query .= " WHERE domain LIKE '%.".$addon."%'";
					$connector->query($query);
				}
			}
			else {
				echo '<span style="color:red;">';
				echo 'Domain '.$domain.' was removed, but there was an error updating your '.$type.'.';
				echo '</span>';
			}
			
		}
		else {
			echo '<span style="color:red;">';
			echo 'There was an error deleting your domain.  Try again or contact customer service to have it manually removed.';
			echo '<br />'.$delete['message'];
			echo '</span>';
		}
	}
?>
<h1>Domains Manager</h1>
Below are the domains you have successfully tied to our server.<br />
Removing a domain does not delete the whole hub or blog from your dashboard -- It only the deletes "domain" field and stops the domain from forwarding to that site.<br />
<b>Note:</b> When you remove a domain it removes all of the subdomains attached to it as well.  However, you can remove a subdomain without affecting the master domain and its other subdomains.  After removing a domain or subdomain it can easily be re-added.
<div id="domains">
	<h2>Hub Domains</h2>
	<?php
		//Select required information from hub table
		//Would use hub class's getHub() function for this but it fetches too much unneeded data
		$query = "SELECT id, name, domain, lincoln_domain FROM hub";
		$query .= " WHERE user_id = '".$_SESSION['user_id']."' AND (cid = '".$_SESSION['campaign_id']."'";
		if($_SESSION['reseller']) $query .= " OR cid = 99999";
		$query .= ") AND domain != ''";
		$result = $connector->query($query);
		if($result && $connector->numRows($result)){
			while($row = $connector->fetchArray($result)){
				//Break up domain and remove http://
				$domain = $connector->breakUpURL($row['domain']);
				$dtype = $domain['sub']=="www" ? "addon" : "sub";
				
				echo '<div class="domain-item" id="hub'.$row['id'].'">';
				echo '<p class="title"><b>'.$domain['sub'].'</b>.'.$domain['name'].'.'.$domain['tld'].'</p>';
				echo '<p class="desc" style="width:300px;"><i>'.$row['name'].'</i> Hub</p>';
				echo '<p class="remove">';
				echo '<a href="domains-manager.php?delete='.$row['domain'].'&type=hub&id='.$row['id'].'&dtype='.$dtype.'" class="dflt-link">';
				echo '[Remove]';
				echo '</a>';
				echo '</p>';
				if($dtype=='addon'){
					//echo '<p style="float:right;">';
					//echo '<a href="#" class="installGoogleApps dflt-link" rel="'.$row['domain'].'">';
					//echo '[Install Google Apps Support]';
					//echo '</a>&nbsp;&nbsp;&nbsp;';
					//echo '</p>';
					echo '<p style="float:right;">';
					echo '<a href="setup_email.php?domain='.$row['domain'].'&lincoln='.$row['lincoln_domain'].'" class="dflt-link">';
					echo '[Setup Email]';
					echo '</a>&nbsp;&nbsp;&nbsp;';
					echo '</p>';
				}
				echo '</div>';
			}
		}
	?>
	<h2>Blog Domains</h2>
	<?php
		//Select required information from hub table
		//Would use blog class's getBlogs() function for this but it fetches too much unneeded data
		$query = "SELECT id, domain, blog_title FROM blogs";
		$query .= " WHERE user_id = {$_SESSION['user_id']} AND cid = {$_SESSION['campaign_id']}";
		$query .= " AND domain != ''";
		$result = $connector->query($query);
		if($result && $connector->numRows($result)){
			while($row = $connector->fetchArray($result)){
				//Break up domain and remove http://
				$domain = $connector->breakUpURL($row['domain']);
				$dtype = $domain['sub']=="www" ? "addon" : "sub";
				
				echo '<div class="domain-item" id="blogs'.$row['id'].'">';
				echo '<p class="title"><b>'.$domain['sub'].'</b>.'.$domain['name'].'.'.$domain['tld'].'</p>';
				echo '<p class="desc"><i>'.$row['blog_title'].'</i> Blog</p>';
				echo '<p class="remove">';
				echo '<a href="domains-manager.php?delete='.$row['domain'].'&type=blogs&id='.$row['id'].'&dtype='.$dtype.'" class="dflt-link">';
				echo '[Remove]';
				echo '</a>';
				echo '</p>';
				echo '</div>';
			}
		}
	?>
	<br /><br />
	<h2></h2>
	<br style="clear:both" />
</div>
