<?php

require_once 'ResponseEmail.php';

/**
 * ResponseNotification Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

abstract class ResponseNotification implements Notification
{    
    /**
     *  The Responder
     * @var User
     */
    protected $user;
    /**
     * the SendTo info
     * 
     * @var SendTo
     */
    protected $sendTo;
    
    /**
     *
     * @var Responder
     */
    protected $responder;
    
// see below    protected $_variables = array();
    
    /**
     * @return SendTo 
    abstract function getRecipient();
    
    abstract function hasTheme();
     */
    
    function __construct(ResponseTriggerEvent $Trigger) {
        $this->form = $Trigger->form;
        $this->responder = $Trigger->getResponder();
        $this->user = $Trigger->user;
        
        $this->init($Trigger);
    }
    
    function init(ResponseTriggerEvent $TriggerEvent)
    {
        
    }
    
}
