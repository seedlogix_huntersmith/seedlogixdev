<?php

require_once QUBEPATH . '../library/Smarty-3.1.8/libs/Smarty.class.php';

/**
 * Description of QubeSmarty
 *
 * @author amado
 */
class QubeSmarty extends Smarty {
    
    public function __construct() {
        parent::__construct();
        
        $this->setTemplateDir(QUBEPATH . QUBE::EMAIL_THEMES_PATH)
            ->setCompileDir(QUBEPATH . 'cache' . DS)
            ->setPluginsDir(SMARTY_PLUGINS_DIR)
            ->setCacheDir(QUBEPATH . 'cache' . DS);
#            ->setConfigDir('.' . DS . 'configs' . DS);
        $this->error_reporting = E_ALL & ~E_NOTICE;
    }
}
