<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * QubeDataProcessor Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
abstract class QubeDataProcessor extends Logger
{
    /**
     *
     * @var Qube The Qube Instance
     */
    protected $qube;
    
    function __construct(Qube $Q = NULL, $logmode = Logger::DOLOG)
    {
				parent::__construct($logmode);
        $this->initialize($Q);
    }
		
		/**
		 * Utility method to create a QDP from another QDP
		 * 
		 * 
		 * @param QubeDataProcessor $P
		 * @return static
		 */
		static function create(QubeDataProcessor $P)
		{
			$obj	=	new static($P->getQube());
			return $obj;
		}
		
		/**
		 * 
		 * @return \QubeDriver
		 */
		function getDriver(){
			return new QubeDriver($this->getQube());
		}
		
    protected function initialize(Qube $Q	=	NULL)
    {
        $this->qube = $Q;
    }
    
    /**
     * 
     * @return Qube
     */
    function getQube(){
        return $this->qube ? $this->qube : Qube::Start();
    }
		
    abstract function Execute(VarContainer $vars);
		
		/**
		 * 
		 * @param type $type
		 * @return QubeLogPDO
		 */
		function getPDO($type	=	'master')
		{
			return $this->getQube()->GetPDO($type);
		}

	/**
	 *
	 * @param $query
	 * @return QubeLogPDOStatement
	 */
		function prepare($query)
		{
			return $this->getPDO()->prepare($query);
		}
		
		/**
		 * 
		 * @param string $query
		 * @return \QubeLogPDOStatement
		 * @return QubeLogPDOStatement
		 */
		function query($query)
		{
			return $this->getPDO()->query($query);
		}		
}
