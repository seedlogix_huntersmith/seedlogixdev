<?php

/**
 * Description of EmailResponsesQueueProcessor
 *
 * @author amado
 */
class EmailResponsesQueueProcessor extends QubeDataProcessor {
    
    
    /**
     * build the query to be executed.
     * 
     * @return PDOStatement
     */
    function getLeadFormResponseQuery()
    {
        $Q  =   Qube::GetPDO()->query("SELECT n.name as form_name, h.name as hub_name,
                    ifnull(p.page_title, p2.page_title) as page_name,
                    f.search_engine, f.keyword, f.ip,
                    c.name as collab_name, c.email as collab_email, ifnull(r.main_site, '6qube.com') as reseller_domain,
                    f.*,
group_concat(concat('<p><strong>',d.label, ':</strong><br />', d.value, '</p>') separator '\n') as data
                    FROM collab_email_queue q
                    INNER JOIN campaign_collab c ON ( c.id = q.collab_id ) 
                    INNER JOIN leads f ON ( f.id = q.data_id ) 
                    INNER JOIN lead_forms n ON ( n.id = f.lead_form_id) 
                    LEFT JOIN leads_data d ON ( d.lead_id = f. id)
LEFT JOIN hub h ON (h.id = f.hub_id)
LEFT JOIN hub_page2 p2 ON (p2.id = f.hub_page_id AND h.pages_version=2)
LEFT JOIN hub_page p ON (p.id = f.hub_page_id AND h.pages_version=1)
                    LEFT JOIN users u ON ( u.id = f.user_id ) 
                    LEFT JOIN resellers r ON ( (
                    u.parent_id =0
                    AND r.admin_user = u.id
                    )
                    OR r.admin_user = u.parent_id ) WHERE q.data_type = 'lead' and date_sent = 0
                    GROUP BY q.q_id"); 
                
        $Q->setFetchMode(PDO::FETCH_OBJ);
        
        return $Q;
    }
    /**
     * @return PDOStatement 
     */
    function getContactFormResponseQuery()
    {
        $Q  =   Qube::GetPDO()->query("SELECT c.name as collab_name, c.email as collab_email, ifnull(r.main_site, '6qube.com') as reseller_domain, f.*
                    FROM collab_email_queue q
                    INNER JOIN campaign_collab c ON ( c.id = q.collab_id ) 
                    INNER JOIN contact_form f ON ( f.id = q.data_id ) 
                    LEFT JOIN users u ON ( u.id = f.user_id ) 
                    LEFT JOIN resellers r ON ( (
                    u.parent_id =0
                    AND r.admin_user = u.id
                    )
                    OR r.admin_user = u.parent_id ) WHERE q.data_type = 'contact' and date_sent = 0");  
        
        $Q->setFetchMode(PDO::FETCH_CLASS, 'ContactDataModel');
        
        return $Q;
    }
    
    function sendContactFormNotifications(NotificationMailer $Mailer)
    {        
        $Q = $this->getContactFormResponseQuery();
    
        $emails = $Q->fetchAll();

        foreach($emails as /** @var ContactData */
                    $contact_form)
        {
            $Notification = $contact_form->Notify();
            $Notification->setSender(new SendTo(NULL, 'no-reply@' . $contact_form->reseller_domain));
            //@todo add headers
            if(!$Mailer->Send($Notification, new SendTo($contact_form->collab_name, $contact_form->collab_email)))
            {
                Qube::Fatal('Failed 6Qube Notification', $Notification, $Mailer->getError());
            }
        }
    }
    
    function sendLeadFormNotifications(NotificationMailer $Mailer)
    {
        $Q = $this->getLeadFormResponseQuery();
    
        $emails = $Q->fetchAll();

        foreach($emails as /** @var ContactData */
                    $lead)
        {
            // manually load the lead form notification object
            $Notification   = new LeadFormNotification($lead);            
            $Notification->setSender(new SendTo(NULL, 'no-reply@' . $lead->reseller_domain));
            
            //@todo add headers
            if(!$Mailer->Send($Notification, new SendTo($lead->collab_name, $lead->collab_email)))
            {
                Qube::Fatal('Failed 6Qube Notification', $Notification, $Mailer->getError());
            }
        }
        
    }
    
    /**
     *
     * @param VarContainer $vars (requires Mailer property (NotificationMailer type))
     */
    function Execute(VarContainer $vars) {        
        $Mailer = $vars->Mailer;
        
        $this->sendContactFormNotifications($Mailer);
        $this->sendLeadFormNotifications($Mailer);
    }
}
