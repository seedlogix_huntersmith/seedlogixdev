<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of QubeLogPDOStatement
 *
 * @author amado
 */
class QubeLogPDOStatement extends PDOStatement
{
    protected static $fp   =   NULL;
    protected static $filename  =   '';
    
		protected $pdo	=	NULL;
		
    
    protected function __construct(QubeLogPDO $pdo)
    {
			$this->pdo	=	$pdo;
    }
    
		/*
    static function write($q)
    {
        if(!self::$fp && self::$filename)
            self::$fp   =   fopen(self::$filename, 'a');
        
        if(self::$fp)	fwrite(self::$fp, $q . ";\n\n");
    }
    */
    
    public function execute($input_parameters = NULL) {
        QubeLogPDO::$last_statement =   array($this, $input_parameters);

//	self::write($this->queryString);
				try{
					$retstmt	=	parent::execute($input_parameters);
				}catch(PDOException $e)
				{
					if($e->getCode() != 'HY000' || $this->pdo->isMaster())	// FAIL HARD
					{
						throw $e;
//						Qube::Fatal($e->getMessage() . ', ' . $e->getCode(), $e);
					}else
					{	// case: pdo is not master, and getcode == 'hy000'
						Qube::ForceMaster();
						$this->pdo->LogSlaveFailure();
						Qube::LogError($e->getMessage() . ', ' . $e->getCode(), $e);
					}
				}
				return $retstmt;
    }
    //put your code here
}
