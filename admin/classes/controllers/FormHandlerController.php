<?php

throw new Exception();

require_once 'public.php';

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once QUBEPATH . '../models/Responder.php';

require_once(QUBEADMIN . 'inc/local.class.php');

/**
 * DashBoard Class
 *
 * controlls which views to show to the user
 * 
 */

class FormHandlerController extends PublicController
{    
    
    function handleContactForm($data)
    {
        $Qube   =   Qube::Start();
        $vars   =   new VarContainer($data);
        $D = $Qube->GetDriver();
        $Mailer = $Qube->getMailer();
        
        // @redundant
	$type = $vars->type;    // $validator->sanitizeInput($_POST['type']);
        $table = $type;
        
        $tableref = array('direc' => 'directory',
                            'artic' => 'articles',
                            'press' => 'press_release',
                            'hub' => 'hub',
                            '' => '',
                                'blogs' => 'blogs',
                            'contact_form' => 'contact_form');
        /*
	if($type=="direc") $table = "directory";
	else if($type=="artic") $table = "articles";
	else if($type=="press") $table = "press_release";
//	else $table = $type;
        */
        
        // this is the hub id:
        $type_id = $vars->type_id;        
        // @redundant
        
        $ContactForm = new ContactData;
        $form_columns = $ContactForm->_getInsertColumns();        
        
        foreach($vars as $columnname => $value)
        {
            if($columnname == 'cid') continue;
            $ContactForm->_set($columnname, $value);
        }
        try{
        $ContactForm->_set('comments', htmlentities($vars->comments, ENT_QUOTES));
        }catch(Exception $exception)
        {
	  $ContactForm->_set('comments', '');
	  // @error log
        }
        
        // @fixed load $cid from $table
        $r = $D->getDb()->query('SELECT cid FROM ' . $tableref[$type] . ' WHERE id = ' . (int)$type_id);
        $ContactForm->_set('cid', $r->fetchColumn());
        
        // end
        
        $ContactForm->_set('id', '');
        
        
        
        if(!$ContactForm->Exists())
        {
            $q      = self::GetUserResellerInfoQuery($D, $ContactForm->user_id);
            $user = $q->Exec()->fetch();
            
            $ContactForm->_set('parent_id', $user->parent_id);
            $ContactForm->Save();
            
                    
            
            $collaborators  = $D->queryCampaignCollabWhere('(T.scope = "campaign" OR EXISTS (SELECT collab_id FROM campaign_collab_siteref r WHERE r.collab_id = T.id AND
            r.site_id = %d )) AND T.campaign_id = %d AND T.active = 1 AND T.trashed = "0000-00-00"', $type_id, $ContactForm->cid)->Exec()->fetchAll();

            //** sends notification using reseller data where necessary.
            $Notification   = $ContactForm->Notify();
            
            //** SENDER IS RESELLER (OR OWN USER EMAIL IF RESELLER IS NOT FOUND)
            $sender_addr   =   'no-reply@' . $user->main_site;
            if(!$user->main_site)
            {
                // @log 
                $sender_addr   =   'no-reply@6qube.com';
                #2012-07-24 some reseller entries have blank main_site value, use 6qube.com
            }
            $Sender         = new SendTo($user->reseller_company, $sender_addr);
            // send from user's reseller
            $Notification->setSender($Sender);
            
            // to user
            $Recipient      = new SendTo(NULL, $user->username);
            
            $return = $Mailer->Send($Notification, $Recipient);

            if(!$return)
            {
                // @todo  should enqueue the message or log an error
                mail('error@sofus.mx', 'Failed 6qube contactnotification',
                            print_r($user, true) . print_r($Notification, true) .
                        'error: ' . print_r($Mailer->getError(), true));
            }
            //** END SEND NOTIFICATION TO CLIENT
            
            
            $collaborator   = clone $user;
            $collab_msg_qstmt = Qube::GetPDO()->prepare('INSERT INTO collab_email_queue (data_id, data_type, collab_id)
                    VALUES (?, ?, ?)');
            Qube::GetPDO()->beginTransaction();
            foreach($collaborators as $collab)
            {
                // enqueue these messages
                    $collab_msg_qstmt->execute(array($ContactForm->id, 'contact', $collab->id));
                // end
                    
                $Notification   = $ContactForm->Notify();
                $Notification->setSender($Sender);
                if(!$Mailer->Send($Notification, new SendTo($collab->name, $collab->email)))
                {
                    Qube::Fatal('Failed 6Qube Notification', $Notification, $Mailer->getError());
                }
            }
            Qube::GetPDO()->commit();
            // now process all auto_responders and enqueue any responses
            // 
            // 

            // converted subqueries to outer joins for maximum efficiency.
            //  IGNORE INACTIVE and TRASHED responders.
            // had idea to schedule inactive responders in case the responder
            // was activated in near-future, but on second thought - thats kind 
            // of confusing.            
            
            $this->ProcessResponders($user, $tableref[$type], $vars, $ContactForm, $Mailer);
        }
        

        require_once QUBEPATH . '../models/Responder.php';

        //if the submitted form is a duplicate of an existing row, check to see if there's an
        //autoresponder with a return URL anyway.
        $S = $D->QueryObject('Responder')->Fields('return_url')
#                ->leftJoin(array('trash', 'tr'), 'tr.table_name = "auto_responders" AND tr.table_id=`T`.id')
                ->Where('`T`.table_name = ? AND `T`.table_id = ? AND `T`.sched = 0 AND `T`.active = 1 AND T.trashed = "0000-00-00"')
                ->Limit(1)->Prepare();

        $hasReturnURL = $S->execute(array($table, $type_id));

        $returnURL = $S->fetchColumn();

        //deb($S, $returnURL);
        $this->returnURL = ($hasReturnURL && $returnURL) ? $returnURL : $_POST['domain'];
        
        return new BlankView();
    }
    

    function ProcessResponders(User $user, $table = 'contact_form', VarContainer $vars, ContactData $form, NotificationMailer $Mailer)
    {
        // @todo see Execute comment, this type_id, and type variables
        //      need to be secured.
        
//        deb("Processing Responders...");
        
        require_once QUBEPATH . '../classes/QubeSmarty.php';
        
        // @redundant
	$type = $vars->type;    // $validator->sanitizeInput($_POST['type']);
        /*
	if($type=="direc") $table = "directory";
	else if($type=="artic") $table = "articles";
	else if($type=="press") $table = "press_release";
	else $table = $type;
         * 
         */
        $type_id = $vars->type_id;
        // @redundant
        
        $user_has_email_limit = $user->hasEmailLimit();
        
        /**
         * For some reason, the user has sent more emails than is allowed.
         * possibly their user class has been modified or downgraded. Who knows.
         *
         */
        if($user_has_email_limit && $user->email_limit <= $user->emails_sent)
        {
//            echo 'limit: ' , $user->email_limit, ' sent: ', $user->emails_sent;
            return;
        }
        
        // @todo should we process only a limited # of responders? or
        // should we limit on the cron script? mmmm
        
        $D = $this->Qube->GetDriver();
        
        require_once QUBEPATH . '../models/Responder.php';
        
        $Q = $D->queryActiveResponderWhere('`T`.table_name = ? AND `T`.table_id = ? AND T.trashed = "0000-00-00"')
                    ->leftJoin(array('v_users_wclass_wreseller', 'rc'), 'rc.id = T.user_id')->
                            addFields('rc.main_site, concat("/users/%s/hub_page/",T.logo) as site_logo');
        
        if($user_has_email_limit)
            $Q->Limit($user->email_limit-$user->emails_sent);        
        
        $Prepared = $Q->Prepare();
        
        $Prepared->execute(array($table, $type_id));
        
        $Responders = $Prepared->fetchAll();        

        // DO NOT increment user's emails_sent counter
        //  until all messages are actually sent.
        //  
        //  only schedule non-instant emails at this point to be processed
        //  by autoresponders_cron
        //  
        // respond or queue responses to the person who filled out the form..
        
        
        $total_sent_notifications = 0;
        $form_config    =   (object)array('contact_name' => $form->name,
                'contact_email' => $form->email,
                'contact_phone' => $form->phone,
                'contact_comments' => $form->comments);
        
        $Smarty =   new QubeSmarty();
        
                
        foreach($Responders as /** @var Responder */ $Responder)
        {            
            $R = new ScheduledResponse();
            $R->_set('contactdata_id', $form->id);
            $R->_set('responder_id', $Responder->id);
            $R->setScheduledTime($Responder->sched, $Responder->sched_mode);
            
            if($Responder->sched == 0)
            {
                

            $response_config    =   (object)array('theme' => $Responder->theme,
                    'main_site' => $Responder->main_site,
                    'responder_phone' => $Responder->phone,
                    'site_logo'       => sprintf($Responder->site_logo, UserModel::GetUserSubDirectory($Responder->user_id)),
                );
            list($Response, $sendTo)    =   $Responder->CreateResponse('contact_form', $form_config);
            $Responder->ConfigureResponse($Response,
                        $response_config,
                        $Smarty);
            
#            $sendTo =   new sendTo('test.sofus', 'test.instantresponder@sofus.mx');
            $isSent =   $Mailer->Send($Response, $sendTo);
                if($isSent)
                    $total_sent_notifications++;
                
                
            }
            
            // save the responder
            $R->Save();
        }
        
        $user->UpdateQuery('emails_sent = emails_sent+' . $total_sent_notifications)->Exec();
        
        
//        deb($Mailer->getMailer()->sentMessages);
    }    
    
    function ExecuteAction($action = NULL)
    {
            //load view
            switch($action)
            {
                case 'HandleContactForm':
                        return $this->handleContactForm($_POST);

            }

              # if the action is not supported, just show an error
              die('action does not exist.');
    }    
}
