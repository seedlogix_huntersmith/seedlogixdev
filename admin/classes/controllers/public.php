<?php

require_once QUBEPATH . '../library/Popcorn/BaseController.php';
require_once QUBEPATH . '../library/Popcorn/ThemeView.php';

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
/**
 * Description of admin
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

abstract class PublicController extends BaseController
{
    protected $_showheader = false;
    protected $_showfooter = false;
    
    function preExecute($action) {
        parent::preExecute($action);
        // STRIP SLASHES, PLEASE!
        if (get_magic_quotes_gpc()) {
        $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
        while (list($key, $val) = each($process)) {
            foreach ($val as $k => $v) {
                unset($process[$key][$k]);
                if (is_array($v)) {
                    $process[$key][stripslashes($k)] = $v;
                    $process[] = &$process[$key][stripslashes($k)];
                } else {
                    $process[$key][stripslashes($k)] = stripslashes($v);
                }
            }
        }
        unset($process);
        }
    }
    
    function _rewrite(BaseController $controller, $action, $params = array())
    {
        // action = {controller}_{action}
        list($controller, $action) = explode('_', $action);
        
        $params['action'] = $action;
        $params['ctrl']   = $controller;
        
        return '/admin/controller.php?' . http_build_query($params);
    }   
    
    function Initialize()
    {
        $this->_themes_path = QUBEPATH . 'views/';
        $this->_theme = 'admin';
        $this->setRewriter(array($this, '_rewrite'));
    }
    
    protected function returnAjaxError($message)
    {
        $success = array('id' => 1,
                        'action' => 'replace',
                        'container' => '#error',
                        'message' => $message);
        
        return $this->getAjaxView(array('success' => $success));
    }
    
    protected function returnAjaxRedirect($action, $params = array())
    {
        $success = array('id' => 1,
                        'action' => 'load',
                        'page' => call_user_func(array($this, 'ActionUrl'), $action, $params));
        
        return $this->getAjaxView(array('success' => $success));        
    }
    
}

