<?php

require_once 'admin.php';

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once QUBEPATH . '../models/Responder.php';

require_once(QUBEADMIN . 'inc/local.class.php');

/**
 * DashBoard Class
 *
 * controlls which views to show to the user
 * 
 */

class WebsiteAdminController extends AdminController
{    
	function Initialize()
	{
	  # call parent::Initialize()
	    parent::Initialize();
	    
		//check to see if the user has campaigns
		#use  initialize to initialize member variables		
		#loadDefault();
	}
	
	//view individual website
	function ViewWebsite()
	{
		$V  = $this->getView('website-dashboard');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
		//return $this->getView('campaign-dashboard');
	}
	
	//view Settings
	function ViewSettings()
	{
		$V  = $this->getView('website-settings');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view Theme
	function ViewTheme()
	{
		$V  = $this->getView('website-theme');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view SEO
	function ViewSEO()
	{
		$V  = $this->getView('website-seo');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view Edit Regions
	function ViewEditRegions()
	{
		$V  = $this->getView('website-editRegions');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view Photos
	function ViewPhotos()
	{
		$V  = $this->getView('website-photos');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view Social
	function ViewSocial()
	{
		$V  = $this->getView('website-social');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view Pages
	function ViewPages()
	{
		$V  = $this->getView('website-pages');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
	}
	
	//view default (Dashboard)
	function Dashboard()
	{
		$V  = $this->getView('dashboard-websites');
			
		$V->init('camp', new Local);
		$V->init('reseller', $_SESSION['reseller']);
		$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
			
		return $V;
	}
	
	/**
	 @return 
	 */
	 
	function ExecuteAction($action = NULL)
	{
	  	//load view
		switch($action)
		{
			case 'website':
				return $this->ViewWebsite();
				break;
			case 'settings':
				return $this->ViewSettings();
				break;
			case 'theme':
				return $this->ViewTheme();
				break;
			case 'seo':
				return $this->ViewSEO();
				break;
			case 'editRegions':
				return $this->ViewEditRegions();
				break;
			case 'photos':
				return $this->ViewPhotos();
				break;
			case 'social':
				return $this->ViewSocial();
				break;
			case 'pages':
				return $this->ViewPages();
				break;
		  	case 'loadDefault':
		  		return $this->Dashboard();
		  		break;
		  }
		  
		  # if the action is not supported, just show an error
		  //die('action does not exist.');
		  return $this->Dashboard();
	}    
}
