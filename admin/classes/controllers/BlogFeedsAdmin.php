<?php

require_once 'admin.php';	// admin ctonroller

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once dirname(__FILE__) . '/../../../hybrid/bootstrap.php';
#define('QUBE_NOLAUNCH', 1);
require_once HYBRID_PATH . 'classes/DataAccess/BlogDataAccess.php';
require_once HYBRID_PATH . 'controllers/DashboardBaseController.php';
/**
 * campcollab Class
 *
 * process pages for campaign collaboration contacts.
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class BlogFeedsAdminController extends AdminController {
	/**
	 *
	 * @var BlogDataAccess
	 */
	protected $da;
	
    function showAllMasterPosts($user_id)
    {
			$masterposts		=	$this->da->getUserMasterPosts($user_id);
			$V  = $this->getView('blogfeeds_masterposts');
			$V->init('masterposts', $masterposts);

			return $V;
    }

	function createMasterPost($post_data, $user_id) {// create a post
		    $ctrl   =   new DashboardBaseController();
		    $ctrl->getQube()->setDashboardUserID($user_id);
			$ctrl->setDataOwnerID($user_id);
			
		$dum	=	new UserWithRolesModel();
		$ps	=	new PermissionsSet($dum);
		$ps->setPermission('is_system');
			$dum->setPermissionsSet($ps);
		$dum->renderPermissions();
		    $this->da->setActionUser($dum);
			
		$MM	=	new ModelManager();
		$post_data['user_id']	=	$user_id;
		$post_data['blog_id']	=	0;
#var_dump($user_id);
#deb($post_data);
		    
		$masterPost	=	$MM->doSaveBlogPost($post_data, 0, $this->da);
		if($masterPost instanceof ValidationStack)
		{
                    echo json_encode($masterPost->getErrors());
#                        var_dump($masterPost);
			throw new Exception();
		}else{
                    $ret    =   $MM->doSaveBlogPost(array('master_ID' => $masterPost->getID()), $masterPost->getID(), $this->da);
                }
		return $this->returnFuckedUpAjaxBullshitPage($this->showAllMasterPosts($user_id));
	}
        
        function returnFuckedUpAjaxBullshitPage(ThemeView $p)
        {
            $obj    =   array('success' => array('container' => '#ajax-load',
                        'action' => 'replace',
                            'message' => $p->toString()
                        ));
            return $this->getAjaxView($obj);
        }
        
        function editMasterPost($post_ID)
        {
            $user_feeds =   $this->da->getFeeds(array('user_ID' => $_SESSION['login_uid']));
            $v  =   $this->getView('blogfeeds_editpost');
            $v->init('blogID', 0);
            $v->init('postID', $post_ID);
            $v->init('isMaster', true);
            $v->init('feeds', $user_feeds);
            
            return $v;  //$this->returnFuckedUpAjaxBullshitPage($v);
        }

	function ExecuteAction($action = NULL) {
		$this->Initialize();
		$this->da	=	new BlogDataAccess();
		
		switch ($action) {
                    case 'editmaster':
                        return $this->editMasterPost($_GET['id']);
                        
			case 'newmaster':
				return $this->createMasterPost($_POST, $_SESSION['login_uid']);
		}

		return $this->showAllMasterPosts($_SESSION['login_uid']);
	}

}
