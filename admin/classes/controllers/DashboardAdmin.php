<?php

require_once 'admin.php';

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once QUBEPATH . '../models/Responder.php';

require_once(QUBEADMIN . 'inc/local.class.php');

/**
 * DashBoard Class
 *
 * controlls which views to show to the user
 * 
 */

class DashboardAdminController extends AdminController
{    
	function Initialize()
	{
	  # call parent::Initialize()
	    parent::Initialize();
	    
		//check to see if the user has campaigns
		#use  initialize to initialize member variables		
		#loadDefault();
	}
	
	function ViewCampaign()
	{
		$V  = $this->getView('campaign-dashboard');
			
	  	$V->init('camp', new Local);
	  	$V->init('reseller', $_SESSION['reseller']);
	  	$V->init('resellerParentID', $_SESSION['parent_id']);
		$V->init('items', $C);
		
	  	return $V;
		//return $this->getView('campaign-dashboard');
	}
	
        function viewDashboard()
        {            
            $camp   =   new Local;
            $data_array	= $camp->ANAL_getVisitsForCampaign($_SESSION['parent_id'], 'lastWeek');	
            
            $V  = $this->getView('dashboard-campaigns');

            $V->init('data_array', $data_array);
            $V->init('reseller', $_SESSION['reseller']);
            $V->init('items', $C);

            return $V;
        }
	/**
	 @return 
	 */
	 
	function ExecuteAction($action = NULL)
	{
	  	//load view
		switch($action)
		{
                    case 'campaign':
                            return $this->ViewCampaign();

                    case 'loadDefault':
                            return $this->viewDashboard();
                      break;
                }
		  
		  # if the action is not supported, just show an error
		  die('action does not exist.');
	}    
}
