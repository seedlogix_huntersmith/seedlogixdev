<?php

require_once 'admin.php';   // admin ctonroller

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';

/**
 * campcollab Class
 *
 * process pages for campaign collaboration contacts.
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */

class NotificationsAdminController extends AdminController
{
    /**
     *
     * @param type $scope
     * @param type $campaign_id
     * @return array
     */
    function getCollabs($scope, $campaign_id, $trashed = '0000-00-00')
    {
        $D  = Qube::GetDriver();
        $st = $D->queryCampaignCollabsWhere('campaign_id = ? AND scope= ? AND trashed = ?')->Prepare();
        
        $r  = $st->execute(array($campaign_id, $scope, $trashed));
        
        return $st->fetchAll();
        
        return $D;
    }
    
    function showCollabsByScope($scope)
    {
        $C  = $this->getCollabs($scope, $_SESSION['campaign_id']);
        
        $V  = $this->getView('collabs_list');
        $V->init('items', $C);
        
        return $V;
    }
    
    function createCampaignCollab($collabdata)
    {
        $response = array();
        
//        $response = $collabdata;
        if(!preg_match('/[^\s]+/', $collabdata['name']))
            return $this->returnAjaxError('Name field is empty');
        
        if(!preg_match('/^[^\s@]+@[^\s@]+$/', $collabdata['email']))
            return $this->returnAjaxError('Email value is invalid');
        
        if(!isset($collabdata['scope']))
            $this->returnAjaxError ('Scope value is missing.');
        
        $is_campaign_collab = $collabdata['scope'] == 'all';
        
        if(!$is_campaign_collab && !$collabdata['sites'] || !$collabdata['campaign_id'])
            return $this->returnAjaxError('Invalid values for scope.');
        
        $D = Qube::GetDriver();
//        $D->getf();
        
        $CampCollab = new CampaignCollab();
        $CampCollab->_set('name', $collabdata['name']);
        $CampCollab->_set('email', $collabdata['email']);
        $CampCollab->_set('campaign_id', $collabdata['campaign_id']);
        $CampCollab->_set('active', @$collabdata['active'] ? 1 : 0);
 
	$collabdata['active']	=	isset($collabdata['active']) ? 1 : 0;       
deb($CampCollab, $collabdata);
        if($is_campaign_collab){
            $CampCollab->_set('scope', CampaignCollab::SCOPE_CAMPAIGN);
		$collabdata['scope']	=	CampaignCollab::SCOPE_CAMPAIGN;
        }else{
		$collabdata['scope']	=	CampaignCollab::SCOPE_CUSTOM;
            $CampCollab->_set('scope', CampaignCollab::SCOPE_CUSTOM);
        }
#deb($CampCollab);        
	
        // @todo security checks for deleting and updating the data
	$updat	=	FALSE;
        if(@$collabdata['id'])
        {
		$updat	=	array('id' => (int)$collabdata['id']);
#            $CampCollab->_set('id', $collabdata['id']);
#            $UpdateQ = $CampCollab->Update();
        }
#		$CampCollab->Save();

            Qube::Start()->Save($CampCollab, $collabdata, $updat);
#deb($CampCollab);
        if(!$is_campaign_collab && $collabdata['sites'])
        {
            $CampCollab->setSiteIds($collabdata['sites']);
        }            
        
        $redir_scope = $is_campaign_collab ? 'campaign' : 'website';
        return $this->returnAjaxRedirect('Notifications_scope' . $redir_scope);
    }
    
    function showCollabEditor($id = NULL)
    {        
        $D = Qube::getDriver();
        
        $collab = $id ? $D->getCampaignCollabWhere('id = %d', $id) : false;
        
        // @session
        $campaign_id = $collab ? $collab->campaign_id : $_SESSION['campaign_id'];
        
        //@campaign _id
        $websites = $D->getWebsitesWhere('cid = %d AND trashed = "0000-00-00"', $campaign_id);
        
        $formdata   = $collab ? $collab : new CampaignCollab;
        $sites      = array();
        
        if($id)
        {
            $Q = $D->queryWebsitesWhere('cid = %d', $campaign_id)
                    ->leftJoin(array('campaign_collab_siteref', 'c'),
                                'c.site_id = T.id AND c.collab_id = %d', $id)
                    ->Fields('T.id, NOT ISNULL(c.site_id) AS is_active');
                    ;
            $results    = $Q->Exec()->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);//, 'Website');
            $sites      = array_map('reset', $results); // some array magic @see pdostatement::fetchAll() doc
        }
        
//        echo $Q;    deb($websites);
        
        $V  = $this->getView('campaigncollab_new');
        $V->init('collab', $formdata);
        $V->init('websites', $websites);
        
        // the array of [id][is_active] => ? which indicates
        // if the given collab entry is active.
        $V->init('website_active', $sites);
                
//        deb($websites);
 

#	deb(1);       
#        deb($collab);
        
        return $V;
                
//        $Websites   = Qube::GetDriver()->getWebsitesWhere('id = ')
    }
    
    function getView($page) {
        $V = parent::getView($page);
        
	//set campaign ID
	if(isset($_GET['CID'])){
		$cID = is_numeric($_GET['CID']) ? $_GET['CID'] : '';
		$_SESSION['campaign_id'] = $cID;
	}
	else{
		$cID = $_SESSION['campaign_id'];
	}
        
        $stmt = Qube::GetPDO()->query('SELECT name FROM campaigns WHERE id = ' . (int)$cID);
        $campaign_name = $stmt->fetchColumn();
        
        $V->init('campaign_name', $campaign_name);
        
        return $V;
    }
    
    function ExecuteAction($action = NULL)
    {
        
        switch($action)
        {
            // @todo tighter security
            case 'deletepermanent':
                Qube::GetPDO()->query('DELETE FROM campaign_collab WHERE id = ' . (int)$_GET['id']);
                $this->Redirect('trash-can.php');
                break;
            case 'restore':
                Qube::GetPDO()->query('UPDATE campaign_collab SET trashed = 0 WHERE id = ' . (int)$_GET['id']);
                $this->Redirect('trash-can.php');
                break;
            
            case 'delete':
                $scope = Qube::GetPDO()->query('SELECT scope FROM campaign_collab WHERE id = ' . (int)$_GET['id']);
                Qube::GetPDO()->query('UPDATE campaign_collab SET trashed = NOW() WHERE id = ' . (int)$_GET['id']);
                return $this->showCollabsByScope($scope->fetchColumn());
                break;
            case 'scopewebsite':
                return $this->showCollabsByScope('custom');
                break;
            
            case 'scopecampaign':
                return $this->showCollabsByScope('campaign');
                break;
            
            case 'save':
                return $this->createCampaignCollab($_POST['campaigncollab']);
                break;
            
            case 'edit':
                return $this->showCollabEditor($_GET['id']);
                break;
            
            case 'settings':
                return $this->showCollabEditor();
        }
        
        throw new QubeException($action);
        return $this->getView('page');
    }
    
}
