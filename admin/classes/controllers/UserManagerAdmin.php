<?php

require_once 'admin.php';


/**
 * User Manager Controler
 *
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */

class UserManagerAdminController extends AdminController
{
    
    function showListPage()
    {
        $users =   array(6610 => 'amado');
            
        $V  =   $this->getView('user-list');
        $V->init('users',     $users);
        
        return $V;
    }
    
    
    function ExecuteAction($action = NULL)
    {   
        switch($action)
        {
            case 'save':
                break;
        }
        
        return $this->showListPage();
    }   
}
