<?php

require_once 'admin.php';	// admin ctonroller

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';

/**
 * campcollab Class
 *
 * process pages for campaign collaboration contacts.
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class SpamAdminController extends AdminController {

    function showAllSpam($id = NULL)
    {        
	$perpage	=	30;
	$pagenum	=	0;
        $Q = Qube::GetPDO();
        $campaign_id = $_SESSION['campaign_id'];
	$stmt	=	$Q->query(sprintf('SELECT * FROM leads WHERE spam >= 50 AND cid = %d ORDER BY id DESC LIMIT %d, %d', $_SESSION['campaign_id'], $perpage*$pagenum, $perpage));
        
        $V  = $this->getView('spam_allleads');
        $V->init('spamleads', $stmt->fetchAll(PDO::FETCH_OBJ));
        
        return $V;
    }

	function showAllContactSpam($id = NULL) {
		$perpage = 30;
		$pagenum = 0;
		$Q = Qube::GetPDO();
		$campaign_id = $_SESSION['campaign_id'];
		$stmt = $Q->query(sprintf('SELECT * FROM contact_form WHERE spamstatus != 0 AND cid = %d ORDER BY id DESC LIMIT %d, %d', $_SESSION['campaign_id'], $perpage * $pagenum, $perpage));

		$V = $this->getView('spam_allcontacts');
		$V->init('spamcontacts', $stmt->fetchAll(PDO::FETCH_OBJ));

		return $V;
	}

	function getView($page) {
		$V = parent::getView($page);

		//set campaign ID
		if (isset($_GET['CID'])) {
			$cID = is_numeric($_GET['CID']) ? $_GET['CID'] : '';
			$_SESSION['campaign_id'] = $cID;
		} else {
			$cID = $_SESSION['campaign_id'];
		}

		$stmt = Qube::GetPDO()->query('SELECT name FROM campaigns WHERE id = ' . (int) $cID);
		$campaign_name = $stmt->fetchColumn();

		$V->init('campaign_name', $campaign_name);

		return $V;
	}

	function ExecuteAction($action = NULL) {
$this->Initialize();
		switch ($action) {

			case 'contactspam':
				return $this->showAllContactSpam();

			case 'deletecontact':
				$stmt = Qube::GetPDO()->query('DELETE FROM contact_form WHERE spamstatus != 0 AND id = ' . (int) $_GET['id'] .
								' AND user_id = ' . (int) $_SESSION['user_id']);

				return $this->showAllContactSpam();

			case 'APPROVEcontact':
				$stmt = Qube::GetPDO()->query('UPDATE contact_form SET spamstatus = 0 WHERE id = ' . (int) $_GET['id'] .
								' AND user_id = ' . (int) $_SESSION['user_id']);
				return $this->showAllContactSpam();

			case 'delete':
				$stmt = Qube::GetPDO()->query('DELETE FROM leads WHERE spam != 0 AND id = ' . (int) $_GET['id'] .
								' AND user_id = ' . (int) $_SESSION['user_id']);

				return $this->showAllSpam();

			case 'APPROVE':
				$stmt = Qube::GetPDO()->query('UPDATE leads SET spam = 0 WHERE id = ' . (int) $_GET['id'] .
								' AND user_id = ' . (int) $_SESSION['user_id']);
				return $this->showAllSpam();

			case 'spam':
				return $this->showAllSpam();
		}

		throw new QubeException($action);
		return $this->getView('page');
	}

}
