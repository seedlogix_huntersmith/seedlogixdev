<?php

require_once 'admin.php';	// admin ctonroller

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once dirname(__FILE__) . '/../../../hybrid/bootstrap.php';
#define('QUBE_NOLAUNCH', 1);
require_once HYBRID_PATH . 'classes/DataAccess/BlogDataAccess.php';
require_once HYBRID_PATH . 'controllers/DashboardBaseController.php';
/**
 * campcollab Class
 *
 * process pages for campaign collaboration contacts.
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class RespondersAdminController extends AdminController {
	/**
	 *
	 * @var ResponderDataAccess
	 */
	protected $da;
	
    function showAllMasterResponders($user_id)
    {
		$MasterResp	=	$this->da->getResellerMasterResponders($user_id);
		$V  = $this->getView('muresponders_masters');
		$V->init('responders', $MasterResp);
		return $V;
    }

	function createMasterResponder($post_data, $user_id) {// create a post
		$ctrl   =   new DashboardBaseController();
		$ctrl->getQube()->setDashboardUserID($user_id);
					
        $ctrl->setDataOwnerID($user_id);		
		
		$MM	=	new ModelManager();
		$post_data['user_id']	=	$user_id;
		
		$masterPost	=	$MM->doSaveResponder($post_data, 0, $this->da, ResponderDataAccess::FromController($this));
		if($masterPost instanceof ValidationStack)
		{
			echo json_encode($masterPost->getErrors());
			throw new Exception();
		}else{
			// no-error. save master status
			$this->da->createMaster($masterPost->getObject());
		}
		return $this->returnFuckedUpAjaxBullshitPage($this->showAllMasterPosts($user_id));
	}
        
	function returnFuckedUpAjaxBullshitPage(ThemeView $p)
	{
		$obj    =   array('success' => array('container' => '#ajax-load',
					'action' => 'replace',
						'message' => $p->toString()
					));
		return $this->getAjaxView($obj);
	}
        
	function editMasterResponder($responder_ID)
	{
		$responder =   $this->da->getMasterResponder($responder_ID);
		$v  =   $this->getView('muresponders_edit');
		$v->init('responder_ID', $responder_ID);
		$v->init('isMaster', true);
		$v->init('responder', $responder);

		
		$reseller_masterforms	=	$this->da->getMasterLeadForms(array('reseller_ID' => $responder->user_id));

		$v->init('masterforms', $reseller_masterforms);
		return $v;  //$this->returnFuckedUpAjaxBullshitPage($v);
	}

	function ExecuteAction($action = NULL) {
		$this->Initialize();
		$this->da	=	new ResponderDataAccess();
		
		switch ($action) {
			case 'editmaster':
				return $this->editMasterResponder($_GET['id']);
                        
			case 'newmaster':
				return $this->createMasterResponder($_POST, $_SESSION['login_uid']);
		}

		return $this->showAllMasterResponders($_SESSION['login_uid']);
	}
}
