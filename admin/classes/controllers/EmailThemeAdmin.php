<?php

require_once 'admin.php';

require_once QUBEPATH . '../models/CampaignCollab.php';
require_once QUBEPATH . '../models/Website.php';
require_once QUBEPATH . '../models/Responder.php';

/**
 * campcollab Class
 *
 * process pages for campaign collaboration contacts.
 * 
 * @author Amado Martinez <amado@projectivemotion.com>
 */

class EmailThemeAdminController extends AdminController
{    
    
    function getThemes()
    {
        return Qube::GetEmailThemes();
    }
    
    function showThemeConfig(Responder $R)
    {
        $ThemeName  =   $R->theme;
        
        $Themes     =   Qube::GetEmailThemes();
        
        if(array_key_exists($ThemeName, $Themes))                
        {
            $ThemeInfo  =   $Themes[$ThemeName];
            $ThemeDefinition =   Qube::GetEmailThemeConfig($ThemeInfo);
        }
        else
        {
#            $ThemeInfo  =   array_shift($Themes);
            $ThemeDefinition  =   array();
            $R->_set('theme', 'None');
        }
        
        $notheme    =   array('path' => 'notheme',
                    'thumbnail' => 'notheme.jpeg',
                    'active' => true);
        
        $Themes     =   array_reverse($Themes, true);
        $Themes['None']     = $notheme;
        $Themes     =   array_reverse($Themes, true);
                
        if(!is_array($ThemeDefinition))
                $ThemeDefinition = array();
        
        if($_SERVER['SERVER_NAME'] == 'pastephp.com')
            $V  =   $this->getView('themeconfig-inline');
        else
            $V  =   $this->getView('themeconfig');
                
        $Pdo    =   Qube::GetPDO();
        $stmt   =   $Pdo->query('SELECT variable, value FROM theme_config WHERE ref_id = '  . (int)$R->id . ' AND ref_table = "auto_responder"');
        $ThemeConfig    =   $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
                
        
        /**
         * The local convention is
         *  ThemeDefinition =    The actual Theme settings (.ini) 
         *  ThemeConfig     =   The values stored in the database for the theme.
         * 
         */
        $V->init('themes', $Themes);
        $V->init('Responder', $R);
        
        $V->init('ThemeDefinition', $ThemeDefinition);
        $V->init('ThemeConfig',     $ThemeConfig);
        
        return $V;
    }
    
    function setThemeOptions($responder_id, $options)
    {
        $D  =   Qube::GetDriver();
        $P  =   $D->getDb()->prepare('REPLACE INTO theme_config (ref_id, ref_table, variable, value) VALUES (?, "auto_responder", ?, ?)');
        $D->getDb()->beginTransaction();
        // ref_id = responder id
        // @todo security
        foreach($options as $varname => $value)
        {
            $P->execute(array($responder_id, $varname, $value));
        }
        $D->getDb()->commit();        
    }
    
    /**
     *
     * @param type $responder_ID
     * @param type $form_data
     * @return ResponseNotification
     */
    function getDummyEmailResponse($responder_ID, $form_data = array())
    {
        $D  =   Qube::GetDriver();
        $Smarty =   new QubeSmarty();
        $Responder  =   $D->getResponderModelWhere('id = ' . $responder_ID);
        $W          =   $D->getWebsiteWhere('id = ' . (int)$Responder->table_id);

        $formdata   =   array_merge($form_data, 
                array('contact_name' => 'john smith',
                    'contact_email' => 'john.smith@smith.com',
                    'contact_phone' => '999 999 9999', 'lead_id' => 0,
                    'contact_comments' => "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. "
                    )
                );
        list($Response, $recipient) =   $Responder->createFormResponse((object)$formdata);
#	deb($Responder);

#        list($Response, $recipient) =   $Responder->CreateResponse(                 'lead_forms' == $Responder->table_name ? 'leads' : 'contact_form', (object)$formdata);
        $vars	=	ResponderModel::loadUserInfo(NULL, $_SESSION['user_id'], $Response);
deb($Response, $_SESSION['user_id']);
        $Responder->ConfigureResponse($Response, (object)array(
                'main_site' => $_SERVER['SERVER_NAME'],
                'responder_phone' => @$W->phone), $Smarty, array(), $vars);
deb($Response);        
        return $Response;
    }
    
    function PrintPreview(ThemedEmail $R)
    {
#        if(! $R instanceof ThemedEmail) return false;
        
        $settings =   $R->getThemeConfig();
        
        $types  =   array();
        $templatevars   =   $R->getTemplateVars();
        foreach($settings as $key => $setting)
        {
            $types[$setting['type']][$key]   =   $setting;
        }
        
        foreach($types['html'] as $key => $setting)
        {
#            echo 'setting-' . $key;
            $R->setThemeVar($key, "<div class=\"inline-edit\" id=\"preview_$key\">{$templatevars[$key]}</div>");
        }
        echo $R->getContent();
#        var_dump();
                ?>

<script type="text/javascript" language="javascript" src="/admin/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    var jQuery = $ = top.jQuery;
    var editors =   {};
    
    function getBackground(jqueryElement) {
    // Is current element's background color set?
    var color = jqueryElement.css("background-color");
    
    if (color !== 'rgba(0, 0, 0, 0)' && color !== 'transparent') {  //chrome && FF
        // if so then return that color
        return color;
    }

    // if not: are you at the body element?
    if (jqueryElement.is("body")) {
        // return known 'false' value
        return false;
    } else {
        // call getBackground with parent item
        return getBackground(jqueryElement.parent());
    }
}

    $('<div style="position:absolute;top:0px;left:0px;"><a href="javascript:void((function (){$(\'#preview\').remove(); $(\'#ajax-load\').fadeIn()})());">Cancel</a> ' +
            ' | <a href="#">Save</a></div>').prependTo(document.body);
    <?php foreach($types['html'] as $name => $var)
    { ?>
        editors['<?php echo $name; ?>'] =   new tinymce.Editor('preview_<?php echo $name; ?>', {
setup : function(ed) {    
    ed.onInit.add(function(ed, evt) {
        ed.hide();
        var mBody   =   ed.getBody();
        var el      =   $('#preview_<?php echo $name; ?>', document);
        var c       =   getBackground(el);
        $(mBody).css('background-color', c);
        $(mBody).css('color', el.css('color')).css('font', el.css('font'));
        
        $(mBody).blur(function (){
            ed.hide();
        });
//        console.log(arguments);
        /*
        tinymce.dom.Event.add(doc, 'blur', function(e) {
            // Do something when the editor window is blured.
            alert('blur!!!');
        }); /**/
    });
},
    theme : "simple",
    mode: "exact",
//    elements : "elm1",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull," + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"   +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
//    height:"350px",
//    width:"600px"
}); 
        editors['<?php echo $name; ?>'].render();
//        editors['<?php echo $name; ?>'].hide();
        <?php
    } ?>
        
    $(document).ready(function ($){
            var timeout =   null;
            var editout = function (id){
//                console.log(this.id, idmatch);
                editors[idmatch[1]].hide();                
            };
            var editover = function (){
                if(timeout) clearTimeout(timeout);
                
                var idmatch   =   this.id.match(/^preview_(.*)/);
                editors[idmatch[1]].show();
                
                setTimeout(function (){
                    editors[idmatch[1]].getBody().focus();
                }, 25);
                
                var ctrid   =   '#' + this.id + '_container';
                var ctr =   $(ctrid, document);
                
                var msover  =   function (){
                    
                    timeout =   setTimeout(function (){
                        editors[idmatch[1]].hide();
                        timeout     =   null;
                    }, 200);
                    
                };
                
            };
            $('.inline-edit', document).click(editover);
        }
    );
</script>
<?php

    }
    function ExecuteAction($action = NULL)
    {        
        
        require_once QUBEPATH . '../classes/QubeSmarty.php';
        require_once QUBEPATH . '../classes/ThemeConfig.php';
        require_once QUBEADMIN . 'classes/ResponseNotification.php';
        require_once QUBEADMIN . 'classes/LeadResponseNotification.php';
        $id =   @$_GET['id'];
        $D  =   Qube::GetDriver();
        
        global $qube;
        /**
         *Note :Session keys
         *  user_id : user id
         */
        
        switch($action)
        {
            case 'test':
                $Response   =   $this->getDummyEmailResponse((int)$_GET['id']);
                $sendTo =   new SendTo(NULL, $_POST['user_email']);
                $Mailer =   $qube->getMailer();
deb(Qube::IS_DEV(), $Mailer, __FILE__);
                $is_sent   =   $Mailer->Send($Response, $sendTo);
#	deb($Response);
                $V  =   new BlankView;
                $V->init('sent', $is_sent);
                return $V;
                break;
                
            case 'preview':
                $Response   =   $this->getDummyEmailResponse((int)$_GET['id']);                
                $this->PrintPreview($Response);
                echo '<!-- end -->';        
                //fff
                exit;
                
                break;
            
            case 'upload':
                $varname    =   $_GET['varname'];
#                deb($_FILES);
                
                $filepath   =   $_SESSION['login_uid'] . '/emails/';    
                $tmpfile    =   $_FILES['theme']['tmp_name'][$varname];
		$filename	=	$_FILES['theme']['name'][$varname];

		if(preg_match('/\.(php[0-9]*|htaccess)$/i', $filename))
			Qube::Fatal('Failed to Upload File', $_FILE, $_POST, $_GET, $_SESSION);

                $destfile   =   QUBEPATH . Qube::STORAGE_PATH . $filepath;
                $num        =   '';
                list($basename, $extension) =   explode('.', $_FILES['theme']['name'][$varname], 2);
                
                #create the dir
                is_dir($destfile)    || mkdir($destfile);
                
                while(file_exists($destfile . $basename . $num . '.' . $extension))
                    $num = (int)$num +  1;
                
                $filename   =   $basename . $num . '.' . $extension;
                $destfile   =   $destfile . $filename;
                
                if(move_uploaded_file($tmpfile, $destfile))
                {
                    $url_src    =   Qube::STORAGE_URL_PATH . $filepath . $filename;
                    $this->setThemeOptions($_GET['id'], array($varname => $url_src));
                    echo '<img src="' . $url_src . '" />';
                }else
                    echo '<b>An Error Ocurred</b>';
                
                exit;
                break;
            
            case 'settheme':
                    $newtheme   =   ($_GET['theme'] == 'None') ? '' : $_GET['theme'];
                
                    
                    $P  =   $D->getDb()->prepare('UPDATE auto_responders SET theme = ? WHERE id = ? AND user_id = ?');
                    $P->execute(array($newtheme, $id, $_SESSION['user_id']));

                    if($newtheme != '')
                    {
                        $Themes     =   Qube::GetEmailThemes();
                        $ThemeInfo  =   $Themes[$_GET['theme']];
                        $ThemeDefinition =   Qube::GetEmailThemeConfig($ThemeInfo);                    
                        $colors =   array();
                        foreach($ThemeDefinition as $varname => $input):
                            if(@$input['type'] != 'color') continue;
                            $colors[$varname]   =   $input['default'];
                        endforeach;

                        $this->setThemeOptions($_GET['id'], $colors);
                    }
                break;
            case 'save':
                    $this->setThemeOptions($_GET['id'], $_POST['theme']);
                    return $this->getAjaxView(array('success' => 1));
                break;
        }
        $Responder  =   Qube::GetDriver()->getResponderWhere('id = ' . (int) $id);
        
        
        return $this->showThemeConfig($Responder);
    }   
}
