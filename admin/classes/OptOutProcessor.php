<?php

/**
 * OptOutProcessor Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

// didnt want to start writing classes just yet, but It will be nice
// to have a set of constants for declaring the opt-out results
// as opposed to using arbitrary integer values
class OptoutProcessor
{
    const OPTOUT_OK = 1;
    const OPTOUT_FAILED = 2;
    const OPTOUT_INVALID = 3;
    const OPTOUT_NONE = 0;

    protected static $result;

    static function getMessage()
    {
        static $message = array(self::OPTOUT_OK => '<b>You were successfully removed from our client\'s mailing list.</b>', 
                                self::OPTOUT_FAILED => '<b>Internal error.</b>  Try the opt-out link in the email again or <a href="mailto:support@6qube.com">email us</a> to be manually removed.  We apologize for the inconvenience.<br />',
                                self::OPTOUT_INVALID => '<b>Internal error.</b>  Try the opt-out link in the email again or <a href="mailto:support@6qube.com">email us</a> to be manually removed.<br />',
                                self::OPTOUT_NONE => '<b>Valid email address is required!</b><br />');

        return $message[self::getResult()];
    }

    static function setResult($result)
    {
        self::$result = $result;
    }

    static function getResult()
    {
        return self::$result;
    }

    static function getIncludeFile($themepath)
    {
        $fullpath = $themepath . 'client-optout.php';
        if(!file_exists($fullpath)) // redirect to 6qube domain.
        {
            Header('Location: http://6qube.com' . $_SERVER['REQUEST_URI']);
            exit;
//                                return QUBEPATH . '../../hubs/themes/6qube/client-optout.php';
        }

        return $fullpath;
    }
}