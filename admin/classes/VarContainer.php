<?php

/**
 * VarContainer Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class VarContainer
{        
    public function __construct($data = NULL, $escape = true)
    {
      if (get_magic_quotes_gpc() && $escape && !is_null($data)) {
	  $in = array(&$data);
	  while (list($k,$v) = each($in)) {
		if(!is_array($v)) continue;
		  foreach ($v as $key => $val) {
			  if (!is_array($val) && is_string($val)) {
				  $in[$k][$key] = stripslashes($val);
				  continue;
			  }
			  $in[] =& $in[$k][$key];
		  }
	  }
	  unset($in);
	}
	
        if($data)
            $this->load ($data);
    }
    
    function __get($var)
    {
        return $this->get($var);
    }

	/**
	 * @param $var
	 * @param callable $func
	 * @return string|mixed
	 * @throws Exception
	 */
    function get($var, $func = NULL)
    {
        if(!isset($this->$var))
                    throw new Exception('Var "' . $var . '" not set. ');
        
        return $func ? $func($this->$var) : $this->$var;
    }
    
    function getValue($var, $default    =   null){
        if(isset($this->$var)) return $this->$var;
        
        return $default;
    }
    /**
     * Check that a value is set.
     * returns true or false.
     * if true, then assigns the value of $this->$var to $set
     * 
     * @param string $var
     * @param type $set
     * @return boolean
     */
    function checkValue($var, &$set){
        if(isset($this->$var)){
            $set    =   $this->$var;
            return true;
        }
        return false;
    }
    
	/**
	 * returns isset($key)
	 * 
	 * @param string $key
	 * @return boolean
	 */
    function exists($key)
    {
      return isset($this->$key);
    }
    
    function __set($var, $val)
    {
        $this->$var = $val;
    }

    function getData()
    {
        return (array)$this;
    }

    function load($array)
    {
        foreach($array as $var => $val)
        {
            $this->$var = $val;
        }
    }
    
    function isEmpty($var){
        return empty($this->$var);
    }
    
    static function getContainer($array_or_obj){
        if($array_or_obj instanceof VarContainer) return $array_or_obj;
		if($array_or_obj instanceof Zend_Console_Getopt)
			return self::getContainer ($array_or_obj->toArray());
		
        $vc =   new VarContainer($array_or_obj, false);
        return $vc;
    }
    
		/**
		 * Check single or multiple keys using a function.
		 * 
		 * @param callable $func
		 * @param mixed $var
		 * @return boolean
		 */
    function check($func, $var){
			
			if(func_num_args() > 2){
				$allargs	=	func_get_args();				array_shift($allargs);
				foreach($allargs as $arg)
				{
					if(!$this->check($func, $arg)) return false;
				}
				return true;
			}
			
        if($this->isEmpty($var)) return false;
        return $func($this->$var);
    }

	/**
	 * @param string $var
	 * @param mixed $value
	 * @param callable $func
	 */
	function set($var, $value, $func = NULL){
		$this->$var = $func !== NULL ? $func($value) : $value;
	}
}

