<?php

require_once 'Notification.php';

/**
 * Description of EmailNotification
 *
 * @author amado
 */

abstract class EmailNotification implements Notification
{
    protected $theme_id     = NULL;
    protected $sender       = NULL;
    
    function getThemeID()
    {
        return $this->theme_id;
    }
    
    function hasTheme()
    {
        return !is_null($this->theme_id);
    }    
    
    
    function setSender(SendTo $sender)
    {
        $this->sender = $sender;
    }
    
    function getSender()
    {
        return $this->sender;
    }
    
    abstract function getContent();
    abstract function getSubject();
    
    function doNotify(Mail $m)
    {
        throw new Exception('Invalid Method for EmailNotification!');
    }
}

class ThemedEmail extends EmailNotification
{
    protected   $_config    = NULL;
    protected   $_smarty    = NULL;
    protected   $_themename =   NULL;
    protected   $_email     =   NULL;
    
    function __construct(QubeSmarty $s, EmailNotification $Email) {
        $this->_smarty  = $s;
        $this->_email   =   $Email;
    }
    
    function setThemeVar($name, $val)
    {
        $this->_smarty->assign($name, $val);
        
    }
    
    function setThemeVars($values)
    {
        $this->_smarty->clearAllAssign()->assign($values);
        return $this;
    }
    
    function getTemplateVars()
    {
        return $this->_smarty->getTemplateVars();
    }
    
    function setThemeName($name)
    {
        $this->_themename   =   $name;
    }
    
    function getThemeConfig()
    {
        $Themes  =   Qube::GetEmailThemes();
        return Qube::GetEmailThemeConfig($Themes[$this->_themename]);
    }
    
    function getContent()
    {
        $themename  = $this->_themename;
        $T = $this->_smarty;
        /*
        $T->error_reporting = E_ALL;
        ini_set('display_errors', true);
        error_reporting(E_ALL);
        
        $T->testInstall();
        */
        
#        $T->testInstall();
        
        $T->assign('email', array('body' => $this->_email->getContent(),
                'subject' => $this->_email->getSubject()));
        
        $Config  =   $this->getThemeConfig();
        $Themes  =   Qube::GetEmailThemes();
        
        $T->assign($Config['CONSTANTS']);
#        deb($C, $Themes[$themename], $Config, $Themes[$themename]['path']);
#        die('error');
        
        $theme_file =   "{$Themes[$themename]['path']}/theme.html";
        
        $C  =  $T->fetch($theme_file); 
        
        return $C;
    }
    
    function getSender() {
        return $this->_email->getSender();
    }
    
    function setSender(SendTo $sender) {
        return $this->_email->setSender($sender);
    }
    
    function getSubject()
    {
        $s = $this->_email->getSubject();
        return $s;
    }
}
