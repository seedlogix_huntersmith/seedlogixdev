<?php


/**
 * Description of LeadFormNotification
 *
 * @author amado
 */
class LeadFormNotification extends EmailNotification
{
    protected $lead = NULL;
    
    /**
     * 
     * @param type $lead_row_object object with form_name, page_name, search_engine, hub_name, keyword, ip, data (html formatted)
     */
    function __construct($lead_row_object) {
        $this->lead = $lead_row_object;
    }
    
    function getContent()
    {
        $msg    =   "<html><body>
            <h1>Information Captured</h1>
            {$this->lead->data}
            <br />
            <p><small><strong>HUB Name</strong>: {$this->lead->hub_name}</small><br />
            ";
            if($this->lead->page_name)
                    $msg    .= '<small><strong>Page Submitted</strong>: ' . $this->lead->page_name . '</small><br />';
            if($this->lead->search_engine)
                    $msg    .= "<small><strong>Search Engine</strong>: {$this->lead->search_engine}</small><br />";
            if($this->lead->keyword)
                    $msg    .= "<small><strong>Keyword Searched</strong>: {$this->lead->keyword}</small><br />";
            $msg    .= "<small><strong>IP Address</strong>: {$this->lead->ip}</strong></small></p></body></html>";
            
            return $msg;
    }
    
    function getSubject() {
        return 'NEW LEAD from ' . $this->lead->form_name;
    }
}