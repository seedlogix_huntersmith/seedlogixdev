<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Notification Interface
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

interface Notification
{
    /**
     * 
     * Allow Notifications to use their own logic
     * for parsing, creating, and sending Mail
     * 
     * @return bool true for success, false for error
     */
    function doNotify(Mail $mailer);
}

class SendTo
{
    protected $name;
    protected $email;
    
    function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }
    
    function __toString()
    {
        if(!$this->name) return $this->email;
        return "\"{$this->name}\" <{$this->email}>";
    }
    
    function getName()
    {
        return $this->name;
    }
    
    function getEmail()
    {
        return $this->email;
    }
}

