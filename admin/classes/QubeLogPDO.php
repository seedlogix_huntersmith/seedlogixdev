<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of QubeLogPDO
 *
 * @author amado
 */
class QubeLogPDO extends \PDO {
    //put your code here
    
    static $totalsqltime    =   0;
    static $last_statement  =   NULL;
    
    protected $is_master    =   false;
    protected $master_pdo   =   NULL;
    protected $qube;
    protected $dsn	=	'';
		protected $isFallBack		=	false;
		
		function isFallback($isfb	=	NULL)
		{
			if(is_null($isfb))	return $this->isFallBack;
			$this->isFallBack	=	$isfb;
		}
		
		function isMaster()
		{
			return $this->is_master;
		}

    /**
     * Given an array, returns a QubeLogPDO Instance
     *
     * @param array $config
     * @param Qube $qube
     * @param bool $is_master
     * @return QubeLogPDO
     */
    static function createInstance(array $config, Qube $qube, $is_master = false)
    {
        return new self($config, $qube, $is_master);
    }

    function __construct($s, Qube $qube, $is_master =   false) {
        
        $this->dsn = 'mysql:dbname=' . $s['dbname'] . ';host=' . $s['dbhost'];        

        parent::__construct($this->dsn, $s['dbusername'], $s['dbpassword']);
        
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        require_once QUBEADMIN . 'classes/QubeLogPDOStatement.php';        
        $this->setAttribute(PDO::ATTR_STATEMENT_CLASS,  array('QubeLogPDOStatement', array($this)));//$s['dblogfile'])));
        
        $this->is_master    =   $is_master;
        $this->qube =   $qube;
    }
    
    
    function query($statement) {
        self::$last_statement   =   $statement;
#        return call_user_func_array('parent::query', func_get_args());  //($statement);
        
        $t  = microtime(TRUE);
        $args   = func_get_args();
#        $call_obj   =   $this->is_master ? 'parent' : ;
        $r  =   call_user_func_array(array($this->get_proper_dbconnection($statement), 'query'), $args);
        $t  = microtime(true) - $t;
        self::$totalsqltime += $t;
        
//        QubeLogPDOStatement::write('/* t = ' . $t . ' */ ' . $statement);
        return $r;
    }
    
    function exec($statement) {
        self::$last_statement       =   $statement;
        $args = func_get_args();
//	QubeLogPDOStatement::write($statement);
        return call_user_func_array(array($this->get_proper_dbconnection($statement), 'exec'), $args);
    }

	/**
	 * @param string $statement
	 * @param array $options
	 * @return QubeLogPDOStatement
	 */
    function prepare($statement, $options =   array()) {
        $args = func_get_args();
        $prepstmt	=	call_user_func_array(array($this->get_proper_dbconnection($statement), 'prepare'), $args);
				
				return $prepstmt;
    }
    
    function get_masterpdo()
    {
        static $master_pdo  =   NULL;
        
        if($this->is_master)
            return 'parent';
        
        if(!$master_pdo)
            $master_pdo = $this->qube->GetDB ('master');
        
        return $master_pdo;
    }
		
		function LogSlaveFailure()
		{
			$q	=	"INSERT INTO 6qube_history (user_ID, object, event, data, object_ID, value, context) VALUES (0, 'SLAVE', 'DOWN', '" .	addslashes($this->dsn) . "', 0, 1, '" . Qube::get_configname() . "')";
			$this->get_masterpdo()->query($q);
			return;
		}
    
    protected function get_proper_dbconnection($query){
        
        if(!preg_match('/^\s*SELECT/i', $query) && !$this->is_master)
            return $this->get_masterpdo();
        
        // if $query is select, use current object.. could be slave could be master pdo.. who cares
        return 'parent';
    }
    
    function lastInsertId($name = null) {
        $args   =   func_get_args();
        return call_user_func_array(array($this->get_masterpdo(), 'lastInsertId'), $args);
    }
    
    function beginTransaction() {
        $args   =   func_get_args();
        return call_user_func_array(array($this->get_masterpdo(), 'beginTransaction'), $args);
    }
    
    function commit() {
        $args   =   func_get_args();
        return call_user_func_array(array($this->get_masterpdo(), 'commit'), $args);
    }
    
    function rollBack() {
        $args   =   func_get_args();
        return call_user_func_array(array($this->get_masterpdo(), 'rollBack'), $args);
    }
#    function __destruct() {
#        QubeLogPDOStatement::write('/* total = ' . self::$totalsqltime . ' */');
#        parent::__destruct();
#    }
}

