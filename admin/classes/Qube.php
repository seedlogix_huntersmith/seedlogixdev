<?php

/**
 * Qube Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

/**
 * QQube (class names must begin with characters only)
 * 
 * Singleton class that integrates the core necessary classes (database,..)
 */


class Qube extends AppCore  // implements hasTables
{
    const PIWIKDB = 'sapphire_analytics';
    const MAIL_ERRORS = false; //'info@sofus.mx';    
    #relative to QUBEPATH
    const EMAIL_THEMES_PATH = '../../emails/themes/';

    #relative to SERVER_NAME
    const EMAIL_THEMES_URL = '/emails/themes/';

    #users storage path
    const STORAGE_PATH = '../../users/';

    #users storage url
    const STORAGE_URL_PATH = '/users/';

    /**
     * Instance.
     * 
     * @var QQube
     */
    protected static $qube;
    protected static $slave_dbh;
    protected static $master_dbh;
    protected $force_master = false;

    /**
     *
     * @var QubeDriver
     */
    protected $qd = NULL;
    public $qube_storage_url = '';
    public $qube_storage_path = QUBEROOT;
    public $qube_home_url = '/';
    protected static $tables   =   array('Responder' => 'auto_responders',
        'ResponderModel' => 'auto_responders',
        'ActiveResponder' => 'auto_responders',
        'NoteModel' => 'notes',
        'AutoResponderTheme' => 'auto_responder_themes', // custom class for parsing themes
        'Lead' => 'contact_form',
        'ContactData' => 'contact_form', // this table is misleadingly named 'contact_form'
        'ContactModel' => 'contacts',
		'AccountsModel' => 'accounts',
		'ProposalModel' => 'proposals',
		'ProductsModel' => 'products',
		'TasksModel' => 'tasks',
		'TextsModel' => 'texts',
		'EmailsModel' => 'emails',
		'PhoneRecordsModel' => 'phone_records',
		'PhoneModel' => 'phone_numbers',
		'EmailSettingsModel' => 'email_settings',
		'TranscriptionsModel' => 'transcriptions',
		'VoicemailModel' => 'voicemails',
        'SocialAccountsModel' => 'social_accounts',
        'SocialPostsModel' => 'social_posts',
        'SocialSchedModel' => 'social_sched',
        'NetworkListingsModel' => 'directory',
        'ScheduledResponse' => 'queued_responses',
        'Reseller' => 'resellers',
        'ResellerModel' => 'resellers',
        'BrandingModel' => 'resellers',
        'User' => 'users',
        'CampaignCollab' => 'campaign_collab',
        'Website' => 'hub',
        'UserModel' => 'users',
        'CampaignModel' => 'campaigns',
        'HubModel' => 'hub',
//                                'WebsiteModel' => 'active_sites',
        'WebsiteModel' => 'hub',
        'LeadModel' => 'active_leads',
        'HubThemeModel' => 'themes',
        'BlogModel' => 'blogs',
        'BlogThemeModel' => 'blogs',
        'BlogPostModel' => 'blog_post',
        'PendingFBBlogPostModel' => 'blog_post',
        'PendingTwitterBlogPostModel' => 'blog_post',
        'PendingLinkedInBlogPostModel' => 'blog_post',
        'SlideModel' => 'slides',
        'LeadFormModel' => 'lead_forms',
        'ResellerNetworkModel' => 'resellers_network',
        'HistoryEventModel' => '6qube_history',
        'ContactDataModel' => 'contact_form',
        'QubeRoleModel' => '6q_roles',
        'QubePermissionModel' => '6q_permissions',
        'UserWithRolesModel' => 'users',
        'UserInfoModel' => 'user_info',
        'ProspectModel' => 'prospects', //6q_prospects_campaign', 
        //'NavModel'  =>  '6q_navs_tbl',
        'NavModel' => 'hub_page2',
        //'PageModel' =>  '6q_hubpages_tbl',
        'PageModel' => 'hub_page2',
        //'PageTreeNodeModel' =>  '6q_website_treenodes',
        'PageTreeNodeModel' => 'hub_page2', 'NetworkModel' => 'resellers_network',
        'LeadFormFieldModel' => '6q_formfields_tbl', 'PathIndexModel' => '6q_pathindex',
        'NetworkListingModel' => '6q_listings',
        'NetworkItemLegacyModel' => '6q_vlistings',
        'NetworkArticleModel' => 'articles',
        'DirectoryArticleModel' => 'directory',
        'PressArticleModel' => 'press_release',
        'BlogFeedModel' => 'blog_feedsmu',
        'SubscriptionModel' => 'subscriptions',
        'RankingSiteModel' => 'ranking_sites',
        'RankingKeywordModel' => 'ranking_keywords',
        'ReportModel' => 'campaign_reports',
        'TouchpointInfoModel' => 'touchpoints_info', 'LeadFormEmailerModel' => 'lead_forms_emailers',
    );

    static function DisconnectDB(){
		self::$slave_dbh	=	NULL;
		self::$master_dbh	=	NULL;
	}            

    static function IS_corporate($site)
    {
        return in_array($site, array('6qube.com', 'staging.6qube.com'));
    }

    public static function dev_dump($object)
    {
        if(self::IS_DEV())
            var_dump($object);
    }

    function squery()
    {
        return $this->GetDB()->query(call_user_func_array('sprintf', func_get_args()));        
    }

	/**
	 * @param $q
	 * @param null $model
	 * @return QubeLogPDOStatement
	 */
	function query($q, $model   =   NULL)
    {
        
        if($model)
        {
#            $callback, $param_arr)$this->db->query(sprintf($q, $this->q->getTable ($model))
            $args   = func_get_args();
#            array_shift($args);
            
            $args[1]    =   $this->getTable($model);
#            var_dump($args);

            return $this->GetDB()->query(call_user_func_array('sprintf', $args));
        }
        return $this->GetDB()->query($q);
    }

    static function get_configname()
    {   
        static $dev_servers = array(
            'corfro' => 'config.cory',
            'gravedigger-noerrors' => 'config.gravedigger-noerrors',
            'gravedigger' => 'config.gravedigger',
            'dev.6qube.com' => 'config.dev.6qube.com',
            //'seedlogix.dev' => 'config.hunter', 
            'seedlogix.dev' => 'config.seedlogix', 
            '6qube3.6qube.com' => 'config.dev.6qube.com',
            'qube.pastephp.com' => 'config.qube.pastephp.com',
            'cory.dev.6qube.com' => 'config.cory.dev.6qube.com',
            'reese.dev.6qube.com' => 'config.reese.6qube.com',
            'hybrid' => 'config.gravedigger',
            'krista' => 'config.krista',
            '6qube1' => 'config.6qube1', 'hulk' => 'config.eric',
            'eric' => 'config.eric',
            'Eric-VAIO' => 'config.eric',
            '6qube.com' => 'config.6qube1',
            '12k' => 'config.12k',
            'gravedigger-test' => 'config.gravedigger-test',
            'router-6qube2' => 'config.router-6qube2', // LIVE ROUTER config (6qube2)
            '6qube3' => 'config.6qube3'
        );

        if(isset($_SERVER['QUBECONFIG']))
	{
		$config_id	=	$_SERVER['QUBECONFIG'];
	}elseif(isset($_ENV['QUBECONFIG'])) {
		$config_id = $_ENV['QUBECONFIG'];
	}elseif(isset($_SERVER['SERVER_NAME']))
	{
		$config_id	=	$_SERVER['SERVER_NAME'];
	}else{
		deb('Using  HTTP_HOST');
		
		if(!isset($_SERVER['HTTP_HOST']))
				throw new QubeException('HTTP_HOST NOT DECLARED.');
		
		$config_id	=	$_SERVER['HTTP_HOST'];		
	}
	switch($config_id)
	{
        case 'eric':
		case '12k':
        case 'cesar':
        case 'sempron':
            return 'config.' . $config_id;
			
		case '6qube3.6qube.com':
			return 'config.6qube3';
		case '6qube1':
		case '6qube1.6qube.com':
			return 'config.6qube1';
		break;
		default:
		    $server_name    =   $config_id;
	}
        
        // end @dev
#echo $server_name;        
       return ($server_name !== false && array_key_exists($server_name, $dev_servers) ? $dev_servers[$server_name] : false);
    }

	static function IS_DEV()
	{
		if(@$_COOKIE['amado'] == md5('amado'))	return true;
//		return false;

		$config	=	self::get_settings();
		return @$config['IS_DEV'];
	}

    static function GetEmailThemes()
    {
        static $themes  =   NULL;
        if(is_null($themes))
            $themes = parse_ini_file(QUBEPATH . self::EMAIL_THEMES_PATH . 'themes.ini', TRUE);
        
        return $themes;
    }
    
    static function GetEmailThemeConfig($theme_info)
    {
        static $vars    =   array();
        $inipath   =   $theme_info['path'] . '/' . $theme_info['presets'];
        if(!isset($vars[$inipath]))
            $vars[$inipath] =   parse_ini_file(QUBEPATH . self::EMAIL_THEMES_PATH . $inipath, TRUE);
        
        return $vars[$inipath];
    }
    
    static function get_settings($setting_name = NULL)
    {
        // @todo security-note, it's not too secure to store database info in a global function/variable. modify this sometime in the future.
        static $settings = NULL;
        
        if($settings) return is_null($setting_name) ? $settings : @$settings[$setting_name];
        
        $settings = array();
        require QUBEPATH . 'config.php';
        
	$configname	=	self::get_configname();
        // overwrite settings based on local server setup
        if($configname)	//self::get_configname())
        {            
            require QUBEPATH . $configname . '.php';
        }
        
        return is_null($setting_name) ? $settings : @$settings[$setting_name];
    }
    
    /**
     *
     * @return NotificationMailer
     */
    function getMailer($force_mock_object = false)
    {
        require_once QUBEPATH . '../classes/Notification.php';
        require_once QUBEPATH . '../classes/NotificationMailer.php';
        
        require_once 'Mail.php';
        
        if( self::IS_DEV() || $force_mock_object)
            $args = array('mock');
        else
            $args = array('smtp', array('host' => 'express-relay.jangosmtp.net',
			'port' => 2525));
        
        return new NotificationMailer(@call_user_func_array('Mail::factory', $args));
    }
    
    /**
     *
     * @return ContactFormProcessor 
     */
    function getContactFormProcessor()
    {
        require_once QUBEPATH . '../classes/ContactFormProcessor.php';
        return new ContactFormProcessor($this);
    }
    
    protected function connect_masterdb($settings)
    {
        if(self::$master_dbh)   return self::$master_dbh;
        
        if(isset($_GET['debug']) || Qube::IS_DEV())
        {
            $this->dolog    =true;
        }       
        
        try{
            $pdo    =   QubeLogPDO::createInstance($settings, $this, true);
            self::$master_dbh   =   $pdo;
        }catch(PDOException $e)
        {
            // failed connection to mater.. this is some serious shit.
            Qube::Fatal('Failed to connect to master: ' . $e->getMessage());
            return FALSE;
        }
        
        return $pdo;
    }
    
    protected function connect_slavedb($settings)
    {
        static $has_slave   =   true;
        
        if(!$has_slave || !isset($settings['dbslaves']))
        {
            $has_slave  =   false;
            return $this->connect_masterdb($settings);
        }

        try{
            $pdo    =   QubeLogPDO::createInstance($settings['dbslaves'][0], $this, false);
        }catch(PDOException $e){
            // connect master if slave connection fails
            $pdo    =   $this->connect_masterdb($settings);
						$pdo->isFallback(TRUE);
//            Qube::LogError('Failed Connection to Slave :' . $e->getMessage(), 'Master Connection:', $pdo);
        }
        
        self::$slave_dbh    =   $pdo;
        
        return $pdo;
    }
    
    function setDashboardUserID($user_ID){
		$this->query('SET @dashboard_userid = ' . (int)$user_ID);
	}

    /**
     * 
     * @param string $type  'master' 'slave' 'piwikdb'
     * @return QubeLogPdo
     */
    function GetDB($type    =   'slave') {

        $s = self::get_settings();

        if($type == 'piwikdb')
        {
            return QubeLogPDO::createInstance($s['piwikdb'], $this, false);
        }

        if($this->force_master || $type    ==  'master'){
            // connect master!
            if(self::$master_dbh)   return self::$master_dbh;
            
            require_once QUBEADMIN . 'classes/QubeLogPDO.php';
            
            return $this->connect_masterdb($s);
        }
        
        // otherwise connect a slave, if slave connection exists. return that. note that this can also be a MASTER connection 
        // if the slave connection failed.
        if(self::$slave_dbh)   return self::$slave_dbh;
        
        require_once QUBEADMIN . 'classes/QubeLogPDO.php';        
        return $this->connect_slavedb($s);
    }
    
    protected function setup()
    {
        $s = self::get_settings();
        if(self::IS_DEV())
        {
            ini_set('display_errors', $s['QUBE_DISPLAY_ERRORS']);
            error_reporting($s['QUBE_ERROR_REPORTING']);            
        }
               
        $this->qube_storage_url =   $s['6qube_storage_url'];
        $this->qube_home_url    =   $s['6qube_home_url'];
        

        // QUBEROOTURLPATH  => URL PATH TO QUBEROOT (may be an alias)
        define('QUBESTORAGEURL', $this->qube_storage_url);

        // QUBESTORAGEPATH  => LOCAL PATH TO STORAGE DIRECTORY
        define('QUBESTORAGEPATH', $this->qube_storage_path);
    }
    
		static function AutoLoader()
		{
			static $loaded	=	false;
			if($loaded)	return;
			
			spl_autoload_register('Qube::ClassLoader');
		}
		
    private function __construct()
    {
//        if(!defined('QUBE_NOLAUNCH'))            
        $this->setup();
				self::AutoLoader();
        set_exception_handler('Qube::Fatal');
//        AppCore::Initialize();
    }
    
    /**
     * get Object for Saving Objects to Database
     * 
		 * @deprecated
     * @return QubeDriver 
     */
    static function GetDriver(Qube $qube = NULL)
    {
        return new QubeDriver($qube ? $qube : self::Start());
    }
		
		function setQubeDriver(QubeDriver $QD)
		{
			$this->qd = $QD;
		}
		
		function getQubeDriver()
		{
			return $this->qd ? $this>qd : new QubeDriver($this);
		}
    
    function getTable($obj) {
        
        return self::$tables[is_string($obj) ? $obj : get_class($obj)];
    }
    
    // this method MUST BE STATIC
    static function GetPDO($slmast	=	'slave')
    {
        return self::Start()->GetDB($slmast);
#        return self::Start()->db;
    }
    
    static function Fatal($exception)
    {
        $url	=	@$_SERVER['HTTP_HOST'] . @$_SERVER['REQUEST_URI'];
        $subject = "6Qube:: Fatal Error ( $url )";
        if($exception instanceof PDOException)
        {
            $subject = 'Mysql Exception: ' . $exception->getCode() . ' / ' . $exception->getMessage();
        }
        $bt = debug_backtrace();
        $data = array_shift($bt);

        $msg    = "\n" . date('Y-m-d H:i:s') . ' File: ' . @$data['file'] . '#' . @$data['line'] . "\n";
        $args   = func_get_args();


        $args[] = QubeLogPDO::$last_statement;

        $args[]	=	'CONFIG:' . self::get_configname();
        $args[]	=	'_SERVER';

        $args[]	= $_SERVER;
        if(session_id()){
            $args[]	=	'_SESSION:';
            $args[]	= $_SESSION;
        }
        $args[] = 	'_GET:';
        $args[]	= $_GET;
        $args[]	=	'_POST';
        $args[]	= $_POST;

        $args[]	=	'BACKTRACE:';
        $args[]	=	$bt;

        $numargs= count($args);

        $data   = array_map('print_r', $args, array_fill(0, $numargs, 1));
        $msg    .= join("\n", $data);
        $m	=	@mail('admin@6qube.com', $subject, $msg);
# DISABLED BY AMADO 11-24-2015
#        $m	=	@mail('system_events@sofus.mx', $subject, $msg);

        if(self::IS_DEV())
        {
            echo '<h3>(IS_DEV=1) Error:(', $m, ')</h3><pre>';
            echo $msg,
            '</pre>';
        }else{

		}
        
        if(@$_SESSION['user_id'] == 6610){
            echo '<pre> Hello Admin. user_id = 6610. Error info:';
            echo $msg;
            echo '</pre>';
    #			print_r($data);
        }
		
		if(@$_SESSION['user_id'] == 48205){
            echo '<pre> Hello Admin. user_id = 48205. Error info:';
            echo $msg;
            echo '</pre>';
    #			print_r($data);
        }

		ErrorPages::InternalServerError();
		exit (10);
//        die('Oops something weird happened, please refresh. /' . $m);
//        return	$m;
    }
    
    static function LogError()
    {
	if(self::MAIL_ERRORS === FALSE) return;
        $bt = debug_backtrace();
        $data = array_shift($bt);

        $msg    = "\n" . date('Y-m-d H:i:s') . ' File: ' . $data['file'] . '#' . $data['line'] . "\n";
        $args   = func_get_args();
        $args[] = QubeLogPDO::$last_statement;

        $args[]	=	'SERVER CONFIG:' . self::get_configname();
	$args[]	= $_SERVER;        
	if(session_id()){
		$args[]	=	'_SESSION:';
		$args[]	= $_SESSION;       
	}
	$args[] = 	'_GET:';
	$args[]	= $_GET;
	$args[]	=	'_POST';
	$args[]	= $_POST;

        $numargs= count($args);
        $data   = array_map('print_r', $args, array_fill(0, $numargs, 1));
        
        $msg    = join("\n", $data);
        
        if(self::IS_DEV() && false)
        {
            echo '<h3>Dev Error:</h3><pre>';
            echo $msg,
            '</pre>';
            return false;
        }
        $url	=	@$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $m	=	mail(self::MAIL_ERRORS, '6Qube:: Error Notification (' . $url . ')', $msg);
               return $m;
    }

static	function getDBConnectionInfo()
	{
		static $slaveACTIVE	=	NULL;
		if(is_null($slaveACTIVE)){
                                                $sysini = @parse_ini_file(QUBEPATH . 'system.ini.php');
                                                if($sysini !== FALSE && isset($sysini['slave_enabled']) && $sysini['slave_enabled']     ==      0)
                                                {
					#		return false;
		$slaveACTIVE	=	false;
                                                }else
		$slaveACTIVE	=	true;
		}
		return $slaveACTIVE;
	}

    /**
     *
     * @return Qube
     */
    static function Start(QubeLogPDO $pdo   =   NULL)
    {
        if(!self::$qube){
            self::$qube = new Qube;
		if(Qube::getDBConnectionInfo() == false)		
						{
							Qube::ForceMaster();
						}
        }
        
        if($pdo){
            self::$master_dbh = $pdo;
            self::ForceMaster();
        }
        
        return self::$qube;
    }
    
    static function ForceMaster()
    {
        self::Start()->force_master =   true;        
    }
    
    static function ClassLoader($className) {
        $f  = AppCore::ClassLoader($className);
//        $f  =   parent::ClassLoader($className);
        if($f   === true) return false;
		if(strpos($className, '\\'))
		{
			if(strpos($className, 'Qube') === 0)
			{
				$subpath	=	str_replace('Qube\\Hybrid\\', "", $className);
	//			list($path, $subpath)	= array(HYBRID_PATH, $subpath);
				$path = HYBRID_PATH;
					$inc	=	$path . 'classes/' . str_replace('\\', '/', $subpath);
				require $inc . '.php';
				return true;
			}
			if(strpos($className, 'Zend') === 0)
			{
				$path	=	str_replace(
						array('Zend', '\\'), 
						array('Zend2', '/'), $className) . '.php';
				if(!defined('HYBRID_LIB'))
					throw new QubeException();

				require HYBRID_LIB . $path;
				return true;
			}
		}
		
        if(preg_match('/Model$/', $className))
        {
            $file   =   QUBEADMIN . 'models/' . $className . '.php';
            require_once $file;
            return true;
        }
        $file   =   QUBEADMIN . 'classes/' . $className . '.php';
        if(file_exists($file))
        {
            require_once $file; 
            return true;
        }
        
        $file   =   HYBRID_PATH . 'classes/' . $className . '.php';
        if(file_exists($file))
        {
            require_once $file; 
            return true;
        }
        
        if(preg_match('/^Zend_/', $className))
        {
            $file   =   HYBRID_LIB . str_replace('_', '/', $className) . '.php';
            require_once $file;
            return true;
        }
				
				if(preg_match('/Table$/', $className) && !preg_match('/^Piwik/', $className))
				{
					require_once HYBRID_PATH . 'classes/tables/' . $className . '.php';
					return true;
				}
				
				if(preg_match('/Report$/', $className) && !preg_match('/^Piwik/', $className))
				{
					require_once HYBRID_PATH . 'classes/reports/' . $className . '.php';
					return true;
				}
				if(preg_match('/DataAccess$/', $className))
				{
					require_once HYBRID_PATH . 'classes/DataAccess/' . $className . '.php';
					return true;
				}
        
        return false;
    }
    /** Not Necessary to close PDO, PHP closes the connection automatically. */
    /*
    function __destruct() {
        $this->GetPDO()->;
    }
     * 
     */
}

function deb()
{
    global $qube;
    if(!QUBE::IS_DEV()) return; // print nothing; allways print
    
    $bt = debug_backtrace();
    $data = array_shift($bt);
    
    echo "\n", 'File: ', $data['file'] . '#' . $data['line'], "\n";
    $args = func_get_args();
    call_user_func_array('var_dump', $args);
}

if(!defined('QUBE_NOLAUNCH'))    // PHPUnit cases and other.
    $qube = Qube::Start();
