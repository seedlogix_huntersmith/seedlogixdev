<?php

/**
 * ThemeResponseNotification Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ThemedResponseNotification extends ResponseNotification
{
    protected $theme = NULL;
    
    function setTheme(AutoResponderTheme $Theme)
    {
        $this->theme = $Theme;
    }
    
    function parseVars($matches)
    {
      if($matches[1] == 'BLOGSTREAM')
      {
	require_once QUBEPATH . '../inc/blogs.class.php';
	
	$stream_blog_id = $this->responder->stream_blog_id;
	if(!$stream_blog_id) return ' no stream blog id';
	
	// have the urge to refactor this.. but I'll wait for the right moment :)
	$blogs = new Blog;
	$blogpost = $blogs->getBlogPost($stream_blog_id, $limit = 5, NULL, NULL, NULL, TRUE);
	
	if(!$blogpost) return 'no blogposts';
	
	$stream = 'posts: ';
	while($post = $blogs->fetchArray($blogpost))
	{
	  $stream .= '<br />' . $post['post_title'];
	}
	
	return $stream;
      }
    }
    
    function __toString() {
        /**
         * @var AutoResponderTheme
         */
        $Theme = $this->theme;
        $Responder = $this->responder;
        
//        deb($Theme);
        if(!$Theme) return $Responder->body;
        
        $themevars = $Theme->getVariables();
        
        $row = $this->responder->toArray();
        $recipient_email = $this->form->getEmail();      // SendTo object (recipient)
        
        $content = $Theme->ReplaceVars($Responder);
        
        
        $main = preg_replace_callback('/\[\[(BLOGSTREAM)\]\]/', array($this, 'parseVars'), $content);        
        
        // return parsed message with anti-spam disclaimers
        
        return $main . 
                            "<br /><br />".$row['contact'] .
                            "<br />".$row['anti_spam'] . 
                            '<br /><a href="http://' . $themevars['main_site'].'/?page=client-optout&email='. $recipient_email.'&pid='.$themevars['prospect_id'].'" target="_blank">' .
                            $row['optout_msg'].'</a>';
    }
    
    function doNotify(Mail $mail)
    {
        throw new QubeException('deprecated method');
        $form       = $this->form;
        $responder  = $this->responder;
        $user       = $this->user;
        
        $to = $form;    // use __toString  
        
        $from = $responder->from_field ? $responder->from_field : $user->username;
        $subject = str_replace("#name", $form->getName(), $responder->subject);
        $body = $this;//stripslashes($this);
        $body = str_replace("#name", $form->getName(), $body);
                                                        ;
//					$headers = "From: ".$row2['from_field']."\r\n";
//					$headers .= "Reply-To: ".$row2['from_field']."\r\n";
//					$headers .= "MIME-Version: 1.0\r\n";
//					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";$
        $sent = $mail->send($to, $headers, $body);
        
        //if mail was sent update user's emails_sent
        return !PEAR::isError($sent);    
    }
}

