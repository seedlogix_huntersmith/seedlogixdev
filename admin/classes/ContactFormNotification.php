<?php

/*
class QubeNotification implements Notification
{
    
}

/**
 * ContactFormNotification Class
 * 
 * Implements templating and any special features for contact forms
 * 
 *
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

class ContactFormNotification extends EmailNotification
{    
    /**
     * the ContactForm object
     * 
     * @var ContactData
     */
    protected $form;
    
    function __construct($form) {
        $this->form = $form;
    }
    // @todo use smarty to parse this kind of templates.
    
    function getInnerContent(){
        $form   =   $this->form;
        return 
        '<h1>Information Captured</h1>
        <p><strong>Name:</strong> '.$form->name.'</p>
        <p><strong>Email:</strong> '.$form->email.'</p>
        <p><strong>Phone:</strong> '.$form->phone.'</p>
        <p><strong>Comments:</strong> '.$form->comments.'</p>
        <p><strong>Website:</strong> '.$form->website.'</p>
        <p><strong>Page:</strong> '.$form->page.'</p>';
    }
		
    function getContent() {
        
        $form = $this->form;
        
        //begin HTML message 
        $message = '<html><body>';
        $message .= $this->getInnerContent();
        $message .= '</body></html>';
        
        return $message;
    }
    
    function getSubject()
    {
        return 'New Contact Form Notification';
    }    
}
