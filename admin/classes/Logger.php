<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logger
 *
 * @author sofus
 */
abstract class Logger {
	/**
	 *
	 * @var Zend\Log\Logger
	 */
	protected $logger	=	NULL;
	protected $_memorystream	=	NULL;
    
	private $mode = 0;
    const DOPRINT      =    1;
    const DOLOG         =   2;
	

	function __construct($MODE  =   Logger::DOLOG) {
		$this->mode	=	$MODE;
			if($this->mode & self::DOPRINT)
				$this->addStdoutWriter ();
			
			if($this->mode & self::DOLOG)
				$this->addMemoryWriter ();
			if($MODE == 0)
				$this->getLogger()->addWriter(new Zend\Log\Writer\Null);
	}
		
	function addStdoutWriter()
	{
		return $this->addFileWriter('php://output');
	}
	
	function addFileWriter($filename)
	{
		$writer = new Zend\Log\Writer\Stream($filename);
		$this->getLogger()->addWriter($writer);
		return $writer;
	}
	
	function addMemoryWriter()
	{
		$this->getLogger()->addWriter(new Zend\Log\Writer\Stream($this->getMemoryStream()));
	}
	
	function getMemoryStream()
	{
		if(!$this->_memorystream)
		{
			$this->_memorystream	=	fopen('php://memory', 'r+');
		}
		return $this->_memorystream;
	}
    
	function setLogger($L)
	{
		if($L instanceOf Zend\Log\Logger)
		{
			$this->logger	=	$L;
			return;
		}
		if($L instanceof Logger)
		{
			$this->logger = $L->getLogger();
			return;
		}
		throw new QubeException('Logger Type Error.');
	}

	/**
	 * 
	 * @return Zend\Log\Logger
	 */
	function getLogger()
	{
		if(!$this->logger){
			$this->setLogger(new Zend\Log\Logger());
		}
		return $this->logger;
	}

	function logINFO()
	{
		$msg	=	call_user_func_array('sprintf', func_get_args());
		return $this->log(Zend\Log\Logger::INFO, $msg);
	}

	function logERROR()
	{
		$msg	=	call_user_func_array('sprintf', func_get_args());
		return $this->log(Zend\Log\Logger::ERR, $msg);
	}
	
	function logDEBUG()
	{
		$msg	=	call_user_func_array('sprintf', func_get_args());
		return $this->log(Zend\Log\Logger::DEBUG, $msg);
	}
	
	function log($priority, $message = "", $extra = array()) {
		if(is_string($priority))
		{
			return $this->logINFO($priority);
		}
//			return $this->logger->log($priority, $message, $extra);
//		$message	=	$this->prepareMessage($message);
		return $this->getLogger()->log($priority, $message, $extra);
	}
		
		function showProgress($func, $timeBegin, $msgformat = "Time Elapsed: %s")
		{
			if(func_num_args() > 3){
				$args = func_get_args();
				array_shift($args);
				array_shift($args);

				$msgformat = call_user_func_array('sprintf', $args);
			}
			$time	=	microtime(true) - $timeBegin;
			$formatted	=	sprintf("%01.2f", $time);
			$this->logDEBUG($func . '::' . $msgformat, $formatted);
			return $time;
		}
}
