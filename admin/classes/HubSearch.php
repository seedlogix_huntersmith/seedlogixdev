<?php

/**
 * Description of HubSearch
 *
 * @author amado
 */
class HubSearch {
    
    /**
     *
     * @var PDO
     */
    protected $pdo;
    /**
     *
     * @var PDOStatement
     */
    protected   $count_statement;
    protected   $search_statement;
    
    protected   $num_results;
    
    protected   $hub_id;
    
    protected   $last_search;
    
    function __construct(PDO $P, $hub_id, $page = 1, $limit = 20)
    {
        $this->pdo  =   $P;
        $this->hub_id   =   $hub_id;
        
        $this->count_statement  =   $this->getStatement('count(*) as num_results');
        $this->search_statement =   $this->getStatement('*', $page, $limit);
    }
    
    
    function getCount()
    {
        return $this->num_results;
    }
    
    
    /**
     *
     * @return PDOStatement
     */
    function getStatement($cols =   '*', $page  =   1, $limit =   20)
    {
        $st     =   $this->pdo->prepare('SELECT ' . $cols . ' FROM `hub_page2` WHERE 
                        trashed = "0000-00-00" AND hub_id = ? AND
                        MATCH(page_keywords, page_title, page_seo_title, page_seo_desc, page_region, page_edit_2, page_edit_3) against (? IN BOOLEAN MODE)
                        LIMIT ' . ((max(1,$page)-1)*$limit) . ', ' . $limit) ;
        return $st;
    }
    
    function getResults($search, $page = 1, $limit = 20)
    {
        $this->last_search  =   $search;
        
        $st =   $this->search_statement;
        try{
        $st->execute(array($this->hub_id, $search));
        }Catch(PDOException $e)
        {
            Qube::Fatal($e, $st, 'page=' . $page, 'limit=' . $limit);
            die('Fatal mysql error.!');            
        }
        
        $this->count_statement->execute(array($this->hub_id, $this->last_search));
        $this->num_results  =   $this->count_statement->fetchColumn();
        
        return $st->fetchAll();
    }
    
    function getNumPages($results_per_page)
    {
        return (ceil($this->getCount()/$results_per_page)+1);
    }
    
    /**
     *
     * Quick and dirty Pagination.
     * 
     * @param type $format
     * @param type $current_page
     * @param type $limit
     * @return type 
     */
    function createPagination($format, $current_page, $limit =   20)
    {
        $links  =   array();
        $numpages   =   $this->getNumPages($limit);
        
#        echo 'numcount', $this->getCount(), 'limit=', $limit, 'nump=', $numpages;
        
        for($i  =   1;  $i  < $numpages; $i++)
        {
            $link    =   sprintf($format, $i, $i);
            if($i   ==  $current_page)
                $links[]    =   '<strong class="active">' . $link . '</strong>';
            else
                $links[]    =   $link;
        }
        
        return join(', ', $links);
    }
}

