<?php

/**
 * ContactResponse Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ContactResponse extends ResponseEmail #ThemedEmailNotification 
{        
    function loadVars($contact_info)
    {
        $this->setPlaceHolders($contact_info);
        return $this;
    }
}

