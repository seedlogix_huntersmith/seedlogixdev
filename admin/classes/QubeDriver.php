<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'SystemDriver.php';

/**
 * QubeDriver Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class QubeDriver extends SystemDriver {
        
    /**
     *
     * @var Qube
     */
    protected $Q;
    
    function __construct(Qube $Q) {
        $this->Q=   $Q;
#        $this->db = $db;
    }
		
		/**
		 * 
		 * @return Qube
		 */
		function getQube(){
			return $this->Q;
		}
    
    function getDb() {
        return $this->Q->GetDB();
#        return $this->db;
    }
    
    function getTableName($obj_or_class)
    {
        // some of this data is redundant, but it's better to have it all
        // in one location than spread around in different models,
        // because php 5.2 does not support inherited static members - some tables have to be declared
        // multiple times for derived models.
        // 
        // @todo improve.
        
	        
        return Qube::Start()->getTable($obj_or_class);
    }
}
