<?php

/**
 * PreparedDbObject Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
abstract class PreparedDbObject extends DbObject
{
    private static $insert_statement;
    
    private static function SetInsertStatement($class, PDOStatement $s)
    {
        self::$insert_statement[$class] = $s;
    }
    
    public static function Prepare($class)
    {
        //self::SetInsertStatement(Qube::GetDriver()->getInsertStatement(new $class));
        self::SetInsertStatement($class, Qube::GetDriver()->getInsertStatement(new $class));
    }
    
    /**
     * @return PDOStatement
     */
    protected function getInsertStatement()
    {
        $class = get_class($this);
        if(!array_key_exists($class, self::$insert_statement))
                throw new Exception('Insert statement does not exist for ' . $class);
        
        return self::$insert_statement[$class];
    }
    
    function _afterInsert()
    {
        $this->id = Qube::GetPDO()->lastInsertId();
    }
    
    /**
     *
     * @param $variables any variables to update using flexible dbwhereexpresion syntax
     * @return DBUpdateQuery 
     */
    function UpdateQuery($variables)
    {
        $Conditions = new DBWhereExpression($this->_getDeleteClause());
        
        $Update = new DBUpdateQuery(Qube::GetPDO());
        $Update->Table(QubeDriver::getTableName($this))
                ->Set($variables)
                ->Where($this->_getDeleteClause());
//        $Q = 'UPDATE ' .  . ' SET ' . $Vars . ' WHERE ' . $Conditions;
        
//        echo $Update;
        return $Update;
    }
    
    function Update()
    {
        $cols = $this->toArray();        
        
        $update = array();
        foreach($cols as $k => $c)
        {
            $update[] = "`$k` = :$k";
        }
        
        $query = sprintf('UPDATE %s SET %s WHERE %s',
                    QubeDriver::getTableName($this),
                    implode(', ', $update),
                    DBWhereExpression::Create($this->_getDeleteClause())
                );
        
        $Q = Qube::GetPDO();
        $stmt = $Q->prepare($query);
        
//        deb($cols, $query, $stmt, $this->_getDeleteClause(),        DBWhereExpression::Create($this->_getDeleteClause()));
        
        ;
        
        return $stmt->execute($cols);
    }
    
    function Save()
    {
        if(!$this->getInsertStatement()->execute($this->_getInsertValues()))                
                throw new Exception();           
        $this->_afterInsert();
    }
}