<?php
/**
 * NotificationMailer Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */

/**
 * Generates Headers and sends Mail
 */
class NotificationMailer extends Logger
{
    /**
     *
     * @var Mail
     */
    protected $mailer;
    protected $error = NULL;
		
    function __construct(Mail $M, Logger $L = NULL)
    {
				if($L){
					$this->setLogger ($L);					
				}
				else {
					parent::__construct(0);
				}
        $this->mailer = $M;
    }
    
    function getError()
    {
        return $this->error;
    }
    
    protected function SendEmail(EmailNotification $n, SendTo $recipient)
    {
        $subject = $n->getSubject();
        $headers = array ('From' => $n->getSender(),
                                   'Reply-To' => $n->getSender(),
                                   'To' => $recipient,
                                   'Subject' => $subject ? $subject : ' no subject ',
                                   'Content-Type' => 'text/html; charset=ISO-8859-1',
                                   'MIME-Version' => '1.0');
        
				$content	=	$n->getContent();
				$time = microtime(true);
        $result = $this->mailer->send($recipient, $headers, $content);
				$this->showProgress(__FUNCTION__, $time);
                
//				return $result;
        if(@Pear::isError($result))
        {
					$this->error = $result->toString();
					$this->logERROR("Failed To Send: %s", $this->error);
					return false;
        }
        
				$this->logDEBUG("Sent Email: %s%s", print_r($headers, true), $content);
        return true;
    }
    
    function Send_Old(Notification $n)
    {
        $result = $n->doNotify($this->mailer);
        if(Pear::isError($result))
        {
            $this->error = $result;
            return false;
        }
        return true;        
    }
    
    function Send(Notification $n, SendTo $recipient = NULL)
    {
        if($n instanceof EmailNotification)
            return $this->SendEmail ($n, $recipient);
        
        $result = $n->doNotify($this->mailer);
        if(@Pear::isError($result))
        {
            $this->error = $result;
            return false;
        }
        return true;
    }
    
		/**
		 * 
		 * @return \Mail
		 */
    function getMailer()
    {
        return $this->mailer;
    }
}
