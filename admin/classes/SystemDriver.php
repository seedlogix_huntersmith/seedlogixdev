<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * SystemDriver Class
 * 
 * php 5.2 compatible.
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
abstract class SystemDriver {
    
    /**
     *
     * @return DbConn
     */
    abstract function getDb();
    
    
    function QueryObject($classname, $alias = 'T')
    {
        // instantiate an object to get the specific select columns and conditions
        /**
         * 
         * @var DbObject
         */
        $obj = new $classname;        
        
        $Q = new DBSelectQuery($this->getDb(), PDO::FETCH_CLASS, $classname);
        
        $Q->From($this->getTableName($classname), $alias)
                ->Fields($obj->_getColumns($alias, DBObject::FOR_SELECT))
                ->Where($obj->_getCondition($alias));
        
#        sleep(5);
        
        return $Q;
    }
    
    function __call($name, $arguments) {
        
        
        if(preg_match('/^(get|prepare|query)(.*?)(s?)Where$/', $name, $m))
        {
            if(!class_exists($m[2])) throw new Exception($m[2] . ' does not exist');
            /**
             * 
             * @return PdoStatement
             */
            $Q = call_user_func_array(array($this->QueryObject($m[2]), 'Where'), $arguments);
            
            if($m[1] == 'query')
                /**
                 * @return DBSelectQuery
                 */
                    return $Q;
            
            if($m[1] == 'prepare')
                /**
                 * @return PdoStatement
                 */
                return $Q->Prepare();
            
            // else.. return an a fetched object/array
            
            $PdoStatement = $Q->Exec();            
            
            // return array
            if($m[3] == 's')
                return $PdoStatement->fetchAll();
            
            // return single
            $object = $PdoStatement->fetch();
            if($object instanceof LiveDbObject)
                return $object->setDriver ($this);
            
            return $object;
        }
        
        throw new Exception('method does not exist (' . $name . ') class: ' . get_class($this));
    }
    
//    abstract function getTableName($obj_or_class);
}
