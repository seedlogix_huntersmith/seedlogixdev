<?php

/**
 * DatabaseObject Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
abstract class DbObject_FACK {
    // constants for column retrieval
    const GET_VALUE     = 0;
    const FOR_INSERT    = 1;
    const FOR_UPDATE    = 2;
    
    /**
     * Class to add identifier to columns in an sql query.
     * 
     * @param type $alias
     * @param type $column single column, or array of column names
     * @return string|array
     */
    
    protected static function addAliasToColumns($alias, $column)
    {
        if($alias == NULL) return $column;
        $ret = array();
        foreach((array)$column as $c)
        {
            $ret[] = '`' . $alias . '`.' . $c;
        }
        return is_array($column) ? $ret : array_pop($ret);
    }
    
    /**
     * Convert a DatabaseObject Model to a different Object Model Class.
     * Useful in join queries where data is merged into one object from different classes.
     * 
     * @param type $newmodel
     * @return newmodel 
     */
    final function convertToModel($newmodel)
    {
        $f = new $newmodel;
        $cols = $f->_getSelectColumns();
        foreach($cols as $c)
        {
            if(!isset($this->$c)) throw new Exception('column ' . $c . ' not existant in object of type ' . get_class($this) . ' for duplication to class ' . $newmodel);
            $f->_set($c, $this->$c);
        }
        
        return $f;
    }
    
    function _set($col, $value)
    {
        $this->$col = $value;
    }
    
    function _getColumn($col, $get_type = 0)
    {
        return $this->{$col};
    }    
    
    /**
     * @return array
     */
    abstract function _getSelectColumns();    
    abstract function _getDeleteClause();
    
    
    function _getInsertColumns()
    {
        return $this->_getSelectColumns();
    }
    
    function _getUpdateColumns()
    {
        return $this->_getSelectColumns();
    }
    
    function _getInsertColumnBinding($column_name)
    {
        return ':' . $column_name;
    }
    
    function _getUpdateColumnBinding($column_name)
    {
        return $this->_getInsertColumnBinding($column_name);
    }
    
    function _getPrepareBindings($get_type = 0)
    {
        $b = array();
        foreach($this->_getInsertColumns() as $column_name)
        {
            if($get_type == self::FOR_INSERT)
            {
                $b[] = $this->_getInsertColumnBinding($column_name);
            }elseif($get_type == self::FOR_UPDATE)
            {
                $b[] = $this->_getUpdateColumnBinding($column_name);
            }else
                   throw new Exception();
        }
        
        return $b;
    }
    
    function _getInsertValues()
    {
        $cols = $this->_getInsertColumns();
        $v = array();
        foreach($cols as $colname)
        {
            try{
                $v[$colname] = $this->_getColumn($colname, self::FOR_INSERT);
            }catch(Exception $e)
            {
                
            }
        }
        
        return $v;
    }
    
    function _Insert(SystemDriver $D)
    {
        throw new Exception();
    }
    
    function _getSelectClause(){
        return array();//new DBWhereExpression();
    }    
    
    /** Magic Methods **/
    function __get($colname)
    {
        return $this->_getColumn($colname);
    }
    
    function __call($name, $arguments) {
        if(preg_match('/^getAliased(.+)$/', $name, $m) && method_exists($this, "_get{$m[1]}"))
        {
            $m = "_get{$m[1]}";
            if(!isset($arguments[0])) throw new Exception('missing arguments');
            
            return self::addAliasToColumns($arguments[0], $this->$m());
        }
        
        throw new Exception('Method ' . $name  . ' does not exist or is invalid!');
    }        
    
    function _getTableName()
    {
        return Qube::GetDriver()->getTableName(get_class($this));
    }
    
    function toArray()
    {
        $columns = $this->_getSelectColumns();
        return get_object_vars($this);
    }
    
    // public utility functions
    
}

/*

abstract class LiveDbObject_depr extends DbObject{
    /**
     *
     * @var SystemDriver
     *
    protected $D;
    
    /**
     *
     * @param SystemDriver $D
     * @return LiveDbObject 
     *
    function setDriver(SystemDriver $D)
    {
        $this->D = $D;
        return $this;
    }
}
 * 
 * 
 */