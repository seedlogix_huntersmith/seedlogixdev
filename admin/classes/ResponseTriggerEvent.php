<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ResponseTriggerEvent Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ResponseTriggerEvent
{
    /**
     *
     * @var User
     */
    public $user;

    /**
     *
     * @var NotificationMaier
     */
    public $mailer;

    /**
     *
     * @var SendTo
     */
    public $form;
    
    /**
     *
     * @var Responder
     */
    protected $responder;
    
    protected $data = array();
    
    public $evt_type;   // contact_form, leads  see responder->table_name

    
    function getData()
    {
        return $this->data;
    }
    
    function setData($data)
    {
        $this->data = $data;
    }
    
    function __construct(User $user, NotificationMailer $mailer, SendTo $form, $type = 'contact_form')
    {
        $this->user = $user;
        $this->mailer = $mailer;
        $this->form = $form;
        $this->evt_type = $type;
    }
    
    function setResponder(Responder $r)
    {
        $this->responder = $r;
    }
    
    function getResponder()
    {
        return $this->responder;
    }
    
    function setRecipient(SendTo $to)
    {
        $this->form = $to;
    }
    
}

