<?php

require_once 'QubeDataProcessor.php';
require_once 'EmailNotification.php';

/**
 * ContactFormProcessor Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class ContactFormProcessor extends QubeDataProcessor
{   
	protected $returnURL;
	/**
	 * @var ProspectDataAccess
	 */
	protected $PDA	=	NULL;
	protected $Mailer	=	NULL;
		
	function setProspectDA(ProspectDataAccess $PDA)
	{
		$this->PDA = $PDA;
	}

	/**
	 * @return ProspectDataAccess
	 */
	function getProspectDA()
	{
		if(!$this->PDA)
		{
			$this->setProspectDA(new ProspectDataAccess());
		}
		return $this->PDA;
	}
	
	function __construct(Qube $q = NULL, $logmode = 0)
	{
		if(Qube::IS_DEV()) $logmode = $logmode | Logger::DOPRINT;
		parent::__construct($q, $logmode);
	}
    
    function getReturnURL()
    {
        return $this->returnURL;
    }
    
    function sendSpamNotification(ContactDataModel $form){
       $notification    =   new SpamNotification($form);
       $notification->setSender(new SendTo('6Qube', 'support@6qube.com'));

	$admins	=	array('6qube@sofus.mx', 'admin@6qube.com');
	foreach($admins as $email){
	       $this->getMailer()->Send($notification, new SendTo(NULL, $email));
	}
    }
		
		/**
		 * 
		 * @param array $vars
		 * @return ContactDataModel
		 */
		function getContactData($vars)
		{
			$ContactForm = new ContactDataModel;
			$ContactForm->loadArray((array)$vars);
			$ContactForm->_set('cid', $this->getProspectDA()->ValidateSubmissionSource(
				$ContactForm->getTableNameFromType(), $vars->type_id, 'cid'
			));
			try{
					$ContactForm->_set('comments', htmlentities($vars->comments, ENT_QUOTES));
			}catch(Exception $exception)
			{
	$ContactForm->_set('comments', '');
	// @error log
			}
			$ContactForm->_set('id', '');
			return $ContactForm;
		}
    
    static function GetUserResellerInfoQuery($user_id)
    {        
        return Qube::Start()->queryObjects('UserModel')->Where('`T`.id = %d',
                        $user_id)
                    ->leftJoin(array('user_class', 'c'),
                        'c.id = `T`.class')
                        ->leftJoin(array('resellers', 'r'),
                                '(T.parent_id =0 AND r.admin_user = T.id)
                            OR r.admin_user = T.parent_id ')
                    ->addFields('c.email_limit,
                            ifnull(r.main_site, "6qube.com") as main_site,
                            ifnull(r.company, "6Qube") as reseller_company');
    }
    
    
    function SaveEvent(VarContainer $vars){
        
        $array  =   'insert into hybrid_eventinput (id, server, class, reseller_id, hub_id, user_id ) values (null, :server,
                    :class , (select parent_id from users where id = :user_id), :hub_id, :user_id)';
        
        $st     =   Qube::GetPDO()->prepare($array);
        $st->execute(array('server' => $_SERVER['QUBECONFIG'], 
                        'class' => get_class($this),
                        'hub_id' => $vars->get('type_id'),
                        'user_id' => $vars->get('user_id') ));
        $inputeventid   =   Qube::GetPDO()->lastInsertId();
        
        $array2 =   'insert delayed into hybrid_eventinputdata (eventid, name, value) values (? , ?, ?)';
        $st = Qube::GetPDO()->prepare($array2);
        foreach($vars->getData() as $key => $value)
        {
            $st->execute(array($inputeventid, $key, $value));
        }
    }

		/**
		 * 1 - Sends Notifications to Users
		 * 2 - Executes Responders
		 * 
		 * @param ContactDataModel $ContactForm
		 * @param type $vars
		 * @return type
		 */
		function ProcessContactForm(ContactDataModel $ContactForm, UserDataAccess $uda, ResponderDataAccess $RDA)
		{
			// send emails 
			if(!$ContactForm->canNotifyUsers()){   // if is spam.. notify me
					$this->sendSpamNotification($ContactForm);
					return;
			}

			$user	=	$uda->getUserWithRoles(array(
					'user_ID' => $ContactForm->user_id,
					'reseller_fields' => 'ifnull(r.main_site, "6qube.com") as main_site,
														          ifnull(r.company, "6Qube") as reseller_company, -1 as email_limit')
			);
			$this->DispatchNotifications($ContactForm, $user);
			// now process all auto_responders and enqueue any responses
			// 
			// 

			// converted subqueries to outer joins for maximum efficiency.
			//  IGNORE INACTIVE and TRASHED responders.
			// had idea to schedule inactive responders in case the responder
			// was activated in near-future, but on second thought - thats kind 
			// of confusing.            

			$Responders = $RDA->getFormInstantResponders(array('R.table_id' => $ContactForm->type_id,
					'R.user_ID'	=> $ContactForm->user_id,
					'R.table_name' => $ContactForm->getTableNameFromType())
			);
			$this->ProcessResponders($ContactForm, $Responders);
		}

	function DispatchNotifications(ContactDataModel $M, UserModel $user)
	{
		// Send Notification to Site Owner
		$Recipient      = new SendTo(NULL, $user->username);
		$Notification	=	$this->getNotification($M, $user);
		if(!$this->getMailer()->Send($Notification, $Recipient))
		{
			// @todo  should enqueue the message or log an error
			mail('error@sofus.mx', 'Failed 6qube contactnotification',
				print_r($Notification, true) .
				'error: ' . print_r($this->getMailer()->getError(), true));
		}

		// send notification to campaign collaborators
		$this->SendNotification($M, $Notification);
	}

	function getNotification(ContactDataModel $ContactForm, UserModel $user)
	{
		//** sends notification using reseller data where necessary.
		$Notification   = $ContactForm->Notify();

		//** SENDER IS RESELLER (OR OWN USER EMAIL IF RESELLER IS NOT FOUND)
		$sender_addr   =   'no-reply@' . $user->main_site;
		if(!$user->main_site)
		{
			// @log
			$sender_addr   =   'no-reply@6qube.com';
			#2012-07-24 some reseller entries have blank main_site value, use 6qube.com
		}
		$Sender         = new SendTo($user->reseller_company, $sender_addr);
		// send from user's reseller
		$Notification->setSender($Sender);

		return $Notification;
	}
		
    function SendNotification(ContactDataModel $Prospect, Notification $notification ){
			require_once QUBEPATH . '../models/CampaignCollab.php';
			$Mailer	=	$this->getMailer();
			$PDA	=	$this->getProspectDA();
			$collaborators  = $PDA->getCampaignEmailRecipients($Prospect->type_id, $Prospect->cid);

			//** END SEND NOTIFICATION TO CLIENT

			if(count($collaborators))
			{
				$PDA->beginTransaction();
				foreach($collaborators as $collab)
				{
					$PDA->SaveEmailInQueue($Prospect->getID(), 'prospect', $collab->id);

//					$notification   = $Prospect->Notify();
//					$notification->setSender($Sender);
					if(!$Mailer->Send($notification, new SendTo($collab->name, $collab->email)))
					{
							Qube::Fatal('Failed 6Qube Notification', $notification, $Mailer->getError());
					}
				}
				$PDA->commit();
			}
    }
    
    function Execute(VarContainer $vars, $checkspam =   true) {
        /**
         * @todo restrict values that are submitted
         *       such as user_id and other ids, its better to declares these values
         *          in index.php or saving it in $_SESSION
         *          so that 'evil' users or hackers are not able to alter this information.
         */


		if(!$vars->checkValue('userID', $skip)){
			$vars->userID   =   0;
		}
		$ContactForm = $this->getContactData($vars);
				
        $PDA		=	$this->getProspectDA();
				
				// Save the form
				if(!$ContactForm->Exists() || Qube::IS_DEV()){
					$this->SaveForm($ContactForm);
					$uda	=	new UserDataAccess();
					$rda	=	new ResponderDataAccess();

					$this->ProcessContactForm($ContactForm, $uda, $rda);
				}

        $returnURL = $PDA->getReturnURL($ContactForm->getTableNameFromType(), $ContactForm->type_id, $vars->userID);
        $this->returnURL = ($returnURL) ? $returnURL : $_POST['domain'];
        return NULL;
    }
    
	function SaveForm(ContactDataModel $ContactForm)
	{
//		$ContactForm->_set('parent_id', $user->getParentID());
		$ContactForm->CheckForSpam();
		$this->getProspectDA()->SaveContactForm($ContactForm);
	}
		
    
    function ProcessResponders(ContactDataModel $form, array $Responders)
    {
		$Mailer	=	$this->getMailer();
        
        require_once QUBEPATH . '../classes/QubeSmarty.php';
//        $user_has_email_limit = $user->hasEmailLimit();
        
        /**
         * For some reason, the user has sent more emails than is allowed.
         * possibly their user class has been modified or downgraded. Who knows.
         *
         */
//        if($user_has_email_limit && $user->email_limit <= $user->emails_sent)
//        {
////            echo 'limit: ' , $user->email_limit, ' sent: ', $user->emails_sent;
//            return;
//        }
        
        // @todo should we process only a limited # of responders? or
        // should we limit on the cron script? mmmm
        
//        $D = $this->getQube()->GetDriver();
//		$t = microtime(true);
//        $Q = $D->queryActiveResponderWhere('`T`.table_name = ? AND `T`.table_id = ? AND T.trashed = "0000-00-00" AND (user_id = ? or ? = 0)')
//                    ->leftJoin(array('v_users_wclass_wreseller', 'rc'), 'rc.id = T.user_id')
//			->addFields('rc.main_site, concat("/users/%s/hub_page/",T.logo) as site_logo');
				
//        if($user_has_email_limit)            $Q->Limit($user->email_limit-$user->emails_sent);
        
//        $Prepared = $Q->Prepare();
//        $Prepared->execute(array($form->getTableNameFromType(), $form->type_id, $form->user_id, $form->user_id));
        
//        $this->showProgress('queryActiveResponders', $t);
				
#        $Prepared->execute(array($table, $type_id, $vars->get('userID'), $vars->get('userID')));
        
//        $Responders = $Prepared->fetchAll();

        // DO NOT increment user's emails_sent counter
        //  until all messages are actually sent.
        //  
        //  only schedule non-instant emails at this point to be processed
        //  by autoresponders_cron
        //  
        // respond or queue responses to the person who filled out the form..
        
        
        $total_sent_notifications = 0;
        $form_config    =   (object)array('contact_name' => $form->name,
                'contact_email' => $form->email,
                'contact_phone' => $form->phone,
                'contact_comments' => $form->comments);
        
        $Smarty =   new QubeSmarty();
        
                
        foreach($Responders as /** @var ResponderModel */ $Responder)
        {            
            $R = new ScheduledResponse();
            $R->_set('contactdata_id', $form->getID());
            $R->_set('responder_id', $Responder->getID());
            $R->setScheduledTime($Responder->sched, $Responder->sched_mode);
            
            if($Responder->sched == 0)
            {
                

            $response_config    =   (object)array('theme' => $Responder->theme,
                    'main_site' => $Responder->main_site,
                    'responder_phone' => $Responder->phone,
                    'site_logo'       => sprintf($Responder->site_logo, UserModel::GetUserSubDirectory($Responder->user_id)),
                );
            list($Response, $sendTo)    =   $Responder->CreateResponse('contact_form', $form_config);
            $Responder->ConfigureResponse($Response,
                        $response_config,
                        $Smarty);
            
#            $sendTo =   new sendTo('test.sofus', 'test.instantresponder@sofus.mx');
            $isSent =   $Mailer->Send($Response, $sendTo);
                if($isSent)
                    $total_sent_notifications++;
                
                
            }
            
            // save the responder
            $this->getQube()->Save($R);
//            $R->Save();
        }
        
        $this->getQube()->query('UPDATE %s  SET emails_sent = emails_sent+%d WHERE id = %d', 'UserModel', $total_sent_notifications, $form->user_id);
//        deb($Mailer->getMailer()->sentMessages);
    }
		
		function setMailer(NotificationMailer $Mailer)
		{
			$Mailer->setLogger($this);
			$this->Mailer	=	$Mailer;
		}
		
		/**
		 * 
		 * @return NotificationMailer
		 */
		function getMailer()
		{
			if(!$this->Mailer)
			{
				$this->setMailer($this->getQube()->getMailer());
			}
			return $this->Mailer;
		}
}
