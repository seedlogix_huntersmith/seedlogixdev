<?php

/**
 * InstantResponseNotification Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * 
 * this class is OBSOLETE. use themedresponsenotification class
 */
class InstantResponseNotification extends ResponseNotification
{
    function doNotify(Mail $mail)
    {
        $form       = $this->form;   // SendTo object (recipient)
        $responder  = $this->responder;
        $user       = $this->user;
        
        $to = $form;
                
        $from = $responder->from_field ? $responder->from_field : $user->username;
        $subject = str_replace("#name", $form->getName(), $responder->subject);
        $body = stripslashes($responder->body);
        $body = str_replace("#name", $form->getName(), $body);
//					$headers = "From: ".$row2['from_field']."\r\n";
//					$headers .= "Reply-To: ".$row2['from_field']."\r\n";
//					$headers .= "MIME-Version: 1.0\r\n";
//					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers = array ('From' => $from,
                                   'Reply-To' => $from,
                                   'To' => $to,
                                   'Subject' => $subject,
                                   'Content-Type' => 'text/html; charset=ISO-8859-1',
                                   'MIME-Version' => '1.0');
        $sent = $mail->send($to, $headers, $body);
        
        //if mail was sent update user's emails_sent
        return !PEAR::isError($sent);
    }
}

