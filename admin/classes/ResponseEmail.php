<?php
/**
 * Created by PhpStorm.
 * User: amado
 * Date: 1/29/2015
 * Time: 6:41 AM
 */

require_once 'EmailNotification.php';


/**
 * A parsable email class.
 * replaces variables in subject, and body.
 *
 */
class ResponseEmail    extends EmailNotification
{
    protected   $_placeholders  =   array();
    protected   $_message       =   'ResponseEmail::_message';
    protected   $_subject       =   'ResponseEmail::_subject';

    function setMessage($html)
    {
        $this->_message = $html;
    }

    function setSubject($subj)
    {
        $this->_subject = $subj;
        return $this;
    }

    function __construct($vars = array())
    {
        $this->setPlaceHolders($vars);
    }

    function    setPlaceHolders($data)
    {
        foreach($data as $key => $value)
        {
            $data[strtolower($key)] = $value;
        }
        $this->_placeholders    =   array_merge($data, $this->_placeholders);
        return $this;
    }

    protected function getMessage()
    {
        return $this->_message;
    }

    function parseVars($matches)
    {
//        deb($matches[1], $matches[2]);
        $var = strtolower($matches[1]);     //empty($matches[1]) ? $matches[2] : $matches[1];



        if(!isset($this->_placeholders[$var])) return '';//00NOTFOUND' . $var . '00';

        if(is_array($this->_placeholders[$var]))
            return $this->_placeholders[$var]['value'];

        return $this->_placeholders[$var];
    }

    function parse_str($str)
    {
        $str    =   str_replace('#name', $this->_placeholders['name'], $str);

        $main = preg_replace_callback('/\[\[([^\]]+)\]\]/', array($this, 'parseVars'), $str);

        return $main;
    }

    function getSubject()
    {
        return $this->parse_str($this->_subject);
    }

    function getContent()
    {
        return $this->parse_str($this->getMessage()) . $this->parse_str(
            "<br /><br />[[responder_contact]]
                            <br />[[responder_anti_spam]]
                            <br /><a href=\"[[site_root]]?page=client-optout&email=[[recipient_email]]&pid=[[prospect_id]]\"
                            target=\"_blank\">[[responder_optout_msg]]</a>"
        );

    }
}
