<?php
require_once 'ResponseNotification.php';
/**
 * LeadResponseNotification Class
 * 
 * This class constitutes a response to a lead form submission. The recipient is the lead. The sender is the site owner.
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class LeadResponse extends ResponseEmail #ThemedEmailNotification 
{    
    function init(ResponseTriggerEvent $T)
    {
        $this->_leadvars = $T->getData();
    }
    
    function loadVars(QubeDriver $D, $lead_id)
    {
        require_once QUBEPATH . '../models/LeadData.php';
        $this->setPlaceHolders(LeadData::getData($D, $lead_id));
        return $this;
    }
        
    function getContent_f() {
        
        $body = parent::__toString();
        $leadInfo = $this->_leadvars;
        
        foreach($leadInfo['fields'] as $key=>$value)
        {
                $searchTag = Hub::convertKeywordUrl($key, 1, '_');
                
                $body = str_replace('[['.$searchTag.']]', $value, $body);
                $subject = str_replace('[['.$searchTag.']]', $value, $subject);
                unset($searchTag);
        }
        
        
        return $body;
    }
}

