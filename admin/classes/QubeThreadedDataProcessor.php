<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QubeThreadedDataProcessor
 *
 * @author sofus
 */
abstract class QubeThreadedDataProcessor extends QubeDataProcessor {

	const THREAD_FAIL_NONFATAL = 100;
	const THREAD_FAIL_FATAL = 1;

	const MAIN_THREAD = 0;
	const THREAD_EXIT = 10;

	private $thread_id	=	0;
	private $thread_count	=	0;
	private $active_threads = 0;
	protected $num_threads	=	10;
	
	/**
	 * Returns false if there was an error with a thread.
	 * 
	 * @return boolean
	 */
	function WaitForThreads()
	{
		$status	=	NULL;
		$this->logINFO("Launched %d threads... waiting..", $this->num_threads);
		while(pcntl_waitpid(0, $status) != -1){
			$exitstatus	=	pcntl_wexitstatus($status);
			
			if($exitstatus !== 0)
			{
				$this->logERROR("A thread exited with an error code. Script execution has been aborted.");
				if($exitstatus === self::THREAD_FAIL_FATAL)
					return false;
			}
		}
		$this->thread_count	=	0;
		$this->active_threads = 0;
		return true;
	}
	
	abstract function RunThreads();
	
	abstract function RunChildThread($data);
	
	abstract function setOptions(\VarContainer $vars);
	
	function Execute(\VarContainer $vars) {
		if(!$vars->isEmpty('numthreads')) 	$this->num_threads	=	$vars->numthreads;
		
		if( FALSE === $this->setOptions($vars) ) 
			return;
		
		if($vars->isEmpty('confirm')){
			$this->logERROR("Execution not confirmed. %s", "\n");
			return;
		}
		
		return $this->RunThreads();
	}
	
	/**
	 * Returns True if Child has executed.
	 * 
	 * returns false if parent thread.
	 * 
	 * @param  $data
	 * @return boolean
	 * @throws QubeException
	 */
	function LaunchThread($data)
	{
		$single_thread_simulate = ($this->num_threads == 1);

		$this->beforeLaunchThreads();

		if($single_thread_simulate){
			$pid = 0;
		}else{
			$pid	=	pcntl_fork();
			Qube::DisconnectDB();
		}

		if($pid	==	0){
			$this->thread_id = $this->thread_count+1;
			
			$formatter	=	new Zend\Log\Formatter\Simple("\t\tThread $this->thread_id - %priorityName%: %message%");
			
			foreach($this->getLogger()->getWriters()->toArray() as $writer)
			{
				$writer->setFormatter($formatter);
			}
			$this->RunChildThread($data);
			$this->logINFO("Returning From Child Thread #%d", $this->thread_id);
			
			if($single_thread_simulate) return false;	// return false, to continue iterations of threads. returning TRUe will exit out of the program.
			return TRUE;
		}elseif($pid	==	-1){
			throw new QubeException('Failed creating threads.');
		}
		$this->thread_count++;
		$this->active_threads++;
		return FALSE;
	}
	
	function getThreadID()
	{
		return $this->thread_id;
	}

	protected function beforeLaunchThreads(){

	}

	public function WaitForThread(){
		$status	=	NULL;
		$this->logINFO("Waiting for any thread..", $this->num_threads);
		if(pcntl_waitpid(0, $status) != -1){
			$exitstatus	=	pcntl_wexitstatus($status);

			if($exitstatus !== 0)
			{
				$this->logERROR("A thread exited with an error code. Script execution has been aborted.");
				if($exitstatus === self::THREAD_FAIL_FATAL)
					return false;
			}
			$this->active_threads--;
		}
		$this->thread_count %= $this->num_threads;
		return true;
	}

	public function getThreadsCount(){
		return $this->thread_count;
	}

	protected function canCreateChild(){
		return $this->active_threads < $this->num_threads;
	}
}
