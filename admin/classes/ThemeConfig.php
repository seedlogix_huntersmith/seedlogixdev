<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ThemeConfig
 *
 * @author amado
 */
class ThemeConfig {
    
    static function Load($ref_id, $ref_table)
    {
        $p  =   Qube::GetPDO();
        $stmt   =   $p->query('SELECT variable, value FROM theme_config WHERE ref_id = ' . (int)$ref_id .
                    ' AND ref_table="' . $ref_table . '"');
        
        $array  =   $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        
        return $array;
    }
}