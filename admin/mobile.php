<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once('./inc/auth.php');
//include classes
require_once('./inc/hub.class.php');
//create new instance of class
$hub = new Hub();
$i = is_numeric($_GET['id']) ? $_GET['id'] : 0;
$_SESSION['current_id'] = $current_id = $i;
$reseller_site = $_GET['reseller_site'];
if($_GET['multi_user'] || $multi_user) $_SESSION['multi_user_hub'] = $multi_user = 1;
else $_SESSION['multi_user_hub'] = $multi_user = NULL;
if(!$reseller_site && !$multi_user && !$_GET['form']){
	//class limits
	$hub_limit = $_SESSION['user_limits']['hub_limit'];
	$landing_limit = $_SESSION['user_limits']['landing_limit'];
	$social_limit = $_SESSION['user_limits']['social_limit'];
	$hub_dupe_limit = $_SESSION['user_limits']['hub_dupe_limit'];
	//type counts
	$hub_count = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 1); //main hubs
	$hub_count += $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 4); //multi-user hubs
	$landing_count = $hub->getCampaignLandings($_SESSION['campaign_id']); //landing pages
	$social_count = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], 1, 5); //social pages
	$dupes_count = $hub->getCampaignHubDupes(NULL, $_SESSION['campaign_id']); //main hub dupes
	//main hubs limit
	if($hub_limit>-1) $hubsRemaining = $hub_limit - $hub_count;
	else $hubsRemaining = "Unlimited";
	//landing pages limit
	if($landing_limit>-1) $landingsRemaining = $landing_limit - $landing_count;
	else $landingsRemaining = "Unlimited";
	//social pages limit
	if($social_limit>-1) $socialRemaining = $social_limit - $social_count;
	else $socialRemaining = "Unlimited";
	//dupes limit
	if($hub_dupe_limit>-1) $dupesRemaining = $hub_dupe_limit - $dupes_count;
	else $dupesRemaining = "Unlimited";
	//fix numbers glitch
	if(is_numeric($hubsRemaining) && $hubsRemaining<0) $hubsRemaining = 0;
	if(is_numeric($landingsRemaining) && $landingsRemaining<0) $landingsRemaining = 0;
	if(is_numeric($socialRemaining) && $socialRemaining<0) $socialRemaining = 0;
	if(is_numeric($dupesRemaining) && $dupesRemaining<0) $dupesRemaining = 0;
}
else if($multi_user && !$_GET['form']){
	$hub_count = $hub->getHubs($_SESSION['user_id'], NULL, NULL, 99999, 1, NULL, NULL, 1);
}
$continue = 1;
//content
if(!$partialRefresh){
	if($reseller_site || ($_SESSION['reseller'] && ($_SESSION['current_id'] == $_SESSION['main_site_id']))){
		$reseller_site = true;
		if(!$_SESSION['main_site']){
			$continue = 0;
			include('reseller-site-setup.php');
		}
		else {
			echo '<h1 style="display:inline;">Reseller - Main Site: </h1>';
			echo '<h2 style="display:inline;">'.$_SESSION['main_site'].'</h2><br /><br />';
			if(!$_GET['form']){
				$anal_id = $hub->ANAL_getAnalyticsId('hub', $_SESSION['current_id']);
?>
		<!--[if !IE]> -->
		<object type="application/x-shockwave-flash" data="http://6qube.com/analytics/libs/open-flash-chart/open-flash-chart.swf?piwik=0.9" width="100%" height="150" style="visibility: visible; " bgcolor="#FFFFFF">
		<!-- <![endif]-->
		<!--[if IE]>
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
		  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
		  width="100%" height="150" bgcolor="#FFFFFF" style="visibility: visible; ">
		  <param name="movie" value="http://6qube.com/analytics/libs/open-flash-chart/open-flash-chart.swf?piwik=0.9" />
		<!-->
		  <param name="allowScriptAccess" value="always">
			<param name="wmode" value="transparent">
			<param name="flashvars" value="data-file=http%3A//6qube.com/analytics/index.php%3Fmodule%3DVisitsSummary%26action%3DgetEvolutionGraph%26columns%5B%5D%3Dnb_visits%26idSite%3D<?=$anal_id?>%26period%3Dday%26date%3Dlast30%26viewDataTable%3DgenerateDataChartEvolution&id=VisitsSummarygetEvolutionGraphChart_swf&loading=Loading...">
		
		  <p>Flash player must be installed to view this content.</p>
		</object>
		<!-- <![endif]-->
        <div id="message_cnt"></div>
		<br /><br style="clear:both;" />
	<?
			} //end if(!$_GET['form'])
		} //end if(!$_SESSION['main_site'])
	} //end if($reseller_site)...
	else {
		echo '<div id="message_cnt"></div><h1>';
		if($multi_user || $_SESSION['multi_user_hub']) echo 'Multi-User Hubs';
		else if($_GET['form']) {
			//get hub name
			$searchID = ($_GET['hub_id'] && is_numeric($_GET['hub_id'])) ? $_GET['hub_id'] : $_SESSION['current_id'];
			$query = "SELECT name FROM hub 
					  WHERE id = '".$searchID."' AND user_id = '".$_SESSION['user_id']."'";
			$hubInfo = $hub->queryFetch($query, NULL, 1);
			echo $hubInfo['name'];
		}
		else echo 'Mobile Websites';
		echo '</h1>';
	}
} //end if(!$partialRefresh)
	
if($continue){
	
	if($_GET['form'] || $reseller_site){
		$form = $_GET['form'] ? $_GET['form'] : 'settings';
		$i = is_numeric($_GET['id']) ? $_GET['id'] : 0;
		$_SESSION['current_id'] = $current_id = $i;
		
		//Multi-User hub stuff
		if($form=='theme' && $_GET['themeID'] && ($_SESSION['reseller_client'] || $_SESSION['reseller'])){
			//if user is a reseller or reseller client who has just changed the theme on their hub
			//check to see if the theme they chose is a multi-user one
			if($hub->isMultiUserTheme($_GET['themeID'])){
				//if it is update the hub's fields with the theme's presets
				$hub->setupMultiUserHub($_SESSION['current_id'], $_GET['themeID'], $_SESSION['user_id']);
			}
		}
		//Check hub page version
		include_once('./inc/forms/add_hub_'.$form.'.php');
	}
	else {
		if($multi_user){
			$result = $hub->getHubs($_SESSION['login_uid'], NULL, NULL, 99999, NULL, NULL, NULL, 1);
		}
		else 
			$result = $hub->getHubs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id'], NULL, 3, true); //data
		if(!$partialRefresh){
			echo '<div id="createDir">';
			if(!$_SESSION['theme']){
				echo '<div class="watch-video"><ul><li><a href="#campVideo" class="watch_video trainingvideo" >Watch Video</a></li></ul></div>';
			}
			echo '<form method="post" action="add-mobile-site.php" class="ajax"><input type="text" name="name" ';
			if((is_numeric($hubsRemaining) && $hubsRemaining==0) && 
				(is_numeric($landingsRemaining) && $landingsRemaining==0) && 
				(is_numeric($socialRemaining) && $socialRemaining==0)) 
				echo 'readonly';
			echo ' />';
			if($multi_user) echo '<input type="hidden" name="multi_user" value="1" />';
			echo '<input type="submit" value="Create Mobile Website" name="submit" style="font-size:15px;" /></form></div><div class="clear"></div>';
			if(!$_SESSION['theme']){
				echo '<div style="display:none;"><div id="campVideo" style="width:640px;height:480px;overflow:auto;"><a href="http://6qube.com/videos/training/hubs-overview.flv" style="display:block;width:640px;height:480px"id="player"> </a><script>flowplayer("player", "http://6qube.com/videos/flowplayer/flowplayer-3.2.2.swf");</script></div></div>';
			}
		}
		
		if($hub_count || $landing_count || $social_count){
			echo '<br /><div id="dash"><div id="fullpage_mobile" class="dash-container"><div class="container">';
			while($row = $hub->fetchArray($result)){ //if so loop through and display links to each Hub
				$tracking = $row['tracking_id'];
				$cid = $row['cid'];
				if(!$tracking || $tracking==0) $tracking = 26;
				if($_SESSION['reseller'] || $_SESSION['reseller_client']) $rid = "&rid=".$_SESSION['parent_id'];
				$hubName = $row['name'];
				//$reqThemeTypesArray = $hub->parseReqThemeType($row['req_theme_type']);
				$themeType = $hub->getThemeType($row['id']);
				
				echo '<div class="item">
						<div class="icons">
							<a href="http://'.$_SESSION['main_site'].'/hub-preview.php?id='.$row['id'].'&uid='.$row['user_id'].$rid.'" target="_new" class="view" title="View"><span>View</span></a>
							<a href="hub.php?form=settings&id='.$row['id'];
							if($multi_user) echo '&multi_user=1';
							echo '" class="edit" title="Edit"><span>Edit</span></a>
							<a href="#" class="delete rmParent" rel="table=hub&id='.$row['id'].'&undoLink=mobile" title="Delete"><span>Delete</span></a>';
							echo '<a href="mobile.php" class="duplicateHub" title="Duplicate" rel="'.$row['id'].'" style="padding:0px;"><img src="img/duplicate_icon.gif" alt="Duplicate" border="0" /></a>';
					echo '</div>';
					echo '<img src="'.$hub->ANAL_getGraphImg('hub',$row['id'],$tracking,$_SESSION['user_id']).'" class="graph trigger" />';
					echo '<a href="hub.php?form=settings&id='.$row['id'];
					if($multi_user) echo '&multi_user=1';
					echo '">'.$hubName.'</a>';
				echo '</div>';
				$hubName = $showDupeButton = $themeType = $reqThemeTypesArray = NULL; //clear vars for next loop iteration
			} //end while loop
			if(!$multi_user){
				echo '<br />You have:<br />
					<strong>'.($landingsRemaining).'</strong> Mobile Websites remaining.<br />';
					if(is_numeric($hubsRemaining))
					echo '<small>Remaining amounts include items in the <a href="trash-can.php" class="dflt-link">trash</a></small>';
				if(!$_SESSION['theme']){
					if(($hub_limit-$hub_count)<=0 && $hub_limit!=-1 && $hubsRemaining!="Unlimited") echo '  <a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else {
				echo '<br />Use the form above to create a website that your users can copy and edit only certain parts of.';
			}
			echo '</div></div></div>';
		}
		else { //if no hubs
			echo '<div id="dash"><div id="fullpage_mobile" class="dash-container"><div class="container">';
			if(!$hubsRemaining && !$multi_user){
				echo 'You have <strong>0</strong> Websites remaining.';
				if(!$_SESSION['theme']){
					echo '<a href="https://6qube.com/billing/cart.php?gid=2" class="external">Upgrade your account</a> to access this feature.';
				}
			}
			else if($multi_user){
				echo 'Use the form above to create a website that your users can copy and edit only certain parts of.';
			}
			else echo 'Use the form above to create a new Mobile Website.  You have:<br />
					<strong>'.($landingsRemaining).'</strong> Mobile Websites remaining.<br />';
			echo '</div></div></div>';
		}
	}
}//end if($continue)
?>
<? if(!$_SESSION['theme'] && !$partialRefresh){ ?>
<script type="text/javascript" src="http://6qube.com/admin/js/flowplayer-3.2.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".trainingvideo").fancybox({
		'titlePosition'	: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'	: 'none'
	});
});
</script>
<? } ?>