<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

//Get user info
$id = $_GET['id'];
$id = is_numeric($id) ? $id : '';
//if user is a 6Qube admin creating a profile for a reseller
if($id2 = $_GET['id2']){
	$id2 = is_numeric($id2) ? $id2 : '';
	$secID = $id2;
}
else $secID = $id;

$rid = $reseller->getParentID($id);
if(!$rid) $rid = 0;

$sixqube_user = $_GET['6qube'] ? true : false;

//Verify permission
//since this file is called on the 6qube.com domain from the reseller's domain
//we use an md5 with a salt only we know to verify
$salt = '4a0fBMMDDR(Cc,,c.bh07890';
if($secID && ($_GET['sec']==md5($secID.$salt))){
	if($_GET['action']=="create"){
		$query = "SELECT firstname, lastname, phone, company, address, address2, city, state, zip 
				FROM user_info WHERE user_id = '".$id."'";
		$user = $reseller->queryFetch($query);
		$firstname = $user['firstname'];
		$lastname = $user['lastname'];
		$company = $user['company'];
		$address = $user['address'].' '.$user['address2'];
		$city = $user['city'];
		$state = $user['state'];
		$zip = $user['zip'];
		$phone = $user['phone'];
		$action = 'addNewBillingProfile';
		if($_GET['upgradeClass']){
			$upgradeClass = $_GET['upgradeClass'];
		}
	}
	else if($_GET['action']=="update"){
		require_once(QUBEADMIN . 'inc/Authnet.class.php');
		$anet = new Authnet();
		$profileInfo = $anet->getPaymentProfile($id, $apiLogin, $apiAuth);
		$firstname = $profileInfo['info']['firstName'];
		$lastname = $profileInfo['info']['lastName'];
		$company = $profileInfo['info']['company'];
		$address = $profileInfo['info']['address'];
		$city = $profileInfo['info']['city'];
		$state =$profileInfo['info']['state'];
		$zip = $profileInfo['info']['zip'];
		$phone = $profileInfo['info']['phone'];
		$action = 'updateBillingProfile';
	}
	
	if($sixqube_user) $submitForm = '6qube-functions.php';
	else $submitForm = 'reseller-functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Billing Profile</title>
<style type="text/css">
body {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 14px;
}
</style>
</head>
<body>
<div style="text-align:left;position:absolute;left:300px;">
	<h2>Verify Billing Information</h2>
	<form action="https://6qube.com/admin/<?=$submitForm?>" method="post">
		<label>First Name</label><br />
		<input type="text" name="firstname" value="<?=$firstname?>" /><br />
		<label>Last Name</label><br />
		<input type="text" name="lastname" value="<?=$lastname?>" /><br />
		<label>Company</label><br />
		<input type="text" name="company" value="<?=$company?>" /><br />
		<label>Address</label><br />
		<input type="text" name="address" value="<?=$address?>" /><br />
		<label>City</label><br />
		<input type="text" name="city" value="<?=$city?>" /><br />
		<label>State</label><br />
		<input type="text" name="state" value="<?=$state?>" maxlength="2" size="3" /><br />
		<label>Zip Code</label><br />
		<input type="text" name="zip" value="<?=$zip?>" maxlength="10" size="6" /><br />
		<label>Phone</label><br />
		<input type="text" name="phone" value="<?=$reseller->stripNonAlphaNum($phone)?>" /><br /><br />
		
		<h2>Payment Information</h2>
		<label>Card Number <small>(numbers only)</small></label><br />
		<input type="text" name="cardnumber" value="" maxlength="16" autocomplete="off" /><br />
		<label>Card Expiration Date</label><br />
		<select name="expMonth">
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select> 
		<select name="expYear">
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
			<option value="2014">2014</option>
			<option value="2015">2015</option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020">2020</option>
		</select><br />
		<label>CVV Code <small>(3-4 digit code on back)</small></label><br />
		<input type="text" name="cvv" value="" size="4" maxlength="4" autocomplete="off" />
		<input type="hidden" name="uid" value="<?=$id?>" />
		<input type="hidden" name="rid" value="<?=$rid?>" />
		<input type="hidden" name="sec" value="<?=md5($id.$salt)?>" />
		<input type="hidden" name="action" value="<?=$action?>" />
		<? if($upgradeClass){ ?>
		<input type="hidden" name="upgradeClass" value="<?=$upgradeClass?>" />
		<? } ?>
		<br /><br />
		<input type="submit" value="Submit" name="submit" style="font-weight:bold;font-size:18px;width:200px;" />
	</form>
</div>
</body>
</html>
<? } ?>