<?php
//start session
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
//authorize
require_once(QUBEADMIN . 'inc/auth.php');
if($_GET['mode']=='view'){
  $id = is_numeric($_GET['id']) ? $_GET['id'] : '';
  include_once(QUBEADMIN . 'inc/forms/view-rankings.php');
}
else {
  //include classes
  require_once(QUBEADMIN . 'inc/rankings.class.php');
  $rankings = new Rankings();
  //get rankings
  $viewlist = $rankings->getRankings($_SESSION['user_id'], $_SESSION['campaign_id'], NULL);

?>
<div id="message_cnt" style="display:none;"></div>

<h1>Search Engine Rankings</h1>
<h2>Add New Website</h2>

<!-- error message container -->
<div id="formErrorMessageContainer" style="display:none"></div>

<!--add website form container -->
<div id="createDir">
	<form method="post" action="inc/forms/add-ranking.php" class="ajax">
		<input type="text" name="name" value="" placeholder="Website Name" />
		<br class="clear" style="margin:20px;" />
		<input type="text" name="site" value="" placeholder="Website URL" />
		<input type="submit" value="Add to Rankings" name="submit" />
	</form>
</div>
<br />
<h2>Current Websites</h2>
<div id="message_cnt" style="display:none;"></div>
<div id="new_ranking" title="New Ranking" ><div class="newRanking"></div></div>
<? if($viewlist && is_array($viewlist)){
  echo '<div class="contactContain">';
  foreach($viewlist as $id=>$view){
    echo '
    <ul class="prospects rankings" title="'.$id.'">
    <li class="name" style="width:25%;"><a href="rankings.php?mode=view&id='.$id.'">'.$view['name'].'</a></li>
    <li class="email" style="width:22%;"><i>'.$view['site'].'</i></li>
    <li class="email" style="width:20%;"><i>'.$view['keywordupdate'].'</i></li>
    </ul>';
  }
  echo '</div>';
} else { ?>
Use the form above to create a new contact.

<? } ?>

<? } ?>
<br style="clear:both;" />