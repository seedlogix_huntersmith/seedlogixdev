<?php

	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

        if(1 || $_SESSION['user_id'] == 6610)
        {
            require_once '6qube/core.php';
            require_once QUBEPATH . '../classes/VarContainer.php';
            require_once QUBEPATH . '../classes/QubeDataProcessor.php';
            require_once QUBEPATH . '../classes/ContactFormProcessor.php';
            require_once QUBEPATH . '../models/Response.php';
            require_once QUBEPATH . '../classes/Notification.php';
            require_once QUBEPATH . '../classes/NotificationMailer.php';
            require_once QUBEPATH . '../models/Responder.php';
            require_once QUBEPATH . '../models/AutoResponderTheme.php';
            require_once QUBEPATH . '../classes/ResponseTriggerEvent.php';
            
            $responder_id = $_POST['emailid'];  // @todo emailid? rename variable.
            $theme_id = $_POST['id'];   // @todo 
            
            $stmt = Qube::GetPDO()->prepare('UPDATE ' . QubeDriver::getTableName('Responder') . ' SET theme = ? WHERE id = ?');
            $success = $stmt->execute(array($theme_id, $responder_id));
            
            $return = array('error' => !$success, 'html' => '');
            // do nothing. 
            echo json_encode($return);
            exit;
        }
        
	require_once QUBEADMIN . 'inc/db_connector.php';
	$db = new DbConnector();

	$type = $_POST['type'];
	$id = $_POST['id'];
	$emailid = $_POST['emailid'];
	$tableid = $_POST['tableid'];
	$tablename = $_POST['tablename'];
	
	$query = mysqli_query("UPDATE auto_responders SET theme = '".$id."' WHERE id = '".$emailid."'") 
				or die(mysqli_error());  
	
	if($type=="autoresponder") $table = "auto_responder_themes";
	
	$query2 = "SELECT * FROM {$table} WHERE id = {$id}";
	$result = $db->queryFetch($query2);
	
	$query3 = "SELECT bg_color, links_color, accent_color, titles_color, call_action, table_id, table_name, user_id, edit_region1, edit_region2, logo FROM auto_responders WHERE id = {$emailid}";
	$row = $db->queryFetch($query3);
	
	$query4 = "SELECT logo, phone FROM {$tablename} WHERE id = {$tableid}";
	$row2 = $db->queryFetch($query4);
	
	if($row2['logo']) $logo = 'https://'.$_SESSION['main_site'].'/users/'.UserModel::GetUserSubDirectory($row['user_id']).'/'.$row['table_name'].'/'.$row2['logo'];
	else $logo = 'http://'.$_SESSION['main_site'].'/users/'.  UserModel::GetUserSubDirectory($row['user_id']).'/hub_page/'.$row['logo'];
	
	$imgLink = 'http://'.$_SESSION['main_site'].'/';
	
	if($result && $result['html']){
		$html = $result['html'];
		$tags = array('[[BGCOLOR]]','[[LINKSCOLOR]]','[[ACCENTCOLOR]]','[[TITLESCOLOR]]','[[CALLTOACTION]]','[[LOGO]]','[[TABLEID]]','[[TABLENAME]]','[[USER]]','[[PHONE]]','[[EDIT1]]','[[EDIT2]]','[[IMGLINK]]'); 
		$fields = array($row['bg_color'],$row['links_color'],$row['accent_color'],$row['titles_color'],$row['call_action'],$logo,$row['table_id'],$row['table_name'],$row['user_id'],$row2['phone'],$row['edit_region1'],$row['edit_region2'],$imgLink);
		$return['error'] = 0;
		$newhtml = htmlentities(str_replace($tags, $fields, $html));
		$htmlup = str_replace($tags, $fields, $html);
		$query5 = mysqli_query("UPDATE auto_responders SET body = '".$htmlup."' WHERE id = '".$emailid."'") 
				or die(mysqli_error()); 
		$return['html'] = $newhtml;		

	}
	else {
		$return['error'] = 1;
		$return['message'] = "Couldn't find template";
	}
	
	echo json_encode($return);
?>