<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/hub.class.php');

//create new instance of class
$connector = new Hub();
$offset = $_GET['offset'] ? $_GET['offset'] : 0;
if($_SESSION['reseller']) $reseller_site = $_SESSION['main_site_id'];
?>


<h1>Directory Listing Prospects</h1>
<p>Click a person's email address to contact them using our Prospect Emailer.</p><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'direc', NULL, 5, $offset, 'directory-prospects.php', $reseller_site);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'direc', NULL, 5, $offset, 'directory-prospects.php', NULL, $reseller_site);
}
?>
<br />
<br style="clear:both;" />
<h1>Article Prospects</h1>
<p>Click a person's email address to contact them using our Prospect Emailer.</p><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'artic', NULL, 5, $offset, 'articles-prospects.php', $reseller_site);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'artic', NULL, 5, $offset, 'articles-prospects.php', NULL, $reseller_site);
}
?>
<br />
<br style="clear:both;" />
<h1>Press Prospects</h1>
<p>Click a person's email address to contact them using our Prospect Emailer.</p><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'press', NULL, 5, $offset, 'press-prospects.php', $reseller_site);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'press', NULL, 5, $offset, 'press-prospects.php', NULL, $reseller_site);
}
?>
<br />
<br style="clear:both;" />