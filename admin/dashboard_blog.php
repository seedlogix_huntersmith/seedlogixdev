<?php
//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//require db connection
	require_once('inc/blogs.class.php');
	$connector = new Blog();
	
	//get page settings
	$settings = $connector->getSettings();
	
	//set page variables 
	$display_count = $settings['display_count'];
	$blog_count = 0;
	//$blog_limit = $connector->getLimits($_SESSION['user_id'], "blog");
	$blog_limit = $_SESSION['user_limits']['blog_limit'];
	if($blog_limit==-1 || $blog_limit=="") $blog_limit = "Unlimited";
	
	//blog
	$result = $connector->getBlogs($_SESSION['user_id'], NULL, NULL, $_SESSION['campaign_id']);

	if($blog_rows = $connector->getBlogRows()){
		$blog_count = $blog_rows;
		while($row = $connector->fetchArray($result)){
			if($row['id'] != $past){
				$blog_title[] = $connector->blogDashUpdate($row['id'], $row['blog_title'], $_SESSION['user_id'], $row['tracking_id'], 'block');
				$past = $row['id'];
			}
		}			
	}
?>

	<div class="headline">
		<h2>Blogs:</h2>
		<!--<p>You currently have <span class="bold"><?=$blog_count?></span> Blogs out of <span class="bold"><?=$blog_limit?></span> available.</p>-->
		<a href="blog.php"><p>New Blog</p></a>
	</div>
	<?
		if($blog_count){
			if($blog_title){
				echo '<div class="container">';
				foreach($blog_title as $key => $value){
					echo $value;
				}
				echo '</div>';
			}
		}
		else {
			echo '<div class="container">None.</div>';
		}
	?>