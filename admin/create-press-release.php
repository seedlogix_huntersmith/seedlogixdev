<?php
/*
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	//authorize
	require_once('./inc/auth.php');
	//include classes
	require_once('./inc/db_connector.php');
	require_once('./inc/validator.php');
	require_once('./inc/press.class.php');
	//create new instance of class
	$connector = new DbConnector();
	$validate = new Validator();
	$press = new Press();
	//header
	require_once('./admin/inc/header.php');
	
	if(isset($_POST['headline'])){
		$press->updatePress($_POST);	
	}
			
	include('./inc/forms/add-press-release.php');
	
	//footer
	require_once('./admin/inc/footer.php');
*/

	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once('inc/auth.php');
	
	//iniate the page class
	require_once('inc/press.class.php');
	$press = new Press();
	
	
	if(isset($_POST['headline'])){
		$message = $press->updatePress($_POST);
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#message-container';
		$response['success']['action'] = 'replace';
		$response['success']['message'] = $message;	
	}

	echo json_encode($response);
?>