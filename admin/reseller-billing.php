<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');

//include classes
require_once(QUBEADMIN . 'inc/reseller.class.php');
$reseller = new Reseller();

$offset = $_GET['offset'] ? $_GET['offset'] : 0;
if($offset) $offset = is_numeric($offset) ? $offset : 0;

//Page viewing types:
//1 - User/Reseller viewing their own history
//2 - Reseller/Admin viewing a user's history
//3 - Reseller viewing all their users' histories
//4 - Admin viewing entire billing log

if(!$specView)
	$view = $_GET['view'] ? $_GET['view'] : '1';
else 
	$view = $specView;
if($view==1 || $view==2){
	$uid = $_GET['id'] ? $_GET['id'] : $_SESSION['user_id'];
	//make sure user has permission
	if($uid==$_SESSION['user_id']) $continue = true;
	else if($_SESSION['admin'] || ($_SESSION['reseller'] && $reseller->getParentID($uid)==$_SESSION['login_uid'])) $continue = 1;
}
if($view==3){
	if($_SESSION['reseller']){
		$rid = $_SESSION['login_uid'];
		$continue = true;
	}
	else if($_SESSION['admin']){
		$rid = $_GET['id'];
		$continue = true;
	}
}
if($view==4){
	if($_SESSION['admin']){
		$all = 1;
		$continue = true;
	}
}

if($continue){
	//get company name
	if($uid || $rid){
		$query = "SELECT company FROM user_info WHERE user_id = '";
		if($uid) $query .= $uid."'";
		else $query .= $rid."'";
		$userInfo = $reseller->queryFetch($query);
	}
?>
<div style="width:100%;">
<? if($_SESSION['super_user']){ ?>

<? } else { ?>
	<? if(!$all){ ?>
        <h1>Billing History for <?=$userInfo['company']?></h1>
        <? if($uid){ ?>
        History of items we've attempted to bill you for.
        <? } else { ?>
        History of items we've charged your users for on your behalf.
        <? } ?>
    <? } else { ?>
        <h1>System Billing Log</h1>
    <? } ?>
    <br /><br />
    
    <? $reseller->displayBillingHistory(50, $offset, $uid, $rid, $all, $view); ?>
<? } ?>
</div>
<br style="clear:both;" />
<? } ?>