<?php
//start session
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/hub.class.php');

//create new instance of class
$connector = new Hub();
$offset = $_GET['offset'] ? $_GET['offset'] : 0;
if($_SESSION['reseller']) $reseller_site = $_SESSION['main_site_id'];
?>

<h1>Contact Form Prospects</h1>
<p>Click a person's email address to contact them using our Prospect Emailer.</p><br />
<?
if($_GET['reseller_site']){
	$connector->displayProspects($_SESSION['user_id'], 'hub', NULL, 40, $offset, 'contact-form-prospects.php', $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
} else {
	$connector->displayProspects($_SESSION['user_id'], 'hub', NULL, 40, $offset, 'contact-form-prospects.php', NULL, $reseller_site, NULL, NULL, $_SESSION['campaign_id']);
}
?>
<br />
<br style="clear:both;" />