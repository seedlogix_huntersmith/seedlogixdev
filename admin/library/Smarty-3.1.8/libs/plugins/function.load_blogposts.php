<?php


/**
 *
 *  accepts blog_id and limit
 * assigns array to blogposts
 * 
 * @param type $params
 * @param type $smarty
 * @return array 
 */
function smarty_function_load_blogposts($params, $smarty)
{
    require_once QUBEPATH . '../inc/blogs.class.php';

    $stream_blog_id = $params['blog_id'];
    $limit          =   $params['limit'];
    
    $posts  =   array();
    
    if($stream_blog_id){
	// have the urge to refactor this.. but I'll wait for the right moment :)
	$blogs = new Blog;
	$blogpost = $blogs->getBlogPost($stream_blog_id, $limit, NULL, NULL, NULL, TRUE);
	
	if(!$blogpost) return $posts;
	
	$stream = 'posts: ';
	while($post = $blogs->fetchArray($blogpost))
	{
	  $posts[] =   $post;
	}
    }
    
    $smarty->assign('blogposts', $posts);
}
