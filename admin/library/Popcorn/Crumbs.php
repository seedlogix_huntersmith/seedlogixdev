<?php

/**
 * Crumbs Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class Crumbs {
    
    protected $paths = array();
    
    //put your code here
    function addPath($url, $text)
    {
//        var_dump($url);
        
        $this->paths[] = func_get_args();
        return $this;
    }
    
    /**
     *
     * @param string $format printf format ("<a href="%s">%s</a>")
     * @param type $separator 
     */
    function _print($format, $separator)
    {
        $links = array();
        foreach($this->paths as $p)
        {
            $links[] = sprintf($format, $p[0], $p[1]);
        }
        
        echo implode($separator, $links);
    }
}
