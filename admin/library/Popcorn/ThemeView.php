<?php

/**
 * ThemeView Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
require_once 'Template.php';
require_once 'BaseView.interface.php';

/**
 * This class Executes (Displays) a Template Object using the $page parameter 
 * 
 * @author Amado Martinez
 */
class ThemeView implements BaseView
{
    /**
     *
     * @var Template
     */
    protected $Template;
    /**
     *
     * @var BaseController
     */
    protected $Controller;
    protected $displayargs = array();
    protected $_view_path   =   '';
    
    function __construct(BaseController $Controller, Template $T, $page, $showheader = true, $showfooter = true)
    {
        require_once 'Crumbs.php';
        
        $this->Controller = $Controller;
        $this->Template = $T;
        
        $T->crumbs = new Crumbs;      
        $T->init('page_title', 'Page Title');
        $this->_path    =   $this->Template->getPath();
        $this->init('_path', $this->_path);
        $this->displayargs = array($page, $showheader, $showfooter);
        $this->Template->func('action_url', array($this->Controller, 'ActionUrl'));
    }
    
    function addCrumbs($action, $text, $args = array())
    {
        $url = $this->Template->action_url($action, $args);
//        var_dump($url);
        $this->Template->crumbs->addPath($url, htmlentities($text));
        return $this;
    }
    
    function init($var, $val)
    {
        $this->Template->init($var, $val);
        return $this;
    }
    
    function Exists()
    {
        return file_exists($this->Template->getPath() . '/' . $this->displayargs[0]);
    }

    function setPage($newpage)
    {
        $this->page = $newpage;
        return $this;
    }
    
    /**
     * 
     * @return Template
     */
    function getTemplate()
    {
        return $this->Template;
    }
    
    function setArgs()
    {
        $this->displayargs = func_get_args();
    }
    
    function display()
    {
        call_user_func_array(array($this->Template, 'display'), $this->displayargs);//display($this->page, true, true);
    }
    
    function toString(){
        ob_start();
        $this->display();
        $return = ob_get_clean();
//        if(ob_get_length()) ob_end_clean();
        
        return $return;
    }
}

