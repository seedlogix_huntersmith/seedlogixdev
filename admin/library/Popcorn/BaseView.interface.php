<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This interface is used to display information, (ajax or template, etc.)
 * 
 * It's used by controllers to display any type of object which can be 'displayed.' (ajax, html, even images.)
 * 
 */
interface BaseView
{
    function display();
    function init($var, $val);
}

class BlankView implements BaseView
{
    function display()
    {
        // do nothing
    }
    
    function init($var, $val)
    {
        $this->$var = $val;
    }
}

class HttpRedirectView implements BaseView
{    
    protected $url  =   '';
    function __construct($url) {
        $this->url  =   $url;
    }
    
    function init($var, $val)
    {
        $this->$var =   $val;
    }
    
    function display()
    {
        Header('Location: ' . $this->url);        
    }
}

class PrintStringView implements BaseView{
	function display()
	{
		echo $this->var;
	}

	function init($var, $val)
	{
		$this->$var = $val;
	}

}

class CSVExportView implements BaseView{
	/** @var  ExportableTable */
	private $exportable;
	private $args = array();

	public function __construct(ExportableTable $exportable){
		$this->exportable = $exportable;
	}

	function display()
	{
		header('Content-type: text/csv');
		header("Content-Disposition: attachment;filename=file.csv");
		$handle = fopen('php://output', 'w');
		$this->exportable->Export($handle, $this->args);
	}

	function init($var, $val)
	{
		$this->args[$var] = $val;
	}

}