<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
define('APP_PATH', realpath(dirname(__FILE__) . '/../') . '/');

if(!defined('APP_CONTROLLERS'))
    define('APP_CONTROLLERS', APP_PATH . 'controllers/');

interface queriesObjects {
    function queryObjects($modelorclass, $alias = 'T', $where = NULL);
}

abstract class AppCore implements hasTables, queriesObjects
{
    
    /**
     *
     * @var PDO
     */
    protected static $dbh    =   NULL;
    protected static $tables    =   array();
    protected static $insert    =   array();
    protected static $update    =   array();
    
    static function isDev()
    {
        return FALSE;   // @todo
    }
    /**
     * 
     * @return PDO
     */
    function GetDB($type =   'slave')
    {
        if(!self::$dbh)
        {
            $config =   array();
            $server =   isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'dashboard';
            require_once APP_PATH . 'include/' . $server . '.config.php';

            $dsn = 'mysql:dbname=' . $config['dbname']  .   ';host=' . $config['dbhost'];

            try {
                self::$dbh = new PDO($dsn, $config['dbuser'], $config['dbpass']);
            } catch (PDOException $e) {
                die('Connection failed: ' . $e->getMessage());
            }
        }
        return self::$dbh;
    }

	/**
	 * @param $classobj
	 * @param string $alias
	 * @param null $andwhere
	 * @return DBSelectQuery
	 */
    function queryObjects($classobj, $alias='T', $andwhere = NULL)
    {
        static $models;
        $classname  = is_object($classobj) ? get_class($classobj) : $classobj;
        $dbtable = $this->getTable($classname);       
        $Q = new DBSelectQuery($this->GetDB(), PDO::FETCH_CLASS, $classname);
        $Q->hasTables($this);
        
        if(!isset($models[$classname]))
            $models[$classname] =   new $classname;
        
        $obj = $models[$classname];
        
        $Q->From($dbtable, $alias)
                ->Fields($obj->_getColumns($alias, DBObject::FOR_SELECT))
                ->Where($obj->_getCondition($alias));
        if($andwhere){
            $params= func_get_args();   array_shift($params);array_shift($params);
            call_user_func_array(array($Q, 'Where'), $params);
        }
//        echo $Q;
        return $Q;
    }
    
    abstract function getTable($obj); /*
    {
        return self::$tables[is_string($obj) ? $obj : get_class($obj)];
    }*/
    
    /**
     * if update, return boolean.
     * if insert, return false or $object
     * 
     * @param DBObject $object
     * @param type $data
     * @param DBWhereExpression $update
     * @return boolean
     * @throws Exception
     */
    function Save(DBObject &$object, $data  =   NULL, $update   =   FALSE)
    {
        $DB =   $this->GetDB();
        $class  =   get_class($object);
        
        if($data){
            $object->loadArray($data);
//        }        
            $data_keys  =   array_keys($data);
        }else{
            $data_keys  =   $object->__getColumns();
        }
        
        if($update)
        {
            $table  =   $this->getTable($class);
            $columns =   $object->_getColumns('T', DBObject::FOR_UPDATE, $data_keys);

            if(empty($columns)) throw new Exception('Invalid columns set for object for save. Please Update the object columns or check that $data contains valid keys.');
            
            $Where   =   DBWhereExpression::Create($object->_getCondition('T', DBObject::FOR_UPDATE));
            
#            var_dump($object->_getCondition('myballs', DBObject::FOR_UPDATE), $Where, $class, "TABLE $table");exit;
            if(is_array($update) || $update instanceof DBWhereExpression    || is_string($update))
            {
                if(is_string($update))
                {
                    // use call_user_func for sprintf effects
                    $params = func_get_args();
                    array_shift($params);
                    array_shift($params);
                    array_unshift($params, ' AND ');
                    call_user_func_array(array($Where, 'appendWhere'), $params);
                }else
                    $Where->appendWhere(' AND ', $update);
            }
            
            $q  =   "UPDATE $table T SET "   . join(',', $columns) .
                        ' WHERE ' . $Where;
            $statement  =   $DB->prepare($q);
            
            return $statement->execute($object->getValues($data_keys, DBObject::FOR_UPDATE));
        }
        
        if(!isset(self::$insert[$class]))
        {
            $table  =   $this->getTable($class);
            $columns =   $object->_getColumns(FALSE, DBObject::FOR_INSERT, $data_keys);

            $q  =   "INSERT INTO $table set "   .   join(',', $columns); /* . " VALUES(" .
                        join(', ', $object->getPlaceholders($data_keys)) . ') ';    
             */
            
            $statement  =   $DB->prepare($q);
        }else
            $statement  =   self::$insert[$class];
//        try{
			
			if($statement->execute($object->getValues($data_keys, DBObject::FOR_INSERT)))
                    return $object->_afterInsert($DB);
			
//		}catch( \Exception $e )
//		{
//			throw $e;
//		}
		
        return false;
    }
    
    static function ClassLoader($className)
    {
        if(preg_match('/.+Controller$/', $className) && file_exists(APP_CONTROLLERS . $className . '.php'))
        {
                include APP_CONTROLLERS . $className . '.php';        
                return true;
        }
            else {
                $filename   =   APP_PATH . 'include/' . $className . '.php';
                if(file_exists($filename))
                {
                    include $filename;
                    return true;
                }
            }
            return false;
    }
    
    function Initialize()
    {
        throw new Exception('deprecated');  // @todo deprecate
        spl_autoload_register(array($this, 'ClassLoader'));
    }
}
