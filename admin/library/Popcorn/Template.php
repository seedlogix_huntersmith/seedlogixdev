<?php

/**
 * Template Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 * @license GPL
 * 
 * This class is free software. You may use or distribute this code under GPL License terms.
 *
 * @method string action_url(string $ControllerAction, array $params = array()) Generate a link string using the following format "Controller_action"
 * @method void getWidget(string $template, array $params = array())
 * @method void getDialog(String $template, array $params = array())
 * @method void getControl(String $template, array $params = array())
 */
class Template {

    private $template_base;
    protected $parts;
    protected $_scripts;
    protected $scripts = array(); // raw js
    protected $_vars = array();
    protected $funcs = array();
    protected $_css;

    function getVars() {
        return $this->_vars;
    }

    function getJSON() {
        return json_encode($this->getVars());
    }

    function __construct($template_base, $header = 'header.php', $footer = 'footer.php') {
        $this->_scripts = array();
        $this->_css = array();
        $this->template_base = $template_base;

        $this->parts['header'] = $header;
        $this->parts['footer'] = $footer;
    }

    function getPath() {
        return $this->template_base;
    }

    function display($_include_file, $header = false, $footer = false) {
        if (isset($this->_vars))
            extract($this->_vars);
        
        ob_start();
        include($this->template_base . '/' . $_include_file);
        $body = ob_get_clean();
//                @ob_end_clean();                

        if ($this->parts['header'] && $header)
            include($this->template_base . '/' . $this->parts['header']);

        echo $body;

        if ($this->parts['footer'] && $footer)
            include($this->template_base . '/' . $this->parts['footer']);
    }

    function start_script() {
        ob_start();
    }

    function end_script() {
        $this->scripts[] = ob_get_clean();
    }

    function __call($name, $arguments) {
        if (array_key_exists($name, $this->funcs)) {
            return call_user_func_array($this->funcs[$name], $arguments);
        }
        if (preg_match('/^get([A-Za-z]+)$/', $name, $matches)) {
            $p = $this->template_base . '/' . strtolower($matches[1]) . 's/' . $arguments[0];
            if (isset($this->_vars))
                extract($this->_vars);
            if (count($arguments) > 1 && is_array($arguments[1]))
                extract($arguments[1]);
            return include($p);
        }
    }

    /**
     * Stores one or multiple values to be used in the template
     *
     * @param array|string $array the array of key => value pairs, or the variable name
     * @param string $value the variable value if $array is the variable name.
     */
    function init($array, $value = null) {
        if (isset($value)) {
            $this->_vars[$array] = $value;
        } else {
            $this->_vars = (array) $array + (isset($this->_vars) ? $this->_vars : null);
        }
    }

    function func($name, $callback) {
        $this->funcs[$name] = $callback;
        return $this;
    }

    function sortLink($key, $title, $page) {
        $link = '%s';
        $direction = '&asc=1';
        $sort = @$_GET['sort'];
        if ($sort == $key && isset($_GET['asc']))
            $direction = '';

        $classname = array();
        if (@$sort == $key) {
            $classname[] = 'sorted';
            if ($direction)
                $classname[] = 'asc';
            else
                $classname[] = 'desc';
        }

        printf("<a href=\"$page?sort=$key$direction&p=%d%s%s\"%s>%s</a>", max(1, (int) @$_GET['p']), (isset($_GET['tags']) ? '&tags=' . urlencode($_GET['tags']) : ''), (@$_GET['tag'] ? '&tag=' . (int) $_GET['tag'] : ''), (!empty($classname) ? ' class="' . implode(' ', $classname) . '"' : ''), $title);
    }

    /**
     * safely escapes and prints a string
     *
     * @param string $strPrint
     */
    function p($strPrint) {
        print(htmlentities($strPrint, ENT_QUOTES));
    }

    function date($time, $fmt = 'Y-M-D') {
        $this->p(date($fmt, $time));
    }

    function checked($t, $v = false) {
        $b = is_bool($t) ? $t : $t == $v;
        if ($b)
            echo ' checked';
    }

    function selected($t, $v = false) {
        $b = is_bool($t) ? $t : $t == $v;
        if ($b)
            echo ' selected';
    }

    /**
     * converts a relative path to absolute
     *
     * @param string $file
     * @return string
     */
    function url($file) {
        $url = $this->_path . '/' . $file;
//		if(IS_LOCAL){   
        /*
          $urlp = explode('?', $url);
          $v = parse_str(@$urlp[1]); */
//		}
        return $url;
    }

    function addScript($src, $part = 'head') {
        $this->_scripts[$part][] = $src;
    }

    function printScripts($part = 'head') {
        foreach ($this->_scripts[$part] as $src) {
            echo '<script type="text/javascript" src="' . $src . '"></script>' . "\n";
        }
    }

    function __get($name) {
        return $this->_vars[$name];
    }

    function __set($name, $value) {
        $this->_vars[$name] = $value;
    }

}
