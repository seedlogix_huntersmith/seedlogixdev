<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

/**
 * Description of DBObject
 *
 * @author amado
 */
abstract class DBObject {
    const FOR_INSERT    =   1;
    const FOR_UPDATE    =   2;
    CONST FOR_SELECT    =   3;
    const FOR_OUTPUT    =   4;
    const FOR_DELETE    =   5;
    const FOR_REFRESH   =   6;
    
    protected $_PR_KEY  =   'ID';
    protected $_defaults   =   array();
		protected $_setfields	=	array();
    //put your code here
    
    function _afterInsert(PDO $p)
    {
        if($this->_PR_KEY)
					$this->set($this->_PR_KEY, $p->lastInsertId ());
        
        return $this;
    }
    
    /**
     * @deprecated since version number
     */
    // backward compatible (deprecated)
    function _getSelectColumns(){
        return $this->__getColumns();
    }
    
    /**
     * @deprecated since version number
     */
    static function Prepare(){
        
    }
    
    /**
     * Convert a DatabaseObject Model to a different Object Model Class.
     * Useful in join queries where data is merged into one object from different classes.
     * 
     * @deprecated since version number
     * @param type $newmodel
     * @return newmodel 
     */
    final function convertToModel($newmodel)
    {
        $f = new $newmodel;
        $cols = $f->_getSelectColumns();
        foreach($cols as $c)
        {
            if(!isset($this->$c)) throw new Exception('column ' . $c . ' not existant in object of type ' . get_class($this) . ' for duplication to class ' . $newmodel);
            $f->_set($c, $this->$c);
        }
        
        return $f;
    }
    // end
    
    function loadArray($array)
    {
        foreach($this->__getColumns() as $c)
        {
            isset($array[$c]) && $this->set($c, $array[$c]);
        }
    }
    
    function set($c,$n)
    {
        $this->_set($c, $n);
    }
    
    function _set($c, $n)
    {
        $this->$c   =   $n;
				$this->_setfields[$c]	=	1;
    }
    
    function meta($c)
    {
        return $this->get($c);
    }
    function get($c, $purpose  =  self::FOR_OUTPUT)
    {
        return $this->$c;
    }
   
    /**
     * Return a list of raw column names. Not prefixed or escaped.
     * 
     * @return array
     */
    function __getColumns()
    {
        return (array)$this;
    }
    
    function _getCondition($alias, $purpose =   self::FOR_SELECT)
    {
        if(!is_null($this->_PR_KEY)
                &&                ($purpose == self::FOR_DELETE ||                 $purpose ==  self::FOR_UPDATE
                    || $purpose == self::FOR_REFRESH)
                && isset($this->{$this->_PR_KEY}))
        {
            return array($this->_PR_KEY => $this->get($this->_PR_KEY));
        }
        return '';
    }
    /**
     * return various column data.
     * 
     * a. Given a list of possible columns, intersect the provided columns
     * with the model columns andd return only valid model columns.
     * 
     * b. given an 'alas' value, return an array of 'prefixed' column. ex: T.`col1`, T.`col2`
     * 
     * c. if purpose is FOR_UPDATE, return an array of: T.`colname` = :colname strings
     *      which are useful for preparing statements
     * 
     * @param type $alas
     * @param type $purpose
     * @param type $columns
     * @return string
     */
    function _getColumns($alas  =   NULL, $purpose   =   self::FOR_INSERT, &$columns =   NULL)
    {
        $cols   =   $this->__getColumns ($purpose);
        
        if($columns)
        {
						$keysset	=	array_keys($this->_setfields + ($purpose == self::FOR_INSERT ? $this->_defaults : array()));
            $cols   =  array_intersect($keysset, array_intersect($columns, $cols));
            $columns    =   $cols;
        }
        
        if($alas === NULL
                ) return $cols;
        
        foreach($cols as $k => $col)
        {
            $cols[$k]   =   ($alas ? $alas . '.' : '') .  "`$col`";
            if($purpose == self::FOR_UPDATE || $purpose == self::FOR_INSERT)
                $cols[$k]   .=  ' = ' . $this->_getColumnPlaceholder ($col, $purpose);  //self::FOR_UPDATE);
        }
        return $cols;
    }
    
    function getValue($c, $p, &$array)
    {
        if(($p   ==  self::FOR_INSERT OR $p	==	self::FOR_UPDATE) && array_key_exists($c, $this->_defaults))
        {
            return NULL;
        }
//            else{
            // sanitize $c (:col-name is known placeholder bug)
            $ck  =   str_replace("-", "_", $c);
//            if($c == 'bg-img'){ var_dump($ck, $this->get($c), $this); exit; }
            $array[$ck]  =   $this->get($c, $p);
//        }
    }
    
    function getValues($columns =   NULL, $purpose   =   self::FOR_OUTPUT)
    {
        if(is_null($columns)){
		$columns	=	$this->_getColumns (NULL, $purpose, $columns);
#		deb($s);
	}
        if($purpose == self::FOR_OUTPUT)
        {
            die('not supported yet');
        }
        $v  =   array();
        foreach($columns as $c)
        {
            $this->getValue($c, $purpose, $v);            
        }
        return $v;
    }
    
    function _getColumnPlaceholder($c, $purpose =   self::FOR_UPDATE)
    {
        return array_key_exists($c, $this->_defaults) ? $this->_defaults[$c] : ':' . str_replace("-", "_", $c);        
    }
    
    function getPlaceholders($columns   =   array())
    {
        $p  =   array();
        foreach($columns as $c)
        {
            $p[$c]  = $this->_getColumnPlaceholder($c);
        }
        return $p;
    }
    
    function _getTableName()
    {
	return	Qube::Start()->getTable($this);
        return AppCore::getTable($this);
    }

    
    /**
     * php 5.3 required for get_called_class
     * 
     * @param type $andwhere
     * @return DBSelectQuery
     */
    static function Query($andwhere =   NULL){
        $args   = func_get_args();
        array_unshift($args, 'T');
        array_unshift($args, get_called_class());
        return call_user_func_array(array(Qube::Start(), 'queryObjects'),
                $args);
    }    
}
