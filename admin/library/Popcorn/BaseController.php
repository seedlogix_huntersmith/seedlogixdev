<?php
/**
 * Popcorn PHP Bootstrapping MVC Library
 * @author amado martinez <amado@projectivemotion.com>
 * 
 * @license GPL V2
 */
require_once dirname(__FILE__) . '/Template.php';
require_once dirname(__FILE__) . '/ThemeView.php';
/**
 * BaseController Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
abstract class BaseController
{
    protected $_themes_path = 'themes/';
    protected $_theme = 'default';
    protected $_rewriterfunc = NULL;
    protected $_showheader = true;
    protected $_showfooter = true;
        
    protected function setThemesPath($path)
    {
        $this->_themes_path =   $path;
        return $this;
    }
    /**
     * a function which accepts action and parameter values
     * to produce a url
     * 
     * @param type $callback
     * @return BasicController 
     */
    function setRewriter($callback)
    {
        $this->_rewriterfunc = $callback;
        return $this;
    }
    
    /**
     * Having assigned a rewriterfunc, calling this method will pass a string ("controller_action") to the 
     * function so that it properly redirects to the file to handle that action.
     * 
     * This is useful for rewriting urls for handling specific actions.
     * 
     * @param string $action 
     */
    function ActionRedirect($action, $arguments = array())
    {
        return $this->Redirect($this->ActionUrl($action, $arguments));
    }
    
    function ActionUrl($action, $arguments = array())
    {
        return call_user_func($this->_rewriterfunc, $this, $action, $arguments);
    }
    
    function Redirect($s)
    {
        return new HttpRedirectView($s);
    }
    
//    function dprInitialize()
//    {
//        if(isset($_GET['THEME']) && is_dir($this->_themes_path . $_GET['THEME']))
//        {
//            $this->_theme = $_GET['template'];
//        }
//    }
    
    function __construct() {
//        $this->Initialize();
    }
    
    /**
     *
     * @param string $page  the page to load from the _theme directory under 'templates/';
     * @return ThemeView 
     */
    function getView($page, $header = 'header.php', $footer = 'footer.php')
    {
        $T = new Template($this->_themes_path . $this->_theme, $header, $footer);  
        $view = new ThemeView($this, $T, $page . '.php', $this->_showheader, $this->_showfooter);        
        
        return $view;
    }
    
    
    function getAjaxView($object){
        $V = $this->getView('ajax', false, false);
        $V->getTemplate()->init('object', $object);
//        $V->setArgs('ajax.php', false, false);
        return $V;
    }
    
    /**
     * 
     * @return BaseView
     */
    function Execute($action = NULL)
    {
        $V  =   $this->preExecute($action);
        if($V instanceof BaseView)
            return $V;
        $T = $this->ExecuteAction($action);
        $this->postExecute($action, $T);
        return $T;
    }
    
    
    abstract function ExecuteAction($action = NULL);
    
    function preExecute($action)
    {
        
    }
    
    function postExecute($action, BaseView $View)
    {
        
    }
}
