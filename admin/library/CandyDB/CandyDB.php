<?php

/**
 * 
 * @author amado martinez projectivemotion
 * 
 */
	require_once(dirname(__FILE__). '/CandyUtils.php');
	
	class CandyDB
	{
		private $c;
		protected $lastresult;
		private static $inst = null;
		
	    public function __clone()
	    {
	        trigger_error('Clone is not allowed.', E_USER_ERROR);
	    }
	    
	    public function getConnectionResource()
	    {
	    	return $this->c;
	    }
	    
	   	public static function Expr($str)
	   	{
	   		return new DBSqlExpr($str);
	   	}
	   	
	   	static public function close()
	   	{
	   		if(self::$inst == null) return;
	   		mysqli_close(self::$inst->getConnectionResource());
	   		self::$inst = null;
	   	}
	   	
	   	/**
	   	 * 
	   	 * @return CandyDB instance
	   	 */
		static public function getInstance($h = null, $u = null, $p = null, $ddb = null)
		{
			if(self::$inst == null && func_num_args() > 1)
			{
				$c = __CLASS__;
				self::$inst = new $c($h, $u, $p, $ddb);	
			}
			return self::$inst;
		}
		
		public function getResultResource()
		{
			return $this->lastresult;			
		}
		
		private function __construct($h, $u, $p, $ddb = null)
		{
			// use an existing connection
			if($h == '#assign')
			{
				$this->c = $u;
				return;		
			}
			
			$this->c = mysqli_connect($h, $u, $p);
			if($ddb)
				$this->select_db($ddb);
		}
		
		public function select_db($db)
		{
			mysqli_select_db($db, $this->c);
		}
		
		/**
		 * Execute a string query.
		 * Not Safe At All
		 * 
		 * @param string $s the string to execute
		 */
		public function E($s)
		{
			$this->lastresult = mysqli_query($s, $this->c);
			if($this->lastresult === false)
				throw new Exception($s . ':' . mysqli_error(), mysqli_errno($this->getConnectionResource()));
			
			list($qRESULTtype) = explode(' ', ucwords($s));	
			$qRESULTtype = 'DB' . $qRESULTtype . 'Result';
			
			if(class_exists($qRESULTtype))
				return new $qRESULTtype($this, $s);
			
			return new DBResult($this, $s);
		}
		
		public function __call($name, $arguments)
		{
			$classname = 'DB' . ucwords($name) . 'Query';
			if(class_exists($classname))
				return new $classname($this);
			
			throw new Exception($classname .' Class Does Not Exist Or Method Undefined');
			return null;
		}
		
		/**
		 * 
		 * @param string $table name of table
		 * @param array|string $fields fields
		 * @param string $limit limit params
		 * @param string $where the conditions
		 * @return DBResult a DBResult object
		 */
		public function select($table ='', $fields = '*', $order = '', $limit = '', $where = '')
		{
			if(func_num_args() == 0)
				return new DBSelectQuery($this);
			
			return $this->E($this->select()->From($table)->Fields($fields)->orderBy($order)->Limit($limit)->Where($where));
		}
		
		/*
		 * Helper Function to Select count(*) from table with given conditions
		 * 
		 * @param string $table
		 * @param string $where
		 */
		static public function hCount($table, $where = '')
		{
			
			if(is_a($table, 'DBSelectQuery'))
			{
				$select = clone $table;
			}else
				$select = $this->select()->Where($where)->From($table); 
				
			return $select->Fields('count(*) as total')->orderBy(null)->Exec()->current(1)->total;
		}
	}
	
	//@todo
//	interface updatable
//	{
//		public function update()
//	}

	class DBSqlExpr
	{
		protected $strval;
		public function __construct($expr)
		{
			$this->strval = $expr;
		}
		public function __toString()
		{
			return $this->strval;
		}
	}
	
	class DBIntArr extends DBSqlExpr
	{
		public function __construct($array)
		{
			$newarr = array();
			foreach($array as $val)
				$newarr[] = (int) $val;
			parent::__construct('(' . implode(',', $newarr). ')');
		}
	}
	
//	CandyDB::$inst = null;
	require_once(dirname(__FILE__) . '/DBWhereExpression.php');
	require_once(dirname(__FILE__) . '/DBQuery.php');
	require_once(dirname(__FILE__) . '/DBInsertQuery.php');
	require_once(dirname(__FILE__) . '/DBUpdateQuery.php');
	require_once(dirname(__FILE__) . '/DBResult.php');
	require_once(dirname(__FILE__) . '/DBTable.php');