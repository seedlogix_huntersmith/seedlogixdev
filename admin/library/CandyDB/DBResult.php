<?php
	
	class DBResult implements Countable
	{
		protected $rh;	// result handle
		protected $q;	// query
		private $count;
		public function __construct(CandyDB &$dbhandle, $q)
		{
			$this->rh = $dbhandle->getResultResource();
			$this->count = $this->rh === true ? mysqli_affected_rows($dbhandle->getConnectionResource()) : @mysqli_num_rows($this->rh);
			$this->q = $q;	// could be string .. most likely a DBQuery
		}
	
		public function &getQuery(array $newParts = null)
		{
			$q = &$this->q;
			if($newParts)
				foreach($newParts as $part => $value)
					if(!is_string($part))
						throw Exception('Indexes Must be string type');
					else
						$q->$part($value);
			
			return $q;			
		}
		
		/*
		 * Interface Function: Returns Number of Rows, Affected Rows, etc
		 */
		public function count()
		{
			return $this->count;			
		}
	}
	
	class DBInsertResult extends DBResult
	{
		private $last_insert_id;
		public function __construct(CandyDB $db, $q)
		{
			parent::__construct($db, $q);
			$this->last_insert_id = mysqli_insert_id($db->getConnectionResource());
		}
		
		public function getInsertId()
		{
			if($this->count() > 0)
				return $this->last_insert_id;
			throw new Exception('Failed to insert into table');
			return null;
		}
	}
	class DBSelectResult extends DBResult implements Iterator
	{
		private $r, $i;
		
		public function __construct($rhandle, $q)
		{
			parent::__construct($rhandle, $q);
			$this->i = 0;
			$this->next();	// load current
			
		}
		
		public function toArray($unique = null)
		{
			$array = array();
			while($row = $this->current())
			{
				if($unique)
					$array[$row[$unique]] = $row;
				else
					$array[] = $row;
					$this->next();
			}
			return $array;
		}
		
		public function singleValue()
		{
			if($this->r)
				return array_pop($this->r);
			return null;
		}
		
		/*
		 * Interface Function (Iterator)
		 */
		public function current($object = false, $class = 'DBTableRow')
		{
			return $this->generateRow($object, $class);
		}
	
		/*
		 * Interface Function (Iterator)
		 */
		public function key()
		{
			return $this->i;
		}
		
		private function generateRow($object = false, $class = 'DBTableRow')
		{
			if($this->r === false) return false;
			$from = is_string($this->q) ? null : $this->q->getPart('FROM');	// could be a plain string
			return $object ? 
					new $class(
						is_array($from) ? $from[0] : $from,
					$this->q->getPart('WHERE'),
					$this->r)
					: $this->r;
		}
		
		/*
		 * Interface Function (Iterator)
		 * 
		 */
		public function next($object = false)
		{
			if($this->i++ > $this->count())
			{
				$this->r = false;
				return false;
			}
			$this->r = mysqli_fetch_assoc($this->rh);
			return $this->generateRow($object);
		}
		
		public function rewind()
		{
			// do not do anything..
		}
		public function valid()
		{
			return ($this->r !== FALSE);
		}
	}
?>