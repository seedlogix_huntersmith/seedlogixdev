<?php

class DBQuery
{
    /**
     *
     * @var PDO
     */
    protected $db;

    /*
     * @param string $q a simple query SEL [] FROM [] WHERE [] ORDER BY [] LIMIT []
     */
    public function __construct(PDO $db)
    {
        if ($db)
            $this->db = &$db;
    }

    /*
     * @param CandyDB $db Reference to a CandyDB
     */
    public function Exec(PDO $db = NULL)
    {
        if (!$db) $db = &$this->db;
        return $db->query((string)$this);
    }


    /**
     *
     * @return PDOStatement
     */
    function Prepare()
    {
        return $this->db->prepare((string)$this);
    }

    public function __clone()
    {
        foreach ($this as $name => $value) {
            if (gettype($value) == 'object' && !is_a($value, 'CandyDB') && !is_a($value, 'PDO')) {
                $this->$name = clone($this->$name);
            }
        }
//                $this->
    }
}

class DBQueryConditional extends DBQuery
{
    private $wherecondition;
    protected $parts;
    protected $bindings;

    public function __construct(PDO $db)
    {
        parent::__construct($db);
        $this->parts = new stdClass();
        $this->wherecondition = new DBWhereExpression();
    }

    /**
     *
     * @param string $clause
     * @param string $array
     * @return DBQueryConditional
     */
    public function andIn($clause, $array)
    {
        $this->wherecondition->andIn($clause, $array);
        $this->parts->WHERE = (string)$this->wherecondition;
        return $this;
    }

    public function appendWhere($joinby = ' AND ', $config = NULL)
    {
        if (empty($config)) return $this;
        $this->wherecondition->appendWhere($joinby, $config);
        $this->parts->WHERE = (string)$this->wherecondition;
        return $this;

        $w = (isset($this->parts->WHERE) ? '( ' . $this->parts->WHERE . ') ' . $joinby : '');

        $params = func_get_args();
        array_shift($params);
        $aw = is_string($config) ? call_user_func_array('sprintf', $params) : (is_array($config) ? DBUtil::whereToStr($config) : (string)$config);
        $w .= $aw;

        $this->parts->WHERE = $w;
        return $this;
    }

    /**
     *
     * if multiple parameters, uses sprintf format
     *
     * @param string $clause
     * @param string $arr
     * @return $this
     */
    public function Where($clause, $arr = null)//$c, $op = null, $v = null)
    {
//                    var_dump($this);exit;
        if (empty($clause)) return $this;
        $args = func_get_args();    // in 5.2 func_get_args cannot be passed directly
        call_user_func_array(array($this->wherecondition, 'Where'), $args);
//                    $this->wherecondition->Where($clause, $arr);
        $this->parts->WHERE = (string)$this->wherecondition;
        return $this;

        $params = func_get_args();
        array_unshift($params, ' AND ');
        return call_user_func_array(array($this, 'appendWhere'), $params);

        if ($arr == null) {
            if (!is_array($clause))
                $this->parts->WHERE = $clause;
            else
                $this->parts->WHERE = DBUtil::whereToStr($clause);
            return $this;
        }
        $params = func_get_args();
        $this->parts->WHERE = call_user_func_array('sprintf', $params);
        return $this;
    }
}

interface hasTables
{
    function getTable($tableorobj);
}

class DBSelectQuery extends DBQueryConditional
{
    protected $pdo_q_args;
    /**
     *
     * @var hasTables
     */
    private $tablenames;

    /**
     *
     * @var boolean
     */
    private $calc_found_rows = false;

    function calc_found_rows($b = true)
    {
        $this->calc_found_rows = $b;
        return $this;
    }

    public function hasTables(hasTables $t)
    {
        $this->tablenames = $t;
        return $this;
    }

    public function __construct(PDO $db)//$table ='', $fields = '*', $order = '', $limit = '', $where = '')
    {
        parent::__construct($db);

        $args = func_get_args();
        array_shift($args);
        $this->pdo_q_args = $args;

        $this->parts->FIELDS = '*';
        $this->parts->GROUPBY = '';
    }

    function setPdoQArgs()
    {
        $this->pdo_q_args = func_get_args();
    }

    public function asSubSelect($alias = 'x')
    {
        $q = new DBSelectQuery($this->db);
        call_user_func_array(array($q, 'setPdoQArgs'), $this->pdo_q_args);
        $q->From($this, $alias);

        return $q;
    }

    public function &groupBy($groupBy = '')
    {
        $this->parts->GROUPBY = $groupBy;
        return $this;
    }

    /**
     *
     * @param string $table
     * @param string $OnClause
     * @return \DBSelectQuery
     */
    public function &leftJoin($table, $OnClause = null)
    {
        $args = func_get_args();
        $this->parts->JOINLEFT[] = ' LEFT JOIN ' . call_user_func_array(array($this, 'parseJoin'), $args);
        return $this;
    }

    /*
     *
     * @params string|array $table;
     */
    public function &rightJoin($table, $OnClause = null)
    {
        $args = func_get_args();
        $this->parts->JOINLEFT[] = ' RIGHT JOIN ' . call_user_func_array(array($this, 'parseJoin'), $args);
        return $this;
    }

    /**
     * Join Implementation
     *
     * @param string $table
     * @param null|string $OnClause
     * @return $this
     */
    public function &Join($table, $OnClause = null)
    {
        $args = func_get_args();
        $this->parts->JOIN[] = call_user_func_array(array($this, 'parseJoin'), $args);
        return $this;
    }

    protected function parseJoin($table, $OnClause = NULL)
    {
        $JOINLEFT = '';
        $args = func_get_args();
        array_shift($args);
        $W = call_user_func_array('DBWhereExpression::Create', $args);//new DBWhereExpression($where);
        $OnClause = $W;

        if (is_array($table)) {
            list($classname, $alias) = $table;

            if (class_exists($classname)) {
                /**
                 * @var DatabaseObject
                 */
                $obj = new $classname;
                $tablename = $this->tablenames ? $this->tablenames->getTable($classname) : $obj->_getTableName(); //get_const($classname, 'TABLE');
                $where = $obj->_getCondition($alias);

//                            deb($where, $classname);
                /*
                if(!empty($where))
                {
                    $OnClause = is_null($OnClause) ? $W : $W->appendWhere(' AND ', $OnClause);
                }
                 *
                 */
            } else
                $tablename = $classname;
            $JOINLEFT = $tablename . ' as ' . $alias;

        } else {
            $JOINLEFT = $table;
        }

        if ($OnClause) {
            $JOINLEFT .= ' ON (' . $OnClause . ')';
        }

        return $JOINLEFT;
    }

    public function From($table = null, $alias = null)
    {
        $argc = func_num_args();
        if ($alias && $argc == 2) {
            $this->parts->FROM[] = array($table, $alias);
            $this->alias = $alias;
        } else {
            $args = func_get_args();
            for ($i = 0; $i < $argc; $i++) {
                if (is_a($args[$i], 'Pair')) {
                    $this->parts->FROM[] = (is_a($f, 'Pair') ? $f->first . ' as ' . $f->second : $f);
                } else
                    $this->parts->FROM[] = $args[$i];
            }
        }
        return $this;
    }

    public function &orderBy($o = null)
    {
        $this->parts->ORDERBY = $o;
        return $this;
    }

    public function &Limit($p = null, $q = null)
    {
        if ($q == null) {
            $this->parts->LIMIT = $p;
        } else
            $this->parts->LIMIT = new Pair($p, $q);

        return $this;
    }

    public function &Fields($f = '*')
    {
        $this->parts->FIELDS = (is_array($f) ? implode(',', $f) : $f);
        return $this;
    }

    function addFields($f)
    {
        $this->parts->FIELDS .= ',' . (is_array($f) ? implode(',', $f) : $f);
        return $this;
    }

    /*
     * @return Pair A Pair containing limit info
     */
    public function &getLimit()
    {
        return $this->parts->LIMIT;
    }


    public function getPart($name)
    {
        return @$this->parts->$name;
    }

    /**
     *
     * @param type $q_args
     * @return PDOStatement
     */
    function Exec($q_args = NULL)
    {

//                    echo $this, "\n\n"; //sleep(4);
        if (is_null($q_args)) //$db = $this->db;
            $q_args = $this->pdo_q_args;
        else
            $q_args = func_get_args();

        array_unshift($q_args, (string)$this);
//                    var_dump($this, func_get_args(), $this->db, $q_args);

        return call_user_func_array(array($this->db, 'query'), $q_args);
    }

    /**
     *
     * @return PDOStatement
     */
    function Prepare()
    {
        $stmt = parent::Prepare();
        call_user_func_array(array($stmt, 'setFetchMode'), $this->pdo_q_args);
        return $stmt;
    }

    protected function getFromPart($p)
    {
        $from = array();
        foreach ($p->FROM as $fri) {
            if (is_string($fri))
                $from[] = $fri;
            else
                $from[] = (is_string($fri[0]) ? $fri[0] : ' (' . $fri[0] . ') ') . ' as ' . $fri[1];
        }

        return join(', ', $from);
    }

    function setPart($name, $value)
    {
        $this->parts->$name = $value;
        return $this;
    }

    public function __toString()
    {
        $p = &$this->parts;
        /*
    $q = sprintf('SELECT %s%s%s',   // fields, FROM + JOINS, WHERE
                $p->FIELDS,
                @$p->FROM ? sprintf(' FROM %s', implode(', ', $p->FROM)) .
                    (@$p->JOINLEFT ? ' LEFT JOIN ' . implode(' LEFT JOIN ', $p->JOINLEFT) : '')
                : '',
                @$p->WHERE ? ' WHERE ' . $p->WHERE : ''
            );
    echo $q;
    $stmt = $this->db->prepare($q);
    */

        return 'SELECT ' . ($this->calc_found_rows ? 'SQL_CALC_FOUND_ROWS ' : '') . $p->FIELDS . ' FROM ' . $this->getFromPart($p) .

            (@$p->JOIN ? ' INNER JOIN ' . implode(' INNER JOIN ', $p->JOIN) : '') .

            (@$p->JOINLEFT ? implode(' ', $p->JOINLEFT) : '') .
            (@$p->WHERE ? ' WHERE ' . $p->WHERE : '') .
            (@$p->GROUPBY ? ' GROUP BY ' . $p->GROUPBY : '') .
            (@$p->ORDERBY ? ' ORDER BY ' . $p->ORDERBY : '') .
            (@$p->LIMIT ? ' LIMIT ' . (string)$p->LIMIT : '');
    }
}
        