<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * DBInsertQuery Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */
class DBInsertQuery extends DBQuery {

    protected $values;
    protected $fields;
    protected $Into;

    public static function quotateValues(&$arr) {
        foreach ($arr as $k => $v) {
            if (is_a($v, 'DBSqlExpr'))
                continue;
            $arr[$k] = '"' . addslashes($v) . '"';
        }
        return $arr;
    }

    public function __construct(PDO $db) {
        parent::__construct($db);
        $this->fields = array();
        $this->values = array();
    }

    public function &clearValues() {
        $this->values = array();
        return $this;
    }

    public function &Exec($clearValues = null, $two = true) {
        if ($two) {
            $ret = &parent::Exec($clearValues);
            $this->clearValues();
            return $ret;
        }

        if (is_bool($clearValues)) {
            if ($clearValues)
                $this->clearValues();
            return parent::Exec();
        }
    }

    public function &Into($table) {
        $this->Into = $table;
        return $this;
    }

    public function getTable() {
        return $this->Into;
    }

    /*
     * 
     * @param mixed $one
     */

    public function &Values($row) {
        return $this->V($row);
    }

    public function Prepare($with_values = NULL) {
        $this->values[] = $with_values;//array_fill(0, count($this->fields), '?');
        
        return parent::Prepare();
    }
    
    public function &V($vals) {
//			$vals = array_values($row);
        if (!empty($this->fields) && count($vals) != count($this->fields))
            throw new Exception('Value Count Does Not Equal Field Count fields(' . implode(',', $this->fields) . ') [' . count($this->fields) . '] vals(' . implode(',', $vals) . ') [' . count($vals) . ']');

//			$this->values[] = array_values(self::quotateValues($row));
        foreach ($vals as $k => $v) {
            if (!is_string($v))
                $vals[$k] = (string) $v;
            else
                $vals[$k] = '"' . addslashes($v) . '"';

            if (empty($vals[$k]))
                $vals[$k] = '""';
        }
        $this->values[] = $vals;

        return $this;
    }

    public function &Fields($fields_only) {
        if (!is_array($fields_only))
            $this->fields = $fields_only;
        else {
            $this->fields = array_values($fields_only);
        }
        return $this;
    }

    public function __toString() {
        if (empty($this->values) || empty($this->Into)) {
            return '/* Missing Values, Table From DBInsertQuery Object */';
        }

        $sql = 'INSERT INTO ' . $this->Into .
                (!empty($this->fields) ? ' (' . implode(', ', $this->fields) . ')' : '') .
                ' VALUES ';
        $count = count($this->values);
        $values = array();
        for ($i = 0; $i < $count; $i++) {
                $values[] = '(' . implode(',', $this->values[$i]) . ')';
        }

        $q = $sql . implode(',', $values);
        return $q;
    }

}