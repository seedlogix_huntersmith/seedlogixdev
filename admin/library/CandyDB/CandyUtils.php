<?php

class CandyUtil
{
	static function getPublicVars($o)
	{
		return get_object_vars($o);
	}
	
	static function addslash_values($arr)
	{
		foreach($arr as $k => $v)
		{
			$arr[$k] = addslashes($v);
		}
		return $arr;
	}
		
	static function &mergeKeys($assoc, &$o)
	{
		if(is_object($assoc))
			$assoc = self::getPublicVars($assoc);
		
		if($assoc == null) return $o;
		
		foreach($assoc as $k => $v)
		{
			$o->$k = $v;
		}
		return $o;
	}	
}

class DBUtil
{
	static function quoteExpr($k, $op, $v)
	{
		$k = (strpos($k, '.') === false) ? '`' . $k . '`' : $k;
		return $k . ' ' . $op . ' ' . ((is_string($v) or (!$v))
			? '"' . $v . '"' : (string)$v);
	}
	
	static function toExprArray($assoc)
	{
		$exprs = array();
		foreach($assoc as $k => $val)
		{
			if(is_string($k))
				$exprs[] = self::quoteExpr($k, '=', is_string($val) ? addslashes($val) : $val);
			else
				$exprs[] = $val;	// careful here!!
		}
		return $exprs;
	}
	
	static function unsetEmptyValues(&$assoc)
	{
		foreach($assoc as $k => $v)
		{
			if(empty($v)) unset($assoc[$k]);
		}
		return $assoc;
	}
	
	static function wrapQuotes($n)
	{
		return '"' . $n . '"';
	}
	
	static function whereToStr($where)
	{
		if(is_string($where)) return $where;
		if(is_array($where))
		{            
			return implode(' AND ', self::toExprArray($where));
		}
		if(is_object($where))
			return (string)$where;
		return null;
	}
}


class Pair
{
	public $first,$second;
	public function __construct($one, $two = null)
	{
		if($two == null)
		{
			if(is_string($one) && strpos($one, ',') !== false)
			{
			$f = explode(',', $one);
			$this->first = (int)$f[0];
			$this->second = (int)$f[1];
			return;
			}
			if(is_a($one, 'Pair'))
			{
				$this->first = $one->first;
				$this->second = $one->second;
			}
			return;
		}
		$this->first = $one;
		$this->second = $two;
	}
	
	final public function __toString()
	{
		return $this->first . ($this->second ? ',' . $this->second : ''); 
	}
}

class Pagination extends Pair
{
	private $data;
	/**
	 * 
	 * 
	 * @param DBSelectResult|Pair|string $pair A SQL Query or a Pair (curpage, perpage)
	 * 
	 */
	public function __construct($page, $perpage = null, $max = null)
	{
		$data = new stdClass;
		if(is_string($page))
			$page = new Pair($page);
		
		if(is_a($page, 'Pair'))
		{
			$data->currentpage = max(1,$page->first);
			$data->perpage = $page->second;
			$max = $perpage;	// shift param one
			$perpage = null;				
		}
		else{
			$data->currentpage = max(1,$page);
			$data->perpage = (int)$perpage;	//default
		}
		
		$this->data = $data;
		$this->data->max = ceil($max/$data->perpage)+1;
		if($perpage)
			$this->data->perpage = $perpage;
		parent::__construct(($this->data->currentpage-1)*$this->data->perpage, $this->data->perpage);
	}
	
	public function &nextPage(DBSelectQuery $q = null)
	{
		if(++$this->data->currentpage > $this->data->max) $this->data->currentpage--;
		if($q)
			return $this->Apply($q);
		return $this;
	}
	
	public function Paginate($path, $padding = 20)
	{
		$d = &$this->data;
		$numberpadding = round($padding/2);
		$r = ''; $l = '&#171;';
		
		$begin = max(1, $d->currentpage-$numberpadding);
		
		$end = min($d->max, ($d->currentpage < $numberpadding ? $padding : $d->currentpage+$numberpadding)+1);
		
		if($d->currentpage > 1)
		{
			$r .= '<a href="' . sprintf($path, 1) . '">' . $l . '</a> ';			
			$r .= '<a href="' . sprintf($path, $d->currentpage-1) . '">Previous</a> | ';
		}
		$r .= 'Page ';
		for($i = $begin; $i<$end; $i++)
		{
			$r .= '<a href="' . sprintf($path, $i) . '"' . 
				  ($i == $d->currentpage ? ' class=now' : '') .
				  '> ' . $i . '</a>';
		}
	
	
		if($d->max == 2) // 2 = 1 page!
				$r .= ' of 1';
		else
		if($d->currentpage < $d->max-1)
		{					
			$r .= ' | ';
			$r .= '<a href="' . sprintf($path, $d->currentpage+1) . '">Next</a> ' .
				  '<a href="' . sprintf($path, $d->max-1) .'">&#187;</a>';
		}
		return $r;
	}
	
	/*
	 * @params $result 
	 */
	public function &Apply(DBSelectQuery $q)
	{
		return $q->Limit((string)$this);
	}
}

?>