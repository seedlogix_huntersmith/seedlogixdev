<?php

/*
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

class DBWhereExpression {

	protected $str = '';

	function __construct() {
		if (!func_num_args())
			return;
		$args = func_get_args();
		call_user_func_array(array($this, 'Where'), $args);
	}

	public function appendWhere($joinby = ' AND ', $config = NULL) {
		if (is_null($config))
			throw new Exception('sorry for this.. provide config.');

		if (empty($config))
			return $this;

		$w = (!empty($this->str) ? '( ' . $this->str . ') ' . $joinby : '');

		$params = func_get_args();
		array_shift($params);
		$aw = is_string($config) && count($params) > 1 ? call_user_func_array('sprintf', $params) : (is_array($config) ? DBUtil::whereToStr($config) : (string) $config);
		$w .= $aw;

		$this->str = $w;
		return $this;
	}

	/**
	 *
	 * Places placeholders in side an IN () clause. 
	 * 
	 * @param string $clause example " ID IN (%s)"
	 * @param array $arr        an array of values to insert in the clause
	 * @return DBWhereExpression 
	 */
	public function andIn($clause, $array) {
		$c = sprintf($clause, join(', ', array_fill(0, count($array), '?')));
		return $this->appendWhere(' AND ', $c);
	}

	public function Where($clause, $arr = null) {//$c, $op = null, $v = null)
		if (empty($clause))
			return $this;

		$params = func_get_args();
		array_unshift($params, ' AND ');
		return call_user_func_array(array($this, 'appendWhere'), $params);
	}

	/**
	 * 
	 * @param string[] | DBWhereExpression $build_args
	 * @return DBWhereExpression
	 */
	static function Create($build_args) {
		if ($build_args instanceof DBWhereExpression)
			return $build_args;

		$w = new self;
		$args = func_get_args();

		return call_user_func_array(array($w, 'Where'), $args);
	}

	/**
	 * 
	 * Previx the Where String with WHERE or AND or OR or whatever.
	 */
	function FullClause($type = 'WHERE') {
		return $this->isEmpty() ? '' : " $type $this";
	}

	function isEmpty() {
		return empty($this->str);
	}

	function __toString() {
		return $this->str;
	}

}
