<?php
set_include_path(realpath(dirname(__FILE__) . '/../..') . PATH_SEPARATOR . get_include_path());
require_once('simpletest/unit_tester.php');
require_once('simpletest/reporter.php');

$test = &new GroupTest('NoseCandy Test');
$test->addTestFile('DBTest.php');
// $test->addTestFile('UtilsTest.php');
$test->run(new HtmlReporter());

?>