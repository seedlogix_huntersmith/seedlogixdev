<?php
require_once('simpletest/unit_tester.php');
require_once('NoseCandy2/CandyDB.php');

class DBUtils extends UnitTestCase {

	public function testPair()
	{
		$p1 = new Pair('1,2');
		$p2 = new Pair(1,2);
		
		$this->assertEqual($p1->first, 1, 'Pair Constructor Failed to Init From String');
		$this->assertEqual($p1->second, 2, 'Pair Constructor Failed to Init From String');
		
		$this->assertEqual($p2->first, 1, 'Pair Constructor Failed From Int');
		$this->assertEqual($p2->second, 2, 'Pair Constructor Failed From Int');
	}
	
	public function testPagination()
	{
		$p1 = new Pagination(1, 20, 200);	// page #1, 20 per page, set of 200
		$p2 = new Pagination(new Pair(1,20), 200);	// ^^
		$sel1 = new DBSelectQuery();
		$sel1->Limit(0,20);
		
//		var_dump($sel1->getLimit());
		$testPair = new Pair(0,20);
		
//		$this->assert
		$this->assertEqual((string)$p1,(string)$testPair, '(string) Pair != (string) Pagination(int,int,int)');
		$this->assertEqual((string)$p2, (string)$testPair, '(string) Pair != (string) Pagination(Pair,Max)');
		$this->assertEqual((string)$sel1->getLimit(), (string)$p1, 'DBSelectQuery::getLimit() != Pair(int,int)');
	}
}

?>