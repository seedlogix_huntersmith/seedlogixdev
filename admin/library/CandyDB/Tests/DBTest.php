<?php
require_once('simpletest/unit_tester.php');
require_once('NoseCandy2/CandyDB.php');
require_once('TableRowTest.class.php');

class DBTest extends UnitTestCase {
	private $DB;
	private $firephp;

	function setUp()
	{
		//global $db;	// for test purposes, connect to local
		$this->DB = CandyDB::getInstance('localhost', 'root', '', 'rubyx');
		
	}
	
	function testBasicExec()
	{
		
		$create_table_sql = ' CREATE TEMPORARY TABLE test (
		`identifier` INT NOT NULL AUTO_INCREMENT,
		`text` TEXT NOT NULL ,
		`intg` INT NOT NULL ,
		`datet` DATETIME NOT NULL ,
		PRIMARY KEY ( `identifier` )
		) ENGINE = InnoDB ';
		
		$this->assertIsA($this->DB->E($create_table_sql), 'DBResult');
	}
	
	function testTableRowClass()
	{
		$row = new DBTableRow('test');
		$row->identifier = 4;
		$row->text = "hello!";
		$row->intg = 32;
		$row->datet = CandyDB::Expr('NOW()');
		$row->insert('identifier');						// saving 1
		// set the key to be used with updates, delete, etc
		
		// find where identifier = 4
		$otherrow = DBTableRow::instance('test', array('identifier' => 4));	// load a local row
		$this->assertEqual($row->text, $otherrow->text);
				
		// updates text where identifier = 4
		$row->text = 'byebye!';
		$row->update();						// updating.. still only 1 row
		
		$otherrow->fetch(array('identifier' => 4));	// reloading into new variable
		$this->assertEqual($otherrow->text, 'byebye!');
		
		$otherrow->delete();				// deleting	the row from database (0 current rows)
		$ConfigTable = new DBTable('test', CandyDB::getInstance(), 'TableRowTest');
		
		$row = $ConfigTable->rowExists(array('identifier' => 4))
			? $ConfigTable->getExisting()
			: $ConfigTable->newRow(array('identifier' => 4));		// create new local row
		
		$countrows = $ConfigTable->select()->Fields('count(*) as howmanyrows');
		$howmanyrows = $countrows->Exec()->singleValue();
		
		$this->assertTrue($howmanyrows == 0);
		
		$row->text = 'hello again!';
		$row->save();								// and insert it
		
		$this->assertTrue($countrows->Exec()->singleValue() == 1);
		
		$otherrow->fetch(array('identifier' => 4));
		$this->assertEqual($otherrow->text, $row->text);
		
		$row->identifier = 87868;	// copy to new record
		$row->save(); 
		
		$this->assertTrue($countrows->Exec()->singleValue() == 2);
		$this->assertTrue($ConfigTable->rowExists(array('identifier' => 87868)));
		
		$row = $ConfigTable->getExisting(true);	// return instance of TableRowTest!!!;
		$this->assertTrue(is_a($row, 'TableRowTest'));
		
		// insert new row identifier:9000
		$ConfigTable->newRow(array('identifier' => 9000, 'text' => 'nine k'))->save();
		$this->assertTrue($ConfigTable->rowExists(array('identifier' => 9000)));	
		
		
		$this->assertEqual($countrows->Exec()->singleValue(), 3);
		$row->identifier = 686;	// this will UPDATE the row..
		$row->save();
		
		$row->identifier = 687;
		$row->insert();	// will insert row
		
		$this->assertTrue($countrows->Exec()->singleValue() == 4);
		
		// get all the rows
		$etc = $ConfigTable->select()->Exec()->toArray();
		$this->assertEqual(count($etc), 4);
	}
	
	function testValidateInsert()
	{
		$dirtydata = array ('identifier' => 'new',
			'text' => "one' two \" three text\"),",
			'integer' => '32\',',
			'datet' => new DBSqlExpr('NOW()'),
		'invalid' => 'should be invisible');
		
		$dirtyunassoc = array('-3', "abc'", "32#", new DBSqlExpr('NOW()'));
		
		$Table = new DBTable('test', array('identifier' => 'intval',
			'text' => 'addslashes',
			'datet', 'integer'), $this->DB);
		
//		$w1 = $Table->insert_assoc($dirtydata)->insert($dirtyunassoc);
		
		/*echo $w1 . '<br><br>';
		echo $w2;*/
	}
}

?>