<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * DBUpdateQuery Class
 *
 * @author Amado Martinez <amado@projectivemotion.com>
 */


class DBUpdateQuery extends DBQueryConditional
{
    /**
     * 
     * @param array $scheme array('field' => '%d', 'field2' => '%s',..)
     * 
     */
    public function &Table($table)
    {
            $this->parts->TABLE = $table;
            return $this;
    }

    private function arrayToValues($arrAssoc)
    {
            $str = array();
            foreach($arrAssoc as $key => $val)
            {
                    $str[] = "$keys = " . (
                            isset($this->escapeScheme[$key]) 
                            ? sprintf(
                                    (is_object($val) ? $this->escapeScheme[$key] : '"' . $this->escapeScheme[$key])
                                    , (string)$val
                                            ) 
                            : (!is_object($val) ? '"' . addslashes($val) . '"' : (string)$val)
                            );
            }

            return join(', ', $str);
    }

    private function escapeFields($assocARR)
    {
            $newvarr = array();
            foreach($assocARR as $key => $val)
            {
                    $newvarr[] = "$key = " . $this->escapeField($key, $val);
            }
            return $newvarr;
    }

    private function &escapeField($fieldName, &$fieldValue)
    {
            if(!isset($this->escapeScheme[$fieldName]))				
                    return (!is_a($fieldValue, 'DBSqlExpr') ? '"' . addslashes($fieldValue) . '"' : $fieldValue);// field name exists in scheme


            if(is_a($fieldValue, 'DBSqlExpr'))
                    return $fieldValue;

            $fieldValue = sprintf($this->escapeScheme[$fieldName], $fieldValue);

            return $fieldValue;
    }

    public function &Set($assoc_arr)
    {
        if($assoc_arr instanceof DBWhereExpression) throw new Exception(' this produces undesired behavior do not use that');
        
        if(is_string($assoc_arr) || $assoc_arr instanceof DBWhereExpression)
        {
            $this->parts->NEWVALUES = $assoc_arr;
            return $this;
        }
//        throw new Exception('deprecated use of thsi method without string or dbwherexpression');
            $ret = DBUtil::toExprArray($assoc_arr);
            $this->parts->NEWVALUES = implode(',', $ret);
            
            return $this;
    }

    public function __toString()
    {
            return 'UPDATE ' . (string)$this->parts->TABLE . ' SET ' . $this->parts->NEWVALUES
                                             . ($this->parts->WHERE ? ' WHERE ' . $this->parts->WHERE : '');
    }
}