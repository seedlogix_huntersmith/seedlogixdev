<?php

interface InterfaceDBInsert
{	
	public function insert_assoc($assoc, DBTable $validator);	
}

class DBSqlInsert
{
	private $extendor;
	protected $tablename;
//	
//	public function __call($func, $args)
//	{
//		if($this->extendor && method_exists($this->extendor, $func))
//					
//	}
	public function Into($tablename, $extendor = null)
	{
		$this->tablename = $tablename;
		if($extendor)
			$this->extendor = $extendor;
	}
}

class DBSqlInsertSet //implements InterfaceDBInsert
{
	private $table;
	public function insert_assoc(){}
}

/**
 * This class is a handler for methods executed on DBTable Class
 * It will execute the appropriate query
 *
 */
class DBTableQuery extends DBQuery
{
	private $table;
	private $execObj;
	public function __construct(DBTable $table, DBQuery $queryType)
	{
		echo 'wrapper created<br>';
		$this->table = $table;
		$this->execObj = $queryType;
	}
	
	/**
	 * Essentially, all methods called on this object will be executed on the
	 * appropriate dbquery object, or on the table object-- affecting those elements directly.
	 * 
	 * 
	 */
	public function __call($fname, $args)
	{
		$arr = array($this->table, $fname);
		if(method_exists($this->table, $fname))
		{
			$args[]  = $this;	// pass self so that new wrappers are not instantiated
			return call_user_func_array($arr, $args);
		}
		throw new Exception($fname . ' Does Not Exist In DBTable');
		return null;
	}
	
	public function __toString()
	{
		return (string)$this->execObj;
	}
	
	public function &Exec()
	{
		return $this->execObj->Exec();
	}
}



class DBTableRow extends stdClass
{
	private $_table;
	protected $_where;
	
	protected function where()
	{
		return $this->_where;
	}
	
	public function __construct($table, $where = null, $rowdata = null)
	{
		$this->_table = (is_string($table) ? new DBTable($table, CandyDB::getInstance()) : $table);
		if($where) $this->_where = DBUtil::whereToStr($where);
		if($rowdata != null)
			CandyUtil::mergeKeys($rowdata, $this);
	}
	
	public function fetch($where)
	{
		$this->_where = DBUtil::whereToStr($where);
		
		$q = CandyDB::getInstance()->select()
			->From($this->_table)
			->Where($this->where())
			->Limit(1);
		$r = $q->Exec();
		$vals = $r->current();
		return CandyUtil::mergeKeys($vals, $this);
	}
	
	public function update($where = null)
	{
		if(null == $where) $where = $this->_where;
		if($where == null)
		{
			throw new Exception(02, 'Where not defined');
			return null;
		}
		
		return CandyDB::getInstance()->update()->Table($this->_table)
			->Set(CandyUtil::getPublicVars($this))
			->Where($where)->Exec();
	}
	
	public function save()
	{
		$where = $this->where();
		return empty($where) ? $this->insert() : $this->update();
	}
	
	public function delete()
	{
		return CandyDB::getInstance()->E('DELETE FROM ' . $this->_table . ' WHERE '
			. $this->where() . ' LIMIT 1');
	}
	
	public static function instance($table, $where, $data = null)
	{
		if($data == null)
		{
			$res = CandyDB::getInstance()->select()
				->From($table)
				->Where(DBUtil::whereToStr($where))
				->Limit(1);
			$res = $res->Exec();
			$data = $res->current();
		}
		return CandyUtil::mergeKeys($data, new DBTableRow($table, $key));
	}
	
	public function fromAssoc($assoc)
	{
		return CandyUtil::mergeKeys($assoc, $this);
	}
	
	// means this is a fresh row..
	public function insert($key = null)
	{
		$myvars = CandyUtil::getPublicVars($this);
		$ret = CandyDB::getInstance()->insert()->Into($this->_table)->Fields(array_keys($myvars))->Values($myvars)->Exec();
		if($key)
		{
			$id = ($ret->getInsertId());
			// new
			$this->$key = $id;
			$this->_where = DBUtil::quoteExpr($key, '=', $id);
		}
		
		return $ret;
	}
	
	public function asArray()
	{
		return CandyUtil::getPublicVars($this);
	}
}

class DBTable
{
	private $name;
	private $escape;
	private $fields;
	private $db;
	private $_insertObj;
	private $_selectObj;
	private $_exists = null;
	private $_rowclass = 'DBTableRow';
	
	public function rowExists($where)
	{
		$this->_exists = $this->db->select()->From($this)->Where(DBUtil::whereToStr($where))->Limit($limit)->Exec();
		return count($this->_exists) > 0;
	}
	
	public function getExisting($object = false)
	{
		return $this->_exists->current($object, $this->_rowclass);
	}
	
	public function newRow($assoc = null)
	{
		$r = new $this->_rowclass($this);
		if(!$assoc) return $r;
		return $r->fromAssoc($assoc);
	}
	
	public function insert($one, DBTableQuery $wrapper = null)
	{
		if(is_object($one))
		{
			
		}
	}
	
	/**
	 * 
	 * @return DBSelectResult a select object
	 */
	public function select()
	{
		return $this->db->select()->From($this);
	}
	
	public function insert_assoc($assoc, DBTableQuery $wrapper = null)
	{
		$row = array();
		if($this->fields)
		{
			foreach($this->fields as $field)
			{
				if(!isset($assoc[$field])) $row[$field] = '';
				else
					if($this->escape[$field])
						$row[$field] = $this->escape[$field]($assoc[$field]);
					else
						$row[$field] = $assoc[$field];
			}			
		}else	// no fields specified.. do this loosely
			$row = array_values($assoc);
		
		if(!$this->_insertObj)
			$this->_insertObj = $this->db->insert()->Into($this->name);
		
		$this->_insertObj->V($row);
		
		// don't create new objects!!
		if($wrapper)
			return $wrapper;

		return new DBTableQuery($this, $this->_insertObj);
	}
	
	public function __construct($name, $scheme, $db = null, $rowclass = 'DBTableRow')
	{
		$this->name = $name;
		$this->fields = array();
		$this->escape = array();
		
		if(is_a($scheme, 'CandyDB'))
		{
			if(is_string($db))
			{
				$this->_rowclass = $db;
			}
			$db = $scheme;
		}
		
		foreach($scheme as $field => $escapefunc)
		{
			if(is_int($field))
			{
				$field = $escapefunc;
				$escapefunc = null;
			}
			$this->fields[] = $field;
			$this->escape[$field] = $escapefunc;
		}
		$this->db = $db;
		$this->_insertObj = $this->db->insert()->Into($this->name);
	}
	
	public function __toString()
	{
		return $this->name;
	}
}

class DBConditionClause
{
	private $op, $left, $right, $query;
	private $clause;
	private $values;
	
	public function __construct($clause, $arr, &$q = null)
	{
		$this->query = &$q;
		
		foreach($arr as $d)
		{
			if(is_array($d) && count($d) == 2)
			{
				// 0 = val, 1 = type
				$val = $d[0];
				if(!settype($val, $d[1])) throw new Exception('Invalid Type in DBConditionClause::__construct');
				$this->values[] = $val;
			}else
				$this->values[] = $d;
		}
		
//		if(is_a($left, __CLASS__))
//		{
//			$this->left = &$left->left;
//			$this->right = &$left->right;
//			$this->op = $left->op;
//			if($left->q)
//			$this->q = &$left->q;
//			return;
//		}
//		if(strpos($left, '%') !== false)
//		{
//			$args = func_get_args();
//			array_shift($args);	// lets do some magic!
//			$op = array_pop($args);
//			call_user_func_array('sprintf', $args);
//			$q = &$right;
//			$right = $op;
//			$v = strrpos($left, ' ', 0);
//			$op = substr($left, $v);
//			$left = substr($left, 0, $v);
//			
//			echo 'left=' . $left . 'op=' . $op;
//		}
//		$this->left = $left;
//		$this->right = $right;
//		$this->op = $op;
//		if($q) 
//			$this->query = &$q;
	}
	
	public function __call($funcname, $args)
	{
		// ending chain.. apply where and return query
		return call_user_func_array(array($this->query->Where($this), $funcname), $args);
	}
	
	public function setQuery(DBQuery $q)
	{
		$this->query = &$q;
	}
	
	public function __toString()
	{
		return $this->clause;
		//return $this->left . ' ' . $this->op . ' ' . $this->right;			
	}
	
	private function &shiftOperator($op, $right)
	{
		$this->left = new DBConditionClause($this);
		$this->right = &$right;
		$this->op = $op; 
		return $this;		
	}
	public function &_and($left, $op, $right)
	{
		//return $this->shiftOperator('AND', new DBConditionClause($left, $op, $right, $this->query));
	}
	
	public function &_or($left, $op, $right)
	{
		return $this->shiftOperator('OR', new DBConditionClause($left, $op, $right, $this->query)); 
	}
}
?>