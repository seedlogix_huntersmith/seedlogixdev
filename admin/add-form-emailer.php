<?php
session_start();         require_once( dirname(__FILE__) . '/6qube/core.php');

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$newEmailer = $hub->validateText($_POST['name'], 'Email Template Name');
$formID = is_numeric($_POST['formID']) ? $_POST['formID'] : '';

if($hub->foundErrors()){ //check if there were validation errors
	//$error = $directory->listErrors('<br />'); //list the errors
}
else { //if no errors
	if($newID = $hub->addNewFormEmailer($_SESSION['user_id'], $formID, $newEmailer)){
		$html = 
		'<div class="item3">
			<div class="icons">
				<a href="inc/forms/form_emailers.php?form=settings&id='.$newID.'&formID='.$formID.'" class="edit"><span>Edit</span></a>
				<a href="#" class="delete" rel="table=lead_forms_emailers&id='.$newID.'"><span>Delete</span></a>
			</div>
			<img src="img/field-email.png" class="icon" />
			<a href="inc/forms/form_emailers.php?form=settings&id='.$newID.'&formID='.$formID.'">'.$newEmailer.'</a>
		</div>';
		
		$response['success']['id'] = '1';
		$response['success']['container'] = '#fullpage_form-emailers .container';
		$response['success']['message'] = $html;
	}
}

echo json_encode($response);
?>