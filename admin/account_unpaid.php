<?php session_start(); ?>
<h1>
<? 
if($_SESSION['thm_welcome-msg']) echo $_SESSION['thm_welcome-msg'];
else echo 'Welcome to the organic marketing suite.';
?>
</h1>

<h2 style="color:black;">Account access denied.</h2>
The account you're trying to access doesn't have a billing profile set up yet.<br />
To enable access to this account you must first go to the Billing tab above and set up a billing profile.

