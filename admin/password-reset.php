<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('inc/local.class.php');
	$db = new Local();
	
	$salt = '?v8jfFFFGMcxckOMFG!'; //used to generate code for email
	$pepper = '08dgjdfgdfgm3w00t#'; //only on this page
	if($_GET['r'] || $_POST['r']){
		if(is_numeric($_GET['r'])) $_SESSION['temp_parent_id'] = $_GET['r'];
		else if(is_numeric($_POST['r'])) $_SESSION['temp_parent_id'] = $_POST['r'];
	}
	
	if($_GET){
		$email = $db->validateEmail($_GET['email'], 'Email');
		$query = "SELECT id FROM users WHERE username = '".$email."'";
		$row = $db->queryFetch($query);
		$id = $row['id'];
		$validation = md5($id.$salt.$email);
		if($row){
			if($_GET['x'] == $validation){
				$continue = true;
			}
			else $error = 1;
		}
		else $error = 2;
	}
	else if($_POST){
		$email = $db->validateEmail($_POST['email'], 'Email');
		$bl = $db->checkBlacklist($_SERVER['REMOTE_ADDR']);
		//bruteforce protection
		if((!$_SESSION['reset_attempts'] || $_SESSION['reset_attempts']<5) && !$bl){
			$newPass = $db->validatePassword($_POST['newPass'], 'Password', 3, 12);
			if($db->foundErrors()){
				$continue = true;
				$errors = $db->listErrors('<br />');
			}
			else {
				$id = is_numeric($_POST['id']) ? $_POST['id'] : '9999999999';
				//make sure the id and email match up
				$query = "SELECT username FROM users WHERE id = '".$id."'";
				$result = $db->queryFetch($query);
				if($result && $result['username']==$email){
					$validation = md5($id.$salt.$email);
					$validation2 = md5($validation.$pepper);
				}
				if($_POST['y'] && ($_POST['y'] == $validation2)){
					//if all checks were passed, update the password and notify the user
					$query = "UPDATE users SET password = '".$db->encryptPassword($newPass)."' WHERE id = '".$id."' LIMIT 1";
					if($db->query($query)) $success = true;
					else $error = 4;
				}
				else {
					if(isset($_SESSION['reset_attempts'])) $_SESSION['reset_attempts'] += 1;
					else $_SESSION['reset_attempts'] = 1;
					$error = 5;
				}
			}
		}
		else {
			if(!$bl){
				//blacklist IP if bruteforce attempt
				$query = "INSERT INTO blacklist (ip, username, reason)
						VALUES ('".$_SERVER['REMOTE_ADDR']."', '".$email."', 'Password reset bruteforce attempt.')";
				$db->query($query);
			}
		}
	}
	
	if(!$_SESSION['temp_parent_id']) include_once('inc/forms/password-reset-form.php');
	else include_once('inc/forms/password-reset-form-custom.php');
	
?>