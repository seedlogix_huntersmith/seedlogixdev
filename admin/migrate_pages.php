<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
/////////////////////////////////////////////////
// Migrate Pages Script
// Hub info is posted to this script and it 
// moves/converts all of the hub's pages to V2
// format.
// Created 9/4/11 by Reese

require_once(QUBEADMIN . 'inc/auth.php');
require_once(QUBEADMIN . 'inc/hub.class.php');
$hub = new Hub();

$hubID = $_GET['hubID'];
if(!is_numeric($hubID)) $hubID = 0;
$userID = $_SESSION['user_id'];
if(!is_numeric($userID)) $userID = 0;
if($_SESSION['reseller']) $parentID = $_SESSION['login_uid'];
else $parentID = $_SESSION['parent_id'];
$return['success'] = 0;

//Check if hub is already on V2 (and that user has access)
$query = "SELECT pages_version FROM hub WHERE id = '".$hubID."' AND user_id = '".$userID."'";
if($_SESSION['reseller']) $query .= " OR user_parent_id = '".$_SESSION['login_uid']."' ";
$hubInfo = $hub->queryFetch($query, NULL, 1);
if($hubInfo['pages_version']!=2){
	//Get hub pages
	$query = "SELECT * FROM hub_page WHERE hub_id = '".$hubID."' 
			  AND trashed = '0000-00-00'";
	$pages = $hub->query($query);
	
	if($pages && ($numPages = $hub->numRows($pages))){
		$navSectionsOrder = 0;
		$errors = 0;
		$errorMsg = array();
		$errosMsg2 = array();
		//Loop through each page and migrate it
		while($row = $hub->fetchArray($pages, 1)){
			//First back it up
			$hub->serialBackup($row, 'v2-migrate-bckp', 'hub_page-'.$row['id']);
			$continue = true;
			//Check to see if there's already a nav section with this one's navname
			$query = "SELECT id FROM hub_page2 WHERE hub_id = '".$hubID."' 
					  AND page_title = '".$hub->sanitizeInput($row['page_navname'])."' AND type = 'nav' LIMIT 1";
			$a = $hub->queryFetch($query);
			if($a['id']){
				//If there is, get the highest order # of subpages
				$nav_parent_id = $a['id'];
				$nav_order = $hub->getMaxOrderNum($nav_parent_id, $hubID)+1;
			}
			else {
				//If not, create the nav section and give this new page order #0
				$query = 
				"INSERT INTO hub_page2 
				(hub_id, user_id, user_parent_id, type, nav_parent_id, nav_root_id, nav_order, page_title, page_title_url, created)
				VALUES 
				('".$hubID."', '".$userID."', '".$parentID."', 'nav', 0, 0, '".$navSectionsOrder."', 
				 '".$hub->sanitizeInput($row['page_navname'])."', '".$hub->convertKeywordFolder($row['page_navname'])."', NOW())";
				if($hub->query($query)){
					$navSectionsOrder++;
					$nav_parent_id = $hub->getLastId();
					$nav_order = 0;
				}
				else {
					$errors++;
					$errorMsg[] = 'Error creating nav section "'.$row['page_navname'].'"';
					$errosMsg2[] = mysqli_error();
				}
			}
			//Nav section figured out, now copy page
			//Change/add fields
			$row['inc_nav'] = $row['nav'];
			$row['nav_parent_id'] = $nav_parent_id;
			$row['nav_order'] = $nav_order;
			$row['type'] = 'page';
			//Create V2 page
			if($newPageID = $hub->addNewPage($hubID, $row, $userID, $parentID, 1)){
				//Delete the old page
				$query = "DELETE FROM hub_page WHERE id = '".$row['id']."' LIMIT 1";
				$hub->query($query);
			}
			else {
				$errors++;
				$errorMsg[] = 'Error copying page "'.$row['page_title'].'"';
				$errosMsg2[] = mysqli_error();
			}
			
		} //end while($row = $hub->fetchArray($pages, 1))
		//Done looping, check for errors and continue
		if($errors){
			$return['message'] = 'Error migrating pages: ';
			foreach($errorMsg as $key=>$value){
				$return['message'] .= '\\n'.$value;
				$emailMessage = $value.'<br />'.$errorMsg2[$key].'<br /><br />';
			}
			//if all pages failed don't continue
			if($errors>=$numPages) $continue = false;
		}
		if($continue) $updateHub = true;
	} //end if($pages && $hub->numRows($pages))
	else {
		//No pages, just change hub row
		$updateHub = true;
	}
} //end if($hubInfo['pages_version']!=2)
else {
	$return['message'] = 'Hub has already been upgraded.';
}

if($updateHub){
	$query = "UPDATE hub SET pages_version='2' WHERE id = '".$hubID."'";
	if($hub->query($query)){
		$return['success'] = 1;
	}
	else {
		$return['message'] = 'Unknown error upgrading hub.';
	}
}

//if errors, email admins for further investigation
if($errorMsg){
	$to = array('admin@6qube.com', 'reese@6qube.com');
	$from = 'no-reply@6qube.com';
	$subject = 'Error migrating pages for hub #'.$hubID.', user #'.$userID;
	$message = 'User #'.$userID.' ran into the following errors while trying to upgrade Hub #'.$hubID.' that may need further investigation:<br />';
	$message .= str_replace('\\n', '<br />', $emailMessage);
	$hub->sendMail($to, $subject, $message, NULL, $from, 0, 1);
}

echo json_encode($return);
?>