<?php
session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');

//authorize
require_once('./inc/auth.php');

//include classes
require_once('./inc/reseller.class.php');
$reseller = new Reseller();

if($_SESSION['admin']){
	$section = $_GET['section'] ? $_GET['section'] : 'users';
	
	if($section == 'users'){
		//get # of users online now
		$query = "SELECT count(id) FROM online_now";
		$a = $reseller->queryFetch($query);
		$html = '<h2>Users online right now: <strong>'.$a['count(id)'].'</strong></h2>';
	}
	
	$query = "SELECT ".$section." FROM metrics WHERE id = 1";
	$a = $reseller->queryFetch($query);
	$html .= $a[$section];
?>
<style type="text/css">
td { padding:3px; }
</style>

<h1>System Metrics</h1>
<a href="admin-metrics.php?section=users">Users</a> | <a href="admin-metrics.php?section=resellers">Resellers</a> | <a href="admin-metrics.php?section=networks">Networks</a> | <a href="admin-metrics.php?section=autoresponders">Autoresponders</a> | <a href="admin-metrics.php?section=sites">Sites</a><br />
<?=$html?>

<? } ?>