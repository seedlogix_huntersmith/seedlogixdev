<?php
	//start session
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	//authorize Access to page
	require_once(QUBEADMIN . 'inc/auth.php');
	
	//iniate the page class
	require_once(QUBEADMIN . 'inc/hub.class.php');
	$hub = new Hub();
	
	$title = isset($_POST['title']) ? $hub->sanitizeInput($_POST['title']) : "";
	$id = isset($_POST['hubID']) ? $_POST['hubID'] : "";
	$nav_id = isset($_POST['nav_parent_id']) ? $_POST['nav_parent_id'] : 0;
	$type = isset($_POST['type']) ? $_POST['type'] : 'nav';
	$multi_user = isset($_POST['multi_user']) ? $_POST['multi_user'] : '';
	if($_SESSION['reseller']) $parent_id = $_SESSION['login_uid'];
	else $parent_id = $_SESSION['parent_id'];
	
	if($title){
		//get highest order # and add 1
		$highest = $hub->getMaxOrderNum($nav_id, $id);
		if($highest!==false) $thisOrder = $highest+1;
		else $thisOrder = 0; //there are no pages yet
		$page = array('nav_parent_id'=>$nav_id, 'type'=>$type, 'page_title'=>$title, 
					  'inc_contact'=>1, 'inc_nav'=>1, 'nav_order'=>$thisOrder);
		
		$page_id = $hub->addNewPage($id, $page, $user_id, $parent_id, 1);
		//if user is a reseller adding a new page to a multi-user hub theme
		if($multi_user){
			$result = $hub->pushNewMultiUserHubPage($id, $title, $_SESSION['login_uid'], $page_id, 1, $nav_id, $thisOrder, $type);
		}
		
		if((!$multi_user && $page_id) || ($result && $page_id)){
			$response['error'] = 0;	
			$response['page_id'] = $page_id;
		} else {
			$response['error'] = 1;
			$response['message'] = 'error: '.mysqli_error();
			//if(!$result1) $response['message'] .= '\nresult1 error';
			//if(!$result2) $response['message'] .= '\nresult2 error: '.$result2;
			//if(!$page_id) $response['message'] .= '\npage_id error';
		}
	}

	echo json_encode($response);
?>