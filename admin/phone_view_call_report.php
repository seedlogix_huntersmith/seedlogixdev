<?php
	
	$phoneid = $_GET['mid'];

?>
<div id="jquery_jplayer_1" class="jp-jplayer"></div>
  <div id="jp_container_1" class="jp-audio" style="display:none;">
    <div class="jp-type-single">
      <div class="jp-gui jp-interface">
        <ul class="jp-controls">
          <li><a href="javascript:;" class="jp-close">X</a></li>
          <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
          <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
          <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
          <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
        </ul>
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
        <div class="jp-time-holder">
          <div class="jp-current-time"></div>
          <div class="jp-duration"></div>
          <ul class="jp-toggles">
            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
          </ul>
        </div>
      </div>
      <div class="jp-title">
        <ul>
          <li>Bubble</li>
        </ul>
      </div>
      <div class="jp-no-solution">
        <span>Update Required</span>
        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
      </div>
    </div>
  </div>
<script type="text/javascript" src="http://6qube.com/admin/js/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
  	$('#wait').show();
    var storeJson;
		$('.jp-controls').delegate('.jp-close', 'click', function(){
			$('#jp_container_1').hide();
			$('#jquery_jplayer_1').jPlayer('stop');
		});
		$.ajax({
			type: 'GET',
			url: 'phone_get_call_report.php?id=<?=$phoneid?>',
			success: function(jsonData){
				var resp = $.parseJSON(jsonData);
				storeJson = resp.Data;
				
				
				
        PhoneMessage = Backbone.Model.extend({
        	convertDate: function(date){
        		var dateString = date.replace("/Date(", "").replace(")/", "");
        		var convertedDate = new Date(parseInt(dateString));
        		return convertedDate.toDateString();            		            		
        	}
        	
        });
        
        PhoneMessageList = Backbone.Collection.extend({
        	model: PhoneMessage
        });
        
        PhoneMessageView = Backbone.View.extend({
        	el: '#phoneMessages',
        	initialize: function(){
        		_.bindAll(this, "render");
        		this.phoneMessageList = new PhoneMessageList(storeJson);
        		this.phoneMessageList.bind("reset", this.render);
        		this.render();
        	},
        	render: function(model){
        		var containerEl = $('#phoneMessages');
        		this.phoneMessageList.each(function(item){
        			var phoneMessageItemView = new PhoneMessageItemView({
        				model: item
        			});
        			$(this.el).append(phoneMessageItemView.render().el);          			
        		}, this);            		
        		return this;
        	}         	
        });
        
        PhoneMessageItemView = Backbone.View.extend({
        	tagName: "ul",
        	className: "prospects",
        	initialize: function(){
        		_.bindAll(this, 'render');           		
        	},
        	events: {
        		"click .message-row": "showMessageId",
        		"click .test": "showMessageId",
        		"click .mp3": "getRecording"
        	},
        	render: function(){
        		var newDate = this.model.convertDate(this.model.get('DateCreated'));
        		$(this.el).html('<li class="dateRecorded" >' + newDate + '</li><li class="recordingStatus">' 
        		+ this.model.get('Status') + '</li><li class="from">' + this.model.get('From') + '</li><li class="lengthOfCall">'
        		+ this.model.get('Size') + '</li><li class="recordingLink"><a class="mp3" style="cursor:pointer;" href="' + this.model.get('MessageURL') + '">Play</a></li>');
        		return this;
        	},
        	showMessageId: function(evt){
        		evt.preventDefault();
        		alert(this.model.get('MessageId'))
        	},
        	getRecording: function(){
        		var mp3Link = this.model.get('MessageURL');
        		var jp = $("#jquery_jplayer_1");
        		jp.jPlayer('clearMedia');
                jp.jPlayer('setMedia', {
                	mp3: mp3Link
                });
                jp.jPlayer('play');
                if($('#jp_container_1:hidden')){
                	$('#jp_container_1').fadeIn(4000);
                }
        		return false;
        	}            	
        });

        var messageView = new PhoneMessageView();
        $('#wait').hide();
	            
				$("#jquery_jplayer_1").jPlayer({
					ready: function () {},
					swfPath: "/admin/js/jplayer/",
					supplied: "mp3",
          volume: 1
				});
			}		
		});
	
}); //END DOC READY	
</script>

<div id="side_nav">
	<ul id="subform-nav">
		<li class="first">
			<a href="phone_temp.php">Call Services</a>
		</li>
        <li>
			<a href="phone_view_call_details.php?mid=<?=$phoneid?>&rqtype=MonthToDate">Call Details</a>
		</li>
        <li>
			<a class="current" href="phone_view_call_report.php?mid=<?=$phoneid?>">Call Recordings</a>
		</li>
	
	</ul>
</div>
<br style="clear:left;" />
<br />

<ul class="prospects">
	<li class="dateRecorded"><strong>Date</strong></li>
	<li class="recordingStatus"><strong>Status</strong></li>
	<li class="from"><strong>From</strong></li>	
	<li class="lengthOfCall"><strong>Length</strong></li>	
	<li class="recordingLink"><strong>Recording Link</strong></li>
</ul>
<div id="phoneMessages"></div>
