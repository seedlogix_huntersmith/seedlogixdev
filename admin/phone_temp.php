<?php
	session_start();
         require_once( dirname(__FILE__) . '/6qube/core.php');
	
	require_once('./inc/auth.php');
	
	//include classes
	require_once(QUBEADMIN . 'inc/phone.class.php');
	$phone = new Phone();
	
	//get customerID
	$customer_id = $phone->getCustomerID($_SESSION['user_id'], $_SESSION['campaign_id']);
	
	$custID = $customer_id['customer_id'];
	
	//get mailboxes
	$mailboxView = $phone->getMailboxes($_SESSION['user_id'], $custID);

	
	$mailboxNumbers = $phone->getMBXNumbers($_SESSION['user_id'], $mailboxID);

?>
<? if($customer_id){ ?>
<h1>Phone Tracking Products</h1>
<? } else { ?>
<h1>Add Customer ID</h1>
<? } ?>


<script type="text/javascript">
		$('#createPhone').delegate('#addMailboxID', 'submit', function(event){
		event.stopPropagation();		
    	
		var mailboxID = $('#custID').val();
		if(mailboxID.length === 0){
			alert('Please enter a customer id.');
			return false;
		}
		
		$('#wait').show();
		$.ajax({
			type:'POST',
			url:'phone_settings_save.php',
			data: $('#addMailboxID').serialize(),
			success: function(d){
				if(d == "ok"){
				  alert('Customer ID added successfully!');
				}
				else if (d == "not valid"){
				  alert('That is not a valid customer id, please contact support.');
				}
				else{
				  alert('There was a problem with your submission, please contact support.');
				}
				$.get('phone_temp.php', function(r){
				  $('#ajax-load').html(r);
				  $('#wait').hide();
				});
			}
		});
		return false;		
	});
</script>
<? if(!$customer_id){ ?>
<div id="createPhone" class="createItem">
	<form id="addMailboxID" action="" class="ajax" method="post">
		<label for="mid">Enter Phone Customer ID</label>
		<input type="text" name="custID" id="custID" class="custID">
		<input type="hidden" name="xyz" value="<?=$_SESSION['user_id']?>" />
        <input type="hidden" name="cid" value="<?=$_SESSION['campaign_id']?>" />
		<input type="hidden" name="newMid" value="true" />
		
		<input type="submit" name="submit" value="Add Customer ID"><br />
		<label style="font-size:0.7em;">* This is different than your login user id. If you do not have a phone customer id, please contact support</label>
	</form>
</div>

<? } ?>

<br style="clear:both; margin-bottom:20px;" />
<div id="message_cnt" style="display:none;"></div>
<? if($mailboxView && is_array($mailboxView)){
	echo '<div class="mailboxContain">';
	foreach($mailboxView as $json=>$mailbox){
		echo '
		<ul class="prospects" title="'.$mailbox['MailboxId'].'">
		<li class="name" style="width:25%;background:url(\'img/v3/phones-icon-folder.png\') no-repeat; padding:15px 0 0 70px;height:35px;"><a href="phone_view_call_details.php?mid='.$mailbox['MailboxId'].'&rqtype=MonthToDate">Main Mailbox Number: '.$mailbox['PhoneNumberList'][0]['Description'].'</a></li>
		<li class="email" style="width:22%;"><i></i></li>
		<li class="email" style="width:20%;"><i></i></li>
		<li class="phone" style="width:10%;"><i></i></li>
		<li class="details" style="width:5%;float:right;"><a href="phone_view_call_details.php?mid='.$mailbox['MailboxId'].'&rqtype=MonthToDate"><img src="img/v3/view-mailbox.png" border="0" /></a></li>
		</ul>';
	}
	echo '</div><br />';
} else { ?>

	<? if(!$customer_id){ ?>
    <? } else { ?>
    <p>Service line connection lost.  Please try again in 10 seconds. <a href="phone_temp.php">Click here to refresh</a>.</p>
    <? } ?>

<? } ?>

<br style="clear:both;" />