<?php

defined('QUBEROOT') || die('Access Denied');

session_start();
$site = strtolower($_SERVER['SERVER_NAME']);
$a = explode('.', $site);
if(count($a)>3 && $a[0]!='www')
	$domain = $a[count($a)-3].'.'.$a[count($a)-2].'.'.$a[count($a)-1];
else
	$domain = $a[count($a)-2].'.'.$a[count($a)-1];

if($domain!='6qube.com'){
	require_once( QUBEADMIN . 'inc/db_connector.php');
    
	$connector = new DbConnector();
	
	//if(!$_SESSION['siteInfo']){ //use session vars so db isn't called on every page load
		if(TRUE){		
			$resellerID = $siteInfo['admin_user'];
			
			//get reseller's main site info
#			$query = "SELECT main_site, main_site_id FROM resellers WHERE admin_user = ".$resellerID;
#			$a = $connector->queryFetch($query);
#			$siteInfo['reseller_main_site'] = $a['main_site'];
#			$siteInfo['reseller_main_site_id'] = $a['main_site_id'];
			
			//get select data from reseller's main site
#			$query = "SELECT domain, company_name, site_title, keyword1, keyword2, blog, connect_blog_id  
					#FROM hub 
					#WHERE id = ".$siteInfo['reseller_main_site_id'];
                        
#			$mainSite = $connector->queryFetch($query);
                        $mainHub    =   $info->getResellerHub();
                        $mainSite   =   (array)$mainHub;
                        
			if($mainSite['blog']) $hasBlog = true;
			
			//check if reseller's main site has any custom pages
			require_once(QUBEADMIN . 'inc/hub.class.php');
			$hub = new Hub();
			$hub_pages = $hub->getHubPage($siteInfo['reseller_main_site_id'], NULL, NULL, NULL, TRUE, TRUE);
			if($hub->getPageRows()) $hasPages = true;
			
#			$query = "SELECT path FROM themes_network WHERE id = ".$siteInfo['theme'];
#			$themeInfo = $connector->queryFetch($query);
                        $themeInfo  =   array('path'    =>  $siteInfo['network_theme_path']);
			$themeInfo['themeUrl'] = 'http://'.$site.'/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themeUrl2'] = 'http://'.$siteInfo['reseller_main_site'].'/reseller/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themePath'] = QUBEROOT . 'search/network-themes/'.$themeInfo['path'].'/';
			$themeInfo['themePath2'] = QUBEROOT . 'reseller/network-themes/'.$themeInfo['path'].'/';
			
			include(QUBEROOT . 'search/network-themes/'.$themeInfo['path'].'/index.php');
		}
	//}
	//else include('/home/sapphire/public_html/6qube.com/search/network-themes/'.$_SESSION[$site.'_themeInfo']['path'].'/index.php');
}
else {
	if($_GET){
		$_SESSION['keyword'] = $_GET['business'];
		$_SESSION['location'] = $_GET['location'];
	}
	
	if(isset($_SESSION['keyword']) && isset($_SESSION['location'])){
		$searchBox1 = "  ".$_SESSION['keyword'];
		$searchBox2 = "  ".$_SESSION['location'];
	} else {
		$searchBox1 = "  Find What";
		$searchBox2 = "  Find Where";
	}
	//How many results do you want per heading?
	$limit = 20;
	//do not touch
	isset($_GET['offset']) ? $offset = $_GET['offset'] : $offset = 0;
	//$offset = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Search Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/search_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/search_favicon.png" />
<!--jquery library -->
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.hint.js"></script> 
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="js/scroll.js"></script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="css/ie6.css" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript"> 
$(function(){ 
		$('input[title!=""]').hint();
	});
</script> 
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
					<a href="/" title="Search Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube" ><img src="images/search-logo.png"  /></a></div>
			  </div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="http://local.6qube.com/" title="Local Yellow Pages Directory by 6Qube Local"><span>Local</span></a></li>
										<li><a href="http://browse.6qube.com/" title="Browse 6Qube"><span>Browse</span></a></li>
										<li><a href="http://hubs.6qube.com/" title="Local Website Directory by 6Qube Local | Local Websites"><span>HUBs</span></a></li>
                                        <li><a href="http://press.6qube.com/" title="Local Press Directory by 6Qube Local | Local Press Releases"><span>Press</span></a></li>												
										<li><a href="http://articles.6qube.com/" title="Local Articles Directory by 6Qube Local | Local Articles"><span>Articles</span></a></li>
										<li><a href="http://blogs.6qube.com/" title="Local Blog Directory by 6Qube Local | Local Blogs"><span>Blogs</span></a></li>
                                        <li><a href="http://elements.6qube.com/" title="Free Business Listing | Internet Marketing Software"><span>Free Business Listing</span></a></li>
									</ul>
									
 
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					<br clear="all" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<div id="search">
			<?	
		// ** Search ** //
					require_once(QUBEROOT . 'includes/searchForm.inc.php');
?>
		</div><!-- /search -->  
					<br clear="all" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container 5-->
			<div id="container_5">
				<!--Start Blogs Container-->
				<div id="blogs_container">
					<!--Start Blogs Container Blogs Container Top-->
					<div id="blogs_container_top">
						<!--Start Blogs Container Blogs Container Bottom-->
						<div id="blogs_container_bottom">
						  <!--Start Breadcrumb-->
						  <div id="breadcrumb">
							<h1>Search Local. Find Local.</h1>
						  </div>
						  <!--End Breadcrumb-->
						  <!--Start Blog Content-->
						  <div id="blog_content">
                          
                              <div id="search_display">
                                <?               
							require_once(QUBEADMIN . '/inc/local.class.php');
							$local = new Local();  
									                   
							$local->displaySearch($_SESSION['keyword'], $_SESSION['location'], $limit, $offset);
                                ?>                    
                              </div>
                          
						  </div>
						  	<!--End Blog Content-->
							<!--Start Blog Right Panel-->
						  <!--End Blog Right Panel-->
						  <br clear="all" />
						</div>
						<!--End Blogs Container Bottom-->
					</div>
					<!--End Blogs Container Top-->
				</div>
				<!--End Blogs Container-->
			</div>
			<!--End Container 5-->
		</div>
		<!--End Page Container-->
		<?	
		// ** Footer ** //
	require_once(QUBEROOT . 'includes/v2footer.inc.php');
?>
</body>
</html>
<? } ?>
