<?php
	//How many results do you want?
	$resultsLimit = 25; //This is the total limit of results before it collapses sections
	$limit = 5; //NOTE this is not total limit of results
	//do not touch
	$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	}
	
	//title and meta description -- gets changed if viewing city or category
	$title = "Local Directory by ".$resellerCompany." HUBs | Local Websites";
	$meta_desc = $resellerCompany." HUBs offers search for Local Websites in your local city. Simply search our Local Website Directory for local websites.";
	
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	
	if($_GET){
		$keyword = $local->validateText($_GET['keyword'], 'Keyword');
		$location = $local->validateText($_GET['location'], 'Location');
	}
	
	if(isset($keyword) && isset($location)){
		$searchBox1 = $keyword;
		$searchBox2 = $location;
		
		$searchResults = $local->getSearchResultsForNetwork($keyword, $location, $resellerID);
		if($searchResults){
			if(!$searchResults['numResults']){
				$headerTitle = "No results found";
				$noResults = true;
			}
			else {
				$headerTitle = "Search results for <strong>".$keyword."</strong> in <strong>".$location."</strong>";
				$dispResults = true;
				$numResults = $searchResults['numResults'];
			}
		}
	} else {
		$searchBox1 = "FIND WHAT";
		$searchBox2 = "FIND WHERE";
		$headerTitle = "Could not complete search";
		$badInput = true;
	}
?>
<?
	//COMMON HEADER
	$searchPage = true;
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
					<? if($siteInfo['search_network_name']){ ?>
					<h1><?=$siteInfo['search_network_name']?></h1>
                <? } else { ?>
                	<h1>Local Directory Search</h1>
                <?php } ?>
					<!-- Search-->
					<? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
				</div>
				
				<!-- Header Title -->
				<h1 style="font-size:36px;"><?=$headerTitle?></h1>
				
				<!-- Start Two Third -->
				<div class="two-third">						
					<!-- Breadcrumb -->
					<div id="breadcrumb">&nbsp;</div>
					
					<!-- Start Display of Results -->
					<div id="numResults" rel="<?=$numResults?>" title="<?=$resultsLimit?>" style="display:none;"></div>
<?php
if($badInput){
	echo "Please enter a value for both search boxes.";
}
else if($noResults){
	echo "Sorry, we couldn't find any results for <strong>".$keyword."</strong> in <strong>".$location."</strong>.";
}
else {
	$numLocalResults = count($searchResults['results']['local']);
	$numHubResults = count($searchResults['results']['hubs']);
	$numPressResults = count($searchResults['results']['press']);
	$numArticleResults = count($searchResults['results']['articles']);
	$numBlogResults = count($searchResults['results']['blogposts']);

	if($numLocalResults){		
		////////////////////////////////////////////////////////////////////////
		//	start LOCAL listing html loop
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		$i = 0; $hidden = false; $limExceed = false;
		if($numLocalResults > $limit) $limExceed = true;
		echo '<h1 style="display:inline;">Local business listing results ('.$numLocalResults.'):</h1>';
		echo ' <small><a href="#" class="hideAllLink" rel="Local">(click here to hide/show)</a></small>';
		echo '<div style="width:100%;border-bottom:#bbb solid 1px;"></div>';
		
		echo '<div id="allLocal">';
		foreach($searchResults['results']['local'] as $key=>$row){
			if($i >= $limit && !$hidden){
				echo '<br style="clear:both;height:50px;" />';
				echo '<h1><a href="#" class="displayMoreLink" rel="Local">Show more local business listing results...</a></h1>';
				echo '<div style="display:none;" id="displayMoreLocal">';
				$hidden = true;
			}
			$listingUrl = 'http://local.'.$domain.'/'.$local->convertKeywordUrl($row['keyword_one']).
											$local->convertKeywordUrl($row['company_name']).$row['id'].'/';
					
			if(!$row['photo'])
				$icon = $dflt_noImage;
			else 
				$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/directory/'.$row['photo'];
						
			$lastUpdated = date("F jS, Y", strtotime($row['last_edit']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
?>
				<!-- Start Listing -->
				<div class="blog-post">
					<!-- Date -->
					<div class="post-date">
						<span class="post-month"><?=$createdMo?></span><br />
						<span class="post-day"><?=$createdDay?></span>
					</div>
					
					<!-- Title -->
					<div class="post-title">
						<h3><a href="<?=$listingUrl?>"><?=$row['company_name']?></a></h3>
						<span class="clear">
							Tags <a href="<?=$listingUrl?>" title="<?=$row['keyword_one']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_one']?></a>
						<? if($row['keyword_two']){ ?>
							, <a href="<?=$listingUrl?>" title="<?=$row['keyword_two']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_two']?></a>
						<? } ?>
						<? if($row['keyword_three']){ ?>
							, <a href="<?=$listingUrl?>" title="<?=$row['keyword_three']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword_three']?></a>
						<? } ?>
						</span>
					</div>		
					<br style="clear:both;" />
					
					<!-- Image -->
					<div class="blog2-post-img">
						<div class="fade-img" lang="zoom-icon">
							<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['company_name']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
						</div>
					</div>
					
					<!-- Details -->
					<div class="displayPhone"><?=$row['phone']?></div>
					<p class="date"><i>Last Updated: <br/><?=$lastUpdated?></i></p>
					<?=$local->shortenSummary($row['display_info'], 250)?><br />
					<a href="<?=$listingUrl?>" title="Local Yellow Pages Directory by <?=$resellerCompany?> Presents <?=$row['company_name']?>">Visit <?=$row['company_name']?> Local Profile</a></p>
					<br class="clear" />
					
					<div class="add-container">
						<p class="address"><?=$row['street']?></p>
						<p class="city"><?=$row['city']?>, <?=$row['state']?> <?=$row['zip']?></p>
					</div>
					
					<div class="right">
						<a href="<?=$listingUrl?>" class="button_size4"><span>View Listing</span></a>
					</div>			
				</div> 
				<!-- End Listing -->
<?
			$i++;
		}
		if($limExceed) echo '</div>';
		echo '</div>'; //end <div id="allLocal">
		echo '<div style="height:50px;clear:both;">&nbsp;</div>';
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		//	end LOCAL listing html loop
		////////////////////////////////////////////////////////////////////////
	} //end if($numLocalResults)

//******************************************************************************//
		
	if($numHubResults){
		////////////////////////////////////////////////////////////////////////
		//	start HUBS listing html loop
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		$i = 0; $hidden = false; $limExceed = false;
		if($numHubResults > $limit) $limExceed = true;
		echo '<h1 style="display:inline;">Local website results ('.$numHubResults.'):</h1>';
		echo ' <small><a href="#" class="hideAllLink" rel="Hubs">(click here to hide/show)</a></small>';
		echo '<div style="width:100%;border-bottom:#bbb solid 1px;"></div>';
		
		echo '<div id="allHubs">';
		foreach($searchResults['results']['hubs'] as $key=>$row){
			if($i >= $limit && !$hidden){
				echo '<br style="clear:both;height:50px;" />';
				echo '<h1><a href="#" class="displayMoreLink" rel="Hubs">Show more local website results...</a></h1>';
				echo '<div style="display:none;" id="displayMoreHubs">';
				$hidden = true;
			}
			if(!$row['logo'])
				$icon = $dflt_noImage;
			else 
				$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/hub/'.$row['logo'];
						
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
						
			//Some hubs don't have description field filled.  use meta_description otherwise
			$description = $row['description'] ? $row['description'] : $row['meta_description'];
?>
				<!-- Start Listing -->
				<div class="blog-post">
					<!-- Date -->
					<div class="post-date">
						<span class="post-month"><?=$createdMo?></span><br />
						<span class="post-day"><?=$createdDay?></span>
					</div>
					
					<!-- Title -->
					<div class="post-title">
						<h3><a href="<?=$row['domain']?>"><?=$row['company_name']?></a></h3>
						<span class="clear">
							Tags <a href="<?=$row['domain']?>" title="<?=$row['keyword1']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword1']?></a>
						<? if($row['keyword2']){ ?>
							, <a href="<?=$row['domain']?>" title="<?=$row['keyword2']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword2']?></a>
						<? } ?>
						<? if($row['keyword3']){ ?>
							, <a href="<?=$row['domain']?>" title="<?=$row['keyword3']?> | <?=$row['company_name']?>" class="category_name"><?=$row['keyword3']?></a>
						<? } ?>
						</span>
					</div>		
					<br style="clear:both;" />
					
					<!-- Image -->
					<div class="blog2-post-img">
						<div class="fade-img" lang="zoom-icon">
							<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['company_name']?>"><img src="<?=$icon?>"  width="175px" alt="" /></a>
						</div>
					</div>
					
					<!-- Details -->
					<?=$local->shortenSummary($description, 250)?><br />
					<a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?> | <?=$row['keyword2']?> | <?=$row['keyword3']?>">Learn More</a></p>
					<br class="clear" />
					
					<div class="add-container">
						<p class="address">Created by <a href="<?=$row['domain']?>" title="<?=$row['company_name']?>"><?=$row['company_name']?></a> on <?=$created?></p>
					</div>
					
					<div class="right">
						<a href="<?=$row['domain']?>" class="button_size4"><span>View Hub</span></a>
					</div>			
				</div> 
				<!-- End Listing -->
<? 
			$i++;
		}
		if($limExceed) echo '</div>';
		echo '</div>'; //end <div id="allHubs">
		echo '<div style="height:50px;clear:both;">&nbsp;</div>';
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		//	end HUBS listing html loop
		////////////////////////////////////////////////////////////////////////
	}//end if($numHubResults)
	
//******************************************************************************//
		
	if($numPressResults){
		////////////////////////////////////////////////////////////////////////
		//	start PRESS listing html loop
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		$i = 0; $hidden = false; $limExceed = false;
		if($numPressResults > $limit) $limExceed = true;
		echo '<h1 style="display:inline;">Local press release results ('.$numPressResults.'):</h1>';
		echo ' <small><a href="#" class="hideAllLink" rel="Press">(click here to hide/show)</a></small>';
		echo '<div style="width:100%;border-bottom:#bbb solid 1px;"></div>';
		
		echo '<div id="allPress">';
		foreach($searchResults['results']['press'] as $key=>$row){
			if($i >= $limit && !$hidden){
				echo '<br style="clear:both;height:50px;" />';
				echo '<h1><a href="#" class="displayMoreLink" rel="Press">Show more local press release results...</a></h1>';
				echo '<div style="display:none;" id="displayMorePress">';
				$hidden = true;
			}
			if(!$row['thumb_id'])
				$icon = $dflt_noImage;
			else 
				$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/press_release/'.$row['thumb_id'];
			
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$keywords = explode(",", $row['keywords']);
			foreach($keywords as $key=>$value){
				$keywords[$key] = trim($value);
			}
			
			$a = explode('->', $row['category']);
			$pressCategory = $a[1];
			
			$pressUrl = 'http://press.'.$domain.'/'.$local->convertKeywordUrl($pressCategory).
											$local->convertKeywordUrl($row['headline']).$row['id'].'/';
?>
				<!-- Start Listing -->
				<div class="blog-post">
					<!-- Date -->
					<div class="post-date">
						<span class="post-month"><?=$createdMo?></span><br />
						<span class="post-day"><?=$createdDay?></span>
					</div>
					
					<!-- Title -->
					<div class="post-title">
						<h3><a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>"><?=$row['headline']?></a></h3>
						<span class="clear">
							Tags <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?>" class="category_name"><?=$keywords[0]?></a>
						<? if($keywords[1]){ ?>
							, <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[1]?>" class="category_name"><?=$keywords[1]?></a>
						<? } ?>
						<? if($keywords[2]){ ?>
							, <a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[2]?>" class="category_name"><?=$keywords[2]?></a>
						<? } ?>
						</span>
					</div>		
					<br style="clear:both;" />
					
					<!-- Image -->
					<div class="blog2-post-img">
						<div class="fade-img" lang="zoom-icon">
							<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['author']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
						</div>
					</div>
					
					<!-- Details -->
					<?=$local->shortenSummary($row['summary'], 250)?><br />
					<a href="<?=$pressUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>">Read More</a></p>
					<br class="clear" />
					
					<div class="add-container">
						<p class="address">Published by <a href="<?=$pressUrl?>" title="<?=$row['author']?>"><?=$row['author']?></a> on <?=$created?></p>
					</div>
					
					<div class="right">
						<a href="<?=$pressUrl?>" class="button_size4"><span>View Press Release</span></a>
					</div>			
				</div> 
				<!-- End Listing -->
<? 
			$i++;
		}
		if($limExceed) echo '</div>';
		echo '</div>'; //end <div id="allPress">
		echo '<div style="height:50px;clear:both;">&nbsp;</div>';
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		//	end PRESS listing html loop
		////////////////////////////////////////////////////////////////////////
	}//end if($numPressResults)
	
//******************************************************************************//
		
	if($numArticleResults){
		////////////////////////////////////////////////////////////////////////
		//	start ARTICLES listing html loop
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		$i = 0; $hidden = false; $limExceed = false;
		if($numArticleResults > $limit) $limExceed = true;
		echo '<h1 style="display:inline;">Local article results ('.$numArticleResults.'):</h1>';
		echo ' <small><a href="#" class="hideAllLink" rel="Articles">(click here to hide/show)</a></small>';
		echo '<div style="width:100%;border-bottom:#bbb solid 1px;"></div>';
		
		echo '<div id="allArticles">';
		foreach($searchResults['results']['articles'] as $key=>$row){
			if($i >= $limit && !$hidden){
				echo '<br style="clear:both;height:50px;" />';
				echo '<h1><a href="#" class="displayMoreLink" rel="Articles">Show more local article results...</a></h1>';
				echo '<div style="display:none;" id="displayMoreArticles">';
				$hidden = true;
			}
			if(!$row['thumb_id'])
				$icon = $dflt_noImage;
			else 
				$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/articles/'.$row['thumb_id'];
			
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$keywords = explode(",", $row['keywords']);
			foreach($keywords as $key=>$value){
				$keywords[$key] = trim($value);
			}
			
			$a = explode('->', $row['category']);
			$articleCategory = $a[1];
			
			$articleUrl = 'http://articles.'.$domain.'/'.$local->convertKeywordUrl($articleCategory).
												$local->convertKeywordUrl($row['headline']).$row['id'].'/';
?>
			<!-- Start Listing -->
				<div class="blog-post">
					<!-- Date -->
					<div class="post-date">
						<span class="post-month"><?=$createdMo?></span><br />
						<span class="post-day"><?=$createdDay?></span>
					</div>
					
					<!-- Title -->
					<div class="post-title">
						<h3><a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>"><?=$row['headline']?></a></h3>
						<span class="clear">
							Tags <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?>" class="category_name"><?=$keywords[0]?></a>
						<? if($keywords[1]){ ?>
							, <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[1]?>" class="category_name"><?=$keywords[1]?></a>
						<? } ?>
						<? if($keywords[2]){ ?>
							, <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[2]?>" class="category_name"><?=$keywords[2]?></a>
						<? } ?>
						</span>
					</div>		
					<br style="clear:both;" />
					
					<!-- Image -->
					<div class="blog2-post-img">
						<div class="fade-img" lang="zoom-icon">
							<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['author']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
						</div>
					</div>
					
					<!-- Details -->
					<?=$local->shortenSummary($row['summary'], 250)?><br />
					<a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>">Read More</a></p>
					<br class="clear" />
					
					<div class="add-container">
						<p class="address">Published by <a href="<?=$articleUrl?>" title="<?=$row['author']?>"><?=$row['author']?></a> on <?=$created?></p>
					</div>
					
					<div class="right">
						<a href="<?=$articleUrl?>" class="button_size4"><span>View Article</span></a>
					</div>			
				</div> 
				<!-- End Listing -->
<? 
			$i++;
		}
		if($limExceed) echo '</div>';
		echo '</div>'; //end <div id="allArticles">
		echo '<div style="height:50px;clear:both;">&nbsp;</div>';
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		//	end ARTICLES listing html loop
		////////////////////////////////////////////////////////////////////////
	}//end if($numArticleResults)
	
//******************************************************************************//
		
	if($numBlogResults){
		////////////////////////////////////////////////////////////////////////
		//	start BLOG POST listing html loop
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		$i = 0; $hidden = false; $limExceed = false;
		if($numBlogResults > $limit) $limExceed = true;
		echo '<h1 style="display:inline;">Local blog post results ('.$numBlogResults.'):</h1>';
		echo ' <small><a href="#" class="hideAllLink" rel="BlogPosts">(click here to hide/show)</a></small>';
		echo '<div style="width:100%;border-bottom:#bbb solid 1px;"></div>';
		
		echo '<div id="allBlogPosts">';
		foreach($searchResults['results']['blogposts'] as $key=>$row){
			if($i >= $limit && !$hidden){
				echo '<br style="clear:both;height:50px;" />';
				echo '<h1><a href="#" class="displayMoreLink" rel="BlogPost">Show more local blog post results...</a></h1>';
				echo '<div style="display:none;" id="displayMoreBlogPost">';
				$hidden = true;
			}
			//the getPostsForNetwork() function in local.class.php combines the required fields
			//from the blog_post and blogs tables.				
			if(!$row['photo'])
				$icon = $dflt_noImage;
			else 
				$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/blog_post/'.$row['photo'];
			
			$created = date("F jS, Y", strtotime($row['created']));
			$createdMo = date("M", strtotime($row['created']));
			$createdDay = date("j", strtotime($row['created']));
			
			$keywords = explode(",", $row['tags']);
			foreach($keywords as $key=>$value){
				$keywords[$key] = trim($value);
			}
			
			$postUrl = $row['domain']."/".$local->convertCategoryBlog($row['category']).
									 $local->convertPressUrl($row['post_title'], $row['post_id']);
?>
				<!-- Start Listing -->
				<div class="blog-post">
					<!-- Date -->
					<div class="post-date">
						<span class="post-month"><?=$createdMo?></span><br />
						<span class="post-day"><?=$createdDay?></span>
					</div>
					
					<!-- Title -->
					<div class="post-title">
						<h3><a href="<?=$postUrl?>" title="Read blog post <?=$row['post_title']?> about <?=$keywords[0]?> at <?=$row['blog_title']?>"><?=$row['post_title']?></a></h3>
						<span class="clear">
							Tags <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[0]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[0]?></a>
						<? if($keywords[1]){ ?>
							, <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[1]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[1]?></a>
						<? } ?>
						<? if($keywords[2]){ ?>
							, <a href="<?=$postUrl?>" title="Blog posts about <?=$keywords[2]?> at <?=$row['blog_title']?>" class="category_name"><?=$keywords[2]?></a>
						<? } ?>
						</span>
					</div>		
					<br style="clear:both;" />
					
					<!-- Image -->
					<div class="blog2-post-img">
						<div class="fade-img" lang="zoom-icon">
							<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['blog_title']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
						</div>
					</div>
					
					<!-- Details -->
					<?=$local->shortenSummary($row['post_content'], 250)?><br />
					<a href="<?=$postUrl?>" title="Read blog post about <?=$row['post_title']?> on <?=$row['blog_title']?>">Read More</a></p>
					<br class="clear" />
					
					<div class="add-container">
						<p class="address">Posted on <a href="<?=$row['domain']?>" title="<?=$row['blog_title']?>"><?=$row['blog_title']?></a> on <?=$created?></p>
					</div>
					
					<div class="right">
						<a href="<?=$postUrl?>" class="button_size4"><span>Read Blog Post</span></a>
					</div>			
				</div> 
				<!-- End Listing -->
<? 
			$i++;
		}
		if($limExceed) echo '</div>';
		echo '</div>'; //end <div id="allBlogPosts">
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		//	end BLOG POSTS listing html loop
		////////////////////////////////////////////////////////////////////////
	}//end if($numBlogResults)
}
?>
					
					<!-- End Display of Results -->					
				</div>
				<!-- End Two Third -->
				
				<!-- Last Third -->
				<div class="one-third last">&nbsp;</div>
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
