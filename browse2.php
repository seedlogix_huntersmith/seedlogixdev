<?php

require_once(dirname(__FILE__) . '/admin/6qube/core.php');
	session_start();
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
	
	// ** Header ** //
	require_once('includes/header.inc.php');
	
	if($_GET){
		if($_SESSION['section'] != $_GET['section'])
			$_SESSION['category'] = 'all';
		else 
			$_SESSION['category'] = $_GET['category'];
		$_SESSION['section'] = $_GET['section'];
		$_SESSION['state'] = $_GET['state'];
		$_SESSION['city'] = $_GET['city'];
	}
	
	$select_state = NULL;
	$select_city = NULL;
	
	$select_section = $local->browseGetSections($_SESSION['section']);
	$select_category = $local->browseGetCategories($_SESSION['section'], $_SESSION['category']);	
	
	if($_SESSION['section'] == 'directory' || $_SESSION['section'] == 'hub'){
		$select_state = $local->browseGetStates($_SESSION['section'], $_SESSION['category'], $_SESSION['state']);
		$select_city = $local->browseGetCities($_SESSION['section'], $_SESSION['category'], $_SESSION['state'], $_SESSION['city']);
	}
?>
    <h1>Browse</h1>
    <div id="browse_search">
    <form action="browse2.php" method="get" id="browse">
        <label>You can narrow by <strong>Section</strong>:
        <?=$select_section?>
        <br /><br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
        <label>And <strong>Category</strong>:</label>
        <?=$select_category?>
        <?=$select_state?>
        <?=$select_city?>
    </form>
    </div>
<?
	$local->displayBrowse($_SESSION['section'], $_SESSION['category'], $_SESSION['state'], $_SESSION['city']);
	
	// ** Footer ** //
	require_once('includes/footer.inc.php');
?>