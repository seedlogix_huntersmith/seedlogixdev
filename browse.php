<?php
	// 	 ## LEGEND ######################
	//   mi_id = user id
	//	 mi_397 = business name
	//	 mi_401 = business category
	//	 mi_398 = pointer to logo file
	//	 mi_399 = business description
	
	include './config/config.php';
	include './config/text_functions.php';
	include './config/browse_functions.php';
	include './config/open_db.php';
	include './config/sessions.php';
	
	$descLimit = 160; //set the letter limit for the description
	$newAdditionsLimit = 5; //set the number of new additions to display
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$cityState = displayIP2City($ip);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>6Qube Browse | Listing All Sides Of the Businesses You Want | Local Business Directory </title>
      <?php
	  	include './includes/meta.inc.php';
	  ?>
<style type="text/css" media="screen">
<!--
 @import url("css/style.css");
-->
</style>
</head>
<body>
<div id="wrapper">
  <div id="header">
    <div id="header_form"></div>
    <div class="call-back">
      <div class="style4"><span class="style5">Start Your </span><br />
        <span class="style7">Business Search Now... </span></div>
    </div>
    <div id="nav-menu">
     <?php
	  	include './includes/nav.inc.php';
	  ?>
      <div id="search-text"> <span class="style3">Search for a business below or use <a href="browse.php?id=<?=$_SESSION['plex_id']?>">browse category </a></span></div>
    </div>
  </div>
  <div id="main-content">
    <div id="left_col">
      <div class="padding">
        <?php
				if(!empty($_REQUEST['state'])) {
					$cat = $_GET['cat'];
					$sub = $_GET['sub'];
					$city = $_GET['city'];
					$state = $_GET['state'];
					
					$query = "SELECT mi_id, mi_1, mi_2, mi_3, mi_4, mi_397, mi_398, mi_401, mi_399 FROM members_info ";
					$query .= "WHERE mi_401 = '$cat->$sub' AND mi_3 = '$city' AND mi_4 = '$state' ";
					$query .= "ORDER BY mi_397 ASC";
					$query_result = mysqli_query($query);
					
					echo '<div class="search"><p class="results">Browsing <span class="business_name">'.$_GET['sub'].'</span> in <span class="location_name">'.$city.', '.$state.'</span></p></div>';
					displayResults($query_result, $descLimit);
				}
				else if(!empty($_REQUEST['cat'])) {
				// ################# displays all the states and cities for a specific sub category ########################
					echo '<div class="search"><p class="results">Browse <span class="business_name">'.$_GET['sub'].'</span> by Location</p></div>';
					displayStatesandCities($_GET['cat'], $_GET['sub']);
				}
				else {
					// ################# displays all the categories and sub categories ########################
					echo '<div class="search"><p class="results">Browse by Category</p></div>';
					displayCategories();
					
				} //close else
	  		?>
      </div>
    </div>
    <div id="right_col">
      <div id="search-contact-bussiness">
        <div id="search-form">
          <form method="get" action="index.php">
            <label for="searchPhrase"><span class="style3">Business or Category</span></label>
            <input type="text" name="business" value="<?php echo $_REQUEST['business'] ?>" size="28" />
            <label for="location"><br />
            <br />
            <span class="style3">Location</span><br />
            </label>
            <input type="text" name="location" value="<?php if($_REQUEST['location']) echo $_REQUEST['location']; else echo $cityState['city'] .', '. $cityState['region']; ?>" size="28" />
            <br />
            <br />
			<input name="id" type="hidden" value="<?=$_SESSION['plex_id']?>" />
            <input type="image" name="submit" src="images/search_button.jpg" alt="Submit">
          </form>
          <div align="left" style="padding-left:10px; " ><span class="style11"><a href="advanced_search.php?id=<?=$_SESSION['plex_id']?>">Advanced Search</a> | <a href="browse.php?id=<?=$_SESSION['plex_id']?>">Browse Now</a></span> </div>
        </div>
      </div>
    </div>
    <br style="clear:both;" />
    <br />
  </div>
  <div id="footer"> </div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9757019-6");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>