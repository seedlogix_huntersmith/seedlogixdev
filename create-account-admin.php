<?php
require_once(dirname(__FILE__) . '/admin/6qube/core.php');
require_once('admin/inc/local.class.php');
$local = new Local();

isset($_SESSION['createaccountauth']) ? $loggedIn = $_SESSION['createaccountauth'] : $loggedIn = false;

if($_POST){ //if the form is submitted
	if($_POST['adminp']){
		if($_POST['password']=="3lements"){
			$_SESSION['createaccountauth'] = $loggedIn = $displayForm = $adminForm = true;
		}
	}
	else {
		//validate all the form fields
		$user_id = $local->validateUserId($_POST['email'], 'User ID Email');
		$password = $local->validatePassword($_POST['password'], 'Password', 3, 12);
		$password2 = $_POST['password2'];
		$firstName = $local->validateText($_POST['first_name'], 'First Name');
		$lastName = $local->validateText($_POST['last_name'], 'Last Name');
		$phone = $local->validateNumber($_POST['phone'], 'Phone');
		$company = $local->validateText($_POST['company'], 'Company Name');
		$address = $local->validateText($_POST['address'], 'Address');
		$address2 = $_POST['address2'];
		$city = $local->validateText($_POST['city'], 'City');
		$state = $local->validateText($_POST['state'], 'State');
		$zip = $local->validateNumber($_POST['zip'], 'Zip Code');
		isset($_POST['userType']) ? $userType = $_POST['userType'] : $userType = 1;
		
		if($local->foundErrors() || ($password != $password2)){ //check to see if there were any errors
			 $errors = $local->listErrors('<br />'); //if so then store the errors
			 if($password != $password2){
				$errors .= 'Passwords did not match.<br />'; 
			 }
		}
		else {
			$password = $local->encryptPassword($password);
			$query = "INSERT INTO users (username, password, access, class) VALUES ('{$user_id}', '{$password}', 0, '{$userType}')";
			$result = $local->query($query);
			if($result){
				$insertID = mysqli_insert_id();
				$query = "INSERT INTO user_info (user_id, firstname, lastname, phone, company, address, address2, city, state, zip) VALUES ({$insertID}, '{$firstName}', '{$lastName}', '{$phone}', '{$company}', '{$address}', '{$address2}', '{$city}', '{$state}', '{$zip}')";
				if($result = $local->query($query)){ //if the query was successful (New user added)				
					//if user chose a free account
					if($userType==1){
						//Send the user a confirmation email
						if($local->sendRegisterMail($insertID, 1)){
							$displayMessage .= "<h1 style='color:black;'>Thanks for signing up!</h1><br /><h2>A confirmation email was sent to your email address.<br />You must click the link in the email within 24 hours to activate your account otherwise it will be deleted and you will have to make a new one!</h2>";
						} else {
							$errors .= "Sorry, an error occured while sending your confirmation email.<br />";
							$errors .= 'Please contact us at 877-570-5005 or email <a href="mailto:admin@6qube.com">admin@6qube.com</a> for help activating your account.<br />';
						}
					}
					//Create billing account
					$postfields["action"] = "addclient"; 
					$postfields["firstname"] = $firstName;
					$postfields["lastname"] = $lastName;
					$postfields["companyname"] = $company;
					$postfields["email"] = $user_id;
					$postfields["address1"] = $address;
					$postfields["address2"] = $address2;
					$postfields["city"] = $city;
					$postfields["state"] = $state;
					$postfields["postcode"] = $zip;
					$postfields["country"] = "US";
					$postfields["phonenumber"] = $phone;
					$postfields["password2"] = $password;
					$postfields["currency"] = "1";
					$postfields["groupid"] = $userType;
					$postfields["noemail"] = true;
					
					$results = $local->billAPI($postfields);
					
					if($results["result"]=="success") {
						//successfully created billing account.  update users table with billing accound id
						$query = "UPDATE users 
								  SET billing_id = {$results['clientid']}, 
									  access = 1, 
									  class = {$userType} 
								  WHERE username = '{$user_id}'";
						$result = $local->query($query);
						
						if($userType!=1){
							//Create user folders.  For free accounts, folders are created when account is activated
							$local->createUserFolders($insertID);
							//redirect browser to checkout page.
							//NOTE: make sure $userType (dropdown option value in create-account-form.php)
							//      equals the correct Product ID # in WHMCS (pid #2 = Professional acct., etc.)
							//header("Location: https://6qube.com/billing/cart.php?a=add&pid=".$userType);
							$displayMessage = "<h1 style='color:black;'>User #".$insertID." created successfully.  <a href='http://www.6qube.com/admin/'>Click here</a> to login.</h1>";
						}
					} else {
						# An error occured
						$errors .= "The following error occured while creating billing account:<br />".$results["message"];
					}
					
				}//end if($result = $local->query($query))
			}//end if($insert_id = mysqli_insert_id())
			else{
				$errors .= 'Something weird happened.  Please try again?';
			}
		}// end first else
	}
}//end if($_POST)

//If errors - show them
if($errors){
	$displayForm = true;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elements Internet Marketing Software Sign-Up</title>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<link rel="stylesheet" href="http://hubs.6qube.com/themes/elements/css/style.css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<!--jquery library -->
<!--<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>-->
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<!--dropdown menu files-->
<link rel="stylesheet" type="text/css" href="http://hubs.6qube.com/themes/elements/css/ddlevelsmenu-base.css" />
<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="http://hubs.6qube.com/themes/elements/js/scroll.js"></script>
<link href="http://www.6qube.com/css/uniform.default.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="http://www.6qube.com/js/jquery.uniform.min.js"></script>
<script type="text/javascript">
$(function(){
	$("select").uniform();
});
</script>
<!--other files-->
<!--[if IE 6]>  
<link rel="stylesheet" href="css/ie6.css" type="text/css" />
<![endif]-->
</head>
<body>
<!--Start Inner Main Div-->
<div id="inner_main_div">
	<!--Start Main Div_2-->
	<div id="main_div_2">
		<!--Start Page Container-->
		<div id="page_container">
			<!--Start Header_2-->
			<div id="header_2">
				<div id="header_left_div">
					<div class="logo">
					<a title="Internet Marketing Software | Elements by 6Qube" href="http://elements.6qube.com/" ><img src="http://hubs.6qube.com/themes/elements/images/logo.png"  /></a></div>
				</div><!--End Header Left Div-->
				<div id="header_right_div">
					<!--Start Top Menu-->
					<div id="top_menu">
						<div id="top_menu_right">
							<div id="top_menu_bg">
								<div id="ddtopmenubar">
									<ul>
										<li><a href="http://elements.6qube.com/" title="Internet Marketing Software | Elements by 6Qube"><span>Home</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-about/" title="About Internet Marketing Software Elements by 6Qube"><span>About Us</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-features/" title="Internet Marketing Software Features | Elements Internet Marketing Tools"><span>App Features</span></a></li>
                                        <li><a href="http://elements.6qube.com/internet-marketing-tools-signup/" class="activelink" title="Sign-up for Internet Marketing Software Elements | Free | Professional | Agency"><span>Sign Up</span></a></li>												
										<li><a href="http://elementsblog.6qube.com/" title="Internet Marketing Software Blog | Internet Marketing Tools"><span>Blog</span></a></li>
										<li><a href="http://elements.6qube.com/internet-marketing-software-contact/" title="Contact Internet Marketing Software Elements by 6Qube"><span>Contact Us</span></a></li>
                                        <li><a href="http://login.6qube.com/" title="Login to Elements | Internet Marketing Software"><span>Login</span></a></li>
									</ul>
									
 
								</div>
							</div>
						</div>
					</div>
					<!--End Top Menu-->
					<br clear="all" />
				</div><!--End Header Right Div-->
				<br clear="all" />
				<div id="main_title_div">
					<h1>Elements Internet Marketing Software Packages</h1>
					<p></p>
					<br clear="all" />
				</div><!--End Main Title Div-->
			</div>
			<!--End Header_2-->
			<!--Start Container_3-->
			<div id="container_3">
				<!--Start Container_3 Top-->
				<div id="container_3_top">
					<!--Start Container_3 Bottom-->
					<div id="container_3_bottom">
						<!--Start Breadcrumb-->
						<div id="breadcrumb">
							<ul>
								<li>You Are Here:</li>
								<li><a title="Internet Marketing Software | Elements by 6Qube" href="/" >Home</a></li>
								<li class="boldbreadcrumb">Elements Packages</li>							 
							</ul>
						</div>
						<!--End Breadcrumb-->
						<br clear="all" />
						<!--Start Content-->
						<div id="content">
						<?
							if($displayMessage) echo $displayMessage."<br />";
							if(!$loggedIn){
								//login form
								echo '<form action="create-account-admin.php" method="post">';
								echo '<input type="password" name="password" />';
								echo '<input type="hidden" name="adminp" value="1" />';
								echo '<input type="submit" name="submit" value="Submit" />';
								echo '</form>';
							}
							else {
								echo '<div id="error" style="padding:15px;color:red;font-weight:bold;">'.$errors.'</div>';
								if($displayForm) include('admin/inc/forms/create-account-form.php');
								else if($displayMessage) echo $displayMessage;
								else echo "Error";
							}
						?>	
						</div>
						<!--End Content-->
						<!--Start Right Panel-->
						<div id="right_panel">
							<!--Start Right Box 1-->
							<div class="right_box_1">
								<div>
									<div>
										<h2>App Features</h2>
										<ul>
											<li><a href="#" >SEO Website Builder</a></li>
											<li><a href="#">Blogging Platform</a></li>
											<li><a href="#">Landing Page Builder</a></li>
											<li><a href="#">Press Release Submission</a></li>
											<li><a href="#">Article Submission</a></li>
											<li><a href="#">SEO Local Listings</a></li>
                                            <li><a href="#">Content Distribution</a></li>
                                            <li><a href="#">Email Marketing</a></li>
                                            <li><a href="#">Advanced Web Analytics</a></li>
                                            <li><a href="#">Prospect Managment</a></li>
                                            <li><a href="#">Training & Support</a></li>
										</ul>
									</div>
								</div>
							</div>
							<!--End Right Box 1-->
						  <!--Start Right Box 2-->
					  <div class="right_box_2">
								<div>
									<div>
										<h2>Newsletter Signup</h2>
										<p>Sign up for our newsletter to receive the latest news, tips and event postings.</p>
										<input name="" type="text" /> <a href="#" class="signup"><img src="http://hubs.6qube.com/themes/elements/images/right-box-2-signup-button.jpg" alt="" /></a>
										<br clear="all" />
									</div>
								</div>
							</div>
							<!--End Right Box 2-->
							
						</div>
				    <!--End Right Panel-->
						<br clear="all" />
					</div>
					<!--End Container_3 Bottom-->
				</div>
				<!--End Container_3 Top-->
			</div>
			<!--End Container_3-->
		</div>
		<!--End Page Container-->
<?	
		// ** Footer ** //
		require_once(QUBEROOT . 'includes/v2Efooter.inc.php');
?>
</body>
</html>