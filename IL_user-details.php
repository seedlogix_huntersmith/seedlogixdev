<?php
    $viewSubscriptions = ($userobj->isAdmin() && $userobj->getID() == $User->getID()) || $userobj->isAdmin();
?>
<div class="fluid chosen_dropfix">

    <div class="nNote nInformation">
        <p>Changes in User Details will not be reflected until your next login.</p>
    </div>

    <form action="<?php echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save">
        <div class="widget grid6">
            <div class="whead">
				<h6>User#: <?=$userobj->getID()?> </h6>
			</div>
            <div class="formRow">
                <div class="grid3"><label>Login (Username):</label></div>
                <div class="grid9"><input type="text" name="user[username]" value="<?php $this->p($userobj->username); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>First Name:</label></div>
                <div class="grid9"><input type="text" name="info[firstname]" value="<?php $this->p($userobj->firstname); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Last Name:</label></div>
                <div class="grid9"><input type="text" name="info[lastname]" value="<?php $this->p($userobj->lastname); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Phone:</label></div>
                <div class="grid9"><input type="text" name="info[phone]" value="<?php $this->p($userobj->phone); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Company Name:</label></div>
                <div class="grid9"><input type="text" name="info[company]" value="<?php $this->p($userobj->company); ?>"></div>
            </div>
            <div class="formRow noBorderB">
                <div class="grid3"><label>Address:</label></div>
                <div class="grid9"><input type="text" name="info[address]" value="<?php $this->p($userobj->address); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Address 2:</label></div>
                <div class="grid9"><input type="text" name="info[address2]" value="<?php $this->p($userobj->address2); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>City:</label></div>
                <div class="grid9"><input type="text" name="info[city]" value="<?php $this->p($userobj->city); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>State/Province:</label></div>
                <div class="grid9">
                    <?php $this->getWidget('states-dropdown.php', array('value' => $userobj->state, 'name' => 'info[state]'));?>
                </div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Zip/Postal Code:</label></div>
                <div class="grid9"><input type="text" name="info[zip]" value="<?php $this->p($userobj->zip); ?>"></div>
            </div>
            <div class="formRow">
                <div class="grid3"><label>Website:</label></div>
                <div class="grid9"><input type="text" name="info[website_url]" value="<?php $this->p($userobj->website_url);
                    ?>"></div>
            </div>


            <div class="formRow">
            <div class="grid3"><label>Bio</label>:</label></div>
            <div class="grid9"><div class="j-dual-ide" style="position:relative" >
                    <ul class="tabs hand">
                        <li class="j-codemirror"><a href="#tabA1"><em>{Source Code}</em></a></li>
                        <li class="j-cleditor"><a href="#tabB1">Easy Editor</a></li>
                    </ul>
                    <div class="tab_container">
                        <div class="tab_content no_padding" id="tabA1" style="display: block;">
                            <textarea class="j-codemirror " name="info[bio]"><?php $this->p($userobj->bio)?></textarea>
                        </div>
                        <div class="tab_content" id="tabB1">
                            <textarea class="j-cleditor"><?php $this->p($userobj->bio)?></textarea>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                </div>
            </div>

            <?php if($userobj->parent_id==2556): ?>
                <div class="formRow">
                    <div class="grid3"><label>Main Sort Order:</label></div>
                    <div class="grid9"><input width="50px" type="text" name="info[tcity]" value="<?php $this->p
                        ($userobj->tcity);
                        ?>"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Team Sort Order:</label></div>
                    <div class="grid9"><input width="50px" type="text" name="info[api_id]" value="<?php $this->p
                        ($userobj->api_id);
                        ?>"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>NMLS Number:</label></div>
                    <div class="grid9"><input type="text" name="info[account_number]" value="<?php $this->p($userobj->account_number);
                        ?>"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Title:</label></div>
                    <div class="grid9"><input type="text" name="info[biz_contact_name]" value="<?php $this->p($userobj->biz_contact_name);
                        ?>"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Team Name:</label></div>
                    <div class="grid9"><input type="text" name="info[biz_company_name]" value="<?php $this->p($userobj->biz_company_name);?>"></div>
                </div>

            <?php endif; ?>

            <?php if($userobj->parent_id==99396): ?>

                <div class="formRow">
                    <div class="grid3"><label>FEG Agent ID: </label></div>
                    <div class="grid9"><input type="text" name="info[license_info]" value="<?php $this->p($userobj->license_info);
                        ?>"></div>
                </div>

            <?php endif; ?>


                <?php if($userobj->parent_id==6312 || $userobj->parent_id==100967): ?>
                    <div class="whead">
                        <h6>Business Profile</h6>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Company Name:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_company_name]" value="<?php $this->p($userobj->biz_company_name);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business Contact Name:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_contact_name]" value="<?php $this->p($userobj->biz_contact_name);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business Phone:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_phone_field]" value="<?php $this->p($userobj->biz_phone_field);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business Address:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_address]" value="<?php $this->p($userobj->biz_address);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business City:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_city]" value="<?php $this->p($userobj->biz_city);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business State:</label></div>
                        <div class="grid9">
                            <?php $this->getWidget('states-dropdown.php', array('value' => $userobj->biz_state, 'name' => 'info[biz_state]'));?></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Business Zip:</label></div>
                        <div class="grid9"><input type="text" name="info[biz_zip]" value="<?php $this->p($userobj->biz_zip);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>License Information:</label></div>
                        <div class="grid9"><input type="text" name="info[license_info]" value="<?php $this->p($userobj->license_info);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>License Information (additional):</label></div>
                        <div class="grid9"><input type="text" name="info[license_info_2]" value="<?php $this->p($userobj->license_info_2);
                            ?>"></div>
                    </div>
                    <div class="formRow">
                        <div class="grid3"><label>Hours:</label></div>
                        <div class="grid9"><input type="text" placeholder="Mon - Sat 8:00am - 8:00pm" name="info[hours]" value="<?php $this->p($userobj->hours);
                            ?>"></div>
                    </div>

                    <?php if($userobj->parent_id==6312): ?>
                    <div class="formRow">
                        <div class="grid3"><label>Security Camera Systems:</label></div>

                        <div class="grid9">
                            <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                ($userobj->custom1), 1)?> <?php $this->checked(($userobj->custom1 == 1) ? 1 : 0, 1); ?>
                                                                   name="info[custom1]"/></div>
                            <div class="clear"></div>
                        </div>

                    </div>
                        <div class="formRow">
                            <div class="grid3"><label>Access Control:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom2), 1)?> <?php $this->checked(($userobj->custom2 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom2]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Burglar Alarm Systems:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom3), 1)?> <?php $this->checked(($userobj->custom3 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom3]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Fire Alarm Systems:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom4), 1)?> <?php $this->checked(($userobj->custom4 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom4]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="formRow">
                            <div class="grid3"><label>Home Automation Systems:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom5), 1)?> <?php $this->checked(($userobj->custom5 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom5]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>

                        <div class="formRow">
                            <div class="grid3"><label>Intercom Systems:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom6), 1)?> <?php $this->checked(($userobj->custom6 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom6]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>

                        <div class="formRow">
                            <div class="grid3"><label>Network Cabling Services:</label></div>

                            <div class="grid9">
                                <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                                    ($userobj->custom7), 1)?> <?php $this->checked(($userobj->custom7 == 1) ? 1 : 0,
                                        1); ?>
                                                                       name="info[custom7]"/></div>
                                <div class="clear"></div>
                            </div>

                        </div>

                    <?php endif; ?>

            <div class="whead">
                <h6>Integration Settings</h6>
            </div>
                <div class="formRow">
                    <div class="grid3"><label>Account Manager:</label></div>
                    <div class="grid9"><input type="text" name="info[account_manager]" value="<?php $this->p($userobj->account_manager);
                        ?>"></div>
                </div>
                <div class="formRow">
                    <div class="grid3"><label>Account Number:</label></div>
                    <div class="grid9"><input type="text" name="info[account_number]" value="<?php $this->p($userobj->account_number);
                        ?>"></div>
                </div>
                    <div class="formRow">
                        <div class="grid3"><label>SFDC ID:</label></div>
                        <div class="grid9"><input width="50px" type="text" name="info[api_id]" value="<?php $this->p
                            ($userobj->api_id);
                            ?>"></div>
                    </div>
            <?php endif; ?>

        </div>
    </form>

	<!-- right column -->
    <div class="widget grid6">
        <div class="whead"><h6>Password</h6><div class="clear"></div></div>

        <form action="<?php echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save" onkeypress="return event.keyCode != 13;">
					<input type="hidden" name="user[ID]" value="<?php echo $userobj->getID(); ?>" />
                <div class="formRow">
                    <div class="grid3"><label>Set password:</label></div>
                    <div class="grid9">
                        <input type="password" name="user[password]" value="" />
                    </div>
                    <div class="clear"></div>
                </div>
        </form>
    </div>



    <div class="widget grid6">
        <div class="whead">
			<h6><?php if($userobj->parent_id==6312){ ?>Company Logo<?php } else { ?>Profile Image<?php } ?></h6>
		</div>
        <form action="<?php echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save">
			<div class="formRow">
				<div class="grid3"><label><?php if($userobj->parent_id==6312){ ?>Company Logo<?php } else { ?>Profile
                            Photo<?php } ?>:</label></div>
                <div class="grid9">
                    <?php
                    $this->getWidget('moxiemanager-filepicker.php', array(
                        'name' => 'info[profile_photo]',
                        'value' => $userobj->get('profile_photo'),
                        'idWidget' => 'profile_photo',
                        'noHost' => true,
                        'extensions' => 'jpg,jpeg,png,gif',
                        'path' => $storagePath
                    ));
                    ?>
				</div>
			</div>
        </form>
    </div>

    <?php if($userobj->parent_id==48205): ?>
    <div class="widget grid6">
        <div class="whead">
            <h6>Agent Onboarding</h6>
        </div>
        <form action="<?php echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save">
        <div class="formRow">
            <div class="grid3"><label>Bank Name:</label></div>
            <div class="grid9"><input type="text" name="info[bank_name]" value="<?php $this->p($userobj->bank_name);
                ?>"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Routing Number:</label></div>
            <div class="grid9"><input type="text" name="info[routing_number]" value="<?php $this->p($userobj->routing_number);
                ?>"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>Bank Account Number:</label></div>
            <?php
            // This should probably go into the base controller as a method. But hard to tell what base is with this code :-)
            /*$SECRET_KEY = "SEEDLOGIX-BEAST-01-01-2018";
            $SECRET_IV = "CHILANGO-FRIDAY";

            $encrypt_method = "AES-256-CBC";
            $key = hash('sha256', SECRET_KEY);
            $iv = substr(hash('sha256', SECRET_IV), 0, 16);
            
            // Decrypt the account number.
            $decrypted_account_number = openssl_decrypt($this->p($userobj->baccount_number), $encrypt_method, $key, 0, $iv);
            
            // Show masked account number.
            $masked_account_number =  str_pad(substr($decrypted_account_number, -4), strlen($decrypted_account_number), '#', STR_PAD_LEFT);*/
            ?>
            <div class="grid9"><input width="50px" type="text" name="info[baccount_number]" value="<?php echo $this->p($userobj->baccount_number) //$masked_account_number ?>"></div>
        </div>
        <div class="formRow">
            <div class="grid3"><label>W9 Form:</label></div>
            <div class="grid9">
                <a href="https://secure.rightsignature.com/templates/b598665c-7707-4212-b6e4-0283fd39c875/template-signer-link/9ee37e6a0d29e05eaa81f3e2a7f2fffb" class="sideB bCrimson" target="_blank">
                    <span>Complete W9 Form</span>
                </a>
            </div>
        </div>
        </form>
    </div>
    <?php endif; ?>

    <?php if($User->isAdmin()): ?>
        <div class="widget grid6">
            <div class="whead">
                <h6>User Access</h6>
            </div>
            <form action="<?php echo $this->action_url('UserManager_save?ID=' . $userobj->getID()); ?>" method="POST" class="ajax-save">
    <div class="formRow">
        <div class="grid3"><label>Disable Touchpoints:<?php echo $this->p($userobj->canceled);?></label></div>
        <div class="grid9">
        <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
            ($userobj->canceled), 1)?> <?php $this->checked(($userobj->canceled == 1) ? 1 : 0, 1); ?>
                                               name="user[canceled]"/></div>
        <div class="clear"></div>
        </div>
    </div>
                <div class="formRow">
                    <div class="grid3"><label>Login Enabled:</label></div>
                    <div class="grid9">
                        <div class="floatL mr10 on_off"><input type="checkbox" value="1" <?php $this->checked($this->p
                            ($userobj->access), 1)?> <?php $this->checked(($userobj->access == 1) ? 1 : 0, 1); ?>
                                                               name="user[access]"/></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </form>
        </div>
    <?php endif; ?>

    <?php
        $this->getWidget('user-roles.php');
    ?>
</div> <!-- /fluid -->

<?php // @todo make pretty and move to another view 
    if($viewSubscriptions): 
?>
<div class="fluid">
    <div class="row">
        <?php $widgets['users']->display('subscriptions.php'); ?>
    </div>
</div>
<?php endif; ?>

<?php
    $this->getWidget('user-subscription.php');
?>

<script type="text/javascript">
<?php $this->start_script()?>

/*
    function getPhoto(args)
    {
        var input = $('input[name="info[profile_photo]"]');
        var newValue = args.files[0].url;
        input.val(newValue);
        new AjaxSaveDataRequest(input.get(0), newValue);

        var preview = $('.preview');
        if(preview.length == 0){
            preview = $('<img class="preview">');
            preview.appendTo($('.preview-container'));
        }

        preview.attr('src', newValue);
    }
 */   
    function setDatePicker(datePickerID, userDate)
    {
        $( "#"+datePickerID ).val(userDate).trigger( "change" );
        return false;
    }

    //On Page Load
    $(document).ready(function (){

        <?php $this->getJ('wysiwyg-ide.php'); ?>

        $('.setDate').click(function(e){
            setDatePicker('sq_subExpDate', '00/00/0000');
        });
    
    });

<?php $this->end_script()?>
</script>