<?php
	//How many results do you want per heading?
	$mainLimit = 10;
	$rightLimit = 4;
	//do not touch
	$mainOffset = 0;
	$offset = 0;
	$dispArticles = true;
	$continue = true;
	
	$imgUrl = 'http://'.$siteInfo['reseller_main_site'].'/reseller/'.$resellerID.'/';
	$resellerCompany = $siteInfo['company'];
	if($siteInfo['no_image']){
		$dflt_noImage =  $imgUrl.$siteInfo['no_image'];
	} else {
		$dflt_noImage = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
	}
	
	//title and meta description -- gets changed if viewing city or category
	if($siteInfo['seo_title']){
		$title = $siteInfo['seo_title'];
	}
	else $title = "".$resellerCompany." Article Directory | Published Articles";
	if($siteInfo['seo_desc']){
		$meta_desc = $siteInfo['seo_desc'];
	}
	else $meta_desc = $resellerCompany." Articles Directory of Local Published Articles. View current articles from local businesses.";
	
	require_once(QUBEADMIN . 'inc/articles.class.php');
	$article = new Articles();

	if($siteInfo['city_focus']) $city = $siteInfo['city_focus'];
	if($siteInfo['state_focus']) $state = $siteInfo['state_focus'];
	
	if($args = $_GET['args']){
		$args_array = explode("/", $args);
		
		if($args_array[0]=="cities"){
			//List of cities
			//echo "cities";
			$dispArticles = false;
			$dispCities = true;
			$title = "Browse Local Local Articles by City | Local Articles";
			$meta_desc = "Find Local Business Articles by browsing ".$resellerCompany." Local Article Display of local articles.";
		}
		else if(is_numeric($args_array[0])){
			//Paginated all listings
			//echo "all listings - page ".$args_array[0];
			$dispCities = true;
			$currPage = $args_array[0];
			$mainOffset = ($mainLimit*$currPage)-$mainLimit;
			$title = "Local Articles Directory by ".$resellerCompany." Articles| Page ".$currPage;
			$meta_desc .= " Page ".$currPage." of results.";
		}
		else if($city = $article->validateText($args_array[0])){
			$a = explode("-", $city);
			if(count($a)>1){
				//check for state
				$statesAbv = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
				if(in_array(strtolower($a[count($a)-1]), $statesAbv)){
					//if last section of city arg is a state abbreviation (lubbock-tx)
					$state = strtoupper($a[count($a)-1]);
					$a = implode(" ", $a);
					$city = trim($a);
					$city = substr($city, 0, -3);
					$city = ucwords($city); //capitalize first letter(s)
				}
				else {
					//last section of city arg isn't a state (santa-fe or san-antonio)
					$a = implode(" ", $a);
					$city = trim($a);
					$city = ucwords($city);
					$state = $article->majorCity($city);
					$majorCity = true;
				}
			}
			else {
				//if the city arg is only one word (austin)
				$a = implode(" ", $a);
				$city = trim($a);
				$city = ucwords($city);
				$state = $article->majorCity($city);
				$majorCity = true;
			}
			$cityLink = $article->getCityLink($city, $state, $site, NULL, true);			
			
			if($args_array[1]){
				if(is_numeric($args_array[1])){
					//Paginated listings for a city
					//echo "$city." listings, page #".$args_array[1];
					$dispCats = true;
					$currPage = $args_array[1];
					$mainOffset = ($mainLimit*$currPage)-$mainLimit;
					$title = $city.', '.$state.' Local Articles Directory by '.$resellerCompany.' Articles | Page '.$currPage;
					$meta_desc = 'Find '.$city.', '.$state.' local business Articles in '.$city.' '.$article->getStateFromAbv($state).'. '.$resellerCompany.' Articles '.$city.' '.$state.' local article directory. Page '.$currPage.' of results.';
				}
				else if($args_array[1]=="categories"){
					//List of categories for a city
					//echo $city." Categories";
					$dispArticles = false;
					$dispCities = false;
					$dispCats = true;
					$title = $city.', '.$state.' Local Business Articles Categories | All Business Article Categories';
					$meta_desc = 'Find Local Business Articles in '.$city.', '.$state.' by browsing '.$resellerCompany.' Articles All Categories Display of '.$city.' '.$article->getStateFromAbv($state).' Local Articles.';
				}
				else if($category = $article->validateText($args_array[1])){
					$b = explode("-", $category);
					$b = implode(" ", $b);
					$category = ucwords(trim($b));
					$mainLimit = 25;
					
					if($args_array[2]){
						if(is_numeric($args_array[2])){
							if($args_array[2]<10){
								//Paginated listings for a certain category in a city
								//echo $city." ".$category."s, page ".$args_array[2];
								$currPage = $args_array[2];
								$mainOffset = ($mainLimit*$currPage)-$mainLimit;
								$title = $city.' '.$state.' '.$category.' Articles | '.$category.' Articles in '.$city.' | Page '.$currPage;
								$meta_desc = $city.' '.$state.' Article Directory of '.$category.' by '.$resellerCompany.' Articles.  Discover local '.$category.' Articles in '.$city.' '.$state.'. Page '.$currPage.' of results.';
							}
							else {
								$article_id = $args_array[2];
								$continue = 0;
								include('articles.php');
							}
						}
					}
					else {
						//All listings for a certain category in a city
						//echo $city." ".$category."s";\
						if($siteInfo['citycat_seo_title']){
						$title = $siteInfo['citycat_seo_title'];
						}	
						else $title = $city.' '.$state.' '.$category.' Articles | '.$category.' Articles in '.$city;
						
						if($siteInfo['citycat_seo_desc']){
						$meta_desc = $siteInfo['citycat_seo_desc'];
						}	
						else $meta_desc = $city.' '.$state.' Articles Directory of '.$category.' by '.$resellerCompany.' Articles.  View local '.$category.' Articles in '.$city.' '.$state.'.';
					}
				}
			} //end if($args_array[1])
			else {
				//All listings for a city
				//echo "All ".$city." listings";
				$dispCities = false;
				$dispCats = true;
				
				if($siteInfo['city_seo_title']){
				$title = $siteInfo['city_seo_title'];
				}	
				else $title = ''.$resellerCompany.' '.$city.', '.$state.' Articles Directory';
				
				if($siteInfo['city_seo_desc']){
				$meta_desc = $siteInfo['city_seo_desc'];
				}	
				else $meta_desc = 'Find '.$city.' '.$state.' Local Articles in '.$city.' '.$article->getStateFromAbv($state).'. '.$city.', '.$state.' Articles Directory by '.$resellerCompany.'';
			}
		}
	} //end if($args = $_GET['args'])
	else {
		//Root site
		$dispCities = true;
	}
	
if($continue){
?>
<?
	//COMMON HEADER
	include($themeInfo['themePath2'].'common_header.php');
?>
			
			<div class="inner-container">
				<div id="innerpage-title">
					<? if($siteInfo['remove_search']){ ?>
                <?=$siteInfo['edit_region_7']?>
				<? } else { ?>
                 	<? if($siteInfo['network_name']){ ?>
                        <h1><?=$siteInfo['network_name']?></h1>
                    <? } else { ?>
                        <h1>Articles Directory</h1>
                    <?php } ?>
                        <!-- Search-->
                        <? include($themeInfo['themePath2'].'/inc/common_search.php'); ?>
                <?php } ?>


				</div>
                
                <br class="clear" />
                <br class="clear" />
				
				<!-- Start Two Third -->
				<div class="two-third">	
				<?=$siteInfo['edit_region_1']?>
					<!-- Header Title -->
					<?php
					if($dispArticles){
						if($category) 
							echo '<h1>Local '.$category.' articles for '.$cityLink.'</h1>';
						else if($city) 
							if($siteInfo['city_focus']){

							}
							else echo '<h1><a href="http://'.$site.'">Local articles</a> for '.ucwords($city).', '.$state.'</h1>';
					}
					?>
						
					<!-- Breadcrumb -->
					<div id="breadcrumb">
					<?
					if($dispArticles){
						if($dispCities) {
							if($siteInfo['city_focus']){

							}
							else $article->displayCitiesArticles(6, $site, $resellerID, $siteInfo['category'], $state); 
						}
						else if($dispCats) $article->displayCategoriesArticles($city, $state, 4, $site, $resellerID, $siteInfo['category']);
					}
					?>
					</div>
					
					<!-- Start Display of Listings -->
			<?php
			if($dispArticles){
				if($results = $article->getArticlesForNetwork($mainLimit, $mainOffset, $city, $state, $category, $resellerID)){
					$numListings = $results['numResults'];
					$listings = $results['results'];
					while($row = $article->fetchArray($listings)){					
						if(!$row['thumb_id']) 
						$icon = $dflt_noImage;
						else 
						$icon ='http://'.$siteInfo['reseller_main_site'].'/users/'.$row['user_id'].'/articles/'.$row['thumb_id'];
						
						$created = date("F jS, Y", strtotime($row['created']));
						$createdMo = date("M", strtotime($row['created']));
						$createdDay = date("j", strtotime($row['created']));
						
						$keywords = explode(",", $row['keywords']);
						foreach($keywords as $key=>$value){
							$keywords[$key] = trim($value);
						}
						
						$a = explode('->', $row['category']);
						$articleCategory = $a[1];
						
						$articleUrl = 'http://'.$site.'/'.$article->convertKeywordUrl($articleCategory).
						$article->convertKeywordUrl($row['headline']).$row['id'].'/';
			?>
					<!-- Start Listing -->
					<div class="blog-post">
						<!-- Date -->
						<div class="post-date">
							<span class="post-month"><?=$createdMo?></span><br />
							<span class="post-day"><?=$createdDay?></span>
						</div>
						
						<!-- Title -->
						<div class="post-title">
							<h3><a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>"><?=$row['headline']?></a></h3>
							<span class="clear">
								Tags <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?>" class="category_name"><?=$keywords[0]?></a>
							<? if($keywords[1]){ ?>
								, <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[1]?>" class="category_name"><?=$keywords[1]?></a>
							<? } ?>
							<? if($keywords[2]){ ?>
								, <a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[2]?>" class="category_name"><?=$keywords[2]?></a>
							<? } ?>
							</span>
						</div>		
						<br style="clear:both;" />
						
						<!-- Image -->
						<div class="blog2-post-img">
							<div class="fade-img" lang="zoom-icon">
								<a href="<?=$icon?>" rel="prettyPhoto[gallery2column]" title="<?=$row['author']?>"><img src="<?=$icon?>" width="175px" alt="" /></a>
							</div>
						</div>
						
						<!-- Details -->
						<?=$article->shortenSummary($row['summary'], 250)?><br />
						<a href="<?=$articleUrl?>" title="<?=$row['author']?> | <?=$keywords[0]?> | <?=$keywords[1]?> | <?=$keywords[2]?>">Read More</a></p>
						<br class="clear" />
						
						<div class="add-container">
							<p class="address">Published by <a href="<?=$articleUrl?>" title="<?=$row['author']?>"><?=$row['author']?></a> on <?=$created?></p>
						</div>
						
						<div class="right">
							<a href="<?=$articleUrl?>" class="button_size4"><span>View Article</span></a>
						</div>			
					</div> 
					<!-- End Listing -->
				<? 
					} //end while($row = $article->fetchArray($listings))
				?>
					<!-- End Display of Listings -->
					<br clear="all" />
					
					<!-- Pagination -->
					<? include('inc/pagination.php'); ?>
				<?
				} //end if($listings = $article->getArticlesForNetwork()
				else echo 'No articles found.';
			} //end if($dispHubs)
			else {
				if($dispCities){
					echo '<h2>All cities with <a href="http://'.$site.'">local articles</a></h2>';
					$article->displayCitiesArticles(NULL, $site, $resellerID);
				}
				else if($dispCats){
					echo '<h2>All <a href="http://'.$site.'">local articles</a> categories for '.$cityLink.'</h2>';
					$article->displayCategoriesArticles($city, $state, NULL, $site, $resellerID);
				}
			}
			?>
			<p><?=$siteInfo['edit_region_4']?></p>
				</div>
				<!-- End Two Third -->
				
				<!-- Start Last Third -->
				<div class="one-third last">
					<div id="sidebar">
						<div id="sidebar-bottom">
							<div id="sidebar-top">
							<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_2']?>
							</div>
							<? if($siteInfo['parent'] == "0"){ ?>
                            <? } else { ?>
							<!-- Hubs -->
								 <? if($siteInfo['hubs_network_name']){ ?>
								<h2><?=$siteInfo['hubs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local HUBs</h2>
                                 <?php } ?>
								<div class="sidebar-advertise">
								<?php
								if($results = $article->getHubsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$hubs = $results['results'];
									
									echo '<ul>';
									while($row = $article->fetchArray($hubs)){
										if(!$row['logo']) 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/img/no_image.jpg';
										else 
											$icon = 'http://'.$siteInfo['reseller_main_site'].'/users/'
													.$row['user_id'].'/hub/'.$row['logo'];
								?>
										<li><a href="<?=$row['domain']?>" title="<?=$row['company_name']?> | <?=$row['keyword1']?>"><div style="background:url('<?=$icon?>') center no-repeat; width:125px; height:125px; border:1px solid #CCCCCC;"></div></a></li>
								<?php
									} //end while loop
									echo '</ul>';
								} //end if($results...
								else echo 'No HUBs found.';
								?>
								</div>
								<!-- End Hubs -->
								
								<!-- Local -->
								<? if($siteInfo['network_name']){ ?>
								<h2><?=$siteInfo['network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Business Listings</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $article->getListingsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$listings = $results['results'];
									while($row = $article->fetchArray($listings)){
										$a = explode("->", $row['category']);
										$listingCategory = $a[1];
										$listingUrl = 'http://local.'.$domain.'/'.
													$article->convertKeywordUrl($row['keyword_one']).
													$article->convertKeywordUrl($row['company_name']).$row['id'].'/';
								?>
									<li><a href="<?=$listingUrl?>" title=""><?=$row['company_name']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No directory listings found.';
								?>
								</ul>
								<!-- End Local -->
								
								<!-- Blog Posts -->
								<? if($siteInfo['blogs_network_name']){ ?>
								<h2><?=$siteInfo['blogs_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Blog Posts</h2>
                                 <?php } ?>
								<div class="recent-comments">
								<ul>
								<?php
								if($results = $article->getPostsForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$posts = $results['results'];
									while($row = $article->fetchArray($posts)){
										$postUrl = $row['domain']."/".$article->convertKeywordUrl($row['category']).
					 								$article->convertPressUrl($row['post_title'], $row['post_id']);
								?>
										<li><a href="<?=$postUrl?>"><?=$row['post_title']?></a></li>
                                        <?php
									} //end while loop
								} //end if($results...
								else echo 'No blog posts found.';
								?>
								</ul>
								</div>
								<!-- End Blog Posts -->
								
								<!-- Press -->
								 <? if($siteInfo['press_network_name']){ ?>
								<h2><?=$siteInfo['press_network_name']?></h2>
                                <? } else { ?>
                                <h2>Local Press Releases</h2>
                                 <?php } ?>
								<ul>
								<?php
								if($results = $article->getPressForNetwork($rightLimit,$offset,$city,$state,NULL,$resellerID)){
									$press = $results['results'];
									while($row = $article->fetchArray($press)){
										$a = explode("->", $row['category']);
										$pressCategory = $a[1];
										$pressUrl = 'http://press.'.$domain.'/'.
													$article->convertKeywordUrl($pressCategory).
													$article->convertKeywordUrl($row['headline']).$row['id'].'/';
								?>
									<li><a href="<?=$pressUrl?>" title=""><?=$row['headline']?></a></li>
								<?php
									} //end while loop
								} //end if($results...
								else echo 'No press releases found.';
								?>
								</ul>
								<!-- End Press -->
								<?php } ?>
								<div class="sidebar-advertise">
							<?=$siteInfo['edit_region_3']?>
							</div>
							</div><!-- End Sidebar Top -->
						</div><!-- End Sidebar Bottom -->
					</div><!-- End Sidebar -->
				</div>
				<!-- End Last Third -->
				<div class="clear"></div>
				
			</div><!--End Inner Page Container-->				
		</div><!-- End Main Container -->
		
	<?
		//COMMON FOOTER
		include($themeInfo['themePath2'].'common_footer.php');
	?>
<? } //end if($continue) ?>