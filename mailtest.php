<?php
require_once(dirname(__FILE__) . '/admin/6qube/core.php');

	require_once(QUBEADMIN . 'inc/db_connector.php');
	$connector = new DbConnector();
	
	//SMTP settings
	require_once 'Mail.php';
	$host = "relay.jangosmtp.net";
	$smtp = Mail::factory('smtp',
				array ('host' => $host,
					  'port' => 25, 
					  'auth' => false));
				
	if($_POST){
		$email = $_POST['test_address'];
		
		$to = $email;
		$from = 'no-reply@6qube.com';
		$subject = "Testing jangosmtp";
		$body = "Test message.";
		$headers = array ('From' => $from,
					   'Reply-To' => $from,
					   'To' => $to,
					   'Subject' => $subject,
					   'Content-Type' => 'text/html; charset=ISO-8859-1',
					   'MIME-Version' => '1.0');
		if($mail = $smtp->send($to, $headers, $body)) echo "Test message sent successfully.";
		else echo "Error sending message.";
		echo "<br /><br />";
	}

?>
<html>
<head><title>smtp test</title></head>
<body>
	<form action="mailtest.php" method="post">
		email address: <input type="text" name="test_address" value="" /><br />
		<input type="submit" name="submit" value="Send Test Mail" />
	</form>
</body>
</html>