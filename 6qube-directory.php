<?php

	include './config/config.php';
	include './config/sessions.php';
	include './config/text_functions.php';
	include './config/search_functions.php';
	include './config/open_db.php';
	
	$cityState = displayIP2City($ip);
	
	//$addresses = retrieveAddresses($newAdditionsLimit, $cityState, 'address');
	//$map_names = retrieveAddresses($newAdditionsLimit, $cityState, 'names');
	if(($addresses) && ($map_names))  { 
		// Your Google Maps API key
	   $key = "ABQIAAAAy-r1CaE6GQ0PT8bJtxJk8hQBwVSsGjV4Kb2VNM4d2d_Qe8idNhRZs1m1h7o6A4uBdr48ENclNJsgxQ";
	   // Desired address
	   $address = "http://maps.google.com/maps/geo?q=$cityState[city],+$cityState[region]&output=xml&key=$key";
	   // Retrieve the URL contents
	   $page = file_get_contents($address);
	   // Parse the returned XML file
	   $xml = new SimpleXMLElement($page);
	   // Retrieve the desired XML node
	  $cords = $xml->Response->Placemark->Point->coordinates;
	  $cordsArray = explode(",", $cords);
	 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
	include './includes/meta.inc.php';
?>
<meta content="6Qube is the culmination of Six Powerful Tools that benefit every business.&nbsp;By Claiming your FREE My6QubeSpot Business Listing, your business will receive a much greater online presence resulting in the ability to attract more customers." name="Description" />
<meta content="Sign up for 6Qube Directory, Free Business Directory, Free Business Listing, Free Business Advertising, Free Website, Free Lead Capture Page, Free Autorepsonder, Business Listings for Free" name="Keywords" />
<meta content="6Qube" name="author" />
<meta content="admin@6qube.com" name="email" />
<meta content="INDEX,FOLLOW" name="Robots" />
<meta content="7 Days" name="Revisit-after" />
<meta content="never" name="Expires" />
<?php if(($addresses) && ($map_names))  { ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAy-r1CaE6GQ0PT8bJtxJk8hQBwVSsGjV4Kb2VNM4d2d_Qe8idNhRZs1m1h7o6A4uBdr48ENclNJsgxQ"
    type="text/javascript">
  </script>
<script type="text/javascript">
    var map;
	var map2;
    var geocoder = null;
    var geocoder2 = null;
    var addressMarker;
	var map_names = [<?=$map_names?>];
    var addresses = [<?=$addresses?>];
	var gMarkers= new Array(addresses.length);
    var numGeocoded = 0;
	var gMarkers2= new Array(addresses.length);
    var numGeocoded2 = 0;
 
    function geocodeAll() {
      if (numGeocoded < addresses.length) {
        geocoder.getLocations(addresses[numGeocoded], addressResolved);
      }
	    
    }
	
		function createMarker(point,html) {
        var marker = new GMarker(point);
        GEvent.addListener(marker, "click", function(){showMap();});
        return marker;
      }
	function createMarker2(point,html) {
        var marker = new GMarker(point);
        GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
        return marker;
      }
 
   function addressResolved(response) {
     var delay = 0;
     if (response.Status.code == 620) {
       // Too fast, try again, with a small pause
       delay = 80000;
     } else {
       if (response.Status.code == 200) {
         place = response.Placemark[0];
         point = new GLatLng(place.Point.coordinates[1],
                             place.Point.coordinates[0]);
         marker = createMarker(point)
		 gMarkers[numGeocoded] = point;
		 //alert(gMarkers[numGeocoded]);
         map.addOverlay(marker);
       }
       numGeocoded += 1;
     }
     window.setTimeout(geocodeAll, delay);
   }
   
   
       function geocodeAll2() {
      if (numGeocoded2 < addresses.length) {
        geocoder2.getLocations(addresses[numGeocoded], addressResolved2);
      }
	    
    }
	
   function addressResolved2(response) {
     var delay = 0;
     if (response.Status.code == 620) 
	 {
       // Too fast, try again, with a small pause
       delay = 5000;
     }
	 else 
	 {
     	if (response.Status.code == 200) 
		{
         	place = response.Placemark[0];
         	point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
         	marker = createMarker2(point,map_names[numGeocoded2] + '<br /><a target="_blank" href="'+ 'http://www.google.com/search?q='+addresses[numGeocoded2]+'" >'+addresses[numGeocoded2]+'</a>')
		 	gMarkers2[numGeocoded2] = point;
		 	//alert(gMarkers[numGeocoded]);
         	map2.addOverlay(marker);
       }
       numGeocoded2 += 1;
     }
     window.setTimeout(geocodeAll2, delay);
   }
   
   
   
   function setStaticMap(centerpt, zoomlevel)
   {
  
		var bounds = new GLatLngBounds;
		for (var i=0; i<gMarkers.length; i++) {
		
		//alert(gMarkers[i].toString());
		bounds.extend(gMarkers[i]);
		}
		//alert(map.getBoundsZoomLevel(bounds));
		var bgCenter = bounds.getCenter();
		var gbZoom = map2.getBoundsZoomLevel(bounds);
		map2.setCenter(bgCenter,gbZoom); 		
   }
 	function setCenterZoom()
	{
	    var bounds = new GLatLngBounds;
		for (var i=0; i<gMarkers.length; i++) {
		

		//alert(gMarkers[i].toString());
		bounds.extend(gMarkers[i]);
		}
		//alert(map.getBoundsZoomLevel(bounds));
		var bgCenter = bounds.getCenter();
		var gbZoom = map.getBoundsZoomLevel(bounds);
		map.setCenter(bgCenter,gbZoom); 			
		setStaticMap(bgCenter,gbZoom);
	}
    function load() {
      if (GBrowserIsCompatible()) {     
		
		map = new GMap2(document.getElementById("staticMap"));		
		map2 = new GMap2(document.getElementById("map"));	
        geocoder = new GClientGeocoder();
        geocoder.setCache(null);
        //window.setTimeout(geocodeAll, 50);
		geocodeAll();
		
        //window.setTimeout(geocodeAll, 50);
		geocoder2 = new GClientGeocoder();
        geocoder2.setCache(null);
		geocodeAll2();
		//alert(map.isLoaded().toString());
		setTimeout(setCenterZoom,1500);		
		map.disableDragging()
		var topRight = new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(50,10));
		map2.addControl(new GMapTypeControl(),topRight);
		map2.removeMapType(G_SATELLITE_MAP);
		//map2.addControl(new GScaleControl  ());
		//map2.addControl(new GLargeMapControl ());
		
      }
    }
</script>

<script type="text/javascript" src="jquery-1.3.2.min.js"></script>
<?php } ?>
<style type="text/css" media="screen">
<!--
@import url("css/style.css");
.style10 { font-size:12px; color:#666; text-align:right; padding-right:410px; }
.next { float:right; }
#the-additions-image { clear:right; }
-->
</style>
</head>
<body onload="load()" onunload="GUnload()">
<div id="wrapper">
  <div id="header">
    <div id="header_form"></div>
    <div class="call-back">
      <div class="style4"><span class="style5">Start Your </span><br />
        <span class="style7">Business Search Now... </span></div>
    </div>
    <div id="nav-menu">
      <?php
	  	include './includes/nav.inc.php';
	  ?>
      <div id="search-text"> <span class="style3">Search for a business below or use <a href="browse.php?id=<?=$_SESSION['plex_id']?>">browse category </a></span></div>
    </div>
  </div>
  <div id="main-content">
    <div id="left_col">
      <div class="padding">
		<?php
				// checks if a search has been submitted
				if( (!empty($_REQUEST['business'])) || (!empty($_REQUEST['location'])) )
				{
					$city_and_state = explode(",",strip_tags($_GET['location']));
				  	$searchArray["business"] = strip_tags($_GET['business']);
				  	$searchArray["city"] = trim($city_and_state[0]);
				  	$searchArray["state"] = trim($city_and_state[1]);

				  	$query_result = basicSearch($_GET['business'], $_GET['location']);
				 	//output the results message
				  	displayResultsMessage($searchArray, mysqli_num_rows($query_result));
				  	//output the result items
				  	if(mysqli_num_rows($query_result) > 0)
				  		displayResults($query_result);
				}
				else
				{
					if(!empty($_REQUEST['offset']))
						$offset = sanitizeQuery(strip_tags($_GET['offset']));
					else
						$offset = 0;
					
				 	//display the featured spots
				 	displayFeatured($featuredLimit, $offset, $cityState);
					
				  	//display the new additions
				  	listNewAdditions($newAdditionsLimit, $cityState);
					
				  	mysqli_close();
				}
			?>      
		</div>
    </div>
    <div id="right_col">
      <div id="search-contact-bussiness">
        <div id="search-form" >
          <form method="get">
            <label for="searchPhrase"><span class="style3">Business or Category</span></label>
            <input type="text" name="business" value="<?php echo strip_tags($_REQUEST['business']) ?>" size="28" />
            <br /><br />
			<label for="location"><span class="style3">Location</span></label>
			<br />
            <input type="text" name="location" value="<?php 
				if($_REQUEST['location']) 
					echo strip_tags($_REQUEST['location']); 
				else {
					if ($cityState['city'] != "")
						echo $cityState['city'];
					
					if (($cityState['city'] != "") && ($cityState['region'] != ""))
						echo ', '.$cityState['region'];
					else if ($cityState['region'])
						echo $cityState['region'];
				}
				?>" size="28" />
            <br />
            <br />
			<input name="id" type="hidden" value="<?=$_SESSION['plex_id']?>" />
            <input type="image" name="submit" src="images/search_button.jpg" alt="Submit">
          </form>
          <div align="left" style="padding-left:10px; " ><span class="style11"><a href="advanced_search.php?id=<?=$_SESSION['plex_id']?>">Advanced Search</a> | <a href="browse.php?id=<?=$_SESSION['plex_id']?>">Browse Now</a></span> </div>
        </div>
      </div>
<?php if(($addresses) && ($map_names))  { ?>	  
	  <!-- MAP SHIT -->
      <script>
	  var isMouseOn=0;
	  function showMap()
	  {
	  	 
	  if(document.getElementById('staticMap').style.display=="none")
		  {
		      
			  $("#map_holder").animate({width: "210px",height: "300px"}, 400 );
			  $("#dark_overlay").animate({ width:"0px", height:"0px"}, 0 );
			  document.getElementById('closeButt').style.display="none";
			  document.getElementById('map').style.display="none";
			  setTimeout("hideMap();",400);
		  }
		else
		  {
			  document.getElementById('staticMap').style.display="none";
			  document.getElementById('closeButt').style.display="block";	
			  document.getElementById('map').style.display="block";	
			  $("#map_holder").animate({width: "860px",height: "530px"}, 400 );
			  $("#dark_overlay").animate({ width: document.body.offsetWidth, height: document.body.offsetHeight}, 0 );
			  
		  }
	  }
	  
	  function trickster()
	  {
	          document.getElementById('closeButt').style.display="block";	
			  document.getElementById('map').style.display="block";		  
			  $("#map_holder").animate({width: "860px",height: "530px"}, 400 );
			  $("#dark_overlay").animate({ width: document.body.offsetWidth, height: document.body.offsetHeight}, 0 );
			  load();
	  }
	  
	  function hideMap()
	  {
			  document.getElementById('staticMap').style.display="block";
	  
	  }
	  function doResize()
	  {
	  //alert('1 second');
	  }
	  
	  </script>

      
			  <div id="wtf">
				
				<div id="map_holder">
				
				<div id="closeButt" onclick="showMap();" value="X" style="display:none;position:absolute;float:right; z-index:990; width:40px; font-size:20pt; background:#000;filter: alpha(opacity=70);-moz-opacity: 0.7;color:#AAA; height:40px; right:0px; top:0px; border:solid 1px #666;"><center>X</center></div>
                
               <!-- <img src="http://maps.google.com/staticmap?center=30.533285,-97.719269&zoom=8&size=210x300&maptype=roadmap&markers=30.533285,-97.719269,blued%7C30.533285,-97.719269,greeng%7C40.718217,-73.998284,redc&key=ABQIAAAAy-r1CaE6GQ0PT8bJtxJk8hQBwVSsGjV4Kb2VNM4d2d_Qe8idNhRZs1m1h7o6A4uBdr48ENclNJsgxQ&sensor=false" id="staticMap"  onclick="showMap()" />-->
               <div id="staticMap" style="width:210px; height:300px;" onclick="showMap()" ></div>
                
				<div id="map" style="width:860px; height:530px;"   >
				</div></div></div>
	<!-- END MAP -->
<?php } ?>
    </div>
    <br style="clear:left;" />
    <br style="clear:both;" />
    <br />
  </div>
  <div id="footer">
    <div class="footer-info">
      <p class="style10">Copyright &copy; 2009
        <script language="JavaScript" type="text/javascript">
var d=new Date(); 
yr=d.getFullYear();
if (yr!=2003)
document.write("- "+yr);
            </script>
          <a href="http://!ID!.6qubedirectory.com">6Qube Directory</a> | Designed by <a href="http://bluejagweb.com" target="_blank">BlueJag Web</a> | Search for Businesses Locally at <a href="http://6qube.com">6Qube.com</a></p>
      <p class="style10">&nbsp;</p>
      <p class="style10">&nbsp;</p>
    </div>
</div>
</div>
<div id="dark_overlay"></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9757019-6");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>