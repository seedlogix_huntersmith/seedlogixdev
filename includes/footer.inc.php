	</div><!-- /content -->
</div><!-- /wrapper -->
</div><!-- /brown -->

<div id="footer">
<div class="wrapper">
	<div class="one_col">
		<h4>6qube Local Yellow Pages</h4>
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="http://listings.6qube.com">Free Business Listing</a></li>
		  <li><a href="http://6qube.com">Advertise With Us</a></li>
		  <li><a href="http://6qube.com/local-internet-marketing-blog/">Blog</a></li>
			<li><a href="http://6qube.com/contact-6qube-local-internet-advertising-company/">Contact</a></li>
		</ul>
	</div>
	<div class="one_col">
		<h4>6Qube Network</h4>
		<ul>
			<li><a href="http://local.6qube.com">Directory</a></li>
			<li><a href="http://hubs.6qube.com">HUB</a></li>
			<li><a href="http://press.6qube.com">Press</a></li>
			<li><a href="http://articles.6qube.com">Articles</a></li>
			<li><a href="http://blogs.6qube.com">Blogs</a></li>
		</ul>
	</div>
	<div class="two_col">
		<h4>Why 6Qube for your Local Yellow Pages Directory?</h4>
	  <p>6Qube is a Local Yellow Pages Directory that is a combination of Local Businesses, Local Press, Local Blogs and Local Websites all in one place.  This allows our visitors to learn more than just the traditional information other Yellow Pages offer about the businesses they want. </p>
	  <!-- /newsletter -->
	</div>
	<!-- /two_col -->
	<br class="clear" /><br />
</div><!-- /wrapper -->
</div><!-- /footer -->
<!-- 6qube logo -->
<div id="cube"></div>
<!-- Login Box -->
<div id="login_box">
<form action="" method="post" name="login">
<input id="username" type="text" name="username" />
<input id="password" type="password" name="password" />
<input name="go" type="submit" value="" />
</form>
</div>
<!-- Network Links
<div id="network">
	<ul>
		<li><a href="#hub" class="hub"><span>6Qube Hub</span></a></li>
		<li><a href="#directory" class="directory"><span>6Qube Directory</span></a></li>
		<li><a href="#press" class="press"><span>6Qube Press</span></a></li>
		<li><a href="#blog" class="blog"><span>6Qube Blog</span></a></li>
		<li><a href="#articles" class="articles"><span>6Qube Articles</span></a></li>
	</ul>
</div>
 -->
 <script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://6qube.com/analytics/" : "http://6qube.com/analytics/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://6qube.com/analytics/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript>
</body>
</html>