<?php

require_once(dirname(__FILE__) . '/../admin/6qube/core.php');
?><!--Start Footer-->
<div id="footer">
	<!--Start Grey Footer Bg-->
	<div id="grey_footer_bg">
		<!--Start Inner Div-->
		<div id="inner_div">
			<!--Start Footer Box-->
			<div class="footer_box float_left">
				<h2>Local Internet Marketing Software</h2>	
				<div id="resources-listing">
					<ul>
						<li><a href="http://elements.6qube.com/" title="Internet Marketing Software | Elements by 6Qube">Internet Marketing Software</a></li>
						<li><a href="http://elements.6qube.com/internet-marketing-software-about/" title="About Internet Marketing Software Elements by 6Qube">About Elements by 6Qube</a></li>
						<li><a href="http://elements.6qube.com/internet-marketing-software-features/" title="Internet Marketing Software Features | Elements Internet Marketing Tools">Elements Features</a></li>
						<li><a href="http://elements.6qube.com/internet-marketing-tools-signup/" title="Elements Internet Marketing Software Sign-Up">Elements Sign-Up</a></li>
						<li><a href="http://elementsblog.6qube.com/" title="Internet Marketing Software Blog | Internet Marketing Tools">Elements Blog</a></li>
						<li><a href="http://elements.6qube.com/internet-marketing-software-contact/" title="Contact Internet Marketing Software Elements by 6Qube">Contact Support</a></li>
						<li><a href="http://login.6qube.com/" title="Elements Login by 6Qube">Elements Login</a></li>
						<li><a href="http://6qube.com/" title="Local Internet Marketing by 6Qube | Small Business Local Advertising">Local Internet Marketing</a></li>
						<li><a title="6Qube Directory | Free Business Listing | Free Business Directory | Free Directory Listing" href="http://6qubedirectory.com">Free Business Listing</a></li>
					</ul>
				</div><!--End Resources Listing-->
			</div>
			<!--End Footer Box-->
			<!--Start Contact Info-->
			<div id="contact_info">
				<h2>6Qube Network</h2>
				<div id="resources-listing">
					<ul>
						<li><a href="http://local.6qube.com/" title="Local Yellow Pages Directory by 6Qube Local"><span>6Qube Local</span></a></li>
						<li><a href="http://contractor.6qube.com/" title="Contractor Marketing | Contractor Advertising"><span>6Qube Contractor</span></a></li>
						<li><a href="http://browse.6qube.com/" title="Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube"><span>6Qube Browse</span></a></li>
						<li><a href="http://hubs.6qube.com/" title="Local Website Directory by 6Qube Local | Local Websites"><span>6Qube HUBs</span></a></li>
						<li><a href="http://press.6qube.com/" title="Local Press Directory by 6Qube Local | Local Press Releases"><span>6Qube Press</span></a></li>
						<li><a href="http://articles.6qube.com/" title="Local Articles Directory by 6Qube Local | Local Articles"><span>6Qube Articles</span></a></li>
						<li><a href="http://blogs.6qube.com/" title="Local Blog Directory by 6Qube Local | Local Blogs"><span>6Qube Blogs</span></a></li>
					</ul>
				</div><br />
				<!--End Resources Listing-->
				
				<h2>Follow Us</h2>	
				<div id="social-media">
					<ul>
						<li><a href="http://twitter.com/6Qube" class="follow_icon" title="Twitter"><img src="https://6qube.com/local/images/twitter-icon.png" alt="Twitter" /></a></li>
						<li><a href="http://www.facebook.com/6qube" class="follow_icon" title="Facebook"><img src="https://6qube.com/local/images/facebook-icon.png" alt="Facebook" /></a></li>
						<li><a href="http://www.youtube.com/user/6qube" class="follow_icon" title="YouTube"><img src="https://6qube.com/local/images/youtube-icon.png" alt="YouTube" /></a></li>
						<li><a href="http://www.linkedin.com/companies/6qube?trk=fc_badge" class="follow_icon" title="Linked In"><img src="https://6qube.com/local/images/linked-in-icon.png" alt="Linked In" /></a></li>
						<li><a href="http://www.vimeo.com/sixqube" class="follow_icon" title="Vimeo"><img src="https://6qube.com/local/images/vimeo-icon.png" alt="Vimeo" /></a></li>
						<li><a href="http://delicious.com/6qube" class="follow_icon" title="Delicious"><img src="https://6qube.com/local/images/delicious-icon.png" alt="Delicious" /></a></li>
					</ul>
				</div><!--End Social Media-->										
			</div><!--End Contact Info-->
			<!--Start Latest from Blog-->
			<div id="latest_from_blog">
				<h2>Latest from the Blog</h2>	
				<?
					require_once(QUBEADMIN . 'inc/local.class.php');
					$camp = new Local();
					
					$elements_results = $camp->getElementsBlogPost(2);
					if(!$elements_results){
						echo '<p>Check back soon for New post from the Elements Blog!</p>';
					} else {
						while($elements_post = $camp->fetchArray($elements_results)){
							echo '
								<div class="blog">
									<div>
										<a href="http://elementsblog.6qube.com/'.$camp->convertKeywordUrl($elements_post['category']).$camp->convertPressUrl($elements_post['post_title'], $elements_post['id']).'" class="blog_title">'.$elements_post['post_title'].'</a>
										<p>'.$camp->shortenSummary($elements_post['post_content'],150).'</p>
										<a href="http://elementsblog.6qube.com/'.$camp->convertKeywordUrl($elements_post['category']).''.$camp->convertPressUrl($elements_post['post_title'], $elements_post['id']).'" class="read_full_article">read full article...</a>					
									</div><br style="clear:both" />
								</div>
								<div class="seperator_2"></div>';
						}
					}
				?>
			</div><br style="clear:both;" />
			<!--End Latest from Blog-->
		</div>
		<!--End Inner Div-->
	</div>
	<!--End Grey Footer Bg-->
	<!--Start Green Footer Bg-->
	<div id="green_footer_bg">
		<!--Start Inner Green Bg-->
		<div id="inner_green_bg">
			<div id="go_to_top_div">
				<a href="#top" id="go_to_top_arrow" class="scroll" title="Click to Go Top"><img src="https://6qube.com/images/go-to-top-arrow.png" alt="" /></a>
			</div>
			<div id="footer-links">
				<ul>
					<li><a href="http://elements.6qube.com/" title="Internet Marketing Software | Elements by 6Qube" class="firstlink">Home</a></li>
					<li><a href="http://elements.6qube.com/internet-marketing-software-about/" title="About Internet Marketing Software Elements by 6Qube" >About Us</a></li>
					<li><a href="http://elements.6qube.com/internet-marketing-software-features/" title="Internet Marketing Software Features | Elements Internet Marketing Tools">App Features</a></li>
					<li><a href="http://elements.6qube.com/internet-marketing-tools-signup/" title="Elements Internet Marketing Software Sign-Up">Sign Up</a></li>
					<li><a href="http://elementsblog.6qube.com/" title="Internet Marketing Software Blog | Internet Marketing Tools">Blog</a></li>
					<li><a href="http://elements.6qube.com/internet-marketing-software-contact/" title="Contact Internet Marketing Software Elements by 6Qube">Contact Us</a></li>
					<li class="lastlink"><a href="http://login.6qube.com/" title="Elements Login by 6Qube" >Login</a></li>
				</ul>				
			</div><!--End Footer Links-->
			<span>Copyright &copy; <?=date('Y')?> Elements by 6Qube, All Rights Reserved.</span>
		</div>
		<!--End Inner Green Bg-->
	</div>
	<!--End Green Footer Bg-->
</div>
<!--End Footer-->
</div>
<!--End Main Div_2-->
</div>
<!--End Inner Main Div-->
<div id="cube"></div>