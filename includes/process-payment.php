<?php
session_start();
require_once(dirname(__FILE__) . '/../admin/6qube/core.php');
//This script is used by 6qube.com/process.php to create a new user's CIM profile and payment profile, 
//then charge them and begin recurring billing

//make sure this script is pulled as an include and by the same person
if($sessID = session_id()){
	require_once(QUBEADMIN . 'inc/Authnet.class.php');
	require_once QUBEADMIN . 'inc/reseller.class.php';
	$reseller = new Reseller();
	$anet = new Authnet();
	
	//first attempt to set up CIM profile
	$newProfile = $anet->setupCIMProfile($uid, NULL, NULL, NULL, 1);
	if($newProfile['success']){
		$profile_id = $newProfile['profile_id'];
		//attempt payment profile setup
		$params = array('firstname' => $firstName,
					 'lastname' => $lastName,
					 'company' => $company,
					 'address' => $address,
					 'city' => $city,
					 'state' => $state,
					 'zip' => $zip,
					 'phone' => $phone,
					 'cardnumber' => $cardNumber,
					 'expmonth' => $expMonth,
					 'expyear' => $expYear);
		$newPaymentProfile = $anet->setupPaymentProfile($uid, $profile_id, $params, NULL, NULL, 1);
		if($newPaymentProfile['success']){
			$continue = 0;
			if($setupCharge && $setupCharge > 0){
				//if setup charges, attempt now
				$payment1 = $anet->chargeCIM($uid, NULL, $productID, $setupCharge, 'initialSetupFees', 0);
				if($payment1['success']){
					//setup charges success
					$continue = $setupFeesSuccess = 1;
					$transaction_id = $payment1['message'];
					//log transaction
					$note = "Initial setup fee";
					if($addSite) $note .= ' and custom site';
					$query = "INSERT INTO sixqube_billing 
							(user_id, product_id, transaction_type, transaction_id, amount, 
							 method_identifier, success, note, timestamp)
							VALUES
							('".$uid."', '".$productID."', 'initialSetupFees','".$transaction_id."', '".$setupCharge."', 
							'".$payment1['profile_id']."', 1, '".$note."', now())";
					$reseller->query($query);
				}
				else {
					//setup charges failure
					$continue = 0;
					$errors = 
					"Sorry, but there was an error processing the setup fees with the card provided:<br />".
					$payment1['message'];
				}
			} //end if($setupCharge && $setupCharge > 0)
			else $continue = 1;
			
			if($continue){
				//attempt to charge for first month
				$logNote = 'Monthly billing for '.$productName.' - first month';
				$payment2 = $anet->chargeCIM($uid, NULL, $productID, $monthlyCost, 'initialSubscription', 0);
				if($payment2['success']){
					//first month success, log transaction
					$query = "INSERT INTO sixqube_billing 
							(user_id, product_id, transaction_type, transaction_id, amount, note)
							VALUES
							('".$uid."', '".$productID."', 'initialSubscription', '".$payment2['message']."', 
							 '".$monthlyCost."', 'Initial monthly subscription')";
					$reseller->query($query);
					//set up recurring
					$nextBillDate = $reseller->get_x_months_to_the_future(NULL, 1);
					$query = "INSERT INTO sixqube_billing 
							(user_id, product_id, transaction_type, amount, note, sched, sched_date, sched_reattempt)
							VALUES
							('".$uid."', '".$productID."', 'monthlySubscription', '".$monthlyCost."', 
							'Scheduled monthly billing for ".date('F', strtotime($nextBillDate))."', 
							1, '".$nextBillDate."', 2)";
					if(!$reseller->query($query)) $schedErr = true;
					
					//update user stuff
					//create folders
					$reseller->createUserFolders($uid);
					//update user row
					$query = "UPDATE users SET access = 1 WHERE id = '".$uid."'";
					$reseller->query($query);
					//create first campaign
					$reseller->addNewCampaign($uid, $company, NULL, true);
					//display confirmation
					$paymentSuccess = 1;
					
					////////////////////////////////////////////////////
					// SEND USER WELCOME EMAIL					//
					////////////////////////////////////////////////////
					$to = $email;
					$from = "6Qube <no-reply@6qube.com>"; 
					$subject = "Welcome to Elements by 6Qube!";
					$message = 
					"<html><body>
					<h1>Welcome to Elements by 6Qube, ".$firstName."!</h1>
					<p>
					Congratulations on taking the next step to making your business more visible online and increasing your amount of internet-based sales and clients.<br />
					This email is the receipt for your initial payment and also the confirmation that your new account is active
					</p>
					<h2>Payment information</h2>";
					if($setupCharge){
						$message .= 
						'<p>You paid $'.$totalDue.' today, which included:<br />
						&bull; $'.$monthlyCost.' for the first month of your service<br />
						&bull; $'.$setupCharge.' as a one-time setup fee';
						if($addSite){
							$message .= ' (including $'.$addSiteCost.' for us to build you a custom website and blog)';
						}
						$message .= '<br />';
					}
					else {
						$message .=
						'<p>You paid $'.$monthlyCost.' today for the first month of your service.<br />';
					}
					$message .= 'Please note that this is a monthly service.  Our secure third-party payment processor has your payment information on file and will automatically bill you on this day every month for $'.$monthlyCost.'.</p>
					<p>We appreciate your business and look forward to helping you exceed your online marketing goals!  If you ever need assistance or have any questions feel free to email us at <a href="mailto:support@6qube.com">support@6qube.com</a> or give us a call at (877) 570-5005.</p>
					Sincerely,<br />
					-The Elements by 6Qube Team
					<br /><br />
					<p>
						Elements by 6Qube, a BlueJag Enterprises, LLC. Company
						<br />
						13740 Research Blvd., Suite D4 <br />
						Austin, TX 78750 <br />
						Toll Free Number: 877-570-5005
					</p>
					</body></html>';
					$reseller->sendMail($to, $subject, $message, NULL, $from, NULL, TRUE);
					////////////////////////////////////////////////////
					// SEND 6QUBE NOTIFICATION					//
					////////////////////////////////////////////////////
					$to = 'admin@6qube.com';
					$from = 'no-reply@6qube.com';
					$subject = 'New user signup from website - ID #'.$uid;
					$message = 
					'<h1>New User Notification</h1>
					<p>
						A new paid user has signed up from the website.  Below is detailed information:<br />
						<ul>
						<li>User ID: '.$uid.'</li>
						<li>User Product Name/ID: '.$productName.' / '.$productID.'</li>
						<li>Name: '.$firstName.' '.$lastName.'</li>
						<li>Company Name: '.$company.'</li>
						<br />
						<li>Add Site Option?: ';
					if($addSite) $message .= 'Yes'; else $message .= 'No';
					$message .= '</li>';
					if($setupCharge){
						$message .= '<li>Setup fee success?: ';
						if($setupFeesSuccess) $message .= 'Yes'; else $message .= '<strong>No</strong>';
						$message .= '</li>';
					}
					$message .= '<li>Recurring setup success?: ';
					if(!$schedErr) $message .= 'Yes'; else $message .= '<strong>No</strong>';
					$message .= '</li>
						</ul>
					</p>';
					$reseller->sendMail($to, $subject, $message, NULL, $from, NULL, TRUE);
				} //end if($payment2['success'])
				else {
					//first month failure, notify user
					$errors = "Sorry, an error occured while attempting to bill you for the first month (9994):<br />";
					$errors .= $payment2['message'];
				}
			} //end if($continue)
		} //end if($newPaymentProfile['success'])
		else {
			//payment profile setup failure
			$errors = "Sorry, an error occured (9993):<br />";
			$errors .= $newPaymentProfile['message'];
		}
	} //end if($newProfile['success'])
	else {
		//CIM profile failure
		$errors = "Sorry, an error occured (9993):<br />";
		$errors .= $newProfile['message'];
	}
}
?>