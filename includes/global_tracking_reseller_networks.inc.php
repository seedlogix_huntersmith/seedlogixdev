<?php

        if(PIWIK_VERSION):
                $piwik_url      =       'analytics.6qube.com';
?>
<script type="text/javascript">
    var _paq = _paq || [];
    (function(){ var u=(("https:" == document.location.protocol) ? "https://<?php echo $piwik_url; ?>/" : "http://<?php echo $piwik_url; ?>/");
    _paq.push(['setSiteId', <?php echo $tracking; ?>]);
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.defer=true; g.async=true; g.src=u+'piwik.js';
    s.parentNode.insertBefore(g,s); })();
</script><noscript><img src="http://analytics.6qube.com/piwik.php?idsite=<?php
        echo $row['tracking_id']; ?>&rec=1" style="border:0" alt="" /></noscript>

<?php

                return;
        endif;
?>

<!-- Analytics -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://analytics.6qube.com/" : "http://analytics.6qube.com/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", <?=$tracking?>);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://analytics.6qube.com/piwik.php?idsite=<?=$tracking?>" style="border:0" alt=""/></p></noscript>
<!-- end -->
