<!--Start Container_3-->
			<div id="container_new">
				<!--Start Container_3 Top-->
				<div id="container_new_top">
					<!--Start Container_3 Bottom-->
					<div id="container_new_bottom">
						<div class="grid">
				<aside>
				<ul>
					<li>SEO Campaigns</li>
					<li>SEO Websites</li>
					<li>SEO Web Pages</li>
					<li>Replicated Websites</li>
					<li>SEO Landing Pages</li>
					<li>Blogs/Micro Blogs</li>
					<li>Social Media Websites</li>
					<li>Content Management System</li>
					<li>Prospect Management</li>
					<li>Email Marketing System</li>
					<li>Emails Per Month</li>
					<li>Autoresponder Messages</li>
					<li>Advanced Web Analytics</li>
					<li>SEO Local Listings</li>
					<li>SEO Press Releases</li>
					<li>SEO Articles</li>
					<li>*Exclusive/Website Import</li>
					<li>Training & Support</li>
				</ul>
				</aside>
				<div class="sections col5">
				<section>
					<header>
						<h1>Free</h1>
						<h2>FREE<span>Yes, free.</span>
                        </h2>
                        
                        
					</header>
					<footer>
						<ul>
							<li>
								<span>1</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Campaigns</h3>
										<p>Campaigns allow users to strategically develop internet marketing touch points and manage those touch points in a single place.  Manage websites, microsites, landing pages, blogs, social networks, prospects, email marketing and much, much more. </p>
									</div>
								</div>	
							</li>
					  <li>
								<span>1 (Limited)</span>
                                <div class="tooltip">
									<div>
										<h3>Free Website</h3>
										<p>The free platform does allow users to create a website, however this website has a powered by link at the bottom of the website.  Also users only have access to one theme for their free website. </p>
									</div>
								</div>
							</li>
					  <li>
								<span><img src="http://6qube.com/versions/bluejag/images/i_no.png" alt="&radic;" />	</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web Pages</h3>
										<p>On the free platform users cannot create pages on the free website theme they have access to.  At any point users can upgrade and begin building pages and receive access to the full theme library. </p>
									</div>
								</div>
							</li>
					  <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_no.png" alt="X" /></span>
                                <div class="tooltip">
									<div>
										<h3>Replicated Websites</h3>
										<p>Our replication engine allow our users to replicate websites for specific service areas, products, or solutions.  By clicking the replication button you are able to quickly launch a website for a specific service area or location allowing you to focus on customizing only the elements that need to be changed, such as location and search optimization.  </p>
									</div>
								</div>
							</li>
					  <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_no.png" alt="X" /></span>
                                <div class="tooltip">
									<div>
										<h3>SEO Landing Pages</h3>
										<p>SEO Landing Pages are an excellent way to generate relevant content for specific internet marketing around a target keyword. Competition is only increasing so you need have a system designed and setup to maximize on not only lead generation, but content relevancy.   </p>
									</div>
								</div>
							</li>
					  <li>
								<span >1 (Limited)</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Blog</h3>
										<p>On the free platform you can leverage our blogging platform and create a search engine optimized blog for your business or personal use, however you will have a powered by link at the bottom of your blog and will only have access to 1 theme available.  At any point you can upgrade to access to all available blog themes.  </p>
									</div>
								</div>
														</li>
								<li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_no.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Social Media Websites</h3>
										<p>Social Media Websites, such as Facebook iFrame App Websites that require “like” status, is a great way to convert prospects on social media websites.<br /> <br />Quickly create applications on Facebook for promotions, coupons, downloads, landing pages and much more! </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Content Management System</h3>
										<p>Our Advanced Content Management System provides full control of your entire internet marketing platform.  Updates and additions can be made quickly and easily without any technical knowledge.   </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Prospect Management</h3>
										<p>Managing your prospects is the key to conversion.  Our internet marketing software provides our users with a simple and easy interface to communicate with your prospects increasing your chances of conversions.</p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Email Marketing System</h3>
										<p>Email marketing is very important element to your internet marketing strategy.  Most leads generated online are never followed up with or marketed too.  Our email marketing systems allows users to send broadcast emails for upcoming events, promotions or special offers. The email themes that are available will make your business look highly professional.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >500</span>	
								 <div class="tooltip">
									<div>
										<h3>Monthly Email Limit</h3>
										<p>On our free platform users can send out up to 500 free emails a month to all prospects generated within their system.  Email counts include broadcast emails and autoresponder emails.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span >5</span>	
								 <div class="tooltip">
									<div>
										<h3>Autoresponder System</h3>
										<p>The free platform is limited to only 5 autoresponder messages.  Autoresponder messages enable users to automate follow-up with prospects and also provide lead nurturing touch points essential to maximizing conversions.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Advanced Web Analytics</h3>
										<p>The analytics component offer an advanced look at how your prospects are finding you. Stats ranging from backlinks, links clicked, conversion rates, referral links and much, much more.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >Up to 3</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Local Listings</h3>
										<p>SEO Local Listings provide high quality citations that are essential in your local only success.  Our internet marketing software is attached to a well established network that is designed to benefit your website greatly.    </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Up to 3</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Press Releases</h3>
										<p>Press Releases offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized press releases designed to help your website rank better in search engines.       </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Up to 3</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Articles</h3>
										<p>Articles offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized articles designed to help your website rank better in search engines.</p>
									</div>
								</div>
														</li>
														 
								<li>
								<span >Not Available</span>														</li>
								<li>
									<span >(Limited)</span>
									<div class="tooltip"><div>
										<h3>Internet Marketing Training & Support</h3>
										<p>Our free platform users do have access to a repository of knowledge to help them maximize our internet marketing software.  Through weekly webinars, tutorials, and free trainings, as a free user you will feel confident you will not be left in the dark.</p>
									</div></div>
								</li>
								<li class="last">
									<a class="btn" title="Internet Marketing Software" href="http://elements.6qube.com/create-account/elements-free/178/"><span>Choose Plan</span></a>
								</li>
					  </ul>
					</footer>
				</section>
				<section>
					<header>
						<h1>Professional</h1>
						<h2>$50<span>Monthly</span></h2>
					</header>
					<footer>
						<ul>
							<li>
								<span>3</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Campaigns</h3>
										<p>Campaigns allow users to strategically develop internet marketing touch points and manage those touch points in a single place.  Manage websites, microsites, landing pages, blogs, social networks, prospects, email marketing and much, much more. </p>
									</div>
								</div>	
							</li>
					  <li>
								<span class="small" >1 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web 2.0 Website</h3>
										<p>Build a beautiful Web 2.0 website leveraing our theme engine of websites that have been tested based on layout to convert more prospects.  Each website have an easy to use SEO Engine that will optimize your website on the technical level by simply entering your target keywords.  From alt tags, title tags, anchor tags, folder based optimization and more can be accomplished in a matter of minutes with our internet marketing software.</p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web Pages</h3>
										<p>Each page built through our website builder engine has advanced settings giving our users full control not only of the on-page optimization, but also layout, design and look of each page.  Users can create unlimited pages within our application. </p>
									</div>
								</div>
							</li>
					  <li>
								<span>2 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>Replicated Websites</h3>
										<p>Our replication engine allow our users to replicate websites for specific service areas, products, or solutions.  By clicking the replication button you are able to quickly launch a website for a specific service area or location allowing you to focus on customizing only the elements that need to be changed, such as location and search optimization.  </p>
									</div>
								</div>
							</li>
					  <li>
								<span>10 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Landing Pages</h3>
										<p>SEO Landing Pages are an excellent way to generate relevant content for specific internet marketing around a target keyword. Competition is only increasing so you need have a system designed and setup to maximize on not only lead generation, but content relevancy.   </p>
									</div>
								</div>
							</li>
					  <li>
								<span >3 Per Campaign</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Blog</h3>
										<p>Users have the ability to build not only a blog for their master website, but individual micro blogs for each landing page or replicated website created.  Our blogging platform makes it simple to be up and running with an advanced blog that loads fast and ranks better.</p>
									</div>
								</div>
														</li>
								<li>
								<span >1 Per Campaign</span>
								 <div class="tooltip">
									<div>
										<h3>Social Media Websites</h3>
										<p>Social Media Websites, such as Facebook iFrame App Websites that require “like” status, is a great way to convert prospects on social media websites.<br /> <br />Quickly create applications on Facebook for promotions, coupons, downloads, landing pages and much more! </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Content Management System</h3>
										<p>Our Advanced Content Management System provides full control of your entire internet marketing platform.  Updates and additions can be made quickly and easily without any technical knowledge.   </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Prospect Management</h3>
										<p>Managing your prospects is the key to conversion.  Our internet marketing software provides our users with a simple and easy interface to communicate with your prospects increasing your chances of conversions.</p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Email Marketing System</h3>
										<p>Email marketing is very important element to your internet marketing strategy.  Most leads generated online are never followed up with or marketed too.  Our email marketing systems allows users to send broadcast emails for upcoming events, promotions or special offers. The email themes that are available will make your business look highly professional.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >12,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Monthly Email Limit</h3>
										<p>On our professional platform users can send out up to 12,000 emails a month to all prospects generated within their system.  Email counts include broadcast emails and autoresponder emails.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>Autoresponder System</h3>
										<p>The free platform is limited to only 5 autoresponder messages.  Autoresponder messages enable users to automate follow-up with prospects and also provide lead nurturing touch points essential to maximizing conversions.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Advanced Web Analytics</h3>
										<p>The analytics component offer an advanced look at how your prospects are finding you. Stats ranging from backlinks, links clicked, conversion rates, referral links and much, much more.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Local Listings</h3>
										<p>SEO Local Listings provide high quality citations that are essential in your local only success.  Our internet marketing software is attached to a well established network that is designed to benefit your website greatly.    </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Press Releases</h3>
										<p>Press Releases offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized press releases designed to help your website rank better in search engines.       </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Articles</h3>
										<p>Articles offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized articles designed to help your website rank better in search engines.</p>
									</div>
								</div>
														</li>
														 
								<li>
								<span>$1,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Exclusive Theme or Website Import</h3>
										<p>Many users have a website they absolutely love or would like a website that is exclusive in our system.  We can import any type of website into our application or let our team put together an exclusive internet marketing platform for your business. <br /><br />Professional Package includes a website theme & blog theme that can be replicated.</p>
									</div>
								</div>
														</li>
								<li>
									<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Internet Marketing Training & Support</h3>
										<p>Each our internet marketing packages provide an SEO manual, online training, videos, weekly webinars and much more to get you up and running and ranking your business in more searches faster than every before. </p>
									</div>
								</div>
														</li>
														
					  <li class="last">
								<a class="btn" title="Internet Marketing Software" href="http://elements.6qube.com/create-account/elements-professional/179/"><span>Choose Plan</span></a>
						</li>
					  </ul>
					</footer>
				</section>
				<section class="on">
					<header>
						<h1>Advanced</h1>
						<h2>$99<span>Monthly</span></h2>
					</header>
					<footer>
						<ul>
							<li>
								<span>5</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Campaigns</h3>
										<p>Campaigns allow users to strategically develop internet marketing touch points and manage those touch points in a single place.  Manage websites, microsites, landing pages, blogs, social networks, prospects, email marketing and much, much more. </p>
									</div>
								</div>	
							</li>
					  <li>
								<span class="small" >1 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web 2.0 Website</h3>
										<p>Build a beautiful Web 2.0 website leveraing our theme engine of websites that have been tested based on layout to convert more prospects.  Each website have an easy to use SEO Engine that will optimize your website on the technical level by simply entering your target keywords.  From alt tags, title tags, anchor tags, folder based optimization and more can be accomplished in a matter of minutes with our internet marketing software.</p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web Pages</h3>
										<p>Each page built through our website builder engine has advanced settings giving our users full control not only of the on-page optimization, but also layout, design and look of each page.  Users can create unlimited pages within our application. </p>
									</div>
								</div>
							</li>
					  <li>
								<span>4 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>Replicated Websites</h3>
										<p>Our replication engine allow our users to replicate websites for specific service areas, products, or solutions.  By clicking the replication button you are able to quickly launch a website for a specific service area or location allowing you to focus on customizing only the elements that need to be changed, such as location and search optimization.  </p>
									</div>
								</div>
							</li>
					  <li>
								<span>10 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Landing Pages</h3>
										<p>SEO Landing Pages are an excellent way to generate relevant content for specific internet marketing around a target keyword. Competition is only increasing so you need have a system designed and setup to maximize on not only lead generation, but content relevancy.   </p>
									</div>
								</div>
							</li>
					  <li>
								<span >5 Per Campaign</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Blog</h3>
										<p>Users have the ability to build not only a blog for their master website, but individual micro blogs for each landing page or replicated website created.  Our blogging platform makes it simple to be up and running with an advanced blog that loads fast and ranks better.</p>
									</div>
								</div>
														</li>
								<li>
								<span class="x">3 Per Campaign</span>	
								 <div class="tooltip">
									<div>
										<h3>Social Media Websites</h3>
										<p>Social Media Websites, such as Facebook iFrame App Websites that require “like” status, is a great way to convert prospects on social media websites.<br /> <br />Quickly create applications on Facebook for promotions, coupons, downloads, landing pages and much more! </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Content Management System</h3>
										<p>Our Advanced Content Management System provides full control of your entire internet marketing platform.  Updates and additions can be made quickly and easily without any technical knowledge.   </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Prospect Management</h3>
										<p>Managing your prospects is the key to conversion.  Our internet marketing software provides our users with a simple and easy interface to communicate with your prospects increasing your chances of conversions.</p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Email Marketing System</h3>
										<p>Email marketing is very important element to your internet marketing strategy.  Most leads generated online are never followed up with or marketed too.  Our email marketing systems allows users to send broadcast emails for upcoming events, promotions or special offers. The email themes that are available will make your business look highly professional.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >25,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Monthly Email Limit</h3>
										<p>On our professional platform users can send out up to 12,000 emails a month to all prospects generated within their system.  Email counts include broadcast emails and autoresponder emails.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>Autoresponder System</h3>
										<p>The free platform is limited to only 5 autoresponder messages.  Autoresponder messages enable users to automate follow-up with prospects and also provide lead nurturing touch points essential to maximizing conversions.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Advanced Web Analytics</h3>
										<p>The analytics component offer an advanced look at how your prospects are finding you. Stats ranging from backlinks, links clicked, conversion rates, referral links and much, much more.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Local Listings</h3>
										<p>SEO Local Listings provide high quality citations that are essential in your local only success.  Our internet marketing software is attached to a well established network that is designed to benefit your website greatly.    </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Press Releases</h3>
										<p>Press Releases offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized press releases designed to help your website rank better in search engines.       </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Articles</h3>
										<p>Articles offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized articles designed to help your website rank better in search engines.</p>
									</div>
								</div>
														</li>
														
								<li>
								<span >$1,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Exclusive Theme or Website Import</h3>
										<p>Many users have a website they absolutely love or would like a website that is exclusive in our system.  We can import any type of website into our application or let our team put together an exclusive internet marketing platform for your business. <br /><br />Advanced Package includes a website theme and blog theme that can be replicated.</p>
									</div>
								</div>
														</li>
								<li>
									<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Internet Marketing Training & Support</h3>
										<p>Each our internet marketing packages provide an SEO manual, online training, videos, weekly webinars and much more to get you up and running and ranking your business in more searches faster than every before. </p>
									</div>
								</div>
														</li>
														
					  <li class="last">
								<a class="btn" title="Internet Marketing Software" href="http://elements.6qube.com/create-account/elements-advanced/180/"><span>Choose Plan</span></a>
						</li>
					  </ul>
					</footer>
				</section>
				<section> 
					<header>
						<h1>Corporate</h1>
						<h2>$199<span>Monthly</span></h2>
					</header>
					<footer>
						<ul>
							<li>
								<span>10</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Campaigns</h3>
										<p>Campaigns allow users to strategically develop internet marketing touch points and manage those touch points in a single place.  Manage websites, microsites, landing pages, blogs, social networks, prospects, email marketing and much, much more. </p>
									</div>
								</div>	
							</li>
					  <li>
								<span class="small" >1 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web 2.0 Website</h3>
										<p>Build a beautiful Web 2.0 website leveraing our theme engine of websites that have been tested based on layout to convert more prospects.  Each website have an easy to use SEO Engine that will optimize your website on the technical level by simply entering your target keywords.  From alt tags, title tags, anchor tags, folder based optimization and more can be accomplished in a matter of minutes with our internet marketing software.</p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web Pages</h3>
										<p>Each page built through our website builder engine has advanced settings giving our users full control not only of the on-page optimization, but also layout, design and look of each page.  Users can create unlimited pages within our application. </p>
									</div>
								</div>
							</li>
					  <li>
								<span>9 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>Replicated Websites</h3>
										<p>Our replication engine allow our users to replicate websites for specific service areas, products, or solutions.  By clicking the replication button you are able to quickly launch a website for a specific service area or location allowing you to focus on customizing only the elements that need to be changed, such as location and search optimization.  </p>
									</div>
								</div>
							</li>
					  <li>
								<span>10 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Landing Pages</h3>
										<p>SEO Landing Pages are an excellent way to generate relevant content for specific internet marketing around a target keyword. Competition is only increasing so you need have a system designed and setup to maximize on not only lead generation, but content relevancy.   </p>
									</div>
								</div>
							</li>
					  <li>
								<span >10 Per Campaign</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Blog</h3>
										<p>Users have the ability to build not only a blog for their master website, but individual micro blogs for each landing page or replicated website created.  Our blogging platform makes it simple to be up and running with an advanced blog that loads fast and ranks better.</p>
									</div>
								</div>
														</li>
								<li>
								<span class="x">6 Per Campaign</span>	
								 <div class="tooltip">
									<div>
										<h3>Social Media Websites</h3>
										<p>Social Media Websites, such as Facebook iFrame App Websites that require “like” status, is a great way to convert prospects on social media websites.<br /> <br />Quickly create applications on Facebook for promotions, coupons, downloads, landing pages and much more! </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Content Management System</h3>
										<p>Our Advanced Content Management System provides full control of your entire internet marketing platform.  Updates and additions can be made quickly and easily without any technical knowledge.   </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Prospect Management</h3>
										<p>Managing your prospects is the key to conversion.  Our internet marketing software provides our users with a simple and easy interface to communicate with your prospects increasing your chances of conversions.</p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Email Marketing System</h3>
										<p>Email marketing is very important element to your internet marketing strategy.  Most leads generated online are never followed up with or marketed too.  Our email marketing systems allows users to send broadcast emails for upcoming events, promotions or special offers. The email themes that are available will make your business look highly professional.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >50,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Monthly Email Limit</h3>
										<p>On our professional platform users can send out up to 12,000 emails a month to all prospects generated within their system.  Email counts include broadcast emails and autoresponder emails.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>Autoresponder System</h3>
										<p>The free platform is limited to only 5 autoresponder messages.  Autoresponder messages enable users to automate follow-up with prospects and also provide lead nurturing touch points essential to maximizing conversions.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Advanced Web Analytics</h3>
										<p>The analytics component offer an advanced look at how your prospects are finding you. Stats ranging from backlinks, links clicked, conversion rates, referral links and much, much more.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Local Listings</h3>
										<p>SEO Local Listings provide high quality citations that are essential in your local only success.  Our internet marketing software is attached to a well established network that is designed to benefit your website greatly.    </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Press Releases</h3>
										<p>Press Releases offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized press releases designed to help your website rank better in search engines.       </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Articles</h3>
										<p>Articles offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized articles designed to help your website rank better in search engines.</p>
									</div>
								</div>
														</li>
														 
								<li>
								<span >$1,500</span>	
								 <div class="tooltip">
									<div>
										<h3>Exclusive Theme or Website Import</h3>
										<p>Many users have a website they absolutely love or would like a website that is exclusive in our system.  We can import any type of website into our application or let our team put together an exclusive internet marketing platform for your business.  <br /><br />Corporate Package includes a website theme, blog theme and landing page theme that can be replicated.</p>
									</div>
								</div>
														</li>
								<li>
									<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Internet Marketing Training & Support</h3>
										<p>Each our internet marketing packages provide an SEO manual, online training, videos, weekly webinars and much more to get you up and running and ranking your business in more searches faster than every before. </p>
									</div>
								</div>
														</li>
														
					  <li class="last">
								<a class="btn" title="Internet Marketing Software" href="http://elements.6qube.com/create-account/elements-corporate/181/"><span>Choose Plan</span></a>
						</li>
							</ul>
					</footer> 
				</section> 
				<section>
					<header>
						<h1>Enterprise</h1>
						<h2>$499<span>Monthly</span></h2>
					</header>
					<footer>
						<ul>
							<li>
								<span>25</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Campaigns</h3>
										<p>Campaigns allow users to strategically develop internet marketing touch points and manage those touch points in a single place.  Manage websites, microsites, landing pages, blogs, social networks, prospects, email marketing and much, much more. </p>
									</div>
								</div>	
							</li>
					  <li>
								<span class="small" >1 Per Campaign</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web 2.0 Website</h3>
										<p>Build a beautiful Web 2.0 website leveraing our theme engine of websites that have been tested based on layout to convert more prospects.  Each website have an easy to use SEO Engine that will optimize your website on the technical level by simply entering your target keywords.  From alt tags, title tags, anchor tags, folder based optimization and more can be accomplished in a matter of minutes with our internet marketing software.</p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Web Pages</h3>
										<p>Each page built through our website builder engine has advanced settings giving our users full control not only of the on-page optimization, but also layout, design and look of each page.  Users can create unlimited pages within our application. </p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>Replicated Websites</h3>
										<p>Our replication engine allow our users to replicate websites for specific service areas, products, or solutions.  By clicking the replication button you are able to quickly launch a website for a specific service area or location allowing you to focus on customizing only the elements that need to be changed, such as location and search optimization.  </p>
									</div>
								</div>
							</li>
					  <li>
								<span>Unlimited</span>
                                <div class="tooltip">
									<div>
										<h3>SEO Landing Pages</h3>
										<p>SEO Landing Pages are an excellent way to generate relevant content for specific internet marketing around a target keyword. Competition is only increasing so you need have a system designed and setup to maximize on not only lead generation, but content relevancy.   </p>
									</div>
								</div>
							</li>
					  <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Blog</h3>
										<p>Users have the ability to build not only a blog for their master website, but individual micro blogs for each landing page or replicated website created.  Our blogging platform makes it simple to be up and running with an advanced blog that loads fast and ranks better.</p>
									</div>
								</div>
														</li>
								<li>
								<span class="x">Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>Social Media Websites</h3>
										<p>Social Media Websites, such as Facebook iFrame App Websites that require “like” status, is a great way to convert prospects on social media websites.<br /> <br />Quickly create applications on Facebook for promotions, coupons, downloads, landing pages and much more! </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Content Management System</h3>
										<p>Our Advanced Content Management System provides full control of your entire internet marketing platform.  Updates and additions can be made quickly and easily without any technical knowledge.   </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>	
								 <div class="tooltip">
									<div>
										<h3>Prospect Management</h3>
										<p>Managing your prospects is the key to conversion.  Our internet marketing software provides our users with a simple and easy interface to communicate with your prospects increasing your chances of conversions.</p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Email Marketing System</h3>
										<p>Email marketing is very important element to your internet marketing strategy.  Most leads generated online are never followed up with or marketed too.  Our email marketing systems allows users to send broadcast emails for upcoming events, promotions or special offers. The email themes that are available will make your business look highly professional.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >125,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Monthly Email Limit</h3>
										<p>On our professional platform users can send out up to 12,000 emails a month to all prospects generated within their system.  Email counts include broadcast emails and autoresponder emails.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>Autoresponder System</h3>
										<p>The free platform is limited to only 5 autoresponder messages.  Autoresponder messages enable users to automate follow-up with prospects and also provide lead nurturing touch points essential to maximizing conversions.  </p>
									</div>
								</div>
														</li>
							     <li>
								<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Advanced Web Analytics</h3>
										<p>The analytics component offer an advanced look at how your prospects are finding you. Stats ranging from backlinks, links clicked, conversion rates, referral links and much, much more.</p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Local Listings</h3>
										<p>SEO Local Listings provide high quality citations that are essential in your local only success.  Our internet marketing software is attached to a well established network that is designed to benefit your website greatly.    </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Press Releases</h3>
										<p>Press Releases offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized press releases designed to help your website rank better in search engines.       </p>
									</div>
								</div>
														</li>
							     <li>
								<span >Unlimited</span>	
								 <div class="tooltip">
									<div>
										<h3>SEO Articles</h3>
										<p>Articles offer high quality and relevant backlinks search engines love.  Our internet marketing software provides a platform for delivering search engine optimized articles designed to help your website rank better in search engines.</p>
									</div>
								</div>
														</li>
														 
								<li>
								<span >$3,000</span>	
								 <div class="tooltip">
									<div>
										<h3>Exclusive Theme or Website Import</h3>
										<p>Many users have a website they absolutely love or would like a website that is exclusive in our system.  We can import any type of website into our application or let our team put together an exclusive internet marketing platform for your business. <br /><br />Enterprise Package includes a website theme, blog theme, micro-site theme, landing page theme abd social media website theme that can be replicated.</p>
									</div>
								</div>
														</li>
								<li>
									<span class="x"><img src="http://6qube.com/versions/bluejag/images/i_sticky.png" alt="X" /></span>		
								 <div class="tooltip">
									<div>
										<h3>Internet Marketing Training & Support</h3>
										<p>Each our internet marketing packages provide an SEO manual, online training, videos, weekly webinars and much more to get you up and running and ranking your business in more searches faster than every before. </p>
									</div>
								</div>
														</li>
														
					  <li class="last">
								<a class="btn" title="Internet Marketing Software" href="http://elements.6qube.com/create-account/elements-enterprise/182/"><span>Choose Plan</span></a>
						</li>
					  </ul>
					</footer>
				</section>
				</div>
				
			</div>
						
						<br clear="all" />
					</div>
					<!--End Container_3 Bottom-->
				</div>
				<!--End Container_3 Top-->
				<div class="asterisk">*Optional add-on to each package except the free platform.  If you have any questions please <a href="/internet-marketing-software-contact/">Click Here</a> to contact us.</div>
			</div>
			<!--End Container_3-->
			
<br clear="all" />