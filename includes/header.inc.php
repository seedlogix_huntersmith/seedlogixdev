<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Local Yellow Pages Directory by 6Qube Local</title>
<meta content="Find Local Yellow Pages, Local Press, Local Blogs, Local Websites in one local place. Local Yellow Pages Directory by 6Qube." name="description" />
<!-- Themed CSS -->
<link href="http://6qube.com/css/brown_v2.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<link rel="apple-touch-icon" href="http://www.6qube.com/img/6qube_favicon.png" />
<!-- JS Resources -->
<script language="javascript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/ajax_form.js"></script>
<script language="javascript" type="text/javascript"> 
//this is the code that loads new content for each section
function miniNav($offset,$target_div){
	$("#"+$target_div).load($target_div+".php?limit="+3+"&offset="+$offset);
}
</script> 
<style type="text/css">
<!--
#packages {background-color:#E8F3DE;width:665px; height:450px;border: 1px dotted; border-color: #999;padding:10px 15px 10px 15px; }
#packages h4 { text-align:center; padding-bottom:10px; padding-top:10px;}
#packages ul { list-style:none;padding:5px 10px 10px 35px; background:url(http://6qube.com/img/smallArrow.png) no-repeat top left; }
.resultsBox1 { width:215px; text-align:left; float:left; padding-left:5px; padding-bottom:15px; }
.resultsBox2 { width:215px; text-align:left; background-color:#DCE8ED; float:left; padding-left:5px; padding-bottom:15px; }
.resultsBoxRight { width:215px; float:right; text-align:left; padding-left:5px; padding-bottom:15px; padding-right:0px; }
-->
</style>
</head>
<body class="directory">
<div class="wrapper">
	<div id="header">
		<h1><span>6Qube Local - Local Internet Marketing</span></h1>
        <div id="tagline"><div class="taglineText">Search Better. </div> <div class="taglineNumber">Find Local.</div>
        </div>
	  <!-- <div id="network_tab"><span>Access 6Qube Network</span></div> -->
	</div><!-- /header -->
	<ul id="nav">
		<li><a href="http://local.6qube.com/">Home</a></li>
		<li><a href="#">Browse</a></li>
		<li><a href="http://hubs.6qube.com">HUBs</a></li>
		<li><a href="http://press.6qube.com">Press</a></li>
		<li><a href="http://articles.6qube.com">Articles</a></li>
		<li><a href="http://blogs.6qube.com">Blogs</a></li>
        <li><a href="http://listings.6qube.com">Free Business Listing</a></li>
	</ul>
</div><!-- /wrapper -->
<div class="brown"> 
		<div id="search">
			<form method="get" name="search" action="http://www.6qube.com/search.php">
				<div class="top">
					<label for="business" title="business or category"><img src="http://6qube.com/img/business.png" alt="Business or Category" /></label>
					<label for="location" title="location"><img src="http://6qube.com/img/location.png" alt="Location" /></label>
				</div>
				<div class="bottom">
					<input type="text" name="business" value="<?=$_SESSION['keyword']?>" style="margin-right:35px;" />
					<input type="text" name="location" value="<?=$_SESSION['location']?>" />
					<input type="image" src="http://6qube.com/img/spacer.gif" width="41" height="40" value="" />
				</div>
			</form>
		</div><!-- /search -->
<div class="wrapper_inside">
	<div id="content_inside">
