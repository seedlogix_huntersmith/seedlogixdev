<!--Start Footer-->
<div id="footer">
	<!--Start Grey Footer Bg-->
	<div id="grey_footer_bg">
		<!--Start Inner Div-->
		<div id="inner_div">
			<!--Start Footer Box-->
			<div class="footer_box float_left">
				<h2>6Qube Solutions</h2>	
				<div id="resources-listing">
					<ul>
						  <li><a href="http://6qube.com/solutions/marketing-automation-software/" title="Marketing Automation Software">Marketing Automation Software</a></li>
        <li><a href="http://6qube.com/solutions/inbound-marketing-software/" title="Inbound Marketing Software">Inbound Marketing Software</a></li>
        <li><a href="http://6qube.com/solutions/internet-marketing-services/" title="Internet Marketing Services">Internet Marketing Services</a></li>
        <li><a href="http://6qube.com/solutions/social-media-solutions/" title="Social Media Solutions">Social Media Solutions</a></li>
        <li><a href="http://6qube.com/solutions/multiple-website-management/" title="Multiple Website Management">Multiple Website Management</a></li>
        <li><a href="http://6qube.com/solutions/marketing-service-providers/" title="Marketing Service Providers">Marketing Service Providers</a></li>
					</ul>
				</div><!--End Resources Listing-->
                
                 <h2>Follow Us</h2>	
				<div id="social-media">
					<ul>
						<li><a href="http://twitter.com/6Qube" class="follow_icon" title="Twitter"><img src="http://local.6qube.com/images/twitter-icon.png" alt="Twitter" /></a></li>
						<li><a href="http://www.facebook.com/6qube" class="follow_icon" title="Facebook"><img src="http://local.6qube.com/images/facebook-icon.png" alt="Facebook" /></a></li>
						<li><a href="http://www.youtube.com/user/6qube" class="follow_icon" title="YouTube"><img src="http://local.6qube.com/images/youtube-icon.png" alt="YouTube" /></a></li>
						<li><a href="http://www.linkedin.com/companies/6qube?trk=fc_badge" class="follow_icon" title="Linked In"><img src="http://local.6qube.com/images/linked-in-icon.png" alt="Linked In" /></a></li>
						<li><a href="http://www.vimeo.com/sixqube" class="follow_icon" title="Vimeo"><img src="http://local.6qube.com/images/vimeo-icon.png" alt="Vimeo" /></a></li>
						<li><a href="http://delicious.com/6qube" class="follow_icon" title="Delicious"><img src="http://local.6qube.com/images/delicious-icon.png" alt="Delicious" /></a></li>
					</ul>
				</div><!--End Social Media-->		
               
				
			</div>
			<!--End Footer Box-->
			<!--Start Contact Info-->
			<div id="contact_info">
				 <h2>6Qube Services</h2>
				<div id="resources-listing">
					<ul>
						 <li><a href="http://6qube.com/services/local-internet-marketing/" title="Local Internet Marketing">Local Internet Marketing</a></li>
        <li><a href="http://6qube.com/services/national-internet-marketing/" title="National Internet Marketing">National Internet Marketing</a></li>
        <li><a href="http://6qube.com/services/search-engine-optimization/" title="Search Engine Optimization">Search Engine Optimization</a></li>
        <li><a href="http://6qube.com/services/pay-per-click-management/" title="Pay Per Click Management">Pay Per Click Management</a></li>
        <li><a href="http://6qube.com/services/social-media-marketing/" title="Social Media Marketing">Social Media Marketing</a></li>
        <li><a href="http://6qube.com/services/ecommerce-marketing/" title="eCommerce Marketing">eCommerce Marketing</a></li>
        <li><a href="http://6qube.com/services/seo-website-design/" title="SEO Website Design">SEO Website Design</a></li>
        <li><a href="http://6qube.com/services/seo-website-copy/" title="SEO Website Copy">SEO Website Copy</a></li>
					</ul>
				</div><br />
				<!--End Resources Listing-->
                
               
				
							
			</div><!--End Contact Info-->
			<!--Start Latest from Blog-->
			<div id="latest_from_blog">
				<h2>6Qube Network</h2>
				<div id="resources-listing">
					<ul>
						<li><a href="http://local.6qube.com/" title="Local Yellow Pages Directory by 6Qube Local"><span>6Qube Local</span></a></li>
						<li><a href="http://browse.6qube.com/" title="Browse Local Businesses, Local Websites, Local Blogs, Press & Articles with 6Qube"><span>6Qube Browse</span></a></li>
						<li><a href="http://hubs.6qube.com/" title="Local Website Directory by 6Qube Local | Local Websites"><span>6Qube HUBs</span></a></li>
						<li><a href="http://press.6qube.com/" title="Local Press Directory by 6Qube Local | Local Press Releases"><span>6Qube Press</span></a></li>
						<li><a href="http://articles.6qube.com/" title="Local Articles Directory by 6Qube Local | Local Articles"><span>6Qube Articles</span></a></li>
						<li><a href="http://blogs.6qube.com/" title="Local Blog Directory by 6Qube Local | Local Blogs"><span>6Qube Blogs</span></a></li>
					</ul>
				</div><br />
				<!--End Resources Listing-->
				
				
			</div><br style="clear:both;" />
			<!--End Latest from Blog-->
		</div>
		<!--End Inner Div-->
	</div>
	<!--End Grey Footer Bg-->
	<!--Start Green Footer Bg-->
	<div id="green_footer_bg">
		<!--Start Inner Green Bg-->
		<div id="inner_green_bg">
			<div id="go_to_top_div">
				<a href="#top" id="go_to_top_arrow" class="scroll" title="Click to Go Top"><img src="/images/go-to-top-arrow.png" alt="" /></a>
			</div>
			<div id="footer-links">
				<ul>
					<li><a href="http://6qube.com/solutions/inbound-marketing-software/" title="Internet Marketing Software" class="firstlink">Internet Marketing Software</a></li>
					<li><a href="http://6qubedirectory.com" title="Free Business Listing" >Free Business Listing</a></li>
					<li class="lastlink"><a href="http://login.6qube.com/" title="Login to 6Qube" >Login</a></li>
				</ul>				
			</div><!--End Footer Links-->
			<span>Copyright &copy; <?=date('Y')?> 6Qube, All Rights Reserved.</span>
		</div>
		<!--End Inner Green Bg-->
	</div>
	<!--End Green Footer Bg-->
</div>
<!--End Footer-->
</div>
<!--End Main Div_2-->
</div>
<!--End Inner Main Div-->
<div id="cube"></div>
