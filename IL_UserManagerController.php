<?php

/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */
require_once QUBEADMIN . 'inc/reseller.class.php';
require_once QUBEADMIN . 'inc/user_permissions.class.php';        
require_once QUBEADMIN . 'models/CampaignModel.php';        
require_once QUBEADMIN . 'models/HubModel.php';        
require_once QUBEADMIN . 'models/WebsiteModel.php';        
require_once QUBEADMIN . 'models/LeadModel.php';        
require_once HYBRID_PATH . 'classes/DashboardAnalytics.php';


class UserManagerController extends AdminBaseController
{
    protected $search_q =   NULL;

    // time in seconds
    const UPLOADED_IF_MTIME = 600;

    function viewUserDashboard(UserModel $User)
    {
        // if r->admin_user == $this->user_ID AND requested user is a subuser of the current reseller fetch the user
        //  OR $ID == $this->user_ID
                
//        var_dump($User);
        
        $this->getCrumbsRoot()->addCrumbTxt('UserManager_view?ID=' . $User->getID(), 'Details');
        $V  =   $this->getPage('dashboard-user', 'User Dashboard');
        $V->init('userobj', $User); //        
                
        return $V;
    }
    
    function viewUserDetails(UserModel $User)
    {
//        $AvailableRoles = SystemController::queryRoleObjects($this->q)
//                ->addFields('exists (select role_ID FROM 6q_userroles WHERE user_ID = ' . $User->getID() . ' AND role_ID = T.ID) as checked')
//                ->Exec()->fetchAll();

        $AvailableRoles = UserDataAccess::FromController($this)->getRoles(0, array('reseller_user' => $User->getParentID()));
        $UserRole = $this->q->query('SELECT role_ID FROM 6q_userroles WHERE user_ID = ' . $User->getID() . ' LIMIT 1')->fetch();
        
#        var_dump($AvailableRoles);
        // if r->admin_user == $this->user_ID AND requested user is a subuser of the current reseller fetch the user
        //  OR $ID == $this->user_ID
                
//        var_dump($User);
        
        $this->getCrumbsRoot()->addCrumbTxt('UserManager_view?ID=' . $User->getID(), 'Details');

        $sda= new UserDataAccess();
        $totalLimits = $sda->CountAccountLimits();
        $V  =   $this->getPage('user-details', "User Details");
        $V->init('totalLimits', $totalLimits);
        $V->init('parent_id', $User->getParentID());
        $V->init('subscriptions', $sda->GetAllSubscriptions());

        $W  =   new \SubscriptionsWidget($this, array(
                'serverParams' => array('user_id' => $User->getParentID()),
                'createSubscriptionsButton' => true,
                'changeSubscriptionsButton' => true,
                'removeSubscriptionButton' => true
            )
        );
        $W->appendToView($V, 'users');

		if($this->getUser()->isSystem()){
			$V->init('storagePath', $this->getUser()->getStorageUrl());
            $V->init('user_limit_row', $sda->getLimitRow(array('user_ID' => $User->getID(), 'returnSingleObj' => 'stdClass')));
		}
        $V->init('Roles', $AvailableRoles);
        $V->init('UserRole', $UserRole['role_ID']);
        
        /* Put this (decrypted Banking Account info) in view, but would like to 
         * test here first.
         */
        // Here goes. Decrypt Account Number and only diplay last 4 digits.
        $SECRET_KEY = "SEEDLOGIX-BEAST-01-01-2018";
        $SECRET_IV = "CHILANGO-FRIDAY";

        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', SECRET_KEY);
        $iv = substr(hash('sha256', SECRET_IV), 0, 16);

        // Decrypt the account number.
        $decrypted_account_number = openssl_decrypt($User->baccount_number, $encrypt_method, $key, 0, $iv);

        // Show masked account number.
        $masked_account_number =  str_pad(substr($decrypted_account_number, -4), strlen($decrypted_account_number), '#', STR_PAD_LEFT);
        
        // Update the object property 'baccount_number'
        $User->baccount_number = $masked_account_number;
        
        $V->init('userobj', $User); //
        
        return $V;
    }
        
    function viewUserBillingInfo(UserModel $User)
    {
		if($deny = $this->Protect('view_billing'))
			return $deny;

        $this->getCrumbsRoot()->addCrumbTxt('UserManager_billinginfo?ID=' . $User->getID(), 'Billing Info');
        
        $V  =   $this->getPage('user-details', 'Billing Info');
        $V->init('userobj', $User); //        
        return $V;
        
    }
    
    function viewUserProducts(UserModel $User)
    {
        $this->getCrumbsRoot()->addCrumbTxt('UserManager_products?ID=' . $User->getID(), 'Products');
        $V  =   $this->getPage('dashboard-user', 'Products');
        $V->init('userobj', $User); //        
        return $V;
    }
    
    function viewUserSysteminfo(UserModel $User)
    {
		throw new Exception("???");
		
        $this->getCrumbsRoot()->addCrumbTxt('UserManager_systeminfo?ID=' . $User->getID(), 'System Info');
        $V  =   $this->getPage('user-systeminfo', 'System Info');
        $V->init('userobj', $User); //        
        return $V;
        
    }
    
    function prepareView(\ThemeView &$view) {
        
        $view->init('UserRoles', 
                QubeRoleModel::GetRoles($this->User)
            );
        
        parent::prepareView($view);
    }
    
    function getPage($view, $title, $menu = "usermanager", $active_classes = array(), $classes = array())
    {
        $active_classes[]   =   'admin-nav';
        return parent::getPage($view, $title, $menu, $active_classes, $classes);
    }
        
    
    function viewUserList($status   =   '')
    {
        if($bye   =   $this->Protect('view_users', 'Campaign_dashboard')) return $bye;
        
        // @todo SECURITY AUDIT
        $r  =   $this->ResellerData;
        // if r->admin_user == $this->user_ID AND requested user is a subuser of the current reseller fetch the user
        //  OR $ID == $this->user_ID
        
        $V  =   $this->getPage('users-list', "User List");
        
        $W  =   new UserListWidget($this,
                    array('status' => $status,
                            'roles' => isset($_GET['role']) ? $_GET['role'] : NULL,
                        'serverParams' => array('status' => $status),
                        'limit' => 20));
        $W->appendToView($V, 'users');
        return $V;
    }
    
    function ExecuteAction($action = NULL) {
        $this->setAdminCrumb($this->getCrumbsRoot()->addCrumbTxt('UserManager_list', 'Users'));
            
        
        if(isset($_GET['ID']))
        {
            // @todo SECURITY AUDIT
            $r  =   $this->ResellerData;
            $User   =   UserListWidget::queryUsers($this)->Where('T.id = %d', $_GET['ID'])->Exec()->fetch();
            
            if(!$User)
						{
							throw new QubeException("User Not Found.");
						}
						
            $Page   =   $this->doAction($action, $User);
            return $Page;
        }
//
//		if($deny = $this->Protect('view_admin'))
//			return $deny;
        
        switch ($action){
            case 'save':
                if(isset($_POST['user'])){
                    $userata	=	$_POST['user'];	//array('parent_user' => $this->User);
                    
                    return $this->doAjaxSaveUser($userata);
                }
                break;
                
            case 'ajaxList':
                return $this->ajaxUserList(new VarContainer($_POST), @$_GET['status']);
            case 'list':
                return $this->viewUserList(@$_GET['status']);
        }
        
        return $this->ActionRedirect('UserManager_list');
    }
    
    //Not used
    function doSaveUser(VarContainer $VC, UserModel $user){
        $Validation =   new ValidationStack;
        
//        UserModel::Validate($VC, $Validation);
                
        
        if($Validation->hasErrors())
            return $Validation;

        $update_user_ID = $user->getID();  
        $is_update    =   $user->getID();
        $update_where   =   false;  // not update
        
        if($is_update && !empty($update_user_ID)){
            
            $is_self_update =   $update_user_ID   ==  $this->user_ID;

            if($is_self_update || $user->parent_id == $this->user_ID || $this->User->can('edit_user', $update_user_ID)){
                // a user is being 'updated' and its ok to edit them.
                $update_where   = DBWhereExpression::Create('id = %d', $update_user_ID);
            }  else {
                // @todo log abuse
                $Validation->addError('permission', 'Permission Denied');
                return $Validation;
            }
        }else{
            // is not update
            if($this->User->can('is_reseller') || $this->User->can('create_users'))
                $VC->parent_id = $this->user_ID;
            else {  // permission denied
                // @todo log abuse
                return $Validation->addError('permission', 'Permission Denied');
//                return $this->returnAjaxError('Permission denied.');
            }
        }
        $result    =   $this->q->Save($user, $VC->getData(), $update_where);
        if($result instanceof HybridModel) return true;
		
		return $Validation->addError('database', 'Database error Ocurred.');
    }
    
    function doAjaxSaveUser(array $data){
        if(!$this->getUser()->can('edit_users', $data['ID']))
            return $this->Protect('is_admin');

		$uda	= UserDataAccess::FromController($this);
		
		$ID	=	isset($data['ID']) ? $data['ID'] : 0;

//        if(isset($data['password'])){
//            $ID	= isset($_GET['ID']) ? $_GET['ID'] : $ID;
//        }
//		
		$SDA	=	new SystemDataAccess();
		
		// sign up user to trial subscription
		if(!$ID){
			$data['sub_ID']	=	$SDA->getTrialSubscriptionID();
		}
		
        $modelManager = new ModelManager();
        $result =   $modelManager->doSaveUser($data, $ID, UserDataAccess::FromController($this));
        
        if($result instanceof ValidationStack){
            return $this->returnValidationError($result);
        }
        $ajax   =   array();
        
        // @todo security (no passwords)
        $ajax['result'] = UserListWidget::queryUsers($this)->Where('T.id = %d', $user_id)->Exec()->fetch();
//        $ajax['row']    =   $this->getView('rows/user', false, false)->init('nuser', $ajax['result'])->toString();
        $ajax['message'] = $result['message'];
        return $this->getAjaxView($result);
    }
    
    function doAction($action, UserModel $User){			
			if($deny = $this->Protect('edit_users', 'Auth', $User->getID()))
				return $deny;
        
        $crumb  =   $this->getCrumbsRoot()->addCrumbTxt('UserManager_view', $User->username);

         switch($action)
         {
            case 'save':
                if(isset($_POST['user'])|| isset($_FILES['user'])){
										$_POST['user']['ID']	=	$User->getID();
                    return $this->doAjaxSaveUser(@$_POST['user']);
                }
                if(isset($_POST['info']) || isset($_FILES['info'])){
                    
                    // Add encrypt to bank account info.
                    if (!empty($_POST['info']['baccount_number'])) {
                        // This should probably go into the base controller as a method. But hard to tell what base is with this code :-)
                        $SECRET_KEY = "SEEDLOGIX-BEAST-01-01-2018";
                        $SECRET_IV = "CHILANGO-FRIDAY";
                        
                        $encrypt_method = "AES-256-CBC";
                        $key = hash('sha256', SECRET_KEY);
                        $iv = substr(hash('sha256', SECRET_IV), 0, 16);
                        
                        $encrypted_account_number = openssl_encrypt($_POST['info']['baccount_number'], $encrypt_method, $key, 0, $iv);
                        $_POST['info']['baccount_number'] = $encrypted_account_number;
                    }
                    
                    $VC =   new VarContainer(@$_POST['info']);
                    return $this->doAjaxSaveInfo($User, $VC);
                }
                break;
                
             //save a role for a user
             case 'saverole':
             	return $this->doAjaxSaveRole($User, new VarContainer($_POST['user']));
                 
             case 'view':
             case 'profile':
             	return $this->viewUserDetails($User, $crumb);

#            case 'view':                
#            	return $this->viewUserDashboard($User, $crumb);

             case 'billinginfo':
             	return $this->viewUserBillingInfo($User, $crumb);

             case 'products':
             	return $this->viewUserProducts($User, $crumb);

             case 'systeminfo':
             	return $this->viewUserSysteminfo($User, $crumb);
            case 'updateupload':
                return $this->UpdateUploadDate($_GET['rurl']);
         }
         
         return $this->ActionRedirect('UserManager_list');
    }

    function UpdateUploadDate( $file_path_rel ){
        $DA = UserDataAccess::FromController($this);
        $DA->UpdateLastUpload($file_path_rel);

        return $this->Redirect('/' . UserModel::USER_DATA_DIR . $file_path_rel);

//        return $this->returnAjaxError('error');
    }
    
    function doAjaxSave(UserModel $U, VarContainer $VC){
         
        // @todo security
        
        $this->User->can('is_system') || die('function disabled');
        
        
    }
    
    function doSaveInfo(UserModel $U, VarContainer $VC, &$resultarr){
        
        $Vs =   new ValidationStack;
        
        if(!($U->getID() == $this->user_ID || $this->user_ID == $U->parent_id || 
                $this->User->can('edit_user', $U->getID())))
                return $Vs->addError ('Permission', 'Permission denied.');
        
        $data   =   $VC->getData();
        unset($data['user_id'], $data['id']);   // anything else can be updated.
        $uinfo = new \UserInfoModel;
        
        $UploadInfo =   new UploadsInfo();
        $resultarr['uploads']  =   array();
        
                
                
        if($UploadInfo->accept('info', $Vs, $U->getStoragePath('user_info')))
        {
            foreach($UploadInfo->getFiles() as $key => $file_info){
                $data[$key]     =   $file_info['name']; // filename
                $resultarr['uploads'][$key]    =   $U->getStorageUrl('user_info', $file_info['name']);
            }   
        }
        if($Vs->hasErrors()){
            return $this->returnValidationError($Vs);
        }
        
#        $c  =   $uinfo->__getColumns();
        if(!$U->user_id)    // if the user_id column of the joined user_info table is missing, create the user_info row;
            $this->query('REPLACE INTO %s (user_id) VALUES(%d)', $uinfo, $U->getID());

        if($this->q->Save($uinfo, $data, 'user_id = %d', $U->getID()))
				return true;
		
		return $Vs->addError ('Database', 'Database error ocurred');
    }
    
    
    function doAjaxSaveInfo(UserModel $U, VarContainer $VC){       
        // @todo security
        $results    =   array('result' => true);
        $result =   $this->doSaveInfo($U, $VC, $results);
        if($result instanceof ValidationStack){
            return $this->returnValidationError($result);
        }
        return $this->getAjaxView($results);
    }
    
    function doAjaxSaveRole(UserModel $U, VarContainer $VC){
        if(!$VC->checkValue('role', $roleid)) return $this->returnAjaxError ('Error');
        $this->db->beginTransaction();
        $this->query('DELETE FROM 6q_userroles WHERE user_ID = ' . $U->getID());
        $this->query('INSERT INTO 6q_userroles (user_ID, role_ID) VALUES (' . $U->getID() . ', ' . (int)$roleid . ' )');
        $this->db->commit();
        
        return $this->returnAjaxData('error', false);
    }
}
