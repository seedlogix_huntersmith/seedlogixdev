CREATE TABLE `6q_listings` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`cid` INT NOT NULL DEFAULT '0',
	`tbl` ENUM('directory', 'press', 'article') NOT NULL DEFAULT 'directory',
	`tbl_ID` INT NOT NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
)
COMMENT='Listings for Networks (v2)'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


ALTER TABLE `6q_listings` ADD UNIQUE INDEX `cid_tbl_tbl_ID` (`cid`, `tbl`, `tbl_ID`);


create or replace view 6q_vlistings as 
	select l.*, a.user_id, a.name, a.trashed, unix_timestamp(a.created) as created FROM articles a right join 6q_listings l on (l.tbl_ID = a.id and l.tbl="article") and a.user_id = dashboard_userid() union 
	select l.*, p.user_id, p.name, p.trashed, unix_timestamp(p.created) FROM press_release p right join 6q_listings l on (l.tbl_ID = p.id and l.tbl="press") and p.user_id = dashboard_userid() union
	select l.*, d.user_id, d.name, d.trashed, unix_timestamp(d.created) FROM directory d right join 6q_listings l on (l.tbl_ID = d.id and  l.tbl="directory") and d.user_id = dashboard_userid();

06 15 59
05 05 63

CREATE TABLE `sapphire_6qube`.`piwik_reports` (
  `tbl` ENUM( 'hub' ) NOT NULL,
  `tbl_ID` INT NOT NULL,
  `point_type` ENUM('DAY', 'MONTH' ) NOT NULL,
  `point_date` DATE NOT NULL,
  `point_value` INT NULL,
  `ts` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'piwik reports',
  PRIMARY KEY (`tbl`, `tbl_ID`, `point_type`, `point_date`));


create or replace view last24hr_visits_hubs as
SELECT distinct v.idsite, h.id FROM sapphire_analytics.`analytics_log_visit` v inner join sapphire_6qube.hub h on (h.tracking_id = v.idsite and h.trashed = 0) WHERE v.visit_last_action_time > date_sub(now(), INTERVAL 24 HOUR)


ALTER TABLE `sapphire_6qube`.`piwikreports` 
ADD COLUMN `user_ID` INT(11) NULL AFTER `tbl_ID`;

ALTER TABLE `sapphire_6qube`.`piwikreports` 
CHANGE COLUMN `user_ID` `user_ID` INT(11) NOT NULL DEFAULT 0 ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`user_ID`, `tbl`, `tbl_ID`, `point_type`, `point_date`)
