/* 
 * Author:     amado martinez <amado@projectivemotion.com>
 * License:    
 */

require(['jquery', 'jquery-ui'], function ($){
$(document).ready(function ($){
    
    var activebox   =   null;
    /*
    function widgetover(){
        $(this).addClass('qwOver');
    }
    function widgetout(){
        $(this).removeClass('qwOver');
    }
    */
    function launchtextwidget()
    {
        
    }
    
    function hasElement(box)
    {
        console.log('has element', box);
        return true;
    }
    function replaceElement(box)
    {
        console.log('replace element', box);
    }
    
    $('.qw') /* .hover(widgetover, widgetout) */.droppable({ accept: 'li',
            hoverClass: 'qwOver',
            drop:   function (event, ui) {
                    // this = el: .qw , ui.draggable = 'li''
                console.log(this, event, ui);
                
                if(!hasElement(this) || confirm('Are you sure you want to replace this box?'))
                {
                    replaceElement(this);
                }
            }}).click(function (){
                activebox   =   $(this);
                    $('#hubeditor #wysiwyg').slideDown();
                $("#hubeditor #editor").val($(this).html()).cleditor()[0].updateFrame().refresh();
//                $(this).prepend($('#hubeditor .cleditorMain'));

            });
            /*            
    var cledit  =   $("#editor").cleditor({
            width:"100%", 
            bodyStyle: "margin: 10px; font: 12px Arial,Verdana; cursor:text",
            useCSS:true,
    })[0];
    
    cledit.change(function (){activebox.html(cledit.$area.val());});
    
    */
   
	Aloha.require( ['aloha', 'aloha/jquery'], function( Aloha, jQuery) {
            
                // add right sidebar panels
                var CssPanel    =   {'title': 'Edit Css',
                                        'content': '<table id="classlist"><tr><th>class</th><th>class</th></tr>'
                };
                
                var exp = /class=['"](.*?)['"]/g;
                var cnt=$('#page').html();
                var classes =   [];
                
                while(test = exp.exec(cnt)){
                    console.log(test);
                    var classattr   =   test[1].split(/\s+/);
                    for(var mi = 0; mi < classattr.length; mi++)
                        {
                            var curclass    =   classattr[mi];
                            if(!$.inArray(curclass, classes))
                                classes.push(curclass);
                            CssPanel.content += '<tr><td>class:</td><td><a class="classname-label">' + curclass + '</a></td></tr>';
                        }
                }
                
                CssPanel.content += '</tr>';
                
                var p   =   Aloha.Sidebar.right.addPanel(CssPanel);
                console.log(p);
                
                // get THEMEID
                var templateArea    =   $('#hubeditor .templateArea');
                var THEMEID =   templateArea.attr('id').split('-').pop();
                
                var attributesPanel =   null;
                function getAttributesPanel(classname){
                    if(attributesPanel){
                        return attributesPanel;
                    }
                    var AttributePanel  =   {'title': 'Attributes',
                                                'content': '<table id="attributelist"><tr><th>Attribute</th><th>Value</th></tr>'
                                            };
                             
                                            // loading..
                                            
                                            var reg =   new RegExp(THEMEID + '/([^\.]+.css)$');
                                            console.log(reg);
                                            
                    var attributeRequest = {'theme' : THEMEID, 'files': [], 'class': classname};
                    // so here, the conention is that the css files must reside in the THEMEID/ folder directly and not THEMEID/whatever/file.css
                    templateArea.find('link').each(function (){
                        if(matched = this.href.match(reg)){
                                attributeRequest.files[attributeRequest.files.length] = matched[1];
                        }
                    });
                    
                    console.log(attributeRequest);
                    var request     =   $.ajax({
				url: "csseditor.php",
				type: "POST",
				data: attributeRequest,
				dataType: "html"
			});
                    
			request.done(function(msg) {
                            console.log(msg);
			});

			request.error(function(jqXHR, textStatus) {
				alert( "Request failed: " + textStatus );
			});
                }
                var curselitems =   null;
                $('#classlist .classname-label').hover(function (){
                    var text = $(this).text();
                    console.log(text);
                    curselitems    =   $('#page .' + text);
                    curselitems.css('border', '1px solid green');
                }, function (){
                    curselitems.css('border', '');
                }).click(function (){
                    p.collapse();
                    getAttributesPanel($(this).text());
                });
                // end
		
		// save all changes after leaving an editable
		Aloha.bind('aloha-editable-deactivated', function(){
			var content = Aloha.activeEditable.getContents();
			var contentId = Aloha.activeEditable.obj[0].id;
			var pageId = window.location.pathname;
                        
//                        console.log( Aloha.activeEditable );
			
			// textarea handling -- html id is "xy" and will be "xy-aloha" for the aloha editable
			if ( contentId.match(/-aloha$/gi) ) {
				contentId = contentId.replace( /-aloha/gi, '' );
			}
                        
                        var contentmeta =   contentId.split('-');
                        var pageid=contentmeta[0];
                        var objectname=contentmeta[1];
                        var objectid=contentmeta[2];
                        
//                        console.log(contentmeta);

			var request = $.ajax({
				url: "save.php",
				type: "POST",
				data: {
					'content' : content,
					'object-id' : objectid,
					'page-id' : pageid,
					'object-name' : objectname
				},
				dataType: "html"
			});

			request.done(function(msg) {
//                                alert('saved');
			});

			request.error(function(jqXHR, textStatus) {
				alert( "Request failed: " + textStatus );
			});
		});
		
	});
        
   Aloha.require(['ui/ui', 'ui/button'], function(Ui, Button) {
        var button = Ui.adopt("myButton", Button, {
            click: function(){
                alert("Click!");
            }
        });
    });
   
    $('#hubeditor .dragNDrop li').draggable({ revert: true });
    $('.qw-text').click(launchtextwidget);
    
    var area = $('#hubeditor .templateArea');

    area.mousemove(function (e){        
            var poffset =   $(this).parent().offset();
            var curscroll   =   this.scrollTop;
            var relY    =   e.pageY - poffset.top;
            if(relY > this.clientHeight-40)
                $(this).scrollTo( '+=10px', { axis:'y' } );
            else if(this.scrollTop > 0 && relY < 190)
                $(this).scrollTo( '-=10px', { axis:'y' } );
    //    console.log(relY, this); return;
    });   

});
});