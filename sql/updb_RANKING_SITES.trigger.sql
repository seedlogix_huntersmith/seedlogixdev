
DROP TRIGGER IF EXISTS updb_RANKING_SITES;

delimiter //

CREATE TRIGGER updb_RANKING_SITES BEFORE UPDATE ON ranking_sites
  FOR EACH ROW
  BEGIN
    declare tp_hostname varchar(255);
    set @touchpoint_ID = NEW.touchpoint_ID;
--  select @touchpoint_ID;
    if @touchpoint_ID != 0 THEN
      select hostname INTO @tp_hostname FROM 6q_touchpoints WHERE ID = @touchpoint_ID AND user_ID = NEW.user_id ;
      SET NEW.hostname = @tp_hostname;
    END IF;
--    SET NEW.hostname = "awesoem";
  END;
//

  delimiter ;
