-- view all slide info for child hubs.
select s.* from `slides` s, hub h where s.hub_id = h.id and h.hub_parent_id = 51044 and internalname in ('one', 'two', 'three', 'four')

-- reset slide descriptions for child hubs.

update `slides` s, hub h set s.description = '' 
	where s.hub_id = h.id and h.hub_parent_id = 51044 and internalname in ('one', 'two', 'three', 'four')