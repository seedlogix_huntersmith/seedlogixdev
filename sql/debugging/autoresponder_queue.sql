select * from q_sent_emails where r_user_id = 6610;
update queued_responses set delivered_date = 0 where q_id = 18652;

describe leads;
SELECT T.`contactdata_id`,T.`responder_id`,T.`delivered_date`,T.`schedule_date`,T.`contactdata_table`,ifnull(h.phone, "") as responder_phone, T.q_id,
                if(r.logo="",
                        ifnull(h.logo,"")
                    ,
                    concat("/users/%s/hub_page/",r.logo)) as site_logo ,
                    d.name  as contact_name,
                    d.email as contact_email,
                    d.phone as contact_phone,
                    d.comments  as contact_comments,
                    r.contact   as responder_contact,
                    r.anti_spam as responder_anti_spam,
                    r.optout_msg as responder_optout_msg,

                    leads.id as lead_id,
                leads.lead_name, leads.lead_email, leads.lead_form_id, d.*, u.*, r.*, IFNULL(reseller.main_site, "6qube.com") as main_site FROM queued_responses as T INNER JOIN auto_responders as r ON (r.id = T.responder_id AND r.trashed = "0000-00-00") INNER JOIN users as u ON (u.id = r.user_id) LEFT JOIN contact_form as d ON (d.id = T.contactdata_id AND T.contactdata_table = "contact_form" AND d.trashed = "0000-00-00" AND d.spamstatus = 0)  LEFT JOIN leads ON (T.contactdata_table = "leads" AND leads.optout != 1 AND leads.id = T.contactdata_id AND leads.trashed = "0000-00-00")  LEFT JOIN resellers as reseller ON ((reseller.support_email != "" AND ((u.parent_id = 0 AND reseller.admin_user = u.id) OR reseller.admin_user = u.parent_id)))  LEFT JOIN hub h ON (r.table_name = "hub" AND h.id = r.table_id) WHERE ( r.from_field IS NOT NULL AND r.body IS NOT NULL
            AND r.contact IS NOT NULL AND r.anti_spam IS NOT NULL
            AND r.optout_msg IS NOT NULL
            AND (NOT ISNULL(leads.id) or NOT ISNULL(d.id)) )  AND delivered_date = "0000-00-00 00:00:00" AND schedule_date < NOW() + INTERVAL 10 MINUTE;