CREATE TABLE `sapphire_6qube`.`full_contact_data` (
  `email` VARCHAR(255) NOT NULL,
  `data` VARCHAR(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`email`));
