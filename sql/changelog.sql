ALTER TABLE  `ranking_keywords` ADD INDEX (  `campaign_id` );
ALTER TABLE  `sapphire_6qube`.`ranking_dailystats` DROP INDEX  `user_ID` ,
ADD INDEX  `user_ID` (  `campaign_ID` ,  `user_ID` ,  `date` ,  `keyword_ID` );
ALTER TABLE  `ranking_sites` ADD  `touchpoint_ID` INT UNSIGNED NOT NULL DEFAULT  '0' AFTER  `hostname`;

ALTER TABLE `sapphire_6qube`.`lead_forms`
ADD COLUMN `submit_button_text` VARCHAR(255) NOT NULL DEFAULT '' AFTER `timestamp`,
ADD COLUMN `redirect_url` VARCHAR(255) NOT NULL DEFAULT '' AFTER `submit_button_text`,
ADD COLUMN `show_reset_button` TINYINT(1) NOT NULL DEFAULT 0 AFTER `redirect_url`,
ADD COLUMN `ajax_submit` TINYINT(1) NOT NULL DEFAULT 0 AFTER `show_reset_button`;


CREATE TABLE IF NOT EXISTS `lead_forms_roles` (
  `lead_form_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `lead_forms_roles`
 ADD PRIMARY KEY (`lead_form_id`,`role_id`), ADD KEY `fk_lead_forms_has_6q_roles_6q_roles1_idx` (`role_id`), ADD KEY `fk_lead_forms_has_6q_roles_lead_forms1_idx` (`lead_form_id`);

--
  
  -- 01/08
  ALTER TABLE `prospects` ADD `touchpoint_ID` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `ID`, ADD INDEX (`touchpoint_ID`) ;
  update prospects set touchpoint_ID = (select touchpoint_ID from hub where id = prospects.hub_ID);
  delete from prospects where touchpoint_ID = 0;
  ALTER TABLE `prospects` ADD FOREIGN KEY (`touchpoint_ID`) REFERENCES `sapphire_6qube`.`6q_touchpoints`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

	ALTER TABLE `prospects`  DROP `hub_ID`;

ALTER TABLE `6q_touchpoints` ADD `touchpoint_type` ENUM('LANDING','SOCIAL','MOBILE','MULTIUSER','WEBSITE','BLOG') NOT NULL DEFAULT 'WEBSITE' AFTER `hostname`, ADD INDEX (`touchpoint_type`) ;

ALTER TABLE  `account_limits` ADD  `cancelation_date` DATETIME NOT NULL DEFAULT  '0000-00-00',
ADD  `ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;


-- 01/13
ALTER TABLE `users` ADD `last_upload` DATETIME NOT NULL AFTER `upgraded`;
ALTER TABLE `users` ADD INDEX(`last_upload`);

-- 01/15
ALTER TABLE `sapphire_6qube`.`leads_datax`
CHANGE COLUMN `field_ID` `field_ID` VARCHAR(255) NOT NULL COMMENT 'Actually stores a field name if is a legacy field, for backwards compatibilty' ;

-- 01/20
ALTER TABLE `sapphire_6qube`.`leads_datax`
CHANGE COLUMN `field_ID` `field_ID` VARCHAR(255) NOT NULL DEFAULT 0,
ADD COLUMN `label` VARCHAR(255) NOT NULL DEFAULT '' AFTER `valueint`;
#Saves the labels stored in field_id into label column
UPDATE leads_datax SET label = field_ID, field_ID = 0 WHERE field_id REGEXP '[A-z^]+';

-- 01/21
ALTER TABLE `sapphire_6qube`.`leads_datax`
CHANGE COLUMN `field_ID` `field_ID` INT(11) NOT NULL DEFAULT '0';

-- 01/21
ALTER TABLE `prospects` CHANGE `contact_ID` `contactform_ID` INT(11) NOT NULL;
ALTER TABLE  `contacts` ENGINE = INNODB;
ALTER TABLE  `contacts` ADD  `prospect_ID` INT UNSIGNED NOT NULL AFTER  `cid` ,
ADD INDEX (  `prospect_ID` );

-- 01/24
#Query for update all prospects names and emails from leads
UPDATE prospects p,
	leads l
SET
	p.name = IF(p.name = '' OR p.name IS NULL,
				l.lead_name,
				p.name),
	p.email = IF(p.email = '' OR p.email IS NULL,
				 l.lead_email,
				 p.email)
WHERE
	p.lead_ID != 0 AND p.lead_ID = l.ID;

-- 01/29
ALTER TABLE `sapphire_6qube`.`lead_forms_emailers`
ADD COLUMN `campaign_id` INT(11) NOT NULL AFTER `lead_form_id`;
#Update existing lead_forms_emailers.campaign_id
UPDATE lead_forms_emailers e, lead_forms f SET e.campaign_id = f.cid WHERE f.id = e.lead_form_id;


-- 01/30
ALTER TABLE `sapphire_6qube`.`campaign_reports`
ADD COLUMN `start_date` DATE NULL AFTER `touchpoint_type`,
ADD COLUMN `end_date` DATE NULL AFTER `start_date`;

-- 02/09
INSERT INTO `sapphire_6qube`.`6q_permissions` (`ID`, `flag`) VALUES (NULL, 'impersonate');

-- 02/10
ALTER TABLE `sapphire_6qube`.`prospects`
CHANGE COLUMN `photo` `photo` BLOB NOT NULL ;

ALTER TABLE `sapphire_6qube`.`full_contact_data`
CHANGE COLUMN `data` `data` VARCHAR(15000) NOT NULL DEFAULT '' ;

-- 02/11
ALTER TABLE `sapphire_6qube`.`full_contact_data`
CHANGE COLUMN `data` `data` BLOB NOT NULL DEFAULT '' ;

-- 02/19
ALTER TABLE `sapphire_6qube`.`lead_forms_emailers`
CHANGE COLUMN `trashed` `trashed` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

-- 02/20
ALTER TABLE `ranking_sites` DROP `hub_ID` ;
ALTER TABLE `ranking_sites` DROP `blog_ID`;

-- 02/26
ALTER TABLE `hub` ADD INDEX ( `last_updated` ) ;
update prospects p set touchpoint_ID = (select touchpoint_ID from hub where id = p.hub_ID) where p.hub_ID != 0 and p.touchpoint_ID = 1;

-- 02/27
INSERT INTO `6q_permissions` (`ID`, `flag`) VALUES (NULL, 'create_contact'), (NULL, 'editcontact');

-- 02/28
INSERT INTO `sapphire_6qube`.`6q_permissions` (`ID`, `flag`) VALUES (NULL, 'view_HOME');

-- 03/06
ALTER TABLE `sapphire_6qube`.`prospects`
CHANGE COLUMN `visitor_ID` `visitor_ID` VARBINARY(8) NOT NULL;

-- 03/18
ALTER TABLE `sapphire_6qube`.`themes`
ADD COLUMN `theme_type` ENUM('LANDING','SOCIAL','MOBILE','MULTIUSER','WEBSITE') NULL AFTER `last_edit`,  -- Determines for which touchpoint type will be available
ADD COLUMN `hybrid` VARCHAR(45) NULL AFTER `theme_type`; -- hybrid = 1 means will only available on hybrid version;

-- 03/19
ALTER TABLE `sapphire_6qube`.`user_class`
ADD COLUMN `pemail_edit_1` BLOB NULL AFTER `secure_edit3`,
ADD COLUMN `pemail_edit_2` BLOB NULL AFTER `pemail_edit_1`;

ALTER TABLE `sapphire_6qube`.`user_info`
ADD COLUMN `tcity` VARCHAR(255) NOT NULL DEFAULT '' AFTER `date_field`;

-- 03/31
ALTER TABLE `sapphire_6qube`.`blog_post`
CHANGE COLUMN `master_ID` `master_ID` INT(11) NOT NULL DEFAULT 0 ;

-- 04/01
ALTER TABLE `sapphire_6qube`.`themes`
CHANGE COLUMN `theme_type` `theme_type` ENUM('LANDING','SOCIAL','MOBILE','MULTIUSER','WEBSITE','BLOG') NULL DEFAULT NULL ;

-- 04/06
INSERT INTO `sapphire_6qube`.`6q_permissions` (`flag`) VALUES ('view_theme');


-- 05/27
ALTER TABLE `sapphire_6qube`.`piwikreports`
ADD COLUMN `bounce_count` INT(11) NOT NULL DEFAULT 0 AFTER `nb_leads`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`tbl`, `tbl_ID`, `user_ID`, `cid`, `point_type`, `point_date`);

ALTER TABLE `sapphire_6qube`.`piwikreports_ref`
ADD COLUMN `bounce_count` INT(11) NOT NULL DEFAULT 0 AFTER `referrer`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`tbl`, `tbl_ID`, `user_ID`, `cid`, `point_type`, `point_date`, `referrer`);

ALTER TABLE `sapphire_6qube`.`piwikreports_se`
ADD COLUMN `bounce_count` INT(11) NOT NULL DEFAULT 0 AFTER `nb_leads`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`tbl`, `tbl_ID`, `user_ID`, `cid`, `point_type`, `point_date`, `searchengine`);

ALTER TABLE `sapphire_6qube`.`piwikreports_social`
ADD COLUMN `bounce_count` INT(11) NOT NULL DEFAULT 0 AFTER `nb_leads`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`tbl`, `tbl_ID`, `user_ID`, `cid`, `point_type`, `point_date`, `network`);

-- 06/05
ALTER TABLE `sapphire_6qube`.`blog_post`
ADD COLUMN `post_edit_1` BLOB NOT NULL AFTER `post_content`;

-- 06/19
#changing touchpoint_ID column to null
ALTER TABLE `sapphire_6qube`.`prospects`
CHANGE COLUMN `touchpoint_ID` `touchpoint_ID` INT(10) UNSIGNED NULL DEFAULT '0' COMMENT '' ;

-- 07/21
ALTER TABLE `sapphire_6qube`.`ranking_proxys`
ADD COLUMN `active` INT NOT NULL DEFAULT 1 COMMENT '' AFTER `total_attempts`;

-- 07/29
-- CHANGE "NAME" OF "FIELD "error_keyword_count" ---------------
ALTER TABLE `ranking_proxys` CHANGE `error_keyword_count` `error_keyword_count_google` Int( 11 ) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "error_timeout_count" ---------------
ALTER TABLE `ranking_proxys` CHANGE `error_timeout_count` `error_timeout_count_google` Int( 11 ) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "total_attempts" --------------------
ALTER TABLE `ranking_proxys` CHANGE `total_attempts` `total_attempts_google` Int( 11 ) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------

-- CREATE FIELD "error_keyword_count_bing" ---------------------
ALTER TABLE `ranking_proxys` ADD COLUMN `error_keyword_count_bing` Int( 255 ) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------

-- CREATE FIELD "error_timeout_count_bing" ---------------------
ALTER TABLE `ranking_proxys` ADD COLUMN `error_timeout_count_bing` Int( 255 ) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------

-- CREATE FIELD "total_attempts_bing" --------------------------
ALTER TABLE `ranking_proxys` ADD COLUMN `total_attempts_bing` Int( 255 ) NOT NULL;
-- -------------------------------------------------------------

-- 07/31
ALTER TABLE `sapphire_6qube`.`ranking_proxys`
ADD COLUMN `last_updated` DATE NOT NULL DEFAULT 0 COMMENT '' AFTER `total_attempts_bing`;


-- 11/14
ALTER TABLE sapphire_6qube.leads ADD touchpoint_id INT DEFAULT 0 NOT NULL;
ALTER TABLE sapphire_6qube.leads ALTER COLUMN cid SET DEFAULT 0;

-- 12/07
ALTER TABLE sapphire_6qube.prospects MODIFY name varchar(255);
ALTER TABLE sapphire_6qube.leads MODIFY lead_name varchar(128);