
drop procedure if exists getHUBForDisplay;

delimiter //

CREATE PROCEDURE getHUBForDisplay(IN phub_ID INT(11), IN ptheme_ID INT ( 11 ) ) 
begin
	SELECT "HubModel",
		ifnull(r.main_site, "6qube.com") as reseller_domain, 
			h.*, h.theme as theme_id, 
			t.path as theme_path,
            @a := exists( select id  from hub_page2 p2 where p2.hub_id = h.id AND p2.trashed = 0), 
			@ac := (@a AND exists( select id from hub_page2 p2 where p2.hub_id = h.id and p2.trashed = 0 and p2.has_custom_form = 1)), 
			@b := ( @a OR exists( select id from hub_page p1 where p1.hub_id = h.id and p1.trashed = 0)) as haspages, 
			(@ac OR (@b AND !@a AND exists( select id from hub_page p1 where p1.hub_id = h.id and p1.trashed = 0 and p1.has_custom_form = 1))) as hascustomformpages,
			f.root, f.classname, f.pagename, f.key_match
		FROM hub h
			INNER JOIN users u ON (u.id = h.user_id AND u.canceled = 0 )
			LEFT JOIN themes t ON  ( ( ptheme_ID = 0 AND t.id = h.theme ) OR t.id = ptheme_ID)
			LEFT JOIN resellers r ON ( r.admin_user = h.user_parent_id and h.user_parent_id != 0)
			LEFT JOIN feeds f ON (h.has_feedobjects != 0 AND f.ID = h.has_feedobjects)			
		WHERE h.ID = phub_ID;
END; //
