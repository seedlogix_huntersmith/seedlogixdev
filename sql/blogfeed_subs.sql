CREATE TABLE IF NOT EXISTS `blogfeed_subs` (
  `blog_ID` int(11) NOT NULL,
  `feed_ID` int(11) NOT NULL,
  UNIQUE KEY `x` (`blog_ID`,`feed_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='subscriptions to feeds.';