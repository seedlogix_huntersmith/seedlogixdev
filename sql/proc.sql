drop procedure dashboard_getcampaignstats;

DELIMITER $$

CREATE PROCEDURE `dashboard_getcampaignstats` (d1 DATE, d2 DATE, d3 DATE, d4 DATE, cid INT) DETERMINISTIC READS SQL DATA
BEGIN
-- uses dashboard_userid()
-- select the stats per campaign, and right join the campaigns to the stats
	select j.id, j.name, coalesce(j.visitsprev, 0) as visitsprev, coalesce(j.visitsnow, 0) as visitsnow, j.num_touchpoints,
				j.num_leads, if(j.num_leads = 0, 0, j.visitsnow/j.num_leads)*100 as conversionspct,
				if(j.visitsprev != 0, (j.visitsnow-j.visitsprev)/j.visitsprev, j.visitsnow)*100 as visitschangepct,
				DATE_FORMAT(j.date_created, "%M %D, %Y") as created_str
		from (
		select c.id, c.name, c.date_created, 
			(
				select sum(r.point_value) from piwikreports r where 
					r.user_id = c.user_id and r.cid = c.id and r.point_date BETWEEN d1 AND d2
			) as visitsprev,		
			(
				select sum(r2.point_value) from piwikreports r2 where 
					r2.user_id = c.user_id and r2.cid = c.id
					and	r2.point_date BETWEEN d3 AND d4				
			) as visitsnow, 
			(
				select count(t.id) from 6q_trackingids t where (t.cid = c.id)
			) as num_touchpoints, 
			(
				select count(l.id) from leads l where (l.cid = c.id and l.created BETWEEN d3 AND d4)
			) as num_leads
		from campaigns c 
		where c.user_id = dashboard_userid() and (cid = 0 OR c.id = cid) order by visitsnow desc
		) as j ;

END
$$

delimiter ;
set @dashboard_userid	=	6610;
call dashboard_getcampaignstats("2014-06-01", "2014-06-10", "2014-06-10", "2014-06-19", 0);
call dashboard_getcampaignstats("2014-06-01", "2014-06-10", "2014-06-10", "2014-06-19", 7221);
#show status like 'qc%' ;

#set sql_safe_updates = 0;
#update `piwikreports` set `cid` = 7221 where user_id = 6610 ;