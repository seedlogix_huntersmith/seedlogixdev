
drop procedure if exists getTouchPointByHost;
delimiter //
CREATE PROCEDURE getTouchPointByHost ( IN phostname_in VARCHAR(255) )
BEGIN
--	select phostname_in;
	set @hash = unhex(md5(phostname_in));
	set @hub_ID = 0;
	set @blog_ID = 0;
	SELECT blog_ID, hub_ID, ID INTO @blog_ID, @hub_ID, @tp_ID FROM 6q_touchpoints WHERE hosthash = @hash AND active = 1 LIMIT 1;
--	SELECT @blog_ID, @hub_ID, @tp_ID, @hash;
	IF @hub_ID != 0 THEN
		CALL getHUBForDisplay(@hub_ID, 0);
	ELSEIF @blog_ID != 0 then
		SELECT "BlogModel", b.* 
				FROM blogs b 
			INNER JOIN users u ON (u.id = b.user_id AND u.canceled = 0)
			WHERE b.ID = @blog_ID;
	else	-- COULD NOT FIND A TOUCHPOINT
		select blog_ID, hub_ID INTO @blog_ID, @hub_ID FROM (
			select 0 as blog_ID, ID as hub_ID from hub where httphostkey = phostname_in AND trashed = 0 limit 1
			union select ID, 0 FROM blogs where httphostkey = phostname_in AND trashed = 0 LIMIT 1
			) f;
--		call getHubForDisplayByHost(phostname_in, 0);
--		select phostname_in, @blog_ID, @hub_ID;
		if @blog_ID != 0 THEN
			-- still not found in hub table, LOOK IN blog table
			SELECT "BlogModel", b.* 
					FROM blogs b 
				INNER JOIN users u ON (u.id = b.user_id AND u.canceled = 0)
				WHERE b.ID = @blog_ID;
		elseif @hub_ID != 0 THEN
			call getHUBForDisplay(@hub_ID, 0);
		end if;
	END IF;
END;

//

delimiter ;

call getTouchPointByHost('blog.sofus.qu');
call getTouchPointByHost('hub.sofus.qu');
call getTouchPointByHost('sofus.hybrid.qu');
call getTouchPointByHost('hybrid.qu');
select * from hub where httphostkey = 'hub.sofus.qu';
delete from hub where id = 50584;
-- insert into hub set httphostkey = 'hub.sofus.qu', user_ID = 6610;
-- insert into blogs set httphostkey = 'blog.sofus.qu', user_ID = 6610;