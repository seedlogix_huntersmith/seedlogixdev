drop trigger if exists updb_HUB;

delimiter //

CREATE TRIGGER updb_HUB BEFORE UPDATE ON hub
  FOR EACH ROW 
  BEGIN
  set @istrashed = NEW.trashed<>"0000-00-00";
  set @isactive = (NEW.trashed="0000-00-00");
  set @hash = if(trim(NEW.httphostkey)="", NULL, unhex(md5(NEW.httphostkey)));
  set @hub_ID = NEW.id;

  if NEW.touchpoint_ID=0 THEN
	if OLD.touchpoint_ID = 0 THEN
	-- old value = 0 and new value = 0, do an insert only if host string is provided
		if NEW.httphostkey != "" THEN	
		INSERT INTO 6q_touchpoints 
			SET hub_ID = @hub_ID, hosthash = @hash, active = @isactive, hostname = NEW.httphostkey, touchpoint_type = NEW.touchpoint_type,
			user_ID = NEW.user_ID, user_parent_ID = NEW.user_parent_id, campaign_ID = NEW.cid;
		SET NEW.touchpoint_ID = LAST_INSERT_ID();
		end if;
	ELSE
		DELETE FROM 6q_touchpoints WHERE hub_ID = @hub_ID;
	END IF;
  ELSE
    SET @tp_ID = NEW.touchpoint_ID;

 -- if NEW.httphostkey = OLD.httphostkey THEN
    UPDATE 6q_touchpoints SET active = @isactive, hosthash = @hash, hub_ID = @hub_ID, hostname = NEW.httphostkey, touchpoint_type = NEW.touchpoint_type,
        user_ID = NEW.user_ID, user_parent_ID = NEW.user_parent_id, campaign_ID = NEW.cid
        WHERE ID = @tp_ID;
 -- else
        -- disable old host
        -- UPDATE 6q_touchpoints SET active = 0 WHERE ID = @tp_ID;
      
        -- activate new host
--        INSERT INTO 6q_touchpoints SET hosthash = @hash,
  --          active = @isactive, hostname = NEW.httphostkey, hub_ID = @hub_ID, blog_ID = 0
   --      ON DUPLICATE KEY UPDATE hub_ID = @hub_ID, blog_ID = 0, active = @isactive;

     -- end if;
  END IF;
  END;
  //

delimiter ;

-- update hub set name = lower(name);