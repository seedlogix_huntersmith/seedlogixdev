#use sapphire_analytics;
create or replace view sapphire_analytics.last24hr_visits as
SELECT v.idsite, h.id, h.touchpoint_type, h.user_id, h.cid FROM sapphire_analytics.`analytics_log_visit` v 
	inner join sapphire_6qube.6q_trackingids h on (h.tracking_id = v.idsite)
	WHERE v.visit_last_action_time > date_sub(now(), INTERVAL 24 HOUR) group by v.idsite
