create or replace view 6q_trackingids as 
#set @dashboard_userid = 10545;
select 'hub' AS `touchpoint_type`,`hub`.`id` AS `id`,
	`hub`.`user_id` AS `user_id`,
	`hub`.`cid` AS `cid`,
	`hub`.`tracking_id` AS `tracking_id`,
	`hub`.`created` AS `created` from `hub` where ((`hub`.`trashed` = 0 and hub.tracking_id !=0) and (dashboard_userid() = 0 OR `hub`.`user_id` = `dashboard_userid`()) and 
				(isnull(`dashboard_campaignid`()) or (`hub`.`cid` = `dashboard_campaignid`())))

union select 'blog' AS `blog`,`blogs`.`id` AS `id`,
	`blogs`.`user_id` AS `user_id`,
	`blogs`.`cid` AS `cid`,
	`blogs`.`tracking_id` AS `tracking_id`,
	`blogs`.`created` AS `created` from `blogs` where ((`blogs`.`trashed` = 0 and blogs.tracking_id !=0) and (dashboard_userid() = 0 OR `blogs`.`user_id` = `dashboard_userid`()) 
			and (isnull(`dashboard_campaignid`()) or (`blogs`.`cid` = `dashboard_campaignid`())))

union select 'blog_post', p.id,
	p.user_id,
	0,
	p.tracking_id,
	p.created from blog_post p where (p.trashed = 0  and p.tracking_id !=0 and (dashboard_userid() = 0 OR p.user_id = dashboard_userid()))
	/* missing campaign id in blog_post table */

union select 'directory', d.id,
	d.user_id,
	d.cid,
	d.tracking_id,
	d.created from directory d where (d.trashed = 0 and d.tracking_id !=0 and (dashboard_userid() = 0 OR d.user_id = dashboard_userid()))
		and (isnull(dashboard_campaignid()) or d.cid = dashboard_campaignid())

union select 'press_release', pr.id,
	pr.user_id,
	pr.cid,
	pr.tracking_id,
	pr.created from press_release pr where (pr.trashed = 0 and pr.tracking_id !=0 and (dashboard_userid() = 0 OR pr.user_id = dashboard_userid()))
		and (isnull(dashboard_campaignid()) or pr.cid = dashboard_campaignid());




	set @dashboard_userid	=	10545;
	select * from 6q_trackingids;
	show status like 'qc%';
