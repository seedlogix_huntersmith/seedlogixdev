CREATE OR REPLACE VIEW touchpoints_info AS
--  explain
    SELECT t.*, coalesce(h.tracking_id, b.tracking_id, 0) as tracking_id,
    coalesce(h.name, b.blog_title) as touchpoint_name,
    coalesce(h.created, b.created) as created_date
    FROM `6q_touchpoints` t
    LEFT JOIN hub h ON (h.ID = t.hub_ID AND t.blog_ID = 0)
    LEFT JOIN blogs b ON (b.ID = t.blog_ID AND t.hub_ID = 0)
;

-- explain select * from touchpoints_info where touchpoint_type = 'WEBSITE' LIMIT 5;