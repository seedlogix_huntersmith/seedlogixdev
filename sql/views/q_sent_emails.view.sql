drop view if exists q_sent_emails;
CREATE ALGORITHM=UNDEFINED DEFINER=`sapphire`@`localhost` SQL SECURITY DEFINER VIEW `q_sent_emails` AS
SELECT T.q_id, r.user_id as r_user_id, r.name,
			`T`.`responder_id` as resp_id, r.cid, r.table_name, r.table_id,
			if(T.contactdata_table="leads",T.contactdata_id,0) as lead_id,
			if(T.contactdata_table="contact_form",T.contactdata_id,0) as c_form_id,
			`T`.`delivered_date`, `T`.`schedule_date`,
			`theme`.`theme_name`,
			if(T.contactdata_table="contact_form", CONCAT(`d`.`name`,'/',`d`.`email`,'/',`d`.`phone`),
				CONCAT(leads.lead_name, '/', leads.lead_email)) as info,
				`u`.`username` AS responder_username, 
				`r`.`from_field`, `r`.`subject`,
			IFNULL(`reseller`.`main_site`,'6qube.com') AS `main_site`
FROM `queued_responses` `T`
	JOIN `auto_responders` `r` ON `r`.`id` = `T`.`responder_id`
	JOIN `users` `u` ON `u`.`id` = `r`.`user_id`
-- LEFT JOIN `trash` `respondertrash` ON `respondertrash`.`table_id` = `r`.`id` AND `respondertrash`.`table_name` = 'auto_responders'
LEFT JOIN `contact_form` `d` ON `d`.`id` = `T`.`contactdata_id` AND `T`.`contactdata_table` <> 'leads'
LEFT JOIN `leads` ON `T`.`contactdata_table` = 'leads' AND `leads`.`optout` <> 1 AND `leads`.`id` = `T`.`contactdata_id`
LEFT JOIN `resellers` `reseller` ON `reseller`.`admin_user` = `u`.`id` AND `r`.`user_parent_id` <> 0 AND `r`.`user_parent_id` <> 7
LEFT JOIN `auto_responder_themes` `theme` ON `theme`.`id` = `r`.`theme_id`
WHERE -- ISNULL(`respondertrash`.`table_id`) AND 
	r.trashed = "0000-00-00" AND
	(`r`.`from_field` IS NOT NULL) AND
	(`r`.`body` IS NOT NULL) AND 
	(`r`.`contact` IS NOT NULL) AND 
	(`r`.`anti_spam` IS NOT NULL) AND 
	(`r`.`optout_msg` IS NOT NULL) AND 
	(`T`.`delivered_date` <> 0)
ORDER BY `T`.`delivered_date` DESC;
-- LIMIT 300;
