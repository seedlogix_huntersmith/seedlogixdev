-- CREATE ALGORITHM=UNDEFINED VIEW `6q_prospects_campaign` AS 
	select 'contact_form' AS `table`,`c`.`id` AS `ID`,`c`.`cid` AS `CID`,0 AS `form_id`,
	ifnull(`m`.`valint`,0) AS `grade`,`c`.`name` AS `name`,`c`.`email` AS `email`,`c`.`phone` AS `phone`,`c`.`type` AS `transport`,
	`c`.`type_id` AS `transport_ID`,'unknown' AS `TP_TYPE`,`c`.`website` AS `TP_NAME`,'unknown' AS `TP_PAGE`,`c`.`keyword` AS `keyword`,
	`c`.`search_engine` AS `search_engine`,`c`.`created` AS `created` from (`contact_form` `c` 
		left join `6q_meta` `m` on(((`m`.`table` = 'contact_form') and (`m`.`PK` = `c`.`id`) and (`m`.`col` = 'grade'))))
	where (`c`.`user_id` = `dashboard_userid`())

 union select 'leads' AS `table`,`l`.`id` AS `ID`,`l`.`cid` AS `CID`,`l`.`lead_form_id` AS `lead_form_id`,ifnull(`m`.`valint`,0) AS `grade`,`l`.`lead_name` AS `lead_name`,`l`.`lead_email` AS `lead_email`,'unknown' AS `phone`,'hub' AS `hub`,`l`.`hub_id` AS `hub_id`,'unknown' AS `unknown`,(select `hub`.`name` from `hub` where (`hub`.`id` = `l`.`hub_id`)) AS `(select ``sapphire_6qube``.``hub``.``name`` from ``sapphire_6qube``.``hub`` where
(``sapphire_6qube``.``hub``.``id`` = ``l``.``hub_id``))`,(select `6q_hubpages_tbl`.`page_title` from `6q_hubpages_tbl` where ((`l`.`hub_page_id` <> 0) and (`6q_hubpages_tbl`.`oldtbl_ID` = `l`.`hub_page_id`))) AS `(select ``sapphire_6qube``.``6q_hubpages_tbl``.``page_title`` from ``sapphire_6qube``.``6q_hubpages_tbl`` where ((``l``.``hub_page_id`` <> 0) and
 (``sapphire_6qube``.``6q_hubpages_tbl``.``oldtbl_ID`` = ``l``.``hub_page_id``)))`,
	`l`.`keyword` AS `keyword`,`l`.`search_engine` AS `search_engine`,`l`.`timestamp` AS `timestamp` 
	from (`leads` `l` left join `6q_meta` `m` on(((`m`.`table` = 'leads') and (`m`.`PK` = `l`.`id`) and (`m`.`col` = 'grade'))))
	where (`l`.`user_id` = `dashboard_userid`()) ;
	
	-- new 
	CREATE OR REPLACE ALGORITHM=UNDEFINED VIEW `6q_prospects_campaign` AS /* 
	select 'contact_form' AS `table`,`c`.`id` AS `ID`,`c`.`cid` AS `CID`,0 AS `form_id`,
	c.grade as grade,`c`.`name` AS `name`,`c`.`email` AS `email`,`c`.`phone` AS `phone`,`c`.`type` AS `transport`,
	`c`.`type_id` AS `transport_ID`,'unknown' AS `TP_TYPE`,`c`.`website` AS `TP_NAME`,'unknown' AS `TP_PAGE`,`c`.`keyword` AS `keyword`,
	`c`.`search_engine` AS `search_engine`,`c`.`created` AS `created` from (`contact_form` `c`)
	where (`c`.`user_id` = `dashboard_userid`())
 union */
 		select 'leads' AS `table`,`l`.`id` AS `ID`,`l`.`cid` AS `CID`,`l`.`lead_form_id` AS form_id,`l`.`grade`,`l`.`lead_name` AS name,
 	`l`.`lead_email` AS email,'unknown' AS `phone`,'hub' AS transport,`l`.`hub_id` AS transport_ID,'unknown' AS TP_TYPE,
 	(select `hub`.`name` from `hub` where (`hub`.`id` = `l`.`hub_id`)) as TP_NAME,
	(select `6q_hubpages_tbl`.`page_title` from `6q_hubpages_tbl` where ((`l`.`hub_page_id` <> 0) and (`6q_hubpages_tbl`.`oldtbl_ID` = `l`.`hub_page_id`))) as TP_PAGE,
	`l`.`keyword` AS `keyword`,`l`.`search_engine` AS `search_engine`,`l`.created 
	from `leads` `l`
	where (`l`.`user_id` = `dashboard_userid`()) ;