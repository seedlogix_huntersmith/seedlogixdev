CREATE TABLE IF NOT EXISTS `blogfeed_masterposts` (
  `feed_ID` int(11) NOT NULL,
  `master_post_ID` int(11) NOT NULL,
  KEY `feed_ID` (`feed_ID`,`master_post_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
