

	delete t from 6q_touchpoints t, disposable_tps p where t.id = p.id;
	
select * from hub where id = 105526;
select * from hub where httphostkey = 'wvmbfarmington.com';

-- find touchpoints with inva	
-- get ids of hubs with a touchpoint that does not point back to them.
-- explain 
-- create temporary table hub_reset_tp as 
select h.ID -- , h.httphostkey, t.*
	 from hub h inner join hub_dups u on h.touchpoint_ID = u.touchpoint_ID
	left join 6q_touchpoints t on t.ID = u.touchpoint_ID and t.hub_ID = h.id
	where t.ID is null;
	
	select * from hub_reset_tp;
	
	update hub set touchpoint_ID = 0 where id in (select ID from hub_reset_tp);
	
-- find collisions of multiple touchpoints pointing to the same hub.
-- create temporary table duplicate_touchpoints_temp as 
select tp.id from 6q_touchpoints tp inner join ( SELECT COUNT(*) AS `Rows`, `hub_ID` FROM `6q_touchpoints`
where hub_ID != 0
-- and hub_ID = 106085
GROUP BY `hub_ID` having `Rows` > 1 ) t 
	on t.hub_ID = tp.hub_ID
	LEFT JOIN hub h on h.touchpoint_ID = tp.ID
	where isnull(h.ID)	-- select only touchpoints without a matching hub.
  order by tp.hub_ID desc, tp.ID ASC;

-- select hubs, pointed to by tp, that do not have the same touchpoint_ID as the tp
-- create temporary table update_hubs as 
	select -- tp.*, h.touchpoint_ID
			tp.id as tp_id, h.id as hub_id, h.touchpoint_ID as incorrect_ID  
		from 6q_touchpoints tp inner join hub h on h.id = tp.hub_ID
	where h.touchpoint_ID != tp.id	-- select only mismatches
--  order by tp.hub_ID desc, tp.ID ASC
  	union  all
  select * from update_hubs;
  
  select touchpoint_ID from hub where id = 105614;
  select * from 6q_touchpoints where hub_ID = 105614;
  select * from hub where touchpoint_ID = 2557;
  select * from 6q_touchpoints where hub_ID = 5832;
--  update 6q_touchpoints set hub_ID = 5832 where id = 2557;
  
--  delete from 6q_touchpoints where 
  drop table update_hubs;
  
    
  update hub h, update_hubs u SET h.touchpoint_ID = u.tp_id WHERE h.id = u.hub_id;
  -- select touchpoints with mismatching 
  
  -- select touchpoints without a hash
  select t.*,h.touchpoint_ID,h.httphostkey from 6q_touchpoints t, hub h 
  where h.touchpoint_ID = t.id AND h.httphostkey != "" AND 
  	hosthash IS NULL;
  	
  select * from duplicate_touchpoints_temp;
  call getTouchPointByHost('oceansidepestcontrol.net');
--  call getTouchPointByHost('pastephp.com');
--  select * from 6q_touchpoints where hosthash = unhex(md5('ignitedapp.com'));
--  select * from 6q_touchpoints where user_id = 6610;
--  delete from 6q_touchpoints where id IN (select id from duplicate_touchpoints_temp);