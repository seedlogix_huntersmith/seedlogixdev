-- finds users whose oldest stats date is not equal to the user's creation date. should return an empty set, meaning
-- that all users stats have been generated starting on the user's creation date up to curdate()
SELECT u.id,
	min( r.point_date ) AS mindate,
	max( r.point_date ) AS maxdate,
	date(u.created) as created,
	concat(date(created), ",", min(r.point_date)) as range1,
	if(max(point_date) != curdate(),concat(max(r.point_date), ",", curdate()),"") as range2
FROM users u
LEFT JOIN piwikreports r ON ( r.user_ID = u.ID  and point_type = "DAY")
WHERE u.created > 0 AND u.last_login > "2015-01-01"
GROUP BY u.ID having mindate > created
ORDER BY `range1`  ASC