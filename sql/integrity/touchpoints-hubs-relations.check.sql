-- find duplicate hub_ID refrences in touchpoints table.  ( should return empty set)
SELECT COUNT(*) AS `Rows`, group_concat(id), `hub_ID` FROM `6q_touchpoints` where hub_ID != 0 GROUP BY `hub_ID` having `Rows` > 1 ORDER BY `Rows`  DESC;

-- find duplicate touchpoint_ID refrences in hub ( should return empty set)
 -- create temporary table hub_dups as
select count(*) as `Rows`, group_concat(id), touchpoint_ID from hub h 
	where touchpoint_ID != 0
	group by touchpoint_ID having `Rows` > 1 order by `Rows` DESC;

-- find hubs with invalid touchpoint_ID ( should return an empty set )
select h.id, h.httphostkey, h.touchpoint_ID from hub h left join 6q_touchpoints p on p.id = h.touchpoint_ID
	where p.id is null and h.touchpoint_ID != 0;
	

-- find touchpoints with invalid hub_ID ( should return an empty set )
-- create temporary table disposable_tps as 
	select t.id, t.hostname, t.hub_ID from 6q_touchpoints t
	left join hub h on h.id = t.hub_ID
	where h.id is null and t.hub_ID != 0;