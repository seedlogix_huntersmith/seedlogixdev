drop trigger if exists updb_BLOGS;

delimiter //

CREATE TRIGGER updb_BLOGS BEFORE UPDATE ON blogs
  FOR EACH ROW 
  BEGIN
  set @istrashed = NEW.trashed<>"0000-00-00";
  set @isactive = (NEW.trashed="0000-00-00");
  set @hash = if(trim(NEW.httphostkey)="", NULL, unhex(md5(NEW.httphostkey)));
  set @blog_ID = NEW.id;

  if NEW.touchpoint_ID=0 THEN
	if OLD.touchpoint_ID = 0 THEN
    INSERT INTO 6q_touchpoints 
      SET blog_ID = @blog_ID, hosthash = @hash, active = @isactive, hostname = NEW.httphostkey, touchpoint_type = 'BLOG',
        user_ID = NEW.user_ID, user_parent_ID = NEW.user_parent_id, campaign_ID = NEW.cid;
    SET NEW.touchpoint_ID = LAST_INSERT_ID();
	ELSE
		DELETE FROM 6q_touchpoints WHERE blog_ID = @blog_ID;
	END IF;
  ELSE
    SET @tp_ID = NEW.touchpoint_ID;

 -- if NEW.httphostkey = OLD.httphostkey THEN
    UPDATE 6q_touchpoints SET active = @isactive, hosthash = @hash, blog_ID = @blog_ID, hostname = NEW.httphostkey, touchpoint_type = 'BLOG',
        user_ID = NEW.user_ID, user_parent_ID = NEW.user_parent_id, campaign_ID = NEW.cid
        WHERE ID = @tp_ID;
  END IF;
  END;
  //

delimiter ;
