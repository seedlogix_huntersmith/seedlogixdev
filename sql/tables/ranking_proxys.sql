CREATE TABLE `sapphire_6qube`.`ranking_proxys` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `proxy` VARCHAR(45) NOT NULL COMMENT '',
  `error_timeout_count` INT NOT NULL DEFAULT 0 COMMENT '',
  `error_keyword_count` INT NOT NULL DEFAULT 0 COMMENT '',
  `total_attempts` INT NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `proxy_UNIQUE` (`proxy` ASC)  COMMENT '');