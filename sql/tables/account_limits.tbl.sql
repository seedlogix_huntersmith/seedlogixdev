
DELETE FROM subscriptions;

drop table subscriptions;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` FLOAT NULL,
  `limits_json` TEXT NULL,
  `reseller_ID` INT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;

drop table account_limits;
CREATE TABLE IF NOT EXISTS `account_limits` (
  `user_ID` INT(11) NOT NULL,
  `sub_ID` INT UNSIGNED NULL,
  `expiration_date` DATE NOT NULL,
  `billing_ref` INT NULL,
  `limits_json` TEXT NULL,
  INDEX `fk_user_ID_idx` (`user_ID` ASC),
  CONSTRAINT `fk_user_ID`
    FOREIGN KEY (`user_ID`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

drop table account_limits;

create or replace view subscription_logs as
select user_ID, sub_ID, expiration_date, billing_ref, limits_json from account_limits where ifnull(sub_ID, 0) != 0
WITH CHECK OPTION;

select * from subscriptions;
select * from account_limits;

select 1 from Subscriptions where ID = 1;

select * from account_limits;

insert into Subscriptions set name="Hello";
insert into subscription_logs set user_ID = 1, sub_ID = 1, billing_ref=2, limits_json = "";
insert into subscription_logs set user_ID = 2, sub_ID = NULL, billing_ref=2, limits_json = "";
show warnings;

INSERT INTO account_limits SET user_ID = '1', sub_ID = '1',
                                        billing_ref = '0', limits_json = (select limits_json FROM subscriptions where ID = '1'),
                                        expiration_date = '2014-10-01 00:00:00';
INSERT INTO subscriptions set `name` = 'My first subscription package',`price` = '1.1';