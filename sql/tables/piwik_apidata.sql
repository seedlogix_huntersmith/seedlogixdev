CREATE TABLE `piwik_apidata` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`apimethod` VARCHAR(64) NOT NULL,
	`touchpoint_ID` INT NOT NULL,
	`hub_ID` INT NOT NULL COMMENT 'for temporary use for compatibility with current reports code.  use touchpoinT_ID in the future',
	`tracking_ID` INT NOT NULL,
	`datestr` DATE NOT NULL,
	`json` TEXT NOT NULL,
	PRIMARY KEY (`ID`)
)
COMMENT='stores json results from piwik api calls for each touchpoint, for each touchpoint, for each day, for each api call.'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
