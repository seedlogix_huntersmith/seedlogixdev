select x.*, x.LANDING + x.SOCIAL + x.WEBSITE + x.MOBILE + x.BLOG as TOTAL
	from (
		select c.id, (select count(*) FROM blogs WHERE user_id = c.user_id and cid = c.id AND trashed = 0) as BLOG, 
	sum((`hub`.`touchpoint_type` = 'WEBSITE')) AS `WEBSITE`,
	sum((`hub`.`touchpoint_type` = 'LANDING')) AS `LANDING`,
	sum((`hub`.`touchpoint_type` = 'SOCIAL')) AS `SOCIAL`,
	sum((`hub`.`touchpoint_type` = 'MOBILE')) AS `MOBILE`
	from campaigns c left join hub on (hub.cid = c.id)
	where c.user_id = dashboard_userid()
	group by c.id ) x;

select * from blogs where cid = 249;
select x.*, 
	x.LANDING + x.SOCIAL + x.WEBSITE + x.MOBILE + @blogcount as TOTAL from (
	select `hub`.`cid` AS `cid`,
	sum((`hub`.`touchpoint_type` = 'WEBSITE')) AS `WEBSITE`,
	sum((`hub`.`touchpoint_type` = 'LANDING')) AS `LANDING`,
	sum((`hub`.`touchpoint_type` = 'SOCIAL')) AS `SOCIAL`,
	sum((`hub`.`touchpoint_type` = 'MOBILE')) AS `MOBILE`
	from `hub` 
	where ((`hub`.`user_id` = `dashboard_userid`()) and (`hub`.`trashed` = 0)) group by `hub`.`cid`
	) x;
