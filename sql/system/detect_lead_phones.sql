SET @phone_length = 5;

drop table if exists phones_temp;

-- get lead ids for leads with empty phone values
CREATE TEMPORARY TABLE phones_temp
	SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- obtain phone values where label contains 'contact' and 'phone' in any position
update phones_temp, leads_data 
	set phones_temp.phone = leads_data.value 
		WHERE leads_data.label LIKE '%Phone%' AND leads_data.label LIKE '%Contact%' and length(leads_data.value) > @phone_length
		and trim(leads_data.value) REGEXP '^[-0-9 ()+]+$' AND phones_temp.lead_id = leads_data.lead_id;

-- update prospects table with acquired values
UPDATE prospects, phones_temp 
	SET prospects.phone = trim(phones_temp.phone) 
	WHERE prospects.id = phones_temp.id ;

-- drop temporary table
drop table phones_temp;

-- get lead ids with empty phone values
CREATE TEMPORARY TABLE phones_temp
		SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- obtain values of inputs with labels starting with 'Phone'
update phones_temp, leads_data set phones_temp.phone = leads_data.value 
		WHERE leads_data.label LIKE 'Phone%' and length(leads_data.value) > @phone_length
			and trim(leads_data.value) REGEXP '^[-0-9 ()+]+$'  AND phones_temp.lead_id = leads_data.lead_id;

-- update prospects table with acquired values
UPDATE prospects, phones_temp 
	SET prospects.phone = trim(phones_temp.phone) 
	WHERE prospects.id = phones_temp.id;

-- drop temporary table
drop table phones_temp;

-- get lead ids with empty phone values
CREATE TEMPORARY TABLE phones_temp
		SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- obtain values of inputs with labels starting ending in 'Phone'
update phones_temp, leads_data 
	set phones_temp.phone = leads_data.value 
	WHERE leads_data.label LIKE '%Phone' and length(leads_data.value) > @phone_length
	and trim(leads_data.value) REGEXP '^[-0-9 ()+]+$'  AND phones_temp.lead_id = leads_data.lead_id;

-- update prospects table
UPDATE prospects, phones_temp SET prospects.phone = trim(phones_temp.phone) 
	WHERE prospects.id = phones_temp.id;

drop table phones_temp;

-- obtain values of inputs with labels starting with 'Phone'
CREATE TEMPORARY TABLE phones_temp
	SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- apply last filter, obtain values with 'PHONE' anywhere in the label
update phones_temp, leads_data set phones_temp.phone = leads_data.value 
	WHERE leads_data.label LIKE '%Phone%' and length(leads_data.value) > @phone_length
	and trim(leads_data.value) REGEXP '^[-0-9 ()+]+$' AND phones_temp.lead_id = leads_data.lead_id;

-- update prospect tables with acquired values.
UPDATE prospects, phones_temp SET prospects.phone = trim(phones_temp.phone) WHERE prospects.id = phones_temp.id;

-- drop temporary table
drop table phones_temp;

-- obtain last remaining leads with empty phones
CREATE TEMPORARY TABLE phones_temp
		SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- fid inputs with 'NUMBER' in the label
update phones_temp, leads_data set phones_temp.phone = leads_data.value 
	WHERE leads_data.label LIKE 'Number' and length(leads_data.value) > @phone_length
	and trim(leads_data.value) REGEXP '^[-0-9 ()+]+$' AND phones_temp.lead_id = leads_data.lead_id;

-- update values
UPDATE prospects, phones_temp SET prospects.phone = trim(phones_temp.phone) WHERE prospects.id = phones_temp.id;

-- drop temporary table
drop table phones_temp;

-- obtain lead ids
CREATE TEMPORARY TABLE phones_temp
		SELECT id, lead_ID, phone FROM prospects WHERE phone = '';

-- find inputs in spanish.
update phones_temp, leads_data set phones_temp.phone = leads_data.value
	WHERE leads_data.label LIKE '%Numero%' and leads_data.label like '%Telefono%' and length(leads_data.value) > @phone_length
	and leads_data.value REGEXP '^[-0-9 ()+]+$' AND phones_temp.lead_id = leads_data.lead_id;

-- update with values
UPDATE prospects, phones_temp SET prospects.phone = trim(phones_temp.phone) WHERE prospects.id = phones_temp.id;

SELECT prospects.ID,prospects.lead_ID,leads_data.label,leads_data.value from prospects LEFT JOIN leads_data on prospects.lead_ID = leads_data.lead_id
where phone = '' and leads_data.label like '%Phone%';

# SET @@wait_timeout = 50000;
#
# SELECT @@wait_timeout;
#

# SELECT lead_ID, label, value as phone FROM leads_data l
# where label LIKE '%_Phone%' AND label LIKE '%Contact%' AND value REGEXP '^[-0-9 ()+]+$';
#
#
# drop table phones_temp;
#
# create temporary table phones_temp as
# 	SELECT lead_ID, label, value as phone FROM leads_data l
# 	where label LIKE 'Phone%' AND value REGEXP '^[-0-9 ()+]+$';
#
# update phones_temp, prospects p SET p.phone = phones_temp.phone WHERE p.phone = '' AND phones_temp.lead_id = p.lead_ID;