
--
--
-- select all leads with no names
create temporary table leadnames as 
select l.id, d.value as first_name, repeat(" ", 35) as last_name from leads l, leads_data d 
	where 
		d.lead_id = l.id and l.lead_name = '' and d.value != ''
		and d.label LIKE 'First N%';

-- update the last names
update 
 leadnames l, leads_data d 
	set l.last_name = d.value
	where 
		d.lead_id = l.id and l.last_name = ''
		and d.label like 'Last %';

-- save lead names
update leads l, leadnames n
	set l.lead_name = concat(n.first_name, " ", n.last_name)
	where l.id = n.id and l.lead_name = "";

-- select l.* from leadnames f, leads l where l.id = f.id and l.lead_name != "";
-- update leads l, leadnames n set l.lead_name = "" where l.lead_name != "";

-- select * from leads l;

drop table leadnames;

--
--
-- detect full name fields
-- 
create temporary table leadfullnames as
select l.id, d.label, d.value as fullname from leads l, leads_data d 
	where 
		d.lead_id = l.id and l.lead_name = ''
		and d.label LIKE 'Name%' and d.value != "" 
			and d.label NOT LIKE '%ref%';

-- select * from leadfullnames;

-- save lead names
update leads l, leadfullnames n
	set l.lead_name = n.fullname
	where l.id = n.id and l.lead_name = "";

drop table leadfullnames;



--
-- 
-- detect random names
-- 
create temporary table leadrandomnames as 
select l.id, d.value as randomname
	from leads_data d, leads l
	where 
		(d.label not like 'First%' and d.label not like 'Last%' and d.label LIKE '%name%' and d.label != 'name' and d.value != '') and d.lead_id = l.id
		and l.lead_name = ''
	group by d.lead_id
	order by label = ' First Name' asc;

-- select * from leadrandomnames;

-- save random lead names
update leads l, leadrandomnames n
	set l.lead_name = n.randomname
	where l.id = n.id and l.lead_name = "";


drop table leadrandomnames;

select * from leads where lead_name = '';
