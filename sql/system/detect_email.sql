drop table if exists tmp_emails;

-- obtain ids for blank email values
CREATE TEMPORARY TABLE tmp_emails
		SELECT id, lead_ID, email FROM prospects WHERE email = '';

-- match "EMail" anywhere in label
update tmp_emails, leads_data set tmp_emails.email = leads_data.value 
		WHERE leads_data.label LIKE '%EMail%'
		and trim(leads_data.value) REGEXP '^[A-Z0-9._%+-]+@[A-Z0-9.-]+[[...]][A-Z]{2,4}$' AND tmp_emails.lead_id = leads_data.lead_id;

UPDATE prospects, tmp_emails SET prospects.email = trim(tmp_emails.email) WHERE prospects.id = tmp_emails.id and prospects.email = '';

drop table tmp_emails;

CREATE TEMPORARY TABLE tmp_emails
		SELECT id, lead_ID, email FROM prospects WHERE email = '';

-- match E-Mail anywhere in label
update tmp_emails, leads_data set tmp_emails.email = leads_data.value WHERE leads_data.label LIKE '%E-Mail%'
			and trim(leads_data.value) REGEXP '^[A-Z0-9._%+-]+@[A-Z0-9.-]+[[...]][A-Z]{2,4}$' AND tmp_emails.lead_id = leads_data.lead_id;

UPDATE prospects, tmp_emails SET prospects.email = trim(tmp_emails.email) WHERE prospects.id = tmp_emails.id and prospects.email = '';

drop table tmp_emails;

CREATE TEMPORARY TABLE tmp_emails
		SELECT id, lead_ID, email FROM prospects WHERE email = '';

-- match 'spanish'
update tmp_emails, leads_data set tmp_emails.email = leads_data.value WHERE leads_data.label LIKE '%Correo%' and leads_data.label like '%Electronico'
																			and trim(leads_data.value) REGEXP '^[A-Z0-9._%+-]+@[A-Z0-9.-]+[[...]][A-Z]{2,4}$' AND tmp_emails.lead_id = leads_data.lead_id;

UPDATE prospects, tmp_emails SET prospects.email = trim(tmp_emails.email) WHERE prospects.id = tmp_emails.id and prospects.email = '';

drop table tmp_emails;

CREATE TEMPORARY TABLE tmp_emails
		SELECT id, lead_ID, email FROM prospects WHERE email = '';

-- find any inputs with label matching '%mail%' and value matchign '%@%'
update tmp_emails, leads_data set tmp_emails.email = leads_data.value WHERE leads_data.label LIKE '%mail%' and leads_data.value like '%@%'
	and tmp_emails.lead_id = leads_data.lead_id;

UPDATE prospects, tmp_emails SET prospects.email = trim(tmp_emails.email) WHERE prospects.id = tmp_emails.id and prospects.email = '';

drop table tmp_emails;

-- select count(*) from tmp_emails where email != '';