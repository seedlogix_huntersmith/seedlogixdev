-- select count(*) from 6q_touchpoints where active != 1;
select 'Total Touchpoints:', count(*) from 6q_touchpoints
	union 
select 'Touchpoints with Hostname:', count(*) from 6q_touchpoints where hosthash is not null
	union
select 'Active TPs with Hostname:', count(*) from 6q_touchpoints where hosthash is not null and active != 0
	union
select 'InActive TPs with Hostname:', count(*) from 6q_touchpoints where hosthash is not null and active = 0;

