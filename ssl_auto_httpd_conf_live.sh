#!/bin/bash

# Run from root user folder.

### Turn on debug mode ###
#set -x

### Script Arguments ###
#$1 = URL of the website.
#$2 = IP address of website (pointer)
website_url=$1
website_ip_addr="$2:443"

### Global Vars ###
httpd_conf_file=/usr/local/apache/conf/httpd.conf

###########################################################
## Step 5. Add new url and ip to Apache httpd.conf file. ##
###########################################################
printf "Adding URL and IP address to httpd.conf file...\n"

#Prepare new virtual host XML
virtual_host="\n<VirtualHost ${website_ip_addr}>\n"

virtual_host+="\tServerName ${website_url}\n"
virtual_host+="\tServerAlias www.${website_url} ${website_url} *.${website_url}\n"
virtual_host+="\tDocumentRoot \/usr\/local\/apache\/dev-trunk\/hybrid \n\n"

virtual_host+="\tSSLCertificateFile \/etc\/letsencrypt\/live\/${website_url}\/cert.pem \n"
virtual_host+="\tSSLCertificateKeyFile \/etc\/letsencrypt\/live\/${website_url}\/privkey.pem \n"
virtual_host+="\tInclude \/etc\/letsencrypt\/options-ssl-apache.conf \n"
virtual_host+="\tSSLCertificateChainFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \n"

virtual_host+="\tSSLCACertificatePath \/etc\/letsencrypt\/live\/${website_url}\/ \n"
virtual_host+="\tSSLCACertificateFile \/etc\/letsencrypt\/live\/${website_url}\/fullchain.pem \n"

virtual_host+="<\/VirtualHost>\n"

# -F = fixed string (make list of strings), -q quiet (do not write anything to standard output)
if grep -Fq "${website_ip_addr}" ${httpd_conf_file}
then
    printf "Found matching ip address: \"${website_ip_addr}\"\n"

    # Write to file in area as same ip.
    sed -i "0,/<VirtualHost ${website_ip_addr}>/s//${virtual_host}\n<VirtualHost ${website_ip_addr}>/" ${httpd_conf_file}
else
    printf "No matching ip address found... appending new virtual host to httpd.conf\n."

    # Append to file with new ip.
    printf "\n${virtual_host}" >> ${httpd_conf_file}
fi

#############################
## Step 4: Restart Apache. ##
#############################
sudo apachectl -k restart

# End program.
exit 0
