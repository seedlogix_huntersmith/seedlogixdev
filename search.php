<?php
require_once(dirname(__FILE__) . '/admin/6qube/core.php');
	session_start();
	
	require_once(QUBEADMIN . 'inc/local.class.php');
	$local = new Local();
		
	if($_GET){
		$_SESSION['keyword'] = $_GET['business'];
		$_SESSION['location'] = $_GET['location'];
	}
	
	// ** Header ** //
	require_once('includes/header.inc.php');

?>
    <h1>Search</h1>
<?
	
	$local->displaySearch($_SESSION['keyword'], $_SESSION['location']);
	
	// ** Footer ** //
	require_once('includes/footer.inc.php');
?>